;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

;;; The top-level DUIM library, exports all core functionality
(defpackage-plus-1:defpackage+ :duim
  (:use #:common-lisp)
  (:inherit #:duim-geometry
	    #:duim-DCs
	    #:duim-sheets
	    #:duim-graphics
	    #:duim-layouts
	    #:duim-gadgets
	    #:duim-frames
	    #:duim-recording))

;;; An implementor's library, exports all internal functionality
(defpackage-plus-1:defpackage+ :duim-internals
  (:use #:common-lisp)
  (:inherit #:duim-utilities
	    #:duim-geometry-internals
	    #:duim-DCs-internals
	    #:duim-sheets-internals
	    #:duim-graphics-internals
	    #:duim-layouts-internals
	    #:duim-gadgets-internals
	    #:duim-frames-internals
	    #:duim-recording-internals))
