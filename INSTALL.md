# Introduction

CL-DUIM is a port of the [Dylan][OPENDYLAN] User Interface Manager
(DUIM) to Common Lisp.

# Installing

TLDR; as long as the project was checked out to a location asdf
understands (~/common-lisp/), you should be able to get going just
with:

`(ql:quickload "gui-tests")`

/TLDR;

## Prerequisites

### A Threaded Common Lisp Implementation
Not all lisp implementations handle treading equally, as it is a non
standard (i.e. the CL standard does not mention threading) behaviour.

CCL is known to work well across all supported platforms. SBCL works
well on Linux.

### Install GTK2+
Currently DUIM works with only GTK2+ as a back end. Development takes
place on Linux and MS Windows. First install the latest GTK2 shared
libraries available on your platform. **Note** the 'bit-ness' of the
library must match that of your lisp implementation. I.e. if you are
using a 64 bit lisp, the GTK libraries must also be 64 bit.

On MS Windows you can use [tschoonj's][GTK-WIN-64] libraries that have
been compiled for 64 bit architectures. GTK2 is no longer available
from the gtk.org website. If the directory with the DLLs is in the
PATH environment variable, then you should have no problems loading
the GTK libraries. By default, tschoon's package does this for you
during the install.

### Configure your lisp for UTF-8
The duim-backend/gtk/cffi-gtk-api.lisp file contains a few non-ASCII
characters and SBCL will barf on them unless it is configured for
UTF-8 encodings. To set this, place the following form in your
~/.sbclrc file:

`(setf sb-impl::*default-external-format* :utf-8)`

### Quicklisp
Whilst you technically don't have to have quicklisp, things will be
much simpler if you do, and this manual assumes so.

## Getting the Source Code
Check out the souce into a directory configured for
ASDF. ~/common-lisp/ usually works, but any location will do.

# Loading DUIM
DUIM is loaded like any other Common Lisp package, either with ASDF,
or with Quicklisp, which will handle the dependency loading for
you. To get started quickly try:

`(ql:quickload "backend")`

Run the gui tests with:

`(ql:quickload "gui-tests")`

Load tests with '(asdf:make "duim-tests")'.
Run tests with '(duim-tests::run-all-tests)'.

Load GUI tests: `(asdf:make "gui-tests")`
And run: `(gui-test-suite::main nil)`

You can also try the tic-tac-toe game:
`(ql:quickload "duim-tic-tac-toe")`
`(tic-tac-toe:play-tic-tac-toe)`




[OPENDYLAN]: https://opendylan.org/
[GTK-WIN-64]: https://github.com/tschoonj/GTK-for-Windows-Runtime-Environment-Installer/releases
