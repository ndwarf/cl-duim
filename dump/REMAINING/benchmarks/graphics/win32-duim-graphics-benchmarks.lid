Library:   duim-graphics-benchmarks
Author:    Andy Armstrong
Synopsis:  Interactive benchmarks for DUIM graphics
Files:	  library
	  module
          harness
	  drawing
	  text
          start-benchmarks
Linker-Options: $(guilflags)
Start-Function: main
Copyright:    Original Code is Copyright (c) 1995-2004 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND
Other-files: Open-Source-License.txt

