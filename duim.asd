;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

(asdf:defsystem :duim
  :description  "DUIM API library, exports all core API functionality"
  :version      (:read-file-form "version.sexp")
  :depends-on (#:defpackage-plus
		#:geometry
		#:extended-geometry ; Confusing. Should be in duim-extended?
		#:dcs
		#:sheets
		#:graphics
		#:layouts
		#:gadgets
		#:frames
		#:recording)
  :serial t
  :components ((:file "package"))
  :in-order-to ((test-op (test-op duim-tests))))

(asdf:defsystem :duim/backend
  :description "Backend(s) for DUIM"
  :depends-on (#:backend/gtk2
	       #:backend/debug))
               ;#+linux #:gtk2
	       ;#+win32 #:win32))

