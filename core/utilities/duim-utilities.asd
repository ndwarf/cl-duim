;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

(asdf:defsystem :duim-utilities
  :description  "DUIM utilities"
  :version      (:read-file-form "version.sexp")
  :author       "Scott McKay, Andy Armstrong (Lisp port: Duncan Rose <duncan@robotcat.demon.co.uk>)"
  :licence      "BSD-2-Clause"
  :depends-on (:bordeaux-threads :closer-mop)
  :in-order-to ((test-op (test-op "duim-utilities/tests")))
  :components
  ((:file "package")
   (:file "metaclasses")
   (:file "comparison")
   (:file "basic-macros")
   (:file "basic-defs")
   (:file "strings")
   (:file "protocols")
   (:file "sequences")
   (:file "deque"))
  :serial t)

(asdf:defsystem :duim-utilities/tests
  :description "Unit tests for DUIM utilities"
  :author "Steven Nunez <steve.nunez@symbolics.com.sg"
  :license "Same as DUIM-UTILITIES -- this is part of the DUIM-UTILS library."
  #+asdf-unicode :encoding #+asdf-unicode :utf-8
  :depends-on (#:duim-utilities
               #:fiveam)
  :pathname "tests/"
  :serial t
  :components
  ((:file "test-package")
   (:file "main")
   (:file "unit-tests"))
  :perform (asdf:test-op (o s)
			 (uiop:symbol-call :fiveam :run!
					   (uiop:find-symbol* :all-tests
							      :duim-utilities-tests))))
