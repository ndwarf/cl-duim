;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-UTILITIES -*-
(in-package :duim-utilities)

;;; Debug support

(defparameter *debug-duim-function* nil ;#'format
  "Indicates which function should be invoked by DUIM-DEBUG-MESSAGE
to display debug information. This defaults to NIL (do not write
debug messages), but might otherwise be bound to #'FORMAT or
other procedure that accepts stream, message and argument
parameters (in that order)")

(defparameter *debug-duim-stream* *debug-io*
  "The stream to which DUIM debug messages are written. This defaults
to *DEBUG-IO* but can be rebound to any stream.")

(defun duim-debug-message (message &rest args)
  "Writes 'message' (substituting 'args' appropriately) using the
function bound to *DEBUG-DUIM-FUNCTION*.
If *DEBUG-DUIM-FUNCTION* is bound to NIL, no output is generated."
  (let ((message-function *debug-duim-function*))
    (when message-function
      (apply message-function *debug-duim-stream* message args)
      (terpri *debug-duim-stream*))))


;;; Some Dylan-like predicates
;;; SN: Are these used by Dylan? If not, perhaps it's better to leave
;;; such definitions to the application.

(defun integral? (number)
  "Returns T if 'number' is an integral value and NIL otherwise; this
works for real numbers and doesn't demand a number of type INTEGER."
  (and (typep number 'number)
       (multiple-value-bind (quot rem)
	   (truncate number)
         (declare (ignore quot))
	 (= rem 0))))


(declaim (inline unsupplied?))
(defun unsupplied? (param)
"
Return T if PARAM is EQL to :UNSUPPLIED, otherwise return NIL.
"
  (eql param :unsupplied))


(declaim (inline supplied?))
(defun supplied? (param)
"
Return NIL if PARAM is EQL to :UNSUPPLIED, otherwise return T.
"
  (not (eql param :unsupplied)))


(declaim (inline negative?))
(defun negative? (param)
"
Return T if PARAM is negative (i.e. less than 0) and NIL otherwise.
"
  (< param 0))


(defgeneric instance? (type1 target-type)
  (:documentation
"
Returns T if _type1_ is an instance of _target-type_.
"))

(defmethod instance? (type1 target-type)
  (typep type1 target-type))

(defmethod instance? (type1 (target-type class))
  (typep type1 (class-name target-type)))

(defmethod instance? ((type1 class) (target-type class))
  (subtypep (class-name type1) (class-name target-type)))


;;; Numeric constants

;; Various forms of pi, as single floats

(declaim (type single-float +pi+ +2pi+ +pi/2+))
(defconstant +pi+   (coerce pi 'single-float))
(defconstant +2pi+  (coerce (* pi 2) 'single-float))
(defconstant +pi/2+ (coerce (/ pi 2) 'single-float))


;;; Class validity
(defun required-slot (slot-name class-name)
  (error "Required slot ~a not provided constructing ~a"
	 slot-name
	 class-name))


(defun %STRING-DESIGNATOR->STRING (designator)
"
If _designator_ is a string designator returns the string that it
designates, otherwise raise an error.
"
  (cond ((stringp designator) designator)
	((symbolp designator) (symbol-name designator))
	((characterp designator) (make-string 1 :initial-element designator))
	(t (error "%STRING-DESIGNATOR->STRING - Not a string designator: ~a" designator))))


;;; Versioning
(defun system-version (system-designator)
  "Returns a string that represents the version of the ASDF system"
  (let ((system (asdf:find-system system-designator nil)))
    (when (and system (slot-boundp system 'asdf:version))
      (asdf:component-version system))))


;;; Metaclass support

(defmethod closer-mop:validate-superclass ((class standard-class)
                                           (superclass <abstract-metaclass>))
  t)


(defmethod closer-mop:validate-superclass ((class standard-class)
                                           (superclass <protocol-class-metaclass>))
  t)


(defmethod closer-mop:validate-superclass ((class <abstract-metaclass>)
                                           (superclass <protocol-class-metaclass>))
  t)


(defmethod closer-mop:validate-superclass ((class <abstract-metaclass>)
                                           (superclass standard-class))
  t)


(defmethod closer-mop:validate-superclass ((class <protocol-class-metaclass>)
                                           (superclass standard-class))
  t)


(defmethod make-instance ((class <abstract-metaclass>) &key)
  (error "~A is abstract and should not be instantiated" class))


(defmethod make-instance ((class <protocol-class-metaclass>) &key)
  (error "~A is a protocol class and should not be instantiated. Maybe use ~A?"
	 class
	 (format nil "MAKE-~A"
		 (remove-if (lambda (c)
			      (or (eql c #\<) (eql c #\>)))
			    (symbol-name (class-name class))))))


;;; These were taken from Franz CLIM2. See:
;;; https://github.com/franzinc/clim2/blob/e8d03da80e1f000be40c37d088e283d95365bfdd/utils/lisp-utilities.lisp#L417
;;; and are required for define-protocol-class.
;;; Interning. There are three places where the package matters here:
;;; the current package, the package that is current when FORMAT is
;;; called (matters for ~S &c), and the package into which the symbol
;;; is interned.  The aim is that the first two should always be the
;;; current package, but you get to choose where the symbol is interned
;;; for PACKAGE-FINTERN.
;;;
;;; I am not sure if this stuff is really worth the cost, but...
;;; spr24505, carefully extract symbol-names, so
;;; case-sensitity works better.
(defun package-fintern (package format-string &rest format-args)
  ;; this argument order is unfortunate.
  (declare (dynamic-extent format-args))
  (intern (let ((pkg *package*))
	    (with-standard-io-syntax
		(let ((*package* pkg))
		  (apply #'lisp:format () format-string
			 (mapcar #'(lambda (x)
				     (if (symbolp x)
					(symbol-name x)
				       x))
				 format-args)))))
	  package))

(defun fintern (format-string &rest format-args)
  (declare (dynamic-extent format-args))
  (apply #'package-fintern *package* format-string format-args))


;; Used in generating internal function and method names.
;; (remove-word-from-string "com-" 'com-show-file) => "SHOW-FILE"
;; Always returns a new string that can be bashed to your heart's content
(defun remove-word-from-string (word string-or-symbol &optional only-from-beginning-p)
  (let ((string (etypecase string-or-symbol
		  (string string-or-symbol)
		  (symbol (string string-or-symbol)))))
    (let ((word-position (search word string :test #'char-equal)))
      (cond ((null word-position)
	     (concatenate 'string string))
	    ((zerop word-position)
	     (subseq string (length word)))
	    (only-from-beginning-p
	     (concatenate 'string string))
	    (t
	     (concatenate 'string
			  (subseq string 0 word-position)
			  (subseq string (+ word-position (length word)))))))))
