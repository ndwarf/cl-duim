;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-UTILITIES -*-
(in-package #:duim-utilities)

;;; Dylan (generic) = method

(defgeneric equal? (object1 object2)
  (:documentation
"
Compares two objects for identity.

Returns true if object1 and object2 are equal. Otherwise it returns
false.

Programmers may define methods for = specialized on classes they
define. A programmer may be required to provide an = method when
defining subclasses of some predefined classes in order to fulfil the
protocol of the class, as described below. For objects that do not have
a more specific = method, = returns the same as ==.

= is not guaranteed to return. For example, it may not return when
called on circular structures or otherwise unbounded structures.

In addition to the sealed domains specified by the methods below, = is
sealed over the following domains:

 <object>,    <symbol>
 <symbol>,    <object>
 <object>,    <character>
 <character>, <object>
 <object>,    <boolean>
 <boolean>,   <object>

object1 = object2 => boolean
The default method on = calls == and returns the result returned by ==.

complex1 = complex2 => boolean
Complex numbers are equal if they have the same mathematical value.

collection1 = collection2 => boolean
Two collections are equal if they have identical key-test functions,
they have the same keys (as determined by their key-test function),
the elements of corresponding keys are =, and neither collection is a
dotted list.

sequence1 = sequence2 => boolean
For sequences, = returns true if sequence1 and sequence2 have the
same size and elements with = keys are =, and false otherwise.

list1 = list2 => boolean
For lists, = returns true if the two lists are the same size, corresponding
elements of list1 and list2 are = and the final tails are =. It returns false
otherwise.

list = sequence => boolean
sequence = list => boolean
For mixed lists and sequences, = returns true if the list is not a dotted
list, both have the same size, and elements with = keys are =. It returns
false otherwise.

range1 = range2 => boolean
When called with two ranges, = always terminates, even if one or both
ranges are unbounded in size.
"))


(defmethod equal? (object1 object2)
  (eql object1 object2))


(defmethod equal? ((obj1 number) (obj2 number))
  (= obj1 obj2))


(defmethod equal? ((object1 string) (object2 string))
  (string= object1 object2))


;; complex = complex if they have the same mathematical value
(defmethod equal? ((object1 complex) (object2 complex))
  (and (= (imagpart object1) (imagpart object2))
       (= (realpart object1) (realpart object2))))


(defmethod equal? ((object1 cons) (object2 cons))
  (equalp object1 object2))


(defmethod equal? ((object1 vector) (object2 vector))
  (equalp object1 object2))


;; Dylan's semantics of = on sequences
(defmethod equal? ((object1 sequence) (object2 sequence))
  (map nil #'(lambda (elt1 elt2)
	       (when (not (equal? elt1 elt2))
		 (return-from equal? nil)))
       object1 object2)
  t)


