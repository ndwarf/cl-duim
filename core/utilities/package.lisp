;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

(defpackage :duim-utilities
  (:use #:common-lisp)
  (:export

   ;; metaclasses.lisp
   #:<abstract-metaclass>
   #:<protocol-class-metaclass>

   ;; sequences.lisp
   #:sequence-elt
   #:|(setf duim-utilities:sequence-elt)|
   #:make-simple-vector
   #:make-stretchy-vector
   #:insert-at!
   #:remove-at!
   #:find-pair
   #:make-array-from-contents
   #:fill-array!
   #:push!
   #:remove-all-keys!
   #:get-property
   #:every?
   #:size
   #:find-value
   #:add!
   #:empty?
   #:member?
   #:add-new!
   #:find-key
   #:fill!
   #:add
   #:range
   #:stretchy-vector-p
   #:map-as
   #:sequence-remove!
   #:put-property!
   #:sequence-copy
   #:add-new

   ;; comparison.lisp
   #:equal?

   ;; basic-defs.lisp
   #:*debug-duim-function*
   #:duim-debug-message
   #:integral?
   #:unsupplied?
   #:supplied?
   #:negative?
   #:+pi+
   #:+2pi+
   #:+pi/2+
   #:required-slot
   #:instance?

   ;; protocols.lisp
   #:define-protocol-class
   #:define-protocol-predicate
   #:define-protocol

   ;; basic-macros.lisp
   #:swap!
   #:remove-keywords
   #:with-keywords-removed
   #:dynamic-let
   #:dynamic-bind
   #:with-abort-restart-loop
   #:with-abort-restart
   #:with-stack-vector
   #:%STRING-DESIGNATOR->STRING
   #:without-bounds-checks
   #:define-thread-variable
   ;; FIXME-DO THIS TOO
   ;;	   #:define-thread-slot

   ;; strings.lisp
   #:string-pluralize
   #:string-a-or-an
   #:subsequence-position
   #:string-search-set

   ;; deque.lisp
   #:<deque>
   #:<object-deque>
   #:deque-pop
   #:deque-push
   #:deque-pop-last
   #:deque-push-last
   #:make-deque

   ;; Interning
   #:fintern-package
   #:fintern))

#||
The Dylan definitions below have not be implemented in CL-DUIM

define library duim-utilities
  use functional-dylan;
  use collections;

  export duim-utilities;
  export duim-imports;
end library duim-utilities;


define module duim-utilities
  use dylan;
  use duim-imports;

  // Useful stuff
  export \inc!, \dec!,
         \min!, \max!,
         \pop!, \swap!,

;;; I think there are CL equivelents of these, or third party packages
that provide this functionality, e.g. let-plut for destructuring-let
         \destructuring-let,
         \simple-restart-loop,
	 \with-abort-restart,
         \with-abort-restart-loop,
         \with-restart,
         \with-simple-restart,

  // Stack allocation
  export \with-stack-list, evacuate-list,
	 \with-stack-object, evacuate-object,
         \with-stack-vector, evacuate-vector;

  export <string-or-object-table>,
         substitute, substitute!,
	 range-check;

  //--- Need a way to cheaply get the current time in microseconds
  export get-internal-real-time;

end module duim-utilities;
||#
