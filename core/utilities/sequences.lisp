;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-UTILITIES -*-
(in-package #:duim-utilities)

;;; extract element from a sequence
(defgeneric sequence-elt (sequence index)
  (:documentation "Return the element at _index_ in _sequence_."))
(defmethod sequence-elt ((sequence sequence) (index integer))
  (elt sequence index))
(defmethod sequence-elt ((sequence vector) (index integer))
  (aref sequence index))


;;; update element in a sequence
(defgeneric (setf sequence-elt) (element sequence index)
  (:documentation "Sets the item at _index_ in _sequence_ to be _element_."))
(defmethod (setf sequence-elt) (element (sequence sequence) (index integer))
  (setf (elt sequence index) element))
(defmethod (setf sequence-elt) (element (sequence vector) (index integer))
  (setf (aref sequence index) element))


;;; simple and stretchy vector fabricators
(defun make-simple-vector (&key (size 0) (initial-element nil) (contents nil))
  "Construct a non-adjustable one-dimensional array with _size_ elements where each element is initialized to the value _initial-element_."
  (cond ((and initial-element contents)
	 (error "Can't supply both :initial-element and :contents to MAKE-SIMPLE-VECTOR"))
	(initial-element (make-array size :initial-element initial-element))
	(contents (make-array (size contents) :initial-contents contents))
	(t (make-array size))))

(defun make-stretchy-vector (&key (size 0) (initial-element nil) (contents nil))
  "Construct an adjustable one-dimensional array with _size_ elements where each element is initialized to the value _initial-element_."
  (cond ((and initial-element contents)
	 (error "Can't supply both :initial-element and :contents to MAKE-STRETCHY-VECTOR"))
	(initial-element (make-array size :adjustable t :fill-pointer t :initial-element initial-element))
	(contents (make-array (size contents) :adjustable t :fill-pointer t :initial-contents contents))
	(t (make-array size :adjustable t :fill-pointer t))))

;; TODO: stretch-vector? is a better name; it makes it clear it's from the DUIM code.
(defun stretchy-vector-p (object)
"Returns T if _object_ is a 'stretchy vector' (i.e. a one-dimensional adjustable array) and NIL otherwise."
  (and (vectorp object) (array-has-fill-pointer-p object)))


;;; insert element in sequence
#||
define method insert-at!
    (sv :: <stretchy-object-vector>, item, index)
 => (sv :: <stretchy-object-vector>)
  local method move-up
	    (sv :: <stretchy-object-vector>, index :: <integer>) => ()
	  without-bounds-checks
	    for (i :: <integer> from (size(sv) - 1) to (index + 1) by -1)
	      sv[i] := sv[i - 1]
	    end
	  end
	end method;
  select (index)
    #"start" =>
      sv.size := sv.size + 1;
      move-up(sv, 0);
      sv[0] := item;
    #"end" =>
      add!(sv, item);
    otherwise =>
      sv.size := sv.size + 1;
      move-up(sv, index);
      sv[index] := item;
  end;
  sv
end method insert-at!;
||#

(defgeneric insert-at! (sv item index)
  (:documentation "Insert _item_ into the sequence _sv_ at position
_index_ _index_ may be a numeric index into the sequence, or
:START (in which case _item_ will be inserted at the start of _sv_)or
:END."))

(defmethod insert-at! ((sv vector) item index)
  (if (stretchy-vector-p sv)
      (flet ((move-up (sv index)
	       (loop for i from (- (size sv) 1) downto (+ index 1)
		  do (setf (aref sv i) (aref sv (- i 1))))))
	(when (= (array-dimension sv 0) (fill-pointer sv))
	  (adjust-array sv (+ (array-dimension sv 0) 1)))
	(case index
	  (:start (incf (fill-pointer sv))
		  (move-up sv 0)
		  (setf (aref sv 0) item))
	  (:end   (add! sv item))
	  (t      (incf (fill-pointer sv))
		  (move-up sv index)
		  (setf (aref sv index) item)))
	sv)
      ;; else - not stretchy
      (let ((result (make-simple-vector :size (+ (size sv) 1))))
	(case index
	  (:start (setf (aref result 0) item)
		  (replace result sv :start1 1))
	  (:end (replace result sv)
		(setf (aref result (size sv)) item))
	  (t (replace result sv :end2 index)
	     (setf (aref result index) item)
	     (replace result sv :start1 (+ index 1) :start2 index)))
	result)))

(defmethod insert-at! ((sv list) item index)
  (let ((result (make-list (+ (size sv) 1))))
    (case index
      (:start (setf (nth 0 result) item)
	      (replace result sv :start1 1))
      (:end (replace result sv)
	    (setf (nth (size sv) result) item))
      (t (replace result sv :end2 index)
	 (setf (nth index result) item)
	 (replace result sv :start1 (+ index 1) :start2 index)))
    result))


;;; remove element from sequence
#||
define method remove-at!
    (sv :: <stretchy-object-vector>, index)
 => (sv :: <stretchy-object-vector>)
  local method move-down
	    (sv :: <stretchy-object-vector>, index :: <integer>) => ()
	  without-bounds-checks
	    for (i :: <integer> from index to (size(sv) - 2))
	      sv[i] := sv[i + 1]
	    end
	  end
	end method;
  select (index)
    #"start"  => move-down(sv, 0);
    #"end"    => #f;
    otherwise => move-down(sv, index);
  end;
  sv.size := sv.size - 1;
  sv
end method remove-at!;
||#

(defgeneric remove-at! (sv index)
  (:documentation "Removes the element at position _index_ from the sequence _sv_."))

(defmethod remove-at! ((sv vector) index)
  (assert (stretchy-vector-p sv))
  (flet ((move-down (sv index)
	   (loop for i from index to (- (size sv) 2)
	      do (setf (aref sv i) (aref sv (+ i 1))))))
    (case index
      (:start (move-down sv 0))
      (:end   nil)
      (t      (move-down sv index)))
    (decf (fill-pointer sv)))
  sv)


#||
define method find-pair
    (sequence :: <sequence>, item, #key test = \==) => (result)
  block (return)
    for (pair in sequence)
      when (pair & test(pair[0], item))
	return(pair)
      end
    end;
    #f
  end
end method find-pair;
||#

(defgeneric find-pair (sequence item &key test))

;;; FIXME: remove this method, I don't believe this is ever called on non-vector types
(defmethod find-pair ((sequence sequence)
		      item
		      &key
		      (test #'equal?))
  (loop for index from 0 to (- (length sequence) 1)
     do (let ((pair (elt sequence index)))
	  (when (and pair
		     (funcall test (elt pair 0) item))
	    (return-from find-pair pair)))))

(defmethod find-pair ((sequence vector)
		      item
		      &key
		      (test #'equal?))
  (loop for pair across sequence
       when (and pair (funcall test (elt pair 0) item))
       do (return-from find-pair pair)))

#||

/// Array hacking

// Makes and fills a 2d-array from contents, which is a sequence of sequences
define method make-array-from-contents
    (contents :: <sequence>) => (array :: <array>)
  let nrows :: <integer> = size(contents);
  let ncols :: <integer> = reduce(method (v, x) max(v, size(x)) end, 0, contents);
  let array :: <array>   = make(<array>, dimensions: list(nrows, ncols));
  for (row :: <integer> from 0 below nrows)
    let subcontents = contents[row];
    for (col :: <integer> from 0 below min(size(subcontents), ncols))
      array[row,col] := subcontents[col]
    end
  end;
  array
end method make-array-from-contents;
||#


(defgeneric make-array-from-contents (contents)
  (:documentation "Makes and fills a 2d-array from _contents_, which is a sequence of sequences."))

(defmethod make-array-from-contents ((contents sequence))
  (let* ((nrows (length contents))
	 (ncols (reduce #'(lambda (v x)
			    (max v (length x)))
			contents
			:initial-value 0))
	 (array (make-array (list nrows ncols))))
    (loop for row from 0 below nrows
       do (let ((subcontents (elt contents row)))
	    (loop for col from 0 below (min (length subcontents) ncols)
	       do (setf (aref array row col)
			(elt subcontents col)))))
    array))


#||
// Fills a 2d-array from a sequence
define method fill-array!
    (array :: <array>, sequence :: <sequence>) => (array :: <array>)
  let ncols :: <integer> = dimension(array, 1);
  let row :: <integer> = 0;
  let col :: <integer> = 0;
  fill!(array, #f);
  for (item in sequence)
    array[row, col] := item;
    inc!(col);
    when (col = ncols)
      col := 0;
      inc!(row)
    end
  end;
  array
end method fill-array!;
||#

(defgeneric fill-array! (array sequence)
  (:documentation "Fills a 2d-array from a sequence."))

(defmethod fill-array! ((array array) (sequence cons))
  (let ((ncols (array-dimension array 1))
        (row 0)
        (col 0))
    (fill! array nil)
    (loop for item in sequence
       do (setf (aref array row col) item)
       do (incf col)
       do (when (= col ncols)
	    (setf col 0)
	    (incf row))))
  array)

(defmethod fill-array! ((array array) (sequence vector))
  (let ((ncols (array-dimension array 1))
        (row 0)
        (col 0))
    (fill! array nil)
    (loop for item across sequence
       do (setf (aref array row col) item)
       do (incf col)
       do (when (= col ncols)
	    (setf col 0)
	    (incf row))))
  array)


;; FIXME: remove this method, previous 2 should cover everything used in practice
(defmethod fill-array! ((array array) (sequence sequence))
  (let ((ncols (array-dimension array 1))
        (row 0)
        (col 0))
    (fill! array nil)
    (loop for i from 0 below (length sequence)
       do (setf (aref array row col) (elt sequence i))
       do (incf col)
       do (when (= col ncols)
	    (setf col 0)
	    (incf row))))
  array)


(defgeneric push! (sequence element))

(defmethod push! ((sv vector) element)
  (assert (stretchy-vector-p sv))
  (insert-at! sv element :start))



(defgeneric size (sequence)
  (:documentation "Returns the number of elements in the sequence _sequence_."))

(defmethod size ((sequence list))
  (length sequence))

(defmethod size ((sequence vector))
  (length sequence))

(defmethod size ((sequence hash-table))
  (hash-table-count sequence))

(defmethod size ((sequence array))
  (array-total-size sequence))


(defgeneric (setf size) (size sequence)
  (:documentation "Modifies _sequence_ to contain _size_ elements."))

(defmethod (setf size) ((size integer) (sequence vector))
  (assert (stretchy-vector-p sequence)
	  (sequence)
	  "SEQUENCE must have a fill pointer")
  (setf (fill-pointer sequence) size))



(defgeneric find-value (collection predicate &key skip failure))
(defmethod find-value (collection predicate &key skip failure)
  (declare (ignore collection predicate skip failure))
  (error "FIND-VALUE is valid only for sequences, arrays and hashtables"))

(defmethod find-value ((collection list)
 		       predicate
 		       &key
 		       (skip 0)
 		       (failure nil))
  (loop for element in collection
 	with skipped = 0
 	do (when (funcall predicate element)
 	     ;; Found a match; check if we want this (skipped >= skip)
 	     (if (>= skipped skip)
 		 (return-from find-value element)
 	       ;; We don't want it...
 	       (incf skipped))))
   failure)

(defmethod find-value ((vec vector) predicate &key
		       (skip 0)
		       failure)
  (loop for element across vec
	with skipped = 0
	when (funcall predicate element)
	if (>= skipped skip)
	;; TODO: Maybe this should return the 'rest' of the list?
	;; Would that be more useful? Find call points and work out
	;; what is being done.
	do (return-from find-value element)
	else
	do (incf skipped))
  failure)


(defgeneric add! (sequence element)
  (:documentation
"
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;               add!                               [Open Generic Function]
;;;
;;;               Adds an element to a sequence.
;;;
;;; Signature:    add! source-sequence new-element => result-sequence
;;;
;;; Arguments:    source-sequence  An instance of <sequence>.
;;;               new-element      An instance of <object>.
;;;
;;; Values:       result-sequence  An instance of <sequence>.
;;;
;;; Description:  Returns a sequence that contains new-element and all the
;;;               elements of source-sequence. The result-sequence may or
;;;               may not be freshly allocated. It may share structure
;;;               with a preexisting sequence. source-sequence and
;;;               result-sequence may or may not be ==.
;;;
;;;               source-sequence may be modified by this operation.
;;;
;;;               result-sequence's size is one greater than the size of
;;;               source-sequence. The generic function add! doesn't specify
;;;               where the new element will be added, although individual
;;;               methods may do so.
;;;
;;;               define variable *numbers* = list (3, 4, 5)
;;;               add! (*numbers*, 1) => #(1, 3, 4, 5)
;;;               *numbers* => {undefined}
;;;
;;;               add! deque new-value => deque [Sealed G.F. Method]
;;;
;;;               The result of add! on a deque is == to the deque argument,
;;;               which is modified by this operation. add! adds new-element
;;;               at the beginning of the deque.
;;;
;;;               add! stretchy-vector new-element =>
;;;                                     stretchy-vector [Sealed G.F. Method]
;;;
;;;               The result of add! on a stretchy vector is == to the
;;;               stretchy-vector argument, which is modified by this
;;;               operation.
;;;
;;;               add! adds new-element at the end of the stretchy-vector.
;;;
;;;               add! list element => pair [Sealed G.F. Method]
;;;
;;;               The result of add! on a list is equivalent to (pair element
;;;               list).
;;;               The result will share structure with the list argument, but
;;;               it will not be == to the argument, and the argument will
;;;               not be modified.
"))
(defmethod add! (set element)
  (declare (ignore set element))
  (error "ADD! is valid only for sequences"))
(defmethod add! ((set list) element)
  (cons element set))
(defmethod add! ((set vector) element)
  (if (stretchy-vector-p set)
      (vector-push-extend element set)
      ;; FIXME: Is this right? Maybe we should just return a new vector
      ;; with the appropriate elements...
      (error "Can't ADD! to vector with no fill pointer"))
  set)


(defgeneric empty? (sequence)
  (:documentation
"
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;              empty?                              [Open Generic Function]
;;;
;;;              Returns true if its argument is empty.
;;;
;;; Signature:    empty? object => boolean
;;; Arguments:    _object_    An instance of <object>.
;;; Values:       _boolean_   An instance of <boolean>.
;;;
;;; Description: Returns true if object is empty. Otherwise returns #f.
;;;
;;;             empty? collection => boolean [G.F. Method]
;;;
;;; A set of methods defined for the class <collection> returns true
;;; if the collection has zero elements."))

(defmethod empty? ((object t))
  (error "EMPTY? is valid only for sequences, arrays and hashtables"))
(defmethod empty? ((object sequence))
  (= (length object) 0))
(defmethod empty? ((object hash-table))
  (= (hash-table-count object) 0))
(defmethod empty? ((object vector))
  (= (length object) 0))
(defmethod empty? ((object array))
  (= (array-total-size object) 0))


(defgeneric member? (element sequence)
  (:documentation "Like CL:MEMBER, except works on both lists and vectors.
Returns either element or NIL. Doesn't accept additional arguments (but what is presented here is sufficient for DUIM)."))
(defmethod member? (element (sequence sequence))
  (find element sequence))


(defgeneric add-new! (sequence element &key test)
  (:documentation "Adds element to set iff it is not already present in set (as determined by _test_). May be destructive to _sequence_."))

(defmethod add-new! (set element &key test)
  (declare (ignore set element test))
  (error "ADD-NEW! is valid only for sequences"))

(defmethod add-new! ((set sequence) element &key (test #'eql))
  (let ((present (find-value set #'(lambda (cf)
				     (funcall test element cf)) :failure nil)))
    (if present
	set
	(add! set element))))

(defgeneric add-new (sequence element &key test)
  (:documentation "Adds element to set iff it is not already present in set (as determined by _test_). Does not mutate _sequence_."))

(defmethod add-new ((set list) element &key (test #'eql))
  (let ((present (find-value set #'(lambda (cf) (funcall test element cf)) :failure nil)))
    (if present
	set
	(cons element set))))

(defmethod add-new ((set vector) element &key (test #'eql))
  (let ((present (find-value set #'(lambda (cf) (funcall test element cf)) :failure nil)))
    (if present
	set
	(if (stretchy-vector-p set)
	    (let ((copy (sequence-copy set)))
	      (add! copy element))
	    ;; else
	    (let ((fresh (make-simple-vector :size (+ (size set) 1))))
	      (map-into fresh #'identity set)
	      (setf (aref fresh (size set)) element)
	      fresh)))))


(defgeneric find-key (collection predicate &key skip failure)
  (:documentation
"
;;; FIND-KEY:

;;; Returns the key in a collection such that the corresponding collection
;;; element satisfies a predicate.

;;; (FIND-KEY COLLECTION PREDICATE &KEY SKIP FAILURE) => KEY

;;; Returns a key value such that (funcall predicate element) => T. If no
;;; element satisfies the predicate, find-key returns FAILURE.

;;; The SKIP argument indicates that the first SKIP matching elements should be
;;; ignored. If SKIP or fewer elements of COLLECTION satisfy PREDICATE then
;;; FAILURE is returned.
"))

(defmethod find-key (collection predicate &key skip failure)
  (declare (ignore collection predicate skip failure))
  (error "FIND-KEY is valid only for sequences, arrays and hashtables"))

(defmethod find-key ((seq list) predicate &key
		     (skip 0)
		     failure)
  (loop for element in seq
	for index from 0
	with skipped = 0
	when (funcall predicate element)
	if (>= skipped skip)
	do (return-from find-key index)
	else
	do (incf skipped))
  failure)

(defmethod find-key ((vec vector) predicate &key
		     (skip 0)
		     failure)
  (loop for element across vec
	for index from 0
	with skipped = 0
	when (funcall predicate element)
	if (>= skipped skip)
	do (return-from find-key index)
	else
	do (incf skipped))
  failure)

(defmethod find-key ((collection hash-table)
		     predicate
		     &key
		     (skip 0)
		     (failure nil))
  (loop for key being each hash-key in collection
	with skipped = 0
	do (when (funcall predicate (gethash key collection))
	     (if (>= skipped skip)
		 (return-from find-key key)
	       (incf skipped))))
  failure)

(defmethod find-key ((collection array)
		     predicate
		     &key
		     (skip 0)
		     failure)
  (declare (ignore predicate skip failure))
  (error "FIXME: FIND-KEY for ARRAY types : array provided = ~a" collection))


(defgeneric fill! (collection element &key start end)
  (:documentation
"
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;              fill!                               [open generic function]
;;;
;;;              Fills a collection with a specified value.
;;;
;;; Signature:   fill! _mutable-collection_ _value_ #key _start_ _end_
;;;                    => _mutable collection_
;;;
;;; Arguments:   _mutable-collection_  An instance of <mutable-collection>
;;;              _value_               An instance of <object>
;;;              _start_               An instance of <integer>
;;;              _end_                 An instance of <integer>
;;;
;;; Values:      _mutable-collection_  An instance of <mutable-collection>
;;;
;;; Description: Modifies _mutable collection_ so that
;;;              element(_mutable collection_, _key_) returns _value_
;;;              for every _key_.
;;;
;;;              If _mutable collection_ is a sequence, then _start_ and
;;;              _end_ keywords may be specified to indicate that only a
;;;              part of the sequence should be filled. _start_ is
;;;              considered an inclusive bound and defaults to 0; _end_
;;;              is an exclusive bound and defaults to the length of the
;;;              sequence.
;;;
;;;              define variable numbers = list(10, 13, 16, 19)
;;;              fill!(numbers, 3, start: 2)
;;;                => #(10, 13, 3, 3)
"))

(defmethod fill! (sequence value &key start end)
  (declare (ignore sequence value start end))
  (error "FILL! is valid only for sequences and arrays"))

(defmethod fill! ((obj sequence)
		  value
		  &key
		  (start 0)
		  (end (length obj)))
  (when (> end (length obj))
    (setf end (length obj)))
  (loop for i from start below end
	do (setf (elt obj i) value))
  obj)

(defmethod fill! ((sequence array)
 		  value
 		  &key
 		  (start 0)
 		  (end (array-total-size sequence)))
"
Destructively modify SEQUENCE so that the elements between
:START and :END are all EQ to VALUE.

If :START is unspecified it defaults to 0.
If :END is unspecified, it defaults to the index of the last element
in SEQUENCE.
If :END is specified to be greater than the number of elements in
SEQUENCE, the default value for :END is used.
"
  (when (> end (array-total-size sequence))
    (setf end (array-total-size sequence)))
  (loop for i from start below end
	do (setf (row-major-aref sequence i) value))
  sequence)


(defgeneric add (sequence element)
  (:documentation "like ADD! but non-destructive"))
(defmethod add (sequence element)
  (declare (ignore sequence element))
  (error "ADD is valid only for sequences"))
(defmethod add ((seq list) new)
  ;; Share structure (permissable), leaving original list undisturbed.
  (cons new seq))
(defmethod add ((seq sequence) new)
;;  (format t "ADD on ~a entered; adding ~a" seq new)
  (let* ((orig-length+1 (1+ (length seq)))
	 (result (make-array orig-length+1)))
    ;; FIXME: maybe we could do better by displacing the original
    ;; vector? Look into it.
    (setf (aref result 0) new)
    (loop for i from 1 below orig-length+1
	 do (setf (aref result i)
		  (aref seq (1- i))))
    (format t "    - returning ~a" result)
    result))


(defun range (&key ((:from _from) 0) ((:to _to)) ((:below _below)) ((:by _by) 1))
"
Generates a list containing the values indicated by the
arguments.
  :FROM  - the first element value
  :TO    - the last element value
  :BELOW - the last element value will be smaller than this
           value
  :BY    - the increment between values
"
  (assert (not (zerop _by))
	  (_by)
	  "Can't use increment of zero")
  ;; TODO: There are other combinations of parameters that will cause
  ;; this to blow out, but these are sufficient for DUIM's usage at
  ;; the moment.
  (cond ;;((and _from _to (= _from _to)) #())  ;; Is this what's wanted?
	(_to (if (plusp _by)
		 (loop for item from _from to _to by _by collecting item)
		 (loop for item from _from downto _to by _by collecting item)))
	(_below (if (plusp _by)
		    (loop for item from _from below _below by _by collecting item)
		    (error "Can't use negative increment in RANGE with :BELOW key argument")))
	(t (error "One of :TO or :BELOW must be provided."))))


(defgeneric remove-all-keys! (table)
  (:documentation "Remove all keys from the hash-table _table_."))
(defmethod remove-all-keys! ((table hash-table))
  (clrhash table))


(defgeneric get-property (properties key &key default)
  (:documentation "Return the value associated with _key_ in _properties_, or _default_ if not found."))
(defmethod get-property ((properties list) key &key (default nil))
  (getf properties key default))


(defgeneric put-property! (properties key value)
  (:documentation "Store _key_ and _value_ in the property list _properties_."))
(defmethod put-property! ((properties list) key value)
  (setf (getf properties key) value))


(defgeneric every? (predicate-fn sequence)
  (:documentation "Returns T if every element in _sequence_ generates T when applied to _predicate-fn_, and NIL otherwise."))
(defmethod every? ((predicate-fn function) (sequence sequence))
  (every predicate-fn sequence))


(defgeneric map-as (type function sequence &key reuse?)
  (:documentation "Applies _function_ to each element of _sequence_ storing the result in an object with the same type as that passed in."))

(defmethod map-as ((type vector) (function function) (sequence sequence) &key (reuse? t))
  (if (array-has-fill-pointer-p type)
      (if reuse?
	  ;; reuse the vector passed in
	  (progn
	    (setf (size type) 0)
	    (map nil #'(lambda (element)
			 (let ((result (funcall function element)))
			   (add! type result)))
		 sequence)
	    type)
	  ;; else - don't reuse, make a new stretchy vector
	  (let ((result-sequence (MAKE-STRETCHY-VECTOR)))
	    (map nil #'(lambda (element)
			 (let ((result (funcall function element)))
			   (add! result-sequence result)))
		 sequence)
	    result-sequence))
      ;; else - no fill pointer, construct a new array
      (map 'vector function sequence)))

(defmethod map-as ((type list) (function function) (sequence sequence) &key (reuse? t))
  (declare (ignore reuse?))
  (map 'list function sequence))



(defgeneric sequence-remove! (sequence element)
  (:documentation "Remove _element_ from _sequence_. Returns _sequence_."))
(defmethod sequence-remove! ((sequence sequence) element)
  (delete element sequence))


(defgeneric sequence-copy (sequence &key start end)
  (:documentation "Creates a copy of _sequence_. The result will have the same type as _sequence_ and will contain the same element references."))
(defmethod sequence-copy ((sequence vector) &key start end)
  (if (or start end)
      (if end
	  (subseq sequence (or start 0) end)
	  (subseq sequence start))
      (copy-seq sequence)))
