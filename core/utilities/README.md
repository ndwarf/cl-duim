# DUIM Collection notes

DUIMs class hierarchy, taken from Dylan's, differs from the common lisp collection hierarchy in that the superclss of all collections in Dylan is _collection_, whereas in common lisp there are two, _array_ and _sequence_ (with superclass _t_).

![Dylan collection hierarchy][DYLAN-COLLECTION-HIERARCHY]

![Common Lisp collection hierarchy][LISP-COLLECTION-HIERARCHY]

At some point it may make sense to create a CL collections library, likely based on [extensible sequences][EXTENSIBLE-SEQUENCES], layering that protocol/API over hash tables and sets.


[DYLAN-COLLECTION-HIERARCHY] https://opendylan.org/books/dpg/_images/figure-11-1.png
[LISP-COLLECTION-HIERARCHY] http://sellout.github.io/media/CL-type-hierarchy.png
[EXTENSIBLE-SEQUENCES] http://www.sbcl.org/manual/index.html#Extensible-Sequences

