;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-UTILITIES -*-
(in-package :duim-utilities)

;;; Taken from Franz CLIM2, MIT style license
;;; https://github.com/franzinc/clim2/blob/4d4712f28b397523d8b30059de60d5b48f52a1b7/utils/lisp-utilities.lisp#L734
(defun remove-keywords (list keywords)
  (macrolet ((remove-keywords-1 (name-var predicate-form)
	       `(let ((head nil)
		      (tail nil))
		  (do ()
		      ((null list))
		    (let ((,name-var (pop list))
			  (value (pop list)))
		      (unless ,predicate-form
			(setq tail (setq head (list ,name-var value)))
			(return))))
		  (do ()
		      ((null list) head)
		    (let ((,name-var (pop list))
			  (value (pop list)))
		      (unless ,predicate-form
			(setq tail (setf (cddr tail) (list ,name-var value)))))))))
    (cond ((null list) nil)
	  ((null keywords) list)
	  ;; Special case: use EQ instead of MEMBER when only one keyword is supplied.
	  ((null (cdr keywords))
	   (let ((keyword (car keywords)))
	     (remove-keywords-1 name (eq name keyword))))
	  (t
	   (remove-keywords-1 name (member name keywords))))))

;; This is slightly different than the Franz CLIM2 version. Easier to
;; update lambda list with "=" (oddly, ignored) here than to change
;; all the callers.
;; SN: not quoting is a bit confusing, but was in the inherited DUIM version. 
(defmacro with-keywords-removed ((new-list = list keywords-to-remove) &body body)
    "Binds NEW-VAR to VAR with the keyword arguments specified in KEYWORDS removed.
NOTE: the LIST of KEYWORDS should NOT be quoted." 
  (declare (ignore =))
  `(let ((,new-list (remove-keywords ,list ',keywords-to-remove)))
     ,@body))


(defmacro with-stack-vector ((name = &rest elements) &body body)
  (declare (ignore =))
  `(let ((,name (vector ,@elements)))
     (declare (dynamic-extent ,name))
     ,@body))


(defmacro dynamic-bind ((&rest binding-forms) &body body)
  `(let ((orig-vals  (vector ,@(loop for form in binding-forms
				     collecting (first form)))))
     ,@(loop for form in binding-forms
	     collecting `(setf ,(first form) ,(third form)))
     (let ((result (progn
		     ,@body)))
       ,@(loop for form in binding-forms
	       for count from 0
	       collecting `(setf ,(first form) (aref orig-vals ,count)))
       result)))


(defmacro dynamic-let ((&rest dl-binding-forms) &body dl-body)
  "DYNAMIC-LET is used to locally bind variables declared with a
DEFINE-THREAD-VARIABLE form."
  `(let ,dl-binding-forms
     ,@dl-body))


;;; Conditions and restarts

#||
define macro with-abort-restart
  { with-abort-restart ()
      ?:body
    end }
  =>
  { block ()
      ?body
    exception (<abort>)
      values(#f, #t)
    end }
end macro with-abort-restart;
||#

(defmacro with-abort-restart (&body body)
  `(restart-case
       (progn ,@body)
     (abort () (values nil t))))

#||
define macro with-abort-restart-loop
  { with-abort-restart-loop (?format-string:expression, ?format-args:*)
      ?:body
    end }
  =>
  { with-simple-restart (?format-string, ?format-args)
      while (#t)
        with-abort-restart ()
          ?body
        end
      end
    end }
end macro with-abort-restart-loop;
||#

(defmacro with-abort-restart-loop ((format-string &rest format-args)
				   &body body)
  `(with-simple-restart (restart ,format-string ,@format-args)
     (loop do (with-abort-restart
		,@body))))


#||
define macro simple-restart-loop
  { simple-restart-loop (?format-string:expression, ?format-args:*)
      ?:body
    end }
    => { with-simple-restart (?format-string, ?format-args)
	   while (#t)
	     ?body
	   end
         end }
end macro simple-restart-loop;
||#



;;; Additional thread support

(defmacro define-thread-variable (name initval)
  `(progn
     (defparameter ,name ,initval)
     (setf bordeaux-threads:*default-special-bindings*
	   (acons ',name ',name bordeaux-threads:*default-special-bindings*))))


#||
// Per-thread (i.e., dynamically bindable) slots
//--- It would be nice if Threads did this natively...
//---*** WEBSTER 9000 BUG: Defining the method as inline kills the compiler!
define macro thread-slot-definer
  { define thread-slot ?:name :: ?type:expression of ?class:expression}
    => { define /* inline */ method ?name ## "-dynamic-binder"
	     (new :: ?type, continuation :: <function>, object :: ?class) => (#rest values)
	   let old = object.?name;
	   block ()
	     object.?name := new;
	     continuation()
	   cleanup
	     object.?name := old;
	   end
	 end method }
  { define thread-slot ?:name :: ?type:expression}
    => { define thread-slot ?name :: ?type of <object> }
  { define thread-slot ?:name of ?class:expression}
    => { define thread-slot ?name :: <object> of ?class }
  { define thread-slot ?:name }
    => { define thread-slot ?name :: <object> of <object> }
end macro thread-slot-definer;

//--- Why Tony won't provide this is beyond me...
define method destroy-thread (thread)
  #f
end method destroy-thread;

||#

(defmacro without-bounds-checks (&body body)
    "Runs BODY with bounds checking supressed."
  `(locally (declare (optimize (speed 3) (safety 0) (space 0)))
     ,@body))

