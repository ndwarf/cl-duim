;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-UTILITIES -*-
(in-package :duim-utilities)

#||
/// Protocols

// Given a name, defines an abstract class (no slots, but may have methods
// that are implemented purely by calling functions in the advertised protocol),
// and the predicates.  For example, 'define protocol class sheet (object) end'
// creates the class <sheet>, the generic function 'sheet?', and two methods for
// 'sheet?', one on <sheet> and one on <object>.
define macro protocol-class-definer
  { define protocol-class ?:name (?supers:*) ?slots:* end }
    => { define open abstract class "<" ## ?name ## ">" (?supers)
           ?slots
	 end class;
         define protocol-predicate ?name; }
 slots:
  { } => { }
  { ?slot:*; ... } => { ?slot; ... }
 slot:
  { virtual slot ?:variable, #rest ?options:expression }
    => { virtual slot ?variable, ?options }
end macro protocol-class-definer;
||#

(defmacro define-protocol-class (name &optional superclasses slots &rest options)
  (let* ((symbol-name (symbol-name name))
         (class-name (intern (concatenate 'string "<" symbol-name ">")))
	 (predicate-name (if (find #\- (string symbol-name))
			     (fintern "~A-~A" symbol-name 'p)
			   (fintern "~A~A" symbol-name 'p)))
	 (predicate-docstring (concatenate 'string "Returns t if object is a " symbol-name ", otherwise returns nil")))
    `(progn
       (defclass ,class-name ,superclasses ,slots
	 (:metaclass <protocol-class-metaclass>)
	 ,@options)
       (let ((the-class (find-class ',class-name)))
	 #-genera (setf (documentation the-class 'type) "DUIM protocol class")
	 #+genera (setf (documentation the-class) "DUIM protocol class"))
       (defgeneric ,predicate-name (object)
	 (:method ((object t))
	   nil)
	 (:method ((object ,class-name))
	   t)
	 (:documentation ,predicate-docstring))
       ',class-name)))


#||
define macro protocol-predicate-definer
  { define protocol-predicate ?:name }
    => { define open generic ?name ## "?" (x) => (true? :: <boolean>);
         define method ?name ## "?" (x :: "<" ## ?name ## ">") => (true? :: <boolean>) #t end;
         define method ?name ## "?" (x :: <object>) => (true? :: <boolean>) #f end; }
end macro protocol-predicate-definer;
||#


(defmacro define-protocol-predicate (name)
  (let* ((symbol-name (symbol-name name))
         (symbol-symbol (intern (concatenate 'string symbol-name "?"))))
    `(progn
      (defgeneric ,symbol-symbol (object))
      (defmethod  ,symbol-symbol ((object t)) nil)
      (defmethod  ,symbol-symbol ((object ,(intern (concatenate 'string "<" symbol-name ">"))))
      t))))


#||
define macro protocol-definer
  //--- We don't use the name or supers yet...
  { define protocol ?:name (?supers:*) ?slots-and-generics:* end }
    => { ?slots-and-generics }
 slots-and-generics:
  { } => { }
  { ?slot-or-generic:*; ... }
    => { ?slot-or-generic; ... }
 slot-or-generic:
  { getter ?getter-name:name ?getter-arglist:* => ?values:* }
    => { define open generic ?getter-name ?getter-arglist => ?values }
  { getter ?getter-name:name ?getter-arglist:* }
    => { define open generic ?getter-name ?getter-arglist }
  { setter ?setter-name:name ?setter-arglist:* => ?values:* }
    => { define open generic ?setter-name ?setter-arglist => ?values }
  { setter ?setter-name:name ?setter-arglist:* }
    => { define open generic ?setter-name ?setter-arglist }
  { function ?function-name:name ?function-arglist:* => ?values:* }
    => { define open generic ?function-name ?function-arglist => ?values }
  { function ?function-name:name ?function-arglist:* }
    => { define open generic ?function-name ?function-arglist }
end macro protocol-definer;
||#

#||
define macro protocol-definer
  //--- We don't use the name or supers yet...
  { define protocol ?:name (?supers:*) ?slots-and-generics:* end }
    => { ?slots-and-generics }
 slots-and-generics:
  { } => { }
  { ?slot-or-generic:*; ... }
    => { ?slot-or-generic; ... }
 slot-or-generic:
  { getter ?getter-name:name ?getter-arglist:* => ?values:* }
    => { define open generic ?getter-name ?getter-arglist => ?values }
  { getter ?getter-name:name ?getter-arglist:* }
    => { define open generic ?getter-name ?getter-arglist }
  { setter ?setter-name:name ?setter-arglist:* => ?values:* }
    => { define open generic ?setter-name ?setter-arglist => ?values }
  { setter ?setter-name:name ?setter-arglist:* }
    => { define open generic ?setter-name ?setter-arglist }
  { function ?function-name:name ?function-arglist:* => ?values:* }
    => { define open generic ?function-name ?function-arglist => ?values }
  { function ?function-name:name ?function-arglist:* }
    => { define open generic ?function-name ?function-arglist }
end macro protocol-definer;
||#

(defmacro define-protocol (name supers &rest slots-and-generics)
  (declare (ignore name supers))
  `(progn
     ,@(loop for slot-or-generic in slots-and-generics
	     collecting (let ((type (car slot-or-generic))
			      (name (cadr slot-or-generic))
			      (args (cddr slot-or-generic)))
			  (ecase type
			    ('defgeneric
			      slot-or-generic)
			    (:getter
			     ;; (:getter name (args) [=> return values])
			     `(defgeneric ,name ,@args))
			    (:setter
			     ;; (:setter (setf name) (args) [=> return values])
			     `(defgeneric ,name ,@args))
			    (:function
			     ;; (:function name (args) [=> return values])
			     `(defgeneric ,name ,@args)))))))
