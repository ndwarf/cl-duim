;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-UTILITIES -*-
(in-package #:duim-utilities)

;;; Cool string hacks

(defun string-pluralize (string &key count)
  "Pluralizes the last word in /string/ (for example, child -\> children,
man -\> men, watch -\> watches). If the pluralization for a specific
number of nouns is required (e.g. 3 objects of type 'vector'), provide
the count as the keyword /:count/.

Passing a /:count/ of 1 prevents pluralization."
  (let ((length (length string))
	(pos (1+ (or (string-search-set string
                                        #(#\Space #\Tab)
                                        :from-end? t)
		     -1))))
    (if (or (= length 0)
	    (and count (= count 1)))
	string
      (let ((flush nil)
	    (suffix nil)
	    (last-char (schar string (1- length)))
	    (penult-char (if (> length 1)
			     (schar string (- length 2))
			   #\*)))
	(cond ((and (char-equal last-char #\y)
		    (not (member penult-char (coerce "aeiou" 'list))))
	       (setf flush 1)
	       (setf suffix "ies"))
	      ((or (string-equal string "ox" :start1 pos)
		   (string-equal string "vax" :start1 pos))
	       (setf suffix "en"))
	      ((and (>= length 5)
		    (string-equal string "index" :start1 (- length 5)))
	       (setf flush 2)
	       (setf suffix "ices"))
	      ((or (and (char-equal last-char #\h)
			(member penult-char (coerce "cs" 'list)))
		   (member last-char (coerce "szx" 'list)))
	       (setf suffix "es"))
	      ((and (>= length 3)
		    (string-equal string "man" :start1 (- length 3))
		    (not (string-equal string "human" :start1 pos)))
	       (setf flush 2)
	       (setf suffix "en"))
	      ((and (>= length 3)
		    (string-equal string "ife" :start1 (- length 3)))
	       (setf flush 2)
	       (setf suffix "ves"))
	      ((and (>= length 5)
		    (string-equal string "child" :start1 (- length 5)))
	       (setf suffix "ren"))
	      (t
	       (setf suffix "s")))
	(concatenate 'string
		     (if flush
			 (copy-seq (subseq string 0 (- length flush)))
		       string)
		     suffix)))))


#||
// Returns an article to be used with the specified string
// We admittedly heuristicate...
||#

(defun string-a-or-an (string)
  (let ((length (length string)))
    (if (= 0 length)
	""
        ;; else
      (let ((char (schar string 0)))
	(if (digit-char-p char)
	    (string-a-or-an (pronounce-string string))   ; pronounce leading digits number
	  (cond ((or (string-equal string "one")
		     (and (>= length 4)
			  (string-equal string "one" :end1 4)))
		 "a ")
		((or (= length 1)
		     ;; "an x" but "a xylophone"; "an fff" but "a frog"
		     (not (string-search-set string "aeiou"))
		     ;; "an xl400" but "a xylophone"
		     (string-search-set string "0123456789"))
		 (if (member char (coerce "aefhilmnorsx" 'list))
		     "an "
		   "a "))
		(t
		 (if (or (member char (coerce "aio" 'list))
			 ;; "an egg" but "a eunich"
			 (and (char-equal char #\e)
			      (not (string-equal string "eu" :end1 2)))
			 ;; "an umbrella" but "a unicorn"
			 ;; "a uniform" but "an uninformed ..."
			 ;; And of course, "a unix"
			 (and (char-equal char #\u)
			      (not (and (string-equal string "uni" :end1 3)
                                        (or (< length 5)
                                            ;; treat "y" as a vowel here, eg. "unicycle"
                                            (not (member (schar string 4)
                                                         (coerce "bcdfghjklmnpqrstvwxz"
                                                                 'list))))))))
		     "an "
		   "a "))))))))


(defun pronounce-string (string)
  (format nil "~R" (parse-integer string :junk-allowed t)))


;; Find any of the given characters within a string
(defun string-search-set (string
                          char-set
                          &key
                          ((:start _start) 0)
                          ((:end _end) (length string))
                          from-end?
                          (test #'char-equal))
"
Find the position of any of the characters in CHAR-SET in the
string STRING. Returns the position of the first character found
that exists in CHAR-SET, or NIL if none of the elements of STRING
exist in CHAR-SET. CHAR-SET should be an array of characters (either
a string, or a vector).
"
  (let ((set-length (length char-set)))
    (if from-end?
        (loop for i = (- _end 1) then (- i 1)
              until (< i _start)
              do (let ((char (char string i)))
                   (loop for j from 0 below set-length
                         when (funcall test char (aref char-set j))
                         do (return-from string-search-set i))))
        ;; else
        (loop for i = _start then (+ i 1)
              until (>= i _end)
              do (let ((char (char string i)))
                   (loop for j from 0 below set-length
                         when (funcall test char (aref char-set j))
                         do (return-from string-search-set i)))))))



(defun subsequence-position (big
                             pat
			     &key
                             (test #'eq)
                             (count 1))
"
 SUBSEQUENCE-POSITION (BIG PATTERN                                 [FUNCTION]
                      &KEY (TEST #'eq)
                      (COUNT 1))

 Searches BIG for a subsequence that is element-for-element equal to
 PATTERN, as determined by the TEST argument. 

 TEST is applied to elements of successive subsequences of BIG and
 corresponding elements of the pattern to determine whether a match has
 occurred. If a subsequence is found, SUBSEQUENCE-POSITION returns the
 index at which the subsequence starts; otherwise, it returns #f. If
 there is more than one match, COUNT determines which subsequence is
 selected. A COUNT of 1 (the default) indicates that the first match
 should be returned. 

 Example usage:

   (subsequence-position \"Ralph Waldo Emerson\" \"Waldo\") => 6"
  ;; DR: Kludge to handle COUNT parameter... there must be a better way.
  (if (eql count 1)
      (search pat big :test test)
      ;; else
      (let ((start-idx 0))
        (loop repeat count
              do (setf start-idx (search pat
                                         big
                                         :test test
                                         :start2 (1+ start-idx)))
              when (null start-idx)
              do (return-from subsequence-position nil))
        start-idx)))
