;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-UTILITIES -*-
(in-package :duim-utilities)


(defclass <abstract-metaclass> (standard-class)
  ())


(defclass <protocol-class-metaclass> (<abstract-metaclass>)
  ())

