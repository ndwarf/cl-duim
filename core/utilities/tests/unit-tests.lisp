;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-UTILITIES-TESTS -*-
(in-package #:duim-utilities-tests)

(def-suite duim-utilities
    :description "DUIM-UTILITIES unit tests"
    :in all-tests)
(in-suite duim-utilities)

;;; TODO: separate unit tests, each class of tests in a different
;;; file. Each file in the ASD under duim-utilities/tests

;;; COMPARISON.LISP | dylan-like =

(test equality
  (is (equal? 1 1) "1 and 1 not EQUAL?")
  (is-false (equal? 1 2) "1 and 2 EQUAL?")
  (is (equal? #c(1.0 2.0) #c(1 2)) "Complex numbers EQUAL? if magnitudes EQUAL?")
  (is-false (equal? #c(1 2) #c(2 3)) "Complex numbers not EQUAL? if magnitudes differ")

  (is (equal? "abc" "abc"))
  (is (not (equal? "ABC" "abc")))

  (is (equal? (cons 1 2) (cons 1 2)))
  (is (equal? (list 1 2 3) (list 1 2 3)))
  (is (not (equal? (cons 1 2) (cons 2 1))))

  (is (equal? #(1 2 3) #(1 2 3)))
  (is (not (equal? #(1 2 3) #(2 3 4)))))


;;; BASIC-MACROS.LISP
(defparameter lambda-list '(:foo foo :alpha a))
(test remove-keywords
  (is (equal? '(:alpha a)          (remove-keywords lambda-list '(:foo))))
  (is (equal? '(:foo foo :alpha a) (remove-keywords lambda-list '(:bar))) "Attempting to remove a non-existant keyword should return original keyword list")
  (is (not                         (remove-keywords '(:foo foo) '(:foo))) "Removing the only keyword should return nil")
  (is (not                         (remove-keywords '() '(:foo)))         "Trying to remove a keyword from an empty lambda list should return nil")
  (is (not                         (remove-keywords lambda-list '(:foo :alpha)))) "Removing all keywords should return nil")


;;; BASIC-DEFS.LISP

(test basic-definitions
  (is (string= (%STRING-DESIGNATOR->STRING :symbol) "SYMBOL"))
  (is (string= (%STRING-DESIGNATOR->STRING "String") "String"))
  (is (string= (%STRING-DESIGNATOR->STRING #\x) "x")))


;;; STRINGS.LISP

(test strings
  (is (string= (string-a-or-an "elephant") "an "))
  (is (string= (string-a-or-an "zebra") "a "))
  (is (string= (string-a-or-an "english") "an "))
  (is (string= (string-a-or-an "uniform") "a "))

  (is (= (string-search-set "floppy" #(#\y #\o)) 2))
  (is (= (string-search-set "floppy" "yo") 2))

  (is (= (subsequence-position "Ralph Waldo Emerson" "Waldo") 6))

  (is (string= (string-pluralize "man") "men"))
  (is (string= (string-pluralize "human") "humans"))
  (is (string= (string-pluralize "knife") "knives")))

;;; PROTOCOLS.LISP
;;; DEQUE.LISP
