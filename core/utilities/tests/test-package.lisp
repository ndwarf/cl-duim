;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

(defpackage #:duim-utilities-tests
  (:use #:cl
	#:duim-utilities
        #:fiveam)
  (:export
   ;; Strings
   #:equal?
   #:string-a-or-an
   #:%string-designator->string
   #:string-pluralize
   #:string-search-set
   #:subsequence-position

   ;; Test infrastructure
   #:run!
   #:all-tests))
