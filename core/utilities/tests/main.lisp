;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-UTILITIES-TESTS -*-
(in-package #:duim-utilities-tests)

(def-suite all-tests
    :description "The master suite of all DUIM-UTILITIES tests")
(in-suite all-tests)

(defun test-utilities ()
  (run! 'all-tests))
