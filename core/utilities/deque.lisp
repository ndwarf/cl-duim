;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-UTILITIES -*-
(in-package #:duim-utilities)

(defclass <deque> ()
  ()
  (:metaclass <abstract-metaclass>))

(defgeneric deque-pop (deque))
(defgeneric deque-pop-last (deque))
(defgeneric deque-push (deque new-value))
(defgeneric deque-push-last (deque new-value))

(defun make-deque (&rest all-keys &key size fill &allow-other-keys)
  (declare (ignore size fill))
  (apply 'make-instance '<object-deque> all-keys))


(defclass <object-deque> (<deque>)
  ((representation :type vector :initform (MAKE-STRETCHY-VECTOR) :accessor representation)))


;;; This wasn't included for this type before, add it now... (size is exported
;;; from duim utilities)
(defmethod size ((object <object-deque>))
  (length (representation object)))


(defmethod (setf size) ((size integer)
			(target <object-deque>))
  (setf (fill-pointer (representation target)) size))


(defmethod empty? ((collection <object-deque>))
  (= (length (representation collection)) 0))


(defmethod deque-push ((object t)
		       new-element)
  (error "Attempt to DEQUE-PUSH ~a into non-deque ~a" new-element object))


(defmethod deque-push ((deque <object-deque>) new-element)
  (labels ((move-up (sv index)
	       (loop for i from (- (length sv) 1)
		     downto (1+ index)
		     do (setf (aref sv i) (aref sv (- i 1))))))
    (cond ((empty? deque)
	   (vector-push-extend new-element (representation deque)))
	  (t
	   ;; Not empty; make space at the front of the vector and
	   ;; insert element
	   (vector-push-extend new-element (representation deque))
	   (move-up (representation deque) 0)
	   (setf (aref (representation deque) 0) new-element))))
  deque)


(defmethod deque-pop ((object t))
  (error "Attempt to DEQUE-POP on non-deque object ~a" object))


(defmethod deque-pop ((deque <object-deque>))
  (let ((retval nil))
    (labels ((move-down (sv index)
	         (loop for i from index to (- (length sv) 2)
		       do (setf (aref sv i) (aref sv (1+ i))))))
      (cond ((empty? deque)
	     (error "Attempt to POP empty <OBJECT-DEQUE> ~a" deque))
	    (t
	     (setf retval (aref (representation deque) 0))
	     (move-down (representation deque) 0)
	     (decf (fill-pointer (representation deque))))))
    retval))


(defmethod deque-push-last ((deque <object-deque>)
			    new-element)
  (vector-push-extend new-element (representation deque))
  deque)


(defmethod deque-pop-last ((deque <object-deque>))
  (vector-pop (representation deque)))

