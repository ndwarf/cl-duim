;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-EXTENDED-GEOMETRY-INTERNALS -*-
(in-package #:duim-extended-geometry-internals)

#||
/// Lines

define protocol <<line-protocol>> (<<polygon-protocol>>)
  function line-start-point
    (line :: <line>) => (point :: <point>);
  function line-end-point
    (line :: <line>) => (point :: <point>);
  function line-start-position
    (line :: <line>) => (x :: <real>, y :: <real>);
  function line-end-position
    (line :: <line>) => (x :: <real>, y :: <real>);
end protocol <<line-protocol>>;
||#
#||
(define-protocol <<line-protocol>> (<<polygon-protocol>>)
  (:function line-start-point (line))
  (:function line-end-point (line))
  (:function line-start-position (line))
  (:function line-end-position (line)))
||#
#||
define sealed class <standard-line> (<line>)
  sealed constant slot %start-x :: <real>,
    required-init-keyword: start-x:;
  sealed constant slot %start-y :: <real>,
    required-init-keyword: start-y:;
  sealed constant slot %end-x :: <real>,
    required-init-keyword: end-x:;
  sealed constant slot %end-y :: <real>,
    required-init-keyword: end-y:;
  sealed slot %points :: false-or(<vector>) = #f,
    init-keyword: points:;
end class <standard-line>;

define sealed domain make (singleton(<standard-line>));
define sealed domain initialize (<standard-line>);
||#

(defclass <standard-line> (<line>)
  ((%start-x :type real :initarg :start-x :initform (required-slot ":start-x" "<standard-line>") :reader %start-x)
   (%start-y :type real :initarg :start-y :initform (required-slot ":start-y" "<standard-line>") :reader %start-y)
   (%end-x :type real :initarg :end-x :initform (required-slot ":end-x" "<standard-line>") :reader %end-x)
   (%end-y :type real :initarg :end-y :initform (required-slot ":end-y" "<standard-line>") :reader %end-y)
   (%points :type (or null vector) :initarg :points :initform nil :accessor %points)))


#||
define inline function make-line
    (start-x, start-y, end-x, end-y) => (line :: <standard-line>)
  make(<standard-line>,
       start-x: start-x, start-y: start-y,
       end-x: end-x, end-y: end-y)
end function make-line;
||#

(defun make-line (start-x start-y end-x end-y)
"
Returns an object of class <line> that connects the two
positions (_start-x_, _start-y_) and (_end-x_, _end-y_) or the two
points _start-point_ and _end-point_.
"
  (make-instance '<standard-line>
                 :start-x start-x :start-y start-y
                 :end-x end-x :end-y end-y))


#||
define inline function make-line*
    (start-point, end-point) => (line :: <standard-line>)
  make(<standard-line>,
       start-x: point-x(start-point), start-y: point-y(start-point),
       end-x: point-x(end-point), end-y: point-y(end-point),
       points: vector(start-point, end-point))
end function make-line*;
||#

(defun make-line* (start-point end-point)
"
Returns an object of class <line> that connects the two
positions (_start-x_, _start-y_) and (_end-x_, _end-y_) or the two
points _start-point_ and _end-point_.
"
  (make-instance '<standard-line>
                 :start-x (point-x start-point) :start-y (point-y start-point)
                 :end-x   (point-x end-point) :end-y (point-y end-point)
                 :points  (vector start-point end-point)))


#||
define sealed inline method make
    (class == <line>, #key start-point, end-point)
 => (line :: <standard-line>)
  make-line*(start-point, end-point);
end method make;


define method line-start-position
    (line :: <standard-line>) => (x :: <real>, y :: <real>)
  values(line.%start-x, line.%start-y)
end method line-start-position;
||#

(defmethod line-start-position ((line <standard-line>))
"
Returns the start position of _line_ as x and y values.
"
  (values (%start-x line) (%start-y line)))


#||
define method line-end-position
    (line :: <standard-line>) => (x :: <real>, y :: <real>)
  values(line.%end-x, line.%end-y)
end method line-end-position;
||#

(defmethod line-end-position ((line <standard-line>))
"
Returns the end position of _line_ as x and y values.
"
  (values (%end-x line) (%end-y line)))


#||
define method polygon-points (line :: <standard-line>) => (points :: <vector>)
  line.%points
  | (line.%points := vector(make-point(line.%start-x, line.%start-y),
			    make-point(line.%end-x, line.%end-y)))
end method polygon-points;
||#

(defmethod polygon-points ((line <standard-line>))
"
Returns a VECTOR containing all the points making up _line_.
"
  (or (%points line)
      (setf (%points line) (vector (make-point (%start-x line) (%start-y line))
				   (make-point (%end-x line) (%end-y line))))))


#||
define method line-start-point
    (line :: <standard-line>) => (point :: <standard-point>)
  polygon-points(line)[0]
end method line-start-point;
||#

(defmethod line-start-point ((line <standard-line>))
"
Returns a <STANDARD-POINT>.
"
  (aref (polygon-points line) 0))


#||
define method line-end-point
    (line :: <standard-line>) => (point :: <standard-point>)
  polygon-points(line)[1]
end method line-end-point;
||#

(defmethod line-end-point ((line <standard-line>))
"
Returns a <STANDARD-POINT>.
"
  (aref (polygon-points line) 1))


#||
define method polyline-closed? (line :: <standard-line>) => (true? :: <boolean>)
  #f
end method polyline-closed?;
||#

(defmethod polyline-closed? ((line <standard-line>))
  nil)


#||
define method do-polygon-coordinates
    (function :: <function>, line :: <standard-line>) => ()
  function(line.%start-x, line.%start-y);
  function(line.%end-x, line.%end-y)
end method do-polygon-coordinates;
||#

(defmethod do-polygon-coordinates ((function function) (line <standard-line>))
  (funcall function (%start-x line) (%start-y line))
  (funcall function (%end-x line) (%end-y line)))


#||
define method do-polygon-segments
    (function :: <function>, line :: <standard-line>) => ()
  function(line.%start-x, line.%start-y, line.%end-x, line.%end-y)
end method do-polygon-segments;
||#

(defmethod do-polygon-segments ((function function) (line <standard-line>))
  (funcall function (%start-x line) (%start-y line) (%end-x line) (%end-y line)))


#||
define method transform-region
    (transform :: <transform>, line :: <standard-line>) => (line :: <standard-line>)
  let (sx, sy) = transform-position(transform, line.%start-x, line.%start-y);
  let (ex, ey) = transform-position(transform, line.%end-x, line.%end-y);
  make-line(sx, sy, ex, ey)
end method transform-region;
||#

(defmethod transform-region ((transform <transform>) (line <standard-line>))
  (multiple-value-bind (sx sy)
      (transform-position transform (%start-x line) (%start-y line))
    (multiple-value-bind (ex ey)
	(transform-position transform (%end-x line) (%end-y line))
      (make-line sx sy ex ey))))


#||
define method box-edges
    (line :: <standard-line>)
 => (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
  fix-box(min(line.%start-x, line.%end-x), min(line.%start-y, line.%end-y),
          max(line.%start-x, line.%end-x), max(line.%start-y, line.%end-y))
end method box-edges;
||#

(defmethod box-edges ((line <standard-line>))
  (fix-box (min (%start-x line) (%end-x line)) (min (%start-y line) (%end-y line))
	   (max (%start-x line) (%end-x line)) (max (%start-y line) (%end-y line))))


#||
define method region-equal
    (line1 :: <standard-line>, line2 :: <standard-line>) => (true? :: <boolean>)
  (line1.%start-x = line2.%start-x
   & line1.%start-y = line2.%start-y
   & line1.%end-x = line2.%end-x
   & line1.%end-y = line2.%end-y)
  | (line1.%start-x = line2.%end-x
     & line1.%start-y = line2.%end-y
     & line1.%end-x = line2.%start-x
     & line1.%end-y = line2.%start-y)
end method region-equal;
||#

(defmethod region-equal ((line1 <standard-line>) (line2 <standard-line>))
  (or (and (= (%start-x line1) (%start-x line2))
	   (= (%start-y line1) (%start-y line2))
	   (= (%end-x line1)   (%end-x line2))
	   (= (%end-y line1)   (%end-y line2)))
      (and (= (%start-x line1) (%end-x line2))
	   (= (%start-y line1) (%end-y line2))
	   (= (%end-x line1)   (%start-x line2))
	   (= (%end-y line1)   (%start-y line2)))))


#||
define method region-contains-position?
    (line :: <standard-line>, x :: <real>, y :: <real>) => (true? :: <boolean>)
  let (left, top, right, bottom) = box-edges(line);
  ltrb-contains-position?(left, top, right, bottom,
			  fix-coordinate(x), fix-coordinate(y))
  & position-close-to-line?(x, y,
			    line.%start-x, line.%start-y,
			    line.%end-x, line.%end-y)
end method region-contains-position?;
||#

(defmethod region-contains-position? ((line <standard-line>) (x real) (y real))
  (multiple-value-bind (left top right bottom) (box-edges line)
    (and (ltrb-contains-position? left top right bottom
				  (fix-coordinate x) (fix-coordinate y))
	 (position-close-to-line? x y
				  (%start-x line) (%start-y line)
				  (%end-x line) (%end-y line)))))


#||
define method region-contains-region?
    (line1 :: <standard-line>, line2 :: <standard-line>) => (true? :: <boolean>)
  region-contains-position?(line1, line2.%start-x, line2.%start-y)
  & region-contains-position?(line1, line2.%end-x, line2.%end-y)
end method region-contains-region?;
||#

(defmethod region-contains-region? ((line1 <standard-line>) (line2 <standard-line>))
  (and (region-contains-position? line1 (%start-x line2) (%start-y line2))
       (region-contains-position? line1 (%end-x line2) (%end-y line2))))


#||
define method region-intersects-region?
    (line1 :: <standard-line>, line2 :: <standard-line>) => (true? :: <boolean>)
  line-intersects-line?(line1.%start-x, line1.%start-y, line1.%end-x, line1.%end-y,
			line2.%start-x, line2.%start-y, line2.%end-x, line2.%end-y)
end method region-intersects-region?;
||#

(defmethod region-intersects-region? ((line1 <standard-line>) (line2 <standard-line>))
  (line-intersects-line? (%start-x line1) (%start-y line1) (%end-x line1) (%end-y line1)
			 (%start-x line2) (%start-y line2) (%end-x line2) (%end-y line2)))


#||
define method region-intersection
    (line1 :: <standard-line>, line2 :: <standard-line>) => (region :: <region>)
  if (region-intersects-region?(line1, line2))
    make-line
      (max(line1.%start-x, line2.%start-x),
       max(line1.%start-y, line2.%start-y), min(line1.%end-x, line2.%end-x),
       min(line1.%end-y, line2.%end-y))
  else
    $nowhere
  end
end method region-intersection;
||#

(defmethod region-intersection ((line1 <standard-line>) (line2 <standard-line>))
  (if (region-intersects-region? line1 line2)
      (make-line (max (%start-x line1) (%start-x line2))
		 (max (%start-y line1) (%start-y line2)) (min (%end-x line1)   (%end-x line2))
		 (min (%end-y line1)   (%end-y line2)))
      *nowhere*))



#||

/// Line geometry

define method line-intersects-line?
    (sx1 :: <real>, sy1 :: <real>, ex1 :: <real>, ey1 :: <real>,
     sx2 :: <real>, sy2 :: <real>, ex2 :: <real>, ey2 :: <real>)
 => (true? :: <boolean>)
  max(sx2, ex2) >= min(sx1, ex1)
  & max(sx1, ex1) >= min(sx2, ex2)
  & begin
      let dx1 = ex1 - sx1;
      let dy1 = ey1 - sy1;
      let dx2 = ex2 - sx2;
      let dy2 = ey2 - sy2;
      dx1 * dy2 = dx2 * dy1		// slopes equal
      & dx1 * (sy1 - sy2) = dy1 * (sx1 - sx2)
    end
end method line-intersects-line?;
||#

(defgeneric line-intersects-line? (sx1 sy1 ex1 ey1 sx2 sy2 ex2 ey2))

(defmethod line-intersects-line? ((sx1 real) (sy1 real) (ex1 real) (ey1 real)
				  (sx2 real) (sy2 real) (ex2 real) (ey2 real))
  (and (>= (max sx2 ex2) (min sx1 ex1))
       (>= (max sx1 ex1) (min sx2 ex2))
       (let ((dx1 (- ex1 sx1))
	     (dy1 (- ey1 sy1))
	     (dx2 (- ex2 sx2))
	     (dy2 (- ey2 sy2)))
	 (and (= (* dx1 dy2) (* dx2 dy1))   ;; slopes equal
	      (= (* dx1 (- sy1 sy2)) (* dy1 (- sx1 sx2)))))))


#||
// Returns either the new line endpoints, or (#f,#f,#f,#f) if the line is gone
define method clip-line-to-box
    (x0 :: <real>, y0 :: <real>, x1 :: <real>, y1 :: <real>,
     left :: <real>, top :: <real>, right :: <real>, bottom :: <real>)
 => (x0 :: false-or(<real>), y0 :: false-or(<real>),
     x1 :: false-or(<real>), y1 :: false-or(<real>))
  block (return)
    local method clip-bound (value, lower, upper) => (how)
	    case
	      value < lower => #"below";
	      value > upper => #"above";
	      otherwise => #f
	    end
	  end method,
          method interpolate (u0, v0, u1, v1, u) => (u1)
	    ((u1 - u) * v0 + (u - u0) * v1) / as(<single-float>, u1 - u0)
	  end method;
    let bx0 = clip-bound(x0, left, right);
    let by0 = clip-bound(y0, top, bottom);
    let bx1 = clip-bound(x1, left, right);
    let by1 = clip-bound(y1, top, bottom);
    when ((bx0 & (bx0 == bx1)) | (by0 & (by0 == by1)))
      return(#f, #f, #f, #f)
    end;
    let cy0 = by0;
    let cy1 = by1;
    when (bx0)
      if (bx0 == #"below")
	y0 := interpolate(x0, y0, x1, y1, left);
	x0 := left
      else	// bx0 == #"above"
	y0 := interpolate(x0, y0, x1, y1, right);
	x0 := right
      end;
      cy0 := clip-bound(y0, top, bottom);
      when (cy0 & (cy0 == cy1))
	return(#f, #f, #f, #f)
      end
    end;
    when (bx1)
      if (bx1 == #"below")
	y1 := interpolate(x0, y0, x1, y1, left);
	x1 := left
      else	// bx1 == #"above"
	y1 := interpolate(x0, y0, x1, y1, right);
	x1 := right
      end;
      cy1 := clip-bound(y1, top, bottom);
      when (cy1 & (cy0 == cy1))
	return(#f, #f, #f, #f)
      end
    end;
    when (cy0)
      if (cy0 == #"below")
	x0 := interpolate(y0, x0, y1, x1, top);
	y0 := top
      else	// cy0 == #"above"
	x0 := interpolate(y0, x0, y1, x1, bottom);
	y0 := bottom
      end
    end;
    when (cy1)
      if (cy1 == #"below")
	x1 := interpolate(y0, x0, y1, x1, top);
	y1 := top
      else	// cy0 == #"above"
	x1 := interpolate(y0, x0, y1, x1, bottom);
	y1 := bottom
      end
    end;
    values(x0, y0, x1, y1)
  end
end method clip-line-to-box;
||#

(defgeneric clip-line-to-box (x0 y0 x1 y1 left top right bottom)
  (:documentation
"
Returns either the new line endpoints, or (#f,#f,#f,#f) if the line is gone.
"))

(defmethod clip-line-to-box ((x0 real) (y0 real) (x1 real) (y1 real)
			     (left real) (top real) (right real) (bottom real))
  (labels ((clip-bound (value lower upper)
	     (cond ((< value lower) :below)
		   ((> value upper) :above)
		   (t nil)))
	   (interpolate (u0 v0 u1 v1 u)
	     (+ (* (- u1 u) v0) (/ (* (- u u0) v1) (coerce (- u1 u0) 'single-float)))))
    (let ((bx0 (clip-bound x0 left right))
	  (by0 (clip-bound y0 top bottom))
	  (bx1 (clip-bound x1 left right))
	  (by1 (clip-bound y1 top bottom)))
      (when (or (and bx0 (eql bx0 bx1)) (and by0 (eql by0 by1)))
	(return-from clip-line-to-box (values nil nil nil nil)))
      (let ((cy0 by0)
	    (cy1 by1))
	(when bx0
	  (if (eql bx0 :below)
	      (setf y0 (interpolate x0 y0 x1 y1 left)
		    x0 left)
	      ;; (else) bx0 == #"above"
	      (setf y0 (interpolate x0 y0 x1 y1 right)
		    x0 right))
	  (setf cy0 (clip-bound y0 top bottom))
	  (when (and cy0 (eql cy0 cy1))
	    (return-from clip-line-to-box (values nil nil nil nil))))
	(when bx1
	  (if (eql bx1 :below)
	      (setf y1 (interpolate x0 y0 x1 y1 left)
		    x1 left)
	      ;; (else) bx1 == #"above"
	      (setf y1 (interpolate x0 y0 x1 y1 right)
		    x1 right))
	  (setf cy1 (clip-bound y1 top bottom))
	  (when (and cy1 (eql cy0 cy1))
	    (return-from clip-line-to-box (values nil nil nil nil))))
	(when cy0
	  (if (eql cy0 :below)
	      (setf x0 (interpolate y0 x0 y1 x1 top)
		    y0 top)
	      ;; (else) cy0 == #"above"
	      (setf x0 (interpolate y0 x0 y1 x1 bottom)
		    y0 bottom)))
	(when cy1
	  (if (eql cy1 :below)
	      (setf x1 (interpolate y0 x0 y1 x1 top)
		    y1 top)
	      ;; (else) cy0 == #"above"
	      (setf x1 (interpolate y0 x0 y1 x1 bottom)
		    y1 bottom)))
	(values x0 y0 x1 y1)))))



