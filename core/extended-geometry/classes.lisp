;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-EXTENDED-GEOMETRY-INTERNALS -*-
(in-package #:duim-extended-geometry-internals)

#||
/// Extended region protocol classes

define protocol-class polyline (<path>) end;
||#

(define-protocol-class polyline (<path>) ()
  (:documentation
"
The protocol class that corresponds to a polyline.

A polyline can be described either in terms of the individual x and y
coordinates that constitute its vertices, or by using composite
points. If the former is used, then they can be specified at the time
of creation using the :coordinates init-keyword, which is a sequence
of real numbers, with x and y coordinates alternating within the
sequence.

To describe a polyline in terms of composite point objects, use
the :points init-keyword, which is a sequence of instances of
<point>. You should be aware that using composite points may lead to a
loss of performance.

Exactly one of :coordinates and :points is required.
"))

;;; TODO: where to put the docs for 'polyline?' predicate?
#||
Returns t if _object_ is a polyline, otherwise returns nil.
||#


#||
define protocol-class polygon (<area>) end;
||#

(define-protocol-class polygon (<area>) ()
  (:documentation
"
The class that corresponds to a polygon. This is a subclass of <area>.

A polygon can be described either in terms of the individual x and y
coordinates that constitute its vertices, or by using composite
points. If the former is used, then they can be specified at the time
of creation using the :coordinates init-keyword, which is a sequence
of real numbers, with x and y coordinates alternating within the
sequence.

To describe a polygon in terms of composite point objects, use
the :points init-keyword, which is a sequence of instances of
<point>. You should be aware that using composite points may lead to a
loss of performance.

Exactly one of :coordinates or :points is required.
"))

;;; TODO: where to put the docs for 'polygon?' predicate?
#||
Returns t if _object_ is a polygon, otherwise returns nil.
||#


#||
define protocol-class line (<path>) end;
||#

(define-protocol-class line (<path>) ()
  (:documentation
"
The class that corresponds to a line. This is a subclass of <path>.

This is the instantiable class that implements a line
segment. 'make-line' instantiates an object of type <line>.
"))

;;; TODO: Where to put documentation for 'line?' predicate?
#||
Returns t if _object_ is a line, otherwise returns nil.
||#


#||
define protocol-class rectangle (<area>) end;
||#

(define-protocol-class rectangle (<area>) ()
  (:documentation
"
The protocol class that corresponds to a rectangle. This is a subclass
of <polygon>.

Rectangles whose edges are parallel to the coordinate axes are a
special case of polygon that can be specified completely by four real
numbers _x1_, _y1_, _x2_, _y2_). They are *not* closed under general
affine transformations (although they are closed under rectilinear
transformations).
"))

;;; TODO: where to put documentation for 'rectangle?' predicate?
#||
Returns t if _object_ is a rectangle, otherwise returns nil.
||#


#||
define protocol-class elliptical-arc (<path>) end;
||#

(define-protocol-class elliptical-arc (<path>) ()
  (:documentation
"
An _elliptical arc_ is a path consisting of all or a portion of the
outline of an ellipse. Circular arcs are special cases of elliptical
arcs.
"))

;;; TODO: Where to put docs for 'elliptical-arc?' predicate?
#||
Returns t if _object_ is an elliptical arc, otherwise returns nil.
||#


#||
define protocol-class ellipse (<area>) end;
||#

(define-protocol-class ellipse (<area>) ()
  (:documentation
"
An _ellipse_ is an area that is the outline and interior of an
ellipse. Circles are special cases of ellipses.

The :center-x init-keyword specifies the x coordinate of the center of
the ellipse.

The :center-y init-keyword specifies the y coordinate of the center of
the ellipse.

The :center-point init-keyword specifies the center of the ellipse as
a point.

An ellipse is specified in a manner that is easy to transform, and
treats all ellipses on an equal basis. An ellipse is specified by its
center point and two vectors that describe a bounding parallelogram of
the ellipse. The bounding parallelogram is made by adding and
subtracting the vectors from the center point in the following manner:

                           x coordinate     y coordinate
Center of ellipse              Xc               Yc

Vectors                        dx1              dy1
                               dx2              dy2

Corners of parallelogram   Xc + dx1 + dx2   Yc + dx1 + dx2
                           Xc + dx1 - dx2   Yc + dx1 - dx2
                           Xc - dx1 - dx2   Yc - dx1 - dx2
                           Xc - dx1 + dx2   Yc - dx1 + dx2

Note that several different parallelograms specify the same
ellipse. One parallelogram is bound to be a rectangle -- the vectors
will be perpendicular and correspond to the semi-axes of the ellipse.
"))

#||
(setf (documentation 'ellipse? 'function) "Returns t if _object_ is an ellipse, otherwise returns nil.")
||#


