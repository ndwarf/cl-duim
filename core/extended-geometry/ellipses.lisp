;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-EXTENDED-GEOMETRY-INTERNALS -*-
(in-package #:duim-extended-geometry-internals)

#||
/// Ellipses and elliptical arcs

define protocol <<ellipse-protocol>> (<<region-protocol>>)
  function ellipse-center-point
    (ellipse) => (point :: <point>);
  function ellipse-center-position
    (ellipse) => (x :: <real>, y :: <real>);
  function ellipse-radii
    (ellipse)
 => (radius-1-dx :: <real>, radius-1-dy :: <real>,
     radius-2-dx :: <real>, radius-2-dy :: <real>);
  function ellipse-start-angle
    (ellipse) => (angle :: false-or(<real>));
  function ellipse-end-angle
    (ellipse) => (angle :: false-or(<real>));
end protocol <<ellipse-protocol>>;
||#
#||
(define-protocol <<ellipse-protocol>> (<<region-protocol>>)
  (:function ellipse-center-point (ellipse))
  (:function ellipse-center-position (ellipse))
  (:function ellipse-radii (ellipse))
  (:function ellipse-start-angle (ellipse))
  (:function ellipse-end-angle (ellipse)))
||#

(defgeneric make-elliptical-arc (center-x center-y radius-1-dx radius-1-dy radius-2-dx radius-2-dy &key start-angle end-angle)
  (:documentation
"
Returns an object of class <elliptical-arc>. The center of the ellipse
is at the position _center-x_, _center-y_ or the point _center-point_.

Two vectors, (_radius-1-dx_, _radius-1-dy_) and (_radius-2-dx_,
_radius-2-dy_), specify the bounding parallelogram of the ellipse. All
of the radii are real numbers. If the two vectors are colinear, the
ellipse is not well-defined and the 'ellipse-not-well-defined' error
will be signalled. The special case of an elliptical arc with its axes
aligned with the coordinate axes can be obtained by setting both
_radius-1-dy_ and _radius-2-dx_ to 0.

If _start-angle_ and _end-angle_ are supplied, the arc is swept from
_start-angle_ to _end-angle_. Angles are measured counter-clockwise
with respect to the positive x axis. If _end-angle_ is supplied, the
default for _start-angle_ is 0; if _start-angle_ is supplied, the
default for _end-angle_ is 2pi; if neither is supplied then the region
is a closed elliptical path and the angles are meaningless.

The function 'make-elliptical-arc*' is identical to
'make-elliptical-arc', except that it passes composite objects, rather
than separate coordinates, in its arguments. You should be aware that
using this function may lead to a loss of performance.
"))

(defgeneric make-elliptical-arc* (center-point radius-1-dx radius-1-dy radius-2-dx radius-2-dy &key start-angle end-angle))
(defgeneric make-ellipse (center-x center-y radius-1-dx radius-1-dy radius-2-dx radius-2-dy &key start-angle end-angle)
  (:documentation
"
Returns an object of class <ellipse>. The center of the ellipse is at
the position _center-x_, _center-y_ or the point _center-point_.

Two vectors, (_radius-1-dx_, _radius-1-dy_) and (_radius-2-dx_,
_radius-2-dy_) specify the bounding parallelogram of the ellipse. All
of the radii are real numbers. If the two vectors are colinear, the
ellipse is not well-defined and the 'ellipse-not-well-defined' error
is signalled. The special case of an ellipse with its axes aligned
with the coordinate axes can be obtained by setting both _radius-1-dy_
and _radius-2-dx_ to 0.

If _start-angle_ or _end-angle_ are supplied, the ellipse is the _pie
slice_ area swept out by a line from the center of the ellipse to a
point on the boundary as the boundary point moves from the angle
_start-angle_ to _end-angle_. Angles are measured counter-clockwise
with respect to the positive x axis. If _end-angle_ is supplied, the
default for _start-angle_ is 0; if _start-angle_ is supplied, the
default for _end-angle_ is 2pi; if neither is supplied then the region
is a full ellipse and the angles are meaningless.

The function 'make-ellipse*' is identical to 'make-ellipse', except
that it passes composite objects, rather than separate coordinates, in
its arguments. You should be aware that using this function may lead
to a loss of performance.
"))

(defgeneric make-ellipse* (center-point radius-1-dx radius-1-dy radius-2-dx radius-2-dy &key start-angle end-angle))

#||
define abstract class <ellipse-mixin> (<object>)
  sealed constant slot %center-x :: <real>,
    required-init-keyword: center-x:;
  sealed constant slot %center-y :: <real>,
    required-init-keyword: center-y:;
  sealed slot %center-point :: false-or(<standard-point>) = #f,
    init-keyword: center-point:;
  sealed constant slot %radius-1-dx :: <real>,
    required-init-keyword: radius-1-dx:;
  sealed constant slot %radius-1-dy :: <real>,
    required-init-keyword: radius-1-dy:;
  sealed constant slot %radius-2-dx :: <real>,
    required-init-keyword: radius-2-dx:;
  sealed constant slot %radius-2-dy :: <real>,
    required-init-keyword: radius-2-dy:;
  sealed constant slot ellipse-start-angle :: false-or(<single-float>) = #f,
    init-keyword: start-angle:;
  sealed constant slot ellipse-end-angle :: false-or(<single-float>) = #f,
    init-keyword: end-angle:;
end class <ellipse-mixin>;
||#

(defclass <ellipse-mixin> ()
  ((%center-x :type real :initarg :center-x :initform (required-slot ":center-x" "<ellipse-mixin>") :reader %center-x)
   (%center-y :type real :initarg :center-y :initform (required-slot ":center-y" "<ellipse-mixin>") :reader %center-y)
   (%center-point :type (or null <standard-point>) :initarg :center-point :initform nil :accessor %center-point)
   (%radius-1-dx :type real :initarg :radius-1-dx :initform (required-slot ":radius-1-dx" "<ellipse-mixin>") :reader %radius-1-dx)
   (%radius-1-dy :type real :initarg :radius-1-dy :initform (required-slot ":radius-1-dy" "<ellipse-mixin>") :reader %radius-1-dy)
   (%radius-2-dx :type real :initarg :radius-2-dx :initform (required-slot ":radius-2-dx" "<ellipse-mixin>") :reader %radius-2-dx)
   (%radius-2-dy :type real :initarg :radius-2-dy :initform (required-slot ":radius-2-dy" "<ellipse-mixin>") :reader %radius-2-dy)
   (ellipse-start-angle :type (or null single-float) :initarg :start-angle :initform nil :reader ellipse-start-angle)
   (ellipse-end-angle :type (or null single-float) :initarg :end-angle :initform nil :reader ellipse-end-angle))
  (:metaclass <abstract-metaclass>))


#||
define method ellipse-center-position
    (ellipse :: <ellipse-mixin>) => (x :: <real>, y :: <real>)
  values(ellipse.%center-x, ellipse.%center-y)
end method ellipse-center-position;
||#

(defmethod ellipse-center-position ((ellipse <ellipse-mixin>))
"
Returns the center-x, center-y of _ellipse_ as two values.
"
  (values (%center-x ellipse) (%center-y ellipse)))


#||
define method ellipse-center-point
    (ellipse :: <ellipse-mixin>) => (point :: <standard-point>)
  ellipse.%center-point
  | (ellipse.%center-point := make-point(ellipse.%center-x, ellipse.%center-y))
end method ellipse-center-point;
||#

(defmethod ellipse-center-point ((ellipse <ellipse-mixin>))
"
Returns the center-x, center-y of _ellipse_ as a <POINT> instance.
"
  (or (%center-point ellipse)
      (setf (%center-point ellipse) (make-point (%center-x ellipse) (%center-y ellipse)))))


#||
define method ellipse-radii
    (ellipse :: <ellipse-mixin>)
 => (radius-1-dx :: <real>, radius-1-dy :: <real>,
     radius-2-dx :: <real>, radius-2-dy :: <real>);
  values(ellipse.%radius-1-dx, ellipse.%radius-1-dy,
         ellipse.%radius-2-dx, ellipse.%radius-2-dy)
end method ellipse-radii;
||#

(defmethod ellipse-radii ((ellipse <ellipse-mixin>))
"
Returns the RADIUS-1-DX, RADIUS-1-DY, RADIUS-2-DX and RADIUS-2-DY
values of _ellipse_ as four values.
"
  (values (%radius-1-dx ellipse) (%radius-1-dy ellipse)
	  (%radius-2-dx ellipse) (%radius-2-dy ellipse)))


#||
define sealed class <standard-elliptical-arc>
    (<ellipse-mixin>, <elliptical-arc>)
end class <standard-elliptical-arc>;
||#

(defclass <standard-elliptical-arc>
    (<ellipse-mixin> <elliptical-arc>)
  ())


#||
define sealed domain make (singleton(<standard-elliptical-arc>));
define sealed domain initialize (<standard-elliptical-arc>);

//--- Should signal <ellipse-not-well-defined> if the axes are collinear
define inline function make-elliptical-arc
    (center-x :: <real>, center-y :: <real>,
     radius-1-dx :: <real>, radius-1-dy :: <real>, radius-2-dx :: <real>, radius-2-dy :: <real>,
     #key start-angle, end-angle)
 => (arc :: <standard-elliptical-arc>)
  make(<standard-elliptical-arc>,
       center-x: center-x, center-y: center-y,
       radius-1-dx: radius-1-dx, radius-1-dy: radius-1-dy,
       radius-2-dx: radius-2-dx, radius-2-dy: radius-2-dy,
       start-angle: case
                      start-angle => as(<single-float>, start-angle);
                      end-angle => 0.0;
                      otherwise => #f
                    end,
       end-angle: case
                    end-angle => as(<single-float>, end-angle);
                    start-angle => $2pi;
                    otherwise => #f
                  end)
end function make-elliptical-arc;
||#

;; TODO: Should signal an error if the axes are collinear (i.e. lie on the same line).
(defmethod make-elliptical-arc ((center-x real) (center-y real)
				(radius-1-dx real) (radius-1-dy real) (radius-2-dx real) (radius-2-dy real)
				&key start-angle end-angle)
  (make-instance '<standard-elliptical-arc>
                 :center-x center-x :center-y center-y
                 :radius-1-dx radius-1-dx :radius-1-dy radius-1-dy
                 :radius-2-dx radius-2-dx :radius-2-dy radius-2-dy
                 :start-angle (cond (start-angle (coerce start-angle 'single-float))
				    (end-angle 0.0)
				    (t nil))
                 :end-angle (cond (end-angle (coerce end-angle 'single-float))
				  (start-angle +2pi+)
				  (t nil))))


#||
//--- Should signal <ellipse-not-well-defined> if the axes are collinear
define inline function make-elliptical-arc*
    (center-point :: <standard-point>,
     radius-1-dx :: <real>, radius-1-dy :: <real>, radius-2-dx :: <real>, radius-2-dy :: <real>,
     #key start-angle, end-angle)
 => (arc :: <standard-elliptical-arc>)
  make(<standard-elliptical-arc>,
       center-point: center-point,
       center-x: point-x(center-point), center-y: point-y(center-point),
       radius-1-dx: radius-1-dx, radius-1-dy: radius-1-dy,
       radius-2-dx: radius-2-dx, radius-2-dy: radius-2-dy,
       start-angle: case
                      start-angle => as(<single-float>, start-angle);
                      end-angle => 0.0;
                      otherwise => #f
                    end,
       end-angle: case
                    end-angle => as(<single-float>, end-angle);
                    start-angle => $2pi;
                    otherwise => #f
                  end)
end function make-elliptical-arc*;
||#

;; TODO: Should signal an error if the axes are collinear (i.e. lie on the same line).
(defmethod make-elliptical-arc* ((center-point <standard-point>)
				 (radius-1-dx real) (radius-1-dy real) (radius-2-dx real) (radius-2-dy real)
				 &key start-angle end-angle)
  (make-instance '<standard-elliptical-arc>
                 :center-point center-point
                 :center-x (point-x center-point) :center-y (point-y center-point)
                 :radius-1-dx radius-1-dx :radius-1-dy radius-1-dy
                 :radius-2-dx radius-2-dx :radius-2-dy radius-2-dy
                 :start-angle (cond (start-angle (coerce start-angle 'single-float))
				    (end-angle 0.0)
				    (t nil))
                 :end-angle (cond (end-angle (coerce end-angle 'single-float))
				  (start-angle +2pi+)
				  (t nil))))


#||
define sealed inline method make
    (class == <elliptical-arc>,
     #key center-point, radius-1-dx, radius-1-dy, radius-2-dx, radius-2-dy,
	  start-angle, end-angle)
 => (arc :: <standard-elliptical-arc>)
  make-elliptical-arc*(center-point, 
		       radius-1-dx, radius-1-dy, radius-2-dx, radius-2-dy,
		       start-angle: start-angle, end-angle: end-angle)
end method make;


define method transform-region
    (transform :: <transform>, ellipse :: <standard-elliptical-arc>)
 => (arc :: <standard-elliptical-arc>)
  let (cx, cy)
    = transform-position(transform, ellipse.%center-x, ellipse.%center-y);
  let (r1-dx, r1-dy)
    = transform-distance
        (transform, ellipse.%radius-1-dx, ellipse.%radius-1-dy);
  let (r2-dx, r2-dy)
    = transform-distance
        (transform, ellipse.%radius-2-dx, ellipse.%radius-2-dy);
  let start-angle = ellipse-start-angle(ellipse);
  let end-angle   = ellipse-end-angle(ellipse);
  when (start-angle)		// non-#f => end angle is non-#f
    let (sa, ea) = transform-angles(transform, start-angle, end-angle);
    start-angle := sa;
    end-angle   := ea
  end;
  make-elliptical-arc(cx, cy, r1-dx, r1-dy, r2-dx, r2-dy,
		      start-angle: start-angle, end-angle: end-angle)
end method transform-region;
||#

(defmethod transform-region ((transform <transform>) (ellipse <standard-elliptical-arc>))
  (multiple-value-bind (cx cy)
      (transform-position transform (%center-x ellipse) (%center-y ellipse))
    (multiple-value-bind (r1-dx r1-dy)
	(transform-distance transform (%radius-1-dx ellipse) (%radius-1-dy ellipse))
      (multiple-value-bind (r2-dx r2-dy)
	  (transform-distance transform (%radius-2-dx ellipse) (%radius-2-dy ellipse))
	(let ((start-angle (ellipse-start-angle ellipse))
	      (end-angle   (ellipse-end-angle ellipse)))
	  (when start-angle		;; non-#f => end angle is non-#f
	    (multiple-value-bind (sa ea)
		(transform-angles transform start-angle end-angle)
	      (setf start-angle sa)
	      (setf end-angle   ea)))
	  (make-elliptical-arc cx cy r1-dx r1-dy r2-dx r2-dy
			       :start-angle start-angle :end-angle end-angle))))))


#||
define method box-edges
    (ellipse :: <standard-elliptical-arc>)
 => (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
  elliptical-arc-box(ellipse.%center-x, ellipse.%center-y,
                     ellipse.%radius-1-dx, ellipse.%radius-1-dy,
                     ellipse.%radius-2-dx, ellipse.%radius-2-dy,
                     start-angle: ellipse-start-angle(ellipse),
		     end-angle: ellipse-end-angle(ellipse))
end method box-edges;
||#

(defmethod box-edges ((ellipse <standard-elliptical-arc>))
  (elliptical-arc-box (%center-x ellipse)    (%center-y ellipse)
		      (%radius-1-dx ellipse) (%radius-1-dy ellipse)
		      (%radius-2-dx ellipse) (%radius-2-dy ellipse)
		      :start-angle (ellipse-start-angle ellipse)
		      :end-angle   (ellipse-end-angle ellipse)))


#||
define method region-equal
    (e1 :: <standard-elliptical-arc>, e2 :: <standard-elliptical-arc>) => (true? :: <boolean>)
  e1.%center-x = e2.%center-x
  & e1.%center-y = e2.%center-y
  & e1.%radius-1-dx = e2.%radius-1-dx
  & e1.%radius-1-dy = e2.%radius-1-dy
  & e1.%radius-2-dx = e2.%radius-2-dx
  & e1.%radius-2-dy = e2.%radius-2-dy
  & ellipse-start-angle(e1) = ellipse-start-angle(e2)
  & ellipse-end-angle(e1) = ellipse-end-angle(e2)
end method region-equal;
||#

(defmethod region-equal ((e1 <standard-elliptical-arc>) (e2 <standard-elliptical-arc>))
  (and (= (%center-x e1)    (%center-x e2))
       (= (%center-y e1)    (%center-y e2))
       (= (%radius-1-dx e1) (%radius-1-dx e2))
       (= (%radius-1-dy e1) (%radius-1-dy e2))
       (= (%radius-2-dx e1) (%radius-2-dx e2))
       (= (%radius-2-dy e1) (%radius-2-dy e2))
       ;; Use EQUAL? since start / end angles can be numeric or nil.
       (equal? (ellipse-start-angle e1) (ellipse-start-angle e2))
       (equal? (ellipse-end-angle e1)   (ellipse-end-angle e2))))


#||
define method region-contains-position?
    (ellipse :: <standard-elliptical-arc>, x :: <real>, y :: <real>) => (true? :: <boolean>)
  let (left, top, right, bottom) = box-edges(ellipse);
  ltrb-contains-position?(left, top, right, bottom,
			  fix-coordinate(x), fix-coordinate(y))
  & position-on-thick-ellipse?(x - ellipse.%center-x, y - ellipse.%center-y,
			       ellipse.%radius-1-dx, ellipse.%radius-1-dy,
			       ellipse.%radius-2-dx, ellipse.%radius-2-dy)
end method region-contains-position?;
||#

(defmethod region-contains-position? ((ellipse <standard-elliptical-arc>) (x real) (y real))
  (multiple-value-bind (left top right bottom)
      (box-edges ellipse)
    (and (ltrb-contains-position? left top right bottom
				  (fix-coordinate x) (fix-coordinate y))
	 (position-on-thick-ellipse? (- x (%center-x ellipse))
				     (- y (%center-y ellipse))
				     (%radius-1-dx ellipse) (%radius-1-dy ellipse)
				     (%radius-2-dx ellipse) (%radius-2-dy ellipse)))))


#||
define sealed class <standard-ellipse>
    (<ellipse-mixin>, <ellipse>)
end class <standard-ellipse>;
||#

(defclass <standard-ellipse> (<ellipse-mixin> <ellipse>) ())


#||
define sealed domain make (singleton(<standard-ellipse>));
define sealed domain initialize (<standard-ellipse>);

define inline function make-ellipse
    (center-x :: <real>, center-y :: <real>,
     radius-1-dx :: <real>, radius-1-dy :: <real>, radius-2-dx :: <real>, radius-2-dy :: <real>,
     #key start-angle, end-angle)
 => (ellipse :: <standard-ellipse>)
  make(<standard-ellipse>,
       center-x: center-x, center-y: center-y,
       radius-1-dx: radius-1-dx, radius-1-dy: radius-1-dy,
       radius-2-dx: radius-2-dx, radius-2-dy: radius-2-dy,
       start-angle: case
                      start-angle => as(<single-float>, start-angle);
                      end-angle => 0.0;
                      otherwise => #f
                    end,
       end-angle: case
                    end-angle => as(<single-float>, end-angle);
                    start-angle => $2pi;
                    otherwise => #f
                  end)
end function make-ellipse;
||#

(defmethod make-ellipse ((center-x real) (center-y real)
			 (radius-1-dx real) (radius-1-dy real) (radius-2-dx real) (radius-2-dy real)
			 &key start-angle end-angle)
  (make-instance '<standard-ellipse>
                 :center-x center-x :center-y center-y
                 :radius-1-dx radius-1-dx :radius-1-dy radius-1-dy
                 :radius-2-dx radius-2-dx :radius-2-dy radius-2-dy
                 :start-angle (cond (start-angle (coerce start-angle 'single-float))
				    (end-angle 0.0)
				    (t nil))
                 :end-angle (cond (end-angle (coerce end-angle 'single-float))
				  (start-angle +2pi+)
				  (t nil))))


#||
define inline function make-ellipse*
    (center-point :: <standard-point>,
     radius-1-dx :: <real>, radius-1-dy :: <real>, radius-2-dx :: <real>, radius-2-dy :: <real>,
     #key start-angle, end-angle)
 => (ellipse :: <standard-ellipse>)
  make(<standard-ellipse>,
       center-point: center-point,
       center-x: point-x(center-point), center-y: point-y(center-point),
       radius-1-dx: radius-1-dx, radius-1-dy: radius-1-dy,
       radius-2-dx: radius-2-dx, radius-2-dy: radius-2-dy,
       start-angle: case
                      start-angle => as(<single-float>, start-angle);
                      end-angle => 0.0;
                      otherwise => #f
                    end,
       end-angle: case
                    end-angle => as(<single-float>, end-angle);
                    start-angle => $2pi;
                    otherwise => #f
                  end)
end function make-ellipse*;
||#

(defmethod make-ellipse* ((center-point <standard-point>)
			  (radius-1-dx real) (radius-1-dy real) (radius-2-dx real) (radius-2-dy real)
			  &key start-angle end-angle)
  (make-instance '<standard-ellipse>
                 :center-point center-point
                 :center-x (point-x center-point) :center-y (point-y center-point)
                 :radius-1-dx radius-1-dx :radius-1-dy radius-1-dy
                 :radius-2-dx radius-2-dx :radius-2-dy radius-2-dy
                 :start-angle (cond (start-angle (coerce start-angle 'single-float))
				    (end-angle 0.0)
				    (t nil))
                 :end-angle (cond (end-angle (coerce end-angle 'single-float))
				  (start-angle +2pi+)
				  (t nil))))


#||
define sealed inline method make
    (class == <ellipse>,
     #key center-point, radius-1-dx, radius-1-dy, radius-2-dx, radius-2-dy,
	 start-angle, end-angle)
 => (ellipse :: <standard-ellipse>)
  make-ellipse*(center-point, 
		radius-1-dx, radius-1-dy, radius-2-dx, radius-2-dy,
		start-angle: start-angle, end-angle: end-angle)
end method make;


define method transform-region
    (transform :: <transform>, ellipse :: <standard-ellipse>)
 => (ellipse :: <standard-ellipse>)
  let (cx, cy)
    = transform-position(transform, ellipse.%center-x, ellipse.%center-y);
  let (r1-dx, r1-dy)
    = transform-distance(transform, ellipse.%radius-1-dx, ellipse.%radius-1-dy);
  let (r2-dx, r2-dy)
    = transform-distance(transform, ellipse.%radius-2-dx, ellipse.%radius-2-dy);
  let start-angle = ellipse-start-angle(ellipse);
  let end-angle   = ellipse-end-angle(ellipse);
  when (start-angle)		// non-#f => end angle is non-#f
    let (sa, ea) = transform-angles(transform, start-angle, end-angle);
    start-angle := sa;
    end-angle   := ea
  end;
  make-ellipse(cx, cy, r1-dx, r1-dy, r2-dx, r2-dy,
	       start-angle: start-angle, end-angle: end-angle)
end method transform-region;
||#

(defmethod transform-region ((transform <transform>) (ellipse <standard-ellipse>))
  (multiple-value-bind (cx cy)
      (transform-position transform (%center-x ellipse) (%center-y ellipse))
    (multiple-value-bind (r1-dx r1-dy)
	(transform-distance transform (%radius-1-dx ellipse) (%radius-1-dy ellipse))
      (multiple-value-bind (r2-dx r2-dy)
	  (transform-distance transform (%radius-2-dx ellipse) (%radius-2-dy ellipse))
	(let ((start-angle (ellipse-start-angle ellipse))
	      (end-angle   (ellipse-end-angle ellipse)))
	  (when start-angle		;; non-#f => end angle is non-#f
	    (multiple-value-bind (sa ea)
		(transform-angles transform start-angle end-angle)
	      (setf start-angle sa)
	      (setf end-angle   ea)))
	  (make-ellipse cx cy r1-dx r1-dy r2-dx r2-dy
			:start-angle start-angle :end-angle end-angle))))))


#||
define method box-edges
    (ellipse :: <standard-ellipse>)
 => (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
  elliptical-arc-box(ellipse.%center-x, ellipse.%center-y,
                     ellipse.%radius-1-dx, ellipse.%radius-1-dy,
                     ellipse.%radius-2-dx, ellipse.%radius-2-dy,
                     start-angle: ellipse-start-angle(ellipse),
		     end-angle: ellipse-end-angle(ellipse),
		     thickness: #f)	// filled...
end method box-edges;
||#

(defmethod box-edges ((ellipse <standard-ellipse>))
  (elliptical-arc-box (%center-x ellipse) (%center-y ellipse)
		      (%radius-1-dx ellipse) (%radius-1-dy ellipse)
		      (%radius-2-dx ellipse) (%radius-2-dy ellipse)
		      :start-angle (ellipse-start-angle ellipse)
		      :end-angle (ellipse-end-angle ellipse)
		      :thickness nil))	;; filled...


#||
define method region-equal
    (e1 :: <standard-ellipse>, e2 :: <standard-ellipse>) => (true? :: <boolean>)
  e1.%center-x = e2.%center-x
  & e1.%center-y = e2.%center-y
  & e1.%radius-1-dx = e2.%radius-1-dx
  & e1.%radius-1-dy = e2.%radius-1-dy
  & e1.%radius-2-dx = e2.%radius-2-dx
  & e1.%radius-2-dy = e2.%radius-2-dy
  & ellipse-start-angle(e1) = ellipse-start-angle(e2)
  & ellipse-end-angle(e1) = ellipse-end-angle(e2)
end method region-equal;
||#

(defmethod region-equal ((e1 <standard-ellipse>) (e2 <standard-ellipse>))
  (and (= (%center-x e1) (%center-x e2))
       (= (%center-y e1) (%center-y e2))
       (= (%radius-1-dx e1) (%radius-1-dx e2))
       (= (%radius-1-dy e1) (%radius-1-dy e2))
       (= (%radius-2-dx e1) (%radius-2-dx e2))
       (= (%radius-2-dy e1) (%radius-2-dy e2))
       ;; Use EQUAL? since start / end angles can be numeric or nil.
       (equal? (ellipse-start-angle e1) (ellipse-start-angle e2))
       (equal? (ellipse-end-angle e1)   (ellipse-end-angle e2))))


#||
define method region-contains-position?
    (ellipse :: <standard-ellipse>, x :: <real>, y :: <real>) => (true? :: <boolean>)
  let (left, top, right, bottom) = box-edges(ellipse);
  ltrb-contains-position?(left, top, right, bottom,
			  fix-coordinate(x), fix-coordinate(y))
  & position-inside-ellipse?(x - ellipse.%center-x, y - ellipse.%center-y,
			     ellipse.%radius-1-dx, ellipse.%radius-1-dy,
			     ellipse.%radius-2-dx, ellipse.%radius-2-dy)
end method region-contains-position?;
||#

(defmethod region-contains-position? ((ellipse <standard-ellipse>) (x real) (y real))
  (multiple-value-bind (left top right bottom)
      (box-edges ellipse)
    (and (ltrb-contains-position? left top right bottom
				  (fix-coordinate x) (fix-coordinate y))
	 (position-inside-ellipse? (- x (%center-x ellipse)) (- y (%center-y ellipse))
				   (%radius-1-dx ellipse) (%radius-1-dy ellipse)
				   (%radius-2-dx ellipse) (%radius-2-dy ellipse)))))
