;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-EXTENDED-GEOMETRY-INTERNALS -*-
(in-package #:duim-extended-geometry-internals)

#||
/// Region class-based graphics

define open generic draw-design
    (drawable :: <drawable>, design :: <region>) => (record);
||#

(defgeneric draw-design (drawable design)
  (:documentation
"
Draws _design_ onto the sheet medium _drawable_.

Facilitates drawing an arbitrary _design_ onto the drawable
_drawable_.  Note that many of the designs in DUIM are abstractly
defined (such as arcs, rectangles, ellipses etc.) but the direct
drawing methods require specific coordinates and distances; The
DRAW-DESIGN methods allow these abstract designs to be drawn onto a
drawable without having to deconstruct the encapsulated values.
"))

#||
/// Easy cases

define sealed method draw-design
    (sheet :: <sheet>, region :: <everywhere>) => (record)
  // Just fill with the foreground color
  let (left, top, right, bottom) = box-edges(sheet-region(sheet));
  with-drawing-options (sheet, brush: $foreground)
    draw-rectangle(sheet, left, top, right, bottom, filled?: #t)
  end
end method draw-design;
||#

(defmethod draw-design ((sheet <sheet>) (region <everywhere>))
  ;; Just fill with the foreground color
  (multiple-value-bind (left top right bottom)
      (box-edges (sheet-region sheet))
    (with-drawing-options (sheet :brush *foreground*)
      (draw-rectangle sheet left top right bottom :filled? t))))


#||
define sealed method draw-design
    (medium :: <medium>, region :: <everywhere>) => (record)
  let sheet = medium-sheet(medium);
  let (left, top, right, bottom) = box-edges(sheet-region(sheet));
  with-drawing-options (medium, brush: $foreground)
    draw-rectangle(medium, left, top, right, bottom, filled?: #t)
  end
end method draw-design;
||#

(defmethod draw-design ((medium <medium>) (region <everywhere>))
  (let ((sheet (medium-sheet medium)))
    (multiple-value-bind (left top right bottom)
	(box-edges (sheet-region sheet))
      (with-drawing-options (medium :brush *foreground*)
        (draw-rectangle medium left top right bottom :filled? t)))))


#||
define sealed method draw-design
    (drawable :: <drawable>, region :: <nowhere>) => (record)
  #f
end method draw-design;
||#

(defmethod draw-design ((drawable <abstract-sheet>) (region <nowhere>))
  nil)

(defmethod draw-design ((drawable <abstract-medium>) (region <nowhere>))
  nil)


#||
/// Composite regions

define sealed method draw-design
    (drawable :: <drawable>, region :: <region-union>) => (record)
  do-regions(method (region) draw-design(region, drawable) end,
	     region)
end method draw-design;
||#

(defmethod draw-design ((drawable <abstract-sheet>) (region <region-union>))
  (do-regions #'(lambda (region)
		  (draw-design region drawable))
	      region))

(defmethod draw-design ((drawable <abstract-medium>) (region <region-union>))
  (do-regions #'(lambda (region)
		  (draw-design region drawable))
	      region))


#||
define sealed method draw-design
    (drawable :: <drawable>, region :: <region-intersection>) => (record)
  //---*** Should draw the intersection, but I dunno how to do that in general
end method draw-design;
||#

(defmethod draw-design ((drawable <abstract-sheet>) (region <region-intersection>))
  ;;---*** Should draw the intersection, but I dunno how to do that in general
  )

(defmethod draw-design ((drawable <abstract-medium>) (region <region-intersection>))
  ;;---*** Should draw the intersection, but I dunno how to do that in general
  )


#||
define sealed method draw-design
    (drawable :: <drawable>, region :: <region-difference>) => (record)
  //---*** Should draw the difference, but I dunno how to do that in general
end method draw-design;
||#

(defmethod draw-design ((drawable <abstract-sheet>) (region <region-difference>))
  ;;---*** Should draw the difference, but I dunno how to do that in general
  )

(defmethod draw-design ((drawable <abstract-medium>) (region <region-difference>))
  ;;---*** Should draw the difference, but I dunno how to do that in general
  )


#||
/// Simple figures

define sealed method draw-design
    (drawable :: <drawable>, point :: <standard-point>) => (record)
  let (x, y) = point-position(point);
  draw-point(drawable, x, y)
end method draw-design;
||#

(defmethod draw-design ((drawable <abstract-sheet>) (point <standard-point>))
  (multiple-value-bind (x y)
      (point-position point)
    (draw-point drawable x y)))

(defmethod draw-design ((drawable <abstract-medium>) (point <standard-point>))
  (multiple-value-bind (x y)
      (point-position point)
    (draw-point drawable x y)))


#||
define sealed method draw-design
    (drawable :: <drawable>, line :: <standard-line>) => (record)
  let (x1, y1) = line-start-position(line);
  let (x2, y2) = line-end-position(line);
  draw-line(drawable, x1, y1, x2, y2)
end method draw-design;
||#

(defmethod draw-design ((drawable <abstract-sheet>) (line <standard-line>))
  (multiple-value-bind (x1 y1)
      (line-start-position line)
    (multiple-value-bind (x2 y2)
	(line-end-position line)
      (draw-line drawable x1 y1 x2 y2))))

(defmethod draw-design ((drawable <abstract-medium>) (line <standard-line>))
  (multiple-value-bind (x1 y1)
      (line-start-position line)
    (multiple-value-bind (x2 y2)
	(line-end-position line)
      (draw-line drawable x1 y1 x2 y2))))


#||
define sealed method draw-design
    (drawable :: <drawable>, rectangle :: <standard-rectangle>) => (record)
  let (x1, y1, x2, y2) = rectangle-edges(rectangle);
  draw-rectangle(drawable, x1, y1, x2, y2, filled?: #t)
end method draw-design;
||#

(defmethod draw-design ((drawable <abstract-sheet>) (rectangle <standard-rectangle>))
  (multiple-value-bind (x1 y1 x2 y2)
      (rectangle-edges rectangle)
    (draw-rectangle drawable x1 y1 x2 y2 :filled? t)))

(defmethod draw-design ((drawable <abstract-medium>) (rectangle <standard-rectangle>))
  (multiple-value-bind (x1 y1 x2 y2)
      (rectangle-edges rectangle)
    (draw-rectangle drawable x1 y1 x2 y2 :filled? t)))


#||
define sealed method draw-design
    (drawable :: <drawable>, polygon :: <standard-polygon>) => (record)
  let coords = polygon-coordinates(polygon);
  draw-polygon(drawable, coords, closed?: #t, filled?: #t)
end method draw-design;
||#

(defmethod draw-design ((drawable <abstract-sheet>) (polygon <standard-polygon>))
  (let ((coords (polygon-coordinates polygon)))
    (draw-polygon drawable coords :closed? t :filled? t)))

(defmethod draw-design ((drawable <abstract-medium>) (polygon <standard-polygon>))
  (let ((coords (polygon-coordinates polygon)))
    (draw-polygon drawable coords :closed? t :filled? t)))


#||
define sealed method draw-design
    (drawable :: <drawable>, polyline :: <standard-polyline>) => (record)
  let coords = polygon-coordinates(polyline);
  draw-polygon(drawable, coords, closed?: #f, filled?: #f)
end method draw-design;
||#

(defmethod draw-design ((drawable <abstract-sheet>) (polyline <standard-polyline>))
  (let ((coords (polygon-coordinates polyline)))
    (draw-polygon drawable coords :closed? nil :filled? nil)))

(defmethod draw-design ((drawable <abstract-medium>) (polyline <standard-polyline>))
  (let ((coords (polygon-coordinates polyline)))
    (draw-polygon drawable coords :closed? nil :filled? nil)))


#||
define sealed method draw-design
    (drawable :: <drawable>, ellipse :: <standard-ellipse>) => (record)
  let (center-x, center-y) = ellipse-center-position(ellipse);
  let (radius-1-dx, radius-1-dy, radius-2-dx, radius-2-dy) = ellipse-radii(ellipse);
  draw-ellipse(drawable,
	       center-x, center-y,
	       radius-1-dx, radius-1-dy, radius-2-dx, radius-2-dy,
	       start-angle: ellipse-start-angle(ellipse),
	       end-angle: ellipse-end-angle(ellipse),
	       filled?: #t)
end method draw-design;
||#

(defmethod draw-design ((drawable <abstract-sheet>) (ellipse <standard-ellipse>))
  (multiple-value-bind (center-x center-y)
      (ellipse-center-position ellipse)
    (multiple-value-bind (radius-1-dx radius-1-dy radius-2-dx radius-2-dy)
	(ellipse-radii ellipse)
      (draw-ellipse drawable
		    center-x center-y
		    radius-1-dx radius-1-dy radius-2-dx radius-2-dy
		    :start-angle (ellipse-start-angle ellipse)
		    :end-angle   (ellipse-end-angle ellipse)
		    :filled? t))))

(defmethod draw-design ((drawable <abstract-medium>) (ellipse <standard-ellipse>))
  (multiple-value-bind (center-x center-y)
      (ellipse-center-position ellipse)
    (multiple-value-bind (radius-1-dx radius-1-dy radius-2-dx radius-2-dy)
	(ellipse-radii ellipse)
      (draw-ellipse drawable
		    center-x center-y
		    radius-1-dx radius-1-dy radius-2-dx radius-2-dy
		    :start-angle (ellipse-start-angle ellipse)
		    :end-angle   (ellipse-end-angle ellipse)
		    :filled? t))))


#||
define sealed method draw-design
    (drawable :: <drawable>, arc :: <standard-elliptical-arc>) => (record)
  let (center-x, center-y) = ellipse-center-position(arc);
  let (radius-1-dx, radius-1-dy, radius-2-dx, radius-2-dy) = ellipse-radii(arc);
  draw-ellipse(drawable,
	       center-x, center-y,
	       radius-1-dx, radius-1-dy, radius-2-dx, radius-2-dy,
	       start-angle: ellipse-start-angle(arc),
	       end-angle: ellipse-end-angle(arc),
	       filled?: #f)
end method draw-design;
||#

(defmethod draw-design ((drawable <abstract-sheet>) (arc <standard-elliptical-arc>))
  (multiple-value-bind (center-x center-y)
      (ellipse-center-position arc)
    (multiple-value-bind (radius-1-dx radius-1-dy radius-2-dx radius-2-dy)
	(ellipse-radii arc)
      (draw-ellipse drawable
		    center-x center-y
		    radius-1-dx radius-1-dy radius-2-dx radius-2-dy
		    :start-angle (ellipse-start-angle arc)
		    :end-angle   (ellipse-end-angle arc)
		    :filled? nil))))

(defmethod draw-design ((drawable <abstract-medium>) (arc <standard-elliptical-arc>))
  (multiple-value-bind (center-x center-y)
      (ellipse-center-position arc)
    (multiple-value-bind (radius-1-dx radius-1-dy radius-2-dx radius-2-dy)
	(ellipse-radii arc)
      (draw-ellipse drawable
		    center-x center-y
		    radius-1-dx radius-1-dy radius-2-dx radius-2-dy
		    :start-angle (ellipse-start-angle arc)
		    :end-angle   (ellipse-end-angle arc)
		    :filled? nil))))

