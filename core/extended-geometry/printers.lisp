;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-EXTENDED-GEOMETRY-INTERNALS -*-
(in-package #:duim-extended-geometry-internals)

(defmethod print-object ((transform <general-transform>) stream)
  (print-unreadable-object (transform stream :type t :identity t)
    (if (and (zerop (%mxy transform))
             (zerop (%myx transform)))
      (format stream
              "scale (~d,~d) translate (~d,~d)"
              (%mxx transform)
              (%myy transform)
              (%tx transform)
              (%ty transform))
      (format stream
              "[~d ~d ~d ~d] ~d ~d"
              (%mxx transform)
              (%mxy transform)
              (%myx transform)
              (%myy transform)
              (%tx transform)
              (%ty transform)))))


(defmethod print-object ((line <line>) stream)
  (print-unreadable-object (line stream :type t :identity t)
    (multiple-value-bind (start-x start-y)
        (line-start-position line)
      (multiple-value-bind (end-x end-y)
          (line-end-position line)
        (format stream
                "(~d,~d):(~d,~d)"
                start-x
                start-y
                end-x
                end-y)))))


(defmethod print-object ((rectangle <standard-rectangle>) stream)
  (print-unreadable-object (rectangle stream :type t :identity t)
    (multiple-value-bind (left top right bottom)
        (rectangle-edges rectangle)
      (format stream
              "(~d,~d):(~d,~d)"
              left
              top
              right
              bottom))))



#||
define method print-object
    (transform :: <general-transform>, stream :: <stream>) => ()
  printing-object (transform, stream, type?: #t, identity?: #t)
    if (zero?(transform.%mxy) & zero?(transform.%myx))
      format(stream, "scale (%d,%d) translate (%d,%d)",
	     transform.%mxx, transform.%myy, transform.%tx, transform.%ty)
    else
      format(stream, "[%d %d %d %d] %d %d", 
	     transform.%mxx, transform.%mxy, transform.%myx, transform.%myy,
	     transform.%tx, transform.%ty)
    end
  end
end method print-object;


define method print-object
    (line :: <line>, stream :: <stream>) => ()
  printing-object (line, stream, type?: #t, identity?: #t)
    let (start-x, start-y) = line-start-position(line);
    let (end-x, end-y) = line-end-position(line);
    format(stream, "(%d,%d):(%d,%d)", start-x, start-y, end-x, end-y)
  end
end method print-object;

define method print-object
    (rectangle :: <standard-rectangle>, stream :: <stream>) => ()
  printing-object (rectangle, stream, type?: #t, identity?: #t)
    let (left, top, right, bottom) = rectangle-edges(rectangle);
    format(stream, "(%d,%d):(%d,%d)", left, top, right, bottom)
  end
end method print-object;
||#
