;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

(asdf:defsystem :extended-geometry
    :description "DUIM extended geometry"
    :long-description "The DUIM-Extended-Geometry Library module provides more extensive support for co-ordinate geometry that is only required for more specialist uses."
    :author      "Scott McKay, Andy Armstrong (Lisp port: Duncan Rose <duncan@robotcat.demon.co.uk>)"
    :version     (:read-file-form "version.sexp")
    :licence     "BSD-2-Clause"
    :depends-on (#:duim-utilities
		 #:geometry
		 #:dcs
		 #:sheets
		 #:graphics)
    :serial t
    :components
    ((:file "package")
     (:file "protocols")
     (:file "classes")
     (:file "transforms")
     (:file "polygons")
     (:file "lines")
     (:file "rectangles")
     (:file "ellipses")
     (:file "region-graphics")))

