;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

;;; Two packages are defined in this file, a public and private API.

(defpackage :duim-extended-geometry-internals
  (:use #:common-lisp
        #:duim-utilities
        #:duim-geometry-internals
        #:duim-dcs-internals
        #:duim-sheets-internals
        #:duim-graphics-internals)
  (:export

  ;;;
  ;;; The private API
  ;;;

   ;; Complex transforms

   #:<general-transform>

   ;; Complex regions

   #:<standard-ellipse>
   #:<standard-elliptical-arc>
   #:<standard-line>
   #:<standard-polygon>
   #:<standard-polyline>
   #:<standard-rectangle>



  ;;;
  ;;; The public API
  ;;;

   ;; Complex transforms

   #:<reflection-underspecified>
   #:<transform-underspecified>
   #:make-3-point-transform
   #:make-3-point-transform*

   ;; Complex regions

   #:<ellipse>
   #:<elliptical-arc>
   #:<line>
   #:<polygon>
   #:<polyline>
   #:<rectangle>
   #:do-polygon-coordinates
   #:do-polygon-segments
   #:ellipse-center-position
   #:ellipse-center-point
   #:ellipse-end-angle
   #:ellipse-radii
   #:ellipse-start-angle
   #:ellipsep
   #:elliptical-arc-p
   #:line-end-position
   #:line-end-point
   #:line-start-position
   #:line-start-point
   #:linep
   #:make-ellipse
   #:make-ellipse*
   #:make-elliptical-arc
   #:make-elliptical-arc*
   #:make-line
   #:make-line*
   #:make-polygon
   #:make-polygon*
   #:make-polyline
   #:make-polyline*
   #:make-rectangle
   #:make-rectangle*
   #:polygon-coordinates
   #:polygon-points
   #:polygonp
   #:polyline-closed?
   #:polylinep
   #:rectangle-edges
   #:rectangle-height
   #:rectangle-max-point
   #:rectangle-max-position
   #:rectangle-min-point
   #:rectangle-min-position
   #:rectangle-size
   #:rectangle-width
   #:rectanglep

   ;; Region-based graphics

   #:draw-design))


(defpackage :duim-extended-geometry
  (:use #:common-lisp
        #:duim-utilities
        #:duim-geometry
        #:duim-dcs
        #:duim-sheets
        #:duim-graphics
	#:duim-extended-geometry-internals)
  (:export

   ;; Complex transforms

   #:<reflection-underspecified>
   #:<transform-underspecified>
   #:make-3-point-transform
   #:make-3-point-transform*

   ;; Complex regions

   #:<ellipse>
   #:<elliptical-arc>
   #:<line>
   #:<polygon>
   #:<polyline>
   #:<rectangle>
   #:do-polygon-coordinates
   #:do-polygon-segments
   #:ellipse-center-position
   #:ellipse-center-point
   #:ellipse-end-angle
   #:ellipse-radii
   #:ellipse-start-angle
   #:ellipsep
   #:elliptical-arc?
   #:line-end-position
   #:line-end-point
   #:line-start-position
   #:line-start-point
   #:linep
   #:make-ellipse
   #:make-ellipse*
   #:make-elliptical-arc
   #:make-elliptical-arc*
   #:make-line
   #:make-line*
   #:make-polygon
   #:make-polygon*
   #:make-polyline
   #:make-polyline*
   #:make-rectangle
   #:make-rectangle*
   #:polygon-coordinates
   #:polygon-points
   #:polygonp
   #:polyline-closed?
   #:polylinep
   #:rectangle-edges
   #:rectangle-height
   #:rectangle-max-point
   #:rectangle-max-position
   #:rectangle-min-point
   #:rectangle-min-position
   #:rectangle-size
   #:rectangle-width
   #:rectanglep

   ;; Region-based graphics

   #:draw-design))



#||
define library duim-extended-geometry
  use dylan;

  use duim-utilities;
  use duim-geometry;
  use duim-DCs;
  use duim-sheets;
  use duim-graphics;  

  export duim-extended-geometry;
  export duim-extended-geometry-internals;
end library duim-extended-geometry;

define module duim-extended-geometry
  // Complex transforms
  create <reflection-underspecified>,
         <transform-underspecified>,
         make-3-point-transform, make-3-point-transform*;

  // Complex regions
  create <ellipse>,
         <elliptical-arc>,
         <line>,
         <polygon>,
         <polyline>,
         <rectangle>,
         do-polygon-coordinates,
         do-polygon-segments,
         ellipse-center-position,
         ellipse-center-point,
         ellipse-end-angle,
         ellipse-radii,
         ellipse-start-angle,
         ellipse?,
         elliptical-arc?,
         line-end-position,
         line-end-point,
         line-start-position,
         line-start-point,
         line?,
         make-ellipse, make-ellipse*,
         make-elliptical-arc, make-elliptical-arc*,
         make-line, make-line*,
         make-polygon, make-polygon*,
         make-polyline, make-polyline*,
         make-rectangle, make-rectangle*,
         polygon-coordinates,
         polygon-points,
         polygon?,
         polyline-closed?,
         polyline?,
         rectangle-edges,
         rectangle-height,
         rectangle-max-point,
         rectangle-max-position,
         rectangle-min-point,
         rectangle-min-position,
         rectangle-size,
         rectangle-width,
         rectangle?;

  // Region-based graphics
  create draw-design;
end module duim-extended-geometry;

define module duim-extended-geometry-internals
  use dylan;
  use duim-imports;
  use duim-utilities;
  use duim-geometry-internals;
  use duim-DCs-internals;
  use duim-sheets-internals;
  use duim-graphics-internals; 
  use duim-extended-geometry, export: all;

  // Complex transforms
  export <general-transform>;

  // Complex regions
  export <standard-ellipse>,
         <standard-elliptical-arc>,
         <standard-line>,
         <standard-polygon>,
         <standard-polyline>,
         <standard-rectangle>;
end module duim-extended-geometry-internals;
||#
