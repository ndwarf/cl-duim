;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-EXTENDED-GEOMETRY-INTERNALS -*-
(in-package #:duim-extended-geometry-internals)

#||
/// Polygons and polylines

define protocol <<polygon-protocol>> (<<region-protocol>>)
  function polygon-points
    (polygon) => (points :: <sequence>);
  function do-polygon-coordinates
    (function :: <function>, polygon) => ();
  function do-polygon-segments
    (function :: <function>, polygon) => ();
  function polyline-closed?
    (polyline) => (true? :: <boolean>);
end protocol <<polygon-protocol>>;
||#
#||
(define-protocol <<polygon-protocol>> (<<region-protocol>>)
  (:function polygon-points (polygon))
  (:function do-polygon-coordinates (function polygon))
  (:function do-polygon-segments (function polygon))
  (:function polyline-closed? (polyline)))
||#

#||
define abstract class <polygon-mixin> (<object>)
  sealed slot %coords :: false-or(<vector>) = #f,
    init-keyword: coordinates:;
  sealed slot %points :: false-or(<vector>) = #f,
    init-keyword: points:;
end class <polygon-mixin>;
||#

(defclass <polygon-mixin> ()
  ((%coords :type (or null vector) :initarg :coordinates :initform nil :accessor %coords)
   (%points :type (or null vector) :initarg :points :initform nil :accessor %points))
  (:metaclass <abstract-metaclass>))


#||
define method polygon-points
    (polygon :: <polygon-mixin>) => (points :: <vector>)
  polygon.%points
  | begin
      // If %points empty, %coords will not be
      let coords = polygon.%coords;
      let npoints :: <integer> = truncate/(size(coords), 2);
      let points :: <simple-object-vector> = make(<simple-vector>, size: npoints);
      without-bounds-checks
	for (i :: <integer> from 0 below npoints)
	  points[i] := make-point(coords[i * 2 + 0], coords[i * 2 + 1])
	end
      end;
      polygon.%points := points
    end
end method polygon-points;
||#

(defmethod polygon-points ((polygon <polygon-mixin>))
  (or (%points polygon)
      ;; If %points empty, %coords will not be
      (let* ((coords  (%coords polygon))
	     (npoints (truncate (length coords) 2))
	     (points  (make-array npoints)))
	(without-bounds-checks
	  (loop for i from 0 below npoints
	     do (setf (aref points i)
		      (make-point (aref coords (+ (* i 2) 0)) (aref coords (+ (* i 2) 1))))))
	(setf (%points polygon) points))))


#||
define method polygon-coordinates
    (polygon :: <polygon-mixin>) => (coords :: <vector>)
  polygon.%coords
  | begin
      // If %coords empty, %points will not be
      let points = polygon.%points;
      let npoints :: <integer> = size(points);
      let coords :: <simple-object-vector> = make(<simple-vector>, size: npoints * 2);
      without-bounds-checks
	for (i :: <integer> from 0 below npoints)
	  coords[i * 2 + 0] := point-x(points[i]);
	  coords[i * 2 + 1] := point-y(points[i])
	end
     end;
      polygon.%coords := coords
    end
end method polygon-coordinates;
||#

(defgeneric polygon-coordinates (polygon)
  (:documentation
"
Returns a sequence of coordinate pairs that specify the segments in
_polygon-or-polyline_.
"))

(defmethod polygon-coordinates ((polygon <polygon-mixin>))
  (or (%coords polygon)
      ;; If %coords empty, %points will not be
      (let* ((points  (%points polygon))
	     (npoints (length points))
	     (coords  (make-array (* npoints 2))))
	(without-bounds-checks
	  (loop for i from 0 below npoints
	     do (setf (aref coords (+ (* i 2) 0)) (point-x (aref points i)))
	     do (setf (aref coords (+ (* i 2) 1)) (point-y (aref points i)))))
	(setf (%coords polygon) coords))))


#||
define method do-polygon-coordinates
    (function :: <function>, polygon :: <polygon-mixin>) => ()
  if (polygon.%coords)
    let coords = polygon.%coords;
    let ncoords :: <integer> = size(coords) - 1;
    let i :: <integer> = -1;
    until (i = ncoords)
      function(coords[inc!(i)], coords[inc!(i)])
    end
  else
    local method do-coords (point) => ()
	    function(point-x(point), point-y(point))
	  end method;
    do(do-coords, polygon.%points)
  end
end method do-polygon-coordinates;
||#

(defmethod do-polygon-coordinates ((function function) (polygon <polygon-mixin>))
  (if (%coords polygon)
      (let* ((coords  (%coords polygon))
	     (ncoords (- (length coords) 1))
	     (i       -1))
        (loop until (= i ncoords)
	   do (funcall function (aref coords (incf i)) (aref coords (incf i)))))
      (labels ((do-coords (point)
	         (funcall function (point-x point) (point-y point))))
	;; FIXME: If %points was a list, this could be MAPCAR
	(map nil #'do-coords (%points polygon)))))


#||
define method do-polygon-segments
    (function :: <function>, polygon :: <polygon-mixin>) => ()
  if (polygon.%coords)
    let coords = polygon.%coords;
    let ncoords :: <integer> = size(coords) - 1;
    let x1 = coords[0];
    let y1 = coords[1];
    let x = x1;
    let y = y1;
    let i :: <integer> = 1;
    until (i = ncoords)
      function(x, y,
	       x := coords[inc!(i)], y := coords[inc!(i)])
    end;
    when (polyline-closed?(polygon))
      function(x, y, x1, y1)
    end
  else
    let (x1, y1) = point-position(polygon.%points[0]);
    let x = x1;
    let y = y1;
    let points = polygon.%points;
    let npoints :: <integer> = size(points);
    for (i :: <integer> from 0 below npoints - 1)
      let (nx, ny) = point-position(points[i + 1]);
      function(x, y, nx, ny);
      x := nx;
      y := ny
    end;
    when (polyline-closed?(polygon))
      function(x, y, x1, y1)
    end
  end
end method do-polygon-segments;
||#

(defmethod do-polygon-segments ((function function) (polygon <polygon-mixin>))
  (if (%coords polygon)
      (let* ((coords (%coords polygon))
	     (ncoords (- (length coords) 1))
	     (x1 (aref coords 0))
	     (y1 (aref coords 1))
	     (x x1)
	     (y y1)
	     (i 1))
	(loop until (= i ncoords)
	   do (funcall function x y
		       (setf x (aref coords (incf i)))
		       (setf y (aref coords (incf i)))))
	(when (polyline-closed? polygon)
	  (funcall function x y x1 y1)))
      ;; else
      (multiple-value-bind (x1 y1)
	  (point-position (aref (%points polygon) 0))
	(let* ((x x1)
	       (y y1)
	       (points (%points polygon))
	       (npoints (length points)))
	  (loop for i from 0 below (- npoints 1)
	     do (multiple-value-bind (nx ny)
		    (point-position (aref points (+ i 1)))
		  (funcall function x y nx ny)
		  (setf x nx)
		  (setf y ny)))
	  (when (polyline-closed? polygon)
	    (funcall function x y x1 y1))))))


#||
define method box-edges
    (polygon :: <polygon-mixin>)
 => (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
  let min-x = $largest-coordinate;
  let min-y = $largest-coordinate;
  let max-x = $smallest-coordinate;
  let max-y = $smallest-coordinate;
  local method add-coord (x, y) => ()
	  min!(min-x, x);
	  min!(min-y, y);
	  max!(max-x, x);
	  max!(max-y, y)
	end method;
  do-polygon-coordinates(add-coord, polygon);
  fix-box(min-x, min-y, max-x, max-y)
end method box-edges;
||#

(defmethod box-edges ((polygon <polygon-mixin>))
  (let ((min-x +largest-coordinate+)
	(min-y +largest-coordinate+)
	(max-x +smallest-coordinate+)
	(max-y +smallest-coordinate+))
    (labels ((add-coord (x y)
	       (setf min-x (min min-x x))
	       (setf min-y (min min-y y))
	       (setf max-x (max max-x x))
	       (setf max-y (max max-y y))))
      (do-polygon-coordinates #'add-coord polygon))
    (fix-box min-x min-y max-x max-y)))


#||
define sealed class <standard-polyline> (<polygon-mixin>, <polyline>)
  sealed constant slot polyline-closed? :: <boolean> = #f,
    init-keyword: closed?:;
end class <standard-polyline>;

define sealed domain make (singleton(<standard-polyline>));
define sealed domain initialize (<standard-polyline>);
||#

(defclass <standard-polyline> (<polygon-mixin> <polyline>)
  ((polyline-closed? :type boolean :initform nil :initarg :closed? :reader polyline-closed?)))


#||
define inline function make-polyline
    (coord-seq, #key closed?) => (polyline :: <standard-polyline>)
  assert(even?(size(coord-seq)),
	 "There must be an even number of coordinates in %=", coord-seq);
  make(<standard-polyline>,
       coordinates: as(<simple-vector>, coord-seq),
       closed?: closed?)
end function make-polyline;
||#

(defun make-polyline (coord-seq &key closed?)
"
Returns an object of class <polyline> consisting of the segments
connecting each of the points in _point-seq_ or the points represented
by the coordinate pairs in _coord-seq_. _point-seq_ is a sequence of
points; _coord-seq_ is a sequence of coordinate pairs, which are real
numbers. It is an error if _coord-seq_ does not contain an even number
of elements.

If closed? is #t, then the segment connecting the first point and the
last point is included in the polyline. The default for closed? is #f.

The function 'make-polyline*' is identical to 'make-polyline', except
that it passes composite objects, rather than separate coordinates, in
its arguments. You should be aware that using this function may lead
to a loss of performance.
"
  (unless (evenp (length coord-seq))
    (error "There must be an even number of coordinates in ~a" coord-seq))
  (make-instance '<standard-polyline>
                 :coordinates (coerce coord-seq 'vector)
                 :closed? closed?))


#||
define inline function make-polyline*
    (point-seq, #key closed?) => (polyline :: <standard-polyline>)
  make(<standard-polyline>,
       points: as(<simple-vector>, point-seq), closed?: closed?)
end function make-polyline*;
||#

(defun make-polyline* (point-seq &key closed?)
"
Returns an object of class <polyline> consisting of the segments
connecting each of the points in _point-seq_ or the points represented
by the coordinate pairs in _coord-seq_. _point-seq_ is a sequence of
points; _coord-seq_ is a sequence of coordinate pairs, which are real
numbers. It is an error if _coord-seq_ does not contain an even number
of elements.

If closed? is #t, then the segment connecting the first point and the
last point is included in the polyline. The default for closed? is #f.

The function 'make-polyline*' is identical to 'make-polyline', except
that it passes composite objects, rather than separate coordinates, in
its arguments. You should be aware that using this function may lead
to a loss of performance.
"
  (make-instance '<standard-polyline>
                 :points (coerce point-seq 'vector)
                 :closed? closed?))


#||
define sealed inline method make
    (class == <polyline>, #key points, closed?)
 => (polyline :: <standard-polyline>)
  make-polyline*(points, closed?: closed?)
end method make;


define method transform-region
    (transform :: <transform>, polyline :: <standard-polyline>)
 => (polyline :: <standard-polyline>)
  let coords :: <stretchy-object-vector> = make(<stretchy-vector>);
  local method transform-coord (x, y) => ()
	  let (nx, ny) = transform-position(transform, x, y);
	  add!(coords, ny);
	  add!(coords, nx)
	end method;
  do-polygon-coordinates(transform-coord, polyline);
  make-polyline(coords, closed?: polyline-closed?(polyline))
end method transform-region;
||#

(defmethod transform-region ((transform <transform>) (polyline <standard-polyline>))
  (let ((coords (MAKE-STRETCHY-VECTOR)))
    (labels ((transform-coord (x y)
	       (multiple-value-bind (nx ny)
		   (transform-position transform x y)
		 (add! coords ny)
		 (add! coords nx))))
      (do-polygon-coordinates #'transform-coord polyline))
    (make-polyline coords :closed? (polyline-closed? polyline))))


#||
define method region-equal
    (p1 :: <standard-polyline>, p2 :: <standard-polyline>) => (true? :: <boolean>)
  polygon-coordinates(p1) = polygon-coordinates(p2)
  & polyline-closed?(p1) = polyline-closed?(p2)
end method region-equal;
||#

(defmethod region-equal ((p1 <standard-polyline>) (p2 <standard-polyline>))
  (and (equal? (polygon-coordinates p1) (polygon-coordinates p2))
       (equal? (polyline-closed? p1)    (polyline-closed? p2))))


#||
define sealed class <standard-polygon> (<polygon-mixin>, <polygon>)
end class <standard-polygon>;

define sealed domain make (singleton(<standard-polygon>));
define sealed domain initialize (<standard-polygon>);
||#


(defclass <standard-polygon> (<polygon-mixin> <polygon>) ())


#||
define inline function make-polygon
    (coord-seq) => (polygon :: <standard-polygon>)
  assert(even?(size(coord-seq)),
	 "There must be an even number of coordinates in %=", coord-seq);
  make(<standard-polygon>,
       coordinates: as(<simple-vector>, coord-seq))
end function make-polygon;
||#

(defun make-polygon (coord-seq)
"
Returns an object of class <polygon> consisting of the area contained
in the boundary that is specified by the segments connecting each of
the points in _point-seq_ or the points represented by the coordinate
pairs in _coord-seq_. _point-seq_ is a sequence of points; _coord-seq_
is a sequence of coordinate pairs, which are real numbers. It is an
error if _coord-seq_ does not contain an even number of elements.

The function 'make-polygon*' is identical to 'make-polygon', except
that it passes composite objects, rather than separate coordinates, in
its arguments. You should be aware that using this function may lead
to a loss of performance.
"
  (assert (evenp (length coord-seq)) (coord-seq) "There must be an even number of coordinates in ~A" coord-seq)
  (make-instance '<standard-polygon> :coordinates (coerce coord-seq 'vector)))


#||
define inline function make-polygon*
    (point-seq) => (polygon :: <standard-polygon>)
  make(<standard-polygon>,
       points: as(<simple-vector>, point-seq))
end function make-polygon*;
||#

(defun make-polygon* (point-seq)
"
Returns an object of class <polygon> consisting of the area contained
in the boundary that is specified by the segments connecting each of
the points in _point-seq_ or the points represented by the coordinate
pairs in _coord-seq_. _point-seq_ is a sequence of points; _coord-seq_
is a sequence of coordinate pairs, which are real numbers. It is an
error if _coord-seq_ does not contain an even number of elements.

The function 'make-polygon*' is identical to 'make-polygon', except
that it passes composite objects, rather than separate coordinates, in
its arguments. You should be aware that using this function may lead
to a loss of performance.
"
  (make-instance '<standard-polygon> :points (coerce point-seq 'vector)))


#||
define sealed inline method make
    (class == <polygon>, #key points)
 => (polygon :: <standard-polygon>)
  make-polygon*(points);
end method make;


define method polyline-closed? (polygon :: <standard-polygon>) => (true? :: <boolean>)
  #t
end method polyline-closed?;
||#

(defmethod polyline-closed? ((polygon <standard-polygon>))
  t)


#||
define method transform-region
    (transform :: <transform>, polygon :: <standard-polygon>)
 => (polygon :: <standard-polygon>)
  let coords :: <stretchy-object-vector> = make(<stretchy-vector>);
  local method transform-coord (x, y) => ()
	  let (nx, ny) = transform-position(transform, x, y);
	  add!(coords, ny);
	  add!(coords, nx)
	end method;
  do-polygon-coordinates(transform-coord, polygon);
  make-polygon(coords)
end method transform-region;
||#

(defmethod transform-region ((transform <transform>) (polygon <standard-polygon>))
  (let ((coords (MAKE-STRETCHY-VECTOR)))
    (labels ((transform-coord (x y)
	       (multiple-value-bind (nx ny)
		   (transform-position transform x y)
		 (add! coords ny)
		 (add! coords nx))))
      (do-polygon-coordinates #'transform-coord polygon))
    (make-polygon coords)))


#||
define method region-equal
    (p1 :: <standard-polygon>, p2 :: <standard-polygon>) => (true? :: <boolean>)
  polygon-coordinates(p1) = polygon-coordinates(p2)
end method region-equal;
||#

(defmethod region-equal ((p1 <standard-polygon>) (p2 <standard-polygon>))
  (equal? (polygon-coordinates p1) (polygon-coordinates p2)))


#||
define method region-contains-position?
    (polygon :: <standard-polygon>, x :: <real>, y :: <real>) => (true? :: <boolean>)
  let (left, top, right, bottom) = box-edges(polygon);
  ltrb-contains-position?(left, top, right, bottom,
			  fix-coordinate(x), fix-coordinate(y))
  & position-inside-polygon?(x, y, polygon-coordinates(polygon), closed?: #t)
end method region-contains-position?;
||#

(defmethod region-contains-position? ((polygon <standard-polygon>) (x real) (y real))
  (multiple-value-bind (left top right bottom)
      (box-edges polygon)
    (and (ltrb-contains-position? left top right bottom
				  (fix-coordinate x) (fix-coordinate y))
	 (position-inside-polygon? x y (polygon-coordinates polygon) :closed? t))))



