;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-EXTENDED-GEOMETRY-INTERNALS -*-
(in-package #:duim-extended-geometry-internals)

;;; From ellipses.lisp

(define-protocol <<ellipse-protocol>> (<<region-protocol>>)
  (:function ellipse-center-point (ellipse)
	     (:documentation
"
Returns the center point of _ellipse-object_ as a <point> object.

Defined by: <<ellipse-protocol>>
"))
  (:function ellipse-center-position (ellipse)
	     (:documentation
"
Returns the coordinates of the center point of _elliptical-object_ as
two values, x and y.

The values _x_ and _y_ represent the x and y coordinates of the center
of the elliptical object, respectively.

Defined by: <<ellipse-protocol>>
"))
  (:function ellipse-radii (ellipse)
	     (:documentation
"
Returns four values corresponding to the two radius vectors of
_elliptical-object_. These values may be canonicalized in some way,
and so may not be the same as the values passed to the constructor
function.

Defined by: <<ellipse-protocol>>
"))
  (:function ellipse-start-angle (ellipse)
	     (:documentation
"
Returns the start angle of _elliptical-object_. If _elliptical-object_
is a full ellipse or closed path then 'ellipse-start-angle' returns
nil; otherwise the value will be a number greater than or equal to
zero, and less than 2pi.

Defined by: <<ellipse-protocol>>
"))
  (:function ellipse-end-angle (ellipse)
	     (:documentation
"
Returns the end angle of _elliptical-object_. If _elliptical-object_
is a full ellipse or closed path then 'ellipse-end-angle' returns nil;
otherwise the value is a number greater than 0, and less than or equal
to 2pi.

Defined by: <<ellipse-protocol>>
")))


;;; From lines.lisp

(define-protocol <<line-protocol>> (<<polygon-protocol>>)
  (:function line-start-point (line)
	     (:documentation
"
Returns the starting point of _line_ as a <point> object.

Defined by: <<line-protocol>>
"))
  (:function line-end-point (line)
	     (:documentation
"
Returns the ending point of _line_ as a <point> object.

Defined by: <<line-protocol>>
"))
  (:function line-start-position (line)
	     (:documentation
"
Returns two real numbers representing the _x_ and _y_ coordinates of
the starting point of _line_.

The values _x_ and _y_ represent the x and y coordinates of the start
of the line, respectively.

Defined by: <<line-protocol>>
"))
  (:function line-end-position (line)
	     (:documentation
"
Returns two real numbers representing the _x_ and _y_ coordinates of
the ending point of _line_.

The values _x_ and _y_ represent the x and y coordinates of the end of
the line, respectively.

Defined by: <<line-protocol>>
")))


;;; From polygons.lisp

(define-protocol <<polygon-protocol>> (<<region-protocol>>)
  (:function polygon-points (polygon)
	     (:documentation
"
Returns a sequence of points that specify the segments in
_polygon-or-polyline_.

Defined by: <<polygon-protocol>>
"))
  (:function do-polygon-coordinates (function polygon)
	     (:documentation
"
Applies _function_ to all of the coordinates of the vertices of
_polygon_. _function_ is a function of two arguments, the _x_ and _y_
coordinates of the vertex. 'do-polygon-coordinates' returns nil.

Defined by: <<polygon-protocol>>
"))
  (:function do-polygon-segments (function polygon)
	     (:documentation
"
Applies _function_ to the segments that compose _polygon_. _function_
is a function of four arguments, the _x_ and _y_ coordinates of the
start of the segment, and the _x_ and _y_ coordinates of the end of
the segment. When 'do-polygon-segments' is called on a closed
polyline, it calls _function_ on the segment that connects the last
point back to the first point.

The function 'do-polygon-segments' returns nil.

Defined by: <<polygon-protocol>>
"))
  (:function polyline-closed? (polyline)
	     (:documentation
"
Returns t if the polyline _polyline_ is closed, otherwise returns
nil. This function need be implemented only for polylines, not for
polygons.

Defined by: <<polygon-protocol>>
")))


;;; From rectangles.lisp

(define-protocol <<rectangle-protocol>> (<<polygon-protocol>>)
  (:function rectangle-edges (rectangle)
	     (:documentation
"
Returns the coordinates of the minimum x and y and maximum x and y of
the rectangle _rectangle_ as four values, _min-x_, _min-y_, _max-x_,
and _max-y_.

The value _min-x_ represents the x coordinate of the top left of the
rectangle.

The value _min-y_ represents the y coordinate of the top left of the
rectangle.

The value _max-x_ represents the x coordinate of the bottom right of
the rectangle.

The value _max-y_ represents the y coordinate of the bottom right of
the rectangle.

Defined by: <<rectangle-protocol>>
"))
  (:function rectangle-min-point (rectangle)
	     (:documentation
"
Returns the left top point of the rectangle.

Defined by: <<rectangle-protocol>>
"))
  (:function rectangle-max-point (rectangle)
	     (:documentation
"
Returns the bottom right point of the rectangle.

Defined by: <<rectangle-protocol>>
"))
  (:function rectangle-min-position (rectangle)
	     (:documentation
"
Returns the x and y coordinates of the left top of the rectangle.

Defined by: <<rectangle-protocol>>
"))
  (:function rectangle-max-position (rectangle)
	     (:documentation
"
Returns the x and y coordinates of the bottom right of the rectangle
as two values.

Defined by: <<rectangle-protocol>>
"))
  (:function rectangle-size (rectangle)
	     (:documentation
"
Returns two values, the width and height.

Defined by: <<rectangle-protocol>>
"))
  (:function rectangle-width (rectangle)
	     (:documentation
"
Returns the width of the rectangle _rectangle_, which is the
difference between the maximum x and its minimum x.

Defined by: <<rectangle-protocol>>
"))
  (:function rectangle-height (rectangle)
	     (:documentation
"
Returns the height of the rectangle, which is the difference between
the maximum y and its minimum y.

Defined by: <<rectangle-protocol>>
")))


