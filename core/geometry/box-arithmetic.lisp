;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GEOMETRY-INTERNALS -*-
(in-package #:duim-geometry-internals)

#||
/// Arithmetic on simple regions and region sets

/// General region union

define sealed class <region-union> (<region-set>)
  sealed constant slot %regions :: <vector>,
    required-init-keyword: regions:;
end class <region-union>;

||#

(defclass <region-union> (<region-set>)
  ((%regions :type list :initarg :regions :initform (required-slot ":regions" "<region-union>") :reader %regions)))

#||
define sealed domain make (singleton(<region-union>));
define sealed domain initialize (<region-union>);

define inline function make-region-union
    (#rest regions) => (region :: <region-union>)
  make(<region-union>, regions: as(<simple-vector>, regions))
end function make-region-union;
||#

(defun make-region-union (&rest regions)
"
Constructs a <REGION-UNION> object from the <REGION> instances
passed as parameters.
"
  (make-instance '<region-union> :regions regions))


#||
define sealed method region-set-function
    (region :: <region-union>) => (function)
  union
end method region-set-function;
||#

(defmethod region-set-function ((region <region-union>))
  #'union)

#||
define sealed method region-set-regions
    (region :: <region-union>, #key normalize?) => (regions :: <vector>)
  ignore(normalize?);
  region.%regions
end method region-set-regions;
||#

(defmethod region-set-regions ((region <region-union>) &key normalize?)
  (declare (ignore normalize?))
  (%regions region))

#||
define sealed method transform-region
    (transform :: <transform>, region-set :: <region-union>)
 => (region :: <region-union>)
  let regions :: <stretchy-object-vector> = make(<stretchy-vector>);
  local method do-transform (region) => ()
	  add!(regions, transform-region(transform, region))
	end method;
  do-regions(do-transform, region-set);
  make(<region-union>, regions: regions)
end method transform-region;
||#

(defmethod transform-region ((transform <transform>) (region-set <region-union>))
  (let ((regions (list)))
    (flet ((do-transform (region)
	       (cons (transform-region transform region) regions)))
      (do-regions #'do-transform region-set)
      (make-instance '<region-union> :regions regions))))

#||
define method region-union
    (region :: <region>, nowhere :: <nowhere>) => (region :: <region>)
  region
end method region-union;
||#

(defmethod region-union ((region  <region>) (nowhere <nowhere>))
  region)

#||
define method region-union
    (nowhere :: <nowhere>, region :: <region>) => (region :: <region>)
  region
end method region-union;
||#

(defmethod region-union ((nowhere <nowhere>) (region <region>))
  region)

#||
define method region-union 
    (everywhere :: <everywhere>, region :: <region>) => (region :: <everywhere>)
  $everywhere
end method region-union;
||#

(defmethod region-union ((everywhere <everywhere>) (region <region>))
  *everywhere*)

#||
define method region-union
    (region :: <region>, everywhere :: <everywhere>) => (region :: <everywhere>)
  $everywhere
end method region-union;
||#

(defmethod region-union ((region <region>) (everywhere <everywhere>))
  *everywhere*)

#||
// Take the region of maximum dimensionality
define method region-union
    (point :: <point>, path :: <path>) => (region :: <path>)
  path
end method region-union;
||#

(defmethod region-union ((point <point>) (path <path>))
  path)

#||
define method region-union
    (path :: <path>, point :: <point>) => (region :: <path>)
  path
end method region-union;
||#

(defmethod region-union ((path <path>) (point <point>))
  path)

#||
define method region-union
    (point :: <point>, area :: <area>) => (region :: <area>)
  area
end method region-union;
||#

(defmethod region-union ((point <point>) (area <area>))
  area)

#||
define method region-union
    (area :: <area>, point :: <point>) => (region :: <area>)
  area
end method region-union;
||#

(defmethod region-union ((area <area>) (point <point>))
  area)

#||
define method region-union
    (path :: <path>, area :: <area>) => (region :: <area>)
  area
end method region-union;
||#

(defmethod region-union ((path <path>) (area <area>))
  area)

#||
define method region-union
    (area :: <area>, path :: <path>) => (region :: <area>)
  area
end method region-union;
||#

(defmethod region-union ((area <area>) (path <path>))
  area)

#||
define method region-union
    (point1 :: <point>, point2 :: <point>) => (region :: <region>)
  if (region-equal(point1, point2))
    point1
  else
    make-region-union(point1, point2)
  end
end method region-union;
||#

(defmethod region-union ((point1 <point>) (point2 <point>))
  (if (region-equal point1 point2)
      point1
      (make-region-union point1 point2)))

#||
define method region-union
    (path1 :: <path>, path2 :: <path>) => (region :: <region>)
  case
    region-contains-region?(path1, path2) => path1;
    region-contains-region?(path2, path1) => path2;
    otherwise => make-region-union(path1, path2)
  end
end method region-union;
||#

(defmethod region-union ((path1 <path>) (path2 <path>))
  (cond ((region-contains-region? path1 path2) path1)
        ((region-contains-region? path2 path1) path2)
        (t (make-region-union path1 path2))))

#||
define method region-union
    (area1 :: <area>, area2 :: <area>) => (region :: <region>)
  case
    region-contains-region?(area1, area2) => area1;
    region-contains-region?(area2, area1) => area2;
    otherwise => make-region-union(area1, area2)
  end
end method region-union;
||#

(defmethod region-union ((area1 <area>) (area2 <area>))
  (cond ((region-contains-region? area1 area2) area1)
        ((region-contains-region? area2 area1) area2)
        (t (make-region-union area1 area2))))

#||
define method region-union
    (region1 :: <region>, region2 :: <region>) => (region :: <region-union>)
  make-region-union(region1, region2)
end method region-union;
||#

(defmethod region-union ((region1 <region>) (region2 <region>))
  (make-region-union region1 region2))

#||
define method region-union
    (region :: <region>, union :: <region-union>) => (region :: <region-union>)
  apply(make-region-union, region, union.%regions)
end method region-union;
||#

(defmethod region-union ((region <region>) (union <region-union>))
  (apply #'make-region-union region (%regions union)))


#||
define method region-union
    (union :: <region-union>, region :: <region>) => (region :: <region-union>)
  apply(make-region-union, region, union.%regions)
end method region-union;
||#

(defmethod region-union ((union <region-union>) (region <region>))
  (apply #'make-region-union region (%regions union)))


#||
define method region-union
    (region1 :: <region-union>, region2 :: <region-union>)
 => (region :: <region-union>)
  apply(make-region-union, concatenate(region1.%regions, region2.%regions))
end method region-union;
||#

(defmethod region-union ((region1 <region-union>) (region2 <region-union>))
  (apply #'make-region-union (append (%regions region1) (%regions region2))))



#||

/// General region intersection

define sealed class <region-intersection> (<region-set>)
  sealed constant slot %regions :: <vector>,
    required-init-keyword: regions:;
end class <region-intersection>;
||#

(defclass <region-intersection> (<region-set>)
  ((%regions :type list :initarg :regions :initform (required-slot ":regions" "<region-intersection>") :reader %regions)))


#||
define sealed domain make (singleton(<region-intersection>));
define sealed domain initialize (<region-intersection>);

define inline function make-region-intersection
    (#rest regions) => (region :: <region-intersection>)
  make(<region-intersection>, regions: as(<simple-vector>, regions))
end function make-region-intersection;
||#

(defun make-region-intersection (&rest regions)
"
Constructs a <REGION-INTERSECTION> object from the
<REGION> instances passed as parameters.
"
  (make-instance '<region-intersection> :regions regions))


#||
define sealed method region-set-function
    (region :: <region-intersection>) => (function)
  intersection
end method region-set-function;
||#

(defmethod region-set-function ((region <region-intersection>))
  #'intersection)


#||
define sealed method region-set-regions
    (region :: <region-intersection>, #key normalize?) => (regions :: <vector>)
  ignore(normalize?);
  region.%regions
end method region-set-regions;
||#

(defmethod region-set-regions ((region <region-intersection>) &key normalize?)
  (declare (ignore normalize?))
  (%regions region))


#||
define sealed method transform-region
    (transform :: <transform>, region-set :: <region-intersection>)
 => (region :: <region-intersection>)
  let regions :: <stretchy-object-vector> = make(<stretchy-vector>);
  local method do-transform (region) => ()
	  add!(regions, transform-region(transform, region))
	end method;
  do-regions(do-transform, region-set);
  make(<region-intersection>, regions: regions)
end method transform-region;
||#

(defmethod transform-region ((transform <transform>) (region-set <region-intersection>))
  (let ((regions (list)))
    (flet ((do-transform (region)
	       (cons (transform-region transform region) regions)))
      (do-regions #'do-transform region-set)
      (make-instance '<region-intersection> :regions regions))))


#||
define method region-intersection
    (region :: <region>, nowhere :: <nowhere>) => (region :: <nowhere>)
  $nowhere
end method region-intersection;
||#

(defmethod region-intersection ((region <region>) (nowhere <nowhere>))
  *nowhere*)


#||
define method region-intersection
    (nowhere :: <nowhere>, region :: <region>) => (region :: <nowhere>)
  $nowhere
end method region-intersection;
||#

(defmethod region-intersection ((nowhere <nowhere>) (region <region>))
  *nowhere*)


#||
define method region-intersection
    (everywhere :: <everywhere>, region :: <region>) => (region :: <region>)
  region
end method region-intersection;
||#

(defmethod region-intersection ((everywhere <everywhere>) (region <region>))
  region)


#||
define method region-intersection
    (region :: <region>, everywhere :: <everywhere>) => (region :: <region>)
  region
end method region-intersection;
||#

(defmethod region-intersection ((region <region>) (everywhere <everywhere>))
  region)


#||
// Take the region of minumum dimensionality
define method region-intersection
    (point :: <point>, path :: <path>) => (region :: <region>)
  if (region-intersects-region?(point, path))
    point
  else
    $nowhere
  end
end method region-intersection;
||#

;; Take the region of minimum dimensionality
(defmethod region-intersection ((point <point>) (path  <path>))
  (if (region-intersects-region? point path)
      point
      *nowhere*))


#||
define method region-intersection
    (path :: <path>, point :: <point>) => (region :: <region>)
  if (region-intersects-region?(point, path))
    point
  else
    $nowhere
  end
end method region-intersection;
||#

(defmethod region-intersection ((path <path>) (point <point>))
  (if (region-intersects-region? point path)
      point
      *nowhere*))


#||
define method region-intersection
    (point :: <point>, area :: <area>) => (region :: <region>)
  if (region-intersects-region?(point, area))
    point
  else
    $nowhere
  end
end method region-intersection;
||#

(defmethod region-intersection ((point <point>) (area <area>))
  (if (region-intersects-region? point area)
      point
      *nowhere*))


#||
define method region-intersection
    (area :: <area>, point :: <point>) => (region :: <region>)
  if (region-intersects-region?(point, area))
    point
  else
    $nowhere
  end
end method region-intersection;
||#

(defmethod region-intersection ((area <area>) (point <point>))
  (if (region-intersects-region? point area)
      point
      *nowhere*))


#||
define method region-intersection
    (path :: <path>, area :: <area>) => (region :: <region>)
  if (region-intersects-region?(path, area))
    path
  else
    $nowhere
  end
end method region-intersection;
||#

(defmethod region-intersection ((path <path>) (area <area>))
  (if (region-intersects-region? path area)
      path
      *nowhere*))


#||
define method region-intersection
    (area :: <area>, path :: <path>) => (region :: <region>)
  if (region-intersects-region?(path, area))
    path
  else
    $nowhere
  end
end method region-intersection;
||#

(defmethod region-intersection ((area <area>) (path <path>))
  (if (region-intersects-region? path area)
      path
      *nowhere*))


#||
define method region-intersection
    (point1 :: <point>, point2 :: <point>) => (region :: <region>)
  if (region-equal(point1, point2))
    point1
  else
    $nowhere
  end
end method region-intersection;
||#

(defmethod region-intersection ((point1 <point>) (point2 <point>))
  (if (region-equal point1 point2)
      point1
      *nowhere*))


#||
// This catches paths and areas, too
define method region-intersection
    (region1 :: <region>, region2 :: <region>) => (region :: <region>)
  if (region-intersects-region?(region1, region2))
    make-region-intersection(region1, region2)
  else
    $nowhere
  end
end method region-intersection;
||#

;; This catches paths and areas, too
(defmethod region-intersection ((region1 <region>) (region2 <region>))
  (if (region-intersects-region? region1 region2)
      (make-region-intersection region1 region2)
      *nowhere*))


#||
define method region-intersection
    (region :: <region>, intersection :: <region-intersection>) => (region :: <region-intersection>)
  apply(make-region-intersection, region, intersection.%regions)
end method region-intersection;
||#

(defmethod region-intersection ((region <region>) (intersection <region-intersection>))
  (apply #'make-region-intersection region (%regions intersection)))


#||
define method region-intersection
    (intersection :: <region-intersection>, region :: <region>) => (region :: <region-intersection>)
  apply(make-region-intersection, region, intersection.%regions)
end method region-intersection;
||#

(defmethod region-intersection ((intersection <region-intersection>) (region <region>))
  (apply #'make-region-intersection region (%regions intersection)))


#||
define method region-intersection
    (region1 :: <region-intersection>, region2 :: <region-intersection>)
 => (region :: <region-intersection>)
  apply(make-region-intersection, concatenate(region1.%regions, region2.%regions))
end method region-intersection;
||#

(defmethod region-intersection ((region1 <region-intersection>) (region2 <region-intersection>))
  (apply #'make-region-intersection (append (%regions region1) (%regions region2))))



#||

/// General region difference

define sealed class <region-difference> (<region-set>)
  sealed constant slot %region1 :: <region>,
    required-init-keyword: region1:;
  sealed constant slot %region2 :: <region>,
    required-init-keyword: region2:;
  sealed slot %regions :: false-or(<vector>) = #f;
end class <region-difference>;
||#

(defclass <region-difference> (<region-set>)
  ((%region1 :type <region> :initarg :region1 :initform (required-slot ":region1" "<region-difference>") :reader %region1)
   (%region2 :type <region> :initarg :region2 :initform (required-slot ":region2" "<region-difference>") :reader %region2)
   (%regions :type (or null list) :initform nil :accessor %regions)))


#||
define sealed domain make (singleton(<region-difference>));
define sealed domain initialize (<region-difference>);

define inline function make-region-difference
    (region1, region2) => (region :: <region-difference>)
  make(<region-difference>, region1: region1, region2: region2)
end function make-region-difference;
||#

(defun make-region-difference (region1 region2)
"
Constructs a <REGION-DIFFERENCE> instance from the parameters
'region1' and 'region2', which must both be instances of
<REGION>.
"
  (make-instance '<region-difference> :region1 region1 :region2 region2))


#||
define sealed method region-set-function
    (region :: <region-difference>) => (function)
  difference
end method region-set-function;
||#

(defmethod region-set-function ((region <region-difference>))
  #'set-difference)


#||
define sealed method region-set-regions
    (region :: <region-difference>, #key normalize?) => (regions :: <vector>)
  ignore(normalize?);
  region.%regions
  | (region.%regions := vector(region.%region1, region.%region2))
end method region-set-regions;
||#

(defmethod region-set-regions ((region <region-difference>) &key normalize?)
  (declare (ignore normalize?))
  (or (%regions region)
      (setf (%regions region)
	    (list (%region1 region)
		  (%region2 region)))))


#||
define sealed method do-regions
    (function :: <function>, region :: <region-difference>, #key normalize?) => ()
  ignore(normalize?);
  function(region.%region1);
  function(region.%region2)
end method do-regions;
||#

(defmethod do-regions (function (region <region-difference>) &key normalize?)
  (declare (ignore normalize?))
  (funcall function (%region1 region))
  (funcall function (%region2 region)))


#||
define sealed method transform-region
    (transform :: <transform>, region-set :: <region-difference>)
 => (region :: <region-difference>)
  make-region-difference
    (transform-region(transform, region-set.%region1),
     transform-region(transform, region-set.%region2))
end method transform-region;
||#

(defmethod transform-region ((transform <transform>) (region-set <region-difference>))
  (make-region-difference (transform-region transform (%region1 region-set))
                          (transform-region transform (%region2 region-set))))


#||
define method region-difference
    (nowhere :: <nowhere>, region :: <region>) => (region :: <nowhere>)
  $nowhere
end method region-difference;
||#

(defmethod region-difference ((nowhere <nowhere>) (region <region>))
  *nowhere*)


#||
define method region-difference
    (region :: <region>, nowhere :: <nowhere>) => (region :: <region>)
  region
end method region-difference;
||#

(defmethod region-difference ((region <region>) (nowhere <nowhere>))
  region)


#||
define method region-difference
    (region :: <region>, everywhere :: <everywhere>) => (region :: <nowhere>)
  $nowhere
end method region-difference;
||#

(defmethod region-difference ((region <region>) (everywhere <everywhere>))
  *nowhere*)


#||
// For the case where the first region has higher dimensionality, the
// first region is the result.
define method region-difference
    (path :: <path>, point :: <point>) => (region :: <path>)
  path
end method region-difference;
||#

;; For the case where the first region has higher dimensionality, the
;; first region is the result.
(defmethod region-difference ((path <path>) (point <point>))
  path)


#||
define method region-difference
    (area :: <area>, point :: <point>) => (region :: <area>)
  area
end method region-difference;
||#


(defmethod region-difference ((area <area>) (point <point>))
  area)


#||
define method region-difference
    (area :: <area>, path :: <path>) => (region :: <area>)
  area
end method region-difference;
||#

(defmethod region-difference ((area <area>) (path <path>))
  area)


#||
define method region-difference
    (region1 :: <region>, region2 :: <region>) => (region :: <region-difference>)
  make-region-difference(region1, region2)
end method region-difference;
||#

(defmethod region-difference ((region1 <region>) (region2 <region>))
  (make-region-difference region1 region2))



#||

/// Simple box (LTRB) arithmetic
/// These operate only on <integer>'s, so be careful!

//--- Should we really allow zero-sized LTRBs?
define inline function ltrb-well-formed?
    (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
 => (true? :: <boolean>)
  right >= left & bottom >= top
end function ltrb-well-formed?;
||#

;;--- Should we really allow zero-sized LTRBs?
(defun ltrb-well-formed? (left top right bottom)
  (declare (type integer left top right bottom))
  (and (>= right left) (>= bottom top)))


#||
define inline function ltrb-equals-ltrb? 
    (left1 :: <integer>, top1 :: <integer>, right1 :: <integer>, bottom1 :: <integer>,
     left2 :: <integer>, top2 :: <integer>, right2 :: <integer>, bottom2 :: <integer>)
 => (true? :: <boolean>)
  left1 = left2 & top1 = top2
  & right1 = right2 & bottom1 = bottom2
end function ltrb-equals-ltrb?;
||#

(defun ltrb-equals-ltrb? (left1 top1 right1 bottom1
                          left2 top2 right2 bottom2)
  (and (= left1 left2)
       (= top1 top2)
       (= right1 right2)
       (= bottom1 bottom2)))


#||
define inline function ltrb-size-equal?
    (left1 :: <integer>, top1 :: <integer>, right1 :: <integer>, bottom1 :: <integer>,
     left2 :: <integer>, top2 :: <integer>, right2 :: <integer>, bottom2 :: <integer>)
 => (true? :: <boolean>)
  right1 - left1 = right2 - left2
  & bottom1 - top1 = bottom2 - top2
end function ltrb-size-equal?;
||#

(defun ltrb-size-equal? (left1 top1 right1 bottom1
                         left2 top2 right2 bottom2)
  (and (= (- right1 left1) (- right2 left2))
       (= (- bottom1 top1) (- bottom2 top2))))


#||
define inline function ltrb-contains-position?
    (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>,
     x :: <integer>, y :: <integer>)
 => (true? :: <boolean>)
  left <= x & top <= y
  & right >= x & bottom >= y
end function ltrb-contains-position?;
||#

(defun ltrb-contains-position? (left top right bottom x y)
  (and (<= left x)
       (<= top y)
       (>= right x)
       (>= bottom y)))


#||
// Returns #t iff LTRB1 wholly contains LTRB2
define inline function ltrb-contains-ltrb?
    (left1 :: <integer>, top1 :: <integer>, right1 :: <integer>, bottom1 :: <integer>,
     left2 :: <integer>, top2 :: <integer>, right2 :: <integer>, bottom2 :: <integer>)
 => (true? :: <boolean>)
  left1 <= left2 & top1 <= top2
  & right1 >= right2 & bottom1 >= bottom2
end function ltrb-contains-ltrb?;
||#

(defun ltrb-contains-ltrb? (left1 top1 right1 bottom1
                            left2 top2 right2 bottom2)
"
Returns T iff LTRB1 wholly contains LTRB2.
"
  (and (<= left1 left2)
       (<= top1 top2)
       (>= right1 right2)
       (>= bottom1 bottom2)))


#||
define function ltrb-intersects-ltrb? 
    (left1 :: <integer>, top1 :: <integer>, right1 :: <integer>, bottom1 :: <integer>,
     left2 :: <integer>, top2 :: <integer>, right2 :: <integer>, bottom2 :: <integer>)
 => (valid? :: <boolean>,
     left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
  let left = max(left1, left2);
  let top  = max(top1, top2);
  let right  = min(right1, right2);
  let bottom = min(bottom1, bottom2);
  if (ltrb-well-formed?(left, top, right, bottom))
    values(#t, left, top, right, bottom)
  else
    values(#f, 0, 0, 0, 0)
  end
end function ltrb-intersects-ltrb?;
||#

(defun ltrb-intersects-ltrb? (left1 top1 right1 bottom1
                              left2 top2 right2 bottom2)
"
Returns t and the ltrb of the intersection if the two ltrb's intersect, or
nil and four 0's otherwise.
"
  (let ((left   (max left1 left2))
        (top    (max top1 top2))
        (right  (min right1 right2))
        (bottom (min bottom1 bottom2)))
    (if (ltrb-well-formed? left top right bottom)
        (values t left top right bottom)
      (values nil 0 0 0 0))))


#||
// Returns a sequence of bounding boxes that represent the union
define sealed method ltrb-union
    (left1 :: <integer>, top1 :: <integer>, right1 :: <integer>, bottom1 :: <integer>,
     left2 :: <integer>, top2 :: <integer>, right2 :: <integer>, bottom2 :: <integer>,
     #key banding = #"x-banding")
 => (boxes :: <vector>)
  case
    ltrb-contains-ltrb?(left1, top1, right1, bottom1,
			left2, top2, right2, bottom2) =>
      vector(make-bounding-box(left1, top1, right1, bottom1));
    ltrb-contains-ltrb?(left2, top2, right2, bottom2,
			left1, top1, right1, bottom1) =>
      vector(make-bounding-box(left2, top2, right2, bottom2));
    ~ltrb-intersects-ltrb?(left1, top1, right1, bottom1,
			   left2, top2, right2, bottom2) =>
      vector(make-bounding-box(left1, top1, right1, bottom1),
	     make-bounding-box(left2, top2, right2, bottom2));
    otherwise =>
      select (banding)
	#"x-banding" =>
	  when (abs(left2) < abs(left1))
	    swap!(left1, left2);
	    swap!(top1,  top2);
	    swap!(right1,  right2);
	    swap!(bottom1, bottom2)
	  end;
	  let result :: <stretchy-object-vector> = make(<stretchy-vector>);
	  when (top1 < top2)
	    add!(result, make-bounding-box(left1, top1, right1, top2))
	  end;
	  when (bottom2 > bottom1)
	    add!(result, make-bounding-box(left2, bottom2, right2, bottom1))
	  end;
	  when (left1 < left2)
	    let top = max(top1, top2);
	    let bottom = min(bottom1, bottom2);
	    when (bottom > top)
	      add!(result, make-bounding-box(left1, top, right2, bottom))
	    end
	  end;
	  when (right1 > right2)
	    let top = min(bottom1, bottom2);
	    let bottom = max(top1, top2);
	    when (bottom > top)
	      add!(result, make-bounding-box(left2, top, right1, bottom))
	    end
	  end;
	  result;
	#"y-banding" =>
	  when (abs(top2) < abs(top1))
	    swap!(left1, left2);
	    swap!(top1,  top2);
	    swap!(right1,  right2);
	    swap!(bottom1, bottom2)
	  end;
	  let result :: <stretchy-object-vector> = make(<stretchy-vector>);
	  when (left1 < left2)
	    add!(result, make-bounding-box(left1, top1, left2, bottom1))
	  end;
	  when (right2 > right1)
	    add!(result, make-bounding-box(right1, top2, right2, bottom2))
	  end;
	  when (top1 < top2)
	    let left = max(left1, left2);
	    let right = min(right1, right2);
	    when (right > left)
	      add!(result, make-bounding-box(left, top1, right, bottom2))
	    end
	  end;
          when (bottom1 > bottom2)
	    let left = min(right1, right2);
	    let right = max(left1, left2);
	    when (right > left)
	      add!(result, make-bounding-box(left, top2, right, bottom1))
	    end
	  end;
	  result;
	#f =>
	  vector(make-bounding-box(left1, top1, right1, bottom1),
		 make-bounding-box(left2, top2, right2, bottom2));
      end
  end
end method ltrb-union;
||#

(defun ltrb-union (left1 top1 right1 bottom1
                   left2 top2 right2 bottom2
                   &key
                   (banding :x-banding))
"
Returns a region-set representing the unions of the two LTRBs
provided. If BANDING is provided the results are banded appropriately
(but share some points!). If no banding is performed (i.e. BANDING
is explicitly NIL) the result will be a region-union of both LTRBS
with no points excluded.

Unlike the original Dylan, this returns a LIST (rather than a VECTOR).
"
  (format t "~%box-arithmetic.ltrb-union - banding, FIXME")
  (cond ((ltrb-contains-ltrb? left1 top1 right1 bottom1
                              left2 top2 right2 bottom2)
         (list (make-bounding-box left1 top1 right1 bottom1)))
        ((ltrb-contains-ltrb? left2 top2 right2 bottom2
                              left1 top1 right1 bottom1)
         (list (make-bounding-box left2 top2 right2 bottom2)))
        ((not (ltrb-intersects-ltrb? left1 top1 right1 bottom1
                                     left2 top2 right2 bottom2))
         (list (make-bounding-box left1 top1 right1 bottom1)
               (make-bounding-box left2 top2 right2 bottom2)))
	(t
#||
           (format t "~%t case, check banding")
           (cond ((eq banding :x-banding)
                  (format t "~%:x-banding ~a" banding)
		  (when (< (abs left2) (abs left1))
		    (rotatef left1 left2)
		    (rotatef top1 top2)
		    (rotatef right1 right2)
		    (rotatef bottom1 bottom2))
		  (let ((result (list)))
		    (when (< top1 top2)
		      (setf result (cons (make-bounding-box left1 top1 right1 top2) result)))
		    (when (> bottom1 bottom2)
		      (setf result (cons (make-bounding-box left2 bottom2 right2 bottom1) result)))
		    (when (< left1 left2)
		      (let ((top (max top1 top2))
			    (bottom (min bottom1 bottom2)))
			(when (> bottom top)
			  (setf result (cons (make-bounding-box left1 top right2 bottom) result)))))
		    (when (> right1 right2)
		      (let ((top (min bottom1 bottom2))
			    (bottom (max top1 top2)))
			(when (> bottom top)
			  (setf result (cons (make-bounding-box left2 top right1 bottom) result)))))
		    result))
		 ((eq banding :y-banding)
                  (format t "~%:y-banding ~a" banding)
		  (when (< (abs top2) (abs top1))
		    (rotatef left1 left2)
		    (rotatef top1 top2)
		    (rotatef right1 right2)
		    (rotatef bottom1 bottom2))
		  (let ((result (list)))
		    (when (< left1 left2)
		      (setf result (cons (make-bounding-box left1 top1 left2 bottom1) result)))
		    (when (> right2 right1)
		      (setf result (cons (make-bounding-box right1 top2 right2 bottom2) result)))
		    (when (< top1 top2)
		      (let ((left (max left1 left2))
			    (right (min right1 right2)))
			(when (> right left)
			  (setf result (cons (make-bounding-box left top1 right bottom2) result)))))
		    (when (> bottom1 bottom2)
		      (let ((left (min right1 right2))
			    (right (max left1 left2)))
			(when (> right left)
			  (setf result (cons (make-bounding-box left top2 right bottom1) result)))))
		    result))
		 (t
||#
                  (list (make-bounding-box left1 top1 right1 bottom1)
		        (make-bounding-box left2 top2 right2 bottom2)))));;))


#||
// Returns a single bounding box that represents the intersection, or #f.
define sealed method ltrb-intersection
    (left1 :: <integer>, top1 :: <integer>, right1 :: <integer>, bottom1 :: <integer>,
     left2 :: <integer>, top2 :: <integer>, right2 :: <integer>, bottom2 :: <integer>)
 => (box :: false-or(<bounding-box>))
  let (valid?, left, top, right, bottom)
    = ltrb-intersects-ltrb? (left1, top1, right1, bottom1,
			     left2, top2, right2, bottom2);
  when (valid?)
    make-bounding-box(left, top, right, bottom)
  end
end method ltrb-intersection;
||#

(defun ltrb-intersection (left1 top1 right1 bottom1
                          left2 top2 right2 bottom2)
"
Returns a single bounding box representing those points that exist in
both the first and second ltrbs, or NIL if there is no intersection.
"
  (multiple-value-bind (valid? left top right bottom)
      (ltrb-intersects-ltrb? left1 top1 right1 bottom1
                             left2 top2 right2 bottom2)
    (when valid?
      (make-bounding-box left top right bottom))))


#||
// Returns a sequence of bounding boxes that represent the difference, or #f.
// Diagrams of box differences:
//
//     111111111111111111
//     1aaaaaaaaaaaaaaaa1
//     1aaaaaaaaaaaaaaaa1
//     1aaaaaaaaaaaaaaaa1
//     1aaaaaaaaaaaaaaaa1
//     1cccccc222222222232222222222
//     1cccccc2         1         2
//     1cccccc2         1         2
//     1cccccc2         1         2
//     111111131111111111         2
//            2                   2
//            2                   2
//            222222222222222222222
//
//
//     111111111111111111
//     1aaaaaaaaaaaaaaaa1
//     1aaaaaaaaaaaaaaaa1
//     1aaaaaaaaaaaaaaaa1
//     1aaaaaaaaaaaaaaaa1
// 2222322222222222222dd1
// 2   1             2dd1
// 2   1             2dd1
// 2   1             2dd1
// 2   1             2dd1
// 2   1             2dd1
// 2   1             2dd1
// 2222322222222222222dd1
//     1bbbbbbbbbbbbbbbb1
//     1bbbbbbbbbbbbbbbb1
//     111111111111111111
define sealed method ltrb-difference
    (left1 :: <integer>, top1 :: <integer>, right1 :: <integer>, bottom1 :: <integer>,
     left2 :: <integer>, top2 :: <integer>, right2 :: <integer>, bottom2 :: <integer>)
 => (box :: false-or(<vector>))
  // If the second ltrb contains the first ltrb, the difference is #f
  unless (ltrb-contains-ltrb?(left2, top2, right2, bottom2,
			      left1, top1, right1, bottom1))
    if (~ltrb-intersects-ltrb?(left1, top1, right1, bottom1,
			       left2, top2, right2, bottom2))
      vector(make-bounding-box(left1, top1, right1, bottom1))
    else
      let result :: <stretchy-object-vector> = make(<stretchy-vector>);
      when (top1 < top2)
	// Area A above
	add!(result, make-bounding-box(left1, top1, right1, top2))
      end;
      when (bottom1 > bottom2)
	// Area B above
	add!(result, make-bounding-box(left1, bottom2, right1, bottom1))
      end;
      when (left1 < left2)
	// Area C above
	let top = max(top1, top2);
	let bottom = min(bottom1, bottom2);
	when (bottom > top)
	  add!(result, make-bounding-box(left1, top, left2, bottom))
	end
      end;
      when (right1 > right2)
	// Area D above
	let top = max(top1, top2);
	let bottom = min(bottom1, bottom2);
	when (bottom > top)
	  add!(result, make-bounding-box(right2, top, right1, bottom))
	end
      end;
      if (empty?(result))
	#f
      else
	result
      end
    end
  end
end method ltrb-difference;
||#

(defun ltrb-difference (left1 top1 right1 bottom1
                        left2 top2 right2 bottom2)
"
Returns bounding boxes for those regions that exist in the first ltrb
 (specified by the first four arguments) that do not also exist in the
second ltrb (specified by the second four arguments).

Unlike the original Dylan method, returns a list rather than a vector.
"
  ;; If the second ltrb contains the first ltrb, the difference is #f
  (unless (ltrb-contains-ltrb? left2 top2 right2 bottom2
                               left1 top1 right1 bottom1)
    (if (not (ltrb-intersects-ltrb? left1 top1 right1 bottom1
                                    left2 top2 right2 bottom2))
        (list (make-bounding-box left1 top1 right1 bottom1))
      ;; else
      (let ((result (list)))
        (when (< top1 top2)
          ;; Area A above
          (setf result (cons (make-bounding-box left1 top1 right1 top2) result)))
        (when (> bottom1 bottom2)
          ;; Area B above
	  (setf result (cons (make-bounding-box left1 bottom2 right1 bottom1) result)))
        (when (< left1 left2)
          ;; Area C above
          (let ((top (max top1 top2))
                (bottom (min bottom1 bottom2)))
            (when (> bottom top)
	      (setf result (cons (make-bounding-box left1 top left2 bottom) result)))))
        (when (> right1 right2)
          ;; Area D above
          (let ((top (max top1 top2))
                (bottom (min bottom1 bottom2)))
            (when (> bottom top)
	      (setf result (cons (make-bounding-box right2 top right1 bottom) result)))))
        (if (empty? result)
            nil
	    result)))))



#||

/// Special cases for sets of boxes

define sealed class <box-set> (<region-set>)
  sealed constant slot %left :: <integer>,
    required-init-keyword: left:;
  sealed constant slot %top  :: <integer>,
    required-init-keyword: top:;
  sealed constant slot %right  :: <integer>,
    required-init-keyword: right:;
  sealed constant slot %bottom :: <integer>,
    required-init-keyword: bottom:;
  sealed slot box-set-boxes :: <vector>,
    required-init-keyword: boxes:;
  sealed slot %x-banded-boxes :: false-or(<vector>) = #f;
  sealed slot %y-banded-boxes :: false-or(<vector>) = #f;
end class <box-set>;
||#

(defclass <box-set> (<region-set>)
  ((%left :type integer :initarg :left :initform (required-slot ":left" "<box-set>") :reader %left)
   (%top :type integer :initarg :top :initform (required-slot ":top" "<box-set>") :reader %top)
   (%right :type integer :initarg :right :initform (required-slot ":right" "<box-set>") :reader %right)
   (%bottom :type integer :initarg :bottom :initform (required-slot ":bottom" "<box-set>") :reader %bottom)
   (box-set-boxes :type list :initarg :boxes :initform (required-slot ":boxes" "<box-set>") :accessor box-set-boxes)
   (%x-banded-boxes :type (or null list) :initform nil :accessor %x-banded-boxes)
   (%y-banded-boxes :type (or null list) :initform nil :accessor %y-banded-boxes)))


#||
define sealed domain make (singleton(<box-set>));
define sealed domain initialize (<box-set>);

define method make-box-set
    (#rest boxes) => (boxes :: <box-set>)
  let left :: <integer> = $largest-coordinate;
  let top  :: <integer> = $largest-coordinate;
  let right  :: <integer> = $smallest-coordinate;
  let bottom :: <integer> = $smallest-coordinate;
  for (box in boxes)
    let (rl, rt, rr, rb) = box-edges(box);
    min!(left, rl);
    min!(top, rt);
    max!(right, rr);
    max!(bottom, rb)
  end;
  make(<box-set>,
       boxes: as(<simple-vector>, boxes),
       left: left, top: top, right: right, bottom: bottom)
end method make-box-set;
||#

(defun make-box-set (&rest boxes)
  (let ((left   +largest-coordinate+)
        (top    +largest-coordinate+)
        (right  +smallest-coordinate+)
        (bottom +smallest-coordinate+))
    (dolist (box boxes)
      (multiple-value-bind (rl rt rr rb)
	  (box-edges box)
	(setf left (min left rl))
	(setf top (min top rt))
	(setf right (max right rr))
	(setf bottom (max bottom rb))))
    (make-instance '<box-set>
                   :boxes  boxes
                   :left   left
                   :top    top
                   :right  right
                   :bottom bottom)))


#||
define sealed inline method box-edges
    (box :: <box-set>)
 => (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
  values(box.%left, box.%top, box.%right, box.%bottom)
end method box-edges;
||#

(defmethod box-edges ((box <box-set>))
  (values (%left box) (%top box) (%right box) (%bottom box)))


#||
define sealed method transform-region
    (transform :: <transform>, set :: <box-set>) => (boxes :: <box-set>)
  local method do-transform (box) => ()
	  transform-region(transform, box)
	end method;
  apply(make-box-set, map-as(<simple-vector>, do-transform, box-set-boxes(set)))
end method transform-region;
||#

(defmethod transform-region ((transform <transform>) (set <box-set>))
  (flet ((do-transform (box)
	     (transform-region transform box)))
    (apply #'make-box-set
	   (map 'list #'do-transform (box-set-boxes set)))))


#||
define sealed method region-set-function (region :: <box-set>) => (function)
  union
end method region-set-function;
||#

(defmethod region-set-function ((region <box-set>))
  #'union)


#||
define sealed method region-set-regions
    (region :: <box-set>, #key normalize?) => (regions :: <vector>)
  select (normalize?)
    #f => box-set-boxes(region);
    #"x-banding" => region-set-x-banded-boxes(region);
    #"y-banding" => region-set-y-banded-boxes(region)
  end
end method region-set-regions;
||#

(defmethod region-set-regions ((region <box-set>) &key normalize?)
  (ecase normalize?
    ((nil)      (box-set-boxes region))
    (:x-banding (region-set-x-banded-boxes region))
    (:y-banding (region-set-y-banded-boxes region))))


#||
define sealed method region-set-x-banded-boxes
    (region :: <box-set>) => (regions :: <vector>)
  region.%x-banded-boxes
  | begin
      let boxes = normalize-box-set(region, #"x-banding");
      region.%x-banded-boxes := boxes;
      boxes
    end
end method region-set-x-banded-boxes;
||#

;; FIXME: This should probably be in the <box-set> protocol
(defgeneric region-set-x-banded-boxes (region))

(defmethod region-set-x-banded-boxes ((region <box-set>))
  (or (%x-banded-boxes region)
      (let ((boxes (normalize-box-set region :x-banding)))
        (setf (%x-banded-boxes region) boxes)
        boxes)))


#||
define sealed method region-set-y-banded-boxes
    (region :: <box-set>) => (regions :: <vector>)
  region.%y-banded-boxes
  | begin
      let boxes = normalize-box-set(region, #"y-banding");
      region.%y-banded-boxes := boxes;
      boxes
    end
end method region-set-y-banded-boxes;
||#

;; FIXME: This should probably be in the <box-set> protocol
(defgeneric region-set-y-banded-boxes (region))

(defmethod region-set-y-banded-boxes ((region <box-set>))
  (or (%y-banded-boxes region)
      (let ((boxes (normalize-box-set region :y-banding)))
        (setf (%y-banded-boxes region) boxes)
        boxes)))


#||
define sealed method region-union
    (box1 :: <bounding-box>, box2 :: <bounding-box>) => (region :: <region>)
  let (left1, top1, right1, bottom1) = box-edges(box1);
  let (left2, top2, right2, bottom2) = box-edges(box2);
  let new-boxes = ltrb-union(left1, top1, right1, bottom1,
			     left2, top2, right2, bottom2);
  if (size(new-boxes) = 1)
    new-boxes[0]
  else
    apply(make-box-set, new-boxes)
  end
end method region-union;
||#

(defmethod region-union ((box1 <bounding-box>) (box2 <bounding-box>))
  (multiple-value-bind (left1 top1 right1 bottom1)
      (box-edges box1)
    (multiple-value-bind (left2 top2 right2 bottom2)
        (box-edges box2)
      (let ((new-boxes (ltrb-union left1 top1 right1 bottom1
                                   left2 top2 right2 bottom2)))
        (if (= (length new-boxes) 1)
            (first new-boxes)
	    (apply #'make-box-set new-boxes))))))


#||
define sealed method region-union
    (box :: <bounding-box>, set :: <box-set>) => (region :: <box-set>)
  apply(make-box-set, box, box-set-boxes(set))
end method region-union;
||#

(defmethod region-union ((box <bounding-box>) (set <box-set>))
  (apply #'make-box-set box (box-set-boxes set)))


#||
define sealed method region-union 
    (set :: <box-set>, box :: <bounding-box>) => (region :: <box-set>)
  apply(make-box-set, box, box-set-boxes(set))
end method region-union;
||#

(defmethod region-union ((set <box-set>) (box <bounding-box>))
  (apply #'make-box-set box (box-set-boxes set)))


#||
define sealed method region-union
    (set1 :: <box-set>, set2 :: <box-set>) => (region :: <box-set>)
  apply(make-box-set, concatenate(box-set-boxes(set1), box-set-boxes(set2)))
end method region-union;
||#

(defmethod region-union ((set1 <box-set>) (set2 <box-set>))
  (apply #'make-box-set (append (box-set-boxes set1) (box-set-boxes set2))))


#||
define sealed method region-intersection
    (box1 :: <bounding-box>, box2 :: <bounding-box>) => (region :: <region>)
  let (left1, top1, right1, bottom1) = box-edges(box1);
  let (left2, top2, right2, bottom2) = box-edges(box2);
  let box = ltrb-intersection(left1, top1, right1, bottom1,
			      left2, top2, right2, bottom2);
  box | $nowhere
end method region-intersection;
||#

(defmethod region-intersection ((box1 <bounding-box>) (box2 <bounding-box>))
  (multiple-value-bind (left1 top1 right1 bottom1)
      (box-edges box1)
    (multiple-value-bind (left2 top2 right2 bottom2)
        (box-edges box2)
      (let ((box (ltrb-intersection left1 top1 right1 bottom1
                                    left2 top2 right2 bottom2)))
        (or box *nowhere*)))))


#||
define sealed method region-intersection
    (box :: <bounding-box>, set :: <box-set>) => (region :: <region>)
  let new-boxes :: <stretchy-object-vector> = make(<stretchy-vector>);
  let (left1, top1, right1, bottom1) = box-edges(box);
  local method do-intersection (b) => ()
	  let (left2, top2, right2, bottom2) = box-edges(b);
	  let new = ltrb-intersection(left1, top1, right1, bottom1,
				      left2, top2, right2, bottom2);
	  when (new)
	    add!(new-boxes, new)
	  end
	end method;
  do-regions(do-intersection, set);
  if (empty?(new-boxes))
    $nowhere
  else
    apply(make-box-set, new-boxes)
  end
end method region-intersection;
||#

(defmethod region-intersection ((box <bounding-box>) (set <box-set>))
  (let ((new-boxes (list)))
    (multiple-value-bind (left1 top1 right1 bottom1)
        (box-edges box)
      (flet ((do-intersection (b)
                 (multiple-value-bind (left2 top2 right2 bottom2)
                     (box-edges b)
                   (let ((new (ltrb-intersection left1 top1 right1 bottom1
                                                 left2 top2 right2 bottom2)))
                     (when new
		       (cons new new-boxes))))))
        (do-regions #'do-intersection set)))
    (if (empty? new-boxes)
	*nowhere*
	(apply #'make-box-set new-boxes))))


#||
define sealed method region-intersection
    (set :: <box-set>, box :: <bounding-box>) => (region :: <region>)
  let new-boxes :: <stretchy-object-vector> = make(<stretchy-vector>);
  let (left2, top2, right2, bottom2) = box-edges(box);
  local method do-intersection (b) => ()
	  let (left1, top1, right1, bottom1) = box-edges(b);
	  let new = ltrb-intersection(left1, top1, right1, bottom1,
				      left2, top2, right2, bottom2);
	  when (new)
	    add!(new-boxes, new)
	  end
	end method;
  do-regions(do-intersection, set);
  if (empty?(new-boxes))
    $nowhere
  else
    apply(make-box-set, new-boxes)
  end
end method region-intersection;
||#

(defmethod region-intersection ((set <box-set>) (box <bounding-box>))
  (let ((new-boxes (list)))
    (multiple-value-bind (left2 top2 right2 bottom2)
        (box-edges box)
      (flet ((do-intersection (b)
                 (multiple-value-bind (left1 top1 right1 bottom1)
                     (box-edges b)
                   (let ((new (ltrb-intersection left1 top1 right1 bottom1
                                                 left2 top2 right2 bottom2)))
                     (when new
                       (cons new new-boxes))))))
        (do-regions #'do-intersection set)))
    (if (empty? new-boxes)
	*nowhere*
	(apply #'make-box-set new-boxes))))


#||
define sealed method region-intersection
    (set1 :: <box-set>, set2 :: <box-set>) => (region :: <region>)
  let new-boxes :: <stretchy-object-vector> = make(<stretchy-vector>);
  do-regions
    (method (box1)
       do-regions
         (method (box2)
	    let (left1, top1, right1, bottom1) = box-edges(box1);
	    let (left2, top2, right2, bottom2) = box-edges(box2);
	    let new = ltrb-intersection(left1, top1, right1, bottom1,
					left2, top2, right2, bottom2);
	    when (new)
	      add!(new-boxes, new)
	    end
	  end,
	  set2)
     end,
     set1);
  if (empty?(new-boxes))
    $nowhere
  else
    apply(make-box-set, new-boxes)
  end
end method region-intersection;
||#

(defmethod region-intersection ((set1 <box-set>) (set2 <box-set>))
  (let ((new-boxes (list)))
    (do-regions
        #'(lambda (box1)
            (do-regions
                #'(lambda (box2)
                    (multiple-value-bind (left1 top1 right1 bottom1)
                        (box-edges box1)
                      (multiple-value-bind (left2 top2 right2 bottom2)
                          (box-edges box2)
                        (let ((new (ltrb-intersection left1 top1 right1 bottom1
                                                      left2 top2 right2 bottom2)))
                          (when new
			    (cons new new-boxes))))))
              set2))
      set1)
    (if (empty? new-boxes)
        *nowhere*
	(apply #'make-box-set new-boxes))))


#||
define sealed method region-difference
    (box1 :: <bounding-box>, box2 :: <bounding-box>) => (region :: <region>)
  let (left1, top1, right1, bottom1) = box-edges(box1);
  let (left2, top2, right2, bottom2) = box-edges(box2);
  let new-boxes = ltrb-difference(left1, top1, right1, bottom1,
				  left2, top2, right2, bottom2);
  if (empty?(new-boxes))
    $nowhere
  else
    if (size(new-boxes) = 1)
      new-boxes[0]
    else
      apply(make-box-set, new-boxes)
    end
  end
end method region-difference;
||#

(defmethod region-difference ((box1 <bounding-box>)
                              (box2 <bounding-box>))
  (multiple-value-bind (left1 top1 right1 bottom1)
      (box-edges box1)
    (multiple-value-bind (left2 top2 right2 bottom2)
        (box-edges box2)
      (let ((new-boxes (ltrb-difference left1 top1 right1 bottom1
                                        left2 top2 right2 bottom2)))
        (cond ((empty? new-boxes)       *nowhere*)
              ((= (length new-boxes) 1) (car new-boxes))
              (t
               (apply #'make-box-set new-boxes)))))))


#||
define sealed method region-difference
    (box :: <bounding-box>, set :: <box-set>) => (region :: <region>)
  let new-boxes :: <stretchy-object-vector> = make(<stretchy-vector>);
  let (left1, top1, right1, bottom1) = box-edges(box);
  local method do-difference (b) => ()
	  let (left2, top2, right2, bottom2) = box-edges(b);
	  let new = ltrb-difference(left1, top1, right1, bottom1,
				    left2, top2, right2, bottom2);
	  when (new)
	    add!(new-boxes, new)
	  end
	end method;
  do-regions(do-difference, set);
  if (new-boxes)
    apply(make-box-set, new-boxes)
  else
    $nowhere
  end
end method region-difference;
||#

(defmethod region-difference ((box <bounding-box>) (set <box-set>))
  (let ((new-boxes (list)))
    (multiple-value-bind (left1 top1 right1 bottom1)
        (box-edges box)
      (flet ((do-difference (b)
                 (multiple-value-bind (left2 top2 right2 bottom2)
                     (box-edges b)
                   (let ((new (ltrb-difference left1 top1 right1 bottom1
                                               left2 top2 right2 bottom2)))
                     (when new
		       (cons new new-boxes))))))
        (do-regions #'do-difference set)
        (if new-boxes
	    (apply #'make-box-set new-boxes))
	    *nowhere*))))


#||
define sealed method region-difference
    (set :: <box-set>, box :: <bounding-box>) => (region :: <region>)
  let new-boxes :: <stretchy-object-vector> = make(<stretchy-vector>);
  let (left2, top2, right2, bottom2) = box-edges(box);
  local method do-difference (b) => ()
	  let (left1, top1, right1, bottom1) = box-edges(b);
	  let new = ltrb-difference(left1, top1, right1, bottom1,
				    left2, top2, right2, bottom2);
	  when (new)
	    add!(new-boxes, new)
	  end
	end method;
    do-regions(do-difference, set);
  if (empty?(new-boxes))
    $nowhere
  else
    apply(make-box-set, new-boxes)
  end
end method region-difference;
||#

(defmethod region-difference ((set <box-set>) (box <bounding-box>))
  (let ((new-boxes (list)))
    (multiple-value-bind (left2 top2 right2 bottom2)
        (box-edges box)
      (flet ((do-difference (b)
                 (multiple-value-bind (left1 top1 right1 bottom1)
                     (box-edges b)
                   (let ((new (ltrb-difference left1 top1 right1 bottom1
                                               left2 top2 right2 bottom2)))
                     (when new
                       (cons new new-boxes))))))
        (do-regions #'do-difference set)
        (if (empty? new-boxes)
            *nowhere*
	    (apply #'make-box-set new-boxes))))))


#||
define sealed method region-difference
    (set1 :: <box-set>, set2 :: <box-set>) => (region :: <region>)
  let new-boxes :: <stretchy-object-vector> = make(<stretchy-vector>);
  do-regions
    (method (box1)
       do-regions
         (method (box2)
	    let (left1, top1, right1, bottom1) = box-edges(box1);
	    let (left2, top2, right2, bottom2) = box-edges(box2);
	    let new = ltrb-difference(left1, top1, right1, bottom1,
				      left2, top2, right2, bottom2);
	    when (new)
	      add!(new-boxes, new)
	    end
	  end,
	  set2)
     end,
     set1);
  if (empty?(new-boxes))
    $nowhere
  else
    apply(make-box-set, new-boxes)
  end
end method region-difference;
||#

(defmethod region-difference ((set1 <box-set>) (set2 <box-set>))
  (let ((new-boxes (list)))
    (do-regions
       #'(lambda (box1)
	   (do-regions
	      #'(lambda (box2)
		  (multiple-value-bind (left1 top1 right1 bottom1)
		      (box-edges box1)
		    (multiple-value-bind (left2 top2 right2 bottom2)
			(box-edges box2)
		      (let ((new (ltrb-difference left1 top1 right1 bottom1
						  left2 top2 right2 bottom2)))
			(when new
			  (cons new new-boxes))))))
	      set2))
       set1)
    (if (empty? new-boxes)
        *nowhere*
	(apply #'make-box-set new-boxes))))


#||
define sealed method region-empty?
    (set :: <box-set>) => (true? :: <boolean>)
  every?(region-empty?, box-set-boxes(set))
end method region-empty?;
||#

(defmethod region-empty? ((set <box-set>))
  (every #'region-empty? (box-set-boxes set)))


#||
define sealed method normalize-box-set
    (set :: <box-set>, banding) => (boxes :: <vector>)
  local method collect-boxes (region) => (boxes)
          select (region by instance?)
            <box-set> =>
              apply(concatenate-as, <list>, map(collect-boxes, box-set-boxes(region)));
	    <bounding-box> => list(region);
	    <everywhere> => list(region);
          end
        end method,
        method reduce-boxes (pending-boxes, processed-boxes) => (boxes)
          case
            empty?(pending-boxes) =>
              processed-boxes;
            region-empty?(head(pending-boxes)) =>
              reduce-boxes(tail(pending-boxes), processed-boxes);
            otherwise =>
              let intersecting-region
                = begin
		    local method intersects? (box) => (intersects :: <boolean>)
			    region-intersects-region?(box, head(pending-boxes))
			  end method;
                    find-value(tail(pending-boxes), intersects?)
                  end;
              if (empty?(intersecting-region))
                reduce-boxes
		  (tail(pending-boxes), pair(head(pending-boxes), processed-boxes))
              else
                reduce-boxes
                  (concatenate!
                     (reduce-box-pair(head(pending-boxes), intersecting-region),
		      remove!(tail(pending-boxes), intersecting-region)),
                   processed-boxes)
	      end;
          end
        end method,
        method reduce-box-pair (box1, box2) => (boxes)
          // Don't use 'region-union', because we are only prepared
          // to deal with bounding boxes
	  let (left1, top1, right1, bottom1) = box-edges(box1);
	  let (left2, top2, right2, bottom2) = box-edges(box2);
	  remove!(ltrb-union(left1, top1, right1, bottom1,
			     left2, top2, right2, bottom2,
			     banding: banding),
		  #f,
		  test: method (_x, _y) ignore(_y); region-empty?(_x) end)
	end method;
  as(<simple-vector>, reduce-boxes(collect-boxes(set), #()))
end method normalize-box-set;
||#

;; FIXME: This should probably be in the <box-set> protocol
(defgeneric normalize-box-set (set banding))

(defmethod normalize-box-set ((set <box-set>) banding)
  ;; Recursively breaks down a <box-set> into its constituent regions, returning
  ;; a list of those regions.
  (labels ((collect-boxes (region)
               (etypecase region
                 (<box-set>
                  (loop for box in (box-set-boxes region)
                        appending (collect-boxes box)))
                 (<bounding-box>
                  (list region))
                 (<everywhere>
                  (list region))))
           ;; pending-boxes and processed-boxes must both be lists.
           (reduce-boxes (pending-boxes processed-boxes)
	       (cond ((empty? pending-boxes) (return-from reduce-boxes processed-boxes))
		     ((region-empty? (car pending-boxes)) (reduce-boxes (cdr pending-boxes) processed-boxes))
		     (t
		      ;; do any of the rest of the pending boxes intersect with the first
		      ;; of the pending boxes?
		      (let ((intersecting-region (labels ((intersects? (box)
							    (region-intersects-region? box (car pending-boxes))))
						   (find-if #'intersects? (cdr pending-boxes)))))
			(if (empty? intersecting-region)
			    (reduce-boxes (cdr pending-boxes) (cons (car pending-boxes) processed-boxes))
			    (reduce-boxes (append (reduce-box-pair (car pending-boxes) intersecting-region)
						  (remove-if (alexandria:curry #'eql intersecting-region) (cdr pending-boxes)))
					  processed-boxes))))))
	   (reduce-box-pair (box1 box2)
	     ;; Don't use 'region-union', because we are only prepared
	     ;; to deal with bounding boxes
	     (multiple-value-bind (left1 top1 right1 bottom1)
		 (box-edges box1)
	       (multiple-value-bind (left2 top2 right2 bottom2)
		   (box-edges box2)
		 (remove-if #'(lambda (_x _y) (declare (ignore _y)) (region-empty? _x))
			    (ltrb-union left1 top1 right1 bottom1
					left2 top2 right2 bottom2
					:banding banding))))))
    (reduce-boxes (collect-boxes set) ())))

