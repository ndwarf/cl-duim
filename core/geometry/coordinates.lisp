;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GEOMETRY-INTERNALS -*-
(in-package #:duim-geometry-internals)

#||
/// Coordinate hacking

define constant $largest-coordinate  :: <integer> = $maximum-integer;
define constant $smallest-coordinate :: <integer> = $minimum-integer;
||#

(defconstant +largest-coordinate+  most-positive-fixnum
"
The largest valid coordinate.
")

(defconstant +smallest-coordinate+ most-negative-fixnum
"
The smallest valid coordinate. Coordinates must be instances of type
<integer>.
")

#||
// Coerce a coordinate to an integer
define inline function fix-coordinate
    (x :: <real>) => (x :: <integer>)
  truncate(x)
end function fix-coordinate;
||#

(defun fix-coordinate (x)
"
Coerces the given coordinate into an <integer>.
"
  (truncate x))

#||
define inline function fix-box
    (left :: <real>, top :: <real>, right :: <real>, bottom :: <real>)
 => (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
  values(floor(left), floor(top), ceiling(right), ceiling(bottom))
end function fix-box;
||#

(defun fix-box (left top right bottom)
  (values (floor left)
	  (floor top)
	  (ceiling right)
	  (ceiling bottom)))


#||
/// Transforming positions and distances

// Translate the coordinate pairs by dx/dy
// translate-coordinates!(dx, dy, x1, y1, x2, y2, ...);
define macro translate-coordinates!
  { translate-coordinates! (?dx:expression, ?dy:expression) }
    => { }
  { translate-coordinates!
      (?dx:expression, ?dy:expression, ?x:expression, ?y:expression, ?more:*) }
    => { ?x := ?x + ?dx;
         ?y := ?y + ?dy;
         translate-coordinates!(?dx, ?dy, ?more); }
end macro translate-coordinates!;
||#

;;; TODO: This should be a "modify macro"

(defmacro translate-coordinates! (dx dy &rest coordinates)
  (let ((_dx (gensym "DX-"))
        (_dy (gensym "DY-")))
  `(let ((,_dx ,dx)
         (,_dy ,dy))
     ,@(loop for i from 0 to (- (length coordinates) 2) by 2
             collecting `(setf ,(nth i coordinates)
                               (+ ,(nth i coordinates) ,_dx)
                               ,(nth (1+ i) coordinates)
                               (+ ,(nth (1+ i) coordinates) ,_dy))))))

#||
define method translate-coordinate-sequence!
    (dx, dy, coords :: <sequence>) => (coords :: <sequence>)
  unless (zero?(dx) & zero?(dy))
    let ncoords :: <integer> = size(coords);
    without-bounds-checks
      for (i :: <integer> from 0 below ncoords by 2)
	coords[i + 0] := coords[i + 0] + dx;
	coords[i + 1] := coords[i + 1] + dy
      end
    end
  end;
  coords
end method translate-coordinate-sequence!;
||#

(defgeneric translate-coordinate-sequence! (dx dy coords))

(defmethod translate-coordinate-sequence! (dx dy (coords array))
  (unless (and (zerop dx) (zerop dy))
    (let ((ncoords (length coords)))
      (do ((i 0 (+ i 2)))
	  ((= i ncoords))
	(setf (aref coords (+ i 0)) (+ (aref coords (+ i 0)) dx))
	(setf (aref coords (+ i 1)) (+ (aref coords (+ i 1)) dy)))))
  coords)

#||
define method translate-coordinate-sequence!
    (dx, dy, coords :: <simple-object-vector>) => (coords :: <simple-object-vector>)
  unless (zero?(dx) & zero?(dy))
    let ncoords :: <integer> = size(coords);
    without-bounds-checks
      for (i :: <integer> from 0 below ncoords by 2)
	coords[i + 0] := coords[i + 0] + dx;
	coords[i + 1] := coords[i + 1] + dy
      end
    end
  end;
  coords
end method translate-coordinate-sequence!;
||#

(defmethod translate-coordinate-sequence! (dx dy
					   (coords vector))
  (unless (and (zerop dx) (zerop dy))
    (let ((ncoords (size coords)))
      (loop for i from 0 below ncoords by 2
	    do (setf (aref coords (+ i 0)) (+ (aref coords (+ i 0)) dx)
		     (aref coords (+ i 1)) (+ (aref coords (+ i 1)) dy)))))
  coords)

#||
define method translate-coordinate-sequence!
    (dx, dy, coords :: <list>) => (coords :: <list>)
  unless (zero?(dx) & zero?(dy))
    let new-coords = coords;
    while (~empty?(new-coords))
      head(new-coords) := head(new-coords) + dx;
      pop!(new-coords);
      head(new-coords) := head(new-coords) + dy;
      pop!(new-coords)
    end
  end;
  coords
end method translate-coordinate-sequence!;
||#

(defmethod translate-coordinate-sequence! (dx dy
					   (coords list))
  (unless (and (zerop dx) (zerop dy))
    (let ((new-coords coords))
      (loop while (not (empty? new-coords))
	    do (setf (car new-coords) (+ (car new-coords) dx))
            do (pop new-coords)
            do (setf (car new-coords) (+ (car new-coords) dy))
            do (pop new-coords))))
  coords)

#||
// Apply a general transformation to the coordinate pairs
// transform-coordinates!(transform, x1, y1, x2, y2, ...);
define macro transform-coordinates!
  { transform-coordinates! (?transform:expression) }
    => { }
  { transform-coordinates!
      (?transform:variable, ?x:expression, ?y:expression, ?more:*) }
    => { let (_x, _y) = transform-position(?transform, ?x, ?y);
         ?x := _x;
         ?y := _y;
         transform-coordinates!(?transform, ?more); }
  { transform-coordinates!
      (?transform:expression, ?x:expression, ?y:expression, ?more:*) }
    => { let _transform = ?transform;
         let (_x, _y) = transform-position(_transform, ?x, ?y);
         ?x := _x;
         ?y := _y;
         transform-coordinates!(_transform, ?more); }
end macro transform-coordinates!;
||#

;;; (transform-coordinates! transform x1 y1 x2 y2 x3 y3)
;;; =>
;;; (multiple-value-bind (xp yp)
;;;     (transform-position transform x1 y1)
;;;   (setf x1 xp y1 yp))
;;; (multiple-value-bind (xp yp)
;;;     (transform-position transform x2 y2)
;;;   (setf x2 xp y2 yp))
;;; ...

(defmacro transform-coordinates! (transform &rest coordinates)
  (let ((_x (gensym))
	(_y (gensym))
	(_t (gensym)))
    `(progn
       (let ((,_t ,transform))
	 ,@(loop for index from 0 below (length coordinates) by 2
		 collecting (let ((coord1 (elt coordinates index))
				  (coord2 (elt coordinates (1+ index))))
			      `(multiple-value-bind (,_x ,_y)
				   (transform-position ,_t ,coord1 ,coord2)
				 (setf ,coord1 ,_x ,coord2 ,_y))))))))


#||
// Apply a general transformation to the dx/dy pairs
// transform-distances!(transform, dx1, dy1, dx2, dy2, ...);
define macro transform-distances!
  { transform-distances! (?transform:expression) }
    => { }
  { transform-distances!
      (?transform:variable, ?dx:expression, ?dy:expression, ?more:*) }
    => { let (_dx, _dy) = transform-distance(?transform, ?dx, ?dy);
         ?dx := _dx;
         ?dy := _dy;
         transform-distances!(?transform, ?more); }
  { transform-distances!
      (?transform:expression, ?dx:expression, ?dy:expression, ?more:*) }
    => { let _transform = ?transform;
         let (_dx, _dy) = transform-distance(_transform, ?dx, ?dy);
         ?dx := _dx;
         ?dy := _dy;
         transform-distances!(_transform, ?more); }
end macro transform-distances!;
||#

(defmacro transform-distances! (transform &rest more)
  (let ((_dx (gensym "DX-"))
        (_dy (gensym "DY-"))
        (trans (gensym "TRANSFORM-")))
    `(let ((,trans ,transform))
       ,@(loop for i from 0 to (- (length more) 2) by 2
               collecting `(multiple-value-bind (,_dx ,_dy)
                               (transform-distance ,trans
                                                   ,(nth i more)
                                                   ,(nth (1+ i) more))
                             (setf ,(nth i more) ,_dx
                                   ,(nth (1+ i) more) ,_dy))))))


#||
/// Fixing coordinates and distances

// fix-coordinates!(x1, y1, x2, y2, ...);
// Side-effects x1, y1, ...
define macro fix-coordinates!
  { fix-coordinates! (?x:expression, ?y:expression) }
    => { ?x := floor(?x);
         ?y := floor(?y); }
  { fix-coordinates! (?x:expression, ?y:expression, ?more:*) }
    => { ?x := floor(?x);
         ?y := floor(?y);
         fix-coordinates!(?more); }
end macro fix-coordinates!;
||#

(defmacro fix-coordinates! (&rest coordinates)
  `(progn
    ,@(loop for coord in coordinates
            collect `(setf ,coord (floor ,coord)))))


#||
/// Device transformations

// Use this in preference to 'convert-to-device-coordinates!', since it
// the coordinates to device coordinates having tighter type declarations
// with-device-coordinates (transform, x1, y1, x2, y2, ...) body end;
define macro with-device-coordinates
  { with-device-coordinates (?transform:expression)
      ?:body
    end}
    => { ?body }
  { with-device-coordinates (?transform:expression, ?x:name, ?y:name, ?more:*)
      ?:body
    end }
    => { begin
	   let (?x :: <integer>, ?y :: <integer>)
	     = if (?transform == $identity-transform)	// for speed...
		 values(floor(?x), floor(?y))
	       else
		 let (_x, _y) = transform-position(?transform, ?x, ?y);
		 values(floor(_x), floor(_y))
	       end;
	   with-device-coordinates (?transform, ?more)
	     ?body
	   end 
	 end }
end macro with-device-coordinates;
||#

;;; MOVED TO WITH-DEVICE-COORDS.LISP
;;; TODO: Move this back here and use an eval-when on it?

#-(and)
(defmacro with-device-coordinates ((transform &rest coords) &body body)
  ;; coords must be non-empty and have an even number of elements.
  ;; :fixme: this is just wrong...
  (let ((x (pop coords))
        (y (pop coords)))
    (if (null coords)
        `(multiple-value-bind (,x ,y)
             ,(if (eq transform $identity-transform)
                  `(values (floor ,x) (floor ,y))
                  `(transform-position ,transform ,x ,y))
           ,@body)
        `(multiple-value-bind (,x ,y)
             ,(if (eq transform $identity-transform)
                  `(values (floor ,x) (floor ,y))
                  `(transform-position ,transform ,x ,y))
           (with-device-coordinates (,transform ,@coords)
             ,@body)))))
        

#||
// with-device-distances (transform, dx1, dy1, dx2, dy2, ...) body end;
define macro with-device-distances
  { with-device-distances (?transform:expression)
      ?:body
    end}
    => { ?body }
  { with-device-distances (?transform:expression, ?dx:name, ?dy:name, ?more:*)
      ?:body
    end }
    => { begin
	   let (?dx :: <integer>, ?dy :: <integer>)
	     = if (?transform == $identity-transform)	// for speed...
		 values(floor(?dx), floor(?dy))
	       else
		 let (_dx, _dy) = transform-distance(?transform, ?dx, ?dy);
		 values(floor(_dx), floor(_dy))
	       end;
	   with-device-distances (?transform, ?more)
	     ?body
	   end
	 end }
end macro with-device-distances;
||#

;;; MOVED TO WITH-DEVICE-COORDS.LISP
;;; TODO: Use eval-when instead?

#-(and)
(defmacro with-device-distances ((transform &rest distances) &body body)
  (let ((dx (pop distances))
        (dy (pop distances)))
    (if (null distances)
        `(multiple-value-bind (,dx ,dy)
             ,(if (eq transform $identity-transform)
                  `(values (floor ,dx) (floor ,dy))
                  `(transform-distance ,transform ,dx ,dy))
          ,@body)
        `(multiple-value-bind (,dx ,dy)
             ,(if (eq transform $identity-transform)
                  `(values (floor ,dx) (floor ,dy))
                  `(transform-distance ,transform ,dx ,dy))
          (with-device-distances (,transform ,@distances)
            ,@body)))))

#||
// convert-to-device-coordinates!(transform, x1, y1, x2, y2, ...);
define macro convert-to-device-coordinates!
  { convert-to-device-coordinates! (?transform:expression) }
    => { }
  { convert-to-device-coordinates!
      (?transform:expression, ?x:expression, ?y:expression, ?more:*) }
    => { let (_x, _y) = transform-position(?transform, ?x, ?y);
	 ?x := floor(_x);
         ?y := floor(_y);
         convert-to-device-coordinates!(?transform, ?more); }
end macro convert-to-device-coordinates!;
||#

(defmacro convert-to-device-coordinates! (transform &rest coords)
  (let ((x (pop coords))
        (y (pop coords)))
  `(progn
     (multiple-value-bind (_x _y)
	 (transform-position ,transform ,x ,y)
       (setf ,x _x ,y _y))
     ,(unless (null coords)
       `(convert-to-device-coordinates! ,transform ,@coords)))))

#||
// convert-to-device-distances!(transform, dx1, dy1, dx2, dy2, ...);
define macro convert-to-device-distances!
  { convert-to-device-distances! (?transform:expression) }
    => { }
  { convert-to-device-distances!
      (?transform:expression, ?dx:expression, ?dy:expression, ?more:*) }
    => { let (_dx, _dy) = transform-distance(?transform, ?dx, ?dy);
	 ?dx := floor(_dx);
         ?dy := floor(_dy);
         convert-to-device-distances!(?transform, ?more); }
end macro convert-to-device-distances!;
||#

(defmacro convert-to-device-distances! (transform dx dy &rest distances)
  `(progn
     (multiple-value-bind (_dx _dy)
	 (transform-distance ,transform ,dx ,dy)
       (setf ,dx _dx ,dy _dy))
     ,(unless (null distances)
       `(convert-to-device-distances! ,transform ,@distances))))


#||
/// Mapping over coordinate sequences

define method do-coordinates
    (function :: <function>, coordinates :: <sequence>) => ()
  dynamic-extent(function);
  let ncoords :: <integer> = size(coordinates);
  without-bounds-checks
    for (i :: <integer> = 0 then i + 2,
	 until: i >= ncoords)
      function(coordinates[i], coordinates[i + 1])
    end
  end
end method do-coordinates;
||#

(defgeneric do-coordinates (function coordinates)
  (:documentation
"
Applies _function_ to each coordinate pair in _coordinates_. The
length of _coordinates_ must be a multiple of 2. _function_ takes two
arguments, the x and y value of each coordinate pair.
"))

;;; geometry tests fail with this method defined; presumably, for whatever
;;; reason, CLISP has a problem dispatching on #(1 2 3 4) when there are
;;; methods for (sequence), (vector) and (list). Maybe (vector) should
;;; be (array)?

;;; XXX: I'm replacing this method; if CLISP is ever revisited as a target
;;; platform, it'll need looking at again I guess.

(defmethod do-coordinates ((function function)
			   (coordinates sequence))
  (declare (dynamic-extent function))
  (let ((ncoords (length coordinates)))
    (loop for i from 0 by 2
	  until (>= i ncoords)
	  do (funcall function
		      (elt coordinates i)
		      (elt coordinates (1+ i))))))

#||
define method do-coordinates
    (function :: <function>, coordinates :: <simple-object-vector>) => ()
  dynamic-extent(function);
  let ncoords :: <integer> = size(coordinates);
  without-bounds-checks
    for (i :: <integer> = 0 then i + 2,
	 until: i >= ncoords)
      function(coordinates[i], coordinates[i + 1])
    end
  end
end method do-coordinates;
||#

(defmethod do-coordinates ((function function)
			   (coordinates vector))
  (declare (dynamic-extent function))
  (let ((ncoords (size coordinates)))
    (loop for i from 0 by 2
	  until (>= i ncoords)
	  do (funcall function
		      (aref coordinates i)
		      (aref coordinates (1+ i))))))

#||
define method do-coordinates
    (function :: <function>, coordinates :: <list>) => ()
  dynamic-extent(function);
  until (empty?(coordinates))
    let x = pop!(coordinates);
    let y = pop!(coordinates);
    function(x, y)
  end
end method do-coordinates;
||#

(defmethod do-coordinates ((function function)
			   (coordinates list))
  (declare (dynamic-extent function))
  (loop until (empty? coordinates)
	do (let ((x (pop coordinates))
		 (y (pop coordinates)))
	     (funcall function x y))))

#||
define method do-endpoint-coordinates
    (function :: <function>, coordinates :: <vector>) => ()
  dynamic-extent(function);
  let ncoords :: <integer> = size(coordinates);
  without-bounds-checks
    for (i :: <integer> = 0 then i + 4,
	 until: i >= ncoords)
      function(coordinates[i],     coordinates[i + 1],
	       coordinates[i + 2], coordinates[i + 3])
    end
  end
end method do-endpoint-coordinates;
||#

(defgeneric do-endpoint-coordinates (function coordinates)
  (:documentation
"
Applies _function_ to each pair of coordinate pairs in
_coordinates_. The arguments _coordinates_ represents a set of line
segments rather than a set of points: the length of this sequence must
therefore be a multiple of 4. _function_ takes 4 arguments, (x1, y1,
x2, y2).
"))

#-(and)
(defmethod do-endpoint-coordinates ((function function)
				    (coordinates array))
  (declare (dynamic-extent function))
  (let ((limit (length coordinates)))
    (loop for i from 0 below limit by 4
	  do (funcall function
		      (aref i coordinates)
		      (aref (1+ i) coordinates)
		      (aref (+ 2 i) coordinates)
		      (aref (+ 3 i) coordinates)))))

#||
define method do-endpoint-coordinates
    (function :: <function>, coordinates :: <simple-object-vector>) => ()
  dynamic-extent(function);
  let ncoords :: <integer> = size(coordinates);
  without-bounds-checks
    for (i :: <integer> = 0 then i + 4,
	 until: i >= ncoords)
      function(coordinates[i],     coordinates[i + 1],
	       coordinates[i + 2], coordinates[i + 3])
    end
  end
end method do-endpoint-coordinates;
||#

(defmethod do-endpoint-coordinates ((function function)
				    (coordinates vector))
  (declare (dynamic-extent function))
  (let ((limit (length coordinates)))
    (loop for i from 0 below limit by 4
	  do (funcall function
		      (svref coordinates i)
		      (svref coordinates (1+ i))
		      (svref coordinates (+ 2 i))
		      (svref coordinates (+ 3 i))))))

#||
define method do-endpoint-coordinates
    (function :: <function>, coordinates :: <list>) => ()
  dynamic-extent(function);
  until (empty?(coordinates))
    let x1 = pop!(coordinates);
    let y1 = pop!(coordinates);
    let x2 = pop!(coordinates);
    let y2 = pop!(coordinates);
    function(x1, y1, x2, y2)
  end
end method do-endpoint-coordinates;
||#

(defmethod do-endpoint-coordinates ((function function)
				    (coordinates list))
  (declare (dynamic-extent function))
  (loop until (empty? coordinates)
	do (let ((x1 (pop coordinates))
		 (y1 (pop coordinates))
		 (x2 (pop coordinates))
		 (y2 (pop coordinates)))
	     (funcall function x1 y1 x2 y2))))

#||
define method spread-point-sequence
    (sequence :: <sequence>) => (coords :: <simple-object-vector>)
  let length = size(sequence);
  let result :: <simple-object-vector> = make(<simple-vector>, size: 2 * length);
  let i :: <integer> = -1;
  without-bounds-checks
    for (point :: <standard-point> in sequence)
      result[inc!(i)] := point-x(point);
      result[inc!(i)] := point-y(point)
    end
  end;
  result
end method spread-point-sequence;
||#

(defgeneric spread-point-sequence (sequence))

(defmethod spread-point-sequence ((sequence sequence))
  (let* ((length (length sequence))
	 (result (make-array (* 2 length) :initial-element nil))
	 (i -1)
	 (point nil))
    (loop for c from 0 below length
	  do (setf point (elt sequence c))
	  do (setf (svref result (incf i)) (point-x point))
	  do (setf (svref result (incf i)) (point-y point)))
    result))


