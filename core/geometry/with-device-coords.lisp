;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GEOMETRY-INTERNALS -*-
(in-package #:duim-geometry-internals)

;;; MOVED FROM COORDINATES.LISP ($IDENTITY-TRANSFORM NOT DEFINED
;;; UNTIL TRANSFORMS.LISP!)

(defmacro with-device-coordinates ((transform &rest coords) &body body)
  ;; coords must be non-empty and have an even number of elements.
  ;; :fixme: this is just wrong...
  (let ((x (pop coords))
        (y (pop coords)))
    (if (null coords)
        `(multiple-value-bind (,x ,y)
             ,(if (eql transform *identity-transform*)
                  `(values (floor ,x) (floor ,y))
                  `(transform-position ,transform ,x ,y))
           ,@body)
        `(multiple-value-bind (,x ,y)
             ,(if (eql transform *identity-transform*)
                  `(values (floor ,x) (floor ,y))
                  `(transform-position ,transform ,x ,y))
           (with-device-coordinates (,transform ,@coords)
             ,@body)))))

(defmacro with-device-distances ((transform &rest distances) &body body)
  (let ((dx (pop distances))
        (dy (pop distances)))
    (if (null distances)
        `(multiple-value-bind (,dx ,dy)
             ,(if (eql transform *identity-transform*)
                  `(values (floor ,dx) (floor ,dy))
                  `(transform-distance ,transform ,dx ,dy))
          ,@body)
        `(multiple-value-bind (,dx ,dy)
             ,(if (eql transform *identity-transform*)
                  `(values (floor ,dx) (floor ,dy))
                  `(transform-distance ,transform ,dx ,dy))
          (with-device-distances (,transform ,@distances)
            ,@body)))))
