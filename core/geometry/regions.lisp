;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GEOMETRY-INTERNALS -*-
(in-package #:duim-geometry-internals)

#||
/// The basic region protocol

define protocol <<region-protocol>> ()
  // Basic protocol
  function region-empty?
    (region :: <region>) => (true? :: <boolean>);
  function region-equal
    (region1 :: <region>, region2 :: <region>)
 => (true? :: <boolean>);
  function region-contains-position?
    (region :: <region>, x :: <real>, y :: <real>)
 => (true? :: <boolean>);
  function region-contains-region?
    (region1 :: <region>, region2 :: <region>)
 => (true? :: <boolean>);
  function region-intersects-region?
    (region1 :: <region>, region2 :: <region>)
 => (true? :: <boolean>);
  function region-union
    (region1 :: <region>, region2 :: <region>)
 => (region :: <region>);
  function region-intersection
    (region1 :: <region>, region2 :: <region>)
 => (region :: <region>);
  function region-difference
    (region1 :: <region>, region2 :: <region>)
 => (region :: <region>);
  // Compound regions
  function do-regions
    (function :: <function>, region :: <region>, #key normalize?) => ();
  function region-set-function
    (region :: <region>) => (function);
  function region-set-regions
    (region :: <region>, #key normalize?) => (regions :: <sequence>);
  // Regions meet transforms
  function transform-region
    (transform :: <transform>, region :: <region>)
 => (region :: <region>);
  function untransform-region
    (transform :: <transform>, region :: <region>)
 => (region :: <region>);
  function transform-region!
    (transform :: <transform>, region :: <region>)
 => (region :: <region>);
  function untransform-region!
    (transform :: <transform>, region :: <region>)
 => (region :: <region>);
end protocol <<region-protocol>>;
||#
#||
(define-protocol <<region-protocol>> ()
  (:function region-empty? (region) (:documentation " Defined by: <<REGION-PROTOCOL>> "))
  (:function region-equal (region1 region2) (:documentation " Defined by: <<REGION-PROTOCOL>> "))
  (:function region-contains-position? (region x y) (:documentation " Defined by: <<REGION-PROTOCOL>> "))
  (:function region-contains-region? (region1 region2) (:documentation " Defined by: <<REGION-PROTOCOL>> "))
  (:function region-intersects-region? (region1 region2) (:documentation " Defined by: <<REGION-PROTOCOL>> "))
  (:function region-union (region1 region2) (:documentation " Defined by: <<REGION-PROTOCOL>> "))
  (:function region-intersection (region1 region2) (:documentation " Defined by: <<REGION-PROTOCOL>> "))
  (:function region-difference (region1 region2) (:documentation " Defined by: <<REGION-PROTOCOL>> "))
  ;; Compound regions
  (:function do-regions (function region &key normalize?) (:documentation " Defined by: <<REGION-PROTOCOL>> "))
  (:function region-set-function (region) (:documentation " Defined by: <<REGION-PROTOCOL>> "))
  (:function region-set-regions (region &key normalize?) (:documentation " Defined by: <<REGION-PROTOCOL>> "))
  ;; Regions meet transforms
  (:function transform-region (transform region) (:documentation " Defined by: <<REGION-PROTOCOL>> "))
  (:function untransform-region (transform region) (:documentation " Defined by: <<REGION-PROTOCOL>> "))
  (:function transform-region! (transform region) (:documentation " Defined by: <<REGION-PROTOCOL>> "))
  (:function untransform-region! (transform region) (:documentation " Defined by: <<REGION-PROTOCOL>> ")))
||#

#||
/// The basic region classes

define method untransform-region
    (transform :: <transform>, region :: <region>) => (region :: <region>)
  transform-region(invert-transform(transform), region)
end method untransform-region;
||#

(defmethod untransform-region ((transform <transform>)
                               (region    <region>))
  (transform-region (invert-transform transform) region))

#||
// The default method creates a new region
define method transform-region!
    (transform :: <transform>, region :: <region>) => (region :: <region>)
  transform-region(transform, region)
end method transform-region!;
||#

(defmethod transform-region! ((transform <transform>)
                              (region    <region>))
  (transform-region transform region))

#||
// The default method creates a new region
define method untransform-region!
    (transform :: <transform>, region :: <region>) => (region :: <region>)
  transform-region!(invert-transform(transform), region)
end method untransform-region!;
||#

(defmethod untransform-region! ((transform <transform>)
                                (region    <region>))
  (transform-region! (invert-transform transform) region))

#||
define method \=
    (region1 :: <region>, region2 :: <region>) => (true? :: <boolean>)
  region1 == region2
  | region-equal(region1, region2)
end method \=;
||#

(defmethod equal? ((region1 <region>) (region2 <region>))
"
Tests if its arguments are equal. Returns T if the two regions or
transforms are the same, otherwise returns nil. Two regions are
considered equal if they contain exactly the same set of points. Two
transforms are considered equal if they transform every region in the
same way.
"
  (or (eql region1 region2)
      (region-equal region1 region2)))

#||
/// General cases of region arithmetic

// Exclude the general case of 'region-equal'
define method region-equal
    (region1 :: <region>, region2 :: <region>) => (true? :: <boolean>)
  #f
end method region-equal;
||#

(defmethod region-equal ((region1 <region>)
                         (region2 <region>))
  nil)

#||
// Exclude the general case of 'region-contains-position?'
define method region-contains-position?
    (region :: <region>, x :: <real>, y :: <real>) => (true? :: <boolean>)
  #f
end method region-contains-position?;
||#

(defmethod region-contains-position? ((region <region>)
                                      (x real)
                                      (y real))
  nil)

#||
// Exclude the general case of 'region-contains-region?'
define method region-contains-region?
    (region1 :: <region>, region2 :: <region>) => (true? :: <boolean>)
  #f
end method region-contains-region?;
||#

(defmethod region-contains-region? ((region1 <region>)
                                    (region2 <region>))
  nil)

#||
// Exclude the general case of 'region-intersects-region?'
define method region-intersects-region?
    (region1 :: <region>, region2 :: <region>) => (true? :: <boolean>)
  #f
end method region-intersects-region?;
||#

(defmethod region-intersects-region? ((region1 <region>)
                                      (region2 <region>))
  nil)

#||
define method region-empty? (region :: <region>) => (true? :: <boolean>)
  #f
end method region-empty?;
||#

(defmethod region-empty? ((region <region>))
  nil)


#||
/// Nowhere

define sealed class <nowhere> (<region>)
end class <nowhere>;

define sealed domain make (singleton(<nowhere>));
define sealed domain initialize (<nowhere>);
||#

(defclass <nowhere> (<region>) ())

#||
define constant $nowhere :: <nowhere> = make(<nowhere>);
||#

(defvar *nowhere* (make-instance '<nowhere>)
"
The empty region, the opposite of $everywhere.
")

#||
define method region-equal
    (nowhere1 :: <nowhere>, nowhere2 :: <nowhere>) => (true? :: <boolean>)
  #t
end method region-equal;
||#

(defmethod region-equal ((nowhere1 <nowhere>)
                         (nowhere2 <nowhere>))
  t)

#||
define method region-contains-region?
    (region :: <region>, nowhere :: <nowhere>) => (true? :: <boolean>)
  #t
end method region-contains-region?;
||#

(defmethod region-contains-region? ((region  <region>)
                                    (nowhere <nowhere>))
  t)

#||
define method region-contains-region?
    (nowhere :: <nowhere>, region :: <region>) => (true? :: <boolean>)
  #f
end method region-contains-region?;
||#

(defmethod region-contains-region? ((nowhere <nowhere>)
                                    (region  <region>))
  nil)

#||
define method region-contains-region?
    (nowhere1 :: <nowhere>, nowhere2 :: <nowhere>) => (true? :: <boolean>)
  #f
end method region-contains-region?;
||#

(defmethod region-contains-region? ((nowhere1 <nowhere>)
                                    (nowhere2 <nowhere>))
  nil)

#||
define method region-empty? (region :: <nowhere>) => (true? :: <boolean>)
  #t
end method region-empty?;
||#

(defmethod region-empty? ((region <nowhere>))
  t)

#||
define method transform-region
    (transform :: <transform>, region :: <nowhere>) => (region :: <region>)
  region
end method transform-region;
||#

(defmethod transform-region ((transform <transform>)
                             (region    <nowhere>))
  region)

#||
/// Everywhere

define sealed class <everywhere> (<region>)
end class <everywhere>;

define sealed domain make (singleton(<everywhere>));
define sealed domain initialize (<everywhere>);
||#

(defclass <everywhere> (<region>) ())

#||
define constant $everywhere :: <everywhere> = make(<everywhere>);
||#

(defvar *everywhere* (make-instance '<everywhere>)
"
The region that includes all the points on the two-dimensional
infinite drawing plane.
")

#||
define inline function everywhere? (region) => (true? :: <boolean>)
  region == $everywhere
end function everywhere?;
||#

(defun everywhere? (region)
  (eql region *everywhere*))

#||
define method region-equal
    (everywhere1 :: <everywhere>, everywhere2 :: <everywhere>) => (true? :: <boolean>)
  #t
end method region-equal;
||#

(defmethod region-equal ((everywhere1 <everywhere>)
                         (everywhere2 <everywhere>))
  t)

#||
define method region-contains-position?
    (everywhere :: <everywhere>, x :: <real>, y :: <real>) => (true? :: <boolean>)
  #t
end method region-contains-position?;
||#

(defmethod region-contains-position? ((everywhere <everywhere>)
                                      (x real)
                                      (y real))
  t)

#||
define method region-contains-region?
    (region :: <region>, everywhere :: <everywhere>) => (true? :: <boolean>)
  #f
end method region-contains-region?;
||#

(defmethod region-contains-region? ((region     <region>)
                                    (everywhere <everywhere>))
  nil)

#||
define method region-contains-region?
    (everywhere :: <everywhere>, region :: <region>) => (true? :: <boolean>)
  #t
end method region-contains-region?;
||#

(defmethod region-contains-region? ((everywhere <everywhere>)
                                    (region     <region>))
  t)

#||
define method region-contains-region?
    (everywhere1 :: <everywhere>, everywhere2 :: <everywhere>) => (true? :: <boolean>)
  #t
end method region-contains-region?;
||#

(defmethod region-contains-region? ((everywhere1 <everywhere>)
                                    (everywhere2 <everywhere>))
  t)

#||
define method region-intersects-region?
    (everywhere :: <everywhere>, region :: <region>) => (true? :: <boolean>)
  ~(region == $nowhere)
end method region-intersects-region?;
||#

(defmethod region-intersects-region? ((everywhere <everywhere>)
                                      (region     <region>))
  (not (eql region *nowhere*)))

#||
define method region-intersects-region?
    (region :: <region>, everywhere :: <everywhere>) => (true? :: <boolean>)
  ~(region == $nowhere)
end method region-intersects-region?;
||#

(defmethod region-intersects-region? ((region     <region>)
                                      (everywhere <everywhere>))
  (not (eql region *nowhere*)))

#||
define method region-intersects-region?
    (everywhere1 :: <everywhere>, everywhere2 :: <everywhere>) => (true? :: <boolean>)
  #t
end method region-intersects-region?;
||#

(defmethod region-intersects-region? ((everywhere1 <everywhere>)
                                      (everywhere2 <everywhere>))
  t)

#||
define method region-empty? (region :: <everywhere>) => (true? :: <boolean>)
  #f
end method region-empty?;
||#

(defmethod region-empty? ((region <everywhere>))
  nil)

#||
define method transform-region
    (transform :: <transform>, region :: <everywhere>) => (region :: <region>)
  region
end method transform-region;
||#

(defmethod transform-region ((transform <transform>)
                             (region    <everywhere>))
  region)


#||
/// Points

define protocol <<point-protocol>> (<<region-protocol>>)
  function point-position
    (point :: <point>) => (x :: <real>, y :: <real>);
  getter point-x
    (point :: <point>) => (x :: <real>);
  getter point-y
    (point :: <point>) => (y :: <real>);
end protocol <<point-protocol>>;
||#
#||
(define-protocol <<point-protocol>> (<<region-protocol>>)
  (:function point-position (point) (:documentation " Defined by: <<POINT-PROTOCOL>> "))
  (:getter point-x (point) (:documentation " Defined by: <<POINT-PROTOCOL>> "))
  (:getter point-y (point) (:documentation " Defined by: <<POINT-PROTOCOL>> ")))
||#
#||
define sealed class <standard-point> (<point>)
  sealed slot point-x :: <real>,
    required-init-keyword: x:;
  sealed slot point-y :: <real>,
    required-init-keyword: y:;
end class <standard-point>;
||#

(defclass <standard-point> (<point>)
  ((point-x :type real :initarg :x :initform (required-slot ":x" "<standard-point>") :accessor point-x)
   (point-y :type real :initarg :y :initform (required-slot ":y" "<standard-point>") :accessor point-y)))

#||
define inline function make-point
    (x :: <real>, y :: <real>) => (point :: <standard-point>)
  make(<standard-point>, x: x, y: y)
end function make-point;
||#

(defun make-point (x y)
"
Returns an object of class <point> whose coordinates are x and y.
"
  (check-type x real)
  (check-type y real)
  (make-instance '<standard-point> :x x :y y))

#||
define sealed inline method make
    (class == <point>, #key x, y)
 => (point :: <standard-point>)
  make-point(x, y)
end method make;

define sealed domain make (singleton(<standard-point>));
define sealed domain initialize (<standard-point>);


define sealed inline method point-position
    (point :: <standard-point>) => (x :: <real>, y :: <real>)
  values(point-x(point), point-y(point))
end method point-position;
||#

(defmethod point-position ((point <standard-point>))
  (values (point-x point) (point-y point)))

#||
define sealed method region-equal
    (point1 :: <standard-point>, point2 :: <standard-point>) => (true? :: <boolean>)
  point-x(point1) = point-x(point2)
  & point-y(point1) = point-y(point2)
end method region-equal;
||#

(defmethod region-equal ((point1 <standard-point>)
                         (point2 <standard-point>))
  (and (= (point-x point1) (point-x point2))
       (= (point-y point1) (point-y point2))))

#||
define method transform-region
    (transform :: <transform>, point :: <standard-point>) => (point :: <standard-point>)
  let (x, y) = transform-position(transform, point-x(point), point-y(point));
  make-point(x, y)
end method transform-region;
||#

(defmethod transform-region ((transform <transform>)
                             (point     <standard-point>))
  (multiple-value-bind (x y)
      (transform-position transform (point-x point) (point-y point))
    (make-point x y)))

#||
define sealed method box-edges
    (point :: <standard-point>)
 => (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
  fix-box(point-x(point),     point-y(point),
	  point-x(point) + 1, point-y(point) + 1)
end method box-edges;
||#

(defmethod box-edges ((point <standard-point>))
  (fix-box     (point-x point)      (point-y point)
	   (1+ (point-x point)) (1+ (point-y point))))

#||
define sealed method region-contains-position?
    (point :: <standard-point>, x :: <real>, y :: <real>) => (true? :: <boolean>)
  point-x(point) = x
  & point-y(point) = y
end method region-contains-position?;
||#

(defmethod region-contains-position? ((point <standard-point>)
                                      (x real)
                                      (y real))
  (and (= (point-x point) x)
       (= (point-y point) y)))

#||
define sealed method region-contains-region?
    (point1 :: <standard-point>, point2 :: <standard-point>) => (true? :: <boolean>)
  point-x(point1) = point-x(point2)
  & point-y(point1) = point-y(point2)
end method region-contains-region?;
||#

(defmethod region-contains-region? ((point1 <standard-point>)
                                    (point2 <standard-point>))
  (and (= (point-x point1) (point-x point2))
       (= (point-y point1) (point-y point2))))

#||
define method region-contains-region?
    (region :: <region>, point :: <standard-point>) => (true? :: <boolean>)
  region-contains-position?(region, point-x(point), point-y(point))
end method region-contains-region?;
||#

(defmethod region-contains-region? ((region <region>)
                                    (point  <standard-point>))
  (region-contains-position? region (point-x point) (point-y point)))

#||
define sealed method region-intersects-region?
    (point1 :: <standard-point>, point2 :: <standard-point>) => (true? :: <boolean>)
  point-x(point1) = point-x(point2)
  & point-y(point1) = point-y(point2)
end method region-intersects-region?;
||#

(defmethod region-intersects-region? ((point1 <standard-point>)
                                      (point2 <standard-point>))
  (and (= (point-x point1) (point-x point2))
       (= (point-y point1) (point-y point2))))

#||
define method region-intersects-region?
    (point :: <standard-point>, region :: <region>) => (true? :: <boolean>)
  region-contains-position?(region, point-x(point), point-y(point))
end method region-intersects-region?;
||#

(defmethod region-intersects-region? ((point  <standard-point>)
                                      (region <region>))
  (region-contains-position? region (point-x point) (point-y point)))

#||
define method region-intersects-region?
    (region :: <region>, point :: <standard-point>) => (true? :: <boolean>)
  region-contains-position?(region, point-x(point), point-y(point))
end method region-intersects-region?;
||#

(defmethod region-intersects-region? ((region <region>)
                                      (point  <standard-point>))
  (region-contains-position? region (point-x point) (point-y point)))

#||
define sealed method region-intersection
    (point1 :: <standard-point>, point2 :: <standard-point>) => (region :: <region>)
  if (point-x(point1) = point-x(point2)
      & point-y(point1) = point-y(point2))
    point1
  else
    $nowhere
  end
end method region-intersection;
||#

(defmethod region-intersection ((point1 <standard-point>)
                                (point2 <standard-point>))
  (if (and (= (point-x point1) (point-x point2))
           (= (point-y point1) (point-y point2)))
      point1
      *nowhere*))


#||
/// Region Sets

define method region-set-function (region :: <region>) => (function)
  union
end method region-set-function;
||#

(defmethod region-set-function ((region <region>))
  #'union)

#||
define method region-set-regions
    (region :: <region>, #key normalize?) => (regions :: <vector>)
  ignore(normalize?);
  vector(region)
end method region-set-regions;
||#

(defmethod region-set-regions ((region <region>)
                               &key
                               (normalize? nil))
  (declare (ignore normalize?))
  (vector region))

#||
define method do-regions
    (function :: <function>, region :: <region>, #key normalize?) => ()
  ignore(normalize?);
  function(region)
end method do-regions;
||#

(defmethod do-regions (function
		       (region <region>)
		       &key
		       (normalize? nil))
  (declare (ignore normalize?))
  (funcall function region))

#||
define method do-regions
    (function :: <function>, region :: <region-set>, #rest args, #key normalize?) => ()
  dynamic-extent(args);
  ignore(normalize?);
  do(function, apply(region-set-regions, region, args))
end method do-regions;
||#

(defmethod do-regions (function
		       (region <region-set>)
                       &rest
		       args
		       &key
		       normalize?)
  (declare (dynamic-extent args)
           (ignore normalize?))
  (map nil function (apply #'region-set-regions region args)))

#||
define method region-contains-position?
    (region-set :: <region-set>, x :: <real>, y :: <real>) => (true? :: <boolean>)
  block (return)
    local method contains-position? (region) => ()
	    when (region-contains-position?(region, x, y))
	      return(#t)
	    end
	  end method;
    do-regions(contains-position?, region-set);
    #f
  end
end method region-contains-position?;
||#

(defmethod region-contains-position? ((region-set <region-set>)
                                      (x real)
                                      (y real))
  (flet ((contains-position? (region)
             (when (region-contains-position? region x y)
               (return-from region-contains-position? t))))
    (do-regions #'contains-position? region-set)
    nil))

#||
define method region-contains-region?
    (region-set :: <region-set>, other-region :: <region>) => (true? :: <boolean>)
  block (return)
    local method contains-region? (region) => ()
	    when (region-contains-region?(region, other-region))
	      return(#t)
	    end
	  end method;
    do-regions(contains-region?, region-set);
    #f
  end
end method region-contains-region?;
||#

(defmethod region-contains-region? ((region-set   <region-set>)
                                    (other-region <region>))
  (flet ((contains-region? (region)
             (when (region-contains-region? region other-region)
               (return-from region-contains-region? t))))
    (do-regions #'contains-region? region-set))
  nil)

#||
define method box-edges
    (region-set :: <region-set>)
 => (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
  let left :: <integer> = $largest-coordinate;
  let top  :: <integer> = $largest-coordinate;
  let right  :: <integer> = $smallest-coordinate;
  let bottom :: <integer> = $smallest-coordinate;
  local method add-region (region) => ()
	  let (rl, rt, rr, rb) = box-edges(region);
	  min!(left, rl);
	  min!(top, rt);
	  max!(right, rr);
	  max!(bottom, rb);
	end method;
  do-regions(add-region, region-set);
  values(left, top, right, bottom)
end method box-edges;
||#

(defmethod box-edges ((region-set <region-set>))
  (let ((left   +largest-coordinate+)
        (top    +largest-coordinate+)
        (right  +smallest-coordinate+)
        (bottom +smallest-coordinate+))
    (flet ((add-region (region)
               (multiple-value-bind (rl rt rr rb)
                   (box-edges region)
		 (setf left (min left   rl))
                 (setf top (min top    rt))
		 (setf right (max right  rr))
		 (setf bottom (max bottom rb)))))
      (do-regions #'add-region region-set))
    (values left top right bottom)))


