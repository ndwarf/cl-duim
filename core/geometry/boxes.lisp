;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GEOMETRY-INTERNALS -*-
(in-package #:duim-geometry-internals)

#||
/// Bounding boxes

/// Box protocol

define protocol <<box-protocol>> ()
  function bounding-box
    (region, #key into) => (box :: <bounding-box>);
  function box-edges
    (region)
 => (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>);
  function set-box-edges
    (region, left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
 => (region);
  function box-position (region) => (x :: <integer>, y :: <integer>);
  function set-box-position (region, x :: <integer>, y :: <integer>) => (region);
  function box-size (region) => (width :: <integer>, height :: <integer>);
  function set-box-size (region, width :: <integer>, height :: <integer>) => (region);
  function box-width  (region) => (width :: <integer>);
  function box-height (region) => (height :: <integer>);
end protocol <<box-protocol>>;
||#
#||
(define-protocol <<box-protocol>> ()
  (:function bounding-box (region &key into) (:documentation " Defined by: <<BOX-PROTOCOL>> "))
  ;; XXX: this is already defined in REGIONS.LISP. Maybe we need to move
  ;; these protocols out into their own file?
  (:function box-edges (region) (:documentation " Defined by: <<BOX-PROTOCOL>> "))
  (:function set-box-edges (region left top right bottom) (:documentation " Defined by: <<BOX-PROTOCOL>> "))
  (:function box-position (region) (:documentation " Defined by: <<BOX-PROTOCOL>> "))
  (:function set-box-position (region x y) (:documentation " Defined by: <<BOX-PROTOCOL>> "))
  (:function box-size (region) (:documentation " Defined by: <<BOX-PROTOCOL>> "))
  (:function set-box-size (region width height) (:documentation " Defined by: <<BOX-PROTOCOL>> "))
  (:function box-width (region) (:documentation " Defined by: <<BOX-PROTOCOL>> "))
  (:function box-height (region) (:documentation " Defined by: <<BOX-PROTOCOL>> ")))
||#
#||
/// Box creation

// Canonicalizes x1/y1/x2/y2 into left/top/right/bottom
// Note that boxes are in an _inverted_ cartesian coordinate system,
// with the positive Y axis running _downward_.  This matches the
// notion of most window systems
define sealed method make-bounding-box
    (x1 :: <real>, y1 :: <real>, x2 :: <real>, y2 :: <real>)
 => (box :: <bounding-box>)
  let (x1 :: <integer>, y1 :: <integer>, x2 :: <integer>, y2 :: <integer>)
    = fix-box(x1, y1, x2, y2);
  when (x1 > x2)
    swap!(x1, x2)
  end;
  when (y1 > y2)
    swap!(y1, y2)
  end;
  if (zero?(x1) & zero?(y1))
    make(<simple-box>, width: x2, height: y2)
  else
    make(<general-box>, left: x1, top: y1, right: x2, bottom: y2)
  end
end method make-bounding-box;
||#

(defun make-bounding-box (x1 y1 x2 y2)
"
Returns an object of the class <bounding-box> with the edges specified
by _x1_, _y1_, _x2_, and _y2_. _x1_, _y1_, _x2_, and _y2_ are
canonicalized in the following way. The min point of the box has an x
coordinate that is the smaller of _x1_ and _x2_ and a y coordinate
that is the smaller of _y1_ and _y2_. The max point of the box has an
x coordinate that is the larger of _x1_ and _x2_ and a y coordinate
that is the larger of _y1_ and _y2_. (Therefore, in a right-handed
coordinate system the canonicalized values of _x1_, _y1_, _x2_ and
_y2_ correspond to the left, top, right, and bottom edges of the box,
respectively.)

This is a convenient shorthand function for (make-instance
'<bounding-box> :left :top :right :bottom)
"
  (multiple-value-bind (x1 y1 x2 y2)
      (fix-box x1 y1 x2 y2)
    (when (> x1 x2) (rotatef x1 x2))
    (when (> y1 y2) (rotatef y1 y2))
    (if (and (zerop x1) (zerop y1))
        (make-instance '<simple-box> :width x2 :height y2)
      (make-instance '<general-box>
                     :left x1 :top y1
                     :right x2 :bottom y2))))

#||
define sealed inline method make
    (class == <bounding-box>, #key left, top, right, bottom)
 => (box :: <bounding-box>)
  make-bounding-box(left, top, right, bottom)
end method make;

// Seal the constructors and initializers for all bounding boxes
define sealed domain make (subclass(<bounding-box>));
define sealed domain initialize (<bounding-box>);


/// General bounding boxes

define sealed class <general-box> (<bounding-box>)
  sealed slot %left :: <integer>,
    required-init-keyword: left:;
  sealed slot %top  :: <integer>,
    required-init-keyword: top:;
  sealed slot %right  :: <integer>,
    required-init-keyword: right:;
  sealed slot %bottom :: <integer>,
    required-init-keyword: bottom:;
end class <general-box>;
||#


(defclass <general-box> (<bounding-box>)
  ((%left :type integer :initarg :left :initform (required-slot ":left" "<general-box>") :accessor %left)
   (%top :type integer :initarg :top :initform (required-slot ":top" "<general-box>") :accessor %top)
   (%right :type integer :initarg :right :initform (required-slot ":right" "<general-box>") :accessor %right)
   (%bottom :type integer :initarg :bottom :initform (required-slot ":bottom" "<general-box>") :accessor %bottom)))

#||
define sealed inline method box-edges
    (box :: <general-box>)
 => (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
  values(box.%left, box.%top, box.%right, box.%bottom)
end method box-edges;
||#

(defmethod box-edges ((box <general-box>))
  (values (%left box) (%top box) (%right box) (%bottom box)))

#||
define sealed method transform-region 
    (transform :: <transform>, box :: <general-box>) => (box :: <bounding-box>)
  let (x1, y1, x2, y2)
    = transform-box(transform, box.%left, box.%top, box.%right, box.%bottom);
  make-bounding-box(x1, y1, x2, y2)
end method transform-region;
||#

(defmethod transform-region ((transform <transform>)
                             (box       <general-box>))
  (multiple-value-bind (x1 y1 x2 y2)
      (transform-box transform
		     (%left box)
		     (%top box)
		     (%right box)
		     (%bottom box))
    (make-bounding-box x1 y1 x2 y2)))

#||
define sealed method untransform-region 
    (transform :: <transform>, box :: <general-box>) => (box :: <bounding-box>)
  let (x1, y1, x2, y2)
    = untransform-box(transform, box.%left, box.%top, box.%right, box.%bottom);
  make-bounding-box(x1, y1, x2, y2)
end method untransform-region;
||#

(defmethod untransform-region ((transform <transform>)
                               (box       <general-box>))
  (multiple-value-bind (x1 y1 x2 y2)
      (untransform-box transform
		       (%left box)
		       (%top box)
		       (%right box)
		       (%bottom box))
    (make-bounding-box x1 y1 x2 y2)))

#||
define sealed method transform-region!
    (transform :: <transform>, box :: <general-box>) => (box :: <bounding-box>)
  let (x1, y1, x2, y2)
    = transform-box(transform, box.%left, box.%top, box.%right, box.%bottom);
  when (x1 > x2)
    swap!(x1, x2)
  end;
  when (y1 > y2)
    swap!(y1, y2)
  end;
  box.%left := x1;
  box.%top  := y1;
  box.%right  := x2;
  box.%bottom := y2;
  box
end method transform-region!;
||#

(defmethod transform-region! ((transform <transform>)
                              (box       <general-box>))
  (multiple-value-bind (x1 y1 x2 y2)
      (transform-box transform
		     (%left box)
		     (%top box)
		     (%right box)
		     (%bottom box))
    (when (> x1 x2) (rotatef x1 x2))
    (when (> y1 y2) (rotatef y1 y2))
    (setf (%left   box) x1
          (%top    box) y1
          (%right  box) x2
          (%bottom box) y2))
  box)

#||
define sealed method untransform-region!
    (transform :: <transform>, box :: <general-box>) => (box :: <bounding-box>)
  let (x1, y1, x2, y2)
    = untransform-box(transform, box.%left, box.%top, box.%right, box.%bottom);
  when (x1 > x2)
    swap!(x1, x2)
  end;
  when (y1 > y2)
    swap!(y1, y2)
  end;
  box.%left := x1;
  box.%top  := y1;
  box.%right  := x2;
  box.%bottom := y2;
  box
end method untransform-region!;
||#

(defmethod untransform-region! ((transform <transform>)
                                (box       <general-box>))
  (multiple-value-bind (x1 y1 x2 y2)
      (untransform-box transform
		       (%left box)
		       (%top box)
		       (%right box)
		       (%bottom box))
    (when (> x1 x2) (rotatef x1 x2))
    (when (> y1 y2) (rotatef y1 y2))
    (setf (%left   box) x1
          (%top    box) y1
          (%right  box) x2
          (%bottom box) y2))
  box)

#||
define sealed method region-empty?
    (box :: <general-box>) => (true? :: <boolean>)
    box.%right <= box.%left
  | box.%bottom <= box.%top
end method region-empty?;
||#

(defmethod region-empty? ((box <general-box>))
  (or (<= (%right box) (%left box))
      (<= (%bottom box) (%top box))))

#||
// Set the edges of the box, and return the box as the value
// Note that we don't downgrade a <general-box> to a <simple-box>,
// because that seems more likely to cause oscillating consathons
define sealed method set-box-edges
    (box :: <general-box>,
     left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
 => (box :: <general-box>)
  box.%left := left;
  box.%top  := top;
  box.%right  := right;
  box.%bottom := bottom;
  box
end method set-box-edges;
||#

(defmethod set-box-edges ((box <general-box>)
                          (left integer)
                          (top integer)
                          (right integer)
                          (bottom integer))
  (setf (%left   box) left
        (%top    box) top
        (%right  box) right
        (%bottom box) bottom)
  box)

#||
// Set the position of the box, and return the box as the value
define sealed method set-box-position
    (box :: <general-box>, x :: <integer>, y :: <integer>)
 => (box :: <general-box>)
  let width  :: <integer> = box.%right  - box.%left;
  let height :: <integer> = box.%bottom - box.%top;
  box.%left := x;
  box.%top  := y;
  box.%right  := x + width;
  box.%bottom := y + height;
  box
end method set-box-position;
||#

(defmethod set-box-position ((box <general-box>)
                             (x integer)
                             (y integer))
  (let ((width (- (%right box) (%left box)))
        (height (- (%bottom box) (%top box))))
    (setf (%left   box) x
          (%top    box) y
          (%right  box) (+ x width)
          (%bottom box) (+ y height)))
  box)

#||
// Set the size of the box, and return the box as the value
define sealed method set-box-size
    (box :: <general-box>, width :: <integer>, height :: <integer>)
 => (box :: <general-box>)
  let new-right  :: <integer> = box.%left + width;
  let new-bottom :: <integer> = box.%top + height;
  box.%right  := new-right;
  box.%bottom := new-bottom;
  box
end method set-box-size;
||#

(defmethod set-box-size ((box <general-box>)
                         (width integer)
                         (height integer))
  (let ((new-right (+ (%left box) width))
        (new-bottom (+ (%top box) height)))
    (setf (%right  box) new-right
          (%bottom box) new-bottom))
  box)

#||
define sealed method invalidate-box!
    (box :: <general-box>) => ()
  box.%left := $largest-coordinate
end method invalidate-box!;
||#

(defgeneric invalidate-box! (box))

(defmethod invalidate-box! ((box <general-box>))
  (setf (%left box) +largest-coordinate+))

#||
define sealed method box-invalidated?
    (box :: <general-box>) => (invalid? :: <boolean>)
  box.%left = $largest-coordinate
end method box-invalidated?;
||#

(defgeneric box-invalidated? (box))

(defmethod box-invalidated? ((box <general-box>))
  (= (%left box) +largest-coordinate+))



#||
/// Simple bounding boxes

define sealed class <simple-box> (<bounding-box>)
  sealed slot %width  :: <integer>,
    required-init-keyword: width:;
  sealed slot %height :: <integer>,
    required-init-keyword: height:;
end class <simple-box>;
||#

(defclass <simple-box> (<bounding-box>)
  ((%width :type integer :initarg :width :initform (required-slot ":width" "<simple-box>") :accessor %width)
   (%height :type integer :initarg :height :initform (required-slot ":height" "<simple-box>") :accessor %height)))

#||
define sealed inline method box-edges
    (box :: <simple-box>)
 => (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
  values(0, 0, box.%width, box.%height)
end method box-edges;
||#

(defmethod box-edges ((box <simple-box>))
  (values 0 0 (%width box) (%height box)))

#||
define sealed method transform-region 
    (transform :: <transform>, box :: <simple-box>) => (box :: <bounding-box>)
  let (x1, y1, x2, y2)
    = transform-box(transform, 0, 0, box.%width, box.%height);
  make-bounding-box(x1, y1, x2, y2)
end method transform-region;
||#

(defmethod transform-region ((transform <transform>)
                             (box       <simple-box>))
  (multiple-value-bind (x1 y1 x2 y2)
      (transform-box transform 0 0 (%width box) (%height box))
    (make-bounding-box x1 y1 x2 y2)))

#||
define sealed method untransform-region 
    (transform :: <transform>, box :: <simple-box>) => (box :: <bounding-box>)
  let (x1, y1, x2, y2)
    = untransform-box(transform, 0, 0, box.%width, box.%height);
  make-bounding-box(x1, y1, x2, y2)
end method untransform-region;
||#

(defmethod untransform-region ((transform <transform>)
                               (box       <simple-box>))
  (multiple-value-bind (x1 y1 x2 y2)
      (untransform-box transform 0 0 (%width box) (%height box))
    (make-bounding-box x1 y1 x2 y2)))

#||
define sealed method transform-region!
    (transform :: <transform>, box :: <simple-box>) => (box :: <bounding-box>)
  let (x1, y1, x2, y2)
    = transform-box(transform, 0, 0, box.%width, box.%height);
  when (x1 > x2)
    swap!(x1, x2)
  end;
  when (y1 > y2)
    swap!(y1, y2)
  end;
  if (zero?(x1) & zero?(y1))
    box.%width  := x2;
    box.%height := y2;
    box
  else
    make(<general-box>, left: x1, top: y1, right: x2, bottom: y2)
  end
end method transform-region!;
||#

(defmethod transform-region! ((transform <transform>)
                              (box       <simple-box>))
  (multiple-value-bind (x1 y1 x2 y2)
      (transform-box transform 0 0 (%width box) (%height box))
    (when (> x1 x2) (rotatef x1 x2))
    (when (> y1 y2) (rotatef y1 y2))
    (cond
      ((and (zerop x1) (zerop y1))
       (setf (%width  box) x2
             (%height box) y2)
       box)
      (t
       (make-instance '<general-box>
                      :left   x1 :top    y1
                      :right  x2 :bottom y2)))))

#||
define sealed method untransform-region!
    (transform :: <transform>, box :: <simple-box>) => (box :: <bounding-box>)
  let (x1, y1, x2, y2)
    = untransform-box(transform, 0, 0, box.%width, box.%height);
  when (x1 > x2)
    swap!(x1, x2)
  end;
  when (y1 > y2)
    swap!(y1, y2)
  end;
  if (zero?(x1) & zero?(y1))
    box.%width  := x2;
    box.%height := y2;
    box
  else
    make(<general-box>, left: x1, top: y1, right: x2, bottom: y2)
  end
end method untransform-region!;
||#

(defmethod untransform-region! ((transform <transform>)
                                (box       <simple-box>))
  (multiple-value-bind (x1 y1 x2 y2)
      (untransform-box transform 0 0 (%width box) (%height box))
    (when (> x1 x2) (rotatef x1 x2))
    (when (> y1 y2) (rotatef y1 y2))
    (cond
      ((and (zerop x1) (zerop y1))
       (setf (%width  box) x2
             (%height box) y2)
       box)
      (t
       (make-instance '<general-box>
                      :left  x1 :top y1
                      :right x2 :bottom y2)))))

#||
define sealed method region-empty?
    (box :: <simple-box>) => (true? :: <boolean>)
  box.%width <= 0
  | box.%height <= 0
end method region-empty?;
||#

(defmethod region-empty? ((box <simple-box>))
  (or (<= (%width  box) 0)
      (<= (%height box) 0)))

#||
// Set the edges of the box, and return the box as the value
define sealed method set-box-edges
    (box :: <simple-box>,
     left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
 => (box :: <bounding-box>)
  if (zero?(left) & zero?(top))
    box.%width  := right;
    box.%height := bottom;
    box
  else
    make-bounding-box(left, top, right, bottom)
  end
end method set-box-edges;
||#

(defmethod set-box-edges ((box <simple-box>)
                          (left integer)
                          (top integer)
                          (right integer)
                          (bottom integer))
  (cond
    ((and (zerop left) (zerop top))
     (setf (%width  box) right
           (%height box) bottom)
     box)
    (t
     (make-bounding-box left top right bottom))))

#||
// Set the position of the box, and return the box as the value
define sealed method set-box-position
    (box :: <simple-box>, x :: <integer>, y :: <integer>)
 => (box :: <bounding-box>)
  if (zero?(x) & zero?(y))
    box
  else
    make-bounding-box(x, y, x + box.%width, y + box.%height)
  end
end method set-box-position;
||#

(defmethod set-box-position ((box <simple-box>)
                             (x integer)
                             (y integer))
  (if (and (zerop x) (zerop y))
      box
    (make-bounding-box x y (+ x (%width box)) (+ y (%height box)))))

#||
// Set the size of the box, and return the box as the value
define sealed method set-box-size
    (box :: <simple-box>, width :: <integer>, height :: <integer>)
 => (box :: <simple-box>)
  box.%width  := width;
  box.%height := height;
  box
end method set-box-size;
||#

(defmethod set-box-size ((box <simple-box>)
                         (width integer)
                         (height integer))
  (setf (%width  box) width
        (%height box) height)
  box)

#||
define sealed method invalidate-box!
    (box :: <simple-box>) => ()
  box.%width := $smallest-coordinate
end method invalidate-box!;
||#

(defmethod invalidate-box! ((box <simple-box>))
  (setf (%width box) +smallest-coordinate+))

#||
define sealed method box-invalidated?
    (box :: <simple-box>) => (invalid? :: <boolean>)
  box.%width = $smallest-coordinate
end method box-invalidated?;
||#

(defmethod box-invalidated? ((box <simple-box>))
  (= (%width box) +smallest-coordinate+))



#||

/// The rest of the box protocol...

define sealed method region-equal
    (box1 :: <bounding-box>, box2 :: <bounding-box>) => (true? :: <boolean>)
  let (left1, top1, right1, bottom1) = box-edges(box1);
  let (left2, top2, right2, bottom2) = box-edges(box2);
  ltrb-equals-ltrb?(left1, top1, right1, bottom1,
		    left2, top2, right2, bottom2)
end method region-equal;
||#

(defmethod region-equal ((box1 <bounding-box>)
                         (box2 <bounding-box>))
  (multiple-value-bind (left1 top1 right1 bottom1)
      (box-edges box1)
    (multiple-value-bind (left2 top2 right2 bottom2)
        (box-edges box2)
      (ltrb-equals-ltrb? left1 top1 right1 bottom1
                         left2 top2 right2 bottom2))))

#||
define sealed method region-contains-position? 
    (box :: <bounding-box>, x :: <real>, y :: <real>) => (true? :: <boolean>)
  let (left, top, right, bottom) = box-edges(box);
  ltrb-contains-position?(left, top, right, bottom,
			  fix-coordinate(x), fix-coordinate(y))
end method region-contains-position?;
||#

(defmethod region-contains-position? ((box <bounding-box>)
                                      (x real)
                                      (y real))
  (multiple-value-bind (left top right bottom)
      (box-edges box)
    (ltrb-contains-position? left top right bottom
                             (fix-coordinate x) (fix-coordinate y))))

#||
define sealed method region-contains-region? 
    (box1 :: <bounding-box>, box2 :: <bounding-box>) => (true? :: <boolean>)
  let (left1, top1, right1, bottom1) = box-edges(box1);
  let (left2, top2, right2, bottom2) = box-edges(box2);
  ltrb-contains-ltrb?(left1, top1, right1, bottom1,
		      left2, top2, right2, bottom2)
end method region-contains-region?;
||#

(defmethod region-contains-region? ((box1 <bounding-box>)
                                    (box2 <bounding-box>))
  (multiple-value-bind (left1 top1 right1 bottom1)
      (box-edges box1)
    (multiple-value-bind (left2 top2 right2 bottom2)
        (box-edges box2)
      (ltrb-contains-ltrb? left1 top1 right1 bottom1
                           left2 top2 right2 bottom2))))

#||
define sealed method region-intersects-region?
    (box1 :: <bounding-box>, box2 :: <bounding-box>) => (true? :: <boolean>)
  let (left1, top1, right1, bottom1) = box-edges(box1);
  let (left2, top2, right2, bottom2) = box-edges(box2);
  ltrb-intersects-ltrb?(left1, top1, right1, bottom1,
			left2, top2, right2, bottom2)
end method region-intersects-region?;
||#

(defmethod region-intersects-region? ((box1 <bounding-box>)
                                      (box2 <bounding-box>))
  (multiple-value-bind (left1 top1 right1 bottom1)
      (box-edges box1)
    (multiple-value-bind (left2 top2 right2 bottom2)
        (box-edges box2)
      (ltrb-intersects-ltrb? left1 top1 right1 bottom1
                             left2 top2 right2 bottom2))))

#||
define method box-position
    (region) => (x :: <integer>, y :: <integer>)
  let (left, top, right, bottom) = box-edges(region);
  ignore(right, bottom);
  values(left, top)
end method box-position;
||#

(defmethod box-position (region)
  (multiple-value-bind (left top right bottom)
      (box-edges region)
    (declare (ignore right bottom))
    (values left top)))

#||
define method box-width
    (region) => (width :: <integer>)
  let (left, top, right, bottom) = box-edges(region);
  ignore(top, bottom);
  right - left
end method box-width;
||#

(defmethod box-width (region)
  (multiple-value-bind (left top right bottom)
      (box-edges region)
    (declare (ignore top bottom))
    (- right left)))

#||
define method box-height
    (region) => (height :: <integer>)
  let (left, top, right, bottom) = box-edges(region);
  ignore(left, right);
  bottom - top
end method box-height;
||#

(defmethod box-height (region)
  (multiple-value-bind (left top right bottom)
      (box-edges region)
    (declare (ignore left right))
    (- bottom top)))

#||
define method box-size
    (region) => (width :: <integer>, height :: <integer>)
  let (left, top, right, bottom) = box-edges(region);
  values(right - left, bottom - top)
end method box-size;
||#

;;; Not specialised (on, for example, <region>) because we invoke this
;;; on sheets as well as regions.

(defmethod box-size (region)
  (multiple-value-bind (left top right bottom)
      (box-edges region)
    (values (- right left) (- bottom top))))

#||
define function box-positions-equal
    (region1, region2) => (true? :: <boolean>)
  let (x1, y1) = box-position(region1);
  let (x2, y2) = box-position(region2);
  x1 = x2 & y1 = y2
end function box-positions-equal;
||#

(defun box-positions-equal (region1 region2)
  (multiple-value-bind (x1 y1)
      (box-position region1)
    (multiple-value-bind (x2 y2)
        (box-position region2)
      (and (= x1 x2) (= y1 y2)))))

#||
define function box-edges-equal
    (region1, region2) => (true? :: <boolean>)
  let (left1, top1, right1, bottom1) = box-edges(region1);
  let (left2, top2, right2, bottom2) = box-edges(region2);
  left1 = left2 & top1 = top2 & right1 = right2 & bottom1 = bottom2
end function box-edges-equal;
||#

(defun box-edges-equal (region1 region2)
  (multiple-value-bind (left1 top1 right1 bottom1)
      (box-edges region1)
    (multiple-value-bind (left2 top2 right2 bottom2)
        (box-edges region2)
      (and (= left1 left2)
           (= top1 top2)
           (= right1 right2)
           (= bottom1 bottom2)))))

#||
// This should only be used to compare integer coordinates
define inline function position-difference
    (x1 :: <integer>, y1 :: <integer>,
     x2 :: <integer>, y2 :: <integer>)
 => (dx :: <integer>, dy :: <integer>)
  values(x1 - x2, y1 - y2)
end function position-difference;
||#

(defun position-difference (x1 y1 x2 y2)
  (check-type x1 integer)
  (check-type y1 integer)
  (check-type x2 integer)
  (check-type y2 integer)
  (values (- x1 x2) (- y1 y2)))

#||
define function box-position-difference
    (region1, region2) => (dx :: <integer>, dy :: <integer>)
  let (x1, y1) = box-position(region1);
  let (x2, y2) = box-position(region2);
  position-difference(x1, y1, x2, y2)
end function box-position-difference;
||#

(defun box-position-difference (region1 region2)
  (multiple-value-bind (x1 y1)
      (box-position region1)
    (multiple-value-bind (x2 y2)
        (box-position region2)
      (position-difference x1 y1 x2 y2))))

#||
define function box-sizes-equal
    (region1, region2) => (true? :: <boolean>)
  let (left1, top1, right1, bottom1) = box-edges(region1);
  let (left2, top2, right2, bottom2) = box-edges(region2);
  ltrb-size-equal?(left1, top1, right1, bottom1,
		   left2, top2, right2, bottom2)
end function box-sizes-equal;
||#

(defun box-sizes-equal (region1 region2)
  (multiple-value-bind (left1 top1 right1 bottom1)
      (box-edges region1)
    (multiple-value-bind (left2 top2 right2 bottom2)
        (box-edges region2)
      (ltrb-size-equal? left1 top1 right1 bottom1
                        left2 top2 right2 bottom2))))



#||
/// Convenience functions

// Guaranteed to cons a new box unless INTO is supplied
define sealed method bounding-box
    (region, #key into) => (box :: <bounding-box>)
  let (left, top, right, bottom) = box-edges(region);
  if (into)
    select (into by instance?)
      <simple-box> =>
	assert(zero?(left) & zero?(top),
	       "The simple box %= cannot be modified this way", into);
	into.%width  := right;
	into.%height := bottom;
	into;
      <general-box> =>
	into.%left := left;
	into.%top  := top;
	into.%right  := right;
	into.%bottom := bottom;
	into;
    end
  else
    make-bounding-box(left, top, right, bottom)
  end
end method bounding-box;
||#

(defmethod bounding-box (region
                         &key
                         into)
  (multiple-value-bind (left top right bottom)
      (box-edges region)
    (if into
        (etypecase into
          (<simple-box>
	   (unless (and (zerop left) (zerop top))
	     (error "The simple box ~a cannot be modified this way" into))
           (setf (%width  into) right
                 (%height into) bottom)
           into)
          (<general-box>
           (setf (%left   into) left
                 (%top    into) top
                 (%right  into) right
                 (%bottom into) bottom)
           into))
        ;; else
        (make-bounding-box left top right bottom))))

#||
// Make a new bounding box for the region, and shift its position by DX,DY,
// and return the new box
define sealed method shift-box-position
    (region, dx :: <integer>, dy :: <integer>, #key into)
 => (box :: <bounding-box>)
  let (left, top, right, bottom) = box-edges(region);
  let box :: <general-box>
    = if (into & instance?(into, <general-box>))
	into
      else
	make(<general-box>,
	     left: left, top: top, right: right, bottom: bottom)
      end;
  box.%left := box.%left + dx;
  box.%top  := box.%top + dy;
  box.%right  := box.%right + dx;
  box.%bottom := box.%bottom + dy;
  box
end method shift-box-position;
||#

(defgeneric shift-box-position (region dx dy &key into))

(defmethod shift-box-position (region
                               (dx integer)
                               (dy integer)
                               &key
                               into)
  (multiple-value-bind (left top right bottom)
      (box-edges region)
    (let ((box (if (and into (typep into '<general-box>))
                   into
                 (make-instance '<general-box>
                                :left   left :top    top
                                :right  right :bottom bottom))))
      (setf (%left   box) (+ (%left   box) dx)
            (%top    box) (+ (%top    box) dy)
            (%right  box) (+ (%right  box) dx)
            (%bottom box) (+ (%bottom box) dy))
      box)))

#||
define method box-center
    (region) => (x :: <integer>, y :: <integer>)
  let (left, top, right, bottom) = box-edges(region);
  values(left + floor/((right - left), 2), top + floor/((bottom - top), 2))
end method box-center;
||#

(defgeneric box-center (region))

(defmethod box-center (region)
  (multiple-value-bind (left top right bottom)
      (box-edges region)
    (values (+ left (floor (- right  left) 2))
            (+ top  (floor (- bottom top)  2)))))

#||
define method box-center*
    (region) => (point :: <standard-point>)
  let (left, top, right, bottom) = box-edges(region);
  make-point(left + floor/((right - left), 2), top + floor/((bottom - top), 2))
end method box-center*;
||#

(defgeneric box-center* (region))

(defmethod box-center* (region)
  (multiple-value-bind (left top right bottom)
      (box-edges region)
    (make-point (+ left (floor (- right  left) 2))
                (+ top  (floor (- bottom top)  2)))))

#||
/// The following are always implemented in terms of 'box-edges'

define inline function box-left
    (region) => (left :: <integer>)
  let (left, top, right, bottom) = box-edges(region);
  ignore(top, right, bottom);
  left
end function box-left;
||#

(defun box-left (region)
"
Returns the x coordinate of the upper left corner of the bounding box
_region_. The argument _region_ must be either a bounded region or
some other object that obeys the bounding box protocol, such as a
sheet.
"
  (multiple-value-bind (left top right bottom)
      (box-edges region)
    (declare (ignore top right bottom))
    left))

#||
define inline function box-top
    (region) => (top :: <integer>)
  let (left, top, right, bottom) = box-edges(region);
  ignore(left, right, bottom);
  top
end function box-top;
||#

(defun box-top (region)
"
Returns the y coordinate of the upper left corner of the bounding box
_region_. The argument _region_ must be either a bounded region of
some other object that obeys the bounding box protocol.
"
  (multiple-value-bind (left top right bottom)
      (box-edges region)
    (declare (ignore left right bottom))
    top))

#||
define inline function box-right
    (region) => (right :: <integer>)
  let (left, top, right, bottom) = box-edges(region);
  ignore(left, top, bottom);
  right
end function box-right;
||#

(defun box-right (region)
"
Returns the x coordinate of the bottom right corner of the bounding
box _region_. The argument _region_ must be either a bounded region or
some other object that obeys the bounding box protocol, such as a
sheet.
"
  (multiple-value-bind (left top right bottom)
      (box-edges region)
    (declare (ignore left top bottom))
    right))

#||
define inline function box-bottom
    (region) => (bottom :: <integer>)
  let (left, top, right, bottom) = box-edges(region);
  ignore(left, top, right);
  bottom
end function box-bottom;
||#

(defun box-bottom (region)
"
Returns the y coordinate of the bottom right corner of the bounding
box of _region_. The argument _region_ must be either a bounded region
or some other object that obeys the bounding box protocol.
"
  (multiple-value-bind (left top right bottom)
      (box-edges region)
    (declare (ignore left top right))
    bottom))

