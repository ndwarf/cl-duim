;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GEOMETRY-INTERNALS -*-
(in-package #:duim-geometry-internals)

;;; From 'boxes.lisp'

(define-protocol <<box-protocol>> ()
  (:function bounding-box (region &key into)
	     (:documentation
"
Returns the bounding box of a region. The argument _region_ must be
either a bounded region (such as a line or an ellipse) or some other
object that obeys the bounding box protocol, such as a sheet.

This function often returns an existing object, so you should not
modify the returned result.

If _into_ is supplied, it is a bounding box that might be
destructively modified to contain the result.

Defined by: <<BOX-PROTOCOL>>
"
))
  ;; XXX: this is already defined in REGIONS.LISP. Maybe we need to move
  ;; these protocols out into their own file?
  (:function box-edges (region)
	     (:documentation
"
Returns the bounding box of _region_ as four integers specifying the x
and y coordinates of the top left point and the x and y coordinates of
the bottom right point of the box.

The argument _region_ must be either a bounded region (such as a line
or an ellipse) or some other object that obeys the bounding box
protocol, such as a sheet.

The four returned values left, top, right, and bottom will satisfy the
inequalities

    left <= right
    top <= bottom

Defined by: <<BOX-PROTOCOL>>
"))
  (:function set-box-edges (region left top right bottom)
	     (:documentation
"
Sets the edges of a box and returns the bounding box _box_. This might
destructively modify _box_ or it might not, depending on what class
_box_ is.

Defined by: <<BOX-PROTOCOL>>
"))
  (:function box-position (region)
	     (:documentation
"
Returns the position of the bounding box of _region_ as two
values. The position of a bounding box is specified by its top left
point.

Defined by: <<BOX-PROTOCOL>>
"))
  (:function set-box-position (region x y)
	     (:documentation
"
Sets the position of the bounding box _box_ and might or might not
modify the box.

Defined by: <<BOX-PROTOCOL>>
"))
  (:function box-size (region)
	     (:documentation
"
Returns the width and height of the bounding box of _region_ as two
values. The argument _region_ must be either a bounded region or some
other object that obeys the bounding box protocol, such as a sheet.

Defined by: <<BOX-PROTOCOL>>
"))
  (:function set-box-size (region width height)
	     (:documentation
"
Sets the size (width and height) of the bounding box _box_.

Defined by: <<BOX-PROTOCOL>>
"))
  (:function box-width (region)
	     (:documentation
"
Returns the width of the bounding box _region_. The width of a
bounding box is the difference between its maximum x
coordinate (right) and its minimum x coordinate (left). The argument
_region_ must be either a bounded region or some other object that
obeys the bounding box protocol, such as a sheet.

Defined by: <<BOX-PROTOCOL>>
"))
  (:function box-height (region)
	     (:documentation
"
Returns the height of the bounding box _region_. The height of a
bounding box is the difference between the maximum y coordinate and
its minimum y coordinate. The argument _region_ must be either a
bounded region or some other object that obeys the bounding box
protocol.

Defined by: <<BOX-PROTOCOL>>
")))


;;; From 'regions.lisp'

(define-protocol <<region-protocol>> ()
  (:function region-empty? (region)
	     (:documentation
"
Returns t if the region is empty, otherwise returns nil.

Defined by: <<REGION-PROTOCOL>>
"))
  (:function region-equal (region1 region2)
	     (:documentation
"

Returns t if the two regions _region1_ and _region2_ contain exactly
the same set of points, otherwise returns nil. There is a method on =
on <region> and <region> that calls 'region-equal'.

Defined by: <<REGION-PROTOCOL>>
"))
  (:function region-contains-position? (region x y)
	     (:documentation
"
Returns t if the point at _x_, _y_ is contained in the region
_region_, otherwise returns nil. Since regions in DUIM are closed,
this returns t if the point at _x_, _y_ is on the region's boundary.

Defined by: <<REGION-PROTOCOL>>
"))
  (:function region-contains-region? (region1 region2)
	     (:documentation
"
Returns t if all points in the region _region2_ are members of the
region _region1_, otherwise returns nil. 'region-contains-position?'
is a special case of 'region-contains-region?' in which the region is
the point x,y.

Defined by: <<REGION-PROTOCOL>>
"))
  (:function region-intersects-region? (region1 region2)
	     (:documentation
"
Returns nil if the 'region-intersection' of the two regions _region1_
and _region2_ would be $nowhere (that is, they do not intersect),
otherwise returns t.

Defined by: <<REGION-PROTOCOL>>
"))
  (:function region-union (region1 region2)
	     (:documentation
"
Returns a region that contains all points that are in either of the
regions _region1_ or _region2_ (possibly with some points removed in
order to satisfy the dimensionality rule).

The result of 'region-union' always has dimensionality that is the
maximum dimensionality of _region1_ and _region2_. For example, the
union of a path and an area produces an area; the union of two paths
is a path.

Note: 'region-union' may return either a simple region or a region
set.

Defined by: <<REGION-PROTOCOL>>
"))
  (:function region-intersection (region1 region2)
	     (:documentation
"
Returns a region that contains all points that are in both of the
regions _region1_ and _region2_ (possibly with some points removed in
order to satisfy the dimensionality rule).

The result of 'region-intersection' has dimensionality that is the
minimum dimensionality of _region1_ and _region2_, or is $nowhere. For
example, the intersection of two areas is either another area or
$nowhere; the intersection of two paths is either another path or
$nowhere; the intersection of a path and an area produces the path
clipped to stay inside of the area.

Note: 'region-intersection' may return either a simple region or a
region set.

Defined by: <<REGION-PROTOCOL>>
"))
  (:function region-difference (region1 region2)
	     (:documentation
"
Returns a region that contains all points in the region _region1_ that
are not in the region _region2_ (possibly plus additional boundary
points to make the result closed).

The result of 'region-difference' has the same dimensionality as
_region1_, or is $nowhere. For example, the difference of an area and
a path produces the same area; the difference of a path and an area
produces the path clipped to stay outside of the area.

Note: 'region-difference' may return either a simple region or a
region set.

Defined by: <<REGION-PROTOCOL>>
"))
  ;; Compound regions
  (:function do-regions (function region &key normalize?)
	     (:documentation
"
Calls _function_ on each region in the region set _region_. This is
often more efficient than calling 'region-set-regions'. _function_ is
a function of one argument, a region. _region_ can be either a region
set or a simple region, in which case _function_ is called once on
_region_ itself. If _normalize?_ is supplied, it must be
either :x-banding or :y-banding. If it is :x-banding and all the
regions in _region_ are axis-aligned rectangles, the result is
normalized by merging adjacent rectangles with banding done in the x
direction. If it is :y-banding and all the regions in _region_ are
rectangles, the result is normalized with banding done in the y
direction. Normalizing a region set that is not composed entirely of
axis-aligned rectangles using x- or y-banding causes DUIM to signal
the <region-set-not-rectangular> error.

Defined by: <<REGION-PROTOCOL>>
"))
  (:function region-set-function (region)
	     (:documentation
"
Returns the function that composed the region, one of 'intersection',
'union', or 'difference'.

Defined by: <<REGION-PROTOCOL>>
"))
  (:function region-set-regions (region &key normalize?)
	     (:documentation
"
Returns a sequence of the regions in the region set _region_. _region_
can be either a region set or a simple region, in which case the
result is simply a sequence of one element: region.

For the case of region sets that are unions of axis-aligned
rectangles, the rectangles returned by 'region-set-regions' are
guaranteed not to overlap. If _normalize?_ is supplied it must be
either :x-banding or :y-banding. If it is :x-banding and all the
regions in _region_ are axis-aligned rectangles, the result is
normalized by merging adjacent rectangles with banding done in the x
direction. If it is :y-banding and all the regions in _region_ are
rectangles, the result is normalized with banding done in the y
direction.

Normalizing a region set that is not composed entirely of axis-aligned
rectangles using x- or y-banding causes DUIM to signal the
<region-set-not-rectangular> error.

Defined by: <<REGION-PROTOCOL>>
"))
  ;; Regions meet transforms
  (:function transform-region (transform region)
	     (:documentation
"
Applies _transform_ to the region _region_, and returns the
transformed region.

Defined by: <<REGION-PROTOCOL>>
"))
  (:function untransform-region (transform region)
	     (:documentation
"
Undoes the previous transformation on the region _region_, returning
the original region. This is exactly equivalent to:

    (transform-region (invert-transform _transform_) region)

Signals <singular-transform> if _transform_ cannot be inverted.

Defined by: <<REGION-PROTOCOL>>
"))
  (:function transform-region! (transform region) (:documentation " Defined by: <<REGION-PROTOCOL>> "))
  (:function untransform-region! (transform region) (:documentation " Defined by: <<REGION-PROTOCOL>> ")))


(define-protocol <<point-protocol>> (<<region-protocol>>)
  (:function point-position (point)
	     (:documentation
"
Returns both the x and y coordinates of the point _point_ as two
values.

Defined by: <<POINT-PROTOCOL>>
"))
  (:getter point-x (point)
	   (:documentation
"
Returns the x coordinate of _point_.

Defined by: <<POINT-PROTOCOL>>
"))
  (:getter point-y (point)
	   (:documentation
"
Returns the y coordinate of _point_.

Defined by: <<POINT-PROTOCOL>>
")))


;;; From 'transforms.lisp'

(define-protocol <<transform-protocol>> ()
  (:function transform-components (transform) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function transform-coordinate-sequence (transform coordinates &key copy?) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function make-transform (mxx mxy myx myy tx ty)
	     (:documentation
"
Returns a general transform whose effect is:

    x' = MxxX + MxyY + Tx
    y' = MyxX + MyyY + Ty

where X and Y are the coordinates of a point before the transform and
x' and y' are the coordinates of the corresponding point after.

All of the arguments to 'make-transform' must be real numbers.

This is a convenient shorthand for (make-instance '<transform> ...).

Defined by: <<TRANSFORM-PROTOCOL>>
"))
  (:function make-translation-transform (tx ty)
	     (:documentation
"
Returns a transform that translates all points by _dx_ in the x
direction and _dy_ in the y direction.

The argument _dx_ represents the delta by which to translate the x
coordinate.

The argument _dy_ represents the delta by which to translate the y
coordinate.

A translation is a transform that preserves length, angle, and
orientation of all geometric entities.

Defined by: <<TRANSFORM-PROTOCOL>>
"))
  (:function make-scaling-transform (mx my &key origin-x origin-y)
	     (:documentation
"
Returns a transform that multiplies the x-coordinate distance of every
point from _origin_ by _scale-x_ and the y-coordinate distance of
every point from origin by _scale-y_.

The argument _scale-x_ represents the scaling factor for the x
direction.

The argument _scale-y_ represents the scaling factor for the y
direction.

The arguments _origin-x_ and _origin-y_ represent the point around
which scaling is performed. The default is to scale around the origin.

There is no single definition of a scaling transformation. Transforms
that preserve all angles and multiply all lengths by the same
factor (preserving the shape of all entities) are certainly scaling
transformations. However, scaling is also used to refer to transforms
that scale distances in the x direction by one amount and distances in
the y direction by another amount.

The function 'make-scaling-transform*' is identical to
'make-scaling-transform', except that it passes composite objects,
rather than separate coordinates, in its arguments. You should be
aware that using this function may lead to a loss of performance.

Defined by: <<TRANSFORM-PROTOCOL>>
"))
  (:function make-scaling-transform* (mx my &key origin) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function make-rotation-transform (angle &key origin-x origin-y)
	     (:documentation
"
Returns a transform that rotates all points by _angle_ around the
point specified by coordinates _origin-x_ and _origin-y_ or the point
object _origin_. The angle must be expressed in radians.

A rotation is a transform that preserves length and angles of all
geometric entities. Rotations also preserve one point (the origin) and
the distance of all entities from that point.

The function 'make-rotation-transform*' is identical to
'make-rotation-transform', except that it passes composite objects,
rather than separate coordinates, in its arguments. You should be
aware that using this function may lead to a loss of performance.

Defined by: <<TRANSFORM-PROTOCOL>>
"))
  (:function make-rotation-transform* (angle &key origin) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function make-reflection-transform (x1 y1 x2 y2)
	     (:documentation
"
Returns a transform that reflects every point through the line passing
through the positions _x1_, _y1_ and _x2_, _y2_ or through the points
_point1_ and _point2_.

The arguments _x1_ and _y1_ represent the coordinates of the first
point of reflection. The arguments _x2_ and _y2_ represent the
coordinates of the second point of reflection.

A reflection is a transform that preserves lengths and magnitudes of
angles, but changes the sign (or handedness) of angles. If you think
of the drawing plane on a transparent sheet of paper, a reflection is
a transformation that turns the paper over.

The function 'make-reflection-transform*' is identical to
'make-reflection-transform', except that it passes composite objects,
rather than separate coordinates, in its arguments. You should be
aware that using this function may lead to a loss of performance.

Defined by: <<TRANSFORM-PROTOCOL>>
"))
  (:function make-reflection-transform* (point-1 point-2) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  ;; Predicates
  (:function transform-equal (transform1 transform2) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function identity-transform? (transform)
	     (:documentation
"
Returns t if the transform _transform_ is equal (in the sense of
'transform-equal') to the identity transform, otherwise returns nil.

Defined by: <<TRANSFORM-PROTOCOL>>
"))
  (:function translation-transform? (transform)
	     (:documentation
"
Returns t if the transform _transform_ is a pure translation, that is,
a transform such that there are two distance components transform _dx_
and _dy_ and every point (x,y) is moved to (x+dx, y+dy). Otherwise,
'translation-transform?' returns nil.

Defined by: <<TRANSFORM-PROTOCOL>>
"))
  (:function invertible-transform? (transform)
	     (:documentation
"
Returns t if the transform _transform_ has an inverse, otherwise
returns nil.

Defined by: <<TRANSFORM-PROTOCOL>>
"))
  (:function reflection-transform? (transform)
	     (:documentation
"
Returns t if the transform _transform_ inverts the handedness of the
coordinate system, otherwise returns nil.

Note that this is a very inclusive category -- transforms are
considered reflections even if they distort, scale, or skew the
coordinate system, as long as they invert the handedness.

Defined by: <<TRANSFORM-PROTOCOL>>
"))
  (:function rigid-transform? (transform)
	     (:documentation
"
Returns t if the _transform_ transforms the coordinate system as a
rigid object, that is, as a combination of translations, rotations,
and pure reflections. Otherwise, it returns nil.

Rigid transforms are the most general category of transforms that
preserve magnitudes of all lengths and angles.

Defined by: <<TRANSFORM-PROTOCOL>>
"))
  (:function even-scaling-transform? (transform)
	     (:documentation
"
Returns T if the transform _transform_ multiplies all x lengths and y
lengths by the same magnitude, otherwise returns
nil. 'even-scaling-transform?' includes pure reflections through
vertical and horizontal lines.

Defined by: <<TRANSFORM-PROTOCOL>>
"))
  (:function scaling-transform? (transform)
	     (:documentation
"
Returns t if the transform _transform_ multiplies all x lengths by one
magnitude and all y lengths by another magnitude, otherwise returns
nil. This category includes even scalings as a subset.

Defined by: <<TRANSFORM-PROTOCOL>>
"))
  (:function rectilinear-transform? (transform)
	     (:documentation
"
Returns t if the transform _transform_ always transforms any
axis-aligned rectangle into another axis-aligned rectangle, otherwise
returns nil.

This category includes scalings as a subset, and also includes 90
degree rotations.

Rectilinear transforms are the most general category of transforms for
which the bounding rectangle of a transformed object can be found by
transforming the bounding rectangle of the original object.

Defined by: <<TRANSFORM-PROTOCOL>>
"))
  ;; Composition
  (:function compose-transforms (transform1 transform2)
	     (:documentation
"
Returns a transform that is the mathematical composition of its
arguments. Composition is in right-to-left order, that is, the
resulting transform represents the effects of applying the transform
_transform2_ followed by the transform _transform1_.

Defined by: <<TRANSFORM-PROTOCOL>>
"))
  (:function compose-translation-with-transform (transform tx ty)
	     (:documentation
"
Creates a new transform by composing the transform _transform_ with
the given translation. The order of composition is that the
translation transform is applied first, followed by the argument
_transform_.

The argument _dx_ represents the delta by which to translate the x
coordinate.

The argument _dy_ represents the delta by which to translate the y
coordinate.

Note that this function could be implemented by using
'make-translation-transform' and 'compose-transforms'. It is provided
because it is common to build up a transform as a series of simple
transforms.

Defined by: <<TRANSFORM-PROTOCOL>>
"))
  (:function compose-transform-with-translation (transform tx ty)
	     (:documentation
"
Creates a new transform by composing a given translation with the
transform _transform_. The order of composition is _transform_ first,
followed by the translation transform.

The argument _dx_ represents the delta by which to translate the x
coordinate.

The argument _dy_ represents the delta by which to translate the y
coordinate.

Note that this function could be implemented by using
'make-translation-transform' and 'compose-transforms'. It is provided
because it is common to build up a transform as a series of simple
transforms.

Defined by: <<TRANSFORM-PROTOCOL>>
"))
  (:function compose-scaling-with-transform (transform mx my &key origin)
	     (:documentation
"
Creates a new transform by composing the transform _transform_ with
the given scaling. The order of composition is that the scaling
transform is applied first, followed by the argument _transform_.

The argument _scale-x_ represents the scaling factor for the x
direction.

The argument _scale-y_ represents the scaling factor for the y
direction.

The argument _origin_ represents the point around which scaling is
performed. The default is to scale around the origin.

Note that this function could be implemented by using
'make-scaling-transform' and 'compose-transforms'. It is provided
because it is common to build up a transform as a series of simple
transforms.

Defined by: <<TRANSFORM-PROTOCOL>>
"))
  (:function compose-transform-with-scaling (transform mx my &key origin)
	     (:documentation
"
Creates a new transform by composing a given scaling with the
transform _transform_. The order of composition is _transform_ first,
followed by the scaling transform.

The argument _scale-x_ represents the scaling factor for the x
direction.

The argument _scale-y_ represents the scaling factor for the y
direction.

The argument _origin_ represents the point around which scaling is
performed. The default is to scale around the origin.

Note that this function could be implemented by using
'make-scaling-transform' and 'compose-transforms'. It is provided
because it is common to build up a transform as a series of simple
transforms.

Defined by: <<TRANSFORM-PROTOCOL>>
"))
  (:function compose-rotation-with-transform (transform angle &key origin)
	     (:documentation
"
Creates a new transform by composing the transform _transform_ with
the given rotation. The order of composition is that the rotation
transform is applied first, followed by the argument _transform_.

Note that this function could be implemented by using
'make-rotation-transform' and 'compose-transforms'. It is provided
because it is common to build up a transform as a series of simple
transforms.

Defined by: <<TRANSFORM-PROTOCOL>>
"))
  (:function compose-transform-with-rotation (transform angle &key origin)
	     (:documentation
"
Creates a new transform by composing a given rotation with the
transform _transform_. The order of composition is _transform_ first,
followed by the rotation transform.

The argument _angle_ represents the angle by which to rotate, in
radians.

The argument _origin_ represents the point about which to rotate. The
default is to rotate around (0,0).

Note that this function could be implemented by using
'make-rotation-transform' and 'compose-transforms'. It is provided
because it is common to build up a transform as a series of simple
transforms.

Defined by: <<TRANSFORM-PROTOCOL>>
"))
  (:function invert-transform (transform)
	     (:documentation
"
Returns a transform that is the inverse of the transform
_transform_. The result of composing a transform with its inverse is
equal to the identity transform.

If _transform_ is singular, 'invert-transform' signals the
<singular-transform> error.

Note: With finite-precision arithmetic there are several low-level
conditions that might occur during the attempt to invert a singular or
almost singular transform. (These include comutation of a zero
determinant, floating-point underflow during computation of the
determinant, or float-point overflow during subsequent
multiplication.) 'invert-transform' signals the <singular-transform>
error for all of these cases.

Defined by: <<TRANSFORM-PROTOCOL>>
"))
  ;; Simple transformations
  (:function transform-position (transform x y)
	     (:documentation
"
Applies the transform _transform_ to the point whose coordinates are
_x_ and _y_. 'transform-position' is the spread version of
'transform-region' in the case where the region is a point.

Defined by: <<TRANSFORM-PROTOCOL>>
"))
  (:function untransform-position (transform x y)
	     (:documentation
"
Undoes the previous transformation on the point _x_,_y_, returning the
original point. This is exactly equivalent to:

    (transform-position (invert-transform _transform_) x y)

Signals <singular-transform> if _transform_ cannot be inverted.

Defined by: <<TRANSFORM-PROTOCOL>>
"))
  (:function transform-distance (transform dx dy)
	     (:documentation
"
Applies the transform _transform_ to the distance represented by _dx_
and _dy_, and returns the transformed _dx_ and _dy_. A distance
represents the difference between two points. It does not transform
like a point.

Defined by: <<TRANSFORM-PROTOCOL>>
"))
  (:function untransform-distance (transform dx dy)
	     (:documentation
"
Undoes the previous transformation on the distance _dx_, _dy_,
returning the original dx, dy. This is exactly equivalent to:

    (transform-distance (invert-transform _transform_) dx dy)

Signals <singular-transform> if _transform_ cannot be inverted.

Defined by: <<TRANSFORM-PROTOCOL>>
"))
  (:function transform-angles (transform start-angle end-angle)
	     (:documentation
"
Applies the transform _transform_ to the angles _start-angle_ and
_end-angle_ of an object, and returns the transformed angles.

Defined by: <<TRANSFORM-PROTOCOL>>
"))
  (:function untransform-angles (transform start-angle end-angle)
	     (:documentation
"
Undoes the transform _transform_ to the angles _new-start_ and
_new-end_, returning the original _orig-start_ and _orig-end_. This is
exactly equivalent to:

    (transform-angles (invert-transform _transform_)
                      start-angle
                      end-angle)

Signals <singular-transform> if _transform_ cannot be inverted.

Defined by: <<TRANSFORM-PROTOCOL>>
"))
  ;; Boxes, by definition, have integer coordinates
  (:function transform-box (transform x1 y1 x2 y2)
	     (:documentation
"
Applies the transform _transform_ to the rectangle specified by the
four coordinate arguments. 'transform-box' is the spread version of
'transform-region' in the case where the transform is rectilinear and
the region is a rectangle.

The arguments _x1_, _y1_, _x2_, and _y2_ are canonicalized and the
four return values specify the minimum and maximum points of the
transformed rectangle in the order left, top, right, and bottom.

An error is signalled if _transform_ does not satisfy
'rectilinear-transform?'.

Defined by: <<TRANSFORM-PROTOCOL>>
"))
  (:function untransform-box (transform x1 y1 x2 y2)
	     (:documentation
"
Undoes the previous transformation on the rectangle _top-left-x_,
_top-left-y_, _bottom-right-x_, _bottom-right-y_, returning the
original box. This is exactly equivalent to:

    (transform-box (invert-transform _transform_) x1 y1 x2 y2)

Signals <singular-transform> if _transform_ cannot be inverted.

Defined by: <<TRANSFORM-PROTOCOL>>
")))


