;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GEOMETRY-INTERNALS -*-
(in-package #:duim-geometry-internals)

#||
/// Basic protocol classes

/// Regions

define protocol-class region (<object>) end;
define protocol-class region-set (<region>) end;
||#

(define-protocol-class region () ()
  (:documentation
"
The class that corresponds to a set of points. The <region> class
includes both bounded and unbounded regions.

There is no 'make-instance' method for <region> because of the
impossibility of a uniform way to specify the arguments to such a
function.
"))

;;; TODO: also 'region?'
#||
Returns t if _object_ is a region, otherwise returns nil.
||#

(define-protocol-class region-set (<region>) ()
  (:documentation
"
The class that represents a region set; a subclass of <region>.
"))

;;; TODO: also for 'region-set?'
#||
Returns t if _object_ is a region set, otherwise returns nil.
||#

#||
/// Points, paths, and areas

define protocol-class point (<region>) end;
define protocol-class path (<region>) end;
define protocol-class area (<region>) end;
||#

(define-protocol-class point (<region>) ()
  (:documentation
"
The class that corresponds to a mathematical point. <point> is a
subclass of <region>. The :x and :y initargs correspond to the x and y
coordinates respectively.
"))

;;; TODO: Also the predicate 'point?'
#||
Returns t if _object_ is a point.
||#

(define-protocol-class path (<region>) ()
  (:documentation
"
The class <path> denotes bounded regions that have dimensionality
1 (that is, have length).

<path> is a subclass of <region>.

Constructing a <path> object with no length (via 'make-line*', for
example) may canonicalize it to $nowhere.
"))

;;; TODO: Also for the predicate 'path?'
#||
Returns t if _object_ is a path, otherwise returns nil.
||#

(define-protocol-class area (<region>) ()
  (:documentation
"
The class <area> denotes bounded regions that have dimensionality
2 (that is, have area). <area> is a subclass of <region>.

Note that constructing an area object with no area (such as calling
'make-rectangle' with two coincident points, for example) may
canonicalise it to +nowhere+.
"))

;;; TODO: Also doc for 'area?'
#||
Returns T if _object_ is an area, otherwise returns nil.
||#

#||
/// Transforms

define protocol-class transform (<object>) end;
||#

(define-protocol-class transform () ()
  (:documentation
"
The superclass of all transforms. There are one or more subclasses of
<transform> with implementation-dependent names that implement
transforms. The exact names of these classes is explicitly
unspecified.

All of the instantiable transformation classes provided by DUIM are
immutable.
"))

;;; TODO: also documentation for 'transform?'
#||
Returns t if _object_ is a transform, otherwise returns nil.
||#

#||
/// Bounding boxes

// Note well that bounding boxes are not the same thing as rectangles!
define protocol-class bounding-box (<region>) end;
||#

(define-protocol-class bounding-box (<region>) ()
  (:documentation
"
A bounding box is an axis aligned rectangle that contains some
region. The representation of bounding boxes in DUIM is chosen to be
efficient. This representation is not sufficient to represent the
result of arbitrary transformations (such as rotations) of bounding
boxes. The most general class of transformations that is guaranteed to
transform a box into another box is the class of transformations that
satisfy 'rectilinear-transformation?'.

Bounding boxes are immutable, but since they reflect the live state of
such mutable objects as sheets, bounding boxes are
volatile. Therefore, programmers must not depend on the bounding box
associated with a mutable object remaining constant.
"))

;;; TODO: Ditto 'bounding-box?'
#||
Returns T if _object_ is a bounding box (that is, supports the
bounding box protocol), otherwise returns nil.
||#
