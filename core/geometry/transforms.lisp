;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GEOMETRY-INTERNALS -*-

(in-package #:duim-geometry-internals)

#|| Transformation protocol

define protocol <<transform-protocol>> ()
  function transform-components
    (transform :: <transform>)
 => (mxx :: <real>, mxy :: <real>, myx :: <real>, myy :: <real>,
     tx :: <real>, ty :: <real>);
  function transform-coordinate-sequence
    (transform :: <transform>, coordinates :: <sequence>, #key copy?)
 => (coordinates :: <vector>);
  // Constructors
  function make-transform
    (mxx :: <real>, mxy :: <real>, myx :: <real>, myy :: <real>,
     tx :: <real>,  ty :: <real>) => (transform :: <transform>);
  function make-translation-transform
    (tx :: <real>, ty :: <real>) => (transform :: <transform>);
  function make-scaling-transform
    (mx :: <real>, my :: <real>, #key origin-x, origin-y)
 => (transform :: <transform>);
  function make-scaling-transform*
    (mx :: <real>, my :: <real>, #key origin) => (transform :: <transform>);
  function make-rotation-transform
    (angle :: <real>, #key origin-x, origin-y) => (transform :: <transform>);
  function make-rotation-transform*
    (angle :: <real>, #key origin) => (transform :: <transform>);
  function make-reflection-transform
    (x1 :: <real>, y1 :: <real>, x2 :: <real>, y2 :: <real>)
 => (transform :: <transform>);
  function make-reflection-transform*
    (point-1 :: <point>, point-2 :: <point>) => (transform :: <transform>);
  // Predicates
  function transform-equal
    (transform1 :: <transform>, transform2 :: <transform>)
 => (true? :: <boolean>);
  function identity-transform?
    (transform :: <transform>) => (true? :: <boolean>);
  function translation-transform?
    (transform :: <transform>) => (true? :: <boolean>);
  function invertible-transform?
    (transform :: <transform>) => (true? :: <boolean>);
  function reflection-transform?
    (transform :: <transform>) => (true? :: <boolean>);
  function rigid-transform?
    (transform :: <transform>) => (true? :: <boolean>);
  function even-scaling-transform?
    (transform :: <transform>) => (true? :: <boolean>);
  function scaling-transform?
    (transform :: <transform>) => (true? :: <boolean>);
  function rectilinear-transform?
    (transform :: <transform>) => (true? :: <boolean>);
  // Composition
  function compose-transforms
    (transform1 :: <transform>, transform2 :: <transform>)
 => (transform :: <transform>);
  function compose-translation-with-transform
    (transform :: <transform>, tx :: <real>, ty :: <real>)
 => (transform :: <transform>);
  function compose-transform-with-translation
    (transform :: <transform>, tx :: <real>, ty :: <real>)
 => (transform :: <transform>);
  function compose-scaling-with-transform
    (transform :: <transform>, mx :: <real>, my :: <real>, #key origin)
 => (transform :: <transform>);
  function compose-transform-with-scaling
    (transform :: <transform>, mx :: <real>, my :: <real>, #key origin)
 => (transform :: <transform>);
  function compose-rotation-with-transform
    (transform :: <transform>, angle :: <real>, #key origin)
 => (transform :: <transform>);
  function compose-transform-with-rotation
    (transform :: <transform>, angle :: <real>, #key origin)
 => (transform :: <transform>);
  function invert-transform
    (transform :: <transform>) => (transform :: <transform>);
  // Simple transformations
  function transform-position
    (transform :: <transform>, x :: <real>, y :: <real>)
 => (x :: <real>, y :: <real>);
  function untransform-position
    (transform :: <transform>, x :: <real>, y :: <real>)
 => (x :: <real>, y :: <real>);
  function transform-distance
    (transform :: <transform>, dx :: <real>, dy :: <real>)
 => (dx :: <real>, dy :: <real>);
  function untransform-distance
    (transform :: <transform>, dx :: <real>, dy :: <real>)
 => (dx :: <real>, dy :: <real>);
  function transform-angles
    (transform :: <transform>, start-angle :: <real>, end-angle :: <real>)
 => (start-angle :: <real>, end-angle :: <real>);
  function untransform-angles
    (transform :: <transform>, start-angle :: <real>, end-angle :: <real>)
 => (start-angle :: <real>, end-angle :: <real>);
  // Boxes, by definition, have integer coordinates
  function transform-box
    (transform :: <transform>, x1 :: <integer>, y1 :: <integer>, x2 :: <integer>, y2 :: <integer>)
 => (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>);
  function untransform-box
    (transform :: <transform>, x1 :: <integer>, y1 :: <integer>, x2 :: <integer>, y2 :: <integer>)
=> (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>);
end protocol <<transform-protocol>>;
||#
#||
(define-protocol <<transform-protocol>> ()
  (:function transform-components (transform) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function transform-coordinate-sequence (transform coordinates &key copy?) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function make-transform (mxx mxy myx myy tx ty) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function make-translation-transform (tx ty) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function make-scaling-transform (mx my &key origin-x origin-y) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function make-scaling-transform* (mx my &key origin) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function make-rotation-transform (angle &key origin-x origin-y) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function make-rotation-transform* (angle &key origin) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function make-reflection-transform (x1 y1 x2 y2) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function make-reflection-transform* (point-1 point-2) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  ;; Predicates
  (:function transform-equal (transform1 transform2) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function identity-transform? (transform) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function translation-transform? (transform) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function invertible-transform? (transform) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function reflection-transform? (transform) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function rigid-transform? (transform) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function even-scaling-transform? (transform) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function scaling-transform? (transform) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function rectilinear-transform? (transform) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  ;; Composition
  (:function compose-transforms (transform1 transform2) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function compose-translation-with-transform (transform tx ty) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function compose-transform-with-translation (transform tx ty) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function compose-scaling-with-transform (transform mx my &key origin) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function compose-transform-with-scaling (transform mx my &key origin) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function compose-rotation-with-transform (transform angle &key origin) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function compose-transform-with-rotation (transform angle &key origin) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function invert-transform (transform) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  ;; Simple transformations
  (:function transform-position (transform x y) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function untransform-position (transform x y) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function transform-distance (transform dx dy) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function untransform-distance (transform dx dy) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function transform-angles (transform start-angle end-angle) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function untransform-angles (transform start-angle end-angle) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  ;; Boxes, by definition, have integer coordinates
  (:function transform-box (transform x1 y1 x2 y2) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> "))
  (:function untransform-box (transform x1 y1 x2 y2) (:documentation " Defined by: <<TRANSFORM-PROTOCOL>> ")))
||#

#||
/// The identity transform

define sealed class <identity-transform> (<transform>)
end class <identity-transform>;

define sealed domain make (singleton(<identity-transform>));
define sealed domain initialize (<identity-transform>);
||#

(defclass <identity-transform> (<transform>) ())


#||
define sealed inline method transform-components
    (transform :: <identity-transform>)
 => (mxx :: <integer>, mxy :: <integer>, myx :: <integer>, myy :: <integer>,
     tx :: <integer>, ty :: <integer>);
  values(1, 0, 0, 1, 0, 0)
end method transform-components;
||#

(defmethod transform-components ((transform <identity-transform>))
  (declare (ignore transform))
  (values 1 0 0 1 0 0))


#||
define constant $identity-transform :: <identity-transform> = make(<identity-transform>);
||#

(defvar *identity-transform* (make-instance '<identity-transform>)
"
An instance of a transform that is guaranteed to be an identity
transform, that is, the transform that does nothing.
")


#||
/// Translation transformations

define open abstract class <translation-transform> (<transform>)
  sealed slot %inverse :: false-or(<translation-transform>) = #f;
end class <translation-transform>;
||#

(defclass <translation-transform> (<transform>)
  ((%inverse :type (or null <translation-transform>) :initform nil :accessor %inverse))
  (:metaclass <abstract-metaclass>))


#||
/// Float translation transformations

define sealed class <float-translation-transform> (<translation-transform>)
  sealed slot %tx :: <single-float>,
    required-init-keyword: tx:;
  sealed slot %ty :: <single-float>,
    required-init-keyword: ty:;
end class <float-translation-transform>;

define sealed domain make (singleton(<float-translation-transform>));
define sealed domain initialize (<float-translation-transform>);
||#

(defclass <float-translation-transform>
    (<translation-transform>)
  ((%tx :type single-float :initarg :tx :initform (required-slot ":tx" "<float-translation-transform>") :accessor %tx)
   (%ty :type single-float :initarg :ty :initform (required-slot ":ty" "<float-translation-transform>") :accessor %ty)))


#||
define sealed inline method transform-components
    (transform :: <float-translation-transform>)
 => (mxx :: <single-float>, mxy :: <single-float>, myx :: <single-float>, myy :: <single-float>,
     tx :: <single-float>, ty :: <single-float>);
  values(1.0, 0.0, 0.0, 1.0, transform.%tx, transform.%ty)
end method transform-components;
||#

(defmethod transform-components ((transform <float-translation-transform>))
  (values 1.0 0.0 0.0 1.0 (%tx transform) (%ty transform)))


#||
/// Integer translation transformations

define sealed class <integer-translation-transform> (<translation-transform>)
  sealed slot %tx :: <integer>,
    required-init-keyword: tx:;
  sealed slot %ty :: <integer>,
    required-init-keyword: ty:;
end class <integer-translation-transform>;

define sealed domain make (singleton(<integer-translation-transform>));
define sealed domain initialize (<integer-translation-transform>);
||#

(defclass <integer-translation-transform>
    (<translation-transform>)
  ((%tx :type integer :initarg :tx :initform (required-slot ":tx" "<integer-translation-transform>") :accessor %tx)
   (%ty :type integer :initarg :ty :initform (required-slot ":ty" "<integer-translation-transform>") :accessor %ty)))


#||
define sealed inline method transform-components
    (transform :: <integer-translation-transform>)
 => (mxx :: <integer>, mxy :: <integer>, myx :: <integer>, myy :: <integer>,
     tx :: <integer>, ty :: <integer>);
  values(1, 0, 0, 1, transform.%tx, transform.%ty)
end method transform-components;
||#

(defmethod transform-components ((transform <integer-translation-transform>))
  (values 1 0 0 1 (%tx transform) (%ty transform)))



#||
/// Conditions

define open abstract class <transform-error> (<error>)
end class <transform-error>;
||#

;;; :todo: when should this get signalled?

(define-condition <transform-error> (error)
  ()
  (:report (lambda (condition stream)
             (format stream "<transform-error>, oops [~a]" condition)))
  (:documentation
"
The class that is the superclass of three error conditions,
<transform-underspecified>, <reflection-underspecified>, and
<singular-transform>.
"))


#||
define sealed class <singular-transform> (<transform-error>)
  sealed constant slot %transform,
    required-init-keyword: transform:;
end class <singular-transform>;

define method condition-to-string
    (condition :: <singular-transform>) => (string :: <string>)
  format-to-string("The transformation %= is singular", condition.%transform)
end method condition-to-string;
||#

;;; :todo: when should this get signalled?

(define-condition <singular-transform> (<transform-error>)
  ((%transform :initarg :transform :accessor %transform))
  (:report (lambda (condition stream)
             (format stream
		     "The transformation ~a is singular"
		     (%transform condition))))
  (:documentation
"
The error that is signalled when 'invert-transform' is called on a
singular transform, that is, a transform that has no inverse.

This condition handles the :transform initarg, which is used to supply
the transform that is singular.
"))



#||
/// Constructors

define sealed method make-translation-transform
    (tx :: <real>, ty :: <real>) => (transform :: <transform>)
  case
    zero?(tx) & zero?(ty) =>
      $identity-transform;
    integral?(tx) & integral?(ty) =>
      make(<integer-translation-transform>,
           tx: truncate(tx), ty: truncate(ty));
    otherwise =>
      make(<float-translation-transform>, 
	   tx: as(<single-float>, tx), ty: as(<single-float>, ty))
  end
end method make-translation-transform;
||#

(defmethod make-translation-transform ((tx real) (ty real))
  (cond
    ((and (zerop tx) (zerop ty))
     *identity-transform*)
    ((and (typep tx 'integer) (typep ty 'integer))
     (make-instance '<integer-translation-transform>
                    :tx tx
                    :ty ty))
    (t
     (make-instance '<float-translation-transform>
                    :tx (coerce tx 'single-float)
                    :ty (coerce ty 'single-float)))))


#||
define sealed inline method make 
    (class == <translation-transform>, #key tx = 0, ty = 0)
 => (transform :: <transform>)
  make-translation-transform(tx, ty)
end method make;
||#



#||
/// Predicates

define method \=
    (tr1 :: <transform>, tr2 :: <transform>) => (true? :: <boolean>)
  tr1 == tr2
  | transform-equal(tr1, tr2)
end method \=;
||#

(defmethod equal? ((tr1 <transform>) (tr2 <transform>))
  (or (eql tr1 tr2)
      (transform-equal tr1 tr2)))


#||
define sealed method transform-equal
    (tr1 :: <identity-transform>, tr2 :: <identity-transform>) => (true? :: <boolean>)
  #t
end method transform-equal;
||#

(defmethod transform-equal ((tr1 <identity-transform>) (tr2 <identity-transform>))
  t)


#||
define sealed method transform-equal
    (tr1 :: <translation-transform>, tr2 :: <translation-transform>) => (true? :: <boolean>)
  // NB: the translations can be integers or single floats here
    tr1.%tx = tr2.%tx
  & tr1.%ty = tr2.%ty
end method transform-equal;
||#

(defmethod transform-equal ((tr1 <translation-transform>) (tr2 <translation-transform>))
  ;; NB: the translations can be integers or single floats here
  (and (= (%tx tr1) (%tx tr2))
       (= (%ty tr1) (%ty tr2))))


#||
define method transform-equal
    (tr1 :: <transform>, tr2 :: <transform>) => (true? :: <boolean>)
  let (mxx1, mxy1, myx1, myy1, tx1, ty1) = transform-components(tr1);
  let (mxx2, mxy2, myx2, myy2, tx2, ty2) = transform-components(tr2);
  mxx1 = mxx2
  & mxy1 = mxy2
  & myx1 = myx2
  & myy1 = myy2
  & tx1 = tx2
  & ty1 = ty2
end method transform-equal;
||#

(defmethod transform-equal ((tr1 <transform>) (tr2 <transform>))
  (multiple-value-bind (mxx1 mxy1 myx1 myy1 tx1 ty1)
      (transform-components tr1)
    (multiple-value-bind (mxx2 mxy2 myx2 myy2 tx2 ty2)
	(transform-components tr2)
      (and (= mxx1 mxx2)
	   (= mxy1 mxy2)
	   (= myx1 myx2)
	   (= myy1 myy2)
	   (= tx1 tx2)
	   (= ty1 ty2)))))


#||
// Identity transform?
define sealed method identity-transform?
    (transform :: <identity-transform>) => (true? :: <boolean>)
  #t
end method identity-transform?;
||#

(defmethod identity-transform? ((transform <identity-transform>))
  t)


#||
define sealed method identity-transform?
    (transform :: <translation-transform>) => (true? :: <boolean>)
  #f
end method identity-transform?;
||#

(defmethod identity-transform? ((transform <translation-transform>))
  nil)


#||
// Translation transform?
define sealed method translation-transform?
    (transform :: <identity-transform>) => (true? :: <boolean>)
  #t
end method translation-transform?;
||#

(defmethod translation-transform? ((transform <identity-transform>))
  t)


#||
define sealed method translation-transform?
    (transform :: <translation-transform>) => (true? :: <boolean>)
  #t
end method translation-transform?;
||#

(defmethod translation-transform? ((transform <translation-transform>))
  t)


#||
// Invertible transform?
define sealed method invertible-transform?
    (transform :: <identity-transform>) => (true? :: <boolean>)
  #t
end method invertible-transform?;
||#

(defmethod invertible-transform? ((transform <identity-transform>))
  t)


#||
define sealed method invertible-transform?
    (transform :: <translation-transform>) => (true? :: <boolean>)
  #t
end method invertible-transform?;
||#

(defmethod invertible-transform? ((transform <translation-transform>))
  t)

#||
// Reflection transform?
define sealed method reflection-transform?
    (transform :: <identity-transform>) => (true? :: <boolean>)
  #f
end method reflection-transform?;
||#

(defmethod reflection-transform? ((transform <identity-transform>))
  nil)


#||
define sealed method reflection-transform?
    (transform :: <translation-transform>) => (true? :: <boolean>)
  #f
end method reflection-transform?;
||#

(defmethod reflection-transform? ((transform <translation-transform>))
  nil)


#||
// Rigid transform?
define sealed method rigid-transform?
    (transform :: <identity-transform>) => (true? :: <boolean>)
  #t
end method rigid-transform?;
||#

(defmethod rigid-transform? ((transform <identity-transform>))
  t)


#||
define sealed method rigid-transform?
    (transform :: <translation-transform>) => (true? :: <boolean>)
  #t
end method rigid-transform?;
||#

(defmethod rigid-transform? ((transform <translation-transform>))
  t)


#||
// Even scaling transform?
define sealed method even-scaling-transform?
    (transform :: <identity-transform>) => (true? :: <boolean>)
  #t
end method even-scaling-transform?;
||#

(defmethod even-scaling-transform? ((transform <identity-transform>))
  t)


#||
define sealed method even-scaling-transform?
    (transform :: <translation-transform>) => (true? :: <boolean>)
  #t
end method even-scaling-transform?;
||#

(defmethod even-scaling-transform? ((transform <translation-transform>))
  t)


#||
// Scaling transform?
define sealed method scaling-transform?
    (transform :: <identity-transform>) => (true? :: <boolean>)
  #t
end method scaling-transform?;
||#

(defmethod scaling-transform? ((transform <identity-transform>))
  t)


#||
define sealed method scaling-transform?
    (transform :: <translation-transform>) => (true? :: <boolean>)
  #t
end method scaling-transform?;
||#

(defmethod scaling-transform? ((transform <translation-transform>))
  t)


#||
// Rectilinear transform?
define sealed method rectilinear-transform?
    (transform :: <identity-transform>) => (true? :: <boolean>)
  #t
end method rectilinear-transform?;
||#

(defmethod rectilinear-transform? ((transform <identity-transform>))
  t)


#||
define sealed method rectilinear-transform?
    (transform :: <translation-transform>) => (true? :: <boolean>)
  #t
end method rectilinear-transform?;
||#

(defmethod rectilinear-transform? ((transform <translation-transform>))
  t)



#||
/// Inversion

define sealed method invert-transform
    (transform :: <identity-transform>) => (transform :: <transform>)
  transform
end method invert-transform;
||#

(defmethod invert-transform ((transform <identity-transform>))
  transform)


#||
define sealed method invert-transform
    (transform :: <translation-transform>) => (transform :: <transform>)
  transform.%inverse
  | begin
      // NB: the translations can be integers or single floats here, so this
      // call might make either a normal or an integer translation
      let inverse :: <translation-transform>
	= make-translation-transform(-transform.%tx, -transform.%ty);
      inverse.%inverse := transform;
      transform.%inverse := inverse;
      inverse
    end
end method invert-transform;
||#

(defmethod invert-transform ((transform <translation-transform>))
  (or (%inverse transform)
      ;; NB: the translations can be integers or single floats here, so this
      ;; call might make either a normal or an integer translation
      (let ((inverse (make-translation-transform (- (%tx transform)) (- (%ty transform)))))
        (setf (%inverse inverse)   transform
              (%inverse transform) inverse)
	inverse)))



#||
/// Composition operators

define method compose-transforms
    (tr1 :: <identity-transform>, tr2 :: <transform>) => (transform :: <transform>)
  tr2
end method compose-transforms;
||#

(defmethod compose-transforms ((tr1 <identity-transform>) (tr2 <transform>))
  tr2)


#||
define method compose-transforms
    (tr1 :: <transform>, tr2 :: <identity-transform>) => (transform :: <transform>)
  tr1
end method compose-transforms;
||#

(defmethod compose-transforms ((tr1 <transform>) (tr2 <identity-transform>))
  tr1)


#||
define method compose-transforms
    (tr1 :: <identity-transform>, tr2 :: <identity-transform>) => (transform :: <transform>)
  $identity-transform
end method compose-transforms;
||#

(defmethod compose-transforms ((tr1 <identity-transform>) (tr2 <identity-transform>))
  *identity-transform*)


#||
define method compose-transforms
    (tr1 :: <translation-transform>, tr2 :: <translation-transform>)
 => (transform :: <transform>)
  // NB: the translations can be integers or single floats here
  let tx = tr1.%tx + tr2.%tx;
  let ty = tr1.%ty + tr2.%ty;
  make-translation-transform(tx, ty)
end method compose-transforms;
||#

(defmethod compose-transforms ((tr1 <translation-transform>) (tr2 <translation-transform>))
  ;; NB: the translations can be integers or single floats here
  (let ((tx (+ (%tx tr1) (%tx tr2)))
	(ty (+ (%ty tr1) (%ty tr2))))
    (make-translation-transform tx ty)))


#||
define method compose-transforms
    (tr1 :: <integer-translation-transform>, tr2 :: <integer-translation-transform>)
 => (transform :: <transform>)
  let tx = tr1.%tx + tr2.%tx;
  let ty = tr1.%ty + tr2.%ty;
  if (zero?(tx) & zero?(ty))
    $identity-transform
  else
    make(<integer-translation-transform>, tx: tx, ty: ty)
  end
end method compose-transforms;
||#

(defmethod compose-transforms ((tr1 <integer-translation-transform>) (tr2 <integer-translation-transform>))
  (let ((tx (+ (%tx tr1) (%tx tr2)))
	(ty (+ (%ty tr1) (%ty tr2))))
    (if (and (zerop tx) (zerop ty))
	*identity-transform*
	(make-instance '<integer-translation-transform>
		       :tx tx :ty ty))))


#||
/// Translation composition operators

define sealed method compose-translation-with-transform
    (transform :: <identity-transform>, tx :: <real>, ty :: <real>)
 => (transform :: <transform>)
  make-translation-transform(tx, ty)
end method compose-translation-with-transform;
||#

(defmethod compose-translation-with-transform ((transform <identity-transform>) (tx real) (ty real))
  (make-translation-transform tx ty))


#||
define sealed method compose-translation-with-transform
    (transform :: <translation-transform>, tx :: <real>, ty :: <real>)
 => (transform :: <transform>)
  let tx = as(<single-float>, tx);
  let ty = as(<single-float>, ty);
  if (tx = 0.0 & ty = 0.0)
    transform
  else
    let tx = tx + transform.%tx;
    let ty = ty + transform.%ty;
    if (tx = 0.0 & ty = 0.0)
      $identity-transform
    else
      make-translation-transform(tx, ty)
    end
  end
end method compose-translation-with-transform;
||#

(defmethod compose-translation-with-transform ((transform <translation-transform>) (tx real) (ty real))
  (let ((tx (coerce tx 'single-float))
	(ty (coerce ty 'single-float)))
    (if (= 0.0 tx ty)
        transform
        (let ((tx (+ tx (%tx transform)))
              (ty (+ ty (%ty transform))))
          (if (= 0.0 tx ty)
              *identity-transform*
              (make-translation-transform tx ty))))))


#||
define sealed method compose-translation-with-transform
    (transform :: <integer-translation-transform>, tx :: <real>, ty :: <real>)
 => (transform :: <transform>)
  make-translation-transform(transform.%tx + tx, transform.%ty + ty)
end method compose-translation-with-transform;
||#

(defmethod compose-translation-with-transform ((transform <integer-translation-transform>) (tx real) (ty real))
  (make-translation-transform (+ (%tx transform) tx) (+ (%ty transform) ty)))


#||
define method compose-transform-with-translation
    (transform :: <transform>, tx :: <real>, ty :: <real>)
 => (transform :: <transform>)
  compose-transforms(make-translation-transform(tx, ty), transform)
end method compose-transform-with-translation;
||#

(defmethod compose-transform-with-translation ((transform <transform>) tx ty)
  (compose-transforms (make-translation-transform tx ty) transform))



#||
/// Transforming and untransforming of "spread" points

define sealed inline method transform-position
    (transform :: <identity-transform>, x :: <real>, y :: <real>)
 => (x :: <real>, y :: <real>)
  values(x, y)
end method transform-position;
||#

(defmethod transform-position ((transform <identity-transform>) (x real) (y real))
  (values x y))


#||
define sealed inline method transform-position
    (transform :: <translation-transform>, x :: <real>, y :: <real>)
 => (x :: <real>, y :: <real>)
  values(x + transform.%tx, y + transform.%ty)
end method transform-position;
||#

(defmethod transform-position ((transform <translation-transform>) (x real) (y real))
  (values (+ x (%tx transform)) (+ y (%ty transform))))


#||
define sealed inline method transform-position
    (transform :: <integer-translation-transform>, x :: <real>, y :: <real>)
 => (x :: <real>, y :: <real>)
  values(x + transform.%tx, y + transform.%ty)
end method transform-position;
||#

(defmethod transform-position ((transform <integer-translation-transform>) (x real) (y real))
  (values (+ x (%tx transform)) (+ y (%ty transform))))


#||
define sealed inline method transform-position
    (transform :: <integer-translation-transform>, x :: <integer>, y :: <integer>)
 => (x :: <integer>, y :: <integer>)
  values(x + transform.%tx, y + transform.%ty)
end method transform-position;
||#

(defmethod transform-position ((transform <integer-translation-transform>) (x integer) (y integer))
  (values (+ x (%tx transform)) (+ y (%ty transform))))


#||
define sealed inline method untransform-position
    (transform :: <identity-transform>, x :: <real>, y :: <real>)
 => (x :: <real>, y :: <real>)
  values(x, y)
end method untransform-position;
||#

(defmethod untransform-position ((transform <identity-transform>) (x real) (y real))
  (values x y))


#||
define sealed inline method untransform-position
    (transform :: <translation-transform>, x :: <real>, y :: <real>)
 => (x :: <real>, y :: <real>)
  values(x - transform.%tx, y - transform.%ty)
end method untransform-position;
||#

(defmethod untransform-position ((transform <translation-transform>) (x real) (y real))
  (values (- x (%tx transform)) (- y (%ty transform))))


#||
define sealed inline method untransform-position 
    (transform :: <integer-translation-transform>, x :: <real>, y :: <real>)
 => (x :: <real>, y :: <real>)
  values(x - transform.%tx, y - transform.%ty)
end method untransform-position;
||#

(defmethod untransform-position ((transform <integer-translation-transform>) (x real) (y real))
  (values (- x (%tx transform)) (- y (%ty transform))))


#||
define sealed inline method untransform-position
    (transform :: <integer-translation-transform>, x :: <integer>, y :: <integer>)
 => (x :: <integer>, y :: <integer>)
  values(x - transform.%tx, y - transform.%ty)
end method untransform-position;
||#

(defmethod untransform-position ((transform <integer-translation-transform>) (x integer) (y integer))
  (values (- x (%tx transform)) (- y (%ty transform))))


#||
/// Transforming and untransforming of distances

define sealed inline method transform-distance
    (transform :: <identity-transform>, dx :: <real>, dy :: <real>)
 => (x :: <real>, y :: <real>)
  values(dx, dy)
end method transform-distance;
||#

(defmethod transform-distance ((transform <identity-transform>) (dx real) (dy real))
  (values dx dy))


#||
define sealed inline method transform-distance
    (transform :: <translation-transform>, dx :: <real>, dy :: <real>)
 => (dx :: <real>, dy :: <real>)
  values(dx, dy)
end method transform-distance;
||#

(defmethod transform-distance ((transform <translation-transform>) (dx real) (dy real))
  (values dx dy))


#||
define sealed inline method transform-distance
    (transform :: <translation-transform>, dx :: <integer>, dy :: <integer>)
 => (dx :: <integer>, dy :: <integer>)
  values(dx, dy)
end method transform-distance;
||#

(defmethod transform-distance ((transform <translation-transform>) (dx integer) (dy integer))
  (values dx dy))


#||
define sealed inline method untransform-distance
    (transform :: <identity-transform>, dx :: <real>, dy :: <real>)
 => (dx :: <real>, dy :: <real>)
  values(dx, dy)
end method untransform-distance;
||#

(defmethod untransform-distance ((transform <identity-transform>) (dx real) (dy real))
  (values dx dy))


#||
define sealed inline method untransform-distance
    (transform :: <translation-transform>, dx :: <real>, dy :: <real>)
 => (dx :: <real>, dy :: <real>)
  values(dx, dy)
end method untransform-distance;
||#

(defmethod untransform-distance ((transform <translation-transform>) (dx real) (dy real))
  (values dx dy))


#||
define sealed inline method untransform-distance
    (transform :: <translation-transform>, dx :: <integer>, dy :: <integer>)
 => (dx :: <integer>, dy :: <integer>)
  values(dx, dy)
end method untransform-distance;
||#

(defmethod untransform-distance ((transform <translation-transform>) (dx integer) (dy integer))
  (values dx dy))


#||
/// Transforming and untransforming of angles

define sealed inline method transform-angles
    (transform :: <identity-transform>, start-angle :: <real>, end-angle :: <real>)
 => (start-angle :: <real>, end-angle :: <real>)
  values(start-angle, end-angle)
end method transform-angles;
||#

(defmethod transform-angles ((transform <identity-transform>) (start-angle real) (end-angle   real))
  (values start-angle end-angle))


#||
define sealed inline method transform-angles
    (transform :: <translation-transform>, start-angle :: <real>, end-angle :: <real>)
 => (start-angle :: <real>, end-angle :: <real>)
  values(start-angle, end-angle)
end method transform-angles;
||#

(defmethod transform-angles ((transform <translation-transform>) (start-angle real) (end-angle real))
  (values start-angle end-angle))


#||
define sealed inline method untransform-angles
    (transform :: <identity-transform>, start-angle :: <real>, end-angle :: <real>)
 => (start-angle :: <real>, end-angle :: <real>)
  values(start-angle, end-angle)
end method untransform-angles;
||#

(defmethod untransform-angles ((transform <identity-transform>) (start-angle real) (end-angle   real))
  (values start-angle end-angle))


#||
define sealed inline method untransform-angles
    (transform :: <translation-transform>, start-angle :: <real>, end-angle :: <real>)
 => (start-angle :: <real>, end-angle :: <real>)
  values(start-angle, end-angle)
end method untransform-angles;
||#

(defmethod untransform-angles ((transform <translation-transform>) (start-angle real) (end-angle real))
  (values start-angle end-angle))


#||
/// Transforming and untransforming of "spread" rectangles

define sealed method transform-box
    (transform :: <identity-transform>,
     x1 :: <integer>, y1 :: <integer>, x2 :: <integer>, y2 :: <integer>)
 => (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
  values(min(x1, x2), min(y1, y2),
         max(x1, x2), max(y1, y2))
end method transform-box;
||#

(defmethod transform-box ((transform <identity-transform>)
			  (x1 integer) (y1 integer) (x2 integer) (y2 integer))
  (values (min x1 x2) (min y1 y2)
          (max x1 x2) (max y1 y2)))


#||
define sealed method transform-box
    (transform :: <translation-transform>,
     x1 :: <integer>, y1 :: <integer>, x2 :: <integer>, y2 :: <integer>)
 => (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
  let nx1 = x1 + transform.%tx;
  let ny1 = y1 + transform.%ty;
  let nx2 = x2 + transform.%tx;
  let ny2 = y2 + transform.%ty;
  fix-box(min(nx1, nx2), min(ny1, ny2),
	  max(nx1, nx2), max(ny1, ny2))
end method transform-box;
||#

(defmethod transform-box ((transform <translation-transform>)
			  (x1 integer) (y1 integer) (x2 integer) (y2 integer))
  (let ((nx1 (+ x1 (%tx transform)))
	(ny1 (+ y1 (%ty transform)))
	(nx2 (+ x2 (%tx transform)))
	(ny2 (+ y2 (%ty transform))))
    (fix-box (min nx1 nx2) (min ny1 ny2)
	     (max nx1 nx2) (max ny1 ny2))))


#||
define sealed method transform-box
    (transform :: <integer-translation-transform>,
     x1 :: <integer>, y1 :: <integer>, x2 :: <integer>, y2 :: <integer>)
 => (left :: <integer>, top :: <integer>,
     right :: <integer>, bottom :: <integer>)
  let nx1 = x1 + transform.%tx;
  let ny1 = y1 + transform.%ty;
  let nx2 = x2 + transform.%tx;
  let ny2 = y2 + transform.%ty;
  values(min(nx1, nx2), min(ny1, ny2),
	 max(nx1, nx2), max(ny1, ny2))
end method transform-box;
||#

(defmethod transform-box ((transform <integer-translation-transform>)
			  (x1 integer) (y1 integer) (x2 integer) (y2 integer))
  (let ((nx1 (+ x1 (%tx transform)))
	(ny1 (+ y1 (%ty transform)))
	(nx2 (+ x2 (%tx transform)))
	(ny2 (+ y2 (%ty transform))))
    (values (min nx1 nx2) (min ny1 ny2)
	    (max nx1 nx2) (max ny1 ny2))))


#||
define sealed method untransform-box
    (transform :: <identity-transform>,
     x1 :: <integer>, y1 :: <integer>, x2 :: <integer>, y2 :: <integer>)
 => (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
  values(min(x1, x2), min(y1, y2),
         max(x1, x2), max(y1, y2))
end method untransform-box;
||#

(defmethod untransform-box ((transform <identity-transform>)
			    (x1 integer) (y1 integer) (x2 integer) (y2 integer))
  (values (min x1 x2) (min y1 y2)
	  (max x1 x2) (max y1 y2)))


#||
define sealed method untransform-box
    (transform :: <translation-transform>,
     x1 :: <integer>, y1 :: <integer>, x2 :: <integer>, y2 :: <integer>)
 => (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
  let nx1 = x1 - transform.%tx;
  let ny1 = y1 - transform.%ty;
  let nx2 = x2 - transform.%tx;
  let ny2 = y2 - transform.%ty;
  fix-box(min(nx1, nx2), min(ny1, ny2),
	  max(nx1, nx2), max(ny1, ny2))
end method untransform-box;
||#

(defmethod untransform-box ((transform <translation-transform>)
			    (x1 integer) (y1 integer) (x2 integer) (y2 integer))
  (let ((nx1 (- x1 (%tx transform)))
	(ny1 (- y1 (%ty transform)))
	(nx2 (- x2 (%tx transform)))
	(ny2 (- y2 (%ty transform))))
    (fix-box (min nx1 nx2) (min ny1 ny2)
	     (max nx1 nx2) (max ny1 ny2))))


#||
define sealed method untransform-box
    (transform :: <integer-translation-transform>,
     x1 :: <integer>, y1 :: <integer>, x2 :: <integer>, y2 :: <integer>)
 => (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
  let nx1 = x1 - transform.%tx;
  let ny1 = y1 - transform.%ty;
  let nx2 = x2 - transform.%tx;
  let ny2 = y2 - transform.%ty;
  values(min(nx1, nx2), min(ny1, ny2),
	 max(nx1, nx2), max(ny1, ny2))
end method untransform-box;
||#

(defmethod untransform-box ((transform <integer-translation-transform>)
			    (x1 integer) (y1 integer)
			    (x2 integer) (y2 integer))
  (let ((nx1 (- x1 (%tx transform)))
	(ny1 (- y1 (%ty transform)))
	(nx2 (- x2 (%tx transform)))
	(ny2 (- y2 (%ty transform))))
    (values (min nx1 nx2) (min ny1 ny2)
	    (max nx1 nx2) (max ny1 ny2))))



#||
// Transforms all of the coordinate pairs in the sequence.  This returns
// the original sequence if the transformation is the identity and COPY?
// is false, otherwise it returns a new vector containing the result.
define method transform-coordinate-sequence
    (transform :: <transform>, coordinates :: <vector>, #key copy?)
 => (coordinates :: <vector>)
  let length :: <integer> = size(coordinates);
  assert(even?(length),
	 "Coordinate sequences must have an even number of x/y pairs");
  if (transform == $identity-transform)
    if (copy?) copy-sequence(coordinates) else coordinates end
  else
    let result = make(<simple-vector>, size: length);
    transform-coordinates-into!(transform, coordinates, result)
  end
end method transform-coordinate-sequence;
||#

(defmethod transform-coordinate-sequence ((transform   <transform>) (coordinates vector) &key (copy? nil))
  (let ((length (length coordinates)))
    ;; FIXME: change back to 'assert' form
    (unless (evenp length)
      (error "Coordinate sequences must have an even number of x/y pairs"))
    (if (eql transform *identity-transform*)
	(if copy?
	    (copy-seq coordinates)
	    coordinates)
	(let ((result (MAKE-SIMPLE-VECTOR :size length)))
	  (transform-coordinates-into! transform coordinates result)))))


#||
define method transform-coordinate-sequence
    (transform :: <transform>, coordinates :: <sequence>, #key copy?)
 => (coordinates :: <vector>)
  ignore(copy?);
  let length :: <integer> = size(coordinates);
  assert(even?(length),
	 "Coordinate sequences must have an even number of x/y pairs");
  let result = as(<simple-vector>, coordinates);
  if (transform == $identity-transform)
    result
  else
    transform-coordinates-into!(transform, result, result)
  end
end method transform-coordinate-sequence;
||#

(defmethod transform-coordinate-sequence ((transform <transform>) (coordinates sequence) &key copy?)
  (declare (ignore copy?))
  ;; This might be a bit expensive...
  (let ((length (size coordinates)))
    (unless (evenp length)
      (error "Coordinate sequences must have an even number of x/y pairs")))
  (let ((result (MAKE-STRETCHY-VECTOR :contents coordinates)))
    (if (eql transform *identity-transform*)
	result
	(transform-coordinates-into! transform result result))))


#||
define method transform-coordinates-into!
    (transform :: <transform>, coordinates :: <vector>, result :: <vector>)
 => (result :: <vector>)
  // Inline 'do-coordinates' for speed...
  let ncoords :: <integer> = size(coordinates);
  without-bounds-checks
    for (i :: <integer> = 0 then i + 2,
	 until: i = ncoords)
      let x = coordinates[i];
      let y = coordinates[i + 1];
      transform-coordinates!(transform, x, y);
      result[i]     := x;
      result[i + 1] := y
    end
  end;
  result
end method transform-coordinates-into!;
||#

;;; FIXME: Revisit this...

(defgeneric transform-coordinates-into! (transform coordinates result))

(defmethod transform-coordinates-into! ((transform <transform>) (coordinates array) (result array))
  ;; Inline 'do-coordinates' for speed...
  (let ((ncoords (length coordinates)))
    ;; without-bounds-checks ...
    (do ((i 0 (+ i 2)))
	((= i ncoords))
      (let ((x (aref coordinates i))
	    (y (aref coordinates (1+ i))))
	(transform-coordinates! transform x y)
	(setf (aref result i) x)
	(setf (aref result (1+ i)) y))))
  result)



#||
/// Mutable integer translation transforms

define generic make-translation-transform-into!
    (tx :: <real>, ty :: <real>, into :: <transform>)
 => (into :: <transform>);
define generic compose-transform-into!
    (transform :: <transform>, into :: <transform>)
 => (into :: <transform>);
define generic compose-translation-into!
    (x :: <real>, y :: <real>, into :: <transform>)
 => (into :: <transform>);
||#

(defgeneric make-translation-transform-into! (tx ty into))
(defgeneric compose-transform-into! (transform into))
(defgeneric compose-translation-into! (x y into))


#||
define sealed class <mutable-translation-transform> (<integer-translation-transform>)
end class <mutable-translation-transform>;
||#

(defclass <mutable-translation-transform>
    (<integer-translation-transform>)
  ())


#||
define sealed domain make (singleton(<mutable-translation-transform>));
define sealed domain initialize (<mutable-translation-transform>);

define method make-translation-transform-into!
    (tx :: <real>, ty :: <real>, into :: <transform>) => (into :: <transform>)
  if (integral?(tx) & integral?(ty))
    make(<mutable-translation-transform>,
	 tx: truncate(tx), ty: truncate(ty))
  else
    make-translation-transform(tx, ty)
  end
end method make-translation-transform-into!;
||#

(defmethod make-translation-transform-into! ((tx real) (ty real) (into <transform>))
  (if (and (integral? tx) (integral? ty))
      (make-instance '<mutable-translation-transform>
                     :tx (truncate tx)
                     :ty (truncate ty))
      ;; else
      (make-translation-transform tx ty)))


#||
define sealed method make-translation-transform-into!
    (tx :: <integer>, ty :: <integer>, into :: <mutable-translation-transform>)
 => (into :: <mutable-translation-transform>)
  into.%tx := tx;
  into.%ty := ty;
  into.%inverse := #f;
  into
end method make-translation-transform-into!;
||#

(defmethod make-translation-transform-into! ((tx integer) (ty integer) (into <mutable-translation-transform>))
  (setf (%tx into) tx)
  (setf (%ty into) ty)
  (setf (%inverse into) nil)
  into)


#||
define method compose-transform-into!
    (transform :: <transform>, into :: <transform>)
 => (into :: <transform>)
  compose-transforms(transform, into)
end method compose-transform-into!;
||#

(defmethod compose-transform-into! ((transform <transform>) (into <transform>))
  (compose-transforms transform into))


#||
define method compose-transform-into!
    (transform :: <identity-transform>, into :: <transform>)
 => (into :: <transform>)
  into
end method compose-transform-into!;
||#

(defmethod compose-transform-into! ((transform <identity-transform>) (into <transform>))
  into)


#||
define sealed method compose-transform-into!
    (transform :: <integer-translation-transform>, into :: <mutable-translation-transform>)
 => (into :: <mutable-translation-transform>)
  into.%tx := into.%tx + transform.%tx;
  into.%ty := into.%ty + transform.%ty;
  into.%inverse := #f;
  into
end method compose-transform-into!;
||#

(defmethod compose-transform-into! ((transform <integer-translation-transform>) (into <mutable-translation-transform>))
  (setf (%tx into) (+ (%tx into) (%tx transform)))
  (setf (%ty into) (+ (%ty into) (%ty transform)))
  (setf (%inverse into) nil)
  into)


#||
define method compose-translation-into!
    (tx :: <real>, ty :: <real>, into :: <transform>)
 => (into :: <transform>)
  compose-translation-with-transform(into, tx, ty)
end method compose-translation-into!;
||#

(defmethod compose-translation-into! ((tx real) (ty real) (into <transform>))
  (compose-translation-with-transform into tx ty))


#||
define sealed method compose-translation-into!
    (tx :: <real>, ty :: <real>, into :: <mutable-translation-transform>)
 => (into :: <transform>)
  if (integral?(tx) & integral?(ty))
    into.%tx := into.%tx + truncate(tx);
    into.%ty := into.%ty + truncate(ty);
    into.%inverse := #f;
    into
  else
    compose-translation-with-transform(into, tx, ty)
  end
end method compose-translation-into!;
||#

(defmethod compose-translation-into! ((tx real) (ty real) (into <mutable-translation-transform>))
  (cond
    ((and (integral? tx) (integral? ty))
     (setf (%tx into) (+ (%tx into) (truncate tx)))
     (setf (%ty into) (+ (%ty into) (truncate ty)))
     (setf (%inverse into) nil)
     into)
    (t
     (compose-translation-with-transform into tx ty))))


