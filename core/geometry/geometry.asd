;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

(asdf:defsystem :geometry
    :description "DUIM geometry"
    :long-description "The DUIM-Geometry Library module provides basic support for coordinate geometry. This allows the position of elements in a window object to be determined correctly."
    :version     (:read-file-form "version.sexp")
    :author      "Scott McKay, Andy Armstrong (Lisp port: Duncan Rose <duncan@robotcat.demon.co.uk>)"
    :licence     "BSD-2-Clause"
    :depends-on (#:duim-utilities)
    :serial t
    :components
    ((:file "package")
     (:file "protocols")
     (:file "classes")
     (:file "coordinates")
     (:file "transforms")
     (:file "with-device-coords")
     (:file "geometry")
     (:file "regions")
     (:file "boxes")
     (:file "box-arithmetic")
     (:file "printers")))

