;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GEOMETRY-INTERNALS -*-
(in-package #:duim-geometry-internals)

#||
define method print-object
    (transform :: <identity-transform>, stream :: <stream>) => ()
  printing-object (transform, stream, type?: #f, identity?: #f)
    format(stream, "identity transform")
  end
end method print-object;
||#
(defmethod print-object ((transform <identity-transform>) stream)
  (print-unreadable-object (transform stream :type t :identity t)
    (princ "identity transform" stream)))

#||
define method print-object
    (transform :: <translation-transform>, stream :: <stream>) => ()
  printing-object (transform, stream, type?: #t, identity?: #t)
    format(stream, "(%d,%d)", transform.%tx, transform.%ty)
  end
end method print-object;
||#
(defmethod print-object ((transform <translation-transform>) stream)
  (print-unreadable-object (transform stream :type t :identity t)
    (format stream "(~d,~d)" (%tx transform) (%ty transform))))

#||
define method print-object
    (point :: <point>, stream :: <stream>) => ()
  printing-object (point, stream, type?: #t, identity?: #t)
    format(stream, "(%d,%d)", point-x(point), point-y(point))
  end
end method print-object;
||#
(defmethod print-object ((point <point>) stream)
  (print-unreadable-object (point stream :type t :identity t)
    (format stream "(~d,~d)" (point-x point) (point-y point))))


#||
define method print-object
    (box :: <general-box>, stream :: <stream>) => ()
  printing-object (box, stream, type?: #t, identity?: #t)
    format(stream, "(%d,%d):(%d,%d)",
	   box.%left, box.%top, box.%right, box.%bottom)
  end
end method print-object;
||#
(defmethod print-object ((box <general-box>) stream)
  (print-unreadable-object (box stream :type t :identity t)
    (format stream "(~d,~d):(~d,~d)"
            (%left box)
            (%top box)
            (%right box)
            (%bottom box))))

#||
define method print-object
    (box :: <simple-box>, stream :: <stream>) => ()
  printing-object (box, stream, type?: #t, identity?: #t)
    format(stream, "(%d,%d):(%d,%d)",
	   0, 0, box.%width, box.%height)
  end
end method print-object;
||#
(defmethod print-object ((box <simple-box>) stream)
  (print-unreadable-object (box stream :type t :identity t)
    (format stream
            "(~d,~d):(~d,~d)"
            0 0 (%width box) (%height box))))


