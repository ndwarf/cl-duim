;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-RECORDING-INTERNALS -*-
(in-package #:duim-recording-internals)

#||
/// Output recording sheet mixin

// Note that this needs to precede any stream classes (<input-stream-mixin>,
// <output-stream-mixin>) in any class precedence list
define open abstract class <output-recording-mixin> (<abstract-sheet>)
  sealed slot sheet-drawing? :: <boolean> = #t,
    init-keyword: draw?:;
  sealed slot sheet-recording? :: <boolean> = #t,
    init-keyword: record?:;
  // The top level output record
  sealed slot sheet-output-history :: <output-record> = make(<tree-output-history>),
    init-keyword: output-history:,
    setter: %output-history-setter;
  // The currently open output record and its absolute position
  sealed slot sheet-output-record :: false-or(<output-record>) = #f;
  sealed slot sheet-output-record-position = make-point(0, 0);
  sealed slot %medium-state :: false-or(<medium-state>) = #f;
  sealed slot stream-text-output-record :: false-or(<output-record>) = #f;
  sealed slot sheet-highlighted-record :: false-or(<output-record>) = #f;
  sealed slot sheet-redisplay-record :: false-or(<output-record>) = #f;
  sealed slot sheet-redisplaying? = #f;
end class <output-recording-mixin>;
||#

#||  MOVED TO output-recording-mixin.lisp ||#

#||
define thread-slot sheet-recording? :: <boolean>;
define thread-slot sheet-drawing?   :: <boolean>;
define thread-slot %output-history  :: <output-record>;
define thread-slot sheet-output-record       :: false-or(<output-record>);
define thread-slot stream-text-output-record :: false-or(<output-record>);
define thread-slot %medium-state :: false-or(<medium-state>);
||#

;;; FIXME: WHAT TO DO ABOUT THREAD-SLOTS?

;;define thread-slot sheet-recording? :: <boolean>;
;;define thread-slot sheet-drawing?   :: <boolean>;
;;define thread-slot %output-history  :: <output-record>;
;;define thread-slot sheet-output-record       :: false-or(<output-record>);
;;define thread-slot stream-text-output-record :: false-or(<output-record>);
;;define thread-slot %medium-state :: false-or(<medium-state>);


(defgeneric %output-history (sheet))
(defgeneric (setf %output-history) (record sheet))
(defgeneric (setf sheet-output-history) (record sheet))
(defgeneric sheet-medium-state (sheet))
(defgeneric (setf sheet-medium-state) (state sheet))
(defgeneric stream-close-text-output-record (sheet))
(defgeneric do-with-end-of-line-action (sheet continuation action))
(defgeneric do-with-end-of-page-action (sheet continuation action))
(defgeneric make-output-record (record-class &rest initargs &key &allow-other-keys))
(defgeneric do-with-output-recording-options (sheet continuation &key record? draw?))
(defgeneric do-with-new-output-record (sheet continuation &rest initargs &key record-class constructor parent &allow-other-keys))
(defgeneric do-with-new-output-record-1 (sheet continuation record-class constructor parent &rest initargs &key &allow-other-keys))
(defgeneric outer-stream (sheet))
(defgeneric do-with-output-record (sheet record continuation record-x record-y))
(defgeneric do-with-output-to-output-record (sheet continuation &rest initargs &key))
(defgeneric add-output-record (sheet record))
(defgeneric clear-output-history (sheet))
(defgeneric output-history? (record))
(defgeneric sheet-caret-position (sheet))
(defgeneric set-sheet-caret-position (sheet x y))
(defgeneric do-with-local-coordinates (sheet continuation &key x y))
(defgeneric do-with-first-quadrant-coordinates (sheet continuation &key x y))
(defgeneric do-with-room-for-graphics (sheet continuation &key x y height first-quadrant? move-caret? record-class))
(defgeneric move-caret-beyond-output-record (sheet record))
(defgeneric erase-output-record (record sheet))

#||
define method initialize (sheet :: <output-recording-mixin>, #key)
  next-method();
  let (x, y) = point-position(sheet-output-record-position(sheet));
  let history = sheet-output-history(sheet);
  set-sheet-position(history, x, y);
  when (output-history?(history))
    output-history-sheet(history) := sheet
  end
end method initialize;
||#

(defmethod initialize-instance :after ((sheet <output-recording-mixin>) &key &allow-other-keys)
  (multiple-value-bind (x y)
      (point-position (sheet-output-record-position sheet))
    (let ((history (sheet-output-history sheet)))
      (set-sheet-position history x y)
      (when (output-history? history)
        (setf (output-history-sheet history) sheet)))))


#||
//--- Yetch, we need this for the 'dynamic-bind' in 'do-with-output-to-output-record'
define method %output-history
    (sheet :: <output-recording-mixin>) => (record :: <output-record>)
  sheet-output-history(sheet)
end method %output-history;
||#

;;--- Yetch, we need this for the 'dynamic-bind' in 'do-with-output-to-output-record'
(defmethod %output-history ((sheet <output-recording-mixin>))
  (sheet-output-history sheet))

;;(defmethod (setf %output-history) ((record <output-record>)
;;				   (sheet  <output-recording-mixin>))
;;  (%output-history-setter record sheet))


#||
define method sheet-output-history-setter
    (record :: <output-record>, sheet :: <output-recording-mixin>) => (record :: <output-record>)
  sheet.%output-history := record
end method sheet-output-history-setter;
||#

(defmethod (setf sheet-output-history) ((record <output-record>) (sheet <output-recording-mixin>))
  (%output-history-setter record sheet))


#||
define method sheet-medium-state
    (sheet :: <output-recording-mixin>) => (state :: <medium-state>)
  sheet.%medium-state
  | begin
      let medium = sheet-medium(sheet);
      sheet.%medium-state
        := make(<medium-state>,
                brush: medium-brush(medium),
                pen: medium-pen(medium),
                text-style: medium-merged-text-style(medium),
                clipping-region: medium-clipping-region(medium))
    end
end method sheet-medium-state;
||#

(defmethod sheet-medium-state ((sheet <output-recording-mixin>))
  (or (%medium-state sheet)
      (let ((medium (sheet-medium sheet)))
        (setf (%medium-state sheet)
	      (make-instance '<medium-state>
			     :brush (medium-brush medium)
			     :pen   (medium-pen medium)
			     :text-style (medium-merged-text-style medium)
			     :clipping-region (medium-clipping-region medium))))))


#||
define method sheet-medium-state-setter
    (state :: <medium-state>, sheet :: <output-recording-mixin>) => (state :: <medium-state>)
  sheet.%medium-state := state
end method sheet-medium-state-setter;
||#

(defmethod (setf sheet-medium-state) ((state <medium-state>) (sheet <output-recording-mixin>))
  (setf (%medium-state sheet) state))


#||
// Repainting an output recording sheet replays its history after doing
// any other repainting tasks
define method repaint-sheet
    (sheet :: <output-recording-mixin>, region :: <region>, #key medium, force?) => ()
  next-method();
  repaint-sheet(sheet-output-history(sheet), region,
		medium: medium, force?: force?)
end method repaint-sheet;
||#

;; Repainting an output recording sheet replays its history after doing
(defmethod repaint-sheet ((sheet <output-recording-mixin>) (region <region>) &key medium force?)
  (call-next-method)
  (repaint-sheet (sheet-output-history sheet)
                 region
                 :medium medium
                 :force? force?))


#||
// Text recording supplies this method
define method stream-close-text-output-record (sheet :: <output-recording-mixin>) => ()
  #f
end method stream-close-text-output-record;
||#

;; Text recording supplies this method
(defmethod stream-close-text-output-record ((sheet <output-recording-mixin>))
  nil)


#||
// Reset the medium state cache and continue
define method do-with-drawing-options
    (sheet :: <output-recording-mixin>, continuation :: <function>, #rest options, #key)
 => (#rest values)
  ignore(continuation, options);
  dynamic-bind (sheet.%medium-state = #f)
    next-method()
  end
end method do-with-drawing-options;
||#

;; Reset the medium state cache and continue
(defmethod do-with-drawing-options ((sheet <output-recording-mixin>) (continuation function) &rest options &key &allow-other-keys)
  (declare (ignore continuation options))
  (dynamic-bind (((%medium-state sheet) = nil))
    (call-next-method)))


#||
// Reset the medium state cache and continue
define method do-with-text-style
    (sheet :: <output-recording-mixin>, continuation :: <function>, style :: <text-style>)
 => (#rest values)
  ignore(continuation);
  dynamic-bind (sheet.%medium-state = #f)
    next-method()
  end
end method do-with-text-style;
||#

;; Reset the medium state cache and continue
(defmethod do-with-text-style ((sheet <output-recording-mixin>) (continuation function) (style <text-style>))
  (declare (ignore continuation))
  (dynamic-bind (((%medium-state sheet) = nil))
    (call-next-method)))


#||
define method do-with-end-of-line-action
    (sheet :: <sheet>, continuation :: <function>, action)
 => (#rest values)
  ignore(action);
  continuation()
end method do-with-end-of-line-action;
||#

(defmethod do-with-end-of-line-action ((sheet <sheet>) (continuation function) action)
  (declare (ignore action))
  (funcall continuation))


#||
define method do-with-end-of-page-action
    (sheet :: <sheet>, continuation :: <function>, action)
 => (#rest values)
  ignore(action);
  continuation()
end method do-with-end-of-page-action;
||#

(defmethod do-with-end-of-page-action ((sheet <sheet>) (continuation function) action)
  (declare (ignore action))
  (funcall continuation))



#||

/// Output record constructors

define variable *output-record-constructors* :: <object-table> = make(<table>);
||#

(defparameter *output-record-constructors* (make-hash-table :test #'eql))


#||
// Define a constructor function that calls 'make' directly, on the assumption
// that the compiler can optimize a static call to 'make' better than it can
// optimize a call to 'apply make' on a non-constant object.
define macro output-record-constructor-definer
  { define output-record-constructor ?class:name (?arglist:*) ?initargs:* end }
    => { define constant ?class ## "-constructor"
           = method (?arglist)
	       make(?class, ?initargs)
	     end method;
         gethash(*output-record-constructors*, ?class) := ?class ## "-constructor"
       }
end macro output-record-constructor-definer;
||#

#-(and)
(defmacro define-output-record-constructor (class arglist initargs)
"
Create a function that invokes an appropriate constructor (with appropriate
initargs) and store that function in the *OUTPUT-RECORD-CONSTRUCTORS*
hashtable using the class name as the key.

Example usage:-

(DEFINE-OUTPUT-RECORD-CONSTRUCTOR (<MYCLASS>
                                   (&KEY REGION PARENT)
                                   (:REGION REGION :PARENT PARENT)))
"
  ;; This should create a form which when invoked returns a function that
  ;; constructs an appropriate class and store that function in the
  ;; constructors hash-table.
  (let ((gclass (gensym "CLASS-")))   ;; stop multiple evaluation.
  `(let* ((,gclass ',class)
          (con #'(lambda (,@arglist) (make-instance ,gclass ,@initargs))))
    (setf (gethash ,gclass *output-record-constructors*) con))))



(defmacro define-output-record-constructor (class arglist initargs)
  `(progn
     (defparameter ,(intern (concatenate 'string (symbol-name class) "-CONSTRUCTOR"))
       #'(lambda (,@arglist) (make-instance ',class ,@initargs)))
     (setf (gethash (find-class ',class) *output-record-constructors*)
           ,(intern (concatenate 'string (symbol-name class) "-CONSTRUCTOR")))))


#||
// It's only really useful to define output record constructors on composite
// output records -- records that get created with 'do-with-new-output-record'.
// That's because leaf output records get created directly with 'make'.
define method make-output-record (record-class, #rest initargs, #key)
  dynamic-extent(initargs);
  let constructor = gethash(*output-record-constructors*, record-class);
  if (constructor)
    apply(constructor, initargs)
  else
    apply(make, record-class, initargs)
  end
end method make-output-record;
||#

;; It's only really useful to define output record constructors on composite
;; output records -- records that get created with 'do-with-new-output-record'.
;; That's because leaf output records get created directly with 'make'.
(defmethod make-output-record (record-class &rest initargs &key &allow-other-keys)
  (declare (dynamic-extent initargs))
  (let ((builder (if (eql record-class (find-class '<tree-record>))
		     #'make-tree-record
		     #'make-sequence-record)))
    (apply builder initargs))
  #+(or)
  (let ((constructor (gethash record-class *output-record-constructors*)))
    (if constructor
        (apply constructor initargs)
        (apply #'make-instance record-class initargs))))



#||

/// Output record creation

define method do-with-output-recording-options
    (sheet :: <output-recording-mixin>, continuation :: <function>,
     #key record? = sheet-recording?(sheet), draw? = sheet-drawing?(sheet))
 => (#rest values)
  dynamic-bind (sheet-recording?(sheet) = record?,
		sheet-drawing?(sheet) = draw?)
    continuation()
  end
end method do-with-output-recording-options;
||#

(defmethod do-with-output-recording-options ((sheet <output-recording-mixin>) (continuation function)
                                             &key (record? (sheet-recording? sheet)) (draw? (sheet-drawing? sheet)))
  (dynamic-bind (((sheet-recording? sheet) = record?)
                 ((sheet-drawing? sheet)   = draw?))
    (funcall continuation)))


#||
// Creates a new output record for the sheet, calls the continuation, and
// adds the new record to an appropriate parent
define method do-with-new-output-record
    (sheet :: <output-recording-mixin>, continuation :: <function>, #rest initargs,
     #key record-class = <sequence-record>, constructor, parent, #all-keys)
 => (record :: <output-record>)
  dynamic-extent(initargs);
  with-keywords-removed (initargs = initargs, #[record-class:, constructor:, parent:])
    apply(do-with-new-output-record-1,
	  sheet, continuation, record-class, constructor, parent, initargs)
  end
end method do-with-new-output-record;
||#

;; Creates a new output record for the sheet, calls the continuation, and
;; adds the new record to an appropriate parent
(defmethod do-with-new-output-record ((sheet <output-recording-mixin>) (continuation function) &rest initargs
                                      &key (record-class (find-class '<sequence-record>)) constructor parent &allow-other-keys)
  (declare (dynamic-extent initargs))
  (with-keywords-removed (initargs = initargs (:record-class :constructor :parent))
    (apply #'do-with-new-output-record-1
	   sheet continuation record-class constructor parent initargs)))


#||
// Split this out so internal functions don't re-cons the initargs
define method do-with-new-output-record-1
    (sheet :: <output-recording-mixin>, continuation :: <function>,
     record-class, constructor, parent, #rest initargs, #key, #all-keys)
 => (record :: <output-record>)
  dynamic-extent(initargs);
  let current-record = sheet-output-record(sheet);
  let redisplaying? = sheet-redisplaying?(sheet);
  let new-record
    = redisplaying?
      & current-record
      & apply(find-child-output-record, current-record, record-class, initargs);
  let parent = parent | current-record | sheet-output-history(sheet);
  //---*** Is this really the right value for RECORD-X/Y?
  let (record-x, record-y)
    = transform-position(sheet-device-transform(parent), 0, 0);
  if (new-record)
    // Start the region and transform at "zero", and fix them up later
    copy-display-state(new-record, #f)
  else
    let region = make-bounding-box(0, 0, 0, 0);
    let transform = make-translation-transform(0, 0);
    // Output records that care about the caret position will
    // record the caret position inside of 'initialize'
    new-record := if (constructor)
		    apply(constructor,
			  sheet: sheet,
			  region: region,
			  transform: transform,
			  initargs)
		  else
		    apply(make-output-record,
			  record-class,
			  sheet: sheet,
			  region: region,
			  transform: transform,
			  initargs)
		  end;
  end;
  do-with-output-record(sheet, new-record, continuation, record-x, record-y);
  when (redisplaying?)
    recompute-contents-ok(new-record)
  end;
  // We add the child to its parent after doing everything else, so that
  // calls to 'recompute-contents-ok' inside the dynamic extent of the
  // continuation won't take forever.
  when (parent)
    add-child(parent, new-record)
  end;
  new-record
end method do-with-new-output-record-1;
||#

(defmethod do-with-new-output-record-1 ((sheet <output-recording-mixin>) (continuation function)
                                        record-class constructor parent &rest initargs &key &allow-other-keys)
  (declare (dynamic-extent initargs))
  (let* ((current-record (sheet-output-record sheet))
         (redisplaying?  (sheet-redisplaying? sheet))
         (new-record     (and redisplaying?
                              current-record
                              (apply #'find-child-output-record current-record record-class initargs)))
         (parent         (or parent current-record (sheet-output-history sheet))))
    ;;---*** Is this really the right value for RECORD-X/Y?
    (multiple-value-bind (record-x record-y)
        (transform-position (sheet-device-transform parent) 0 0)
      (if new-record
          ;; Start the region and transform at "zero", and fix them up later
          (copy-display-state new-record nil)
          ;; (else)
          (let ((region (make-bounding-box 0 0 0 0))
                (transform (make-translation-transform 0 0)))
            ;; Output records that care about the caret position will
            ;; record the caret position inside of 'initialize'
            (setf new-record (if constructor
                                 (apply constructor
					:sheet sheet
					:region region
					:transform transform
					initargs)
                                 (apply #'make-output-record
					record-class
					:sheet sheet
					:region region
                                        :transform transform
					initargs)))))
      (do-with-output-record sheet new-record continuation record-x record-y)
      (when redisplaying?
        (recompute-contents-ok new-record))
      ;; We add the child to its parent after doing everything else, so that
      ;; calls to 'recompute-contents-ok' inside the dynamic extent of the
      ;; continuation won't take forever.
      (when parent
        (add-child parent new-record))
      new-record)))


#||
define method do-with-new-output-record
    (sheet :: <sheet>, continuation :: <function>, #rest initargs, #key)
 => (record :: singleton(#f))
  ignore(initargs);
  continuation(#f);		// no output record for you!
  #f
end method do-with-new-output-record;
||#

(defmethod do-with-new-output-record ((sheet <sheet>) (continuation function) &rest initargs &key &allow-other-keys)
  (declare (ignore initargs))
  (funcall continuation nil)    ;; no output record for you!
  nil)


#||
//---*** I hope the generic function isn't overly restrictive...
define method outer-stream (sheet :: <sheet>)
  sheet
end method outer-stream;
||#

;;---*** I hope the generic function isn't overly restrictive...
(defmethod outer-stream ((sheet <sheet>))
  sheet)


#||
// Given a sheet and an output record, calls the continuation with the
// appropriate output recording state set up.
define method do-with-output-record
    (sheet :: <output-recording-mixin>, record :: <output-record-element-mixin>,
     continuation :: <function>, record-x, record-y) => ()
  // Close the text record before and after
  stream-close-text-output-record(outer-stream(sheet));
  let current-position = sheet-output-record-position(sheet);
  dynamic-bind (point-x(current-position) = record-x,
		point-y(current-position) = record-y,
		sheet-output-record(sheet) = record)
    continuation(record);
    // Close the any text record here.  Records that care about
    // the caret position should also record the end caret position.
    stream-close-text-output-record(outer-stream(sheet))
  end
end method do-with-output-record;
||#

;; Given a sheet and an output record, calls the continuation with the
;; appropriate output recording state set up.
(defmethod do-with-output-record ((sheet <output-recording-mixin>) (record <output-record-element-mixin>)
                                  (continuation function) record-x record-y)
  ;; Close the text record before and after
  (stream-close-text-output-record (outer-stream sheet))
  (let ((current-position (sheet-output-record-position sheet)))
    (dynamic-bind (((point-x current-position) = record-x)
                   ((point-y current-position) = record-y)
                   ((sheet-output-record sheet) = record))
      (funcall continuation record)
      ;; Close the any text record here.  Records that care about
      ;; the caret position should also record the end caret position.
      (stream-close-text-output-record (outer-stream sheet)))))


#||
//--- Pretty yucky that we have to do this here
define thread-slot point-x :: <real> of <standard-point>;
define thread-slot point-y :: <real> of <standard-point>;
||#

;; TODO: This thread-slot stuff is a little worrying. What is going to
;; break now it's dropped on the floor?

;;define thread-slot point-x :: <real> of <standard-point>;
;;define thread-slot point-y :: <real> of <standard-point>;

#||
// Creates a new output record for the sheet, calls the continuation, but does
// not add the record to the sheet
define method do-with-output-to-output-record
    (sheet :: <output-recording-mixin>, continuation :: <function>, #rest initargs, #key)
 => (record :: <output-record>)
  dynamic-extent(initargs);
  with-output-recording-options (sheet, draw?: #f, record?: #t)
    dynamic-bind (sheet.%output-history = make(<sequence-output-history>),
		  sheet-output-record(sheet) = #f,
		  stream-text-output-record(sheet) = #f)
      apply(do-with-new-output-record, sheet, continuation, initargs)
    end
  end
end method do-with-output-to-output-record;
||#

;; Creates a new output record for the sheet, calls the continuation, but does
;; not add the record to the sheet
(defmethod do-with-output-to-output-record ((sheet <output-recording-mixin>) (continuation function) &rest initargs &key)
  (declare (dynamic-extent initargs))
  (with-output-recording-options (sheet :draw? nil :record? t)
    (dynamic-bind (((%output-history sheet) = (make-instance '<sequence-output-history>))
                   ((sheet-output-record sheet) = nil)
                   ((stream-text-output-record sheet) = nil))
      (apply #'do-with-new-output-record sheet continuation initargs))))


#||
define method do-with-output-to-output-record
    (sheet :: <sheet>, continuation :: <function>, #rest initargs, #key)
 => (record :: singleton(#f))
  ignore(initargs);
  continuation(#f);		// no output record for you!
  #f
end method do-with-output-to-output-record;
||#

(defmethod do-with-output-to-output-record ((sheet <sheet>) (continuation function) &rest initargs &key)
  (declare (ignore initargs))
  (funcall continuation nil)		;; no output record for you!
  nil)


#||
define method add-output-record (sheet :: <output-recording-mixin>, record)
  let output-record = sheet-output-record(sheet) | sheet-output-history(sheet);
  add-child(output-record, record)
end method add-output-record;
||#

(defmethod add-output-record ((sheet <output-recording-mixin>) record)
  (let ((output-record (or (sheet-output-record sheet)
			   (sheet-output-history sheet))))
    (add-child output-record record)))


#||
define method clear-output-history (sheet :: <output-recording-mixin>)
  when (sheet-output-history(sheet))
    sheet-children(sheet-output-history(sheet)) := #[]
  end;
  stream-text-output-record(sheet) := #f;
  sheet-highlighted-record(sheet) := #f
end method clear-output-history;
||#

(defmethod clear-output-history ((sheet <output-recording-mixin>))
  (when (sheet-output-history sheet)
    (setf (sheet-children (sheet-output-history sheet)) #()))
  (setf (stream-text-output-record sheet) nil)
  (setf (sheet-highlighted-record sheet)  nil))



#||

/// Top level output histories

define open abstract class <output-history-mixin> (<abstract-sheet>)
  sealed slot output-history-sheet :: false-or(<sheet>) = #f,
    init-keyword: sheet:;
end class <output-history-mixin>;
||#

(defclass <output-history-mixin> (<abstract-sheet>)
  ((output-history-sheet :type (or null <sheet>) :initarg :sheet :initform nil :accessor output-history-sheet))
  (:metaclass <abstract-metaclass>))


#||
define method output-history? (record :: <output-history-mixin>) #t end;
define method output-history? (record) #f end;
||#

(defmethod output-history? ((record <output-history-mixin>))
  t)


(defmethod output-history? (record)
  (declare (ignore record))
  nil)


#||
// If someone sets the output history, be sure the sheet and the new
// output history stay in sync.  Leave it to a higher level to clear
// the sheet's viewport, reset the drawing/recording state, etc.
define method sheet-output-history-setter
    (record :: <output-history-mixin>, sheet :: <output-recording-mixin>)
 => (record :: <output-record>)
  next-method();
  output-history-sheet(record) := sheet;
  record
end method sheet-output-history-setter;
||#

;; If someone sets the output history, be sure the sheet and the new
;; output history stay in sync. Leave it to a higher level to clear
;; the sheet's viewport, reset the drawing/recording state, etc.
(defmethod (setf sheet-output-history) ((record <output-history-mixin>) (sheet <output-recording-mixin>))
  (call-next-method)
  (setf (output-history-sheet record) sheet)
  record)


#||
define method note-region-changed
    (record :: <output-history-mixin>) => ()
  next-method();
  let (rleft, rtop, rright, rbottom) = box-edges(record);
  // Top-level output records must not have their upper left corner
  // any "later" than (0,0), or else scroll bars and scrolling will
  // not do the right thing.
  // Note the implicit assumption that histories are top-level records,
  // which strikes me as pretty reasonable...
  let left = min(rleft, 0);
  let top  = min(rtop, 0);
  let right  = rright;
  let bottom = rbottom;
  let changed? = (rleft ~= left) | (rtop ~= top);
  when (changed?)
    sheet-region(record)
      := set-box-edges(sheet-region(record), left, top, right, bottom)
  end;
  let sheet = output-history-sheet(record);
  let viewport = sheet & sheet-viewport(sheet);
  when (viewport)
    let (sleft, stop, sright, sbottom) = box-edges(sheet);
    // Update the sheet's region, and then the scroll bars
    when (left  < sleft
	  | top < stop
	  | right  > sright
	  | bottom > sbottom)
      min!(left, sleft);
      min!(top,  stop);
      max!(right,  sright);
      max!(bottom, sbottom);
      sheet-region(sheet) :=		// might cons a new region...
	set-box-edges(sheet-region(sheet), left, top, right, bottom);
      note-region-changed(sheet)
    end;
    update-scroll-bars(viewport)
  end
end method note-region-changed;
||#

(defmethod note-region-changed ((record <output-history-mixin>))
  (call-next-method)
  (multiple-value-bind (rleft rtop rright rbottom)
      (box-edges record)
    ;; Top-level output records must not have their upper left corner
    ;; any "later" than (0,0), or else scroll bars and scrolling will
    ;; not do the right thing.
    ;; Note the implicit assumption that histories are top-level records,
    ;; which strikes me as pretty reasonable...
    (let* ((left (min rleft 0))
           (top  (min rtop 0))
           (right rright)
           (bottom rbottom)
           (changed? (or (/= rleft left) (/= rtop top))))
      (when changed?
        (setf (sheet-region record)
              (set-box-edges (sheet-region record) left top right bottom)))
      (let* ((sheet (output-history-sheet record))
             (viewport (and sheet (sheet-viewport sheet))))
        (when viewport
          (multiple-value-bind (sleft stop sright sbottom)
              (box-edges sheet)
            ;; Update the sheet's region, and then the scroll bars
            (when (or (< left sleft)
                      (< top stop)
                      (> right sright)
                      (> bottom sbottom))
	      (setf left (min left sleft))
	      (setf top (min top stop))
	      (setf right (max right sright))
	      (setf bottom (max bottom sbottom))
              (setf (sheet-region sheet)    ;; might cons a new region...
                    (set-box-edges (sheet-region sheet) left top right bottom))
              (note-region-changed sheet))
            (update-scroll-bars viewport)))))))



#||

/// Graphics

define function sheet-caret-position (sheet :: <sheet>) => (x, y)
  let caret = sheet-caret(sheet);
  if (caret)
    caret-position(caret)
  else
    values(0, 0)
  end
end function sheet-caret-position;
||#

(defmethod sheet-caret-position ((sheet <sheet>))
  (let ((caret (sheet-caret sheet)))
    (if caret
        (caret-position caret)
        (values 0 0))))


#||
define function set-sheet-caret-position (sheet :: <sheet>, x, y) => ()
  let caret = sheet-caret(sheet);
  when (caret)
    set-caret-position(caret, x, y)
  end
end function set-sheet-caret-position;
||#

(defmethod set-sheet-caret-position ((sheet <sheet>) x y)
  (let ((caret (sheet-caret sheet)))
    (when caret
      (set-caret-position caret x y))))


#||
// Establish a local +Y-downward coordinate system at the current caret position,
// and execute the body
define method do-with-local-coordinates
    (sheet :: <output-recording-mixin>, continuation :: <function>, #key x, y)
 => (#rest values)
  let medium = sheet-medium(sheet);
  let (cx, cy) = if (x & y) values(x, y) else sheet-caret-position(sheet) end;
  let (tx, ty) = transform-position(medium-transform(medium), 0, 0);
  let transform = make-translation-transform(cx - tx, cy - ty);
  with-transform (sheet, transform)
    continuation()
  end
end method do-with-local-coordinates;
||#

;; Establish a local +Y-downward coordinate system at the current caret position,
;; and execute the body
(defmethod do-with-local-coordinates ((sheet <output-recording-mixin>) (continuation function) &key x y)
  (let ((medium (sheet-medium sheet)))
    (multiple-value-bind (cx cy)
        (if (and x y)
            (values x y)
            (sheet-caret-position sheet))
      (multiple-value-bind (tx ty)
          (transform-position (medium-transform medium) 0 0)
        (let ((transform (make-translation-transform (- cx tx) (- cy ty))))
          (with-transform (sheet transform)
            (funcall continuation)))))))


#||
// Establish a local +Y-upward coordinate system at the current caret position,
// and execute the body
define method do-with-first-quadrant-coordinates
    (sheet :: <output-recording-mixin>, continuation :: <function>, #key x, y)
 => (#rest values)
  let medium = sheet-medium(sheet);
  let (cx, cy) = if (x & y) values(x, y) else sheet-caret-position(sheet) end;
  let (tx, ty) = transform-position(medium-transform(medium), 0, 0);
  let transform = if (medium-+Y-upward?(medium))
                    $identity-transform
                  else
                    make-transform(1, 0, 0, -1, cx - tx, cy - ty)
                  end;
  with-transform (sheet, transform)
    dynamic-bind (medium-+Y-upward?(medium) = #t)
      continuation()
    end
  end
end method do-with-first-quadrant-coordinates;
||#

;; Establish a local +Y-upward coordinate system at the current caret position,
;; and execute the body
(defmethod do-with-first-quadrant-coordinates ((sheet <output-recording-mixin>) (continuation function) &key x y)
  (let ((medium (sheet-medium sheet)))
    (multiple-value-bind (cx cy)
        (if (and x y)
            (values x y)
            (sheet-caret-position sheet))
      (multiple-value-bind (tx ty)
          (transform-position (medium-transform medium) 0 0)
        (let ((transform (if (medium-+Y-upward? medium)
                             *identity-transform*
                             (make-transform 1 0 0 -1 (- cx tx) (- cy ty)))))
          (with-transform (sheet transform)
            (dynamic-bind (((medium-+Y-upward? medium) = t))
              (funcall continuation))))))))


#||
define method do-with-room-for-graphics
    (sheet :: <output-recording-mixin>, continuation :: <function>,
     #key x, y, height, first-quadrant? = #t, move-caret? = #t,
          record-class = <sequence-record>)
 => (record :: <output-record>)
  let record
    = if (first-quadrant?)
        with-output-recording-options (sheet, draw?: #f, record?: #t)
          with-first-quadrant-coordinates (sheet, x: x, y: y)
            with-new-output-record (record = sheet, record-class: record-class)
              ignore(record);
              continuation()
            end
          end
        end
      else
        with-output-recording-options (sheet, draw?: #f, record?: #t)
          with-local-coordinates (sheet, x: x, y: y)
            with-new-output-record (record = sheet, record-class: record-class)
              ignore(record);
              continuation()
            end
          end
        end
      end;
  let (x, y) = sheet-position(record);
  when (height)
    inc!(y, height - box-height(record))
  end;
  set-sheet-position(record, x, y);	//---*** just mung the transform?
  recompute-region(record);		//---*** do we really need to do this?
  when (sheet-drawing?(sheet))
    repaint-sheet(record, $everywhere)
  end;
  when (move-caret?)
    move-caret-beyond-output-record(sheet, record)
  end;
  record
end method do-with-room-for-graphics;
||#

(defmethod do-with-room-for-graphics ((sheet <output-recording-mixin>) (continuation function)
                                      &key x y height (first-quadrant? t) (move-caret? t)
                                      (record-class (find-class '<sequence-record>)))
  (let ((record
	 (if first-quadrant?
	     (with-output-recording-options (sheet :draw? nil :record? t)
	       (with-first-quadrant-coordinates (sheet :x x :y y)
	         (with-new-output-record ((record = sheet) :record-class record-class)
		   (declare (ignore record))
		   (funcall continuation))))
	     ;; (else)
	     (with-output-recording-options (sheet :draw? nil :record? t)
	       (with-local-coordinates (sheet :x x :y y)
		 (with-new-output-record ((record = sheet) :record-class record-class)
		   (declare (ignore record))
		   (funcall continuation)))))))
    (multiple-value-bind (x y)
        (sheet-position record)
      (when height
        (incf y (- height (box-height record))))
      (set-sheet-position record x y)	;;---*** just mung the transform?
      (recompute-region record)		;;---*** do we really need to do this?
      (when (sheet-drawing? sheet)
        (repaint-sheet record *everywhere*))
      (when move-caret?
        (move-caret-beyond-output-record sheet record))
      record)))


#||
define method do-with-room-for-graphics
    (sheet :: <sheet>, continuation :: <function>, #rest keys, #key)
 => (record :: singleton(#f))
  ignore(keys);
  continuation();
  #f
end method do-with-room-for-graphics;
||#

(defmethod do-with-room-for-graphics ((sheet <sheet>) (continuation function) &rest keys &key &allow-other-keys)
  (declare (ignore keys))
  (funcall continuation)
  nil)


#||
define method move-caret-beyond-output-record
    (sheet :: <sheet>, record :: <output-record>) => ()
  let (left, top, right, bottom) = box-edges(record);
  ignore(left, top);
  let (x, y) = transform-position(sheet-device-transform(record), right, bottom);
  with-end-of-page-action (sheet, #"allow")
    set-sheet-caret-position(sheet, x, y - sheet-line-height(sheet))
  end
end method move-caret-beyond-output-record;
||#

(defmethod move-caret-beyond-output-record ((sheet <sheet>) (record <output-record>))
  (multiple-value-bind (left top right bottom)
      (box-edges record)
    (declare (ignore left top))
    (multiple-value-bind (x y)
        (transform-position (sheet-device-transform record) right bottom)
      (with-end-of-page-action (sheet :allow)
        (set-sheet-caret-position sheet x (- y (sheet-line-height sheet)))))))



#||

/// Erasing

define method erase-output-record
    (record :: <output-record>, sheet :: <output-recording-mixin>) => ()
  let parent = sheet-parent(record);
  let medium = sheet-medium(sheet);
  let (left, top, right, bottom) = box-edges(record);
  let transform = sheet-device-transform(record);
  transform-coordinates!(transform, left, top, right, bottom);
  with-drawing-options(medium, brush: $background)
    if (left = right | top = bottom)
      // Handle specially, since a thin line is wider than a
      // rectangle of zero width or height
      draw-line(medium, left, top, right, bottom)
    else
      draw-rectangle(medium, left, top, right, bottom, filled?: #t)
    end
  end;
  when (parent)
    remove-child(parent, record)
  end;
  // Use the output record itself as the repaint region, and repaint
  // the stuff that might have been obscured by the erased output.
  // Note that 'repaint-sheet' sets up a clipping region using the
  // bbox of the original record, so we don't end up erroneously
  // redrawing anything outside the erasure region (which could
  // clobber useful output).
  //---*** This assumes 'repaint-sheet' sets up a clipping region.
  //---*** Does it really?
  repaint-sheet(record, record)
end method erase-output-record;
||#

(defmethod erase-output-record ((record <output-record>) (sheet <output-recording-mixin>))
  (let ((parent (sheet-parent record))
        (medium (sheet-medium sheet)))
    (multiple-value-bind (left top right bottom)
        (box-edges record)
      (let ((transform (sheet-device-transform record)))
        (transform-coordinates! transform left top right bottom)
        (with-drawing-options (medium :brush *background*)
          (if (or (= left right) (= top bottom))
              ;; Handle specially, since a thin line is wider than a
              ;; rectangle of zero width or height
              (draw-line medium left top right bottom)
              (draw-rectangle medium left top right bottom :filled? t)))
        (when parent
          (remove-child parent record))
        ;; Use the output record itself as the repaint region, and repaint
        ;; the stuff that might have been obscured by the erased output.
        ;; Note that 'repaint-sheet' sets up a clipping region using the
        ;; bbox of the original record, so we don't end up erroneously
        ;; redrawing anything outside the erasure region (which could
        ;; clobber useful output).
        ;;---*** This assumes 'repaint-sheet' sets up a clipping region.
        ;;---*** Does it really?
        (repaint-sheet record record)))))


#||
// Like the above, except it erases a while set of output records in one
// go, then replays the "damaged" reason just once.  This is much faster
// than calling 'erase-output-record' multiple times on single records.
define method erase-output-record
    (records :: <sequence>, sheet :: <output-recording-mixin>)
  let bl = $largest-coordinate;
  let bt = $largest-coordinate;
  let br = $smallest-coordinate;
  let bb = $smallest-coordinate;
  let medium = sheet-medium(sheet);
  dynamic-bind (medium-transform(medium) = $identity-transform,
		medium-brush(medium) = $background)
    for (record in records)
      let parent = sheet-parent(record);
      let (left, top, right, bottom) = box-edges(record);
      let transform = sheet-device-transform(record);
      transform-coordinates!(transform, left, top, right, bottom);
      min!(bl, left);
      min!(bt, top);
      max!(br, right);
      max!(bb, bottom);
      if (left = right | top = bottom)
	draw-line(medium, left, top, right, bottom)
      else
	draw-rectangle(medium, left, top, right, bottom, filled?: #t)
      end;
      when (parent)
        remove-child(parent, record)
      end
    end
  end;
  repaint-sheet(sheet, make-bounding-box(bl, bt, br, bb))
end method erase-output-record;
||#

;; Like the above, except it erases a while set of output records in one
;; go, then replays the "damaged" reason just once. This is much faster
;; than calling 'erase-output-record' multiple times on single records.
(defmethod erase-output-record ((records sequence) (sheet <output-recording-mixin>))
  (let ((bl +largest-coordinate+)
        (bt +largest-coordinate+)
        (br +smallest-coordinate+)
        (bb +smallest-coordinate+)
        (medium (sheet-medium sheet)))
    (dynamic-bind (((medium-transform medium) = *identity-transform*)
                   ((medium-brush medium)     = *background*))
      (loop for record in records
	 do (let ((parent (sheet-parent record)))
	      (multiple-value-bind (left top right bottom)
		  (box-edges record)
		(let ((transform (sheet-device-transform record)))
		  (transform-coordinates! transform left top right bottom)
		  (setf bl (min bl left))
		  (setf bt (min bt top))
		  (setf br (max br right))
		  (setf bb (max bb bottom))
		  (if (or (= left right) (= top bottom))
		      (draw-line medium left top right bottom)
		      (draw-rectangle medium left top right bottom :filled? t))
		  (when parent
		    (remove-child parent record)))))))
    (repaint-sheet sheet (make-bounding-box bl bt br bb))))



#||

/// Fit for a king...

define open abstract class <recording-pane>
    (<output-recording-mixin>, <drawing-pane>)
end class <recording-pane>;
||#

(defclass <recording-pane>
    (<output-recording-mixin> <drawing-pane>)
  ()
  (:metaclass <abstract-metaclass>))


#||
define method clear-output-history (pane :: <recording-pane>) => ()
  next-method();
  clear-box*(pane, sheet-viewport-region(pane))
end method clear-output-history;
||#

(defmethod clear-output-history ((pane <recording-pane>))
  (call-next-method)
  (clear-box* pane (sheet-viewport-region pane)))


#||
define sealed class <concrete-recording-pane> (<recording-pane>)
end class <concrete-recording-pane>;
||#

(defclass <concrete-recording-pane> (<recording-pane>) ())


#||
define sealed inline method make
    (class == <recording-pane>, #rest initargs, #key, #all-keys)
 => (pane :: <concrete-recording-pane>)
  apply(make, <concrete-recording-pane>, initargs)
end method make;

define sealed domain make (singleton(<concrete-recording-pane>));
define sealed domain initialize (<concrete-recording-pane>);
||#

;;; FIXME -- DECIDE WHAT TO DO WITH THESE...

;;; FIXME: BAH. IT'S A PANE TYPE AND WE'RE NOT INVOKING
;;; MAKE-PANE. PERHAPS THIS (AND OTHER MAKE-INSTANCE METHODS
;;; SCATTERED THROUGHOUT THE CODE ON PANE TYPES) SHOULD BE
;;; MAKE-PANE METHODS INSTEAD?
#-(and)
(defmethod make-instance ((class (eql (find-class '<recording-pane>)))
                          &rest initargs
                          &key
                          &allow-other-keys)
  (apply #'make-instance '<concrete-recording-pane> initargs))

;;; TODO: I added MAKE-PANE in the hopes of making things work
;;; more smoothly.

(defmethod make-pane ((class (eql (find-class '<recording-pane>)))
		      &rest initargs
		      &key
		      &allow-other-keys)
  (apply #'make-pane (find-class '<concrete-recording-pane>) initargs))


