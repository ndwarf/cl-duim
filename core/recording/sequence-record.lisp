;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-RECORDING-INTERNALS -*-
(in-package :duim-recording-internals)

#||
/// Sequence output record

// The Z-ordering of the children is that earlier elements are lower in
// the stack.  That is, the last (most recently added) element in '%children'
// is at the top.
define open abstract class <sequence-record> (<basic-composite-record>)
  sealed slot %children = #f;
end class <sequence-record>;
||#

(defclass <sequence-record>
    (<basic-composite-record>)
  ((%children :initform nil :accessor %children))
  (:documentation
"
The Z-ordering of the children is that earlier elements are lower in
the stack. That is, the last (most recently added) element in '%children'
is at the top.
")
  (:metaclass <abstract-metaclass>))


#||
define sealed class <concrete-sequence-record> (<sequence-record>)
end class <concrete-sequence-record>;
||#

(defclass <concrete-sequence-record> (<sequence-record>) ())


#||
define sealed inline method make
    (class == <sequence-record>, #rest initargs, #key, #all-keys)
 => (pane :: <concrete-sequence-record>)
  apply(make, <concrete-sequence-record>, initargs)
end method make;
||#

(defmethod make-sequence-record (&rest initargs &key &allow-other-keys)
  (apply #'make-instance '<concrete-sequence-record> initargs))


#||
define sealed domain make (singleton(<concrete-sequence-record>));
define sealed domain initialize (<concrete-sequence-record>);

define output-record-constructor <sequence-record>
    (#key children, parent, sheet, region, transform)
  children: children, parent: parent, sheet: sheet,
  region: region, transform: transform
end;
||#

(define-output-record-constructor <sequence-record> (&key children parent sheet region transform)
  (:children children :parent parent :sheet sheet
	     :region region :transform transform))


#||
define method initialize (record :: <sequence-record>, #key sheet, children)
  ignore(sheet);
  next-method();
  when (children)
    for (child in children)
      add-child(record, child)
    end
  end
end method initialize;
||#

(defmethod initialize-instance :after ((record <sequence-record>) &key sheet children &allow-other-keys)
  (declare (ignore sheet))
  (when children
    ;; FIXME: WRITE 'FOR' WHICH JUST CALLS 'LOOP'?
    (map nil #'(lambda (child)
		 (add-child record child))
	 children)))


#||
define method sheet-children
    (record :: <sequence-record>) => (children :: <vector>)
  let children = record.%children;
  case
    children == #f           => #[];
    output-record?(children) => vector(children);
    otherwise                => children;
  end
end method sheet-children;
||#

(defmethod sheet-children ((record <sequence-record>))
  (let ((children (%children record)))
    (cond ((null children) #())
          ((output-record-p children) (vector children))
          (t children))))


#||
//--- Do we need a fast way to clear an output record?
define method sheet-children-setter
    (children :: <vector>, record :: <sequence-record>) => (children :: <vector>)
  until (empty?(record.%children))
    remove-child(record, record.%children[0])
  end;
  // Reset the region before adding the new children
  sheet-region(record) := set-box-edges(sheet-region(record), 0, 0, 0, 0);
  for (child in children)
    add-child(record, child);
  end;
  children
end method sheet-children-setter;
||#

;;--- Do we need a fast way to clear an output record?
(defmethod (setf sheet-children) ((children vector) (record <sequence-record>))
  (loop until (empty? (%children record))
     do (remove-child record (aref (%children record) 0)))
  ;; Reset the region before adding the new children
  (setf (sheet-region record) (set-box-edges (sheet-region record) 0 0 0 0))
  (loop for child across children
     do (add-child record child))
  children)


#||
define method sheet-child-count
    (record :: <sequence-record>, #key fast?) => (count :: <integer>)
  ignore(fast?);
  let children = record.%children;
  case
    children == #f           => 0;
    output-record?(children) => 1;
    otherwise                => size(children);
  end
end method sheet-child-count;
||#

(defmethod sheet-child-count ((record <sequence-record>) &key fast?)
  (declare (ignore fast?))
  (let ((children (%children record)))
    (cond ((null children) 0)
          ((output-record-p children) 1)
          (t (length children)))))


#||
define method sheet-element
    (record :: <sequence-record>, index) => (record :: false-or(<output-record>))
  let children = record.%children;
  case
    children == #f           => #f;
    output-record?(children) => if (zero?(index)) children else #f end;
    otherwise                => children[index];
  end
end method sheet-element;
||#

(defgeneric sheet-element (record index))

(defmethod sheet-element ((record <sequence-record>) index)
  (let ((children (%children record)))
    (cond ((null children) nil)
          ((output-record-p children) (if (zerop index) children nil))
          (t (aref children index)))))


#||
define method do-add-child
    (record :: <sequence-record>, child :: <sheet>, #key index = #"end") => ()
  let children = record.%children;
  case
    children == #f =>
      record.%children := child;
    output-record?(children) =>
      let old-child = children;
      let new-children :: <stretchy-object-vector> = make(<stretchy-vector>);
      // Upgrade the single child to a vector, getting the order
      // of the (now two) children correct
      case
        index = #"end" | index > 0 =>
          add!(new-children, old-child);
          add!(new-children, child);
        otherwise =>
	  add!(new-children, child);
	  add!(new-children, old-child);
      end;
      record.%children := new-children;
    otherwise =>
      insert-at!(children, child, index);
  end
end method do-add-child;
||#

(defmethod do-add-child ((record <sequence-record>) (child <sheet>) &key (index :end))
  (let ((children (%children record)))
    (cond ((null children) (setf (%children record) child))
          ((output-record-p children)
           (let ((old-child children)
                 (new-children (make-array 0 :adjustable t :fill-pointer t)))
             ;; Upgrade the single child to a vector, getting the order
             ;; of the (now two) children correct
             (cond ((or (eql index :end) (> index 0))
                    (add! new-children old-child)
                    (add! new-children child))
                   (t
                    (add! new-children child)
                    (add! new-children old-child)))
	     (setf (%children record) new-children)))
          (t
           (setf (%children record)
		 (insert-at! children child index))))))


#||
define method do-remove-child
    (record :: <sequence-record>, child :: <sheet>) => ()
  let children = record.%children;
  case
    children == #f =>
      #f;
    output-record?(children) =>
      when (children == child)
	record.%children := #f
      end;
    otherwise =>
      remove!(record.%children, child);
  end
end method do-remove-child;
||#

(defmethod do-remove-child ((record <sequence-record>) (child <sheet>))
  (let ((children (%children record)))
    (cond ((null children) nil)
          ((output-record-p children)
           (when (eql children child)
             (setf (%children record) nil)))
          (t
           (setf (%children record)
		 (delete (%children record) child))))))


#||
define method do-replace-child
    (record :: <sequence-record>, old-child :: <sheet>, new-child :: <sheet>) => ()
  substitute!(record.%children, old-child, new-child)
end method do-replace-child;
||#

(defmethod do-replace-child ((record <sequence-record>) (old-child <sheet>) (new-child <sheet>))
  (nsubstitute new-child old-child (%children record)))


#||
define method do-sheet-children
    (function :: <function>, record :: <sequence-record>,
     #key z-order :: <z-order> = #f) => ()
  let children = record.%children;
  case
    children == #f =>
      #f;
    output-record?(children) =>
      function(children);
    otherwise =>
      let iteration-protocol
	= if (z-order == #"top-down") top-down-iteration-protocol
	  else bottom-up-iteration-protocol end;
      for (child :: <sheet> in children using iteration-protocol)
	function(child)
      end;
  end
end method do-sheet-children;
||#


(defmethod do-sheet-children ((function function) (record <sequence-record>)
                              &key (z-order nil))
  (let ((children (%children record)))
    (cond ((null children) nil)
          ((output-record-p children)
           (funcall function children))
          (t
           (let ((children (if (eql z-order :top-down)
                               (reverse children)   ;; :top-down: end -> start
                               children)))          ;; bottom-up: start -> end
             (loop for child in children
		do (funcall function child)))))))


#||
// Breadth-first recursive...
define method do-sheet-tree
    (function :: <function>, record :: <sequence-record>) => ()
  dynamic-extent(function);
  function(record);
  let children = record.%children;
  case
    children == #f =>
      #f;
    output-record?(children) =>
      do-sheet-tree(function, children);
    otherwise =>
      without-bounds-checks
	for (i :: <integer> from 0 below size(children))
	  let child = children[i];
	  do-sheet-tree(function, child)
	end
      end;
  end
end method do-sheet-tree;
||#

;; Breadth-first recursive...
(defmethod do-sheet-tree ((function function) (record <sequence-record>))
  (declare (dynamic-extent function))
  (funcall function record)
  (let ((children (%children record)))
    (cond ((null children) nil)
          ((output-record-p children)
           (do-sheet-tree function children))
          (t
           (without-bounds-checks
             (loop for i from 0 below (length children)
		do (let ((child (aref children i)))
		     (do-sheet-tree function child))))))))


#||
// X and Y are in the coordinate system of the sequence record
define method do-children-containing-position
    (function :: <function>, record :: <sequence-record>, x :: <real>, y :: <real>) => ()
  let children = record.%children;
  case
    children == #f =>
      #f;
    output-record?(children) =>
      let child = children;
      let (left, top, right, bottom) = box-edges(child);
      transform-coordinates!(sheet-transform(child), left, top, right, bottom);
      when (ltrb-contains-position?(left, top, right, bottom, x, y))
	function(child)
      end;
    otherwise =>
      // Walk from the top to the bottom of the Z-ordering
      without-bounds-checks
	for (i :: <integer> from size(children) - 1 to 0 by -1)
	  let child = children[i];
	  let (left, top, right, bottom) = box-edges(child);
	  transform-coordinates!(sheet-transform(child), left, top, right, bottom);
	  when (ltrb-contains-position?(left, top, right, bottom, x, y))
	    function(child)
	  end
	end
      end;
  end
end method do-children-containing-position;
||#

(defmethod do-children-containing-position ((function function) (record <sequence-record>) (x real) (y real))
  (let ((children (%children record)))
    (cond ((null children) nil)
          ((output-record-p children)
           (let ((child children))
             (multiple-value-bind (left top right bottom)
                 (box-edges child)
               (transform-coordinates! (sheet-transform child) left top right bottom)
               (when (ltrb-contains-position? left top right bottom x y)
                 (funcall function child)))))
          (t
           ;; Walk from the top to the bottom of the Z-ordering
           (without-bounds-checks
	     (loop for i from (- (length children) 1) downto 0 by 1 ;;-1
		do (let ((child (aref children i)))
		     (multiple-value-bind (left top right bottom)
			 (box-edges child)
		       (transform-coordinates! (sheet-transform child) left top right bottom)
		       (when (ltrb-contains-position? left top right bottom x y)
			 (funcall function child))))))))))


#||
// The region is in the coordinate system of the sequence record
define method do-children-overlapping-region
    (function :: <function>, record :: <sequence-record>, region :: <region>) => ()
  let children = record.%children;
  case
    children == #f =>
      #f;
    output-record?(children) =>
      let child = children;
      if (everywhere?(region))
	function(child)
      else
	let (left, top, right, bottom) = box-edges(region);
	let (cleft, ctop, cright, cbottom) = box-edges(child);
	transform-coordinates!(sheet-transform(child),
			       cleft, ctop, cright, cbottom);
	when (ltrb-intersects-ltrb?(left, top, right, bottom,
				    cleft, ctop, cright, cbottom))
	  function(child)
	end
      end;
    otherwise =>
      if (everywhere?(region))
	do(function, children)
      else
	let (left, top, right, bottom) = box-edges(region);
	// Walk from the bottom to the top of the Z-ordering
	without-bounds-checks
	  for (i :: <integer> from 0 below size(children))
	    let child = children[i];
	    let (cleft, ctop, cright, cbottom) = box-edges(child);
	    transform-coordinates!(sheet-transform(child),
				   cleft, ctop, cright, cbottom);
	    when (ltrb-intersects-ltrb?(left, top, right, bottom,
					cleft, ctop, cright, cbottom))
	      function(child)
	    end
	  end
	end
      end
  end
end method do-children-overlapping-region;
||#

;; The region is in the coordinate system of the sequence record
(defmethod do-children-overlapping-region ((function function) (record <sequence-record>) (region <region>))
  (let ((children (%children record)))
    (cond ((null children) nil)
          ((output-record-p children)
           (let ((child children))
             (if (everywhere? region)
                 (funcall function child)
                 ;; else
                 (multiple-value-bind (left top right bottom)
                     (box-edges region)
                   (multiple-value-bind (cleft ctop cright cbottom)
                       (box-edges child)
                     (transform-coordinates! (sheet-transform child)
                                             cleft ctop cright cbottom)
                     (when (ltrb-intersects-ltrb? left top right bottom
                                                  cleft ctop cright cbottom)
                       (funcall function child)))))))
          (t
           (if (everywhere? region)
               (map nil function children)
               ;; else
               (multiple-value-bind (left top right bottom)
                   (box-edges region)
                 ;; Walk from the bottom to the top of the Z-ordering
                 (without-bounds-checks
                   (loop for i from 0 below (length children)
		      do (let ((child (aref children i)))
			   (multiple-value-bind (cleft ctop cright cbottom)
			       (box-edges child)
			     (transform-coordinates! (sheet-transform child)
						     cleft ctop cright cbottom)
			     (when (ltrb-intersects-ltrb? left top right bottom
							  cleft ctop cright cbottom)
			       (funcall function child))))))))))))


#||
define method do-raise-sheet
    (parent :: <sequence-record>, record :: <output-record>, #key activate?) => ()
  ignore(activate?);
  remove!(parent.%children, record);
  insert-at!(parent.%children, record, #"start")
end method do-raise-sheet;
||#

(defmethod do-raise-sheet ((parent <sequence-record>) (record <output-record>) &key activate?)
  (declare (ignore activate?))
  ;; XXX: safe *ONLY* when SEQUENCE is a VECTOR... 
  ;; (DELETE SEQUENCE ITEM)
  (setf (%children parent) (delete (%children parent) record))
  (setf (%children parent) (insert-at! (%children parent) record :start)))


#||
define method do-lower-sheet
    (parent :: <sequence-record>, record :: <output-record>) => ()
  remove!(parent.%children, record);
  insert-at!(parent.%children, record, #"end")
end method do-lower-sheet;
||#

(defmethod do-lower-sheet ((parent <sequence-record>) (record <output-record>))
  (setf (%children parent) (delete (%children parent) record))
  (setf (%children parent) (insert-at! (%children parent) record :end)))



#||

/// Sequence output history

define sealed class <sequence-output-history>
  (<output-history-mixin>, <sequence-record>)
end class <sequence-output-history>;
||#

(defclass <sequence-output-history>
    (<output-history-mixin> <sequence-record>)
  ())

#||
define sealed domain make (singleton(<sequence-output-history>));
define sealed domain initialize (<sequence-output-history>);
||#



