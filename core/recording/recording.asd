;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

(asdf:defsystem :recording
  :description "DUIM output recording"
  :author      "Scott McKay (Lisp port: Duncan Rose <duncan@robotcat.demon.co.uk>)"
  :version     (:read-file-form "version.sexp")
  :licence     "BSD-2-Clause"
  :depends-on (#:duim-utilities
	       #:geometry
	       #:dcs
	       #:sheets
	       #:graphics
	       #:layouts
	       #:gadgets
	       #:frames)
  :components
  ((:file "package")
   (:file "recording-defs")
   (:file "recording-macros")
   (:file "output-recording-mixin")
   (:file "recording-classes")
   (:file "recording-sheets")
   (:file "sequence-record")
   (:file "tree-record")
   (:file "figure-recording")
   (:file "path-recording")
   (:file "gadget-record")
   (:file "tracking-pointer")
   #+(or)
   (:file "recording-tests"))
  :serial t)
