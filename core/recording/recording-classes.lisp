;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-RECORDING-INTERNALS -*-
(in-package :duim-recording-internals)

#||
/// Output record element mixin

// A mixin for output records that can be stored by other output records --
// in practice, _all_ output records use this.
// Note that this must follow <composite-output-record-mixin> in any CPL.
//---*** Do we need a port/graft/sheet slot for use by gadget records, etc?
define open abstract class <output-record-element-mixin> (<abstract-output-record>)
  sealed slot sheet-parent :: false-or(<output-record>) = #f,
    init-keyword: parent:,
    setter: %parent-setter;
  sealed slot sheet-region :: <region> = make-bounding-box(0, 0, 0, 0),
    init-keyword: region:,
    setter: %region-setter;
  sealed slot sheet-transform :: <transform> = make-translation-transform(0, 0),
    init-keyword: transform:,
    setter: %transform-setter;
  // We cache the device transform, but the device region is so rarely
  // used it's not worth the extra slot
  sealed slot %device-transform :: false-or(<transform>) = #f;
  sealed slot default-background :: false-or(<ink>) = #f,
    init-keyword: background:,
    setter: %background-setter;
  sealed slot record-redisplay-state = #f;
end class <output-record-element-mixin>;
||#

;;; FIXME: CHECK THIS DEFCLASS

(defclass <output-record-element-mixin>
    (<abstract-output-record>)
  ((sheet-parent :type (or null <output-record>) :initarg :parent :initform nil :reader sheet-parent :writer %parent-setter)
   (sheet-region :type <region> :initarg :region :initform (make-bounding-box 0 0 0 0) :reader sheet-region :writer %region-setter)
   (sheet-transform :type <transform> :initarg :transform :initform (make-translation-transform 0 0)
		    :reader sheet-transform :writer %transform-setter)
   ;; We cache the device transform, but the device region is so rarely
   ;; used it's not worth the extra slot
   (%device-transform :type (or null <transform>) :initform nil :accessor %device-transform)
   (default-background :type (or null <ink>) :initarg :background :initform nil :reader default-background :writer %background-setter)
   (record-redisplay-state :initform nil :accessor record-redisplay-state))
  (:documentation
"
A mixin for output records that can be stored by other output records --
in practice, _all_ output records use this.
Note that this must follow <COMPOSITE-OUTPUT-RECORD-MIXIN> in any CPL.
")
  (:metaclass <abstract-metaclass>))


(defgeneric repaint-background (record medium))
(defgeneric sheet-child-count (record &key fast?))
(defgeneric note-child-added-1 (record child))
(defgeneric note-child-removed-1 (record child))
(defgeneric normalize-repaint-region (region sheet)
  (:documentation
"
Return a new (mutable) bounding box for the region if possible,
otherwise just return an immutable bounded region
"))
(defgeneric update-region-for-new-child (record child)
  (:documentation
"
Updates the region of RECORD to be large enough to hold the new child,
recursively propagating the region expansion up the output record tree.
"))
(defgeneric update-region-for-changed-child (record child old-left old-top old-right old-bottom)
  (:documentation
"
Updates the region of RECORD to be reflect the change in the child's size,
growing or shrinking RECORD's region as necessary, and recursively propagating
the region change up the output record tree.
Child's old edges are passed in parent's coordinate system because their
reference point may have changed.
"))
(defgeneric update-region-for-changed-child-1 (record))
(defgeneric recompute-region (record)
  (:documentation
"
This is for adjusting extents after a bunch of leaves have been moved.
It starts by recomputing all the regions _down_ the output record tree,
then notifying _up_ the tree if there was a change.
"))
(defgeneric do-recompute-region (record))
(defgeneric copy-display-state (record old-is-ok?))
(defgeneric recompute-contents-ok (record))
(defgeneric find-child-output-record (record use-old-children? record-class &rest initargs &key))


#||
define method do-destroy-sheet (record :: <output-record-element-mixin>) => ()
  #f
end method do-destroy-sheet;
||#

(defmethod do-destroy-sheet ((record <output-record-element-mixin>))
  nil)


#||
define method default-background-setter
    (background :: false-or(<ink>), record :: <output-record-element-mixin>)
 => (background :: false-or(<ink>))
  record.%background := background;
  // If we're removing background highlighting, fill in the old background by hand
  unless (background)
    let sheet = top-level-sheet(record);
    let medium = sheet-medium(sheet);
    dynamic-bind (medium-brush(medium) = medium-background(medium),
		  medium-pen(medium)   = $solid-pen,
		  medium-clipping-region(medium) = $everywhere)
      let (left, top, right, bottom) = box-edges(record);
      transform-coordinates!(sheet-device-transform(record),
			     left, top, right, bottom);
      draw-rectangle(medium, left, top, right, bottom, filled?: #t)
    end
  end;
  repaint-sheet(record, $everywhere);
  background
end method default-background-setter;
||#

(defmethod (setf default-background) ((background null) (record <output-record-element-mixin>))
  (%background-setter background record)
  ;; If we're removing background highlighting, fill in the old background by hand
  (unless background
    (let* ((sheet (top-level-sheet record))
	   (medium (sheet-medium sheet)))
      (dynamic-bind (((medium-brush medium) = (medium-background medium))
		     ((medium-pen medium)   = *solid-pen*)
		     ((medium-clipping-region medium) = *everywhere*))
        (multiple-value-bind (left top right bottom)
	    (box-edges record)
	  (transform-coordinates! (sheet-device-transform record)
				  left top right bottom)
	  (draw-rectangle medium left top right bottom :filled? t)))))
  (repaint-sheet record *everywhere*)
  background)

(defmethod (setf default-background) ((background <ink>) (record <output-record-element-mixin>))
  (%background-setter background record)
  ;; If we're removing background highlighting, fill in the old background by hand
  (unless background
    (let* ((sheet (top-level-sheet record))
	   (medium (sheet-medium sheet)))
      (dynamic-bind (((medium-brush medium) = (medium-background medium))
		     ((medium-pen medium)   = *solid-pen*)
		     ((medium-clipping-region medium) = *everywhere*))
        (multiple-value-bind (left top right bottom)
	    (box-edges record)
	  (transform-coordinates! (sheet-device-transform record)
				  left top right bottom)
	  (draw-rectangle medium left top right bottom :filled? t)))))
  (repaint-sheet record *everywhere*)
  background)


#||
// Most 'handle-repaint' methods start with this in order to display their
// backgrounds correctly.  Note that this function gets called with the
// medium's transform set up to cause drawing to happen in the right place.
define method repaint-background
    (record :: <output-record-element-mixin>, medium :: <medium>) => ()
  dynamic-bind (medium-brush(medium) = default-background(record),
		medium-pen(medium)   = $solid-pen,
		//---*** This is not right, methinks (see 'with-record-medium-state')
		medium-clipping-region(medium) = $everywhere)
    let (left, top, right, bottom) = box-edges(record);
    draw-rectangle(medium, left, top, right, bottom, filled?: #t)
  end
end method repaint-background;
||#

;; Most 'handle-repaint' methods start with this in order to display their
;; backgrounds correctly. Note that this function gets called with the
;; medium's transform set up to cause drawing to happen in the right place.
(defmethod repaint-background ((record <output-record-element-mixin>) (medium <medium>))
  (dynamic-bind (((medium-brush medium) = (default-background record))
		 ((medium-pen medium)   = *solid-pen*)
		 ;;---*** This is not right, methinks (see 'with-record-medium-state')
		 ((medium-clipping-region medium) =  *everywhere*))
    (multiple-value-bind (left top right bottom)
	(box-edges record)
      (draw-rectangle medium left top right bottom :filled? t))))


#||
define method sheet-parent-setter
    (parent :: <output-record>, record :: <output-record-element-mixin>)
 => (parent :: <output-record>)
  add-child(parent, record);
  parent
end method sheet-parent-setter;
||#

(defmethod (setf sheet-parent) ((parent <output-record>) (record <output-record-element-mixin>))
  (add-child parent record)
  parent)


#||
define method sheet-parent-setter
    (parent == #f, record :: <output-record-element-mixin>)
 => (parent :: singleton(#f))
  remove-child(sheet-parent(record), record);
  #f
end method sheet-parent-setter;
||#

(defmethod (setf sheet-parent) ((parent null) (record <output-record-element-mixin>))
  (remove-child (sheet-parent record) record)
  nil)


#||
// By default, output records have no children
define method sheet-children
    (record :: <output-record-element-mixin>) => (children :: <vector>)
  #[]
end method sheet-children;
||#

;; By default, output records have no children
(defmethod sheet-children ((record <output-record-element-mixin>))
  #())


#||
define method sheet-child-count
    (record :: <output-record-element-mixin>, #key fast?) => (count :: <integer>)
  ignore(fast?);
  0
end method sheet-child-count;
||#

(defmethod sheet-child-count ((record <output-record-element-mixin>) &key fast?)
  (declare (ignore fast?))
  0)


#||
//---*** Shouldn't allow setting the region of a leaf record this way!
define method sheet-region-setter
    (region :: <region>, record :: <output-record-element-mixin>) => (region :: <region>)
  record.%region := region;
  note-region-changed(record);
  region
end method sheet-region-setter;
||#

;;---*** Shouldn't allow setting the region of a leaf record this way!
(defmethod (setf sheet-region) ((region <region>) (record <output-record-element-mixin>))
  (%region-setter region record)
  (note-region-changed record)
  region)


#||
define method note-region-changed
    (record :: <output-record-element-mixin>) => ()
  // Don't do anything.  Let the layout protocol take care of notifying
  // our parent of relevant changes.
  #f
end method note-region-changed;
||#

(defmethod note-region-changed ((record <output-record-element-mixin>))
  ;; Don't do anything.  Let the layout protocol take care of notifying
  ;; our parent of relevant changes.
  nil)


#||
define method box-edges
    (record :: <output-record-element-mixin>)
 => (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
  box-edges(sheet-region(record))
end method box-edges;
||#

(defmethod box-edges ((record <output-record-element-mixin>))
  (box-edges (sheet-region record)))


#||
define method sheet-transform-setter
    (transform :: <transform>, record :: <output-record-element-mixin>)
 => (transform :: <transform>)
  record.%transform := transform;
  record.%device-transform := #f;
  note-transform-changed(record);
  transform
end method sheet-transform-setter;
||#


(defmethod (setf sheet-transform) ((transform <transform>) (record <output-record-element-mixin>))
  (%transform-setter transform record)
  (setf (%device-transform record) nil)
  (note-transform-changed record)
  transform)


#||
define method note-transform-changed
    (record :: <output-record-element-mixin>) => ()
  // Don't do anything.  Let the layout protocol take care of notifying
  // our parent of relevant changes.
  #f
end method note-transform-changed;
||#

(defmethod note-transform-changed ((record <output-record-element-mixin>))
  ;; Don't do anything.  Let the layout protocol take care of notifying
  ;; our parent of relevant changes.
  nil)


#||
define method sheet-device-region
    (record :: <output-record>) => (region :: <region>)
  transform-region(sheet-device-transform(record), sheet-region(record))
end method sheet-device-region;
||#


(defmethod sheet-device-region ((record <output-record>))
  (transform-region (sheet-device-transform record) (sheet-region record)))


#||
define method invalidate-cached-region (record :: <output-record>) => ()
  #f
end method invalidate-cached-region;
||#

(defmethod invalidate-cached-region ((record <output-record>))
  nil)


#||
define method sheet-device-transform
    (record :: <output-record>) => (transform :: <transform>)
  record.%device-transform
  | begin
      let dx = 0;
      let dy = 0;
      let rec = record;
      let parent = record;
      block (return)
	while (#t)
	  parent := sheet-parent(parent);
	  unless (parent)
	    return()
	  end;
	  // If this record has a parent, transform the (dx,dy) by the
	  // sheet->parent transform
	  transform-coordinates!(sheet-transform(rec), dx, dy);
	  rec := parent
	end
      end;
      // Cache and return the result
      record.%device-transform := make-translation-transform(dx, dy)
    end
end method sheet-device-transform;
||#

(defmethod sheet-device-transform ((record <output-record>))
  (or (%device-transform record)
      (let ((dx 0)
	    (dy 0)
	    (rec record)
	    (parent record))
	(loop while t
	   do (setf parent (sheet-parent parent))
	   do (unless parent
		(return nil))
	   ;; If this record has a parent, transform the (dx,dy) by the
	   ;; sheet->parent transform
	   do (transform-coordinates! (sheet-transform rec) dx dy)
	   do (setf rec parent))
	;; Cache and return the result
	(setf (%device-transform record) (make-translation-transform dx dy)))))


#||
define method invalidate-cached-transform (record :: <output-record>) => ()
  record.%device-transform := #f
end method invalidate-cached-transform;
||#

(defmethod invalidate-cached-transform ((record <output-record>))
  (setf (%device-transform record) nil))


#||
define method sheet-delta-transform
    (record :: <output-record>, ancestor :: <output-record>) => (transform :: <transform>)
  if (sheet-parent(record) == ancestor)
    sheet-transform(record)
  else
    let dx = 0;
    let dy = 0;
    let parent = record;
    block (return)
      while (#t)
        parent := sheet-parent(parent);
        unless (parent)
          error("The sheet %= has no parent", record)
        end;
        transform-coordinates!(sheet-transform(record), dx, dy);
        record := parent;
        when (record == ancestor)
          return()
        end
      end
    end;
    make-translation-transform(dx, dy)
  end
end method sheet-delta-transform;
||#

(defmethod sheet-delta-transform ((record <output-record>) (ancestor <output-record>))
  (if (eql (sheet-parent record) ancestor)
      (sheet-transform record)
      ;; (else)
      (let ((dx 0)
	    (dy 0)
	    (parent record))
	(loop while t
	   do (setf parent (sheet-parent parent))
	   do (unless parent
		(error "The sheet ~a has no parent" record))
	   do (transform-coordinates! (sheet-transform record) dx dy)
	   do (setf record parent)
	   do (when (eq record ancestor)
		(return nil)))
	(make-translation-transform dx dy))))


#||
define method do-sheet-children
    (function :: <function>, record :: <output-record>,
     #key z-order :: <z-order> = #f) => ()
  #f
end method do-sheet-children;
||#

(defmethod do-sheet-children ((function function) (record <output-record>)
			      &key (z-order nil))
  (declare (ignore z-order))
  nil)


#||
define method do-sheet-tree (function :: <function>, record :: <output-record>) => ()
  dynamic-extent(function);
  function(record)
end method do-sheet-tree;
||#

(defmethod do-sheet-tree ((function function) (record <output-record>))
  (declare (dynamic-extent function))
  (funcall function record))


#||
// For output records, this returns the sheet that holds the output history
define method top-level-sheet
    (record :: <output-record-element-mixin>) => (sheet :: false-or(<sheet>))
  block (return)
    while (#t)
      unless (record)
        return(#f)
      end;
      when (output-history?(record))
        return(output-history-sheet(record))
      end;
      record := sheet-parent(record)
    end
  end
end method top-level-sheet;
||#

;; For output records, this returns the sheet that holds the output history
(defmethod top-level-sheet ((record <output-record-element-mixin>))
  (loop while t
     do (unless record
	  (return-from top-level-sheet nil))
     do (when (output-history? record)
	  (return-from top-level-sheet (output-history-sheet record)))
     do (setf record (sheet-parent record))))


#||
define method sheet-attached?
    (record :: <output-record-element-mixin>)
 => (mapped? :: <boolean>)
  top-level-sheet(record) & #t
end method sheet-attached?;
||#

(defmethod sheet-attached? ((record <output-record-element-mixin>))
  (and (top-level-sheet record) t))


#||
define method sheet-mapped?
    (record :: <output-record-element-mixin>)
 => (mapped? :: <boolean>)
  top-level-sheet(record) & #t
end method sheet-mapped?;
||#

(defmethod sheet-mapped? ((record <output-record-element-mixin>))
  (and (top-level-sheet record) t))


#||
define method sheet-mapped?-setter
    (mapped? :: <boolean>, record :: <output-record-element-mixin>, 
     #key do-repaint? = #t, clear? = do-repaint?)
 => (mapped? :: <boolean>)
  ignore(mapped?, do-repaint?, clear?);
  error("You can't explicitly set the 'mapped?' flag for output records")
end method sheet-mapped?-setter;
||#

(defmethod (setf sheet-mapped?) (mapped? (record <output-record-element-mixin>)
				 &key (do-repaint? t) (clear? do-repaint?))
  (declare (type boolean mapped?)
	   (ignore mapped? clear?))
  (error "You can't explicitly set the 'mapped?' flag for output records"))


#||
define method sheet-cursor
    (record :: <output-record-element-mixin>) => (cursor :: <cursor>)
  #"default"
end method sheet-cursor;
||#

(defmethod sheet-cursor ((record <output-record-element-mixin>))
  :default)


#||
define method sheet-cursor-setter
    (cursor :: <cursor>, record :: <output-record-element-mixin>) => (cursor :: <cursor>)
  cursor
end method sheet-cursor-setter;
||#

(defmethod (setf sheet-cursor) (cursor (record <output-record-element-mixin>))
  (check-type cursor <cursor>)
  cursor)


#||
define method sheet-caret
    (record :: <output-record-element-mixin>) => (caret :: singleton(#f))
  #f
end method sheet-caret;
||#

(defmethod sheet-caret ((record <output-record-element-mixin>))
  nil)


#||
define method raise-sheet
    (record :: <output-record-element-mixin>, #key activate? = #t)
 => (record :: <output-record>)
  let parent = sheet-parent(record);
  when (parent)
    do-raise-sheet(parent, record, activate?: activate?)
  end;
  repaint-sheet(parent, record);
  record
end method raise-sheet;
||#

(defmethod raise-sheet ((record <output-record-element-mixin>) &key (activate? t))
  (let ((parent (sheet-parent record)))
    (when parent
      (do-raise-sheet parent record :activate? activate?))
    (repaint-sheet parent record)
    record))


#||
define method lower-sheet
    (record :: <output-record-element-mixin>) => (record :: <output-record>)
  let parent = sheet-parent(record);
  when (parent)
    do-lower-sheet(parent, record)
  end;
  repaint-sheet(parent, record);
  record
end method lower-sheet;
||#

(defmethod lower-sheet ((record <output-record-element-mixin>))
  (let ((parent (sheet-parent record)))
    (when parent
      (do-lower-sheet parent record))
    (repaint-sheet parent record)
    record))


#||
// For specialization by graphics output records, for example.
// (X,Y) is in the coordinate system of the record
define open generic refined-position-test
    (record :: <abstract-output-record>, x, y) => (true? :: <boolean>);
||#

;; For specialization by graphics output records, for example
;; (X,Y) is in the coordinate system of the record
(defgeneric refined-position-test (record x y)
  (:documentation
"
For specialization by graphics output records, for example.
 (X, Y) is in the coordinate system of the record.
"))


#||
define method refined-position-test
    (record :: <output-record-element-mixin>, x, y) => (true? :: <boolean>)
  ignore(x, y);
  #t
end method refined-position-test;
||#

(defmethod refined-position-test ((record <output-record-element-mixin>) x y)
  (declare (ignore x y))
  t)


#||
define constant $highlighting-pen :: <standard-pen> = make(<pen>, width: 1);
||#

(defvar *highlighting-pen* (make-pen :width 1))


#||
define open generic highlight-output-record
    (record :: <abstract-output-record>, sheet :: <abstract-sheet>, state) => ();
||#

(defgeneric highlight-output-record (record sheet state)
  (:documentation
"
For specialization by graphics output records.
_state_ is :HIGHLIGHT or :UNHIGHLIGHT.
"))


#||
// For specialization by graphics output records, for example
define method highlight-output-record
    (record :: <output-record-element-mixin>, sheet :: <output-recording-mixin>, state) => ()
  // State is HIGHLIGHT: or UNHIGHLIGHT:
  ignore(state);
  let medium = sheet-medium(sheet);
  let (left, top, right, bottom) = box-edges(record);
  //--- Would it be better to recursively maintain the proper transform
  //--- on the medium itself?  This would require 'do-highlight' methods...
  let transform = sheet-device-transform(record);
  transform-coordinates!(transform, left, top, right, bottom);
  with-drawing-options (medium, /* ---*** brush: $xor-brush, */ pen: $highlighting-pen)
    draw-rectangle(medium, left, top, right, bottom, filled?: #f)
  end
end method highlight-output-record;
||#

;; For specialization by graphics output records, for example.
(defmethod highlight-output-record ((record <output-record-element-mixin>) (sheet <output-recording-mixin>) state)
  ;; State is HIGHLIGHT: or UNHIGHLIGHT:
  (declare (ignore state))
  (let ((medium (sheet-medium sheet)))
    (multiple-value-bind (left top right bottom)
	(box-edges record)
      ;;--- Would it be better to recursively maintain the proper transform
      ;;--- on the medium itself?  This would require 'do-highlight' methods...
      (let ((transform (sheet-device-transform record)))
	(transform-coordinates! transform left top right bottom)
	(with-drawing-options (medium #||/* ---*** brush: $xor-brush, */||# :pen *highlighting-pen*)
	  (draw-rectangle medium left top right bottom :filled? nil))))))



#||

/// Getting and setting output record positions

// Returns sheet's edges relative to its parent
define method sheet-edges
    (record :: <output-record-element-mixin>)
 => (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
  let (left, top, right, bottom) = box-edges(record);
  transform-box(sheet-transform(record), left, top, right, bottom)
end method sheet-edges;
||#

;; Returns the sheet's edges relative to its parent
(defmethod sheet-edges ((record <output-record-element-mixin>))
  (multiple-value-bind (left top right bottom)
      (box-edges record)
    (transform-box (sheet-transform record) left top right bottom)))


#||
// Sets the sheet's edges.  LTRB is relative to parent
define method set-sheet-edges
    (record :: <output-record-element-mixin>,
     left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>) => ()
  // Untransform the edges w.r.t. the parent transform
  let (left, top, right, bottom)
    = untransform-box(sheet-transform(record), left, top, right, bottom);
  // Modify the existing region (if possible) and note the change
  sheet-region(record)
    := set-box-edges(sheet-region(record), left, top, right, bottom);
  // We don't cache regions, but we have to invalidate transforms
  invalidate-cached-transforms(record)
end method set-sheet-edges;
||#

;; Sets the sheet's edges. LTRB is relative to parent
(defmethod set-sheet-edges ((record <output-record-element-mixin>)
			    left top right bottom)
  ;; Untransform the edges w.r.t. the parent transform
  (multiple-value-bind (left top right bottom)
      (untransform-box (sheet-transform record) left top right bottom)
    ;; Modify the existing region (if possible) and note the change
    (setf (sheet-region record)
	  (set-box-edges (sheet-region record) left top right bottom)))
  ;; We don't cache regions, but we have to invalidate transforms
  (invalidate-cached-transforms record))


#||
// Returns the sheet position relative to its parent
define method sheet-position
    (record :: <output-record-element-mixin>)
 => (x :: <integer>, y :: <integer>)
  let (x, y) = box-position(sheet-region(record));
  transform-position(sheet-transform(record), x, y)
end method sheet-position;
||#

;; Returns the sheet position relative to its parent
(defmethod sheet-position ((record <output-record-element-mixin>))
  (multiple-value-bind (x y)
      (box-position (sheet-region record))
    (transform-position (sheet-transform record) x y)))


#||
// Sets the sheet's position by hacking its transform.  XY is relative to parent
define method set-sheet-position
    (record :: <output-record-element-mixin>, x :: <integer>, y :: <integer>) => ()
  // Modify the existing transform (if possible) and note the change.
  // Note that, since the position is encoded in both the transform and
  // the region, we need to be a bit careful
  let (old-x, old-y) = sheet-position(record);
  let dx = x - old-x;
  let dy = y - old-y;
  sheet-transform(record)
    := compose-translation-into!(dx, dy, sheet-transform(record));
  invalidate-cached-transforms(record)
end method set-sheet-position;
||#

;; Sets the sheet's position by hacking its transform. XY is relative to parent
(defmethod set-sheet-position ((record <output-record-element-mixin>) x y)
  ;; Modify the existing transform (if possible) and note the change.
  ;; Note that, since the position is encoded in both the transform and
  ;; the region, we need to be a bit careful
  (multiple-value-bind (old-x old-y)
      (sheet-position record)
    (let ((dx (- x old-x))
	  (dy (- y old-y)))
      (setf (sheet-transform record)
            (compose-translation-into! dx dy (sheet-transform record)))))
  (invalidate-cached-transforms record))


#||
define method sheet-size
    (record :: <output-record-element-mixin>)
 => (width :: <integer>, height :: <integer>)
  box-size(sheet-region(record))
end method sheet-size;
||#

(defmethod sheet-size ((record <output-record-element-mixin>))
  (box-size (sheet-region record)))


#||
define method set-sheet-size
    (record :: <output-record-element-mixin>, width :: <integer>, height :: <integer>) => ()
  let (left, top, right, bottom) = box-edges(record);
  ignore(right, bottom);
  let right  = left + width;
  let bottom = top  + height;
  // Modify the existing region (if possible) and note the change.
  // The new size gets added to the right and bottom
  sheet-region(record)
    := set-box-edges(sheet-region(record), left, top, right, bottom)
end method set-sheet-size;
||#

(defmethod set-sheet-size ((record <output-record-element-mixin>) width height)
  (multiple-value-bind (left top right bottom)
      (box-edges record)
    (declare (ignore right bottom))
    (let ((right (+ left width))
	  (bottom (+ top height)))
      ;; Modify the existing region (if possible) and note the change.
      ;; The new size gets added to the right and bottom
      (setf (sheet-region record)
            (set-box-edges (sheet-region record) left top right bottom)))))



#||

/// Composite output record mixin

// Note that this must precede <output-record-element-mixin> in any CPL.
define open abstract class <composite-output-record-mixin> (<abstract-output-record>)
end class <composite-output-record-mixin>;
||#

(defclass <composite-output-record-mixin>
    (<abstract-output-record>)
  ()
  (:documentation
"
Note that this must precede <OUTPUT-RECORD-ELEMENT-MIXIN> in any CPL.
")
  (:metaclass <abstract-metaclass>))


#||
define method add-child
    (record :: <output-record>, child :: <output-record>, #rest keys, #key index)
 => (record :: <output-record>)
  ignore(index);
  assert(~sheet-parent(child),
         "The record %= already has a parent", child);
  apply(do-add-child, record, child, keys);
  note-child-added(record, child);
  record
end method add-child;
||#

(defmethod add-child ((record <output-record>) (child <output-record>) &rest keys &key index)
  (declare (ignore index))
  (when (sheet-parent child)
    (error "The record ~a already has a parent" child))
  (apply #'do-add-child record child keys)
  (note-child-added record child)
  record)


#||
define method note-child-added
    (record :: <output-record>, child :: <output-record>) => ()
  note-child-added-1(record, child)
end method note-child-added;
||#

(defmethod note-child-added ((record <output-record>) (child <output-record>))
  (note-child-added-1 record child))


#||
define method note-child-added
    (record :: <composite-output-record-mixin>, child :: <output-record>) => ()
  next-method();
  update-region-for-new-child(record, child)
end method note-child-added;
||#

(defmethod note-child-added ((record <composite-output-record-mixin>) (child <output-record>))
  (call-next-method)
  (update-region-for-new-child record child))


#||
define method note-child-added-1
    (record :: <output-record>, child :: <output-record>) => ()
  #f
end method note-child-added-1;
||#

(defmethod note-child-added-1 ((record <output-record>) (child <output-record>))
  nil)


#||
define method note-child-added-1
    (record :: <output-record>, child :: <output-record-element-mixin>) => ()
  next-method();
  child.%parent := record
end method note-child-added-1;
||#

(defmethod note-child-added-1 ((record <output-record>) (child <output-record-element-mixin>))
  (call-next-method)
  (%parent-setter record child))


#||
define method remove-child
    (record :: <output-record>, child :: <output-record>)
 => (record :: <output-record>)
  assert(sheet-parent(child) == record,
         "The sheet %= is not a child of %=", child, record);
  do-remove-child(record, child);
  note-child-removed(record, child);
  record
end method remove-child;
||#

(defmethod remove-child ((record <output-record>) (child <output-record>))
  (unless (eq (sheet-parent child) record)
    (error "The sheet ~a is not a child of ~a" child record))
  (do-remove-child record child)
  (note-child-removed record child)
  record)


#||
define method note-child-removed
    (record :: <output-record>, child :: <output-record>) => ()
  note-child-removed-1(record, child)
end method note-child-removed;
||#

(defmethod note-child-removed ((record <output-record>) (child <output-record>))
  (note-child-removed-1 record child))


#||
define method note-child-removed
    (record :: <composite-output-record-mixin>, child :: <output-record>) => ()
  next-method();
  let (cleft, ctop, cright, cbottom) = box-edges(child);
  transform-coordinates!(sheet-transform(child), cleft, ctop, cright, cbottom);
  update-region-for-changed-child(record, child, cleft, ctop, cright, cbottom)
end method note-child-removed;
||#

(defmethod note-child-removed ((record <composite-output-record-mixin>) (child <output-record>))
  (call-next-method)
  (multiple-value-bind (cleft ctop cright cbottom)
      (box-edges child)
    (transform-coordinates! (sheet-transform child) cleft ctop cright cbottom)
    (update-region-for-changed-child record child cleft ctop cright cbottom)))


#||
define method note-child-removed-1
    (record :: <output-record>, child :: <output-record>) => ()
  #f
end method note-child-removed-1;
||#

(defmethod note-child-removed-1 ((record <output-record>) (child <output-record>))
  nil)


#||
define method note-child-removed-1
    (record :: <output-record>, child :: <output-record-element-mixin>) => ()
  next-method();
  child.%parent := #f
end method note-child-removed-1;
||#

(defmethod note-child-removed-1 ((record <output-record>) (child <output-record-element-mixin>))
  (call-next-method)
  (%parent-setter nil child))


#||
define method replace-child
    (record :: <output-record>, old-child :: <output-record>, new-child :: <output-record>)
 => (record :: <output-record>)
  do-replace-child(record, old-child, new-child);
  note-child-removed(record, old-child);
  note-child-added(record, new-child);
  record
end method replace-child;
||#

(defmethod replace-child ((record <output-record>) (old-child <output-record>) (new-child <output-record>))
  (do-replace-child record old-child new-child)
  (note-child-removed record old-child)
  (note-child-added record new-child)
  record)


#||
define method child-containing-position
    (record :: <output-record>, x :: <real>, y :: <real>)
 => (child :: false-or(<output-record>))
  block (return)
    do-children-containing-position
      (method (child) return(child) end,
       record, x, y);
    #f
  end
end method child-containing-position;
||#

(defmethod child-containing-position ((record <output-record>) (x real) (y real))
  (do-children-containing-position
      #'(lambda (child)
	  (return-from child-containing-position child))
    record x y)
  nil)


#||
define method children-overlapping-region
    (record :: <output-record>, region :: <region>) => (children :: <sequence>)
  let children :: <stretchy-object-vector> = make(<stretchy-vector>);
  do-children-overlapping-region
    (method (child) add!(children, child) end,
     record, region);
  children
end method children-overlapping-region;
||#

(defmethod children-overlapping-region ((record <output-record>) (region <region>))
  (let ((children (make-array 0 :adjustable t :fill-pointer t)))
    (do-children-overlapping-region #'(lambda (child)
					(add! children child))
      record region)
    children))


#||
//---*** Does setting the edges of a composite need to notify any kids?
define method set-sheet-edges
    (record :: <composite-output-record-mixin>,
     left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>) => ()
  next-method()
end method set-sheet-edges;
||#

;;---*** Does setting the edges of a composite need to notify any kids?
(defmethod set-sheet-edges ((record <composite-output-record-mixin>)
			    (left integer) (top integer) (right integer) (bottom integer))
  (call-next-method))


#||
//---*** Does setting the edges of a composite need to notify any kids?
//---*** Yes, in order to cause gadget records to move properly...
define method set-sheet-position
    (record :: <composite-output-record-mixin>, x :: <integer>, y :: <integer>) => ()
  next-method()
end method set-sheet-position;
||#

;;---*** Does setting the edges of a composite need to notify any kids?
;;---*** Yes, in order to cause gadget records to move properly...
(defmethod set-sheet-position ((record <composite-output-record-mixin>) (x integer) (y integer))
  (call-next-method))


#||
//---*** Does setting the size of a composite need to notify any kids?
define method set-sheet-size
    (record :: <composite-output-record-mixin>, width :: <integer>, height :: <integer>) => ()
  next-method()
end method set-sheet-size;
||#

;;---*** Does setting the size of a composite need to notify any kids?
(defmethod set-sheet-size ((record <composite-output-record-mixin>) (width integer) (height integer))
  (call-next-method))


#||
define method raise-sheet
    (record :: <output-record>, #key activate? = #t)
 => (record :: <output-record>)
  let parent = sheet-parent(record);
  when (parent)
    do-raise-sheet(parent, record, activate?: activate?)
  end;
  record
end method raise-sheet;
||#

(defmethod raise-sheet ((record <output-record>) &key (activate? t))
  (let ((parent (sheet-parent record)))
    (when parent
      (do-raise-sheet parent record :activate? activate?)))
  record)


#||
define method do-raise-sheet
    (parent :: <output-record>, record :: <output-record>, #key activate? = #t) => ()
  ignore(activate?);
  #f
end method do-raise-sheet;
||#

(defmethod do-raise-sheet ((parent <output-record>) (record <output-record>) &key (activate? t))
  (declare (ignore activate?))
  nil)


#||
define method lower-sheet (record :: <output-record>) => (record :: <output-record>)
  let parent = sheet-parent(record);
  when (parent)
    do-lower-sheet(parent, record)
  end;
  record
end method lower-sheet;
||#

(defmethod lower-sheet ((record <output-record>))
  (let ((parent (sheet-parent record)))
    (when parent
      (do-lower-sheet parent record)))
  record)


#||
define method do-lower-sheet
    (parent :: <output-record>, record :: <output-record>) => ()
  #f
end method do-lower-sheet;
||#

(defmethod do-lower-sheet ((parent <output-record>) (record <output-record>))
  nil)



#||

/// Repainting

// Repainting an output record first normalizes the drawing state, then
// calls 'handle-repaint'
// This could establish a clipping region for the supplied region, but I
// think it's better to be fast.  The user can easily do that himself.
define method repaint-sheet
    (record :: <output-record>, region :: <region>, #key medium, force?) => ()
  ignore(force?);
  let sheet = top-level-sheet(record);
  let medium = medium | sheet-medium(sheet);
  let region = normalize-repaint-region(region, sheet); 
  // Put a mutable transform into the sheet's medium so that the 
  // 'handle-repaint' method on composite records can be more efficient
  let (tx, ty) = transform-position(sheet-device-transform(record), 0, 0);
  let transform = make(<mutable-translation-transform>, tx: tx, ty: ty);
  dynamic-bind (medium-transform(medium) = transform)
    handle-repaint(record, medium, region)
  end;
  let _port = port(sheet);
  when (_port)
    force-display(_port)
  end
end method repaint-sheet;
||#

;; Repainting an output record first normalizes the drawing state, then
;; calls 'handle-repaint'
;; This could establish a clipping region for the supplied region, but I
;; think it's better to be fast. The user can easily do that himself.
(defmethod repaint-sheet ((record <output-record>) (region <region>) &key medium force?)
  (declare (ignore force?))
  (let* ((sheet (top-level-sheet record))
	 (medium (or medium (sheet-medium sheet)))
	 (region (normalize-repaint-region region sheet)))
    ;; Put a mutable transform into the sheet's medium so that the 
    ;; 'handle-repaint' method on composite records can be more efficient
    (multiple-value-bind (tx ty)
	(transform-position (sheet-device-transform record) 0 0)
      (let ((transform (make-instance '<mutable-translation-transform> :tx tx :ty ty)))
	(dynamic-bind (((medium-transform medium) = transform))
          (handle-repaint record medium region))))
    (let ((_port (port sheet)))
      (when _port
	(force-display _port)))))


#||
// Return a new (mutable) bounding box for the region if possible,
// otherwise just return an immutable bounded region
define method normalize-repaint-region
    (region, sheet :: <sheet>) => (region :: <region>)
  case
    everywhere?(region) =>
      // If we've been asked to replay everything, set the replay
      // region to the viewport
      let region = sheet-viewport-region(sheet) | sheet-region(sheet);
      if (bounding-box?(region)) bounding-box(region) else region end;
    output-record?(region) =>
      // If the replay region is itself an output record, make a
      // new region in the proper coordinate system
      let (left, top, right, bottom) = box-edges(region);
      transform-coordinates!(sheet-device-transform(region),
			     left, top, right, bottom);
      make-bounding-box(left, top, right, bottom);
    otherwise =>
      if (bounding-box?(region)) bounding-box(region) else region end;
  end
end method normalize-repaint-region;
||#

;; Return a new (mutable) bounding box for the region if possible,
;; otherwise just return an immutable bounded region
(defmethod normalize-repaint-region (region (sheet <sheet>))
  (cond
    ((everywhere? region)
     ;; If we've been asked to replay everything, set the replay
     ;; region to the viewport
     (let ((region (or (sheet-viewport-region sheet) (sheet-region sheet))))
       (if (bounding-box-p region) (bounding-box region) region)))
    ((output-record-p region)
     ;; If the replay region is itself an output record, make a
     ;; new region in the proper coordinate system
     (multiple-value-bind (left top right bottom)
	 (box-edges region)
       (transform-coordinates! (sheet-device-transform region)
			       left top right bottom)
       (make-bounding-box left top right bottom)))
    (t
     (if (bounding-box-p region) (bounding-box region) region))))


#||
// All we do here is call 'handle-repaint' on the children within the region,
// keeping the medium transform set up correctly.
define method handle-repaint
    (record :: <composite-output-record-mixin>, medium :: <medium>, region :: <region>) => ()
  // If there's a non-default background, fill it in now
  when (default-background(record))
    repaint-background(record, medium)
  end;
  do-children-overlapping-region
    (method (child)
       let (cx, cy) = transform-position(sheet-transform(child), 0, 0);
       medium-transform(medium)
         := compose-translation-into!(cx, cy, medium-transform(medium));
       handle-repaint(child, medium, untransform-region(sheet-transform(child), region));
       medium-transform(medium)
         := compose-translation-into!(-cx, -cy, medium-transform(medium));
     end,
     record, region)
end method handle-repaint;
||#

;; All we do here is call 'handle-repaint' on the children within the region,
;; keeping the medium transform set up correctly.
(defmethod handle-repaint ((record <composite-output-record-mixin>) (medium <medium>) (region <region>))
  ;; If there's a non-default background, fill it in now
  (when (default-background record)
    (repaint-background record medium))
  (do-children-overlapping-region
      #'(lambda (child)
	  (multiple-value-bind (cx cy)
	      (transform-position (sheet-transform child) 0 0)
	    (setf (medium-transform medium)
		  (compose-translation-into! cx cy (medium-transform medium)))
	    (handle-repaint child medium (untransform-region (sheet-transform child) region))
	    (setf (medium-transform medium)
		  (compose-translation-into! (- cx) (- cy) (medium-transform medium)))))
    record region))


#||
// 'normalize-repaint-region' might have given us a mutable bounding box,
// in which case we can avoid consing intermediate regions
define method handle-repaint
    (record :: <composite-output-record-mixin>, medium :: <medium>, region :: <bounding-box>) => ()
  when (default-background(record))
    repaint-background(record, medium)
  end;
  do-children-overlapping-region
    (method (child)
       let transform = sheet-transform(child);
       let (cx, cy) = transform-position(transform, 0, 0);
       medium-transform(medium)
         := compose-translation-into!(cx, cy, medium-transform(medium));
       untransform-region!(transform, region);
       handle-repaint(child, medium, region);
       transform-region!(transform, region);
       medium-transform(medium)
         := compose-translation-into!(-cx, -cy, medium-transform(medium));
     end,
     record, region)
end method handle-repaint;
||#

;; 'normalize-repaint-region' might have given us a mutable bounding box,
;; in which case we can avoid consing intermediate regions
(defmethod handle-repaint ((record <composite-output-record-mixin>) (medium <medium>) (region <bounding-box>))
  (when (default-background record)
    (repaint-background record medium))
  (do-children-overlapping-region
      #'(lambda (child)
	  (let ((transform (sheet-transform child)))
	    (multiple-value-bind (cx cy)
		(transform-position transform 0 0)
	      (setf (medium-transform medium)
		    (compose-translation-into! cx cy (medium-transform medium)))
	      (untransform-region! transform region)
	      (handle-repaint child medium region)
	      (transform-region! transform region)
	      (setf (medium-transform medium)
		    (compose-translation-into! (- cx) (- cy) (medium-transform medium))))))
    record region))



#||

/// Geometry maintenance

// Updates the region of RECORD to be large enough to hold the new child,
// recursively propagating the region expansion up the output record tree.
define method update-region-for-new-child
    (record :: <output-record-element-mixin>, child) => ()
  let (rleft, rtop, rright, rbottom) = box-edges(record);
  let (cleft, ctop, cright, cbottom) = box-edges(child);
  // Get the new child's box into the record's coordinate system
  transform-coordinates!(sheet-transform(child),
			 cleft, ctop, cright, cbottom);
  let first-child?
    = sheet-child-count(record, fast?: #t) = 1;
  let growing?
    = ~first-child?
      & ~ltrb-contains-ltrb?(rleft, rtop, rright, rbottom,
			     cleft, ctop, cright, cbottom);
  case
    first-child? =>
      // If this is the first child, we must always set the
      // bounding rectangle of the parent
      sheet-region(record)
	:= set-box-edges(sheet-region(record),
			 cleft, ctop, cright, cbottom);
    growing? =>
      // Don't bother to change the edges if we're not growing
      sheet-region(record)
	:= set-box-edges(sheet-region(record),
			 min(rleft, cleft), min(rtop, ctop),
			 max(rright, cright), max(rbottom, cbottom))
  end;
  // Inform the parent only in the case where we actually grew or
  // added the initial child
  let parent = sheet-parent(record);
  when (parent & (first-child? | growing?))
    transform-coordinates!(sheet-transform(record),
			   rleft, rtop, rright, rbottom);
    update-region-for-changed-child(parent, record,
				    rleft, rtop, rright, rbottom)
  end
end method update-region-for-new-child;
||#

;;; FIXME: THIS IS BROKEN, SOMEWHERE... TRYING TO UPDATE WHEN ADDING A
;;; BEZIER-CURVE-RECORD MAKES IT GO *BOOM*

;; Updates the region of RECORD to be large enough to hold the new child,
;; recursively propagating the region extension up the output record tree.
(defmethod update-region-for-new-child ((record <output-record-element-mixin>) child)
  (multiple-value-bind (rleft rtop rright rbottom)
      (box-edges record)
    (multiple-value-bind (cleft ctop cright cbottom)
	(box-edges child)
      ;; Get the new child's box into the record's coordinate system
      (transform-coordinates! (sheet-transform child)
			      cleft ctop cright cbottom)
      (let* ((first-child? (= (sheet-child-count record :fast? t) 1))
	     (growing? (and (not first-child?)
			    (not (ltrb-contains-ltrb? rleft rtop rright rbottom
						      cleft ctop cright cbottom)))))
	(cond
	  (first-child?
	   ;; If this is the first child, we must always set the
	   ;; bounding rectangle of the parent
	   (setf (sheet-region record)
		 (set-box-edges (sheet-region record)
				cleft ctop cright cbottom)))
	  (growing?
	   ;; Don't bother to change the edges if we're not growing
	   (setf (sheet-region record)
		 (set-box-edges (sheet-region record)
				(min rleft cleft) (min rtop ctop)
				(max rright cright) (max rbottom cbottom)))))
	;; Inform the parent only in the case where we actually grew or
	;; added the initial child
	(let ((parent (sheet-parent record)))
	  (when (and parent (or first-child? growing?))
	    (transform-coordinates! (sheet-transform record)
				    rleft rtop rright rbottom)
	    (update-region-for-changed-child parent record
					     rleft rtop rright rbottom)))))))


#||
// Updates the region of RECORD to be reflect the change in the child's size,
// growing or shrinking RECORD's region as necessary, and recursively propagating
// the region change up the output record tree.
// Child's old edges are passed in parent's coordinate system because their
// reference point may have changed.
//---*** Make sure this really works!
define method update-region-for-changed-child
    (record :: <output-record-element-mixin>, child,
     old-left, old-top, old-right, old-bottom) => ()
  let (rleft, rtop, rright, rbottom) = box-edges(record);
  let (cleft, ctop, cright, cbottom) = box-edges(child);
  transform-coordinates!(sheet-transform(child),
			 cleft, ctop, cright, cbottom);
  // We must recompute the region if the child is not completely contained
  // or if it used to "define" one of the old edges.
  when (~ltrb-contains-ltrb?(rleft, rtop, rright, rbottom,
			     cleft, ctop, cright, cbottom)
	| old-left = rleft
	| old-top  = rtop
	| old-right  = rright
	| old-bottom = rbottom)
    update-region-for-changed-child-1(record)
  end
end method update-region-for-changed-child;
||#

;; Updates the region of RECORD to be reflect the change in the child's size,
;; growing or shrinking RECORD's region as necessary, and recursively propagating
;; the region change up the output record tree.
;; Child's old edges are passed in parent's coordinate system because their
;; reference point may have changed.
;;---*** Make sure this really works!
(defmethod update-region-for-changed-child ((record <output-record-element-mixin>) child
					    old-left old-top old-right old-bottom)
  (multiple-value-bind (rleft rtop rright rbottom)
      (box-edges record)
    (multiple-value-bind (cleft ctop cright cbottom)
	(box-edges child)
      (transform-coordinates! (sheet-transform child)
			      cleft ctop cright cbottom)
      ;; We must recompute the region if the child is not completely contained
      ;; or if it used to "define" one of the old edges.
      (when (or (not (ltrb-contains-ltrb? rleft rtop rright rbottom
					  cleft ctop cright cbottom))
		(= old-left   rleft)
		(= old-top    rtop)
		(= old-right  rright)
		(= old-bottom rbottom))
	(update-region-for-changed-child-1 record)))))


#||
//---*** Make sure this really works!
define method update-region-for-changed-child-1
    (record :: <output-record-element-mixin>) => ()
  let (old-left, old-top, old-right, old-bottom) = box-edges(record);
  let once? = #f;
  let left :: <integer> = $largest-coordinate;
  let top  :: <integer> = $largest-coordinate;
  let right  :: <integer> = $smallest-coordinate;
  let bottom :: <integer> = $smallest-coordinate;
  begin
    local method compute-child-region (child)
	    let (cleft, ctop, cright, cbottom) = box-edges(child);
	    transform-coordinates!(sheet-transform(child),
				   cleft, ctop, cright, cbottom);
	    min!(left, cleft);
	    min!(top,  ctop);
	    max!(right,  cright);
	    max!(bottom, cbottom);
	    once? := #t
	  end method;
    unless (once?)
      left := 0;
      top  := 0;
      right  := 0;
      bottom := 0
    end;
    // Compute the box that will hold all of the children
    do-sheet-children(compute-child-region, record)
  end;
  //---*** Are left/top/right/bottom in the correct coordinate system here?
  sheet-region(record)
    := set-box-edges(sheet-region(record), left, top, right, bottom);
  let parent = sheet-parent(record);
  when (parent)
    // Pass these coordinates in parent's coordinate system
    transform-coordinates!(sheet-transform(record),
			   old-left, old-top, old-right, old-bottom);
    update-region-for-changed-child(parent, record,
				    old-left, old-top, old-right, old-bottom)
  end
end method update-region-for-changed-child-1;
||#

;;---*** Make sure this really works!
(defmethod update-region-for-changed-child-1 ((record <output-record-element-mixin>))
  (multiple-value-bind (old-left old-top old-right old-bottom)
      (box-edges record)
    (let ((once?  nil)
	  (left   +largest-coordinate+)
	  (top    +largest-coordinate+)
	  (right  +smallest-coordinate+)
	  (bottom +smallest-coordinate+))
      (labels ((compute-child-region (child)
	         (multiple-value-bind (cleft ctop cright cbottom)
		     (box-edges child)
		   (transform-coordinates! (sheet-transform child)
					   cleft ctop cright cbottom)
		   (setf left (min left cleft))
		   (setf top (min top ctop))
		   (setf right (max right cright))
		   (setf bottom (max bottom cbottom))
		   (setf once? t))))
	(unless once?
	  (setf left   0)
	  (setf top    0)
	  (setf right  0)
	  (setf bottom 0))
	;; Compute the box that will hold all of the children
	(do-sheet-children #'compute-child-region record))
      ;;---*** Are left/top/right/bottom in the correct coordinate system here?
      (setf (sheet-region record)
	    (set-box-edges (sheet-region record) left top right bottom))
      (let ((parent (sheet-parent record)))
	(when parent
	  ;; Pass these coordinates in parent's coordinate system
	  (transform-coordinates! (sheet-transform record)
				  old-left old-top old-right old-bottom)
	  (update-region-for-changed-child parent record
					   old-left old-top old-right old-bottom))))))


#||
// This is for adjusting extents after a bunch of leaves have been moved.
// It starts by recomputing all the regions _down_ the output record tree,
// then notifying _up_ the tree if there was a change.
//---*** Make sure this really works!
define method recompute-region (record :: <output-record-element-mixin>) => ()
  let (old-left, old-top, old-right, old-bottom) = box-edges(record);
  do-recompute-region(record);
  let parent = sheet-parent(record);
  when (parent)
    transform-coordinates!(sheet-transform(record),
			   old-left, old-top, old-right, old-bottom);
    update-region-for-changed-child(parent, record,
				    old-left, old-top, old-right, old-bottom)
  end
end method recompute-region;
||#

;; This is for adjusting extents after a bunch of leaves have been moved.
;; It starts by recomputing all the regions _down_ the output record tree,
;; then notifying _up_ the tree if there was a change.
;;---*** Make sure this really works!
(defmethod recompute-region ((record <output-record-element-mixin>))
  (multiple-value-bind (old-left old-top old-right old-bottom)
      (box-edges record)
    (do-recompute-region record)
    (let ((parent (sheet-parent record)))
      (when parent
	(transform-coordinates! (sheet-transform record)
				old-left old-top old-right old-bottom)
	(update-region-for-changed-child parent record
					 old-left old-top old-right old-bottom)))))


#||
define method do-recompute-region
    (record :: <output-record-element-mixin>)
 => (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
  box-edges(record)
end method do-recompute-region;
||#

(defmethod do-recompute-region ((record <output-record-element-mixin>))
  (box-edges record))


#||
//---*** Make sure this really works!
define method do-recompute-region
    (record :: <composite-output-record-mixin>)
 => (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
  let once? = #f;
  let left :: <integer> = $largest-coordinate;
  let top  :: <integer> = $largest-coordinate;
  let right  :: <integer> = $smallest-coordinate;
  let bottom :: <integer> = $smallest-coordinate;
  begin
    local method recompute-child-region (child)
	    let (cleft, ctop, cright, cbottom) = do-recompute-region(child);
	    transform-coordinates!(sheet-transform(child),
				   cleft, ctop, cright, cbottom);
	    min!(left, cleft);
	    min!(top,  ctop);
	    max!(right,  cright);
	    max!(bottom, cbottom);
	    once? := #t
	  end method;
    do-sheet-children(recompute-child-region, record)
  end;
  unless (once?)
    left := 0;
    top  := 0;
    right  := 0;
    bottom := 0
  end;
  //---*** Are left/top/right/bottom in the correct coordinate system here?
  sheet-region(record)
    := set-box-edges(sheet-region(record), left, top, right, bottom);
  values(left, top, right, bottom)
end method do-recompute-region;
||#

;;---*** Make sure this really works!
(defmethod do-recompute-region ((record <composite-output-record-mixin>))
  (let ((once?  nil)
	(left   +largest-coordinate+)
	(top    +largest-coordinate+)
	(right  +smallest-coordinate+)
	(bottom +smallest-coordinate+))
    (labels ((recompute-child-region (child)
	       (multiple-value-bind (cleft ctop cright cbottom)
		   (do-recompute-region child)
		 (transform-coordinates! (sheet-transform child)
					 cleft ctop cright cbottom)
		 (setf left (min left cleft))
		 (setf top (min top ctop))
		 (setf right (max right cright))
		 (setf bottom (max bottom cbottom))
		 (setf once? t))))
      ;; ::Note:: this call is the opposite way around in update-region-for-changed-child-1; does that matter?
      (do-sheet-children #'recompute-child-region record))
    (unless once?
      (setf left 0)
      (setf top 0)
      (setf right 0)
      (setf bottom 0))
    ;;---*** Are left/top/right/bottom in the correct coordinate system here?
    (setf (sheet-region record)
	  (set-box-edges (sheet-region record) left top right bottom))
    (values left top right bottom)))



#||

/// Default methods for incremental redisplay

define method copy-display-state (record :: <output-record>, old-is-ok?)
  ignore(old-is-ok?);
  #f
end method copy-display-state;
||#

(defmethod copy-display-state ((record <output-record>) old-is-ok?)
  (declare (ignore old-is-ok?))
  nil)


#||
define method recompute-contents-ok (record :: <output-record>)
  #f
end method recompute-contents-ok;
||#

(defmethod recompute-contents-ok ((record <output-record>))
  nil)


#||
define method find-child-output-record
    (record :: <output-record>, use-old-children?, record-class,
     #rest initargs, #key)
  ignore(use-old-children?, record-class, initargs);
  #f
end method find-child-output-record;
||#

(defmethod find-child-output-record ((record <output-record>) use-old-children? record-class
				     &rest initargs &key)
  (declare (ignore use-old-children? record-class initargs))
  nil)



#||

/// Basic output record classes

define open abstract primary class <basic-leaf-record>
  (<output-record-element-mixin>,
   <leaf-output-record>)
  sealed slot record-medium-state :: <medium-state>,
    required-init-keyword: medium-state:;
end class <basic-leaf-record>;
||#

(defclass <basic-leaf-record>
    (<output-record-element-mixin>
     <leaf-output-record>)
  ((record-medium-state :type <medium-state> :initarg :medium-state
			:initform (required-slot ":medium-state" "<basic-leaf-record>")
			:accessor record-medium-state))
  (:metaclass <abstract-metaclass>))

#||
define open abstract primary class <basic-composite-record>
  (<composite-output-record-mixin>,
   <output-record-element-mixin>,
   <composite-output-record>)
end class <basic-composite-record>;
||#

(defclass <basic-composite-record>
    (<composite-output-record-mixin>
     <output-record-element-mixin>
     <composite-output-record>)
  ()
  (:metaclass <abstract-metaclass>))


#||
define sealed class <medium-state> (<object>)
  sealed slot medium-state-brush,
    required-init-keyword: brush:;
  sealed slot medium-state-pen,
    required-init-keyword: pen:;
  sealed slot medium-state-clipping-region,
    required-init-keyword: clipping-region:;
  sealed slot medium-state-text-style,
    required-init-keyword: text-style:;
end class <medium-state>;

define sealed domain make (singleton(<medium-state>));
define sealed domain initialize (<medium-state>);
||#

(defclass <medium-state> ()
  ((medium-state-brush :initarg :brush :initform (required-slot ":brush" "<medium-state>") :accessor medium-state-brush)
   (medium-state-pen :initarg :pen :initform (required-slot ":pen" "<medium-state>") :accessor medium-state-pen)
   (medium-state-clipping-region :initarg :clipping-region :initform (required-slot ":clipping-region" "<medium-state>")
				 :accessor medium-state-clipping-region)
   (medium-state-text-style :initarg :text-style :initform (required-slot ":text-style" "<medium-state>")
			    :accessor medium-state-text-style)))


#||
//---*** CLIM intersects the medium clipping region with the record clipping record
define macro with-record-medium-state
  { with-record-medium-state (?state:name = ?medium:expression, ?record:expression) ?:body end }
    => { begin
	   let ?state = record-medium-state(?record);
	   dynamic-bind (medium-brush(?medium) = medium-state-brush(?state),
			 medium-pen(?medium)   = medium-state-pen(?state),
			 medium-clipping-region(?medium)
			   = medium-state-clipping-region(?state))
	     ?body
	   end
         end }
  { with-record-medium-state (?medium:expression, ?record:expression) ?:body end }
    => { begin
	   let _state = record-medium-state(?record);
	   dynamic-bind (medium-brush(?medium) = medium-state-brush(_state),
			 medium-pen(?medium)   = medium-state-pen(_state),
			 medium-clipping-region(?medium)
			   = medium-state-clipping-region(_state))
	     ?body
	   end
         end }
end macro with-record-medium-state;
||#

;;---*** CLIM intersects the medium clipping region with the record clipping record
(defmacro with-record-medium-state ((atom-or-list record) &body body)
"Invoked in one of these two ways:-

 (WITH-RECORD-MEDIUM-STATE ((STATE = MEDIUM) RECORD) ...)
 (WITH-RECORD-MEDIUM-STATE (MEDIUM RECORD) ...)

 In the former case, _state_ is just a name that can be used in the body
 forms."
  (let ((state (if (listp atom-or-list)
                   (first atom-or-list)   ;; Yes, we want to capture the var in this case...
                   (gensym "STATE-")))
        (medium (gensym "MEDIUM-")))
    `(let ((,medium ,(if (listp atom-or-list)
                         (third atom-or-list)
                         atom-or-list))
           (,state (record-medium-state ,record)))
       (dynamic-bind (((medium-brush ,medium) = (medium-state-brush ,state))
		      ((medium-pen ,medium)   = (medium-state-pen ,state))
		      ((medium-clipping-region ,medium)
                          = (medium-state-clipping-region ,state)))
         ,@body))))





