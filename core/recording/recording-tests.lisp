;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-RECORDING-INTERNALS -*-
(in-package #:duim-recording-internals)

#||
/// Recording test harness

//---*** This is all stolen intact from duim/examples/harness.dylan

define variable *example-frames* = make(<stretchy-vector>);
||#

(defparameter *example-frames* (make-array 0 :adjustable t :fill-pointer t))

#||
define method find-example-class (class :: <class>)
  block (return)
    for (example in *example-frames*)
      if (example[0] = class)
        return(example)
      end
    end
  end
end method find-example-class;
||#

(defmethod find-example-class (class)
  (loop for example across *example-frames*
        when (eq (aref example 0) class)
        do (return-from find-example-class example)))

#||
define method install-example 
    (frame-class :: <class>, title :: <string>)
  let example = find-example-class(frame-class);
  if (example)
    example[1] := title;
  else
    add!(*example-frames*, vector(frame-class, title))
  end;
  frame-class
end method install-example;
||#

(defmethod install-example (frame-class
                            (title string))
  (let ((example (find-example-class frame-class)))
    (if example
        (setf (aref example 1) title)
        (add! *example-frames* (vector frame-class title))))
  frame-class)

#||
define method start-example-frame 
    (frame :: <frame>, class :: <class>, 
     #rest args,
     #key frame-manager: framem)
 => (example-frame :: <frame>)
  let example-frame
    = with-busy-cursor (frame)
        let example = find-example-class(class);
        let frame-class = example[0];
        let title = example[1];
        let example-frame
          = if (example)
	      apply(make, frame-class, owner: frame, title: title, args)
	    else
	      apply(make, class, owner: frame, title: "Example", args)
	    end;
        frame-mapped?(example-frame) := #t;
        example-frame
      end;
  start-frame(example-frame);
  example-frame
end method start-example-frame;
||#

(defmethod start-example-frame ((frame <frame>)
                                class
                                &rest args
                                &key ((frame-manager :framem)))
  (let ((example-frame
         (with-busy-cursor (frame)
           (let* ((example (find-example-class class))
                  (frame-class (aref example 0))
                  (title (aref example 1))
                  (example-frame (if example
                                     (apply #'make-instance frame-class :owner frame :title title args)
                                     (apply #'make-instance class :owner frame :title "Example" args))))
             (setf (frame-mapped? example-frame) t)
             example-frame))))
    (start-frame example-frame)
    example-frame))

#||
define method sorted-example-frames ()
  sort(*example-frames*,
       test: method (example1, example2)
               example1[1] < example2[1]
             end)
end method sorted-example-frames;
||#

(defmethod sorted-example-frames ()
  (sort *example-frames*
        :test #'(lambda (example1 example2)
                  (string< (aref example1 1) (aref example2 1)))))

#||
define frame <examples-harness> (<simple-frame>)
  pane update (frame)
    make(<push-button>,
	      label: "Update",
	      activate-callback: update-examples-harness);
  pane examples (frame)
    make(<list-control>,
         items: sorted-example-frames(),
         label-key: second,
         value-key: first,
         activate-callback: method (sheet :: <sheet>)
                              let frame = sheet-frame(sheet);
                              let example = gadget-value(sheet);
                              start-example-frame(frame, example)
                            end);
  pane main-layout (frame)
    vertically (spacing: 2)
      frame.update;
      frame.examples;
    end;
  layout (frame) frame.main-layout;
end frame <examples-harness>;
||#

(define-frame <examples-harness> (<simple-frame>)
  ((:pane update (frame)
          (make-instance '<push-button>
                         :label "Update"
                         :activate-callback
			 #'update-examples-harness))
   (:pane examples (frame)
          (make-instance '<list-control>
                         :items (sorted-example-frames)
                         :label-key #'second
                         :value-key #'first
                         :activate-callback
			 #'(lambda (sheet)
			     (let ((frame (sheet-frame sheet))
				   (example (gadget-value sheet)))
			       (start-example-frame frame example)))))
   (:pane main-layout (frame)
          (vertically (:spacing 2)
                      (update frame)
                      (examples frame)))
   (:layout (frame)
            (main-layout frame))))

#||
define method update-examples-harness 
    (sheet :: <sheet>) => ()
  let frame = sheet-frame(sheet);
  gadget-items(frame.examples) := sorted-example-frames()
end method update-examples-harness;
||#

(defmethod update-examples-harness ((sheet <sheet>))
  (let ((frame (sheet-frame sheet)))
    (setf (gadget-items (examples frame)) (sorted-example-frames))))

#||
define method start-examples 
    () => (status-code :: false-or(<integer>))
  let frame = make(<examples-harness>, title: "Recording Examples");
  start-frame(frame);
  #f
end method start-examples;
||#

(defmethod start-examples ()
  (let ((frame (make-instance '<examples-harness> :title "Recording Examples")))
    (start-frame frame)
    nil))


#||
/// Simple drawing tests

//---*** This is all based on duim/tests/gui/graphics.dylan

define constant $default-graphic-width   = 60;
define constant $default-graphic-height  = 60;
define constant $default-graphic-spacing = 30;
define constant $default-graphic-border  = 10;
||#

(defparameter $default-graphic-width   60)
(defparameter $default-graphic-height  60)
(defparameter $default-graphic-spacing 30)
(defparameter $default-graphic-border  10)

#||
define class <graphics-pane> (<recording-pane>)
  constant slot graphics-to-test :: <list>,
    init-keyword: graphics:;
  constant slot graphic-width  :: <integer> = $default-graphic-width;
  constant slot graphic-height :: <integer> = $default-graphic-height;
end class <graphics-pane>;
||#

(define-dylan-class (:open) <graphics-pane> (<recording-pane>)
  (((:constant) :slot graphics-to-test
                      #-(or :sbcl) :type
                      #-(or :sbcl) list
                      :initarg :graphics)
   ((:constant) :slot graphic-width
                      #-(or :sbcl) :type
                      #-(or :sbcl) integer
                      :initform $default-graphic-width)
   ((:constant) :slot graphic-height
                      #-(or :sbcl) :type
                      #-(or :sbcl) integer
                      :initform $default-graphic-height)))

#||
define method do-compose-space
    (pane :: <graphics-pane>, #key width, height)
 => (space-requirement :: <space-requirement>)
  ignore(width, height);
  let width   = graphic-width(pane);
  let height  = graphic-height(pane);
  let spacing = $default-graphic-spacing;
  let border  = $default-graphic-border;
  let no-of-items = size(graphics-to-test(pane));
  let _port = port(pane);
  let text-height = font-height(get-default-text-style(_port, pane), _port);
  make(<space-requirement>,
       width:  (2 * (border + width)) + spacing,
       height: text-height + ((no-of-items - 1) * (height + spacing))
                 + height + 4 + 3 * border)
end method do-compose-space;
||#

(defmethod do-compose-space ((pane <graphics-pane>)
                             &key
                             width
                             height)
  (declare (ignore width height))
  (let ((width       (graphic-width pane))
        (height      (graphic-height pane))
        (spacing     $default-graphic-spacing)
        (border      $default-graphic-border)
        (no-of-items (length (graphics-to-test pane)))
        (_port       (port pane))
        (text-height (font-height (get-default-text-style _port pane) _port)))
    (make-space-requirement :width (+ (* 2 (+ border width)) spacing)
			    :height (+ text-height
				       (* (1- no-of-items) (+ height spacing))
				       height
				       4
				       (* 3 border)))))

#||
define method handle-event
    (sheet :: <graphics-pane>, 
     event :: <pointer-motion-event>) => ()
  let old = sheet-highlighted-record(sheet);
  let new = child-containing-position(sheet-output-history(sheet), event-x(event), event-y(event));
  when (old & old ~== new)
    highlight-output-record(old, sheet, #"unhighlight")
  end;
  when (new)
    highlight-output-record(new, sheet, #"highlight")
  end;
  sheet-highlighted-record(sheet) := new
end method handle-event;
||#

(defmethod handle-event ((sheet <graphics-pane>)
                         (event <pointer-motion-event>))
  (let ((old (sheet-highlighted-record sheet))
        (new (child-containing-position (sheet-output-history sheet) (event-x event) (event-y event))))
    (when (and old (dylan-~== old new))
      (highlight-output-record old sheet :unhighlight))
    (when new
      (highlight-output-record new sheet :highlight))
    (setf (sheet-highlighted-record sheet) new)))

#||
define constant $test-graphics-color      = #(#"Blue", #"Black", #"Red");
define constant $test-graphics-thickness  = #(0, 1, 2, 3);
||#

(defparameter $test-graphics-color     (vector :blue :black :red))
(defparameter $test-graphics-thickness (vector 0 1 2 3))

#||
define pattern $pixmap-to-test
  (list($red, $green, $blue))
    2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2;
    1, 2, 0, 0, 0, 0, 0, 0, 0, 2, 1;
    1, 0, 2, 0, 0, 1, 0, 0, 2, 0, 1;
    1, 0, 0, 2, 0, 1, 0, 2, 0, 0, 1;
    1, 0, 0, 0, 2, 1, 2, 0, 0, 0, 1;
    1, 0, 1, 1, 1, 2, 1, 1, 1, 0, 1;
    1, 0, 0, 0, 2, 1, 2, 0, 0, 0, 1;
    1, 0, 0, 2, 0, 1, 0, 2, 0, 0, 1;
    1, 0, 2, 0, 0, 1, 0, 0, 2, 0, 1;
    1, 2, 0, 0, 0, 0, 0, 0, 0, 2, 1;
    2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2;
end pattern $pixmap-to-test;
||#

(define-pattern $pixmap-to-test
    ($red $green $blue)
    ((2 1 1 1 1 1 1 1 1 1 2)
     (1 2 0 0 0 0 0 0 0 2 1)
     (1 0 2 0 0 1 0 0 2 0 1)
     (1 0 0 2 0 1 0 2 0 0 1)
     (1 0 0 0 2 1 2 0 0 0 1)
     (1 0 1 1 1 2 1 1 1 0 1)
     (1 0 0 0 2 1 2 0 0 0 1)
     (1 0 0 2 0 1 0 2 0 0 1)
     (1 0 2 0 0 1 0 0 2 0 1)
     (1 2 0 0 0 0 0 0 0 2 1)
     (2 1 1 1 1 1 1 1 1 1 2)))

#||
define method draw-test-graphics (sheet :: <graphics-pane>) => ()
  clear-output-history(sheet);
  let frame  = sheet-frame(sheet);
  let medium = sheet-medium(sheet);
  let color = frame.%color;
  let brush = select (color by \==)
		#"Blue"  => $blue;
		#"Red"   => $red;
		#"Black" => $black;
	      end;
  let thickness = frame.%thickness;
  let pen       = make(<pen>, width: thickness);
  let width     = graphic-width(sheet);
  let height    = graphic-height(sheet);
  let spacing   = $default-graphic-spacing;
  let border    = $default-graphic-border;
  let x1 = border;
  let y1 = border;
  let x2 = x1 + width;
  let y2 = y1 + height;
  let x3 = x2 + spacing;
  let x4 = x3 + width;
  with-drawing-options (sheet, brush: brush, pen: pen)
    let text-style = medium-merged-text-style(medium);
    let text-height = font-height(text-style, port(sheet));
    draw-text(sheet, "Unfilled", x1, y1, align-y: #"top");
    draw-text(sheet, "Filled",   x3, y1, align-y: #"top");
    draw-line(sheet, x1, y1 + text-height + 2, x4, y1 + text-height + 2);
    y1 := y1 + text-height + 4 + border;
    y2 := y2 + text-height + 4 + border;
    for (graphic in sheet.graphics-to-test)
      draw-graphic(graphic, sheet, #f, x1, x2, y1, y2);
      draw-graphic(graphic, sheet, #t, x3, x4, y1, y2);
      y1 := y1 + height + spacing;
      y2 := y2 + height + spacing;
    end
  end
end method draw-test-graphics;
||#

(defmethod draw-test-graphics ((sheet <graphics-pane>))
  (clear-output-history sheet)
  (let* ((frame  (sheet-frame sheet))
         (medium (sheet-medium sheet))
         (color  (%color frame))
         (brush  (ecase color
                   (:blue  $blue)
                   (:red   $red)
                   (:black $black)))
         (thickness (%thickness frame))
         (pen     (make-instance '<pen> :width thickness))
         (width   (graphic-width sheet))
         (height  (graphic-height sheet))
         (spacing $default-graphic-spacing)
         (border  $default-graphic-border)
         (x1 border)
         (y1 border)
         (x2 (+ x1 width))
         (y2 (+ y1 height))
         (x3 (+ x2 spacing))
         (x4 (+ x3 width)))
    (with-drawing-options (sheet :brush brush :pen pen)
      (let* ((text-style  (medium-merged-text-style medium))
             (text-height (font-height text-style (port sheet))))
        (draw-text sheet "Unfilled" x1 y1 :align-y :top)
        (draw-text sheet "Filled" x3 y1 :align-y :top)
        (draw-line sheet x1 (+ y1 text-height 2) x4 (+ y1 text-height 2))
        (setf y1 (+ y1 text-height 4 border)
              y2 (+ y2 text-height 4 border))
        (loop for graphic in (graphics-to-test sheet)
              do (draw-graphic graphic sheet nil x1 x2 y1 y2)
              do (draw-graphic graphic sheet t x3 x4 y1 y2)
              do (setf y1 (+ y1 height spacing))
              do (setf y2 (+ y2 height spacing)))))))

#||
define method draw-graphic
    (graphic-type == #"rectangle", sheet :: <graphics-pane>, filled? :: <boolean>, 
     x1 :: <integer>, x2 :: <integer>, y1 :: <integer>, y2 :: <integer>) => () 
  draw-rectangle(sheet, x1, y1, x2, y2, filled?: filled?)
end method draw-graphic;
||#

(defmethod draw-graphic ((graphic-type (eql :rectangle))
                         (sheet <graphics-pane>)
                         filled?
                         x1 x2 y1 y2)
  (draw-rectangle sheet x1 y1 x2 y2 :filled? filled?))

#||
define method draw-graphic
    (graphic-type == #"arrow", sheet :: <graphics-pane>, filled? :: <boolean>, 
     x1 :: <integer>, x2 :: <integer>, y1 :: <integer>, y2 :: <integer>) => ()
  let xinc = round/(x2 - x1, 5);
  let yinc = round/(y2 - y1, 5);
  for (i :: <integer> from x1 to x2 by xinc, j :: <integer> from x2 to x1 by -xinc)
    draw-arrow(sheet, i, y1, j, y2, from-head?: filled?, to-head?: filled?);
  end;
  for (i :: <integer> from y1 to y2 by yinc, j :: <integer> from y2 to y1 by -yinc)
    draw-arrow(sheet, x1, i, x2, j, from-head?: filled?, to-head?: filled?);
  end;
end method draw-graphic;
||#

(defmethod draw-graphic ((graphic-type (eql :arrow))
                         (sheet <graphics-pane>)
                         filled?
                         x1 x2 y1 y2)
  (let ((xinc (round (- x2 x1) 5))
        (yinc (round (- y2 y1) 5)))
    (loop for i from x1 to x2 by xinc
          for j downfrom x2 to x1 by xinc
          do (draw-arrow sheet i y1 j y2 :from-head? filled? :to-head? filled?))
    (loop for i from y1 to y2 by yinc
          for j downfrom y2 to y1 by yinc
          do (draw-arrow sheet x1 i x2 j :from-head? filled? :to-head? filled?))))

#||
define method draw-graphic
    (graphic-type == #"polygon", sheet :: <graphics-pane>, filled? :: <boolean>, 
     x1 :: <integer>, x2 :: <integer>, y1 :: <integer>, y2 :: <integer>) => () 
  let xcen = x1 + (round/(x2 - x1, 2));
  let ycen = y1 + (round/(y2 - y1, 2));
  let x3 = xcen + (round/(x2 - xcen, 2));
  draw-polygon(sheet, list(xcen, y2, x1, ycen, xcen, y1, x3, ycen),
               closed?: filled?, filled?: filled?);
end method draw-graphic;
||#

(defmethod draw-graphic ((graphic-type (eql :polygon))
                         (sheet <graphics-pane>)
                         filled?
                         x1 x2 y1 y2)
  (let* ((xcen (+ x1 (round (- x2 x1) 2)))
         (ycen (+ y1 (round (- y2 y1) 2)))
         (x3   (+ xcen (round (- x2 xcen) 2))))
    (draw-polygon sheet
                  (list xcen y2 x1 ycen xcen y1 x3 ycen)
                  :closed? filled?
                  :filled? filled?)))

#||
define method draw-graphic
    (graphic-type == #"regular-polygon", sheet :: <graphics-pane>, filled? :: <boolean>, 
     x1 :: <integer>, x2 :: <integer>, y1 :: <integer>, y2 :: <integer>) => ()
  let xcen = floor/(x1 + x2, 2);
  let line-size = floor/(x2 - x1, 3);
  let x3 = xcen - floor/(line-size, 2);
  let x4 = xcen + floor/(line-size, 2);
  draw-regular-polygon(sheet, x3, y1, x4, y1, 8, closed?: filled?, filled?: filled?);
end method draw-graphic;
||#

(defmethod draw-graphic ((graphic-type (eql :regular-polygon))
                         (sheet <graphics-pane>)
                         filled?
                         x1 x2 y1 y2)
  (let* ((xcen (floor (+ x1 x2) 2))
         (line-size (floor (- x2 x1) 3))
         (x3 (- xcen (floor line-size 2)))
         (x4 (+ xcen (floor line-size 2))))
    (draw-regular-polygon sheet x3 y1 x4 y1 8 :closed? filled? :filled? filled?)))

#||
define method draw-graphic
    (graphic-type == #"triangle", sheet :: <graphics-pane>, filled? :: <boolean>, 
     x1 :: <integer>, x2 :: <integer>, y1 :: <integer>, y2 :: <integer>) => () 
  let xcen = x1 + (round/(x2 - x1, 2));
  draw-triangle(sheet, xcen, y1, x1, y2, x2, y2, filled?: filled?);
end method draw-graphic;
||#

(defmethod draw-graphic ((graphic-type (eql :triangle))
                         (sheet <graphics-pane>)
                         filled?
                         x1 x2 y1 y2)
  (let ((xcen (+ x1 (round (- x2 x1) 2))))
    (draw-triangle sheet xcen y1 x1 y2 x2 y2 :filled? filled?)))

#||
define method draw-graphic
    (graphic-type == #"circle", sheet :: <graphics-pane>, filled? :: <boolean>, 
     x1 :: <integer>, x2 :: <integer>, y1 :: <integer>, y2 :: <integer>) => () 
  let xcen = x1 + (round/(x2 - x1, 2));
  let ycen = y1 + (round/(y2 - y1, 2));
  draw-circle(sheet, xcen, ycen, (ycen - y1), filled?: filled?)
end method draw-graphic;
||#

(defmethod draw-graphic ((graphic-type (eql :circle))
                         (sheet <graphics-pane>)
                         filled?
                         x1 x2 y1 y2)
  (let ((xcen (+ x1 (round (- x2 x1) 2)))
        (ycen (+ y1 (round (- y2 y1) 2))))
    (draw-circle sheet xcen ycen (- ycen y1) :filled? filled?)))

#||
define method draw-graphic
    (graphic-type == #"ellipse", sheet :: <graphics-pane>, filled? :: <boolean>, 
     x1 :: <integer>, x2 :: <integer>, y1 :: <integer>, y2 :: <integer>) => () 
  let xcen = x1 + (round/(x2 - x1, 2));
   let ycen = y1 + (round/(y2 - y1, 2));
   let xrad = round/(xcen - x1, 2);
   let yrad = ycen - y1;
   draw-ellipse(sheet, xcen, ycen, xrad, 0, 0, yrad, filled?: filled?);
end method draw-graphic;
||#

(defmethod draw-graphic ((graphic-type (eql :ellipse))
                         (sheet <graphics-pane>)
                         filled?
                         x1 x2 y1 y2)
  (let* ((xcen (+ x1 (round (- x2 x1) 2)))
         (ycen (+ y1 (round (- y2 y1) 2)))
         (xrad (round (- xcen x1) 2))
         (yrad (- ycen y1)))
    (draw-ellipse sheet xcen ycen xrad 0 0 yrad :filled? filled?)))

#||
define method draw-graphic
    (graphic-type == #"oval", sheet :: <graphics-pane>, filled? :: <boolean>, 
     x1 :: <integer>, x2 :: <integer>, y1 :: <integer>, y2 :: <integer>) => () 
  let xcen = x1 + (round/(x2 - x1, 2));
  let ycen = y1 + (round/(y2 - y1, 2));
  let yrad = round/(ycen - y1, 2);
  with-new-output-record (sheet)
    draw-oval(sheet, xcen, ycen, xcen - x1, yrad, filled?: filled?)
  end;
end method draw-graphic;
||#

(defmethod draw-graphic ((graphic-type (eql :oval))
                         (sheet <graphics-pane>)
                         filled?
                         x1 x2 y1 y2)
  (let* ((xcen (+ x1 (round (- x2 x1) 2)))
         (ycen (+ y1 (round (- y2 y1) 2)))
         (yrad (round (- ycen y1) 2)))
    (with-new-output-record (sheet)
      (draw-oval sheet xcen ycen (- xcen x1) yrad :filled? filled?))))

#||
define method draw-graphic
    (graphic-type == #"bezier-curve", sheet :: <graphics-pane>, filled? :: <boolean>, 
     x1 :: <integer>, x2 :: <integer>, y1 :: <integer>, y2 :: <integer>) => () 
  let xcen = x1 + (round/(x2 - x1, 2));
  let ycen = y1 + (round/(y2 - y1, 2));
  draw-bezier-curve(sheet, list(x1, ycen, xcen, y1, x2, y2, x2, ycen), filled?: filled?);
end method draw-graphic;
||#

(defmethod draw-graphic ((graphic-type (eql :bezier-curve))
                         (sheet <graphics-pane>)
                         filled?
                         x1 x2 y1 y2)
  (let ((xcen (+ x1 (round (- x2 x1) 2)))
        (ycen (+ y1 (round (- y2 y1) 2))))
    (draw-bezier-curve sheet (list x1 ycen xcen y1 x2 y2 x2 ycen) :filled? filled?)))

#||
define method draw-graphic
    (graphic-type == #"image", sheet :: <graphics-pane>, filled? :: <boolean>, 
     x1 :: <integer>, x2 :: <integer>, y1 :: <integer>, y2 :: <integer>) => ()
  let xcen = x1 + (round/(x2 - x1, 2));
  let ycen = y1 + (round/(y2 - y1, 2));
  draw-image(sheet, $pixmap-to-test, xcen, ycen);
end method draw-graphic;
||#

(defmethod draw-graphic ((graphic-type (eql :image))
                         (sheet <graphics-pane>)
                         filled?
                         x1 x2 y1 y2)
  (let ((xcen (+ x1 (round (- x2 x1) 2)))
        (ycen (+ y1 (round (- y2 y1) 2))))
    (draw-image sheet $pixmap-to-test xcen ycen)))

#||
define command-table *graphics-test-file-comtab* (*global-command-table*)
  menu-item "E&xit" = exit-frame,
    documentation: "Exit";
end command-table *graphics-test-file-comtab*;
||#

(define-command-table *graphics-test-file-comtab* (*global-command-table*)
  ((:menu-item "E&xit" = exit-frame
               :documentation "Exit")))

#||
define command-table *graphics-test-comtab* (*global-command-table*)
  menu-item "File" = *graphics-test-file-comtab*;
end command-table *graphics-test-comtab*;
||#

(define-command-table *graphics-test-comtab* (*global-command-table*)
  ((:menu-item "File" = *graphics-test-file-comtab*)))

#||
define frame <graphics-test-frame> (<simple-frame>)
  slot %color = $test-graphics-color[0],
    init-keyword: color:;
  slot %thickness = $test-graphics-thickness[0],
    init-keyword: thickness:;
  pane color-radio-box (frame)
    make(<radio-box>,
	 items: $test-graphics-color,
	 value: frame.%color,
         label-key: curry(as, <string>),
 	 value-changed-callback:
	   method (gadget)
	     let frame = sheet-frame(gadget);
	     frame.%color := gadget-value(gadget);
	     repaint-panes(frame)
	   end method);
  pane thickness-list-box (frame)
    make(<spin-box>,
         items: $test-graphics-thickness,
         value: frame.%thickness,
         value-changed-callback:
	   method (gadget)
	     let frame = sheet-frame(gadget);
	     frame.%thickness := gadget-value(gadget);
	     repaint-panes(frame)
	   end method);
  pane drawable-left (frame)
    make(<graphics-pane>,
         graphics: list(#"rectangle", #"circle", #"polygon", #"regular-polygon"));
  pane drawable-right (frame)
    make(<graphics-pane>,
         graphics: list(#"triangle", #"ellipse", #"oval", #"bezier-curve", #"image"));
  pane main-layout (frame)
    vertically (spacing: 5)
      make(<separator>);
      make(<table-layout>,
	   x-spacing: 10, y-spacing: 2,
           x-alignment: #[#"right", #"left"],
           contents: vector(vector(make(<label>, label: "Color:"),
				   frame.color-radio-box),
			    vector(make(<label>, label: "Thickness:"),
				   frame.thickness-list-box)));
      make(<separator>);
      horizontally (spacing: 10)
        with-border (type: #"sunken")
	  frame.drawable-left
	end;
        with-border (type: #"sunken")
	  frame.drawable-right
	end
      end
    end;
  layout (frame) frame.main-layout;
  command-table (frame) *graphics-test-comtab*;
end frame <graphics-test-frame>;
||#

(define-frame <graphics-test-frame> (<simple-frame>)
  ((%color :initarg :color
	   :initform (aref $test-graphics-color 0)
	   :accessor %color)
   (%thickness :initarg :thickness
	       :initform (aref $test-graphics-thickness 0)
	       :accessor %thickness)
   (:pane color-radio-box (frame)
          (make-pane '<radio-box>
		     :items $test-graphics-color
		     :value (%color frame)
		     :label-key (curry #'coerce 'string)
		     :value-changed-callback
		     #'(lambda (gadget)
			 (let ((frame (sheet-frame gadget)))
			   (setf (%color frame) (gadget-value gadget))
			   (repaint-panes frame)))))
   (:pane thickness-list-box (frame)
          (make-pane '<spin-box>
		     :items $test-graphics-thickness
		     :value (%thickness frame)
		     :value-changed-callback
		     #'(lambda (gadget)
			 (let ((frame (sheet-frame gadget)))
			   (setf (%thickness frame) (gadget-value gadget))
			   (repaint-panes frame)))))
   (:pane drawable-left (frame)
          (make-pane '<graphics-pane>
		     :graphics
		     (list :rectangle :circle :polygon :regular-polygon)))
   (:pane drawable-right (frame)
          (make-pane '<graphics-pane>
		     :graphics
		     (list :triangle :ellipse :oval :bezier-curve :image)))
   (:pane main-layout (frame)
          (vertically (:spacing 5)
            (make-pane '<separator>)
	    ;; MAKE-PANE?
            (make-instance '<table-layout>
                           :x-spacing 10
                           :y-spacing 2
                           :x-alignment (vector :right :left)
                           :contents
			   (vector (vector (make-pane '<label>
						      :label "Color:")
					   (color-radio-box frame))
				   (vector (make-pane '<label>
						      :label "Thickness:")
					   (thickness-list-box frame))))
            (make-pane '<separator>)
            (horizontally (:spacing 10)
              (with-border (:type :sunken)
                (drawable-left frame))
              (with-border (:type :sunken)
                (drawable-right frame)))))
   (:layout (frame)
     (main-layout frame))
   (:command-table (frame) *graphics-test-comtab*)))

#||
// Redraw methods for new color and thickness, etc
define method repaint-panes
    (frame :: <graphics-test-frame>) => ()
  draw-test-graphics(frame.drawable-left);
  draw-test-graphics(frame.drawable-right);
end method repaint-panes;
||#

(defmethod repaint-panes ((frame <graphics-test-frame>))
  (draw-test-graphics (drawable-left frame))
  (draw-test-graphics (drawable-right frame)))

#||
install-example(<graphics-test-frame>, "Graphics");
||#

(install-example (find-class '<graphics-test-frame>) "Graphics")


#||
/// Simple drawing tests with scrolling

define frame <scrolling-graphics-test-frame> (<simple-frame>)
  slot %color = $test-graphics-color[0],
    init-keyword: color:;
  slot %thickness = $test-graphics-thickness[0],
    init-keyword: thickness:;
  pane color-radio-box (frame)
    make(<radio-box>,
	 items: $test-graphics-color,
	 value: frame.%color,
         label-key: curry(as, <string>),
 	 value-changed-callback:
	   method (gadget)
	     let frame = sheet-frame(gadget);
	     frame.%color := gadget-value(gadget);
	     repaint-panes(frame)
	   end method);
  pane thickness-list-box (frame)
    make(<spin-box>,
         items: $test-graphics-thickness,
         value: frame.%thickness,
         value-changed-callback:
	   method (gadget)
	     let frame = sheet-frame(gadget);
	     frame.%thickness := gadget-value(gadget);
	     repaint-panes(frame)
	   end method);
  pane drawable-left (frame)
    make(<graphics-pane>,
         graphics: list(#"rectangle", #"circle", #"polygon", #"regular-polygon"),
         height: 200);
  pane drawable-right (frame)
    make(<graphics-pane>,
         graphics: list(#"triangle", #"ellipse", #"oval", #"bezier-curve", #"image"),
         height: 200);
  pane main-layout (frame)
    vertically (spacing: 5)
      make(<separator>);
      make(<table-layout>,
	   x-spacing: 10, y-spacing: 2,
           x-alignment: #[#"right", #"left"],
           contents: vector(vector(make(<label>, label: "Color:"),
				   frame.color-radio-box),
			    vector(make(<label>, label: "Thickness:"),
				   frame.thickness-list-box)));
      make(<separator>);
      horizontally (spacing: 10)
        with-border (type: #"sunken")
	  scrolling () frame.drawable-left end
	end;
        with-border (type: #"sunken")
	  scrolling () frame.drawable-right end
	end
      end
    end;
  layout (frame) frame.main-layout;
  command-table (frame) *graphics-test-comtab*;
end frame <scrolling-graphics-test-frame>;
||#

(define-frame <scrolling-graphics-test-frame> (<simple-frame>)
  ((%color :initarg :color
	   :initform (aref $test-graphics-color 0)
	   :accessor %color)
   (%thickness :initarg :thickness
	       :initform (aref $test-graphics-thickness 0)
	       :accessor %thickness)
   (:pane color-radio-box (frame)
          (make-pane '<radio-box>
		     :items $test-graphics-color
		     :value (%color frame)
		     :label-key (curry #'coerce 'string)
		     :value-changed-callback
		     #'(lambda (gadget)
			 (let ((frame (sheet-frame gadget)))
			   (setf (%color frame) (gadget-value gadget))
			   (repaint-panes frame)))))
   (:pane thickness-list-box (frame)
          (make-pane '<spin-box>
		     :items $test-graphics-thickness
		     :value (%thickness frame)
		     :value-changed-callback
		     #'(lambda (gadget)
			 (let ((frame (sheet-frame gadget)))
			   (setf (%thickness frame) (gadget-value gadget))
			   (repaint-panes frame)))))
   (:pane drawable-left (frame)
          (make-pane '<graphics-pane>
		     :graphics
		     (list :rectangle :circle :polygon :regular-polygon)
		     :height 200))
   (:pane drawable-right (frame)
          (make-pane '<graphics-pane>
		     :graphics
		     (list :triangle :ellipse :oval :bezier-curve :image)
		     :height 200))
   (:pane main-layout (frame)
          (vertically (:spacing 5)
            (make-pane '<separator>)
	    ;; MAKE-PANE?
            (make-instance '<table-layout>
                           :x-spacing 10
                           :y-spacing 2
                           :x-alignment (vector :right :left)
                           :contents
			   (vector (vector (make-pane '<label>
						      :label "Color:")
					   (color-radio-box frame))
				   (vector (make-pane '<label>
						      :label "Thickness:")
					   (thickness-list-box frame))))
            (make-pane '<separator>)
            (horizontally (:spacing 10)
              (with-border (:type :sunken)
                (scrolling () (drawable-left frame)))
              (with-border (:type :sunken)
                (scrolling () (drawable-right frame))))))
   (:layout (frame) (main-layout frame))
   (:command-table (frame) *graphics-test-comtab*)))

#||
// Redraw methods for new color and thickness, etc
define method repaint-panes
    (frame :: <scrolling-graphics-test-frame>) => ()
  draw-test-graphics(frame.drawable-left);
  draw-test-graphics(frame.drawable-right);
end method repaint-panes;
||#

(defmethod repaint-panes ((frame <scrolling-graphics-test-frame>))
  (draw-test-graphics (drawable-left frame))
  (draw-test-graphics (drawable-right frame)))

#||
install-example(<scrolling-graphics-test-frame>, "Scrolling graphics");
||#

(install-example (find-class '<scrolling-graphics-test-frame>) "Scrolling graphics")


#||
/// Simple scribble application

//---*** This is all based on duim/examples/scribble/scribble.dylan

define class <scribble-pane> (<recording-pane>)
  slot scribble-segment = #f;
  constant slot scribble-popup-menu-callback = #f,
    init-keyword: popup-menu-callback:;
end class <scribble-pane>;
||#

(define-dylan-class (:open) <scribble-pane> (<recording-pane>)
  (((:open)     :slot scribble-segment
                      :initform nil
                      :accessor scribble-segment)
   ((:constant) :slot scribble-popup-menu-callback
                      :initform nil
                      :initarg :popup-menu-callback)))

#||
define method handle-button-event
    (sheet :: <scribble-pane>, 
     event :: <button-press-event>, 
     button == $left-button) => ()
  sheet.scribble-segment := make(<stretchy-vector>);
  add-scribble-segment(sheet, event.event-x, event.event-y);
  block (break)
    tracking-pointer (event, sheet)
      <pointer-drag-event> =>
        add-scribble-segment(sheet, event.event-x, event.event-y);
      <button-release-event> =>
        begin
          when (sheet.scribble-segment)
            with-output-recording-options (sheet, record?: #t)
              draw-polygon(sheet, sheet.scribble-segment, closed?: #f, filled?: #f)
            end
          end;
          sheet.scribble-segment := #f;
          break()
        end;
    end
  end
end method handle-button-event;
||#

(defmethod handle-button-event ((sheet <scribble-pane>)
                                (event <button-press-event>)
                                (button (eql $left-button)))
  (setf (scribble-segment sheet) (make-array 0 :adjustable t :fill-pointer t))
  (add-scribble-segment sheet (event-x event) (event-y event))
  (block break
    (tracking-pointer (event sheet)
      (<pointer-drag-event>
       (add-scribble-segment sheet (event-x event) (event-y event)))
      (<button-release-event>
       (progn
         (when (scribble-segment sheet)
           (with-output-recording-options (sheet :record? t)
             (draw-polygon sheet (scribble-segment sheet) :closed? nil :filled? nil)))
         (setf (scribble-segment sheet) nil)
         (return-from break))))))

#||
define method handle-button-event
    (sheet :: <scribble-pane>, 
     event :: <button-release-event>, 
     button == $right-button) => ()
  let popup-menu-callback = scribble-popup-menu-callback(sheet);
  when (popup-menu-callback)
    popup-menu-callback(sheet, event.event-x, event.event-y)
  end
end method handle-button-event;
||#

(defmethod handle-button-event ((sheet <scribble-pane>)
                                (event <button-release-event>)
                                (button (eql $right-button)))
  (let ((popup-menu-callback (scribble-popup-menu-callback sheet)))
    (when popup-menu-callback
      (funcall popup-menu-callback sheet (event-x event) (event-y event)))))

#||
define method add-scribble-segment
    (sheet :: <scribble-pane>, x, y) => ()
  let segment = sheet.scribble-segment;
  // The app can generate drag and release events before it has ever
  // seen a press event, so be careful
  when (segment)
    add!(segment, x);
    add!(segment, y);
    with-output-recording-options (sheet, record?: #f)
      draw-polygon(sheet, segment, closed?: #f, filled?: #f)
    end
  end
end method add-scribble-segment;
||#

(defmethod add-scribble-segment ((sheet <scribble-pane>)
                                 x y)
  (let ((segment (scribble-segment sheet)))
    ;; The app can generate drag and release events before it has ever
    ;; seen a press event, so be careful
    (when segment
      (add! segment x)
      (add! segment y)
      (with-output-recording-option (sheet :record? nil)
        (draw-polygon sheet segment :closed? nil :filled? nil)))))

#||
define frame <scribble-frame> (<simple-frame>)
  pane surface (frame)
    make(<scribble-pane>, 
	 popup-menu-callback: method (sheet, x, y)
				let frame = sheet-frame(sheet);
				popup-scribble-menu(frame, x, y)
			      end,
	 width:  300, max-width:  $fill,
	 height: 200, max-height: $fill);
  pane file-menu (frame)
    make(<menu>,
	 label: "File",
	 children: vector(make(<menu-button>,
			       label: "&Clear",
			       selection-mode: #"none",
			       activate-callback: method (button)
						    clear-output-history(frame.surface)
						  end),
			  make(<menu-button>,
			       label: "E&xit",
			       selection-mode: #"none",
			       activate-callback: method (button)
						    exit-frame(sheet-frame(button))
						  end)));
  pane scribble-popup-menu (frame)
    make(<menu>,
	 owner: frame.surface,
	 children: vector(make(<menu-button>,
			       label: "&Clear",
			       selection-mode: #"none",
			       activate-callback: method (button)
						    ignore(button);
						    clear-output-history(frame.surface)
						  end)));
  layout (frame) frame.surface;
  menu-bar (frame)
    make(<menu-bar>, children: vector(frame.file-menu));
end frame <scribble-frame>;
||#

(define-frame <scribble-frame> (<simple-frame>)
  ((:pane surface (frame)
          (make-pane '<scribble-pane>
		     :popup-menu-callback
		     #'(lambda (sheet x y)
			 (let ((frame (sheet-frame sheet)))
			   (popup-scribble-menu frame x y)))
		     :width 300 :max-width $fill
		     :height 200 :max-height $fill))
   (:pane file-menu (frame)
          (make-pane '<menu>
		     :label "File"
		     :children
		     (vector (make-pane '<menu-button>
					:label "&Clear"
					:selection-mode :none
					:activate-callback
					#'(lambda (button)
					    (clear-output-history (surface frame))))
			     (make-pane '<menu-button>
					:label "E&xit"
					:selection-mode :none
					:activate-callback
					#'(lambda (button)
					    (exit-frame (sheet-frame button)))))))
   (:pane scribble-popup-menu (frame)
          (make-pane '<menu>
		     :owner (surface frame)
		     :children
		     (vector (make-pane '<menu-button>
					:label "&Clear"
					:selection-mode :none
					:activate-callback
					#'(lambda (button)
					    (declare (ignore button))
					    (clear-output-history (surface frame)))))))
   (:layout (frame) (surface frame))
   (:menu-bar (frame)
              (make-pane '<menu-bar>
			 :children
			 (vector (file-menu frame))))))

#||
define method popup-scribble-menu
    (frame :: <scribble-frame>, x :: <integer>, y :: <integer>) => ()
  let menu = frame.scribble-popup-menu;
  display-menu(menu, x: x, y: y)
end method popup-scribble-menu;
||#

(defmethod popup-scribble-menu ((frame <scribble-frame>)
                                x y)
  (let ((menu (scribble-popup-menu frame)))
    (display-menu menu :x x :y y)))

#||
install-example(<scribble-frame>, "Scribble");
||#

(install-example (find-class '<scribble-frame>) "Scribble")


#||
/// 'with-room-for-graphics' test

define class <wrfg-pane> (<recording-pane>)
end class <wrfg-pane>;
||#

(define-dylan-class (:open) <wrfg-pane> (<recording-pane>) ())

#||
define method handle-button-event
    (sheet :: <wrfg-pane>, 
     event :: <button-press-event>, 
     button == $left-button) => ()
  let medium = sheet-medium(sheet);
  let x = event-x(event);
  let y = event-y(event);
  with-drawing-options (medium, pen: $dotted-pen)
    draw-line(sheet, x, y, x + 20, y + 20)
  end;
  with-drawing-options (medium, pen: $solid-pen)
    with-room-for-graphics (sheet, x: x, y: y)
      draw-line(sheet, 0, 0, 20, 20)
    end
  end;
end method handle-button-event;
||#

(defmethod handle-button-event ((sheet <wrfg-pane>)
                                (event <button-press-event>)
                                (button (eql $left-button)))
  (let ((medium (sheet-medium sheet))
        (x (event-x event))
        (y (event-y event)))
    (with-drawing-options (medium :pen $dotted-pen)
      (draw-line sheet x y (+ x 20) (+ y 20)))
    (with-drawing-options (medium :pen $solid-pen)
      (with-room-for-graphics (sheet :x x :y y)
        (draw-line sheet 0 0 20 20)))))

#||
define frame <wrfg-frame> (<simple-frame>)
  pane surface (frame)
    make(<wrfg-pane>,
	 width:  300, max-width:  $fill,
	 height: 200, max-height: $fill);
  pane file-menu (frame)
    make(<menu>,
	 label: "File",
	 children: vector(make(<menu-button>,
			       label: "&Clear",
			       selection-mode: #"none",
			       activate-callback: method (button)
						    clear-output-history(frame.surface)
						  end),
			  make(<menu-button>,
			       label: "E&xit",
			       selection-mode: #"none",
			       activate-callback: method (button)
						    exit-frame(sheet-frame(button))
						  end)));
  layout (frame) frame.surface;
  menu-bar (frame)
    make(<menu-bar>, children: vector(frame.file-menu));
end frame <wrfg-frame>;
||#

(define-frame <wrfg-frame> (<simple-frame>)
  ((:pane surface (frame)
          (make-pane '<wrfg-pane>
		     :width 300 :max-width $fill
		     :height 200 :max-height $fill))
   (:pane file-menu (frame)
          (make-pane '<menu>
		     :label "File"
		     :children
		     (vector (make-pane '<menu-button>
					:label "&Clear"
					:selection-mode :none
					:activate-callback
					#'(lambda (button)
					    (clear-output-history (surface frame))))
			     (make-pane '<menu-button>
					:label "E&xit"
					:selection-mode :none
					:activate-callback
					#'(lambda (button)
					    (exit-frame (sheet-frame button)))))))
   (:layout (frame) (surface frame))
   (:menu-bar (frame)
              (make-pane '<menu-bar>
			 :children
			 (vector (file-menu frame))))))

#||
install-example(<wrfg-frame>, "Room...");
||#

(install-example (find-class '<wrfg-frame>) "Room...")


#||
/// Initialization

start-examples();

// end
||#

(start-examples)
