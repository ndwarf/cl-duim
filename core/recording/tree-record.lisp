;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-RECORDING-INTERNALS -*-
(in-package #:duim-recording-internals)

#||
/// "Coordinate sorted sets"

// '%children' kept in "lexicographic" order based on the bottom right corner
// of each child.  '%maximum-height' is the height of the tallest child.
define open abstract class <tree-record> (<basic-composite-record>)
  sealed slot %children = #f;
  sealed slot %maximum-height :: <integer> = 0;
end class <tree-record>;
||#

(defclass <tree-record>
    (<basic-composite-record>)
  ((%children :initform nil :accessor %children)
   (%maximum-height :type integer :initform 0 :accessor %maximum-height))
  (:documentation
"
'%children' kept in 'lexicographic' order based on the bottom right corner
of each child. '%maximum-height' is the height of the tallest child.
")
  (:metaclass <abstract-metaclass>))


#||
define sealed class <concrete-tree-record> (<tree-record>)
end class <concrete-tree-record>;
||#

(defclass <concrete-tree-record> (<tree-record>) ())


#||
define sealed inline method make
    (class == <tree-record>, #rest initargs, #key, #all-keys)
 => (pane :: <concrete-tree-record>)
  apply(make, <concrete-tree-record>, initargs)
end method make;
||#

(defmethod make-tree-record (&rest initargs &key &allow-other-keys)
  (apply #'make-instance '<concrete-tree-record> initargs))


#||
define sealed domain make (singleton(<concrete-tree-record>));
define sealed domain initialize (<concrete-tree-record>);

define output-record-constructor <tree-record>
    (#key children, parent, sheet, region, transform)
  children: children, parent: parent, sheet: sheet,
  region: region, transform: transform
end;
||#

(define-output-record-constructor <tree-record> (&key children parent sheet region transform)
  (:children  children :parent parent :sheet sheet
	      :region region :transform transform))


#||
define method initialize (record :: <tree-record>, #key sheet, children)
  ignore(sheet);
  next-method();
  record.%children := make(<stretchy-vector>);
  when (children)
    for (child in children)
      add-child(record, child)
    end
  end
end method initialize;
||#

(defmethod initialize-instance :after ((record <tree-record>) &key sheet children &allow-other-keys)
  (declare (ignore sheet))
  (setf (%children record) (make-array 0 :adjustable t :fill-pointer t))
  (when children
    (map nil #'(lambda (child)
		 (add-child record child))
	 children)))


#||
define method sheet-children
    (record :: <tree-record>) => (children :: <vector>)
  record.%children
end method sheet-children;
||#

(defmethod sheet-children ((record <tree-record>))
  (%children record))


#||
//--- Do we need a fast way to clear an output record?
define method sheet-children-setter
    (children :: <vector>, record :: <tree-record>) => (children :: <vector>)
  until (empty?(record.%children))
    remove-child(record, record.%children[0])
  end;
  // Reset the region before adding the new children
  sheet-region(record) := set-box-edges(sheet-region(record), 0, 0, 0, 0);
  record.%maximum-height := 0;
  for (child in children)
    add-child(record, child);
  end;
  children
end method sheet-children-setter;
||#

;;--- Do we need a fast way to clear an output record?
(defmethod (setf sheet-children) ((children vector) (record <tree-record>))
  (loop until (empty? (%children record))
     do (remove-child record (aref (%children record) 0)))
  ;; Reset the region before adding the new children
  (setf (sheet-region record) (set-box-edges (sheet-region record) 0 0 0 0))
  (setf (%maximum-height record) 0)
  (loop for child across children
     do (add-child record child))
  children)


#||
define method sheet-child-count
    (record :: <tree-record>, #key fast?) => (count :: <integer>)
  ignore(fast?);
  size(record.%children)
end method sheet-child-count;
||#

(defmethod sheet-child-count ((record <tree-record>) &key fast?)
  (declare (ignore fast?))
  (length (%children record)))


#||
define method sheet-element
    (record :: <tree-record>, index) => (record :: <output-record>)
  record.%children[index]
end method sheet-element;
||#

(defmethod sheet-element ((record <tree-record>) index)
  (aref (%children record) index))


#||
define method do-add-child
    (record :: <tree-record>, child :: <sheet>,
     #key index: preferred-index = #"end") => ()
  block (return)
    let children = record.%children;
    let fp = size(children);
    max!(record.%maximum-height, box-height(child));
    let (left, top, right, bottom) = box-edges(child);
    transform-coordinates!(sheet-transform(child), left, top, right, bottom);
    // Quick check for doing output at the bottom of the window
    if (zero?(fp)
	| begin
	    let other-child = children[fp - 1];
	    when (other-child == child)
	      return()
	    end;
	    let (oleft, otop, oright, obottom) = box-edges(other-child);
	    ignore(oleft, otop);
	    transform-coordinates!(sheet-transform(other-child), oright, obottom);
	    bottom > obottom | (bottom = obottom & right >= oright)
	  end)
      add!(children, child)
    else
      let index = index-for-position(children, right, bottom);
      unless (preferred-index == #"start")
	// Make sure that the new child comes after any child it overlaps
	// so that replaying happens in the right order.
	block (break)
	  while (#t)
	    if (index < fp
		& begin
		    let other-child = children[index];
		    let (oleft, otop, oright, obottom) = box-edges(other-child);
		    transform-coordinates!(sheet-transform(other-child),
					   oleft, otop, oright, obottom);
		    ltrb-intersects-ltrb?(left, top, right, bottom,
					  oleft, otop, oright, obottom)
		  end)
	      inc!(index)
	    else
	      break()
	    end
	  end
	end
      end;
      insert-at!(children, child, index)
    end
  end
end method do-add-child;
||#

(defmethod do-add-child ((record <tree-record>) (child <sheet>)
                         &key ((:index preferred-index) :end))
    (let* ((children (%children record))
           (fp (length children)))
      (setf (%maximum-height record) (max (%maximum-height record) (box-height child)))
      (multiple-value-bind (left top right bottom)
          (box-edges child)
        (transform-coordinates! (sheet-transform child) left top right bottom)
        ;; Quick check for doing output at the bottom of the window
        (if (or (zerop fp)
                (let ((other-child (aref children (- fp 1))))
                  (when (eql other-child child)
                    (return-from do-add-child nil))
                  (multiple-value-bind (oleft otop oright obottom)
                      (box-edges other-child)
                    (declare (ignore oleft otop))
                    (transform-coordinates! (sheet-transform other-child) oright obottom)
                    (or (> bottom obottom) (and (= bottom obottom) (>= right oright))))))
            (add! children child)
            ;; else
            (let ((index (index-for-position children right bottom)))
              (unless (eql preferred-index :start)
                ;; Make sure that the new child comes after any child it overlaps
                ;; so that replaying happens in the right order.
                (block break
                  (loop while t
		     do (if (and (< index fp)
				 (let ((other-child (aref children index)))
				   (multiple-value-bind (oleft otop oright obottom)
				       (box-edges other-child)
				     (transform-coordinates! (sheet-transform other-child)
							     oleft otop oright obottom)
				     (ltrb-intersects-ltrb? left top right bottom
							    oleft otop oright obottom))))
			    (incf index)
			    ;; else
			    (return-from break nil)))))
              (insert-at! children child index))))))


#||
define method do-remove-child
    (record :: <tree-record>, child :: <sheet>) => ()
  let children = record.%children;
  let index
    = index-for-child(children, child)
      // If we couldn't find it with the binary search, try
      // again the hard way.  If these things were more
      // disciplined with respect to managing overlapping
      // records, we wouldn't have to resort to this.
      | position(children, child);
  when (index)
    remove-at!(children, index)
  end
end method do-remove-child;
||#

(defmethod do-remove-child ((record <tree-record>) (child <sheet>))
  (let* ((children (%children record))
         (index (or (index-for-child children child)
                    ;; If we couldn't find it with the binary search, try
                    ;; again the hard way.  If these things were more
                    ;; disciplined with respect to managing overlapping
                    ;; records, we wouldn't have to resort to this.
                    (position child children))))
    (when index
      (remove-at! children index))))


#||
define method do-replace-child
    (record :: <tree-record>, old-child :: <sheet>, new-child :: <sheet>) => ()
  remove-child(record, old-child);
  add-child(record, new-child)
end method do-replace-child;
||#

(defmethod do-replace-child ((record <tree-record>) (old-child <sheet>) (new-child <sheet>))
  (remove-child record old-child)
  (add-child record new-child))


#||
//---*** Arg! We don't have our hands on the child at this point!
// (define-method note-transform-changed
//		(child (record <tree-record>))
//   (bind ((children (slot-value record %children))
//	  (proper-index (index-for-child children child)))
//     (unless proper-index                                        ;this guy's misfiled now
//       (with-box-edges (left top right bottom) child
//	 (ignore left top)
//	 (set! proper-index (index-for-position children right bottom)))
//       (bind ((index (find-key children (curry id? child))))
//	 (if (> index proper-index)
//	     (for ((i+1 = index then i)
//		   (i = (- index 1) then (- i 1))
//		   (until (= i+1 proper-index)))
//	       (set! (element children i+1) (element children i)))
//	     (for ((i = index then i+1)
//		   (i+1 = (+ index 1) then (+ i+1 1))
//		   (until (= i proper-index)))
//	       (set! (element children i) (element children i+1))))
//	 (set! (element children proper-index) child)))))

define method do-sheet-children
    (function :: <function>, record :: <tree-record>,
     #key z-order :: <z-order> = #f) => ()
  let children = record.%children;
  let iteration-protocol
    = if (z-order == #"top-down") top-down-iteration-protocol
      else bottom-up-iteration-protocol end;
  for (child :: <sheet> in children using iteration-protocol)
    function(child)
  end
end method do-sheet-children;
||#

(defmethod do-sheet-children ((function function) (record <tree-record>) &key (z-order nil))
  (let ((children (%children record)))
    (if (eql z-order :top-down)
	(let ((start (- (length children) 1))
	      (end   0))
	  (loop for index from start downto end
	     do (let ((child (aref children index)))
		  (funcall function child))))
	;; else
	(loop for child across children
	   do (funcall function child)))))


#||
// Breadth-first recursive...
define method do-sheet-tree
    (function :: <function>, record :: <tree-record>) => ()
  dynamic-extent(function);
  function(record);
  let children = record.%children;
  without-bounds-checks
    for (i :: <integer> from 0 below size(children))
      let child = children[i];
      do-sheet-tree(function, child)
    end
  end
end method do-sheet-tree;
||#

;; Breadth-first recursive...
(defmethod do-sheet-tree ((function function) (record <tree-record>))
  (declare (dynamic-extent function))
  (funcall function record)
  (let ((children (%children record)))
    (without-bounds-checks
      (loop for i from 0 below (length children)
	 do (let ((child (aref children i)))
	      (do-sheet-tree function child))))))


#||
// X and Y are in the coordinate system of the tree record
define method do-children-containing-position
    (function :: <function>, record :: <tree-record>, x :: <real>, y :: <real>) => ()
  let children = record.%children;
  let fp = size(children);
  let bound = record.%maximum-height;
  let start = index-for-position(children, 0, y + bound + 1);
  let limit = y - bound;
  block (return)
    without-bounds-checks
      for (index :: <integer> = min(fp - 1, start) then index - 1,
	   until: index < 0)
	let child = children[index];
	// Get the position into the coordinate system of the child
	let (left, top, right, bottom) = box-edges(child);
	transform-coordinates!(sheet-transform(child), left, top, right, bottom);
	when (ltrb-contains-position?(left, top, right, bottom, x, y))
	  function(child)
	end;
	when (bottom < limit)
	  return()
	end
      end
    end
  end
end method do-children-containing-position;
||#

;; X and Y are in the coordinate system of the tree record
(defmethod do-children-containing-position ((function function) (record <tree-record>) (x real) (y real))
  (let* ((children (%children record))
         (fp (length children))
         (bound (%maximum-height record))
         (start (index-for-position children 0 (+ y bound 1)))
         (limit (- y bound)))
    (without-bounds-checks
      (loop for index = (min (- fp 1) start) then (- index 1)
	 until (< index 0)
	 do (let ((child (aref children index)))
	      ;; Get the position into the coordinate system of the child
	      (multiple-value-bind (left top right bottom)
		  (box-edges child)
		(transform-coordinates! (sheet-transform child) left top right bottom)
		(when (ltrb-contains-position? left top right bottom x y)
		  (funcall function child))
		(when (< bottom limit)
		  (return-from do-children-containing-position nil))))))))


#||
// The region is in the coordinate system of the tree record
define method do-children-overlapping-region
    (function :: <function>, record :: <tree-record>, region :: <region>) => ()
  let children = record.%children;
  if (everywhere?(region))
    do(function, children)
  else
    let fp = size(children);
    let (left, top, right, bottom) = box-edges(region);
    let start = index-for-position(children, 0, top);
    let limit = bottom + record.%maximum-height;
    block (return)
      without-bounds-checks
	for (index :: <integer> = start then index + 1,
	     until: index = fp)
	  let child = children[index];
	  let (cleft, ctop, cright, cbottom) = box-edges(child);
	  transform-coordinates!(sheet-transform(child),
				 cleft, ctop, cright, cbottom);
	  when (ltrb-intersects-ltrb?(left, top, right, bottom,
				      cleft, ctop, cright, cbottom))
	    function(child)
	  end;
	  when (cbottom > limit)
	    return()
	  end
	end
      end
    end
  end
end method do-children-overlapping-region;
||#

;; The region is in the coordinate system of the tree record
(defmethod do-children-overlapping-region ((function function) (record <tree-record>) (region <region>))
  (let ((children (%children record)))
    (if (everywhere? region)
        (map nil function children)
        ;; else
        (let ((fp (length children)))
          (multiple-value-bind (left top right bottom)
              (box-edges region)
            (let ((start (index-for-position children 0 top))
                  (limit (+ bottom (%maximum-height record))))
              (without-bounds-checks
                (loop for index = start then (+ index 1)
		   :until (= index fp)
		   do (let ((child (aref children index)))
			(multiple-value-bind (cleft ctop cright cbottom)
			    (box-edges child)
			  (transform-coordinates! (sheet-transform child)
						  cleft ctop cright cbottom)
			  (when (ltrb-intersects-ltrb? left top right bottom
						       cleft ctop cright cbottom)
			    (funcall function child))
			  (when (> cbottom limit)
			    (return-from do-children-overlapping-region nil))))))))))))



#||

/// Utilities...

// Like 'find-key', but searches coordinate sorted sets for a given child
define method index-for-child
    (vector :: <vector>, record :: <output-record>) => (index :: false-or(<integer>))
  block (return)
    let (left, top, right, bottom) = box-edges(record);
    transform-coordinates!(sheet-transform(record), left, top, right, bottom);
    // Binary search to find where this one goes.
    let fp = size(vector);
    let initial-index = index-for-position(vector, right, bottom);
    // Search back over things in the same place, accounting for overlap
    when (initial-index < fp)
      without-bounds-checks
	for (index :: <integer> from initial-index to 0 by -1)
	  let child = vector[index];
	  when (child == record)
	    return(index)
	  end;
	  let (cleft, ctop, cright, cbottom) = box-edges(child);
	  transform-coordinates!(sheet-transform(child),
				 cleft, ctop, cright, cbottom);
	  unless (right = cright & bottom = cbottom)
	    unless (ltrb-intersects-ltrb?(left, top, right, bottom,
					  cleft, ctop, cright, cbottom))
	      return(#f)
	    end
	  end
	end
      end
    end;
    // Search forward too, also accounting for overlap
    without-bounds-checks
      for (index :: <integer> from (if (initial-index < fp) initial-index + 1 else 0 end) below fp)
	let child = vector[index];
	when (child == record)
	  return(index)
	end;
	let (cleft, ctop, cright, cbottom) = box-edges(child);
	transform-coordinates!(sheet-transform(child),
			       cleft, ctop, cright, cbottom);
	when (cbottom > bottom)
	  unless (ltrb-intersects-ltrb?(left, top, right, bottom,
					cleft, ctop, cright, cbottom))
	    return(#f)
	  end
	end
      end
    end
  end
end method index-for-child;
||#

(defgeneric index-for-child (vector record))

(defmethod index-for-child ((vector vector) (record <output-record>))
  (multiple-value-bind (left top right bottom)
      (box-edges record)
    (transform-coordinates! (sheet-transform record) left top right bottom)
    ;; Binary search to find where this one goes.
    (let ((fp (length vector))
          (initial-index (index-for-position vector right bottom)))
      ;; Search back over things in the same place, accounting for overlap
      (when (< initial-index fp)
        (without-bounds-checks
	  (loop for index from initial-index downto 0 by 1  ;;-1
	     do (let ((child (aref vector index)))
		  (when (eql child record)
		    (return-from index-for-child index))
		  (multiple-value-bind (cleft ctop cright cbottom)
		      (box-edges child)
		    (transform-coordinates! (sheet-transform child)
					    cleft ctop cright cbottom)
		    (unless (and (= right cright) (= bottom cbottom))
		      (unless (ltrb-intersects-ltrb? left top right bottom
						     cleft ctop cright cbottom)
			(return-from index-for-child nil))))))))
      ;; Search forward too, also accounting for overlap
      (without-bounds-checks
        (loop for index from (if (< initial-index fp) (+ initial-index 1) 0) below fp
	   do (let ((child (aref vector index)))
		(when (eql child record)
		  (return-from index-for-child index))
		(multiple-value-bind (cleft ctop cright cbottom)
		    (box-edges child)
		  (transform-coordinates! (sheet-transform child)
					  cleft ctop cright cbottom)
		  (when (> cbottom bottom)
		    (unless (ltrb-intersects-ltrb? left top right bottom
						   cleft ctop cright cbottom)
		      (return-from index-for-child nil))))))))))


#||
// Binary search; dictionary order Y, X.
// X and Y are in the coordinate space of the tree record
define method index-for-position
    (vector :: <vector>, right, bottom) => (index :: <integer>)
  let below :: <integer> = 0;
  let above :: <integer> = size(vector);
  block (return)
    while (#t)
      when (above = below)
        return(above)
      end;
      let index :: <integer> = ash(above + below, -1);
      let child = vector[index];
      let (cleft, ctop, cright, cbottom) = box-edges(child);
      ignore(cleft, ctop);
      transform-coordinates!(sheet-transform(child), cright, cbottom);
      case
        bottom < cbottom | (bottom = cbottom & right < cright) =>
          above := index;
        bottom > cbottom | (bottom = cbottom & right > cright) =>
          if (below = index)
            return(above)
          else
            below := index
          end;
        otherwise =>
          return(index)
      end
    end
  end
end method index-for-position;
||#

;; Binary search; dictionary order Y, X.
(defgeneric index-for-position (vector right bottom))

(defmethod index-for-position ((vector vector) right bottom)
  (let ((below 0)
        (above (length vector)))
    (loop while t
       do (when (= above below)
	    (return-from index-for-position above))
       do (let* ((index (ash (+ above below) -1))
		 (child (aref vector index)))
	    (multiple-value-bind (cleft ctop cright cbottom)
		(box-edges child)
	      (declare (ignore cleft ctop))
	      (transform-coordinates! (sheet-transform child) cright cbottom)
	      (cond ((or (< bottom cbottom)
			 (and (= bottom cbottom)
			      (< right cright)))
		     (setf above index))
		    ((or (> bottom cbottom)
			 (and (= bottom cbottom)
			      (> right cright)))
		     (if (= below index)
			 (return-from index-for-position above)
			 (setf below index)))
		    (t
		     (return-from index-for-position index))))))))



#||

/// Tree output history

define sealed class <tree-output-history>
    (<output-history-mixin>, <tree-record>)
end class <tree-output-history>;
||#

(defclass <tree-output-history>
    (<output-history-mixin> <tree-record>)
  ())

#||
define sealed domain make (singleton(<tree-output-history>));
define sealed domain initialize (<tree-output-history>);
||#



