
DUIM-RECORDING

Need to go through all this and work out what recording is doing; particularly
since it's not actually complete and will need finishing off at some point
(although it's not too horrendously urgent since there aren't higher-level
facilities that rely on recording, yet).


FIGURE-RECORDING.LISP:

Basically this provides methods for all the drawing operations for
<OUTPUT-RECORDING-MIXIN> sheets that records what was drawn, the drawing
options, and the position as an output record (if we're actually recording at
the moment). If we really want to draw (output records can be replayed without
drawing I guess), we then invoke the next method (the drawing method on the
<DRAWABLE>) to actually render something.

Apparently COORDINATE-SEQUENCE-BOX is broken; need to investigate this and
maybe fix it up (see comment in DRAW-POINTS). I'm not sure the mentioned
method even exists!

WITH-NEW-OUTPUT-RECORD? This seems to be used when we record a figure that is
made up of several discrete figures (for example, arrows are made up of lines
and filled polygons). Presumably we want all pieces to be captured in the same
output record.

TEXT-BOUNDING-BOX is suspect; surely different alignments and rotations will
affect the bounding box?


GADGET-RECORD.LISP:

This needs writing / debugging. Read CLIM spec to work out what's going on.


OUTPUT-RECORDING-MIXIN.LISP:
PATH-RECORDING.LISP:

This needs writing! I have no idea how it's supposed to work. Think about.


RECORDING-CLASSES.LISP:

Note that we treat output records as sheets for the purposes of setting
transforms etc.

I'm not sure about the :AFTER methods in this file.

Check WITH-RECORD-MEDIUM-STATE macro expands into the expected code...


RECORDING-DEFS.LISP:

There's an explanation of what is going on in some of the recording classes in
this file which should be digested at some point.


RECORDING-MACROS.LISP:

Read the output recording section in the CLIM spec sometime...


RECORDING-SHEETS.LISP:

Note that MEDIUM-+Y-UPWARD? on medium is dynamically bound in
DO-WITH-FIRST-QUADRANT-COORDINATES.

Need to test / understand output recording and how it can be built on to
provide accept / present, incremental redisplay, etc. etc.


SEQUENCE-RECORD.LISP:

This just stores output records as a linear sequence. This should be
reasonably efficient if there is a small number of output records, or if we
always want to traverse over all the output records in order.

*** GENERAL NOTE: Things we iterate over using TOP-DOWN or BOTTOM-UP iteration
    protocols probably want to be held as VECTORS so we can just step
    start->end or end->start without a performance penalty. This basically
    means sheet children and output record children to get the Z-ordering
    right. Everything else should probably be a list.


TRACKING-POINTER.LISP:

What's 'tracking-pointer' used for? Highlightling output records as we pass
over them?

Need an implementation for 'dragging-output'.


TREE-RECORD.LISP:

Keeps track of output records in an R-tree structure so that finding output
records by position is fast. This is likely to be used where many output
records are generated and we want some kind of 'random access' to them (maybe
in graph output, for example).





