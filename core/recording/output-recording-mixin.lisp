;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-RECORDING-INTERNALS -*-
(in-package #:duim-recording-internals)

;;; Moved from RECORDING;RECORDING-SHEETS.LISP to shush compiler.

;;; Output recording sheet mixin

;; Note that this needs to precede any stream classes (<input-stream-mixin>,
;; <output-stream-mixin>) in any class precedence list

(defclass <output-recording-mixin> (<abstract-sheet>)
  ((sheet-drawing? :type boolean :initarg :draw? :initform t :accessor sheet-drawing?)
   (sheet-recording? :type boolean :initarg :record? :initform t :accessor sheet-recording?)
   (sheet-output-history :type <output-record> :initarg :output-history
			 :initform (make-instance '<tree-output-history>)
			 :reader sheet-output-history :writer %output-history-setter)
   (sheet-output-record :type (or null <output-record>) :initform nil :accessor sheet-output-record)
   (sheet-output-record-position :initform (make-point 0 0) :accessor sheet-output-record-position)
   (%medium-state :type (or null <medium-state>) :initform nil :accessor %medium-state)
   (stream-text-output-record :type (or null <output-record>) :initform nil :accessor stream-text-output-record)
   (sheet-highlighted-record :type (or null <output-record>) :initform nil :accessor sheet-highlighted-record)
   (sheet-redisplay-record :type (or null <output-record>) :initform nil :accessor sheet-redisplay-record)
   (sheet-redisplaying? :initform nil :accessor sheet-redisplaying?))
  (:documentation "This needs to precede any stream
  classes (<INPUT-STREAM-MIXIN>, <OUTPUT-STREAM-MIXIN> in any class
  precedence list.")
  (:metaclass <abstract-metaclass>))
