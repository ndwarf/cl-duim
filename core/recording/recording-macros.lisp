;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-RECORDING-INTERNALS -*-
(in-package #:duim-recording-internals)

#||
define macro with-end-of-line-action
  { with-end-of-line-action (?sheet:variable, ?action:expression)
      ?:body
    end }
    => { begin
	   let with-end-of-line-action-body = method () ?body end;
           do-with-end-of-line-action(?sheet, with-end-of-line-action-body, ?action)
         end }
end macro with-end-of-line-action;
||#

(defmacro with-end-of-line-action ((sheet action) &rest body)
  (let ((with-end-of-line-action-body (gensym "WITH-END-OF-LINE-ACTN-BDY-")))
    `(let ((,with-end-of-line-action-body #'(lambda () ,@body)))
       (do-with-end-of-line-action ,sheet ,with-end-of-line-action-body ,action))))


#||
define macro with-end-of-page-action
  { with-end-of-page-action (?sheet:variable, ?action:expression)
      ?:body
    end }
    => { begin
	   let with-end-of-page-action-body = method () ?body end;
           do-with-end-of-page-action(?sheet, with-end-of-page-action-body, ?action)
         end }
end macro with-end-of-page-action;
||#

(defmacro with-end-of-page-action ((sheet action) &rest body)
  (let ((with-end-of-page-action-body (gensym "WITH-END-OF-PAGE-ACTN-BDY-")))
    `(let ((,with-end-of-page-action-body #'(lambda () ,@body)))
       (do-with-end-of-page-action ,sheet ,with-end-of-page-action-body ,action))))


#||
// Options can be DRAW?: and RECORD?:
define macro with-output-recording-options
  { with-output-recording-options (?sheet:variable, #rest ?options:expression)
      ?:body
    end }
    => { begin
           let with-output-recording-options-body = method () ?body end;
           do-with-output-recording-options
             (?sheet, with-output-recording-options-body, ?options)
         end }
end macro with-output-recording-options;
||#

(defmacro with-output-recording-options ((sheet &rest options) &body body)
  `(let ((with-output-recording-options-body #'(lambda () ,@body)))
    (do-with-output-recording-options ,sheet with-output-recording-options-body ,@options)))


#||
// Options are output record initargs, plus RECORD-CLASS:
define macro with-new-output-record
  { with-new-output-record (?record:variable = ?sheet:variable, #rest ?options:expression)
      ?:body
    end }
    => { begin
	   let with-new-output-record-body
	     = method (?record) ?body end;
           do-with-new-output-record
             (?sheet, with-new-output-record-body, ?options)
         end }
  { with-new-output-record (?sheet:variable, #rest ?options:expression)
      ?:body
    end }
    => { begin
           let with-new-output-record-body
	     = method (_record) ignore(_record); ?body end;
           do-with-new-output-record
             (?sheet, with-new-output-record-body, ?options)
         end }
end macro with-new-output-record;
||#

(defmacro with-new-output-record ((sheet-or-record/sheet &rest options)
				  &body body)
"Invoked in one of the following ways:-

(with-new-output-record ((record = sheet) option1 option2 ...) ...)
(with-new-output-record (sheet option1 option2 ...) ...)

In the former case, _record_ becomes a name that can be used in the body
(i.e. it is captured in the macro).
"
  (let ((record (if (listp sheet-or-record/sheet)
                    (first sheet-or-record/sheet)
                    (gensym "RECORD-")))
        (sheet  (if (listp sheet-or-record/sheet)
                    (third sheet-or-record/sheet)
                    sheet-or-record/sheet)))
    `(let ((with-new-output-record-body #'(lambda (,record)
                                            (declare (ignorable ,record))
                                            ,@body)))
      (do-with-new-output-record ,sheet
                                 with-new-output-record-body
                                 ,@options))))


#||
// Options are output record initargs, plus RECORD-CLASS:
define macro with-output-to-output-record
  { with-output-to-output-record (?record:variable = ?sheet:variable, #rest ?options:expression)
      ?:body
    end }
    => { begin
           let with-output-to-output-record-body
             = method (?record)
                 with-caret-position-saved (?sheet) ?body end
               end;
           do-with-output-to-output-record
             (?sheet, with-output-to-output-record-body, ?options)
         end }
  { with-output-to-output-record (?sheet:variable, #rest ?options:expression)
      ?:body
    end }
    => { begin
           let with-output-to-output-record-body
             = method (_record)
		 ignore(_record);
                 with-caret-position-saved (?sheet) ?body end
               end;
           do-with-output-to-output-record
             (?sheet, with-output-to-output-record-body, ?options)
         end }
end macro with-output-to-output-record;
||#

(defmacro with-output-to-output-record ((sheet-or-record/sheet &rest options)
					&body body)
  (let ((record (if (listp sheet-or-record/sheet)
                    (first sheet-or-record/sheet)
                    (gensym "RECORD-")))
        (sheet  (if (listp sheet-or-record/sheet)
                    (third sheet-or-record/sheet)
                    sheet-or-record/sheet)))
    `(let ((with-output-to-output-record-body
	        #'(lambda (,record)
		    (declare (ignorable ,record))
		    (with-caret-position-saved (,sheet)
		        ,@body))))
      (do-with-output-to-output-record ,sheet with-output-to-output-record-body ,@options))))



#||

/// Graphics

define macro with-local-coordinates
  { with-local-coordinates (?sheet:variable, #rest ?options:expression)
      ?:body
    end }
    => { begin
           let with-local-coordinates-body = method () ?body end;
           do-with-local-coordinates
             (?sheet, with-local-coordinates-body, ?options)
         end }
end macro with-local-coordinates;
||#

(defmacro with-local-coordinates ((sheet &rest options) &body body)
  `(let ((with-local-coordinates-body #'(lambda () ,@body)))
    (do-with-local-coordinates ,sheet with-local-coordinates-body ,@options)))


#||
define macro with-first-quadrant-coordinates
  { with-first-quadrant-coordinates (?sheet:variable, #rest ?options:expression)
      ?:body
    end }
    => { begin
           let with-first-quadrant-coordinates-body = method () ?body end;
           do-with-first-quadrant-coordinates
             (?sheet, with-first-quadrant-coordinates-body, ?options)
         end }
end macro with-first-quadrant-coordinates;
||#

(defmacro with-first-quadrant-coordinates ((sheet &rest options) &body body)
  `(let ((with-first-quadrant-coordinates-body #'(lambda () ,@body)))
    (do-with-first-quadrant-coordinates ,sheet with-first-quadrant-coordinates-body ,@options)))


#||
// Options are output record initargs, plus RECORD-CLASS:, X:, Y:
define macro with-room-for-graphics
  { with-room-for-graphics (?sheet:variable, #rest ?options:expression)
      ?:body
    end }
    => { begin
           let with-room-for-graphics-body = method () ?body end;
           do-with-room-for-graphics
             (?sheet, with-room-for-graphics-body, ?options)
         end }
end macro with-room-for-graphics;
||#

(defmacro with-room-for-graphics ((sheet &rest options) &body body)
  `(let ((with-room-for-graphics-body #'(lambda () ,@body)))
    (do-with-room-for-graphics ,sheet with-room-for-graphics-body ,@options)))


