;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-RECORDING-INTERNALS -*-
(in-package :duim-recording-internals)

#||
/// Figure graphics output recording

// The base class for graphical displayed output records
// NB: we overload 'medium-state-pen' to indicate whether a figure is filled or not
define open abstract class <graphics-record> (<basic-leaf-record>)
end class <graphics-record>;

define protocol-predicate graphics-record;
||#

(defclass <graphics-record> (<basic-leaf-record>)
  ()
  (:documentation
"
The base class for graphical displayed output records.
NB: we overload 'medium-state-pen' to indicate whether a figure is filled or not.
")
  (:metaclass <abstract-metaclass>))

(define-protocol-predicate graphics-record)



#||

/// 'draw-point'

define sealed class <point-record> (<graphics-record>)
  sealed slot %x, required-init-keyword: x:;
  sealed slot %y, required-init-keyword: y:;
end class <point-record>;

define sealed domain make (singleton(<point-record>));
define sealed domain initialize (<point-record>);
||#

(defclass <point-record> (<graphics-record>)
  ((%x :initarg :x :initform (required-slot ":x" "<point-record>") :accessor %x)
   (%y :initarg :y :initform (required-slot ":y" "<point-record>") :accessor %y)))


#||
// See 'draw-line' for commentary on what exactly is happening here
define method draw-point
    (sheet :: <output-recording-mixin>, x, y) => (record)
  let record = #f;
  when (sheet-recording?(sheet))
    let medium = sheet-medium(sheet);
    let medium-state = sheet-medium-state(sheet);
    transform-coordinates!(medium-transform(medium), x, y);
    let (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
      = begin
          let thickness = pen-width(medium-pen(medium));
          fix-box(x, y, x + thickness, y + thickness)
        end;
    let (rx, ry) = point-position(sheet-output-record-position(sheet));
    let transform = make(<mutable-translation-transform>, tx: left - rx, ty: top - ry);
    let region    = make-bounding-box(0, 0, right - left, bottom - top);
    record := make(<point-record>,
		   x: x - left, y: y - top,
		   region: region, transform: transform,
		   medium-state: medium-state);
    add-output-record(sheet, record)
  end;
  when (sheet-drawing?(sheet))
    next-method()
  end;
  record
end method draw-point;
||#

;; See 'draw-line' for commentary on what exactly is happening here
(defmethod draw-point ((sheet <output-recording-mixin>) x y)
  (let ((record nil))
    (when (sheet-recording? sheet)
      (let ((medium (sheet-medium sheet))
            (medium-state (sheet-medium-state sheet)))
        (transform-coordinates! (medium-transform medium) x y)
        (multiple-value-bind (left top right bottom)
            (let ((thickness (pen-width (medium-pen medium))))
              (fix-box x y (+ x thickness) (+ y thickness)))
          (multiple-value-bind (rx ry)
              (point-position (sheet-output-record-position sheet))
            (let ((transform (make-instance '<mutable-translation-transform> :tx (- left rx) :ty (- top ry)))
                  (region (make-bounding-box 0 0 (- right left) (- bottom top))))
              (setf record (make-instance '<point-record>
					  :x (- x left)
					  :y (- y top)
					  :region region :transform transform
					  :medium-state medium-state))
              (add-output-record sheet record))))))
    (when (sheet-drawing? sheet)
      (call-next-method))
    record))


#||
define method handle-repaint
    (record :: <point-record>, medium :: <medium>, region :: <region>) => ()
  ignore(region);
  when (default-background(record))
    repaint-background(record, medium)
  end;
  with-record-medium-state (medium, record)
    draw-point(medium, record.%x, record.%y)
  end
end method handle-repaint;
||#

(defmethod handle-repaint ((record <point-record>) (medium <medium>) (region <region>))
  (declare (ignore region))
  (when (default-background record)
    (repaint-background record medium))
  (with-record-medium-state (medium record)
    (draw-point medium (%x record) (%y record))))



#||

/// 'draw-points'

define sealed class <points-record> (<graphics-record>)
  sealed slot %coord-seq, required-init-keyword: coord-seq:;
end class <points-record>;

define sealed domain make (singleton(<points-record>));
define sealed domain initialize (<points-record>);
||#

(defclass <points-record> (<graphics-record>)
  ((%coord-seq :initarg :coord-seq :initform (required-slot ":coord-seq" "<points-record>") :accessor %coord-seq)))


#||
// See 'draw-lines' for commentary on what exactly is happening here
define method draw-points
    (sheet :: <output-recording-mixin>, coord-seq :: <coordinate-sequence>) => (record)
  let record = #f;
  when (sheet-recording?(sheet))
    let medium = sheet-medium(sheet);
    let medium-state = sheet-medium-state(sheet);
    let coord-seq
      = transform-coordinate-sequence
          (medium-transform(medium), coord-seq, copy?: #t);
    FOR (I FROM 0 BELOW SIZE(COORD-SEQ))	//---*** until 'coordinate-sequence-box' is fixed
      COORD-SEQ[I] := FLOOR(COORD-SEQ[I])
    END;
    let (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
      = begin
          let thickness = pen-width(medium-pen(medium));
          coordinate-sequence-box(coord-seq, thickness: thickness)
        end;
    let (rx, ry) = point-position(sheet-output-record-position(sheet));
    let transform = make(<mutable-translation-transform>, tx: left - rx, ty: top - ry);
    let region    = make-bounding-box(0, 0, right - left, bottom - top);
    translate-coordinate-sequence!(-left, -top, coord-seq);
    record := make(<points-record>,
		   coord-seq: coord-seq,
		   region: region, transform: transform,
		   medium-state: medium-state);
    add-output-record(sheet, record)
  end;
  when (sheet-drawing?(sheet))
    next-method()
  end;
  record
end method draw-points;
||#

(defmethod draw-points ((sheet <output-recording-mixin>) coord-seq)
  (check-type coord-seq <coordinate-sequence>)
  (let ((record nil))
    (when (sheet-recording? sheet)
      (let* ((medium (sheet-medium sheet))
	     (medium-state (sheet-medium-state sheet))
	     (coord-seq (transform-coordinate-sequence (medium-transform medium) coord-seq :copy? t)))
	;; FIXME?
        (LOOP FOR I FROM 0 BELOW (LENGTH COORD-SEQ)   ;;---*** until 'coordinate-sequence-box' is fixed
	   DO (SETF (AREF COORD-SEQ I) (FLOOR (AREF COORD-SEQ I))))
        (multiple-value-bind (left top right bottom)
            (let ((thickness (pen-width (medium-pen medium))))
              (coordinate-sequence-box coord-seq :thickness thickness))
          (multiple-value-bind (rx ry)
              (point-position (sheet-output-record-position sheet))
            (let ((transform (make-instance '<mutable-translation-transform> :tx (- left rx) :ty (- top ry)))
                  (region (make-bounding-box 0 0 (- right left) (- bottom top))))
              (translate-coordinate-sequence! (- left) (- top) coord-seq)
              (setf record (make-instance '<points-record>
					  :coord-seq coord-seq
					  :region region :transform transform
					  :medium-state medium-state))
              (add-output-record sheet record))))))
    (when (sheet-drawing? sheet)
      (call-next-method))
    record))


#||
define method handle-repaint
    (record :: <points-record>, medium :: <medium>, region :: <region>) => ()
  ignore(region);
  when (default-background(record))
    repaint-background(record, medium)
  end;
  with-record-medium-state (medium, record)
    draw-points(medium, record.%coord-seq)
  end
end method handle-repaint;
||#

(defmethod handle-repaint ((record <points-record>) (medium <medium>) (region <region>))
  (declare (ignore region))
  (when (default-background record)
    (repaint-background record medium))
  (with-record-medium-state (medium record)
    (draw-points medium (%coord-seq record))))


#||
define method refined-position-test
    (record :: <points-record>, x, y) => (true? :: <boolean>)
  block (return)
    let pen = medium-state-pen(record-medium-state(record));
    let thickness = pen-width(pen);
    local method position-in-ltrb? (px, py) => ()
	    when (ltrb-contains-position?(px - thickness, py - thickness, px, py,
					  x, y))
	      return(#t)
	    end
	  end method;
    do-coordinates(position-in-ltrb?, record.%coord-seq);
    #f
  end
end method refined-position-test;
||#

(defmethod refined-position-test ((record <points-record>) x y)
  (let* ((pen (medium-state-pen (record-medium-state record)))
	 (thickness (pen-width pen)))
    (labels ((position-in-ltrb? (px py)
               (when (ltrb-contains-position? (- px thickness) (- py thickness) px py x y)
                 (return-from refined-position-test t))))
      (do-coordinates #'position-in-ltrb? (%coord-seq record))
      nil)))


#||
define method highlight-output-record
    (record :: <points-record>, sheet :: <output-recording-mixin>, state) => ()
  ignore(state);
  let medium = sheet-medium(sheet);
  let pen = medium-state-pen(record-medium-state(record));
  let thickness = pen-width(pen);
  let transform = sheet-device-transform(record);
  with-drawing-options (medium, /* ---*** brush: $xor-brush, */ pen: $highlighting-pen)
    local method highlight (px, py) => ()
	    transform-coordinates!(transform, px, py);
	    draw-rectangle(medium,
			   px - thickness, py - thickness, px, py,
			   filled?: #f)
	  end method;
    do-coordinates(highlight, record.%coord-seq)
  end
end method highlight-output-record;
||#

(defmethod highlight-output-record ((record <points-record>) (sheet <output-recording-mixin>) state)
  (declare (ignore state))
  (let* ((medium (sheet-medium sheet))
	 (pen (medium-state-pen (record-medium-state record)))
	 (thickness (pen-width pen))
	 (transform (sheet-device-transform record)))
    (with-drawing-options (medium #||---*** :brush $xor-brush ||# :pen *highlighting-pen*)
      (labels ((highlight (px py)
		 (transform-coordinates! transform px py)
		 (draw-rectangle medium
				 (- px thickness) (- py thickness) px py
				 :filled? nil)))
        (do-coordinates #'highlight (%coord-seq record))))))



#||

/// 'draw-line'

define sealed class <line-record> (<graphics-record>)
  sealed slot %x1, required-init-keyword: x1:;
  sealed slot %y1, required-init-keyword: y1:;
  sealed slot %x2, required-init-keyword: x2:;
  sealed slot %y2, required-init-keyword: y2:;
end class <line-record>;

define sealed domain make (singleton(<line-record>));
define sealed domain initialize (<line-record>);
||#

(defclass <line-record> (<graphics-record>)
  ((%x1 :initarg :x1 :initform (required-slot ":x1" "<line-record>") :accessor %x1)
   (%y1 :initarg :y1 :initform (required-slot ":y1" "<line-record>") :accessor %y1)
   (%x2 :initarg :x2 :initform (required-slot ":x2" "<line-record>") :accessor %x2)
   (%y2 :initarg :y2 :initform (required-slot ":y2" "<line-record>") :accessor %y2)))


#||
define method draw-line
    (sheet :: <output-recording-mixin>, x1, y1, x2, y2) => (record)
  let record = #f;
  when (sheet-recording?(sheet))
    let medium = sheet-medium(sheet);
    let medium-state = sheet-medium-state(sheet);
    // Apply the medium transform to user-supplied coordinates
    transform-coordinates!(medium-transform(medium), x1, y1, x2, y2);
    // Compute the bounding box in the sheet's coordinate system
    let (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
      = begin
          let thickness = pen-width(medium-pen(medium)) - 1;
          // The convention is that we stroke paths on the outside,
          // where "outside" is biased to the left and top
          fix-box(min(x1, x2) - thickness, min(y1, y2) - thickness,
		  max(x1, x2) - thickness, max(y1, y2) - thickness)
        end;
    // Get the position of the parent record
    let (rx, ry) = point-position(sheet-output-record-position(sheet));
    // Now compute the region and transform for this record, making
    // them be 0-based to get the most efficient representation, with
    // the bounding box coordinates relative to the parent record
    let transform = make(<mutable-translation-transform>, tx: left - rx, ty: top - ry);
    let region    = make-bounding-box(0, 0, right - left, bottom - top);
    // Create the record with all user-supplied coordinates relative
    // to the bounding box
    record := make(<line-record>,
		   x1: x1 - left, y1: y1 - top, x2: x2 - left, y2: y2 - top,
		   region: region, transform: transform,
		   medium-state: medium-state);
    add-output-record(sheet, record)
  end;
  when (sheet-drawing?(sheet))
    next-method()
  end;
  record
end method draw-line;
||#

(defmethod draw-line ((sheet <output-recording-mixin>) x1 y1 x2 y2)
  (let ((record nil))
    (when (sheet-recording? sheet)
      (let ((medium (sheet-medium sheet))
            (medium-state (sheet-medium-state sheet)))
        ;; Apply the medium transform to user-supplied coordinates
        (transform-coordinates! (medium-transform medium) x1 y1 x2 y2)
        ;; Compute the bounding box in the sheet's coordinate system
        (multiple-value-bind (left top right bottom)
            (let ((thickness (- (pen-width (medium-pen medium)) 1)))
              ;; The convention is that we stroke paths on the outside,
              ;; where "outside" is biased to the left and top
              (fix-box (- (min x1 x2) thickness) (- (min y1 y2) thickness)
                       (- (max x1 x2) thickness) (- (max y1 y2) thickness)))
          ;; Get the position of the parent record
          (multiple-value-bind (rx ry)
              (point-position (sheet-output-record-position sheet))
            ;; Now compute the region and transform for this record, making
            ;; them be 0-based to get the most efficient representation, with
            ;; the bounding box coordinates relative to the parent record
            (let ((transform (make-instance '<mutable-translation-transform> :tx (- left rx) :ty (- top ry)))
                  (region (make-bounding-box 0 0 (- right left) (- bottom top))))
              ;; Create the record with all user-supplied coordinates relative
              ;; to the bounding box
              (setf record (make-instance '<line-record>
					  :x1 (- x1 left) :y1 (- y1 top) :x2 (- x2 left) :y2 (- y2 top)
					  :region region :transform transform
					  :medium-state medium-state))
              (add-output-record sheet record))))))
    (when (sheet-drawing? sheet)
      (call-next-method))
    record))


#||
define method handle-repaint
    (record :: <line-record>, medium :: <medium>, region :: <region>) => ()
  ignore(region);
  when (default-background(record))
    repaint-background(record, medium)
  end;
  with-record-medium-state (medium, record)
    draw-line(medium, record.%x1, record.%y1, record.%x2, record.%y2)
  end
end method handle-repaint;
||#

(defmethod handle-repaint ((record <line-record>) (medium <medium>) (region <region>))
  (declare (ignore region))
  (when (default-background record)
    (repaint-background record medium))
  (with-record-medium-state (medium record)
    (draw-line medium (%x1 record) (%y1 record) (%x2 record) (%y2 record))))


#||
define method refined-position-test
    (record :: <line-record>, x, y) => (true? :: <boolean>)
  let pen = medium-state-pen(record-medium-state(record));
  position-close-to-line?(x, y,
		          record.%x1, record.%y1, record.%x2, record.%y2,
		          thickness: pen-width(pen))
end method refined-position-test;
||#

(defmethod refined-position-test ((record <line-record>) x y)
  (let ((pen (medium-state-pen (record-medium-state record))))
    (position-close-to-line? x y
                             (%x1 record) (%y1 record) (%x2 record) (%y2 record)
                             :thickness (pen-width pen))))


#||
define method highlight-output-record
    (record :: <line-record>, sheet :: <output-recording-mixin>, state) => ()
  ignore(state);
  let medium = sheet-medium(sheet);
  let pen = medium-state-pen(record-medium-state(record));
  let transform = sheet-device-transform(record);
  let (x1, y1) = transform-position(transform, record.%x1, record.%y1);
  let (x2, y2) = transform-position(transform, record.%x2, record.%y2);
  outline-line-with-hexagon(medium, x1, y1, x2, y2,
			    thickness: pen-width(pen))
end method highlight-output-record;
||#

(defmethod highlight-output-record ((record <line-record>) (sheet <output-recording-mixin>) state)
  (declare (ignore state))
  (let ((medium (sheet-medium sheet))
        (pen (medium-state-pen (record-medium-state record)))
        (transform (sheet-device-transform record)))
    (multiple-value-bind (x1 y1)
        (transform-position transform (%x1 record) (%y1 record))
      (multiple-value-bind (x2 y2)
          (transform-position transform (%x2 record) (%y2 record))
        (outline-line-with-hexagon medium x1 y1 x2 y2
                                   :thickness (pen-width pen))))))


#||
define method outline-line-with-hexagon
    (drawable, x1, y1, x2, y2, #key thickness = 1)
  let distance = round/(thickness, 2) + 1;
  let (x1, y1, x2, y2, x3, y3, x4, y4, x5, y5, x6, y6)
    = case
        negative?(x2 - x1) == negative?(y2 - y1) =>
          values
            (x1 - distance, y1 - distance, x1 - distance, y1 + distance,
             x2 - distance, y2 + distance, x2 + distance, y2 + distance,
             x2 + distance, y2 - distance, x1 + distance, y1 - distance);
        otherwise =>
          when (y2 > y1)
            // Make line go down and to the right
            swap!(x2, x1);
            swap!(y2, y1)
          end;
          values
            (x1 - distance, y1 + distance, x1 - distance, y1 - distance,
             x2 - distance, y2 - distance, x2 + distance, y2 - distance,
             x2 + distance, y2 + distance, x1 + distance, y1 + distance)
      end;
  with-drawing-options (drawable, /* ---*** brush: $xor-brush, */ pen: $highlighting-pen)
    draw-line(drawable, x1, y1, x2, y2);
    draw-line(drawable, x2, y2, x3, y3);
    draw-line(drawable, x3, y3, x4, y4);
    draw-line(drawable, x4, y4, x5, y5);
    draw-line(drawable, x5, y5, x6, y6);
    draw-line(drawable, x6, y6, x1, y1)
  end
end method outline-line-with-hexagon;
||#

(defgeneric outline-line-with-hexagon (drawable x1 y1 x2 y2 &key thickness))

(defmethod outline-line-with-hexagon (drawable x1 y1 x2 y2 &key (thickness 1))
  (let ((distance (+ 1 (round thickness 2))))
    (multiple-value-bind (x1 y1 x2 y2 x3 y3 x4 y4 x5 y5 x6 y6)
        (cond ((eql (negative? (- x2 x1)) (negative? (- y2 y1)))
               (values (- x1 distance) (- y1 distance) (- x1 distance) (+ y1 distance)
                       (- x2 distance) (+ y2 distance) (+ x2 distance) (+ y2 distance)
                       (+ x2 distance) (- y2 distance) (+ x1 distance) (- y1 distance)))
              (t
               (when (> y2 y1)
                 ;; Make line go down and to the right
                 (rotatef x2 x1)
                 (rotatef y2 y1))
               (values (- x1 distance) (+ y1 distance) (- x1 distance) (- y1 distance)
                       (- x2 distance) (- y2 distance) (+ x2 distance) (- y2 distance)
                       (+ x2 distance) (+ y2 distance) (+ x1 distance) (+ y1 distance))))
      (with-drawing-options (drawable #||---*** brush: $xor-brush ||# :pen *highlighting-pen*)
        (draw-line drawable x1 y1 x2 y2)
        (draw-line drawable x2 y2 x3 y3)
        (draw-line drawable x3 y3 x4 y4)
        (draw-line drawable x4 y4 x5 y5)
        (draw-line drawable x5 y5 x6 y6)
        (draw-line drawable x6 y6 x1 y1)))))


#||
// Group all the bits of the arrow together
define method draw-arrow 
    (sheet :: <output-recording-mixin>, x1, y1, x2, y2,
     #key from-head?, to-head? = #t, head-length, head-width) => (record)
  ignore(from-head?, to-head?, head-length, head-width);  
  with-new-output-record (sheet)
    next-method()
  end
end method draw-arrow;
||#

;; Group all the bits of the arrow together
(defmethod draw-arrow ((sheet <output-recording-mixin>) x1 y1 x2 y2
                       &key from-head? (to-head? t) head-length head-width)
  (declare (ignore x1 y1 x2 y2 from-head? to-head?
		   head-length head-width))
  (with-new-output-record (sheet)
    (call-next-method)))



#||

/// 'draw-lines'

define sealed class <lines-record> (<graphics-record>)
  sealed slot %coord-seq, required-init-keyword: coord-seq:;
end class <lines-record>;

define sealed domain make (singleton(<lines-record>));
define sealed domain initialize (<lines-record>);
||#

(defclass <lines-record> (<graphics-record>)
  ((%coord-seq :initarg :coord-seq :initform (required-slot ":coord-seq" "<lines-record>") :accessor %coord-seq)))


#||
define method draw-lines
    (sheet :: <output-recording-mixin>, coord-seq :: <coordinate-sequence>) => (record)
  let record = #f;
  when (sheet-recording?(sheet))
    let medium = sheet-medium(sheet);
    let medium-state = sheet-medium-state(sheet);
    // Apply the medium transform to user-supplied coordinates
    let coord-seq
      = transform-coordinate-sequence
          (medium-transform(medium), coord-seq, copy?: #t);
    FOR (I FROM 0 BELOW SIZE(COORD-SEQ))	//---*** until 'coordinate-sequence-box' is fixed
      COORD-SEQ[I] := FLOOR(COORD-SEQ[I])
    END;
    // Compute the bounding box in the sheet's coordinate system
    let (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
      = begin
          let thickness = pen-width(medium-pen(medium));
          // The convention is that we stroke paths on the outside,
          // where "outside" is biased to the left and top
          coordinate-sequence-box(coord-seq, thickness: thickness)
        end;
    // Get the position of the parent record
    let (rx, ry) = point-position(sheet-output-record-position(sheet));
    // Now compute the region and transform for this record, making
    // them be 0-based to get the most efficient representation, with
    // the bounding box coordinates relative to the parent record
    let transform = make(<mutable-translation-transform>, tx: left - rx, ty: top - ry);
    let region    = make-bounding-box(0, 0, right - left, bottom - top);
    // Make all user-supplied coordinates relative to bounding box
    translate-coordinate-sequence!(-left, -top, coord-seq);
    record := make(<lines-record>,
		   coord-seq: coord-seq,
		   region: region, transform: transform,
		   medium-state: medium-state);
    add-output-record(sheet, record)
  end;
  when (sheet-drawing?(sheet))
    next-method()
  end;
  record
end method draw-lines;
||#

(defmethod draw-lines ((sheet <output-recording-mixin>) coord-seq)
  (check-type coord-seq <coordinate-sequence>)
  (let ((record nil))
    (when (sheet-recording? sheet)
      (let* ((medium (sheet-medium sheet))
	     (medium-state (sheet-medium-state sheet))
	     ;; Apply the medium transform to user-supplied coordinates
	     (coord-seq (transform-coordinate-sequence (medium-transform medium) coord-seq :copy? t)))
        (LOOP FOR I FROM 0 BELOW (LENGTH COORD-SEQ)   ;;---*** until 'coordinate-sequence-box' is fixed
	   DO (SETF (AREF COORD-SEQ I) (FLOOR (AREF COORD-SEQ I))))
        ;; Compute the bounding box in the sheet's coordinate system
        (multiple-value-bind (left top right bottom)
            (let ((thickness (pen-width (medium-pen medium))))
              ;; The convention is that we stroke paths on the outside,
              ;; where "outside" is biased to the left and top
              (coordinate-sequence-box coord-seq :thickness thickness))
          ;; Get the position of the parent record
          (multiple-value-bind (rx ry)
              (point-position (sheet-output-record-position sheet))
            ;; Now compute the region and transform for this record, making
            ;; them be 0-based to get the most efficient representation, with
            ;; the bounding box coordinates relative to the parent record
            (let ((transform (make-instance '<mutable-translation-transform> :tx (- left rx) :ty (- top ry)))
                  (region (make-bounding-box 0 0 (- right left) (- bottom top))))
              ;; Make all user-supplied coordinates relative to bounding box
              (translate-coordinate-sequence! (- left) (- top) coord-seq)
              (setf record (make-instance '<lines-record>
					  :coord-seq coord-seq
					  :region region :transform transform
					  :medium-state medium-state))
              (add-output-record sheet record))))))
    (when (sheet-drawing? sheet)
      (call-next-method))
    record))


#||
define method handle-repaint
    (record :: <lines-record>, medium :: <medium>, region :: <region>) => ()
  ignore(region);
  when (default-background(record))
    repaint-background(record, medium)
  end;
  with-record-medium-state (medium, record)
    draw-lines(medium, record.%coord-seq)
  end
end method handle-repaint;
||#

(defmethod handle-repaint ((record <lines-record>) (medium <medium>) (region <region>))
  (declare (ignore region))
  (when (default-background record)
    (repaint-background record medium))
  (with-record-medium-state (medium record)
    (draw-lines medium (%coord-seq record))))


#||
define method refined-position-test
    (record :: <lines-record>, x, y) => (true? :: <boolean>)
  block (return)
    let pen = medium-state-pen(record-medium-state(record));
    let thickness = pen-width(pen);
    local method position-in-ltrb? (x1, y1, x2, y2) => ()
	    when (ltrb-contains-position?
		    (min(x1, x2) - thickness, min(y1, y2) - thickness,
		     max(x1, x2), max(y1, y2), x, y)
		  & position-close-to-line?(x, y, x1, y1, x2, y2,
					    thickness: thickness))
	      return(#t)
	    end
	  end method;
    do-endpoint-coordinates(position-in-ltrb?, record.%coord-seq);
    #f
  end
end method refined-position-test;
||#

(defmethod refined-position-test ((record <lines-record>) x y)
  (let* ((pen (medium-state-pen (record-medium-state record)))
	 (thickness (pen-width pen)))
    (labels ((position-in-ltrb? (x1 y1 x2 y2)
               (when (and (ltrb-contains-position? (- (min x1 x2) thickness) (- (min y1 y2) thickness)
                                                   (max x1 x2) (max y1 y2) x y)
                          (position-close-to-line? x y x1 y1 x2 y2
                                                   :thickness thickness))
                 (return-from refined-position-test t))))
      (do-endpoint-coordinates #'position-in-ltrb? (%coord-seq record))))
  nil)


#||
define method highlight-output-record
    (record :: <lines-record>, sheet :: <output-recording-mixin>, state) => ()
  ignore(state);
  let medium = sheet-medium(sheet);
  let pen = medium-state-pen(record-medium-state(record));
  let thickness = pen-width(pen);
  let transform = sheet-device-transform(record);
  with-drawing-options (medium, /* ---*** brush: $xor-brush, */ pen: $highlighting-pen)
    local method highlight (x1, y1, x2, y2) => ()
	    transform-coordinates!(transform, x1, y1, x2, y2);
	    outline-line-with-hexagon(medium, x1, y1, x2, y2,
				      thickness: thickness)
	  end method;
    do-endpoint-coordinates(highlight, record.%coord-seq)
  end
end method highlight-output-record;
||#

(defmethod highlight-output-record ((record <lines-record>) (sheet <output-recording-mixin>) state)
  (declare (ignore state))
  (let* ((medium (sheet-medium sheet))
	 (pen (medium-state-pen (record-medium-state record)))
	 (thickness (pen-width pen))
	 (transform (sheet-device-transform record)))
    (with-drawing-options (medium #||---*** brush: $xor-brush ||# :pen *highlighting-pen*)
      (labels ((highlight (x1 y1 x2 y2)
                 (transform-coordinates! transform x1 y1 x2 y2)
                 (outline-line-with-hexagon medium x1 y1 x2 y2
                                            :thickness thickness)))
        (do-endpoint-coordinates #'highlight (%coord-seq record))))))



#||

/// 'draw-rectangle'

define sealed class <rectangle-record> (<graphics-record>)
  sealed slot %x1, required-init-keyword: x1:;
  sealed slot %y1, required-init-keyword: y1:;
  sealed slot %x2, required-init-keyword: x2:;
  sealed slot %y2, required-init-keyword: y2:;
  sealed slot %filled? = #t, init-keyword: filled?:;
end class <rectangle-record>;

define sealed domain make (singleton(<rectangle-record>));
define sealed domain initialize (<rectangle-record>);
||#

(defclass <rectangle-record> (<graphics-record>)
  ((%x1 :initarg :x1 :initform (required-slot ":x1" "<rectangle-record>") :accessor %x1)
   (%y1 :initarg :y1 :initform (required-slot ":y1" "<rectangle-record>") :accessor %y1)
   (%x2 :initarg :x2 :initform (required-slot ":x2" "<rectangle-record>") :accessor %x2)
   (%y2 :initarg :y2 :initform (required-slot ":y2" "<rectangle-record>") :accessor %y2)
   (%filled? :initarg :filled? :initform t :accessor %filled?)))


#||
// See 'draw-line' for commentary on what exactly is happening here
define method draw-rectangle
    (sheet :: <output-recording-mixin>, x1, y1, x2, y2,
     #key filled? = #t) => (record)
  let medium = sheet-medium(sheet);
  if (~rectilinear-transform?(medium-transform(medium)))
    with-stack-vector (coords = x1, y1, x2, y1, x2, y2, x1, y2)
      draw-polygon(sheet, coords, closed?: #t, filled?: filled?)
    end
  else
    let record = #f;
    when (sheet-recording?(sheet))
      let medium-state = sheet-medium-state(sheet);
      transform-coordinates!(medium-transform(medium), x1, y1, x2, y2);
      let (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
        = begin
            let thickness
              = if (filled?) 0 else pen-width(medium-pen(medium)) end;
            fix-box(min(x1, x2) - thickness, min(y1, y2) - thickness,
		    max(x1, x2) + thickness, max(y1, y2) + thickness)
          end;
      let (rx, ry) = point-position(sheet-output-record-position(sheet));
      let transform = make(<mutable-translation-transform>, tx: left - rx, ty: top - ry);
      let region    = make-bounding-box(0, 0, right - left, bottom - top);
      record
        := make(<rectangle-record>,
		x1: x1 - left, y1: y1 - top, x2: x2 - left, y2: y2 - top,
                filled?: filled?,
		region: region, transform: transform,
                medium-state: medium-state);
      add-output-record(sheet, record)
    end;
    when (sheet-drawing?(sheet))
      next-method()
    end;
    record
  end
end method draw-rectangle;
||#

(defmethod draw-rectangle ((sheet <output-recording-mixin>) x1 y1 x2 y2
                           &key (filled? t))
  (let ((medium (sheet-medium sheet)))
    (if (not (rectilinear-transform? (medium-transform medium)))
        (with-stack-vector (coords = x1 y1 x2 y1 x2 y2 x1 y2)
          (draw-polygon sheet coords :closed? t :filled? filled?))
        ;; else
        (let ((record nil))
          (when (sheet-recording? sheet)
            (let ((medium-state (sheet-medium-state sheet)))
              (transform-coordinates! (medium-transform medium) x1 y1 x2 y2)
              (multiple-value-bind (left top right bottom)
                  (let ((thickness (if filled? 0 (pen-width (medium-pen medium)))))
                    (fix-box (- (min x1 x2) thickness) (- (min y1 y2) thickness)
                             (+ (max x1 x2) thickness) (+ (max y1 y2) thickness)))
                (multiple-value-bind (rx ry)
                    (point-position (sheet-output-record-position sheet))
                  (let ((transform (make-instance '<mutable-translation-transform> :tx (- left rx) :ty (- top ry)))
                        (region (make-bounding-box 0 0 (- right left) (- bottom top))))
                    (setf record (make-instance '<rectangle-record>
						:x1 (- x1 left) :y1 (- y1 top) :x2 (- x2 left) :y2 (- y2 top)
						:filled? filled?
						:region region :transform transform
						:medium-state medium-state))
                    (add-output-record sheet record))))))
          (when (sheet-drawing? sheet)
            (call-next-method))
          record))))


#||
define method handle-repaint
    (record :: <rectangle-record>, medium :: <medium>, region :: <region>) => ()
  ignore(region);
  when (default-background(record))
    repaint-background(record, medium)
  end;
  with-record-medium-state (medium, record)
    draw-rectangle(medium,
		    record.%x1, record.%y1, record.%x2, record.%y2,
		    filled?: record.%filled?)
  end
end method handle-repaint;
||#

(defmethod handle-repaint ((record <rectangle-record>) (medium <medium>) (region <region>))
  (declare (ignore region))
  (when (default-background record)
    (repaint-background record medium))
  (with-record-medium-state (medium record)
    (draw-rectangle medium
		    (%x1 record) (%y1 record) (%x2 record) (%y2 record)
                    :filled? (%filled? record))))


#||
define method refined-position-test
    (record :: <rectangle-record>, x, y) => (true? :: <boolean>)
  record.%filled?
  | begin
      let thickness = pen-width(medium-state-pen(record-medium-state(record)));
      ~(record.%x1 + thickness <= x
        & record.%y1 + thickness <= y
        & record.%x2 - thickness >= x
        & record.%y2 - thickness >= y)
    end
end method refined-position-test;
||#

(defmethod refined-position-test ((record <rectangle-record>) x y)
  (or (%filled? record)
      (let ((thickness (pen-width (medium-state-pen (record-medium-state record)))))
        (not (and (<= (+ (%x1 record) thickness) x)
                  (<= (+ (%y1 record) thickness) y)
                  (>= (- (%x2 record) thickness) x)
                  (>= (- (%y2 record) thickness) y))))))


#||
define method highlight-output-record
    (record :: <rectangle-record>, sheet :: <output-recording-mixin>, state) => ()
  ignore(state);
  let medium = sheet-medium(sheet);
  let thickness
    = if (record.%filled?) 0 else pen-width(medium-state-pen(record-medium-state(record))) end;
  let transform = sheet-device-transform(record);
  let (x1, y1) = transform-position(transform, record.%x1, record.%y1);
  let (x2, y2) = transform-position(transform, record.%x2, record.%y2);
  with-drawing-options (medium, /* ---*** brush: $xor-brush, */ pen: $highlighting-pen)
    draw-rectangle(medium, 
		   x1 - thickness - 1, y1 - thickness - 1,
		   x2 + thickness + 1, y2 + thickness + 1,
		   filled?: #f)
  end
end method highlight-output-record;
||#

(defmethod highlight-output-record ((record <rectangle-record>) (sheet <output-recording-mixin>) state)
  (declare (ignore state))
  (let ((medium (sheet-medium sheet))
        (thickness (if (%filled? record) 0 (pen-width (medium-state-pen (record-medium-state record)))))
        (transform (sheet-device-transform record)))
    (multiple-value-bind (x1 y1)
        (transform-position transform (%x1 record) (%y1 record))
      (multiple-value-bind (x2 y2)
          (transform-position transform (%x2 record) (%y2 record))
        (with-drawing-options (medium #||---*** brush: $xor-brush ||# :pen *highlighting-pen*)
          (draw-rectangle medium
                          (- x1 thickness 1) (- y1 thickness 1)
                          (+ x2 thickness 1) (+ y2 thickness 1)
                          :filled? nil))))))



#||

/// 'draw-rectangles'

define sealed class <rectangles-record> (<graphics-record>)
  sealed slot %coord-seq, required-init-keyword: coord-seq:;
  sealed slot %filled? = #t, init-keyword: filled?:;
end class <rectangles-record>;

define sealed domain make (singleton(<rectangles-record>));
define sealed domain initialize (<rectangles-record>);
||#

(defclass <rectangles-record> (<graphics-record>)
  ((%coord-seq :initarg :coord-seq :initform (required-slot ":coord-seq" "<rectangles-record>") :accessor %coord-seq)
   (%filled? :initarg :filled? :initform t :accessor %filled?)))


#||
// See 'draw-lines' for commentary on what exactly is happening here
define method draw-rectangles
    (sheet :: <output-recording-mixin>, coord-seq :: <coordinate-sequence>,
     #key filled? = #t) => (record)
  let medium = sheet-medium(sheet);
  if (~rectilinear-transform?(medium-transform(medium)))
    do-endpoint-coordinates
      (method (x1, y1, x2, y2)
	 with-stack-vector (coords = x1, y1, x2, y1, x2, y2, x1, y2)
	   draw-polygon(sheet, coords, closed?: #t, filled?: filled?)
         end
       end,
      coord-seq)
  else
    let record = #f;
    when (sheet-recording?(sheet))
      let medium-state = sheet-medium-state(sheet);
      let coord-seq
        = transform-coordinate-sequence
            (medium-transform(medium), coord-seq, copy?: #t);
      FOR (I FROM 0 BELOW SIZE(COORD-SEQ))	//---*** until 'coordinate-sequence-box' is fixed
        COORD-SEQ[I] := FLOOR(COORD-SEQ[I])
      END;
      let (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
        = begin
            let thickness
	      = if (filled?) 0 else pen-width(medium-pen(medium)) end;
            coordinate-sequence-box(coord-seq, thickness: thickness)
          end;
      let (rx, ry) = point-position(sheet-output-record-position(sheet));
      let transform = make(<mutable-translation-transform>, tx: left - rx, ty: top - ry);
      let region    = make-bounding-box(0, 0, right - left, bottom - top);
      translate-coordinate-sequence!(-left, -top, coord-seq);
      record := make(<rectangles-record>,
		     coord-seq: coord-seq,
		     filled?: filled?,
		     region: region, transform: transform,
		     medium-state: medium-state);
      add-output-record(sheet, record)
    end;
    when (sheet-drawing?(sheet))
      next-method()
    end;
    record
  end
end method draw-rectangles;
||#

(defmethod draw-rectangles ((sheet <output-recording-mixin>) coord-seq
                            &key (filled? t))
  (check-type coord-seq <coordinate-sequence>)
  (let ((medium (sheet-medium sheet)))
    (if (not (rectilinear-transform? (medium-transform medium)))
        (do-endpoint-coordinates
            #'(lambda (x1 y1 x2 y2)
                (with-stack-vector (coords = x1 y1 x2 y1 x2 y2 x1 y2)
                  (draw-polygon sheet coords :closed? t :filled? filled?)))
          coord-seq)
        ;; else
        (let ((record nil))
          (when (sheet-recording? sheet)
            (let ((medium-state (sheet-medium-state sheet))
                  (coord-seq (transform-coordinate-sequence (medium-transform medium) coord-seq :copy? t)))
              (LOOP FOR I FROM 0 BELOW (LENGTH COORD-SEQ)	;;---*** until 'coordinate-sequence-box' is fixed
		 DO (SETF (AREF COORD-SEQ I) (FLOOR (AREF COORD-SEQ I))))
              (multiple-value-bind (left top right bottom)
                  (let ((thickness (if filled? 0 (pen-width (medium-pen medium)))))
                    (coordinate-sequence-box coord-seq :thickness thickness))
                (multiple-value-bind (rx ry)
                    (point-position (sheet-output-record-position sheet))
                  (let ((transform (make-instance '<mutable-translation-transform> :tx (- left rx) :ty (- top ry)))
                        (region (make-bounding-box 0 0 (- right left) (- bottom top))))
                    (translate-coordinate-sequence! (- left) (- top) coord-seq)
                    (setf record (make-instance '<rectangles-record>
						:coord-seq coord-seq
						:filled? filled?
						:region region :transform transform
						:medium-state medium-state))
                    (add-output-record sheet record))))))
          (when (sheet-drawing? sheet)
            (call-next-method))
          record))))


#||
define method handle-repaint
    (record :: <rectangles-record>, medium :: <medium>, region :: <region>) => ()
  ignore(region);
  when (default-background(record))
    repaint-background(record, medium)
  end;
  with-record-medium-state (medium, record)
    draw-rectangles(medium, record.%coord-seq, filled?: record.%filled?)
  end
end method handle-repaint;
||#

(defmethod handle-repaint ((record <rectangles-record>) (medium <medium>) (region <region>))
  (declare (ignore region))
  (when (default-background record)
    (repaint-background record medium))
  (with-record-medium-state (medium record)
    (draw-rectangles medium (%coord-seq record) :filled? (%filled? record))))


#||
define method refined-position-test
    (record :: <rectangles-record>, x, y) => (true? :: <boolean>)
  block (return)
    let thickness = pen-width(medium-state-pen(record-medium-state(record)));
    local method position-in-ltrb? (x1, y1, x2, y2) => ()
	    when (if (record.%filled?)
		    ltrb-contains-position?(x1, y1, x2, y2, x, y)
		  else
		    ~(x1 + thickness <= x
		      & y1 + thickness <= y
		      & x2 - thickness >= x
		      & y2 - thickness >= y)
		  end)
	      return(#t)
	    end
	  end method;
    do-endpoint-coordinates(position-in-ltrb?, record.%coord-seq);
    #f
  end
end method refined-position-test;
||#

(defmethod refined-position-test ((record <rectangles-record>) x y)
  (let ((thickness (pen-width (medium-state-pen (record-medium-state record)))))
    (labels ((position-in-ltrb? (x1 y1 x2 y2)
               (when (if (%filled? record)
                         (ltrb-contains-position? x1 y1 x2 y2 x y)
                         ;; else
                         (not (and (<= (+ x1 thickness) x)
                                   (<= (+ y1 thickness) y)
                                   (>= (- x2 thickness) x)
                                   (>= (- y2 thickness) y))))
                 (return-from refined-position-test t))))
      (do-endpoint-coordinates #'position-in-ltrb? (%coord-seq record))))
  nil)


#||
define method highlight-output-record
    (record :: <rectangles-record>, sheet :: <output-recording-mixin>, state) => ()
  ignore(state);
  let medium = sheet-medium(sheet);
  let thickness
    = if (record.%filled?) 0 else pen-width(medium-state-pen(record-medium-state(record))) end;
  let transform = sheet-device-transform(record);
  with-drawing-options (medium, /* ---*** brush: $xor-brush, */ pen: $highlighting-pen)
    local method highlight (x1, y1, x2, y2) => ()
	    transform-coordinates!(transform, x1, y1, x2, y2);
	    draw-rectangle(medium,
			   x1 - thickness - 1, y1 - thickness - 1,
			   x2 + thickness + 1, y2 + thickness + 1,
			   filled?: #f)
	  end method;
    do-endpoint-coordinates(highlight, record.%coord-seq)
  end
end method highlight-output-record;
||#

(defmethod highlight-output-record ((record <rectangles-record>) (sheet <output-recording-mixin>) state)
  (declare (ignore state))
  (let ((medium (sheet-medium sheet))
        (thickness (if (%filled? record) 0 (pen-width (medium-state-pen (record-medium-state record)))))
        (transform (sheet-device-transform record)))
    (with-drawing-options (medium #|| ---*** brush: $xor-brush, ||# :pen *highlighting-pen*)
      (labels ((highlight (x1 y1 x2 y2)
                 (transform-coordinates! transform x1 y1 x2 y2)
                 (draw-rectangle medium
                                 (- x1 thickness 1) (- y1 thickness 1)
                                 (+ x2 thickness 1) (+ y2 thickness 1)
                                 :filled? nil)))
        (do-endpoint-coordinates #'highlight (%coord-seq record))))))



#||

/// 'draw-polygon'

define sealed class <polygon-record> (<graphics-record>)
  sealed slot %coord-seq, required-init-keyword: coord-seq:;
  sealed slot %filled? = #t, init-keyword: filled?:;
  sealed slot %closed? = #t, init-keyword: closed?:;
end class <polygon-record>;

define sealed domain make (singleton(<polygon-record>));
define sealed domain initialize (<polygon-record>);
||#

(defclass <polygon-record> (<graphics-record>)
  ((%coord-seq :initarg :coord-seq :initform (required-slot ":coord-seq" "<polygon-record>") :accessor %coord-seq)
   (%filled? :initarg :filled? :initform t :accessor %filled?)
   (%closed? :initarg :closed? :initform t :accessor %closed?)))


;;; TODO: Why is all this commented-out?

;; #||
;; // See 'draw-lines' for commentary on what exactly is happening here
;; define method draw-polygon
;;     (sheet :: <output-recording-mixin>, coord-seq :: <coordinate-sequence>,
;;      #key filled? = #t, closed? = #t) => (record)
;;   let record = #f;
;;   when (sheet-recording?(sheet))
;;     let medium = sheet-medium(sheet);
;;     let medium-state = sheet-medium-state(sheet);
;;     let coord-seq
;;       = transform-coordinate-sequence
;;           (medium-transform(medium), coord-seq, copy?: #t);
;;     FOR (I FROM 0 BELOW SIZE(COORD-SEQ))	//---*** until 'coordinate-sequence-box' is fixed
;;       COORD-SEQ[I] := FLOOR(COORD-SEQ[I])
;;     END;
;;     let (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
;;       = begin
;;           let thickness
;; 	    = if (filled?) 0 else pen-width(medium-pen(medium)) end;
;;           coordinate-sequence-box(coord-seq, thickness: thickness)
;;         end;
;;     let (rx, ry) = point-position(sheet-output-record-position(sheet));
;;     let transform = make(<mutable-translation-transform>, tx: left - rx, ty: top - ry);
;;     let region    = make-bounding-box(0, 0, right - left, bottom - top);
;;     translate-coordinate-sequence!(-left, -top, coord-seq);
;;     record := make(<polygon-record>,
;; 		   coord-seq: coord-seq,
;; 		   filled?: filled?, closed?: closed?,
;; 		   region: region, transform: transform,
;; 		   medium-state: medium-state);
;;     add-output-record(sheet, record)
;;   end;
;;   when (sheet-drawing?(sheet))
;;     next-method()
;;   end;
;;   record
;; end method draw-polygon;
;; ||#

;; (defmethod draw-polygon ((sheet <output-recording-mixin>)
;;                          coord-seq
;;                          &key (filled? t) (closed? t))
;;   #-(or :sbcl) (check-type coord-seq <coordinate-sequence>)
;;   (let ((record nil))
;;     (when (sheet-recording? sheet)
;;       (let* ((medium (sheet-medium sheet))
;; 	     (medium-state (sheet-medium-state sheet))
;; 	     (coord-seq (transform-coordinate-sequence (medium-transform medium) coord-seq :copy? t)))
;;         (LOOP FOR I FROM 0 BELOW (LENGTH COORD-SEQ)	;;---*** until 'coordinate-sequence-box' is fixed
;;               DO (SETF (AREF COORD-SEQ I) (FLOOR (AREF COORD-SEQ I))))
;;         (multiple-value-bind (left top right bottom)
;;             (let ((thickness (if filled? 0 (pen-width (medium-pen medium)))))
;;               (coordinate-sequence-box coord-seq :thickness thickness))
;;           (multiple-value-bind (rx ry)
;;               (point-position (sheet-output-record-position sheet))
;;             (let ((transform (make-instance '<mutable-translation-transform>
;; 					    :tx (- left rx)
;; 					    :ty (- top ry)))
;;                   (region    (make-bounding-box 0 0 (- right left) (- bottom top))))
;;               (translate-coordinate-sequence! (- left) (- top) coord-seq)
;;               (setf record (make-instance '<polygon-record>
;; 					  :coord-seq coord-seq
;; 					  :filled? filled?
;; 					  :closed? closed?
;; 					  :region region
;; 					  :transform transform
;; 					  :medium-state medium-state))
;;               (add-output-record sheet record))))))
;;     (when (sheet-drawing? sheet)
;;       (call-next-method))
;;     record))

;; #||
;; define method handle-repaint
;;     (record :: <polygon-record>, medium :: <medium>, region :: <region>) => ()
;;   ignore(region);
;;   when (default-background(record))
;;     repaint-background(record, medium)
;;   end;
;;   with-record-medium-state (medium, record)
;;     draw-polygon(medium,
;; 		 record.%coord-seq,
;; 		 filled?: record.%filled?, closed?: record.%closed?)
;;   end
;; end method handle-repaint;
;; ||#

;; (defmethod handle-repaint ((record <polygon-record>)
;;                            (medium <medium>)
;;                            (region <region>))
;;   (declare (ignore region))
;;   (when (default-background record)
;;     (repaint-background record medium))
;;   (with-record-medium-state (medium record)
;;     (draw-polygon medium
;;                   (%coord-seq record)
;;                   :filled? (%filled? record)
;;                   :closed? (%closed? record))))

;; #||
;; define method refined-position-test
;;     (record :: <polygon-record>, x, y) => (true? :: <boolean>)
;;   block (return)
;;     if (record.%filled?)
;;       position-inside-polygon?(x, y, record.%coord-seq,
;; 			       closed?: record.%closed?)
;;     else
;;       let thickness = pen-width(medium-state-pen(record-medium-state(record)));
;;       local method position-on-line? (x1, y1, x2, y2) => ()
;; 	      when (ltrb-contains-position?
;; 		      (min(x1, x2) - thickness, min(y1, y2) - thickness,
;; 		       max(x1, x2) + thickness, max(y1, y2) + thickness, x, y)
;; 		    & position-close-to-line?(x, y, x1, y1, x2, y2,
;; 					      thickness: thickness))
;; 		return(#t)
;; 	      end
;; 	    end method;
;;       let ncoords = size(record.%coord-seq) - 1;
;;       let x1 = record.%coord-seq[0];
;;       let y1 = record.%coord-seq[1];
;;       let x = x1;
;;       let y = y1;
;;       let i = 1;
;;       while (#t)
;;         position-on-line?(x, y,
;; 			  x := record.%coord-seq[inc!(i)], y := record.%coord-seq[inc!(i)]);
;;         when (i = ncoords)
;;           return(when (record.%closed?)
;; 		   position-on-line?(x, y, x1, y1)
;; 		 end)
;;         end
;;       end
;;     end;
;;     #f
;;   end
;; end method refined-position-test;
;; ||#

;; (defmethod refined-position-test ((record <polygon-record>)
;;                                   x y)
;;   (if (%filled? record)
;;       (position-inside-polygon? x y (%coord-seq record)
;;                                 :closed? (%closed? record))
;;       ;; (else)
;;       (let ((thickness (pen-width (medium-state-pen (record-medium-state record)))))
;;         (labels ((position-on-line? (x1 y1 x2 y2)
;;                    (when (and (ltrb-contains-position? (- (min x1 x2)
;; 							  thickness)
;; 						       (- (min y1 y2)
;; 							  thickness)
;;                                                        (+ (max x1 x2)
;; 							  thickness)
;; 						       (+ (max y1 y2)
;; 							  thickness)
;;                                                        x y)
;;                               (position-close-to-line? x y x1 y1 x2 y2
;;                                                        :thickness thickness))
;;                      (return-from refined-position-test t))))
;;           (let* ((ncoords (1- (length (%coord-seq record))))
;; 		 (x1 (aref (%coord-seq record) 0))
;; 		 (y1 (aref (%coord-seq record) 1))
;; 		 (x x1)
;; 		 (y y1)
;; 		 (i 1))
;;             (loop while t
;;                   do (position-on-line? x y (setf x (aref (%coord-seq record) (incf i)))
;;                                         (setf y (aref (%coord-seq record) (incf i))))
;;                   when (= i ncoords)
;;                   do (return-from refined-position-test (when (%closed? record)
;;                                                           (position-on-line? x y x1 y1))))))
;;         nil)))

;; #||
;; //--- This needs a proper highlighting function
;; define method highlight-output-record
;;     (record :: <polygon-record>, sheet :: <output-recording-mixin>, state) => ()
;;   ignore(state);
;;   next-method()
;; end method highlight-output-record;
;; ||#

;; (defmethod highlight-output-record ((record <polygon-record>)
;;                                     (sheet <output-recording-mixin>)
;;                                     state)
;;   (declare (ignore state))
;;   (call-next-method))


;; 
;; #||
;; 
;; /// 'draw-ellipse'

;; define sealed class <ellipse-record> (<graphics-record>)
;;   sealed slot %center-x, required-init-keyword: center-x:;
;;   sealed slot %center-y, required-init-keyword: center-y:;
;;   sealed slot %radius-1-dx, required-init-keyword: radius-1-dx:;
;;   sealed slot %radius-1-dy, required-init-keyword: radius-1-dy:;
;;   sealed slot %radius-2-dx, required-init-keyword: radius-2-dx:;
;;   sealed slot %radius-2-dy, required-init-keyword: radius-2-dy:;
;;   sealed slot %start-angle = #f, init-keyword: start-angle:;
;;   sealed slot %end-angle = #f, init-keyword: end-angle:;
;;   sealed slot %filled? = #t, init-keyword: filled?:;
;; end class <ellipse-record>;

;; define sealed domain make (singleton(<ellipse-record>));
;; define sealed domain initialize (<ellipse-record>);
;; ||#

(defclass <ellipse-record>
    (<graphics-record>)
  ((%center-x :initarg :center-x :initform (required-slot ":center-x" "<ellipse-record>") :accessor %center-x)
   (%center-y :initarg :center-y :initform (required-slot ":center-y" "<ellipse-record>") :accessor %center-y)
   (%radius-1-dx :initarg :radius-1-dx :initform (required-slot ":radius-1-dx" "<ellipse-record>") :accessor %radius-1-dx)
   (%radius-1-dy :initarg :radius-1-dy :initform (required-slot ":radius-1-dy" "<ellipse-record>") :accessor %radius-1-dy)
   (%radius-2-dx :initarg :radius-2-dx :initform (required-slot ":radius-2-dx" "<ellipse-record>") :accessor %radius-2-dx)
   (%radius-2-dy :initarg :radius-2-dy :initform (required-slot ":radius-2-dy" "<ellipse-record>") :accessor %radius-2-dy)
   (%start-angle :initarg :start-angle :initform nil :accessor %start-angle)
   (%end-angle :initarg :end-angle :initform nil :accessor %end-angle)
   (%filled? :initarg :filled? :initform t :accessor %filled?)))


#||
// See 'draw-line' for commentary on what exactly is happening here
define method draw-ellipse
    (sheet :: <output-recording-mixin>, center-x, center-y,
     radius-1-dx, radius-1-dy, radius-2-dx, radius-2-dy,
     #key start-angle, end-angle, filled? = #t) => (record)
  let record = #f;
  when (sheet-recording?(sheet))
    let medium = sheet-medium(sheet);
    let medium-state = sheet-medium-state(sheet);
    let transform = medium-transform(medium);
    transform-coordinates!(transform, center-x, center-y);
    transform-distances!(transform,
			 radius-1-dx, radius-1-dy, radius-2-dx, radius-2-dy);
    when (start-angle | end-angle)
      let (_start-angle, _end-angle)
	= transform-angles(transform, start-angle | 0.0, end-angle | $2pi);
      start-angle := _start-angle;
      end-angle := _end-angle
    end;
    let thickness
      = if (filled?) 0 else pen-width(medium-pen(medium)) end;
    let (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
      = elliptical-arc-box(center-x, center-y,
			   radius-1-dx, radius-1-dy, radius-2-dx, radius-2-dy,
			   start-angle: start-angle, end-angle: end-angle,
			   thickness: thickness);
    let (rx, ry) = point-position(sheet-output-record-position(sheet));
    let transform = make(<mutable-translation-transform>, tx: left - rx, ty: top - ry);
    let region    = make-bounding-box(0, 0, right - left, bottom - top);
    record := make(<ellipse-record>,
		   center-x: center-x - left, center-y: center-y - top,
		   radius-1-dx: radius-1-dx, radius-1-dy: radius-1-dy,
		   radius-2-dx: radius-2-dx, radius-2-dy: radius-2-dy,
		   start-angle: start-angle, end-angle: end-angle,
		   filled?: filled?,
		   region: region, transform: transform,
		   medium-state: medium-state);
    add-output-record(sheet, record)
  end;
  when (sheet-drawing?(sheet))
    next-method()
  end;
  record
end method draw-ellipse;
||#

(defmethod draw-ellipse ((sheet <output-recording-mixin>) center-x center-y
                         radius-1-dx radius-1-dy radius-2-dx radius-2-dy
                         &key start-angle end-angle (filled? t))
  (let ((record nil))
    (when (sheet-recording? sheet)
      (let* ((medium (sheet-medium sheet))
	     (medium-state (sheet-medium-state sheet))
	     (transform (medium-transform medium)))
        (transform-coordinates! transform center-x center-y)
        (transform-distances! transform
                              radius-1-dx radius-1-dy radius-2-dx radius-2-dy)
        (when (or start-angle end-angle)
          (multiple-value-bind (_start-angle _end-angle)
              (transform-angles transform
				(or start-angle 0.0)
				(or end-angle +2pi+))
            (setf start-angle _start-angle)
	    (setf end-angle _end-angle)))
        (let ((thickness (if filled? 0 (pen-width (medium-pen medium)))))
          (multiple-value-bind (left top right bottom)
              (elliptical-arc-box center-x center-y
                                  radius-1-dx radius-1-dy radius-2-dx radius-2-dy
                                  :start-angle start-angle :end-angle end-angle
                                  :thickness thickness)
            (multiple-value-bind (rx ry)
                (point-position (sheet-output-record-position sheet))
              (let ((transform (make-instance '<mutable-translation-transform> :tx (- left rx) :ty (- top ry)))
                    (region    (make-bounding-box 0 0 (- right left) (- bottom top))))
                (setf record (make-instance '<ellipse-record>
					    :center-x (- center-x left) :center-y (- center-y top)
					    :radius-1-dx radius-1-dx :radius-1-dy radius-1-dy
					    :radius-2-dx radius-2-dx :radius-2-dy radius-2-dy
					    :start-angle start-angle :end-angle end-angle
					    :filled? filled?
					    :region region :transform transform
					    :medium-state medium-state))
                (add-output-record sheet record)))))))
    (when (sheet-drawing? sheet)
      (call-next-method))
    record))


#||
define method handle-repaint
    (record :: <ellipse-record>, medium :: <medium>, region :: <region>) => ()
  ignore(region);
  when (default-background(record))
    repaint-background(record, medium)
  end;
  with-record-medium-state (medium, record)
    draw-ellipse(medium,
		 record.%center-x, record.%center-y,
		 record.%radius-1-dx, record.%radius-1-dy,
		 record.%radius-2-dx, record.%radius-2-dy,
		 start-angle: record.%start-angle, end-angle: record.%end-angle,
		 filled?: record.%filled?)
  end
end method handle-repaint;
||#

(defmethod handle-repaint ((record <ellipse-record>) (medium <medium>) (region <region>))
  (declare (ignore region))
  (when (default-background record)
    (repaint-background record medium))
  (with-record-medium-state (medium record)
    (draw-ellipse medium
                  (%center-x record) (%center-y record)
                  (%radius-1-dx record) (%radius-1-dy record)
                  (%radius-2-dx record) (%radius-2-dy record)
                  :start-angle (%start-angle record) :end-angle (%end-angle record)
                  :filled? (%filled? record))))


#||
define method refined-position-test
    (record :: <ellipse-record>, x, y) => (true? :: <boolean>)
  if (record.%filled?)
    position-inside-ellipse?(x - record.%center-x, y - record.%center-y,
			     record.%radius-1-dx, record.%radius-1-dy,
			     record.%radius-2-dx, record.%radius-2-dy)
  else
    let thickness = pen-width(medium-state-pen(record-medium-state(record)));
    position-on-thick-ellipse?(x - record.%center-x, y - record.%center-y,
			       record.%radius-1-dx, record.%radius-1-dy,
                               record.%radius-2-dx, record.%radius-2-dy,
                               thickness: thickness)
  end
end method refined-position-test;
||#

(defmethod refined-position-test ((record <ellipse-record>) x y)
  (if (%filled? record)
      (position-inside-ellipse? (- x (%center-x record)) (- y (%center-y record))
                                (%radius-1-dx record) (%radius-1-dy record)
                                (%radius-2-dx record) (%radius-2-dy record))
      ;; else
      (let ((thickness (pen-width (medium-state-pen (record-medium-state record)))))
        (position-on-thick-ellipse? (- x (%center-x record))
				    (- y (%center-y record))
                                    (%radius-1-dx record) (%radius-1-dy record)
                                    (%radius-2-dx record) (%radius-2-dy record)
                                    :thickness thickness))))


#||
define method highlight-output-record
    (record :: <ellipse-record>, sheet :: <output-recording-mixin>, state) => ()
  ignore(state);
  let medium = sheet-medium(sheet);
  let delta = 2;
  let radius-1
    = sqrt(record.%radius-1-dx * record.%radius-1-dx
           + record.%radius-1-dy * record.%radius-1-dy);
  let radius-2
    = sqrt(record.%radius-2-dx * record.%radius-2-dx
           + record.%radius-2-dy * record.%radius-2-dy);
  unless (record.%filled?)
    inc!(delta, pen-width(medium-state-pen(record-medium-state(record))))
  end;
  let transform = sheet-device-transform(record);
  with-drawing-options (medium, /* ---*** brush: $xor-brush, */ pen: $highlighting-pen)
    let r1dx = record.%radius-1-dx + round/(delta * record.%radius-1-dx, radius-1);
    let r1dy = record.%radius-1-dy + round/(delta * record.%radius-1-dy, radius-1);
    let r2dx = record.%radius-2-dx + round/(delta * record.%radius-2-dx, radius-2);
    let r2dy = record.%radius-2-dy + round/(delta * record.%radius-2-dy, radius-2);
    let (cx, cy) = transform-position(transform, record.%center-x, record.%center-y);
    draw-ellipse(medium, cx, cy, r1dx, r1dy, r2dx, r2dy,
		 start-angle: record.%start-angle, end-angle: record.%end-angle,
		 filled?: #f)
  end
end method highlight-output-record;
||#

(defmethod highlight-output-record ((record <ellipse-record>) (sheet <output-recording-mixin>) state)
  (declare (ignore state))
  (let ((medium (sheet-medium sheet))
        (delta 2)
        (radius-1 (sqrt (+ (* (%radius-1-dx record) (%radius-1-dx record))
                           (* (%radius-1-dy record) (%radius-1-dy record)))))
        (radius-2 (sqrt (+ (* (%radius-2-dx record) (%radius-2-dx record))
                           (* (%radius-2-dy record) (%radius-2-dy record))))))
    (unless (%filled? record)
      (incf delta (pen-width (medium-state-pen (record-medium-state record)))))
    (let ((transform (sheet-device-transform record)))
      (with-drawing-options (medium #|| ---*** brush: $xor-brush, ||# :pen *highlighting-pen*)
        (let ((r1dx (+ (%radius-1-dx record) (round (* delta (%radius-1-dx record)) radius-1)))
              (r1dy (+ (%radius-1-dy record) (round (* delta (%radius-1-dy record)) radius-1)))
              (r2dx (+ (%radius-2-dx record) (round (* delta (%radius-2-dx record)) radius-2)))
              (r2dy (+ (%radius-2-dy record) (round (* delta (%radius-2-dy record)) radius-2))))
          (multiple-value-bind (cx cy)
              (transform-position transform (%center-x record) (%center-y record))
            (draw-ellipse medium cx cy r1dx r1dy r2dx r2dy
                          :start-angle (%start-angle record) :end-angle (%end-angle record)
                          :filled? nil)))))))


#||
// Group all the bits of the oval together
define method draw-oval 
    (sheet :: <output-recording-mixin>, center-x, center-y, x-radius, y-radius,
     #rest keys, #key filled? = #t, #all-keys) => (record)
  dynamic-extent(keys);
  ignore(filled?);
  with-new-output-record (sheet)
    next-method()
  end
end method draw-oval;
||#

(defmethod draw-oval ((sheet <output-recording-mixin>) center-x center-y x-radius y-radius
                      &rest keys &key (filled? t) &allow-other-keys)
  (declare (dynamic-extent keys)
           (ignore center-x center-y x-radius y-radius keys filled?))
  (with-new-output-record (sheet)
    (call-next-method)))



#||

/// 'draw-text'

define sealed class <text-record> (<graphics-record>)
  sealed slot %text, required-init-keyword: text:;
  sealed slot %x, required-init-keyword: x:;
  sealed slot %y, required-init-keyword: y:;
end class <text-record>;

define sealed domain make (singleton(<text-record>));
define sealed domain initialize (<text-record>);
||#

(defclass <text-record> (<graphics-record>)
  ((%text :initarg :text :initform (required-slot ":text" "<text-record>") :accessor %text)
   (%x :initarg :x :initform (required-slot ":x" "<text-record>") :accessor %x)
   (%y :initarg :y :initform (required-slot ":y" "<text-record>") :accessor %y)))


#||
define sealed class <aligned-text-record> (<text-record>)
  sealed slot %align-x = #"left", init-keyword: align-x:;
  sealed slot %align-y = #"baseline", init-keyword: align-y:;
end class <aligned-text-record>;

define sealed domain make (singleton(<aligned-text-record>));
define sealed domain initialize (<aligned-text-record>);
||#

(defclass <aligned-text-record> (<text-record>)
  ((%align-x :initarg :align-x :initform :left :accessor %align-x)
   (%align-y :initarg :align-y :initform :baseline :accessor %align-y)))


#||
define sealed class <oriented-text-record> (<aligned-text-record>)
  // Baseline runs from (X,Y) to (TOWARDS-X, TOWARDS-Y)
  sealed slot %towards-x, required-init-keyword: towards-x:;
  sealed slot %towards-y, required-init-keyword: towards-y:;
  sealed slot %transform-glyphs?, required-init-keyword: transform-glyphs?:;
end class <oriented-text-record>;

define sealed domain make (singleton(<oriented-text-record>));
define sealed domain initialize (<oriented-text-record>);
||#

(defclass <oriented-text-record> (<aligned-text-record>)
  ((%towards-x :initarg :towards-x :initform (required-slot ":towards-x" "<oriented-text-record>") :accessor %towards-x)
   (%towards-y :initarg :towards-y :initform (required-slot ":towards-y" "<oriented-text-record>") :accessor %towards-y)
   (%transform-glyphs? :initarg :transform-glyphs? :initform (required-slot ":transform-glyphs?" "<oriented-text-record>")
		       :accessor %transform-glyphs?))
  (:documentation
"
Baseline runs from (X,Y) to (TOWARDS-X, TOWARDS-Y)
"))


#||
// See 'draw-line' for commentary on what exactly is happening here
define method draw-text
    (sheet :: <output-recording-mixin>, text, x, y,
     #key start: _start, end: _end, align-x, align-y, do-tabs? = #f,
          towards-x, towards-y, transform-glyphs?) => (record)
  let record = #f;
  when (sheet-recording?(sheet))
    let medium = sheet-medium(sheet);
    let medium-state = sheet-medium-state(sheet);
    // Copy iff it's a substring, otherwise ensure it's a string
    let text
      = if (_start | _end)
          copy-sequence(text, start: _start | 0, end: _end)
        else
          as(<string>, text)
        end;
    transform-coordinates!(medium-transform(medium), x, y);
    when (towards-x | towards-y)
      transform-coordinates!(medium-transform(medium), towards-x, towards-y)
    end;
    let (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
      = text-bounding-box(medium, text, x, y,
			  align-x: align-x | #"left", align-y: align-y | #"baseline",
			  do-tabs?: do-tabs?,
			  towards-x: towards-x, towards-y: towards-y,
			  transform-glyphs?: transform-glyphs?);
    when (towards-x | towards-y)
      translate-coordinates!(-left, -top, towards-x, towards-y)
    end;
    let (rx, ry) = point-position(sheet-output-record-position(sheet));
    let transform = make(<mutable-translation-transform>, tx: left - rx, ty: top - ry);
    let region    = make-bounding-box(0, 0, right - left, bottom - top);
    record := case
		towards-x | towards-y | transform-glyphs? =>
		  make(<oriented-text-record>,
		       text: text, x: x - left, y: y - top,
		       towards-x: towards-x, towards-y: towards-y,
		       transform-glyphs?: transform-glyphs?,
		       align-x: align-x | #"left", align-y: align-y | #"baseline",
		       region: region, transform: transform,
		       medium-state: medium-state);
		align-x | align-y =>
		  make(<aligned-text-record>,
		       text: text, x: x - left, y: y - top,
		       align-x: align-x | #"left", align-y: align-y | #"baseline",
		       region: region, transform: transform,
		       medium-state: medium-state);
		otherwise =>
		  make(<text-record>,
		       text: text, x: x - left, y: y - top,
		       region: region, transform: transform,
		       medium-state: medium-state)
	      end;
    add-output-record(sheet, record)
  end;
  when (sheet-drawing?(sheet))
    next-method()
  end;
  record
end method draw-text;
||#

(defmethod draw-text ((sheet <output-recording-mixin>) text x y
                      &key ((:start _start)) ((:end _end)) align-x align-y (do-tabs? nil)
                      towards-x towards-y transform-glyphs?)
  (let ((record nil))
    (when (sheet-recording? sheet)
      (let ((medium (sheet-medium sheet))
            (medium-state (sheet-medium-state sheet))
            ;; Copy iff it's a substring, otherwise ensure it's a string
            (text (if (or _start _end)
                      (subseq text (or _start 0) _end)  ;; was copy-seq
                      ;; else
                      (coerce text 'string))))
        (transform-coordinates! (medium-transform medium) x y)
        (when (or towards-x towards-y)
          (transform-coordinates! (medium-transform medium) towards-x towards-y))
        (multiple-value-bind (left top right bottom)
            (text-bounding-box medium text x y
                               :align-x (or align-x :left)
                               :align-y (or align-y :baseline)
                               :do-tabs? do-tabs?
                               :towards-x towards-x :towards-y towards-y
                               :transform-glyphs? transform-glyphs?)
          (when (or towards-x towards-y)
            (translate-coordinates! (- left) (- top) towards-x towards-y))
          (multiple-value-bind (rx ry)
              (point-position (sheet-output-record-position sheet))
            (let ((transform (make-instance '<mutable-translation-transform> :tx (- left rx) :ty (- top ry)))
                  (region    (make-bounding-box 0 0 (- right left) (- bottom top))))
              (setf record (cond ((or towards-x towards-y transform-glyphs?)
                                  (make-instance '<oriented-text-record>
                                        :text text :x (- x left) :y (- y top)
                                        :towards-x towards-x :towards-y towards-y
                                        :transform-glyphs? transform-glyphs?
                                        :align-x (or align-x :left) :align-y (or align-y :baseline)
                                        :region region :transform transform
                                        :medium-state medium-state))
                                 ((or align-x align-y)
                                  (make-instance '<aligned-text-record>
						 :text text :x (- x left) :y (- y top)
						 :align-x (or align-x :left) :align-y (or align-y :baseline)
						 :region region :transform transform
						 :medium-state medium-state))
                                 (t
                                  (make-instance '<text-record>
						 :text text :x (- x left) :y (- y top)
						 :region region :transform transform
						 :medium-state medium-state))))
              (add-output-record sheet record))))))
    (when (sheet-drawing? sheet)
      (call-next-method))
    record))


#||
define method handle-repaint
    (record :: <text-record>, medium :: <medium>, region :: <region>) => ()
  ignore(region);
  when (default-background(record))
    repaint-background(record, medium)
  end;
  with-record-medium-state (state = medium, record)
    dynamic-bind (medium-merged-text-style(medium) = medium-state-text-style(state))
      draw-text(medium, record.%text, record.%x, record.%y,
		align-x: #"left", align-y: #"baseline")
    end
  end
end method handle-repaint;
||#

(defmethod handle-repaint ((record <text-record>) (medium <medium>) (region <region>))
  (declare (ignore region))
  (when (default-background record)
    (repaint-background record medium))
  (with-record-medium-state ((state = medium) record)
    (dynamic-bind (((medium-merged-text-style medium) = (medium-state-text-style state)))
      (draw-text medium (%text record) (%x record) (%y record)
                 :align-x :left :align-y :baseline))))


#||
define method handle-repaint
    (record :: <aligned-text-record>, medium :: <medium>, region :: <region>) => ()
  ignore(region);
  when (default-background(record))
    repaint-background(record, medium)
  end;
  with-record-medium-state (state = medium, record)
    dynamic-bind (medium-merged-text-style(medium) = medium-state-text-style(state))
      draw-text(medium,
		record.%text, record.%x, record.%y,
		align-x: record.%align-x, align-y: record.%align-y)
    end
  end
end method handle-repaint;
||#

(defmethod handle-repaint ((record <aligned-text-record>) (medium <medium>) (region <region>))
  (declare (ignore region))
  (when (default-background record)
    (repaint-background record medium))
  (with-record-medium-state ((state = medium) record)
    (dynamic-bind (((medium-merged-text-style medium) = (medium-state-text-style state)))
      (draw-text medium
		 (%text record) (%x record) (%y record)
		 :align-x (%align-x record) :align-y (%align-y record)))))


#||
define method handle-repaint
    (record :: <oriented-text-record>, medium :: <medium>, region :: <region>) => ()
  ignore(region);
  when (default-background(record))
    repaint-background(record, medium)
  end;
  with-record-medium-state (state = medium, record)
    dynamic-bind (medium-merged-text-style(medium) = medium-state-text-style(state))
      draw-text(medium,
		record.%text, record.%x, record.%y,
		align-x: record.%align-x, align-y: record.%align-y,
		towards-x: record.%towards-x, towards-y: record.%towards-y,
		transform-glyphs?: record.%transform-glyphs?)
    end
  end
end method handle-repaint;
||#

(defmethod handle-repaint ((record <oriented-text-record>) (medium <medium>) (region <region>))
  (declare (ignore region))
  (when (default-background record)
    (repaint-background record medium))
  (with-record-medium-state ((state = medium) record)
    (dynamic-bind (((medium-merged-text-style medium) = (medium-state-text-style state)))
      (draw-text medium
		 (%text record) (%x record) (%y record)
		 :align-x (%align-x record) :align-y (%align-y record)
		 :towards-x (%towards-x record) :towards-y (%towards-y record)
		 :transform-glyphs? (%transform-glyphs? record)))))


#||
define method text-bounding-box
    (medium :: <basic-medium>, string, x, y,
     #key text-style = medium-merged-text-style(medium),
          align-x = #"left", align-y = #"baseline", do-tabs? = #f, do-newlines? = #f,
          towards-x, towards-y, transform-glyphs?)
  let (font, width, height, ascent, descent)
    = font-metrics(text-style, port(medium));
  ignore(width, height);
  let width = text-size(medium, string, text-style: text-style,
			do-newlines?: do-newlines?, do-tabs?: do-tabs?);
  let height = ascent + descent;
  let vl = #f;
  let vt = #f;
  let vr = #f;
  let vb = #f;
  select (align-x)
    #"left" =>
      vl := x;
      vr := x + width;
    #"right" =>
      vl := x - width;
      vr := x;
    #"center" =>
      vl := x - floor/(width, 2);
      vr := x + ceiling/(width, 2)
  end;
  select (align-y)
    #"baseline" =>
      vt := y - height;
      vb := y + descent;
    #"top" =>
      vt := y;
      vb := y + height;
    #"bottom" =>
      vt := y - height;
      vb := y;
    #"center" =>
      vt := y - floor/(height, 2);
      vb := y + ceiling/(height, 2)
  end;
  fix-box(vl, vt, vr, vb)
end method text-bounding-box;
||#

(defgeneric text-bounding-box (medium string x y
			       &key text-style
			       align-x align-y do-tabs? do-newlines?
			       towards-x towards-y transform-glyphs?))

(defmethod text-bounding-box ((medium <basic-medium>) string x y
			      &key (text-style (medium-merged-text-style medium))
				(align-x :left) (align-y :baseline) (do-tabs? nil) (do-newlines? nil)
				towards-x towards-y transform-glyphs?)
  (declare (ignore towards-x towards-y transform-glyphs?))
  (multiple-value-bind (font width height ascent descent)
      (font-metrics text-style (port medium))
    (declare (ignore font width height))  ;; ::FIXME:: Should we really ignore font? Not in the original Dylan...
    (let ((width (text-size medium string :text-style text-style
			    :do-newlines? do-newlines? :do-tabs? do-tabs?))
	  (height (+ ascent descent))
	  (vl nil)
	  (vt nil)
	  (vr nil)
	  (vb nil))
      (ecase align-x
	(:left (setf vl x)
	       (setf vr (+ x width)))
	(:right (setf vl (- x width))
		(setf vr x))
	(:center (setf vl (- x (floor width 2)))
		 (setf vr (+ x (ceiling width 2)))))
      (ecase align-y
	(:baseline (setf vt (- y height))
		   (setf vb (+ y descent)))
	(:top (setf vt y)
	      (setf vb (+ y height)))
	(:bottom (setf vt (- y height))
		 (setf vb y))
	(:center (setf vt (- y (floor height 2)))
		 (setf vb (+ y (ceiling height 2)))))
      (fix-box vl vt vr vb))))



#||

/// 'draw-bezier-curve'

define sealed class <bezier-curve-record> (<graphics-record>)
  sealed slot %coord-seq, required-init-keyword: coord-seq:;
  sealed slot %filled? = #t, init-keyword: filled?:;
end class <bezier-curve-record>;

define sealed domain make (singleton(<bezier-curve-record>));
define sealed domain initialize (<bezier-curve-record>);
||#

(defclass <bezier-curve-record> (<graphics-record>)
  ((%coord-seq :initarg :coord-seq :initform (required-slot ":coord-seq" "<bezier-curve-record>") :accessor %coord-seq)
   (%filled? :initarg :filled? :initform t :accessor %filled?)))


#||
// See 'draw-lines' for commentary on what exactly is happening here
define method draw-bezier-curve
    (sheet :: <output-recording-mixin>, coord-seq :: <coordinate-sequence>,
     #key filled? = #t) => (record)
  let record = #f;
  when (sheet-recording?(sheet))
    let medium = sheet-medium(sheet);
    let medium-state = sheet-medium-state(sheet);
    let coord-seq
      = transform-coordinate-sequence
          (medium-transform(medium), coord-seq, copy?: #t);
    FOR (I FROM 0 BELOW SIZE(COORD-SEQ))	//---*** until 'coordinate-sequence-box' is fixed
      COORD-SEQ[I] := FLOOR(COORD-SEQ[I])
    END;
    let (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
      = begin
          let thickness
            = if (filled?) 0 else pen-width(medium-pen(medium)) end;
          coordinate-sequence-box(coord-seq, thickness: thickness)
        end;
    let (rx, ry) = point-position(sheet-output-record-position(sheet));
    let transform = make(<mutable-translation-transform>, tx: left - rx, ty: top - ry);
    let region    = make-bounding-box(0, 0, right - left, bottom - top);
    translate-coordinate-sequence!(-left, -top, coord-seq);
    record := make(<bezier-curve-record>,
		   coord-seq: coord-seq,
		   filled?: filled?,
		   region: region, transform: transform,
		   medium-state: medium-state);
    add-output-record(sheet, record)
  end;
  when (sheet-drawing?(sheet))
    next-method()
  end;
  record
end method draw-bezier-curve;
||#

(defmethod draw-bezier-curve ((sheet <output-recording-mixin>) coord-seq
			      &key (filled? t))
  (check-type coord-seq <coordinate-sequence>)
  (let ((record nil))
    (when (sheet-recording? sheet)
      (let* ((medium (sheet-medium sheet))
	     (medium-state (sheet-medium-state sheet))
	     (coord-seq (transform-coordinate-sequence (medium-transform medium) coord-seq :copy? t)))
	(LOOP FOR I FROM 0 BELOW (LENGTH COORD-SEQ)	;;---*** until 'coordinate-sequence-box' is fixed
	   DO (SETF (AREF COORD-SEQ I) (FLOOR (AREF COORD-SEQ I))))
	(multiple-value-bind (left top right bottom)
	    (let ((thickness (if filled? 0 (pen-width (medium-pen medium)))))
	      (coordinate-sequence-box coord-seq :thickness thickness))
	  (multiple-value-bind (rx ry)
	      (point-position (sheet-output-record-position sheet))
	    (let ((transform (make-instance '<mutable-translation-transform> :tx (- left rx) :ty (- top ry)))
		  (region    (make-bounding-box 0 0 (- right left) (- bottom top))))
	      (translate-coordinate-sequence! (- left) (- top) coord-seq)
	      (setf record (make-instance '<bezier-curve-record>
					  :coord-seq coord-seq
					  :filled? filled?
					  :region region :transform transform
					  :medium-state medium-state))
	      (add-output-record sheet record))))))
    (when (sheet-drawing? sheet)
      (call-next-method))
    record))


#||
define method handle-repaint
    (record :: <bezier-curve-record>, medium :: <medium>, region :: <region>) => ()
  ignore(region);
  when (default-background(record))
    repaint-background(record, medium)
  end;
  with-record-medium-state (medium, record)
    draw-bezier-curve(medium, record.%coord-seq, filled?: record.%filled?)
  end
end method handle-repaint;
||#

(defmethod handle-repaint ((record <bezier-curve-record>) (medium <medium>) (region <region>))
  (declare (ignore region))
  (when (default-background record)
    (repaint-background record medium))
  (with-record-medium-state (medium record)
    (draw-bezier-curve medium (%coord-seq record) :filled? (%filled? record))))



#||

/// 'draw-image'

define sealed class <image-record> (<graphics-record>)
  sealed slot %image, required-init-keyword: image:;
  sealed slot %x, required-init-keyword: x:;
  sealed slot %y, required-init-keyword: y:;
end class <image-record>;

define sealed domain make (singleton(<image-record>));
define sealed domain initialize (<image-record>);
||#

(defclass <image-record> (<graphics-record>)
  ((%image :initarg :image :initform (required-slot ":image" "<image-record>") :accessor %image)
   (%x :initarg :x :initform (required-slot ":x" "<image-record>") :accessor %x)
   (%y :initarg :y :initform (required-slot ":y" "<image-record>") :accessor %y)))


#||
// See 'draw-line' for commentary on what exactly is happening here
define method draw-image
    (sheet :: <output-recording-mixin>, image :: <image>, x, y) => (record)
  let record = #f;
  when (sheet-recording?(sheet))
    let medium = sheet-medium(sheet);
    let medium-state = sheet-medium-state(sheet);
    transform-coordinates!(medium-transform(medium), x, y);
    let (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
      = fix-box(x, y, x + image-width(image), y + image-height(image));
    let (rx, ry) = point-position(sheet-output-record-position(sheet));
    let transform = make(<mutable-translation-transform>, tx: left - rx, ty: top - ry);
    let region    = make-bounding-box(0, 0, right - left, bottom - top);
    record := make(<image-record>,
		   image: image, x: x - left, y: y - top,
		   region: region, transform: transform,
		   medium-state: medium-state);
    add-output-record(sheet, record)
  end;
  when (sheet-drawing?(sheet))
    next-method()
  end;
  record
end method draw-image;
||#

(defmethod draw-image ((sheet <output-recording-mixin>) (image <image>) x y)
  (let ((record nil))
    (when (sheet-recording? sheet)
      (let ((medium (sheet-medium sheet))
	    (medium-state (sheet-medium-state sheet)))
	(transform-coordinates! (medium-transform medium) x y)
	(multiple-value-bind (left top right bottom)
	    (fix-box x y (+ x (image-width image)) (+ y (image-height image)))
	  (multiple-value-bind (rx ry)
	      (point-position (sheet-output-record-position sheet))
	    (let ((transform (make-instance '<mutable-translation-transform> :tx (- left rx) :ty (- top ry)))
		  (region    (make-bounding-box 0 0 (- right left) (- bottom top))))
	      (setf record (make-instance '<image-record>
					  :image image :x (- x left) :y (- y top)
					  :region region :transform transform
					  :medium-state medium-state))
	      (add-output-record sheet record))))))
    (when (sheet-drawing? sheet)
      (call-next-method))
    record))


#||
define method handle-repaint
    (record :: <image-record>, medium :: <medium>, region :: <region>) => ()
  ignore(region);
  when (default-background(record))
    repaint-background(record, medium)
  end;
  with-record-medium-state (medium, record)
    draw-image(medium, record.%image, record.%x, record.%y)
  end
end method handle-repaint;
||#

(defmethod handle-repaint ((record <image-record>) (medium <medium>) (region <region>))
  (declare (ignore region))
  (when (default-background record)
    (repaint-background record medium))
  (with-record-medium-state (medium record)
    (draw-image medium (%image record) (%x record) (%y record))))



#||

/// 'draw-pixmap'

define sealed class <pixmap-record> (<graphics-record>)
  sealed slot %pixmap, required-init-keyword: pixmap:;
  sealed slot %x, required-init-keyword: x:;
  sealed slot %y, required-init-keyword: y:;
  sealed slot %function = $boole-1, init-keyword: function:;
end class <pixmap-record>;

define sealed domain make (singleton(<pixmap-record>));
define sealed domain initialize (<pixmap-record>);
||#

(defclass <pixmap-record> (<graphics-record>)
  ((%pixmap   :initarg :pixmap   :initform (required-slot ":pixmap" "<pixmap-record>") :accessor %pixmap)
   (%x        :initarg :x        :initform (required-slot ":x" "<pixmap-record>")      :accessor %x)
   (%y        :initarg :y        :initform (required-slot ":y" "<pixmap-record>")      :accessor %y)
   (%function :initarg :function :initform +boole-1+                                   :accessor %function)))


#||
// See 'draw-line' for commentary on what exactly is happening here
define method draw-pixmap
    (sheet :: <output-recording-mixin>, pixmap :: <pixmap>, x, y,
     #key function = $boole-1) => (record)
  let record = #f;
  when (sheet-recording?(sheet))
    let medium = sheet-medium(sheet);
    let medium-state = sheet-medium-state(sheet);
    transform-coordinates!(medium-transform(medium), x, y);
    let (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
      = fix-box(x, y, x + image-width(pixmap), y + image-height(pixmap));
    let (rx, ry) = point-position(sheet-output-record-position(sheet));
    let transform = make(<mutable-translation-transform>, tx: left - rx, ty: top - ry);
    let region    = make-bounding-box(0, 0, right - left, bottom - top);
    record := make(<pixmap-record>,
		   pixmap: pixmap, x: x - left, y: y - top,
		   function: function,
		   region: region, transform: transform,
		   medium-state: medium-state);
    add-output-record(sheet, record)
  end;
  when (sheet-drawing?(sheet))
    next-method()
  end;
  record
end method draw-pixmap;
||#

(defmethod draw-pixmap ((sheet <output-recording-mixin>) (pixmap <pixmap>) x y
			&key (function +boole-1+))
  (let ((record nil))
    (when (sheet-recording? sheet)
      (let ((medium (sheet-medium sheet))
	    (medium-state (sheet-medium-state sheet)))
	(transform-coordinates! (medium-transform medium) x y)
	(multiple-value-bind (left top right bottom)
	    (fix-box x y (+ x (image-width pixmap)) (+ y (image-height pixmap)))
	  (multiple-value-bind (rx ry)
	      (point-position (sheet-output-record-position sheet))
	    (let ((transform (make-instance '<mutable-translation-transform> :tx (- left rx) :ty (- top ry)))
		  (region    (make-bounding-box 0 0 (- right left) (- bottom top))))
	      (setf record (make-instance '<pixmap-record>
					  :pixmap pixmap :x (- x left) :y (- y top)
					  :function function
					  :region region :transform transform
					  :medium-state medium-state))
	      (add-output-record sheet record))))))
    (when (sheet-drawing? sheet)
      (call-next-method))
    record))


#||
define method handle-repaint
    (record :: <pixmap-record>, medium :: <medium>, region :: <region>) => ()
  ignore(region);
  when (default-background(record))
    repaint-background(record, medium)
  end;
  with-record-medium-state (medium, record)
    draw-pixmap(medium,
		record.%pixmap, record.%x, record.%y,
		function: record.%function)
  end
end method handle-repaint;
||#

(defmethod handle-repaint ((record <pixmap-record>) (medium <medium>) (region <region>))
  (declare (ignore region))
  (when (default-background record)
    (repaint-background record medium))
  (with-record-medium-state (medium record)
    (draw-pixmap medium
		 (%pixmap record) (%x record) (%y record)
		 :function (%function record))))



