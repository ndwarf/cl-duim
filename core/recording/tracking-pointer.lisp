;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-RECORDING-INTERNALS -*-
(in-package #:duim-recording-internals)

#||
/// 'tracking-pointer'

//---*** Perhaps this belongs in the sheets layer?
define macro tracking-pointer
  { tracking-pointer (?event:name, ?sheet:expression, #rest ?options:expression) ?clauses:* end }
    => { begin
	   local method tracking-pointer-clauses (?event)
	     select (?event by instance?)
	       ?clauses;
               otherwise => #f;
	     end
	   end method;
	   do-tracking-pointer(?sheet, tracking-pointer-clauses, ?options)
	 end }
  { tracking-pointer (?sheet:expression, #rest ?options:expression) ?clauses:* end }
    => { begin
	   local method tracking-pointer-clauses (_event)
	     select (_event by instance?)
	       ?clauses;
               otherwise => #f;
	     end
	   end method;
	   do-tracking-pointer(?sheet, tracking-pointer-clauses, ?options)
	 end }
 clauses:
  { } => { }
  { ?clause:*; ... }
    => { ?clause; ... }
 clause:
  { ?event-class:name => ?body:* }
    => { ?event-class => ?body }
end macro tracking-pointer;
||#

(defmacro tracking-pointer ((event sheet &rest options) &body clauses)
  `(let ((event (if ,event ,event (gensym))))
    (labels ((tracking-pointer-clauses (,event)
               (typecase ,event
                 ,@clauses
                 (t nil))))
      ;; Changed to APPLY to cater to options being NIL.
;;      (do-tracking-pointer ,sheet #'tracking-pointer-clauses :multiple-windows? t ,options))))
      (apply #'do-tracking-pointer ,sheet #'tracking-pointer-clauses ,options))))


#||
/// The pointer-tracking event handler

define sealed class <tracking-event-handler> (<event-handler>)
  sealed constant slot %function :: <function>,
    required-init-keyword: function:;
end class <tracking-event-handler>;
||#

(defclass <tracking-event-handler> (<event-handler>)
  ((%function :type function :initarg :function :initform (required-slot ":function" "<tracking-event-handler>")
	      :reader %function)))


#||
// Pass unhandled events to the original event handler
define method handle-event
    (handler :: <tracking-event-handler>, event :: <event>) => ()
  handle-event(event-client(event), event)
end method handle-event;
||#

(defmethod handle-event ((handler <tracking-event-handler>) (event <event>))
  (handle-event (event-client event) event))


#||
define method handle-event
    (handler :: <tracking-event-handler>, event :: <pointer-motion-event>) => ()
  handler.%function(event)
end method handle-event;
||#

(defmethod handle-event ((handler <tracking-event-handler>) (event <pointer-motion-event>))
  (funcall (%function handler) event))


#||
define method handle-event
    (handler :: <tracking-event-handler>, event :: <pointer-button-event>) => ()
  handler.%function(event)
end method handle-event;
||#

(defmethod handle-event ((handler <tracking-event-handler>) (event <pointer-button-event>))
  (funcall (%function handler) event))


#||
define method handle-event
    (handler :: <tracking-event-handler>, event :: <keyboard-event>) => ()
  handler.%function(event)
end method handle-event;
||#

(defmethod handle-event ((handler <tracking-event-handler>) (event <keyboard-event>))
  (funcall (%function handler) event))


#||
/// The guts of 'tracking-pointer'

define open generic do-tracking-pointer
    (sheet :: <abstract-sheet>, clauses :: <function>,
     #rest options,
     #key multiple-windows?, #all-keys)
 => (#rest values);
||#

(defgeneric do-tracking-pointer (sheet clauses
				 &rest options
				 &key multiple-windows? &allow-other-keys))


#||
//---*** Implement 'multiple-windows?'
//---*** Support presentations, highlighting...
define sealed method do-tracking-pointer
    (sheet :: <basic-sheet>, clauses :: <function>,
     #key multiple-windows? = #t)
 => (#rest values)
  let old-handler = event-handler(sheet);
  let new-handler = make(<tracking-event-handler>, function: clauses);
  block ()
    //---*** This should probably do 'with-pointed-grabbed', right?
    event-handler(sheet) := new-handler;
    let top-sheet = top-level-sheet(sheet-frame(sheet));
    while (#t)
      let event = read-event(top-sheet);
      handle-event(new-handler, event);
    end;
  cleanup
    event-handler(sheet) := old-handler;
  end
end method do-tracking-pointer;
||#

(defmethod do-tracking-pointer ((sheet <basic-sheet>) (clauses function)
                                &rest options &key (multiple-windows? t)
                                &allow-other-keys)
  (declare (ignore options multiple-windows?))
  (let ((old-handler (event-handler sheet))
        (new-handler (make-instance '<tracking-event-handler> :function clauses)))
    (unwind-protect
         ;;---*** This should probably do 'with-pointed-grabbed', right?
         (progn
           (setf (event-handler sheet) new-handler)
           (let ((top-sheet (top-level-sheet (sheet-frame sheet))))
             ;; ::fixme:: How do we break out of this loop?!? Is it the responsibity
	     ;; of the caller of TRACKING-POINTER?
             (loop while t
		do (let ((event (read-event top-sheet)))
		     (handle-event new-handler event)))))
      ;; cleanup
      (setf (event-handler sheet) old-handler))))



#||

/// 'dragging-output'

//---*** Do this
||#


