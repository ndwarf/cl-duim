;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-LAYOUTS-INTERNALS -*-
(in-package #:duim-layouts-internals)

#||
/// Box Panes

// In Motif, this would be a row-column widget...
define open abstract class <box-layout-pane> 
    (<layout-border-mixin>,
     <layout-pane>)
  sealed slot %max-major-size :: <integer> = 0;
  sealed slot %max-minor-size :: <integer> = 0;
end class <box-layout-pane>;
||#


(defclass <box-layout-pane>
    (<layout-border-mixin>
     <layout-pane>)
  ((%max-major-size :type integer :initform 0 :accessor %max-major-size)
   (%max-minor-size :type integer :initform 0 :accessor %max-minor-size))
  (:documentation
"
In Motif, this would be a row-column widget...
")
  (:metaclass <abstract-metaclass>))


(defgeneric layout-equalize-widths? (box)
  (:documentation
"
Returns T if the children of _layout_ are all the same width. The
layout must be either a row or a column layout.

You can only set this value when a layout is created, using
the :equalize-widths? initarg. There is no equivalent setter function.
"))

(defgeneric layout-equalize-heights? (box)
  (:documentation
"
Returns T if the children of _layout_ are all the same height. The
layout must be either a row or a column layout.

You can only set this value when a layout is created, using
the :equalize-heights? initarg. There is no equivalent setter
function.
"))
(defgeneric box-pane-major-spacing (box))
;;; FIXME: These two should just be a DEFUNs, not a GFs. Made GFs to get type checks
;;; for free...
(defgeneric box-pane-compose-space (box-pane requested-major fn-major fn-major- fn-major+
					     requested-minor fn-minor fn-minor- fn-minor+
					     space-composer space-req-creator
					     &key equalize-major-size? equalize-minor-size?))
(defgeneric box-pane-allocate-space (box-pane major-sizes box-major-size box-minor-size compose
					      alignment-fn set-child-edges &key major-size-override
					      minor-size-override))

#||
// Bits 12..13 of 'sheet-flags' are reserved for box layouts
define constant %equalize_widths  :: <integer> = #o10000;
define constant %equalize_heights :: <integer> = #o20000;
||#

(defconstant %equalize_widths  #o10000)
(defconstant %equalize_heights #o20000)


#||
define method initialize
    (box :: <box-layout-pane>, #key equalize-widths? = #f, equalize-heights? = #f)
  next-method();
  let bits = logior(if (equalize-widths?)  %equalize_widths  else 0 end,
		    if (equalize-heights?) %equalize_heights else 0 end);
  sheet-flags(box) := logior(sheet-flags(box), bits)
end method initialize;
||#

(defmethod initialize-instance :after ((box <box-layout-pane>) &key (equalize-widths? nil) (equalize-heights? nil)
				       &allow-other-keys)
  (let ((bits (logior (if equalize-widths? %equalize_widths 0)
		      (if equalize-heights? %equalize_heights 0))))
    (setf (sheet-flags box) (logior (sheet-flags box) bits))))


#||
// Don't change the order of the sheets, because that will break the layout
// Note that 'raise-sheet' will arrange to raise mirrors, which is OK
define method do-raise-sheet
    (parent :: <box-layout-pane>, sheet :: <sheet>, #key activate? = #t) => ()
  ignore(activate?);
  #f
end method do-raise-sheet;
||#

;; Don't change the order of the sheets, because that will break the layout
;; Note that 'raise-sheet' will arrange to raise mirrors, which is ok
(defmethod do-raise-sheet ((parent <box-layout-pane>) (sheet <sheet>) &key (activate? t))
  (declare (ignore activate?))
  nil)


#||
define sealed inline method layout-equalize-widths?
    (box :: <box-layout-pane>) => (equalize? :: <boolean>)
  logand(sheet-flags(box), %equalize_widths) = %equalize_widths
end method layout-equalize-widths?;
||#

(defmethod layout-equalize-widths? ((box <box-layout-pane>))
  (= (logand (sheet-flags box) %equalize_widths) %equalize_widths))


#||
define sealed inline method layout-equalize-heights?
    (box :: <box-layout-pane>) => (equalize? :: <boolean>)
  logand(sheet-flags(box), %equalize_heights) = %equalize_heights
end method layout-equalize-heights?;
||#

(defmethod layout-equalize-heights? ((box <box-layout-pane>))
  (= (logand (sheet-flags box) %equalize_heights) %equalize_heights))


#||
define function box-pane-compose-space
    (box-pane :: <box-layout-pane>,
     requested-major :: false-or(<integer>),
     fn-major :: <function>, fn-major- :: <function>, fn-major+ :: <function>,
     requested-minor :: false-or(<integer>),
     fn-minor :: <function>, fn-minor- :: <function>, fn-minor+ :: <function>,
     space-composer :: <function>, space-req-creator :: <function>,
     #key equalize-major-size?, equalize-minor-size?)
 => (space-req :: <space-requirement>)
  ignore(equalize-minor-size?);
  let children = sheet-children(box-pane);
  let n-children :: <integer> = size(children) - count(sheet-withdrawn?, children);
  let major  :: <integer> = 0;
  let major+ :: <integer> = 0;
  let major- :: <integer> = 0;
  let minor  :: <integer> = 0;
  let minor+ :: <integer> = 0;
  let minor- :: <integer> = 0;
  let minor-min :: <integer> = 0;
  let minor-max :: <integer> = 0;
  let max-major-size :: <integer> = 0;
  let max-minor-size :: <integer> = 0;
  local method update-major-space (space-req) => ()
	  let m  :: <integer> = fn-major (box-pane, space-req);
	  let m+ :: <integer> = fn-major+(box-pane, space-req);
	  let m- :: <integer> = fn-major-(box-pane, space-req);
	  max!(max-major-size, m);
	  inc!(major,  m);
	  inc!(major+, m+);
	  inc!(major-, m-)
	end method,
        method update-minor-space (space-req) => ()
	  let m  :: <integer> = fn-minor (box-pane, space-req);
	  let m+ :: <integer> = fn-minor+(box-pane, space-req);
	  let m- :: <integer> = fn-minor-(box-pane, space-req);
	  max!(max-minor-size, m);
	  max!(minor-max, m+);
	  max!(minor-min, m-);
	  minor := min(max(minor, m, minor-min), minor-max)
	end method;
  for (child in children)
    when (child)
      let child :: <basic-sheet> = child;	// force tighter type...
      unless (sheet-withdrawn?(child))
	let space-req = space-composer(child);
	update-major-space(space-req);
	update-minor-space(space-req)
      end
    end
  end;
  let border*2 = layout-border(box-pane) * 2;
  local method cleanup-major-space () => ()
	  let size-for-spacing :: <integer>
	    = (n-children - 1) * box-pane-major-spacing(box-pane) + border*2;
	  if (equalize-major-size?)
	    major := n-children * max-major-size + size-for-spacing;
	    when (major- < $fill) major- := major end;
	    when (major+ < $fill) major+ := major end
	  else
	    inc!(major,  size-for-spacing);
	    inc!(major-, size-for-spacing);
	    inc!(major+, size-for-spacing)
	  end;
	  // If there's a requested size, use it, but ensure that it
	  // falls between the min and max sizes
	  when (requested-major)
	    major := max(major-, min(major+, requested-major))
	  end
	end method,
        method cleanup-minor-space () => ()
	  minor- := minor-min + border*2;
	  minor+ := minor-max + border*2;
	  minor  := minor + border*2;
	  when (requested-minor)
	    minor := max(minor-, min(minor+, requested-minor))
	  end
	end method;
  cleanup-major-space();
  cleanup-minor-space();
  box-pane.%max-major-size := max-major-size;
  box-pane.%max-minor-size := max-minor-size;
  space-req-creator(major, major-, major+, minor, minor-, minor+)
end function box-pane-compose-space;
||#

(defmethod box-pane-compose-space ((box-pane <box-layout-pane>)
				   requested-major
				   (fn-major function) (fn-major- function) (fn-major+ function)
				   requested-minor
				   (fn-minor function) (fn-minor- function) (fn-minor+ function)
				   (space-composer function) (space-req-creator function)
				   &key equalize-major-size? equalize-minor-size?)
  (declare (ignore equalize-minor-size?))
  ;; For <column-layout>, height is major and width is minor.
  ;; For <row-layout> it's the other way around.
  (check-type requested-major (or null integer))
  (check-type requested-minor (or null integer))
  (let* ((children (sheet-children box-pane))
	 (n-children (count-if-not #'sheet-withdrawn? children))
	 ;; The next 6 hold the actual space requirements for the layout pane
	 (major 0)   ;; space wanted in major dimension
	 (major+ 0)  ;; max major space
	 (major- 0)  ;; min major space
	 (minor 0)   ;; space wanted in minor dimension
	 (minor+ 0)  ;; max minor space
	 (minor- 0)  ;; min minor space
	 (minor-min 0)    ;; temporary holder for minor dimension minimum size
	 (minor-max 0)    ;; temporary holder for minor dimension maximum size
	 ;; Used if we're equalizing sizes for the children...
	 (max-major-size 0)   ;; largest major space request seen...
	 (max-minor-size 0))  ;; largest minor space request seen...
    (flet ((update-major-space (space-req)
	     (let ((m  (funcall fn-major  box-pane space-req))
		   (m+ (funcall fn-major+ box-pane space-req))
		   (m- (funcall fn-major- box-pane space-req)))
	       (setf max-major-size (max max-major-size m))
	       (incf major m)
	       (incf major+ m+)
	       (incf major- m-)))
	   (update-minor-space (space-req)
	     (let ((m  (funcall fn-minor  box-pane space-req))
		   (m+ (funcall fn-minor+ box-pane space-req))
		   (m- (funcall fn-minor- box-pane space-req)))
	       (setf max-minor-size (max max-minor-size m))
	       (setf minor-max (max minor-max m+))
	       (setf minor-min (max minor-min m-))
	       (setf minor (min (max minor m minor-min) minor-max)))))
      ;; Loop over children and invoke compose-space (usually) on them;
      ;; update the major + minor requirements of the layout accordingly.
      ;; The 'children' slot is of type SEQUENCE but in practice they are
      ;; always a vector.
      ;; FIXME: make them always be a specific sequence type - always
      ;; vectors.
      (loop for child across children
	 when child
	 do (unless (sheet-withdrawn? child)
	      (let ((space-req (funcall space-composer child)))
		(update-major-space space-req)
		(update-minor-space space-req))))
      (let ((border*2 (* (layout-border box-pane) 2)))
	(flet ((cleanup-major-space ()
		 (let ((size-for-spacing (+ (* (- n-children 1) (box-pane-major-spacing box-pane)) border*2)))
		   ;; if equalizing, make it a fixed-layout large enough
		   ;; for the greediest child.
		   (cond (equalize-major-size?
			  (setf major (+ (* n-children max-major-size) size-for-spacing))
			  (when (< major- +fill+) (setf major- major))
			  (when (< major+ +fill+) (setf major+ major)))
			 (t (incf major  size-for-spacing)
			    (incf major- size-for-spacing)
			    (incf major+ size-for-spacing)))
		   ;; If there's a requested size, use it, but ensure that it
		   ;; falls between the min and max sizes
		   (when requested-major
		     (setf major (max major- (min major+ requested-major))))))
	       (cleanup-minor-space ()
		 ;; Make sure the minor dimension is large enough to include
		 ;; the border... this is less complicated than the major
		 ;; space cleanup since we know that the number of children
		 ;; in the minor dimension = 1 (this is a row or a column,
		 ;; remember?)
		 (setf minor- (+ minor-min border*2))
		 (setf minor+ (+ minor-max border*2))
		 (setf minor  (+ minor border*2))
		 (when requested-minor
		   (setf minor (max minor- (min minor+ requested-minor))))))
	  ;; Include padding for any intra-child border and deal with
	  ;; size-equalization if necessary.
	  (cleanup-major-space)
	  ;; Ensure there's enough space for the border in the minor dimension
	  ;; too...
	  (cleanup-minor-space)
	  (setf (%max-major-size box-pane) max-major-size)
	  (setf (%max-minor-size box-pane) max-minor-size)
	  ;; Create a <space-requirement> for the layout.
	  (funcall space-req-creator major major- major+ minor minor- minor+))))))


#||
define function box-pane-allocate-space 
    (box-pane :: <box-layout-pane>,
     major-sizes :: <vector>, box-major-size :: <integer>, box-minor-size :: <integer>,
     compose :: <function>, alignment-function :: <function>, set-child-edges :: <function>,
     #key major-size-override, minor-size-override) => ()
  let border = layout-border(box-pane);
  let major-spacing  :: <integer> = box-pane-major-spacing(box-pane);
  let major-position :: <integer> = border;
  for (sheet in sheet-children(box-pane),
       suggested-major-size :: <integer> in major-sizes)
    when (sheet)
      let sheet :: <basic-sheet> = sheet;	// force tighter type...
      unless (sheet-withdrawn?(sheet))
	let (major-size :: <integer>, minor-size :: <integer>)
	  = compose(sheet, 
		    minor: box-minor-size,
		    major: suggested-major-size);
	let minor-position :: <integer>
	  = alignment-function(box-pane, sheet, 0, box-minor-size - minor-size) + border;
	let major-size :: <integer> = major-size-override | major-size;
	let minor-size :: <integer> = minor-size-override | minor-size;
	set-child-edges(sheet,
			major-position, minor-position, 
			major-position + major-size, minor-position + minor-size);
	inc!(major-position, major-size + major-spacing)
      end
    end
  end
end function box-pane-allocate-space;
||#


(defmethod box-pane-allocate-space ((box-pane <box-layout-pane>)
				    (major-sizes array) (box-major-size integer) (box-minor-size integer)
				    (compose function) (alignment-function function) (set-child-edges function)
				    &key major-size-override minor-size-override)
  (declare (ignorable box-major-size))
  (let* ((border         (layout-border box-pane))
	 (major-spacing  (box-pane-major-spacing box-pane))
	 (major-position border))
    ;; (sheet-children) is defined as a SEQUENCE, but in practice they are
    ;; always a vector.
    (loop for sheet across (sheet-children box-pane)
       for suggested-major-size across major-sizes
       do (when sheet
	    (unless (sheet-withdrawn? sheet)
	      (multiple-value-bind (major-size minor-size)
		  (funcall compose sheet :minor box-minor-size :major suggested-major-size)
		(let ((minor-position (+ (funcall alignment-function box-pane sheet 0 (- box-minor-size minor-size)) border))
		      (major-size (or major-size-override major-size))
		      (minor-size (or minor-size-override minor-size)))
		  (funcall set-child-edges sheet major-position minor-position
			   (+ major-position major-size) (+ minor-position minor-size))
		  (incf major-position (+ major-size major-spacing)))))))))



#||

/// Row panes, formerly known as hboxes

define open abstract class <row-layout>
    (<horizontal-layout-mixin>, <box-layout-pane>)
end class <row-layout>;
||#


(defclass <row-layout>
    (<horizontal-layout-mixin>
     <box-layout-pane>)
  ()
  (:documentation
"
The class of row layouts. A row layout arranges its children in a row,
automatically calculating the size and placement of each child within
the specified parameters.

The :border initarg provides a border of whitespace around the
children in the layout, and the value of this initarg represents the
size of the border in pixels. This basically has the same effect as
using the macro WITH-SPACING around the layout, except it uses a
simpler syntax.

The :spacing or :x-spacing initargs let you specify how much
horizontal space, in pixels, should be inserted between the children
of the layout. These two initargs can be used interchangeably.

If T, :equalize-heights? ensures that all the children of the layout
have the same height.

If T, :equalize-widths? ensures that all the children of the layout
have the same width.

By default, all the children of a row layout are aligned at the
top. You can specify that they should be aligned at the bottom, or in
the center, by using the :y-alignment initarg.

The :ratios, or :x-ratios initargs let you specify the proportion of
the total layout that should be taken up by each individual
child. These two initargs can be used interchangeably.

The value passed to :ratios needs to be a sequence of as many integers
as there are children in the layout. Each child is then allocated the
appropriate portion of horizontal space in the layout. For example, if
the value #(1 2 3) is specified for the :ratios initarg of a row
layout containing three children, then the first child would claim a
sixth of the available horizontal space, the second child would claim
a third of the horizontal space, and the third child would claim half
the horizontal space, as shown in the diagram below.

                         :ratios #(1 2 3)
              +------+------------+------------------+
              |child1|  child 2   |      child 3     |
              +------+------------+------------------+

Example:

    To make a row of buttons that are all the same size:

        (contain (make-pane '<row-layout>
                            :equalize-widths? T
                            :children buttons))
")
  (:metaclass <abstract-metaclass>))


#||
define method box-pane-major-spacing
    (box :: <row-layout>) => (spacing :: <integer>)
  layout-x-spacing(box)
end method box-pane-major-spacing;
||#

(defmethod box-pane-major-spacing ((box <row-layout>))
  (layout-x-spacing box))


#||
define method do-compose-space
    (box-pane :: <row-layout>, #key width, height)
 => (space-req :: <space-requirement>)
  local method space-composer
	    (child) => (sr :: <space-requirement>)
	  compose-space(child, height: height)
	end method,
        method space-req-creator
	    (width, min-width, max-width, height, min-height, max-height)
	 => (sr :: <space-requirement>)
	  make(<space-requirement>,
	       width:  width,  min-width:  min-width,  max-width:  max-width,
	       height: height, min-height: min-height, max-height: max-height)
	end method;
  dynamic-extent(space-composer, space-req-creator);
  if (empty?(sheet-children(box-pane)))
    default-space-requirement(box-pane, width: width, height: height)
  else
    box-pane-compose-space
      (box-pane,
       width,
       space-requirement-width,  space-requirement-min-width,  space-requirement-max-width,
       height,
       space-requirement-height, space-requirement-min-height, space-requirement-max-height,
       space-composer, space-req-creator,
       equalize-major-size?: layout-equalize-widths?(box-pane),
       equalize-minor-size?: layout-equalize-heights?(box-pane))
  end
end method do-compose-space;
||#

(defmethod do-compose-space ((box-pane <row-layout>) &key width height)
  (labels ((space-composer (child)
	     (compose-space child :height height))
	   (space-req-creator (width min-width max-width height min-height max-height)
	     (make-space-requirement :width  width  :min-width  min-width  :max-width  max-width
				     :height height :min-height min-height :max-height max-height)))
    (declare (dynamic-extent #'space-composer #'space-req-creator))
    (if (empty? (sheet-children box-pane))
	(default-space-requirement box-pane :width width :height height)
	;; else
	(box-pane-compose-space box-pane
				width
				#'space-requirement-width #'space-requirement-min-width #'space-requirement-max-width
				height
				#'space-requirement-height #'space-requirement-min-height #'space-requirement-max-height
				#'space-composer #'space-req-creator
				:equalize-major-size? (layout-equalize-widths? box-pane)
				:equalize-minor-size? (layout-equalize-heights? box-pane)))))


#||
define method do-allocate-space
    (box-pane :: <row-layout>, width :: <integer>, height :: <integer>) => ()
  let space-requirement = compose-space(box-pane, width: width, height: height);
  let children = sheet-children(box-pane);
  let n-children :: <integer> = size(children) - count(sheet-withdrawn?, children);
  let total-spacing = (n-children - 1) * box-pane-major-spacing(box-pane);
  let sizes
    = compose-space-for-items
        (box-pane,
	 width - total-spacing, space-requirement, children,
         space-requirement-width, space-requirement-min-width, space-requirement-max-width,
         method (child)
           compose-space(child, height: height)
         end,
         ratios: layout-x-ratios(box-pane));
  box-pane-allocate-space
    (box-pane, sizes, width, height,
     method (child, #key major, minor)
       let space-req = compose-space(child, width: major, height: minor);
       let (w, w-, w+, h, h-, h+) = space-requirement-components(child, space-req);
       values(constrain-size(major | w, w-, w+),
              constrain-size(minor | h, h-, h+))
     end method,
     layout-align-sheet-y, set-sheet-edges,
     major-size-override: layout-equalize-widths?(box-pane)  & box-pane.%max-major-size,
     minor-size-override: layout-equalize-heights?(box-pane) & box-pane.%max-minor-size)
end method do-allocate-space;
||#

(defmethod do-allocate-space ((box-pane <row-layout>) (width integer) (height integer))
  (let* ((space-requirement (compose-space box-pane :width width :height height))
	 (children (sheet-children box-pane))
	 (n-children (count-if-not #'sheet-withdrawn? children))
	 (total-spacing (* (- n-children 1) (box-pane-major-spacing box-pane)))
	 (sizes (compose-space-for-items box-pane
					 (- width total-spacing) space-requirement children
					 #'space-requirement-width #'space-requirement-min-width #'space-requirement-max-width
					 #'(lambda (child)
					     (compose-space child :height height))
					 :ratios (layout-x-ratios box-pane))))
    (box-pane-allocate-space box-pane sizes width height
			     #'(lambda (child &key major minor)
				 (let ((space-req (compose-space child :width major :height minor)))
				   (multiple-value-bind (w w- w+ h h- h+)
				       (space-requirement-components child space-req)
				     (values (constrain-size (or major w) w- w+)
					     (constrain-size (or minor h) h- h+)))))
			     #'layout-align-sheet-y #'set-sheet-edges
			     :major-size-override (and (layout-equalize-widths?  box-pane) (%max-major-size box-pane))
			     :minor-size-override (and (layout-equalize-heights? box-pane) (%max-minor-size box-pane)))))


#||
// Options can be any of the pane sizing options, plus SPACING: and X-RATIOS:, etc
define macro horizontally
  { horizontally (#rest ?options:expression)
      ?contents:*
    end }
    => { make(<row-layout>, children: vector(?contents), ?options) }
 contents:
  { } => { }
  { ?pane-spec:*; ... } => { ?pane-spec, ... }
 pane-spec:
  //--- It would be nice to have syntax for ratios...
  { ?pane:expression } => { ?pane }
end macro horizontally;
||#

(defmacro horizontally ((&rest options) &body contents)
"
This macro lays a series of gadgets out horizontally, creating the
necessary layouts for you automatically.

The _options_ are passed directly to the row layout, and thus can be
any legitimate combinations of initargs for <ROW-LAYOUT>. If no
options are specified, then the default values for row layout are
used.

The _contents_ argument consists of a number of Lisp expressions, each
of which creates an instance of a gadget or layout that is to be
included in the horizontal layout.

Example:

    (contain (horizontally ()
                 (make-pane '<button> :label \"Hello\")
                 (make-pane '<button> :label \"World\")))
"
  `(make-pane '<row-layout> :children (vector ,@contents) ,@options))


#||
/// Default implementation

define sealed class <row-layout-pane> (<row-layout>)
  keyword accepts-focus?: = #f;
end class <row-layout-pane>;
||#


(defclass <row-layout-pane> (<row-layout>)
  ()
  (:default-initargs :accepts-focus? nil))


#||
define method class-for-make-pane 
    (framem :: <frame-manager>, class == <row-layout>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<row-layout-pane>, #f)
end method class-for-make-pane;

define sealed domain make (singleton(<row-layout-pane>));
define sealed domain initialize (<row-layout-pane>);
||#

(defmethod class-for-make-pane ((framem <frame-manager>) (class (eql (find-class '<row-layout>))) &key)
  (values (find-class '<row-layout-pane>) nil))



#||

/// Column panes, formerly known as vboxes

define open abstract class <column-layout>
    (<vertical-layout-mixin>, <box-layout-pane>)
end class <column-layout>;
||#


(defclass <column-layout>
    (<vertical-layout-mixin>
     <box-layout-pane>)
  ()
  (:documentation
"
The class of column layouts. A column layout arranges its children
in a column, automatically calculating the size and placement of each
child within the specified parameters.

The :border initarg provides a border of whitespace around the
children in the layout, and the value of this initarg represents the
size of the border in pixels. This basically has the same effect as
using the macro WITH-SPACING around the layout, except it uses a
simpler syntax.

The :spacing or :y-spacing initargs let you specify how much vertical
space should be inserted, in pixels, between the children of the
layout. These two initargs can be used interchangeably.

If T, :equalize-heights? ensures that all the children of the layout
have the same height.

If T, :equalize-widths? ensures that all the children of the layout
have the same width.

By default, all the children of a column layout are left-aligned. You
can specify that they should be right or center-aligned using
the :x-alignment initarg.

The :ratios or :y-ratios initargs let you specify the proportion of
the total layout that should be taken up by each individual
child. These two initargs can be used interchangeably.

The value passed to :ratios needs to be a sequence of as many integers
as there are children in the layout. Each child is then allocated the
appropriate portion of vertical space in the layout. For example, if
the value #(1 2 3) is specified for the :ratios initarg of a column
layout containing three children, then the first child would claim a
sixth of the available vertical space, the second child would claim a
third of the vertical space, and the third child would claim half the
vertical space as shown in the diagram below.

  +-----------+
  |  child 1  |
  +-----------+
  |  child 2  |
  |           |
  +-----------+   :ratios #(1 2 3)
  |           |
  |  child 3  |
  |           |
  +---------- +

Example:

    (CONTAIN (MAKE-PANE '<COLUMN-LAYOUT>
                        :children (VECTOR (MAKE-PANE '<BUTTON>
                                                     :label \"Hello\")
                                          (MAKE-PANE '<BUTTON>
                                                     :label \"World\"))
                        :spacing 100
                        :x-alignment :right
                        :ratios #(1 3)))
")
  (:metaclass <abstract-metaclass>))


#||
define method box-pane-major-spacing
    (box :: <column-layout>) => (spacing :: <integer>)
  layout-y-spacing(box)
end method box-pane-major-spacing;
||#

(defmethod box-pane-major-spacing ((box <column-layout>))
  (layout-y-spacing box))


#||
define method do-compose-space
    (box-pane :: <column-layout>, #key width, height)
 => (space-req :: <space-requirement>)
  local method space-composer
	    (child) => (space-req :: <space-requirement>)
	  compose-space(child, width: width)
	end method,
        method space-req-creator
	    (height, min-height, max-height, width, min-width, max-width)
	 => (space-req :: <space-requirement>)
	  make(<space-requirement>,
	       width:  width,  min-width:  min-width,  max-width:  max-width,
	       height: height, min-height: min-height, max-height: max-height)
	end method;
  dynamic-extent(space-composer, space-req-creator);
  if (empty?(sheet-children(box-pane)))
    default-space-requirement(box-pane, width: width, height: height)
  else
    box-pane-compose-space
      (box-pane,
       height,
       space-requirement-height, space-requirement-min-height, space-requirement-max-height,
       width,
       space-requirement-width,  space-requirement-min-width,  space-requirement-max-width,
       space-composer, space-req-creator,
       equalize-major-size?: layout-equalize-heights?(box-pane),
       equalize-minor-size?: layout-equalize-widths?(box-pane))
  end
end method do-compose-space;
||#

(defmethod do-compose-space ((box-pane <column-layout>) &key width height)
  (labels ((space-composer (child)
	     (compose-space child :width width))
	   (space-req-creator (height min-height max-height width min-width max-width)
	     (make-space-requirement :width  width :min-width  min-width :max-width  max-width
				     :height height :min-height min-height :max-height max-height)))
    (declare (dynamic-extent #'space-composer #'space-req-creator))
    (if (empty? (sheet-children box-pane))
	(default-space-requirement box-pane :width width :height height)
	(box-pane-compose-space box-pane
				height
				#'space-requirement-height #'space-requirement-min-height #'space-requirement-max-height
				width
				#'space-requirement-width #'space-requirement-min-width #'space-requirement-max-width
				#'space-composer #'space-req-creator
				:equalize-major-size? (layout-equalize-heights? box-pane)
				:equalize-minor-size? (layout-equalize-widths?  box-pane)))))


#||
define method do-allocate-space
    (box-pane :: <column-layout>, width :: <integer>, height :: <integer>) => ()
  let space-requirement = compose-space(box-pane, width: width, height: height);
  let children = sheet-children(box-pane);
  let n-children :: <integer> = size(children) - count(sheet-withdrawn?, children);
  let total-spacing = (n-children - 1) * box-pane-major-spacing(box-pane);
  let sizes
    = compose-space-for-items
        (box-pane,
	 height - total-spacing, space-requirement, children,
         space-requirement-height, space-requirement-min-height, space-requirement-max-height, 
         method (sheet)
           compose-space(sheet, width: width)
         end,
         ratios: layout-y-ratios(box-pane));
  box-pane-allocate-space
    (box-pane, sizes, height, width,
     method (child, #key major, minor)
       let space-req = compose-space(child, width: major, height: minor);
       let (w, w-, w+, h, h-, h+) = space-requirement-components(child, space-req);
       values(constrain-size(major | h, h-, h+),
	      constrain-size(minor | w, w-, w+))
     end method,
     layout-align-sheet-x,
     method (sheet :: <basic-sheet>,
	     top :: <integer>, left :: <integer>, bottom :: <integer>, right :: <integer>)
       set-sheet-edges(sheet, left, top, right, bottom)
     end,
     major-size-override: layout-equalize-heights?(box-pane) & box-pane.%max-major-size,
     minor-size-override: layout-equalize-widths?(box-pane)  & box-pane.%max-minor-size)
end method do-allocate-space;
||#

(defmethod do-allocate-space ((box-pane <column-layout>) (width integer) (height integer))
  (let* ((space-requirement (compose-space box-pane :width width :height height))
	 (children (sheet-children box-pane))
	 (n-children (count-if-not #'sheet-withdrawn? children))
	 (total-spacing (* (- n-children 1) (box-pane-major-spacing box-pane)))
	 (sizes (compose-space-for-items box-pane
					 (- height total-spacing) space-requirement children
					 #'space-requirement-height #'space-requirement-min-height #'space-requirement-max-height
					 #'(lambda (sheet)
					     (compose-space sheet :width width))
					 :ratios (layout-y-ratios box-pane))))
    (box-pane-allocate-space box-pane sizes height width
			     #'(lambda (child &key major minor)
				 (let ((space-req (compose-space child :width minor :height major)))
				   (multiple-value-bind (w w- w+ h h- h+)
				       (space-requirement-components child space-req)
				     (values (constrain-size (or major h) h- h+)
					     (constrain-size (or minor w) w- w+)))))
			     #'layout-align-sheet-x
			     #'(lambda (sheet top left bottom right)
				 (set-sheet-edges sheet left top right bottom))
			     :major-size-override (and (layout-equalize-heights? box-pane) (%max-major-size box-pane))
			     :minor-size-override (and (layout-equalize-widths? box-pane) (%max-minor-size box-pane)))))


#||
// Options can be any of the pane sizing options, plus SPACING:
define macro vertically
  { vertically (#rest ?options:expression)
      ?contents:*
    end }
    => { make(<column-layout>, children: vector(?contents), ?options) }
 contents:
  { } => { }
  { ?pane-spec:*; ... } => { ?pane-spec, ... }
 pane-spec:
  //--- It would be nice to have syntax for ratios...
  { ?pane:expression } => { ?pane }
end macro vertically;
||#

(defmacro vertically ((&rest options) &body contents)
"
This macro lays a series of gadgets out vertically, creating the
necessary column layout for you automatically.

The _options_ are passed directly to the column layout, and thus can
be any legitimate combinations of initargs for <COLUMN-LAYOUT>. If no
options are specified, then the default values for table layout are
used.

The _contents_ argument consists of a number of Lisp expressions, each
of which creates an instance of a gadget or layout that is to be
included in the vertical layout.

Example:

    (contain (vertically (:border 5 :equalize-widths? T)
               (make-pane '<button> :label \"Hello\")
               (make-pane '<button> :label \"World\")))
"
  `(make-pane '<column-layout> :children (vector ,@contents) ,@options))


#||
/// Default implementation

define sealed class <column-layout-pane> (<column-layout>)
  keyword accepts-focus?: = #f;
end class <column-layout-pane>;
||#


(defclass <column-layout-pane>
    (<column-layout>)
  ()
  (:default-initargs :accepts-focus? nil))


#||
define method class-for-make-pane 
    (framem :: <frame-manager>, class == <column-layout>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<column-layout-pane>, #f)
end method class-for-make-pane;

define sealed domain make (singleton(<column-layout-pane>));
define sealed domain initialize (<column-layout-pane>);
||#


(defmethod class-for-make-pane ((framem <frame-manager>) (class (eql (find-class '<column-layout>))) &key)
  (values (find-class '<column-layout-pane>) nil))



