;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-LAYOUTS-INTERNALS -*-
(in-package #:duim-layouts-internals)

#||
define method print-object
    (space-req :: <null-space-requirement>, stream :: <stream>) => ()
  printing-object (space-req, stream, type?: #t, identity?: #t)
    format(stream, "width 0 height 0")
  end
end method print-object;
||#

(defmethod print-object ((space-req <null-space-requirement>) stream)
  (print-unreadable-object (space-req stream :type t :identity t)
    (format stream "width 0 height 0")))

#||
define method print-object
    (space-req :: <fixed-space-requirement>, stream :: <stream>) => ()
  printing-object (space-req, stream, type?: #t, identity?: #t)
    format(stream, "width %= height %=",
	   space-req.%width, space-req.%height)
  end
end method print-object;
||#

(defmethod print-object ((space-req <fixed-space-requirement>) stream)
  (print-unreadable-object (space-req stream :type t :identity t)
    (format stream "width ~a height ~a"
	    (%width space-req)
	    (%height space-req))))

#||
define method print-object
    (space-req :: <unbounded-space-requirement>, stream :: <stream>) => ()
  printing-object (space-req, stream, type?: #t, identity?: #t)
    format(stream, "width %=-0+$fill height %=-0+$fill",
	   space-req.%width, space-req.%height)
  end
end method print-object;
||#

(defmethod print-object ((space-req <unbounded-space-requirement>) stream)
  (print-unreadable-object (space-req stream :type t :identity t)
    (format stream "width ~a-0+$fill height ~a-0+$fill"
	    (%width space-req)
	    (%height space-req))))

#||
define method print-object
    (space-req :: <general-space-requirement>, stream :: <stream>) => ()
  printing-object (space-req, stream, type?: #t, identity?: #t)
    format(stream, "width %=-%=+%= height %=-%=+%=",
	   space-req.%width,  space-req.%min-width,  space-req.%max-width,
	   space-req.%height, space-req.%min-height, space-req.%max-height)
  end
end method print-object;
||#

(defmethod print-object ((space-req <general-space-requirement>) stream)
  (print-unreadable-object (space-req stream :type t :identity t)
    (format stream "width ~a-~a+~a height ~a-~a+~a"
	    (%width space-req)
	    (%min-width space-req)
	    (%max-width space-req)
	    (%height space-req)
	    (%min-height space-req)
	    (%max-height space-req))))

#||
define method print-object
    (space-req :: <label-space-requirement>, stream :: <stream>) => ()
  printing-object (space-req, stream, type?: #t, identity?: #t)
    format(stream, "label %=", space-req.%label)
  end
end method print-object;
||#

(defmethod print-object ((space-req <label-space-requirement>) stream)
  (print-unreadable-object (space-req stream :type t :identity t)
    (format stream "label ~a" (%label space-req))))



