;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-LAYOUTS-INTERNALS -*-
(in-package #:duim-layouts-internals)

#||
/// Space requirements

define protocol-class space-requirement (<object>) end;
||#

(define-protocol-class space-requirement () ()
  (:documentation
"
The class of all space requirement objects. This type of object is
used to reserve space when it is required in a layout in order to
accommodate gadgets or other layouts.

The various initargs let you constrain the width and height of the
object in a variety of ways.

If no initargs are specified, the object returned tries to fill all
the available space.

Specifying :width or :height specifies the preferred width or height
of the object.

Specifying any of the :min- or :max- initargs lets you specify minimum
and maximum width or height for the object.

The following inequalities hold for all widths and heights:

    :min-height <= :height <= :max-height
    :min-width <= :width  <= :max-width

If either :min-width or :min-height is 0, the object is \"infinitely
shrinkable\" in that direction. If either :max-width or :max-height is
+FILL+, the object is \"infinitely stretchable\" in that
direction. The latter is a particularly useful way of ensuring that
objects fill the available width, and can be used, say, to ensure that
a series of buttons fill the entire width of the layout that they
occupy.

An example of the use of :max-width to force the size of a button to
fit the available space can be found in the entry for <BUTTON>.

The :label initarg specifies a label which is measured to give the
preferred with and height.

Example:

    Given the following definition of a button class:

        (defclass <basic-test-button> (<leaf-pane>))

    The following method for DO-COMPOSE-SPACE creates the necessary
    space requirements to accommodate the new button class in a
    layout.

        (defmethod do-compose-space ((pane <basic-test-button>)
                                     &key width height)
          (declare (ignore width height))
          (make-instance '<space-requirement>
                         :width 40
                         :height 15))
"))

;;; TODO: Documentation for <space-requirement> protocol predicate

#||
Returns T if _object_ is an instance of <SPACE-REQUIREMENT>
||#

#||
define protocol <<space-requirement>> ()
  function space-requirement-width
    (sheet :: <abstract-sheet>, space-req :: <space-requirement>)
 => (width :: false-or(<integer>));
  function space-requirement-min-width
    (sheet :: <abstract-sheet>, space-req :: <space-requirement>)
 => (min-width :: false-or(<integer>));
  function space-requirement-max-width
    (sheet :: <abstract-sheet>, space-req :: <space-requirement>)
 => (max-width :: false-or(<integer>));
  function space-requirement-height
    (sheet :: <abstract-sheet>, space-req :: <space-requirement>)
 => (height :: false-or(<integer>));
  function space-requirement-min-height
    (sheet :: <abstract-sheet>, space-req :: <space-requirement>)
 => (min-height :: false-or(<integer>));
  function space-requirement-max-height
    (sheet :: <abstract-sheet>, space-req :: <space-requirement>)
 => (max-height :: false-or(<integer>));
  function space-requirement-components
    (sheet :: <abstract-sheet>, space-req :: <space-requirement>)
 => (width      :: false-or(<integer>), 
     min-width  :: false-or(<integer>), 
     max-width  :: false-or(<integer>),
     height     :: false-or(<integer>),
     min-height :: false-or(<integer>),
     max-height :: false-or(<integer>));
end protocol <<space-requirement>>;
||#

(define-protocol <<space-requirement>> ()
  (:function space-requirement-width (sheet space-req)
	     (:documentation
"
Returns the preferred width of _space-req_. This is the value of
the :width initarg that was passed when the object was created.

Defined by: <<SPACE-REQUIREMENT>>
"))
  (:function space-requirement-min-width (sheet space-req)
	     (:documentation
"
Returns the mimimum allowed width of _space-req_. This is the value of
the :min-width initarg that was passed when the object was created.

Defined by: <<SPACE-REQUIREMENT>>
"))
  (:function space-requirement-max-width (sheet space-req)
	     (:documentation
"
Returns the maximum allowed with of _space-req_. This is the value of
the :max-width initarg that was passed when the object was created.

Defined by: <<SPACE-REQUIREMENT>>
"))
  (:function space-requirement-height (sheet space-req)
	     (:documentation
"
Returns the preferred height of _space-req_. THis is the value of
the :height initarg that was passed when the object was created.

Defined by: <<SPACE-REQUIREMENT>>
"))
  (:function space-requirement-min-height (sheet space-req)
	     (:documentation
"
Returns the minimum allowed height of _space-req_. This is the value
of the :min-height initarg that was passed when the object was
created.

Defined by: <<SPACE-REQUIREMENT>>
"))
  (:function space-requirement-max-height (sheet space-req)
	     (:documentation
"
Returns the maximum allowed height of _space-req_. This is the value
of the :max-height initarg that was passed when the object was
created.

Defined by: <<SPACE-REQUIREMENT>>
"))
  (:function space-requirement-components (sheet space-req) (:documentation " Defined by: <<SPACE-REQUIREMENT>> ")))

(defgeneric space-requirement-combine (sheet function sr1 sr2))
(defgeneric space-requirement+* (sheet sr1 sr2))
(defgeneric space-requirement+ (sheet space-req &key width min-width max-width height min-height max-height))


#||
// This is the size we use for a "fill" component, the idea being that
// we should be able sum up at least 100 $fill's without overflowing
// into bignums (that is, fit into the 28 bits required by Dylan)
// 'floor(10 ^ floor(logn($maximum-integer, 10)) / 100)'
define constant $fill :: <integer> = 100000;
||#

(defconstant +fill+ 100000
"
This constant is used as the default value for any :width and :height
initargs in layout panes.

These defaults give the intuitive behaviour that specifying only the
width or height of a pane causes it to be allocated at least that much
space, and it may be given extra space if there is extra space in the
layout. This default behaviour can be changed if either the :min-width
or :min-height initargs are specified explicitly.
")



#||

/// Simple space requirement classes

define sealed class <null-space-requirement> (<space-requirement>)
end class <null-space-requirement>;
||#

(defclass <null-space-requirement> (<space-requirement>) ())


#||
define sealed method space-requirement-components
    (sheet :: <sheet>, space-req :: <null-space-requirement>)
 => (width      :: <integer>, 
     min-width  :: <integer>, 
     max-width  :: <integer>,
     height     :: <integer>,
     min-height :: <integer>,
     max-height :: <integer>)
  values(0, 0, 0, 0, 0, 0)
end method space-requirement-components;
||#

(defmethod space-requirement-components ((sheet <sheet>) (space-req <null-space-requirement>))
  (values 0 0 0 0 0 0))


#||
define sealed method space-requirement-width
    (sheet :: <sheet>, sr :: <null-space-requirement>) => (width :: <integer>)
  0
end method space-requirement-width;
||#

(defmethod space-requirement-width ((sheet <sheet>) (sr <null-space-requirement>))
  0)


#||
define sealed method space-requirement-min-width
    (sheet :: <sheet>, sr :: <null-space-requirement>) => (min-width :: <integer>)
  0
end method space-requirement-min-width;
||#

(defmethod space-requirement-min-width ((sheet <sheet>) (sr <null-space-requirement>))
  0)


#||
define sealed method space-requirement-max-width 
    (sheet :: <sheet>, sr :: <null-space-requirement>) => (max-width :: <integer>)
  0
end method space-requirement-max-width;
||#

(defmethod space-requirement-max-width ((sheet <sheet>) (sr <null-space-requirement>))
  0)


#||
define sealed method space-requirement-height
    (sheet :: <sheet>, sr :: <null-space-requirement>) => (height :: <integer>)
  0
end method space-requirement-height;
||#

(defmethod space-requirement-height ((sheet <sheet>) (sr <null-space-requirement>))
  0)


#||
define sealed method space-requirement-min-height
    (sheet :: <sheet>, sr :: <null-space-requirement>) => (min-height :: <integer>)
  0
end method space-requirement-min-height;
||#

(defmethod space-requirement-min-height ((sheet <sheet>) (sr <null-space-requirement>))
  0)


#||
define sealed method space-requirement-max-height
    (sheet :: <sheet>, sr :: <null-space-requirement>) => (max-height :: <integer>)
  0
end method space-requirement-max-height;
||#

(defmethod space-requirement-max-height ((sheet <sheet>) (sr <null-space-requirement>))
  0)


#||
define variable $null-space-requirement :: <null-space-requirement>
    = make(<null-space-requirement>);
||#

(defparameter *null-space-requirement* (make-instance '<null-space-requirement>))


#||
define sealed class <fixed-space-requirement> (<space-requirement>)
  sealed constant slot %width :: false-or(<integer>),
    required-init-keyword: width:;
  sealed constant slot %height :: false-or(<integer>),
    required-init-keyword: height:;
end class <fixed-space-requirement>;
||#

(defclass <fixed-space-requirement> (<space-requirement>)
  ((%width :type (or null integer) :initarg :width :initform (required-slot ":width" "<fixed-space-requiremenet>") :reader %width)
   (%height :type (or null integer) :initarg :height :initform (required-slot ":height" "<fixed-space-requirement>") :reader %height)))


#||
define sealed method space-requirement-components
    (sheet :: <sheet>, space-req :: <fixed-space-requirement>)
 => (width      :: false-or(<integer>), 
     min-width  :: false-or(<integer>), 
     max-width  :: false-or(<integer>),
     height     :: false-or(<integer>),
     min-height :: false-or(<integer>),
     max-height :: false-or(<integer>))
  let width  = space-req.%width;
  let height = space-req.%height;
  values(width, width, width, height, height, height)
end method space-requirement-components;
||#

(defmethod space-requirement-components ((sheet <sheet>) (space-req <fixed-space-requirement>))
  (let ((width (%width space-req))
	(height (%height space-req)))
    (values width width width height height height)))


#||
define sealed method space-requirement-width 
    (sheet :: <sheet>, sr :: <fixed-space-requirement>) => (width :: false-or(<integer>))
  sr.%width
end method space-requirement-width;
||#

(defmethod space-requirement-width ((sheet <sheet>) (sr <fixed-space-requirement>))
  (%width sr))


#||
define sealed method space-requirement-min-width 
    (sheet :: <sheet>, sr :: <fixed-space-requirement>) => (min-width :: false-or(<integer>))
  sr.%width
end method space-requirement-min-width;
||#

(defmethod space-requirement-min-width ((sheet <sheet>) (sr <fixed-space-requirement>))
  (%width sr))


#||
define sealed method space-requirement-max-width 
    (sheet :: <sheet>, sr :: <fixed-space-requirement>) => (max-width :: false-or(<integer>))
  sr.%width
end method space-requirement-max-width;
||#

(defmethod space-requirement-max-width ((sheet <sheet>) (sr <fixed-space-requirement>))
  (%width sr))


#||
define sealed method space-requirement-height 
    (sheet :: <sheet>, sr :: <fixed-space-requirement>) => (height :: false-or(<integer>))
  sr.%height
end method space-requirement-height;
||#

(defmethod space-requirement-height ((sheet <sheet>) (sr <fixed-space-requirement>))
  (%height sr))


#||
define sealed method space-requirement-min-height 
    (sheet :: <sheet>, sr :: <fixed-space-requirement>) => (min-height :: false-or(<integer>))
  sr.%height
end method space-requirement-min-height;
||#

(defmethod space-requirement-min-height ((sheet <sheet>) (sr <fixed-space-requirement>))
  (%height sr))


#||
define sealed method space-requirement-max-height 
    (sheet :: <sheet>, sr :: <fixed-space-requirement>) => (max-height :: false-or(<integer>))
  sr.%height
end method space-requirement-max-height;
||#

(defmethod space-requirement-max-height ((sheet <sheet>) (sr <fixed-space-requirement>))
  (%height sr))


#||
define sealed class <unbounded-space-requirement> (<space-requirement>)
  sealed constant slot %width :: false-or(<integer>),
    required-init-keyword: width:;
  sealed constant slot %height :: false-or(<integer>),
    required-init-keyword: height:;
end class <unbounded-space-requirement>;
||#

(defclass <unbounded-space-requirement> (<space-requirement>)
  ((%width :type (or null integer) :initarg :width :initform (required-slot ":width" "<unbounded-space-requirement>") :reader %width)
   (%height :type (or null integer) :initarg :height :initform (required-slot ":height" "<unbounded-space-requirement>") :reader %height)))


#||
define sealed method space-requirement-components
    (sheet :: <sheet>, space-req :: <unbounded-space-requirement>)
 => (width      :: false-or(<integer>), 
     min-width  :: <integer>, 
     max-width  :: <integer>,
     height     :: false-or(<integer>),
     min-height :: <integer>,
     max-height :: <integer>)
  values(space-req.%width,  0, $fill,
	 space-req.%height, 0, $fill)
end method space-requirement-components;
||#


(defmethod space-requirement-components ((sheet <sheet>) (space-req <unbounded-space-requirement>))
  (values (%width space-req)  0 +fill+
	  (%height space-req) 0 +fill+))


#||
define sealed method space-requirement-width 
    (sheet :: <sheet>, sr :: <unbounded-space-requirement>) => (width :: false-or(<integer>))
  sr.%width
end method space-requirement-width;
||#

(defmethod space-requirement-width ((sheet <sheet>) (sr <unbounded-space-requirement>))
  (%width sr))


#||
define sealed method space-requirement-min-width 
    (sheet :: <sheet>, sr :: <unbounded-space-requirement>) => (min-width :: <integer>)
  0
end method space-requirement-min-width;
||#

(defmethod space-requirement-min-width ((sheet <sheet>) (sr <unbounded-space-requirement>))
  0)


#||
define sealed method space-requirement-max-width 
    (sheet :: <sheet>, sr :: <unbounded-space-requirement>) => (max-width :: <integer>)
  $fill
end method space-requirement-max-width;
||#

(defmethod space-requirement-max-width ((sheet <sheet>) (sr <unbounded-space-requirement>))
  +fill+)


#||
define sealed method space-requirement-height 
    (sheet :: <sheet>, sr :: <unbounded-space-requirement>) => (height :: false-or(<integer>))
  sr.%height
end method space-requirement-height;
||#

(defmethod space-requirement-height ((sheet <sheet>) (sr <unbounded-space-requirement>))
  (%height sr))


#||
define sealed method space-requirement-min-height 
    (sheet :: <sheet>, sr :: <unbounded-space-requirement>) => (min-height :: <integer>)
  0
end method space-requirement-min-height;
||#

(defmethod space-requirement-min-height ((sheet <sheet>) (sr <unbounded-space-requirement>))
  0)


#||
define sealed method space-requirement-max-height 
    (sheet :: <sheet>, sr :: <unbounded-space-requirement>) => (max-height :: <integer>)
  $fill
end method space-requirement-max-height;
||#

(defmethod space-requirement-max-height ((sheet <sheet>) (sr <unbounded-space-requirement>))
  +fill+)


#||
define sealed class <general-space-requirement> (<space-requirement>)
  sealed constant slot %width :: false-or(<integer>),
    required-init-keyword: width:;
  sealed constant slot %min-width :: false-or(<integer>),
    required-init-keyword: min-width:;
  sealed constant slot %max-width :: false-or(<integer>),
    required-init-keyword: max-width:;
  sealed constant slot %height :: false-or(<integer>),
    required-init-keyword: height:;
  sealed constant slot %min-height :: false-or(<integer>),
    required-init-keyword: min-height:;
  sealed constant slot %max-height :: false-or(<integer>),
    required-init-keyword: max-height:;
end class <general-space-requirement>;
||#

(defclass <general-space-requirement> (<space-requirement>)
  ((%width :type (or null integer) :initarg :width :initform (required-slot ":width" "<general-space-requirement>") :reader %width)
   (%min-width :type (or null integer) :initarg :min-width :initform (required-slot ":min-width" "<general-space-requirement>") :reader %min-width)
   (%max-width :type (or null integer) :initarg :max-width :initform (required-slot ":max-width" "<general-space-requirement>") :reader %max-width)
   (%height :type (or null integer) :initarg :height :initform (required-slot ":height" "<general-space-requirement>") :reader %height)
   (%min-height :type (or null integer) :initarg :min-height :initform (required-slot ":min-height" "<general-space-requirement>") :reader %min-height)
   (%max-height :type (or null integer) :initarg :max-height :initform (required-slot ":max-height" "<general-space-requirement>") :reader %max-height)))


#||
define sealed method space-requirement-components
    (sheet :: <sheet>, space-req :: <general-space-requirement>)
 => (width      :: false-or(<integer>), 
     min-width  :: false-or(<integer>), 
     max-width  :: false-or(<integer>),
     height     :: false-or(<integer>),
     min-height :: false-or(<integer>),
     max-height :: false-or(<integer>))
  values(space-req.%width,  space-req.%min-width,  space-req.%max-width,
	 space-req.%height, space-req.%min-height, space-req.%max-height)
end method space-requirement-components;
||#

(defmethod space-requirement-components ((sheet <sheet>) (space-req <general-space-requirement>))
  (values (%width space-req) (%min-width space-req) (%max-width space-req)
	  (%height space-req) (%min-height space-req) (%max-height space-req)))


#||
define sealed method space-requirement-width 
    (sheet :: <sheet>, sr :: <general-space-requirement>) => (width :: false-or(<integer>))
  sr.%width
end method space-requirement-width;
||#

(defmethod space-requirement-width ((sheet <sheet>) (sr <general-space-requirement>))
  (%width sr))


#||
define sealed method space-requirement-min-width 
    (sheet :: <sheet>, sr :: <general-space-requirement>) => (min-width :: false-or(<integer>))
  sr.%min-width
end method space-requirement-min-width;
||#

(defmethod space-requirement-min-width ((sheet <sheet>) (sr <general-space-requirement>))
  (%min-width sr))


#||
define sealed method space-requirement-max-width 
    (sheet :: <sheet>, sr :: <general-space-requirement>) => (max-width :: false-or(<integer>))
  sr.%max-width
end method space-requirement-max-width;
||#

(defmethod space-requirement-max-width ((sheet <sheet>) (sr <general-space-requirement>))
  (%max-width sr))


#||
define sealed method space-requirement-height 
    (sheet :: <sheet>, sr :: <general-space-requirement>) => (height :: false-or(<integer>))
  sr.%height
end method space-requirement-height;
||#

(defmethod space-requirement-height ((sheet <sheet>) (sr <general-space-requirement>))
  (%height sr))


#||
define sealed method space-requirement-min-height 
    (sheet :: <sheet>, sr :: <general-space-requirement>) => (min-height :: false-or(<integer>))
  sr.%min-height
end method space-requirement-min-height;
||#

(defmethod space-requirement-min-height ((sheet <sheet>) (sr <general-space-requirement>))
  (%min-height sr))


#||
define sealed method space-requirement-max-height 
    (sheet :: <sheet>, sr :: <general-space-requirement>) => (max-height :: false-or(<integer>))
  sr.%max-height
end method space-requirement-max-height;
||#

(defmethod space-requirement-max-height ((sheet <sheet>) (sr <general-space-requirement>))
  (%max-height sr))



#||

/// Complex space requirement classes

// A space requirement that allocates enough room to hold the given label
define sealed class <label-space-requirement> (<space-requirement>)
  sealed constant slot %label :: type-union(<string>, <image>),
    required-init-keyword: label:;
  sealed constant slot %min-width :: false-or(<integer>) = #f,
    init-keyword: min-width:;
  sealed constant slot %max-width :: false-or(<integer>) = #f,
    init-keyword: max-width:;
  sealed constant slot %min-height :: false-or(<integer>) = #f,
    init-keyword: min-height:;
  sealed constant slot %max-height :: false-or(<integer>) = #f,
    init-keyword: max-height:;
end class <label-space-requirement>;
||#

;; A space requirement that allocates enough room to hold the given label
(defclass <label-space-requirement> (<space-requirement>)
  ((%label :type (or string <image>) :initarg :label :initform (required-slot ":label" "<label-space-requirement>") :reader %label)
   (%min-width :type (or null integer) :initarg :min-width :initform nil :reader %min-width)
   (%max-width :type (or null integer) :initarg :max-width :initform nil :reader %max-width)
   (%min-height :type (or null integer) :initarg :min-height :initform nil :reader %min-height)
   (%max-height :type (or null integer) :initarg :max-height :initform nil :reader %max-height))
  (:documentation
"
A space requirement that allocates enough room to hold the given label.
"))

(defgeneric space-requirement-label-size (sheet label-space-requirement))


#||
define sealed method space-requirement-components
    (sheet :: <sheet>, space-req :: <label-space-requirement>)
 => (width      :: <integer>,
     min-width  :: <integer>, 
     max-width  :: <integer>,
     height     :: <integer>,
     min-height :: <integer>,
     max-height :: <integer>)
  let (width, height) = space-requirement-label-size(sheet, space-req);
  values(width,  space-req.%min-width  | width,  space-req.%max-width  | width,
	 height, space-req.%min-height | height, space-req.%max-height | height)
end method space-requirement-components;
||#

(defmethod space-requirement-components ((sheet <sheet>) (space-req <label-space-requirement>))
  (multiple-value-bind (width height)
      (space-requirement-label-size sheet space-req)
    (values width (or (%min-width space-req) width) (or (%max-width space-req) width)
	    height (or (%min-height space-req) height) (or (%max-height space-req) height))))


#||
define method space-requirement-label-size
    (sheet :: <sheet>, sr :: <label-space-requirement>)
 => (width :: <integer>, height :: <integer>)
  let label = sr.%label;
  select (label by instance?)
    <string> =>
      let _port = port(sheet);
      let text-style = get-default-text-style(_port, sheet);
      // Use the computed width of the label, but the height of the font
      // Add a few pixels in each direction to keep the label from being squeezed
      values(ceiling(text-size(_port, label,	//--- what about tabs and newlines?
			       text-style: text-style)) + 2,
	     ceiling(font-height(text-style, _port)) + 2);
    <image> =>
      values(image-width(label), image-height(label));
  end
end method space-requirement-label-size;
||#

(defmethod space-requirement-label-size ((sheet <sheet>) (sr <label-space-requirement>))
  ;; returns width, height
  (let ((label (%label sr)))
    (etypecase label
      (string
       (let* ((_port (port sheet))
	      (text-style (get-default-text-style _port sheet)))
	 ;; Use the computed width of the label, but the height of the font
	 ;; Add a few pixels in each direction to keep the label from being squeezed
	 (values (+ (ceiling (text-size _port label	;;--- what about tabs and newlines?
					:text-style text-style))
		    2)
		 (+ (ceiling (font-height text-style
					  _port))
		    2))))
      (<image>
       (values (image-width label) (image-height label))))))


#||
define sealed method space-requirement-width
    (sheet :: <sheet>, sr :: <label-space-requirement>) => (width :: <integer>)
  let (width, height) = space-requirement-label-size(sheet, sr);
  ignore(height);
  width
end method space-requirement-width;
||#

(defmethod space-requirement-width ((sheet <sheet>) (sr <label-space-requirement>))
  (multiple-value-bind (width height)
      (space-requirement-label-size sheet sr)
    (declare (ignore height))
    width))


#||
define sealed method space-requirement-min-width 
    (sheet :: <sheet>, sr :: <label-space-requirement>) => (min-width :: <integer>)
  sr.%min-width
  | begin
      let (width, height) = space-requirement-label-size(sheet, sr);
      ignore(height);
      width
    end
end method space-requirement-min-width;
||#

(defmethod space-requirement-min-width ((sheet <sheet>) (sr <label-space-requirement>))
  (or (%min-width sr)
      (multiple-value-bind (width height)
	  (space-requirement-label-size sheet sr)
	(declare (ignore height))
	width)))


#||
define sealed method space-requirement-max-width 
    (sheet :: <sheet>, sr :: <label-space-requirement>) => (max-width :: <integer>)
  sr.%max-width
  | begin
      let (width, height) = space-requirement-label-size(sheet, sr);
      ignore(height);
      width
    end
end method space-requirement-max-width;
||#

(defmethod space-requirement-max-width ((sheet <sheet>) (sr <label-space-requirement>))
  (or (%max-width sr)
      (multiple-value-bind (width height)
	  (space-requirement-label-size sheet sr)
	(declare (ignore height))
	width)))


#||
define sealed method space-requirement-height 
    (sheet :: <sheet>, sr :: <label-space-requirement>) => (height :: <integer>)
  let (width, height) = space-requirement-label-size(sheet, sr);
  ignore(width);
  height
end method space-requirement-height;
||#

(defmethod space-requirement-height ((sheet <sheet>) (sr <label-space-requirement>))
  (multiple-value-bind (width height)
      (space-requirement-label-size sheet sr)
    (declare (ignore width))
    height))


#||
define sealed method space-requirement-min-height 
    (sheet :: <sheet>, sr :: <label-space-requirement>) => (min-height :: <integer>)
  sr.%min-height
  | begin
      let (width, height) = space-requirement-label-size(sheet, sr);
      ignore(width);
      height
    end
end method space-requirement-min-height;
||#

(defmethod space-requirement-min-height ((sheet <sheet>) (sr <label-space-requirement>))
  (or (%min-height sr)
      (multiple-value-bind (width height)
	  (space-requirement-label-size sheet sr)
	(declare (ignore width))
	height)))


#||
define sealed method space-requirement-max-height 
    (sheet :: <sheet>, sr :: <label-space-requirement>) => (max-height :: <integer>)
  sr.%max-height
  | begin
      let (width, height) = space-requirement-label-size(sheet, sr);
      ignore(width);
      height
    end
end method space-requirement-max-height;
||#

(defmethod space-requirement-max-height ((sheet <sheet>) (sr <label-space-requirement>))
  (or (%max-height sr)
      (multiple-value-bind (width height)
	  (space-requirement-label-size sheet sr)
	(declare (ignore width))
	height)))


#||
// A space requirement that calls a function to get the requirements
//--- Maybe we should have a "one-shot" functional space requirement
//--- that only computes its value once and caches the result?
define sealed class <functional-space-requirement> (<space-requirement>)
  sealed constant slot %function :: <function>,
    required-init-keyword: function:;
end class <functional-space-requirement>;
||#

;; A space requirement that calls a function to get the requirements
;;--- Maybe we should have a "one-shot" functional space requirement
;;--- that only computes its value once and caches the result?
(defclass <functional-space-requirement> (<space-requirement>)
  ((%function :type function :initarg :function :initform (required-slot ":function" "<functional-space-requirement>") :reader %function))
  (:documentation
"
A space requirement that calls a function to get the requirements.
"))


#||
define sealed method space-requirement-components
    (sheet :: <sheet>, space-req :: <functional-space-requirement>)
 => (width      :: <integer>,
     min-width  :: <integer>, 
     max-width  :: <integer>,
     height     :: <integer>,
     min-height :: <integer>,
     max-height :: <integer>)
  space-req.%function(sheet)
end method space-requirement-components;
||#

(defmethod space-requirement-components ((sheet <sheet>) (space-req <functional-space-requirement>))
  (funcall (%function space-req) sheet))


#||
define sealed method space-requirement-width
    (sheet :: <sheet>, sr :: <functional-space-requirement>) => (width :: <integer>)
  let (w, w-, w+, h, h-, h+) = sr.%function(sheet);
  ignore(w-, w+, h, h-, h+);
  w
end method space-requirement-width;
||#

(defmethod space-requirement-width ((sheet <sheet>) (sr <functional-space-requirement>))
  (multiple-value-bind (w w- w+ h h- h+)
      (funcall (%function sr) sheet)
    (declare (ignore w- w+ h h- h+))
    w))


#||
define sealed method space-requirement-min-width 
    (sheet :: <sheet>, sr :: <functional-space-requirement>) => (min-width :: <integer>)
  let (w, w-, w+, h, h-, h+) = sr.%function(sheet);
  ignore(w, w+, h, h-, h+);
  w-
end method space-requirement-min-width;
||#

(defmethod space-requirement-min-width ((sheet <sheet>) (sr <functional-space-requirement>))
  (multiple-value-bind (w w- w+ h h- h+)
      (funcall (%function sr) sheet)
    (declare (ignore w w+ h h- h+))
    w-))


#||
define sealed method space-requirement-max-width 
    (sheet :: <sheet>, sr :: <functional-space-requirement>) => (max-width :: <integer>)
  let (w, w-, w+, h, h-, h+) = sr.%function(sheet);
  ignore(w, w-, h, h-, h+);
  w+
end method space-requirement-max-width;
||#

(defmethod space-requirement-max-width ((sheet <sheet>) (sr <functional-space-requirement>))
  (multiple-value-bind (w w- w+ h h- h+)
      (funcall (%function sr) sheet)
    (declare (ignore w w- h h- h+))
    w+))


#||
define sealed method space-requirement-height 
    (sheet :: <sheet>, sr :: <functional-space-requirement>) => (height :: <integer>)
  let (w, w-, w+, h, h-, h+) = sr.%function(sheet);
  ignore(w, w-, w+, h-, h+);
  h
end method space-requirement-height;
||#

(defmethod space-requirement-height ((sheet <sheet>) (sr <functional-space-requirement>))
  (multiple-value-bind (w w- w+ h h- h+)
      (funcall (%function sr) sheet)
    (declare (ignore w w- w+ h- h+))
    h))


#||
define sealed method space-requirement-min-height 
    (sheet :: <sheet>, sr :: <functional-space-requirement>) => (min-height :: <integer>)
  let (w, w-, w+, h, h-, h+) = sr.%function(sheet);
  ignore(w, w-, w+, h, h+);
  h-
end method space-requirement-min-height;
||#

(defmethod space-requirement-min-height ((sheet <sheet>) (sr <functional-space-requirement>))
  (multiple-value-bind (w w- w+ h h- h+)
      (funcall (%function sr) sheet)
    (declare (ignore w w- w+ h h+))
    h-))


#||
define sealed method space-requirement-max-height 
    (sheet :: <sheet>, sr :: <functional-space-requirement>) => (max-height :: <integer>)
  let (w, w-, w+, h, h-, h+) = sr.%function(sheet);
  ignore(w, w-, w+, h, h-);
  h+
end method space-requirement-max-height;
||#

(defmethod space-requirement-max-height ((sheet <sheet>) (sr <functional-space-requirement>))
  (multiple-value-bind (w w- w+ h h- h+)
      (funcall (%function sr) sheet)
    (declare (ignore w w- w+ h h-))
    h+))



#||

/// Space requirement constructors

define sealed method make
    (class == <space-requirement>,
     #rest initargs,
     #key label, function,
	  width  = 0, min-width  = width,  max-width  = width,
          height = 0, min-height = height, max-height = height)
 => (space-req :: <space-requirement>)
  case
    label =>
      apply(make, <label-space-requirement>,
	    label: label, initargs);
    function =>
      apply(make, <functional-space-requirement>,
	    function: function, initargs);
    width    == min-width
    & width  == max-width
    & height == min-height
    & height == max-height =>
      // Compare with '==' to avoid blowout if width or height is #"compute"
      if (width == 0 & height == 0)
        $null-space-requirement
      else
        make(<fixed-space-requirement>,
             width: width, height: height)
      end;
    min-width    == 0
    & max-width  >= $fill
    & min-height == 0
    & max-height >= $fill =>
      make(<unbounded-space-requirement>,
	   width: width, height: height);
    otherwise =>
      make(<general-space-requirement>,
	   width:  width,  min-width:  min-width,  max-width:  max-width,
	   height: height, min-height: min-height, max-height: max-height);
  end
end method make;

// Seal the constructors and initializers for all space requirements
define sealed domain make (subclass(<space-requirement>));
define sealed domain initialize (<space-requirement>);
||#

;;; Space requirement constructors

(defmethod make-space-requirement (&rest initargs
				   &key
				     label function
				     (width 0) (min-width width) (max-width width)
				     (height 0) (min-height height) (max-height height))
  (cond (label (apply #'make-instance
		      '<label-space-requirement>
		      :label label
		      initargs))
	(function (apply #'make-instance
			 '<functional-space-requirement>
			 :function function
			 initargs))
	((and (eql width min-width)
	      (eql width max-width)
	      (eql height min-height)
	      (eql height max-height))
	 ;; XXX: does DUIM do anything with :compute? Where's it supposed
	 ;; to be used, and how is it supposed to work?
	 ;; Compare with '==' to avoid blowout if width or height is #"compute"
	 (if (and (eql width 0) (eql height 0))
	     *null-space-requirement*
	     (make-instance '<fixed-space-requirement>
			    :width width
			    :height height)))
	((and (eql min-width 0)
	      (>= max-width +fill+)
	      (eql min-height 0)
	      (>= max-height +fill+))
	 (make-instance '<unbounded-space-requirement> :width width :height height))
	(t
	 (make-instance '<general-space-requirement>
                        :width width :min-width min-width :max-width max-width
                        :height height :min-height min-height :max-height max-height))))



#||

/// Space requirements arithmetic

define method space-requirement-combine
    (sheet :: <sheet>, function :: <function>,
     sr1 :: <space-requirement>, sr2 :: <space-requirement>)
 => (space-req :: <space-requirement>)
  let (w1, w1-, w1+, h1, h1-, h1+) = space-requirement-components(sheet, sr1);
  let (w2, w2-, w2+, h2, h2-, h2+) = space-requirement-components(sheet, sr2);
  make(<space-requirement>,
       width:      function(w1,  w2),
       min-width:  function(w1-, w2-),
       max-width:  function(w1+, w2+),
       height:     function(h1,  h2),
       min-height: function(h1-, h2-),
       max-height: function(h1+, h2+))
end method space-requirement-combine;
||#

(defmethod space-requirement-combine ((sheet <sheet>) (function function)
				      (sr1 <space-requirement>) (sr2 <space-requirement>))
  (multiple-value-bind (w1 w1- w1+ h1 h1- h1+)
      (space-requirement-components sheet sr1)
    (multiple-value-bind (w2 w2- w2+ h2 h2- h2+)
	(space-requirement-components sheet sr2)
      (make-space-requirement :width      (funcall function w1  w2)
			      :min-width  (funcall function w1- w2-)
			      :max-width  (funcall function w1+ w2+)
			      :height     (funcall function h1  h2)
			      :min-height (funcall function h1- h2-)
			      :max-height (funcall function h1+ h2+)))))


#||
// Add two space requirements
define inline function space-requirement+*
    (sheet :: <sheet>, sr1 :: <space-requirement>, sr2 :: <space-requirement>)
 => (space-req :: <space-requirement>)
  space-requirement-combine(sheet, \+, sr1, sr2)
end function space-requirement+*;
||#

;; Add two space requirements
(defmethod space-requirement+* ((sheet <sheet>)
				(sr1 <space-requirement>)
				(sr2 <space-requirement>))
  (space-requirement-combine sheet #'+ sr1 sr2))


#||
// The "spread" version of the above...
define method space-requirement+
    (sheet :: <sheet>, space-req :: <space-requirement>,
     #key width      :: <integer> = 0,
     	  min-width  :: <integer> = width,
	  max-width  :: <integer> = width,
          height     :: <integer> = 0,
	  min-height :: <integer> = height,
	  max-height :: <integer> = height)
 => (space-req :: <space-requirement>)
  let (w :: <integer>, w- :: <integer>, w+ :: <integer>,
       h :: <integer>, h- :: <integer>, h+ :: <integer>)
    = space-requirement-components(sheet, space-req);
  make(<space-requirement>,
       width:      w  + width,
       min-width:  w- + min-width,
       max-width:  w+ + max-width,
       height:     h  + height,
       min-height: h- + min-height,
       max-height: h+ + max-height)
end method space-requirement+;
||#

;; The "spread" version of the above
(defmethod space-requirement+ ((sheet <sheet>) (space-req <space-requirement>)
			       &key (width 0) (min-width width) (max-width width)
				 (height 0) (min-height height) (max-height height))
  (multiple-value-bind (w w- w+ h h- h+)
      (space-requirement-components sheet space-req)
    (make-space-requirement :width      (+ w  width)
			    :min-width  (+ w- min-width)
			    :max-width  (+ w+ max-width)
			    :height     (+ h  height)
			    :min-height (+ h- min-height)
			    :max-height (+ h+ max-height))))


