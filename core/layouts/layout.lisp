;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-LAYOUTS-INTERNALS -*-
(in-package :duim-layouts-internals)

#||
/// Layout Protocol

// The superclass of all panes that do layout on their children,
// for example, <row-layout>, <column-layout>, and <table-layout>.
// Panes that merely implement 'do-compose-space' (such as all the
// gadgets) aren't subclasses of <layout>.
define protocol-class layout (<abstract-sheet>)
end protocol-class layout;
||#

(define-protocol-class layout (<abstract-sheet>) ()
  (:documentation
   "The class of layouts. This is the basic class from which all other
forms of layout inherit. You cannot create direct instances of this
class.

The :space-requirement initarg describes the space required for the
layout. It is generally calculated automatically based on the values
of the various width and height initargs, and the class of layout that
is being created.

The :width, :height, :min-width, :min-height, :max-width
and :max-height initargs between them describe the configuration of
the layout. The default values for these initargs (where applicable)
are set such that the layout always fills the available space in any
given direction.

Finally, three initargs are available that control how the layout is
affected when the frame containing it is resized. All three initargs
take boolean values. You can specify whether a layout is resizable
using the :resizable? initarg. If :fixed-width? or :fixed-height? are
T, then the layout cannot be resized in the appropriate
direction. Setting both to T is equivalent to setting :resizable? to
NIL. Different subclasses of layout restrict the values of these
initargs in different ways, such that, for instance, a row layout has
a fixed height.
"))


#||
define constant <vertical-position>
    = one-of(#"top", #"bottom");
define constant <horizontal-position>
    = one-of(#"left", #"right");
define constant <y-alignment>
    = one-of(#"top", #"bottom", #"center", #"centre", #"baseline");
define constant <x-alignment>
    = one-of(#"left", #"right", #"center", #"centre");
||#

;; ::fixme:: Work out what to do with restricted types... these need to
;;           be classes if we want to dispatch on them. Hrm.

;; FIXME: (DEFINE-TYPE-CLASS <VERTICAL-POSITION> (ONE-OF :TOP :BOTTOM))?
(deftype <vertical-position> () '(member :top :bottom))
(deftype <horizontal-position> () '(member :left :right))
(deftype <y-alignment> () '(member :top :bottom :center :centre :baseline))
(deftype <x-alignment> () '(member :left :right :center :centre))


#||
define protocol <<layout-protocol>> ()
  function compose-space 
    (pane :: <abstract-sheet>, #key width, height)
 => (space-req :: <space-requirement>);
  function do-compose-space
    (pane :: <abstract-sheet>, #key width, height)
 => (space-req :: <space-requirement>);
  function allocate-space
    (pane :: <abstract-sheet>, width :: <integer>, height :: <integer>) => ();
  function do-allocate-space
    (pane :: <abstract-sheet>, width :: <integer>, height :: <integer>) => ();
  function default-space-requirement
    (sheet :: <abstract-sheet>,
     #key width, min-width, max-width, height, min-height, max-height, #all-keys)
 => (space-req :: <space-requirement>);
  function invalidate-space-requirements (sheet :: <abstract-sheet>) => ();
end protocol <<layout-protocol>>;
||#

(define-protocol <<layout-protocol>> ()
  (:function compose-space (pane &key width height)
	     (:documentation
"
Returns the amount of space required for _pane_, which is a child of a
composite pane. During the space composition pass, a composite pane
will typically ask each of its children how much space it requires by
calling 'compose-space'. They answer by returning instances of
<space-requirement>. The composite pane then forms its own space
requirement by composing the space requirements of its children
according to its own rules for layout out its children.

The value returned by 'compose-space' is an instance of
<space-requirement> that represents how much space _pane_ requires.

The _width_ and _height_ arguments are real numbers that the
'compose-space' method for a pane may use as \"recommended\" values
for the width and height of the pane. These are used to drive top-down
layout.

The function actually calls 'do-compose-space' to perform the space
calculations. Client code may specialize 'do-compose-space' but should
not call it. Call 'compose-space' instead.

Defined by: <<layout-protocol>> "))
  (:function do-compose-space (pane &key width height)
	     (:documentation
"
This function is called by 'compose-space' to calculate space
requirements for a child. When calculating space requirements for
children in classes of a pane you have defined yourself, you should
specialize this function by adding methods for it. However, you should
not call 'do-compose-space' explicitly; call 'compose-space' instead.

Example:

    Assume that you have defined a new class of scroll bar as follows:

        (defclass <my-scroll-bar> (<scroll-bar> <leaf-pane>) ())

    A new method for 'do-compose-space' can be defined as follows:

        (defmethod do-compose-space ((pane <my-scroll-bar>)
                                     &key width height)
          (ecase (gadget-orientation pane)
            (:horizontal (make-instance '<space-requirement>
                                        :width (or width 50)
                                        :min-width 50
                                        :max-width +fill+
                                        :height 10))
            (:vertical   (make-instance '<space-requirement>
                                        :width 10
                                        :height (or height 50)
                                        :min-height 50
                                        :max-height +fill+))))

Defined by: <<layout-protocol>>
"))
  (:function allocate-space (pane width height)
	     (:documentation
"
Allocates space within a layout for its children. During the space
allocation pass, a composite pane arranges its children within the
available space and allocates space to them according to their space
requirements and its own composition rules by calling 'allocate-space'
on each of the child panes. For example, <column-layout> arranges all
its children in a vertical column. The _width_ and _height_ arguments
are the width and height of _pane_ in device units, that is,
pixels. These arguments give the amount of space into which all
children must fit.

This function actually calls 'do-allocate-space' to perform the
calculations.  Client code may specialize 'do-allocate-space', but not
call it. Call 'allocate-space' instead.

Defined by: <<layout-protocol>>
"))
  (:function do-allocate-space (pane width height)
	     (:documentation
"
This function is called by 'allocate-space' to calculate space
requirements for a pane. When calculating space requirements for
classes of pane you have defined yourself, you should add methods to
this function, but not call it directly. Call 'allocate-space'
instead.

Defined by: <<layout-protocol>>
"))
  (:function default-space-requirement (sheet &key width min-width max-width
                                              height min-height max-height
                                              &allow-other-keys)
	     (:documentation
"
Returns the space required by this layout without taking into
consideration any child requirements.

Defined by: <<layout-protocol>>
"))
  (:function invalidate-space-requirements (sheet)
	     (:documentation
"
Defined by: <<layout-protocol>>
")))

(defgeneric reset-space-requirement (pane &key space-requirement))
(defgeneric sheet-force-fixed-width? (pane))
(defgeneric sheet-force-fixed-height? (pane))
(defgeneric constrain-size (preferred-size min-size max-size))
(defgeneric sheet-interpret-alignment (sheet alignment key))
(defgeneric sheet-x-alignment (sheet alignment left right &key key))
(defgeneric sheet-y-alignment (sheet alignment top bottom &key key))
(defgeneric layout-align-sheet-x (layout sheet left right &key key))
(defgeneric layout-align-sheet-y (layout sheet top bottom &key key))


#||
/// Default methods for layout protocol

define method compose-space
    (pane :: <sheet>, #key width, height) => (space-req :: <space-requirement>)
  let space-req = do-compose-space(pane, width: width, height: height);
  //--- Maybe remove this if the efficiency loss is too huge...
  let (w, w-, w+, h, h-, h+) = space-requirement-components(pane, space-req);
  assert("Space requirement components are all integers",
	   instance?(w, <integer>) & instance?(w-, <integer>) & instance?(w+, <integer>)
	 & instance?(h, <integer>) & instance?(h-, <integer>) & instance?(h+, <integer>));
  space-req
end method compose-space;
||#

;;; Default methods for layout protocol

(defmethod compose-space ((pane <sheet>) &key width height)
  (let ((space-req (do-compose-space pane :width width :height height)))
    ;;--- Maybe remove this if the efficiency loss is too huge...
    (multiple-value-bind (w w- w+ h h- h+)
	(space-requirement-components pane space-req)
      (unless (and (integerp w) (integerp w-) (integerp w+)
		   (integerp h) (integerp h-) (integerp h+))
	(error "Space requirement components must all be integers"))
      space-req)))


#||
define method allocate-space
    (pane :: <sheet>, width :: <integer>, height :: <integer>) => ()
  do-allocate-space(pane, width, height);
  sheet-layed-out?(pane) := #t
end method allocate-space;
||#

(defmethod allocate-space ((pane <sheet>) (width integer) (height integer))
  (do-allocate-space pane width height)
  (setf (sheet-layed-out? pane) t))


#||
//--- This seems dubious...
define method do-allocate-space
    (pane :: <sheet>, width :: <integer>, height :: <integer>) => ()
  ignore(width, height);
  #f
end method do-allocate-space;
||#

(defmethod do-allocate-space ((pane <sheet>) (width integer) (height integer))
  (warn "LAYOUTS;LAYOUT:DO-ALLOCATE-SPACE <SHEET> ~dx~d invoked on ~a, does this sheet type need its own implementation?" width height pane)
  nil)



#||

/// Layout Mixin

define open abstract class <layout-mixin> (<layout>)
end class <layout-mixin>;
||#

(defclass <layout-mixin> (<layout>)
  ()
  (:metaclass <abstract-metaclass>))


#||
define open abstract class <leaf-layout-mixin> (<layout-mixin>)
end class <leaf-layout-mixin>;
||#

(defclass <leaf-layout-mixin> (<layout-mixin>)
  ()
  (:metaclass <abstract-metaclass>))


#||
define open abstract class <composite-layout-mixin> (<layout-mixin>)
end class <composite-layout-mixin>;
||#

(defclass <composite-layout-mixin> (<layout-mixin>)
  ()
  (:metaclass <abstract-metaclass>))


#||
// When the region of a sheet gets changed during layout, we want to
// re-layout its children to conform to the new geometry
define method relayout-children
    (pane :: <composite-layout-mixin>) => ()
  let (width, height) = box-size(pane);
  allocate-space(pane, width, height)
end method relayout-children;
||#

;; When the region of a sheet gets changed during layout, we want to
;; re-layout its children to conform to the new geometry
(defmethod relayout-children ((pane <composite-layout-mixin>))
  (multiple-value-bind (width height)
      (box-size pane)
    (allocate-space pane width height)))


#||
define method sheet-layed-out-to-size?
    (pane :: <composite-layout-mixin>, width :: <integer>, height :: <integer>)
 => (layed-out? :: <boolean>)
  let (old-width, old-height) = box-size(pane);
  sheet-layed-out?(pane)
  & width = old-width & height = old-height
end method sheet-layed-out-to-size?;
||#

(defmethod sheet-layed-out-to-size? ((pane <composite-layout-mixin>) (width integer) (height integer))
  (multiple-value-bind (old-width old-height)
      (box-size pane)
    (and (sheet-layed-out? pane) (= width old-width) (= height old-height))))



#||

/// Space Requirement Mixin

// This class manages a space requirement object for panes that don't
// have a 'compose-space' method.  It's most useful with <layout-mixin>.
define open abstract class <space-requirement-mixin> (<abstract-sheet>)
  sealed slot pane-space-requirement :: false-or(<space-requirement>) = #f,
    init-keyword: space-requirement:;
end class <space-requirement-mixin>;
||#

(defclass <space-requirement-mixin>
    (<abstract-sheet>)
  ((pane-space-requirement :type (or null <space-requirement>)
			   :initarg :space-requirement :initform nil
			   :accessor pane-space-requirement))
  (:documentation
"
This class manages a space requirement object for panes that don't
have a 'compose-space' method. It's most useful with <layout-mixin>.
")
  (:metaclass <abstract-metaclass>))


#||
define method pane-space-requirement (sheet :: <sheet>) => (space-req)
  #f
end method pane-space-requirement;
||#

(defmethod pane-space-requirement ((sheet <sheet>))
  nil)


#||
define method initialize
    (pane :: <space-requirement-mixin>, #rest initargs,
     #key space-requirement,
	  width, min-width, max-width, height, min-height, max-height)
  dynamic-extent(initargs);
  ignore(width, min-width, max-width, height, min-height, max-height);
  next-method();
  let space-req = space-requirement | apply(default-space-requirement, pane, initargs);
  pane-space-requirement(pane) := space-req
end method initialize;
||#

(defmethod initialize-instance :after ((pane <space-requirement-mixin>) &rest initargs
				       &key space-requirement
				       width min-width max-width height min-height max-height
				       &allow-other-keys)
  (declare (dynamic-extent initargs)
	   (ignore width min-width max-width height min-height max-height))
  (let ((space-req (or space-requirement (apply #'default-space-requirement pane initargs))))
    (setf (pane-space-requirement pane) space-req)))


#||
define method validate-sheet-size
    (pane :: <space-requirement-mixin>, width :: <integer>, height :: <integer>) => ()
  let space-req = pane-space-requirement(pane);
  when (space-req)
    let (w, w-, w+, h, h-, h+) = space-requirement-components(pane, space-req);
    ignore(w, w+, h, h+);
    when (width < w- | height < h-)
      warn("Resizing sheet %= to be too small -- %dX%d, not %dX%d",
	   pane, width, height, w-, h-)
    end
  end
end method validate-sheet-size;
||#

;; Is the width + height provided valid for the space requirement of the pane?
;;--- Maybe this should also check the maximums?
(defmethod validate-sheet-size ((pane <space-requirement-mixin>) (width integer) (height integer))
  (let ((space-req (pane-space-requirement pane)))
    (when space-req
      (multiple-value-bind (w w- w+ h h- h+)
	  (space-requirement-components pane space-req)
	(declare (ignore w w+ h h+))
	(when (or (< width w-) (< height h-))
	  (warn "LAYOUTS;LAYOUT:VALIDATE-SHEET-SIZE <space-requirement-mixin>: Resizing sheet ~a to be too small -- ~dX~d, not ~dX~d"
		pane width height w- h-))))))


#||
define method do-compose-space
    (pane :: <space-requirement-mixin>, #rest keys, #key width, height)
 => (space-req :: <space-requirement>)
  dynamic-extent(keys);
  ignore(width, height);
  pane-space-requirement(pane) 
  | apply(default-space-requirement, pane, keys)
end method do-compose-space;
||#

(defmethod do-compose-space ((pane <space-requirement-mixin>) &rest keys &key width height)
  (declare (dynamic-extent keys)
	   (ignore width height))
  (or (pane-space-requirement pane) 
      (apply #'default-space-requirement pane keys)))


#||
define method default-space-requirement
    (sheet :: <sheet>,
     #key width, min-width, max-width, height, min-height, max-height)
 => (space-req :: <space-requirement>)
  make(<space-requirement>,
       width: width | $default-sheet-size, height: height | $default-sheet-size,
       min-width: min-width | 0,           min-height: min-height | 0,
       max-width: max-width | $fill,       max-height: max-height | $fill)
end method default-space-requirement;
||#

(defmethod default-space-requirement ((sheet <sheet>)
				      &key width min-width max-width height min-height max-height)
  (make-space-requirement
		 :width      (or width +default-sheet-size+)
		 :height     (or height +default-sheet-size+)
		 :min-width  (or min-width 0)
		 :min-height (or min-height 0)
		 :max-width  (or max-width +fill+)
		 :max-height (or max-height +fill+)))


#||
// Resets and decaches the space requirement for a single pane
define method reset-space-requirement
    (pane :: <sheet>,
     #key space-requirement :: false-or(<space-requirement>) = #f) => ()
  ignore(space-requirement);
  #f
end method reset-space-requirement;
||#

;; Resets and decaches the space requirement for a single pane
(defmethod reset-space-requirement ((pane <sheet>) &key (space-requirement nil))
  (declare (ignore space-requirement))
  nil)


#||
define method reset-space-requirement
    (pane :: <space-requirement-mixin>,
     #key space-requirement :: false-or(<space-requirement>) = #f) => ()
  pane-space-requirement(pane) := space-requirement
end method reset-space-requirement;
||#

(defmethod reset-space-requirement ((pane <space-requirement-mixin>) &key (space-requirement nil))
  (setf (pane-space-requirement pane) space-requirement))



#||

/// Cached Space Requirements

// If both <cached-space-requirement-mixin> and <space-requirement-mixin>
// appear in the same CPL, then <cached-space-requirement-mixin> must
// precede <space-requirement-mixin>
define open abstract class <cached-space-requirement-mixin> (<abstract-sheet>)
  // Contains a triple of [space-req, width, height]
  sealed slot %space-requirement-cache = #f;
end class <cached-space-requirement-mixin>;
||#

(defclass <cached-space-requirement-mixin>
    (<abstract-sheet>)
  ;; Contains a triple of [space-req, width, height]
  ((%space-requirement-cache :initform nil :accessor %space-requirement-cache))
  (:documentation
"
If both <CACHED-SPACE-REQUIREMENT-MIXIN> and <SPACE-REQUIREMENT-MIXIN>
appear in the same CPL, then <CACHED-SPACE-REQUIREMENT-MIXIN> must
precede <SPACE-REQUIREMENT-MIXIN>.
")
  (:metaclass <abstract-metaclass>))


#||
define method validate-sheet-size
    (pane :: <cached-space-requirement-mixin>, width :: <integer>, height :: <integer>) => ()
  let cache = pane.%space-requirement-cache;
  let space-req = (cache & cache[0]) | pane-space-requirement(pane);
  when (space-req)
    let (w, w-, w+, h, h-, h+) = space-requirement-components(pane, space-req);
    ignore(w, w+, h, h+);
    when (width < w- | height < h-)
      warn("Resizing sheet %= to be too small -- %dX%d, not %dX%d",
	   pane, width, height, w-, h-)
    end
  end
end method validate-sheet-size;
||#

(defmethod validate-sheet-size ((pane <cached-space-requirement-mixin>) (width integer) (height integer))
  (let* ((cache (%space-requirement-cache pane))
	 (space-req (or (and cache (aref cache 0)) (pane-space-requirement pane))))
    (when space-req
      (multiple-value-bind (w w- w+ h h- h+)
	  (space-requirement-components pane space-req)
	(declare (ignore w w+ h h+))
	(when (or (< width w-)
		  (< height h-))
	  (warn "LAYOUTS;LAYOUT:VALIDATE-SHEET-SIZE <cached-space-requirement-mixin> : Resizing sheet ~a to be too small -- ~dX~d, not ~dX~d"
		pane width height w- h-))))))


#||
// Yes, this really is a wrapper on 'compose-space', not on 'DO-compose-space'.
// It's here so we can try using the cache before doing 'do-compose-space'
define method compose-space
    (pane :: <cached-space-requirement-mixin>, #key width, height)
 => (space-req :: <space-requirement>)
  let cache = pane.%space-requirement-cache;
  without-bounds-checks
    if (cache & (~width | width = cache[1]) & (~height | height = cache[2]))
      cache[0]
    else
      let space-req = next-method();
      unless (cache)
	cache := vector(#f, #f, #f);
	pane.%space-requirement-cache := cache
      end;
      cache[0] := space-req;
      cache[1] := width;
      cache[2] := height;
      space-req
    end
  end
end method compose-space;
||#

;; Yes, this really is a wrapper on 'compose-space', not on 'DO-compose-space'.
;; It's here so we can try using the cache before doing 'do-compose-space'
(defmethod compose-space ((pane <cached-space-requirement-mixin>) &key width height)
  (let ((cache (%space-requirement-cache pane)))
    (if (and cache
	     (or (not width) (equal? width (aref cache 1)))
	     (or (not height) (equal? height (aref cache 2))))
	(aref cache 0)
	;; else
	(let ((space-req (call-next-method)))
	  (unless cache
	    (setf cache (make-array 3 :initial-element nil))
	    (setf (%space-requirement-cache pane) cache))
	  (setf (aref cache 0) space-req)
	  (setf (aref cache 1) width)
	  (setf (aref cache 2) height)
	  space-req))))


#||
define method sheet-layed-out?-setter
    (layed-out? == #f, pane :: <cached-space-requirement-mixin>)
 => (layed-out? :: <boolean>)
  pane.%space-requirement-cache := #f;
  next-method()
end method sheet-layed-out?-setter;
||#

(defmethod (setf sheet-layed-out?) ((layed-out? (eql nil)) (pane <cached-space-requirement-mixin>))
  (declare (ignore layed-out?))
  (setf (%space-requirement-cache pane) nil)
  (call-next-method))


#||
// Reset an entire sheet hierarchy to an un-layed-out state
define method invalidate-space-requirements (sheet :: <sheet>) => ()
  local method invalidate (sheet :: <sheet>) => ()
	  sheet-layed-out?(sheet) := #f
	end method;
  do-sheet-tree(invalidate, sheet)
end method invalidate-space-requirements;
||#

(defmethod invalidate-space-requirements ((sheet <sheet>))
  (labels ((invalidate (sheet)
             (setf (sheet-layed-out? sheet) nil)))
    (do-sheet-tree #'invalidate sheet)))


#||
define method reset-space-requirement
    (pane :: <cached-space-requirement-mixin>,
     #key space-requirement :: false-or(<space-requirement>) = #f)
  ignore(space-requirement);
  next-method();
  pane.%space-requirement-cache := #f
end method reset-space-requirement;
||#

(defmethod reset-space-requirement ((pane <cached-space-requirement-mixin>)
				    &key (space-requirement nil))
  (declare (ignore space-requirement))
  (call-next-method)
  (setf (%space-requirement-cache pane) nil))



#||

/// Client Overridability

// Bits 15..16  of 'sheet-flags' are reserved for fixed space requirements
define constant %fixed_width  :: <integer> = #o100000;
define constant %fixed_height :: <integer> = #o200000;
||#

(defconstant %fixed_width  #o100000)
(defconstant %fixed_height #o200000)


#||
// The idea here is that a user can specify an "overriding" space req for
// a pane.  Any non-#f component in the space req is used to override the
// space requirement that would normally be used for the pane.
// Note that this needs to precede some class, such as <layout-mixin>, that
// implements a method for 'compose-space'
define open abstract class <client-overridability-mixin> (<abstract-sheet>)
  sealed slot %override-space-requirement :: false-or(<space-requirement>) = #f;
end class <client-overridability-mixin>;
||#

(defclass <client-overridability-mixin>
    (<abstract-sheet>)
  ((%override-space-requirement :type (or null <space-requirement>) :initform nil :accessor %override-space-requirement))
  (:documentation
"
The idea here is that a user can specify an 'overriding' space req for
a pane.  Any non-#f component in the space req is used to override the
space requirement that would normally be used for the pane.
Note that this needs to precede some class, such as <layout-mixin>, that
implements a method for 'compose-space'
")
  (:metaclass <abstract-metaclass>))


#||
define method initialize
    (pane :: <client-overridability-mixin>,
     #key space-requirement,
	  width, min-width, max-width, height, min-height, max-height,
          resizable? = #t, 
          fixed-width? = ~resizable?, fixed-height? = ~resizable?)
  next-method();
  let bits = logior(if (fixed-width?)  %fixed_width  else 0 end,
		    if (fixed-height?) %fixed_height else 0 end);
  sheet-flags(pane) := logior(sheet-flags(pane), bits);
  when (space-requirement
	| width | min-width | max-width | height | min-height | max-height)
    pane.%override-space-requirement
      := space-requirement
         | make(<space-requirement>,
		width:  width,  min-width:  min-width,  max-width:  max-width,
		height: height, min-height: min-height, max-height: max-height)
  end
end method initialize;
||#

(defmethod initialize-instance :after ((pane <client-overridability-mixin>) &key
				       space-requirement
				       width min-width max-width height min-height max-height
				       (resizable? t)
				       (fixed-width? (not resizable?)) (fixed-height? (not resizable?))
				       &allow-other-keys)
  (let ((bits (logior (if fixed-width? %fixed_width 0)
		      (if fixed-height? %fixed_height 0))))
    (setf (sheet-flags pane) (logior (sheet-flags pane) bits))
    (when (or space-requirement
	      width min-width max-width height min-height max-height)
      (setf (%override-space-requirement pane)
	    (or space-requirement
		(make-space-requirement :width width
					:min-width min-width
					:max-width max-width
					:height height
					:min-height min-height
					:max-height max-height))))))


#||
define sealed inline method sheet-force-fixed-width?
    (pane :: <client-overridability-mixin>) => (fixed-width? :: <boolean>)
  logand(sheet-flags(pane), %fixed_width) = %fixed_width
end method sheet-force-fixed-width?;
||#

(defmethod sheet-force-fixed-width? ((pane <client-overridability-mixin>))
  (= (logand (sheet-flags pane) %fixed_width) %fixed_width))


#||
define sealed inline method sheet-force-fixed-height?
    (pane :: <client-overridability-mixin>) => (fixed-height? :: <boolean>)
  logand(sheet-flags(pane), %fixed_height) = %fixed_height
end method sheet-force-fixed-height?;
||#

(defmethod sheet-force-fixed-height? ((pane <client-overridability-mixin>))
  (= (logand (sheet-flags pane) %fixed_height) %fixed_height))


#||
define inline function constrain-size
    (preferred-size :: <integer>, min-size :: <integer>, max-size :: <integer>)
 => (preferred-size :: <integer>)
  max(min-size, min(max-size, preferred-size))
end function constrain-size;
||#

(defmethod constrain-size ((preferred-size integer) (min-size integer) (max-size integer))
  ;; Ensure preferred size lies between min-size + max-size
  (max min-size (min max-size preferred-size)))


#||
// Yes, this really is a wrapper on 'compose-space', not on 'DO-compose-space'
// Note that 'compose-space' constrains the width and height to be within the
// newly defined bounds.
define method compose-space
    (pane :: <client-overridability-mixin>, #key width, height)
 => (space-req :: <space-requirement>)
  let fixed-width?  = sheet-force-fixed-width?(pane);
  let fixed-height? = sheet-force-fixed-height?(pane);
  let space-req
    = next-method(pane,
		  // Don't take top-down advice for fixed width or height
		  width:  ~fixed-width?  & width,
		  height: ~fixed-height? & height);
  let override-space-req = pane.%override-space-requirement;
  if (override-space-req | fixed-width? | fixed-height?)
    let (w, w-, w+, h, h-, h+)
      = space-requirement-components(pane, space-req);
    let (ow, ow-, ow+, oh, oh-, oh+)
      = space-requirement-components(pane, override-space-req | space-req);
    let nmin-width  = ow- | w-;
    let nmax-width  = ow+ | w+;
    let nmin-height = oh- | h-;
    let nmax-height = oh+ | h+;
    let nwidth      = constrain-size(ow | w, nmin-width,  nmax-width);
    let nheight     = constrain-size(oh | h, nmin-height, nmax-height);
    let (best-width, min-width, max-width)
      = if (fixed-width?)
	  values(nwidth, nwidth, nwidth)
	else
	  values(nwidth, nmin-width, nmax-width)
	end;
    let (best-height, min-height, max-height)
      = if (fixed-height?)
	  values(nheight, nheight, nheight)
	else
	  values(nheight, nmin-height, nmax-height)
	end;
    make(<space-requirement>,
	 width:  best-width,  min-width:  min-width,  max-width:  max-width,
	 height: best-height, min-height: min-height, max-height: max-height)
  else
    space-req
  end
end method compose-space;
||#

;; Yes, this really is a wrapper on 'compose-space', not on 'DO-compose-space'
;; Note that 'compose-space' constrains the width and height to be within the
;; newly defined bounds.
(defmethod compose-space ((pane <client-overridability-mixin>) &key width height)
  (let* ((fixed-width? (sheet-force-fixed-width? pane))
	 (fixed-height? (sheet-force-fixed-height? pane)))
    (let* ((space-req (call-next-method pane
					;; Don't take top-down advice for fixed width or height
					:width  (and (not fixed-width?) width)
					:height (and (not fixed-height?) height)))
	   (override-space-req (%override-space-requirement pane)))
      (if (or override-space-req fixed-width? fixed-height?)
	  (multiple-value-bind (w w- w+ h h- h+)
	      (space-requirement-components pane space-req)
	    (multiple-value-bind (ow ow- ow+ oh oh- oh+)
		(space-requirement-components pane (or override-space-req space-req))
	      (let* ((nmin-width (or ow- w-))
		     (nmax-width (or ow+ w+))
		     (nmin-height (or oh- h-))
		     (nmax-height (or oh+ h+))
		     (nwidth (constrain-size (or ow w) nmin-width nmax-width))
		     (nheight (constrain-size (or oh h) nmin-height nmax-height)))
		(multiple-value-bind (best-width min-width max-width)
		    (if fixed-width?
			(values nwidth nwidth nwidth)
			(values nwidth nmin-width nmax-width))
		  (multiple-value-bind (best-height min-height max-height)
		      (if fixed-height?
			  (values nheight nheight nheight)
			  (values nheight nmin-height nmax-height))
		    (make-space-requirement :width best-width
					    :min-width min-width
					    :max-width max-width
					    :height best-height
					    :min-height min-height
					    :max-height max-height))))))
	  ;; else
	  space-req))))



#||

/// Wrapping Layout Mixin

// This class gets used when the pane in question uses exactly the same
// space requirements as the "sum" of its children's requirements.  Viewport
// panes are good examples of this.
define open abstract class <wrapping-layout-mixin> (<composite-layout-mixin>)
end class <wrapping-layout-mixin>;
||#

(defclass <wrapping-layout-mixin>
    (<composite-layout-mixin>)
  ()
  (:documentation
"
This class gets used when the pane in question uses exactly the same
space requirements as the 'sum' of its children's requirements.  Viewport
panes are good examples of this.
")
  (:metaclass <abstract-metaclass>))


#||
define method do-compose-space
    (pane :: <wrapping-layout-mixin>, #key width, height)
 => (space-req :: <space-requirement>)
  let children = sheet-children(pane);
  case
    empty?(children) =>
      default-space-requirement(pane, width: width, height: height);
    size(children) = 1 =>	// optimize a very common case...
      compose-space(children[0], width: width, height: height);
    otherwise =>
      let (w :: <integer>, w- :: <integer>, w+ :: <integer>,
	   h :: <integer>, h- :: <integer>, h+ :: <integer>)
	= space-requirement-components(pane, compose-space(children[0]));
      for (child :: <basic-sheet> in children)
	unless (sheet-withdrawn?(child))
	  let (srw :: <integer>, srw- :: <integer>, srw+ :: <integer>,
	       srh :: <integer>, srh- :: <integer>, srh+ :: <integer>)
	    = space-requirement-components(child, compose-space(child));
	  let (x, y) = values(0, 0);	//--- sheet-position(child)...
	  max!(w,  srw  + x);
	  max!(h,  srh  + y);
	  max!(w-, srw- + x);
	  max!(h-, srh- + y);
	  max!(w+, srw+ + x);
	  max!(h+, srh+ + y)
	end
      end;
      let w = constrain-size(width  | w, w-, w+);
      let h = constrain-size(height | h, h-, h+);
      make(<space-requirement>,
           width:  w, min-width:  w-, max-width:  w+,
           height: h, min-height: h-, max-height: h+);
  end
end method do-compose-space;
||#

(defmethod do-compose-space ((pane <wrapping-layout-mixin>) &key width height)
  (let ((children (sheet-children pane)))
    (cond ((empty? children)
	   (default-space-requirement pane :width width :height height))
	  ((= 1 (length children))  ;; optimize a very common case...
	   (compose-space (SEQUENCE-ELT children 0) :width width :height height))
	  (t
	   (multiple-value-bind (w w- w+ h h- h+)
	       (space-requirement-components pane (compose-space (SEQUENCE-ELT children 0)))
	     (map nil
		  #'(lambda (child)
		      (unless (sheet-withdrawn? child)
			(multiple-value-bind (srw srw- srw+ srh srh- srh+)
			    (space-requirement-components child (compose-space child))
			  (multiple-value-bind (x y)
			      (values 0 0)   ;;--- FIXME (sheet-position child)...
			    (setf w  (max w  (+ srw x)))
			    (setf h  (max h  (+ srh y)))
			    (setf w- (max w- (+ srw- x)))
			    (setf h- (max h- (+ srh- y)))
			    (setf w+ (max w+ (+ srw+ x)))
			    (setf h+ (max h+ (+ srh+ y)))))))
		  children)
	     (let ((w (constrain-size (or width w) w- w+))
		   (h (constrain-size (or height h) h- h+)))
	       (make-space-requirement :width w :min-width w- :max-width w+
				       :height h :min-height h- :max-height h+)))))))


#||
define method do-allocate-space
    (pane :: <wrapping-layout-mixin>, width :: <integer>, height :: <integer>) => ()
  let children = sheet-children(pane);
  if (size(children) = 1)
    //--- Do we also want to set the position to (0,0)?
    let child :: <basic-sheet> = children[0];
    set-sheet-size(child, width, height)
  else
    for (child :: <basic-sheet> in children)
      unless (sheet-withdrawn?(child))
	let space-req = compose-space(child);
	let (w, w-, w+, h, h-, h+) = space-requirement-components(child, space-req);
	ignore(w-, w+, h-, h+);
	set-sheet-size(child, w, h)
      end
    end
  end
end method do-allocate-space;
||#

(defmethod do-allocate-space ((pane <wrapping-layout-mixin>) (width integer) (height integer))
  (let ((children (sheet-children pane)))
    (if (= (length children) 1)
	;;--- Do we also want to set the position to (0,0)?
	(let ((child (SEQUENCE-ELT children 0)))
	  (set-sheet-size child width height))
	(map nil #'(lambda (child)
		     (unless (sheet-withdrawn? child)
		       (let ((space-req (compose-space child)))
			 (multiple-value-bind (w w- w+ h h- h+)
			     (space-requirement-components child space-req)
			   (declare (ignore w- w+ h- h+))
			   (set-sheet-size child w h)))))
	     children))))



#||

/// Generally useful layout function
/// Used all over to satisfy constraints


// This supa dupa version works by calculating the sizes required
// by the ratios and then constrains these sizes by the max and min
// sizes. Depending on whether the result is larger or smaller than
// the overall required size, the items that cannot be adjusted
// in the right direction to help the fit are then fixed at their
// limiting size. The algorithm then loops back trying to fit the
// remaining items into the remaining space.
//
// SHEET is the sheet on whose behalf the composition is being done.
// DESIRED-SIZE is the desired size for all of the items, and SPACE-REQ
// is a space requirement that describes the items' parent.  The three
// size/min/max functions pull apart space requirements.  ITEMS is a
// sequence of items (sheets or space requirements), and ITEM-COMPOSER
// generates a space requirement from an item.  (For example, when ITEMS
// is a set of sheets, this function is 'compose-space'; when ITEMS is a
// set of space reqs, this function is 'identity'.)  
define function compose-space-for-items
    (sheet :: <sheet>,
     desired-size :: <integer>, space-req :: <space-requirement>, items :: <sequence>, 
     size-function :: <function>, min-function :: <function>, max-function :: <function>,
     item-composer :: <function>, #key ratios)
 => (sizes :: <simple-object-vector>)
  let n-items :: <integer> = size(items);
  let sizes :: <simple-object-vector> = make(<simple-vector>, size: n-items);
  let desired-ratios :: <simple-object-vector> = make(<simple-vector>, size: n-items);
  let sized? :: limited(<vector>, of: <boolean>, size: n-items) = make(limited(<vector>, of: <boolean>, size: n-items));
  let constrained-size :: <integer> = 0;
  let ratio-denominator :: <integer> = 0;
  // Calculate ratios and note withdrawn items
  for (item in items,
       index :: <integer> from 0)
    let child = sheet?(item) & item;
    if (child & sheet-withdrawn?(child))
      sizes[index] := 0;
      sized?[index] := #t;
    else
      let ratio = (ratios & index < size(ratios) & ratios[index]) | 1;
      desired-ratios[index] := ratio;
      ratio-denominator := ratio-denominator + ratio
    end
  end;
  let done? :: <boolean> = #f;
  // Loop until constraints satisfied
  let size-left :: <integer> = desired-size;
  until (done?)
    // Calculate desired sizes and note violators
    let constrained-size :: <integer> = 0;
    let constraining-mins :: <list> = #();
    let constraining-maxs :: <list> = #();
    for (item in items,
         item-sized? in sized?,
         ratio in desired-ratios,
         index :: <integer> from 0)
      unless (item-sized?)
        let child = sheet?(item) & item;
        let item-sr = item-composer(item);
        let desired-item-size :: <integer> = truncate/(size-left * ratio, ratio-denominator);
        let item-max = max-function(child | sheet, item-sr);
        let item-min = min-function(child | sheet, item-sr);
        let constrained-item-size :: <integer> = desired-item-size;
        case
          (desired-item-size < item-min) =>
            constraining-mins := pair(index, constraining-mins);
            constrained-item-size := item-min;
          (desired-item-size > item-max) =>
            constraining-maxs := pair(index, constraining-maxs);
            constrained-item-size := item-max;
          otherwise => #f;
        end;
        sizes[index] := constrained-item-size;
        constrained-size := constrained-size + constrained-item-size;
      end
    end;
    // Nail down the ones that can't resize in the right direction to help fit
    case
      ((constrained-size < size-left) & ~empty?(constraining-maxs)) =>
        for (index in constraining-maxs)
          ratio-denominator := ratio-denominator - desired-ratios[index];
          size-left := size-left - sizes[index];
          sized?[index] := #t;
        end;
      ((constrained-size > size-left) & ~empty?(constraining-mins)) => 
        for (index in constraining-mins)
          ratio-denominator := ratio-denominator - desired-ratios[index];
          size-left := size-left - sizes[index];
          sized?[index] := #t;
        end;
      otherwise =>
        done? := #t;
    end
  end;
  sizes
end function compose-space-for-items;
||#

;;; :fixme: should replace the function specializers on the args for the
;;;         following method.

(defgeneric compose-space-for-items (sheet desired-size space-req items
				     size-function min-function max-function
				     item-composer &key ratios))

(defmethod compose-space-for-items ((sheet <sheet>)
				    (desired-size integer) (space-req <space-requirement>) (items vector) ;;sequence)
				    (size-function function) (min-function function) (max-function function)
				    (item-composer function) &key ratios)
  (declare (ignorable space-req size-function))
  (let* ((n-items (length items))
	 (sizes (make-array n-items))
	 (desired-ratios (make-array n-items))
	 (sized? (make-array n-items :element-type 'boolean :initial-element nil))
	 (constrained-size 0)
	 (ratio-denominator 0))
    (declare (ignorable constrained-size))
    ;; Calculate ratios and note withdrawn items
    (loop for item across items
	  for index = 0 then (+ index 1)
	  do (let ((child (and (sheetp item) item)))
	       (if (and child (sheet-withdrawn? child))
		   (setf (aref sizes index) 0
			 (aref sized? index) t)
		 ;; else
		 (let ((ratio (or (and ratios (< index (length ratios)) (aref ratios index)) 1)))
		   (setf (aref desired-ratios index) ratio)
		   (setf ratio-denominator (+ ratio-denominator ratio))))))
    (let ((done? nil)
	  ;; Loop until constraints satisfied
	  (size-left desired-size))
      (loop until done?
            ;; Calculate desired sizes and note violators
            do (let ((constrained-size 0)
                     (constraining-mins ())
                     (constraining-maxs ()))
                 (loop for item across items
                       for item-sized? across sized?
                       for ratio across desired-ratios
                       for index = 0 then (+ index 1)
                       do (unless item-sized?
                            (let* ((child (and (sheetp item) item))
                                   (item-sr (funcall item-composer item))
				   ;; FIXME: THE DYLAN USES 'TRUNCATE/' -- LOOK IT UP AND MAKE SURE TRUNCATE IS THE SAME!
                                   (desired-item-size (truncate (* size-left ratio) ratio-denominator))
                                   (item-max (funcall max-function (or child sheet) item-sr))
                                   (item-min (funcall min-function (or child sheet) item-sr))
                                   (constrained-item-size desired-item-size))
                              (cond ((< desired-item-size item-min)
                                     (setf constraining-mins (cons index constraining-mins))
				     (setf constrained-item-size item-min))
                                    ((> desired-item-size item-max)
                                     (setf constraining-maxs (cons index constraining-maxs))
				     (setf constrained-item-size item-max))
                                    (t nil))
                              (setf (aref sizes index) constrained-item-size)
                              (setf constrained-size (+ constrained-size constrained-item-size)))))
                 ;; Nail down the ones that can't resize in the right direction to help fit
                 (cond ((and (< constrained-size size-left) (not (empty? constraining-maxs)))
                        (loop for index in constraining-maxs
                              do (setf ratio-denominator (- ratio-denominator (aref desired-ratios index)))
                              do (setf size-left (- size-left (aref sizes index)))
                              do (setf (aref sized? index) t)))
                       ((and (> constrained-size size-left) (not (empty? constraining-mins)))
                        (loop for index in constraining-mins
                              do (setf ratio-denominator (- ratio-denominator (aref desired-ratios index)))
                              do (setf size-left (- size-left (aref sizes index)))
                              do (setf (aref sized? index) t)))
                       (t (setf done? t))))))
    sizes))



#||

/// The basic layout pane

// The internal layout panes (boxes, tables) are all built on this.
define open abstract class <layout-pane>
    (<cached-space-requirement-mixin>,
     <client-overridability-mixin>,
     <composite-layout-mixin>,
     <multiple-child-mixin>,
     <basic-sheet>)
end class <layout-pane>;
||#

(defclass <layout-pane>
    (<cached-space-requirement-mixin>
     <client-overridability-mixin>
     <composite-layout-mixin>
     <multiple-child-mixin>
     <basic-sheet>)
  ()
  (:documentation
"
The internal layout panes (boxes, tables) are all built on this.
")
  (:metaclass <abstract-metaclass>))



#||

/// Horizontal and vertical layout mixins

define open abstract class <horizontal-layout-mixin> (<layout>)
  sealed slot layout-x-spacing :: <integer> = 0,
    init-keyword: x-spacing:;
  // The sequence here allows different alignments for each row in a table
  sealed slot layout-y-alignment :: type-union(<sequence>, <y-alignment>) = #"top",
    init-keyword: y-alignment:;
  sealed slot layout-x-ratios :: false-or(<sequence>) = #f,
    init-keyword: x-ratios:;
end class <horizontal-layout-mixin>;
||#

(defclass <horizontal-layout-mixin>
    (<layout>)
  ((layout-x-spacing :type integer :initarg :x-spacing :initform 0 :accessor layout-x-spacing)
   ;; The sequence here allows different alignments for each row in a table
   (layout-y-alignment :type (or sequence <y-alignment>) :initarg :y-alignment :initform :top :accessor layout-y-alignment)
   (layout-x-ratios :type (or null sequence) :initarg :x-ratios :initform nil :accessor layout-x-ratios))
  (:metaclass <abstract-metaclass>))


#||
define method initialize
    (pane :: <horizontal-layout-mixin>,
     #key spacing = $unsupplied, ratios = $unsupplied)
  next-method();
  when (supplied?(spacing))
    layout-x-spacing(pane) := spacing
  end;
  when (supplied?(ratios))
    layout-x-ratios(pane) := ratios
  end;
end method initialize;
||#

(defmethod initialize-instance :after ((pane <horizontal-layout-mixin>)
				       &key (spacing :unsupplied) (ratios :unsupplied)
				       &allow-other-keys)
  (when (supplied? spacing)
    (setf (layout-x-spacing pane) spacing))
  (when (supplied? ratios)
    (setf (layout-x-ratios pane) ratios)))


#||
define open abstract class <vertical-layout-mixin> (<layout>)
  sealed slot layout-y-spacing :: <integer> = 0,
    init-keyword: y-spacing:;
  // The sequence here allows different alignments for each row in a table
  sealed slot layout-x-alignment :: type-union(<sequence>, <x-alignment>) = #"left",
    init-keyword: x-alignment:;
  sealed slot layout-y-ratios :: false-or(<sequence>) = #f,
    init-keyword: y-ratios:;
end class <vertical-layout-mixin>;
||#

(defclass <vertical-layout-mixin>
    (<layout>)
  ((layout-y-spacing :type integer :initarg :y-spacing :initform 0 :accessor layout-y-spacing)
   ;; The sequence here allows different alignments for each row in a table
   (layout-x-alignment :type (or sequence <x-alignment>) :initarg :x-alignment :initform :left :accessor layout-x-alignment)
   (layout-y-ratios :type (or null sequence) :initarg :y-ratios :initform nil :accessor layout-y-ratios))
  (:metaclass <abstract-metaclass>))


#||
define method initialize
    (pane :: <vertical-layout-mixin>,
     #key spacing = $unsupplied, ratios = $unsupplied)
  next-method();
  when (supplied?(spacing))
    layout-y-spacing(pane) := spacing
  end;
  when (supplied?(ratios))
    layout-y-ratios(pane) := ratios
  end;
end method initialize;
||#

(defmethod initialize-instance :after ((pane <vertical-layout-mixin>)
				       &key (spacing :unsupplied) (ratios :unsupplied)
				       &allow-other-keys)
  (when (supplied? spacing)
    (setf (layout-y-spacing pane) spacing))
  (when (supplied? ratios)
    (setf (layout-y-ratios pane) ratios)))



#||

/// Layout borders

define open abstract class <layout-border-mixin> (<layout>)
  sealed slot layout-border :: <integer> = 0,
    init-keyword: border:;
end class <layout-border-mixin>;
||#

(defclass <layout-border-mixin> (<layout>)
  ((layout-border :type integer :initarg :border :initform 0 :accessor layout-border))
  (:documentation
"
Documentation, for (layout-border):

Returns the amount of whitespace, in pixels, around the children in
_layout_.

Note that this function does not apply to pinboard layouts, because
the positioning of the children in a pinboard layout is completely in
the control of the programmer.


Documentation, for (setf layout-border):

Sets the amount of whitespace, in pixels, around the children in
_layout_.

You can also set this value when a layout is created using the :border
initarg.

Note that this function does not apply to pinboard layouts, because
the positioning of the children in a pinboard layout is completely in
the control of the programmer.
")
  (:metaclass <abstract-metaclass>))



#||

/// Alignment hacking

define method sheet-interpret-alignment
    (sheet :: <sheet>, alignment :: <symbol>, key)
  alignment
end method sheet-interpret-alignment;
||#

(defmethod sheet-interpret-alignment ((sheet <sheet>) alignment key)
  (declare (ignore key))
  alignment)


#||
define method sheet-interpret-alignment
    (sheet :: <sheet>, alignment :: <collection>, key)
  alignment[key]
end method sheet-interpret-alignment;
||#

(defmethod sheet-interpret-alignment ((sheet <sheet>) (alignment sequence) key)
  (nth key alignment))

(defmethod sheet-interpret-alignment ((sheet <sheet>) (alignment array) key)
  (aref alignment key))

(defmethod sheet-interpret-alignment ((sheet <sheet>) (alignment hash-table) key)
  (gethash key alignment))


#||
define method sheet-x-alignment
    (sheet :: <sheet>, alignment, left :: <integer>, right :: <integer>, #key key)
 => (x-position :: <integer>)
  select (sheet-interpret-alignment(sheet, alignment, key))
    #"left"  => left;
    #"right" => right;
    #"center", #"centre" => floor/(left + right, 2);
  end
end method sheet-x-alignment;
||#

(defmethod sheet-x-alignment ((sheet <sheet>) alignment (left integer) (right integer) &key key)
  (ecase (sheet-interpret-alignment sheet alignment key)
    (:left left)
    (:right right)
    ;; FIXME: 'FLOOR/' ?
    ((:center :centre) (floor (+ left right) 2))))


#||
define method sheet-y-alignment
    (sheet :: <sheet>, alignment, top :: <integer>, bottom :: <integer>, #key key)
 => (y-position :: <integer>)
  select (sheet-interpret-alignment(sheet, alignment, key))
    #"top"    => top;
    #"bottom" => bottom;
    #"center", #"centre" => floor/(top + bottom, 2);
  end
end method sheet-y-alignment;
||#

(defmethod sheet-y-alignment ((sheet <sheet>) alignment (top integer) (bottom integer) &key key)
  (ecase (sheet-interpret-alignment sheet alignment key)
    (:top top)
    (:bottom bottom)
    ((:center :centre) (floor (+ top bottom) 2))))


#||
// Returns the aligned Y value for a sheet
define method layout-align-sheet-x
    (layout :: <vertical-layout-mixin>, sheet :: <sheet>,
     left :: <integer>, right :: <integer>, #key key)
  sheet-x-alignment(sheet, layout-x-alignment(layout), left, right, key: key)
end method layout-align-sheet-x;
||#

;; Returns the aligned Y value for a sheet
(defmethod layout-align-sheet-x ((layout <vertical-layout-mixin>) (sheet <sheet>)
				 (left integer) (right integer) &key key)
  (sheet-x-alignment sheet (layout-x-alignment layout) left right :key key))


#||
// Returns the aligned Y value for a sheet
define method layout-align-sheet-y
    (layout :: <horizontal-layout-mixin>, sheet :: <sheet>,
     top :: <integer>, bottom :: <integer>, #key key)
  sheet-y-alignment(sheet, layout-y-alignment(layout), top, bottom, key: key)
end method layout-align-sheet-y;
||#

;; Returns the aligned Y value for a sheet
(defmethod layout-align-sheet-y ((layout <horizontal-layout-mixin>) (sheet <sheet>)
				 (top integer) (bottom integer) &key key)
  (sheet-y-alignment sheet (layout-y-alignment layout) top bottom :key key))



#||

/// Fixed layouts

// Fixed layouts can have any number of children, but there's no layout
// policy at all -- the kids worry about their own geometry
define open abstract class <fixed-layout> (<layout-pane>)
end class <fixed-layout>;
||#

(defclass <fixed-layout> (<layout-pane>)
  ()
  (:documentation
"
The class of fixed layouts. Fixed layouts are similar to pinboard
layouts, in that the positioning and geometry of the children of a
fixed layout are entirely determined by the programmer. You can place
children at any point in a fixed layout, and the layout does not
attempt to calculate an optimum position or size for any of them.

Fixed layouts differ from pinboard layouts, however, in that any
children placed in a fixed layout are left at exactly the size and
position that they were created; pinboard layouts leave the positions
of any children alone, but constrain the sizes of the children to obey
any constraints that they have been given.

Fixed layouts are most useful if you known exactly what size and
position every child in the layout should be.
")
  (:metaclass <abstract-metaclass>))


#||
define method do-compose-space 
    (pane :: <fixed-layout>, #key width, height)
 => (space-req :: <space-requirement>)
  default-space-requirement(pane, width: width, height: height)
end method do-compose-space;
||#

(defmethod do-compose-space ((pane <fixed-layout>) &key width height)
  (default-space-requirement pane :width width :height height))


#||
define method do-allocate-space
    (pane :: <fixed-layout>, width :: <integer>, height :: <integer>) => ()
  ignore(width, height);
  #f
end method do-allocate-space;
||#

(defmethod do-allocate-space ((pane <fixed-layout>) (width integer) (height integer))
  (declare (ignore width height))
  nil)


#||
/// Default implementation

define sealed class <fixed-layout-pane> (<fixed-layout>)
  keyword accepts-focus?: = #f;
end class <fixed-layout-pane>;
||#

(defclass <fixed-layout-pane> (<fixed-layout>)
  ()
  (:default-initargs :accepts-focus? nil))


#||
define method class-for-make-pane 
    (framem :: <frame-manager>, class == <fixed-layout>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<fixed-layout-pane>, #f)
end method class-for-make-pane;

define sealed domain make (singleton(<fixed-layout-pane>));
define sealed domain initialize (<fixed-layout-pane>);
||#

(defmethod class-for-make-pane ((framem <frame-manager>) (class (eql (find-class '<fixed-layout>))) &key)
  (values (find-class '<fixed-layout-pane>) nil))



#||

/// Pinboards

// Pinboards are like fixed layouts, except that they enforce
// the space constraints of their children.
define open abstract class <pinboard-layout> (<layout-pane>)
end class <pinboard-layout>;
||#

(defclass <pinboard-layout> (<layout-pane>)
  ()
  (:documentation
"
 The class of pinboard layouts. Unlike other types of layout, pinboard
 layouts are unusual in that the positioning and geometry of the
 children of a pinboard layout are entirely determined by the
 programmer. You can place children at any point in a pinboard layout,
 and the pinboard layout does not attempt to calculate an optimum
 position or size for any of them.

 A pinboard layout leaves the subsequent positions of any children
 placed in the layout alone. However, the size of each child is
 constrained according to any constraints that have been spcified for
 those children. Compare this to fixed layouts, where the sizes of any
 children are not constrained in this way.

 Because the size of a pinboard layout's children are constrained,
 pinboard layouts are most useful for placing sheets randomly in a
 layout, since DUIM ensures that the sheets remain a sensible size for
 their contents.

 If :stretchable? is T, then the pinboard layout can be resized
 dynamically as its parent is resized (for instance, by the user
 resizing a window on screen).
")
  (:metaclass <abstract-metaclass>))


#||
define method do-compose-space 
    (pane :: <pinboard-layout>, #key width, height)
 => (space-req :: <space-requirement>)
  default-space-requirement(pane, width: width, height: height)
end method do-compose-space;
||#

(defmethod do-compose-space ((pane <pinboard-layout>) &key width height)
  (default-space-requirement pane :width width :height height))


#||
define method do-allocate-space
    (pane :: <pinboard-layout>, width :: <integer>, height :: <integer>) => ()
  ignore(width, height);
  for (child :: <basic-sheet> in sheet-children(pane))
    unless (sheet-withdrawn?(child))
      let space-req = compose-space(child);
      let (w, w-, w+, h, h-, h+) = space-requirement-components(child, space-req);
      ignore(w-, w+, h-, h+);
      set-sheet-size(child, w, h)
    end
  end
end method do-allocate-space;
||#

;; XXX: Why do some layout sheets only include children that are unwithdrawn and
;; others (see row layout) don't appear to care?
(defmethod do-allocate-space ((pane <pinboard-layout>) (width integer) (height integer))
  (declare (ignore width height))
  (map nil #'(lambda (child)
	       (unless (sheet-withdrawn? child)
		 (let ((space-req (compose-space child)))
		   (multiple-value-bind (w w- w+ h h- h+)
		       (space-requirement-components child space-req)
		     (declare (ignore w- w+ h- h+))
		     (set-sheet-size child w h)))))
       (sheet-children pane)))


#||
/// Default implementation

define sealed class <pinboard-layout-pane> (<pinboard-layout>)
  keyword accepts-focus?: = #f;
end class <pinboard-layout-pane>;
||#

(defclass <pinboard-layout-pane> (<pinboard-layout>)
  ()
  (:default-initargs :accepts-focus? nil))


#||
define method class-for-make-pane 
    (framem :: <frame-manager>, class == <pinboard-layout>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<pinboard-layout-pane>, #f)
end method class-for-make-pane;

define sealed domain make (singleton(<pinboard-layout-pane>));
define sealed domain initialize (<pinboard-layout-pane>);
||#

(defmethod class-for-make-pane ((framem <frame-manager>) (class (eql (find-class '<pinboard-layout>))) &key)
  (values (find-class '<pinboard-layout-pane>) nil))



#||

/// Stacks

// Stack layouts position all of their children at the top-left
// one on top of the other. They are primarily useful for creating
// tab-controls or wizards where only one child is visible at a time.
define open abstract class <stack-layout> 
    (<layout-border-mixin>,
     <layout-pane>)
end class <stack-layout>;
||#

(defclass <stack-layout>
    (<layout-border-mixin>
     <layout-pane>)
  ()
  (:documentation
"
The class of stack layouts. Stack layouts position all of their
children at the top-left one on top of the other. The layout sizes
itself to be large enough to fit the largest child. They are primarily
useful for creating layouts that simulate sets of several pages where
only one child is visible at a time, and all the others are withdrawn,
and are used to control the layout of elements such as tab controls or
wizard frames. To make a new page appear, you withdraw the current
page, and then map the new page. The new page is automatically the
correct size and in the correct position.

The :border initarg provides a border of whitespace around the
children in the layout, and the value of this initarg represents the
size of the border in pixels. This basically has the same effect as
using the macro WITH-SPACING around the layout, except it uses a
simpler syntax.

The :mapped-page initarg allows you to assign a page to be mapped onto
the screen when a stack layout is first created. If it is not
specified, then the first page in the stack layout is mapped.
")
  (:metaclass <abstract-metaclass>))


#||
define protocol <<stack-layout>> ()
  getter stack-layout-mapped-page
    (stack :: <stack-layout>)
 => (page :: false-or(<abstract-sheet>));
  setter stack-layout-mapped-page-setter
    (page :: false-or(<abstract-sheet>), stack :: <stack-layout>)
 => (page :: false-or(<abstract-sheet>));
end protocol <<stack-layout>>;
||#

(define-protocol <<stack-layout>> ()
  (:getter stack-layout-mapped-page (stack)
	   (:documentation
"
Returns the currently mapped page for the specified stack layout.

Defined by: <<STACK-LAYOUT>>
"))
  (:setter (setf stack-layout-mapped-page) (page stack)
	   (:documentation
"
Sets the mapped page for the specified stack layout to _page_.

Defined by: <<STACK-LAYOUT>>
")))

#||
define method initialize
    (pane :: <stack-layout>, #key mapped-page :: false-or(<sheet>)) => ()
  next-method();
  let mapped-page
    = mapped-page
        | begin
	    let children = sheet-children(pane);
	    ~empty?(children) & children[0]
	  end;
  stack-layout-mapped-page(pane) := mapped-page
end method initialize;
||#

(defmethod initialize-instance :after ((pane <stack-layout>) &key mapped-page &allow-other-keys)
  (let ((mapped-page (or mapped-page
			 (let ((children (sheet-children pane)))
			   (and (not (empty? children))
				(SEQUENCE-ELT children 0))))))
    (setf (stack-layout-mapped-page pane) mapped-page)))


#||
//--- Children get added withdrawn, and need to be mapped using
//--- stack-layout-mapped-page-setter.
define method note-child-added
    (sheet :: <stack-layout>, child :: <sheet>) => ()
  next-method();
  sheet-withdrawn?(child) := #t
end method note-child-added;
||#

;;--- Children get added withdrawn, and need to be mapped using
;;--- stack-layout-mapped-page-setter.
(defmethod note-child-added ((sheet <stack-layout>) (child <sheet>))
  (call-next-method)
  (setf (sheet-withdrawn? child) t))


#||
define method do-compose-space 
    (pane :: <stack-layout>,
     #key width: requested-width, height: requested-height)
 => (space-req :: <space-requirement>)
  let children = sheet-children(pane);
  if (empty?(children))
    default-space-requirement(pane, width: requested-width, height: requested-height)
  else
    let border*2 = layout-border(pane) * 2;
    let extra-width  :: <integer> = border*2;
    let extra-height :: <integer> = border*2;
    let child-width  = requested-width  & (requested-width  - extra-width);
    let child-height = requested-height & (requested-height - extra-height);
    let width      :: <integer> = 0;
    let height     :: <integer> = 0;
    let min-width  :: <integer> = 0;
    let min-height :: <integer> = 0;
    let max-width  :: <integer> = 0;
    let max-height :: <integer> = 0;
    for (child in children)
      let space-req
	= compose-space(child, width: child-width, height: child-height);
      let (w, w-, w+, h, h-, h+)
	= space-requirement-components(child, space-req);
      max!(width,      w);
      max!(min-width,  w-);
      max!(max-width,  w+);
      max!(height,     h);
      max!(min-height, h-);
      max!(max-height, h+)
    end;
    inc!(width,      extra-width);
    inc!(min-width,  extra-width);
    inc!(max-width,  extra-width);
    inc!(height,     extra-height);
    inc!(min-height, extra-height);
    inc!(max-height, extra-height);
    let width  = requested-width  | width;
    let height = requested-height | height;
    let best-width  = constrain-size(width,  min-width,  max-width);
    let best-height = constrain-size(height, min-height, max-height);
    make(<space-requirement>,
	 width:  best-width,  min-width:  min-width,  max-width:  max-width,
	 height: best-height, min-height: min-height, max-height: max-height)
  end
end method do-compose-space;
||#

(defmethod do-compose-space ((pane <stack-layout>)
			     &key ((:width requested-width)) ((:height requested-height)))
  (let ((children (sheet-children pane)))
    (if (empty? children)
	(default-space-requirement pane :width requested-width :height requested-height)
	;; else
	(let* ((border*2 (* (layout-border pane) 2))
	       (extra-width border*2)
	       (extra-height border*2)
	       (child-width  (and requested-width (- requested-width extra-width)))
	       (child-height (and requested-height (- requested-height extra-height)))
	       (width 0)
	       (height 0)
	       (min-width 0)
	       (min-height 0)
	       (max-width 0)
	       (max-height 0))
	  (map nil #'(lambda (child)
		       (let ((space-req (compose-space child :width child-width :height child-height)))
			 (multiple-value-bind (w w- w+ h h- h+)
			     (space-requirement-components child space-req)
			   (setf width (max width w))
			   (setf min-width (max min-width w-))
			   (setf max-width (max max-width w+))
			   (setf height (max height h))
			   (setf min-height (max min-height h-))
			   (setf max-height (max max-height h+)))))
	       children)
	  (incf width      extra-width)
	  (incf min-width  extra-width)
	  (incf max-width  extra-width)
	  (incf height     extra-height)
	  (incf min-height extra-height)
	  (incf max-height extra-height)
	  (let* ((cwidth (or requested-width width))
		 (cheight (or requested-height height))
		 (best-width (constrain-size cwidth  min-width  max-width))
		 (best-height (constrain-size cheight min-height max-height)))
	    (make-space-requirement :width best-width
				    :min-width min-width
				    :max-width max-width
				    :height best-height
				    :min-height min-height
				    :max-height max-height))))))


#||
define method do-allocate-space
    (pane :: <stack-layout>, width :: <integer>, height :: <integer>) => ()
  let border   = layout-border(pane);
  let border*2 = border * 2;
  let width  = width  - border*2;
  let height = height - border*2;
  for (child in sheet-children(pane))
    let space-req = compose-space(child, width: width, height: height);
    let (w, w-, w+, h, h-, h+) = space-requirement-components(child, space-req);
    ignore(w-, w+, h-, h+);
    set-sheet-edges(child, border, border, border + w, border + h)
  end
end method do-allocate-space;
||#

(defmethod do-allocate-space ((pane <stack-layout>) (width integer) (height integer))
  (let* ((border (layout-border pane))
	 (border*2 (* border 2))
	 (width (- width border*2))
	 (height (- height border*2)))
    (map nil #'(lambda (child)
		 ;; what about withdrawn children?
		 (let ((space-req (compose-space child :width width :height height)))
		   (multiple-value-bind (w w- w+ h h- h+)
		       (space-requirement-components child space-req)
		     (declare (ignore w- w+ h- h+))
		     ;; force child to be wholly contained in the parent
		     ;; stack layout pane.
		     (setf w (min w width))
		     (setf h (min h height))
		     (set-sheet-edges child border border (+ border w) (+ border h)))))
	 (sheet-children pane))))


#||
/// Default implementation

define sealed class <stack-layout-pane> (<stack-layout>)
  keyword accepts-focus?: = #f;
end class <stack-layout-pane>;
||#

(defclass <stack-layout-pane> (<stack-layout>)
  ()
  (:default-initargs :accepts-focus? nil))


#||
define method class-for-make-pane 
    (framem :: <frame-manager>, class == <stack-layout>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<stack-layout-pane>, #f)
end method class-for-make-pane;

define sealed domain make (singleton(<stack-layout-pane>));
define sealed domain initialize (<stack-layout-pane>);
||#

(defmethod class-for-make-pane ((framem <frame-manager>) (class (eql (find-class '<stack-layout>))) &key)
  (values (find-class '<stack-layout-pane>) nil))


#||
define sealed method stack-layout-mapped-page
    (stack :: <stack-layout>)
 => (page :: false-or(<sheet>))
  block (return)
    for (child :: <basic-sheet> in sheet-children(stack))
      unless (sheet-withdrawn?(child))
	return(child)
      end
    end
  end
end method stack-layout-mapped-page;
||#

(defmethod stack-layout-mapped-page ((stack <stack-layout>))
  (map nil #'(lambda (child)
	       (unless (sheet-withdrawn? child)
		 (return-from stack-layout-mapped-page child)))
       (sheet-children stack)))


#||
define sealed method stack-layout-mapped-page-setter
    (page :: false-or(<sheet>), stack :: <stack-layout>)
 => (page :: false-or(<sheet>))
  let damaged-width  :: <integer> = 0;
  let damaged-height :: <integer> = 0;
  let old-page = #f;
  // Be conservative and ensure that we start with everything withdrawn
  for (child :: <basic-sheet> in sheet-children(stack))
    if (~sheet-withdrawn?(child))
      old-page := child;
      if (child ~= page)
	sheet-withdrawn?(child, do-repaint?: #f) := #t;
	let (child-width, child-height) = sheet-size(child);
	max!(damaged-width, child-width);
	max!(damaged-height, child-height);
      end
    end
  end;
  when (page & page ~= old-page)
    // Un-withdraw the new child so that we can do layout, if necessary
    sheet-withdrawn?(page) := #f;
    when (sheet-attached?(stack))
      let (width, height) = sheet-size(stack);
      // Note that this apparent re-layout will be quite inexpensive
      // because we've already layed out all the pages in the stack
      let space-req
	= compose-space(page, width: width, height: height);
      let (w, w-, w+, h, h-, h+)
	= space-requirement-components(page, space-req);
      ignore(w-, w+, h-, h+);
      // We repaint if the child isn't mirrored, or if the old damaged
      // region extends beyond the new child's own region.
      if (~sheet-direct-mirror(page) | damaged-width > w | damaged-height > h)
	clear-box(stack, 0, 0, damaged-width, damaged-height)
      end;
      set-sheet-edges(page, 0, 0, w, h);
      //---*** Can this be removed if we fix 'set-sheet-edges'?
      update-all-mirror-positions(page);
      // Now we can finally display the new child!
      sheet-mapped?(page, clear?: #f) := sheet-mapped?(stack);
    end
  end;
  page
end method stack-layout-mapped-page-setter;
||#

(defmethod (setf stack-layout-mapped-page) ((page null) (stack <stack-layout>))
  (%set-stack-layout-mapped-page-1 stack page))

(defmethod (setf stack-layout-mapped-page) ((page <sheet>) (stack <stack-layout>))
  (%set-stack-layout-mapped-page-1 stack page))

(defun %set-stack-layout-mapped-page-1 (stack page)
  (let ((damaged-width 0)
	(damaged-height 0)
	(old-page nil))
    ;; Be conservative and ensure that we start with everything withdrawn
    (map nil #'(lambda (child)
		 (if (not (sheet-withdrawn? child))
		     (progn
		       (setf old-page child)
		       (if (not (equal? child page))
			   (progn
			     (setf (sheet-withdrawn? child :do-repaint? nil) t)
			     (multiple-value-bind (child-width child-height)
				 (sheet-size child)
			       (setf damaged-width (max damaged-width child-width))
			       (setf damaged-height (max damaged-height child-height))))))))
	 (sheet-children stack))
    (when (and page (not (equal? page old-page)))
      ;; Un-withdraw the new child so that we can do layout, if necessary
      (setf (sheet-withdrawn? page) nil)
      (when (sheet-attached? stack)
	(multiple-value-bind (width height)
	    (sheet-size stack)
	  ;; Note that this apparent re-layout will be quite inexpensive
	  ;; because we've already layed out all the pages in the stack
	  (let ((space-req (compose-space page :width width :height height)))
	    (multiple-value-bind (w w- w+ h h- h+)
		(space-requirement-components page space-req)
	      (declare (ignore w- w+ h- h+))
	      ;; We repaint if the child isn't mirrored, or if the old damaged
	      ;; region extends beyond the new child's own region.
	      (when (or (not (sheet-direct-mirror page)) (> damaged-width w) (> damaged-height h))
		(clear-box stack 0 0 damaged-width damaged-height))
	      (set-sheet-edges page 0 0 w h)
	      ;;---*** Can this be removed if we fix 'set-sheet-edges'?
	      (update-all-mirror-positions page)
	      ;; Now we can finally display the new child!
	      (setf (sheet-mapped? page :clear? nil) (sheet-mapped? stack)))))))
    page))



