;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-LAYOUTS-INTERNALS -*-
(in-package #:duim-layouts-internals)

#||
/// Table panes

define open abstract class <table-layout> 
    (<layout-border-mixin>,
     <horizontal-layout-mixin>,
     <vertical-layout-mixin>,
     <layout-pane>)
  sealed slot table-contents = #f,
    setter: %contents-setter;
  sealed constant slot table-rows :: false-or(<integer>) = #f,
    init-keyword: rows:;
  sealed constant slot table-columns :: false-or(<integer>) = #f,
    init-keyword: columns:;
  sealed slot %row-space-requirements  :: false-or(<sequence>) = #f;
  sealed slot %cell-space-requirements :: false-or(<sequence>) = #f;
end class <table-layout>;
||#

(defclass <table-layout>
    (<layout-border-mixin>
     <horizontal-layout-mixin>
     <vertical-layout-mixin>
     <layout-pane>)
  ((table-contents :initform nil :reader table-contents :writer %contents-setter)
   (table-rows :type (or null integer) :initarg :rows :initform nil :reader table-rows)
   (table-columns :type (or null integer) :initarg :columns :initform nil :reader table-columns)
   (%row-space-requirements :type (or null sequence) :initform nil :accessor %row-space-requirements)
   (%cell-space-requirements :type (or null sequence) :initform nil :accessor %cell-space-requirements))
  (:documentation
"
The class of table layouts.

The :border initarg provides a border of whitespace around the
children in the layout, and the value of this initarg represents the
size of the border in pixels. This basically has the same effect as
using the macro WITH-SPACING around the layout, except it uses a
simpler syntax.

The :rows and :columns initargs are used to specify the number of rows
and columns for the table layout.

The :contents initarg is used to specify the contents of each cell of
the table. It should consist of a sequence of sequences of
sheets. If :contents is not specified, you should supply the children
of the table with a number of rows and columns. You should not supply
both children and rows and columns, however.

The :x-spacing and :y-spacing initargs let you specify how much
vertical and horizontal space should be inserted, in pixels, between
the children of the layout.

THe :x-ratios and :y-ratios initargs let you specify the proportion of
the total horizontal and vertical space that should be taken up by
each individual child.

The two initargs can be used on their own, or together, as described
in the examples below.

For example, if the value #(1 2 3) is specified for the :x-ratios
initarg of a table layout containing three columns of children, then
the first column would claim a sixth of the available horizontal
space, the second column would claim a third of the horizontal space,
and the third column would claim half the horizontal space, as shown
in the diagram below.

                         :x-ratios #(1 2 3)
           +-------+--------------+---------------------+
           |column1|   column 2   |       column 3      |
           +-------+--------------+---------------------+

Alternatively, if the value #(1 2 3) is specified for the :y-ratios
initarg of a table layout containing three rows of children, then the
first row would claim a sixth of the available vertical space, the
second row would claim a third of the vertical space, and the third
row would claim half the vertical space, as shown in the diagram
below.

  +-----------+
  |   row 1   |
  +-----------+
  |   row 2   |
  |           |
  +-----------+   :y-ratios #(1 2 3)
  |           |
  |   row 3   |
  |           |
  +---------- +

Finally, if both the :x-ratios and :y-ratios initargs are specified,
then each child in the layout is affected individually, as shown in
the diagram below.

               :x-ratios #(1 2 3)
 +-------+--------------+---------------------+
 |       |              |                     |
 +-------+--------------+---------------------+
 |       |              |                     |
 |       |              |                     | :y-ratios #(1 2 3)
 +-------+--------------+---------------------+
 |       |              |                     |
 |       |              |                     |
 |       |              |                     |
 +-------+--------------+---------------------+

By default, all the children of a table layout are left-aligned. You
can specify that they should be right or center-aligned using
the :x-alignment keyword.

By default, all the children of a table layout are aligned at the
top. You can specify that they should be aligned at the bottom, or in
the center, using the :y-alignment keyword.

Example:

    (defparameter *table-children* (make-array 9))
    (loop for i from 1 to 9
          do (setf (aref *table-children* (1- i))
                   (make-pane '<button> :label (format nil \"~d\" i))))

    (contain (make-pane '<table-layout>
                        :x-spacing 10 :y-spacing 0
                        :children *table-children* :columns 3))

TODO: Test this example actually works!
")
  (:metaclass <abstract-metaclass>))


;;; TODO: Need to add this documentation for TABLE-CONTENTS:
#||
Returns the contents of _table_.
||#

(defgeneric (setf table-contents) (contents table)
  (:documentation
"
Sets the contents of _table_.
"))

(defgeneric table-end-position (table))
(defgeneric find-table-child (contents child))


#||
define method initialize 
    (table :: <table-layout>, #key contents, children, rows, columns) => ()
  // Either the contents should be a sequence of sequences of sheets,
  // or supply children with a number of rows and columns
  assert(~(contents & children),
	 "You can't supply both contents and children to a table pane");
  next-method();
  when (contents | (children & ~empty?(children)))
    // Compute size of contents array and allocate it
    if (children)
      case
	rows    => columns := columns | ceiling/(size(children), rows);
	columns => rows    := rows    | ceiling/(size(children), columns);
	otherwise => error("You must supply either rows or columns for table panes");
      end
    elseif (contents)
      select (contents by instance?)
	<sequence> =>
	  rows    := size(contents);
	  columns := if (empty?(contents))
		       0		// he asked for a useless table
		     else
		       reduce(method (v, x) max(v, size(x)) end, 0, contents)
		     end;
	<array> => 
	  rows    := dimension(contents, 0);
	  columns := dimension(contents, 1);
      end
    end;
    let rows    :: <integer> = rows;	// tighten up the types
    let columns :: <integer> = columns;
    table.%contents := make(<array>,
			    dimensions: list(rows, columns));
    if (children)
      // Initialize contents from a sequence of children
      assert(size(children) <= rows * columns,
	     "You are giving too many children to %=", table);
      fill-array!(table-contents(table), children)
    elseif (contents & ~empty?(contents))
      select (contents by instance?)
	<sequence> =>
	  // Initialize children nested sequence of contents
	  for (cells in contents,	// a row of panes, that is...
	       row :: <integer> from 0)
	    for (cell in cells,		// a cell is one pane in a row
		 column :: <integer> from 0)
	      when (cell)
		add-child(table, cell, row: row, column: column)
	      end
	    end
	  end;
	<array> => 
	  // Initialize children from a contents array
	  for (row :: <integer> from 0 below rows)
	    for (column :: <integer> from 0 below columns)
	      let cell = contents[row, column];
	      when (cell)
		add-child(table, cell, row: row, column: column)
	      end
	    end
	  end
      end
    end
  end
end method initialize;
||#

(defmethod initialize-instance ((table <table-layout>) &key contents children rows columns &allow-other-keys)
  (declare (ignore rows columns))
  ;; Either the contents should be a sequence of sequences of sheets,
  ;; or supply children with a number of rows and columns
  (when (and contents children)
    (error "You can't supply both contents and children to a table pane"))
  (call-next-method))

;; FIXME: What types are CONTENTS and CHILDREN? Children should probably be a list...
;; FIXME: Maybe we should accept any old sequence type and convert all to lists? Not sure...
(defmethod initialize-instance :after ((table <table-layout>) &key contents children rows columns
				       &allow-other-keys)
  (when (or contents (and children (not (empty? children))))
    ;; Compute size of contents array and allocate it
    (when children
      (cond (rows (setf columns (or columns (ceiling (length children) rows))))
	    (columns (setf rows (or rows (ceiling (length children) columns))))
	    (t (error "You must supply either rows or columns for table panes"))))
    (when (and (not children) contents)
      (etypecase contents
	(sequence (setf rows (length contents))
		  (setf columns (if (empty? contents)
				    0     ;; he asked for a useless table
				    (reduce #'(lambda (v x) (max v (length x))) contents :initial-value 0))))
	(array  ;; 2d-array... 1d array is handled by the sequence case above
	 (setf rows (array-dimension contents 0))
	 (setf columns (array-dimension contents 1)))))
    (%contents-setter (make-array (list rows columns) :initial-element nil) table)
    (when children
      ;; Initialize contents from a sequence of children
      (unless (<= (length children) (* rows columns))
	(error "You are giving too many children to ~a" table))
      (fill-array! (table-contents table) children))
    (when (and (not children) contents (not (empty? contents)))
      (etypecase contents
	(sequence
	 ;; Initialize children nested sequence of contents
	 (loop for cells across contents	;; a row of panes, that is...
	    and row from 0
	    do (loop for cell across cells		;; a cell is one pane in a row
		  and column from 0
		  do (when cell
		       (add-child table cell :row row :column column)))))
	  (array  ;; 2d array... 1d array is dealt with in sequence case
	   ;; Initialize children from a contents array
	   (loop for row from 0 below rows
	      do (loop for column from 0 below columns
		    do (let ((cell (aref contents row column)))
			 (when cell
			   (add-child table cell :row row :column column))))))))))


#||
// Don't change the order of the sheets, because that will break the layout
// Note that 'raise-sheet' will arrange to raise mirrors, which is OK
define method do-raise-sheet
    (parent :: <table-layout>, sheet :: <sheet>, #key activate? = #t) => ()
  ignore(activate?);
  #f
end method do-raise-sheet;
||#

;; Don't change the order of the sheets, because that will break the layout
;; Note that 'raise-sheet' will arrange to raise mirrors, which is OK
(defmethod do-raise-sheet ((parent <table-layout>) (sheet <sheet>) &key (activate? t))
  (declare (ignore activate?))
  nil)


#||
define method table-contents-setter 
    (contents :: <array>, table :: <table-layout>)
  assert(size(dimensions(contents)) = 2,
         "You are supplying a non-two dimensional array for %=", table);
  table.%contents := contents
end method table-contents-setter;
||#

;; FIXME: Should we allow lists of lists?
(defmethod (setf table-contents) ((contents array) (table <table-layout>))
  (unless (= (length (array-dimensions contents)) 2)
    (error "You are supplying a non-two dimensional array for ~a" table))
  (%contents-setter contents table))


#||
define method sheet-children-setter
    (children :: <sequence>, table :: <table-layout>) => (children :: <sequence>)
  let rows    = table-rows(table);
  let columns = table-columns(table);
  case
    rows    => columns := columns | ceiling/(size(children), rows);
    columns => rows    := rows    | ceiling/(size(children), columns);
    otherwise => error("You must supply either rows or columns for table panes");
  end;
  next-method();
  table.%contents := make(<array>, dimensions: list(rows, columns));
  fill-array!(table-contents(table), children);
  children
end method sheet-children-setter;
||#

;; FIXME: WE'RE TRYING TO ALLOW EITHER AN ARRAY OR A LIST HERE. MAYBE WE SHOULDN'T
;; BOTHER...
(defmethod (setf sheet-children) ((children array) (table <table-layout>))
  (let ((rows (table-rows table))
	(columns (table-columns table)))
    (cond (rows (setf columns (or columns (ceiling (length children) rows))))
	  (columns (setf rows (or rows (ceiling (length children) columns))))
	  (t (error "You must supply either rows or columns for table panes")))
    (call-next-method)
    (%contents-setter (make-array (list rows columns)) table)
    (fill-array! (table-contents table) children)
    children))

(defmethod (setf sheet-children) ((children sequence) (table <table-layout>))
  (let ((rows (table-rows table))
	(columns (table-columns table)))
    (cond (rows (setf columns (or columns (ceiling (length children) rows))))
	  (columns (setf rows (or rows (ceiling (length children) columns))))
	  (t (error "You must supply either rows or columns for table panes")))
    (call-next-method)
    (%contents-setter (make-array (list rows columns)) table)
    (fill-array! (table-contents table) children)
    children))


#||
define method table-end-position
    (table :: <table-layout>) => (index :: <integer>)
  block (return)
    let contents = table-contents(table);
    let nrows  :: <integer> = dimension(contents, 1);
    let ncells :: <integer> = dimension(contents, 0) * nrows;
    // Finds the first empty cell at the _end_ of the table
    for (index :: <integer> from ncells - 1 to 0 by -1)
      let (row, column) = floor/(index, nrows);
      when (contents[row, column])
	return(if (index < ncells - 1)
		 index + 1
	       else
		 error("The table pane %= is full", table)
	       end)
      end
    end;
    // If the table is empty, the last shall be the first
    0
  end
end method table-end-position;
||#

(defmethod table-end-position ((table <table-layout>))
  (let* ((contents (table-contents table))
	 (nrows (array-dimension contents 1))
	 (ncells (* (array-dimension contents 0) nrows)))
    ;; Finds the first empty cell at the _end_ of the table
    (loop for index from (- ncells 1) downto 0 ;; by 1 ;; -1
       do (multiple-value-bind (row column)
	      (floor index nrows)
	    (when (aref contents row column)
	      (return-from table-end-position (if (< index (- ncells 1))
						  (+ index 1)
						  (error "The table pane ~a is full" table))))))
    ;; If the table is empty, the last shall be the first
    0))


#||
define method do-add-child
    (table :: <table-layout>, child :: <sheet>,
     #key index = #"end", row, column) => ()
  let contents = table-contents(table);
  unless (row & column)
    let nrows = dimension(contents, 0);
    let index = select (index)
		  #"start"  => 0;
		  #"end"    => table-end-position(table);
		  otherwise => index;
		end;
    let (the-column, the-row) = floor/(index, nrows);
    row := the-row;
    column := the-column;
  end;
  assert(~contents[row, column],
	 "Attempting to replace an existing child in %= using ADD-CHILD", table);
  contents[row, column] := child;
  next-method()
end method do-add-child;
||#

(defmethod do-add-child ((table <table-layout>) (child <sheet>)
			 &key (index :end) row column)
  (let ((contents (table-contents table)))
    (unless (and row column)
      (let ((nrows (array-dimension contents 0))
	    (index (case index (:start 0)
			 (:end (table-end-position table))
			 (t index))))
	(multiple-value-bind (the-column the-row)
	    (floor index nrows)
	  (setf row the-row)
	  (setf column the-column))))
    (unless (not (aref contents row column))
      ;; unless the cell is empty, signal an error
      (error "Attempting to replace an existing child in ~a using ADD-CHILD" table))
    (setf (aref contents row column) child))
  (call-next-method))


#||
define method do-remove-child 
    (table :: <table-layout>, child :: <sheet>) => ()
  let contents = table-contents(table);
  let (row, col) = find-table-child(contents, child);
  when (row)
    contents[row, col] := #f
  end;
  next-method();
end method do-remove-child;
||#

(defmethod do-remove-child ((table <table-layout>) (child <sheet>))
  (let ((contents (table-contents table)))
    (multiple-value-bind (row col)
	(find-table-child contents child)
      (when row
	(setf (aref contents row col) nil))))
  (call-next-method))


#||
define method do-replace-child
    (table :: <table-layout>, old-child :: <sheet>, new-child :: <sheet>) => ()
  let contents = table-contents(table);
  let (row, col) = find-table-child(contents, old-child);
  contents[row, col] := new-child;
  next-method()
end method do-replace-child;
||#

(defmethod do-replace-child ((table <table-layout>) (old-child <sheet>) (new-child <sheet>))
  (let ((contents (table-contents table)))
    (multiple-value-bind (row col)
	(find-table-child contents old-child)
      (setf (aref contents row col) new-child)))
  (call-next-method))


#||
define method find-table-child
    (contents :: <array>, child) => (row, col)
  let nrows :: <integer> = dimension(contents, 0);
  let ncols :: <integer> = dimension(contents, 1);
  block (return)
    for (row :: <integer> from 0 below nrows)
      for (col :: <integer> from 0 below ncols)
	when (contents[row, col] == child)
	  return(row, col)
	end
      end
    end;
    values(#f, #f)
  end
end method find-table-child;
||#

(defmethod find-table-child ((contents array) child)
  (let ((nrows (array-dimension contents 0))
	(ncols (array-dimension contents 1)))
    (loop for row from 0 below nrows
       do (loop for col from 0 below ncols
	     do (when (eql (aref contents row col) child)
		  (return-from find-table-child (values row col))))))
  (values nil nil))


#||
// Options can be any of the pane sizing options
// Note that this has no syntax 
define macro tabling
  { tabling (#rest ?options:expression)
      ?contents:*
    end }
    => { make(<table-layout>, children: vector(?contents), ?options) }
 contents:
  { } => { }
  { ?pane-spec:*; ... } => { ?pane-spec, ... }
 pane-spec:
  { ?pane:expression } => { ?pane }
end macro tabling;
||#

(defmacro tabling ((&rest options) &body contents)
"
This macro lays a series of gadgets out in a table, creating the
necessary layouts for you automatically.

The _options_ are passed directly to the table layout, and thus can be
any legitimate combinations of initargs for <TABLE-LAYOUT>. If no
options are specified, then the default values for table layout are
used.

The _contents_ argument consists of a number of Lisp expressions, each
of which creates an instance of a gadget or layout that is to be
included in the vertical layout.
"
  `(make-pane '<table-layout>
	      :children (vector ,@contents)
	      ,@options))


#||
define method do-compose-space
    (table :: <table-layout>, #key width, height)
 => (space-req :: <space-requirement>)
  let contents = table-contents(table);
  if (~contents | empty?(contents))
    default-space-requirement(table, width: width, height: height)
  else
    let nrows :: <integer> = dimension(contents, 0);
    let ncols :: <integer> = dimension(contents, 1);
    // Overall preferred/min/max width and height
    let ow     :: <integer> = 0;
    let omin-w :: <integer> = 0;
    let omax-w :: <integer> = 0;
    let oh     :: <integer> = 0;
    let omin-h :: <integer> = 0;
    let omax-h :: <integer> = 0;
    let row-srs  :: <stretchy-object-vector> = make(<stretchy-vector>);
    let cell-srs :: <stretchy-object-vector> = make(<stretchy-vector>);
    // Iterate over the rows, determining the height of each
    for (row :: <integer> from 0 below nrows)
      let height     :: <integer> = 0;
      let min-height :: <integer> = 0;
      let max-height :: <integer> = 0;
      for (cell :: <integer> from 0 below ncols)
	let item = contents[row, cell];
	when (item & ~sheet-withdrawn?(item))
	  let space-req = compose-space(item);
	  let (w, w-, w+, h, h-, h+) = space-requirement-components(item, space-req);
	  ignore(w, w-, w+);
	  // Max the heights
	  max!(height,     h);
	  max!(min-height, h-);
	  // That max height of the row is the largest max height
	  // for any cell in the row
	  max!(max-height, h+)
	end
      end;
      add!(row-srs,
	   make(<space-requirement>,
		width: 0,
		height: height, min-height: min-height, max-height: max-height));
      // Add the heights
      inc!(oh,     height);
      inc!(omin-h, min-height);
      inc!(omax-h, max-height)
    end;
    table.%row-space-requirements := row-srs;
    // Iterate over the cells determing the widths of each
    for (cell :: <integer> from 0 below ncols)
      let width     :: <integer> = 0;
      let min-width :: <integer> = 0;
      let max-width :: <integer> = 0;
      for (row :: <integer> from 0 below nrows)
	let item = contents[row, cell];
	when (item & ~sheet-withdrawn?(item))
	  let space-req = compose-space(item);
	  let (w, w-, w+, h, h-, h+) = space-requirement-components(item, space-req);
	  ignore(h, h-, h+);
	  // Max the widths
	  max!(width,     w);
	  max!(min-width, w-);
	  // That max width of the column is the largest max height
	  // for any cell in the column
	  max!(max-width, w+)
	end
      end;
      add!(cell-srs,
	   make(<space-requirement>,
		width: width, min-width: min-width, max-width: max-width,
		height: 0));
      inc!(ow,     width);
      inc!(omin-w, min-width);
      inc!(omax-w, max-width)
    end;
    let border*2 = layout-border(table) * 2;
    let total-x-spacing :: <integer>
      = (layout-x-spacing(table) * (ncols - 1)) + border*2;
    let total-y-spacing :: <integer>
      = (layout-y-spacing(table) * (nrows - 1)) + border*2;
    inc!(ow,     total-x-spacing);
    inc!(omin-w, total-x-spacing);
    inc!(omax-w, total-x-spacing);
    inc!(oh,     total-y-spacing);
    inc!(omin-h, total-y-spacing);
    inc!(omax-h, total-y-spacing);
    when (width)
      ow := max(omin-w, min(width, omax-w))
    end;
    when (height)
      oh := max(omin-h, min(height, omax-h))
    end;
    table.%cell-space-requirements := cell-srs;
    make(<space-requirement>,
	 width:  ow, min-width:  omin-w, max-width:  omax-w,
	 height: oh, min-height: omin-h, max-height: omax-h)
  end
end method do-compose-space;
||#

(defmethod do-compose-space ((table <table-layout>) &key width height)
  (let ((contents (table-contents table)))
    (if (or (not contents) (empty? contents))
	(default-space-requirement table :width width :height height)
	(let* ((nrows (array-dimension contents 0))
	       (ncols (array-dimension contents 1))
	       ;; Overall preferred/min/max width and height
	       (ow     0)
	       (omin-w 0)
	       (omax-w 0)
	       (oh     0)
	       (omin-h 0)
	       (omax-h 0)
	       (row-srs  (MAKE-STRETCHY-VECTOR))
	       (cell-srs (MAKE-STRETCHY-VECTOR)))
	  ;; Iterate over the rows, determining the height of each
	  (loop for row from 0 below nrows
	     do (let ((_height 0)
		      (min-height 0)
		      (max-height 0))
		  (loop for cell from 0 below ncols
		     do (let ((item (aref contents row cell)))
			  (when (and item (not (sheet-withdrawn? item)))
			    (let ((space-req (compose-space item)))
			      (multiple-value-bind (w w- w+ h h- h+)
				  (space-requirement-components item space-req)
				(declare (ignore w w- w+))
				;; Max the heights
				(setf _height (max _height h))
				(setf min-height (max min-height h-))
				;; That max height of the row is the largest max
				;; height for any cell in the row
				(setf max-height (max max-height h+)))))))
		  (add! row-srs (make-space-requirement :width 0
					       :height _height :min-height min-height :max-height max-height))
		  ;; Add the heights
		  (incf oh     _height)
		  (incf omin-h min-height)
		  (incf omax-h max-height)))
	  (setf (%row-space-requirements table) row-srs)
	  ;; Iterate over the cells determing the widths of each
	  (loop for cell from 0 below ncols
	     do (let ((_width 0)
		      (min-width 0)
		      (max-width 0))
		  (loop for row from 0 below nrows
		     do (let ((item (aref contents row cell)))
			  (when (and item (not (sheet-withdrawn? item)))
			    (let ((space-req (compose-space item)))
			      (multiple-value-bind (w w- w+ h h- h+)
				  (space-requirement-components item space-req)
				(declare (ignore h h- h+))
				;; Max the widths
				(setf _width (max _width w))
				(setf min-width (max min-width w-))
				;; That max width of the column is the largest
				;; max width for any cell in the column
			      (setf max-width (max max-width w+)))))))
		  (add! cell-srs (make-space-requirement
						:width _width :min-width min-width :max-width max-width
						:height 0))
		  (incf ow     _width)
		  (incf omin-w min-width)
		  (incf omax-w max-width)))
	  (let* ((border*2 (* (layout-border table) 2))
		 (total-x-spacing (+ (* (layout-x-spacing table) (- ncols 1)) border*2))
		 (total-y-spacing (+ (* (layout-y-spacing table) (- nrows 1)) border*2)))
	    (incf ow     total-x-spacing)
	    (incf omin-w total-x-spacing)
	    (incf omax-w total-x-spacing)
	    (incf oh     total-y-spacing)
	    (incf omin-h total-y-spacing)
	    (incf omax-h total-y-spacing)
	    ;; Deal with supplied width + height
	    (when width
	      (setf ow (max omin-w (min width omax-w))))
	    (when height
	      (setf oh (max omin-h (min height omax-h))))
	    (setf (%cell-space-requirements table) cell-srs)
	    (make-space-requirement
			   :width  ow :min-width  omin-w :max-width  omax-w
			   :height oh :min-height omin-h :max-height omax-h))))))


#||
define method do-allocate-space
    (table :: <table-layout>, width :: <integer>, height :: <integer>) => ()
  let contents = table-contents(table);
  when (contents & ~empty?(contents))
    let space-req = compose-space(table, width: width, height: height);
    let nrows :: <integer> = dimension(contents, 0);
    let ncols :: <integer> = dimension(contents, 1);
    let x-spacing :: <integer> = layout-x-spacing(table);
    let y-spacing :: <integer> = layout-y-spacing(table);
    let border    :: <integer> = layout-border(table);
    let total-x-spacing :: <integer> = x-spacing * (ncols - 1);
    let total-y-spacing :: <integer> = y-spacing * (nrows - 1);
    let row-heights
      = compose-space-for-items
	  (table,
	   height - total-y-spacing, space-req, table.%row-space-requirements,
	   space-requirement-height, space-requirement-min-height, space-requirement-max-height,
	   identity,
	   ratios: layout-y-ratios(table));
    let cell-widths
      = compose-space-for-items
	  (table,
	   width - total-x-spacing, space-req, table.%cell-space-requirements,
	   space-requirement-width, space-requirement-min-width, space-requirement-max-width,
	   identity,
	   ratios: layout-x-ratios(table));
    let y :: <integer> = border;
    for (row :: <integer> from 0 below nrows,
	 row-height :: <integer> in row-heights)
      let cell-widths = cell-widths;
      let x :: <integer> = border;
      for (cell :: <integer> from 0 below ncols,
	   cell-width :: <integer> in cell-widths)
        let item = contents[row, cell];
        when (item & ~sheet-withdrawn?(item))
          let item-space
            = compose-space(item, width: cell-width, height: row-height);
	  let (w, w-, w+, h, h-, h+) = space-requirement-components(item, item-space);
	  ignore(w, h);
	  let item-width  :: <integer> = constrain-size(cell-width, w-, w+);
          let item-height :: <integer> = constrain-size(row-height, h-, h+);
	  let aligned-x :: <integer>
	    = layout-align-sheet-x(table, item,
				   x, x + cell-width - item-width,
				   key: cell);
	  let aligned-y :: <integer>
	    = layout-align-sheet-y(table, item,
				   y, y + row-height - item-height,
				   key: row);
	  set-sheet-edges(item,
			  aligned-x, aligned-y,
			  // Ensure end cells stay within table
			  min(aligned-x + item-width, width),
			  min(aligned-y + item-height, height))
	end;
	inc!(x, cell-width + x-spacing)
      end;
      inc!(y, row-height + y-spacing)
    end
  end
end method do-allocate-space;
||#

(defmethod do-allocate-space ((table <table-layout>) (width integer) (height integer))
  (let ((contents (table-contents table)))
    (when (and contents (not (empty? contents)))
      (let* ((space-req (compose-space table :width width :height height))
	     (nrows (array-dimension contents 0))
	     (ncols (array-dimension contents 1))
	     (x-spacing (layout-x-spacing table))
	     (y-spacing (layout-y-spacing table))
	     (border (layout-border table))
	     (total-x-spacing (* x-spacing (- ncols 1)))
	     (total-y-spacing (* y-spacing (- nrows 1)))
	     (row-heights (compose-space-for-items table
						   (- height total-y-spacing) space-req (%row-space-requirements table)
						   #'space-requirement-height #'space-requirement-min-height #'space-requirement-max-height
						   #'identity
						   :ratios (layout-y-ratios table)))
	     (cell-widths (compose-space-for-items table
						   (- width total-x-spacing) space-req (%cell-space-requirements table)
						   #'space-requirement-width #'space-requirement-min-width #'space-requirement-max-width
						   #'identity
						   :ratios (layout-x-ratios table)))
	     (y border))
	(loop for row from 0 below nrows
	   for row-height across row-heights
	   do (let ((cell-widths cell-widths)
		    (x border))
		(loop for cell from 0 below ncols
		   for cell-width across cell-widths
		   do (let ((item (aref contents row cell)))
			(when (and item (not (sheet-withdrawn? item)))
			  (let ((item-space (compose-space item :width cell-width :height row-height)))
			    (multiple-value-bind (w w- w+ h h- h+)
				(space-requirement-components item item-space)
			      (declare (ignore w h))
			      (let* ((item-width (constrain-size cell-width w- w+))
				     (item-height (constrain-size row-height h- h+))
				     (aligned-x (layout-align-sheet-x table item
								      x (- (+ x cell-width) item-width)
								      :key cell))
				     (aligned-y (layout-align-sheet-y table item
								      y (- (+ y row-height) item-height)
								      :key row)))
				(set-sheet-edges item
						 aligned-x aligned-y
						 ;; Ensure end cells stay within table
						 (min (+ aligned-x item-width) width)
						 (min (+ aligned-y item-height) height)))))))
		   do (incf x (+ cell-width x-spacing))))
	   do (incf y (+ row-height y-spacing)))))))


#||
/// Default implementation

define sealed class <table-layout-pane> (<table-layout>)
  keyword accepts-focus?: = #f;
end class <table-layout-pane>;
||#

(defclass <table-layout-pane> (<table-layout>)
  ()
  (:default-initargs :accepts-focus? nil))


#||
define method class-for-make-pane 
    (framem :: <frame-manager>, class == <table-layout>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<table-layout-pane>, #f)
end method class-for-make-pane;

define sealed domain make (singleton(<table-layout-pane>));
define sealed domain initialize (<table-layout-pane>);
||#

(defmethod class-for-make-pane ((framem <frame-manager>) (class (eql (find-class '<table-layout>))) &key)
  (values (find-class '<table-layout-pane>) nil))



#||

/// Grid panes

define open abstract class <grid-layout> (<table-layout>)
  sealed slot cell-space-requirement :: <space-requirement>,
    required-init-keyword: cell-space-requirement:;
end class <grid-layout>;
||#

(defclass <grid-layout> (<table-layout>)
  ((cell-space-requirement :type <space-requirement> :initarg :cell-space-requirement
			   :initform (required-slot ":cell-space-requirement" "<grid-layout>")
			   :accessor cell-space-requirement))
  (:documentation
"
The class of grid layouts. A grid layout arranges its children in a
grid, automatically calculating the size and placement of each child
within the specified parameters.

The :cell-space-requirement initarg lets you specify the preferred
space requirement for any individual cell in the grid layout.
")
  (:metaclass <abstract-metaclass>))


#||
define method do-compose-space
    (grid :: <grid-layout>, #key width, height)
 => (space-req :: <space-requirement>)
  let contents = table-contents(grid);
  if (~contents | empty?(contents))
    default-space-requirement(grid, width: width, height: height)
  else
    let nrows :: <integer> = dimension(contents, 0);
    let ncols :: <integer> = dimension(contents, 1);
    let border*2 = layout-border(grid) * 2;
    let total-x-spacing :: <integer>
      = (layout-x-spacing(grid) * (ncols - 1)) + border*2;
    let total-y-spacing :: <integer>
      = (layout-y-spacing(grid) * (nrows - 1)) + border*2;
    let (width :: <integer>, min-width :: <integer>, max-width :: <integer>,
	 height :: <integer>, min-height :: <integer>, max-height :: <integer>)
      = space-requirement-components(grid, cell-space-requirement(grid));
    width      := width      * ncols + total-x-spacing;
    min-width  := min-width  * ncols + total-x-spacing;
    min-width  := max-width  * ncols + total-x-spacing;
    height     := height     * nrows + total-y-spacing;
    min-height := min-height * nrows + total-y-spacing;
    min-height := max-height * nrows + total-y-spacing;
    make(<space-requirement>,
	 width:  width,  min-width:  min-width,  max-width:  max-width,
	 height: height, min-height: min-height, max-height: max-height)
  end
end method do-compose-space;
||#

(defmethod do-compose-space ((grid <grid-layout>) &key width height)
  (let ((contents (table-contents grid)))
    (if (or (not contents) (empty? contents))
	(default-space-requirement grid :width width :height height)
	(let* ((nrows (array-dimension contents 0))
	       (ncols (array-dimension contents 1))
	       (border*2 (* (layout-border grid) 2))
	       (total-x-spacing (+ (* (layout-x-spacing grid) (- ncols 1)) border*2))
	       (total-y-spacing (+ (* (layout-y-spacing grid) (- nrows 1)) border*2)))
	  (multiple-value-bind (width min-width max-width height min-height max-height)
	      (space-requirement-components grid (cell-space-requirement grid))
	    (setf width      (+ (* width      ncols) total-x-spacing))
	    (setf min-width  (+ (* min-width  ncols) total-x-spacing))
	    (setf min-width  (+ (* max-width  ncols) total-x-spacing))
	    (setf height     (+ (* height     nrows) total-y-spacing))
	    (setf min-height (+ (* min-height nrows) total-y-spacing))
	    (setf min-height (+ (* max-height nrows) total-y-spacing))
	    (make-space-requirement
			   :width width :min-width min-width :max-width max-width
			   :height height :min-height min-height :max-height max-height))))))


#||
define method do-allocate-space
    (grid :: <grid-layout>, width :: <integer>, height :: <integer>) => ()
  let contents = table-contents(grid);
  when (contents & ~empty?(contents))
    let nrows  :: <integer> = dimension(contents, 0);
    let ncols  :: <integer> = dimension(contents, 1);
    let border :: <integer> = layout-border(grid);
    let x-spacing :: <integer> = layout-x-spacing(grid);
    let y-spacing :: <integer> = layout-y-spacing(grid);
    let (cell-width  :: <integer>, min-width, max-width,
	 cell-height :: <integer>, min-height, max-height)
      = space-requirement-components(grid, cell-space-requirement(grid));
    ignore(min-width, max-width, min-height, max-height);
    //--- This should give extra space to the cell, obeying min and max sizes
    let y :: <integer> = border;
    for (row :: <integer> from 0 below nrows)
      let x :: <integer> = border;
      for (cell :: <integer> from 0 below ncols)
        let item = contents[row, cell];
        when (item & ~sheet-withdrawn?(item))
	  set-sheet-edges(item, x, y, x + cell-width, y + cell-height)
	end;
	inc!(x, cell-width + x-spacing)
      end;
      inc!(y, cell-height + y-spacing)
    end
  end
end method do-allocate-space;
||#

(defmethod do-allocate-space ((grid <grid-layout>) (width integer) (height integer))
  (let ((contents (table-contents grid)))
    (when (and contents (not (empty? contents)))
      (let* ((nrows (array-dimension contents 0))
	     (ncols (array-dimension contents 1))
	     (border (layout-border grid))
	     (x-spacing (layout-x-spacing grid))
	     (y-spacing (layout-y-spacing grid)))
	(multiple-value-bind (cell-width min-width max-width cell-height min-height max-height)
	    (space-requirement-components grid (cell-space-requirement grid))
	  (declare (ignore min-width max-width min-height max-height))
	  ;;--- This should give extra space to the cell, obeying min and max sizes
	  (let ((y border))
	    (loop for row from 0 below nrows
	       do (let ((x border))
		    (loop for cell from 0 below ncols
		       do (let ((item (aref contents row cell)))
			    (when (and item (not (sheet-withdrawn? item)))
			      (set-sheet-edges item x y (+ x cell-width) (+ y cell-height)))
			    (incf x (+ cell-width x-spacing)))))
	       do (incf y (+ cell-height y-spacing)))))))))


#||
/// Default implementation

define sealed class <grid-layout-pane> (<grid-layout>)
  keyword accepts-focus?: = #f;
end class <grid-layout-pane>;
||#

(defclass <grid-layout-pane> (<grid-layout>)
  ()
  (:default-initargs :accepts-focus? nil))


#||
define method class-for-make-pane 
    (framem :: <frame-manager>, class == <grid-layout>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<grid-layout-pane>, #f)
end method class-for-make-pane;

define sealed domain make (singleton(<grid-layout-pane>));
define sealed domain initialize (<grid-layout-pane>);
||#

(defmethod class-for-make-pane ((framem <frame-manager>) (class (eql (find-class '<grid-layout>))) &key)
  (values (find-class '<grid-layout-pane>) nil))




