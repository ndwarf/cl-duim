;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

(asdf:defsystem :layouts
    :description "DUIM layouts"
    :long-description "Support for a layout protocol that makes it easy to create and layout groups of related elements in a given interface. This module can handle layout problems such as the spacing and justification of a group of elements automatically."
    :author      "Scott McKay, Andy Armstrong (Lisp port: Duncan Rose <duncan@robotcat.demon.co.uk>)"
    :licence     "BSD-2-Clause"
    :version     (:read-file-form "version.sexp")
    :depends-on (#:duim-utilities
		 #:geometry
		 #:dcs
		 #:sheets
		 #:graphics)
    :serial t
    :components
    ((:file "package")
     (:file "space-reqs")
     (:file "layout")
     (:file "panes")      ;; revisit macros...
     (:file "box-pane")
     (:file "table-pane")
     (:file "printers")))

