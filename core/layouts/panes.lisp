;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-LAYOUTS-INTERNALS -*-
(in-package :duim-layouts-internals)

#||
/// Concrete pane classes

/// Useful classes for building concrete panes and gadgets

// Subclass this if you want to create a basic leaf pane, that is, a sheet that
// lives at the leaf of the sheet tree that obeys the layout protocols.
// If you want to do output to it, mix in one of the <sheet-with-medium-mixin> classes.
// If you want to do input from it, min in one of the <sheet-with-event-queue> classes.
// If you want to do repaint it, mix in of the <sheet-with-repainting-mixin> classes.
define open abstract class <leaf-pane>
    (<cached-space-requirement-mixin>,
     <client-overridability-mixin>,
    <space-requirement-mixin>,
     <leaf-layout-mixin>,
     <basic-sheet>)
end class <leaf-pane>;
||#

(defclass <leaf-pane>
    (<cached-space-requirement-mixin>
     <client-overridability-mixin>
     <space-requirement-mixin>
     <leaf-layout-mixin>
     <basic-sheet>)
  ()
  (:documentation
"
The class of leaf panes. These are sheets that live at the leaf of the
sheet tree that obey the layout protocols.

Subclass this if you want to create a basic leaf pane.

    + If you want to do output to it, mix in one of the
      <SHEET-WITH-MEDIUM-MIXIN> classes.
    + If you want to do input from it, min in one of the
      <SHEET-WITH-EVENT-QUEUE> classes.
    + If you want to do repaint it, mix in of the
      <SHEET-WITH-REPAINTING-MIXIN> classes.
")
  (:metaclass <abstract-metaclass>))

;;; TODO: Where should the following documentation, for (make-instance '<space-requirement>), go?

#||
Creates an instance of <SPACE-REQUIREMENT>.

The various width and height arguments let you control the values of
corresponding initargs to <SPACE-REQUIREMENT>, thereby controlling the
width and height of a layout under various circumstances. See
<SPACE-REQUIREMENT> for a full description of this behaviour.
||#


#||
// Subclass this if you want to create a basic composite pane.
// If you want to do input from it, mix in one of the <sheet-with-event-queue> classes.
// If you want to do repaint it, mix in one of the <sheet-with-repainting-mixin> classes.
define open abstract class <basic-composite-pane>
    (<cached-space-requirement-mixin>,
     <composite-layout-mixin>,
     <basic-sheet>)
end class <basic-composite-pane>;
||#

(defclass <basic-composite-pane>
    (<cached-space-requirement-mixin>
     <composite-layout-mixin>
     <basic-sheet>)
  ()
  (:documentation
"
Subclass this if you want to create a basic composite pane.
If you want to do input from it, mix in one of the <sheet-with-event-queue> classes.
If you want to do repaint it, mix in one of the <sheet-with-repainting-mixin> classes.
")
  (:metaclass <abstract-metaclass>))


#||
// Subclass this one if you want to create a composite pane with one child
define open abstract class <single-child-composite-pane>
    (<single-child-mixin>,
     <basic-composite-pane>)
end class <single-child-composite-pane>;
||#

(defclass <single-child-composite-pane>
    (<single-child-mixin>
     <basic-composite-pane>)
  ()
  (:documentation
"
The class of composite panes that can only have one child.
")
  (:metaclass <abstract-metaclass>))


#||
// Subclass this one if you want to create a composite pane with multiple children
define open abstract class <multiple-child-composite-pane>
    (<multiple-child-mixin>,
     <basic-composite-pane>)
end class <multiple-child-composite-pane>;
||#

(defclass <multiple-child-composite-pane>
    (<multiple-child-mixin>
     <basic-composite-pane>)
  ()
  (:documentation
"
The class of composite panes that can have multiple children. Subclass
this if you want to create a class of pane that can have more than
one child.
")
  (:metaclass <abstract-metaclass>))



#||

/// Base class for concrete panes

define open abstract class <single-child-wrapping-pane>
    (<cached-space-requirement-mixin>,
     <client-overridability-mixin>,
     <wrapping-layout-mixin>,
     <single-child-mixin>,
     <basic-sheet>)
end class <single-child-wrapping-pane>;
||#

(defclass <single-child-wrapping-pane>
    (<cached-space-requirement-mixin>
     <client-overridability-mixin>
     <wrapping-layout-mixin>
     <single-child-mixin>
     <basic-sheet>)
  ()
  (:metaclass <abstract-metaclass>))


#||
define open abstract class <multiple-child-wrapping-pane>
    (<cached-space-requirement-mixin>,
     <client-overridability-mixin>,
     <wrapping-layout-mixin>,
     <multiple-child-mixin>,
     <basic-sheet>)
end class <multiple-child-wrapping-pane>;
||#

(defclass <multiple-child-wrapping-pane>
    (<cached-space-requirement-mixin>
     <client-overridability-mixin>
     <wrapping-layout-mixin>
     <multiple-child-mixin>
     <basic-sheet>)
  ()
  (:metaclass <abstract-metaclass>))


#||
/// Support for user-defined "embedded" panes

define open abstract class <basic-user-pane>
    (<single-child-wrapping-pane>)
  sealed slot %pane-framem :: false-or(<frame-manager>) = #f;
  sealed slot %pane-layout :: false-or(<abstract-sheet>) = #f;
end class <basic-user-pane>;
||#

(defclass <basic-user-pane>
    (<single-child-wrapping-pane>)
  ((%pane-framem :type (or null <frame-manager>) :initform nil :accessor %pane-framem)
   (%pane-layout :type (or null <abstract-sheet>) :initform nil :accessor %pane-layout))
  (:documentation
"
The class of basic user panes. This is the class that gets
subclassed by DEFINE-PANE.

You specify where on the screen the pane is to be displayed using
the :region initarg.  The region specified should be relative to the
top-left of the pane's parent, since the pane must be displayed within
the confines of its parent.

If you wish the location of the pane to be transformed in some way,
use the :transform initarg.

If you wish to use a port other than the default port, use the :port
initarg.

You can specify the appearance for text in the pane using
the :style-descriptor initarg.

The :help-source and :help-context keywords let you specify pointers
to valid information available in any online help you supply with your
application. The :help-context keyword should specify a context-id
present in the online help. This context-id identifies the help topic
that is applicable to the current pane. The :help-source initarg
identifies the source file in which the help topic identified
by :help-context can be found.  A list of context-ids should be
provided by the author of the online help system.
")
  (:metaclass <abstract-metaclass>))


#||
define method initialize (pane :: <basic-user-pane>, #key frame-manager: framem)
  next-method();
  pane.%pane-framem := framem;
  unless (sheet-child(pane))
    let new-child = pane-layout(pane);
    if (new-child)
      sheet-child(pane) := new-child
    end
  end
end method initialize;
||#

(defmethod initialize-instance :after ((pane <basic-user-pane>) &key ((:frame-manager framem)) &allow-other-keys)
  (setf (%pane-framem pane) framem)
  (unless (sheet-child pane)
    (let ((new-child (pane-layout pane)))
      (when new-child
	(setf (sheet-child pane) new-child)))))


#||
define open generic pane-layout
    (pane :: <abstract-sheet>) => (sheet :: false-or(<abstract-sheet>));
||#

(defgeneric pane-layout (pane)
  (:documentation
"
Returns the layout that contains the specified pane in DEFINE-PANE.
"))


#||
define method pane-layout
    (pane :: <basic-user-pane>) => (sheet :: false-or(<abstract-sheet>))
  #f
end method pane-layout;
||#

(defmethod pane-layout ((pane <basic-user-pane>))
  nil)


#||
// The current pane in this thread
define thread variable *current-pane* = #f;
||#

(define-thread-variable *current-pane* nil)


#||
define inline function current-pane () *current-pane* end;
||#

(defun current-pane ()
"
Returns the current pane; that is, the pane that has the mouse
focus.
"
  *current-pane*)


#||
define macro pane-definer
  { define ?modifiers:* pane ?:name (?superclasses:*) ?slots:* end }
    => { define ?modifiers pane-class ?name (?superclasses) ?slots end;
         define pane-generators ?name (?superclasses) ?slots end;
         define pane-layout ?name (?superclasses) ?slots end; }
end macro pane-definer;
||#

(defmacro define-pane (name superclasses slots &rest class-options)
"
This macro lets you define a new class of DUIM pane.

The _name_ argument represents the name of the new class of pane, and
_supers_ is a list of zero or more superclasses for the new class.

The _slots_ argument represents the slot information for the new
class, together with any initargs and default values that the slots
should take.

Panes are sheets which represent a \"useful unit\" in a GUI. There is
no protocol class called <PANE>.

    + In most cases (such as when defining a frame using DEFINE-FRAME),
      a pane class groups existing gadgets (or panes) to form
      effectively a new gadget, without actually creating a class of
      <GADGET>.

    + Sometimes, a pane class implements some complex output-only sheet.

    + Sometimes, a pane class implements the <SHEET> part of a <GADGET>.

In general, a pane is best described as a *concrete* sheet.

Example:

    (define-pane <my-pane> (<pane-a> <pane-b>)
      ((my-layout :initarg :layout)
       (my-exit-buttons :initarg :exit-buttons))
      (:documentation \"My own pane type\"))

    (define-pane <my-pane> ()
      ((my-layout :initarg :layout)
       (my-exit-buttons :initarg :exit-buttons))
      (:documentation \"My own pane type\"))

    (define-pane <my-pane> (<pane-a> <pane-b>)
      ((my-layout :initarg :layout)
       (my-exit-buttons :initarg :exit-buttons)))

    (define-pane <my-pane> ()
      ((my-layout :initarg :layout)
       (my-exit-buttons :initarg :exit-buttons)))

Note: all provided parameters are evaluated multiple times and
out-of-order; do not rely on passing forms that should be evaluated.
"
  `(progn
     (define-pane-class      ,name ,superclasses ,slots ,@class-options)
     (define-pane-generators ,name ,superclasses ,slots ,@class-options)
     (define-pane-layout     ,name ,superclasses ,slots ,@class-options)))


#||
define macro pane-class-definer
  { define ?modifiers:* pane-class ?:name () ?slots:* end }
    => { define ?modifiers class ?name (<basic-user-pane>) ?slots end }
  { define ?modifiers:* pane-class ?:name (?superclasses:*) ?slots:* end }
    => { define ?modifiers class ?name (?superclasses, <basic-user-pane>) ?slots end }
 slots:
  { } => { }
  { ?slot:*; ... } => { ?slot ... }
 slot:
  { ?modifiers:* pane ?:name (?pane:variable) ?:body }
    => { ?modifiers slot ?name ## "-pane" :: false-or(<abstract-sheet>) = #f; }
  { layout (?pane:variable) ?:body } => { }		// uses %pane-layout slot
  // Catch 'slot', 'keyword', and so forth
  { ?other:* } => { ?other; }
end macro pane-class-definer;
||#

(defun %pane-class-definer-helper (slots)
"
Returns an appropriately mangled slot value for a SLOT that matches
a valid lambda in a define-pane form.

Slots can be of the form:

 (:pane [name] (variable) body)
 ((modifiers) slot [name] [slot-options])
 (:layout (variable) body)

In the 3 cases above, this method returns NIL for the third case, the
slot as-is for the second case, and

 ([name]-pane
       :type (or null <abstract-sheet>)
       :initform nil
       :accessor [name]-pane)

for the first case.
"
  (labels ((pane->slot (slot)
	     (cond ((string= (%STRING-DESIGNATOR->STRING (first slot)) (symbol-name :pane))
		    (destructuring-bind (pane name variable &rest body)
			slot
		      (declare (ignore pane variable body))
		      (let ((pane-name (intern (concatenate 'string (symbol-name name) "-PANE"))))
			(list pane-name
			      :type '(or null <abstract-sheet>)
			      :initform nil
			      :accessor pane-name))))
		   ;; Next one (:layout) is handled by "define-pane-layout"
		   ((string= (%STRING-DESIGNATOR->STRING (first slot)) (symbol-name :layout)) nil)
		   ;; last choice -- 'normal' slot definition, return as-is.
		   (t slot))))
    (loop for slot in slots
       for tv = (pane->slot slot)
       when (not (null tv)) collect tv)))

(defun %fix-pane-superclasses (supers)
  ;; Make sure <basic-user-pane> is in the superclass list
  (if (find '<basic-user-pane> supers)
      supers
      (if (= (size supers) 0)
	  (list '<basic-user-pane>)
	  (append supers (list '<basic-user-pane>)))))

;; FIXME: Does this need to be in a PROGN or something to bestow
;; "top-levelness" on the defclass forms in SBCL?
(defmacro define-pane-class (name superclasses slots &rest class-options)
  (if class-options
      `(defclass ,name ,(%fix-pane-superclasses superclasses) ,(%pane-class-definer-helper slots) ,@class-options)
      ;; else
      `(defclass ,name ,(%fix-pane-superclasses superclasses) ,(%pane-class-definer-helper slots))))


#||
define macro pane-generators-definer
  { define pane-generators ?class:name (?superclasses:*) end }
    => { }
  { define pane-generators ?class:name (?superclasses:*) 
      pane ?:name (?pane:variable) ?:body; ?more-slots:*
    end }
    => { define method ?name (?pane :: ?class)
	   let framem = ?pane.%pane-framem;
	   ?pane.?name ## "-pane"
	   | (?pane.?name ## "-pane"
                := with-frame-manager (framem)
                     dynamic-bind (*current-pane* = ?pane)
                       ?body
                     end
                   end)
         end; 
         define pane-generators ?class (?superclasses) ?more-slots end; }
  { define pane-generators ?class:name (?superclasses:*) 
      ?non-pane-slot:*; ?more-slots:*
    end }
    => { define pane-generators ?class (?superclasses) ?more-slots end; }
end macro pane-generators-definer;
||#

(defun %pane-generators-definer-helper (cname slots)
  ;; Match a pane-definition form, and generate a DEFMETHOD for it that returns
  ;; (or generates then returns) that pane for the defining class (designated by
  ;; 'cname').
  ;; Return NIL for all else
  (labels ((slot->method (slot)
	     (cond ((string= (%STRING-DESIGNATOR->STRING (first slot)) (symbol-name :pane))
		    ;; (:pane spin-box-pane-text-field (pane)
		    ;;        (make-pane '<text-field> :client pane :min-width 80))
		    (destructuring-bind (pane name variable &rest body)
			slot
		      (declare (ignore pane))
		      (let ((getter (intern (concatenate 'string (symbol-name name) "-PANE")))
			    ;; TODO: Check that the (setf getter) is defined!
			    ;; Then remove this 'setter'.
			    (setter (intern (concatenate 'string (symbol-name name) "-PANE-SETTER"))))
			(declare (ignore setter))
			;; FIXME: This should do a DEFGENERIC first, probably...
			`(defmethod ,name ((,@variable ,cname))
			   (let ((framem (%pane-framem ,@variable)))
			     (or (,getter ,@variable)
				 (setf (,getter ,@variable)
				       (with-frame-manager (framem)
					 (dynamic-let ((*current-pane* ,@variable))
					   ,@body)))))))))
		   (t nil))))
    (loop for slot in slots
       for tv = (slot->method slot)
       when (not (null tv)) collect tv)))


(defmacro define-pane-generators (name superclasses slots &rest class-opts)
  (declare (ignore superclasses class-opts))
  ;; %PANE-GENERATORS-DEFINER-HELPER returns multiple forms (hence the PROGN).
  `(progn
     ,@(%pane-generators-definer-helper name slots)))


#||
define macro pane-layout-definer
  { define pane-layout ?class:name (?superclasses:*) end }
    => { }
  { define pane-layout ?class:name (?superclasses:*) 
      layout (?pane:variable) ?:body; ?more-slots:*
    end }
    => { define method pane-layout (?pane :: ?class) => (sheet :: false-or(<abstract-sheet>))
	   let framem = ?pane.%pane-framem;
	   ?pane.%pane-layout
	   | (?pane.%pane-layout
                := with-frame-manager (framem)
                     dynamic-bind (*current-pane* = ?pane)
                       ?body
                     end
                   end)
         end }
  { define pane-layout ?class:name (?superclasses:*) 
      ?non-layout-slot:*; ?more-slots:*
    end }
    => { define pane-layout ?class (?superclasses) ?more-slots end; }
end macro pane-layout-definer;
||#


(defun %pane-layout-definer-helper (cname slots)
  ;; try to match a layout form and return NIL for all else
  (labels ((slot->layout (slot)
	     (cond ((string= (%STRING-DESIGNATOR->STRING (first slot)) (symbol-name :layout))
		    #||
		     (:layout (pane) (horizontally (:spacing 2 :y-alignment :center)
		                         (spin-box-pane-text-field pane)
		                         (vertically (:spacing 2)
		    	                     (spin-box-pane-up-arrow pane)
		    	                     (spin-box-pane-down-arrow pane))))
		    ||#
		    (destructuring-bind (layout variable &rest body)
			slot
		      (declare (ignore layout))
		      ;; FIXME: We need a DEFGENERIC too...
		      `(defmethod pane-layout ((,@variable ,cname))
			 (let ((framem (%pane-framem ,@variable)))
			   (or (%pane-layout ,@variable)
			       (setf (%pane-layout ,@variable)
				     (with-frame-manager (framem)
				       (dynamic-let ((*current-pane* ,@variable))
					 ,@body))))))))
		   (t nil))))
    (loop for slot in slots
       for tv = (slot->layout slot)
       ;; Jump out as soon as one is found; there should only ever be one.
       when (not (null tv)) return tv)))

(defmacro define-pane-layout (name superclasses slots &rest class-opts)
  (declare (ignore superclasses class-opts))
  ;; %PANE-LAYOUT-DEFINER-HELPER returns a single form.
  (%pane-layout-definer-helper name slots))



#||

/// Top level sheets

//--- Maybe we should define <top-level-sheet> in Duim-Sheets, and
//--- this should be called <top-level-layout>?
define open abstract class <top-level-sheet>
    (<single-child-wrapping-pane>)
  sealed slot display :: false-or(<display>) = #f,
    init-keyword: display:,
    setter: %display-setter;
  sealed slot sheet-frame :: false-or(<frame>) = #f,
    init-keyword: frame:,
    setter: %frame-setter;
  sealed slot frame-manager :: false-or(<frame-manager>) = #f,
    init-keyword: frame-manager:,
    setter: %frame-manager-setter;
  // For use in embedded frames, e.g., OLE and Netscape.
  // Note that the container is a native window system object.
  sealed slot sheet-container = #f,
    init-keyword: container:;
  sealed slot sheet-container-region = #f,
    init-keyword: container-region:;
end class <top-level-sheet>;
||#

;;--- Maybe we should define <top-level-sheet> in Duim-Sheets, and
;;--- this should be called <top-level-layout>?
(defclass <top-level-sheet> (<single-child-wrapping-pane>)
  ((display :type (or null <display>) :initarg :display :initform nil :reader display :writer %display-setter)
   (sheet-frame :type (or null <frame>) :initarg :frame :initform nil :reader sheet-frame :writer %frame-setter)
   (frame-manager :type (or null <frame-manager>) :initarg :frame-manager :initform nil :reader frame-manager
		  :writer %frame-manager-setter)
   ;; For use in embedded frames, e.g., OLE and Netscape.
   ;; Note that the container is a native window system object.
   (sheet-container :initarg :container :initform nil :accessor sheet-container)
   (sheet-container-region :initarg :container-region :initform nil :accessor sheet-container-region))
  (:documentation
"
The class of top level sheets.

The :container and :container-region initargs are for use in embedded
frames, such as OLE objects in HTML browser windows. The :container
initarg denotes the container itself, and :container-region is used to
specify the region of the screen in which the container appears. Note
that the container referred to is a native window system object.
")
  (:metaclass <abstract-metaclass>))


#||
define method top-level-sheet 
    (sheet :: <top-level-sheet>) => (sheet :: <top-level-sheet>)
  sheet
end method top-level-sheet;
||#

(defmethod top-level-sheet ((sheet <top-level-sheet>))
  sheet)


#||
define method display-setter
    (_display :: false-or(<display>), sheet :: <top-level-sheet>)
 => (display :: false-or(<display>))
  sheet.%display := _display
end method display-setter;
||#

(defmethod (setf display) ((_display null) (sheet <top-level-sheet>))
  (declare (ignore _display))
  (%display-setter nil sheet))

(defmethod (setf display) ((_display <display>) (sheet <top-level-sheet>))
  (%display-setter _display sheet))


#||
define method sheet-frame-setter
    (frame :: false-or(<frame>), sheet :: <top-level-sheet>)
 => (frame :: false-or(<frame>))
  sheet.%frame := frame
end method sheet-frame-setter;
||#

(defmethod (setf sheet-frame) ((frame <frame>) (sheet <top-level-sheet>))
  (%frame-setter frame sheet))

(defmethod (setf sheet-frame) ((frame null) (sheet <top-level-sheet>))
  (declare (ignore frame))
  (%frame-setter nil sheet))


#||
define method frame-manager-setter
    (framem :: false-or(<frame-manager>), sheet :: <top-level-sheet>)
 => (framem :: false-or(<frame-manager>))
  sheet.%frame-manager := framem
end method frame-manager-setter;
||#

(defmethod (setf frame-manager) ((framem <frame-manager>) (sheet <top-level-sheet>))
  (%frame-manager-setter framem sheet))

(defmethod (setf frame-manager) ((framem null) (sheet <top-level-sheet>))
  (declare (ignore framem))
  (%frame-manager-setter nil sheet))


#||
// When a sheet changes size, this can be used to notify its parent so
// that the new layout gets propagated up the sheet tree.  Note that this
// starts the relayout process at the sheet itself, not its parent (as the
// name of the function would seem to imply).
// "Sideways" because 'relayout-parent' is a forward reference from DUIM-Sheets.
//--- We should maybe do something to protect users from calling this
//--- before the sheets are mirrored, since 'compose-space' will blow out
define sideways method relayout-parent 
    (sheet :: <sheet>, #key width, height) => (did-layout? :: <boolean>)
  when (sheet-attached?(sheet))		// be forgiving
    reset-space-requirement(sheet);	// force 'compose-space' to run anew...
    let (old-width, old-height) = box-size(sheet);
    let space-req = compose-space(sheet,
				  width:  width  | old-width,
				  height: height | old-height);
    let (w, w-, w+, h, h-, h+) = space-requirement-components(sheet, space-req);
    ignore(w-, w+, h-, h+);
    let new-width  :: <integer> = w;
    let new-height :: <integer> = h;
    unless (sheet-layed-out-to-size?(sheet, new-width, new-height))
      let parent = sheet-parent(sheet);
      sheet-layed-out?(sheet) := #f;
      when (~parent | display?(parent) | ~relayout-parent(parent))
	set-sheet-size(sheet, new-width, new-height);
	#t
      end
    end
  end
end method relayout-parent;
||#

;;; TODO: Move documentation into appropriate place in duim-sheets. Ditto
;;; 'relayout-children' documentation...

#||
Lays out the parent of _sheet_ again. If _width_ and _height_ are
specified, then the parent is laid out in accordance with these
dimensions.
||#

(defmethod relayout-parent ((sheet <sheet>) &key width height)
  (duim-debug-message "LAYOUTS;PANES:RELAYOUT-PARENT entered on <sheet> ~a" sheet)
  (when (sheet-attached? sheet)		;; be forgiving
    (reset-space-requirement sheet)	;; force 'compose-space' to run anew...
    (multiple-value-bind (old-width old-height)
	(box-size sheet)
      (duim-debug-message "LAYOUTS;PANES:RELAYOUT-PARENT composing space on ~a" sheet)
      (let ((space-req (compose-space sheet :width (or width old-width) :height (or height old-height))))
	(multiple-value-bind (w w- w+ h h- h+)
	    (space-requirement-components sheet space-req)
	  (declare (ignore w- w+ h- h+))
	  (let ((new-width  w)
		(new-height h))
	    (unless (sheet-layed-out-to-size? sheet new-width new-height)
	      (let ((parent (sheet-parent sheet)))
		(setf (sheet-layed-out? sheet) nil)
		(when (or (not parent) (displayp parent) (not (relayout-parent parent)))
		  (set-sheet-size sheet new-width new-height)
		  t)))))))))



#||

/// Simple user panes

define open generic pane-display-function
    (pane :: <abstract-sheet>) => (function :: false-or(<function>));
||#

(defgeneric pane-display-function (pane)
  (:documentation
"
Returns the function used to display _pane_, where _pane_ is any pane
that can have a :display-function initarg specified. The value
returned by 'pane-display-function' is the value of
the :display-function initarg.

The display function gets called by the 'handle-repaint' method for
<simple-pane> and <drawing-pane>.
"))


#||
define open abstract class <pane-display-function-mixin> (<abstract-sheet>)
  slot pane-display-function :: false-or(<function>) = #f,
    init-keyword: display-function:;
end class <pane-display-function-mixin>;
||#

(defclass <pane-display-function-mixin>
    (<abstract-sheet>)
  ((pane-display-function :type (or null function) :initarg :display-function :initform nil :accessor pane-display-function))
  (:metaclass <abstract-metaclass>))


#||
define method handle-repaint
    (pane :: <pane-display-function-mixin>, medium :: <medium>, region :: <region>) => ()
  let function = pane-display-function(pane);
  when (function)
    function(pane, medium, region)
  end
end method handle-repaint;
||#

(defmethod handle-repaint ((pane <pane-display-function-mixin>) (medium <medium>) (region <region>))
  (let ((function (pane-display-function pane)))
    (when function
      (funcall function pane medium region))))


#||
/// Simple panes

// A pane that provides event handling, but no drawing surface.  Repainting
// should get done on the medium of some parent.
//--- When unmirrored sheets like this get moved, they need to arrange for
//--- their parent to be repainted.  Where should this be done?
define open abstract class <simple-pane>
    (<standard-input-mixin>,
     <standard-repainting-mixin>,
     <pane-display-function-mixin>,
     <multiple-child-wrapping-pane>)
end class <simple-pane>;
||#

(defclass <simple-pane>
    (<standard-input-mixin>
     <standard-repainting-mixin>
     <pane-display-function-mixin>
     <multiple-child-wrapping-pane>)
  ()
  (:documentation
"
The class of simple panes -- panes that provides event handling, but no
drawing surface. Repainting should get done on the medium of some
parent.

The :display-function initarg defines the display function for the
pane. This gets called by the HANDLE-REPAINT method for <SIMPLE-PANE>.
")
  (:metaclass <abstract-metaclass>))


#||
define method handle-repaint
    (sheet :: <simple-pane>, medium :: <medium>, region :: <region>) => ()
  ignore(medium, region);
  if (pane-display-function(sheet))
    next-method()
  else
    error("The pane %= has no display function and no 'handle-repaint' method", sheet)
  end
end method handle-repaint;
||#

(defmethod handle-repaint ((sheet <simple-pane>) (medium <medium>) (region <region>))
  (declare (ignore medium region))
  (if (pane-display-function sheet)
      (call-next-method)
      (error "The pane ~a has no display function and no 'handle-repaint' method" sheet)))


#||
define method sheet-handles-keyboard?
    (sheet :: <simple-pane>) => (true? :: <boolean>)
  sheet-accepts-focus?(sheet)
end method sheet-handles-keyboard?;
||#

(defmethod sheet-handles-keyboard? ((sheet <simple-pane>))
  (sheet-accepts-focus? sheet))


#||
define sealed class <concrete-simple-pane> (<simple-pane>)
end class <concrete-simple-pane>;

define sealed domain make (singleton(<concrete-simple-pane>));
define sealed domain initialize (<concrete-simple-pane>);
||#

(defclass <concrete-simple-pane> (<simple-pane>) ())


#||
define sealed inline method make
    (class == <simple-pane>, #rest initargs, #key, #all-keys)
 => (pane :: <concrete-simple-pane>)
  apply(make, <concrete-simple-pane>, initargs)
end method make;
||#

;; XXX: WHY ARE <SIMPLE-PANE>, <DRAWING-PANE>, and <NULL-PANE> DIFFERENT TO ALL OTHER PANE TYPES?
(defmethod make-pane ((class (eql (find-class '<simple-pane>))) &rest initargs &key &allow-other-keys)
  ;; FIXME: SHOULD THIS CALL "MAKE-PANE"? I THINK IT SHOULD.
  (apply #'make-instance '<concrete-simple-pane> initargs))


#||
/// Drawing panes

// A pane that provides event handling and a drawing surface.  Note that a
// drawing pane can be wrapped around a layout pane to provide a medium for
// all the children of the layout pane.
// Note well! Back ends must supply a 'port-handles-repaint?' method for this!
define open abstract class <drawing-pane>
    (<standard-input-mixin>,
     <standard-repainting-mixin>,
     <permanent-medium-mixin>,		// drawing panes always want a medium
     <mirrored-sheet-mixin>,		// mirroring them gives better behavior, too
     <sheet-with-caret-mixin>,
     <pane-display-function-mixin>,
     <multiple-child-wrapping-pane>)
end class <drawing-pane>;
||#

;;; FIXME: DOES THE GTK BACKEND PROVIDE A PORT-HANDLES-REPAINT? METHOD FOR THIS?
;;; IF NOT IS THAT WHY SOME OF THE GUI TESTS DON'T RUN?

(defclass <drawing-pane>
    (<standard-input-mixin>
     <standard-repainting-mixin>
     <permanent-medium-mixin>    ; drawing panes always want a medium
     <mirrored-sheet-mixin>      ; mirroring them gives better behavior, too
     <sheet-with-caret-mixin>
     <pane-display-function-mixin>
     <multiple-child-wrapping-pane>)
  ()
  (:documentation
"
The class of drawing panes. This is a pane that provides event
handling and a drawing surface. Note that a drawing pane can be
wrapped around a layout pane to provide a medium for all the children
of the layout pane.

The :display-function initarg defines the display function for the
pane. This gets called by the HANDLE-REPAINT method for <SIMPLE-PANE>.
")
  (:metaclass <abstract-metaclass>))


#||
define method sheet-handles-keyboard?
    (sheet :: <drawing-pane>) => (true? :: <boolean>)
  sheet-accepts-focus?(sheet)
end method sheet-handles-keyboard?;
||#

(defmethod sheet-handles-keyboard? ((sheet <drawing-pane>))
  (sheet-accepts-focus? sheet))


#||
define sealed class <concrete-drawing-pane> (<drawing-pane>)
end class <concrete-drawing-pane>;

define sealed domain make (singleton(<concrete-drawing-pane>));
define sealed domain initialize (<concrete-drawing-pane>);
||#

(defclass <concrete-drawing-pane> (<drawing-pane>) ())


#||
define sealed inline method make
    (class == <drawing-pane>, #rest initargs, #key, #all-keys)
 => (pane :: <concrete-drawing-pane>)
  apply(make, <concrete-drawing-pane>, initargs)
end method make;
||#

;; XXX: WHY ARE <SIMPLE-PANE>, <DRAWING-PANE>, and <NULL-PANE> DIFFERENT TO ALL OTHER PANE TYPES?
;; -- because they are the only ones with concrete implementations...?
(defmethod make-pane ((class (eql (find-class '<drawing-pane>))) &rest initargs &key &allow-other-keys)
  ;;; FIXME: APPLY #'MAKE-PANE
  (apply #'make-instance '<concrete-drawing-pane> initargs))


#||
/// Null panes

// This acts as a filler, and nothing more
define open abstract class <null-pane>
    (<leaf-pane>)
end class <null-pane>;
||#

(defclass <null-pane> (<leaf-pane>)
  ()
  (:documentation
"
The class of null panes. This class acts as a filler; use it when you
need to \"fill space\" somewhere in a complex layout.
")
  (:metaclass <abstract-metaclass>))


#||
define method default-space-requirement
    (pane :: <null-pane>,
     #key width, min-width, max-width, height, min-height, max-height)
 => (space-req :: <space-requirement>)
  make(<space-requirement>,
       width: width | 1,                 height: height | 1,
       min-width: min-width | 0,         min-height: min-height | 0,
       max-width: max-width | width | 1, max-height: max-height | height | 1)
end method default-space-requirement;
||#

(defmethod default-space-requirement ((pane <null-pane>)
				      &key width min-width max-width height min-height max-height)
  (make-space-requirement
		 :width (or width 1) :height (or height 1)
		 :min-width (or min-width 0) :min-height (or min-height 0)
		 :max-width (or max-width width 1) :max-height (or max-height height 1)))


#||
define sealed class <concrete-null-pane> (<null-pane>)
  keyword accepts-focus?: = #f;
end class <concrete-null-pane>;

define sealed domain make (singleton(<concrete-null-pane>));
define sealed domain initialize (<concrete-null-pane>);
||#

(defclass <concrete-null-pane> (<null-pane>)
  ()
  (:default-initargs :accepts-focus? nil))


#||
define sealed inline method make
    (class == <null-pane>, #rest initargs, #key, #all-keys)
 => (pane :: <concrete-null-pane>)
  apply(make, <concrete-null-pane>, initargs)
end method make;
||#

;; XXX: WHY ARE <SIMPLE-PANE>, <DRAWING-PANE>, and <NULL-PANE> DIFFERENT TO ALL OTHER PANE TYPES?
(defmethod make-pane ((class (eql (find-class '<null-pane>))) &rest initargs &key &allow-other-keys)
  ;;; FIXME: APPLY #'MAKE-PANE
  (apply #'make-instance '<concrete-null-pane> initargs))

