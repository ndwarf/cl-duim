;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-DCS-INTERNALS -*-
(in-package #:duim-dcs-internals)

#||
/// Images

define protocol <<image-protocol>> ()
  getter image-width
    (image :: <image>) => (width  :: <integer>);
  getter image-height
    (image :: <image>) => (height :: <integer>);
  getter image-depth
    (image :: <image>) => (depth  :: <integer>);
  // Reading and writing images
  function read-image
    (locator, #key image-type, #all-keys) => (image :: false-or(<image>));
  function read-image-as
    (class :: <class>, locator, image-type, #key, #all-keys)
 => (image :: false-or(<image>));
  function write-image
    (image :: <image>, locator) => ();
  // Image conversion
  function convert-image
    (image :: <image>, image-type) => (image :: <image>);
  function image-convertible?
    (image :: <image>, image-type) => (true? :: <boolean>);
end protocol <<image-protocol>>;
||#

(define-protocol <<image-protocol>> ()
  (:getter image-width (image)
	   (:documentation
"
Returns the width of the image _image_.

Defined by: <<image-protocol>>
"))
  (:getter image-height (image)
	   (:documentation
"
Returns the height of the image _image_.

Defined by: <<image-protocol>>
"))
  (:getter image-depth (image)
	   (:documentation
"
Returns the depth of the image _image_.

Defined by: <<image-protocol>>
"))
  ;; Reading and writing images
  (:function read-image (locator &key image-type &allow-other-keys)
	     (:documentation
"
Reads an image from the location _resource-id_. This function calls
'read-image-as'.

Defined by: <<image-protocol>>
"))
  (:function read-image-as (class locator image-type &key &allow-other-keys)
	     (:documentation
"
Reads the image in the location pointed to by _locator_, as an
instance of a particular class. This function is called by
'read-image'.

The _class_ represents the class that the image is read as an instance
of.

Defined by: <<image-protocol>>
"))
  (:function write-image (image locator)
	     (:documentation
"
Writes out a copy of _image_ to the designated medium _locator_.

Defined by: <<image-protocol>>
"))
  ;; Image conversion
  (:function convert-image (image image-type))
  (:function image-convertible? (image image-type)))



#||
/// Stencils

define constant $stencil-colors :: <simple-object-vector>
    = vector($background, $foreground);
||#

(defvar *stencil-colors* (vector *background* *foreground*))


#||
// A stencil is just a bitmap of the foreground and background colors
define sealed class <stencil> (<image>)
  sealed constant slot %array :: <array>,
    required-init-keyword: array:;
  sealed slot %transform = #f,
    init-keyword: transform:;
end class <stencil>;
||#

;; FIXME: THE DESCRIPTION IN THIS CLASS DOES NOT MATCH THE COMMENT
;; IN THE DYLAN IMMEDIATELY ABOVE...

(defclass <stencil> (<image>)
  ((%array :type array
	   :initarg :array
	   :initform (required-slot ":array" "<stencil>")
	   :reader %array)
   (%transform :initarg :transform
	       :initform nil
	       :accessor %transform))
  (:documentation
"
 The class for stencils. A _stencil_ is a special kind of pattern that
 contains only opacities.
"))


#||
define sealed domain make (singleton(<stencil>));
define sealed domain initialize (<stencil>);

define protocol-predicate stencil;
||#

(define-protocol-predicate stencil)

;;; TODO: where should the docs for the 'stencil?' predicate go?
#||
Returns t if its argument is a stencil.
||#
;;; TODO: check that 'decode-ink' for stencils works!


#||
define method make-stencil
    (array :: <array>) => (stencil :: <stencil>)
  make(<stencil>, array: array)
end method make-stencil;
||#

(defgeneric make-stencil (array)
  (:documentation
"
Returns a pattern design that has '(array-dimension _array_ 0)' cells
in the vertical direction and '(array-dimension _array_ 1)' cells in
the horizontal direction. _array_ must be a two-dimensional array of
real numbers between 0 and 1 (inclusive) that represent opacities. The
design in cell i, j of the resulting pattern is the value of
'(make-opacity (aref _array_ i j))'.
"))

(defmethod make-stencil ((args array))
  (make-instance '<stencil> :array args))


#||
define method make-stencil
    (sequence :: <sequence>) => (stencil :: <stencil>)
  let array = make-array-from-contents(sequence);
  make(<stencil>, array: array)
end method make-stencil;
||#

(defmethod make-stencil ((args sequence))
  (let ((array (make-array-from-contents args)))
    (make-instance '<stencil> :array array)))


#||
define macro stencil-definer
  { define stencil ?:name ?array:* end }
    => { define constant ?name :: <stencil> = make-stencil(list(?array)) }
 array:
  { } => { }
  { ?row:*; ... } => { list(?row), ... }
 row:
  { } => { }
  { ?cell:*, ... } => { ?cell, ... }
end macro stencil-definer;
||#

;;; FIXME: this should accept both lists + vectors...

(defmacro define-stencil (name args)
  `(defvar ,name (make-stencil ,args)))


#||
define method box-edges
    (stencil :: <stencil>)
 => (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
  values(0, 0, dimension(stencil.%array, 1), dimension(stencil.%array, 0))
end method box-edges;
||#

(defmethod box-edges ((stencil <stencil>))
  (values 0                                       ; left
	  0                                       ; top
	  (array-dimension (%array stencil) 1)    ; right / width
	  (array-dimension (%array stencil) 0)))  ; bottom / height


#||
define method image-width (stencil :: <stencil>) => (width :: <integer>)
  dimension(stencil.%array, 1)
end method image-width;
||#

(defmethod image-width ((stencil <stencil>))
  (array-dimension (%array stencil) 1))


#||
define method image-height (stencil :: <stencil>) => (width :: <integer>)
  dimension(stencil.%array, 0)
end method image-height;
||#

(defmethod image-height ((stencil <stencil>))
  (array-dimension (%array stencil) 0))


#||
define method decode-pattern (stencil :: <stencil>) => (array, colors, transform)
  values(stencil.%array, $stencil-colors, stencil.%transform)
end method decode-pattern;
||#

(defgeneric decode-pattern (pattern))

(defmethod decode-pattern ((stencil <stencil>))
  (values (%array stencil)
	  *stencil-colors*
	  (%transform stencil)))


#||
define method transform-image
    (transform :: <transform>, stencil :: <stencil>) => (stencil :: <stencil>)
  if (identity-transform?(transform))
    stencil
  else
    make(<stencil>,
	 array: stencil.%array,
	 transform: transform)
  end
end method transform-image;
||#

(defgeneric transform-image (transform pattern))

(defmethod transform-image ((transform <transform>)
                            (stencil <stencil>))
  (if (identity-transform? transform)
      stencil
      (make-instance '<stencil>
		     :array (%array stencil)
		     :transform transform)))


#||
/// Patterns

// A pattern is a portable bitmap of arbitrary colors
define sealed class <pattern> (<stencil>)
  sealed constant slot %colors :: <vector>,
    required-init-keyword: colors:;
end class <pattern>;
||#

(defclass <pattern> (<stencil>)
  ((%colors :type list
	    :initarg :colors
	    :initform (required-slot ":colors" "<pattern>")
	    :reader %colors))
  (:documentation
"
The class for patterns. A pattern is a bounded rectangular arrangement
of color, like a checkerboard. Drawing a pattern draws a different
design in each rectangular cell of the pattern.
"))


#||
define sealed domain make (singleton(<pattern>));
define sealed domain initialize (<pattern>);

define protocol-predicate pattern;
||#

(define-protocol-predicate pattern)

;;; TODO: where should the documentation for the predicate 'pattern?' go?
#||
Returns t if _object_ is a pattern.
||#


#||
define method make-pattern 
    (array :: <array>, colors) => (pattern :: <pattern>)
  make(<pattern>,
       array:  array,
       colors: as(<simple-vector>, colors))
end method make-pattern;
||#

(defgeneric make-pattern (elements colors)
  (:documentation
"
Returns a pattern design that has '(array-dimension _array_ 0)' cells
in the vertical direction and '(array-dimension _array_ 1)' cells in
the horizontal direction. _array_ must be a two-dimensional array of
non-negative integers less than the length of _designs_. _designs_
must be a sequence of designs. The design in cell i,j of the resulting
pattern is the nth element of _designs_, if n is the value of '(aref
_array_ i j)'. For example, _array_ can be a bit-array and _designs_
can be a list of two designs, the design drawn for 0 and the one drawn
for 1. Each cell of a pattern can be regarded as a hole that allows
the design in it to show through. Each cell might have a different
design in it. The portion of the design that shows through a hole is
the portion on the part of the drawing plane where the hole is
located. In other words, incorporating a design into a pattern does
not change its alignment to the drawing plane, and does not apply a
coordinate transformation to the design. Drawing a pattern collects
the pieces of designs that show through all the holes and draws the
pieces where the holes lie on the drawing plane. The pattern is
completely transparent outside the area defined by the array.

Each cell of a pattern occupies a 1 by 1 square. You can use
'transform-region' to scale the pattern to a different cell size and
shape, or to rotate the pattern so that the rectangular cells become
diamond-shaped. Applying a coordinate transformation to a pattern does
not affect the designs that make up the pattern. It only changes the
position, size, and shape of the cells' holes, allowing different
portions of the designs in the cells to show through. Consequently,
applying 'make-rectangular-tile' to a pattern of nonuniform designs
can produce a different appearance in each tile. The pattern cells'
holes are tiled, but the designs in the cells are not tiled and a
different portion of each of those designs shows through in each tile.
"))

(defmethod make-pattern ((elements array) colors)
  (make-instance '<pattern>
		 :array  elements
		 :colors colors))


#||
define method make-pattern 
    (sequence :: <sequence>, colors) => (pattern :: <pattern>)
  let array = make-array-from-contents(sequence);
  make(<pattern>,
       array:  array,
       colors: as(<simple-vector>, colors))
end method make-pattern;
||#

(defmethod make-pattern ((elements sequence) colors)
  (let ((array (make-array-from-contents elements)))
    (make-instance '<pattern>
		   :array  array
		   :colors colors)))


#||
define macro pattern-definer
  { define pattern ?:name (?colors:expression) ?array:* end }
    => { define constant ?name :: <pattern> = make-pattern(list(?array), ?colors) }
 array:
  { } => { }
  { ?row:*; ... } => { list(?row), ... }
 row:
  { } => { }
  { ?cell:*, ... } => { ?cell, ... }
end macro pattern-definer;
||#

;;; FIXME: 26-apr-2010: I added the eval-when around this since patterns created this way
;;; seem not to be able to be referenced by the code without it. To be honest, I'm not
;;; sure they can be referenced *with* it.

(defmacro define-pattern (name colors array)
  `(progn
     (defvar ,name (make-pattern ,array ,colors))))


#||
define method decode-pattern
    (pattern :: <pattern>) => (array, colors, transform)
  values(pattern.%array, pattern.%colors, pattern.%transform)
end method decode-pattern;
||#

(defmethod decode-pattern ((pattern <pattern>))
  (values (%array pattern)
	  (%colors pattern)
	  (%transform pattern)))


#||
define method transform-image
    (transform :: <transform>, pattern :: <pattern>) => (pattern :: <pattern>)
  if (identity-transform?(transform))
    pattern
  else
    make(<pattern>,
	 array: pattern.%array, colors: pattern.%colors,
	 transform: transform)
  end
end method transform-image;
||#

(defmethod transform-image ((transform <transform>)
                            (pattern   <pattern>))
  (if (identity-transform? transform)
      pattern
      (make-instance '<pattern>
		     :array (%array pattern)
		     :colors (%colors pattern)
		     :transform transform)))


#||
/// Images

define method read-image
    (locator, #rest keys, #key image-type, #all-keys)
 => (image :: false-or(<image>))
  dynamic-extent(keys);
  // The idea is that back ends have methods that '==' specialize
  // on the image type, and create various subclasses of <image>
  with-keywords-removed (keys = keys, #[image-type:])
    apply(read-image-as, <pattern>, locator, image-type, keys)
  end
end method read-image;
||#

(defmethod read-image (locator
                       &rest
                       keys
                       &key
                       image-type
                       &allow-other-keys)
  (declare (dynamic-extent keys))
  ;; The idea is that back ends have methods that 'eql' specialize
  ;; on the image type, and create various subclasses of <image>
  (with-keywords-removed (keys = keys (:image-type))
    (apply #'read-image-as (find-class '<pattern>) locator image-type keys)))


