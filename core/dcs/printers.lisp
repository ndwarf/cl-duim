;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-DCS-INTERNALS -*-
(in-package #:duim-dcs-internals)

#||
define method print-object
    (color :: <color>, stream :: <stream>) => ()
  printing-object (color, stream, type?: #t, identity?: #t)
    let (red, green, blue, opacity) = color-rgb(color);
    ignore(opacity);
    format(stream, "r=%d g=%d b=%d", red, green, blue)
  end
end method print-object;
||#

(defmethod print-object ((color <color>) stream)
  (print-unreadable-object (color stream :type t :identity t)
    (multiple-value-bind (red green blue opacity)
	(color-rgb color)
      (declare (ignore opacity))
      (format stream "r=~d g=~d b=~d" red green blue))))


#||
define method print-object
    (color :: <foreground>, stream :: <stream>) => ()
  printing-object (color, stream, type?: #f, identity?: #f)
    format(stream, "foreground ink")
  end
end method print-object;
||#

(defmethod print-object ((color <foreground>) stream)
  (print-unreadable-object (color stream :type nil :identity nil)
    (format stream "foreground ink")))


#||
define method print-object
    (color :: <background>, stream :: <stream>) => ()
  printing-object (color, stream, type?: #f, identity?: #f)
    format(stream, "background ink")
  end
end method print-object;
||#

(defmethod print-object ((color <background>) stream)
  (print-unreadable-object (color stream :type nil :identity nil)
    (format stream "background ink")))


#||
define method print-object
    (color :: <contrasting-color>, stream :: <stream>) => ()
  printing-object (color, stream, type?: #t, identity?: #t)
    format(stream, "%d of %d", color.%which-one, color.%how-many)
  end
end method print-object;
||#

(defmethod print-object ((color <contrasting-color>) stream)
  (print-unreadable-object (color stream :type t :identity t)
    (format stream "~d of ~d" (%which-one color) (%how-many color))))


#||
define method print-object
    (color :: <dynamic-color>, stream :: <stream>) => ()
  printing-object (color, stream, type?: #t, identity?: #t)
    format(stream, "%=", dynamic-color-color(color))
  end
end method print-object;
||#

(defmethod print-object ((color <dynamic-color>) stream)
  (print-unreadable-object (color stream :type t :identity t)
    (format stream "~S" (dynamic-color-color color))))


#||
define method print-object
    (pen :: <pen>, stream :: <stream>) => ()
  printing-object (pen, stream, type?: #t, identity?: #t)
    format(stream, "width %d (%=), dashes %=, joint %=, cap %=",
	   pen-width(pen), pen-units(pen), pen-dashes(pen),
	   pen-joint-shape(pen), pen-cap-shape(pen))
  end
end method print-object;
||#

(defmethod print-object ((pen <pen>) stream)
  (print-unreadable-object (pen stream :type t :identity t)
    (format stream "width ~d (~S), dashes ~S, joint ~S, cap ~S"
	    (pen-width pen)
	    (pen-units pen)
	    (pen-dashes pen)
	    (pen-joint-shape pen)
	    (pen-cap-shape pen))))


#||
define method print-object
    (stencil :: <stencil>, stream :: <stream>) => ()
  printing-object (stencil, stream, type?: #t, identity?: #t)
    format(stream, "%dX%d",
           dimension(stencil.%array, 1), dimension(stencil.%array, 0))
  end
end method print-object;
||#

(defmethod print-object ((stencil <stencil>) stream)
  (print-unreadable-object (stencil stream :type t :identity t)
    (format stream "~d x ~d"
	    (array-dimension (%array stencil) 1)
	    (array-dimension (%array stencil) 0))))


#||
define method print-object
    (pattern :: <pattern>, stream :: <stream>) => ()
  printing-object (pattern, stream, type?: #t, identity?: #t)
    format(stream, "%dX%d n=%d",
           dimension(pattern.%array, 1), dimension(pattern.%array, 0),
           size(pattern.%colors))
  end
end method print-object;
||#

(defmethod print-object ((pattern <pattern>) stream)
  (print-unreadable-object (pattern stream :type t :identity t)
    (format stream "~d x ~d, n=~d"
	    (array-dimension (%array pattern) 1)
	    (array-dimension (%array pattern) 0)
	    (length (%colors pattern)))))

#||
define method print-object
    (style :: <standard-text-style>, stream :: <stream>) => ()
  printing-object (style, stream, type?: #t, identity?: #t)
    format(stream, "%=.%=.%=.%=",
	   text-style-family(style), text-style-weight(style),
	   text-style-slant(style), text-style-size(style))
  end
end method print-object;
||#

(defmethod print-object ((style <standard-text-style>) stream)
  (print-unreadable-object (style stream :type t :identity t)
    (format stream "~S.~S.~S.~S"
	    (text-style-family style)
	    (text-style-weight style)
	    (text-style-slant style)
	    (text-style-size style))))

#||
define method print-object
    (font :: <device-font>, stream :: <stream>) => ()
  printing-object (font, stream, type?: #t, identity?: #t)
    format(stream, "%=", device-font-font(font))
  end
end method print-object;
||#

(defmethod print-object ((font <device-font>) stream)
  (print-unreadable-object (font stream :type t :identity t)
    (format stream "~S" (device-font-font font))))

