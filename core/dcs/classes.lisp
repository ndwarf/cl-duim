;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-DCS-INTERNALS -*-
(in-package #:duim-dcs-internals)

#||
/// Drawing context protocol classes

define protocol-class ink (<object>) end;
define protocol-class color (<ink>) end;

// Subclasses might be <gif-image>, <xbm-image>, and so forth
define protocol-class image (<ink>) end;
define protocol-class palette (<object>) end;
define protocol-class pen (<object>) end;
define protocol-class brush (<object>) end;
define protocol-class text-style (<object>) end;

||#

;;;;
;;;; <INK> and <INK> based protocol classes
;;;;

;;; An <INK> is something that can be drawn with
(define-protocol-class ink () ()
  (:documentation
"
The class of objects that represent a way of arranging colors and
opacities in the drawing plane. Intuitively, it is anything that can
be drawn with. An ink is anything that can be used in
'medium-foreground', 'medium-background', 'medium-ink', or the
foreground or background of a brush.
"))

;;; TODO: where should the docs for the 'ink?' predicate go?
#||
Returns t if _object_ is an ink, otherwise returns nil.
||#

;;; A <COLOR> is a basic <INK> that permits drawing to be performed in
;;; different (ahem) colours
(define-protocol-class color (<ink>) ()
  (:documentation
"
The <color> class is the protocol class for a color, and is a subclass
of <ink>. A member of the class <color> is an ink that represents the
intuitive definition of color: white, black, red, pale yellow, and so
forth. The visual appearance of a single point is completely described
by its color. Drawing a color sets the color of every point in the
drawing plane to that color, and sets the opacity to 1.

The :red, :green, and :blue init-keywords represent the red, green,
and blue components of the color. For an 8-bit color scheme, these can
take any real number in the range 0 to 255.

The intensity describes the brightness of the color. An intensity of 0
is black.

The hue of a color is the characteristic that is represented by a name
such as red, green, blue and so forth. This is the main attribute of a
color that distinguishes it from other colors.

The saturation describes the amount of white in the color. This is
what distinguishes pink from red.

Opacity controls how new color output covers previous color
output (that is, the final appearance when one color is painted on top
of another). Opacity can vary from totally opaque (a new color
completely obliterates the old color) to totally transparent (a new
color has no effect whatsoever; the old color remains
unchanged). Intermediate opacity values result in color blending so
that the earlier color shows through what is drawn on top of it.

All of the standard instantiable color classes provided by DUIM are
immutable.

A color can be specified by four real numbers between 0 and
1 (inclusive), giving the amounts of red, green, blue, and
opacity (alpha). Three 0's for the RGB components mean black; three
1's mean white. The intensity-hue-saturation color model is also
supported, but the red-green-blue color model is the primary model we
will use in the specification.

An opacity may be specified by a real number between 0 and
1 (inclusive). 0 is completely transparent, 1 is completely opaque,
fractions are translucent. The opacity of a color is the degree to
which it hides the previous contents of the drawing plane when it is
drawn.
"))

;;; TODO: where should docs for the 'color?' predicate go?
#||
Returns t if _object_ is a color, otherwise returns nil.
||#

;;; An <IMAGE> is a representation of a picture
(define-protocol-class image (<ink>) ()
  (:documentation
"
The class for objects that are images.
"))

;;; TODO: where should the doc for 'image?' predicate go?
#||
Returns t if its argument is an image.
||#


(define-protocol-class palette () ()
  (:documentation
"
The protocol class for color palettes.
"))

;;; TODO: where should the documentation for 'palette?' predicate go?
#||
Returns t if the object _object_ is a palette. A palette is a color
map that maps 16 bit colors into a, for example, 8 bit display.
||#

;;; A <PEN> defines the properties of stroked lines
(define-protocol-class pen () ()
  (:documentation
"
The protocol class for pens. A pen imparts ink to a medium.
"))

;;; TODO: where should the docs for predicate 'pen?' go?
#||
Returns t if _object_ is a pen, otherwise returns nil.
||#

(define-protocol-class brush () ()
  (:documentation
"
The protocol class for brushes.
"))

;;; TODO: where should the docs for 'brush?' predicate go?
#||
Returns t if its argument is a brush.
||#


(define-protocol-class text-style () ()
  (:documentation
"
The protocol class for text styles. When specifying a particular
appearance for rendered characters, there is a tension between
portability and access to specific font for a display device. DUIM
provides a portable mechanism for describing the desired _text style_
in abstract terms. Each port defines a mapping between these abstract
style specifications and particular device-specific fonts. In this
way, an application programmer can specify the desired text style in
abstract terms secure in the knowledge that an appropriate device font
will be selected at run time. However, some applications may require
direct access to particular device fonts. The text style mechanism
supports specifying device fonts by name, allowing the programmer to
sacrifice portability for control.

If :size is specified as an integer, then it represents the font size
in printer's points.
"))

;;; TODO: where should the docs for 'text-style?' predicate go?
#||
Returns t if its argument is a text-style.
||#

