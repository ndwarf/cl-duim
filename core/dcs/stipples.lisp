;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-DCS-INTERNALS -*-
(in-package #:duim-dcs-internals)

#||
/// Stipple arrays, for use in creating patterned brushes

define macro stipple-definer
  { define stipple ?:name ?stipple:* end }
    => { define constant ?name :: <array> = make-stipple(list(?stipple)) }
 stipple:
  { } => { }
  { ?row:*; ... } => { list(?row), ... }
 row:
  { } => { }
  { ?cell:*, ... } => { ?cell, ... }
end macro stipple-definer;
||#

(defmacro define-stipple (name cells)
  `(defvar ,name (make-stipple ,cells)))


#||
define method make-stipple (rows :: <sequence>) => (stipple :: <array>)
  let height  :: <integer> = size(rows);
  let width   :: <integer> = size(rows[0]);
  let stipple :: <array>   = make(<array>, dimensions: list(width, height));
  without-bounds-checks
    for (row in rows, i :: <integer> from 0)
      assert(size(row) = width,
	     "All the rows in a stipple must have the same length");
      for (cell in row, j :: <integer> from 0)
	stipple[i, j] := cell
      end
    end
  end;
  stipple
end method make-stipple;
||#

(defun make-stipple (rows)
"
ROWS is a list of rows. Each row is a list of cells containing
either 0 or 1.
"
  (let* ((height  (length rows))
         (width   (length (first rows)))
         (stipple (make-array (list width height)
			      :element-type '(integer 0 1)
			      :initial-element 0)))
    (loop for row in rows
	  for i = 0 then (1+ i)
          do (unless (= (length row) width)
	       (error "All the rows in a stipple must have the same length"))
          do (loop for cell in row
		   for j = 0 then (1+ j)
                   do (setf (aref stipple i j) cell)))
    stipple))


#||
define stipple $horizontal-hatch
  1, 1, 1, 1, 1, 1, 1, 1;
  0, 0, 0, 0, 0, 0, 0, 0;
  1, 1, 1, 1, 1, 1, 1, 1;
  0, 0, 0, 0, 0, 0, 0, 0;
  1, 1, 1, 1, 1, 1, 1, 1;
  0, 0, 0, 0, 0, 0, 0, 0;
  1, 1, 1, 1, 1, 1, 1, 1;
  0, 0, 0, 0, 0, 0, 0, 0;
end stipple $horizontal-hatch;
||#

;;; ########
;;;         
;;; ########
;;;         
;;; ########
;;;         
;;; ########
;;;         
(define-stipple *horizontal-hatch*
 '((1 1 1 1 1 1 1 1)
   (0 0 0 0 0 0 0 0)
   (1 1 1 1 1 1 1 1)
   (0 0 0 0 0 0 0 0)
   (1 1 1 1 1 1 1 1)
   (0 0 0 0 0 0 0 0)
   (1 1 1 1 1 1 1 1)
   (0 0 0 0 0 0 0 0)))

;;; TODO: where should the documentation for $horizontal-hatch go?
#||
A stipple pattern for use in creating a patterned brush with
alternating horizontal rows of lines and spaces.
||#


#||
define stipple $vertical-hatch
  1, 0, 1, 0, 1, 0, 1, 0;
  1, 0, 1, 0, 1, 0, 1, 0;
  1, 0, 1, 0, 1, 0, 1, 0;
  1, 0, 1, 0, 1, 0, 1, 0;
  1, 0, 1, 0, 1, 0, 1, 0;
  1, 0, 1, 0, 1, 0, 1, 0;
  1, 0, 1, 0, 1, 0, 1, 0;
  1, 0, 1, 0, 1, 0, 1, 0;
end stipple $vertical-hatch;
||#

;;; # # # # 
;;; # # # # 
;;; # # # # 
;;; # # # # 
;;; # # # # 
;;; # # # # 
;;; # # # # 
;;; # # # # 
(define-stipple *vertical-hatch*
 '((1 0 1 0 1 0 1 0)
   (1 0 1 0 1 0 1 0)
   (1 0 1 0 1 0 1 0)
   (1 0 1 0 1 0 1 0)
   (1 0 1 0 1 0 1 0)
   (1 0 1 0 1 0 1 0)
   (1 0 1 0 1 0 1 0)
   (1 0 1 0 1 0 1 0)))

;;; TODO: where should the docs for $vertical-hatch go?
#||
A stipple pattern for use in creating a patterned brush with
alternating vertical columns of lines and spaces.
||#


#||
define stipple $cross-hatch
  1, 1, 1, 1, 1, 1, 1, 1;
  1, 0, 1, 0, 1, 0, 1, 0;
  1, 1, 1, 1, 1, 1, 1, 1;
  1, 0, 1, 0, 1, 0, 1, 0;
  1, 1, 1, 1, 1, 1, 1, 1;
  1, 0, 1, 0, 1, 0, 1, 0;
  1, 1, 1, 1, 1, 1, 1, 1;
  1, 0, 1, 0, 1, 0, 1, 0;
end stipple $cross-hatch;
||#

;;; ########
;;; # # # # 
;;; ########
;;; # # # # 
;;; ########
;;; # # # # 
;;; ########
;;; # # # # 
(define-stipple *cross-hatch*
 '((1 1 1 1 1 1 1 1)
   (1 0 1 0 1 0 1 0)
   (1 1 1 1 1 1 1 1)
   (1 0 1 0 1 0 1 0)
   (1 1 1 1 1 1 1 1)
   (1 0 1 0 1 0 1 0)
   (1 1 1 1 1 1 1 1)
   (1 0 1 0 1 0 1 0)))

;;; TODO: Where should the documentation for '$cross-hatch' go?
#||
A stipple pattern for use in creating a patterned brush with
alternating solid and dashed lines.
||#


#||
define stipple $diagonal-hatch-down
  1, 0, 1, 0, 1, 0, 1, 0;
  0, 1, 0, 1, 0, 1, 0, 1;
  1, 0, 1, 0, 1, 0, 1, 0;
  0, 1, 0, 1, 0, 1, 0, 1;
  1, 0, 1, 0, 1, 0, 1, 0;
  0, 1, 0, 1, 0, 1, 0, 1;
  1, 0, 1, 0, 1, 0, 1, 0;
  0, 1, 0, 1, 0, 1, 0, 1;
end stipple $diagonal-hatch-down;
||#

;;; # # # # 
;;;  # # # #
;;; # # # # 
;;;  # # # #
;;; # # # # 
;;;  # # # #
;;; # # # # 
;;;  # # # #
(define-stipple *diagonal-hatch-down*
 '((1 0 1 0 1 0 1 0)
   (0 1 0 1 0 1 0 1)
   (1 0 1 0 1 0 1 0)
   (0 1 0 1 0 1 0 1)
   (1 0 1 0 1 0 1 0)
   (0 1 0 1 0 1 0 1)
   (1 0 1 0 1 0 1 0)
   (0 1 0 1 0 1 0 1)))

;;; TODO: where to put the doc for $diagonal-hatch-down?
#||
A stipple pattern for use in creating a patterned brush with
alternating dashes and spaces, the first line starting with a dash,
followed by a space, and the second line starting with a space
followed by a dash.
||#


#||
define stipple $diagonal-hatch-up
  0, 1, 0, 1, 0, 1, 0, 1;
  1, 0, 1, 0, 1, 0, 1, 0;
  0, 1, 0, 1, 0, 1, 0, 1;
  1, 0, 1, 0, 1, 0, 1, 0;
  0, 1, 0, 1, 0, 1, 0, 1;
  1, 0, 1, 0, 1, 0, 1, 0;
  0, 1, 0, 1, 0, 1, 0, 1;
  1, 0, 1, 0, 1, 0, 1, 0;
end stipple $diagonal-hatch-up;
||#

;;; TODO: where to put the doc for $diagonal-hatch-up?
#||
A stipple pattern for use in creating a patterned brush with
alternating dashes and spaces, the first line starting with a space,
followed by a dash, and the second line starting with a dash followed
by a space.
||#

;;;  # # # #
;;; # # # # 
;;;  # # # #
;;; # # # # 
;;;  # # # #
;;; # # # # 
;;;  # # # #
;;; # # # # 
(define-stipple *diagonal-hatch-up*
 '((0 1 0 1 0 1 0 1)
   (1 0 1 0 1 0 1 0)
   (0 1 0 1 0 1 0 1)
   (1 0 1 0 1 0 1 0)
   (0 1 0 1 0 1 0 1)
   (1 0 1 0 1 0 1 0)
   (0 1 0 1 0 1 0 1)
   (1 0 1 0 1 0 1 0)))


#||
define stipple $bricks-stipple
  0, 0, 0, 1, 0, 0, 0, 0;
  0, 0, 0, 1, 0, 0, 0, 0;
  0, 0, 0, 1, 0, 0, 0, 0;
  1, 1, 1, 1, 1, 1, 1, 1;
  0, 0, 0, 0, 0, 0, 0, 1;
  0, 0, 0, 0, 0, 0, 0, 1;
  0, 0, 0, 0, 0, 0, 0, 1;
  1, 1, 1, 1, 1, 1, 1, 1;
end stipple $bricks-stipple;
||#

;;;    #    
;;;    #    
;;;    #    
;;; ########
;;;        #
;;;        #
;;;        #
;;; ########

(define-stipple *bricks-stipple*
 '((0 0 0 1 0 0 0 0)
   (0 0 0 1 0 0 0 0)
   (0 0 0 1 0 0 0 0)
   (1 1 1 1 1 1 1 1)
   (0 0 0 0 0 0 0 1)
   (0 0 0 0 0 0 0 1)
   (0 0 0 0 0 0 0 1)
   (1 1 1 1 1 1 1 1)))

;;; TODO: Where should the docs for '$bricks-stipple' go?
#||
A stipple pattern for use in creating a patterned brush with
horizontal and vertical lines in the pattern of the mortar in a brick
wall.
||#


#||
define stipple $tiles-stipple
  1, 0, 0, 0, 0, 0, 0, 0;
  1, 0, 0, 0, 0, 0, 0, 0 ;
  0, 1, 0, 0, 0, 0, 0, 1 ;
  0, 0, 1, 1, 1, 1, 1, 0;
  0, 0, 0, 0, 1, 0, 0, 0;
  0, 0, 0, 0, 1, 0, 0, 0 ;
  0, 0, 0, 1, 0, 1, 0, 0 ;
  1, 1, 1, 0, 0, 0, 1, 1;
end stipple $tiles-stipple;
||#

;;; #       
;;; #       
;;;  #     #
;;;   ##### 
;;;     #   
;;;     #   
;;;    # #  
;;; ###   ##

(define-stipple *tiles-stipple*
 '((1 0 0 0 0 0 0 0)
   (1 0 0 0 0 0 0 0)
   (0 1 0 0 0 0 0 1)
   (0 0 1 1 1 1 1 0)
   (0 0 0 0 1 0 0 0)
   (0 0 0 0 1 0 0 0)
   (0 0 0 1 0 1 0 0)
   (1 1 1 0 0 0 1 1)))

;;; TODO: where should the docs for $tiles-stipple go?
#||
A stipple pattern for use in creating a patterned brush with lines and
spaces suggesting tiles.
||#


#||
define stipple $parquet-stipple
  1, 0, 0, 0, 0, 0, 0, 0;
  1, 1, 0, 0, 0, 0, 0, 1 ;
  0, 0, 1, 0, 0, 0, 1, 0 ;
  0, 0, 0, 1, 1, 1, 0, 0;
  0, 0, 0, 0, 1, 0, 0, 0;
  0, 0, 0, 1, 0, 0, 0, 0;
  0, 0, 1, 0, 0, 0, 0, 0;
  0, 1, 0, 0, 0, 0, 0, 0;
end stipple $parquet-stipple;
||#

;;; #       
;;; ##     #
;;;   #   # 
;;;    ###  
;;;     #   
;;;    #    
;;;   #     
;;;  #      

(define-stipple *parquet-stipple*
 '((1 0 0 0 0 0 0 0)
   (1 1 0 0 0 0 0 1)
   (0 0 1 0 0 0 1 0)
   (0 0 0 1 1 1 0 0)
   (0 0 0 0 1 0 0 0)
   (0 0 0 1 0 0 0 0)
   (0 0 1 0 0 0 0 0)
   (0 1 0 0 0 0 0 0)))

;;; TODO: where should the documentation for $parquet-stipple go?
#||
A stipple pattern for use in creating a patterned brush that looks
like a parquet floor.
||#


#||
define stipple $hearts-stipple
  0, 1, 1, 0, 1, 1, 0, 0;
  1, 0, 0, 1, 0, 0, 1, 0 ;
  1, 0, 0, 1, 0, 0, 1, 0 ;
  0, 1, 0, 0, 0, 1, 0, 0;
  0, 0, 1, 0, 1, 0, 0, 0;
  0, 0, 0, 1, 0, 0, 0, 0 ;
  0, 0, 0, 0, 0, 0, 0, 0 ;
  0, 0, 0, 0, 0, 0, 0, 0;
end stipple $hearts-stipple;
||#

;;;  ## ##  
;;; #  #  # 
;;; #  #  # 
;;;  #   #  
;;;   # #   
;;;    #    
;;;         
;;;         
(define-stipple *hearts-stipple*
 '((0 1 1 0 1 1 0 0)
   (1 0 0 1 0 0 1 0)
   (1 0 0 1 0 0 1 0)
   (0 1 0 0 0 1 0 0)
   (0 0 1 0 1 0 0 0)
   (0 0 0 1 0 0 0 0)
   (0 0 0 0 0 0 0 0)
   (0 0 0 0 0 0 0 0)))

;;; TODO: where should the documentation for $hearts-stipple go?
#||
A stipple pattern for use in creating a patterned brush that draws a
heart shape.
||#


#||
HORIZONTAL:
VERTICAL:
CROSS:

########################
# # # # # # # # # # # # 
########################
# # # # # # # # # # # # 
########################
# # # # # # # # # # # # 
########################
# # # # # # # # # # # # 
########################
# # # # # # # # # # # # 
########################
# # # # # # # # # # # # 
########################
# # # # # # # # # # # # 
########################
# # # # # # # # # # # # 
########################
# # # # # # # # # # # # 
########################
# # # # # # # # # # # # 
########################
# # # # # # # # # # # # 
########################
# # # # # # # # # # # # 

DIAGONAL-DOWN:

# # # # # # # # # # # # 
 # # # # # # # # # # # #
# # # # # # # # # # # # 
 # # # # # # # # # # # #
# # # # # # # # # # # # 
 # # # # # # # # # # # #
# # # # # # # # # # # # 
 # # # # # # # # # # # #
# # # # # # # # # # # # 
 # # # # # # # # # # # #
# # # # # # # # # # # # 
 # # # # # # # # # # # #
# # # # # # # # # # # # 
 # # # # # # # # # # # #
# # # # # # # # # # # # 
 # # # # # # # # # # # #
# # # # # # # # # # # # 
 # # # # # # # # # # # #
# # # # # # # # # # # # 
 # # # # # # # # # # # #
# # # # # # # # # # # # 
 # # # # # # # # # # # #
# # # # # # # # # # # # 
 # # # # # # # # # # # #

DIAGONAL-UP:

 # # # # # # # # # # # #
# # # # # # # # # # # # 
 # # # # # # # # # # # #
# # # # # # # # # # # # 
 # # # # # # # # # # # #
# # # # # # # # # # # # 
 # # # # # # # # # # # #
# # # # # # # # # # # # 
 # # # # # # # # # # # #
# # # # # # # # # # # # 
 # # # # # # # # # # # #
# # # # # # # # # # # # 
 # # # # # # # # # # # #
# # # # # # # # # # # # 
 # # # # # # # # # # # #
# # # # # # # # # # # # 
 # # # # # # # # # # # #
# # # # # # # # # # # # 
 # # # # # # # # # # # #
# # # # # # # # # # # # 
 # # # # # # # # # # # #
# # # # # # # # # # # # 
 # # # # # # # # # # # #
# # # # # # # # # # # # 


BRICKS:

   #       #       #    
   #       #       #    
   #       #       #    
########################
       #       #       #
       #       #       #
       #       #       #
########################
   #       #       #    
   #       #       #    
   #       #       #    
########################
       #       #       #
       #       #       #
       #       #       #
########################
   #       #       #    
   #       #       #    
   #       #       #    
########################
       #       #       #
       #       #       #
       #       #       #
########################


TILES:

#       #       #       
#       #       #       
 #     # #     # #     #
  #####   #####   ##### 
    #       #       #   
    #       #       #   
   # #     # #     # #  
###   #####   #####   ##
#       #       #       
#       #       #       
 #     # #     # #     #
  #####   #####   ##### 
    #       #       #   
    #       #       #   
   # #     # #     # #  
###   #####   #####   ##
#       #       #       
#       #       #       
 #     # #     # #     #
  #####   #####   ##### 
    #       #       #   
    #       #       #   
   # #     # #     # #  
###   #####   #####   ##


PARQUET:

#       #       #              
##     ###     ###     #
  #   #   #   #   #   # 
   ###     ###     ###  
    #       #       #   
   #       #       #    
  #       #       #     
 #       #       #      
#       #       #       
##     ###     ###     #
  #   #   #   #   #   # 
   ###     ###     ###   
    #       #       #   
   #       #       #    
  #       #       #     
 #       #       #      
#       #       #       
##     ###     ###     #
  #   #   #   #   #   # 
   ###     ###     ###  
   #       #       #   
  #       #       #    
 #       #       #     
#       #       #      


HEART:

 ## ##   ## ##   ## ##  
#  #  # #  #  # #  #  # 
#  #  # #  #  # #  #  # 
 #   #   #   #   #   #  
  # #     # #     # #   
   #       #       #    
        
 ## ##   ## ##   ## ##  
#  #  # #  #  # #  #  # 
#  #  # #  #  # #  #  # 
 #   #   #   #   #   #  
  # #     # #     # #   
   #       #       #    
        
 ## ##   ## ##   ## ##  
#  #  # #  #  # #  #  # 
#  #  # #  #  # #  #  # 
 #   #   #   #   #   #  
  # #     # #     # #   
   #       #       #    
        
        
||#

;; TODO: See if stipples are used *anywhere* in DUIM. I think they
;; aren't. Write a test or two to show off what they look like.
