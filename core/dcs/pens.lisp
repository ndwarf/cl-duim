;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-DCS-INTERNALS -*-
(in-package #:duim-dcs-internals)

;;;; PENS are used to stroke lines; they can have different thicknesses,
;;;; and different dash patterns (of which solid is the most common).
;;;; Pens are also responsible for defining cap and joint shapes for
;;;; line ends and joints.

#||
/// Pens

define constant <pen-unit>
    = one-of(#"normal", #"point", #"device");

define constant <pen-joint-shape>
    = one-of(#"miter", #"bevel", #"round", #"none");

define constant <pen-cap-shape>
    = one-of(#"butt", #"square", #"round", #"no-end-point");
||#

(deftype <pen-unit> () '(member :normal :point :device))
(deftype <pen-joint-shape> () '(member :miter :bevel :round :none))
(deftype <pen-cap-shape> () '(member :butt :square :round :no-end-point))


#||
define protocol <<pen-protocol>> ()
  getter pen-width
    (pen :: <pen>) => (width :: <integer>);
  getter pen-units
    (pen :: <pen>) => (units :: <pen-unit>);
  getter pen-dashes
    (pen :: <pen>) => (dashes :: type-union(<boolean>, <sequence>));
  getter pen-joint-shape
    (pen :: <pen>) => (joint-shape :: <pen-joint-shape>);
  getter pen-cap-shape
    (pen :: <pen>) => (cap-shape :: <pen-cap-shape>);
end protocol <<pen-protocol>>;
||#

(define-protocol <<pen-protocol>> ()
  (:getter pen-width (pen)
	   (:documentation
"
Returns the pen width, that is how wide a stroke the pen draws, of
_pen_. A width of :normal is a comfortably visible thin line.

Defined by: <<pen-protocol>>
"))
  (:getter pen-units (pen)
	   (:documentation
"
Returns the units in which the pen width is specified. They may be
normal, points, or device-dependent. A width of :normal is a
comfortably visible thin line.

Defined by: <<pen-protocol>>
"))
  (:getter pen-dashes (pen)
	   (:documentation
"
Returns t if the lines drawn by _pen_ are dashed. The sequence is a
vector of integers indicating the pattern of dashes. There must be an
even number of integers. The odd elements in the list indicate the
length of inked dashes and the even elements indicate the length of
gaps between dashes.

Defined by: <<pen-protocol>>
"))
  (:getter pen-joint-shape (pen)
	   (:documentation
"
Returns the shape of the joints between line segments of a closed,
unfilled figure drawn by _pen_.

TODO: does it have to be closed? I don't think so... I'm not sure
it needs to be unfilled either...

Defined by: <<pen-protocol>>
"))
  (:getter pen-cap-shape (pen)
	   (:documentation
"
Returns the shape of the end of a line or arc drawn by _pen_.

Defined by: <<pen-protocol>>
")))


#||
//--- Do we need to scale the pen width by the medium transform?
define sealed class <standard-pen> (<pen>)
  sealed constant slot pen-width :: <integer> = 1,
    init-keyword: width:;
  sealed constant slot pen-units :: <pen-unit> = #"normal",
    init-keyword: units:;
  sealed constant slot pen-dashes :: type-union(<boolean>, <sequence>) = #f,
    init-keyword: dashes:;
  sealed constant slot pen-joint-shape :: <pen-joint-shape> = #"miter",
    init-keyword: joint-shape:;
  sealed constant slot pen-cap-shape :: <pen-cap-shape> = #"butt",
    init-keyword: cap-shape:;
end class <standard-pen>;
||#

(defclass <standard-pen> (<pen>)
  ((pen-width :type fixnum :initarg :width :initform 1 :reader pen-width)
   (pen-units :type <pen-unit> :initarg :units :initform :normal :reader pen-units)
   (pen-dashes :type (or null boolean sequence) :initarg :dashes :initform nil :reader pen-dashes)
   (pen-joint-shape :type <pen-joint-shape> :initarg :joint-shape :initform :miter :reader pen-joint-shape)
   (pen-cap-shape :type <pen-cap-shape> :initarg :cap-shape :initform :butt :reader pen-cap-shape))
  (:documentation
"
PENS are used to stroke lines; they can have different thicknesses,
and different dash patterns (of which solid is the most common).
Pens are also responsible for defining cap and joint shapes for
line ends and joints.
"))


#||
define sealed domain make (singleton(<standard-pen>));
define sealed domain initialize (<standard-pen>);

define sealed method \=
    (pen1 :: <standard-pen>, pen2 :: <standard-pen>) => (true? :: <boolean>)
  pen1 == pen2
  | begin
      pen-width(pen1) = pen-width(pen2)
      & pen-units(pen1) == pen-units(pen2)
      & pen-dashes(pen1) == pen-dashes(pen2)
      & pen-joint-shape(pen1) == pen-joint-shape(pen2)
      & pen-cap-shape(pen1) == pen-cap-shape(pen2)
    end
end method \=;
||#

(defmethod equal? ((pen1 <standard-pen>) (pen2 <standard-pen>))
  (or (eql pen1 pen2)
      (and (=   (pen-width pen1)       (pen-width pen2))
           (eql (pen-units pen1)       (pen-units pen2))
           (eql (pen-dashes pen1)      (pen-dashes pen2))
           (eql (pen-joint-shape pen1) (pen-joint-shape pen2))
           (eql (pen-cap-shape pen1)   (pen-cap-shape pen2)))))


#||
// Windows-like "stock" pens
define constant $solid-pen  :: <standard-pen>
    = make(<standard-pen>, width: 1, dashes: #f);
define constant $dashed-pen :: <standard-pen>
    = make(<standard-pen>, width: 1, dashes: #t);
define constant $dotted-pen :: <standard-pen>
    = make(<standard-pen>, width: 1, dashes: #[1, 1]);
define constant $dash-dot-pen :: <standard-pen>
    = make(<standard-pen>, width: 1, dashes: #[4, 1, 1, 1]);
define constant $dash-dot-dot-pen :: <standard-pen>
    = make(<standard-pen>, width: 1, dashes: #[4, 1, 1, 1, 1, 1]);
||#

;;; :dashes, when specified by a sequence, appears to have the form:
;;; (size-multiplier on|off)*
;;; Actually it's easier than this. Alternate values in the sequence specify the
;;; length of line to draw and to leave undrawn respectively. :dashes starts with
;;; a drawn segment by default, so:
;;; #(4 1 1 1) is 'four on, one off, one on, one off'.
;;; =>
;;; '---- - ', or repeated: '---- - ---- - '

(defvar *solid-pen* (make-instance '<standard-pen> :width 1 :dashes nil)
"
A pen that draws a solid line. The width of the line is 1, and :dashes
is nil.
")

(defvar *dashed-pen* (make-instance '<standard-pen> :width 1 :dashes t)
"
A pen that draws a dashed line. The line width is 1 and :dashes is t.
")

(defvar *dotted-pen* (make-instance '<standard-pen> :width 1 :dashes #(1 1))
"
A pen that draws a dotted line. The line width is 1 and :dashes is
#(1 1).
")

(defvar *dash-dot-pen* (make-instance '<standard-pen> :width 1 :dashes #(4 1 1 1))
"
A pen that draws a dashed and dotted line. The line width is 1
and :dashes is #(4 1 1 1).
")

(defvar *dash-dot-dot-pen* (make-instance '<standard-pen> :width 1 :dashes #(4 1 1 1 1 1))
"
A pen that draws a line with two dots between each dash. The line
width is 1 and :dashes is #(4 1 1 1 1 1).
")


#||
define constant $solid-pens :: <simple-object-vector>
    = vector(make(<standard-pen>, width: 0, dashes: #f),
	     $solid-pen,		// width: 1, dashes: #f
	     make(<standard-pen>, width: 2, dashes: #f),
	     make(<standard-pen>, width: 3, dashes: #f),
	     make(<standard-pen>, width: 4, dashes: #f));
||#

(defvar *solid-pens* (vector (make-instance '<standard-pen> :width 0 :dashes nil)
			     *solid-pen*  ;; :width 1 :dashes nil
			     (make-instance '<standard-pen> :width 2 :dashes nil)
			     (make-instance '<standard-pen> :width 3 :dashes nil)
			     (make-instance '<standard-pen> :width 4 :dashes nil)))


#||
define constant $dashed-pens :: <simple-object-vector>
    = vector(make(<standard-pen>, width: 0, dashes: #t),
	     $dashed-pen,		// width: 1, dashes: #t
	     make(<standard-pen>, width: 2, dashes: #t),
	     make(<standard-pen>, width: 3, dashes: #t),
	     make(<standard-pen>, width: 4, dashes: #t));
||#

(defvar *dashed-pens* (vector (make-instance '<standard-pen> :width 0 :dashes t)
			      *dashed-pen* ;; :width 1 :dashes t
			      (make-instance '<standard-pen> :width 2 :dashes t)
			      (make-instance '<standard-pen> :width 3 :dashes t)
			      (make-instance '<standard-pen> :width 4 :dashes t)))


#||
define sealed method make
    (class == <pen>, 
     #key width = 1, units = #"normal", dashes,
	  joint-shape = #"miter", cap-shape = #"butt")
 => (pen :: <standard-pen>)
  if (integral?(width)
      & (0 <= width & width <= 4)
      & units == #"normal"
      & (dashes == #t | dashes == #f)
      & joint-shape == #"miter"
      & cap-shape == #"butt")
    // Cache the common case when only DASHES: and WIDTH: are provided
    (if (dashes) $dashed-pens else $solid-pens end)[width]
  else
    make(<standard-pen>,
	 width: width, units: units, dashes: dashes,
	 joint-shape: joint-shape, cap-shape: cap-shape)
  end
end method make;
||#

(defun make-pen (&key (width 1) (units :normal) dashes (joint-shape :miter) (cap-shape :butt))
  (if (and (integral? width)
           (<= 0 width 4)
           (eql units :normal)
	   (or (eql dashes t) (eql dashes nil))
           (eql joint-shape :miter)
           (eql cap-shape :butt))
      ;; Cache the common cases when only :DASHES and :WIDTH are provided
      (aref (if dashes *dashed-pens* *solid-pens*) width)
      ;; else
      (make-instance '<standard-pen>
		     :width width
		     :units units
		     :dashes dashes
		     :joint-shape joint-shape
		     :cap-shape cap-shape)))


#||

/// Contrasting dash patterns

define constant $dash-pattern-grain-size :: <integer> = 3;
||#

(defconstant +dash-pattern-grain-size+ 3)


#||
define constant $contrasting-dash-patterns :: <simple-object-vector>
    = vector
	(#[1, 1],						// 2
	 #[2, 1], #[1, 2],					// 3
	 #[3, 1], #[2, 2],					// 4
	 #[2, 3], #[1, 4], #[2, 1, 1, 1], #[1, 2, 1, 1],	// 5
	 #[4, 2], #[3, 3], #[2, 4], #[3, 1, 1, 1], #[2, 2, 1, 1], #[3, 2, 1, 1],
	 #[3, 1, 2, 1]);
||#

(defvar *contrasting-dash-patterns*
  (vector #(1 1)                                   ; 2
          #(2 1) #(1 2)                            ; 3
          #(3 1) #(2 2)                            ; 4
          #(2 3) #(1 4) #(2 1 1 1) #(1 2 1 1)      ; 5
          #(4 2) #(3 3) #(2 4) #(3 1 1 1) #(2 2 1 1) #(3 2 1 1)
          #(3 1 2 1)))


#||
define sealed method make-contrasting-dash-patterns
    (n :: <integer>, #key k = $unsupplied) => (dashes)
  check-type(n, limited(<integer>, min: 2, max: 16));
  local method make-dash-pattern (index) => (pattern :: <vector>)
	  let known = $contrasting-dash-patterns[index];
	  let pattern :: <simple-object-vector> = make(<simple-vector>, size: size(known));
	  for (i :: <integer> from 0 below size(known))
	    pattern[i] := pattern[i] * $dash-pattern-grain-size
	  end;
	  pattern
	end method;
  if (unsupplied?(k))
    let patterns :: <simple-object-vector> = make(<simple-vector>, size: n);
    without-bounds-checks
      for (i :: <integer> from 0 below n)
	patterns[i] := make-dash-pattern(i)
      end
    end;
    patterns
  else
    assert(k < n,
	   "The index %d must be smaller than the count %d", k, n);
    make-dash-pattern(k)
  end
end method make-contrasting-dash-patterns;
||#

;; TODO: Should get rid of "unsupplied?" and "supplied?" methods and use
;; CL's keyarg-p machinary...

;; TODO: Need to test contrasting dash patterns and contrasting colours.

(defun make-contrasting-dash-patterns (count &key (index :unsupplied))
"
Returns a vector of n dash patterns with recognizably different
appearances. If the keyword k is supplied,
'make-contrasting-dash-patterns' returns the kth pattern. If there are
not n different dash patterns, an error is signalled.

The argument n represents the number of dash patterns.

The argument k represents the index in the vector of dash patterns
indicating the pattern to use.
"
  (check-type count (integer 2 16))
  (labels ((make-dash-pattern (ind)
             (let* ((known (aref *contrasting-dash-patterns* ind))
                    (pattern (make-array (length known))))
               (loop for i from 0 below (length known)
                     do (setf (aref pattern i)
			      (* (aref known i) +dash-pattern-grain-size+)))
               pattern)))
    (if (unsupplied? index)
        (let ((patterns (make-array count)))
          (loop for i from 0 below count
                do (setf (aref patterns i) (make-dash-pattern i)))
          patterns)
      (progn
	(assert (< index count)
		(index count)
		"The index ~d must be smaller than the count ~d"
		index count)
        (make-dash-pattern index)))))


#||
define method contrasting-dash-patterns-limit
    (_port) => (limit :: <integer>)
  16
end method contrasting-dash-patterns-limit;
||#

(defgeneric contrasting-dash-patterns-limit (port)
  (:documentation
"
Returns the number of contrasting dash patterns that the specified
port can generate.
"))

(defmethod contrasting-dash-patterns-limit (_port)
  (declare (ignore _port))
  16)


