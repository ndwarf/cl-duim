;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-DCS-INTERNALS -*-
(in-package #:duim-dcs-internals)

#||
/// Brushes

define protocol <<brush-protocol>> ()
  getter brush-foreground
    (brush :: <brush>) => (foreground :: <ink>);
  getter brush-background
    (brush :: <brush>) => (background :: <ink>);
  getter brush-mode
    (brush :: <brush>) => (mode :: <integer>);
  getter brush-fill-style
    (brush :: <brush>) => (fill-style);
  getter brush-fill-rule
    (brush :: <brush>) => (fill-rule);
  getter brush-tile
    (brush :: <brush>) => (image);
  getter brush-stipple
    (brush :: <brush>) => (stipple);
  getter brush-ts-x
    (brush :: <brush>) => (x :: false-or(<integer>));
  getter brush-ts-y
    (brush :: <brush>) => (y :: false-or(<integer>));
  getter brush-stretch-mode
    (brush :: <brush>) => (stretch-mode);
end protocol <<brush-protocol>>;
||#

(define-protocol <<brush-protocol>> ()
  (:getter brush-foreground (brush)
	   (:documentation
"
Returns the _ink_ that is the foreground color of _brush_.

Defined by: <<brush-protocol>>
"))
  (:getter brush-background (brush)
	   (:documentation
"
Returns the _ink_ that is the background color of _brush_.

Defined by: <<brush-protocol>>
"))
  (:getter brush-mode (brush)
	   (:documentation
"
Returns an integer representing the drawing mode of _brush_.

Defined by: <<brush-protocol>>
"))
  (:getter brush-fill-style (brush)
	   (:documentation
"
Returns the fill stype of _brush_, or nil if _brush_ does not have a
fill style.

Defined by: <<brush-protocol>>
"))
  (:getter brush-fill-rule (brush)
	   (:documentation
"
Returns the fill rule for _brush_, or nil if _brush_ does not have a
fill rule.

Defined by: <<brush-protocol>>
"))
  (:getter brush-tile (brush)
	   (:documentation
"
Returns the tile pattern of _brush_.

Defined by: <<brush-protocol>>
"))
  (:getter brush-stipple (brush)
	   (:documentation
"
Returns the stipple pattern of _brush_.

Defined by: <<brush-protocol>>
"))
  (:getter brush-ts-x (brush)
	   (:documentation
"
Returns the value of the x coordinate that is used to align the tile
or stipple pattern of _brush_. If _brush_ has no tile or stipple
pattern, 'brush-ts-x' returns nil.

Defined by: <<brush-protocol>>
"))
  (:getter brush-ts-y (brush)
	   (:documentation
"
Returns the value of the y coordinate that is used to align the tile
or stipple pattern of _brush_. If _brush_ has no tile or stipple
pattern, 'brush-ts-y' returns nil.

Defined by: <<brush-protocol>>
"))
  (:getter brush-stretch-mode (brush)
	   (:documentation
"
Returns the stretch mode of the brush.

Defined by: <<brush-protocol>>
")))

#||
// Drawing "functions"...
define constant $boole-clr   :: <integer> = 0;
define constant $boole-set   :: <integer> = 1;
define constant $boole-1     :: <integer> = 2;
define constant $boole-2     :: <integer> = 3;
define constant $boole-c1    :: <integer> = 4;
define constant $boole-c2    :: <integer> = 5;
define constant $boole-and   :: <integer> = 6;
define constant $boole-ior   :: <integer> = 7;
define constant $boole-xor   :: <integer> = 8;
define constant $boole-eqv   :: <integer> = 9;
define constant $boole-nand  :: <integer> = 10;
define constant $boole-nor   :: <integer> = 11;
define constant $boole-andc1 :: <integer> = 12;
define constant $boole-andc2 :: <integer> = 13;
define constant $boole-orc1  :: <integer> = 14;
define constant $boole-orc2  :: <integer> = 15;
||#

;; Drawing "functions"...
(defconstant +boole-clr+   0
"
The logical operator that is always 0. It is a suitable first argument
to the 'boole' function.
")

(defconstant +boole-set+   1
"
The logical operator that is always 1. It is a suitable first argument
to the 'boole' function.
")

(defconstant +boole-1+     2
"
The logical operator that is always the same as the first integer
argument to the 'boole' function. It is a suitable first argument to
the 'boole' function.
")

(defconstant +boole-2+     3
"
The logical operator that is always the same as the second integer
argument to the 'boole' function. It is a suitable first argument to
the 'boole' function.
")

(defconstant +boole-c1+    4
"
The logical operator that is always the same as the complement of the
first integer argument to the 'boole' function. It is a suitable first
argument to the 'boole' function.
")

(defconstant +boole-c2+    5
"
The logical operator that is always the same as the complement of the
second integer argument to the 'boole' function. It is a suitable
first argument to the 'boole' function.
")

(defconstant +boole-and+   6
"
The logical operator 'and'. It is a suitable first argument to the
'boole' function.
")

(defconstant +boole-ior+   7
"
The logical operator 'inclusive or'. It is a suitable first argument
to the 'boole' function.
")

(defconstant +boole-xor+   8
"
The logical operator 'exclusive or'. It is a suitable first argument
to the 'boole' function.
")

(defconstant +boole-eqv+   9
"
The logical operator 'equvalence' ('exclusive nor'). It is a suitable
first argument to the 'boole' function.
")

(defconstant +boole-nand+  10
"
The logical operator 'not-and'. It is a suitable first argument to the
'boole' function.
")

(defconstant +boole-nor+   11
"
The logical operator 'not-or'. It is a suitable first argument to the
'boole' function.
")

(defconstant +boole-andc1+ 12
"
The logical operator that is the 'and' of the complement of the first
integer argument to the 'boole' function with the second. It is a
suitable first argument to the 'boole' function.
")

(defconstant +boole-andc2+ 13
"
The logical operator that is 'and' of the first integer argument to
the 'boole' function with the complement of the second. It is a
suitable first argument to the 'boole' function.
")

(defconstant +boole-orc1+  14
"
The logical operator that is the 'or' of the complement of the first
integer argument to the 'boole' function with the second. It is a
suitable first argument to the 'boole' function.
")

(defconstant +boole-orc2+  15
"
The logical operator that is 'or' of the first integer argument to the
'boole' function with the complement of the second. It is a suitable
first argument to the 'boole' function.
")


#||
//--- What about other Windows arguments?
//--- What about X plane-mask, arc-mode, clip-x/y, clip-ordering?
define sealed class <standard-brush> (<brush>)
  sealed constant slot brush-foreground :: <ink> = $foreground,
    init-keyword: foreground:;
  sealed constant slot brush-background :: <ink> = $background,
    init-keyword: background:;
  sealed constant slot brush-mode :: <integer> = $boole-1,
    init-keyword: mode:;
  sealed constant slot brush-fill-style = #f,
    init-keyword: fill-style:;
  sealed constant slot brush-fill-rule = #f,
    init-keyword: fill-rule:;
  sealed constant slot brush-tile = #f,
    init-keyword: tile:;
  sealed constant slot brush-stipple = #f,
    init-keyword: stipple:;
  sealed constant slot brush-ts-x :: false-or(<integer>) = #f,
    init-keyword: ts-x:;
  sealed constant slot brush-ts-y :: false-or(<integer>) = #f,
    init-keyword: ts-y:;
  sealed constant slot brush-stretch-mode = #f,
    init-keyword: stretch-mode:;
end class <standard-brush>;

define sealed domain make (singleton(<standard-brush>));
define sealed domain initialize (<standard-brush>);
||#

(defclass <standard-brush> (<brush>)
  ((brush-foreground :type <ink> :initarg :foreground :initform *foreground* :reader brush-foreground)
   (brush-background :type <ink> :initarg :background :initform *background* :reader brush-background)
   (brush-mode :type integer :initarg :mode :initform +boole-1+ :reader brush-mode)
   (brush-fill-style :initarg :fill-style :initform nil :reader brush-fill-style)
   (brush-fill-rule :initarg :fill-rule :initform nil :reader brush-fill-rule)
   (brush-tile :initarg :tile :initform nil :reader brush-tile)
   (brush-stipple :initarg :stipple :initform nil :reader brush-stipple)
   (brush-ts-x :type (or null integer) :initarg :ts-x :initform nil :reader brush-ts-x)
   (brush-ts-y :type (or null integer) :initarg :ts-y :initform nil :reader brush-ts-y)
   (brush-stretch-mode :initarg :stretch-mode :initform nil :reader brush-stretch-mode)))


#||
define sealed method \=
    (brush1 :: <standard-brush>, brush2 :: <standard-brush>) => (true? :: <boolean>)
  brush1 == brush2
  | begin
      brush-foreground(brush1) = brush-foreground(brush2)
      & brush-background(brush1) = brush-background(brush2)
      & brush-mode(brush1) == brush-mode(brush2)
      & brush-fill-style(brush1) == brush-fill-style(brush2)
      & brush-fill-rule(brush1) == brush-fill-rule(brush2)
      & brush-tile(brush1) = brush-tile(brush2)
      & brush-stipple(brush1) = brush-stipple(brush2)
      & brush-ts-x(brush1) == brush-ts-x(brush2)
      & brush-ts-y(brush1) == brush-ts-y(brush2)
      & brush-stretch-mode(brush1) == brush-stretch-mode(brush2)
    end
end method \=;
||#

(defmethod equal? ((brush1 <standard-brush>) (brush2 <standard-brush>))
  (or (eql brush1 brush2)
      (and (equal? (brush-foreground brush1)   (brush-foreground brush2))
	   (equal? (brush-background brush1)   (brush-background brush2))
	   (eql    (brush-mode brush1)         (brush-mode brush2))
	   (eql    (brush-fill-style brush1)   (brush-fill-style brush2))
	   (eql    (brush-fill-rule brush1)    (brush-fill-rule brush2))
	   (equal? (brush-tile brush1)         (brush-tile brush2))
	   (equal? (brush-stipple brush1)      (brush-stipple brush2))
	   (eql    (brush-ts-x brush1)         (brush-ts-x brush2))
	   (eql    (brush-ts-y brush1)         (brush-ts-y brush2))
	   (eql    (brush-stretch-mode brush1) (brush-stretch-mode brush2)))))


#||
define sealed inline method make
    (class == <brush>,
     #key foreground = $foreground, background = $background,
          mode = $boole-1, fill-style, fill-rule, tile, stipple, ts-x, ts-y)
 => (brush :: <standard-brush>)
  make(<standard-brush>,
       foreground: foreground, background: background, mode: mode,
       fill-style: fill-style, fill-rule: fill-rule,
       tile: tile, stipple: stipple, ts-x: ts-x, ts-y: ts-y)
end method make;
||#

(defun make-brush (&key (foreground *foreground*)
		   (background *background*)
		   (mode +boole-1+)
		   fill-style fill-rule tile stipple ts-x ts-y)
  (make-instance '<standard-brush>
		 :foreground foreground
		 :background background
		 :mode       mode
		 :fill-style fill-style
		 :fill-rule  fill-rule
		 :tile       tile
		 :stipple    stipple
		 :ts-x       ts-x
		 :ts-y       ts-y))

#||
// This one is pretty useful...
define constant $xor-brush :: <standard-brush>
    = make(<standard-brush>, mode: $boole-xor);
||#

(defvar *xor-brush* (make-instance '<standard-brush> :mode +boole-xor+)
"
A standard brush with the drawing property of +boole-xor+.
")
