;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-DCS-INTERNALS -*-
(in-package #:duim-dcs-internals)

#||
/// Text style protocol

define constant <text-style-weight>
    = one-of(#f, #"normal", #"condensed", #"thin", #"extra-light", #"light",
	     #"medium", #"demibold", #"bold", #"extra-bold", #"black");

define constant <text-style-slant>
    = one-of(#f, #"roman", #"italic", #"oblique");
||#

(deftype <text-style-weight> ()
  '(member nil :normal :condensed :thin :extra-light :light
	   :medium :demibold  :bold :extra-bold  :black))

(deftype <text-style-slant>  () '(member nil :roman :italic :oblique))


#||
define protocol <<text-style-protocol>> ()
  // Components
  function text-style-components
    (style :: <text-style>)
 => (family, name :: false-or(<string>), weight :: <text-style-weight>,
     slant :: <text-style-slant>, size, underline? :: <boolean>,
     strikeout? :: <boolean>);
  getter text-style-family
    (style :: <text-style>) => (family);
  getter text-style-name
    (style :: <text-style>) => (name :: false-or(<string>));
  getter text-style-weight
    (style :: <text-style>) => (weight :: <text-style-weight>);
  getter text-style-slant
    (style :: <text-style>) => (slant :: <text-style-slant>);
  getter text-style-size
    (style :: <text-style>) => (size);
  getter text-style-underline?
    (style :: <text-style>) => (underline? :: <boolean>);
  getter text-style-strikeout?
    (style :: <text-style>) => (strikeout? :: <boolean>);
  // Merging
  function merge-text-styles
    (style :: <text-style>, default :: <text-style>)
 => (style :: <text-style>);
  function fully-merged-text-style?
    (style :: <text-style>) => (true? :: <boolean>);
end protocol <<text-style-protocol>>;
||#

(define-protocol <<text-style-protocol>> ()
  ;; Components
  (:function text-style-components (style)
	     (:documentation
"
Returns the components of the text style _text-style_ as the values
family, face, slant, size, underline and strikeout.

Defined by: <<text-style-protocol>>
"))
  (:getter text-style-family (style)
	   (:documentation
"
Returns the family component of the specified text style (one
of :fix, :serif, :sans-serif or nil).

Defined by: <<text-style-protocol>>
"))
  (:getter text-style-name (style))
  (:getter text-style-weight (style)
	   (:documentation
"
Returns the weight component of the text style (one of
:normal, :condensed, :thin, :extra-light, :light, :medium,
:demibold, :bold, :extra-bold, :black, or nil).

Defined by: <<text-style-protocol>>
"))
  (:getter text-style-slant (style)
	   (:documentation
"
Returns the slant component of the specified text style (one
of :roman, :italic, :oblique or nil).

Defined by: <<text-style-protocol>>
"))
  (:getter text-style-size (style)
	   (:documentation
"
Returns the style component of the specified style (an instance of
integer, or one of :normal, :tiny, :very-small, :small, :large,
:very-large, :huge, :smaller, :larger or nil. The default value
is nil.

Defined by: <<text-style-protocol>>
"))
  (:getter text-style-underline? (style)
	   (:documentation
"
Returns t if the text style is underlined.

Defined by: <<text-style-protocol>>
"))
  (:getter text-style-strikeout? (style)
	   (:documentation
"
Returns t if the text style includes a line through it, striking it
out.

Defined by: <<text-style-protocol>>
"))
  ;; Merging
  (:function merge-text-styles (style default)
	     (:documentation
"
Merges the text styles _text-style_ with _default-style_, that is,
returns a new text style that is the same as _text-style_, except that
unspecified components in _text-style_ are filled in from
_default-style_. For convenience, the two arguments also be style
specs. Note that _default-style_ must be a fully specified text style.

TODO: Does this method really deal with 'style specs'?

When merging the sizes of two text styles, if the size from the first
style is a relative size, the resulting size is either the next
smaller or next larger size than is specified by _default-style_. The
ordering of sizes, from smallest to largest,
is :tiny, :very-small, :small, :normal, :large, :very-large,
and :huge.

Defined by: <<text-style-protocol>>.
"))
  (:function fully-merged-text-style? (style)
	     (:documentation
"
Returns t if the specified text style is completely specified.

Defined by: <<text-style-protocol>>
")))


#||
/// Text styles

define sealed class <standard-text-style> (<text-style>)
  sealed constant slot text-style-family = #f,
    init-keyword: family:;
  sealed constant slot text-style-name :: false-or(<string>) = #f,
    init-keyword: name:;
  // Encodes weight, slant, underline, and strikeout
  sealed slot text-style-face-code :: <integer> = 0,
    init-keyword: face-code:;
  sealed constant slot text-style-size = #f,
    init-keyword: size:;
end class <standard-text-style>;
||#

(defclass <standard-text-style> (<text-style>)
  ((text-style-family :initarg :family :initform nil :reader text-style-family)
   (text-style-name :type (or null string) :initarg :name :initform nil :reader text-style-name)
   ;; Encodes weight, slant, underline, and strikeout
   (text-style-face-code :type integer :initarg :face-code :initform 0 :accessor text-style-face-code)
   (text-style-size :initarg :size :initform nil :reader text-style-size))
  (:documentation
"
Encapsulates family, name, weight, slant, underline, strikeout and size.
TODO: Write better documentation.
"))


#||
define sealed domain make (singleton(<standard-text-style>));
define sealed domain initialize (<standard-text-style>);

// Bits 0..5 are the weight
define constant %weight_shift :: <integer> = 0;
define constant %weight_mask  :: <integer> = #o077;
// define constant %weight_false       = #o000;		// must be zero!
// define constant %weight_normal      = #o001;
// define constant %weight_condensed   = #o002;
// define constant %weight_thin        = #o003;
// define constant %weight_extra_light = #o004;
// define constant %weight_light       = #o005;
// define constant %weight_medium      = #o006;
// define constant %weight_demibold    = #o007;
// define constant %weight_bold        = #o010;
// define constant %weight_extra_bold  = #o011;
// define constant %weight_black       = #o012;
||#

;; Bits 0..5 are the weight
(defconstant %weight_shift 0)
(defconstant %weight_mask #o077)
;; define constant %weight_false       = #o000;		// must be zero!
;; define constant %weight_normal      = #o001;
;; define constant %weight_condensed   = #o002;
;; define constant %weight_thin        = #o003;
;; define constant %weight_extra_light = #o004;
;; define constant %weight_light       = #o005;
;; define constant %weight_medium      = #o006;
;; define constant %weight_demibold    = #o007;
;; define constant %weight_bold        = #o010;
;; define constant %weight_extra_bold  = #o011;
;; define constant %weight_black       = #o012;

#||
define constant $text-style-weights :: <simple-object-vector>
    = #[#f, #"normal", #"condensed", #"thin", #"extra-light", #"light",
        #"medium", #"demibold", #"bold", #"extra-bold", #"black"];

assert($text-style-weights[0] == #f,
       "Zero'th text style weight must be #f");
||#

(defvar *text-style-weights* #(nil
			       :normal :condensed :thin :extra-light :light
			       :medium :demibold  :bold :extra-bold  :black))

(unless (eql (aref *text-style-weights* 0) nil)
  (error "Zero'th text style weight must be NIL"))


#||
// Bits 6..8 are the slant
define constant %slant_shift :: <integer> = 6;
define constant %slant_mask  :: <integer> = #o700;
// define constant %slant_false   = #o000;		// must be zero!
// define constant %slant_roman   = #o100;
// define constant %slant_italic  = #o200;
// define constant %slant_oblique = #o300;
||#


;; Bits 6..8 are the slant
(defconstant %slant_shift 6)
(defconstant %slant_mask #o700)
;; define constant %slant_false   = #o000;		// must be zero!
;; define constant %slant_roman   = #o100;
;; define constant %slant_italic  = #o200;
;; define constant %slant_oblique = #o300;

#||
define constant $text-style-slants :: <simple-object-vector>
    = #[#f, #"roman", #"italic", #"oblique"];

assert($text-style-slants[0] == #f,
       "Zero'th text style slant must be #f");
||#

(defvar *text-style-slants* #(nil :roman :italic :oblique))

(unless (eql (aref *text-style-slants* 0) nil)
  (error "Zero'th text style slant must be NIL"))


#||
// Bits 9 and 10 are the underline and strikeout flags
define constant %underline_style :: <integer> = #o1000;
define constant %strikeout_style :: <integer> = #o2000;
||#

;; Bits 9 and 10 are the underline and strikeout flags
(defconstant %underline_style #o1000)
(defconstant %strikeout_style #o2000)


#||
define inline function compute-face-code
    (weight, slant, underline?, strikeout?) => (face-code :: <integer>)
  let weight-index = position($text-style-weights, weight) | 0;
  let slant-index  = position($text-style-slants,  slant)  | 0;
  ash(weight-index, %weight_shift)
  + ash(slant-index, %slant_shift)
  + if (underline?) %underline_style else 0 end
  + if (strikeout?) %strikeout_style else 0 end
end function compute-face-code;
||#

(defun compute-face-code (weight slant underline? strikeout?)
"
Given _weight_, _slant_, _underline?_ and _strikeout?_ returns a value
encoding these settings.
"
  (let ((weight-index (or (position weight *text-style-weights* :test #'eql) 0))
	(slant-index  (or (position slant  *text-style-slants*  :test #'eql) 0)))
    (+ (ash weight-index %weight_shift)
       (ash slant-index  %slant_shift)
       (if underline?
	   %underline_style
	   0)
       (if strikeout?
	   %strikeout_style
	   0))))


#||
define sealed method initialize
    (style :: <standard-text-style>,
     #key weight = #"normal", slant = #"roman", underline? = #f, strikeout? = #f)
  next-method();
  text-style-face-code(style)
    := compute-face-code(weight, slant, underline?, strikeout?)
end method initialize;    
||#

(defmethod initialize-instance :after ((style <standard-text-style>)
				       &key
				       (weight :normal)
				       (slant :roman)
				       (underline? nil)
				       (strikeout? nil)
				       &allow-other-keys)
  (setf (text-style-face-code style)
        (compute-face-code weight slant underline? strikeout?)))


#||
define sealed method \=
    (style1 :: <standard-text-style>, style2 :: <standard-text-style>) => (true? :: <boolean>)
  // Note that text-style-name is compared for equality (\=) rather than
  // identity (\==), since it can be a <string>
  style1 == style2
  | (text-style-family(style1) == text-style-family(style2)
     & text-style-name(style1) = text-style-name(style2)
     & text-style-face-code(style1) == text-style-face-code(style2)
     & text-style-size(style1) == text-style-size(style2))
end method \=;
||#

(defmethod equal? ((style1 <standard-text-style>) (style2 <standard-text-style>))
  ;; Note that text-style-name is compared for equality (equal?) rather than
  ;; identity (==) since it can be a <string>.
  (or (eql style1 style2)
      (and (eql    (text-style-family style1)    (text-style-family style2))
	   (equal? (text-style-name style1)      (text-style-name style2))
	   (eql    (text-style-face-code style1) (text-style-face-code style2))
	   (eql    (text-style-size style1)      (text-style-size style2)))))


#||
define sealed inline method text-style-weight
    (style :: <standard-text-style>) => (weight :: <text-style-weight>)
  let index = ash(logand(text-style-face-code(style), %weight_mask),
		  -%weight_shift);
  $text-style-weights[index]
end method text-style-weight;
||#

(defmethod text-style-weight ((style <standard-text-style>))
"
Returns the weight of _style_.
"
  (let ((index (ash (logand (text-style-face-code style) %weight_mask)
		    (- %weight_shift))))
    (svref *text-style-weights* index)))


#||
define sealed inline method text-style-slant
    (style :: <standard-text-style>) => (slant :: <text-style-slant>)
  let index = ash(logand(text-style-face-code(style), %slant_mask),
		  -%slant_shift);
  $text-style-slants[index]
end method text-style-slant;
||#

(defmethod text-style-slant ((style <standard-text-style>))
"
Returns the slant of _style_.
"
  (let ((index (ash (logand (text-style-face-code style) %slant_mask)
		    (- %slant_shift))))
    (svref *text-style-slants* index)))


#||
define sealed inline method text-style-underline?
    (style :: <standard-text-style>) => (underline? :: <boolean>)
  logand(text-style-face-code(style), %underline_style) = %underline_style
end method text-style-underline?;
||#

(defmethod text-style-underline? ((style <standard-text-style>))
"
Returns T if _style_ is underlined and NIL otherwise.
"
  (= (logand (text-style-face-code style) %underline_style)
     %underline_style))


#||
define sealed inline method text-style-strikeout?
    (style :: <standard-text-style>) => (strikeout? :: <boolean>)
  logand(text-style-face-code(style), %strikeout_style) = %strikeout_style
end method text-style-strikeout?;
||#

(defmethod text-style-strikeout? ((style <standard-text-style>))
"
Returns T if _style_ is struck through and NIL otherwise.
"
  (= (logand (text-style-face-code style) %strikeout_style)
     %strikeout_style))


#||
define sealed method text-style-components 
    (style :: <standard-text-style>)
 => (family, name :: false-or(<string>), weight :: <text-style-weight>,
     slant :: <text-style-slant>, size, underline? :: <boolean>,
     strikeout? :: <boolean>)
  values(text-style-family(style),
         text-style-name(style),
         text-style-weight(style),
         text-style-slant(style),
         text-style-size(style),
	 text-style-underline?(style),
	 text-style-strikeout?(style))
end method text-style-components;
||#

(defmethod text-style-components ((style <standard-text-style>))
"
Returns the family, name, weight, slant, size, underline and
strikethrough values (in that order) of _style_.
"
  (values (text-style-family     style)
	  (text-style-name       style)
	  (text-style-weight     style)
	  (text-style-slant      style)
	  (text-style-size       style)
	  (text-style-underline? style)
	  (text-style-strikeout? style)))


#||
define variable $text-family-table :: <object-table> = make(<table>);
||#

;; FIXME: What can the keys be here? Can the 'test' be tightened up?
(defparameter *text-family-table* (make-hash-table :test #'equal))


#||
define sealed inline method make
    (class == <text-style>,
     #key family, name, weight, slant, size, underline? = #f, strikeout? = #f)
 => (text-style :: <standard-text-style>)
  make-text-style(family, name, weight, slant, size,
		  underline?: underline?, strikeout?: strikeout?)
end method make;


define sealed method make-text-style
    (family, name, weight, slant, size, #key underline? = #f, strikeout? = #f)
 => (text-style :: <standard-text-style>)
  let family-table = $text-family-table;
  let name-table = gethash(family-table, family);
  unless (name-table)
    name-table := make(<string-or-object-table>);
    gethash(family-table, family) := name-table
  end;
  let face-table = gethash(name-table, name);
  let face = compute-face-code(weight, slant, underline?, strikeout?);
  unless (face-table)
    face-table := make(<table>);
    gethash(name-table, name) := face-table
  end;
  let size-table = gethash(face-table, face);
  unless (size-table)
    size-table := make(<table>);
    gethash(face-table, face) := size-table
  end;
  let text-style = gethash(size-table, size);
  unless (text-style)
    text-style
      := make(<standard-text-style>,
              family: family, name: name, weight: weight, slant: slant, size: size,
	      underline?: underline?, strikeout?: strikeout?);
    gethash(size-table, size) := text-style
  end;
  text-style
end method make-text-style;
||#

(defun make-text-style (family name weight slant size
			&key
                        (underline? nil)
                        (strikeout? nil))
"
Returns an instance of <text-style>.

Text style objects have components for family, face, and size. Not all
of these attributes need be supplied for a given text style
object. Text styles can be merged in much the same way as pathnames
are merged; unspecified components in the style object (that is,
components that have #f in them) may be filled in by the components of
a default style object. A text style object is called fully specified
if none of its components is #f, and the size component is not a
relative size (that is, neither :smaller nor :larger).

If _size_ is an integer, it represents the size of the font in
printer's points.

Implementations are permitted to extend legal values for family, face,
and size.
"
  (let* ((family-table *text-family-table*)
	 (name-table (gethash family family-table)))
    (when (null name-table)
      (setf name-table (make-hash-table :test #'equal))
      (setf (gethash family family-table) name-table))
    (let ((face-table (gethash name name-table))
	  (face (compute-face-code weight slant underline? strikeout?)))
      (when (null face-table)
	(setf face-table (make-hash-table :test #'equal))
	(setf (gethash name name-table) face-table))
      (let ((size-table (gethash face face-table)))
	(when (null size-table)
	  (setf size-table (make-hash-table :test #'equal))
	  (setf (gethash face face-table) size-table))
	(let ((text-style (gethash size size-table)))
	  (when (null text-style)
	    (setf text-style (make-instance '<standard-text-style>
                                            :family family
                                            :name name
                                            :weight weight
                                            :slant slant
                                            :size size
                                            :underline? underline?
                                            :strikeout? strikeout?))
	    (setf (gethash size size-table) text-style))
	  text-style)))))


#||
/// Text style merging

define sealed method fully-merged-text-style?
    (style :: <standard-text-style>) => (true? :: <boolean>)
  text-style-family(style)
  & text-style-name(style)
  & begin
      let code = text-style-face-code(style);
      ~zero?(logand(code, %weight_mask)) & ~zero?(logand(code, %slant_mask))
    end
  & begin
      let size = text-style-size(style);
      size & size ~== #"larger" & size ~== #"smaller"
    end
  & #t
end method fully-merged-text-style?;
||#

(defmethod fully-merged-text-style? ((style <standard-text-style>))
"
A text style is 'fully merged' if all its attributes (family, name,
weight, slant and size) are specified and none of the attributes are
expressed in relative terms (i.e. size can be specified as 'larger'
or 'smaller' in styles that are NOT fully merged).

This method returns T if _style_ is a fully merged style and NIL
otherwise.
"
  (and (text-style-family style)
       (text-style-name   style)
       (let ((code (text-style-face-code style)))
	 (and (not (zerop (logand code %weight_mask)))
	      (not (zerop (logand code %slant_mask)))))
       (let ((size (text-style-size style)))
	 (and size
              (not (eql size :larger))
              (not (eql size :smaller))))
       t))


#||
define sealed method merge-text-styles
    (style :: <standard-text-style>, default :: <standard-text-style>)
 => (text-style :: <standard-text-style>)
  let (family1, name1, weight1, slant1, size1, underline?, strikeout?)
    = text-style-components(style);
  if (family1 & name1 & weight1 & slant1
      & (size1 & size1 ~== #"larger" & size1 ~== #"smaller"))
    style
  else
    let (family2, name2, weight2, slant2, size2)
      = text-style-components(default);
    make-text-style
      (family1 | family2,
       name1   | name2,
       weight1 | weight2,
       slant1  | slant2,
       merge-text-style-sizes(size1, size2),
       underline?: underline?, strikeout?: strikeout?)
  end
end method merge-text-styles;
||#

(defmethod merge-text-styles ((style <standard-text-style>) (default <standard-text-style>))
"
Combine _style_ with _default_ to generate a fully-merged text
style.
"
  (multiple-value-bind (family1 name1 weight1 slant1 size1 underline? strikeout?)
      (text-style-components style)
    (if (and family1
	     name1
	     weight1
	     slant1
	     size1
	     (not (eql size1 :larger))
	     (not (eql size1 :smaller)))
	style
	(multiple-value-bind (family2 name2 weight2 slant2 size2)
	    (text-style-components default)
	  (make-text-style (or family1 family2)
			   (or name1   name2)
			   (or weight1 weight2)
			   (or slant1  slant2)
			   (merge-text-style-sizes size1 size2)
			   :underline? underline? :strikeout? strikeout?)))))


#||
define constant $text-style-sizes :: <simple-object-vector>
    = #[#"tiny", #"very-small", #"small", #"normal", #"large", #"very-large", #"huge"];
||#

(defvar *text-style-sizes* #(:tiny :very-small :small :normal :large :very-large :huge))


#||
define function merge-text-style-sizes
    (size1, size2) => (new-size)
  let max-larger-size = 24;		// limits for #"larger" and #"smaller"
  let min-smaller-size = 4;
  select (size1)
    #"larger" =>
      case
        instance?(size2, <number>) => min(size2 + 2, max-larger-size);
        size2 == #"smaller" => #f;	// let a higher level decide...
        otherwise =>
          let index = position($text-style-sizes, size2);
          if (index)
            $text-style-sizes[index + 1] | #"huge"
          else
            size1
	  end;
      end;
    #"smaller" =>
      case
        instance?(size2, <number>) => max(size2 - 2, min-smaller-size);
        size2 == #"larger" => #f;	// let a higher level decide...
        otherwise =>
          let index = position($text-style-sizes, size2);
          if (index)
            if (zero?(index))
              #"tiny"
            else
              $text-style-sizes[index - 1]
            end
          else
            size1
	  end;
      end;
    otherwise =>
      size1 | size2;
  end
end function merge-text-style-sizes;
||#

(defun merge-text-style-sizes (size1 size2)
"
Merges _size1_ with _size2_ to generate a 'merged' text style size.
If both _size1_ and _size2_ are 'relative' sizes (i.e. :LARGER or
:SMALLER) this method may return NIL or a relative size (in which
case some other method must be used to decide the appropriate
size, presumably in the back-end at rendering time). 
TODO: Could use a better description than this.
"
  (let ((max-larger-size 24)
	(min-smaller-size 4))  ;; limits for #"larger" and #"smaller"
    (case size1
      (:larger
       (cond ((numberp size2) (min (+ size2 2) max-larger-size))
             ((eql size2 :smaller) nil) ;; let a higher level decide...
             (t
              (let ((index (position size2 *text-style-sizes* :test #'eql)))
                (if index
                    ;; we're expecting out-of-bounds aref to return nil,
                    ;; which it doesn't in Lisp; hence change from original.
                    (if (>= (1+ index) (length *text-style-sizes*))
                        :huge
                        (svref *text-style-sizes* (1+ index)))
                    size1)))))
      (:smaller
       (cond ((numberp size2) (max (- size2 2) min-smaller-size))
             ((eql size2 :larger) nil) ;; let a higher level decide...
             (t
              (let ((index (position size2 *text-style-sizes* :test #'eql)))
                (if index
                    (if (zerop index)
                        :tiny
                        (svref *text-style-sizes* (1- index)))
                    size1)))))
      (t (or size1 size2)))))



#||
/// Device fonts

define sealed class <device-font> (<text-style>)
  sealed constant slot device-font-port,
    required-init-keyword: port:;
  sealed constant slot device-font-font,
    required-init-keyword: font:;
end class <device-font>;
||#


(defclass <device-font> (<text-style>)
  ((device-font-port :initarg :port :initform (required-slot ":port" "<device-font>") :reader device-font-port)
   (device-font-font :initarg :font :initform (required-slot ":font" "<device-font>") :reader device-font-font))
  (:documentation
"
 The protocol class for device-specific fonts.

 Instances of this type represent a 'native toolkit' font which can be
 used for drawing.
"))


(defgeneric device-font? (style))

(defmethod device-font? ((style <device-font>))
  t)

(defmethod device-font? ((style t))
  nil)


#||
define sealed domain make (singleton(<device-font>));
define sealed domain initialize (<device-font>);

define method make-device-font
    (port, font) => (device-font :: <device-font>)
  make(<device-font>, port: port, font: font)
end method make-device-font;
||#

(defun make-device-font (port font)
"
Returns a device-specific font. Text styles are mapped to fonts for a
port, a character set, and a text style. All ports must implement
methods for the generic functions, for all classes of text style.

The objects used to represent a font mapping are unspecified and are
likely to vary from port to port. For instance, a mapping might be
some sort of font object on one type of port, or might simply be the
name of a font on another.

Part of initializing a port is to define the mappings between text
styles and font names for the port's host window system.
"
  (make-instance '<device-font> :port port :font font))


#||
// Device fonts can't be merged against anything.
define sealed method merge-text-styles
    (style :: <device-font>, default :: <standard-text-style>) => (style :: <device-font>)
  ignore(default);
  style
end method merge-text-styles;
||#

(defmethod merge-text-styles ((style <device-font>) (default <standard-text-style>))
"
Device fonts can't be merged against anything.
"
  (declare (ignore default))
  style)


#||
define sealed method merge-text-styles
    (style :: <standard-text-style>, default :: <device-font>) => (style :: <device-font>)
  ignore(style);
  default
end method merge-text-styles;
||#

(defmethod merge-text-styles ((style <standard-text-style>) (default <device-font>))
"
Nothing can be merged with a device font.
"
  (declare (ignore style))
  default)


#||
define sealed method fully-merged-text-style? (style :: <device-font>) => (true? :: <boolean>)
  #t
end method fully-merged-text-style?;
||#

(defmethod fully-merged-text-style? ((style <device-font>))
"
Device fonts are always fully merged.
"
  t)



#||
/// Initializations

// Not really used yet...
define variable $standard-character-set = #f;
||#

(defparameter *standard-character-set* nil)


#||
define variable $null-text-style :: <standard-text-style>
    = make(<text-style>,
	   family: #f, name: #f, weight: #f, slant: #f, size: #f);
||#

(defparameter *null-text-style* (make-text-style nil nil nil nil nil))


#||
define variable $undefined-text-style :: <standard-text-style>
    = make(<text-style>,
	   family: #"undefined", name: #f, weight: #"normal", slant: #"roman",
	   size: #"normal");
||#

(defparameter *undefined-text-style* (make-text-style :undefined
						      nil
						      :normal
						      :roman
						      :normal))


#||
// The default default, if no other can be found anywhere
define variable $default-text-style :: <standard-text-style>
    = make(<text-style>,
	   family: #"fix", name: #f, weight: #"normal", slant: #"roman",
	   size: #"normal");
||#

(defparameter *default-text-style* (make-text-style :fix
						     nil
						     :normal
						     :roman
						     :normal)
"
The default default text style if no other can be found anywhere.
")

;; TODO: Check that all that hashtable stuff for holding text styles works.
;; It's possible that it doesn't!
