;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

(asdf:defsystem :dcs
    :description "DUIM disply device contexts"
    :long-description "The DUIM-DCs Library module provides color support to the DUIM library."
    :author      "Scott McKay, Andy Armstrong (Lisp port: Duncan Rose <duncan@robotcat.demon.co.uk>)"
    :version     (:read-file-form "version.sexp")
    :licence     "BSD-2-Clause"
    :depends-on (#:duim-utilities
		 #:geometry)
    :serial t
    :components
    ((:file "package")
     (:file "classes")
     (:file "colors")
     (:file "palettes")
     (:file "stipples")
     (:file "pens")
     (:file "brushes")
     (:file "images")
     (:file "text-styles")
     (:file "styles")
     (:file "printers")))

