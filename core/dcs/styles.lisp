;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-DCS-INTERNALS -*-
(in-package #:duim-dcs-internals)

#||

/// Style descriptors

define open generic default-foreground
    (object) => (ink :: false-or(<ink>));
define open generic default-foreground-setter
    (ink :: false-or(<ink>), object) => (ink :: false-or(<ink>));

define open generic default-background
    (object) => (ink :: false-or(<ink>));
define open generic default-background-setter
    (ink :: false-or(<ink>), object) => (ink :: false-or(<ink>));

define open generic default-text-style
    (object) => (text-style :: false-or(<text-style>));
define open generic default-text-style-setter
    (text-style :: false-or(<text-style>), object) => (text-style :: false-or(<text-style>));
||#

(defgeneric default-foreground (object)
  (:documentation
"
Returns the ink that is the default foreground of _object_.
"))

(defgeneric (setf default-foreground) (ink object)
  (:documentation
"
Sets the default foreground of _object_.
"))

(defgeneric default-background (object)
  (:documentation
"
Returns the ink that is the default background of _object_.
"))

(defgeneric (setf default-background) (ink object)
  (:documentation
"
Sets the default background of _object_.
"))

(defgeneric default-text-style (object)
  (:documentation
"
Returns the default text style for its argument. This function is used
to merge against if the text style is not fully specified, or if no
text style is specified.
"))

(defgeneric (setf default-text-style) (text-style object)
  (:documentation
"
Sets the default text style of _object_.
"))


#||
define sealed class <style-descriptor> (<object>)
  // Default these to #f so that the port can fill them in appropriately
  sealed slot default-foreground :: false-or(<ink>) = #f,
    init-keyword: foreground:;
  sealed slot default-background :: false-or(<ink>) = #f,
    init-keyword: background:;
  sealed slot default-text-style :: false-or(<text-style>) = #f,
    init-keyword: text-style:;
end class <style-descriptor>;
||#

(defclass <style-descriptor> ()
  ;; Default these to NIL so that the port can fill them in appropriately
  ((default-foreground :type (or null <ink>) :initarg :foreground :initform nil :accessor default-foreground)
   (default-background :type (or null <ink>) :initarg :background :initform nil :accessor default-background)
   (default-text-style :type (or null <text-style>) :initarg :text-style :initform nil :accessor default-text-style))
  (:documentation
"
Encapsulates a foreground, background and a text style.
"))


#||
define sealed domain make (singleton(<style-descriptor>));
define sealed domain initialize (<style-descriptor>);
||#



