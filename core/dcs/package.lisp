;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

;;; Two packages are defined in this file:
;;;     -internals, private API
;;;     duim-dcs, the public facing API

(defpackage :duim-dcs-internals
  (:use #:common-lisp
	#:duim-utilities
	#:duim-geometry)
  (:export

   ;;;
   ;;; The private API
   ;;;

   ;; Colors

   #:<rgb-color>
   #:*default-foreground*
   #:*default-background*
   #:contrasting-color-index
   #:contrasting-color->color

   ;; Palettes

   #:<basic-palette>
   #:do-add-colors
   #:allocate-color
   #:deallocate-color
   #:do-remove-colors
   #:update-palette-entry
   #:update-palette-entries

   ;; Dynamic and layered colors

   #:dynamic-color-palettes
   ;;        #:dynamic-color-palettes-setter
   ;;	#:(setf dynamic-color-palettes)
   #:layered-color
   ;;        #:layered-color-color-setter
   ;;	#:(setf layered-color-color)
   #:layered-color-color
   #:make-layered-color-set

   ;; Pens

   #:<standard-pen>

   ;; Brushes

   #:<standard-brush>
   #:make-stipple
   ;;        #:stipple-definer
   #:define-stipple

   ;; Images, patterns, and stencils

   #:<background>
   #:<foreground>
   #:decode-pattern

   ;; Text styles

   #:<standard-text-style>
   #:*default-text-style*
   #:*null-text-style*
   #:*standard-character-set*
   #:*undefined-text-style*
   #:device-font-font
   #:device-font-port
   #:text-style-face-code


   ;;;
   ;;; The public API
   ;;;

   ;; Colours

   #:*background*
   #:*black*
   #:*blue*
   #:*cyan*
   #:*foreground*
   #:*green*
   #:*magenta*
   #:*red*
   #:*white*
   #:*yellow*
   #:<color>
   #:<contrasting-color>
   #:color-ihs
   #:color-luminosity
   #:color-rgb
   #:colorp
   #:contrasting-colors-limit
   #:make-contrasting-colors
   #:make-gray-color
   #:make-ihs-color
   #:make-rgb-color

   ;; Palettes

   #:<color-not-found>
   #:<palette-full>
   #:<palette>
   #:add-colors
   #:color-palette?
   #:dynamic-palette?
   #:find-color
   #:make-palette
   #:palettep
   #:remove-colors

   ;; Dynamic and layered colors

   #:<dynamic-color>
   #:dynamic-color-color
   ;;	   #:dynamic-color-color-setter
   ;;	   #:(setf dynamic-color-color)
   #:make-dynamic-color

   ;; Images, patterns, and stencils

   #:<image>
   #:<ink>
   #:<pattern>
   #:<stencil>
   #:convert-image
   #:image-convertible?
   #:image-depth
   #:image-height
   #:image-width
   #:imagep
   #:inkp
   #:make-pattern
   #:make-stencil
   #:pattern?
   ;;	   #:pattern-definer
   #:define-pattern
   #:read-image
   #:read-image-as
   #:stencil?
   ;;	   #:stencil-definer
   #:define-stencil
   #:transform-image
   #:write-image

   ;; Pens

   #:<pen>
   #:*solid-pen*
   #:*dashed-pen*
   #:*dotted-pen*
   #:*dash-dot-pen*
   #:*dash-dot-dot-pen*
   #:contrasting-dash-patterns-limit
   #:make-contrasting-dash-patterns
   #:make-pen
   #:pen-cap-shape
   #:pen-dashes
   #:pen-joint-shape
   #:pen-units
   #:pen-width
   #:penp

   ;; Brushes

   #:<brush>
   #:+boole-clr+
   #:+boole-set+
   #:+boole-1+
   #:+boole-2+
   #:+boole-c1+
   #:+boole-c2+
   #:+boole-and+
   #:+boole-ior+
   #:+boole-xor+
   #:+boole-eqv+
   #:+boole-nand+
   #:+boole-nor+
   #:+boole-andc1+
   #:+boole-andc2+
   #:+boole-orc1+
   #:+boole-orc2+
   #:*horizontal-hatch*
   #:*vertical-hatch*
   #:*cross-hatch*
   #:*diagonal-hatch-down*
   #:*diagonal-hatch-up*
   #:*bricks-stipple*
   #:*tiles-stipple*
   #:*parquet-stipple*
   #:*hearts-stipple*
   #:*xor-brush*
   #:brush-background
   #:brush-fill-rule
   #:brush-fill-style
   #:brush-foreground
   #:brush-mode
   #:brush-stipple
   #:brush-stretch-mode
   #:brush-tile
   #:brush-ts-x
   #:brush-ts-y
   #:brushp
   #:make-brush

   ;; Text styles

   #:<text-style>
   #:<device-font>
   #:device-font?
   #:fully-merged-text-style?
   #:make-device-font
   #:make-text-style
   #:merge-text-styles
   #:text-style-components
   #:text-style-family
   #:text-style-name
   #:text-style-size
   #:text-style-slant
   #:text-style-strikeout?
   #:text-style-underline?
   #:text-style-weight
   #:text-style-p

   ;; Style descriptors

   #:<style-descriptor>
   #:default-background
   ;;	   #:default-background-setter
   ;;	   #:(setf default-background)
   #:default-foreground
   ;;	   #:default-foreground-setter
   ;;	   #:(setf default-foreground)
   #:default-text-style
   ;;	   #:default-text-style-setter
   ;;	   #:(setf default-text-style)

   ))


(defpackage :duim-dcs
  (:use #:common-lisp
	#:duim-utilities
	#:duim-geometry
	#:duim-dcs-internals)
  (:export ; expose the public API here

   ;; Colours

   #:*background*
   #:*black*
   #:*blue*
   #:*cyan*
   #:*foreground*
   #:*green*
   #:*magenta*
   #:*red*
   #:*white*
   #:*yellow*
   #:<color>
   #:<contrasting-color>
   #:color-ihs
   #:color-luminosity
   #:color-rgb
   #:colorp
   #:contrasting-colors-limit
   #:make-contrasting-colors
   #:make-gray-color
   #:make-ihs-color
   #:make-rgb-color

   ;; Palettes

   #:<color-not-found>
   #:<palette-full>
   #:<palette>
   #:add-colors
   #:color-palette?
   #:dynamic-palette?
   #:find-color
   #:make-palette
   #:palettep
   #:remove-colors

   ;; Dynamic and layered colors

   #:<dynamic-color>
   #:dynamic-color-color
   ;;	   #:(setf dynamic-color-color)
   ;;	   #:dynamic-color-color-setter
   #:make-dynamic-color

   ;; Images, patterns, and stencils

   #:<image>
   #:<ink>
   #:<pattern>
   #:<stencil>
   #:convert-image
   #:image-convertible?
   #:image-depth
   #:image-height
   #:image-width
   #:imagep
   #:inkp
   #:make-pattern
   #:make-stencil
   #:pattern?
   ;;	   #:pattern-definer
   #:define-pattern
   #:read-image
   #:read-image-as
   #:stencil?
   ;;	   #:stencil-definer
   #:define-stencil
   #:transform-image
   #:write-image

   ;; Pens

   #:<pen>
   #:*solid-pen*
   #:*dashed-pen*
   #:*dotted-pen*
   #:*dash-dot-pen*
   #:*dash-dot-dot-pen*
   #:contrasting-dash-patterns-limit
   #:make-contrasting-dash-patterns
   #:make-pen
   #:pen-cap-shape
   #:pen-dashes
   #:pen-joint-shape
   #:pen-units
   #:pen-width
   #:penp

   ;; Brushes

   #:<brush>
   #:+boole-clr+
   #:+boole-set+
   #:+boole-1+
   #:+boole-2+
   #:+boole-c1+
   #:+boole-c2+
   #:+boole-and+
   #:+boole-ior+
   #:+boole-xor+
   #:+boole-eqv+
   #:+boole-nand+
   #:+boole-nor+
   #:+boole-andc1+
   #:+boole-andc2+
   #:+boole-orc1+
   #:+boole-orc2+
   #:*horizontal-hatch*
   #:*vertical-hatch*
   #:*cross-hatch*
   #:*diagonal-hatch-down*
   #:*diagonal-hatch-up*
   #:*bricks-stipple*
   #:*tiles-stipple*
   #:*parquet-stipple*
   #:*hearts-stipple*
   #:*xor-brush*
   #:brush-background
   #:brush-fill-rule
   #:brush-fill-style
   #:brush-foreground
   #:brush-mode
   #:brush-stipple
   #:brush-stretch-mode
   #:brush-tile
   #:brush-ts-x
   #:brush-ts-y
   #:brushp
   #:make-brush

   ;; Text styles

   #:<text-style>
   #:<device-font>
   #:device-font?
   #:fully-merged-text-style?
   #:make-device-font
   #:make-text-style
   #:merge-text-styles
   #:text-style-components
   #:text-style-family
   #:text-style-name
   #:text-style-size
   #:text-style-slant
   #:text-style-strikeout?
   #:text-style-underline?
   #:text-style-weight
   #:text-style?

   ;; Style descriptors

   #:<style-descriptor>
   #:default-background
   ;;	   #:default-background-setter
   ;;	   #:(setf default-background)
   #:default-foreground
   ;;	   #:default-foreground-setter
   ;;	   #:(setf default-foreground)
   #:default-text-style))
;;	   #:default-text-style-setter
;;	   #:(setf default-text-style)))



#||
define module duim-DCs
  // Colors
  create $background,
	 $black,
	 $blue,
	 $cyan,
	 $foreground,
	 $green,
	 $magenta,
	 $red,
	 $white,
	 $yellow,
	 <color>,
	 <contrasting-color>,
	 color-ihs,
	 color-luminosity,
	 color-rgb,
	 color?,
	 contrasting-colors-limit,
	 make-contrasting-colors,
	 make-gray-color,
	 make-ihs-color,
	 make-rgb-color;

  // Palettes
  create <color-not-found>,
	 <palette-full>,
	 <palette>,
	 add-colors,
	 color-palette?,
	 dynamic-palette?,
	 find-color,
	 make-palette,
	 palette?,
	 remove-colors;

  // Dynamic and layered colors
  create <dynamic-color>,
	 dynamic-color-color, dynamic-color-color-setter,
	 make-dynamic-color;

  // Images, patterns, and stencils
  create <image>,
	 <ink>,
	 <pattern>,
	 <stencil>,
	 convert-image,
	 image-convertible?,
	 image-depth,
	 image-height,
	 image-width,
	 image?,
	 ink?,
	 make-pattern,
	 make-stencil,
	 pattern?,
         \pattern-definer,
	 read-image, read-image-as,
	 stencil?,
         \stencil-definer,
         transform-image,
	 write-image;

  // Pens
  create <pen>,
	 $solid-pen,
	 $dashed-pen,
	 $dotted-pen,
	 $dash-dot-pen,
	 $dash-dot-dot-pen,
	 contrasting-dash-patterns-limit,
	 make-contrasting-dash-patterns,
	 pen-cap-shape,
	 pen-dashes,
	 pen-joint-shape,
	 pen-units,
	 pen-width,
	 pen?;

  // Brushes
  create <brush>,
	 $boole-clr,   $boole-set,   $boole-1,    $boole-2,
	 $boole-c1,    $boole-c2,    $boole-and,  $boole-ior,
	 $boole-xor,   $boole-eqv,   $boole-nand, $boole-nor,
	 $boole-andc1, $boole-andc2, $boole-orc1, $boole-orc2,
	 $horizontal-hatch, $vertical-hatch, $cross-hatch,
	 $diagonal-hatch-down, $diagonal-hatch-up,
	 $bricks-stipple, $tiles-stipple,
	 $parquet-stipple, $hearts-stipple,
	 $xor-brush,
	 brush-background,
	 brush-fill-rule,
	 brush-fill-style,
	 brush-foreground,
	 brush-mode,
	 brush-stipple,
	 brush-stretch-mode,
	 brush-tile,
	 brush-ts-x,
	 brush-ts-y,
	 brush?;

  // Text styles
  create <text-style>,
	 <device-font>,
	 fully-merged-text-style?,
	 make-device-font,
	 make-text-style,
	 merge-text-styles,
	 text-style-components,
	 text-style-family,
	 text-style-name,
	 text-style-size,
	 text-style-slant,
	 text-style-strikeout?,
	 text-style-underline?,
	 text-style-weight,
	 text-style?;

  // Style descriptors
  create <style-descriptor>,
         default-background, default-background-setter,
         default-foreground, default-foreground-setter,
         default-text-style, default-text-style-setter;

end module duim-DCs;

define module duim-DCs-internals
  use dylan;
  use duim-imports;
  use duim-utilities;
  use duim-geometry-internals;
  use duim-DCs, export: all;

  // Colors
  export <rgb-color>,
	 $default-foreground,
         $default-background,
         contrasting-color-index,
	 contrasting-color->color;

  // Palettes
  export <basic-palette>,
	 do-add-colors,
         allocate-color,
         deallocate-color,
         do-remove-colors,
         update-palette-entry,
         update-palette-entries;

  // Dynamic and layered colors
  create dynamic-color-palettes, dynamic-color-palettes-setter,
	 layered-color,
	 layered-color-color-setter,
	 make-layered-color-set;

  // Pens
  export <standard-pen>;

  // Brushes
  export <standard-brush>,
         make-stipple,
         \stipple-definer;

  // Images, patterns, and stencils
  export <background>,
	 <foreground>,
	 decode-pattern;

  // Text styles
  export <standard-text-style>,
	 $default-text-style,
	 $null-text-style,
	 $standard-character-set,
	 $undefined-text-style,
         device-font-font,
         device-font-port,
         text-style-face-code;
end module duim-DCs-internals;
||#
