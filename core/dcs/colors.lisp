;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-DCS-INTERNALS -*-
(in-package #:duim-dcs-internals)

#||
/// Colors

define protocol <<color-protocol>> ()
  function color-rgb
    (color :: <color>)
 => (red :: <real>, green :: <real>, blue :: <real>, opacity :: <real>);
  function color-ihs
    (color :: <color>)
 => (intensity :: <real>, hue :: <real>, saturation :: <real>, opacity :: <real>);
  getter color-luminosity (color :: <color>)
 => (value :: <real>);
end protocol <<color-protocol>>;
||#

;;;;
;;;; <<COLOR-PROTOCOL>> - ascertain primitive values for a <COLOR>
;;;; representation.
;;;;

(define-protocol <<color-protocol>> ()
  (:function color-rgb (color)
	     (:documentation
"
Returns four values, the _red_, _green_, _blue_, and _opacity_
components of the color _color_. The values are real numbers between 0
and 1 (inclusive).

Defined by: <<color-protocol>>
"))
  (:function color-ihs (color)
	     (:documentation
"
Returns four values, the _intensity_, _hue_, _saturation_, and
_opacity_ components of the color _color_. The first value is a real
number between 0 and (sqrt 3) inclusive. The second and third values
are real numbers between 0 and 1 (inclusive).

Defined by: <<color-protocol>>
"))
  (:getter color-luminosity (color)
	   (:documentation
"
Returns the brightness of color _color_ as a real number between 0 and
1. The value is the solution of a function that describes the
perception of the color by the human retina.

Defined by: <<color-protocol>>
")))

#||
/// RGB <-> IHS conversions

define constant $ihs-rgb-c1 :: <single-float> = 0.4082483;	// sqrt(1/6)
define constant $ihs-rgb-c2 :: <single-float> = 0.7071068;	// sqrt(1/2)
define constant $ihs-rgb-c3 :: <single-float> = 0.5773503;	// sqrt(1/3)

define constant $sqrt3      :: <single-float> = 1.732051;	// sqrt(3)
||#

;;; Constants useful for converting between IHS (intensity, hue +
;;; saturation) and RGB (red, green + blue)

(defconstant +ihs-rgb-c1+ 0.4082483)	;; sqrt(1/6)
(defconstant +ihs-rgb-c2+ 0.7071068)	;; sqrt(1/2)
(defconstant +ihs-rgb-c3+ 0.5773503)	;; sqrt(1/3)

(defconstant +sqrt3+      1.732051)	;; sqrt(3)

;(declaim (type single-float $ihs-rgb-c1 $ihs-rgb-c2 $ihs-rgb-c3 $sqrt3))

#||
define function ihs->rgb
    (intensity :: <single-float>, hue :: <single-float>, saturation :: <single-float>)
 => (red :: <single-float>, green :: <single-float>, blue :: <single-float>)
  let hh = modulo(hue - 0.5, 1.0) * $2pi - $pi;
  let c3 = cos(saturation);
  let s3 = sin(saturation);
  let cos-hh = cos(hh);
  let sin-hh = sin(hh);
  let x = $ihs-rgb-c1 * s3 * cos-hh * intensity;
  let y = $ihs-rgb-c2 * s3 * sin-hh * intensity;
  let z = $ihs-rgb-c3 * c3 * intensity;
  values(max(0.0, min(1.0, x + x + z)),
	 max(0.0, min(1.0, y + z + -x)),
	 max(0.0, min(1.0, (z - x) - y)))
end function ihs->rgb;
||#

(defun ihs->rgb (intensity hue saturation)
"
Convert _intensity_, _hue_ and _saturation_ values, each of which
should be of type SINGLE-FLOAT, into red, green and blue values
for the colour.
"
  (let* ((hh     (- (* (mod (- hue 0.5) 1.0) +2pi+) +pi+))
	 (c3     (cos saturation))
	 (s3     (sin saturation))
         (cos-hh (cos hh))
         (sin-hh (sin hh))
         (x      (* +ihs-rgb-c1+ s3 cos-hh intensity))
         (y      (* +ihs-rgb-c2+ s3 sin-hh intensity))
         (z      (* +ihs-rgb-c3+ c3 intensity)))
    (values (max 0.0 (min 1.0 (+ x x z)))
	    (max 0.0 (min 1.0 (- (+ y z) x)))
	    (max 0.0 (min 1.0 (- (- z x) y))))))

#||
define function rgb->ihs
    (red :: <single-float>, green :: <single-float>, blue :: <single-float>)
 => (intensity :: <single-float>, hue :: <single-float>, saturation :: <single-float>)
  let x = $ihs-rgb-c1 * (((red + red) - blue) - green);
  let y = $ihs-rgb-c2 * (green - blue);
  let z = $ihs-rgb-c3 * (red + green + blue);
  let q = x * x + y * y;
  let intensity = sqrt(q + z * z);	// == sqrt(r^2 + g^2 + b^2) !
  if (zero?(q))
    // A totally unsaturated color
    values(intensity, 0.0, 0.0)
  else
    let hue = modulo(atan2(y, x) / $2pi, 1.0);
    let f1 = z / intensity;
    let f2 = sqrt(1.0 - f1 * f1);
    let saturation = atan2(f2, f1);
    values(intensity, hue, saturation)
  end
end function rgb->ihs;
||#


(defun rgb->ihs (red green blue)
"
Convert the colour represented by the SINGLE-FLOAT _red_, _green_
and _blue_ values provided into intensity, hue and saturation
values.
"
  (let* ((x (* +ihs-rgb-c1+ (- (- (+ red red) blue) green)))
	 (y (* +ihs-rgb-c2+ (- green blue)))
         (z (* +ihs-rgb-c3+ (+ red green blue)))
         (q (+ (* x x) (* y y)))
         (intensity (sqrt (+ q (* z z)))))	;; == sqrt(r^2 + g^2 + b^2) !
    (if (zerop q)
        ;; A totally unsaturated color
        (values intensity 0.0 0.0)
      (let* ((hue (mod (/ (atan y x) +2pi+) 1.0))
             (f1 (/ z intensity))
             (f2 (sqrt (- 1.0 (* f1 f1))))
             (saturation (atan f2 f1)))
        (values intensity hue saturation)))))

#||
// From Foley and Van Dam, page 613 (discussion of YIQ color model)...
define inline function rgb->luminosity
    (r :: <single-float>, g :: <single-float>, b :: <single-float>)
 => (luminosity :: <single-float>)
  0.299 * r + 0.587 * g + 0.114 * b
end function rgb->luminosity;
||#

;;; RGB->LUMINOSITY : given _red_, _green_ and _blue_ values (as
;;;                   SINGLE-FLOATs), returns the *luminosity*
;;; of the color.

(defun rgb->luminosity (r g b)
"
Return the luminosity of the colour represented by the SINGLE-FLOAT
_red_, _green_ and _blue_ values provided.
"
  (+ (* 0.299 r)
     (* 0.587 g)
     (* 0.114 b)))


#||
/// RGB Colors

define sealed class <rgb-color> (<color>)
  sealed constant slot %red :: <single-float>,
    required-init-keyword: red:;
  sealed constant slot %green :: <single-float>,
    required-init-keyword: green:;
  sealed constant slot %blue :: <single-float>,
    required-init-keyword: blue:;
  sealed constant slot color-opacity :: <single-float> = 1.0,
    init-keyword: opacity:;
end class <rgb-color>;
||#


(defclass <rgb-color> (<color>)
  ((%red :type single-float :initarg :red :initform (required-slot ":red" "<rgb-color>") :reader %red)
   (%green :type single-float :initarg :green :initform (required-slot ":green" "<rgb-color>") :reader %green)
   (%blue :type single-float :initarg :blue :initform (required-slot ":blue" "<rgb-color>") :reader %blue)
   (color-opacity :type single-float :initarg :opacity :initform 1.0 :reader color-opacity))
  (:documentation
"
An <RGB-COLOR> is a colour that is represented by SINGLE-FLOAT
red, green and blue values, and an opacity value.
"))


#||
define method \=
    (color1 :: <color>, color2 :: <color>) => (true? :: <boolean>)
  color1 == color2
  | begin
      let (r1, g1, b1, o1) = color-rgb(color1);
      let (r2, g2, b2, o2) = color-rgb(color2);
      r1 = r2 & g1 = g2 & b1 = b2 & o1 = o2
    end
end method \=;
||#

;;; Compare two instances of <COLOR> for equivalence (are they
;;; the same colour?)
(defmethod equal? ((color1 <color>) (color2 <color>))
  (or (eql color1 color2)
      (multiple-value-bind (r1 g1 b1 o1)
          (color-rgb color1)
        (multiple-value-bind (r2 g2 b2 o2)
            (color-rgb color2)
          (and (= r1 r2)
	       (= g1 g2)
               (= b1 b2)
	       (= o1 o2))))))


#||
// A faster method for two rgb colors...
define method \=
    (color1 :: <rgb-color>, color2 :: <rgb-color>) => (true? :: <boolean>)
  color1 == color2
  | (color1.%red = color2.%red
     & color1.%green = color2.%green
     & color1.%blue = color2.%blue
     & color-opacity(color1) = color-opacity(color2))
end method \=;
||#

;;; Compare two <RGB-COLOR> instances for equivalence (i.e. that they
;;; both represent the same colour). This is hopefully faster than
;;; the general comparison for <COLOR> instances (assuming slot
;;; access is faster than the COLOR-RGB function).

(defmethod equal? ((color1 <rgb-color>) (color2 <rgb-color>))
  (or (eql color1 color2)
      (and (= (%red color1)   (%red color2))
           (= (%green color1) (%green color2))
           (= (%blue color1)  (%blue color2))
           (= (color-opacity color1) (color-opacity color2)))))


#||
define sealed method make
    (class == <color>,
     #key red, green, blue, intensity, hue, saturation, opacity = 1.0)
 => (color :: <rgb-color>)
  if (red | green | blue)
    make-rgb-color(red, green, blue, opacity: opacity)
  else
    make-ihs-color(intensity, hue, saturation, opacity: opacity)
  end
end method make;

define sealed domain make (singleton(<rgb-color>));
define sealed domain initialize (<rgb-color>);


define function make-rgb-color
    (red :: <real>, green :: <real>, blue :: <real>, #key opacity = 1.0)
 => (color :: <rgb-color>)
  assert((0 <= red & 0 <= 1),
         "The red value %= is not a number between 0 and 1", red);
  assert((0 <= green & 0 <= 1),
         "The green value %= is not a number between 0 and 1", green);
  assert((0 <= blue & 0 <= 1),
         "The blue value %= is not a number between 0 and 1", blue);
  make(<rgb-color>,
       red:     as(<single-float>, red),
       green:   as(<single-float>, green),
       blue:    as(<single-float>, blue),
       opacity: as(<single-float>, opacity))
end function make-rgb-color;
||#


(defun make-rgb-color (red green blue &key (opacity 1.0))
"
Returns a member of class <color>. The _red_, _green_, and _blue_
arguments are real numbers between 0 and 1 (inclusive) that specify
the values of the corresponding color components.

When all three color components are 1, the resulting color is
white. When all three color components are 0, the resulting color is
black.
"
  (assert (<= 0 red 1) (red) "The RED value ~a is not a number between 0 and 1" red)
  (assert (<= 0 green 1) (green) "The GREEN value ~a is not a number between 0 and 1" green)
  (assert (<= 0 blue 1) (blue) "The BLUE value ~a is not a number between 0 and 1" blue)
  (make-instance '<rgb-color>
                 :red     (coerce red     'single-float)
                 :green   (coerce green   'single-float)
                 :blue    (coerce blue    'single-float)
                 :opacity (coerce opacity 'single-float)))

;;; Primary colours.

(defparameter *black*   (make-rgb-color 0 0 0)
"
The usual definition of black, the absence of all colors. In the 'rgb'
color model, its value is 000.
")

(defparameter *red*     (make-rgb-color 1 0 0)
"
The usual definition of the color red.
")

(defparameter *green*   (make-rgb-color 0 1 0)
"
The usual definition of the color green.
")

(defparameter *blue*    (make-rgb-color 0 0 1)
"
The usual definition of the color blue.
")

(defparameter *cyan*    (make-rgb-color 0 1 1)
"
The usual definition of the color cyan.
")

(defparameter *magenta* (make-rgb-color 1 0 1)
"
The usual definition of the color magenta.
")

(defparameter *yellow*  (make-rgb-color 1 1 0)
"
The usual definition of the color yellow.
")

(defparameter *white*   (make-rgb-color 1 1 1)
"
The usual definition of white. In the rgb color model, its value is
111.
")


#||
define function make-ihs-color
    (intensity :: <real>, hue :: <real>, saturation :: <real>,
     #key opacity = 1.0)
 => (color :: <rgb-color>)
  assert((0 <= intensity & 0 <= $sqrt3),
         "The intensity value %= is not a number between 0 and sqrt(3)", intensity);
  assert((0 <= hue & 0 <= 1),
         "The hue value %= is not a number between 0 and 1", hue);
  assert((0 <= saturation & 0 <= 1),
         "The saturation value %= is not a number between 0 and 1", saturation);
  case
    intensity = 0 =>
      $black;
    otherwise =>
      let (red, green, blue)
	= ihs->rgb(as(<single-float>, intensity),
		   as(<single-float>, hue),
		   as(<single-float>, saturation));
      make(<rgb-color>,
	   red:     red,
	   green:   green,
	   blue:    blue,
	   opacity: as(<single-float>, opacity))
  end
end function make-ihs-color;
||#

;;; Creates a <COLOR> instance from the _intensity_, _hue_, _saturation_
;;; and optional _opacity_ values provided.

(defun make-ihs-color (intensity hue saturation &key (opacity 1.0))
"
Returns a member of class <color>. The _intensity_ argument is a real
number between 0 and sqrt(3) (inclusive). The _hue_ and _saturation_
arguments are real numbers between 0 and 1 (inclusive).
"
  (assert (<= 0 intensity +sqrt3+) (intensity) "The INTENSITY value ~a is not a number between 0 and sqrt(3)" intensity)
  (assert (<= 0 hue 1) (hue) "The HUE value ~a is not a number between 0 and 1" hue)
  (assert (<= 0 saturation 1) (saturation) "The SATURATION value ~a is not a number between 0 and 1" saturation)
  (if (= intensity 0)
      *black*
      (multiple-value-bind (red green blue)
	  (ihs->rgb (coerce intensity  'single-float)
		    (coerce hue        'single-float)
		    (coerce saturation 'single-float))
	(make-rgb-color red
			green
			blue
			:opacity (coerce opacity 'single-float)))))


#||
define function make-gray-color
    (luminosity :: <real>, #key opacity = 1.0)
 => (color :: <rgb-color>)
  assert((0 <= luminosity & 0 <= 1),
         "The luminosity %= is not a number between 0 and 1", luminosity);
  case
    luminosity = 0 & opacity = 1.0 =>
      $black;
    luminosity = 1 & opacity = 1.0 =>
      $white;
    otherwise =>
      let luminosity = as(<single-float>, luminosity);
      make(<rgb-color>,
	   red:     luminosity,
	   green:   luminosity,
	   blue:    luminosity,
	   opacity: as(<single-float>, opacity))
  end
end function make-gray-color;
||#


(defun make-gray-color (luminosity
                        &key
                        (opacity 1.0))
"
Returns a member of class <color>. The _luminance_ is a real number
between 0 and 1 (inclusive). On a black-on-white display device, 0
means black, 1 means white, and the values in between are shades of
gray. On a white-on-black display device, 0 means white, 1 means
black, and the values in between are shades of gray.
"
  (assert (<= 0 luminosity 1) (luminosity)
	  "The luminosity ~a is not a number between 0 and 1" luminosity)
  (cond ((and (= luminosity 0) (= opacity 1.0)) *black*)
        ((and (= luminosity 1) (= opacity 1.0)) *white*)
        (t
	 (let ((luminosity (coerce luminosity 'single-float)))
           (make-rgb-color luminosity
			   luminosity
			   luminosity
			   :opacity (coerce opacity 'single-float))))))


#||
// The primary colors, constant across all platforms
define constant $black   :: <rgb-color> = make-rgb-color(0, 0, 0);
define constant $red     :: <rgb-color> = make-rgb-color(1, 0, 0);
define constant $green   :: <rgb-color> = make-rgb-color(0, 1, 0);
define constant $blue    :: <rgb-color> = make-rgb-color(0, 0, 1);
define constant $cyan    :: <rgb-color> = make-rgb-color(0, 1, 1);
define constant $magenta :: <rgb-color> = make-rgb-color(1, 0, 1);
define constant $yellow  :: <rgb-color> = make-rgb-color(1, 1, 0);
define constant $white   :: <rgb-color> = make-rgb-color(1, 1, 1);
||#

#||
define sealed inline method color-rgb
    (color :: <rgb-color>)
 => (red :: <single-float>, green :: <single-float>, blue :: <single-float>,
     opacity :: <single-float>);
  values(color.%red, color.%green, color.%blue, color-opacity(color))
end method color-rgb;
||#


(defmethod color-rgb ((color <rgb-color>))
"
Returns the red, green, blue and opacity components of _color_.
"
  (values (%red color)
	  (%green color)
	  (%blue color)
	  (color-opacity color)))


#||
define sealed method color-ihs
    (color :: <rgb-color>)
 => (intensity :: <single-float>, hue :: <single-float>, saturation :: <single-float>,
     opacity :: <single-float>);
  let (intensity, hue, saturation)
    = rgb->ihs(color.%red, color.%green, color.%blue);
  values(intensity, hue, saturation, color-opacity(color))
end method color-ihs;
||#

(defmethod color-ihs ((color <rgb-color>))
"
Returns the components of _color_ as intensity, hue, saturation and
opacity values.
"
  (multiple-value-bind (intensity hue saturation)
      (rgb->ihs (%red color)
		(%green color)
		(%blue color))
    (values intensity
	    hue
	    saturation
	    (color-opacity color))))


#||
define sealed method color-luminosity
    (color :: <rgb-color>) => (luminosity :: <single-float>)
  rgb->luminosity(color.%red, color.%green, color.%blue)
end method color-luminosity;
||#

(defmethod color-luminosity ((color <rgb-color>))
"
Returns the luminosity value for _color_.
"
  (rgb->luminosity (%red color)
		   (%green color)
		   (%blue color)))


#||
/// Foreground and background (indirect) inks

define sealed class <foreground> (<ink>)
end class <foreground>;
||#


(defclass <foreground> (<ink>) ()
  (:documentation
"
An indirect colour allowing drawing to be performed in whatever
colour is set for the foreground instead of having to specify a
colour directly.
"))

#||
define sealed domain make (singleton(<foreground>));
define sealed domain initialize (<foreground>);

define constant $foreground :: <foreground> = make(<foreground>);
||#

(defvar *foreground* (make-instance '<foreground>)
"
An indirect ink that uses the medium's foreground design.
")

#||
// The default defaults, if no others can be found anywhere
define constant $default-foreground :: <rgb-color> = $black;
||#

(defvar *default-foreground* *black*)

#||
define sealed class <background> (<ink>)
end class <background>;
||#


(defclass <background> (<ink>) ()
  (:documentation
"
An indirect colour allowing drawing to be performed in whatever
colour is set for the background instead of having to specify a
colour directly.
"))

#||
define sealed domain make (singleton(<background>));
define sealed domain initialize (<background>);

define constant $background :: <background> = make(<background>);
||#

(defvar *background* (make-instance '<background>)
"
An indirect ink that uses the medium's background design.
")

#||
define constant $default-background :: <rgb-color> = $white;
||#

(defvar *default-background* *white*)


#||
/// Contrasting colors

define sealed class <contrasting-color> (<color>)
  sealed constant slot %how-many  :: <integer>,
    required-init-keyword: how-many:;
  sealed constant slot %which-one :: <integer>,
    required-init-keyword: which-one:;
end class <contrasting-color>;
||#

;;; TODO: where should the docs for 'make-color-for-contrasting-color' go?
#||
Returns a color that is recognisably different from the main color.
||#

(defclass <contrasting-color> (<color>)
  ((%how-many :type fixnum :initarg :how-many :initform (required-slot ":how-many" "<contrasting-color>") :reader %how-many)
   (%which-one :type fixnum :initarg :which-one :initform (required-slot ":which-one" "<contrasting-color>") :reader %which-one))
  (:documentation
"
A cooperative set of colours that will contrast with each other.
"))


#||
define sealed domain make (singleton(<contrasting-color>));
define sealed domain initialize (<contrasting-color>);

//--- 8 is pretty small
define constant $contrasting-colors-count :: <integer> = 8;
||#

;;; FIXME: TODO: Would using an EVAL-WHEN here make +CONTRASTING-COLORS-COUNT+
;;; available to the compilation environment?

(defconstant +contrasting-colors-count+ 8)

#||
define sealed method make-contrasting-colors
    (n :: <integer>, #key k = $unsupplied) => (colors)
  check-type(n, limited(<integer>, min: 2, max: $contrasting-colors-count));
  if (unsupplied?(k))
    let result :: <simple-object-vector> = make(<simple-vector>, size: n);
    without-bounds-checks
      for (k :: <integer> from 0 below n)
	result[k] := make(<contrasting-color>, which-one: k, how-many: n)
      end
    end;
    result
  else
    assert(k < n,
	   "The index %d must be smaller than the count %d", k, n);
    make(<contrasting-color>, which-one: k, how-many: n)
  end
end method make-contrasting-colors;
||#

;;; Constructs either a single <CONTRASTING-COLOR> instance (if the _index_
;;; parameter is provided) or a vector of <CONSTRASTING-COLOR> instances
;;; otherwise.

(defun make-contrasting-colors (count &key (index :unsupplied))
"
Returns a vector of n colors with recognizably different
appearance. Elements of the vector are guaranteed to be acceptable
values for the :brush argument to the drawing functions, and do not
include $foreground, $background, or nil. Their class is otherwise
unspecified. The vector is a fresh object that may be modified.

If :index is supplied, it must be an integer between 0 and _count_ -
1 (inclusive), in which case 'make-contrasting-colors' returns the kth
color in the vector rather than the whole vector.

If the implementation does not have _count_ different contrasting
colors, 'make-contrasting-colors' signals an error. This does not
happen unless _count_ is greater than eight.

The rendering of the color is a true color or a stippled pattern,
depending on whether the output medium supports color.
"
  (check-type count (integer 2 8))  ;; #.$contrasting-colors-count)) ; FIXME - this is ugly
  (if (unsupplied? index)
      (let ((result (MAKE-SIMPLE-VECTOR :size count)))
	(without-bounds-checks
          (loop for index from 0 below count
                do (setf (svref result index)
                         (make-instance '<contrasting-color>
                                        :which-one index
                                        :how-many count))))
	result)
    (progn
      (assert (< index count) (index count)
	      "The index ~d must be smaller than the count ~d" index count)
      (make-instance '<contrasting-color>
                     :which-one index
                     :how-many count))))

#||
define method contrasting-colors-limit
    (port) => (limit :: <integer>)
  $contrasting-colors-count
end method contrasting-colors-limit;
||#


(defgeneric contrasting-colors-limit (port)
  (:documentation
"
Returns the number of contrasting colors (or stipple patterns if
_port_ is monochrome or grayscale) that can be rendered on any medium
on the port _port_. Implementations are encouraged to make this as
large as possible, but it must be at least 8. All classes that obey
the medium protocol must implement a method for this generic function.
"))


(defmethod contrasting-colors-limit ((port t))
"
This default method returns the core code contrasting colors limit
which is held in +contrasting-colors-count+, which is 8.
"
  +contrasting-colors-count+)


#||
define sealed inline method contrasting-color-index
    (color :: <contrasting-color>) => (which :: <integer>, how-many :: <integer>)
  values(color.%which-one, color.%how-many)
end method contrasting-color-index;
||#

;;; Returns the _index_ of the <CONTRASTING-COLOR> instance and the
;;; total _count_ of contrasting colors.

(defgeneric contrasting-color-index (color)
  (:documentation
"
Returns the index of _color_ and the total count of contrasting
colors.
"))


(defmethod contrasting-color-index ((color <contrasting-color>))
  (values (%which-one color)
          (%how-many color)))


#||
define constant $contrasting-colors :: <simple-object-vector>
    = vector($red, $blue, $green, $yellow, $cyan, $magenta, $black, $white);
||#


(defvar *contrasting-colors*
  (vector *red* *blue* *green* *yellow* *cyan* *magenta* *black* *white*)
"
The default system-supplied contrasting colors.
")


#||
define sealed inline method contrasting-color->color
    (color :: <contrasting-color>) => (color :: <rgb-color>)
  $contrasting-colors[color.%which-one]
end method contrasting-color->color;
||#

(defgeneric contrasting-color->color (color)
  (:documentation
"
Given a <CONTRASTING-COLOR> instance _color_, return the <COLOR>
instance it represents.
"))


(defmethod contrasting-color->color ((color <contrasting-color>))
"
Returns the <COLOR> instance of _color_ in the system-supplied default
contrasting color palette.
"
  (svref *contrasting-colors* (%which-one color)))


#||
define sealed method color-rgb
    (color :: <contrasting-color>)
 => (red :: <single-float>, green :: <single-float>, blue :: <single-float>,
     opacity :: <single-float>);
  color-rgb(contrasting-color->color(color))
end method color-rgb;
||#

(defmethod color-rgb ((color <contrasting-color>))
"
Returns the red, green, blue and opacity values for _color_.
"
  (color-rgb (contrasting-color->color color)))


#||
define sealed method color-ihs
    (color :: <contrasting-color>)
 => (intensity :: <single-float>, hue :: <single-float>, saturation :: <single-float>,
     opacity :: <single-float>);
  color-ihs(contrasting-color->color(color))
end method color-ihs;
||#

(defmethod color-ihs ((color <contrasting-color>))
"
Returns the intensity, hue, saturation and opacity values for _color_.
"
  (color-ihs (contrasting-color->color color)))


#||
define sealed method color-luminosity
    (color :: <contrasting-color>) => (luminosity :: <single-float>)
  color-luminosity(contrasting-color->color(color))
end method color-luminosity;
||#

(defmethod color-luminosity ((color <contrasting-color>))
"
Returns the luminosity value of _color_.
"
  (color-luminosity (contrasting-color->color color)))

