;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-SHEETS-INTERNALS -*-
(in-package #:duim-sheets-internals)

#||
/// Macros that are best defined early on

/// Medium binding macros

// Allocates a medium, and attaches it to the sheet
define macro with-sheet-medium
  { with-sheet-medium (?medium:name = ?sheet:expression) ?:body end }
    => { begin
           let with-sheet-medium-body = method (?medium) ?body end;
           do-with-sheet-medium(?sheet, with-sheet-medium-body)
         end }
end macro with-sheet-medium;
||#

(defmacro with-sheet-medium ((mediumvar = sheet) &body body)
"
Associates a sheet with a medium.

Within _body_, the variable _medium_ is bound to the medium allocated
to _sheet_. The _sheet_ specified should be an instance of type
<sheet>. If _sheet_ does not have a medium permanently allocated, one
is allocated and associated with _sheet_ for the duration of _body_,
and then unassociated from _sheet_ and deallocated when _body_ has
been exited. The values of the last form of _body_ are returned as the
values of 'with-sheet-medium'.

The _medium_ argument is not evaluated, and must be a symbol that will
be bound to a medium. The _body_ may have zero or more declarations as
its first forms.

This macro is a useful way of speeding up drawing operations, since
drawing on a sheet requires finding the medium for that sheet. You can
use 'with-sheet-medium' to associate a known sheet with a medium, and
then draw directly onto that medium, as shown in the example.

Example

    (with-sheet-medium (medium = sheet)
      (with-drawing-options (medium :brush color)
        (loop for x from 0 to 199
              do (loop for y from 0 to 199
                       (do draw-point(medium x y))))))
"
  (declare (ignore =))
  (let ((with-sheet-medium-body (gensym)))
    `(let* ((,with-sheet-medium-body #'(lambda (,mediumvar)
                                         ,@body)))
      (do-with-sheet-medium ,sheet ,with-sheet-medium-body))))


#||
// Allocates a temporary medium, and attaches it to the sheet
define macro with-temporary-medium
  { with-temporary-medium (?medium:name = ?sheet:expression) ?:body end }
    => { begin
           let _sheet = ?sheet;
           let ?medium = allocate-medium(port(_sheet), _sheet);
           block ()
             ?body
           cleanup
             deallocate-medium(port(_sheet), ?medium)
           end
         end }
end macro with-temporary-medium;
||#

(defmacro with-temporary-medium ((medium = sheet) &body body)
  (declare (ignore =))
  (let ((_sheet (gensym)))
  `(let* ((,_sheet ,sheet)
          (,medium (allocate-medium (port ,_sheet) ,_sheet)))
    (unwind-protect
	 (progn ,@body)
      (deallocate-medium (port ,_sheet) ,medium)))))



#||
/// Drawing state macros

define macro with-drawing-options
  { with-drawing-options (?medium:name, #rest ?options:expression) ?:body end }
    => { begin
	   let with-drawing-options-body = method () ?body end;
	   do-with-drawing-options(?medium, with-drawing-options-body, ?options)
	 end }
end macro with-drawing-options;
||#

(defmacro with-drawing-options ((medium &rest options) &body body)
"
 Runs a body of code in the context of a set of drawing options. The
 options specified are passed to the function 'do-with-drawing-options'
 for execution.

 The 'medium' expression should evaluate to an instance of <medium>.

 Note that when using 'with-drawing-options' in conjunction with a
 loop. it is computationally much quicker to use a medium (as shown
 here) rather than a sheet, and to place the call to
 'with-drawing-options' outside the loop. If necessary, use
 'with-sheet-medium' to associate the sheet with the medium, thus:

    (with-sheet-medium (medium = sheet)
      (with-drawing-options (medium :brush color)
        (loop for x from 0 to 199
              do (loop for y from 0 to 199
                       do (draw-point medium x y)))))

 Example

    (with-drawing-options (medium :brush *red*)
      (draw-rectangle medium 0 0 100 200 :filled? t))
"
  (let ((with-drawing-options-body (gensym)))
    `(let ((,with-drawing-options-body #'(lambda () ,@body)))
      (do-with-drawing-options ,medium ,with-drawing-options-body ,@options))))


#||
define macro with-pen
  { with-pen (?medium:name, #rest ?options:expression) ?:body end }
    => { begin
	   let with-pen-body = method () ?body end;
	   let _pen = make(<pen>, ?options);
	   do-with-drawing-options(?medium, with-pen-body, pen: _pen)
	 end }
end macro with-pen;
||#

(defmacro with-pen ((medium &rest options) &body body)
"
Executes _body_ using the pen characteristics specified by _options_,
and applies the results to the expression _medium_.

The _medium_ specified should be an instance of type <medium>. The
_options_ can be any valid arguments that specify an instance of
<pen>.
"
  (let ((_pen (gensym))
        (with-pen-body (gensym)))
    `(let ((,with-pen-body #'(lambda () ,@body))
           (,_pen (make-pen ,@options)))
      (do-with-drawing-options ,medium ,with-pen-body :pen ,_pen))))


#||
define macro with-brush
  { with-brush (?medium:name, #rest ?options:expression) ?:body end }
    => { begin
	   let with-brush-body = method () ?body end;
	   let _brush = make(<brush>, ?options);
	   do-with-drawing-options(?medium, with-brush-body, brush: _brush)
	 end }
end macro with-brush;
||#

(defmacro with-brush ((medium &rest options) &body body)
"
Executes _body_ using the brush characteristics specified by
_options_, and applies the results to _medium_. The _medium_ specified
should be an instance of type <medium>. The _options_ can be any valid
arguments that specify an instance of <brush>.
"
  (let ((with-brush-body (gensym))
        (_brush (gensym)))
    `(let ((,with-brush-body #'(lambda () ,@body))
           (,_brush (make-brush ,@options)))
      (do-with-drawing-options ,medium ,with-brush-body :brush ,_brush))))


#||
define macro with-text-style
  { with-text-style (?medium:name, #rest ?options:expression) ?:body end }
    => { begin
	   let with-text-style-body = method () ?body end;
	   let _text-style = make(<text-style>, ?options);
	   do-with-text-style(?medium, with-text-style-body, _text-style)
	 end }
end macro with-text-style;
||#

(defmacro with-text-style ((medium &rest options) &body body)
"
Executes _body_ using the text style characteristics specified by
_options_, and applies the results to _medium_.

The _medium_ specified should be an instance of type <medium>. The
_options_ can be any valid arguments that specify an instance of
<text-style>.

Methods for 'do-with-text-style' are invoked to run the code.
"
  (let ((with-text-style-body (gensym))
        (_text-style (gensym)))
    `(let ((,with-text-style-body #'(lambda () ,@body))
           (,_text-style (make-text-style ,@options)))
      (do-with-text-style ,medium ,with-text-style-body ,_text-style))))


#||
define macro with-atomic-redisplay
  { with-atomic-redisplay (?sheet:name) ?:body end }
    => { begin
           let with-atomic-redisplay-body = method (?sheet) ?body end;
           do-with-atomic-redisplay(?sheet, with-atomic-redisplay-body)
         end }
end macro with-atomic-redisplay;
||#

(defmacro with-atomic-redisplay ((sheet) &body body)
  (let ((with-atomic-redisplay-body (gensym)))
    `(let ((,with-atomic-redisplay-body #'(lambda (,sheet) ,@body)))
      (do-with-atomic-redisplay ,sheet ,with-atomic-redisplay-body))))



#||
/// Medium transform hacking

define macro with-transform
  { with-transform (?medium:name, ?transform:expression) ?:body end }
    => { begin
           let with-transform-body = method () ?body end;
           do-with-transform(?medium, with-transform-body, ?transform)
         end }
end macro with-transform;
||#

(defmacro with-transform ((medium transform) &body body)
"
Executes a body of code with a specified _transform_. The transform
occurs within _medium_. This macro is used by 'with-rotation',
'with-scaling', and 'with-translation', and calls methods for
'do-with-transform'.

The _medium_ specified should be an instance of type <medium>. The
_transform_ specified should be an instance of type <transform>.
"
  (let ((with-transform-body (gensym)))
    `(let ((,with-transform-body #'(lambda () ,@body)))
      (do-with-transform ,medium ,with-transform-body ,transform))))


#||
define macro with-translation
  { with-translation (?medium:name, ?dx:expression, ?dy:expression)
      ?:body
    end }
    => { with-transform (?medium, make-translation-transform(?dx, ?dy))
           ?body
         end }
end macro with-translation;
||#

(defmacro with-translation ((medium dx dy) &body body)
"
Executes a body of code with a specified translation, denoted by _dx_
and _dy_. The translation occurs within _medium_. This macro calls
'with-transform' to perform the translation.

The _medium_ specified should be an instance of type <medium>. The
_dx_ and _dy_ should evaluate to an instance of type <real>.
"
  `(with-transform (,medium (make-translation-transform ,dx ,dy))
    ,@body))


#||
define macro with-rotation
  { with-rotation (?medium:name, ?angle:expression, #key ?x:expression, ?y:expression)
      ?:body
    end }
    => { with-transform (?medium, make-rotation-transform(?angle, origin-x: ?x, origin-y: ?y))
           ?body
         end }
  { with-rotation (?medium:name, ?angle:expression)
      ?:body
    end }
    => { with-transform (?medium, make-rotation-transform(?angle))
           ?body
         end }
end macro with-rotation;
||#

(defmacro with-rotation ((medium angle &optional &key x y) &body body)
"
Executes a body of code with a specified rotation. The rotation occurs
within the expression _medium_. This macro calls 'with-transform' to
perform the rotation.

The _medium_specified should be an instance of type <medium>. The
_angle_ should evaluate to an instance of type <real>.
"
  (if (and x y)
      `(with-transform (,medium (make-rotation-transform ,angle :origin-x ,x :origin-y ,y)) ,@body)
      `(with-transform (,medium (make-rotation-transform ,angle)) ,@body)))


#||
define macro with-scaling
  { with-scaling (?medium:name, ?sx:expression, ?sy:expression)
      ?:body
    end }
    => { with-transform (?medium, make-scaling-transform(?sx, ?sy))
           ?body
         end }
end macro with-scaling;
||#

(defmacro with-scaling ((medium sx sy) &body body)
"
Executes a body of code with a specified scaling, denoted by _scale-x_
and _scale-y_. The scaling occurs within the expression _medium_. This
macro calls 'with-transform' to perform the scaling.

The _medium_ specified should be an instance of type <medium>. The
_scale-x_ and _scale-y_ should evaluate to an instance of type <real>.
"
  `(with-transform (,medium (make-scaling-transform ,sx ,sy))
    ,@body))


#||
define macro with-identity-transform
  { with-identity-transform (?medium:name) ?:body end }
    => { dynamic-bind (medium-transform(?medium) = $identity-transform)
           ?body
         end }
end macro with-identity-transform;
||#

(defmacro with-identity-transform ((medium) &body body)
"
Executes _body_ while retaining the current transform for _medium_.

The _medium_ expression should evaluate to an instance of <medium>.
"
  `(dynamic-bind (((medium-transform ,medium) = *identity-transform*))
    ,@body))


#||
define macro with-clipping-region
  { with-clipping-region (?medium:name, ?region:expression) ?:body end }
    => { dynamic-bind (medium-clipping-region(?medium) = ?region)
           ?body
         end }
end macro with-clipping-region;
||#

(defmacro with-clipping-region ((medium region) &body body)
"
Executes body using the clipping region specified by _region_, and
applies the results to _medium_. The _region_ and _medium_ expressions
should evaluate to instances of <region> and <medium>, respectively.
"
  `(dynamic-bind (((medium-clipping-region ,medium) = ,region))
    ,@body))



#||
/// Frame managers

define macro with-frame-manager
  { with-frame-manager (?framem:expression) ?:body end }
    => { dynamic-bind (*current-frame-manager* = ?framem)
	   ?body
	 end }
end macro with-frame-manager;
||#

(defmacro with-frame-manager ((framem) &body body)
"
Executes _body_ in the context of _framem_, by dynamically binding the
expression _framem_ to *current-frame-manager*.

In practice, you do not need to use 'with-frame-manager' unless you
are certain that your code needs to run on a non-primary frame
manager.

The main place where you need to use this macro is when you call
'make-pane' to create a gadget outside of one of the pane or layout
clauses in 'define-frame'.

Unless you are developing code that needs to run on more than one
platform, this is unlikely to be the case, and you can forego use of
this macro.
"
  `(dynamic-let ((*current-frame-manager* ,framem))
     ,@body))

;;; TODO: What to do about the non-existent macro 'with-cursor-visible'?
#||
Executes _body_ on the specified _sheet_. If _visible?_ is true, then
the pointer cursor associated with _sheet_ is visible throughout the
operation. If _visible?_ is false, then the pointer cursor is hidden.

The expression _sheet_ should evaluate to an instance of <sheet>. The
expression _visible?_ should evaluate to a boolean value.
||#

;;; TODO: what to do with 'withdraw-sheet' which doesn't exist:
#||
Withdraws the specified sheet from the current display.
||#
