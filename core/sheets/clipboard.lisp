;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-SHEETS-INTERNALS -*-
(in-package #:duim-sheets-internals)

#||
/// Clipboard

define constant <clipboard-type> = type-union(<symbol>, <type>);

define protocol <<clipboard-protocol>> ()
  function open-clipboard
    (port :: <abstract-port>, sheet :: <abstract-sheet>)
 => (clipboard :: false-or(<clipboard>));
  function close-clipboard
    (port :: <abstract-port>, clipboard :: <clipboard>) => ();
  getter clipboard-sheet
    (clipboard :: <clipboard>) => (sheet :: <abstract-sheet>);
  getter clipboard-owner
    (clipboard :: <clipboard>) => (owner :: false-or(<abstract-sheet>));
  function add-clipboard-data
    (clipboard :: <clipboard>, data) => (success? :: <boolean>);
  function add-clipboard-data-as
    (type :: <clipboard-type>, clipboard :: <clipboard>, data)
 => (success? :: <boolean>);
  function clear-clipboard
    (clipboard :: <clipboard>) => ();
  function maybe-clear-clipboard
    (clipboard :: <clipboard>) => ();
  function clipboard-data-available?
    (type :: <clipboard-type>, clipboard :: <clipboard>) => (available? :: <boolean>);
  function get-clipboard-data-as
    (type :: <clipboard-type>, clipboard :: <clipboard>) => (data);
end protocol <<clipboard-protocol>>;


(define-protocol <<clipboard-protocol>> ()
  (:function open-clipboard (port sheet))
  (:function close-clipboard (port clipboard))
  (:getter   clipboard-sheet (clipboard))
  (:getter   clipboard-owner (clipboard))
  (:function add-clipboard-data (clipboard data))
  (:function add-clipboard-data-as (type clipboard data))
  (:function clear-clipboard (clipboard))
  (:function maybe-clear-clipboard (clipboard))
  (:function clipboard-data-available? (type clipboard))
  (:function get-clipboard-data-as (type clipboard)))
||#

#||
// Note that the variable clipboard will be #f if we fail to get the clipboard
define macro with-clipboard
  { with-clipboard (?clipboard:name = ?sheet:expression)
      ?:body
    end }
    => { begin
	   let ?clipboard = open-clipboard(port(?sheet), ?sheet);
	   block ()
	     ?body
	   cleanup
	     when (?clipboard)
	       close-clipboard(port(?sheet), ?clipboard)
	     end
	   end
         end }
end macro with-clipboard;
||#

(defmacro with-clipboard ((clipboard = sheet) &body body)
"
Evaluates _body_ with the clipboard grabbed, returning the results to
the clipboard.

The macro grabs a lock on the clipboard, using 'open-clipboard', and
then executes _body_. Once the results of evaluating _body_ have been
sent to the clipboard, the clipboard lock is freed using
'close-clipboard'. The _clipboard_ argument is a Dylan
variable-namebnf used locally in the call to 'with-clipboard'. The
_sheet_ argument is a Dylan variable-namebnf that evaluates to the
sheet associated with _clipboard_.

This macro is the easiest way of manipulating the clipboard from DUIM,
since it removes the need to create and destroy a clipboard lock
yourself.

You can add more than one format of your data to the clipboard within
the scope of this macro. So, for example, you could place an arbitrary
object onto the clipboard, for use within your own application, and a
string representation for other tools applications to see.
"
  (declare (ignore =))
  ;; Protect `sheet'' against multiple evaluation, but allow ``clipboard''
  ;; to be visible within ``body''. (anaphoric macro?)
  (let ((gsheet (gensym "SHEET-")))
  `(let* ((,gsheet ,sheet)
	  (,clipboard (open-clipboard (port ,gsheet) ,gsheet)))
     (unwind-protect
	 (progn
	   ,@body)
       (when ,clipboard
	 (close-clipboard (port ,gsheet) ,clipboard))))))



#||
/// Default clipboard support

define function clipboard-format-error
    (type :: <clipboard-type>)
  error("Unrecognized clipboard format %=", type)
end function clipboard-format-error;
||#

(defun clipboard-format-error (type)
  (error "Unrecognized clipboard format ~a" type))


#||
define method add-clipboard-data-as
    (type :: <clipboard-type>, clipboard :: <clipboard>, object)
 => (success? :: <boolean>)
  clipboard-format-error(type)
end method add-clipboard-data-as;
||#

(defmethod add-clipboard-data-as (type (clipboard <clipboard>) object)
  ;; Default method just signals an error
  (declare (ignore object))
  (clipboard-format-error type))


#||
define method clipboard-data-available?
    (type :: <clipboard-type>, clipboard :: <clipboard>)
 => (available? :: <boolean>)
  clipboard-format-error(type)
end method clipboard-data-available?;
||#

(defmethod clipboard-data-available? (type (clipboard <clipboard>))
  ;; Default method just signals an error
  (clipboard-format-error type))


#||
define method get-clipboard-data-as
    (type :: <clipboard-type>, clipboard :: <clipboard>)
 => (data)
  clipboard-format-error(type)
end method get-clipboard-data-as;
||#

(defmethod get-clipboard-data-as (type (clipboard <clipboard>))
  ;; Default method just signals an error
  (clipboard-format-error type))



#||
/// Clipboard support for strings

// Note: backends do most of the work here
define method add-clipboard-data
    (clipboard :: <clipboard>, string :: <string>)
 => (success? :: <boolean>)
  add-clipboard-data-as(<string>, clipboard, string)
end method add-clipboard-data;
||#

;; Note: backends do most of the work here
(defmethod add-clipboard-data ((clipboard <clipboard>) (string string))
  (add-clipboard-data-as (find-class 'string) clipboard string))



#||
/// Clipboard support for Dylan objects

define variable *dylan-clipboard-owner* :: false-or(<sheet>) = #f;
define variable *dylan-clipboard-value* :: <object> = #f;
||#

(defparameter *dylan-clipboard-owner* nil)
(defparameter *dylan-clipboard-value* nil)


#||
define method add-clipboard-data
    (clipboard :: <clipboard>, object :: <object>)
 => (success? :: <boolean>)
  add-clipboard-data-as(<object>, clipboard, object)
end method add-clipboard-data;
||#

(defmethod add-clipboard-data ((clipboard <clipboard>) object)
  (add-clipboard-data-as (find-class 'standard-class) clipboard object))


#||
define method add-clipboard-data-as
    (type == <object>, clipboard :: <clipboard>, object :: <object>)
 => (success? :: <boolean>)
  maybe-clear-clipboard(clipboard);
  *dylan-clipboard-owner* := clipboard-sheet(clipboard);
  *dylan-clipboard-value* := object;
  #t
end method add-clipboard-data-as;
||#

(defmethod add-clipboard-data-as ((type (eql (find-class 'standard-class))) (clipboard <clipboard>) object)
  (maybe-clear-clipboard clipboard)
  (setf *dylan-clipboard-owner* (clipboard-sheet clipboard))
  (setf *dylan-clipboard-value* object)
  t)


#||
define method clipboard-data-available?
    (type == <object>, clipboard :: <clipboard>)
 => (available? :: <boolean>)
  clipboard-owner(clipboard) == *dylan-clipboard-owner*
end method clipboard-data-available?;
||#

(defmethod clipboard-data-available? ((type (eql (find-class 'standard-class))) (clipboard <clipboard>))
  (eql (clipboard-owner clipboard) *dylan-clipboard-owner*))


#||
define method get-clipboard-data-as
    (type == <object>, clipboard :: <clipboard>)
 => (object :: <object>)
  clipboard-data-available?(type, clipboard)
  & *dylan-clipboard-value*
end method get-clipboard-data-as;
||#

(defmethod get-clipboard-data-as ((type (eql (find-class 'standard-class))) (clipboard <clipboard>))
  (and (clipboard-data-available? type clipboard)
       *dylan-clipboard-value*))


#||
define method clear-clipboard
    (clipboard :: <clipboard>) => ()
  *dylan-clipboard-owner* := #f;
  *dylan-clipboard-value* := #f;
end method clear-clipboard;
||#

(defmethod clear-clipboard ((clipboard <clipboard>))
  (setf *dylan-clipboard-owner* nil
	*dylan-clipboard-value* nil))


