;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-SHEETS-INTERNALS -*-
(in-package #:duim-sheets-internals)

(define-protocol <<caret-protocol>> ()
  (:function make-caret (port sheet &key x y width height) (:documentation " Defined by: <<CARET-PROTOCOL>> "))
  (:getter   caret-sheet (caret)
	     (:documentation
"
Returns the sheet that owns _caret_.

Defined by: <<caret-protocol>>
"))
  (:function caret-position (caret)
	     (:documentation
"
Returns the position of _caret_.

TODO: units? Pixels, or line+column counts?

Defined by: <<caret-protocol>>
"))
  (:function set-caret-position (caret x y &key fast?)
	     (:documentation
"
Sets the position of _caret_ to (_x_, _y_).

Defined by: <<caret-protocol>>
"))
  (:function do-set-caret-position (caret x y) (:documentation " Defined by: <<CARET-PROTOCOL>> "))
  (:function caret-size (caret)
	     (:documentation
"
Returns the size of _caret_.

Defined by: <<caret-protocol>>
"))
  (:function set-caret-size (caret width height) (:documentation " Defined by: <<CARET-PROTOCOL>> "))
  (:function do-set-caret-size (caret width height) (:documentation " Defined by: <<CARET-PROTOCOL>> "))
  (:getter   caret-visible? (caret)
	     (:documentation
"
Returns true if _caret_ is visible.

Defined by: <<caret-protocol>>
"))
  (:setter   (setf caret-visible?) (visible? caret &key tooltip?)
	     (:documentation
"
Specifies whether or not _caret_ is visible.

Defined by: <<caret-protocol>>
"))
  (:function do-show-caret (caret &key tooltip?) (:documentation " Defined by: <<CARET-PROTOCOL>> "))
  (:function do-hide-caret (caret &key tooltip?) (:documentation " Defined by: <<CARET-PROTOCOL>> ")))


(define-protocol <<clipboard-protocol>> ()
  (:function open-clipboard (port sheet)
	     (:documentation
"
Creates a clipboard lock for _sheet_ on _port_. Once a clipboard lock
has been created, you can manipulate the clipboard contents safely. An
instance of <clipboard> is returned, which is used to hold the
clipboard contents.

You should not normally call 'open-clipboard' yourself to create a
clipboard lock. Use the macro 'with-clipboard' to create and free the
lock for you.

Defined by: <<clipboard-protocol>>
"))
  (:function close-clipboard (port clipboard)
	     (:documentation
"
Closes the current clipboard lock for _sheet_ on _port_. A clipboard
lock needs to be closed safely after the clipboard has been used, to
free the clipboard for further use.

You should not normally call 'close-clipboard' yourself to close a
clipboard lock. Use the macro 'with-clipboard' to create and free the
lock for you.

Defined by: <<clipboard-protocol>>
"))
  (:getter   clipboard-sheet (clipboard)
	     (:documentation
"
Returns the sheet with the clipboard lock.

Defined by: <<clipboard-protocol>>
"))
  (:getter   clipboard-owner (clipboard)
	     (:documentation
"
Returns the sheet that owns the current clipboard data.

Defined by: <<clipboard-protocol>>
"))
  (:function add-clipboard-data (clipboard data)
	     (:documentation
"
This generic function adds _data_ to _clipboard_. It returns t if
_data_ was successfully added to the clipboard.

Defined by: <<clipboard-protocol>>
"))
  (:function add-clipboard-data-as (type clipboard data)
	     (:documentation
"
This generic function adds _data_ to _clipboard_, first coercing it to
_type_. The argument _type_ is an instance of 'type-union(<symbol>,
<type>)'. It returns t if _data_ was successfully added to the
clipboard.

Defined by: <<clipboard-protocol>>
"))
  (:function clear-clipboard (clipboard)
	     (:documentation
"
Clears the contents of _clipboard_, which represents the locked
clipboard.

Defined by: <<clipboard-protocol>>
"))
  (:function maybe-clear-clipboard (clipboard) (:documentation " Defined by: <<CLIPBOARD-PROTOCOL>> "))
  (:function clipboard-data-available? (type clipboard)
	     (:documentation
"
Returns t if and only if there is any data of type _type_ on the
clipboard. The argument _type_ is an instance of 'type-union(<symbol>
<type>)'.

Defined by: <<clipboard-protocol>>
"))
  (:function get-clipboard-data-as (type clipboard)
	     (:documentation
"
This generic function returns _data_ of _type_ from the clipboard. The
argument _type_ is an instance of 'type-union(<symbol>, <type>)'.

Defined by: <<clipboard-protocol>>
")))


(define-protocol <<display-protocol>> ()
  ;; Making displays
  (:function port-display-class (_port) (:documentation " Defined by: <<DISPLAY-PROTOCOL>> "))
  (:function display-matches-characteristics? (display &key &allow-other-keys) (:documentation " Defined by: <<DISPLAY-PROTOCOL>> "))
  (:function initialize-display (port display) (:documentation " Defined by: <<DISPLAY-PROTOCOL>> "))
  ;; Display characteristics
  (:function display-width (display &key units)
	     (:documentation
"
Returns the width of _display_, in device-independent units. If
_units_ is specified, then the value returned is converted into the
appropriate type of units.

Valid values for _units_ are :device, :mm, or :pixels with a default
of :device.

Defined by: <<display-protocol>>
"))
  (:function display-height (display &key units)
	     (:documentation
"
Returns the height of _display_, in device-independent units. If
_units_ is specified, then the value returned is converted into the
appropriate type of units.

Valid values for _units_ are :device, :mm, or :pixels with a default
of :device.

Defined by: <<display-protocol>>
"))
  (:function display-depth (display)
	     (:documentation
"
Returns the color depth of _display_. By default, the color depth of
any display is assumed to be 8.

Defined by: <<display-protocol>>
"))
  (:getter   display-units (display)
	     (:documentation
"
Returns the default units for _display_. These are the units in which
height and width measurements are made, both for the display, and for
any children of the display. Unless otherwise specified, the value
returned is :default, so as to maintain a device-independent
measurement as far as possible.

Valid returns values are :device, :mm, or :pixels.

Defined by: <<display-protocol>>
"))
  (:getter   display-orientation (display)
	     (:documentation
"
Returns the orientation of _display_. Unless specified otherwise, the
orientation of any display is :default.

TODO: does display-orientation actually affect anything?

Defined by: <<display-protocol>>
"))
  (:getter   display-mm-height (display)
	     (:documentation
"
Returns the height of _display_ in millimeters. This is equivalent to
calling 'display-height' with the _units_ argument set to :mm.

Defined by: <<display-protocol>>
"))
  (:setter   (setf display-mm-height) (height display) (:documentation " Defined by: <<DISPLAY-PROTOCOL>> "))
  (:getter   display-mm-width (display)
	     (:documentation
"
Returns the width of _display_ in millimeters. This is equivalent to
calling 'display-width' with the _units_ argument set to :mm.

Defined by: <<display-protocol>>
"))
  (:setter   (setf display-mm-width) (width display) (:documentation " Defined by: <<DISPLAY-PROTOCOL>> "))
  (:getter   display-pixel-width (display)
	     (:documentation
"
Returns the width of _display_ in pixels. This is equivalent to
calling 'display-width' with the _units_ argument set to :pixels.

Defined by: <<display-protocol>>
"))
  (:setter   (setf display-pixel-width) (width display) (:documentation " Defined by: <<DISPLAY-PROTOCOL>> "))
  (:getter   display-pixel-height (display)
	     (:documentation
"
Returns the height of _display_ in pixels. This is equivalent to
calling 'display-height' with the _units_ argument set to :pixels.

Defined by: <<display-protocol>>
"))
  (:setter   (setf display-pixel-height) (height display) (:documentation " Defined by: <<DISPLAY-PROTOCOL>> "))
  (:getter   display-pixels-per-point (display)
	     (:documentation
"
Returns the number of pixels per point for _display_.

Defined by: <<display-protocol>>
"))
  (:setter   (setf display-pixels-per-point) (pixels display) (:documentation " Defined by: <<DISPLAY-PROTOCOL>> "))
  ;; Attach top-level sheets to the display
  (:function sheet-attached? (sheet) (:documentation " Defined by: <<DISPLAY-PROTOCOL>> "))
  (:function attach-sheet (display sheet
				   &rest pane-options
				   &key frame-manager sheet-class
				   &allow-other-keys) (:documentation " Defined by: <<DISPLAY-PROTOCOL>> "))
  (:function note-sheet-attached (sheet) (:documentation " Defined by: <<DISPLAY-PROTOCOL>> "))
  (:function do-note-sheet-attached (sheet) (:documentation " Defined by: <<DISPLAY-PROTOCOL>> "))
  (:function detach-sheet (display sheet) (:documentation " Defined by: <<DISPLAY-PROTOCOL>> "))
  (:function note-sheet-detached (sheet) (:documentation " Defined by: <<DISPLAY-PROTOCOL>> "))
  (:function do-note-sheet-detached (sheet) (:documentation " Defined by: <<DISPLAY-PROTOCOL>> ")))


(define-protocol <<event-protocol>> ()
  ;; General events
  (:getter event-timestamp (event) (:documentation " Defined by: <<EVENT-PROTOCOL>> "))
  (:getter event-client (event) (:documentation " Defined by: <<EVENT-PROTOCOL>> "))
  (:getter event-modifier-state (event)
	   (:documentation
"
Returns an integer value that encodes the state of all the modifier
keys on the keyboard. This is a mask consisting of the 'logior' of
$shift-key, $control-key, $meta-key, $super-key, and $hyper-key.

TODO: what, no 'alt' key?

Defined by: <<event-protocol>>
"))
  ;; Pointer events and some gadget events, too
  (:getter event-button (event)
	   (:documentation
"
Returns an integer corresponding to the mouse button that was pressed
or released, which will be one of $left-button, $middle-button, or
$right-button.

Note: The function 'event-button' records the button state at the time
that the event occurred, and hence can be different from
'pointer-button-state'.

Defined by: <<event-protocol>>
"))
  (:getter event-x (event)
	   (:documentation
"
Returns the x position of the pointer at the time the event occurred,
in the coordinate system of the sheet that received the event.

Defined by: <<event-protocol>>
"))
  (:getter event-y (event)
	   (:documentation
"
Returns the y position of the pointer at the time the event occurred,
in the coordinate system of the sheet that received the event.

Defined by: <<event-protocol>>
"))
  ;; Pointer events
  (:getter event-pointer (event)
	   (:documentation
"
Returns the pointer object to which _event_ refers.

Defined by: <<event-protocol>>
"))
  (:getter boundary-event-kind (event)
	   (:documentation
"
Returns the kind of boundary event for _event_. These correspond to
the detail members for X11 enter and exit events (which
are :ancestor, :virtual, :inferior, :nonlinear, :nonlinear-virtual,
and nil).

Defined by: <<event-protocol>>
"))
  ;; Keyboard events and some gadget events, too
  (:getter event-character (event)
	   (:documentation
"
Returns the character associated with the keyboard event, if there is
any.

Defined by: <<event-protocol>>
"))
  (:getter event-key-name (event)
	   (:documentation
"
Returns the name of the key that was pressed or released in a keyboard
event. This will be a symbol whose value is specific to the current
port.

Defined by: <<event-protocol>>
"))
  ;; Window events
  (:getter event-region (event)
	   (:documentation
"
Returns the region of the sheet that is affected by _event_.

Defined by: <<event-protocol>>
")))


(define-protocol <<font-mapping-protocol>> (<<text-style-protocol>>)
  ;; Mapping text styles to fonts
  (:function text-style-mapping (port style &key character-set)
	     (:documentation
"
Returns the mapping for _text-style_ on port. Mapping text styles onto
fonts lets you control how different text styles are displayed on
different servers, depending on the connection. For instance, it is
possible to define how colored text is displayed on monochrome
displays, or how fonts specified by _text-style_ are mapped onto fonts
available on the display.

If _character-set_ is specified, then this character set is used
instead of the default. This is most useful for non-English displays.

Defined by: <<font-mapping-protocol>>
"))
  (:function do-text-style-mapping (port style character-set) (:documentation " Defined by: <<FONT-MAPPING-PROTOCOL>> "))
;;  (:function (setf text-style-mapping) (mapping port style &key character-set))
  (:function set-text-style-mapping (mapping port style &key character-set)
	     (:documentation
"
Sets the mapping for _text-style_ on _port_ to the specified
_font_. This function lets you have some control over the way in which
different text styles are displayed on different servers, depending on
the connection. Using this function, for instance, it is possible to
define how colored text is displayed on monochrome displays, or how
fonts specified by _text-style_ are mapped onto fonts available on the
display.

If _character-set_ is specified, then this character set is used
instead of the default. This is most useful for non-English displays.

Defined by: <<font-mapping-protocol>>
"))
  (:function text-style-mapping-exists? (port style &key character-set exact-size?)
	     (:documentation
"
Returns true if a mapping exists for _text-style_ on _port_. This
control function is useful if, for example, you are setting up text
style mappings for a range of text styles in one go, or for a range of
different ports. Using this function, you can test for the existence
of a previous mapping before creating a new one, thereby ensuring that
existing mappings are not overwritten.

Defined by: <<font-mapping-protocol>>
"))
  (:function standardize-text-style (port style &key character-set) (:documentation " Defined by: <<FONT-MAPPING-PROTOCOL>> "))
  (:function standardize-text-style-size (port style size-alist
					       &key character-set) (:documentation " Defined by: <<FONT-MAPPING-PROTOCOL>> ")))


(define-protocol <<font-protocol>> (<<text-style-protocol>>)
  ;; Font metrics, which get done in the back end
  (:function font-metrics (text-style port &key character-set)
	     (:documentation
"
Returns the metrics of the font in the _text-style_ on _port_ as
multiple values representing the _font_, _width_, _height_, _ascent_,
and _descent_.

Defined by: <<font-protocol>>
"))
  (:function font-width (text-style port &key character-set)
	     (:documentation
"
Returns the width of the font in the _text-style_ on _port_.

Defined by: <<font-protocol>>
"))
  (:function font-height (text-style port &key character-set)
	     (:documentation
"
Returns the height of the font in the _text-style_ on _port_.

Defined by: <<font-protocol>>
"))
  (:function font-ascent (text-style port &key character-set)
	     (:documentation
"
Returns the ascent of the font in the _text-style_ on _port_.

Defined by: <<font-protocol>>
"))
  (:function font-descent (text-style port &key character-set)
	     (:documentation
"
Returns the descent of the font in the _text-style_ on _port_.

Defined by: <<font-protocol>>
"))
  (:function fixed-width-font? (text-style port &key character-set)
	     (:documentation
"
Returns true if _text-style_ uses a fixed-width font.

Defined by: <<font-protocol>>
"))
  ;; Text measurement
  (:function text-size (drawable string-or-char
				 &key
				 text-style start end do-newlines? do-tabs?)
	     (:documentation
"
Returns information about the size of _text_ on _medium_.

If _text-style_ is specified, then the information that 'text-size'
returns is based on the text style it describes.

If _start_ and _end_ are specified, then these values represent a
portion of the string specified by _text_, and only the characters
they represent are examined by 'text-size'. Both _start_ and _end_
represent the index of each character is _text_, starting at 0. By
default, the whole of _text_ is examined.

The _do-newlines?_ and _do-tabs?_ arguments let you specify how
newline or tab characters in _text_ should be handled. If either of
these arguments is true, then any newline or tab characters in text
are examined, as appropriate. By default, newline characters are
ignored.

The method returns five values, representing the _largest-x_,
_total-height_, _last-x_, _last-y_ and _baseline_ of the measured
_text_.

Defined by: <<font-protocol>>
"))
  (:function glyph-for-character (port char text-style &key font) (:documentation " Defined by: <<FONT-PROTOCOL>> ")))


(define-protocol <<sheet-geometry-protocol>> (<<sheet-protocol>>)
  ;; Simple geometry changes
  (:function sheet-position (sheet)
	     (:documentation
"
Returns the position of _sheet_. The position is represented by the
coordinate (x, y), as measured relative to the parent of _sheet_, or
relative to the top left of the screen if _sheet_ has no parent.

Defined by: <<sheet-geometry-protocol>>
"))
  (:function set-sheet-position (sheet x y)
	     (:documentation
"
Sets the position of _sheet_ to (_x_, _y_), relative to the position
of its parent. The layout of _sheet_ is recalculated automatically.

Defined by: <<sheet-geometry-protocol>>
"))
  (:function sheet-size (sheet)
	     (:documentation
"
Returns the width and height of the specified sheet. Use
'set-sheet-size' to set or modify the size of a sheet.

Defined by: <<sheet-geometry-protocol>>
"))
  (:function set-sheet-size (sheet width height)
	     (:documentation
"
Sets the size of _sheet_. The layout of _sheet_ is recalculated
automatically.

Defined by: <<sheet-geometry-protocol>>
"))
  (:function sheet-edges (sheet)
	     (:documentation
"
Returns the edges of _sheet_ as the four values _left_, _top_,
_right_, and _bottom_. Each edge is specified relative to the
corresponding edge of the parent of _sheet_.

 Defined by: <<sheet-geometry-protocol>>
"))
  (:function set-sheet-edges (sheet left top right bottom)
	     (:documentation
"
Sets the edges of _sheet_ to _top_, _left_, _right_, and
_bottom_. Each edge is specified relative to the corresponding edge of
the parent _sheet_. The layout of _sheet_ is recalculated
automatically.

Defined by: <<sheet-geometry-protocol>>
"))
  ;; Geometry propagation
  (:function relayout-children (sheet) (:documentation " Defined by: <<SHEET-GEOMETRY-PROTOCOL>> "))
  (:function relayout-parent (sheet &key width height) (:documentation " Defined by: <<SHEET-GEOMETRY-PROTOCOL>> ")))


(define-protocol <<output-sheet-protocol>> (<<sheet-protocol>>)
  ;; Allocation, attachment, etc.
  (:function make-medium (port sheet) (:documentation " Defined by: <<OUTPUT-SHEET-PROTOCOL>> "))
  (:function allocate-medium (port sheet) (:documentation " Defined by: <<OUTPUT-SHEET-PROTOCOL>> "))
  (:function deallocate-medium (port medium) (:documentation " Defined by: <<OUTPUT-SHEET-PROTOCOL>> "))
  (:function attach-medium (sheet medium) (:documentation " Defined by: <<OUTPUT-SHEET-PROTOCOL>> "))
  (:function do-attach-medium (sheet medium) (:documentation " Defined by: <<OUTPUT-SHEET-PROTOCOL>> "))
  (:function detach-medium (sheet medium) (:documentation " Defined by: <<OUTPUT-SHEET-PROTOCOL>> "))
  (:function do-detach-medium (sheet medium) (:documentation " Defined by: <<OUTPUT-SHEET-PROTOCOL>> "))
  (:function destroy-medium (medium) (:documentation " Defined by: <<OUTPUT-SHEET-PROTOCOL>> "))
  ;; Drawing surfaces
  (:getter   sheet-medium (sheet)
	     (:documentation
"
Returns the medium associated with _sheet_.

Defined by: <<output-sheet-protocol>>
"))
  (:setter   (setf sheet-medium) (medium sheet) (:documentation " Defined by: <<OUTPUT-SHEET-PROTOCOL>> "))
  (:function do-with-sheet-medium (sheet continuation)
	     (:documentation
"
Runs a continuation function on a sheet.

Defined by: <<output-sheet-protocol>>
")))


(define-protocol <<medium-protocol>> ()
  ;; Accessors
  (:getter medium-foreground (medium)
	   (:documentation
"
Returns the foreground of _medium_.

Defined by: <<medium-protocol>>
"))
  (:setter (setf medium-foreground) (foreground medium)
	   (:documentation
"
Sets the foreground of _medium_.

Defined by: <<medium-protocol>>
"))
  (:getter medium-background (medium)
	   (:documentation
"
Returns the background or _medium_.

Defined by: <<medium-protocol>>
"))
  (:setter (setf medium-background) (background medium)
	   (:documentation
"
Sets the background for _medium_.

Defined by: <<medium-protocol>>
"))
  (:getter medium-text-style (medium)
	   (:documentation
"
Returns the text style for _medium_.

Defined by: <<medium-protocol>>
"))
  (:setter (setf medium-text-style) (text-style medium)
	   (:documentation
"
Sets the text style for _medium_.

Defined by: <<medium-protocol>>
"))
  (:getter medium-default-text-style (medium)
	   (:documentation
"
Returns the default text style for _medium_. This style is used for
any subsequent text that is written to _medium_.

Defined by: <<medium-protocol>>
"))
  (:setter (setf medium-default-text-style) (text-style medium)
	   (:documentation
"
Sets the default text style for _medium_. This style is used for any
subsequent text that is written to _medium_.

Defined by: <<medium-protocol>>
"))
  (:getter medium-merged-text-style (medium)
	   (:documentation
"
Returns the merged text style of _medium_.

Defined by: <<medium-protocol>>
"))
  (:getter medium-brush (medium)
	   (:documentation
"
Returns the brush for _medium_. This brush is used by all subsequent
painting operations on _medium_.

Defined by: <<medium-protocol>>
"))
  (:setter (setf medium-brush) (brush medium)
	   (:documentation
"
Sets the brush for _medium_. This brush is used by all subsequent
painting operations on _medium_.

Defined by: <<medium-protocol>>
"))
  (:getter medium-pen (medium)
	   (:documentation
"
Returns the pen for _medium_. This pen is used by all subsequent
drawing operations on _medium_.

Defined by: <<medium-protocol>>
"))
  (:setter (setf medium-pen) (pen medium)
	   (:documentation
"
Sets the pen for _medium_. This pen is used by all subsequent drawing
operations on _medium_.

Defined by: <<medium-protocol>>
"))
  (:getter medium-transform (medium)
	   (:documentation
"
Returns the transform for _medium_.

Defined by: <<medium-protocol>>
"))
  (:setter (setf medium-transform) (transform medium)
	   (:documentation
"
Sets the transform for _medium_.

Defined by: <<medium-protocol>>
"))
  (:getter medium-device-transform (medium) (:documentation " Defined by: <<MEDIUM-PROTOCOL>> "))
  (:getter medium-clipping-region (medium)
	   (:documentation
"
Returns the clipping region for _medium_.

Defined by: <<medium-protocol>>
"))
  (:setter (setf medium-clipping-region) (region medium)
	   (:documentation
"
Sets the clipping region for _medium_.

Defined by: <<medium-protocol>>
"))
  ;; Drawing surfaces
  (:getter medium-sheet (medium)
	   (:documentation
"
Returns the sheet for _medium_, if there is one.

Defined by: <<medium-protocol>>
"))
  (:setter (setf medium-sheet) (sheet medium) (:documentation " Defined by: <<MEDIUM-PROTOCOL>> "))
  (:getter medium-drawable (medium)
	   (:documentation
"
Returns the drawable for _medium_.

Defined by: <<medium-protocol>>
"))
  (:setter (setf medium-drawable) (drawable medium)
	   (:documentation
"
Sets the drawable for _medium_.

Defined by: <<medium-protocol>>
"))
  (:getter medium-pixmap (medium)
	   (:documentation
"
Returns the pixmap for _medium_. This pixmap is used by all subsequent
pixmap operations on _medium_.

Defined by: <<medium-protocol>>
"))
  (:setter (setf medium-pixmap) (pixmap medium)
	   (:documentation
"
Sets the pixmap for _medium_. This pixmap is used by all subsequent
pixmap operations on _medium_.

Defined by: <<medium-protocol>>
"))
  ;; Ring the bell...
  (:function beep (drawable)
	     (:documentation
"
Generate an annoying beep. BEEP!

Defined by: <<medium-protocol>>
"))
  ;; Drawing options
  (:function do-with-drawing-options (drawable function
				      &rest options
				      &key brush pen text-style clipping-region transform
				      &allow-other-keys)
	     (:documentation
"
Runs some code on a drawable in a given drawing context. This function
is called by the macro 'with-drawing-options', and you should define
new methods on it for new classes of drawable.

The _function_ passed to 'do-with-drawing-options' is the result of
encapsulating the body passed to 'with-drawing-options' as a
stand-alone method.

The values returned are the values that are returned from
'with-drawing-options'.

The various keywords specify a drawing context in which _function_ is
run.

Defined by: <<medium-protocol>>
"))
  (:function do-with-text-style (drawable function text-style)
	     (:documentation
"
Runs some code on a drawable in the context of a given text style.

Defined by: <<medium-protocol>>
"))
  (:function do-with-transform (drawable function transform)
	     (:documentation
"
Returns the result of running a function in a transform defined on a
specified medium. Methods on this function are called by
'with-transform' which in turn is used by the similar macros
'with-rotation', 'with-scaling', and 'with-translation'.

Defined by: <<medium-protocol>>
"))
  ;; Like 'force-output' and 'synchronize-output' from Streams...
  ;; These can be called on sheets, mediums, or ports
  (:function force-display (drawable)
	     (:documentation
"
Forces _drawable_ to be displayed.

Defined by: <<medium-protocol>>
"))
  (:function synchronize-display (drawable)
	     (:documentation
"
Synchronizes all displays on which the specified drawable is mapped.

Defined by: <<medium-protocol>>
")))


(define-protocol <<mirrored-sheet-protocol>> ()
  ;; Mirroring a sheet
  (:getter sheet-mirror (sheet) (:documentation " Defined by: <<MIRRORED-SHEET-PROTOCOL>> "))
  (:getter mirror-sheet (mirror) (:documentation " Defined by: <<MIRRORED-SHEET-PROTOCOL>> "))
  (:setter (setf mirror-sheet) (sheet mirror) (:documentation " Defined by: <<MIRRORED-SHEET-PROTOCOL>> "))
  ;; Sheet native and device coordinates
  ;; The "ground" coordinate space for a sheet, almost always
  ;; $identity-transform
  (:getter sheet-native-transform (sheet) (:documentation " Defined by: <<MIRRORED-SHEET-PROTOCOL>> "))
  (:setter (setf sheet-native-transform) (transform sheet) (:documentation " Defined by: <<MIRRORED-SHEET-PROTOCOL>> "))
  ;; The clipping region for a sheet in its "ground" coordinate space
  ;; usually a box of (0,0, width,height).  There's no setter for this
  ;; since that gets done by setting the mirror itself.
  (:getter sheet-native-region (sheet) (:documentation " Defined by: <<MIRRORED-SHEET-PROTOCOL>> "))
  ;; Returns the edges of the <bounding-box> for the sheet's native region
  ;; in its "ground" coordinate space
  (:getter sheet-native-edges (sheet) (:documentation " Defined by: <<MIRRORED-SHEET-PROTOCOL>> "))
  ;; Transform from sheet's coordinate space to the coordinate space of its
  ;; first mirrored ancestor.  For a directly mirrored sheet, this will
  ;; almost always be $identity-transform.
  (:getter sheet-device-transform (sheet) (:documentation " Defined by: <<MIRRORED-SHEET-PROTOCOL>> "))
  ;; The sheet's clipping region expressed in the coordinate space of its
  ;; first mirrored ancestor.
  (:getter sheet-device-region (sheet) (:documentation " Defined by: <<MIRRORED-SHEET-PROTOCOL>> "))
  (:getter sheet-device-edges (sheet) (:documentation " Defined by: <<MIRRORED-SHEET-PROTOCOL>> "))
  ;; Mirror-based repainting
  ;; In the X model, a repaint event comes in for every mirrored sheet.
  ;; In the Windows model, a repaint event comes in for every mirrored sheet.
  ;; In the Mac model, a repaint event comes in only for the top level sheet.
  ;; This function should return #t for any sheet that gets automagically
  ;; repainted by the native Window system, and should return #f for every
  ;; sheet for which 'repaint-sheet' should arrange to call 'handle-repaint'
  (:function port-handles-repaint? (port sheet) (:documentation " Defined by: <<MIRRORED-SHEET-PROTOCOL>> ")))


(define-protocol <<mirror-protocol>> ()
  ;; Creating and destroying mirrors
  (:function make-mirror (port sheet) (:documentation " Defined by: <<MIRROR-PROTOCOL>> "))
  (:function do-make-mirror (port sheet) (:documentation " Defined by: <<MIRROR-PROTOCOL>> "))
  (:function destroy-mirror (port sheet mirror) (:documentation " Defined by: <<MIRROR-PROTOCOL>> "))
  (:function map-mirror (port sheet mirror) (:documentation " Defined by: <<MIRROR-PROTOCOL>> "))
  (:function unmap-mirror (port sheet mirror) (:documentation " Defined by: <<MIRROR-PROTOCOL>> "))
  (:function raise-mirror (port sheet mirror &key activate?) (:documentation " Defined by: <<MIRROR-PROTOCOL>> "))
  (:function lower-mirror (port sheet mirror) (:documentation " Defined by: <<MIRROR-PROTOCOL>> "))
  (:function mirror-visible? (port sheet mirror) (:documentation " Defined by: <<MIRROR-PROTOCOL>> "))
  (:function mirror-origin (port sheet) (:documentation " Defined by: <<MIRROR-PROTOCOL>> "))
  ;; Mirror geometry
  ;; Returns the region's mirror in the mirror coordinate space
  (:function mirror-region (port sheet mirror) (:documentation " Defined by: <<MIRROR-PROTOCOL>> "))
  ;; Returns the coordinates of sheet's mirror's bounding box in the
  ;; coordinates of the parent of the mirror, that is (left,top, right,bottom)
  (:function mirror-edges (port sheet mirror) (:documentation " Defined by: <<MIRROR-PROTOCOL>> "))
  ;; Set the edges of the sheet mirror in the coordinate space of the
  ;; parent mirror.
  ;;--- Should we add 'set-mirror-region' for consistency (+ OpenDoc support)?
  (:function set-mirror-edges (port sheet mirror left top right bottom)
    (:documentation
"
 Set the edges of the sheet mirror in the coordinate space of the
 parent mirror. This is called to inform the mirror of the actual
 size it should appear on the display.

 Defined by: <<MIRROR-PROTOCOL>>
"))

  ;; Sheet <-> mirror synchronization
  (:function note-mirror-geometry-changed (port sheet region) (:documentation " Defined by: <<MIRROR-PROTOCOL>> "))
  (:function update-mirror-region (port sheet mirror) (:documentation " Defined by: <<MIRROR-PROTOCOL>> "))
  (:function update-mirror-transform (port sheet mirror) (:documentation " Defined by: <<MIRROR-PROTOCOL>> ")))


(define-protocol <<pointer-protocol>> ()
  (:getter pointer-sheet (pointer)
	   (:documentation
"
Returns the sheet under _pointer_, or nil if there is no sheet under
the pointer.

Defined by: <<pointer-protocol>>
"))
  (:getter pointer-button-state (pointer)
	   (:documentation
"
Returns the state of _pointer_.

Defined by: <<pointer-protocol>>
"))
  (:function pointer-position (pointer &key sheet)
	     (:documentation
"
Returns the current position of _pointer_ as two values, _x_ and
_y_. If _sheet_ is specified, then the pointer must be over it.

Defined by: <<pointer-protocol>>
"))
  (:function do-pointer-position (port pointer sheet) (:documentation " Defined by: <<POINTER-PROTOCOL>> "))
  (:function set-pointer-position (pointer x y &key sheet)
	     (:documentation
"
Sets the position of _pointer_ to (_x_, _y_), relative to the top left
corner of _sheet_, if specified. Units are measured in pixels.

Defined by: <<pointer-protocol>>
"))
  (:function do-set-pointer-position (port pointer sheet x y) (:documentation " Defined by: <<POINTER-PROTOCOL>> "))
  (:getter pointer-cursor (pointer)
	   (:documentation
"
Returns the cursor used for _pointer_.

Defined by: <<pointer-protocol>>
"))
  (:setter (setf pointer-cursor) (cursor pointer)
	   (:documentation
"
Sets the cursor used for _pointer_.

Defined by: <<pointer-protocol>>
"))
  (:function do-set-pointer-cursor (port pointer cursor) (:documentation " Defined by: <<POINTER-PROTOCOL>> "))
  (:function do-set-sheet-cursor (port sheet cursor) (:documentation " Defined by: <<POINTER-PROTOCOL>> "))
  (:getter pointer-grabbed? (pointer) (:documentation " Defined by: <<POINTER-PROTOCOL>> "))
  (:setter (setf pointer-grabbed?) (sheet pointer) (:documentation " Defined by: <<POINTER-PROTOCOL>> "))
  (:function grab-pointer (port pointer sheet) (:documentation " Defined by: <<POINTER-PROTOCOL>> "))
  (:function ungrab-pointer (port pointer) (:documentation " Defined by: <<POINTER-PROTOCOL>> "))
  (:function do-with-pointer-grabbed (port sheet continuation)
	     (:documentation
"
Runs the code specified in _continuation_, forwarding all pointer
events to _sheet_, even if the pointer leaves the sheet-region of
_sheet_. The argument _continuation_ is an instance of <function>.

This function is called by 'with-pointer-grabbed', and _continuation_
is actually the result of creating a stand-alone method from the body
of code passed to 'with-pointer-grabbed'.

Defined by: <<pointer-protocol>>
")))


(define-protocol <<port-protocol>> ()
  ;; Making and destroying ports
  (:function class-for-make-port (type-name &rest initargs
				  &key &allow-other-keys) (:documentation " Defined by: <<PORT-PROTOCOL>> "))
  (:function destroy-port (port)
	     (:documentation
"
Destroys _port_.

Defined by: <<port-protocol>>
"))
  (:function port-matches-server-path? (port server-path) (:documentation " Defined by: <<PORT-PROTOCOL>> "))
  (:function restart-port (port) (:documentation " Defined by: <<PORT-PROTOCOL>> "))
  ;; The contract of 'process-next-event' is to handle a single "raw"
  ;; event at the back-end.  Note that this will not necessarily result
  ;; in the distribution of a DUIM <event> object.
  (:function process-next-event (port &key timeout) (:documentation " Defined by: <<PORT-PROTOCOL>> "))
  ;; Accessors
  (:getter port-modifier-state (port)
	   (:documentation
"
Returns the modifier state of _port_.

Defined by: <<port-protocol>>
"))
  (:getter port-pointer (port)
	   (:documentation
"
Returns the pointer used on _port_.

Defined by: <<port-protocol>>
"))
  (:setter (setf port-pointer) (pointer port) (:documentation " Defined by: <<PORT-PROTOCOL>> "))
  (:getter port-input-focus (port) (:documentation " Defined by: <<PORT-PROTOCOL>> "))
  (:setter (setf port-input-focus) (focus port) (:documentation " Defined by: <<PORT-PROTOCOL>> "))
  (:getter port-focus-policy (port) (:documentation " Defined by: <<PORT-PROTOCOL>> "))
  (:function note-focus-in (port sheet) (:documentation " Defined by: <<PORT-PROTOCOL>> "))
  (:function note-focus-out (port sheet) (:documentation " Defined by: <<PORT-PROTOCOL>> "))
  (:getter port-frame-managers (port) (:documentation " Defined by: <<PORT-PROTOCOL>> "))
  (:getter port-default-frame-manager (port) (:documentation " Defined by: <<PORT-PROTOCOL>> "))
  (:setter (setf port-default-frame-manager) (framem port) (:documentation " Defined by: <<PORT-PROTOCOL>> "))
  (:getter port-default-palette (port) (:documentation " Defined by: <<PORT-PROTOCOL>> "))
  (:setter (setf port-default-palette) (palette port) (:documentation " Defined by: <<PORT-PROTOCOL>> "))
  (:getter port-name (port)
	   (:documentation
"
Returns the name of _port_.

Defined by: <<port-protocol>>
"))
  (:getter port-type (port)
	   (:documentation
"
Returns the type of _port_.

Defined by: <<port-protocol>>
"))
  (:getter port-server-path (port)
	   (:documentation
"
Returns the server path of _port_.

Defined by: <<port-protocol>>
"))
  (:getter port-properties (port) (:documentation " Defined by: <<PORT-PROTOCOL>> "))
  (:setter (setf port-properties) (properties port) (:documentation " Defined by: <<PORT-PROTOCOL>> "))
  (:getter port-displays (port) (:documentation " Defined by: <<PORT-PROTOCOL>> "))
  (:setter (setf port-displays) (displays port) (:documentation " Defined by: <<PORT-PROTOCOL>> "))
  ;; Text style mapping
  (:getter port-font-mapping-table (port) (:documentation " Defined by: <<PORT-PROTOCOL>> "))
  (:getter port-font-mapping-cache (port) (:documentation " Defined by: <<PORT-PROTOCOL>> "))
  (:getter port-undefined-text-style (port) (:documentation " Defined by: <<PORT-PROTOCOL>> "))
  (:setter (setf port-undefined-text-style) (text-style port) (:documentation " Defined by: <<PORT-PROTOCOL>> "))
  ;; Ports can specialize these to compute foreground and background
  ;; brush based on X resource files or whatever
  (:function get-default-foreground (port sheet &key foreground default)
	     (:documentation
"
Returns the default foreground for _sheet_ on _port_.

If _foreground_ is specified, then this is used instead of the
default.

Defined by: <<port-protocol>>
"))
  (:function port-default-foreground (port sheet) (:documentation " Defined by: <<PORT-PROTOCOL>> "))
  (:function get-default-background (port sheet &key background default)
	     (:documentation
"
Returns the default background for _sheet_ on _port_.

If _background_ is specified, then this is used instead of the
default.

Defined by: <<port-protocol>>
"))
  (:function port-default-background (port sheet) (:documentation " Defined by: <<PORT-PROTOCOL>> "))
  (:function get-default-text-style (port sheet &key text-style default)
	     (:documentation
"
Returns the default text style for _sheet_ on _port_.

If _text-style_ is specified, then this is used instead of the
default.

Defined by: <<port-protocol>>
"))
  (:function port-default-text-style (port sheet) (:documentation " Defined by: <<PORT-PROTOCOL>> ")))


(define-protocol <<sheet-protocol>> ()
  ;; Sheet region
  (:getter sheet-region (sheet)
	   (:documentation
"
Returns an instance of <region> that represents the set of points to
which _sheet_ refers. The region is expressed in the same coordinate
system as _sheet_.

Defined by: <<sheet-protocol>>
"))
  (:setter (setf sheet-region) (region sheet)
	   (:documentation
"
Creates or modifies an instance of <region> that represents the set of
points to which _sheet_ refers. The region is expressed in the same
coordinate system as _sheet_.

Defined by: <<sheet-protocol>>
"))
  (:function note-region-changed (sheet) (:documentation " Defined by: <<SHEET-PROTOCOL>> "))
  ;; Sheet transform
  (:getter sheet-transform (sheet)
	   (:documentation
"
Returns the transform associated with _sheet_.

Defined by: <<sheet-protocol>>
"))
  (:setter (setf sheet-transform) (transform sheet)
	   (:documentation
"
Sets or modifies the transform associated with _sheet_.

Defined by: <<sheet-protocol>>
"))
  (:function note-transform-changed (sheet) (:documentation " Defined by: <<SHEET-PROTOCOL>> "))
  (:function sheet-delta-transform (sheet ancestor) (:documentation " Defined by: <<SHEET-PROTOCOL>> "))
  ;; Sheet mapping
  (:getter sheet-mapped? (sheet)
	   (:documentation
"
Returns true if _sheet_ is mapped, that is, displayed on
screen (issues of occluding windows notwithstanding).

Defined by: <<sheet-protocol>>
"))
  (:setter (setf sheet-mapped?) (mapped? sheet &key do-repaint? clear?)
	   (:documentation
"
Specifies whether _sheet_ is mapped, that is, displayed on
screen (issues of occluding windows notwithstanding). If t, _sheet_ is
mapped, if nil, it is not.

Defined by: <<sheet-protocol>>
"))
  (:getter sheet-withdrawn? (sheet)
	   (:documentation
"
Returns true if _sheet_ has been withdrawn from the display, and is no
longer mapped.

Defined by: <<sheet-protocol>>
"))
  (:setter (setf sheet-withdrawn?) (withdrawn? sheet &key do-repaint? clear?) (:documentation " Defined by: <<SHEET-PROTOCOL>> "))
  (:function note-sheet-mapped (sheet) (:documentation " Defined by: <<SHEET-PROTOCOL>> "))
  (:function note-sheet-unmapped (sheet) (:documentation " Defined by: <<SHEET-PROTOCOL>> "))
  (:getter sheet-layed-out? (sheet) (:documentation " Defined by: <<SHEET-PROTOCOL>> "))
  (:setter (setf sheet-layed-out?) (layed-out? sheet) (:documentation " Defined by: <<SHEET-PROTOCOL>> "))
  ;; Cursors
  (:getter sheet-cursor (sheet) (:documentation " Defined by: <<SHEET-PROTOCOL>> "))
  (:setter (setf sheet-cursor) (cursor sheet) (:documentation " Defined by: <<SHEET-PROTOCOL>> "))
  ;; Carets
  (:getter sheet-caret (sheet) (:documentation " Defined by: <<SHEET-PROTOCOL>> "))
  ;; Focus
  (:getter sheet-input-focus (sheet) (:documentation " Defined by: <<SHEET-PROTOCOL>> "))
  (:getter sheet-accepts-focus? (sheet) (:documentation " Defined by: <<SHEET-PROTOCOL>> "))
  (:setter (setf sheet-accepts-focus?) (focus? sheet) (:documentation " Defined by: <<SHEET-PROTOCOL>> "))
  ;; Clearing
  (:function clear-box (drawable left top right bottom)
	     (:documentation
"
Clears a box-shaped area in the specified drawable, removing anything
that was drawn in that region.

The function 'clear-box*' is identical to 'clear-box' except that it
passes composite objects, rather than separate coordinates, in its
arguments. You should be aware that using this function may lead to a
loss of performance.

Defined by: <<sheet-protocol>>
"))
  (:function clear-box* (drawable region)
	     (:documentation
"
Clears a box-shaped area in the specified drawable, removing anything
that was drawn in that region.

The function 'clear-box*' is identical to 'clear-box' except that it
passes composite objects, rather than separate coordinates, in its
arguments. You should be aware that using this function may lead to a
loss of performance.

Defined by: <<sheet-protocol>>
"))
  ;; Destruction
  (:function destroy-sheet (sheet)
	     (:documentation
"
Destroys _sheet_.

Defined by: <<sheet-protocol>>
"))
  (:function do-destroy-sheet (sheet) (:documentation " Defined by: <<SHEET-PROTOCOL>> "))
  ;; Help
  (:getter sheet-help-context (sheet) (:documentation " Defined by: <<SHEET-PROTOCOL>> "))
  (:setter (setf sheet-help-context) (context sheet) (:documentation " Defined by: <<SHEET-PROTOCOL>> "))
  (:getter sheet-help-source (sheet) (:documentation " Defined by: <<SHEET-PROTOCOL>> "))
  (:setter (setf sheet-help-source) (locator sheet) (:documentation " Defined by: <<SHEET-PROTOCOL>> ")))


(define-protocol <<sheet-genealogy-protocol>> (<<sheet-protocol>>)
  (:getter sheet-parent (sheet)
	   (:documentation
"
Returns the parent of _sheet_.

Defined by: <<sheet-genealogy-protocol>>
"))
  (:setter (setf sheet-parent) (parent sheet)
	   (:documentation
"
Sets the parent of _sheet_.

Defined by: <<sheet-genealogy-protocol>>
"))
  (:function sheet-ancestor? (sheet putative-ancestor)
	     (:documentation
"
Returns true if _putative-ancestor_ is an ancestor of _sheet_.

Defined by: <<sheet-genealogy-protocol>>
"))
  (:getter sheet-children (sheet)
	   (:documentation
"
Returns a list of sheets that are the children of _sheet_. Some sheet
classes support only a single child; in this case, the return value of
'sheet-children' is a list of one element.

Defined by: <<sheet-genealogy-protocol>>
"))
  (:setter (setf sheet-children) (children sheet)
	   (:documentation
"
Sets the children of _sheet_. Some sheet classes support only a single
child; in this case, _children_ must be a list of one element.

Defined by: <<sheet-genealogy-protocol>>
"))
  (:getter sheet-child (sheet)
	   (:documentation
"
Returns the child of _sheet_.

Defined by: <<sheet-genealogy-protocol>>
"))
  (:setter (setf sheet-child) (child sheet)
	   (:documentation
"
Sets the child of _sheet_.

Defined by: <<sheet-genealogy-protocol>>
"))
  ;; Adding a child sheet
  ;; DR: ADD-CHILD on TABLE-PANE uses keys :ROW + :COLUMN too...
  (:function add-child (sheet child &rest keys &key index
			      &allow-other-keys)
	     (:documentation
"
Adds _child_ to _sheet_.

Defined by: <<sheet-genealogy-protocol>>
"))
  (:function do-add-child (sheet child &rest keys &key index
				 &allow-other-keys) (:documentation " Defined by: <<SHEET-GENEALOGY-PROTOCOL>> "))
  (:function note-child-added (sheet child) (:documentation " Defined by: <<SHEET-GENEALOGY-PROTOCOL>> "))
  ;; Removing a child sheet
  (:function remove-child (sheet child)
	     (:documentation
"
Removes _child_ from _sheet_. The remaining children in the sheet are
laid out again appropriately.

Defined by: <<sheet-genealogy-protocol>>
"))
  (:function do-remove-child (sheet child) (:documentation " Defined by: <<SHEET-GENEALOGY-PROTOCOL>> "))
  (:function note-child-removed (sheet child) (:documentation " Defined by: <<SHEET-GENEALOGY-PROTOCOL>> "))
  ;; Replacing a child
  (:function replace-child (sheet old-child new-child)
	     (:documentation
"
Replaces _old-child_ with _new-child_ in _sheet_. The children in the
sheet are laid out again appropriately.

Defined by: <<sheet-genealogy-protocol>>
"))
  (:function do-replace-child (sheet old-child new-child) (:documentation " Defined by: <<SHEET-GENEALOGY-PROTOCOL>> "))
  ;; Traversing children
  (:function child-containing-position (sheet x y)
	     (:documentation
"
Returns the topmost enabled direct child of _sheet_ whose region
contains the position (_x_, _y_). The position is expressed in the
coordinate system used by _sheet_.

Defined by: <<sheet-genealogy-protocol>>
"))
  (:function do-children-containing-position (function sheet x y)
	     (:documentation
"
Invokes _function_ on any children that occupy position (_x_, _y_) in
_sheet_. This is used by 'child-containing-position' to ascertain
which children occupy the position. The function
'child-containing-position' then decides which of the children
returned is the topmost direct enabled child.

Defined by: <<sheet-genealogy-protocol>>
"))
  (:function children-overlapping-region (sheet region)
	     (:documentation
"
Returns the list of enabled direct children of _sheet_ whose region
overlaps _region_.

Defined by: <<sheet-genealogy-protocol>>
"))
  (:function do-children-overlapping-region (function sheet region)
	     (:documentation
"
Invokes _function_ on any children of _sheet_ whose regions overlap
_region_. This is used by 'children-overlapping-region' to ascertain
which children overlap _region_.

Defined by: <<sheet-genealogy-protocol>>
"))
  ;; Raising and lowering
  (:function raise-sheet (sheet &key activate?)
	     (:documentation
"
Raises _sheet_ to the top of the current hierarchy of sheets.

Defined by: <<sheet-genealogy-protocol>>
"))
  ;; activate? should default to T
  (:function do-raise-sheet (parent sheet &key activate?) (:documentation " Defined by: <<SHEET-GENEALOGY-PROTOCOL>> "))
  (:function lower-sheet (sheet)
	     (:documentation
"
Lowers _sheet_ to the bottom of the current hierarchy of sheets.

Defined by: <<sheet-genealogy-protocol>>
"))
  (:function do-lower-sheet (parent sheet) (:documentation " Defined by: <<SHEET-GENEALOGY-PROTOCOL>> ")))

;;; TODO: what about the 'sheet-pointer-cursor' method which doesn't exist:
;;; see <<pointer-protocol>>?
#||
Returns the pointer cursor associated with _sheet_. This is the cursor
used to represent the mouse pointer whenever the mouse pointer is
inside the boundary of _sheet_.
||#
;;; TODO: what about the 'sheet-pointer-cursor-setter' method which doesn't exist:
;;; see <<pointer-protocol>>?
#||
Sets the pointer cursor associated with _sheet_. This is the cursor
used to represent the mouse pointer whenever the mouse pointer is
inside the boundary of _sheet_.
||#

;;; TODO: what about the 'sheet-text-cursor' method, which doesn't exist:
#||
Returns the text cursor associated with _sheet_. The text cursor
associated with a sheet is distinct from the pointer cursor associated
with the same sheet: the pointer cursor represents the current
position of the pointer associated with the attached pointer device,
while the text cursor represents the position in the sheet that any
text typed using the keyboard will be added. Only those sheets that
contain children that allow some form of text-based input have an
associated text cursor.
||#

;;; TODO: I think the above have been replaced by '(sheet-caret)' +
;;; '(sheet-pointer)'.
