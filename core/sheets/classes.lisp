;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-SHEETS-INTERNALS -*-
(in-package #:duim-sheets-internals)

#||
/// Sheet/medium/port protocol classes

define protocol-class event-handler (<object>) end;
||#

(define-protocol-class event-handler ())

#||
// "Primary" sheet classes should inherit from <sheet>
// "Mixin" sheet classes should inherit from <abstract-sheet>
define open abstract class <abstract-sheet> (<event-handler>) end;
define protocol-class sheet (<abstract-sheet>) end;
||#

(defclass <abstract-sheet> (<event-handler>)
  ()
  (:metaclass <abstract-metaclass>))


(define-protocol-class sheet (<abstract-sheet>) ()
  (:documentation
"
The :port init-keyword is true if the pane (and its mirror, if it has
one) has been mapped, #f otherwise. In this case, the term _mapped_
means visible on the display, ignoring issues of occlusion.

The :help-source and :help-context keywords let you specify pointers
to valid information available in any online help you supply with your
application. The :help-context keyword should specify a context-ID
present in the online help. This context-ID identifies the help topic
that is applicable to the current pane. The :help-source init-keyword
identifies the source file in which the help topic identified by
:help-context can be found. A list of context-IDs should be provided
by the author of the online help system.

The :parent, :child, and :children init-keywords let you specify a
lineage for the sheet if you wish, specifying the parent of the sheet
and as many children as you wish.

The :x and :y init-keywords specify the initial position of the sheet
relative to its parent. When :accepts-focus? is true, the sheet will
accept the pointer focus.

The init-keywords :cursor, :foreground, :background, and :text-style
can be used to specify the appearance of elements in the sheet.

The :caret init-keyword is used to specify the caret to be used within
the drawing pane, if one is to be used at all.

The :fixed-width? and :fixed-height? init-keywords are used to fix the
width or height of a sheet to the size defined by other appropriate
init-keywords. This is a useful way of ensuring that the default size
defined for a sheet is fixed in either direction. The init-keywords
force the space requirements for the sheet to make the minimum and
maximum sizes equal to the size defined at the time of creation. These
keywords are most useful when creating sheets of unknown size, when
you want to ensure that any child of that sheet is fixed at that size,
whatever it may be.

If :resizable? is #t then the sheet can be resized in either
direction. If :resizable? is #f then it cannot be resized in either
direction. If :resizable? is #t, but one of :fixed-width?
or :fixed-height? is #t, then the sheet can only be resized in one
direction as appropriate.

Example:

To make a text editor that is fixed at 10 lines high:

    (make-pane '<text-editor> :lines 10 :fixed-height? t)
"))

;;; TODO: also the 'sheet?' predicate
#||
Returns true if _object_ is a sheet.
||#


#||
define open abstract class <abstract-medium> (<object>) end;
define protocol-class medium (<abstract-medium>) end;
||#

(defclass <abstract-medium> ()
  ()
  (:metaclass <abstract-metaclass>))


(define-protocol-class medium (<abstract-medium>) ()
  (:documentation
"
The class of all mediums.

Mediums have the following elements associated with them:

    * A drawing plane, to which text and lines may be drawn
    * A foreground color, which describes the default color of
      anything drawn on the drawing plane
    * A background color, which describes the background color
      of the drawing plane
    * A transformation which describes the position of the drawing
      plane relative to the sheet which is its parent
    * A clipping region, on which any editing operations (such as
      cutting, copying, or pasting) will have effect.
    * A line style that describes the appearance of any lines
      drawn on the drawing plane
    * A text style that describes the appearance of any text
      written to the drawing plane 
"))

;;; TODO: also the 'medium?' predicate
#||
Returns true if _object_ is a medium.
||#


#||
define open abstract class <abstract-port> (<object>) end;
define protocol-class port (<abstract-port>) end;
||#

(defclass <abstract-port> ()
  ()
  (:metaclass <abstract-metaclass>))


(define-protocol-class port (<abstract-port>) ()
  (:documentation
"
The class of all ports. A display, and all the sheets attached to a
display, is associated with a port that is a connection to a display
server. The port manages:

    * A primary input device (usually a keyboard)
    * A pointing device, such as a mouse or trackball
    * An event processor that dispatched events to the appropriate
      sheet.
"))

;;; TODO: also 'port?'
#||
Returns true if _object_ is a port.
||#


#||
define open abstract class <abstract-display> (<abstract-sheet>) end;
define protocol-class display (<sheet>, <abstract-display>) end;
||#

(defclass <abstract-display> (<abstract-sheet>)
  ()
  (:metaclass <abstract-metaclass>))


(define-protocol-class display (<sheet> <abstract-display>) ()
  (:documentation
"
The class of displays. An instance of <display> is an object that
represents a single display (or screen) on some display server. Any
sheet can be attached to an instance of <display>, and a display, and
all the sheets attached to it, are associated with a <port> that is a
connection to a display server.

The :orientation init-keyword is used to specify the orientation of a
display and can be one of the values :vertical, :horizontal, or
:default. The default value is :default.

The :units init-keyword is used to specify the units in which height
and width measurements are made with respect to the display. The
default is whatever units are standard for the display device (usually
pixels). Valid values for :units are :device, :mm, :pixels. The
default value is :device.
"))

;;; TODO: also display?:
#||
Returns true if _object_ is a display.
||#


#||
define protocol-class mirror (<object>) end;
define protocol-class event (<object>) end;
define protocol-class pointer (<object>) end;
define protocol-class caret (<object>) end;
define protocol-class clipboard (<object>) end;
||#

(define-protocol-class mirror ())
(define-protocol-class event () ()
  (:documentation
"
The base class of all DUIM events.

The :timestamp initarg is used to give a unique identifier for the
event.
"))

;;; TODO: also 'event?'
#||
Returns true if _object_ is an instance of <event> or one of its
subclasses.
||#


(define-protocol-class pointer () ()
  (:documentation
"
The class of all pointers.
"))

;;; TODO: also of 'pointer?' predicate
#||
Returns true if _object_ is a pointer.
||#


(define-protocol-class caret () ()
  (:documentation
"
The class of carets, or text cursors. A cursor can actually be any
instance of <symbol> or any instance of <image>.

The :sheet init-keyword specifies the sheet that the caret is
positioned in.

The :x, :y, :width, and :height init-keywords define the position and
size of the caret, with respect to the sheet that contains it. The
position of the caret is measured from the top left of the sheet. All
units are measured in pixels.
"))


(define-protocol-class clipboard () ()
  (:documentation
"
The class of clipboard objects. An instance of this class is created
when a clipboard lock is created, and is used to hold the contents of
the Windows clipboard for the duration of the lock. You do not need to
worry about creating instances of <clipboard> yourself, since this is
handled automatically by the macro 'with-clipboard'.
"))

#||
define open abstract class <abstract-frame-manager> (<object>) end;
define protocol-class frame-manager (<abstract-frame-manager>) end;
||#


(defclass <abstract-frame-manager> ()
  ()
  (:metaclass <abstract-metaclass>))


(define-protocol-class frame-manager (<abstract-frame-manager>) ()
  (:documentation
"
The class of frame managers.

Frame managers control the realization of the look and feel of a
frame. The frame manager interprets the specification of the
application frame in the context of the available window system
facilities, taking into account preferences expressed by the user.

In addition, the frame manager takes care of attaching the pane
hierarchy of an application frame to an appropriate place in a window
hierarchy.

Thus, the frame manager decides the following:

    * What concrete gadget to create for an abstract gadget.
    * How to layout the various parts of a frame, such as its menu,
      tool, and status bars.
    * How to lay out dialogs and their exit buttons.
    * How much spacing to use in various conventional layouts. 

In addition, a frame manager maps dialog functions such as
'choose-file' to their appropriate native dialogs.
"))

;;; TODO: also 'frame-manager?'
#||
Returns true if _object_ is a frame manager.
||#


#||
define open abstract class <abstract-frame> (<event-handler>) end;
define protocol-class frame (<abstract-frame>) end;
||#

(defclass <abstract-frame> (<event-handler>)
  ()
  (:metaclass <abstract-metaclass>))


(define-protocol-class frame (<abstract-frame>) ()
  (:documentation
"
The class of all frames.

The :owner initarg is the parent of the frame.

The :mode initarg lets you specify the mode for the frame. By default,
frames are modeless, that is, they do not take over control of the
whole application when they are mapped, and the user can interact with
other frames in the application normally. Modal frames, on the other
hand, behave like a <dialog-frame>, restricting the user's interaction
with other frames in the application until the modal frame has been
dismissed.

The :default-button initarg is used to specify which button is the
default action for the frame. The default button is usually the one
whose callback is invoked by pressing the RETURN key.

The :x, :y, :width and :height initargs let you specify the initial
size and position of the frame. The position is specified using :x
and :y, which represent the number of pixels from the top left corner
of the screen, and the :width and :height initargs specify the initial
size of the frame.

The :title initarg is used to specify a title for the frame.

The :state initarg is used to specify the initial state of the
frame. This describes whether the frame is mapped, whether it is
iconified, and so on. By default, new frames are detached.

By default, new frames run in their own thread. If desired, a frame
can be run in an existing thread by setting the :thread initarg to the
thread object concerned. For more information about threads, see the
manual Library Reference: Core Features.

As with threads, new frame run in their own event-queue by default. To
run the frame in an existing event-queue, use the :event-queue
initarg.

You can specify which sheet in the frame initially has the input-focus
using the :input-focus initarg. The input-focus dictates where
information can be typed by default.

The :foreground, :background, and :text-style initargs describe the
colors and fonts used in the frame.

Specify a palette for the frame using the :palette init-keyword.

Specify a resource-id for the frame using the :resource-id
init-keyword. This is a platform-specific ID or determining which
resource to use to fill in a frame.

The :resizable?, :fixed-width?, and :fixed-height? init-keywords let
you specify whether or not the user can resize the
frame. If :resizable? is t, then the frame can be resized in either
direction; if it is nil, then it cannot be resized at all. In
addition, if :resizable? is t, and one of :fixed-width?
or :fixed-height? is also t, then the frame is resizable, but is fixed
in the appropriate direction. For example, if :resizable? is t
and :fixed-height? also t, then only the width of the frame can be
resized.
"))

;;; TODO: Also for the 'frame?' predicate
#||
Returns true if _object_ is a frame. Use this generic function to test
that an object is a frame before carrying out frame-related operations
on it.
||#


#||
// This isn't an abstract superclass of <sheet> and <medium> because
// they don't otherwise have much to do with each other.  <drawable> is
// really just a convenience for the functions (such as the drawing
// functions) that trampoline from sheets to mediums.
define constant <drawable> = type-union(<abstract-sheet>, <abstract-medium>);
||#

;;; FIXME - what to do here?

;;;(deftype <drawable> () '(or <abstract-sheet> <abstract-medium>))


#||
// A cursor is a thing that tells what the pointer's cursor should look like.
// Don't confuse this with a "caret", which is a little indicator in a text
// pane that shows you where the next character will be inserted.
define constant <cursor> = type-union(<symbol>, <image>);
||#

(deftype <cursor> ()
"
The class of cursor objects. The cursor is the small image that is
used to display the location of the mouse pointer at any time. A
cursor can actually be any instance of <symbol> or any instance of
<image>.
"
 '(or symbol <image>))


(defgeneric cursor? (object)
  (:documentation
"
Returns true if _object_ is a cursor. In practice, you can create a
cursor from any instance of <symbol> or <image>.
"))

(defmethod cursor? ((object t))
  (typep object '<cursor>))
