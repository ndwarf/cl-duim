;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-SHEETS-INTERNALS -*-
(in-package #:duim-sheets-internals)

#||
/// Basic gesture support

define constant %button_base    :: <integer> = 8;
define constant $left-button    :: <integer> = ash(1, %button_base + 0);
define constant $middle-button  :: <integer> = ash(1, %button_base + 1);
define constant $right-button   :: <integer> = ash(1, %button_base + 2);
||#

(defconstant %button_base    8)
(defconstant +left-button+   (ash 1 (+ %button_base 0))
"
A constant that represents the left button on the attached pointing
device.
")

(defconstant +middle-button+ (ash 1 (+ %button_base 1))
"
A constant that represents the middle button on the attached pointing
device.
")

(defconstant +right-button+  (ash 1 (+ %button_base 2))
"
A constant that represents the right button on the attached pointing
device.
")


#||
// The order of this must match the values above
define constant $pointer-buttons :: <simple-object-vector>
    = #[#"left", #"middle", #"right"];
||#

(defvar *pointer-buttons* '(:left :middle :right)
"
The constant representing the possible buttons on the pointing device
attached to the computer, typically a mouse. Up to three buttons are
provided for.

The order of the elements in this sequence must match the order of the
values of $left-button, $middle-button and $right-button.
")


#||
// Button indices are 0, 1, or 2
define inline function button-index
    (name) => (index :: false-or(<integer>))
  position($pointer-buttons, name)
end function button-index;
||#

(defun button-index (name)
"

Returns the index for _button_, a button on the pointer device
connected to the computer (typically a mouse). The _index_ returned is
either 0, 1, or 2, for the left, middle, or right buttons,
respectively.

_button_ must be one of :left, :middle, or :right.
"
  (position name *pointer-buttons*))


#||
define inline function button-index-name
    (index :: <integer>) => (name)
  $pointer-buttons[index]
end function button-index-name;
||#

(defun button-index-name (index)
"
Returns the button on the pointer device connected to the
computer (typically a mouse) represented by _index_. The _index_ is
either 0, 1, or 2, these values corresponding to the left, middle, or
right buttons, respectively.

_button_ will be one of :left, :middle, or :right.
"
  (nth *pointer-buttons* index))


#||
define constant %modifier_base :: <integer> = 0;
define constant $shift-key     :: <integer> = ash(1, %modifier_base + 0);
define constant $control-key   :: <integer> = ash(1, %modifier_base + 1);
define constant $meta-key      :: <integer> = ash(1, %modifier_base + 2);
define constant $super-key     :: <integer> = ash(1, %modifier_base + 3);
define constant $hyper-key     :: <integer> = ash(1, %modifier_base + 4);
||#

(defconstant %modifier_base  0)
(defconstant +shift-key+     (ash 1 (+ %modifier_base 0))
"
A constant that represents the SHIFT key on the keyboard.
")

(defconstant +control-key+   (ash 1 (+ %modifier_base 1))
"
A constant that represents the CONTROL key on the keyboard.
")

(defconstant +meta-key+      (ash 1 (+ %modifier_base 2))
"
A constant that represents the META key on the keyboard, if it
exists. To deal with the case where there is no META key, the value of
the constant '$alt-key' is bound to this constant.
")

(defconstant +super-key+     (ash 1 (+ %modifier_base 3))
"
A constant that represents the SUPER key on the keyboard, if it
exists. To deal with the case where there is no SUPER key, the value
of the constant $option-key is bound to this constant.
")

(defconstant +hyper-key+     (ash 1 (+ %modifier_base 4))
"
A constant that represents the HYPER key on the keyboard.
")


#||
define constant $bucky-keys :: <integer>
    = logior($control-key, $meta-key, $super-key, $hyper-key);
||#

(defconstant +bucky-keys+ (logior +control-key+ +meta-key+ +super-key+ +hyper-key+))


#||
define constant $alt-key    :: <integer> = $meta-key;
define constant $option-key :: <integer> = $super-key;
||#

;; What if the META key *is* present on the keyboard?
(defconstant +alt-key+ +meta-key+
"
A constant that represents the ALT key on the keyboard. This is set to
the same value as the META key, to deal with the case where the META
key is not present on the keyboard.
")

;; What if the OPTION key *is* present on the keyboard?
(defconstant +option-key+ +super-key+
"
A constant that represents the OPTION key on the keyboard. This is set
to the same value as the SUPER key, to deal with the case where the
OPTION key is not present on the keyboard.
")


#||
// One more magic key for Windows, which shifts to an alternate set
// of characters on the keyboard
define constant $altgr-key    :: <integer> = ash(1, %modifier_base + 5);
define constant $capslock-key :: <integer> = ash(1, %modifier_base + 6);
||#

(defconstant +altgr-key+    (ash 1 (+ %modifier_base 5)))
(defconstant +capslock-key+ (ash 1 (+ %modifier_base 6)))


#||
// The order of this must match the values above
// #"alt" and #"option" are handled as special cases below
define constant $modifier-keys :: <simple-object-vector>
    = #[#"shift", #"control", #"meta", #"super", #"hyper"];
||#

(defvar *modifier-keys* #(:shift :control :meta :super :hyper)
"
The default list of keys on the keyboard that are used as modifiers
for keyboard accelerators and mnemonics.

This variable has the value:

    #(:shift :control :meta :super :hyper)
")


#||
// Modifier key indices are 0, 1, 2, 3, or 4
define inline function modifier-key-index
    (name) => (index :: false-or(<integer>))
  position($modifier-keys, name)
  | (name == #"alt"     & position($modifier-keys, #"meta"))
  | (name == #"option"  & position($modifier-keys, #"super"))
end function modifier-key-index;
||#

(defun modifier-key-index (name)
"
Returns the index number of the specified modifier key. The _key-name_
specified may be any of the elements of '$modifier-keys'.

The returned index value is either 0, 1, 2, 3, or 4.
"
  (or (position name *modifier-keys*)
      (and (eql name :alt) (position :meta *modifier-keys*))
      (and (eql name :option) (position :super *modifier-keys*))))


#||
define inline function modifier-key-index-name
    (index :: <integer>) => (name)
  $modifier-keys[index]
end function modifier-key-index-name;
||#

(defun modifier-key-index-name (index)
"
Returns the key name of the specified modifier key index. The _index_
specified is either 0, 1, 2, 3, or 4.

The _key-name_ returned may be any of the elements of
'*modifier-keys*'.
"
  (aref *modifier-keys* index))


#||
// Modifier states can be compared with =
define function make-modifier-state
    (#rest modifiers) => (state :: <integer>)
  dynamic-extent(modifiers);
  let state = 0;
  for (name in modifiers)
    if (member?(name, #[1, 2, 4, 8, 16]))
      state := logior(state, name)
    else
      let bit = modifier-key-index(name);
      if (bit)
	state := logior(state, ash(1, %modifier_base + bit))
      else
	error("%= is not a valid modifier key name", name)
      end
    end
  end;
  state
end function make-modifier-state;
||#

(defun make-modifier-state (&rest modifiers)
"
Returns a modifier state for _modifiers_.
"
  (declare (dynamic-extent modifiers))
  (let ((state 0))
    (loop for name in modifiers
	  do (if (find name #(1 2 4 8 16))
		 (setf state (logior state name))
	       (let ((bit (modifier-key-index name)))
		 (if bit
		     (setf state (logior state (ash 1 (+ %modifier_base bit))))
		     (error "~a is not a valid modifier key name" name)))))
    state))



#||
/// Higher level gesture support

// The idea here is to provide a "gesture" abstraction that can be matched
// against lower level events.  For example the gesture $left-button + $shift-key
// will match a pointer button event on the left-hand button while the shift
// key is held down.
define protocol-class gesture (<object>) end;
||#

(define-protocol-class gesture () ()
  (:documentation
"
The base class of all gestures.

The idea here is to provide a 'gesture' abstraction that can be matched
against lower level events. For example the gesture +left-button+ & +shift-key+
will match a pointer button event on the left-hand button while the shift
key is held down.
"))


#||
/// Keyboard gestures

define sealed class <keyboard-gesture> (<gesture>)
  sealed constant slot gesture-keysym :: <symbol>,
    required-init-keyword: keysym:;
  sealed constant slot gesture-modifier-state :: <integer>,
    required-init-keyword: modifier-state:;
end class <keyboard-gesture>;
||#

(defgeneric gesture-keysym (keyboard-gesture)
  (:documentation
"
Returns the keysym associated with _keyboard-gesture_.
"))

(defgeneric gesture-modifier-state (gesture)
  (:documentation
"
Returns the modifier-state associated with _gesture_.
"))

(defclass <keyboard-gesture> (<gesture>)
  ((gesture-keysym :type symbol :initarg :keysym :initform (required-slot ":keysym" "<keyboard-gesture>")
		   :reader gesture-keysym)
   (gesture-modifier-state :type integer :initarg :modifier-state :initform (required-slot ":modifier-state" "<keyboard-gesture>")
			   :reader gesture-modifier-state))
  (:documentation
"
The base class of all keyboard gestures.

The :keysym initarg represents the keysym for the gestures, and
the :modifier-state initarg represents the modifer state.
"))


#||
define sealed domain make (singleton(<keyboard-gesture>));
define sealed domain initialize (<keyboard-gesture>);

// Mapping between standard characters and keysyms
define constant $standard-char-keysyms :: <simple-object-vector>
    = make(<vector>, size: 128);

begin
  for (ch :: <integer> from 33 below 127)
    // Each string must be unique, because 'as(<symbol>, ...)'
    // doesn't copy the string as it creates the symbol
    let string = make(<byte-string>, size: 1, fill: as(<character>, ch));
    $standard-char-keysyms[ch]  := as(<symbol>, string)
  end;
  $standard-char-keysyms[as(<integer>, ' ')]   := #"space";
  $standard-char-keysyms[as(<integer>, '\t')]  := #"tab";
  $standard-char-keysyms[as(<integer>, '\b')]  := #"backspace";
  $standard-char-keysyms[as(<integer>, '\n')]  := #"newline";
  $standard-char-keysyms[as(<integer>, '\r')]  := #"return";
  $standard-char-keysyms[as(<integer>, '\f')]  := #"form";
  $standard-char-keysyms[as(<integer>, '\e')]  := #"escape";
  $standard-char-keysyms[as(<integer>, '\0')]  := #"null";
  $standard-char-keysyms[127] := #"delete"	// formerly #"rubout"
end;

define inline function standard-char->keysym
    (char :: <character>) => (keysym :: false-or(<symbol>))
  let code = as(<integer>, char);
  0 <= code & code <= 127 & $standard-char-keysyms[code]
end function standard-char->keysym;
||#

;; In the CL version here, we just fall back on 'char-name'
(defun standard-char->keysym (char)
  (intern (or (char-name char)
	      (make-string 1 :initial-element char))
	  (find-package :keyword)))


#||
define method gesture-character
    (gesture :: <keyboard-gesture>) => (char :: false-or(<character>))
  let code = position($standard-char-keysyms, gesture-keysym(gesture));
  when (code)
    let char = as(<character>, code);
    if (alpha-char?(char))
      if (zero?(logand(gesture-modifier-state(gesture), $shift-key)))
	as-lowercase(char)
      else
	as-uppercase(char)
      end
    else
      char
    end
  end
end method gesture-character;
||#

(defmethod gesture-character ((gesture <keyboard-gesture>))
  (let* ((char (name-char (symbol-name (gesture-keysym gesture)))))
    (if (alpha-char-p char)
	(if (zerop (logand (gesture-modifier-state gesture) +shift-key+))
	    (char-downcase char)
	    (char-upcase char))
	char)))


#||
/// Useful methods on characters

define method gesture-character
    (char :: <character>) => (char :: false-or(<character>))
  char
end method gesture-character;
||#

(defmethod gesture-character ((char character))
  char)


#||
define method gesture-keysym
    (char :: <character>) => (keysym :: false-or(<symbol>))
  standard-char->keysym(char)
end method gesture-keysym;
||#

(defmethod gesture-keysym ((char character))
  (standard-char->keysym char))


#||
define method gesture-modifier-state
    (char :: <character>) => (modifier-state :: <integer>)
  if (upper-case?(char)) $shift-key else 0 end
end method gesture-modifier-state;
||#

(defmethod gesture-modifier-state ((char character))
  (if (upper-case-p char) +shift-key+ 0))


#||
/// Pointer gestures

define sealed class <pointer-gesture> (<gesture>)
  sealed constant slot gesture-button :: <integer>,
    required-init-keyword: button:;
  sealed constant slot gesture-modifier-state :: <integer>,
    required-init-keyword: modifier-state:;
end class <pointer-gesture>;
||#

(defgeneric gesture-button (pointer-gesture)
  (:documentation
"
Returns the button associated with _pointer-gesture_.
"))

(defclass <pointer-gesture> (<gesture>)
  ((gesture-button :type integer :initarg :button :initform (required-slot ":button" "<pointer-gesture>")
		   :reader gesture-button)
   (gesture-modifier-state :type integer :initarg :modifier-state :initform (required-slot ":modifier-state" "<pointer-gesture>")
			   :reader gesture-modifier-state))
  (:documentation
"
The class of all gestures that occur on pointers.

The :button initarg specifies the button on the attached pointer
device on which the gesture has occurred, and the :modifier-state
initarg specifies the modifier-state of the gesture.
"))


#||
define sealed domain make (singleton(<pointer-gesture>));
define sealed domain initialize (<pointer-gesture>);

/// Constructors

define variable *gesture-table* :: <simple-object-vector>
    = make(<simple-vector>, size: 32);
||#

;;; Constructors

;;; Array of hashtables.
(defparameter *gesture-table* (make-array 32 :initial-element nil))


#||
// This interns gestures so that we can use == dispatch on them...
define sealed method make
    (class == <gesture>, #key keysym, button, modifiers, modifier-state = 0)
 => (gesture :: <gesture>)
  case
    keysym =>
      make(<keyboard-gesture>,
	   keysym: keysym,
	   modifiers: modifiers, modifier-state: modifier-state);
    button =>
      make(<pointer-gesture>,
	   button: button,
	   modifiers: modifiers, modifier-state: modifier-state);
    otherwise =>
      error("You must supply one of 'keysym:' or 'button:'");
  end
end method make;
||#

;; This interns gestures so we can use 'eql' dispatch on them...
(defun make-gesture (&key keysym button modifiers (modifier-state 0))
  (cond (keysym
	 (make-keyboard-gesture :keysym keysym
				:modifiers modifiers
				:modifier-state modifier-state))
	(button
	 (make-pointer-gesture :button button
			       :modifiers modifiers
			       :modifier-state modifier-state))
	(t
	 (error "You must supply one of ':keysym' or ':button'"))))


#||
define sealed method make
    (class == <keyboard-gesture>, #key keysym, modifiers, modifier-state = 0)
 => (gesture :: <keyboard-gesture>)
  let modifier-state
    = if (modifiers) apply(make-modifier-state, modifiers)
      else modifier-state end;
  when (instance?(keysym, <character>))
    assert(standard-char?(keysym),
	   "The character %s is not a standard character", keysym);
    when (upper-case?(keysym))
      modifier-state := logior(modifier-state, $shift-key)
    end;
    keysym := standard-char->keysym(keysym)
  end;
  let bucket :: <object-table>
    = begin
        let bucket = *gesture-table*[modifier-state];
        unless (bucket)
	  bucket := make(<table>);
	  *gesture-table*[modifier-state] := bucket
	end;
	bucket
      end;
  gethash(bucket, keysym)
  | begin
      let gesture = next-method(class,
				keysym: keysym, modifier-state: modifier-state);
      gethash(bucket, keysym) := gesture;
      gesture
    end
end method make;
||#

;; Protect people who try to use 'make-instance' on keyboard or
;; pointer gestures
(define-thread-variable *executing-gesture-builder* nil)

;; "MODIFIERS" should be a list of modifier keys
(defun make-keyboard-gesture (&key keysym modifiers (modifier-state 0))
  (let ((modifier-state (if modifiers
			    (apply #'make-modifier-state (map 'list #'identity modifiers)) ;; convert modifiers to a list...
			    modifier-state)))
    (when (typep keysym 'character)
;;      (unless (standard-char? keysym)
;;	(error "The character ~s is not a standard character" keysym))
      (when (upper-case-p keysym)
	(setf modifier-state (logior modifier-state +shift-key+)))
      (setf keysym (standard-char->keysym keysym)))
    (let ((bucket
	   (let ((bucket (aref *gesture-table* modifier-state)))
	     (unless bucket
	       (setf bucket (make-hash-table))
	       (setf (aref *gesture-table* modifier-state) bucket))
	     bucket)))
      (or (gethash keysym bucket)
	  (dynamic-let ((*executing-gesture-builder* t))
	    (let ((gesture (make-instance '<keyboard-gesture> :keysym keysym :modifier-state modifier-state)))
	      (setf (gethash keysym bucket) gesture)
	      gesture))))))


(defmethod initialize-instance :before ((gesture <keyboard-gesture>) &key)
  (unless *executing-gesture-builder*
    (error "Direct make-instance invocation on <keyboard-gesture>; use MAKE-KEYBOARD-GESTURE")))
	   

#||
define sealed method make
    (class == <pointer-gesture>, #key button, modifiers, modifier-state = 0)
 => (gesture :: <pointer-gesture>)
  let modifier-state
    = if (modifiers) apply(make-modifier-state, modifiers)
      else modifier-state end;
  when (member?(button, $pointer-buttons))
    button := ash(1, %button_base + button-index(button))
  end;
  let bucket :: <object-table>
    = begin
        let bucket = *gesture-table*[modifier-state];
        unless (bucket)
	  bucket := make(<table>);
	  *gesture-table*[modifier-state] := bucket
	end;
	bucket
      end;
  gethash(bucket, button)
  | begin
      let gesture = next-method(class,
				button: button, modifier-state: modifier-state);
      gethash(bucket, button) := gesture;
      gesture
    end
end method make;
||#

;; "MODIFIERS" should be a list of modifier keys
(defun make-pointer-gesture (&key button modifiers (modifier-state 0))
  (let ((modifier-state (if modifiers
			    (apply #'make-modifier-state (map 'list #'identity modifiers))
			    modifier-state)))
    (when (member button *pointer-buttons*)
      (setf button (ash 1 (+ %button_base (button-index button)))))
    (let ((bucket
	   (let ((bucket (aref *gesture-table* modifier-state)))
	     (unless bucket
	       (setf bucket (make-hash-table))
	       (setf (aref *gesture-table* modifier-state) bucket))
	     bucket)))
      (or (gethash button bucket)
	  (dynamic-let ((*executing-gesture-builder* t))
	    (let ((gesture (make-instance '<pointer-gesture> :button button :modifier-state modifier-state)))
	      (setf (gethash button bucket) gesture)
	      gesture))))))


(defmethod initialize-instance :before ((gesture <pointer-gesture>) &key)
  (unless *executing-gesture-builder*
    (error "Direct make-instance invocation on <pointer-gesture>; use MAKE-POINTER-GESTURE")))


#||
// Gesture comparisons
define generic gesture-equal
    (gesture1, gesture2) => (true? :: <boolean>);
||#

(defgeneric gesture-equal (gesture1 gesture2)
  (:documentation
"
Returns true if _gesture1_ and _gesture2_ are equivalent.

TODO: this seems to be the missing 'gesture-spec-equal' that is
documented in the fundev reference.
"))


#||
define method \=
    (g1 :: <gesture>, g2 :: <gesture>) => (true? :: <boolean>)
  g1 == g2
  | gesture-equal(g1, g2)
end method \=;
||#

(defmethod equal? ((g1 <gesture>) (g2 <gesture>))
  (or (eql g1 g2)
      (gesture-equal g1 g2)))


#||
define method gesture-equal
    (gesture1 :: <gesture>, gesture2 :: <gesture>) => (true? :: <boolean>)
  #f
end method gesture-equal;
||#

(defmethod gesture-equal ((gesture1 <gesture>)
			  (gesture2 <gesture>))
  nil)


#||
define sealed method gesture-equal
    (gesture1 :: <pointer-gesture>, gesture2 :: <pointer-gesture>)
 => (true? :: <boolean>)
  gesture-button(gesture1) == gesture-button(gesture2)
  & gesture-modifier-state(gesture1) == gesture-modifier-state(gesture2)
end method gesture-equal;
||#

(defmethod gesture-equal ((gesture1 <pointer-gesture>) (gesture2 <pointer-gesture>))
  (and (= (gesture-button gesture1) (gesture-button gesture2))
       (= (gesture-modifier-state gesture1) (gesture-modifier-state gesture2))))


#||
define sealed method gesture-equal
    (gesture1 :: <keyboard-gesture>, gesture2 :: <keyboard-gesture>)
 => (true? :: <boolean>)
  gesture-keysym(gesture1) == gesture-keysym(gesture2)
  & gesture-modifier-state(gesture1) == gesture-modifier-state(gesture2)
end method gesture-equal;
||#

(defmethod gesture-equal ((gesture1 <keyboard-gesture>) (gesture2 <keyboard-gesture>))
  (and (eql (gesture-keysym gesture1) (gesture-keysym gesture2))
       (= (gesture-modifier-state gesture1) (gesture-modifier-state gesture2))))


#||
// Gesture matching
define generic event-matches-gesture?
    (event :: <event>, gesture :: type-union(<gesture>, <character>))
 => (true? :: <boolean>);
||#

(defgeneric event-matches-gesture? (event gesture)
  (:documentation
"
Returns true if an event matches a defined gesture.
"))


#||
define method event-matches-gesture?
    (event :: <event>, gesture :: <gesture>) => (true? :: <boolean>)
  #f
end method event-matches-gesture?;
||#

(defmethod event-matches-gesture? ((event <event>) (gesture <gesture>))
  nil)


#||
define sealed method event-matches-gesture?
    (event :: <pointer-button-event>, gesture :: <pointer-gesture>)
 => (true? :: <boolean>)
  event-button(event) == gesture-button(gesture)
  & event-modifier-state(event) == gesture-modifier-state(gesture)
end method event-matches-gesture?;
||#

(defmethod event-matches-gesture? ((event <pointer-button-event>) (gesture <pointer-gesture>))
  (and (= (event-button event) (gesture-button gesture))
       (= (event-modifier-state event) (gesture-modifier-state gesture))))


#||
define sealed method event-matches-gesture?
    (event :: <keyboard-event>, gesture :: <keyboard-gesture>)
 => (true? :: <boolean>)
  event-key-name(event) == gesture-keysym(gesture)
  //---*** This isn't quite right -- if the shift key is held down to
  //---*** get a shifted keysym, the two modifier states won't match
  & event-modifier-state(event) == gesture-modifier-state(gesture)
end method event-matches-gesture?;
||#

(defmethod event-matches-gesture? ((event <keyboard-event>) (gesture <keyboard-gesture>))
  (and (eql (event-key-name event) (gesture-keysym gesture))
       ;;---*** This isn't quite right -- if the shift key is held down to
       ;;---*** get a shifted keysym, the two modifier states won't match
       (= (event-modifier-state event) (gesture-modifier-state gesture))))


#||
define sealed method event-matches-gesture?
    (event :: <keyboard-event>, char :: <character>)
 => (true? :: <boolean>)
  event-character(event) = char
  & logand(event-modifier-state(event), $bucky-keys) == 0
end method event-matches-gesture?;
||#

(defmethod event-matches-gesture? ((event <keyboard-event>) (char character))
  (and (equal? (event-character event) char)
       (= (logand (event-modifier-state event) +bucky-keys+) 0)))


#||
define generic modifier-state-matches-gesture?
    (modifier-state :: <integer>, gesture :: <gesture>)
 => (true? :: <boolean>);
||#

(defgeneric modifier-state-matches-gesture? (modifier-state gesture))


#||
define sealed method modifier-state-matches-gesture?
    (modifier-state :: <integer>, gesture :: <pointer-gesture>)
 => (true? :: <boolean>)
  modifier-state == gesture-modifier-state(gesture)
end method modifier-state-matches-gesture?;
||#

(defmethod modifier-state-matches-gesture? ((modifier-state integer) (gesture <pointer-gesture>))
  (= modifier-state (gesture-modifier-state gesture)))

