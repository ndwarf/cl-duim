;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-SHEETS-INTERNALS -*-
(in-package #:duim-sheets-internals)

;;; From CARET.LISP

(defgeneric display (object)
  (:documentation
"
Returns the display used to display _object_.
"))

;;; From SHEETS.LISP

(defgeneric port (object)
  (:documentation
"
Returns the port used to display _object_.
"))

(defgeneric sheet-state (sheet)
  (:documentation
"
Returns the current state of _sheet_. The state of a sheet tells you
whether the sheet is currently mapped on screen, or whether it has
been withdrawn from the list of sheets.

This method returns one of the values :withdrawn, :managed, :mapped,
or :unknown.
"))

(defgeneric (setf sheet-state) (state sheet &key do-repaint? clear?))
(defgeneric sheet-tab-stop? (sheet))
(defgeneric sheet-mirror-accepts-children? (sheet))
;;; FIXME: we should probably be making use of this...
(defgeneric sheet-event-mask (sheet)
  (:documentation
"
Returns the event mask of _sheet_.
"))

(defgeneric (setf sheet-event-mask) (mask sheet)
  (:documentation
"
Sets the event mask of _sheet_.
"))

(defgeneric graft-sheet (parent sheet))
(defgeneric degraft-sheet (parent sheet))
(defgeneric find-ancestor-of-type (sheet type))
(defgeneric sheet-mapped-children (sheet))

;;; From POINTER.LISP

(defgeneric update-pointer-cursor (pointer &key sheet frame))
(defgeneric do-with-busy-cursor (sheet cursor continuation))
(defgeneric pointer-state-changed? (pointer old-sheet old-x old-y))

;;; From PORTS.LISP

(defgeneric register-port-class (name class &key default?))
(defgeneric default-port (&key server-path)
  (:documentation
"
Returns the default port for server specified by _server-path_.
"))

(defgeneric default-port-setter (port)
  (:documentation
"
Sets the default port.
"))

(defgeneric find-port (&key server-path &allow-other-keys)
  (:documentation
"
Returns a suitable port for the specified server-path.
"))

(defgeneric port-event-loop (port))

;;; From MIRRORS.LISP

(defgeneric (setf sheet-mirror-accepts-children?) (accepts-children? sheet))
(defgeneric sheet-device-parent-transform (sheet))
(defgeneric sheet-device-parent (sheet &key error?))
(defgeneric update-all-mirror-positions (sheet))

;;; From FRAME-MANAGERS.LISP

(defgeneric find-frame-manager (&key port server-path class palette &allow-other-keys)
  (:documentation
"
Returns a suitable frame manager for the specified criteria.

If necessary, you can specify a _port_, _server-path_, _class_, or
_palette_. If any of these are not specified, then the default value
is used in each case. The _class_ argument specifies the class of
frame manager that should be returned.
"))

(defgeneric frame-manager-matches-options? (framem port &key palette class
						   &allow-other-keys))
(defgeneric destroy-frame-manager (framem))
(defgeneric make-pane (pane-class &key &allow-other-keys)
  (:documentation
"
Selects a class that implements the behaviour of _pane-class_ and
constructs a pane of that class.

Note: This method must be used in place of 'make-instance' for pane
types.
"))

(defgeneric get-frame-manager-and-owner (frame owner))

;;; From DISPLAYS.LISP

(defgeneric find-display (&key server-path port orientation units)
  (:documentation
"
Returns a suitable display for the specified port and server-path
criteria.

The _orientation_ and _units_ arguments can be used to specify the
orientation and display units that the returned _display_ needs to
use.
"))

;;; From MEDIUMS.LISP

(defgeneric sheet-has-medium? (sheet))
(defgeneric (setf medium-drawing-state-cache) (state medium))
(defgeneric (setf medium-merged-text-style) (text-style medium))

;;; From EVENT-QUEUE.LISP

(defgeneric event-queue-push-last (queue event))
(defgeneric event-queue-push (queue event))
(defgeneric event-queue-pop (queue))
(defgeneric event-queue-top (queue))
(defgeneric event-queue-wait (queue &key timeout))
(defgeneric event-queue-empty? (queue))
(defgeneric event-queue-clear (queue))

;;; From EVENTS.LISP

(defgeneric event-matches-modifiers? (event &rest modifiers))
(defgeneric generate-crossing-events (port event))
(defgeneric update-focus-for-enter-event (port sheet pointer))
(defgeneric update-focus-for-exit-event (port sheet pointer))
(defgeneric distribute-function-event (sheet function))

;;; From GESTURES.LISP

(defgeneric gesture-character (gesture))
