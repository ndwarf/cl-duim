;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

(asdf:defsystem :sheets
    :description "DUIM sheets"
    :long-description "Basic support for sheets. Sheets are the basic unit of window applications and can be nested hierarchically to build up a complete user interface."
    :author      "Scott McKay, Andy Armstrong (Lisp port: Duncan Rose <duncan@robotcat.demon.co.uk>)"
    :version     (:read-file-form "version.sexp")
    :licence     "BSD-2-Clause"
    :depends-on (#:trivial-timeout
		 #:bordeaux-threads
		 #:alexandria
		 #:duim-utilities
		 #:geometry
		 #:dcs)
    :serial t
    :components
    ((:file "package")
     (:file "protocols")
     (:file "private-gfs")
     (:file "macros")
     (:file "classes")
     (:file "caret")
     (:file "sheets")
     (:file "geometry")
     (:file "pointer")
     (:file "ports")
     (:file "mirrors")
     (:file "frame-managers")
     (:file "displays")
     (:file "mediums")
     (:file "font-mapping")
     (:file "event-queue")
     (:file "events")
     (:file "gestures")
     (:file "clipboard")
     (:file "printers")))

