;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-SHEETS-INTERNALS -*-
(in-package #:duim-sheets-internals)

#||
/// Ports
define constant <focus-policy>
    = one-of(#"sheet-under-pointer", #"click-to-select");
||#

(deftype <focus-policy> ()
  '(member :sheet-under-pointer :click-to-select))

#||
define constant <event-processor-type>
    = one-of(#"n", #"n+1", #"2n");
||#
(deftype <event-processor-type> ()
  '(member :n :n+1 :2n))


#||
define protocol <<port-protocol>> ()
  // Making and destroying ports
  function class-for-make-port
    (type-name, #rest initargs, #key, #all-keys)
 => (class :: <class>, initargs :: false-or(<sequence>));
  function destroy-port (port :: <abstract-port>) => ();
  function port-matches-server-path?
    (port :: <abstract-port>, server-path) => (true? :: <boolean>);
  function restart-port (port :: <abstract-port>) => ();
  // The contract of 'process-next-event' is to handle a single "raw"
  // event at the back-end.  Note that this will not necessarily result
  // in the distribution of a DUIM <event> object.
  function process-next-event
      (port :: <abstract-port>, #key timeout)
   => (timed-out? :: <boolean>);
  // Accessors
  getter port-modifier-state
    (port :: <abstract-port>) => (state :: <integer>);
  getter port-pointer
    (port :: <abstract-port>) => (pointer :: <pointer>);
  setter port-pointer-setter
    (pointer :: <pointer>, port :: <abstract-port>) => (pointer :: <pointer>);
  getter port-input-focus
    (port :: <abstract-port>) => (focus :: false-or(<abstract-sheet>));
  setter port-input-focus-setter
    (focus :: false-or(<abstract-sheet>), port :: <abstract-port>)
 => (focus :: false-or(<abstract-sheet>));
  getter port-focus-policy
    (port :: <abstract-port>) => (policy :: <focus-policy>);
  function note-focus-in
    (port :: <abstract-port>, sheet :: <abstract-sheet>) => ();
  function note-focus-out
    (port :: <abstract-port>, sheet :: <abstract-sheet>) => ();
  getter port-frame-managers
    (port :: <abstract-port>) => (framems :: <stretchy-vector>);
  getter port-default-frame-manager
    (port :: <abstract-port>) => (framem :: false-or(<abstract-frame-manager>));
  setter port-default-frame-manager-setter
    (framem :: false-or(<abstract-frame-manager>), port :: <abstract-port>)
 => (framem :: false-or(<abstract-frame-manager>));
  getter port-default-palette
    (port :: <abstract-port>) => (palette :: false-or(<palette>));
  setter port-default-palette-setter
    (palette :: false-or(<palette>), port :: <abstract-port>)
 => (palette :: false-or(<palette>));
  getter port-name
    (port :: <abstract-port>) => (name :: false-or(<string>));
  getter port-type
    (port :: <abstract-port>) => (type :: <symbol>);
  getter port-server-path
    (port :: <abstract-port>) => (server-path);
  getter port-properties
    (port :: <abstract-port>) => (properties :: <sequence>);
  setter port-properties-setter
    (properties :: <sequence>, port :: <abstract-port>) => (properties :: <sequence>);
  getter port-displays
    (port :: <abstract-port>) => (displays :: <stretchy-vector>);
  setter port-displays-setter
    (displays :: <stretchy-vector>, port :: <abstract-port>) => (displays :: <stretchy-vector>);
  // Text style mapping
  getter port-font-mapping-table
    (port :: <abstract-port>) => (table :: <table>);
  getter port-font-mapping-cache
    (port :: <abstract-port>) => (pair :: <pair>);
  getter port-undefined-text-style
    (port :: <abstract-port>) => (text-style :: <text-style>);
  setter port-undefined-text-style-setter
    (text-style :: <text-style>, port :: <abstract-port>)
 => (text-style :: <text-style>);
  // Ports can specialize these to compute foreground and background
  // brush based on X resource files or whatever
  function get-default-foreground
    (port :: <abstract-port>, sheet :: <abstract-sheet>, #key foreground, default)
 => (foreground :: false-or(<ink>));
  function port-default-foreground
    (port :: <abstract-port>, sheet :: <abstract-sheet>)
 => (foreground :: false-or(<ink>));
  function get-default-background
    (port :: <abstract-port>, sheet :: <abstract-sheet>, #key background, default)
 => (background :: false-or(<ink>));
  function port-default-background
    (port :: <abstract-port>, sheet :: <abstract-sheet>)
 => (background :: false-or(<ink>));
  function get-default-text-style
    (port :: <abstract-port>, sheet :: <abstract-sheet>, #key text-style, default)
 => (text-style :: false-or(<text-style>));
  function port-default-text-style
    (port :: <abstract-port>, sheet :: <abstract-sheet>)
 => (text-style :: false-or(<text-style>));
end protocol <<port-protocol>>;


(define-protocol <<port-protocol>> ()
  ;; Making and destroying ports
  (:function class-for-make-port (type-name &rest initargs
				  &key &allow-other-keys))
  (:function destroy-port (port))
  (:function port-matches-server-path? (port server-path))
  (:function restart-port (port))
  ;; The contract of 'process-next-event' is to handle a single "raw"
  ;; event at the back-end.  Note that this will not necessarily result
  ;; in the distribution of a DUIM <event> object.
  (:function process-next-event (port &key timeout))
  ;; Accessors
  (:getter port-modifier-state (port))
  (:getter port-pointer (port))
  (:setter (setf port-pointer) (pointer port))
  (:getter port-input-focus (port))
  (:setter (setf port-input-focus) (focus port))
  (:getter port-focus-policy (port))
  (:function note-focus-in (port sheet))
  (:function note-focus-out (port sheet))
  (:getter port-frame-managers (port))
  (:getter port-default-frame-manager (port))
  (:setter (setf port-default-frame-manager) (framem port))
  (:getter port-default-palette (port))
  (:setter (setf port-default-palette) (palette port))
  (:getter port-name (port))
  (:getter port-type (port))
  (:getter port-server-path (port))
  (:getter port-properties (port))
  (:setter (setf port-properties) (properties port))
  (:getter port-displays (port))
  (:setter (setf port-displays) (displays port))
  ;; Text style mapping
  (:getter port-font-mapping-table (port))
  (:getter port-font-mapping-cache (port))
  (:getter port-undefined-text-style (port))
  (:setter (setf port-undefined-text-style) (text-style port))
  ;; Ports can specialize these to compute foreground and background
  ;; brush based on X resource files or whatever
  (:function get-default-foreground (port sheet &key foreground default))
  (:function port-default-foreground (port sheet))
  (:function get-default-background (port sheet &key background default))
  (:function port-default-background (port sheet))
  (:function get-default-text-style (port sheet &key text-style default))
  (:function port-default-text-style (port sheet)))
||#

#||
define open abstract primary class <basic-port> (<port>)
  sealed slot port-server-path;
  sealed constant slot port-lock :: <simple-lock> = make(<simple-lock>);
  sealed slot port-properties :: <stretchy-object-vector> = make(<stretchy-vector>);
  sealed slot port-displays :: <stretchy-object-vector> = make(<stretchy-vector>);
  // This tells us what policy we use for event processing:
  //  - #"n" means event processing happens in each user thread
  //  - #"n+1" means there's a single event processing thread that
  //    distributes events to each user thread
  //  - #"2n" means there's an event processing thread for each and
  //    every user thread
  sealed constant slot port-event-processor-type :: <event-processor-type> = #"n",
    init-keyword: event-processor-type:;
  sealed slot port-event-thread = #f;
  sealed slot port-frame-managers :: <stretchy-object-vector> = make(<stretchy-vector>);
  sealed slot port-input-focus :: false-or(<sheet>) = #f,
    setter: %input-focus-setter;
  sealed constant slot port-focus-policy :: <focus-policy> = #"sheet-under-pointer",
    init-keyword: focus-policy:;
  sealed slot %pointer = #f;
  // The modifier state gets maintained by each back-end
  sealed slot port-modifier-state :: <integer> = make-modifier-state();
  sealed constant slot %trace-stack    :: <stretchy-object-vector> = make(<stretchy-vector>);
  sealed constant slot %ancestor-stack :: <stretchy-object-vector> = make(<stretchy-vector>);
  // The next two are for when the port is generating double click
  // events itself.  When the interval is #f, the underlying platform
  // may still generate double click events
  sealed slot %double-click-interval = #f,
    init-keyword: double-click-interval:;
  sealed slot %last-button-press-time = 0;
  //--- The next two should really be in the display, no?
  //--- Fix this when you change sheet.%port to sheet.%display
  sealed slot port-default-palette :: false-or(<palette>) = #f;
  sealed slot %medium-cache :: <object-deque> = make(<object-deque>);
  // Text style -> font mapping tables and one-element cache
  sealed constant slot port-font-mapping-table :: <object-table> = make(<table>);
  sealed constant slot port-font-mapping-cache :: <pair> = pair(#f, #f);
  sealed slot port-undefined-text-style :: <text-style> = $undefined-text-style;
  // This specifies how size mapping is done.
  //  - #"exact" -- the size given in the text style is exactly the size
  //    that should be used during mapping.
  //  - #"loose" -- the font whose size is closest to the size in the text
  //    style is used.  (In this case, the mapping table ignores the size
  //    and each bucket in the table is a list sorted by size.)
  //  - #"scalable" -- the size is ignored in the cache, and the back-end
  //    takes care of scaling the font
  sealed constant slot %text-style-size-mapping = #"loose",
    init-keyword: text-style-size-mapping:;
  sealed slot port-alive? :: <boolean> = #f;
end class <basic-port>;
||#

(defclass <basic-port> (<port>)
  ((port-server-path :accessor port-server-path)
   (port-lock :initform (bordeaux-threads:make-lock "<PORT> lock") :reader port-lock) ;;:type <simple-lock>
   (port-properties :type vector :initform (make-array 0 :adjustable t :fill-pointer t) :accessor port-properties)
   (port-displays :type list :initform (list) :accessor port-displays)
   ;; This tells us what policy we use for event processing:
   ;;  - #"n" means event processing happens in each user thread
   ;;  - #"n+1" means there's a single event processing thread that
   ;;    distributes events to each user thread
   ;;  - #"2n" means there's an event processing thread for each and
   ;;    every user thread
   (port-event-processor-type :type <event-processor-type> :initarg :event-processor-type :initform :n :reader port-event-processor-type)
   (port-event-thread :initform nil :accessor port-event-thread)
   (port-frame-managers :type list :initform (list) :accessor port-frame-managers)
   (port-input-focus :type (or null <sheet>) :initform nil :reader port-input-focus :writer %input-focus-setter)
   (port-focus-policy :type <focus-policy> :initarg :focus-policy :initform :sheet-under-pointer :reader port-focus-policy)
   (%pointer :initform nil :accessor %pointer)
   ;; The modifier state gets maintained by each back-end
   (port-modifier-state :type integer :initform (make-modifier-state) :accessor port-modifier-state)
   (%trace-stack :type vector :initform (make-array 0 :adjustable t :fill-pointer t) :reader %trace-stack)
   (%ancestor-stack :type vector :initform (make-array 0 :adjustable t :fill-pointer t) :reader %ancestor-stack)
   ;; The next two are for when the port is generating double click
   ;; events itself. When the interval is #f, the underlying platform
   ;; may still generate double click events
   (%double-click-interval :initarg :double-click-interval :initform nil :accessor %double-click-interval)
   (%last-button-press-time :initform 0 :accessor %last-button-press-time)
   ;;--- The next two should really be in the display, no?
   ;;--- Fix this when you change sheet.%port to sheet.%display
   (port-default-palette :type (or null <palette>) :initform nil :accessor port-default-palette)
   (%medium-cache :type <object-deque> :initform (make-instance '<object-deque>) :accessor %medium-cache)
   ;; Text style -> font mapping tables and one-element cache
   ;; EQUALP test because the keys are TEXT STYLES...
   ;; MAPPING-TABLE IS A HASHTABLE OF KEY (PARTIAL TEXT-STYLE) : LIST WHERE EACH LIST ITEM IS A CONS OF
   ;; (FULL STYLE . FONT)
   (port-font-mapping-table :type hash-table :initform (make-hash-table :test #'equalp) :reader port-font-mapping-table)
   (port-font-mapping-cache :type cons :initform (cons nil nil) :reader port-font-mapping-cache)
   (port-undefined-text-style :type <text-style> :initform *undefined-text-style* :accessor port-undefined-text-style)
   ;; This specifies how size mapping is done.
   ;;  - #"exact" -- the size given in the text style is exactly the size
   ;;    that should be used during mapping.
   ;;  - #"loose" -- the font whose size is closest to the size in the text
   ;;    style is used. (In this case, the mapping table ignores the size
   ;;    and each bucket in the table is a list sorted by size.)
   ;;  - #"scalable" -- the size is ignored in the cache, and the back-end
   ;;    takes care of scaling the font
   (%text-style-size-mapping :initarg :text-style-size-mapping :initform :loose :reader %text-style-size-mapping)
   (port-alive? :type boolean :initform nil :accessor port-alive?))
  (:metaclass <abstract-metaclass>))


;; FIXME: WHAT TO DO WITH THIS "THREAD-SLOT"?
#||
define thread-slot port-alive? :: <boolean> of <abstract-port>;

define method port (_port :: <port>) => (port :: <port>)
  _port
end method port;
||#

(defmethod port ((_port <port>))
  _port)


#||
define method port (object) => (port :: singleton(#f))
  #f
end method port;
||#

(defmethod port ((object t))
  (declare (ignore object))
  nil)


#||
define method port-default-frame-manager
    (_port :: <port>) => (framem :: false-or(<frame-manager>))
  let framems = port-frame-managers(_port);
  if (empty?(framems))
    find-frame-manager(port: _port)
  else
    framems[0]
  end
end method port-default-frame-manager;
||#

(defmethod port-default-frame-manager ((_port <port>))
  (let ((framems (port-frame-managers _port)))
    (if (empty? framems)
        (find-frame-manager :port _port)
        (first framems))))


#||
// Default port method uses just gets the color from the port's default palette
define method find-color
    (name, port :: <basic-port>, #key error? = #t) => (color :: <color>)
  find-color(name, port-default-palette(port), error?: error?)
end method find-color;
||#

;; Default port method uses just gets the color from the port's default palette
(defmethod find-color (name (port <basic-port>) &key (error? t))
  (find-color name (port-default-palette port) :error? error?))


#||
define variable *default-port-class-name* :: false-or(<symbol>) = #f;
define variable *port-classes* :: <object-table> = make(<object-table>);
define variable *ports* :: <stretchy-object-vector> = make(<stretchy-vector>);
||#

(defparameter *default-port-class-name* nil)
(defparameter *port-classes* (make-hash-table :test #'eql))
(defparameter *ports* (list))


#||
define method initialize (_port :: <port>, #key)
  next-method();
  add!(*ports*, _port);
  restart-port(_port)
end method initialize;
||#

(defmethod initialize-instance :after ((_port <port>) &key &allow-other-keys)
  (setf *ports* (cons _port *ports*))
  (restart-port _port))


#||
define method initialize (_port :: <basic-port>, #key server-path)
  port-server-path(_port) := (server-path & copy-sequence(server-path));
  next-method()
end method initialize;
||#

;; Yes, this is on the PRIMARY method. The server path needs to be
;; set up before we invoke RESTART-PORT in the :AFTER method of <PORT>.
;; This could probably be a :before method instead, but I'm trying to
;; avoid those (they seem to tend to lead to instability since they (can)
;; run swathes of code before any slots have been initialized!)

;; :before methods run more specific -> less specific, :after methods run
;; less specific -> more specific. <basic-port> is more specific than <port>

;; primary methods run most specific, then call-next-method calls next
;; most specific method.

;; XXX: ^^^ this is key to how the method combinators work, in duim they're
;; /all/ most specific then explicit next methods to less specific. Perhaps
;; we should use a custom method combination order?

(defmethod initialize-instance ((_port <basic-port>) &key server-path &allow-other-keys)
  (setf (port-server-path _port) (and server-path (copy-seq server-path)))
  (call-next-method))


#||
define method register-port-class
    (name :: <symbol>, class :: subclass(<port>), #key default? :: <boolean> = #f) => ()
  *port-classes*[name] := class;
  if (default?)
    *default-port-class-name* := name
  end
end method register-port-class;
||#

(defmethod register-port-class ((name symbol) (class class) &key (default? nil))
  ;; TODO: is there a better way to ensure CLASS is a <PORT>?
  ;; FIXME: these should signal NO-APPLICABLE-METHOD when the wrong type is used
  (assert (subtypep class (find-class '<port>)))
  (setf (gethash name *port-classes*) class)
  (when default?
    (setf *default-port-class-name* name)))



#||
/// Making ports

define macro with-port-locked
  { with-port-locked (?object:expression) ?:body end }
    => { begin
	   let _port = port(?object);
	   with-lock (port-lock(_port))
	     ?body;
	   failure
	     error("Couldn't get port lock for %=", _port);
	   end
	 end }
end macro with-port-locked;
||#

(defmacro with-port-locked ((object) &body body)
  (let ((_port (gensym "PORT-")))
    `(let ((,_port (port ,object)))
      (handler-case
          (bordeaux-threads:with-lock-held ((port-lock ,_port))
            ,@body)
        (error (evar)
	  (error "Error:~&~a~& -- whilst holding (or attempting to hold) port lock for ~a"
		 evar ,_port))))))


#||
define inline function do-ports (function :: <function>) => ()
  dynamic-extent(function);
  do(function, *ports*)
end function do-ports;
||#

(defun do-ports (function)
"
Runs a function on all the current ports.
"
  (declare (dynamic-extent function))
  (map nil function *ports*))


#||
define variable *default-port* :: false-or(<port>) = #f;
||#

(defparameter *default-port* nil)

;; "Primary" back-ends will supply a 'class-for-make-port' method
;; that maps "local" to something more interesting...
(defparameter *default-server-path* (list :local))


#||
define method default-port (#key server-path) => (_port :: false-or(<port>))
  *default-port*
  | (*default-port* := find-port(server-path: server-path | *default-server-path*))
end method default-port;
||#

(defmethod default-port (&key server-path)
  (or *default-port*
      (setf *default-port*
	    (find-port :server-path (or server-path
					*default-server-path*)))))


#||
define method default-port-setter (_port :: <port>) => (_port :: <port>)
  *default-port* := _port
end method default-port-setter;
||#

;;; FIXME: Dylan-specific boiler plate -- remove

(defmethod default-port-setter ((_port <port>))
  (setf *default-port* _port))


#||
// "Primary" back-ends will supply a 'class-for-make-port' method
// that maps "local" to something more interesting...
define variable *default-server-path* = #(#"local");
define variable *global-lock* :: <simple-lock> = make(<simple-lock>);
||#

(defparameter *global-lock* (bordeaux-threads:make-lock "DUIM global lock"))


#||
define method find-port
    (#rest initargs,
     #key server-path = *default-server-path*, #all-keys)
 => (port :: <port>)
  dynamic-extent(initargs);
  with-lock (*global-lock*)
    block (return)
      local method match-port (_port) => ()
	      when (port-matches-server-path?(_port, server-path))
		return(_port)
	      end
	    end method;
      dynamic-extent(match-port);
      do-ports(match-port);
      with-keywords-removed (initargs = initargs, #[server-path:])
	apply(make, <port>, server-path: server-path, initargs)
      end
    end
  failure
    error("Couldn't get the global lock");
  end
end method find-port;
||#

(defmethod find-port (&rest initargs &key (server-path *default-server-path*) &allow-other-keys)
  (declare (dynamic-extent initargs))
  (bordeaux-threads:with-lock-held (*global-lock*)
    (labels ((match-port (_port)
	       (when (port-matches-server-path? _port server-path)
		 (return-from find-port _port))))
      (do-ports #'match-port)
      (with-keywords-removed (new-initargs = initargs (:server-path))
	(let ((_port (apply #'make-port
			    :server-path server-path new-initargs)))
	  (return-from find-port _port)))))
  (error "Couldn't get the global lock"))


#||
//--- How should server path matching really work?.  For instance, should an
//--- unspecified option match any value, or should there be some protocol for
//--- getting at the port-specific default value (noting that we may not have
//--- made any port with the default value yet).  For now, we just do a
//--- strict value-by-value-match.
define method port-matches-server-path?
    (_port :: <port>, server-path) => (true? :: <boolean>)
  destructuring-let ((type, #rest options) = server-path)
    destructuring-let ((port-type, #rest port-options) = port-server-path(_port))
      type == port-type
      // Now verify that the options are equivalent, ignoring ordering.
      // Copy the options so we can keep the lists in sync easily.
      & begin
	  let port-options = copy-sequence(port-options);
	  let options = concatenate-as(<list>, options);
	  block (return)
	    while (#t)
	      when (empty?(options))
		return(empty?(port-options))
	      end;
	      let indicator = pop!(options);
	      let value = pop!(options);
	      unless (get-property(port-options, indicator) = value)
		return(#f)
	      end;
	      remove-property!(port-options, indicator)
	    end
	  end
	end
    end
  end
end method port-matches-server-path?;
||#

;;--- How should server path matching really work? For instance, should
;;--- an unspecified option match any value, or should there be some protocol for
;;--- getting at the port-specific default value (noting that we may not have
;;--- made any port with the default value yet). For now, we just do a
;;--- strict value-by-value-match.
(defmethod port-matches-server-path? ((_port <port>) server-path)
  (destructuring-bind (type &rest options)
      server-path
    (destructuring-bind (port-type &rest port-options)
        (port-server-path _port)
      (and (eql type port-type)
           ;; Now verify that the options are equivalent, ignoring ordering.
           ;; Copy the options so we can keep the lists in sync easily.
           (let ((port-options (copy-seq port-options)))
             (block inner
	       (loop while t
		     do (when (empty? options)
			  (return-from inner (empty? port-options)))
		     do (let ((indicator (pop options))
			      (value (pop options)))
			  (unless (equal? (getf port-options indicator) value)
			    (return-from inner nil))
                          (setf port-options (remf port-options indicator))))))))))


#||
define sealed inline method make
    (class == <port>, #rest initargs, #key server-path, #all-keys)
 => (port :: <port>)
  dynamic-extent(initargs);
  let (port-class, new-initargs)
    = apply(class-for-make-port, first(server-path), initargs);
  apply(make, port-class, new-initargs | initargs)
end method make;
||#

(defmethod make-port (&rest initargs &key server-path &allow-other-keys)
  (declare (dynamic-extent initargs))
  (unless server-path
    (error "Can't make <port> without a server-path. Try FIND-PORT instead."))
  (multiple-value-bind (port-class new-initargs)
      (apply #'class-for-make-port (first server-path) initargs)
    (apply #'make-instance port-class (or new-initargs initargs))))


#||
// If a class came in, send it back out
define method class-for-make-port
    (type :: <class>, #key)
 => (class :: <class>, initargs :: false-or(<sequence>))
  values(type, #f)
end method class-for-make-port;
||#

;; If a class came in, send it back out
(defmethod class-for-make-port ((type class)
				&key)
  (values type nil))


#||
define method class-for-make-port
    (type == #"local", #rest initargs, #key)
 => (class :: <class>, initargs :: false-or(<sequence>))
  unless (*default-port-class-name*)
    error("Cannot create a port, as no port classes were registered")
  end;
  apply(class-for-make-port, *default-port-class-name*, initargs)
end method class-for-make-port;
||#

(defmethod class-for-make-port ((type (eql :local)) &rest initargs &key)
  (unless *default-port-class-name*
    (error "Cannot create a port, as no port classes were registered"))
  (apply #'class-for-make-port *default-port-class-name* initargs))


#||
define method class-for-make-port
    (type, #key)
 => (class :: <class>, initargs :: false-or(<sequence>))
  error("Cannot create a port of type %=", type)
end method class-for-make-port;
||#

(defmethod class-for-make-port (type &key)
  (error "Cannot create a port of type ~a" type))


#||
define method destroy-port (_port :: <port>) => ()
  when (port-event-thread(_port))
    destroy-thread(port-event-thread(_port))
  end;
  while (~empty?(port-frame-managers(_port)))
    destroy-frame-manager(port-frame-managers(_port)[0])
  end;
  do(destroy-sheet, port-displays(_port));
  port-displays(_port).size := 0;
  remove!(*ports*, _port)
end method destroy-port;
||#

(defmethod destroy-port ((_port <port>))
  (when (port-event-thread _port)
    (bordeaux-threads:destroy-thread (port-event-thread _port)))
  (loop while (not (empty? (port-frame-managers _port)))
	do (destroy-frame-manager (first (port-frame-managers _port))))
  (mapcar #'destroy-sheet (port-displays _port))
  (setf (port-displays _port) ())
  (setf *ports* (delete _port *ports*)))


#||
define method destroy-port (_port :: <basic-port>) => ()
  do(destroy-medium, _port.%medium-cache);
  next-method()
end method destroy-port;
||#

(defmethod destroy-port ((_port <basic-port>))
  (loop while (not (empty? (%medium-cache _port)))
        do (destroy-medium (deque-pop-last (%medium-cache _port))))
  (call-next-method))



#||
/// Ports vs. pointers

define sealed method port-pointer
    (_port :: <basic-port>) => (pointer :: <pointer>)
  _port.%pointer
  | (_port.%pointer
       := make(<pointer>, port: _port))
end method port-pointer;
||#

(defmethod port-pointer ((_port <basic-port>))
  (or (%pointer _port)
      (setf (%pointer _port) (make-pointer :port _port))))


#||
define sealed method port-pointer-setter
    (pointer :: <pointer>, _port :: <basic-port>) => (pointer :: <pointer>)
  _port.%pointer := pointer
end method port-pointer-setter;
||#

(defmethod (setf port-pointer) ((pointer <pointer>) (_port <basic-port>))
  (setf (%pointer _port) pointer))



#||
/// Input focus handling

define method port-input-focus-setter
    (new :: false-or(<sheet>), _port :: <basic-port>)
 => (new :: false-or(<sheet>))
  let old = port-input-focus(_port);
  unless (new == old)
    let new-frame = new & sheet-frame(new);
    _port.%input-focus := new;
    if (new-frame)
      frame-input-focus(new-frame) := new
    end;
    when (old)
      note-focus-out(_port, old)
    end;
    when (new)
      note-focus-in(_port, new)
    end;
  end;
  new
end method port-input-focus-setter;
||#

(defmethod (setf port-input-focus) ((new null) (_port <basic-port>))
  (%port-input-focus-setter new _port))

(defmethod (setf port-input-focus) ((new <sheet>) (_port <basic-port>))
  (%port-input-focus-setter new _port))

(defun %port-input-focus-setter (new _port)
  (let ((old (port-input-focus _port)))
    (unless (eql new old)
      (let ((new-frame (and new (sheet-frame new))))
	(%input-focus-setter new _port)
        (when new-frame
          (setf (frame-input-focus new-frame) new))
        (when old
          (note-focus-out _port old))
        (when new
          (note-focus-in _port new))))
    new))


#||
define method note-focus-out
    (_port :: <basic-port>, sheet :: <sheet>) => ()
  distribute-event(_port, make(<input-focus-out-event>, sheet: sheet))
end method note-focus-out;
||#

(defmethod note-focus-out ((_port <basic-port>) (sheet <sheet>))
  (distribute-event _port (make-instance (find-class '<input-focus-out-event>) :sheet sheet)))


#||
define method note-focus-in
    (_port :: <basic-port>, sheet :: <sheet>) => ()
  distribute-event(_port, make(<input-focus-in-event>,  sheet: sheet))
end method note-focus-in;
||#

(defmethod note-focus-in ((_port <basic-port>) (sheet <sheet>))
  (distribute-event _port (make-instance (find-class '<input-focus-in-event>) :sheet sheet)))



#||
/// Port event loop

define method restart-port (_port :: <port>) => ()
  // If the event processing loop is supposed to run in it's own
  // thread, start it up now
  select (port-event-processor-type(_port))
    #"n" => #f;
    #"n+1" =>
      when (port-event-thread(_port))
	destroy-thread(port-event-thread(_port))
      end;
      port-event-thread(_port)
	:= make(<thread>,
		function: method () port-event-loop(_port) end,
		name: format-to-string("CLIM Event Dispatcher for %a",
				       port-server-path(_port)));
    #"2n" => #f;	//---*** what do we do about this case?
  end
end method restart-port;
||#

(defmethod restart-port ((_port <port>))
  ;; If the event processing loop is supposed to run in it's own
  ;; thread, start it up now
  (ecase (port-event-processor-type _port)
    (:n nil)
    (:n+1
     (when (port-event-thread _port)
       (bordeaux-threads:destroy-thread (port-event-thread _port)))
     (setf (port-event-thread _port)
           (bordeaux-threads:make-thread #'(lambda ()
					     (port-event-loop _port))
					 :name
					 (format nil
						 "CLIM Event Dispatcher for ~a"
						 (port-server-path _port)))))
    (:2n nil)))  ;;---*** what do we do about this case?


#||
define method port-event-loop
    (_port :: <port>) => ()
  block (return)
    dynamic-bind (port-alive?(_port) = #t)
      simple-restart-loop ("Exit event loop for port %a", port-name(_port))
        simple-restart-loop ("Restart event loop for port %a", port-name(_port))
          process-next-event(_port)
        end
      end
    end
  end
end method port-event-loop;
||#

(defmethod port-event-loop ((_port <port>))
  (dynamic-bind (((port-alive? _port) = t))
    (with-simple-restart (exit "Exit event loop for port ~a" (port-name _port))
      (loop
	 (with-simple-restart (restart "Restart event loop for port ~a" (port-name _port))
	   (process-next-event _port))))))


#||
define method note-port-terminated (_port :: <port>, condition) => ()
  port-alive?(_port) := #f;
  for (framem in port-frame-managers(_port))
    for (frame in frame-manager-frames(framem))
      distribute-event(_port, make(<port-terminated-event>,
				   condition: condition, frame: frame));
      let sheet = top-level-sheet(frame);
      when (sheet)
	generate-trigger-event(_port, sheet)
      end
    end
  end;
  remove!(*ports*, _port)
end method note-port-terminated;
||#

(defgeneric note-port-terminated (port condition))

(defmethod note-port-terminated ((_port <port>) condition)
  (setf (port-alive? _port) nil)
  (loop for framem in (port-frame-managers _port)
        do (loop for frame in (frame-manager-frames framem)
                 do (distribute-event _port
				      (make-instance (find-class '<port-terminated-event>)
						     :condition condition
						     :frame frame))
                 do (let ((sheet (top-level-sheet frame)))
                      (when sheet
                        (generate-trigger-event _port sheet)))))
  (setf *ports* (delete _port *ports*)))



#||
/// "Resources"

define sealed method get-default-foreground
    (_port :: <port>, sheet :: <sheet>, #key foreground, default = $default-foreground)
 => (foreground :: <ink>)
  foreground
  | default-foreground(sheet)
  | (sheet-frame(sheet) & default-foreground(sheet-frame(sheet)))
  | port-default-foreground(_port, sheet)	// consult resources here...
  | default
end method get-default-foreground;
||#

(defmethod get-default-foreground ((_port <port>) (sheet <sheet>) &key foreground (default *default-foreground*))
  (or foreground
      (default-foreground sheet)
      (and (sheet-frame sheet) (default-foreground (sheet-frame sheet)))
      (port-default-foreground _port sheet)    ;; consult resources here...
      default))


#||
define method port-default-foreground
    (_port :: <port>, sheet :: <sheet>) => (foreground :: false-or(<ink>))
  #f
end method port-default-foreground;
||#

(defmethod port-default-foreground ((_port <port>) (sheet <sheet>))
  nil)


#||
define sealed method get-default-background
    (_port :: <port>, sheet :: <sheet>, #key background, default = $default-background)
 => (background :: <ink>)
  background
  | default-background(sheet)
  | (sheet-frame(sheet) & default-background(sheet-frame(sheet)))
  | port-default-background(_port, sheet)	// consult resources here...
  | default
end method get-default-background;
||#

(defmethod get-default-background ((_port <port>) (sheet <sheet>) &key background (default *default-background*))
  (or background
      (default-background sheet)
      (and (sheet-frame sheet) (default-background (sheet-frame sheet)))
      (port-default-background _port sheet)    ;; consult resources here...
      default))


#||
define method port-default-background
    (_port :: <port>, sheet :: <sheet>) => (background :: false-or(<ink>))
  #f
end method port-default-background;
||#

(defmethod port-default-background ((_port <port>) (sheet <sheet>))
  nil)


#||
define sealed method get-default-text-style
    (_port :: <port>, sheet :: <sheet>, #key text-style, default = $null-text-style)
 => (text-style :: false-or(<text-style>))
  let style
    = text-style
      | default-text-style(sheet)
      | (sheet-frame(sheet) & default-text-style(sheet-frame(sheet)))
      | default;
  if (style & fully-merged-text-style?(style))
    style
  else
    let default-style
      = port-default-text-style(_port, sheet) | $default-text-style;
    style & merge-text-styles(style, default-style)
  end
end method get-default-text-style;
||#

(defmethod get-default-text-style ((_port <port>) (sheet <sheet>) &key text-style (default *null-text-style*))
  (let ((style (or text-style
                   (default-text-style sheet)
                   (and (sheet-frame sheet) (default-text-style (sheet-frame sheet)))
                   default)))
    (if (and style (fully-merged-text-style? style))
        style
        (let ((default-style (or (port-default-text-style _port sheet) *default-text-style*)))
          (and style (merge-text-styles style default-style))))))


#||
define method port-default-text-style
    (_port :: <port>, sheet :: <sheet>) => (text-style :: false-or(<text-style>))
  #f
end method port-default-text-style;
||#

(defmethod port-default-text-style ((_port <port>) (sheet <sheet>))
  nil)

