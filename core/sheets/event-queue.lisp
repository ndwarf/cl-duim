;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-SHEETS-INTERNALS -*-
(in-package #:duim-sheets-internals)

#||
/// Event queues

// Event queues encapsulate both a queue and a notification that serves to
// synchronize multiple threads
define sealed class <event-queue> (<object>)
  sealed constant slot %deque :: <object-deque> = make(<object-deque>);
  sealed constant slot %non-empty :: <notification> = make(<notification>, lock: make(<lock>));
end class <event-queue>;
||#

(defclass <event-queue> ()
  ((%deque :type <object-deque> :initform (make-instance '<object-deque>) :reader %deque)
   (%event-queue-lock :initform (bordeaux-threads:make-lock "DUIM event queue notification lock")
		      :reader associated-lock)
   (%non-empty :initform (bordeaux-threads:make-condition-variable) :reader %non-empty))
  (:documentation
"
Event queues encapsulate both a queue and a notification that serves to
synchronize multiple threads.
"))


#||
define sealed domain make (singleton(<event-queue>));
define sealed domain initialize (<event-queue>);

define sealed method event-queue-push
    (queue :: <event-queue>, event :: <event>) => ()
  with-lock (associated-lock(queue.%non-empty))
    when (empty?(queue.%deque))
      // If there are any threads waiting for something to go into
      // the event queue, wake them up now
      release-all(queue.%non-empty)
    end;
    push(queue.%deque, event)
  end
end method event-queue-push;
||#

(defmethod event-queue-push ((queue <event-queue>) (event <event>))
  (bordeaux-threads:with-lock-held ((associated-lock queue))
    (when (empty? (%deque queue))
      ;; If there are any threads waiting for something to go into
      ;; the event queue, wake them up now
      (bordeaux-threads:condition-notify (%non-empty queue)))
    (deque-push (%deque queue) event)))


#||
define sealed method event-queue-push-last
    (queue :: <event-queue>, event :: <event>) => ()
  with-lock (associated-lock(queue.%non-empty))
    when (empty?(queue.%deque))
      release-all(queue.%non-empty)
    end;
    push-last(queue.%deque, event)
  end
end method event-queue-push-last;
||#

(defmethod event-queue-push-last ((queue <event-queue>) (event <event>))
  (bordeaux-threads:with-lock-held ((associated-lock queue))
    (when (empty? (%deque queue))
      (bordeaux-threads:condition-notify (%non-empty queue)))
    (deque-push-last (%deque queue) event)))


#||
define sealed method event-queue-pop
    (queue :: <event-queue>) => (event :: <event>)
  with-lock (associated-lock(queue.%non-empty))
    // Block until there's something to pop
    while (empty?(queue.%deque))
      wait-for(queue.%non-empty);
    end;
    pop(queue.%deque)
  end
end method event-queue-pop;
||#

(defmethod event-queue-pop ((queue <event-queue>))
  (bordeaux-threads:with-lock-held ((associated-lock queue))
    ;; Block until there's something to pop
    (loop while (empty? (%deque queue))
          do (bordeaux-threads:condition-wait (%non-empty queue) (associated-lock queue)))
    (deque-pop (%deque queue))))


#||
define sealed method event-queue-top
    (queue :: <event-queue>) => (event :: <event>)
  with-lock (associated-lock(queue.%non-empty))
    while (empty?(queue.%deque))
      wait-for(queue.%non-empty);
    end;
    queue.%deque[0]
  end
end method event-queue-top;
||#

(defmethod event-queue-top ((queue <event-queue>))
  (bordeaux-threads:with-lock-held ((associated-lock queue))
    ;; Block until there's something to pop
    (loop while (empty? (%deque queue))
          do (bordeaux-threads:condition-wait (%non-empty queue) (associated-lock queue)))
    (aref (%deque queue)) 0))


#||
define sealed method event-queue-wait
    (queue :: <event-queue>, #key timeout) => (timed-out? :: <boolean>)
  with-lock (associated-lock(queue.%non-empty))
    while (empty?(queue.%deque))
      wait-for(queue.%non-empty, timeout: timeout);
    end;
    // If the queue is empty, we must have timed out
    empty?(queue.%deque)
  end
end method event-queue-wait;
||#

;; Returns #t on timeout, and #f otherwise (code waited and event-queue became non-empty)
(defmethod event-queue-wait ((queue <event-queue>) &key timeout)
  (handler-case
      (trivial-timeout:with-timeout (timeout)
	(bordeaux-threads:with-lock-held ((associated-lock queue))
	  (loop while (empty? (%deque queue))
	     do (bordeaux-threads:condition-wait (%non-empty queue) (associated-lock queue)))))
    (trivial-timeout:timeout-error (c)
      (declare (ignore c))
      ;; If the queue is empty, we must have timed out
      (empty? (%deque queue)))))


#||
define sealed method event-queue-empty?
    (queue :: <event-queue>) => (true? :: <boolean>)
  // Don't bother locking
  empty?(queue.%deque)
end method event-queue-empty?;
||#

(defmethod event-queue-empty? ((queue <event-queue>))
  ;; Don't bother locking
  (empty? (%deque queue)))


#||
define sealed method event-queue-clear (queue :: <event-queue>) => ()
  with-lock (associated-lock(queue.%non-empty))
    queue.%deque.size := 0
  end
end method event-queue-clear;
||#

(defmethod event-queue-clear ((queue <event-queue>))
  (bordeaux-threads:with-lock-held ((associated-lock queue))
    (setf (size (%deque queue)) 0)))



