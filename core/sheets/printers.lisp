;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-SHEETS-INTERNALS -*-
(in-package #:duim-sheets-internals)

#||
define method print-object
    (_port :: <port>, stream :: <stream>) => ()
  printing-object (_port, stream, type?: #t, identity?: #t)
    format(stream, "%s", port-name(_port) | "")
  end
end method print-object;
||#

(defmethod print-object ((_port <port>) stream)
  (print-unreadable-object (_port stream :type t :identity t)
    (format stream "~s" (or (port-name _port) ""))))

#||
define method print-object
    (event :: <window-event>, stream :: <stream>) => ()
  printing-object (event, stream, type?: #t, identity?: #t)
    let (left, top, right, bottom) = box-edges(event-region(event));
    format(stream, "on %= from (%d,%d) to (%d,%d)",
	   event-sheet(event), left, top, right, bottom)
  end
end method print-object;
||#

(defmethod print-object ((event <window-event>) stream)
  (print-unreadable-object (event stream :type t :identity t)
    (multiple-value-bind (left top right bottom)
	(box-edges (event-region event))
      (format stream "on ~a from (~d,~d) to (~d,~d)"
	      (event-sheet event) left top right bottom))))

#||
define method print-object
    (event :: <pointer-boundary-event>, stream :: <stream>) => ()
  printing-object (event, stream, type?: #t, identity?: #t)
    format(stream, "on %= kind %=",
	   event-sheet(event), boundary-event-kind(event))
  end
end method print-object;
||#

(defmethod print-object ((event <pointer-boundary-event>) stream)
  (print-unreadable-object (event stream :type t :identity t)
    (format stream "on ~a kind ~a"
	    (event-sheet event) (boundary-event-kind event))))

#||
define method print-object
    (event :: <pointer-motion-event>, stream :: <stream>) => ()
  printing-object (event, stream, type?: #t, identity?: #t)
    format(stream, "on %= at (%d,%d)",
	   event-sheet(event), event-x(event), event-y(event))
  end
end method print-object;
||#

(defmethod print-object ((event <pointer-motion-event>) stream)
  (print-unreadable-object (event stream :type t :identity t)
    (format stream "on ~a at (~d,~d)"
	    (event-sheet event) (event-x event) (event-y event))))

#||
define method print-object
    (event :: <pointer-button-event>, stream :: <stream>) => ()
  printing-object (event, stream, type?: #t, identity?: #t)
    format(stream, "on %= with %= %=",
	   event-sheet(event), event-button(event), event-modifier-state(event))
  end
end method print-object;
||#

(defmethod print-object ((event <pointer-button-event>) stream)
  (print-unreadable-object (event stream :type t :identity t)
    (format stream "on ~a with ~a ~a"
	    (event-sheet event) (event-button event) (event-modifier-state event))))

#||
define method print-object
    (event :: <keyboard-event>, stream :: <stream>) => ()
  printing-object (event, stream, type?: #t, identity?: #t)
    format(stream, "on %= with %= (%=) %=",
	   event-sheet(event),
	   event-key-name(event), event-character(event), event-modifier-state(event))
  end
end method print-object;
||#

(defmethod print-object ((event <keyboard-event>) stream)
  (print-unreadable-object (event stream :type t :identity t)
    (format stream "on ~a with ~a (~a) ~a"
	    (event-sheet event)
	    (event-key-name event) (event-character event) (event-modifier-state event))))

;;;(defmethod print-object ((sheet <multiple-child-mixin>) stream)
;;;  (print-unreadable-object (sheet stream :type t :identity t)
;;;    (multiple-value-bind (left top right bottom)
;;;        (sheet-edges sheet)
;;;      (multiple-value-bind (mxx mxy myx myy tx ty)
;;;          (transform-components (sheet-transform sheet))
;;;        (declare (ignore mxx mxy myx myy))
;;;        (format stream "(~a,~a):(~a,~a) with transform (~a,~a), children ~a"
;;;                left top right bottom
;;;                tx ty
;;;                (sheet-children sheet))))))

