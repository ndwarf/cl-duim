;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-SHEETS-INTERNALS -*-
(in-package :duim-sheets-internals)

#||
/// Carets

define protocol <<caret-protocol>> ()
  function make-caret
    (port :: <abstract-port>, sheet :: <abstract-sheet>, #key x, y, width, height)
 => (caret :: <caret>);
  getter caret-sheet
    (caret :: <caret>) => (sheet :: false-or(<abstract-sheet>));
  function caret-position
    (caret :: <caret>) => (x :: <integer>, y :: <integer>);
  function set-caret-position
    (caret :: <caret>, x :: <integer>, y :: <integer>, #key fast?) => ();
  function do-set-caret-position
    (caret :: <caret>, x :: <integer>, y :: <integer>) => ();
  function caret-size
    (caret :: <caret>) => (width :: <integer>, height :: <integer>);
  function set-caret-size
    (caret :: <caret>, width :: <integer>, height :: <integer>) => ();
  function do-set-caret-size
    (caret :: <caret>, width :: <integer>, height :: <integer>) => ();
  getter caret-visible?
    (caret :: <caret>) => (visible? :: <boolean>);
  setter caret-visible?-setter
    (visible?, caret :: <caret>, #key tooltip?) => (visible?);
  function do-show-caret
    (caret :: <caret>, #key tooltip?) => ();
  function do-hide-caret
    (caret :: <caret>, #key tooltip?) => ();
end protocol <<caret-protocol>>;


(define-protocol <<caret-protocol>> ()
  (:function make-caret (port sheet &key x y width height))
  (:getter   caret-sheet (caret))
  (:function caret-position (carent))
  (:function set-caret-position (caret x y &key fast?))
  (:function do-set-caret-position (caret x y))
  (:function caret-size (caret))
  (:function set-caret-size (caret width height))
  (:function do-set-caret-size (caret width height))
  (:getter   caret-visible? (caret))
  (:setter   (setf caret-visible?) (visible? caret &key tooltip?))
  (:function do-show-caret (caret &key tooltip?))
  (:function do-hide-caret (caret &key tooltip?)))
||#


#||
define open abstract primary class <basic-caret> (<caret>)
  sealed slot port :: false-or(<port>) = #f,
    init-keyword: port:,
    setter: %port-setter;
  sealed slot caret-sheet :: false-or(<sheet>) = #f,
    init-keyword: sheet:;
  sealed slot %x-position :: <integer> = 0,
    init-keyword: x:;
  sealed slot %y-position :: <integer> = 0,
    init-keyword: y:;
  sealed slot %width  :: <integer> = 0,
    init-keyword: width:;
  sealed slot %height :: <integer> = 0,
    init-keyword: height:;
  // Incremented when we hide the caret, 0 => visible
  sealed slot %hide-count :: <integer> = 0;
end class <basic-caret>;
||#

(defclass <basic-caret> (<caret>)
  ((port :type (or null <port>)
	 :initarg :port :initform nil :reader port :writer %port-setter)
   (caret-sheet :type (or null <sheet>)
		:initarg :sheet :initform nil :accessor caret-sheet)
   (%x-position :type fixnum :initarg :x :initform 0 :accessor %x-position)
   (%y-position :type fixnum :initarg :y :initform 0 :accessor %y-position)
   (%width :type fixnum :initarg :width :initform 0 :accessor %width)
   (%height :type fixnum :initarg :height :initform 0 :accessor %height)
   (%hide-count :type fixnum :initform 0 :accessor %hide-count))
  (:metaclass <abstract-metaclass>))


#||
define method display
    (caret :: <basic-caret>) => (display :: false-or(<display>))
  let sheet = caret-sheet(caret);
  sheet & display(sheet)
end method display;
||#

(defmethod display ((caret <basic-caret>))
  (let ((sheet (caret-sheet caret)))
    (and sheet
         (display sheet))))


#||
// Returns X and Y in the coordinate system of the caret's sheet
define sealed inline method caret-position
    (caret :: <basic-caret>) => (x :: <integer>, y :: <integer>)
  values(caret.%x-position, caret.%y-position)
end method caret-position;
||#

(defmethod caret-position ((caret <basic-caret>))
  (values (%x-position caret) (%y-position caret)))


#||
define sealed method set-caret-position
    (caret :: <basic-caret>, x :: <integer>, y :: <integer>,
     #key fast? = #f) => ()
  unless (caret.%x-position   = x
          & caret.%y-position = y)
    unless (fast?)		// do no work if we're trying to be fast
      do-set-caret-position(caret, x, y)
    end;
    caret.%x-position := x;
    caret.%y-position := y
  end
end method set-caret-position;
||#

(defmethod set-caret-position ((caret <basic-caret>) (x integer) (y integer) &key (fast? nil))
  (unless (and (= (%x-position caret) x)
               (= (%y-position caret) y))
    (unless fast?     ;; do no work if we're trying to be fast
      (do-set-caret-position caret x y))
    (setf (%x-position caret) x)
    (setf (%y-position caret) y)))


#||
// Returns X and Y in the coordinate system of the caret's sheet
define sealed inline method caret-size
    (caret :: <basic-caret>) => (width :: <integer>, height :: <integer>)
  values(caret.%width, caret.%height)
end method caret-size;
||#

(defmethod caret-size ((caret <basic-caret>))
  (values (%width caret) (%height caret)))


#||
define sealed method set-caret-size
    (caret :: <basic-caret>, width :: <integer>, height :: <integer>) => ()
  unless (caret.%width    = width
          & caret.%height = height)
    do-set-caret-size(caret, width, height);
    caret.%width  := width;
    caret.%height := height
  end
end method set-caret-size;
||#

(defmethod set-caret-size ((caret <basic-caret>) (width integer) (height integer))
  (unless (and (= (%width caret) width)
               (= (%height caret) height))
    (do-set-caret-size caret width height)
    (setf (%width caret) width)
    (setf (%height caret) height)))


#||
define sealed inline method caret-visible?
    (caret :: <basic-caret>) => (visible? :: <boolean>)
  caret.%hide-count = 0
end method caret-visible?;
||#

(defmethod caret-visible? ((caret <basic-caret>))
  (= (%hide-count caret) 0))


#||
define sealed method caret-visible?-setter
    (visible? :: <boolean>, caret :: <basic-caret>, #key tooltip?)
 => (visible? :: <boolean>)
  if (visible?)
    unless (caret.%hide-count = 0)	// don't let it go negative
      dec!(caret.%hide-count)
    end;
    do-show-caret(caret, tooltip?: tooltip?)
  else
    inc!(caret.%hide-count);
    do-hide-caret(caret, tooltip?: tooltip?)
  end;
  visible?
end method caret-visible?-setter;
||#

(defmethod (setf caret-visible?) (visible? (caret <basic-caret>) &key tooltip?)
  (cond (visible?
         (unless (= (%hide-count caret) 0)    ;; don't let it go negative
	   (decf (%hide-count caret)))
         (do-show-caret caret :tooltip? tooltip?))
        (t
	 (incf (%hide-count caret))
         (do-hide-caret caret :tooltip? tooltip?)))
  visible?)



#||
/// Carets and sheets

define macro with-caret-position-saved
  { with-caret-position-saved (?sheet:expression)
      ?:body
    end }
    => { begin
	   let _caret = sheet-caret(?sheet);
	   let (_x, _y) = caret?(_caret) & caret-position(_caret);
	   block ()
	     ?body
	   cleanup
	     when (caret?(_caret))
	       set-caret-position(_caret, _x, _y)
	     end;
	   end
         end }
end macro with-caret-position-saved;
||#

(defmacro with-caret-position-saved ((sheet) &body body)
  (let ((_caret (gensym "CARET-"))
	(_x (gensym "X-"))
	(_y (gensym "Y-")))
  `(let ((,_caret (sheet-caret ,sheet)))
    (multiple-value-bind (,_x ,_y)
        (and (caretp ,_caret)
             (caret-position ,_caret))
      (unwind-protect
           (progn
             ,@body)
        (when (caretp ,_caret)
          (set-caret-position ,_caret ,_x ,_y)))))))


#||
define macro with-caret-hidden
  { with-caret-hidden (?sheet:expression)
      ?:body
    end }
    => { begin
	   let _caret = sheet-caret(?sheet);
	   block ()
	     when (caret?(_caret))
	       caret-visible?(_caret, tooltip?: #t) := #f
	     end;
	     ?body
	   cleanup
	     when (caret?(_caret))
	       caret-visible?(_caret, tooltip?: #t) := #t
	     end;
	   end
         end }
end macro with-caret-hidden;
||#

(defmacro with-caret-hidden ((sheet) &body body)
  (let ((_caret (gensym "CARET-")))
    `(let ((,_caret (sheet-caret ,sheet)))
      (unwind-protect
           (progn
             (when (caretp ,_caret)
               (setf (caret-visible? ,_caret :tooltip? t) nil))
             ,@body)
        (when (caretp ,_caret)
          (setf (caret-visible? ,_caret :tooltip? t) t))))))

