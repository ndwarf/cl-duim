;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-SHEETS-INTERNALS -*-
(in-package #:duim-sheets-internals)

#||
/// Pointers (aka, the mouse)

define protocol <<pointer-protocol>> ()
  getter pointer-sheet
    (pointer :: <pointer>) => (sheet :: false-or(<abstract-sheet>));
  getter pointer-button-state
    (pointer :: <pointer>) => (state :: <integer>);
  function pointer-position
    (pointer :: <pointer>, #key sheet) => (x :: <integer>, y :: <integer>);
  function do-pointer-position
    (port :: <abstract-port>, pointer :: <pointer>, sheet :: <abstract-sheet>)
 => (x :: <integer>, y :: <integer>);
  function set-pointer-position
    (pointer :: <pointer>, x :: <integer>, y :: <integer>, #key sheet) => ();
  function do-set-pointer-position 
    (port :: <abstract-port>, pointer :: <pointer>, sheet :: <abstract-sheet>,
     x :: <integer>, y :: <integer>) => ();
  getter pointer-cursor
    (pointer :: <pointer>) => (cursor :: <cursor>);
  setter pointer-cursor-setter
    (cursor :: <cursor>, pointer :: <pointer>) => (cursor :: <cursor>);
  function do-set-pointer-cursor
    (port :: <abstract-port>, pointer :: <pointer>, cursor :: <cursor>) => ();
  function do-set-sheet-cursor
    (port :: <abstract-port>, sheet :: <sheet>, cursor :: <cursor>) => ();
  getter pointer-grabbed?
    (pointer :: <pointer>)
 => (sheet :: false-or(<abstract-sheet>));
  getter pointer-grabbed?-setter
    (sheet :: false-or(<abstract-sheet>), pointer :: <pointer>)
 => (sheet :: false-or(<abstract-sheet>));
  function grab-pointer
    (port :: <abstract-port>, pointer :: <pointer>, sheet :: <abstract-sheet>)
 => (success? :: <boolean>);
  function ungrab-pointer
    (port :: <abstract-port>, pointer :: <pointer>)
 => (success? :: <boolean>);
  function do-with-pointer-grabbed
    (port :: <abstract-port>, sheet :: <sheet>, continuation :: <function>)
 => (#rest values);
end protocol <<pointer-protocol>>;


(define-protocol <<pointer-protocol>> ()
  (:getter pointer-sheet (pointer))
  (:getter pointer-button-state (pointer))
  (:function pointer-position (pointer &key sheet))
  (:function do-pointer-position (port pointer sheet))
  (:function set-pointer-position (pointer x y &key sheet))
  (:function do-set-pointer-position (port pointer sheet x y))
  (:getter pointer-cursor (pointer))
  (:setter (setf pointer-cursor) (cursor pointer))
  (:function do-set-pointer-cursor (port pointer cursor))
  (:function do-set-sheet-cursor (port sheet cursor))
  (:getter pointer-grabbed? (pointer))
  (:setter (setf pointer-grabbed?) (sheet pointer))
  (:function grab-pointer (port pointer sheet))
  (:function ungrab-pointer (port pointer))
  (:function do-with-pointer-grabbed (port sheet continuation)))
||#


#||
define sealed class <standard-pointer> (<pointer>)
  sealed slot port :: false-or(<port>) = #f,
    init-keyword: port:,
    setter: %port-setter;
  sealed slot pointer-sheet :: false-or(<sheet>) = #f;
  // The pointer button state gets maintained by each back-end
  sealed slot pointer-button-state :: <integer> = 0;
  sealed slot %x-position :: <integer> = 0;
  sealed slot %y-position :: <integer> = 0;
  sealed slot %position-changed? :: <boolean> = #f;
  sealed slot pointer-cursor :: <cursor> = #"default",
    setter: %cursor-setter;
  sealed slot pointer-grabbed? :: false-or(<sheet>) = #f,
    setter: %grabbed?-setter;
end class <standard-pointer>;
||#

(defclass <standard-pointer> (<pointer>)
  ((port :type (or null <port>) :initarg :port :initform nil :reader port :writer %port-setter)
   (pointer-sheet :type (or null <sheet>) :initform nil :accessor pointer-sheet)
   ;; The pointer button state gets maintained by each back-end
   ;; FIXME: BEST DO SOMETHING WITH THIS!
   (pointer-button-state :type integer :initform 0 :accessor pointer-button-state)
   (%x-position :type integer :initform 0 :accessor %x-position)
   (%y-position :type integer :initform 0 :accessor %y-position)
   (%position-changed? :initform nil :accessor %position-changed?)
   (pointer-cursor :type <cursor> :initform :default :reader pointer-cursor :writer %cursor-setter)
   (pointer-grabbed? :type (or null <sheet>) :initform nil :reader pointer-grabbed? :writer %grabbed?-setter)))


#||
define sealed domain make (singleton(<standard-pointer>));
define sealed domain initialize (<standard-pointer>);

define sealed inline method make
    (class == <pointer>, #key port, display)
 => (pointer :: <standard-pointer>)
  ignore(display);
  make(<standard-pointer>, port: port)
end method make;
||#

(defun make-pointer (&key port display)
  (make-instance '<standard-pointer> :port port))


#||
define method display
    (pointer :: <standard-pointer>) => (display :: false-or(<display>))
  let sheet = pointer-sheet(pointer);
  sheet & display(sheet)
end method display;
||#

(defmethod display ((pointer <standard-pointer>))
  (let ((sheet (pointer-sheet pointer)))
    (and sheet (display sheet))))


#||
define sealed method pointer-cursor-setter
    (cursor :: <cursor>, pointer :: <standard-pointer>) => (cursor :: <cursor>)
  unless (pointer-cursor(pointer) == cursor)
    do-set-pointer-cursor(port(pointer), pointer, cursor);
    pointer.%cursor := cursor
  end;
  cursor
end method pointer-cursor-setter;
||#

(defmethod (setf pointer-cursor) ((cursor symbol) (pointer <standard-pointer>))
  (unless (eql (pointer-cursor pointer) cursor)
    (do-set-pointer-cursor (port pointer) pointer cursor)
    (%cursor-setter cursor pointer))
  cursor)

(defmethod (setf pointer-cursor) ((cursor <image>) (pointer <standard-pointer>))
  (unless (eql (pointer-cursor pointer) cursor)
    (do-set-pointer-cursor (port pointer) pointer cursor)
    (%cursor-setter cursor pointer))
  cursor)


#||
define method do-set-sheet-cursor
    (_port :: <port>, sheet :: <sheet>, cursor :: <cursor>) => ()
  ignore(sheet, cursor);
  #f
end method do-set-sheet-cursor;
||#

(defmethod do-set-sheet-cursor ((_port <port>) (sheet <sheet>) (cursor symbol))
  (declare (ignore sheet cursor))
  nil)

(defmethod do-set-sheet-cursor ((_port <port>) (sheet <sheet>) (cursor <image>))
  (declare (ignore sheet cursor))
  nil)


#||
define method update-pointer-cursor
    (pointer :: <pointer>,
     #key sheet = pointer-sheet(pointer),
          frame = (sheet & sheet-frame(sheet)))
 => ()
  pointer-cursor(pointer)
    := (frame & frame-cursor-override(frame))
         | (sheet & sheet-cursor(sheet))
         | #"default"
end method update-pointer-cursor;
||#

(defmethod update-pointer-cursor ((pointer <pointer>)
                                  &key
				  (sheet (pointer-sheet pointer))
                                  (frame (and sheet (sheet-frame sheet))))
  (setf (pointer-cursor pointer)
        (or (and frame (frame-cursor-override frame))
            (and sheet (sheet-cursor sheet))
            :default)))


#||
// This is robust against ungrafted sheets and umapped frames,
// because 'with-busy-cursor' often gets used in initialization code...
define macro with-busy-cursor
  { with-busy-cursor (?frame:expression, ?cursor:expression) ?:body end }
    => { begin
	   let with-busy-cursor-body = method () ?body end;
	   do-with-busy-cursor(?frame, ?cursor, with-busy-cursor-body)
         end }
  { with-busy-cursor (?frame:expression) ?:body end }
    => { begin
	   let with-busy-cursor-body = method () ?body end;
	   do-with-busy-cursor(?frame, #"busy", with-busy-cursor-body)
         end }
end macro with-busy-cursor;
||#


;; This is robust against ungrafted sheets and unmapped frames,
;; because 'with-busy-cursor' often gets used in initialization code...
(defmacro with-busy-cursor ((frame &optional cursor) &body body)
  (let ((with-busy-cursor-body (gensym)))
    `(let ((,with-busy-cursor-body #'(lambda ()
                                       ,@body)))
      (do-with-busy-cursor ,frame
        ,(if (null cursor) :busy cursor)
        ,with-busy-cursor-body))))


#||
define method do-with-busy-cursor
    (sheet :: <sheet>, cursor, continuation :: <function>) => (#rest values)
  let frame = sheet-frame(sheet);
  if (frame)
    do-with-busy-cursor(frame, cursor, continuation)
  else
    continuation()		// sheet is not grafted
  end
end method do-with-busy-cursor;
||#

(defmethod do-with-busy-cursor ((sheet <sheet>) cursor continuation)
  (let ((frame (sheet-frame sheet)))
    (if frame
        (do-with-busy-cursor frame cursor continuation)
        (funcall continuation))))  ;; sheet is not grafted


#||
define method do-with-busy-cursor
    (frame :: <frame>, cursor, continuation :: <function>) => (#rest values)
  let old-cursor = frame-cursor-override(frame);
  block ()
    frame-cursor-override(frame) := cursor;
    continuation()
  cleanup
    frame-cursor-override(frame) := old-cursor
  end
end method do-with-busy-cursor;
||#

(defmethod do-with-busy-cursor ((frame <frame>) cursor continuation)
  (let ((old-cursor (frame-cursor-override frame)))
    (unwind-protect
         (progn
           (setf (frame-cursor-override frame) cursor)
           (funcall continuation))
      (setf (frame-cursor-override frame) old-cursor))))


#||
// Returns X and Y in sheet's coordinate system
define sealed method pointer-position
    (pointer :: <standard-pointer>, #key sheet)
 => (x :: <integer>, x :: <integer>)
  if (sheet & ~(sheet == pointer-sheet(pointer)))
    // He's asking for the position w.r.t. another sheet,
    // so we have to consult the port directly
    do-pointer-position(port(pointer), pointer, sheet)
  else
    values(pointer.%x-position, pointer.%y-position)
  end
end method pointer-position;
||#

;; Returns X and Y in sheet's coordinate system
(defmethod pointer-position ((pointer <standard-pointer>) &key sheet)
  (if (and sheet (not (eql sheet (pointer-sheet pointer))))
      ;; He's asking for the position w.r.t. another sheet,
      ;; so we have to consult the port directly
      (do-pointer-position (port pointer) pointer sheet)
      ;; else
      (values (%x-position pointer) (%y-position pointer))))


#||
// X and Y are in the sheet's coordinate system
define sealed method set-pointer-position
    (pointer :: <standard-pointer>, x :: <integer>, y :: <integer>, #key sheet) => ()
  pointer.%x-position := x;
  pointer.%y-position := y;
  pointer.%position-changed? := #t;
  when (sheet & ~(sheet == pointer-sheet(pointer)))
    do-set-pointer-position(port(pointer), pointer, sheet, x, y)
  end
end method set-pointer-position;
||#

;; X and Y are in the sheet's coordinate system
(defmethod set-pointer-position ((pointer <standard-pointer>) (x integer) (y integer) &key sheet)
  (setf (%x-position pointer) x)
  (setf (%y-position pointer) y)
  (setf (%position-changed? pointer) t)
  (when (and sheet (not (eql sheet (pointer-sheet pointer))))
    (do-set-pointer-position (port pointer) pointer sheet x y)))


#||
define method pointer-state-changed? 
    (pointer :: <standard-pointer>, old-sheet, old-x, old-y)
 => (changed? :: <boolean>, sheet :: <sheet>, x :: <integer>, x :: <integer>)
  let sheet = pointer-sheet(pointer);
  let (x-position, y-position) = pointer-position(pointer, sheet: sheet);
  values(~(sheet == old-sheet)
	 // Compare coordinates with eql, not =, because null values can be passed in
	 | ~(old-x == x-position)
	 | ~(old-y == y-position),
	 sheet, x-position, y-position)
end method pointer-state-changed?;
||#

(defmethod pointer-state-changed? ((pointer <standard-pointer>) old-sheet old-x old-y)
  (let ((sheet (pointer-sheet pointer)))
    (multiple-value-bind (x-position y-position)
        (pointer-position pointer :sheet sheet)
      (values (or (not (eql sheet old-sheet))
		  ;; Compare coordinates with eql, not =, because null values can be passed in
		  (not (eql old-x x-position))
                  (not (eql old-y y-position)))
              sheet
              x-position
              y-position))))



#||
/// Pointer grabbing

//---*** What should we do if one thread tries to grab the pointer
//---*** when another thread already has the grab?  
define sealed method pointer-grabbed?-setter
    (sheet :: false-or(<sheet>), pointer :: <standard-pointer>)
 => (sheet :: false-or(<sheet>))
  let _port = port(pointer);
  unless (pointer-grabbed?(pointer) == sheet)
    if (sheet)
      let new-focus = sheet;
      let old-focus = port-input-focus(_port);
      when (grab-pointer(_port, pointer, sheet))
	when (new-focus ~== old-focus
	      & port-focus-policy(_port) == #"sheet-under-pointer"
	      & sheet-handles-keyboard?(new-focus))
	  port-input-focus(_port) := new-focus;
	end;
	pointer.%grabbed? := sheet
      end
    else
      when (ungrab-pointer(_port, pointer))
	pointer.%grabbed? := #f
      end
    end
  end;
  sheet
end method pointer-grabbed?-setter;
||#

;;---*** What should we do if one thread tries to grab the pointer
;;---*** when another thread already has the grab?
(defmethod (setf pointer-grabbed?) ((sheet <sheet>) (pointer <standard-pointer>))
  (let ((_port (port pointer)))
    (unless (eql (pointer-grabbed? pointer) sheet)
      (if sheet
          (let ((new-focus sheet)
                (old-focus (port-input-focus _port)))
            (when (grab-pointer _port pointer sheet)
              (when (and (not (eql old-focus new-focus))
                         (eql (port-focus-policy _port) :sheet-under-pointer)
                         (sheet-handles-keyboard? new-focus))
                (setf (port-input-focus _port) new-focus))
	      (%grabbed?-setter sheet pointer)))
	  ;; else
          (when (ungrab-pointer _port pointer)
	    (%grabbed?-setter nil pointer))))
    sheet))


#||
define method grab-pointer
    (_port :: <port>, pointer :: <pointer>, sheet :: <sheet>)
 => (success? :: <boolean>)
  #t
end method grab-pointer;
||#

(defmethod grab-pointer ((_port <port>) (pointer <pointer>) (sheet <sheet>))
  t)


#||
define method ungrab-pointer
    (_port :: <port>, pointer :: <pointer>)
 => (success? :: <boolean>)
  #t
end method ungrab-pointer;
||#

(defmethod ungrab-pointer ((_port <port>) (pointer <pointer>))
  t)


#||
define macro with-pointer-grabbed
  { with-pointer-grabbed (?sheet:name) ?:body end }
    => { begin
           let with-pointer-grabbed-body = method () ?body end;
	   let _port = port(?sheet);
	   do-with-pointer-grabbed(_port, ?sheet, with-pointer-grabbed-body)
         end }
end macro with-pointer-grabbed;
||#

(defmacro with-pointer-grabbed ((sheet) &body body)
"
Executes a body of code, forwarding all pointer events to _sheet_,
even if the pointer leaves the sheet-region of _sheet_. The _sheet_
specified should be an instance of type <sheet>.

The macro calls methods for 'do-with-pointer-grabbed'. The code
specified by _body_ is used to create a stand-alone method that is
used as the code that is run by 'do-with-pointer-grabbed'.
"
   (let ((_port (gensym "PORT-"))
	 (_sheet (gensym "SHEET-"))
         (with-pointer-grabbed-body (gensym)))
     `(let* ((,with-pointer-grabbed-body #'(lambda ()
                                             ,@body))
             (,_sheet ,sheet)
             (,_port (port ,_sheet)))
       (do-with-pointer-grabbed ,_port ,_sheet ,with-pointer-grabbed-body))))


#||
define sealed method do-with-pointer-grabbed
    (_port :: <port>, sheet :: <sheet>, continuation :: <function>)
 => (#rest values)
  let pointer = port-pointer(_port);
  block ()
    pointer-grabbed?(pointer) := sheet;
    continuation()
  cleanup
    pointer-grabbed?(pointer) := #f;
  end
end method do-with-pointer-grabbed;
||#

(defmethod do-with-pointer-grabbed ((_port <port>) (sheet <sheet>) continuation)
  (let ((pointer (port-pointer _port)))
    (unwind-protect
         (progn
           (setf (pointer-grabbed? pointer) sheet)
           (funcall continuation))
      (setf (pointer-grabbed? pointer) nil))))

