;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-SHEETS-INTERNALS -*-
(in-package :duim-sheets-internals)

#||
/// Frame Managers

//--- Should these be added to the display instead of the port?
define open abstract primary class <basic-frame-manager> (<frame-manager>)
  sealed slot port :: false-or(<port>) = #f,
    init-keyword: port:,
    setter: %port-setter;
  sealed slot frame-manager-frames  :: <stretchy-object-vector> = make(<stretchy-vector>);
  sealed slot frame-manager-palette :: false-or(<palette>) = #f,
    init-keyword: palette:;
end class <basic-frame-manager>;
||#

(defgeneric frame-manager-frames (framem)
  (:documentation
"
Returns the frames managed by _framem_.
"))

(defgeneric frame-manager-palette (framem)
  (:documentation
"
Returns the palette used by _framem_.
"))

(defgeneric (setf frame-manager-palette) (palette framem)
  (:documentation
"
Sets the palette used by _framem_.
"))

(defclass <basic-frame-manager> (<frame-manager>)
  ((port :type (or null <port>) :initarg :port :initform nil :reader port :writer %port-setter)
   (frame-manager-frames :type list :initform (list) :accessor frame-manager-frames)
   (frame-manager-palette :type (or null <palette>) :initarg :palette :initform nil :accessor frame-manager-palette))
  (:metaclass <abstract-metaclass>))


#||
define sealed class <portable-frame-manager> (<basic-frame-manager>)
end class <portable-frame-manager>;
||#

(defclass <portable-frame-manager> (<basic-frame-manager>) ())


#||
//---*** This is most likely wrong, since it forces a 1-to-1 mapping
//---*** between ports and displays
define method display (framem :: <basic-frame-manager>) => (display :: false-or(<display>))
  let displays = port-displays(port(framem));
  ~empty?(displays) & displays[0]
end method display;
||#

;;---*** This is most likely wrong, since it forces a 1-to-1 mapping
;;---*** between ports and displays
(defmethod display ((framem <basic-frame-manager>))
  (let ((displays (port-displays (port framem))))
    (and (not (empty? displays)) (first displays))))


#||
define method initialize (framem :: <frame-manager>, #key)
  next-method();
  unless (frame-manager-palette(framem))
    frame-manager-palette(framem) := port-default-palette(port(framem))
  end
end method initialize;
||#

(defmethod initialize-instance :after ((framem <frame-manager>) &key &allow-other-keys)
  (unless (frame-manager-palette framem)
    (setf (frame-manager-palette framem)
	  (port-default-palette (port framem)))))


#||
define method find-color
    (name, framem :: <basic-frame-manager>, #key error? = #t) => (color :: <color>)
  find-color(name, frame-manager-palette(framem), error?: error?)
end method find-color;
||#

(defmethod find-color (name (framem <basic-frame-manager>) &key (error? t))
  (find-color name (frame-manager-palette framem) :error? error?))



#||
/// Frame manager creation

define method find-frame-manager
    (#rest options,
     #key port: _port, server-path, class, palette, #all-keys)
 => (framem :: <frame-manager>)
  dynamic-extent(options);
  block (return)
    with-keywords-removed (new-options = options, #[port:, server-path:, palette:])
      unless (_port)
	_port := default-port(server-path: server-path)
      end;
      unless (palette)
        palette := port-default-palette(_port)
      end;
      // Don't call 'port-default-frame-manager', because that can
      // get us into a loop...
      let default-framem
        = begin
	    let framems = port-frame-managers(_port);
	    case
	      empty?(framems) => #f;
	      ~class => framems[0];
	      otherwise => find-value(framems, method (f) object-class(f) == class end);
	    end
	  end;
      case
        // 'find-frame-manager' -> default one
	default-framem & empty?(options) =>
	  default-framem;
	// We specified a port, so make sure the default framem matches it
	default-framem
	& apply(frame-manager-matches-options?, default-framem, _port, new-options) =>
	  default-framem;
        // No default, look for one in the port, or create a new one;
        otherwise =>
          for (framem in port-frame-managers(_port))
            when (apply(frame-manager-matches-options?, framem, _port,
                        palette: palette, new-options))
              return(framem)
            end;
          end;
          let framem
            = apply(make-frame-manager, _port,
		    palette: palette, new-options);
          add!(port-frame-managers(_port), framem);
          framem
      end
    end
  end
end method find-frame-manager;
||#

(defmethod find-frame-manager (&rest options &key port server-path class palette &allow-other-keys)
  (declare (dynamic-extent options))
  (with-keywords-removed (new-options = options (:port :server-path :palette))
    (unless port
      (setf port (default-port :server-path server-path)))
    (unless palette
      (setf palette (port-default-palette port)))
    ;; Don't call 'port-default-frame-manager', because that can
    ;; get us into a loop...
    (let ((default-framem
	    (let ((framems (port-frame-managers port)))
	      (cond ((empty? framems) nil)
		    ((not class) (first framems))
		    (t
		     (find-if #'(lambda (f)
				  (eql (class-of f) class))
			      framems))))))
      ;; 'find-frame-manager' -> default one
      (cond ((and default-framem (empty? options)) default-framem)
	    ;; We specified a port, so make sure the default framem matches it
	    ((and default-framem
		  (apply #'frame-manager-matches-options? default-framem port new-options))
	     default-framem)
	    ;; No default, look for one in the port, or create a new one;
	    (t
	     (loop for framem in (port-frame-managers port)
		   do (when (apply #'frame-manager-matches-options? framem port :palette palette new-options)
			(return-from find-frame-manager framem)))
	     (let ((framem (apply #'make-frame-manager port :palette palette new-options)))
	       (setf (port-frame-managers port) (cons framem (port-frame-managers port)))
	       framem))))))


#||
define open generic make-frame-manager
    (port :: <port>, #key class, palette, #all-keys)
 => (framem :: <frame-manager>);
||#

(defgeneric make-frame-manager (port &key class palette &allow-other-keys)
  (:documentation
"
Returns an instance of <frame-manager> on _port_. If specified, the
palette described by _palette_ is used.
"))


#||
define method make-frame-manager
    (_port :: <port>,
     #key palette, class = <portable-frame-manager>, #all-keys)
 => (framem :: <frame-manager>)
  make(class, port: _port, palette: palette)
end method make-frame-manager;
||#

(defmethod make-frame-manager ((port <port>) &key palette (class (find-class '<portable-frame-manager>)) &allow-other-keys)
  (make-instance class :port port :palette palette))


#||
define sealed inline method make
    (class == <frame-manager>, #rest initargs, #key port: _port, #all-keys)
 => (framem :: <frame-manager>)
  dynamic-extent(initargs);
  apply(make-frame-manager, _port, initargs)
end method make;


define method frame-manager-matches-options?
    (framem :: <frame-manager>, _port, #key palette, class, #all-keys)
 => (true? :: <boolean>)
  ignore(palette);
  port(framem) == _port
  & (~class | object-class(framem) == class)
end method frame-manager-matches-options?;
||#

(defmethod frame-manager-matches-options? ((framem <frame-manager>) port &key palette class &allow-other-keys)
  (declare (ignore palette))
  (and (eql (port framem) port)
       (or (not class) (eql (class-of framem) class))))


#||
define method destroy-frame-manager (framem :: <frame-manager>) => ()
  while (~empty?(frame-manager-frames(framem)))
    destroy-frame(frame-manager-frames(framem)[0])
  end;
  let _port = port(framem);
  remove!(port-frame-managers(_port), framem)
end method destroy-frame-manager;
||#

(defmethod destroy-frame-manager ((framem <frame-manager>))
  (loop while (not (empty? (frame-manager-frames framem)))
	do (destroy-frame (first (frame-manager-frames framem))))
  (let ((_port (port framem)))
    (setf (port-frame-managers _port)
	  (delete-if #'(lambda (x)
		   (eql x framem))
	       (port-frame-managers _port)))))



#||
/// Frame protocol

//--- These are all forward references into 'duim-frames'

// The current application frame in this thread
define thread variable *current-frame* = #f;
||#

(define-thread-variable *current-frame* nil)


#||
define inline function current-frame () *current-frame* end;
||#

(defun current-frame ()
  *current-frame*)


#||
define open generic destroy-frame (frame :: <abstract-frame>) => ();
define open generic frame-input-focus
    (frame :: <abstract-frame>) => (sheet :: false-or(<abstract-sheet>));
define open generic frame-input-focus-setter
    (sheet :: false-or(<abstract-sheet>), frame :: <abstract-frame>)
 => (sheet :: false-or(<abstract-sheet>));
define open generic frame-cursor-override
    (frame :: <abstract-frame>) => (cursor :: false-or(<cursor>));
define open generic frame-cursor-override-setter
    (cursor :: false-or(<cursor>), frame :: <abstract-frame>)
 => (cursor :: false-or(<cursor>));
||#

(defgeneric destroy-frame (frame)
  (:documentation
"
Unmaps _frame_ from the screen and destroys it. Generally, you should
not need to call this function explicitly, since 'exit-frame' performs
all the necessary operations in the correct order, including calling
'destroy-frame' if the _destroy?_ argument to 'exit-frame' is true.
"))

(defgeneric frame-input-focus (frame)
  (:documentation
"
Returns the sheet in _frame_ that has the input focus.
"))

(defgeneric (setf frame-input-focus) (sheet frame)
  (:documentation
"
Sets which sheet in _frame_ has the input focus.
"))

(defgeneric frame-cursor-override (frame))
(defgeneric (setf frame-cursor-override) (cursor frame))


#||
define function do-frames
    (function :: <function>,
     #key port: _port, frame-manager: framem, z-order :: <z-order> = #f) => ()
  dynamic-extent(function);
  local method do-port-frames (_port :: <port>) => ()
	  for (framem in port-frame-managers(_port))
	    frame-manager-do-frames(function, framem, z-order: z-order)
	  end
	end method;
  case
    framem =>
      // Frame manager specified, so map over all the frames for
      // just this frame manager
      frame-manager-do-frames(function, framem, z-order: z-order);
    _port =>
      // Port specified, map over all of the frames for all of the
      // frame managers on the port
      do-port-frames(_port);
    otherwise =>
      // Map over all of the port...
      for (_port in *ports*)
	do-port-frames(_port)
      end;
  end
end function do-frames;
||#

(defun do-frames (function &key ((:port _port)) ((:frame-manager framem)) (z-order nil))
"
Runs a function on all the frames managed by a given frame manager. By
default, the current frame manager on the current port is used, unless
_port_ or _frame-manager_ are specified.
"
  (declare (dynamic-extent function))
  (labels ((do-port-frames (_port)
	       (loop for framem in (port-frame-managers _port)
                     do (frame-manager-do-frames function framem :z-order z-order))))
    (cond (framem
	   ;; Frame manager specified, so map over all the frames for
	   ;; just this frame manager
	   (frame-manager-do-frames function framem :z-order z-order))
	  (_port
	   ;; Port specified, map over all of the frames for all of the
	   ;; frame managers on the port
	   (do-port-frames _port))
	  (t
	   ;; Map over all of the port...
	   (loop for _port in *ports*
		 do (do-port-frames _port))))))


#||
define open generic frame-manager-do-frames
    (function :: <function>, framem :: <frame-manager>, #key z-order :: <z-order>) => ();
||#

(defgeneric frame-manager-do-frames (function framem &key z-order))


#||
define method frame-manager-do-frames
    (function :: <function>, framem :: <frame-manager>,
     #key z-order :: <z-order> = #f) => ()
  ignore(z-order);
  // Copy the sequence of frame manager's frames in case the function
  // modifies the sequence, e.g., 'do-frames(exit-frame, port: _port)'
  let frames = copy-sequence(frame-manager-frames(framem));
  do(function, frames)
end method frame-manager-do-frames;
||#

(defmethod frame-manager-do-frames ((function function) (framem <frame-manager>) &key (z-order nil))
  (declare (ignore z-order))
  ;; Copy the sequence of frame manager's frames in case the function
  ;; modifies the sequence, e.g., 'do-frames(exit-frame, port: _port)'
  (let ((frames (copy-seq (frame-manager-frames framem))))
    (mapcar function frames)))



#||
/// Pane creation

// The current application frame in this thread
define thread variable *current-frame-manager* :: false-or(<frame-manager>) = #f;
||#

(define-thread-variable *current-frame-manager* nil)


#||
define inline function current-frame-manager () *current-frame-manager* end;
||#

(defun current-frame-manager ()
  *current-frame-manager*)


#||
// Here for compatibility and self-documentation...
define method make-pane 
    (pane-class :: <class>, #rest pane-options, #key, #all-keys)
 => (pane :: <sheet>)
  dynamic-extent(pane-options);
  apply(make, pane-class, pane-options)
end method make-pane;
||#

;; XXX: This catches the (MAKE-PANE 'symbol ...) case
(defmethod make-pane (pane-class-name &rest pane-options &key &allow-other-keys)
  (apply #'make-pane (find-class pane-class-name) pane-options))

(defmethod make-pane ((pane-class class) &rest pane-options &key &allow-other-keys)
  (declare (dynamic-extent pane-options))
  ;;---*** This needs to cater for both the "subclass(<sheet>)" and
  ;;---*** the "subclass(<gadget>)" case (both of which inherit from
  ;;---*** <abstract-sheet>).
  (unless (subtypep pane-class (find-class '<abstract-sheet>))
    (warn "Using MAKE-PANE to construct non-sheet type ~a" pane-class))
  (apply #'make-pane-1 pane-class pane-options))


#||
//--- If you change this method, change the one in gadgets/gadget-mixins
define method make
    (pane-class :: subclass(<sheet>),
     #rest pane-options,
     #key port, frame-manager: framem, #all-keys)
 => (pane :: <sheet>)
  dynamic-extent(pane-options);
  let framem = framem
               | *current-frame-manager*
               | port-default-frame-manager(port | default-port())
               | error("Can't find a frame manager to use with 'make-pane'");
  let (concrete-class, concrete-options)
    = apply(class-for-make-pane, framem, pane-class, pane-options);
  // If there's a mapping from the abstract pane class to a concrete pane
  // class, then use it.  Otherwise just try to create a class named by the
  // abstract pane class.
  if (concrete-class == pane-class)
    apply(next-method, pane-class,
	  frame-manager: framem, pane-options)
  else
    //---*** Unfortunately, this recursive call to make will call
    //---*** 'class-for-make-pane' again.  How to speed this up?
    apply(make, concrete-class,
	  frame-manager: framem,
	  concrete-options | pane-options)
  end
end method make;
||#

(defun make-pane-1 (pane-class
		    &rest pane-options
		    &key port ((:frame-manager framem))
		    &allow-other-keys)
  (declare (dynamic-extent pane-options))
  (let ((framem (or framem
		    *current-frame-manager*
		    (port-default-frame-manager (or port (default-port)))
		    (error "Can't find frame manager to use with 'make-pane'"))))
    (multiple-value-bind (concrete-class concrete-options)
	(apply #'class-for-make-pane framem pane-class pane-options)
      ;; If there's a mapping from the abstract pane class to a
      ;; concrete pane class, then use it. Otherwise just try to
      ;; create a class named by the abstract pane class.
      (cond
        ((eql concrete-class pane-class)
         (apply #'make-instance
                pane-class
		;; See HyperSpec 7.1.2, "Declaring the Validity of Initialization Arguments".
		;; Basically, MAKE-PANE is invoked for all pane types, even if they haven't
		;; yet been written. We can't possibly know what keyword args they will take,
		;; and we don't want to tread on the user's ability to define initialize-instance
		;; or other methods; so tell the object construction and allocation machinery
		;; to let any old keyword args through.
		:allow-other-keys t
                :frame-manager framem
                pane-options))
	;;---*** Unfortunately, this recursive call to make will call
	;;---*** 'class-for-make-pane' again.  How to speed this up?
	(t
	  (apply #'make-pane-1
                 concrete-class
		 :frame-manager framem
		 ;; Should this include both concrete options AND
		 ;; pane options, rather than one or the other?
                 (or concrete-options pane-options)))))))


#||
// Platform-specific back-ends must supply methods for:
//  - <top-level-sheet>
//  - <viewport>        [pretty sure this one is supplied by DUIM, actually]
//  - <scroll-bar>
//  - <push-button>
//  - <radio-button>
//  - <check-button>
//  - <menu>
//  - <menu-bar>
//  - <push-menu-button>
//  - <radio-menu-button>
//  - <check-menu-button>
//  - <slider>
//  - <list-box>
//  - <option-box>
//  - <text-field>
//  - <password-field>
//  - <text-editor>
//  - <splitter>
// The DUIM gadgets library provides methods for:
//  - <label>
//  - <separator>
//  - <tool-bar>
//  - <status-bar>
//  - <scroller>
//  - <push-box>
//  - <radio-box>
//  - <check-box>
//  - <push-menu-box>
//  - <radio-menu-box>
//  - <check-menu-box>
//  - <property-page>
// The DUIM layouts library provides methods for:
//  - <row-layout>	(default method supplied by DUIM core)
//  - <column-layout>	(default method supplied by DUIM core)
//  - <table-layout>	(default method supplied by DUIM core)
//  - <grid-layout>	(default method supplied by DUIM core)
//  - <pinboard-layout>	(default method supplied by DUIM core)
// The DUIM gadget panes library optionally provides methods for:
//  - <border>
//  - <spacing>
//  - <group-box>
//  - <progress-bar>
//  - <spin-box>
//  - <tab-control>
//  - <list-control>
//  - <table-control>
//  - <tree-control>
define open generic class-for-make-pane
    (framem :: <abstract-frame-manager>, pane-class,
     #rest pane-options, #key, #all-keys)
 => (class :: <class>, options :: false-or(<sequence>));
||#

(defgeneric class-for-make-pane (framem pane-class &rest pane-options &key &allow-other-keys))


#||
// Default method just uses the abstract pane class as the concrete pane class,
// e.g., <row-layout-pane> -> <row-layout-pane>
define method class-for-make-pane
    (framem :: <frame-manager>, pane-class :: <class>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(pane-class, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <frame-manager>) (pane-class class) &key)
  (values pane-class nil))



#||
/// Some high level "chooser" functions

define constant <notification-style>
    = one-of(#"information", #"question", #"warning",
	     #"error", #"serious-error", #"fatal-error");
||#

(deftype <notification-style> ()
  '(member :information :question :warning
           :error :serious-error :fatal-error))


#||
define constant <notification-exit-style>
    = one-of(#"ok", #"ok-cancel",
	     #"yes-no", #"yes-no-cancel");
||#

(deftype <notification-exit-style> ()
  '(member :ok :ok-cancel :yes-no :yes-no-cancel))


#||
// Returns #t on normal exit, or #f on "cancel"
// STYLE is one of #"error", #"warning", #"information", #"question", etc.
define method notify-user
    (message-string :: <string>, #rest options,
     #key frame, owner, title, documentation, exit-boxes, name,
          style :: <notification-style> = #"information",
          exit-style :: false-or(<notification-exit-style>) = #f,
          foreground, background, text-style)
 => (value :: <boolean>, exit-type)
  dynamic-extent(options);
  ignore(title, documentation, exit-boxes, name, exit-style,
	 foreground, background, text-style);
  let (framem, owner) = get-frame-manager-and-owner(frame, owner);
  with-keywords-removed (options = options, #[frame:, owner:])
    apply(do-notify-user, framem, owner, message-string, style, options)
  end
end method notify-user;
||#

;; Returns #t on normal exit, or #f on "cancel"
;; STYLE is one of #"error", #"warning", #"information", #"question", etc.
(defgeneric notify-user (message-string &rest options &key frame owner title documentation
                        exit-boxes name style exit-style foreground background text-style)
  (:documentation
"
creates and displays an alert dialog box with the specified
criteria. use this function as a way of easily displaying simple
messages to the user.

figure 5.3 simple output from notify-user

the _message-string_ is the message that is displayed in the
dialog. the arguments _frame_, _owner_, _title_, and _documentation_
let you specify different attributes for the dialog in the same way as
they can be specified for any other frame or dialog.

the _exit-boxes_ argument lets you specify the buttons that are
available in the dialog. if not supplied, then a single ok button is
used by default, unless the _style_ of the dialog is set to :question,
in which case, two buttons are created, to allow the user to
respond \"yes\" or \"no\".

the _style_ argument lets you specify the style of dialog that is
produced. the different styles available reflect the motif
specification for dialog box types (one of :information,
:question, :warning, :error, :serious-error, or :fatal-error).
depending on the style of dialog you choose, the appearance of the
dialog created may vary. for example, a different icon is commonly
used to distinguish between error, informational, and warning
messages.

the _foreground_, _background_, and _text-style_ arguments let you
specify foreground and background colors, and the font to use in the
message text.

returns t on normal exit, or nil on 'cancel'.
"))

(defmethod notify-user ((message-string string) &rest options
			&key frame owner title documentation exit-boxes name
			(style :information) (exit-style nil) foreground
                        background text-style)
  (declare (dynamic-extent options)
	   (ignore title documentation exit-boxes name exit-style
		   foreground background text-style))
  (multiple-value-bind (framem owner)
      (get-frame-manager-and-owner frame owner)
    (with-keywords-removed (options = options (:frame :owner))
      (apply #'do-notify-user framem owner message-string style options))))


#||
define open generic do-notify-user
    (framem :: <abstract-frame-manager>, owner :: <sheet>,
     message-string :: <string>, style :: <notification-style>,
     #key title, documentation, name, exit-style,
     #all-keys)
 => (value :: <boolean>, exit-type);
||#

(defgeneric do-notify-user (framem owner message-string style &key title documentation name exit-style &allow-other-keys))


#||
// ITEMS can be a sequence or a <menu>
define method choose-from-menu
    (items, #rest options,
     #key frame, owner,
	  title, value, default-item, label-key, value-key,
          width, height, foreground, background, text-style,
          multiple-sets?)
 => (value, success? :: <boolean>)
  dynamic-extent(options);
  ignore(title, label-key, value-key,
	 width, height, foreground, background, text-style, multiple-sets?);
  let (framem, owner) = get-frame-manager-and-owner(frame, owner);
  with-keywords-removed (options = options, #[frame:, owner:, default-item:, value:])
    apply(do-choose-from-menu, framem, owner, items,
	  value: value | default-item,
	  options)
  end
end method choose-from-menu;
||#

(defgeneric choose-from-menu (items &rest options
			      &key frame owner title value default-item label-key value-key width
			      height foreground background text-style multiple-sets?)
  (:documentation
"
Prompt the user to choose from a collection of items, using a pop-up
menu.This generic function is similar to 'choose-from-dialog'.

The function returns the value chosen by the user, and a boolean
value: #t if a value was chosen, #f if nothing was chosen.

At its most basic, 'choose-from-menu' can be passed a simple sequence
of items, as follows:

      (choose-from-menu #(1 2 3))

However, any of a large number of keywords can be supplied to specify
more clearly the menu that is created. A range of typical options can
be chosen: The _frame_ keyword specifies a frame whose top level sheet
becomes the owner of the menu. Alternatively, you can specify this top
level sheet explicitly using _owner_. The _title_ keyword lets you
choose a title for the dialog. By default, each of these values is #f.

In addition, 'choose-from-menu@ offers options similar to collection
gadgets, that can act upon the items specified. The 'default-item'
keyword lets you specify an item that is returned by default if no
value is chosen explicitly (thereby ensuring that 'success?' will
always be #t). You can also specify a _value-key_ or _label-key_ for
the items in the menu.

Finally, you can configure the appearance of the menu itself. The
_width_ and _height_ keywords let you set the size of the menu. The
_foreground_ and _background_ keywords let you set the text color and
the menu color respectively. The _text-style_ keyword lets you specify
a font to display the menu items.
"))

(defmethod choose-from-menu (items &rest options
			     &key frame owner title value default-item label-key value-key width
                             height foreground background text-style multiple-sets?)
  (declare (dynamic-extent options)
	   (ignore title label-key value-key
		   width height foreground background
		   text-style multiple-sets?))
  (multiple-value-bind (framem owner)
      (get-frame-manager-and-owner frame owner)
    (with-keywords-removed (options = options (:frame :owner :default-item :value))
      (apply #'do-choose-from-menu framem owner items :value (or value default-item) options))))


#||
define open generic do-choose-from-menu
    (framem :: <abstract-frame-manager>, owner :: <sheet>, items,
     #key title, value, label-key, value-key, 
          width, height, foreground, background, text-style,
	  multiple-sets?,
     #all-keys)
 => (value, success? :: <boolean>);
||#

(defgeneric do-choose-from-menu (framem owner items
				 &key
				 title value label-key value-key
				 width height foreground background
				 text-style multiple-sets?
				 &allow-other-keys))


#||
// ITEMS can be a sequence or a <menu>
define method choose-from-dialog
    (items, #rest options,
     #key frame, owner,
	  title, value, default-item, label-key, value-key,
          selection-mode = #"single", gadget-class, gadget-options,
          width, height, foreground, background, text-style)
 => (value, success? :: <boolean>,
     width :: false-or(<integer>), height :: false-or(<integer>))
  dynamic-extent(options);
  ignore(title, label-key, value-key,
	 selection-mode, gadget-class, gadget-options,
	 width, height, foreground, background, text-style);
  let (framem, owner) = get-frame-manager-and-owner(frame, owner);
  with-keywords-removed (options = options, #[frame:, owner:, default-item:, value:])
    apply(do-choose-from-dialog, framem, owner, items,
	  value: value | default-item,
	  options)
  end
end method choose-from-dialog;
||#

;; ITEMS can be a sequence or a <menu>
(defgeneric choose-from-dialog (items
				&rest options
				&key frame owner title value default-item
				label-key value-key selection-mode
				gadget-class gadget-options
				width height foreground background text-style)
  (:documentation
"
Prompt the user to choose from a collection of _items_, using a dialog
box. This generic function is similar to 'choose-from-menu'.

The function returns the values chosen by the user, and a boolean
value: #t if a value was chosen, #f if nothing was chosen. Unlike
'choose-from-menu', the user can choose several values if desired,
depending on the value of 'selection-mode', described below.

At its most basic, 'choose-from-dialog' can be passed a simple
sequence of items, as follows:

      (choose-from-dialog (range :from 1 :to 10))

However, any of a large number of keywords can be supplied to specify
more clearly the dialog that is created. A range of typical options
can be chosen: The _frame_ keyword specifies a frame whose top level
sheet becomes the owner of the menu. Alternatively, you can specify
this top level sheet explicitly using _owner_. The _title_ keyword
lets you choose a title for the dialog. By default, each of these
values is #f.

In addition, 'choose-from-dialog' offers options similar to collection
gadgets, that can act upon the items specified. The _default-item_
keyword lets you specify an item that is returned by default if no
value is chosen explicitly (thereby ensuring that _success?_ will
always be #t). You can also specify a _value-key_ or _label-key_ for
the items in the menu. The _selection-mode_ keyword is used to make
the dialog box single-selection (the user can only choose one value)
or multiple-selection (the user can return any number of values). The
default value of _selection-mode_ is :single. By
specifying :selection-mode :multiple, the user can choose several
values from the dialog box. The _gadget-class_ keyword lets you
specify which type of collection gadget is displayed in the dialog
box. This lets you, for example, display a list of check boxes or
radio boxes. Finally, _gadget-options_ let you specify a set of
options to be applied to the collection gadgets in the dialog box.

You can also configure the appearance of the menu itself. The _width_
and _height_ keywords let you set the size of the menu. The
_foreground_ and _background_ keywords let you set the text color and
the menu color respectively. The _text-style_ keyword lets you specify
a font to display the menu items.
"))

(defmethod choose-from-dialog (items &rest options
                              &key
			      frame owner
			      title value default-item
			      label-key value-key
			      (selection-mode :single)
			      gadget-class gadget-options
			      width height foreground background text-style)
  (declare (dynamic-extent options)
	   (ignore title label-key value-key
		   selection-mode gadget-class gadget-options
		   width height foreground background text-style))
  (multiple-value-bind (framem owner)
      (get-frame-manager-and-owner frame owner)
    (with-keywords-removed (options = options (:frame :owner :default-item :value))
      (apply #'do-choose-from-dialog framem owner items :value (or value default-item) options))))


#||
define open generic do-choose-from-dialog
    (framem :: <abstract-frame-manager>, owner :: <sheet>, items,
     #key title, value, label-key, value-key,
          selection-mode, gadget-class, gadget-options,
          width, height, foreground, background, text-style,
     #all-keys)
 => (value, success? :: <boolean>,
     width :: false-or(<integer>), height :: false-or(<integer>));
||#

(defgeneric do-choose-from-dialog (framem owner items
				   &key title value label-key value-key
				   selection-mode gadget-class gadget-options
				   width height foreground background
				   text-style
				   &allow-other-keys))


#||
// Returns the pathname (or pathnames!) on normal exit, or #f on "cancel"
// The 'filters' argument is a bit wierd; it takes the form:
//  #[#["Text files", "*.txt", "*.text"],
//    #["Dylan files", "*.dylan"],
//    ...]
// 'filter-index' is an index into the set of filters
define method choose-file
    (#rest options,
     #key frame, owner, title, documentation, exit-boxes,
          direction = #"input", if-exists = #"ask", if-does-not-exist = #"ask",
	  default, default-type, filters, default-filter, selection-mode = #"single")
 => (locator :: false-or(type-union(<string>, <sequence>)),
     filter :: false-or(<integer>))
  dynamic-extent(options);
  ignore(title, documentation, exit-boxes,
	 default, if-exists, if-does-not-exist,
	 default-type, filters, default-filter, selection-mode);
  let (framem, owner) = get-frame-manager-and-owner(frame, owner);
  with-keywords-removed (options = options, #[frame:, owner:])
    apply(do-choose-file, framem, owner, direction, options)
  end
end method choose-file;
||#

(defgeneric choose-file (&rest options
			 &key frame owner title documentation exit-boxes
			 direction if-exists if-does-not-exist
			 default default-type filters default-filter
			 selection-mode)
  (:documentation
"
Displays the built-in file dialog for the target platform, which
allows the user to choose a file from any of the local or networked
drives currently connected to the computer. The function returns the
name of the file chosen by the user.

Figure 5.2 Typical appearance of a choose-file dialog

If the _frame_ argument is specified, the top-level sheet of _frame_
becomes the owner of the dialog.

Alternatively, you can specify the owner directly using the _owner_
argument, which takes an instance of <sheet> as its value.

By default, both _frame_ and _owner_ are #f, meaning the dialog has no
owner. You should not specify both of these values.

If you wish, you can specify a _title_ for the dialog; this is
displayed in the title bar of the frame containing the dialog.

The _direction_ argument is used to specify whether the file chosen is
being opened (that is, information in the file is loaded into the
application) or saved to (that is, information in the application is
being saved to a file on disk).

The _filters_ argument lets you specify the file filters that should
be offered to the user in the dialog. These filters are typically
available in a drop-down list box, and let the user display only
certain types of file, such as text files. Each filter is described as
a sequence of strings:

    * The first string in the sequence is a description of the files
      that are displayed when this filter is chosen.
    * Each subsequent string is a regular expression that describes
      which files to display in the dialog. 

For example, to specify a filter that lets the user choose to display
either text files, HTML files, or Dylan source files, the following
sequence should be passed to the filters argument:

    #(#(\"Text files\" \"*.txt\" \"*.text\")
      #(\"HTML files\" \"*.htm\" \"*.html\")
      #(\"Dylan files\" \"*.dylan\"))

Here, text files are defined as any file with a filename suffix of
.txt or .text, HTML files have filenames with a suffix of either .htm
or .html, and Dylan files have filenames with a suffix of .dylan.

The _default_ argument is used to specify a default filename to pass
to the dialog. This is a convenient way to suggest a file in which
some information may be saved, or a file to be loaded into an
application.

Example

The following example illustrates how you can define a class of frame
that contains buttons to display both Open and Save As dialogs, using
the pre-built dialog classes for your target environment.

    (define-frame <open-save-dialog-frame> (<simple-frame>)
      (:pane open-file-button (frame)
        (make-pane '<menu-button>,
                   :label \"Open...\"
                   :documentation 
                   \"Example of standard file 'Open' dialog\"
                   :activate-callback
                   #'(lambda (button)
                       (let ((file (choose-file :direction :input
                                                :owner frame)))
                         (when file
                               (setf (frame-status-message frame)
                                     (format nil
                                             \"Opened file ~s\"
                                             file)))))))
      (:pane save-file-button (frame)
        (make-pane '<menu-button>
                   :label \"Save As...\"
                   :documentation
                   \"Example of standard file 'Save As' dialog\"
                   :activate-callback
                   #'(lambda (button)
                       (let ((file (choose-file :direction :output
                                                :owner frame)))
                         (when file
                               (setf (frame-status-message frame)
                                     (format nil
                                             \"Saved file as ~s\"
                                             file))))))))
"))

;; Returns the pathname (or pathnames!) on normal exit, or #f on "cancel"
;; The 'filters' argument is a bit weird; it takes the form:
;;  #[#["Text files", "*.txt", "*.text"],
;;    #["Dylan files", "*.dylan"],
;;    ...]
;; 'filter-index' is an index into the set of filters
(defmethod choose-file (&rest options
                        &key
			frame owner title documentation exit-boxes
			(direction :input) (if-exists :ask)
			(if-does-not-exist :ask)
			default default-type filters default-filter
			(selection-mode :single))

  ;; FIXME: what are valid directions and options to if-exists/if-does-not-exist?

  (declare (dynamic-extent options)
	   (ignore title documentation exit-boxes
		   default if-exists if-does-not-exist
		   default-type filters default-filter selection-mode))
  (multiple-value-bind (framem owner)
      (get-frame-manager-and-owner frame owner)
    (with-keywords-removed (options = options (:frame :owner))
      (apply #'do-choose-file framem owner direction options))))


#||
define open generic do-choose-file
    (framem :: <abstract-frame-manager>, owner :: <sheet>,
     direction :: one-of(#"input", #"output"),
     #key title, documentation, exit-boxes,
	  if-exists, if-does-not-exist,
	  default, default-type, filters, default-filter, selection-mode,
     #all-keys)
 => (locator :: false-or(type-union(<string>, <sequence>)),
     filter :: false-or(<integer>));
||#

(defgeneric do-choose-file (framem owner direction
			    &key
			    title documentation exit-boxes
			    if-exists if-does-not-exist
			    default default-type filters
			    default-filter selection-mode
			    &allow-other-keys))


#||
// Returns the pathname on normal exit, or #f on "cancel"
define method choose-directory
    (#rest options,
     #key frame, owner, title, documentation, exit-boxes, default)
 => (locator)
  dynamic-extent(options);
  ignore(title, documentation, exit-boxes, default);
  let (framem, owner) = get-frame-manager-and-owner(frame, owner);
  with-keywords-removed (options = options, #[frame:, owner:])
    apply(do-choose-directory, framem, owner, options)
  end
end method choose-directory;
||#

(defgeneric choose-directory (&rest options
			      &key frame owner title
			      documentation exit-boxes default)
  (:documentation
"
Displays the built-in directory dialog for the target platform, which
allows the user to choose a directory from any of the local or
networked drives currently connected to the computer.

If the _frame_ argument is specified, the top-level sheet of _frame_
becomes the owner of the dialog.

Alternatively, you can specify the owner directly using the _owner_
argument, which takes an instance of <sheet> as its value.

By default, both _frame_ and _owner_ are #f, meaning the dialog has no
owner. You should not specify both of these values.

If you wish, you can specify a _title_ for the dialog; this is
displayed in the title bar of the frame containing the dialog.

Example

The following example illustrates how you can define a class of frame
that contains a button that displays the Choose Directory dialog,
using the pre-built dialog classes for your target environment.

    (define-frame <directory-dialog-frame> (<simple-frame>)
      (:pane dir-file-button (frame)
        (make-pane '<menu-button>
                   :label \"Choose directory ...\"
                   :documentation
                   \"Example of standard 'Choose Dir' dialog\"
                   :activate-callback 
                   #'(lambda (button)
                       (let ((dir (choose-directory :owner frame)))
                         (when dir
                           (setf (frame-status-message frame)
                                 (format nil
                                         \"Choose directory ~s\", dir)))))))
      (:pane dir-layout (frame)
        (vertically ()
          (dir-file-button frame)))
      (:layout (frame) (dir-layout frame))
      (:default-initargs :title \"Choose directory example\"))
"))

;; Returns the pathname on normal exit, or #f on "cancel"
(defmethod choose-directory (&rest options
			     &key
			     frame owner title documentation
			     exit-boxes default)
  (declare (dynamic-extent options)
	   (ignore title documentation exit-boxes default))
  (multiple-value-bind (framem owner)
      (get-frame-manager-and-owner frame owner)
    (with-keywords-removed (options = options (:frame :owner))
      (apply #'do-choose-directory framem owner options))))


#||
define open generic do-choose-directory
    (framem :: <abstract-frame-manager>, owner :: <sheet>,
     #key title, documentation, exit-boxes, default,
     #all-keys)
 => (locator);
||#

(defgeneric do-choose-directory (framem owner
				 &key title documentation exit-boxes default
				 &allow-other-keys))

#||
// Values are (printer, n-copies :: <integer>, print-to-file? :: <boolean>)
// when choosing a printer, or no values when doing print setup
define method choose-printer
    (#rest options,
     #key frame, owner, title, documentation, exit-boxes, default, setup?)
 => (#rest values);
  dynamic-extent(options);
  ignore(title, documentation, exit-boxes, default, setup?);
  let (framem, owner) = get-frame-manager-and-owner(frame, owner);
  with-keywords-removed (options = options, #[frame:, owner:])
    apply(do-choose-printer, framem, owner, options)
  end
end method choose-printer;
||#

;; Values are (printer, n-copies :: <integer>, print-to-file? :: <boolean>)
;; when choosing a printer, or no values when doing print setup
(defgeneric choose-printer (&rest options
			   &key frame owner title documentation
			   exit-boxes default setup?))

(defmethod choose-printer (&rest
			   options
			   &key
			   frame owner title documentation
			   exit-boxes default setup?)
  (declare (dynamic-extent options)
	   (ignore title documentation exit-boxes default setup?))
  (multiple-value-bind (framem owner)
      (get-frame-manager-and-owner frame owner)
    (with-keywords-removed (options = options (:frame :owner))
      (apply #'do-choose-printer framem owner options))))


#||
define open generic do-choose-printer
    (framem :: <abstract-frame-manager>, owner :: <sheet>,
     #key title, documentation, exit-boxes, default, setup?,
     #all-keys)
 => (#rest values);
||#

(defgeneric do-choose-printer (framem owner
			       &key
			       title documentation exit-boxes default setup?
			       &allow-other-keys))


#||
// Returns the color on normal exit, or #f on "cancel"
define method choose-color
    (#rest options,
     #key frame, owner, title, documentation, exit-boxes, default)
 => (color :: false-or(<color>))
  dynamic-extent(options);
  ignore(title, documentation, exit-boxes, default);
  let (framem, owner) = get-frame-manager-and-owner(frame, owner);
  with-keywords-removed (options = options, #[frame:, owner:])
    apply(do-choose-color, framem, owner, options)
  end
end method choose-color;
||#

(defgeneric choose-color (&rest options
			  &key frame owner title documentation exit-boxes default)
  (:documentation
"
Displays the built-in color dialog for the target platform, which
allows the user to choose a color from the standard palette for
whatever environment the application is running in.

Figure 5.1 The standard Choose Color dialog

If the _frame_ argument is specified, the top-level sheet of _frame_
becomes the owner of the dialog.

Alternatively, you can specify the owner directly using the _owner_
argument, which takes an instance of <sheet> as its value.

By default, both _frame_ and _owner_ are #f, meaning the dialog has no
owner. You should not specify both of these values.

If you wish, you can specify a _title_ for the dialog; this is
displayed in the title bar of the frame containing the dialog.

Example

The following example illustrates how you can define a class of frame
that contains a button that displays the Choose Color dialog, using
the pre-built dialog classes for your target environment. The frame
also contains an ellipse whose color is set to the color chosen from
the dialog.

    (define-frame <color-dialog-frame> (<simple-frame>)
      (:pane ellipse-pane (frame)
        (make-pane '<ellipse-pane> :foreground *red*))
      (:pane choose-color-button (frame)
        (make-pane '<menu-button>
                   :label \"Choose Color...\"
                   :documentation
                   \"Example of standard 'choose color' dialog\"
                   :activate-callback
                   #'(lambda (button)
                       (let ((color (choose-color :owner frame)))
                         (and color
                              (change-ellipse-color frame
                                                    color)))))))
"))

;; Returns the color on normal exit, or #f on "cancel"
(defmethod choose-color (&rest
			 options
			 &key
			 frame owner title documentation exit-boxes default)
  (declare (dynamic-extent options)
	   (ignore title documentation exit-boxes default))
  (multiple-value-bind (framem owner)
      (get-frame-manager-and-owner frame owner)
    (with-keywords-removed (options = options (:frame :owner))
      (apply #'do-choose-color framem owner options))))


#||
define open generic do-choose-color
    (framem :: <abstract-frame-manager>, owner :: <sheet>,
     #key title, documentation, exit-boxes, default,
     #all-keys)
 => (color :: false-or(<color>));
||#

(defgeneric do-choose-color (framem owner
			     &key
			     title documentation exit-boxes default
			     &allow-other-keys))

#||
// Returns the text-style on normal exit, or #f on "cancel"
//---*** hughg, 1999/08/12: The following keywords have been added for
// controlling the Win32 backend, but may not be portable to other GUI
// toolkit backends, in which case a redesign to achieve a unified
// interface might be a good idea!
//   fixed-width-only?, show-help?, show-apply?,
//   choose-character-set?, choose-effects?
define method choose-text-style
    (#rest options,
     #key frame, owner, title, documentation, exit-boxes, default,
	  fixed-width-only? :: <boolean> = #f,
	  show-help? :: <boolean> = #f, show-apply? :: <boolean> = #f,
	  choose-character-set? :: <boolean> = #f,
	  choose-effects? :: <boolean> = #f)
 => (text-style :: false-or(<text-style>))
  dynamic-extent(options);
  ignore(title, documentation, exit-boxes, default);
  let (framem, owner) = get-frame-manager-and-owner(frame, owner);
  with-keywords-removed (options = options, #[frame:, owner:])
    apply(do-choose-text-style, framem, owner, options)
  end
end method choose-text-style;
||#

;; Returns the text-style on normal exit, or #f on "cancel"
;;---*** hughg, 1999/08/12: The following keywords have been added for
;; controlling the Win32 backend, but may not be portable to other GUI
;; toolkit backends, in which case a redesign to achieve a unified
;; interface might be a good idea!
;;   fixed-width-only?, show-help?, show-apply?,
;;   choose-character-set?, choose-effects?
(defgeneric choose-text-style (&rest options
			       &key frame owner title documentation
			       exit-boxes default fixed-width-only? show-help?
			       show-apply? choose-character-set? choose-effects?)
  (:documentation
"
Displays the built-in font dialog for the target platform, thereby
letting the user choose a font.

The _frame_ keyword specifies a frame whose top-level sheet becomes
the owner of the menu. Alternatively, you can specify this top level
sheet explicitly using _owner_. The _title_ keyword lets you choose a
title for the dialog. By default, each of these values is #f.

If you wish, you can specify a _title_ for the dialog; this is an
instance of <string> and is displayed in the title bar of the frame
containing the dialog. If you do not specify _title_, then DUIM uses
the default title for that type of dialog on the target platform.
"))

(defmethod choose-text-style (&rest
			      options
			      &key
			      frame owner title documentation
			      exit-boxes default
			      (fixed-width-only? nil) (show-help? nil)
			      (show-apply? nil)
			      (choose-character-set? nil)
			      (choose-effects? nil))
  (declare (dynamic-extent options)
	   (ignore title documentation exit-boxes default
                   fixed-width-only? show-help? show-apply?
                   choose-character-set? choose-effects?))
  (multiple-value-bind (framem owner)
      (get-frame-manager-and-owner frame owner)
    (with-keywords-removed (options = options (:frame :owner))
      (apply #'do-choose-text-style framem owner options))))


#||
define open generic do-choose-text-style
    (framem :: <abstract-frame-manager>, owner :: <sheet>,
     #key title, documentation, exit-boxes, default,
     #all-keys)
 => (text-style :: false-or(<text-style>));
||#

(defgeneric do-choose-text-style (framem owner
                                  &key
				  title documentation exit-boxes default
				  &allow-other-keys))


#||
define method get-frame-manager-and-owner
    (frame :: false-or(<frame>), owner)
 => (framem :: <frame-manager>, owner :: <sheet>)
  let frame = frame
              | (sheet?(owner) & sheet-frame(owner))
              | (frame?(owner) & owner)
              | current-frame();
  let owner = owner | frame;
  if (owner)
    let framem = frame & frame-manager(frame);
    values(framem, if (frame?(owner)) top-level-sheet(owner) else owner end)
  else
    let _port = default-port();
    let framem = port-default-frame-manager(_port);
    values(framem, find-display(port: _port))
  end
end method get-frame-manager-and-owner;
||#

(defmethod get-frame-manager-and-owner ((frame null) owner)
  (get-frame-manager-and-owner-1 frame owner))


(defmethod get-frame-manager-and-owner ((frame <frame>) owner)
  (get-frame-manager-and-owner-1 frame owner))


(defun get-frame-manager-and-owner-1 (frame owner)
  (let* ((frame (or frame
		    (and (sheetp owner) (sheet-frame owner))
		    (and (framep owner) owner)
		    (current-frame)))
	 (owner (or owner frame)))
    (if owner
	(let ((framem (and frame (frame-manager frame))))
	  (values framem (if (framep owner)
			     ;; protection in case frame is :DETACHED or :DESTROYED
			     ;; so has no top-level sheet
			     (or (top-level-sheet owner)
				 (find-display :port (or (and frame (port frame))
							 (default-port))))
			     owner)))
	(let* ((_port (default-port))
	       (framem (port-default-frame-manager _port)))
	  (values framem (find-display :port _port))))))

