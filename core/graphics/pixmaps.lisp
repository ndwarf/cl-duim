;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GRAPHICS-INTERNALS -*-
(in-package #:duim-graphics-internals)

#||
/// Pixmaps

define protocol-class pixmap (<image>) end;
||#

(define-protocol-class pixmap (<image>) ()
  (:documentation
"
The class of pixmap objects.

A pixmap can be thought of as an 'off-screen window', that is, a
medium that can be used for graphical output, but is not visible on
any display device. Pixmaps are provided to allow you to generate a
piece of output associated with some display device that can then be
rapidly drawn on a real display device. For example, an electrical CAD
system might generate a pixmap that corresponds to a complex,
frequently used part in a VLSI schematic, and then use
'copy-from-pixmap' to draw the part as needed.
"))

;;; TODO: also the documentation for 'pixmap?' predicate
#||
Returns true if _object_ is a pixmap.
||#

#||
define protocol <<pixmap-protocol>> (<<image-protocol>>)
  function draw-pixmap
    (drawable :: <drawable>, pixmap :: <pixmap>, x, y, #key function) => (record);
  function copy-area
    (drawable :: type-union(<drawable>, <pixmap>), from-x, from-y, width, height, to-x, to-y,
     #key function = $boole-1) => ();
  function do-copy-area
    (from-drawable :: type-union(<abstract-medium>, <pixmap>), from-x, from-y, width, height,
     to-drawable :: type-union(<abstract-medium>, <pixmap>), to-x, to-y,
     #key function = $boole-1) => ();
  function make-pixmap
    (medium :: <abstract-medium>, width, height) => (pixmap :: <pixmap>);
  function do-make-pixmap
    (port :: <abstract-port>, medium :: <abstract-medium>, width, height)
 => (pixmap :: <pixmap>);
  function destroy-pixmap
     (pixmap :: <pixmap>) => ();
  // Output to pixmaps...
  getter pixmap-drawable
    (pixmap :: <pixmap>) => (drawable);
  setter pixmap-drawable-setter
    (drawable, pixmap :: <pixmap>) => (drawable);
  function do-with-output-to-pixmap
    (drawable :: <drawable>, continuation :: <function>, #key width, height, clear?)
 => (pixmap :: <pixmap>);
  function do-with-double-buffering
    (drawable :: <drawable>, continuation :: <function>, #key x, y, width, height, pixmap)
 => (#rest values);
end protocol <<pixmap-protocol>>;
||#

(define-protocol <<pixmap-protocol>> (<<image-protocol>>)
  (:function draw-pixmap (drawable pixmap x y &key function)
	     (:documentation
"
Draws the contents of _pixmap_ on _drawable_ at (_x_, _y_).

The function 'draw-pixmap*' is identical to 'draw-pixmap', except that
it passes composite objects, rather than separate coordinates, in its
arguments. You should be aware that using this function may lead to a
loss of performance.

Defined by: <<pixmap-protocol>>
"))
  (:function draw-pixmap* (drawable pixmap point &key function)
	     (:documentation
"
Draws the contents of _pixmap_ on _drawable_ at (_x_, _y_).

The function 'draw-pixmap*' is identical to 'draw-pixmap', except that
it passes composite objects, rather than separate coordinates, in its
arguments. You should be aware that using this function may lead to a
loss of performance.

Defined by: <<pixmap-protocol>>
"))
  (:function copy-area (drawable from-x from-y width height to-x to-y
                        &key function)
	     (:documentation
"
Copies the pixels from the _medium_ starting at the position specified
by (_from-x_, _from-y_) to the position (_to-x_, _to-y_) on the same
medium. A rectangle whose width and height is specified by _width_ and
_height_ is copied. If _medium_ is a medium or a stream, then the x
and y values are transformed by the user transformation. The copying
must be done by 'medium-copy-copy' (TODO: ???).

Defined by: <<pixmap-protocol>>
"))
  (:function do-copy-area (from-drawable from-x from-y width height
		           to-drawable to-x to-y &key function) (:documentation " Defined by: <<PIXMAP-PROTOCOL>> "))
  (:function make-pixmap (medium width height)
	     (:documentation
"
Creates a pixmap from _medium_ with a specified size, in pixels, given
by _width_ and _height_.

Defined by: <<pixmap-protocol>>
"))
  (:function do-make-pixmap (port medium width height) (:documentation " Defined by: <<PIXMAP-PROTOCOL>> "))
  (:function destroy-pixmap (pixmap)
	     (:documentation
"
Destroys _pixmap_.

Defined by: <<pixmap-protocol>>
"))
  ;; Output to pixmaps...
  (:getter pixmap-drawable (pixmap) (:documentation " Defined by: <<PIXMAP-PROTOCOL>> "))
  ;;(defgeneric pixmap-drawable-setter (drawable pixmap))
  (:setter (setf pixmap-drawable) (drawable pixmap) (:documentation " Defined by: <<PIXMAP-PROTOCOL>> "))
  (:function do-with-output-to-pixmap (drawable continuation
			               &key width height clear?)
	     (:documentation
"
Returns a pixmap for the specified medium. This function is called by
'with-output-to-pixmap' and returns the pixmap that is operated on. If
you are subclassing <medium>, you must define new methods on this
function.

The _width_ and _height_ are integers that give the width and height
of the pixmap. If they are unsupplied, the result pixmap will be large
enough to contain all of the output done by the body of code executed
by 'with-output-to-pixmap'.

Defined by: <<pixmap-protocol>>
"))
  (:function do-with-double-buffering (drawable continuation &key x y width height pixmap)
    (:documentation " Defined by: <<PIXMAP-PROTOCOL>> ")))


;;; And a few non-protocol GFs

(defgeneric copy-from-pixmap (from-pixmap pixmap-x pixmap-y width height to-medium medium-x medium-y &key function)
  (:documentation
"
Copies a rectangle of pixels from _from-pixmap_ starting at the
position specified by (_pixmap-x_, _pixmap-y_) into _to-medium_ at the
position (_medium-x_, _medium-y_). A rectangle whose width and height
are specified by _width_ and _height_ is copied. If _medium_ is a
medium or a stream, then _medium-x_ and _medium-y_ are transformed by
the user transformation. The copying must be done by
'medium-copy-copy' (TODO: ???).
"))

(defgeneric copy-to-pixmap (from-medium medium-x medium-y width height to-pixmap pixmap-x pixmap-y &key function)
  (:documentation
"
Copies the pixels from the _from-medium_ starting at the position
specified by (_medium-x_, _medium-y_) into _to-pixmap_ at the position
specified by (_pixmap-x_, _pixmap-y_). A rectangle whose width and
height is specified by _width_ and _height_ is copied. If
_from-medium_ is a medium or a stream, then _medium-x_ and _medium-y_
are transformed by the user transformation. The copying must be done
by 'medium-copy-copy' (TODO: ???).

If _to-pixmap_ is not supplied, a new pixmap will be allocated.
"))

#||
define method make-pixmap
    (medium :: <medium>, width, height) => (pixmap :: <pixmap>)
  do-make-pixmap(port(medium), medium, width, height)
end method make-pixmap;
||#

(defmethod make-pixmap ((medium <medium>) width height)
  (do-make-pixmap (port medium) medium width height))


#||
define method destroy-pixmap (pixmap :: <pixmap>) => ()
  #f
end method destroy-pixmap;
||#

(defmethod destroy-pixmap ((pixmap <pixmap>))
  nil)



#||

/// Pixmap mediums

define open abstract class <pixmap-medium> (<medium>) end;
||#

(defclass <pixmap-medium> (<medium>)
  ()
  (:documentation
"
The class of pixmap mediums, that is, mediums capable of doing output
to a pixmap.
")
  (:metaclass <abstract-metaclass>))


#||
define open abstract class <basic-pixmap-medium> (<basic-medium>, <pixmap-medium>)
  sealed constant slot pixmap-medium-pixmap,
    required-init-keyword: pixmap:;
end class <basic-pixmap-medium>;
||#

(defclass <basic-pixmap-medium>
    (<basic-medium> <pixmap-medium>)
  ((pixmap-medium-pixmap :initarg :pixmap :initform (required-slot ":pixmap" "<basic-pixmap-medium>") :reader pixmap-medium-pixmap))
  (:metaclass <abstract-metaclass>))


#||
define open generic make-pixmap-medium
    (port :: <abstract-port>, sheet :: <abstract-sheet>, #key width, height)
 => (medium :: <pixmap-medium>);
||#

(defgeneric make-pixmap-medium (port sheet &key width height))


#||
define sealed inline method make
    (class == <pixmap-medium>, #key port, sheet, width, height)
 => (medium :: <pixmap-medium>)
  make-pixmap-medium(port, sheet, width: width, height: height)
end method make;



/// COPY-AREA

define method copy-area
    (sheet :: <basic-sheet>, from-x, from-y, width, height, to-x, to-y,
     #key function = $boole-1) => ()
  with-sheet-medium (medium = sheet)
    do-copy-area(medium, from-x, from-y, width, height,
		 medium, to-x, to-y, function: function)
  end
end method copy-area;
||#

(defmethod copy-area ((sheet <basic-sheet>) from-x from-y width height to-x to-y
		      &key (function +boole-1+))
  (with-sheet-medium (medium = sheet)
    (do-copy-area medium from-x from-y width height
		  medium to-x to-y :function function)))


#||
define method copy-area
    (sheet :: <permanent-medium-mixin>, from-x, from-y, width, height, to-x, to-y,
     #key function = $boole-1) => ()
  let medium = sheet-medium(sheet);
  do-copy-area(medium, from-x, from-y, width, height,
	       medium, to-x, to-y, function: function)
end method copy-area;
||#

(defmethod copy-area ((sheet <permanent-medium-mixin>) from-x from-y width height to-x to-y
		      &key (function +boole-1+))
  (let ((medium (sheet-medium sheet)))
    (do-copy-area medium from-x from-y width height
		  medium to-x to-y :function function)))


#||
define sealed inline method copy-area
    (medium :: <basic-medium>,
     from-x, from-y, width, height, to-x, to-y,
     #key function = $boole-1) => ()
  do-copy-area(medium, from-x, from-y, width, height,
	       medium, to-x, to-y, function: function)
end method copy-area;
||#

(defmethod copy-area ((medium <basic-medium>) from-x from-y width height to-x to-y
		      &key (function +boole-1+))
  (do-copy-area medium from-x from-y width height
		medium to-x to-y :function function))


#||
define sealed method copy-from-pixmap
    (pixmap :: <pixmap>, pixmap-x, pixmap-y, width, height,
     medium :: <medium>, medium-x, medium-y,
     #key function = $boole-1) => ()
  do-copy-area(pixmap, pixmap-x, pixmap-y, width, height,
	       medium, medium-x, medium-y, function: function)
end method copy-from-pixmap;
||#

(defmethod copy-from-pixmap ((pixmap <pixmap>) pixmap-x pixmap-y width height
			     (medium <medium>) medium-x medium-y
			     &key (function +boole-1+))
  (do-copy-area pixmap pixmap-x pixmap-y width height
		medium medium-x medium-y :function function))


#||
define sealed method copy-to-pixmap
    (medium :: <medium>, medium-x, medium-y, width, height,
     pixmap :: false-or(<pixmap>), pixmap-x, pixmap-y,
     #key function = $boole-1) => (pixmap :: <pixmap>)
  unless (pixmap)
    pixmap := make-pixmap(medium, width, height)
  end;
  do-copy-area(medium, medium-x, medium-y, width, height,
	       pixmap, pixmap-x, pixmap-y, function: function);
  pixmap
end method copy-to-pixmap;
||#

(defmethod copy-to-pixmap ((medium <medium>) medium-x medium-y width height
			   (pixmap <pixmap>) pixmap-x pixmap-y
			   &key (function +boole-1+))
  (unless pixmap
    (setf pixmap (make-pixmap medium width height)))
  (do-copy-area medium medium-x medium-y width height
		pixmap pixmap-x pixmap-y :function function)
  pixmap)



#||

/// Pixmap sheets

define sealed class <pixmap-sheet>
    (<permanent-medium-mixin>,
     <mirrored-sheet-mixin>,
     <basic-sheet>)
  keyword accepts-focus?: = #f;
end class <pixmap-sheet>;
||#

(defclass <pixmap-sheet>
    (<permanent-medium-mixin>
     <mirrored-sheet-mixin>
     <basic-sheet>)
  ()
  (:default-initargs :accepts-focus? nil))


#||
define sealed domain make (singleton(<pixmap-sheet>));
define sealed domain initialize (<pixmap-sheet>);

define method initialize
    (sheet :: <pixmap-sheet>, #key port: _port, medium, width, height)
  // The medium must be a pixmap medium...
  check-type(medium, <basic-pixmap-medium>);
  next-method();
  sheet-transform(sheet) := $identity-transform;
  sheet-region(sheet) := make-bounding-box(0, 0, width, height);
  sheet.%port := _port;
  sheet-direct-mirror(sheet) := medium-drawable(medium);
  sheet-medium(sheet) := medium
end method initialize;
||#

(defmethod initialize-instance ((sheet <pixmap-sheet>) &key ((:port _port)) medium width height &allow-other-keys)
  (declare (ignore _port width height))
  ;; The medium must be a pixmap medium...
  (check-type medium <basic-pixmap-medium>)
  (call-next-method))

(defmethod initialize-instance :after ((sheet <pixmap-sheet>) &key ((:port _port)) medium width height
				       &allow-other-keys)
  (setf (sheet-transform sheet) *identity-transform*)
  (setf (sheet-region sheet) (make-bounding-box 0 0 width height))
  (%port-setter _port sheet)
  (setf (sheet-direct-mirror sheet) (medium-drawable medium))
  (setf (sheet-medium sheet) medium))


#||
define method update-mirror-region
    (_port :: <port>, sheet :: <pixmap-sheet>, mirror) => ()
  #f
end method update-mirror-region;
||#

(defmethod update-mirror-region ((_port <port>) (sheet <pixmap-sheet>) mirror)
  (declare (ignore mirror))
  nil)


#||
define method update-mirror-transform
    (_port :: <port>, sheet :: <pixmap-sheet>, mirror) => ()
  #f
end method update-mirror-transform;
||#

(defmethod update-mirror-transform ((_port <port>) (sheet <pixmap-sheet>) mirror)
  (declare (ignore mirror))
  nil)



#||

/// Interface to pixmaps

// Options can be WIDTH: and HEIGHT:
// Note that this returns the pixmap, not the values of the body
define macro with-output-to-pixmap
  { with-output-to-pixmap (?medium:name = ?sheet:name, #rest ?options:expression) ?:body end }
    => { begin
	   let with-output-to-pixmap-body = method (?medium) ?body end;
	   do-with-output-to-pixmap(?sheet, with-output-to-pixmap-body, ?options)
	 end }
  { with-output-to-pixmap (?medium:name, #rest ?options:expression) ?:body end }
    => { begin
	   let with-output-to-pixmap-body = method (?medium) ?body end;
	   do-with-output-to-pixmap(?medium, with-output-to-pixmap-body, ?options)
	 end }
end macro with-output-to-pixmap;
||#

;; Options can be :width and :heigth
;; Note that this returns the pixmap, not the values of the body
(defmacro with-output-to-pixmap ((medium &rest options) &body body)
"
Executes a body of code, returning the results to a pixmap.Binds
_medium_ to a pixmap medium, that is, a medium that does output to a
pixmap, and then evaluates _body_ in that context. All the output done
to _medium_ inside of _body_ is drawn on the pixmap stream. The pixmap
medium supports the medium output protocol, including all of the
graphics functions.

The returned value is a pixmap that can be drawn onto _medium_ using
'copy-from-pixmap'.
"
  ;; FIXME: GENSYM MEDIUM - SEE THE "DEFINE-PANE" MACROS FOR A NEATER
  ;; WAY TO DO THIS
  (if (listp medium) ;; (medium = sheet) case
      (let ((medium-name (first medium))
	    (sheet (third medium)))
	`(let ((with-output-to-pixmap-body #'(lambda (,medium-name) ,@body)))
	   (do-with-output-to-pixmap ,sheet with-output-to-pixmap-body ,@options)))
      ;; else
      `(let ((with-output-to-pixmap-body #'(lambda (,medium) ,@body)))
	 (do-with-output-to-pixmap ,medium with-output-to-pixmap-body ,@options))))


#||
define method do-with-output-to-pixmap
    (medium :: <medium>, continuation :: <function>, #key width, height, clear? = #t)
 => (pixmap :: <pixmap>)
  let sheet = medium-sheet(medium);
  let _port = port(sheet);
  let pixmap-medium
    = make-pixmap-medium(_port, sheet,
                         width: width, height: height);
  let pixmap-sheet
    = make(<pixmap-sheet>,
	   port: _port, medium: pixmap-medium,
           width: width, height: height);
  medium-foreground(pixmap-medium) := medium-foreground(medium);
  medium-background(pixmap-medium) := medium-background(medium);
  medium-default-text-style(pixmap-medium) := medium-default-text-style(medium);
  medium-text-style(pixmap-medium) := medium-text-style(medium);
  sheet-mapped?(pixmap-sheet) := #t;
  when (clear?)
    clear-box(pixmap-medium, 0, 0, width, height)
  end;
  continuation(pixmap-medium);
  pixmap-medium-pixmap(pixmap-medium)
end method do-with-output-to-pixmap;
||#


(defmethod do-with-output-to-pixmap ((medium <medium>) (continuation function) &key width height (clear? t))
  (let* ((sheet (medium-sheet medium))
	 (_port (port sheet))
	 (pixmap-medium (make-pixmap-medium _port sheet
					    :width width :height height))
	 ;; FIXME: IS MAKE-PANE RIGHT? PROBABLY IT IS...
	 (pixmap-sheet (make-pane '<pixmap-sheet>
			     :port _port :medium pixmap-medium
			     :width width :height height)))
    (setf (medium-foreground pixmap-medium) (medium-foreground medium))
    (setf (medium-background pixmap-medium) (medium-background medium))
    (setf (medium-default-text-style pixmap-medium) (medium-default-text-style medium))
    (setf (medium-text-style pixmap-medium) (medium-text-style medium))
    (setf (sheet-mapped? pixmap-sheet) t)
    (when clear?
      (clear-box pixmap-medium 0 0 width height))
    (funcall continuation pixmap-medium)
    (pixmap-medium-pixmap pixmap-medium)))


#||
define method do-with-output-to-pixmap
    (sheet :: <sheet>, continuation :: <function>, #key width, height, clear? = #t)
 => (pixmap :: <pixmap>)
  with-sheet-medium (medium = sheet)
    do-with-output-to-pixmap(medium, continuation,
                             width: width, height: height, clear?: clear?)
  end
end method do-with-output-to-pixmap;
||#

(defmethod do-with-output-to-pixmap ((sheet <sheet>) (continuation function) &key width height (clear? t))
  (with-sheet-medium (medium = sheet)
    (do-with-output-to-pixmap medium continuation
			      :width width :height height :clear? clear?)))



#||

/// Double buffering

define macro with-double-buffering
  { with-double-buffering (?medium:name = ?sheet:name, #rest ?options:expression) ?:body end }
    => { begin
	   let with-double-buffering-body = method (?medium) ?body end;
	   do-with-double-buffering(?sheet, with-double-buffering-body, ?options)
	 end }
  { with-double-buffering (?medium:name, #rest ?options:expression) ?:body end }
    => { begin
	   let with-double-buffering-body = method (?medium) ?body end;
	   do-with-double-buffering(?medium, with-double-buffering-body, ?options)
	 end }
end macro with-double-buffering;
||#

(defmacro with-double-buffering ((medium &rest options) &body body)
  ;; FIXME: GENSYM MEDIUM
  (if (listp medium) ;; (medium = sheet) case
      (let ((medium-name (first medium))
	    (sheet (third medium)))
	`(let ((with-double-buffering-body #'(lambda (,medium-name)) ,@body))
	   (do-with-double-buffering ,sheet with-double-buffering-body ,@options)))
      ;; else
      `(let ((with-double-buffering-body #'(lambda (,medium) ,@body)))
	 (do-with-double-buffering ,medium with-double-buffering-body ,@options))))


#||
define method do-with-double-buffering
    (medium :: <medium>, continuation :: <function>,
     #key x = 0, y = 0, width, height, pixmap) => (#rest values)
  let sheet = medium-sheet(medium);
  unless (width & height)
    let (_w, _h) = box-size(sheet-device-region(sheet));
    width := _w;
    height := _h
  end;
  let the-pixmap
    = pixmap | medium-pixmap(medium) | make-pixmap(medium, width, height);
  block ()
    dynamic-bind (medium-drawable(medium) = pixmap-drawable(the-pixmap))
      // Clear the drawing state cache, since we may well need to establish
      // drawing state on the new drawable
      medium-drawing-state-cache(medium) := 0;
      clear-box(medium, 0, 0, width, height);
      continuation(medium)
    end
  cleanup
    medium-drawing-state-cache(medium) := 0;
    copy-from-pixmap(the-pixmap, 0, 0, width, height, medium, x, y);
    // If we allocated a pixmap, get rid of it now
    unless (pixmap | medium-pixmap(medium))
      destroy-pixmap(the-pixmap)
    end
  end
end method do-with-double-buffering;
||#

(defmethod do-with-double-buffering ((medium <medium>) (continuation function)
				     &key (x 0) (y 0) width height pixmap)
  (let ((sheet (medium-sheet medium)))
    (unless (and width height)
      (multiple-value-bind (_w _h)
	  (box-size (sheet-device-region sheet))
	(setf width _w)
	(setf height _h)))
    (let ((the-pixmap (or pixmap
			  (medium-pixmap medium)
			  (make-pixmap medium width height))))
      (unwind-protect
	  (dynamic-bind (((medium-drawable medium) = (pixmap-drawable the-pixmap)))
            ;; Clear the drawing state cache, since we may well need to establish
            ;; drawing state on the new drawable
	    (setf (medium-drawing-state-cache medium) 0)
	    (clear-box medium 0 0 width height)
	    (funcall continuation medium))
	;; cleanup
	(progn
	  (setf (medium-drawing-state-cache medium) 0)
	  (copy-from-pixmap the-pixmap 0 0 width height medium x y)
	  ;; If we allocated a pixmap, get rid of it now
	  (unless (or pixmap (medium-pixmap medium))
	    (destroy-pixmap the-pixmap)))))))


#||
define method do-with-double-buffering
    (sheet :: <sheet>, continuation :: <function>,
     #key x = 0, y = 0, width, height, pixmap) => (#rest values)
  with-sheet-medium (medium = sheet)
    do-with-double-buffering(medium, continuation,
			     x: x, y: y, width: width, height: height, pixmap: pixmap)
  end
end method do-with-double-buffering;
||#

(defmethod do-with-double-buffering ((sheet <sheet>) (continuation function)
				     &key (x 0) (y 0) width height pixmap)
  (with-sheet-medium (medium = sheet)
    (do-with-double-buffering medium continuation
			      :x x :y y :width width :height height :pixmap pixmap)))



#||

/// DRAW-PIXMAP

define method draw-pixmap
    (sheet :: <sheet>, pixmap :: <pixmap>, x, y,
     #rest keys, #key function = $boole-1) => (record)
  dynamic-extent(keys);
  ignore(function);
  with-sheet-medium (medium = sheet)
    apply(draw-pixmap, medium, pixmap, x, y, keys)
  end
end method draw-pixmap;
||#

(defmethod draw-pixmap ((sheet <sheet>) (pixmap <pixmap>) x y
			&rest keys &key (function +boole-1+))
  (declare (dynamic-extent keys)
	   (ignore function))
  (with-sheet-medium (medium = sheet)
    (apply #'draw-pixmap medium pixmap x y keys)))


#||
define method draw-pixmap
    (sheet :: <permanent-medium-mixin>, pixmap :: <pixmap>, x, y,
     #rest keys, #key function = $boole-1) => (record)
  dynamic-extent(keys);
  ignore(function);
  apply(draw-pixmap, sheet-medium(sheet), pixmap, x, y, keys)
end method draw-pixmap;
||#

(defmethod draw-pixmap ((sheet <permanent-medium-mixin>) (pixmap <pixmap>) x y
			&rest keys &key (function +boole-1+))
  (declare (dynamic-extent keys)
	   (ignore function))
  (apply #'draw-pixmap (sheet-medium sheet) pixmap x y keys))


#||
define function draw-pixmap*
    (medium :: <drawable>, pixmap :: <pixmap>, point,
     #rest keys, #key function = $boole-1) => (record)
  dynamic-extent(keys);
  ignore(function);
  apply(draw-pixmap, medium, pixmap,
	point-x(point), point-y(point), keys)
end function draw-pixmap*;
||#

(defmethod draw-pixmap* ((medium <abstract-sheet>) (pixmap <pixmap>) point
			 &rest keys &key (function +boole-1+))
  (declare (dynamic-extent keys)
	   (ignore function))
  (apply #'draw-pixmap medium pixmap
	 (point-x point) (point-y point) keys))

(defmethod draw-pixmap* ((medium <abstract-medium>) (pixmap <pixmap>) point
			 &rest keys &key (function +boole-1+))
  (declare (dynamic-extent keys)
	   (ignore function))
  (apply #'draw-pixmap medium pixmap
	 (point-x point) (point-y point) keys))


#||
// Make 'draw-image' do the right thing on pixmaps
define method draw-image
    (medium :: <drawable>, pixmap :: <pixmap>, x, y) => (record)
  draw-pixmap(medium, pixmap, x, y)
end method draw-image;
||#

(defmethod draw-image ((medium <abstract-sheet>) (pixmap <pixmap>) x y)
  (draw-pixmap medium pixmap x y))

(defmethod draw-image ((medium <abstract-medium>) (pixmap <pixmap>) x y)
  (draw-pixmap medium pixmap x y))


