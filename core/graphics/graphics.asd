;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

(asdf:defsystem :graphics
    :description "DUIM graphics"
    :long-description "Support for graphics drawing."
    :author      "Scott McKay, Andy Armstrong (Lisp port: Duncan Rose <duncan@robotcat.demon.co.uk>)"
    :version     (:read-file-form "version.sexp")
    :licence     "BSD-2-Clause"
    :depends-on (#:duim-utilities
		 #:geometry
		 #:dcs
		 #:sheets)
    :serial t
    :components
    ((:file "package")
     (:file "figure-graphics")
     (:file "path-graphics")
     (:file "pixmaps")))

