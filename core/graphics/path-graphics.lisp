;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GRAPHICS-INTERNALS -*-
(in-package #:duim-graphics-internals)

#||
/// Path-based Graphics

define protocol <<path-graphics-protocol>> ()
  // API level functions
  function start-path (drawable :: <drawable>) => (record);
  function end-path (drawable :: <drawable>) => (record);
  function close-path (drawable :: <drawable>) => (record);
  function abort-path (drawable :: <drawable>) => (record);
  function stroke-path (drawable :: <drawable>, #key filled?) => (record);
  function fill-path (drawable :: <drawable>) => (record);
  function clip-from-path (drawable :: <drawable>, #key function) => (record);
  function save-clipping-region (drawable :: <drawable>) => (record);
  function restore-clipping-region (drawable :: <drawable>) => (record);
  function move-to (drawable :: <drawable>, x, y) => (record);
  function line-to (drawable :: <drawable>, x, y) => (record);
  function arc-to (drawable :: <drawable>, center-x, center-y,
		   radius-1-dx, radius-1-dy, radius-2-dx, radius-2-dy,
		   #key start-angle, end-angle) => (record);
  function curve-to (drawable :: <drawable>, x1, y1, x2, y2, x3, y3) => (record);
end protocol <<path-graphics-protocol>>;
||#

(define-protocol <<path-graphics-protocol>> ()
  ;; API level functions
  (:function start-path (drawable)
	     (:documentation
"
Starts a new path on _drawable_. The path can be created with any
number of calls to 'line-to', 'curve-to' and 'move-to'. Its appearance
can also be manipulated using 'fill-path' and 'stroke-path'.

After creating the path, use either 'close-path' or 'end-path' to
finish the path, or 'abort-path' to abandon it altogether.

Defined by: <<path-graphics-protocol>>
"))
  (:function end-path (drawable)
	     (:documentation
"
Ends the definition of the current path in _drawable_. Once the
definition has been ended, the path can be rendered to the drawable
using 'fill-path' or 'stroke-path'.

The function 'close-path' can also be used to end the definition of a
path.

Defined by: <<path-graphics-protocol>>
"))
  (:function close-path (drawable)
	     (:documentation
"
Closes the current path on _drawable_; that is, creates a closed
figure from the elements already defined.

For example, if you create a path that has four connected lines (using
'line-to'), you can use 'close-path' to join the first and last lines
in the path to create a closed, five-sided figure.

Only closed paths can be filled, although 'fill-path' will close a
non-closed path automatically.

Defined by: <<path-graphics-protocol>>
"))
  (:function abort-path (drawable)
	     (:documentation
"
Aborts the current path on _drawable_. Any operations that have been
performed since the last call to 'start-path' are discarded.

Defined by: <<path-graphics-protocol>>
"))
  (:function stroke-path (drawable &key filled?)
	     (:documentation
"
Uses the current pen to draw the current path on _drawable_. Note that
the path must not have been previously filled. This function does not
close the path: you must use 'close-path' if you wish to do this.

Defined by: <<path-graphics-protocol>>
"))
  (:function fill-path (drawable)
	     (:documentation
"
Uses the current brush to fill the current path on _drawable_. If the
path has not already been closed using 'close-path', it is closed
automatically.

Defined by: <<path-graphics-protocol>>
"))
  (:function clip-from-path (drawable &key function) (:documentation " Defined by: <<PATH-GRAPHICS-PROTOCOL>> "))
  (:function save-clipping-region (drawable) (:documentation " Defined by: <<PATH-GRAPHICS-PROTOCOL>> "))
  (:function restore-clipping-region (drawable) (:documentation " Defined by: <<PATH-GRAPHICS-PROTOCOL>> "))
  (:function move-to (drawable x y)
	     (:documentation
"
Move the position in the current path on _drawable_ to (_x_, _y_).

This function is used, in combination with 'line-to', 'curve-to', and
'arc-to', to define a path. The function 'start-path' should be used
to start the definition of the path, and 'end-path' can be used to
finish the definition.

The function 'move-to' can be used several times within the definition
of a path, allowing for the definition of several visually separate
drawings within the same path.

The function 'move-to*' is identical to 'move-to', except that it
passes composite objects, rather than separate coordinates, in its
arguments. You should be aware that using this function may lead to a
loss of performance.

Defined by: <<path-graphics-protocol>>
"))
  (:function line-to (drawable x y)
	     (:documentation
"
Draws a line from the current position in the path to (_x_, _y_).

This function is used, in combination with 'move-to', 'curve-to', and
'arc-to', to define a path. The function start-path should be used to
start the definition of the path, and 'end-path' can be used to finish
the definition.

The function 'line-to*' is identical to 'line-to', except that it
passes composite objects, rather than separate coordinates, in its
arguments. You should be aware that using this function may lead to a
loss of performance.

Defined by: <<path-graphics-protocol>>
"))
  (:function arc-to (drawable center-x center-y
                     radius-1-dx radius-1-dy radius-2-dx radius-2-dy
                     &key start-angle end-angle)
	     (:documentation
"
Draws an arc in the current path on the specified drawable.

This function is used, in combination with 'line-to', 'curve-to', and
'move-to', to define a path. The function 'start-path' should be used
to start the definition of the path, and 'end-path' can be used to
finish the definition.

The center of the arc is defined by (_center-x_, _center-y_), and the
extreme points of the virtual ellipse around the arc (that is, the
points furthest away from the center for each radius) are calculated
by adding the radius vectors _radius-1-dx_ and _radius-1-dy_ to
_center-x_ and _center-y_ respectively (to calculate the outermost
points for the first radius), and adding the radius vectors
_radius-2-dx_ and _radius-2-dy_ to _center-x_ and _center-y_
respectively (to calculate the outermost points for the second
radius).

Please note that 'arc-to' does not currently support arcs whose
orientation is not axis-aligned ellipses. For all practical purposes,
this means that _radius-1-dy_ and _radius-2-dx_ must always be 0.

The arguments _start-angle_ and _end-angle_ define the extent of the
arc that is drawn.

The function 'arc-to*' is identical to 'arc-to', except that it passes
composite objects, rather than separate coordinates, in its
arguments. You should be aware that using this function may lead to a
loss of performance.

Defined by: <<path-graphics-protocol>>
"))
  (:function curve-to (drawable x1 y1 x2 y2 x3 y3)
	     (:documentation
"
Draws a curve in the current path on _drawable_ starting from the
current position, and passing through (_x1_, _y1_), (_x2_, _y2_),
and (_x3_, _y3_).

This function is used, in combination with 'line-to', 'move-to', and
'arc-to', to define a path. The function 'start-path' should be used
to start the definition of the path, and 'end-path' can be used to
finish the definition.

The function 'curve-to*' is identical to 'curve-to', except that it
passes composite objects, rather than separate coordinates, in its
arguments. You should be aware that using this function may lead to a
loss of performance.

Defined by: <<path-graphics-protocol>>
"))
  ;; Include non-spread versions too...
  (:function move-to* (drawable point)
	     (:documentation
"
Move the position in the current path on _drawable_ to (_x_, _y_).

This function is used, in combination with 'line-to', 'curve-to', and
'arc-to', to define a path. The function 'start-path' should be used
to start the definition of the path, and 'end-path' can be used to
finish the definition.

The function 'move-to' can be used several times within the definition
of a path, allowing for the definition of several visually separate
drawings within the same path.

The function 'move-to*' is identical to 'move-to', except that it
passes composite objects, rather than separate coordinates, in its
arguments. You should be aware that using this function may lead to a
loss of performance.

Defined by: <<path-graphics-protocol>>
"))
  (:function line-to* (drawable point)
	     (:documentation
"
Draws a line from the current position in the path to (_x_, _y_).

This function is used, in combination with 'move-to', 'curve-to', and
'arc-to', to define a path. The function start-path should be used to
start the definition of the path, and 'end-path' can be used to finish
the definition.

The function 'line-to*' is identical to 'line-to', except that it
passes composite objects, rather than separate coordinates, in its
arguments. You should be aware that using this function may lead to a
loss of performance.

Defined by: <<path-graphics-protocol>>
"))
  (:function arc-to* (drawable center-point
                     radius-1-dx radius-1-dy radius-2-dx radius-2-dy
                     &key start-angle end-angle)
	     (:documentation
"
Draws an arc in the current path on the specified drawable.

This function is used, in combination with 'line-to', 'curve-to', and
'move-to', to define a path. The function 'start-path' should be used
to start the definition of the path, and 'end-path' can be used to
finish the definition.

The center of the arc is defined by (_center-x_, _center-y_), and the
extreme points of the virtual ellipse around the arc (that is, the
points furthest away from the center for each radius) are calculated
by adding the radius vectors _radius-1-dx_ and _radius-1-dy_ to
_center-x_ and _center-y_ respectively (to calculate the outermost
points for the first radius), and adding the radius vectors
_radius-2-dx_ and _radius-2-dy_ to _center-x_ and _center-y_
respectively (to calculate the outermost points for the second
radius).

Please note that 'arc-to' does not currently support arcs whose
orientation is not axis-aligned ellipses. For all practical purposes,
this means that _radius-1-dy_ and _radius-2-dx_ must always be 0.

The arguments _start-angle_ and _end-angle_ define the extent of the
arc that is drawn.

The function 'arc-to*' is identical to 'arc-to', except that it passes
composite objects, rather than separate coordinates, in its
arguments. You should be aware that using this function may lead to a
loss of performance.

Defined by: <<path-graphics-protocol>>
"))
  (:function curve-to* (drawable point1 point2 point3)
	     (:documentation
"
Draws a curve in the current path on _drawable_ starting from the
current position, and passing through (_x1_, _y1_), (_x2_, _y2_),
and (_x3_, _y3_).

This function is used, in combination with 'line-to', 'move-to', and
'arc-to', to define a path. The function 'start-path' should be used
to start the definition of the path, and 'end-path' can be used to
finish the definition.

The function 'curve-to*' is identical to 'curve-to', except that it
passes composite objects, rather than separate coordinates, in its
arguments. You should be aware that using this function may lead to a
loss of performance.

Defined by: <<path-graphics-protocol>>
")))



#||

/// START-PATH

define method start-path
    (sheet :: <basic-sheet>) => (record)
  with-sheet-medium (medium = sheet)
    start-path(medium)
  end
end method start-path;
||#

(defmethod start-path ((sheet <basic-sheet>))
  (with-sheet-medium (medium = sheet)
    (start-path medium)))


#||
define method start-path
    (sheet :: <permanent-medium-mixin>) => (record)
  start-path(sheet-medium(sheet))
end method start-path;
||#

(defmethod start-path ((sheet <permanent-medium-mixin>))
  (start-path (sheet-medium sheet)))



#||

/// END-PATH

define method end-path
    (sheet :: <basic-sheet>) => (record)
  with-sheet-medium (medium = sheet)
    end-path(medium)
  end
end method end-path;
||#

(defmethod end-path ((sheet <basic-sheet>))
  (with-sheet-medium (medium = sheet)
    (end-path medium)))


#||
define method end-path
    (sheet :: <permanent-medium-mixin>) => (record)
  end-path(sheet-medium(sheet))
end method end-path;
||#

(defmethod end-path ((sheet <permanent-medium-mixin>))
  (end-path (sheet-medium sheet)))



#||

/// ABORT-PATH

define method abort-path
    (sheet :: <basic-sheet>) => (record)
  with-sheet-medium (medium = sheet)
    abort-path(medium)
  end
end method abort-path;
||#

(defmethod abort-path ((sheet <basic-sheet>))
  (with-sheet-medium (medium = sheet)
    (abort-path medium)))


#||
define method abort-path
    (sheet :: <permanent-medium-mixin>) => (record)
  abort-path(sheet-medium(sheet))
end method abort-path;
||#

(defmethod abort-path ((sheet <permanent-medium-mixin>))
  (abort-path (sheet-medium sheet)))



#||

/// CLOSE-PATH

define method close-path
    (sheet :: <basic-sheet>) => (record)
  with-sheet-medium (medium = sheet)
    close-path(medium)
  end
end method close-path;
||#

(defmethod close-path ((sheet <basic-sheet>))
  (with-sheet-medium (medium = sheet)
    (close-path medium)))


#||
define method close-path
    (sheet :: <permanent-medium-mixin>) => (record)
  close-path(sheet-medium(sheet))
end method close-path;
||#

(defmethod close-path ((sheet <permanent-medium-mixin>))
  (close-path (sheet-medium sheet)))



#||

/// STROKE-PATH

define method stroke-path
    (sheet :: <basic-sheet>, #key filled?) => (record)
  with-sheet-medium (medium = sheet)
    stroke-path(medium, filled?: filled?)
  end
end method stroke-path;
||#

(defmethod stroke-path ((sheet <basic-sheet>) &key filled?)
  (with-sheet-medium (medium = sheet)
    (stroke-path medium :filled? filled?)))


#||
define method stroke-path
    (sheet :: <permanent-medium-mixin>, #key filled?) => (record)
  stroke-path(sheet-medium(sheet), filled?: filled?)
end method stroke-path;
||#

(defmethod stroke-path ((sheet <permanent-medium-mixin>) &key filled?)
  (stroke-path (sheet-medium sheet) :filled? filled?))



#||

/// FILL-PATH

define method fill-path
    (sheet :: <basic-sheet>) => (record)
  with-sheet-medium (medium = sheet)
    fill-path(medium)
  end
end method fill-path;
||#

(defmethod fill-path ((sheet <basic-sheet>))
  (with-sheet-medium (medium = sheet)
    (fill-path medium)))


#||
define method fill-path
    (sheet :: <permanent-medium-mixin>) => (record)
  fill-path(sheet-medium(sheet))
end method fill-path;
||#

(defmethod fill-path ((sheet <permanent-medium-mixin>))
  (fill-path (sheet-medium sheet)))



#||

/// CLIP-FROM-PATH

define method clip-from-path
    (sheet :: <basic-sheet>, #key function = $boole-and) => (record)
  with-sheet-medium (medium = sheet)
    clip-from-path(medium, function: function)
  end
end method clip-from-path;
||#

(defmethod clip-from-path ((sheet <basic-sheet>) &key (function +boole-and+))
  (with-sheet-medium (medium = sheet)
    (clip-from-path medium :function function)))


#||
define method clip-from-path
    (sheet :: <permanent-medium-mixin>, #key function = $boole-and) => (record)
  clip-from-path(sheet-medium(sheet), function: function)
end method clip-from-path;
||#

(defmethod clip-from-path ((sheet <permanent-medium-mixin>) &key (function +boole-and+))
  (clip-from-path (sheet-medium sheet) :function function))



#||

/// SAVE-CLIPPING-REGION

define method save-clipping-region
    (sheet :: <basic-sheet>) => (record)
  with-sheet-medium (medium = sheet)
    save-clipping-region(medium)
  end
end method save-clipping-region;
||#

(defmethod save-clipping-region ((sheet <basic-sheet>))
  (with-sheet-medium (medium = sheet)
    (save-clipping-region medium)))


#||
define method save-clipping-region
    (sheet :: <permanent-medium-mixin>) => (record)
  save-clipping-region(sheet-medium(sheet))
end method save-clipping-region;
||#

(defmethod save-clipping-region ((sheet <permanent-medium-mixin>))
  (save-clipping-region (sheet-medium sheet)))



#||

/// RESTORE-CLIPPING-REGION

define method restore-clipping-region
    (sheet :: <basic-sheet>) => (record)
  with-sheet-medium (medium = sheet)
    restore-clipping-region(medium)
  end
end method restore-clipping-region;
||#

(defmethod restore-clipping-region ((sheet <basic-sheet>))
  (with-sheet-medium (medium = sheet)
    (restore-clipping-region medium)))


#||
define method restore-clipping-region
    (sheet :: <permanent-medium-mixin>) => (record)
  restore-clipping-region(sheet-medium(sheet))
end method restore-clipping-region;
||#

(defmethod restore-clipping-region ((sheet <permanent-medium-mixin>))
  (restore-clipping-region (sheet-medium sheet)))



#||

/// MOVE-TO

define method move-to
    (sheet :: <basic-sheet>, x, y) => (record)
  with-sheet-medium (medium = sheet)
    move-to(medium, x, y)
  end
end method move-to;
||#

(defmethod move-to ((sheet <basic-sheet>) x y)
  (with-sheet-medium (medium = sheet)
    (move-to medium x y)))


#||
define method move-to
    (sheet :: <permanent-medium-mixin>, x, y) => (record)
  move-to(sheet-medium(sheet), x, y)
end method move-to;
||#

(defmethod move-to ((sheet <permanent-medium-mixin>) x y)
  (move-to (sheet-medium sheet) x y))


#||
define function move-to*
    (medium :: <drawable>, point :: <standard-point>) => (record)
  move-to(medium, point-x(point), point-y(point))
end function move-to*;
||#

(defmethod move-to* ((medium <abstract-sheet>) (point <standard-point>))
  (move-to medium (point-x point) (point-y point)))

(defmethod move-to* ((medium <abstract-medium>) (point <standard-point>))
  (move-to medium (point-x point) (point-y point)))



#||

/// LINE-TO

define method line-to
    (sheet :: <basic-sheet>, x, y) => (record)
  with-sheet-medium (medium = sheet)
    line-to(medium, x, y)
  end
end method line-to;
||#

(defmethod line-to ((sheet <basic-sheet>) x y)
  (with-sheet-medium (medium = sheet)
    (line-to medium x y)))


#||
define method line-to
    (sheet :: <permanent-medium-mixin>, x, y) => (record)
  line-to(sheet-medium(sheet), x, y)
end method line-to;
||#

(defmethod line-to ((sheet <permanent-medium-mixin>) x y)
  (line-to (sheet-medium sheet) x y))


#||
define function line-to*
    (medium :: <drawable>, point :: <standard-point>) => (record)
  line-to(medium, point-x(point), point-y(point))
end function line-to*;
||#

(defmethod line-to* ((medium <abstract-sheet>) (point <standard-point>))
  (line-to medium (point-x point) (point-y point)))

(defmethod line-to* ((medium <abstract-medium>) (point <standard-point>))
  (line-to medium (point-x point) (point-y point)))



#||

/// ARC-TO

define method arc-to
    (sheet :: <basic-sheet>, center-x, center-y,
     radius-1-dx, radius-1-dy, radius-2-dx, radius-2-dy,
     #rest keys, #key start-angle, end-angle) => (record)
  dynamic-extent(keys);
  ignore(start-angle, end-angle);
  with-sheet-medium (medium = sheet)
    apply(arc-to, medium, center-x, center-y,
	  radius-1-dx, radius-1-dy, radius-2-dx, radius-2-dy, keys)
  end
end method arc-to;
||#

(defmethod arc-to ((sheet <basic-sheet>) center-x center-y
		   radius-1-dx radius-1-dy radius-2-dx radius-2-dy
		   &rest keys &key start-angle end-angle)
  (declare (dynamic-extent keys)
	   (ignore start-angle end-angle))
  (with-sheet-medium (medium = sheet)
    (apply #'arc-to medium center-x center-y
	   radius-1-dx radius-1-dy radius-2-dx radius-2-dy keys)))


#||
define method arc-to
    (sheet :: <permanent-medium-mixin>, center-x, center-y,
     radius-1-dx, radius-1-dy, radius-2-dx, radius-2-dy,
     #rest keys, #key start-angle, end-angle) => (record)
  dynamic-extent(keys);
  ignore(start-angle, end-angle);
  apply(arc-to, sheet-medium(sheet), center-x, center-y,
	radius-1-dx, radius-1-dy, radius-2-dx, radius-2-dy, keys)
end method arc-to;
||#

(defmethod arc-to ((sheet <permanent-medium-mixin>) center-x center-y
		   radius-1-dx radius-1-dy radius-2-dx radius-2-dy
		   &rest keys &key start-angle end-angle)
  (declare (dynamic-extent keys)
	   (ignore start-angle end-angle))
  (apply #'arc-to (sheet-medium sheet) center-x center-y
	 radius-1-dx radius-1-dy radius-2-dx radius-2-dy keys))


#||
define function arc-to*
    (medium :: <drawable>, center :: <standard-point>,
     radius-1-dx, radius-1-dy, radius-2-dx, radius-2-dy,
     #rest keys, #key start-angle, end-angle) => (record)
  dynamic-extent(keys);
  ignore(start-angle, end-angle);
  apply(arc-to, medium, point-x(center), point-y(center),
	radius-1-dx, radius-1-dy, radius-2-dx, radius-2-dy, keys)
end function arc-to*;
||#

(defmethod arc-to* ((medium <abstract-sheet>) (center <standard-point>)
		    radius-1-dx radius-1-dy radius-2-dx radius-2-dy
		    &rest keys &key start-angle end-angle)
  (declare (dynamic-extent keys)
	   (ignore start-angle end-angle))
  (apply #'arc-to medium (point-x center) (point-y center)
	 radius-1-dx radius-1-dy radius-2-dx radius-2-dy keys))

(defmethod arc-to* ((medium <abstract-medium>) (center <standard-point>)
		    radius-1-dx radius-1-dy radius-2-dx radius-2-dy
		    &rest keys &key start-angle end-angle)
  (declare (dynamic-extent keys)
	   (ignore start-angle end-angle))
  (apply #'arc-to medium (point-x center) (point-y center)
	 radius-1-dx radius-1-dy radius-2-dx radius-2-dy keys))



#||

/// CURVE-TO

define method curve-to
    (sheet :: <basic-sheet>, x1, y1, x2, y2, x3, y3) => (record)
  with-sheet-medium (medium = sheet)
    curve-to(medium, x1, y1, x2, y2, x3, y3)
  end
end method curve-to;
||#

(defmethod curve-to ((sheet <basic-sheet>) x1 y1 x2 y2 x3 y3)
  (with-sheet-medium (medium = sheet)
    (curve-to medium x1 y1 x2 y2 x3 y3)))


#||
define method curve-to
    (sheet :: <permanent-medium-mixin>, x1, y1, x2, y2, x3, y3) => (record)
  curve-to(sheet-medium(sheet), x1, y1, x2, y2, x3, y3)
end method curve-to;
||#

(defmethod curve-to ((sheet <permanent-medium-mixin>) x1 y1 x2 y2 x3 y3)
  (curve-to (sheet-medium sheet) x1 y1 x2 y2 x3 y3))


#||
define function curve-to* 
    (medium :: <drawable>, 
     p1 :: <standard-point>, p2 :: <standard-point>, p3 :: <standard-point>) => (record)
  curve-to(medium, point-x(p1), point-y(p1), 
                   point-x(p2), point-y(p2),
                   point-x(p3), point-y(p3))
end function curve-to*;
||#

(defmethod curve-to* ((medium <abstract-sheet>)
		      (p1 <standard-point>) (p2 <standard-point>) (p3 <standard-point>))
  (curve-to medium (point-x p1) (point-y p1)
                   (point-x p2) (point-y p2)
                   (point-x p3) (point-y p3)))

(defmethod curve-to* ((medium <abstract-medium>)
		      (p1 <standard-point>) (p2 <standard-point>) (p3 <standard-point>))
  (curve-to medium (point-x p1) (point-y p1)
                   (point-x p2) (point-y p2)
                   (point-x p3) (point-y p3)))


