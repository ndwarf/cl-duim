;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GRAPHICS-INTERNALS -*-
(in-package #:duim-graphics-internals)

#||
/// Figure-based Graphics

//--- These could become limited collections...
define constant <coordinate-sequence> = <sequence>;
define constant <point-sequence> = <sequence>;
||#

(deftype <coordinate-sequence> () 'sequence)
(deftype <point-sequence> () 'sequence)

#||
define protocol <<figure-graphics-protocol>> ()
  // API level functions
  function draw-point
    (drawable :: <drawable>, x, y) => (record);
  function draw-points
    (drawable :: <drawable>, coord-seq :: <coordinate-sequence>) => (record);
  function draw-line
    (drawable :: <drawable>, x1, y1, x2, y2) => (record);
  function draw-lines
    (drawable :: <drawable>, coord-seq :: <coordinate-sequence>) => (record);
  function draw-arrow
    (drawable :: <drawable>, x1, y1, x2, y2,
     #key from-head?, to-head?, head-length, head-width) => (record);
  function draw-rectangle
    (drawable :: <drawable>, x1, y1, x2, y2,
     #key filled?) => (record);
  function draw-rectangles
    (drawable :: <drawable>, coord-seq :: <coordinate-sequence>,
     #key filled?) => (record);
  function draw-rounded-rectangle
    (drawable :: <drawable>, x1, y1, x2, y2,
     #key filled?, radius) => (record);
  function draw-polygon
    (drawable :: <drawable>, coord-seq :: <coordinate-sequence>,
     #key closed?, filled?) => (record);
  function draw-triangle
    (drawable :: <drawable>, x1, y1, x2, y2, x3, y3,
     #key filled?) => (record);
  function draw-ellipse
    (drawable :: <drawable>, center-x, center-y,
     radius-1-dx, radius-1-dy, radius-2-dx, radius-2-dy,
     #key start-angle, end-angle, filled?) => (record);
  function draw-oval
    (drawable :: <drawable>, center-x, center-y, x-radius, y-radius,
     #rest keys, #key filled? = #t, #all-keys) => (record);
  function draw-bezier-curve
    (drawable :: <drawable>, coord-seq :: <coordinate-sequence>,
     #key filled? = #t) => (record);
  // Image drawing
  function draw-image
    (drawable :: <drawable>, image :: <image>, x, y) => (record);
  // Text drawing
  function draw-text
    (drawable :: <drawable>, string-or-char, x, y,
     #key start, end: _end, align-x, align-y, do-tabs?,
          towards-x, towards-y, transform-glyphs?) => (record);
  // Pixel drawing
  function set-pixel
    (drawable :: <drawable>, color :: <rgb-color>, x, y) => (record);
  function set-pixels
    (drawable :: <drawable>, color :: <rgb-color>, coord-seq :: <coordinate-sequence>) => (record);
end protocol <<figure-graphics-protocol>>;
||#

(define-protocol <<figure-graphics-protocol>> ()
  (:function draw-point (drawable x y)
	     (:documentation
"
Draws a single point on _drawable_ at (_x_, _y_).

The function 'draw-point*' is identical to 'draw-point', except that
it passes composite objects, rather than separate coordinates, in its
arguments. You should be aware that using this function may lead to a
loss of performance.

Defined by: <<figure-graphics-protocol>>
"))
  (:function draw-points (drawable coord-seq)
	     (:documentation
"
Draws a sequence of points on _drawable_ at the specified positions.

The function 'draw-points*@ is identical to @draw-points@, except that
it passes composite objects, rather than separate coordinates, in its
arguments. You should be aware that using this function may lead to a
loss of performance.

Defined by: <<figure-graphics-protocol>>
"))
  (:function draw-line (drawable x1 y1 x2 y2)
	     (:documentation
"
Draws a line on _drawable_ between (_x1_, _y1_) and (_x2_, _y2_),
using the current pen. Dashed lines start dashing from the first
point.

The function 'draw-line*' is identical to 'draw-line', except that it
passes composite objects, rather than separate coordinates, in its
arguments. You should be aware that using this function may lead to a
loss of performance.

Defined by: <<figure-graphics-protocol>>
"))
  (:function draw-lines (drawable coord-seq)
	     (:documentation
"
Draws a series of lines on _drawable_ between the specified sequence
of points, using the current pen. Dashed lines start dashing from the
first point of each line.

The function 'draw-lines*' is identical to 'draw-line', except that it
passes composite objects, rather than separate coordinates, in its
arguments. You should be aware that using this function may lead to a
loss of performance.

Example

    (draw-lines medium (vector 100 150
                               200 250 
                               300 350
                               400 450))

Defined by: <<figure-graphics-protocol>>
 "))
  (:function draw-arrow (drawable x1 y1 x2 y2
                         &key from-head? to-head? head-length head-width)
	     (:documentation
"
Draws an arrow on _drawable_ between two (_x1_, _y1_) and (_x2_,
_y2_), using the current pen. Dashed lines start dashing from the
first point.

If _from-head?_ is #t, then the arrow-head points from (_x1_, _y1_)
to (_x2_, _y2_). If _to-head?_ is #t, then the arrow-head points
from (_x2_, _y2_) to (_x1_, _y1_).

If both _from-head?_ and _to-head?_ are #t, then a double-headed arrow
is drawn.

The arguments _head-length_ and _head-width_ specify the length and
width of the arrow-head respectively, in pixels.

The function 'draw-arrow*' is identical to 'draw-arrow', except that
it passes composite objects, rather than separate coordinates, in its
arguments. You should be aware that using this function may lead to a
loss of performance.

Defined by: <<figure-graphics-protocol>>
"))
  (:function draw-rectangle (drawable x1 y1 x2 y2 &key filled?)
	     (:documentation
"
Draws a rectangle on _drawable_ with left and right corners at (_x1_,
_y1_) and (_x2_, _y2_), using the current pen. Dashed lines start
dashing at the starting point of the first segment.

Note that the specified points could represent either top or bottom
corners: only one rectangle is possible between any pair of points.

If _filled?_ is t then the rectangle will be filled, using the current
brush.

The function 'draw-rectangle*' is identical to 'draw-rectangle',
except that it passes composite objects, rather than separate
coordinates, in its arguments. You should be aware that using this
function may lead to a loss of performance.

Defined by: <<figure-graphics-protocol>>
"))
  (:function draw-rectangles (drawable coord-seq &key filled?)
	     (:documentation
"
Draws a sequence of rectangles on _drawable_ with left and right
corners at the specified positions, using the current pen. Dashed
lines start dashing at the starting point of the first segment of each
rectangle.

If _filled?_ is #t then the rectangles will be filled, using the
current brush.

The function 'draw-rectangles*' is identical to 'draw-rectangles',
except that it passes composite objects, rather than separate
coordinates, in its arguments. You should be aware that using this
function may lead to a loss of performance.

Defined by: <<figure-graphics-protocol>>
"))
  (:function draw-rounded-rectangle (drawable x1 y1 x2 y2 &key filled? radius)
    (:documentation " Defined by: <<FIGURE-GRAPHICS-PROTOCOL>> "))
  (:function draw-polygon (drawable coord-seq &key closed? filled?)
    (:documentation
"
Draws a polygon on _drawable_ joining the specified points, using the
current pen. Dashed lines start dashing at the starting point of the
first segment.

If _closed?_ is #t, then the polygon is closed, that is, a line is
drawn from the last point in the sequence back to the first.

If _filled?_ is #t then the polygon will be filled, using the current
brush.

The function 'draw-polygon*' is identical to 'draw-polygon', except
that it passes composite objects, rather than separate coordinates, in
its arguments. You should be aware that using this function may lead
to a loss of performance.

Defined by: <<figure-graphics-protocol>>
"))
  (:function draw-triangle (drawable x1 y1 x2 y2 x3 y3 &key filled?)
	     (:documentation
"
Draws a triangle on _drawable_ between the specified points, using the
current pen. Dashed lines start dashing at the starting point of the
first segment.

If _filled?_ is #t then the triangle will be filled, using the current
brush.

The function 'draw-triangle*' is identical to 'draw-triangle', except
that it passes composite objects, rather than separate coordinates, in
its arguments. You should be aware that using this function may lead
to a loss of performance.

Defined by: <<figure-graphics-protocol>>
"))
  (:function draw-circle (drawable center-x center-y radius &rest keys)
	     (:documentation
"
Draws a circle on _drawable_ with center (_center-x_, _center-y_) and
a radius of _radius_ pixels, using the current pen.

The _start-angle_ and _end-angle_ arguments let you draw a sector of a
circle rather than a whole circle.

If _filled?_ is t, then the circle will be filled, using the current
brush.

The function 'draw-circle*' is identical to 'draw-circle', except that
it passes composite objects, rather than separate coordinates, in its
arguments. You should be aware that using this function may lead to a
loss of performance.

Defined by: <<figure-graphics-protocol>>
"))
  (:function draw-ellipse (drawable center-x center-y
                           radius-1-dx radius-1-dy radius-2-dx radius-2-dy
                           &key start-angle end-angle filled?)
	     (:documentation
"
Draws an ellipse on _drawable_ with the specified center and extreme
points, using the current pen.

The center of the ellipse is defined by (_center-x_, _center-y_), and
the extreme points of the ellipse (that is, the points furthest away
from the center for each radius) are calculated by adding the radius
vectors _radius-1-dx_ and _radius-1-dy_ to _center-x_ and _center-y_
respectively (to calculate the outermost points for the first radius),
and adding the radius vectors _radius-2-dx_ and _radius-2-dy_ to
_center-x_ and _center-y_ respectively (to calculate the outermost
points for the second radius).

Please note that 'draw-ellipse' does not currently support
non-axis-aligned ellipses. For all practical purposes, this means that
_radius-1-dy_ and _radius-2-dx_ must always be 0.

The arguments _start-angle_ and _end-angle_ let you draw just a
section of the ellipse, rather than the whole ellipse.

If _filled?_ is #t then the ellipse will be filled, using the current
brush.

The function 'draw-ellipse*' is identical to 'draw-ellipse', except
that it passes composite objects, rather than separate coordinates, in
its arguments. You should be aware that using this function may lead
to a loss of performance.

Defined by: <<figure-graphics-protocol>>
"))
  (:function draw-oval (drawable center-x center-y x-radius y-radius
                        &rest keys &key filled? &allow-other-keys)
	     (:documentation
"
Draws an oval on _drawable_ with center (_center-x_, _center-y_) and
radii defined by _x-radius_ and _y-radius_, using the current pen.

Ovals are similar to ellipses, except that they have straight edges.

If _filled?_ is t then the oval will be filled, using the current
brush.

The function 'draw-oval*' is identical to 'draw-oval', except that it
passes composite objects, rather than separate coordinates, in its
arguments. You should be aware that using this function may lead to a
loss of performance.

Defined by: <<figure-graphics-protocol>>
"))
  (:function draw-bezier-curve (drawable coord-seq &rest keys
                                &key filled? &allow-other-keys)
	     (:documentation
"
Draws a bezier curve on _sheet_ or _drawable_ (depending on the
function you use) through the sequence of coordinates given by
_coord-seq_, using the current pen. Dashed lines start dashing from
the first point.

If _filled?_ is t then the bezier-curve will be filled, using the
current brush.

The function 'draw-bezier-curve*' is identical to 'draw-bezier-curve',
except that is passes composite objects, rather than separate
coordinates, in its arguments. You should be aware that using this
function may lead to a loss of performance.

Defined by: <<figure-graphics-protocol>>
"))
  ;; Image drawing
  (:function draw-image (drawable image x y)
	     (:documentation
"
Draws _image_ on _drawable_ at (_x_, _y_).

The function 'draw-image*' is identical to 'draw-image', except that
it passes composite objects, rather than separate coordinates, in its
arguments. You should be aware that using this function may lead to a
loss of performance.

Defined by: <<figure-graphics-protocol>>
"))
  ;; Text drawing
  (:function draw-text (drawable string-or-char x y
                                  &key start end align-x align-y do-tabs?
                                  towards-x towards-y transform-glyphs?)
	     (:documentation
"
Draws text from _text_ on _drawable_ at (_x_, _y_). Text is drawn in
the direction of the point (_towards-x_, _towards-y_).

If _start_ and _end_ are specified, then only a section of text is
drawn, starting at character _start_, and ending with character
_end_. By default, the whole of text is drawn.

The _align-x_ and _align-y_ arguments let you specify the left-right
alignment and the top-bottom alignment (respectively) of the text that
is written to drawable.

For _align-x_, the whole of the distance between (_x_, _y_)
and (_towards-x_, _towards-y_) is used to align text. Thus, if
_align-x_ is :right, the text will appear closer to (_towards-x_,
_towards-y_) than to (_x_, _y_), assuming text occupies less space
than the distance between these two points.

The argument _transform-glyphs?_ controls whether the text is reversed
in cases when _towards-x_ is less than _x_. If _transform-glyphs?_ is
#t, then text is reversed in these cases, that is, the last character
of text to be written is still closest to the point (_towards-x_,
_towards-y_), and the text appears reversed. If _transform-glyphs?_ is
#f, then the first character of text to be written is closest to the
point (_towards-x_, _towards-y_), and the text does not appear
reversed.

If _do-tabs?_ is #t, then any tab characters in text are honored, and
are drawn as tabs. If _do-tabs?_ is #f, then tab characters are
replaced by spaces.

The function 'draw-text*' is identical to 'draw-text', except that it
passes composite objects, rather than separate coordinates, in its
arguments. You should be aware that using this function may lead to a
loss of performance.

Defined by: <<figure-graphics-protocol>>
"))
  ;; Pixel drawing
  (:function set-pixel (drawable color x y) (:documentation " Defined by: <<FIGURE-GRAPHICS-PROTOCOL>> "))
  (:function set-pixels (drawable color coord-seq) (:documentation " Defined by: <<FIGURE-GRAPHICS-PROTOCOL>> "))
  ;; Also add the non-spread methods.
  (:function draw-point* (drawable point)
	     (:documentation
"
Draws a single point on _drawable_ at (_x_, _y_).

The function 'draw-point*' is identical to 'draw-point', except that
it passes composite objects, rather than separate coordinates, in its
arguments. You should be aware that using this function may lead to a
loss of performance.

Defined by: <<figure-graphics-protocol>>
"))
  (:function draw-points* (drawable points)
	     (:documentation
"
Draws a sequence of points on _drawable_ at the specified positions.

The function 'draw-points*@ is identical to @draw-points@, except that
it passes composite objects, rather than separate coordinates, in its
arguments. You should be aware that using this function may lead to a
loss of performance.

Defined by: <<figure-graphics-protocol>>
"))
  (:function draw-line* (drawable start-pt end-pt)
	     (:documentation
"
Draws a line on _drawable_ between (_x1_, _y1_) and (_x2_, _y2_),
using the current pen. Dashed lines start dashing from the first
point.

The function 'draw-line*' is identical to 'draw-line', except that it
passes composite objects, rather than separate coordinates, in its
arguments. You should be aware that using this function may lead to a
loss of performance.

Defined by: <<figure-graphics-protocol>>
"))
  (:function draw-lines* (drawable points)
	     (:documentation
"
Draws a series of lines on _drawable_ between the specified sequence
of points, using the current pen. Dashed lines start dashing from the
first point of each line.

The function 'draw-lines*' is identical to 'draw-line', except that it
passes composite objects, rather than separate coordinates, in its
arguments. You should be aware that using this function may lead to a
loss of performance.

Example

    (draw-lines medium (vector 100 150
                               200 250 
                               300 350
                               400 450))

Defined by: <<figure-graphics-protocol>>
"))
  (:function draw-arrow* (drawable start-pt end-pt
                         &key from-head? to-head? head-length head-width)
	     (:documentation
"
Draws an arrow on _drawable_ between two (_x1_, _y1_) and (_x2_,
_y2_), using the current pen. Dashed lines start dashing from the
first point.

If _from-head?_ is #t, then the arrow-head points from (_x1_, _y1_)
to (_x2_, _y2_). If _to-head?_ is #t, then the arrow-head points
from (_x2_, _y2_) to (_x1_, _y1_).

If both _from-head?_ and _to-head?_ are #t, then a double-headed arrow
is drawn.

The arguments _head-length_ and _head-width_ specify the length and
width of the arrow-head respectively, in pixels.

The function 'draw-arrow*' is identical to 'draw-arrow', except that
it passes composite objects, rather than separate coordinates, in its
arguments. You should be aware that using this function may lead to a
loss of performance.

Defined by: <<figure-graphics-protocol>>
"))
  (:function draw-rectangle* (drawable point1 point2 &key filled?)
	     (:documentation
"
Draws a rectangle on _drawable_ with left and right corners at (_x1_,
_y1_) and (_x2_, _y2_), using the current pen. Dashed lines start
dashing at the starting point of the first segment.

Note that the specified points could represent either top or bottom
corners: only one rectangle is possible between any pair of points.

If _filled?_ is t then the rectangle will be filled, using the current
brush.

The function 'draw-rectangle*' is identical to 'draw-rectangle',
except that it passes composite objects, rather than separate
coordinates, in its arguments. You should be aware that using this
function may lead to a loss of performance.

Defined by: <<figure-graphics-protocol>>
"))
  (:function draw-rectangles* (drawable points &key filled?)
	     (:documentation
"
Draws a sequence of rectangles on _drawable_ with left and right
corners at the specified positions, using the current pen. Dashed
lines start dashing at the starting point of the first segment of each
rectangle.

If _filled?_ is #t then the rectangles will be filled, using the
current brush.

The function 'draw-rectangles*' is identical to 'draw-rectangles',
except that it passes composite objects, rather than separate
coordinates, in its arguments. You should be aware that using this
function may lead to a loss of performance.

Defined by: <<figure-graphics-protocol>>
"))
  (:function draw-rounded-rectangle* (drawable point1 point2 &key filled? radius)
    (:documentation " Defined by: <<FIGURE-GRAPHICS-PROTOCOL>> "))
  (:function draw-polygon* (drawable points &key closed? filled?)
    (:documentation
"
Draws a polygon on _drawable_ joining the specified points, using the
current pen. Dashed lines start dashing at the starting point of the
first segment.

If _closed?_ is #t, then the polygon is closed, that is, a line is
drawn from the last point in the sequence back to the first.

If _filled?_ is #t then the polygon will be filled, using the current
brush.

The function 'draw-polygon*' is identical to 'draw-polygon', except
that it passes composite objects, rather than separate coordinates, in
its arguments. You should be aware that using this function may lead
to a loss of performance.

Defined by: <<figure-graphics-protocol>>
"))
  (:function draw-triangle* (drawable point1 point2 point3 &key filled?)
	     (:documentation
"
Draws a triangle on _drawable_ between the specified points, using the
current pen. Dashed lines start dashing at the starting point of the
first segment.

If _filled?_ is #t then the triangle will be filled, using the current
brush.

The function 'draw-triangle*' is identical to 'draw-triangle', except
that it passes composite objects, rather than separate coordinates, in
its arguments. You should be aware that using this function may lead
to a loss of performance.

Defined by: <<figure-graphics-protocol>>
"))
  (:function draw-circle* (drawable center-point radius &rest keys)
	     (:documentation
"
Draws a circle on _drawable_ with center (_center-x_, _center-y_) and
a radius of _radius_ pixels, using the current pen.

The _start-angle_ and _end-angle_ arguments let you draw a sector of a
circle rather than a whole circle.

If _filled?_ is t, then the circle will be filled, using the current
brush.

The function 'draw-circle*' is identical to 'draw-circle', except that
it passes composite objects, rather than separate coordinates, in its
arguments. You should be aware that using this function may lead to a
loss of performance.

Defined by: <<figure-graphics-protocol>>
"))
  (:function draw-ellipse* (drawable center-point
                           radius-1-dx radius-1-dy radius-2-dx radius-2-dy
                           &key start-angle end-angle filled?)
	     (:documentation
"
Draws an ellipse on _drawable_ with the specified center and extreme
points, using the current pen.

The center of the ellipse is defined by (_center-x_, _center-y_), and
the extreme points of the ellipse (that is, the points furthest away
from the center for each radius) are calculated by adding the radius
vectors _radius-1-dx_ and _radius-1-dy_ to _center-x_ and _center-y_
respectively (to calculate the outermost points for the first radius),
and adding the radius vectors _radius-2-dx_ and _radius-2-dy_ to
_center-x_ and _center-y_ respectively (to calculate the outermost
points for the second radius).

Please note that 'draw-ellipse' does not currently support
non-axis-aligned ellipses. For all practical purposes, this means that
_radius-1-dy_ and _radius-2-dx_ must always be 0.

The arguments _start-angle_ and _end-angle_ let you draw just a
section of the ellipse, rather than the whole ellipse.

If _filled?_ is #t then the ellipse will be filled, using the current
brush.

The function 'draw-ellipse*' is identical to 'draw-ellipse', except
that it passes composite objects, rather than separate coordinates, in
its arguments. You should be aware that using this function may lead
to a loss of performance.

Defined by: <<figure-graphics-protocol>>
"))
  (:function draw-oval* (drawable center-point x-radius y-radius
                        &rest keys &key filled? &allow-other-keys)
	     (:documentation
"
Draws an oval on _drawable_ with center (_center-x_, _center-y_) and
radii defined by _x-radius_ and _y-radius_, using the current pen.

Ovals are similar to ellipses, except that they have straight edges.

If _filled?_ is t then the oval will be filled, using the current
brush.

The function 'draw-oval*' is identical to 'draw-oval', except that it
passes composite objects, rather than separate coordinates, in its
arguments. You should be aware that using this function may lead to a
loss of performance.

Defined by: <<figure-graphics-protocol>>
"))
  (:function draw-bezier-curve* (drawable points &rest keys
                                &key filled? &allow-other-keys)
	     (:documentation
"
Draws a bezier curve on _sheet_ or _drawable_ (depending on the
function you use) through the sequence of coordinates given by
_coord-seq_, using the current pen. Dashed lines start dashing from
the first point.

If _filled?_ is t then the bezier-curve will be filled, using the
current brush.

The function 'draw-bezier-curve*' is identical to 'draw-bezier-curve',
except that is passes composite objects, rather than separate
coordinates, in its arguments. You should be aware that using this
function may lead to a loss of performance.

Defined by: <<figure-graphics-protocol>>
"))
  ;; Image drawing
  (:function draw-image* (drawable image point)
	     (:documentation
"
Draws _image_ on _drawable_ at (_x_, _y_).

The function 'draw-image*' is identical to 'draw-image', except that
it passes composite objects, rather than separate coordinates, in its
arguments. You should be aware that using this function may lead to a
loss of performance.

Defined by: <<figure-graphics-protocol>>
"))
  ;; Text drawing
  (:function draw-text* (drawable string-or-char point
                                  &key start end align-x align-y do-tabs?
                                  towards-x towards-y transform-glyphs?)
	     (:documentation
"
Draws text from _text_ on _drawable_ at (_x_, _y_). Text is drawn in
the direction of the point (_towards-x_, _towards-y_).

If _start_ and _end_ are specified, then only a section of text is
drawn, starting at character _start_, and ending with character
_end_. By default, the whole of text is drawn.

The _align-x_ and _align-y_ arguments let you specify the left-right
alignment and the top-bottom alignment (respectively) of the text that
is written to drawable.

For _align-x_, the whole of the distance between (_x_, _y_)
and (_towards-x_, _towards-y_) is used to align text. Thus, if
_align-x_ is :right, the text will appear closer to (_towards-x_,
_towards-y_) than to (_x_, _y_), assuming text occupies less space
than the distance between these two points.

The argument _transform-glyphs?_ controls whether the text is reversed
in cases when _towards-x_ is less than _x_. If _transform-glyphs?_ is
#t, then text is reversed in these cases, that is, the last character
of text to be written is still closest to the point (_towards-x_,
_towards-y_), and the text appears reversed. If _transform-glyphs?_ is
#f, then the first character of text to be written is closest to the
point (_towards-x_, _towards-y_), and the text does not appear
reversed.

If _do-tabs?_ is #t, then any tab characters in text are honored, and
are drawn as tabs. If _do-tabs?_ is #f, then tab characters are
replaced by spaces.

The function 'draw-text*' is identical to 'draw-text', except that it
passes composite objects, rather than separate coordinates, in its
arguments. You should be aware that using this function may lead to a
loss of performance.

Defined by: <<figure-graphics-protocol>>
"))
  ;; Pixel drawing
  (:function set-pixel* (drawable color point) (:documentation " Defined by: <<FIGURE-GRAPHICS-PROTOCOL>> "))
  (:function set-pixels* (drawable color points) (:documentation " Defined by: <<FIGURE-GRAPHICS-PROTOCOL>> ")))


#||

/// DRAW-POINT

define method draw-point
    (sheet :: <basic-sheet>, x, y) => (record)
  with-sheet-medium (medium = sheet)
    draw-point(medium, x, y)
  end
end method draw-point;
||#

(defmethod draw-point ((sheet <basic-sheet>) x y)
  (with-sheet-medium (medium = sheet)
    (draw-point medium x y)))


#||
define method draw-point 
    (sheet :: <permanent-medium-mixin>, x, y) => (record)
  let medium :: <basic-medium> = sheet-medium(sheet);
  draw-point(medium, x, y)
end method draw-point;
||#

(defmethod draw-point ((sheet <permanent-medium-mixin>) x y)
  (let ((medium (sheet-medium sheet)))
    (draw-point medium x y)))


#||
define function draw-point* 
    (drawable :: <drawable>, point :: <standard-point>) => (record)
  draw-point(drawable, point-x(point), point-y(point))
end function draw-point*;
||#

(defmethod draw-point* ((drawable <abstract-sheet>) (point <standard-point>))
  (draw-point drawable (point-x point) (point-y point)))

(defmethod draw-point* ((drawable <abstract-medium>) (point <standard-point>))
  (draw-point drawable (point-x point) (point-y point)))



#||

/// DRAW-POINTS

define method draw-points 
    (sheet :: <basic-sheet>, coord-seq :: <coordinate-sequence>) => (record)
  with-sheet-medium (medium = sheet)
    draw-points(medium, coord-seq)
  end
end method draw-points;
||#

(defmethod draw-points ((sheet <basic-sheet>) (coord-seq sequence))
  (with-sheet-medium (medium = sheet)
    (draw-points medium coord-seq)))


#||
define method draw-points 
    (sheet :: <permanent-medium-mixin>, coord-seq :: <coordinate-sequence>) => (record)
  let medium :: <basic-medium> = sheet-medium(sheet);
  draw-points(medium, coord-seq)
end method draw-points;
||#

(defmethod draw-points ((sheet <permanent-medium-mixin>) (coord-seq sequence))
  (let ((medium (sheet-medium sheet)))
    (draw-points medium coord-seq)))


#||
define function draw-points* 
    (drawable :: <drawable>, points :: <point-sequence>) => (record)
  draw-points(drawable, spread-point-sequence(points))
end function draw-points*;
||#

(defmethod draw-points* ((drawable <abstract-sheet>) (points sequence))
  (draw-points drawable (spread-point-sequence points)))

(defmethod draw-points* ((drawable <abstract-medium>) (points sequence))
  (draw-points drawable (spread-point-sequence points)))



#||

/// DRAW-LINE and friends

define method draw-line 
    (sheet :: <basic-sheet>, x1, y1, x2, y2) => (record)
  with-sheet-medium (medium = sheet)
    draw-line(medium, x1, y1, x2, y2)
  end
end method draw-line;
||#

(defmethod draw-line ((sheet <basic-sheet>) x1 y1 x2 y2)
  (with-sheet-medium (medium = sheet)
    (draw-line medium x1 y1 x2 y2)))


#||
define method draw-line 
    (sheet :: <permanent-medium-mixin>, x1, y1, x2, y2) => (record)
  let medium :: <basic-medium> = sheet-medium(sheet);
  draw-line(medium, x1, y1, x2, y2)
end method draw-line;
||#

(defmethod draw-line ((sheet <permanent-medium-mixin>) x1 y1 x2 y2)
  (let ((medium (sheet-medium sheet)))
    (draw-line medium x1 y1 x2 y2)))


#||
define function draw-line* 
    (drawable :: <drawable>, point1 :: <standard-point>, point2 :: <standard-point>) => (record)
  draw-line(drawable, point-x(point1), point-y(point1),
		      point-x(point2), point-y(point2))
end function draw-line*;
||#

(defmethod draw-line* ((drawable <abstract-sheet>) (point1 <standard-point>) (point2 <standard-point>))
  (draw-line drawable (point-x point1) (point-y point1) (point-x point2) (point-y point2)))


(defmethod draw-line* ((drawable <abstract-medium>) (point1 <standard-point>) (point2 <standard-point>))
  (draw-line drawable (point-x point1) (point-y point1) (point-x point2) (point-y point2)))


#||
define method draw-arrow 
    (sheet :: <basic-sheet>, x1, y1, x2, y2,
     #rest keys, #key from-head?, to-head? = #t, head-length, head-width) => (record)
  ignore(from-head?, to-head?, head-length, head-width);
  with-sheet-medium (medium = sheet)
    apply(draw-arrow, medium, x1, y1, x2, y2, keys)
  end
end method draw-arrow;
||#

(defmethod draw-arrow ((sheet <basic-sheet>) x1 y1 x2 y2
		       &rest keys
		       &key from-head? (to-head? t) head-length head-width)
  (declare (ignore from-head? to-head? head-length head-width))
  (with-sheet-medium (medium = sheet)
    (apply #'draw-arrow medium x1 y1 x2 y2 keys)))


#||
define method draw-arrow 
    (sheet :: <permanent-medium-mixin>, x1, y1, x2, y2,
     #rest keys, #key from-head?, to-head? = #t, head-length, head-width) => (record)
  ignore(from-head?, to-head?, head-length, head-width);
  let medium :: <basic-medium> = sheet-medium(sheet);
  apply(draw-arrow, medium, x1, y1, x2, y2, keys)
end method draw-arrow;
||#

(defmethod draw-arrow ((sheet <permanent-medium-mixin>) x1 y1 x2 y2
		       &rest keys
		       &key from-head? (to-head? t) head-length head-width)
  (declare (ignore from-head? to-head? head-length head-width))
  (let ((medium (sheet-medium sheet)))
    (apply #'draw-arrow medium x1 y1 x2 y2 keys)))


#||
define method draw-arrow
    (medium :: <basic-medium>, x1, y1, x2, y2,
     #key from-head?, to-head? = #t, head-length, head-width) => (record)
  case
    ~head-length & ~head-width =>
      let pw = max(pen-width(medium-pen(medium)), 1);
      head-width  := pw * 4 + pw;
      head-length := head-width * 2;
    ~head-length =>
      head-length := head-width * 2;
    ~head-width =>
      head-width  := truncate/(head-length, 2);
  end;
  let dx = x2 - x1;
  let dy = y2 - y1;
  let norm
    = if (zero?(dx))
        if (zero?(dy)) 0.0 else 1.0 / abs(dy) end
      else
        if (zero?(dy)) 1.0 / abs(dx) else 1.0 / sqrt(dx * dx + dy * dy) end
      end;
  when (norm > 0)
    let length-norm = head-length * norm;
    let ldx = dx * length-norm;
    let ldy = dy * length-norm;
    let base-norm = head-width * norm * 0.5;
    let bdx = dy * base-norm;
    let bdy = dx * base-norm;
    when (from-head?)
      let xa = x1 + ldx;
      let ya = y1 + ldy;
      with-stack-vector (coords = x1, y1, xa + bdx, ya - bdy, xa - bdx, ya + bdy)
	draw-polygon(medium, coords, filled?: #t)
      end;
      x1 := xa;
      y1 := ya
    end;
    when (to-head?)
      let xa = x2 - ldx;
      let ya = y2 - ldy;
      with-stack-vector (coords = x2, y2, xa + bdx, ya - bdy, xa - bdx, ya + bdy)
	draw-polygon(medium, coords, filled?: #t)
      end;
      x2 := xa;
      y2 := ya
    end;
    // Draw the line after drawing the arrowheads so that the shortening 
    // by the length of the heads has its useful effect
    draw-line(medium, x1, y1, x2, y2)
  end
end method draw-arrow;
||#

(defmethod draw-arrow ((medium <basic-medium>) x1 y1 x2 y2
		       &key from-head? (to-head? t) head-length head-width)
  (cond ((and (not head-length) (not head-width))
	 (let ((pw (max (pen-width (medium-pen medium)) 1)))
	   (setf head-width (+ (* pw 4) pw))  ; isn't this just (* 5 pw)?
	   (setf head-length (* head-width 2))))
	((not head-length)
	 (setf head-length (* head-width 2)))
	((not head-width)
	 (setf head-width (truncate head-length 2))))
  (let* ((dx (- x2 x1))
	 (dy (- y2 y1))
	 (norm (if (zerop dx)
		   (if (zerop dy) 0.0 (/ 1.0 (abs dy)))
		   (if (zerop dy) (/ 1.0 (abs dx)) (/ 1.0 (sqrt (+ (* dx dx) (* dy dy))))))))
    (when (> norm 0)
      (let* ((length-norm (* head-length norm))
	     (ldx (* dx length-norm))
	     (ldy (* dy length-norm))
	     (base-norm (* head-width norm 0.5))
	     (bdx (* dy base-norm))
	     (bdy (* dx base-norm)))
	(when from-head?
	  (let ((xa (+ x1 ldx))
		(ya (+ y1 ldy)))
            (let ((coords (vector x1 y1 (+ xa bdx) (- ya bdy) (- xa bdx) (+ ya bdy))))
	      (draw-polygon medium coords :filled? t))
	    (setf x1 xa)
	    (setf y1 ya)))
	(when to-head?
	  (let ((xa (- x2 ldx))
		(ya (- y2 ldy)))
            (let ((coords (vector x2 y2 (+ xa bdx) (- ya bdy) (- xa bdx) (+ ya bdy))))
	      (draw-polygon medium coords :filled? t))
	    (setf x2 xa)
	    (setf y2 ya)))
	;; Draw the line after drawing the arrowheads so that the shortening 
	;; by the length of the heads has its useful effect
	(draw-line medium x1 y1 x2 y2)))))


#||
define function draw-arrow*
    (drawable :: <drawable>, point1 :: <standard-point>, point2 :: <standard-point>,
     #rest keys) => (record)
  dynamic-extent(keys);
  apply(draw-arrow, drawable,
	point-x(point1), point-y(point1),
	point-x(point2), point-y(point2), keys)
end function draw-arrow*;
||#

(defmethod draw-arrow* ((drawable <abstract-sheet>) (point1 <standard-point>) (point2 <standard-point>)
			&rest keys)
  (declare (dynamic-extent keys))
  (apply #'draw-arrow drawable
	 (point-x point1) (point-y point1)
	 (point-x point2) (point-y point2) keys))

(defmethod draw-arrow* ((drawable <abstract-medium>) (point1 <standard-point>) (point2 <standard-point>)
			&rest keys)
  (declare (dynamic-extent keys))
  (apply #'draw-arrow drawable
	 (point-x point1) (point-y point1)
	 (point-x point2) (point-y point2) keys))



#||

/// DRAW-LINES

define method draw-lines 
    (sheet :: <basic-sheet>, coord-seq :: <coordinate-sequence>) => (record)
  with-sheet-medium (medium = sheet)
    draw-lines(medium, coord-seq)
  end
end method draw-lines;
||#

(defmethod draw-lines ((sheet <basic-sheet>) (coord-seq sequence))
  (with-sheet-medium (medium = sheet)
    (draw-lines medium coord-seq)))


#||
define method draw-lines 
    (sheet :: <permanent-medium-mixin>, coord-seq :: <coordinate-sequence>) => (record)
  let medium :: <basic-medium> = sheet-medium(sheet);
  draw-lines(medium, coord-seq)
end method draw-lines;
||#

(defmethod draw-lines ((sheet <permanent-medium-mixin>) (coord-seq sequence))
  (let ((medium (sheet-medium sheet)))
    (draw-lines medium coord-seq)))


#||
define function draw-lines* 
    (drawable :: <drawable>, points :: <point-sequence>) => (record)
  draw-lines(drawable, spread-point-sequence(points))
end function draw-lines*;
||#

(defmethod draw-lines* ((drawable <abstract-sheet>) (points sequence))
  (draw-lines drawable (spread-point-sequence points)))

(defmethod draw-lines* ((drawable <abstract-medium>) (points sequence))
  (draw-lines drawable (spread-point-sequence points)))



#||

/// DRAW-RECTANGLE

define method draw-rectangle
    (sheet :: <basic-sheet>, x1, y1, x2, y2,
     #rest keys, #key filled? = #t) => (record)
  dynamic-extent(keys);
  ignore(filled?);
  with-sheet-medium (medium = sheet)
    apply(draw-rectangle, medium, x1, y1, x2, y2, keys)
  end
end method draw-rectangle;
||#

(defmethod draw-rectangle ((sheet <basic-sheet>) x1 y1 x2 y2
			   &rest keys &key (filled? t))
  (declare (dynamic-extent keys)
	   (ignore filled?))
  (with-sheet-medium (medium = sheet)
    (apply #'draw-rectangle medium x1 y1 x2 y2 keys)))


#||
define method draw-rectangle
    (sheet :: <permanent-medium-mixin>, x1, y1, x2, y2,
     #rest keys, #key filled? = #t) => (record)
  dynamic-extent(keys);
  ignore(filled?);
  let medium :: <basic-medium> = sheet-medium(sheet);
  apply(draw-rectangle, medium, x1, y1, x2, y2, keys)
end method draw-rectangle;
||#

(defmethod draw-rectangle ((sheet <permanent-medium-mixin>) x1 y1 x2 y2
			   &rest keys &key (filled? t))
  (declare (dynamic-extent keys)
	   (ignore filled?))
  (let ((medium (sheet-medium sheet)))
    (apply #'draw-rectangle medium x1 y1 x2 y2 keys)))


#||
define function draw-rectangle*
    (drawable :: <drawable>, point1 :: <standard-point>, point2 :: <standard-point>,
     #rest keys, #key filled? = #t) => (record)
  dynamic-extent(keys);
  ignore(filled?);
  apply(draw-rectangle, drawable,
	point-x(point1), point-y(point1),
	point-x(point2), point-y(point2), keys)
end function draw-rectangle*;
||#

(defmethod draw-rectangle* ((drawable <abstract-sheet>) (point1 <standard-point>) (point2 <standard-point>)
			    &rest keys &key (filled? t))
  (declare (dynamic-extent keys)
	   (ignore filled?))
  (apply #'draw-rectangle drawable
	 (point-x point1) (point-y point1)
	 (point-x point2) (point-y point2) keys))

(defmethod draw-rectangle* ((drawable <abstract-medium>) (point1 <standard-point>) (point2 <standard-point>)
			    &rest keys &key (filled? t))
  (declare (dynamic-extent keys)
	   (ignore filled?))
  (apply #'draw-rectangle drawable
	 (point-x point1) (point-y point1)
	 (point-x point2) (point-y point2) keys))



#||

/// DRAW-RECTANGLES

define method draw-rectangles
    (sheet :: <basic-sheet>, coord-seq :: <coordinate-sequence>,
     #rest keys, #key filled? = #t) => (record)
  dynamic-extent(keys);
  ignore(filled?);
  with-sheet-medium (medium = sheet)
    apply(draw-rectangles, medium, coord-seq, keys)
  end
end method draw-rectangles;
||#

(defmethod draw-rectangles ((sheet <basic-sheet>) (coord-seq sequence)
			    &rest keys &key (filled? t))
  (declare (dynamic-extent keys)
	   (ignore filled?))
  (with-sheet-medium (medium = sheet)
    (apply #'draw-rectangles medium coord-seq keys)))


#||
define method draw-rectangles
    (sheet :: <permanent-medium-mixin>, coord-seq :: <coordinate-sequence>,
     #rest keys, #key filled? = #t) => (record)
  dynamic-extent(keys);
  ignore(filled?);
  let medium :: <basic-medium> = sheet-medium(sheet);
  apply(draw-rectangles, medium, coord-seq, keys)
end method draw-rectangles;
||#

(defmethod draw-rectangles ((sheet <permanent-medium-mixin>) (coord-seq sequence)
			    &rest keys &key (filled? t))
  (declare (dynamic-extent keys)
	   (ignore filled?))
  (let ((medium (sheet-medium sheet)))
    (apply #'draw-rectangles medium coord-seq keys)))


#||
define function draw-rectangles*
    (drawable :: <drawable>, points :: <point-sequence>,
     #rest keys, #key filled? = #t) => (record)
  dynamic-extent(keys);
  ignore(filled?);
  apply(draw-rectangles, drawable, spread-point-sequence(points), keys)
end function draw-rectangles*;
||#

(defmethod draw-rectangles* ((drawable <abstract-sheet>) (points sequence)
			     &rest keys &key (filled? t))
  (declare (dynamic-extent keys)
	   (ignore filled?))
  (apply #'draw-rectangles drawable (spread-point-sequence points) keys))

(defmethod draw-rectangles* ((drawable <abstract-medium>) (points sequence)
			     &rest keys &key (filled? t))
  (declare (dynamic-extent keys)
	   (ignore filled?))
  (apply #'draw-rectangles drawable (spread-point-sequence points) keys))



#||

/// DRAW-ROUNDED-RECTANGLE

define method draw-rounded-rectangle
    (sheet :: <basic-sheet>, x1, y1, x2, y2,
     #rest keys, #key filled? = #t, radius) => (record)
  dynamic-extent(keys);
  ignore(filled?, radius);
  with-sheet-medium (medium = sheet)
    apply(draw-rounded-rectangle, medium, x1, y1, x2, y2, keys)
  end
end method draw-rounded-rectangle;
||#

(defmethod draw-rounded-rectangle ((sheet <basic-sheet>) x1 y1 x2 y2
				   &rest keys &key (filled? t) radius)
  (declare (dynamic-extent keys)
	   (ignore filled? radius))
  (with-sheet-medium (medium = sheet)
    (apply #'draw-rounded-rectangle medium x1 y1 x2 y2 keys)))


#||
define method draw-rounded-rectangle
    (sheet :: <permanent-medium-mixin>, x1, y1, x2, y2,
     #rest keys, #key filled? = #t, radius) => (record)
  dynamic-extent(keys);
  ignore(filled?, radius);
  let medium :: <basic-medium> = sheet-medium(sheet);
  apply(draw-rounded-rectangle, medium, x1, y1, x2, y2, keys)
end method draw-rounded-rectangle;
||#

(defmethod draw-rounded-rectangle ((sheet <permanent-medium-mixin>) x1 y1 x2 y2
				   &rest keys &key (filled? t) radius)
  (declare (dynamic-extent keys)
	   (ignore filled? radius))
  (let ((medium (sheet-medium sheet)))
    (apply #'draw-rounded-rectangle medium x1 y1 x2 y2 keys)))


#||
define function draw-rounded-rectangle*
    (drawable :: <drawable>, point1 :: <standard-point>, point2 :: <standard-point>,
     #rest keys, #key filled? = #t, radius) => (record)
  dynamic-extent(keys);
  ignore(filled?, radius);
  apply(draw-rounded-rectangle, drawable,
	point-x(point1), point-y(point1),
	point-x(point2), point-y(point2), keys)
end function draw-rounded-rectangle*;
||#

(defmethod draw-rounded-rectangle* ((drawable <abstract-sheet>) (point1 <standard-point>) (point2 <standard-point>)
				    &rest keys &key (filled? t) radius)
  (declare (dynamic-extent keys)
	   (ignore filled? radius))
  (apply #'draw-rounded-rectangle drawable
	 (point-x point1) (point-y point1)
	 (point-x point2) (point-y point2) keys))

(defmethod draw-rounded-rectangle* ((drawable <abstract-medium>) (point1 <standard-point>) (point2 <standard-point>)
				    &rest keys &key (filled? t) radius)
  (declare (dynamic-extent keys)
	   (ignore filled? radius))
  (apply #'draw-rounded-rectangle drawable
	 (point-x point1) (point-y point1)
	 (point-x point2) (point-y point2) keys))



#||

/// DRAW-POLYGON and friends

define method draw-polygon
    (sheet :: <basic-sheet>, coord-seq :: <coordinate-sequence>,
     #rest keys, #key closed? = #t, filled? = #t) => (record)
  dynamic-extent(keys);
  ignore(closed?, filled?);
  with-sheet-medium (medium = sheet)
    apply(draw-polygon, medium, coord-seq, keys)
  end
end method draw-polygon;
||#

(defmethod draw-polygon ((sheet <basic-sheet>) (coord-seq sequence)
			 &rest keys &key (closed? t) (filled? t) &allow-other-keys)
  (declare (dynamic-extent keys)
	   (ignore closed? filled?))
  (with-sheet-medium (medium = sheet)
    (apply #'draw-polygon medium coord-seq keys)))


#||
define method draw-polygon
    (sheet :: <permanent-medium-mixin>, coord-seq :: <coordinate-sequence>,
     #rest keys, #key closed? = #t, filled? = #t) => (record)
  dynamic-extent(keys);
  ignore(closed?, filled?);
  let medium :: <basic-medium> = sheet-medium(sheet);
  apply(draw-polygon, medium, coord-seq, keys)
end method draw-polygon;
||#

(defmethod draw-polygon ((sheet <permanent-medium-mixin>) (coord-seq sequence)
			 &rest keys &key (closed? t) (filled? t) &allow-other-keys)
  (declare (dynamic-extent keys)
	   (ignore closed? filled?))
  (let ((medium (sheet-medium sheet)))
    (apply #'draw-polygon medium coord-seq keys)))


#||
define function draw-polygon*
    (drawable :: <drawable>, points :: <point-sequence>,
     #rest keys, #key closed? = #t, filled? = #t) => (record)
  dynamic-extent(keys);
  ignore(closed?, filled?);
  apply(draw-polygon, drawable, spread-point-sequence(points), keys)
end function draw-polygon*;
||#

(defmethod draw-polygon* ((drawable <abstract-sheet>) (points sequence)
			  &rest keys &key (closed? t) (filled? t) &allow-other-keys)
  (declare (dynamic-extent keys)
	   (ignore closed? filled?))
  (apply #'draw-polygon drawable (spread-point-sequence points) keys))

(defmethod draw-polygon* ((drawable <abstract-medium>) (points sequence)
			  &rest keys &key (closed? t) (filled? t) &allow-other-keys)
  (declare (dynamic-extent keys)
	   (ignore closed? filled?))
  (apply #'draw-polygon drawable (spread-point-sequence points) keys))


#||
define method draw-regular-polygon
    (drawable :: <drawable>, x1, y1, x2, y2, nsides :: <integer>,
     #rest keys,
     #key handedness = #"left", closed? = #t, filled? = #t,
     #all-keys) => (record)
  dynamic-extent(keys);
  ignore(filled?);
  let theta
    = ($2pi / nsides)
        * select (handedness)
	    #"left" => 1;
	    #"right" => -1
	  end;
  let transform = make-rotation-transform(theta);
  let coords :: <simple-object-vector>
    = make(<simple-vector>, size: (if (closed?) nsides + 1 else nsides end) * 2);
  let index = 4;
  without-bounds-checks
    coords[0] := x1; coords[1] := y1;
    coords[2] := x2; coords[3] := y2;
    let dx = x2 - x1;
    let dy = y2 - y1;
    let next-x = x2;
    let next-y = y2;
    for (i :: <integer> from 0 below nsides - 2)
      transform-distances!(transform, dx, dy);
      inc!(next-x, dx);
      inc!(next-y, dy);
      coords[index + 0] := next-x;
      coords[index + 1] := next-y;
      inc!(index, 2)
    end;
    when (closed?)
      coords[index + 0] := x1;
      coords[index + 1] := y1
    end
  end;
  with-keywords-removed (keys = keys, #[handedness:])
    apply(draw-polygon, drawable, coords, keys)
  end
end method draw-regular-polygon;
||#

(defgeneric draw-regular-polygon (drawable x1 y1 x2 y2 nsides
				  &rest keys
				  &key handedness closed? filled?
				  &allow-other-keys)
  (:documentation
"
Draws a regular polygon on _drawable_, using the current pen, that
touches the specified points, and has the specified number of
sides. Dashed lines start dashing at the starting point of the first
segment.

If _filled?_ is #t then the polygon will be filled, using the current
brush.

The function 'draw-regular-polygon*' is identical to
'draw-regular-polygon', except that it passes composite objects,
rather than separate coordinates, in its arguments. You should be
aware that using this function may lead to a loss of performance.
"))

(defmethod draw-regular-polygon ((drawable <abstract-sheet>) x1 y1 x2 y2 (nsides integer)
				 &rest keys
				 &key (handedness :left) (closed? t) (filled? t)
				 &allow-other-keys)
  (declare (ignorable handedness closed? filled?))   ; quiet compiler
  (apply #'%draw-regular-polygon-trampoline drawable x1 y1 x2 y2 nsides keys))


(defmethod draw-regular-polygon ((drawable <abstract-medium>) x1 y1 x2 y2 (nsides integer)
				 &rest keys
				 &key (handedness :left) (closed? t) (filled? t)
				 &allow-other-keys)
  (declare (ignorable handedness closed? filled?))   ; quiet compiler
  (apply #'%draw-regular-polygon-trampoline drawable x1 y1 x2 y2 nsides keys))


(defun %draw-regular-polygon-trampoline (drawable x1 y1 x2 y2 nsides
					 &rest keys
					 &key
					 (handedness :left)
					 (closed? t)
					 (filled? t)
					 &allow-other-keys)
  (declare (dynamic-extent keys)
	   (ignore filled?))
  (FORMAT T "~%DRAW-REGULAR-POLYGON-TRAMPOLINE")
  (let* ((theta (* (/ +2pi+ nsides)
		   (ecase handedness
		     (:left 1)
		     (:right -1))))
	(transform (make-rotation-transform theta))
	(coords (make-array (* (if closed? (+ nsides 1) nsides) 2) :initial-element nil))
	(index 4))
    (without-bounds-checks
      (setf (aref coords 0) x1)
      (setf (aref coords 1) y1)
      (setf (aref coords 2) x2)
      (setf (aref coords 3) y2)
      (let ((dx (- x2 x1))
	    (dy (- y2 y1))
	    (next-x x2)
	    (next-y y2))
	(loop for i from 0 below (- nsides 2)	;repeat would be better here
	   do (transform-distances! transform dx dy)
	   do (incf next-x dx)
	   do (incf next-y dy)
	   do (setf (aref coords (+ index 0)) next-x)
	   do (setf (aref coords (+ index 1)) next-y)
	   do (incf index 2))
	(when closed?
	  (setf (aref coords (+ index 0)) x1)
	  (setf (aref coords (+ index 1)) y1))))
    (with-keywords-removed (keys = keys (:handedness))
      (apply #'draw-polygon drawable coords keys))))


#||
define function draw-regular-polygon*
    (drawable :: <drawable>, point1 :: <standard-point>, point2 :: <standard-point>, 
     nsides :: <integer>,
     #rest keys) => (record)
  dynamic-extent(keys);
  apply(draw-regular-polygon, drawable,
	point-x(point1), point-y(point1),
	point-x(point2), point-y(point2), nsides, keys)
end function draw-regular-polygon*;
||#

(defgeneric draw-regular-polygon* (drawable point1 point2 nsides &rest keys)
  (:documentation
"
Draws a regular polygon on _drawable_, using the current pen, that
touches the specified points, and has the specified number of
sides. Dashed lines start dashing at the starting point of the first
segment.

If _filled?_ is #t then the polygon will be filled, using the current
brush.

The function 'draw-regular-polygon*' is identical to
'draw-regular-polygon', except that it passes composite objects,
rather than separate coordinates, in its arguments. You should be
aware that using this function may lead to a loss of performance.
"))

(defmethod draw-regular-polygon* ((drawable <abstract-sheet>)
				  (point1 <standard-point>)
				  (point2 <standard-point>)
				  (nsides integer)
				  &rest keys)
  (declare (dynamic-extent keys))
  (apply #'draw-regular-polygon drawable
         (point-x point1) (point-y point1)
	 (point-x point2) (point-y point2) nsides keys))


(defmethod draw-regular-polygon* ((drawable <abstract-medium>)
				  (point1 <standard-point>)
				  (point2 <standard-point>)
				  (nsides integer)
				  &rest keys)
  (declare (dynamic-extent keys))
  (apply #'draw-regular-polygon drawable
         (point-x point1) (point-y point1)
	 (point-x point2) (point-y point2) nsides keys))

#||
define method draw-triangle
    (drawable :: <drawable>, x1, y1, x2, y2, x3, y3,
     #rest keys, #key filled? = #t) => (record)
  dynamic-extent(keys);
  ignore(filled?);
  with-stack-vector (coords = x1, y1, x2, y2, x3, y3)
    apply(draw-polygon, drawable, coords, closed?: #t, keys)
  end
end method draw-triangle;
||#

(defmethod draw-triangle ((drawable <abstract-sheet>) x1 y1 x2 y2 x3 y3
			  &rest keys &key (filled? t))
  (declare (dynamic-extent keys)
	   (ignore filled?))
  (let ((coords (vector x1 y1 x2 y2 x3 y3)))
    (apply #'draw-polygon drawable coords :closed? t keys)))

(defmethod draw-triangle ((drawable <abstract-medium>) x1 y1 x2 y2 x3 y3
			  &rest keys &key (filled? t))
  (declare (dynamic-extent keys)
	   (ignore filled?))
  (let ((coords (vector x1 y1 x2 y2 x3 y3)))
    (apply #'draw-polygon drawable coords :closed? t keys)))


#||
define function draw-triangle*
    (drawable :: <drawable>,
     p1 :: <standard-point>, p2 :: <standard-point>, p3 :: <standard-point>,
     #rest keys) => (record)
  dynamic-extent(keys);
  with-stack-vector (points = p1, p2, p3)
    apply(draw-polygon*, drawable, points, closed?: #t, keys)
  end
end function draw-triangle*;
||#

(defmethod draw-triangle* ((drawable <abstract-sheet>)
			   (p1 <standard-point>) (p2 <standard-point>) (p3 <standard-point>)
			   &rest keys)
  (declare (dynamic-extent keys))
  (let ((points (vector p1 p2 p3)))
    (apply #'draw-polygon* drawable points :closed? t keys)))

(defmethod draw-triangle* ((drawable <abstract-medium>) (p1 <standard-point>) (p2 <standard-point>) (p3 <standard-point>)
			   &rest keys)
  (declare (dynamic-extent keys))
  (let ((points (vector p1 p2 p3)))
    (apply #'draw-polygon* drawable points :closed? t keys)))



#||

/// DRAW-ELLIPSE and friends

define method draw-ellipse
    (sheet :: <basic-sheet>, center-x, center-y,
     radius-1-dx, radius-1-dy, radius-2-dx, radius-2-dy,
     #rest keys, #key start-angle, end-angle, filled? = #t) => (record)
  dynamic-extent(keys);
  ignore(start-angle, end-angle, filled?);
  with-sheet-medium (medium = sheet)
    apply(draw-ellipse, medium, center-x, center-y,
	  radius-1-dx, radius-1-dy, radius-2-dx, radius-2-dy, keys)
  end
end method draw-ellipse;
||#

(defmethod draw-ellipse ((sheet <basic-sheet>) center-x center-y
			 radius-1-dx radius-1-dy radius-2-dx radius-2-dy
			 &rest keys &key start-angle end-angle (filled? t))
  (declare (dynamic-extent keys)
	   (ignore start-angle end-angle filled?))
  (with-sheet-medium (medium = sheet)
    (apply #'draw-ellipse medium center-x center-y
	   radius-1-dx radius-1-dy radius-2-dx radius-2-dy keys)))


#||
define method draw-ellipse
    (sheet :: <permanent-medium-mixin>, center-x, center-y,
     radius-1-dx, radius-1-dy, radius-2-dx, radius-2-dy,
     #rest keys, #key start-angle, end-angle, filled? = #t) => (record)
  dynamic-extent(keys);
  ignore(start-angle, end-angle, filled?);
  let medium :: <basic-medium> = sheet-medium(sheet);
  apply(draw-ellipse, medium, center-x, center-y,
	radius-1-dx, radius-1-dy, radius-2-dx, radius-2-dy, keys)
end method draw-ellipse;
||#

(defmethod draw-ellipse ((sheet <permanent-medium-mixin>) center-x center-y
			 radius-1-dx radius-1-dy radius-2-dx radius-2-dy
			 &rest keys &key start-angle end-angle (filled? t))
  (declare (dynamic-extent keys)
	   (ignore start-angle end-angle filled?))
  (let ((medium (sheet-medium sheet)))
    (apply #'draw-ellipse medium center-x center-y
	   radius-1-dx radius-1-dy radius-2-dx radius-2-dy keys)))


#||
define function draw-ellipse*
    (drawable :: <drawable>, center :: <standard-point>, 
     radius-1-dx, radius-1-dy, radius-2-dx, radius-2-dy,
     #rest keys, #key start-angle, end-angle, filled? = #t) => (record)
  dynamic-extent(keys);
  ignore(start-angle, end-angle, filled?);
  apply(draw-ellipse, drawable, point-x(center), point-y(center),
	radius-1-dx, radius-1-dy, radius-2-dx, radius-2-dy, keys)
end function draw-ellipse*;
||#

(defmethod draw-ellipse* ((drawable <abstract-sheet>) (center <standard-point>)
			  radius-1-dx radius-1-dy radius-2-dx radius-2-dy
			  &rest keys &key start-angle end-angle (filled? t))
  (declare (dynamic-extent keys)
	   (ignore start-angle end-angle filled?))
  (apply #'draw-ellipse drawable (point-x center) (point-y center)
	 radius-1-dx radius-1-dy radius-2-dx radius-2-dy keys))

(defmethod draw-ellipse* ((drawable <abstract-medium>) (center <standard-point>)
			  radius-1-dx radius-1-dy radius-2-dx radius-2-dy
			  &rest keys &key start-angle end-angle (filled? t))
  (declare (dynamic-extent keys)
	   (ignore start-angle end-angle filled?))
  (apply #'draw-ellipse drawable (point-x center) (point-y center)
	 radius-1-dx radius-1-dy radius-2-dx radius-2-dy keys))


#||
define method draw-circle
    (drawable :: <drawable>, center-x, center-y, radius, #rest keys) => (record)
  dynamic-extent(keys);
  apply(draw-ellipse, drawable, center-x, center-y,
	radius, 0, 0, radius, keys)
end method draw-circle;
||#

(defmethod draw-circle ((drawable <abstract-sheet>) center-x center-y radius &rest keys)
  (declare (dynamic-extent keys))
  (apply #'draw-ellipse drawable center-x center-y
         radius 0 0 radius keys))

(defmethod draw-circle ((drawable <abstract-medium>) center-x center-y radius &rest keys)
  (declare (dynamic-extent keys))
  (apply #'draw-ellipse drawable center-x center-y
         radius 0 0 radius keys))


#||
define function draw-circle*
    (drawable :: <drawable>, center :: <standard-point>, radius, #rest keys) => (record)
  dynamic-extent(keys);
  apply(draw-ellipse, drawable, point-x(center), point-y(center),
	radius, 0, 0, radius, keys)
end function draw-circle*;
||#

(defmethod draw-circle* ((drawable <abstract-sheet>) (center <standard-point>) radius &rest keys)
  (declare (dynamic-extent keys))
  (apply #'draw-ellipse drawable (point-x center) (point-y center)
	 radius 0 0 radius keys))

(defmethod draw-circle* ((drawable <abstract-medium>) (center <standard-point>) radius &rest keys)
  (declare (dynamic-extent keys))
  (apply #'draw-ellipse drawable (point-x center) (point-y center)
	 radius 0 0 radius keys))


#||
define method draw-oval
    (drawable :: <drawable>, center-x, center-y, x-radius, y-radius,
     #rest keys, #key filled? = #t, #all-keys) => (record)
  dynamic-extent(keys);
  let left  = center-x - x-radius;
  let right = center-x + x-radius;
  let top    = center-y - y-radius;
  let bottom = center-y + y-radius;
  case
    x-radius = y-radius | zero?(x-radius) =>
      apply(draw-ellipse, drawable, center-x, center-y,
	    y-radius, 0, 0, y-radius, keys);
    zero?(y-radius) =>
      apply(draw-ellipse, drawable, center-x, center-y,
	    x-radius, 0, 0, x-radius, keys);
    x-radius > y-radius =>
      let rect-left  = left + y-radius;
      let rect-right = right - y-radius;
      case
        filled? =>
          draw-rectangle
            (drawable, rect-left, top, rect-right, bottom, filled?: #t);
        otherwise =>
          draw-line(drawable, rect-left, top, rect-right, top);
          draw-line(drawable, rect-left, bottom, rect-right, bottom)
      end;
      let north = $pi/2;
      let south = $pi/2 * 3;
      apply(draw-ellipse, drawable, rect-left, center-y,
	    y-radius, 0, 0, y-radius, start-angle: north, end-angle: south, keys);
      apply(draw-ellipse, drawable, rect-right, center-y,
	    y-radius, 0, 0, y-radius, start-angle: south, end-angle: north, keys);
    otherwise =>
      let rect-top = top + x-radius;
      let rect-bottom = bottom - x-radius;
      case
        filled? =>
          draw-rectangle
            (drawable, left, rect-top, right, rect-bottom, filled?: #t);
        otherwise =>
          draw-line(drawable, left, rect-top, left, rect-bottom);
          draw-line(drawable, right, rect-top, right, rect-bottom)
      end;
      let east = 0.0;
      let west = $pi;
      apply(draw-ellipse, drawable, center-x, rect-top,
	    x-radius, 0, 0, x-radius, start-angle: west, end-angle: east, keys);
      apply(draw-ellipse, drawable, center-x, rect-bottom,
	    x-radius, 0, 0, x-radius, start-angle: east, end-angle: west, keys)
  end
end method draw-oval;
||#

(defmethod draw-oval ((drawable <abstract-sheet>) center-x center-y x-radius y-radius
		      &rest keys &key (filled? t) &allow-other-keys)
  (declare (dynamic-extent keys))
  (let ((left   (- center-x x-radius))
	(right  (+ center-x x-radius))
	(top    (- center-y y-radius))
	(bottom (+ center-y y-radius)))
    (cond ((or (= x-radius y-radius) (zerop x-radius))
	   (apply #'draw-ellipse drawable center-x center-y y-radius 0 0 y-radius keys))
	  ((zerop y-radius)
	   (apply #'draw-ellipse drawable center-x center-y x-radius 0 0 x-radius keys))
	  ((> x-radius y-radius)
	   (let ((rect-left (+ left y-radius))
		 (rect-right (- right y-radius)))
	     (cond (filled?
		    (draw-rectangle drawable rect-left top rect-right bottom :filled? t))
		   (t
		    (draw-line drawable rect-left top rect-right top)
		    (draw-line drawable rect-left bottom rect-right bottom)))
	     (let ((north +pi/2+)
		   (south (* +pi/2+ 3)))
	       (apply #'draw-ellipse drawable rect-left center-y
		      y-radius 0 0 y-radius :start-angle north :end-angle south keys)
	       (apply #'draw-ellipse drawable rect-right center-y
		      y-radius 0 0 y-radius :start-angle south :end-angle north keys))))
	  (t
	   (let ((rect-top (+ top x-radius))
		 (rect-bottom (- bottom x-radius)))
	     (cond (filled?
		    (draw-rectangle drawable left rect-top right rect-bottom :filled? t))
		   (t
		    (draw-line drawable left rect-top left rect-bottom)
		    (draw-line drawable right rect-top right rect-bottom)))
	     (let ((east 0.0)
		   (west +pi+))
	       (apply #'draw-ellipse drawable center-x rect-top
		      x-radius 0 0 x-radius :start-angle west :end-angle east keys)
	       (apply #'draw-ellipse drawable center-x rect-bottom
		      x-radius 0 0 x-radius :start-angle east :end-angle west keys)))))))

(defmethod draw-oval ((drawable <abstract-medium>) center-x center-y x-radius y-radius
		      &rest keys &key (filled? t) &allow-other-keys)
  (declare (dynamic-extent keys))
  (let ((left   (- center-x x-radius))
	(right  (+ center-x x-radius))
	(top    (- center-y y-radius))
	(bottom (+ center-y y-radius)))
    (cond ((or (= x-radius y-radius) (zerop x-radius))
	   (apply #'draw-ellipse drawable center-x center-y y-radius 0 0 y-radius keys))
	  ((zerop y-radius)
	   (apply #'draw-ellipse drawable center-x center-y x-radius 0 0 x-radius keys))
	  ((> x-radius y-radius)
	   (let ((rect-left (+ left y-radius))
		 (rect-right (- right y-radius)))
	     (cond (filled?
		    (draw-rectangle drawable rect-left top rect-right bottom :filled? t))
		   (t
		    (draw-line drawable rect-left top rect-right top)
		    (draw-line drawable rect-left bottom rect-right bottom)))
	     (let ((north +pi/2+)
		   (south (* +pi/2+ 3)))
	       (apply #'draw-ellipse drawable rect-left center-y
		      y-radius 0 0 y-radius :start-angle north :end-angle south keys)
	       (apply #'draw-ellipse drawable rect-right center-y
		      y-radius 0 0 y-radius :start-angle south :end-angle north keys))))
	  (t
	   (let ((rect-top (+ top x-radius))
		 (rect-bottom (- bottom x-radius)))
	     (cond (filled?
		    (draw-rectangle drawable left rect-top right rect-bottom :filled? t))
		   (t
		    (draw-line drawable left rect-top left rect-bottom)
		    (draw-line drawable right rect-top right rect-bottom)))
	     (let ((east 0.0)
		   (west +pi+))
	       (apply #'draw-ellipse drawable center-x rect-top
		      x-radius 0 0 x-radius :start-angle west :end-angle east keys)
	       (apply #'draw-ellipse drawable center-x rect-bottom
		      x-radius 0 0 x-radius :start-angle east :end-angle west keys)))))))


#||
define function draw-oval*
    (drawable :: <drawable>, center :: <standard-point>, x-radius, y-radius, 
     #rest keys) => (record)
  dynamic-extent(keys);
  apply(draw-oval, drawable, point-x(center), point-y(center),
	x-radius, y-radius, keys)
end function draw-oval*;
||#

(defmethod draw-oval* ((drawable <abstract-sheet>) (center <standard-point>) x-radius y-radius
		       &rest keys)
  (declare (dynamic-extent keys))
  (apply #'draw-oval drawable (point-x center) (point-y center)
	 x-radius y-radius keys))

(defmethod draw-oval* ((drawable <abstract-medium>) (center <standard-point>) x-radius y-radius
		       &rest keys)
  (declare (dynamic-extent keys))
  (apply #'draw-oval drawable (point-x center) (point-y center)
	 x-radius y-radius keys))



#||

/// DRAW-IMAGE

define method draw-image
    (sheet :: <basic-sheet>, image :: <image>, x, y) => (record)
  with-sheet-medium (medium = sheet)
    apply(draw-image, medium, image, x, y)
  end
end method draw-image;
||#

(defmethod draw-image ((sheet <basic-sheet>) (image <image>) x y)
  (with-sheet-medium (medium = sheet)
    (apply #'draw-image medium image x y)))


#||
define method draw-image
    (sheet :: <permanent-medium-mixin>, image :: <image>, x, y) => (record)
  let medium :: <basic-medium> = sheet-medium(sheet);
  draw-image(medium, image, x, y)
end method draw-image;
||#

(defmethod draw-image ((sheet <permanent-medium-mixin>) (image <image>) x y)
  (let ((medium (sheet-medium sheet)))
    (draw-image medium image x y)))


#||
define function draw-image*
    (drawable :: <drawable>, image :: <image>, point :: <standard-point>) => (record)
  draw-image(drawable, image, point-x(point), point-y(point))
end function draw-image*;
||#

(defmethod draw-image* ((drawable <abstract-sheet>) (image <image>) (point <standard-point>))
  (draw-image drawable image (point-x point) (point-y point)))

(defmethod draw-image* ((drawable <abstract-medium>) (image <image>) (point <standard-point>))
  (draw-image drawable image (point-x point) (point-y point)))


#||
// The default implementation of 'draw-image' is just a special case of
// 'draw-rectangle', believe it or not...
define method draw-image
    (medium :: <basic-medium>, image :: <image>, x, y) => (record)
  let width  = image-width(image);
  let height = image-height(image);
  with-drawing-options (medium, brush: image)
    draw-rectangle(medium, x, y, x + width, y + height, filled?: #t)
  end
end method draw-image;
||#

;; The default implementation of 'draw-image' is just a special case of
;; 'draw-rectangle', believe it or not...
(defmethod draw-image ((medium <basic-medium>) (image <image>) x y)
  (let ((width  (image-width image))
	(height (image-height image)))
    (with-drawing-options (medium :brush image)
      (draw-rectangle medium x y (+ x width) (+ y height) :filled? t))))



#||

/// The rest of CLEAR-BOX

// Some mediums can do better than this...
// Note that the coordinates are unaffected by the medium transformation!
define sideways method clear-box
    (medium :: <basic-medium>, left, top, right, bottom) => ()
  dynamic-bind (medium-brush(medium) = medium-background(medium),
		medium-transform(medium) = $identity-transform)
    draw-rectangle(medium, left, top, right, bottom, filled?: #t)
  end
end method clear-box;
||#

;; Some mediums can do better than this...
;; Note that the coordinates are unaffected by the medium transformation!
(defmethod clear-box ((medium <basic-medium>) left top right bottom)
  (duim-debug-message "[FIGURE-GRAPHICS CLEAR-BOX] entered for basic-medium ~a" medium)
  (dynamic-bind (((medium-brush medium)     = (medium-background medium))
		 ((medium-transform medium) = *identity-transform*))
    (draw-rectangle medium left top right bottom :filled? t)))



#||

/// DRAW-TEXT

define method draw-text
    (sheet :: <basic-sheet>, string-or-char, x, y,
     #rest keys,
     #key start, end: _end,
          align-x = #"left", align-y = #"baseline", do-tabs?,
          towards-x, towards-y, transform-glyphs?) => (record)
  dynamic-extent(keys);
  ignore(start, _end, align-x, align-y, towards-x, towards-y, transform-glyphs?, do-tabs?);
  with-sheet-medium (medium = sheet)
    apply(draw-text, medium, string-or-char, x, y, keys)
  end
end method draw-text;
||#

(defmethod draw-text ((sheet <basic-sheet>) string-or-char x y
		      &rest keys
		      &key start end
		      (align-x :left) (align-y :baseline) do-tabs?
		      towards-x towards-y transform-glyphs?)
  (declare (dynamic-extent keys)
	   (ignore start end align-x align-y towards-x towards-y transform-glyphs? do-tabs?))
  (with-sheet-medium (medium = sheet)
    (apply #'draw-text medium string-or-char x y keys)))


#||
define method draw-text
    (sheet :: <permanent-medium-mixin>, string-or-char, x, y,
     #rest keys,
     #key start, end: _end,
          align-x = #"left", align-y = #"baseline", do-tabs?,
          towards-x, towards-y, transform-glyphs?) => (record)
  dynamic-extent(keys);
  ignore(start, _end, align-x, align-y, towards-x, towards-y, transform-glyphs?, do-tabs?);
  let medium :: <basic-medium> = sheet-medium(sheet);
  apply(draw-text, medium, string-or-char, x, y, keys)
end method draw-text;
||#

(defmethod draw-text ((sheet <permanent-medium-mixin>) string-or-char x y
		      &rest keys
		      &key start end
		      (align-x :left) (align-y :baseline) do-tabs?
		      towards-x towards-y transform-glyphs?)
  (declare (dynamic-extent keys)
	   (ignore start end align-x align-y towards-x towards-y transform-glyphs? do-tabs?))
  (let ((medium (sheet-medium sheet)))
    (apply #'draw-text medium string-or-char x y keys)))


#||
define function draw-text*
    (drawable :: <drawable>, string-or-char, point :: <standard-point>,
     #rest keys, #key towards-point, #all-keys) => (record)
  dynamic-extent(keys);
  with-keywords-removed (keys = keys, #[towards-point:])
    apply(draw-text, drawable, string-or-char, point-x(point), point-y(point),
          towards-x: towards-point & point-x(towards-point),
          towards-y: towards-point & point-y(towards-point), keys)
  end
end function draw-text*;
||#

(defmethod draw-text* ((drawable <abstract-sheet>) string-or-char (point <standard-point>)
		       &rest keys &key towards-point &allow-other-keys)
  (declare (dynamic-extent keys))
  (with-keywords-removed (keys = keys (:towards-point))
    (apply #'draw-text drawable string-or-char (point-x point) (point-y point)
	   :towards-x (and towards-point (point-x towards-point))
	   :towards-y (and towards-point (point-y towards-point)) keys)))

(defmethod draw-text* ((drawable <abstract-medium>) string-or-char (point <standard-point>)
		       &rest keys &key towards-point &allow-other-keys)
  (declare (dynamic-extent keys))
  (with-keywords-removed (keys = keys (:towards-point))
    (apply #'draw-text drawable string-or-char (point-x point) (point-y point)
	   :towards-x (and towards-point (point-x towards-point))
	   :towards-y (and towards-point (point-y towards-point)) keys)))



#||

/// DRAW-BEZIER-CURVE

define method draw-bezier-curve
    (sheet :: <basic-sheet>, coord-seq :: <coordinate-sequence>,
     #rest keys, #key filled? = #t) => (record)
  dynamic-extent(keys);
  ignore(filled?);
  with-sheet-medium (medium = sheet)
    apply(draw-bezier-curve, medium, coord-seq, keys)
  end
end method draw-bezier-curve;
||#

(defmethod draw-bezier-curve ((sheet <basic-sheet>) (coord-seq sequence)
			      &rest keys &key (filled? t))
  (declare (dynamic-extent keys)
	   (ignore filled?))
  (with-sheet-medium (medium = sheet)
    (apply #'draw-bezier-curve medium coord-seq keys)))


#||
define method draw-bezier-curve
    (sheet :: <permanent-medium-mixin>, coord-seq :: <coordinate-sequence>,
     #rest keys, #key filled? = #t) => (record)
  dynamic-extent(keys);
  ignore(filled?);
  let medium :: <basic-medium> = sheet-medium(sheet);
  apply(draw-bezier-curve, medium, coord-seq, keys)
end method draw-bezier-curve;
||#

(defmethod draw-bezier-curve ((sheet <permanent-medium-mixin>) (coord-seq sequence)
			      &rest keys &key (filled? t))
  (declare (dynamic-extent keys)
	   (ignore filled?))
  (let ((medium (sheet-medium sheet)))
    (apply #'draw-bezier-curve medium coord-seq keys)))


#||
define function draw-bezier-curve*
    (drawable :: <drawable>, points :: <point-sequence>, #rest keys) => (record)
  dynamic-extent(keys);
  apply(draw-bezier-curve, drawable, spread-point-sequence(points), keys)
end function draw-bezier-curve*;
||#

(defmethod draw-bezier-curve* ((drawable <abstract-sheet>) (points sequence) &rest keys)
  (declare (dynamic-extent keys))
  (apply #'draw-bezier-curve drawable (spread-point-sequence points) keys))

(defmethod draw-bezier-curve* ((drawable <abstract-medium>) (points sequence) &rest keys)
  (declare (dynamic-extent keys))
  (apply #'draw-bezier-curve drawable (spread-point-sequence points) keys))


#||
// Primary method for mediums that can't do this natively...
define method draw-bezier-curve
    (medium :: <basic-medium>, coord-seq :: <coordinate-sequence>,
     #key filled? = #t) => (record)
  let npoints :: <integer> = size(coord-seq);
  let points  :: <stretchy-object-vector> = make(<stretchy-vector>);
  let distance = 1;
  assert(zero?(modulo(truncate/(npoints, 2) - 4, 3)),
         "Incorrect number of points for Bezier curve drawing");
  local method collect (x, y) => ()
	  add!(points, x);
	  add!(points, y)
	end method;
  dynamic-extent(collect);
  collect(coord-seq[0], coord-seq[1]);
  for (i :: <integer> = 0 then i + 6,
       until: i = npoints - 2)
    render-bezier-curve
      (collect,
       coord-seq[i + 0], coord-seq[i + 1],
       coord-seq[i + 2], coord-seq[i + 3],
       coord-seq[i + 4], coord-seq[i + 5],
       coord-seq[i + 6], coord-seq[i + 7], distance);
    collect(coord-seq[i + 6], coord-seq[i + 7])
  end;
  draw-polygon(medium, points, closed?: #f, filled?: filled?)
end method draw-bezier-curve;
||#

;; Primary method for mediums that can't do this natively...
(defmethod draw-bezier-curve ((medium <basic-medium>) (coord-seq sequence)
			      &rest keys &key (filled? t))
  (declare (ignore keys))
  (setf coord-seq (MAKE-STRETCHY-VECTOR :contents coord-seq))
  (let ((npoints  (length coord-seq))
	(points   (MAKE-STRETCHY-VECTOR))
	(distance 1))
    (unless (zerop (mod (- (truncate npoints 2) 4) 3))
      (error "Incorrect number of points for Bezier curve drawing"))
    (labels ((%collect (x y)
	       (add! points x)
	       (add! points y)))
      ;;(declare (dynamic-extent collect))
      (%collect (aref coord-seq 0) (aref coord-seq 1))
      (loop for i = 0 then (+ i 6)
	    until (= i (- npoints 2))
	    do (render-bezier-curve #'%collect
		       (aref coord-seq (+ i 0)) (aref coord-seq (+ i 1))
		       (aref coord-seq (+ i 2)) (aref coord-seq (+ i 3))
		       (aref coord-seq (+ i 4)) (aref coord-seq (+ i 5))
		       (aref coord-seq (+ i 6)) (aref coord-seq (+ i 7))
		       distance)
	    do (%collect (aref coord-seq (+ i 6)) (aref coord-seq (+ i 7))))
      (draw-polygon medium points :closed? nil :filled? filled?))))


#||
define method render-bezier-curve
    (function :: <function>, x0, y0, x1, y1, x2, y2, x3, y3, distance) => ()
  local method split-bezier-curve (x0, y0, x1, y1, x2, y2, x3, y3)
	  let r1/2 :: <single-float> = 1.0 / 2.0;
	  let r1/4 :: <single-float> = 1.0 / 4.0;
	  let r1/8 :: <single-float> = 1.0 / 8.0;
	  let r3/8 :: <single-float> = 3.0 / 8.0;
	  // We should write a matrix multiplication macro
	  values
	    (// The first 1/2
	     x0, y0,
	     x0 * r1/2 + x1 * r1/2, y0 * r1/2 + y1 * r1/2,
	     x0 * r1/4 + x1 * r1/2 + x2 * r1/4, y0 * r1/4 + y1 * r1/2 + y2 * r1/4,
	     x0 * r1/8 + x1 * r3/8 + x2 * r3/8 + x3 * r1/8,
	     y0 * r1/8 + y1 * r3/8 + y2 * r3/8 + y3 * r1/8,
	     // The second 1/2
	     x0 * r1/8 + x1 * r3/8 + x2 * r3/8 + x3 * r1/8,
	     y0 * r1/8 + y1 * r3/8 + y2 * r3/8 + y3 * r1/8,
	     x1 * r1/4 + x2 * r1/2 + x3 * r1/4, y1 * r1/4 + y2 * r1/2 + y3 * r1/4,
	     x2 * r1/2 + x3 * r1/2, y2 * r1/2 + y3 * r1/2,
	     x3, y3)
	end method,
        method distance-from-line (x0, y0, x1, y1, x, y)
	  let dx = x1 - x0;
	  let dy = y1 - y0;
	  let r-p-x = x - x0;
	  let r-p-y = y - y0;
	  let dot-v = dx * dx + dy * dy;
	  let dot-r-v = r-p-x * dx + r-p-y * dy;
	  let dp = as(<single-float>, dot-r-v) / as(<single-float>, dot-v);
	  let closest-x = x0 + dp * dx;
	  let closest-y = y0 + dp * dy;
	  let ax = x - closest-x;
	  let ay = y - closest-y;
	  values(ax * ax + ay * ay, closest-x, closest-y)
	end method;
  dynamic-extent(split-bezier-curve, distance-from-line);
  let d1 = distance-from-line(x0, y0, x3, y3, x1, y1);
  let d2 = distance-from-line(x0, y0, x3, y3, x2, y2);
  when (d1 >= distance | d2 >= distance)
    let (x00, y00, x10, y10, x20, y20, x30, y30,
	 x01, y01, x11, y11, x21, y21, x31, y31)
      = split-bezier-curve(x0, y0, x1, y1, x2, y2, x3, y3);
    render-bezier-curve
      (function, x00, y00, x10, y10, x20, y20, x30, y30, distance);
    function(x30, y30);
    render-bezier-curve
      (function, x01, y01, x11, y11, x21, y21, x31, y31, distance)
  end
end method render-bezier-curve;
||#

(defun render-bezier-curve (function x0 y0 x1 y1 x2 y2 x3 y3 distance)
  (labels ((split-bezier-curve (x0 y0 x1 y1 x2 y2 x3 y3)
	     ;; The following values should all be single floats.
	     (let ((r1/2 (/ 1.0 2.0))
		   (r1/4 (/ 1.0 4.0))
		   (r1/8 (/ 1.0 8.0))
		   (r3/8 (/ 3.0 8.0)))
	       ;; We should write a matrix multiplication macro
	       (values
		    ;; The first 1/2
		    x0 y0
		    (+ (* x0 r1/2) (* x1 r1/2)) (+ (* y0 r1/2) (* y1 r1/2))
		    (+ (* x0 r1/4) (* x1 r1/2) (* x2 r1/4)) (+ (* y0 r1/4) (* y1 r1/2) (* y2 r1/4))
		    (+ (* x0 r1/8) (* x1 r3/8) (* x2 r3/8) (* x3 r1/8))
		    (+ (* y0 r1/8) (* y1 r3/8) (* y2 r3/8) (* y3 r1/8))
		    ;; The second 1/2
		    (+ (* x0 r1/8) (* x1 r3/8) (* x2 r3/8) (* x3 r1/8))
		    (+ (* y0 r1/8) (* y1 r3/8) (* y2 r3/8) (* y3 r1/8))
		    (+ (* x1 r1/4) (* x2 r1/2) (* x3 r1/4)) (+ (* y1 r1/4) (* y2 r1/2) (* y3 r1/4))
		    (+ (* x2 r1/2) (* x3 r1/2)) (+ (* y2 r1/2) (* y3 r1/2))
		    x3 y3)))
	   (distance-from-line (x0 y0 x1 y1 x y)
	     (let* ((dx (- x1 x0))
		    (dy (- y1 y0))
		    (r-p-x (- x x0))
		    (r-p-y (- y y0))
		    (dot-v (+ (* dx dx) (* dy dy)))
		    (dot-r-v (+ (* r-p-x dx) (* r-p-y dy)))
		    (dp (/ (coerce dot-r-v 'single-float)
			   (coerce dot-v 'single-float)))
		    (closest-x (+ x0 (* dp dx)))
		    (closest-y (+ y0 (* dp dy)))
		    (ax (- x closest-x))
		    (ay (- y closest-y)))
	       (values (+ (* ax ax) (* ay ay)) closest-x closest-y))))
    ;;(declare (dynamic-extent split-bezier-curve distance-from-line))
    (let ((d1 (distance-from-line x0 y0 x3 y3 x1 y1))
	  (d2 (distance-from-line x0 y0 x3 y3 x2 y2)))
      (when (or (>= d1 distance) (>= d2 distance))
	(multiple-value-bind (x00 y00 x10 y10 x20 y20 x30 y30
			      x01 y01 x11 y11 x21 y21 x31 y31)
	    (split-bezier-curve x0 y0 x1 y1 x2 y2 x3 y3)
	  (render-bezier-curve function x00 y00 x10 y10 x20 y20 x30 y30 distance)
	  (funcall function x30 y30)
	  (render-bezier-curve function x01 y01 x11 y11 x21 y21 x31 y31 distance))))))



#||

/// SET-PIXEL

define method set-pixel
    (sheet :: <basic-sheet>, color :: <rgb-color>, x, y) => (record)
  with-sheet-medium (medium = sheet)
    set-pixel(medium, color, x, y)
  end
end method set-pixel;
||#

(defmethod set-pixel ((sheet <basic-sheet>) (color <rgb-color>) x y)
  (with-sheet-medium (medium = sheet)
    (set-pixel medium color x y)))


#||
define method set-pixel 
    (sheet :: <permanent-medium-mixin>, color :: <rgb-color>, x, y) => (record)
  let medium :: <basic-medium> = sheet-medium(sheet);
  set-pixel(medium, color, x, y)
end method set-pixel;
||#

(defmethod set-pixel ((sheet <permanent-medium-mixin>) (color <rgb-color>) x y)
  (let ((medium (sheet-medium sheet)))
    (set-pixel medium color x y)))


#||
define function set-pixel* 
    (drawable :: <drawable>, color :: <rgb-color>, point :: <standard-point>) => (record)
  set-pixel(drawable, color, point-x(point), point-y(point))
end function set-pixel*;
||#

(defmethod set-pixel* ((drawable <abstract-sheet>) (color <rgb-color>) (point <standard-point>))
  (set-pixel drawable color (point-x point) (point-y point)))

(defmethod set-pixel* ((drawable <abstract-medium>) (color <rgb-color>) (point <standard-point>))
  (set-pixel drawable color (point-x point) (point-y point)))



#||

/// SET-PIXELS

define method set-pixels 
    (sheet :: <basic-sheet>, color :: <rgb-color>, coord-seq :: <coordinate-sequence>)
 => (record)
  with-sheet-medium (medium = sheet)
    set-pixels(medium, color, coord-seq)
  end
end method set-pixels;
||#

(defmethod set-pixels ((sheet <basic-sheet>) (color <rgb-color>) (coord-seq sequence))
  (with-sheet-medium (medium = sheet)
    (set-pixels medium color coord-seq)))


#||
define method set-pixels 
    (sheet :: <permanent-medium-mixin>, color :: <rgb-color>, coord-seq :: <coordinate-sequence>)
 => (record)
  let medium :: <basic-medium> = sheet-medium(sheet);
  set-pixels(medium, color, coord-seq)
end method set-pixels;
||#

(defmethod set-pixels ((sheet <permanent-medium-mixin>) (color <rgb-color>) (coord-seq sequence))
  (let ((medium (sheet-medium sheet)))
    (set-pixels medium color coord-seq)))


#||
define function set-pixels* 
    (drawable :: <drawable>, color :: <rgb-color>, points :: <point-sequence>)
 => (record)
  set-pixels(drawable, color, spread-point-sequence(points))
end function set-pixels*;
||#

(defmethod set-pixels* ((drawable <abstract-sheet>) (color <rgb-color>) (points sequence))
  (set-pixels drawable color (spread-point-sequence points)))

(defmethod set-pixels* ((drawable <abstract-medium>) (color <rgb-color>) (points sequence))
  (set-pixels drawable color (spread-point-sequence points)))


