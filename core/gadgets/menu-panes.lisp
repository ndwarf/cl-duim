;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GADGETS-INTERNALS -*-
(in-package #:duim-gadgets-internals)

#||
/// Generic implementation of <menu-box> using just buttons

// This is really a mixin class, but you can do 'make-pane' on it, so
// we'll go with the more appetizing name...
define open abstract class <menu-box-pane>
    (<gadget-box-pane-mixin>, 
     <abstract-sheet>)
end class <menu-box-pane>;
||#

(defclass <menu-box-pane>
    (<gadget-box-pane-mixin>
     <abstract-sheet>)
  ()
  (:metaclass <abstract-metaclass>))


#||
define method button-class-for-gadget-box
    (box :: <menu-box-pane>) => (class :: <class>)
  <menu-button>
end method button-class-for-gadget-box;
||#

(defmethod button-class-for-gadget-box ((box <menu-box-pane>))
  (find-class '<menu-button>))


#||
define method gadget-pane-default-layout-class
    (box :: <menu-box-pane>) => (class)
  #f
end method gadget-pane-default-layout-class;
||#

(defmethod gadget-pane-default-layout-class ((box <menu-box-pane>))
  nil)


#||
define method gadget-box-buttons-parent
    (box :: <menu-box-pane>) => (sheet :: <sheet>)
  box
end method gadget-box-buttons-parent;
||#

(defmethod gadget-box-buttons-parent ((box <menu-box-pane>))
  box)


#||
define function menu-box-pane-selection-mode-class
    (selection-mode :: <selection-mode>) => (class :: <class>)
  select (selection-mode)
    #"none"     => <push-menu-box-pane>;
    #"single"   => <radio-menu-box-pane>;
    #"multiple" => <check-menu-box-pane>;
  end
end function menu-box-pane-selection-mode-class;
||#

(defun menu-box-pane-selection-mode-class (selection-mode)
  (check-type selection-mode <selection-mode>)
  (ecase selection-mode
    (:none     (find-class '<push-menu-box-pane>))
    (:single   (find-class '<radio-menu-box-pane>))
    (:multiple (find-class '<check-menu-box-pane>))))


#||
define sealed inline method make 
    (class == <menu-box-pane>, #rest initargs,
     #key selection-mode :: <selection-mode> = #"none", #all-keys)
 => (component :: <menu-box-pane>)
  apply(make, menu-box-pane-selection-mode-class(selection-mode), initargs)
end method make;
||#

(defmethod make-pane ((class (eql (find-class '<menu-box-pane>)))
		      &rest initargs &key (selection-mode :node) &allow-other-keys)
  (apply #'make-pane (menu-box-pane-selection-mode-class selection-mode) initargs))


#||
// Relaying-out a menu box doesn't make any sense, so just do nothing
define method relayout-parent
    (sheet :: <menu-box-pane>, #key width, height)
 => (did-layout? :: <boolean>)
  ignore(width, height);
  #f
end method relayout-parent;
||#

(defmethod relayout-parent ((sheet <menu-box-pane>) &key width height)
  (declare (ignore width height))
  nil)


#||
define sealed class <push-menu-box-pane>
    (<menu-box-pane>, 
     <push-menu-box>, 
     <multiple-child-composite-pane>)
end class <push-menu-box-pane>;
||#

(defclass <push-menu-box-pane>
    (<menu-box-pane>
     <push-menu-box>
     <multiple-child-composite-pane>)
  ())


#||
define method class-for-make-pane 
    (framem :: <frame-manager>, class == <push-menu-box>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<push-menu-box-pane>, #f)
end method class-for-make-pane;

define sealed domain make (singleton(<push-menu-box-pane>));
define sealed domain initialize (<push-menu-box-pane>);
||#

(defmethod class-for-make-pane ((framem <frame-manager>) (class (eql (find-class '<push-menu-box>))) &key)
  (values (find-class '<push-menu-box-pane>) nil))


#||
define method do-execute-activate-callback
    (button :: <push-menu-button>, box :: <push-menu-box-pane>, id) => ()
  ignore(id);
  gadget-value(box) := gadget-value(button);
  execute-activate-callback(box, gadget-client(box), gadget-id(box));
  next-method()
end method do-execute-activate-callback;
||#

(defmethod do-execute-activate-callback ((button <push-menu-button>) (box <push-menu-box-pane>) id)
  (declare (ignore id))
  (setf (gadget-value box) (gadget-value button))
  (execute-activate-callback box (gadget-client box) (gadget-id box))
  (call-next-method))


#||
define sealed class <radio-menu-box-pane>
    (<menu-box-pane>,
     <radio-menu-box>,
     <multiple-child-composite-pane>)
end class <radio-menu-box-pane>;
||#

(defclass <radio-menu-box-pane>
    (<menu-box-pane>
     <radio-menu-box>
     <multiple-child-composite-pane>)
  ())


#||
define method class-for-make-pane 
    (framem :: <frame-manager>, class == <radio-menu-box>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<radio-menu-box-pane>, #f)
end method class-for-make-pane;

define sealed domain make (singleton(<radio-menu-box-pane>));
define sealed domain initialize (<radio-menu-box-pane>);
||#

(defmethod class-for-make-pane ((framem <frame-manager>) (class (eql (find-class '<radio-menu-box>))) &key)
  (values (find-class '<radio-menu-box-pane>) nil))


#||
define method do-execute-value-changed-callback
    (button :: <radio-menu-button>, box :: <radio-menu-box-pane>, id) => ()
  ignore(id);
  gadget-selection(box, do-callback?: #t)
    := if (gadget-value(button) == #t)
	 vector(gadget-box-button-index(box, button))
       else
	 #[]
       end;
  next-method()
end method do-execute-value-changed-callback;
||#

(defmethod do-execute-value-changed-callback ((button <radio-menu-button>) (box <radio-menu-box-pane>) id)
  (declare (ignore id))
  (setf (gadget-selection box :do-callback? t)
        (if (gadget-value button)  ;; == #t)
            (vector (gadget-box-button-index box button))
            #()))
  (call-next-method))


#||
define sealed class <check-menu-box-pane>
    (<menu-box-pane>,
     <check-menu-box>,
     <multiple-child-composite-pane>)
end class <check-menu-box-pane>;
||#

(defclass <check-menu-box-pane>
    (<menu-box-pane>
     <check-menu-box>
     <multiple-child-composite-pane>)
  ())


#||
define method class-for-make-pane 
    (framem :: <frame-manager>, class == <check-menu-box>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<check-menu-box-pane>, #f)
end method class-for-make-pane;

define sealed domain make (singleton(<check-menu-box-pane>));
define sealed domain initialize (<check-menu-box-pane>);
||#

(defmethod class-for-make-pane ((framem <frame-manager>) (class (eql (find-class '<check-menu-box>))) &key)
  (values (find-class '<check-menu-box-pane>) nil))


#||
define method do-execute-value-changed-callback
    (button :: <check-menu-button>, box :: <check-menu-box-pane>, id) => ()
  ignore(id);
  let index = gadget-box-button-index(box, button);
  let new-selection
    = if (gadget-value(button) == #t)
	add-new(gadget-selection(box), index)
      else
	remove(gadget-selection(box), index)
      end;
  gadget-selection(box, do-callback?: #t) := new-selection;
  next-method()
end method do-execute-value-changed-callback;
||#

(defmethod do-execute-value-changed-callback ((button <check-menu-button>) (box <check-menu-box-pane>) id)
  (declare (ignore id))
  ;; There's some stuff to fix in here; hence the debug line.
  (duim-debug-message "DO-EXECUTE-VALUE-CHANGED-CALLBACK for ~a" box)
  (let* ((index (gadget-box-button-index box button))
         (new-selection (if (gadget-value button)   ;;== #t)
			    (add-new (gadget-selection box) index)
                            (remove index (gadget-selection box)))))
    (duim-debug-message " + got new-selection=~a" new-selection)
    (setf (gadget-selection box :do-callback? t) new-selection))
  (call-next-method))


;;; TODO: Need to write documentation strings for every generic function
;;; specified, even if all that doc. string says is which protocol the
;;; gf participates in.
