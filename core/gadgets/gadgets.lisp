;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GADGETS-INTERNALS -*-
(in-package #:duim-gadgets-internals)

#||
/// Labels

// Abstract label
define open abstract class <label>
    (<labelled-gadget-mixin>,
     <basic-gadget>)
  keyword accepts-focus?: = #f;
  keyword tab-stop?:      = #f;
end class <label>;
||#

(defclass <label>
    (<labelled-gadget-mixin> <basic-gadget>)
  ()
  (:default-initargs :accepts-focus? nil :tab-stop? nil)
  (:documentation
"
The class of label gadgets.

The :label initarg specifies a string or image that is to be used as a
label for the gadget. If you use an image, you should be wary of
its size; only use images that are the size of a typical icon.

Internally, this class maps into the Windows static control.

Example:

    (contain (make-pane '<label> :label \"Hello\"))
")
  (:metaclass <abstract-metaclass>))


(defgeneric multi-line-label? (label))
(defgeneric handle-button-gadget-click (gadget &key double-click?))
(defgeneric slider-show-value? (slider))


#||
define method initialize
    (gadget :: <label>, #key multi-line? = #f) => ()
  next-method();
  let bit = if (multi-line?) %multi_line_label else 0 end;
  gadget-flags(gadget)
    := logior(logand(gadget-flags(gadget), lognot(%multi_line_label)), bit)
end method initialize;
||#

(defmethod initialize-instance :after ((gadget <label>) &key (multi-line? nil) &allow-other-keys)
  (let ((bit (if multi-line? %multi_line_label 0)))
    (setf (gadget-flags gadget)
	  (logior (logand (gadget-flags gadget) (lognot %multi_line_label)) bit))))


#||
define sealed inline method multi-line-label?
    (gadget :: <label>) => (multi-line? :: <boolean>)
  logand(gadget-flags(gadget), %multi_line_label) = %multi_line_label
end method multi-line-label?;
||#

(defmethod multi-line-label? ((gadget <label>))
  (= (logand (gadget-flags gadget) %multi_line_label) %multi_line_label))


#||
// A <label> is always enabled
define sealed method gadget-enabled?
    (label :: <label>) => (enabled? :: <boolean>)
  #t
end method gadget-enabled?;
||#

(defmethod gadget-enabled? ((label <label>))
  t)


#||
define sealed method gadget-enabled?-setter
    (enabled? :: <boolean>, label :: <label>) => (enabled? :: <boolean>)
  enabled?
end method gadget-enabled?-setter;
||#

(defmethod (setf gadget-enabled?) (enabled? (label <label>))
  (declare (type boolean enabled?))
  enabled?)


#||
define macro labelling
  { labelling (?label:expression)
      ?contents:body
    end }
    => { let _contents = ?contents;	// contents is a single expression
	 horizontally (x-spacing: 2, y-alignment: #"center")
           make(<label>, label: ?label);
           _contents
         end }
  { labelling (?label:expression, #rest ?options:expression)
      ?contents:body
    end }
    => { let _contents = ?contents;	// contents is a single expression
	 horizontally (?options, x-spacing: 2, y-alignment: #"center")
           make(<label>, label: ?label);
           _contents
         end }
end macro labelling;
||#

(defmacro labelling ((label &rest options) &body contents)
"
Creates a pane with a label assigned to it, taking into account any of
the specified _options_.

The options specified may be any of the legal initargs used to specify
an instance of <LABEL>. If no options are specified, then the default
label is used.

The _contents_ is an expression whose return value is the sheet to
which the label should be assigned.

Example:

    (labelling (\"Colour Type:\")
      (make-pane '<check-box> :items #(\"Colour\" \"Monochrome\")))

TODO: Check this example works!
"
  (let ((_label (gensym "LABEL-"))
	(_contents (gensym "CONTENTS-")))
  `(let ((,_label ,label)
	 (,_contents ,@contents))  ;; contents is a single expression
     (horizontally (,@(when options options) :x-spacing 2 :y-alignment :center)
       (make-pane '<label> :label ,_label)
       ,_contents))))


#||
// Everyone always spells this "wrong", so be forgiving
define macro labeling
  { labeling (?label:expression)
      ?contents:*
    end }
    => { labelling (?label)
           ?contents
         end }
  { labeling (?label:expression, #rest ?options:expression)
      ?contents:*
    end }
    => { labelling (?label, ?options)
           ?contents
         end }
end macro labeling;
||#

(defmacro labeling ((label &rest options) &body contents)
  (let ((_label (gensym "LABEL-"))
	(_contents (gensym "CONTENTS-")))
  `(let ((,_label ,label)
	 (,_contents ,@contents))  ;; contents is a single expression
     (horizontally (,@(when options options) :x-spacing 2 :y-alignment :center)
       (make-pane '<label> :label ,_label)
       ,_contents))))


#||
// Default label implementation
define sealed class <label-pane> (<label>, <simple-pane>)
end class <label-pane>;
||#

(defclass <label-pane> (<label> <simple-pane>) ()
  (:documentation
"
Default label implementation.
"))


#||
define method class-for-make-pane
    (framem :: <frame-manager>, class == <label>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<label-pane>, #f)
end method class-for-make-pane;

define sealed domain make (singleton(<label-pane>));
define sealed domain initialize (<label-pane>);
||#

(defmethod class-for-make-pane ((framem <frame-manager>) (class (eql (find-class '<label>))) &key)
  (values (find-class '<label-pane>) nil))


#||
define method do-compose-space
    (pane :: <label-pane>, #key width, height)
 => (space-requirement :: <space-requirement>)
  ignore(width, height);
  let (width, height) = gadget-label-size(pane);
  make(<space-requirement>,
       width: width, height: height)
end method do-compose-space;
||#

(defmethod do-compose-space ((pane <label-pane>) &key width height)
  (declare (ignore width height))
  (multiple-value-bind (width height)
      (gadget-label-size pane)
    (make-space-requirement :width width :height height)))


#||
define method handle-repaint
    (sheet :: <label-pane>, medium :: <medium>, region :: <region>) => ()
  draw-gadget-label(sheet, medium, 0, 0)
end method handle-repaint;
||#

(defmethod handle-repaint ((sheet <label-pane>) (medium <medium>) (region <region>))
  (draw-gadget-label sheet medium 0 0))



#||

/// Buttons

define open abstract class <button>
    (<accelerator-mixin>,
     <mnemonic-mixin>,
     <labelled-gadget-mixin>,
     <gadget-command-mixin>,
     <value-gadget>)
end class <button>;
||#

(defclass <button>
    (<accelerator-mixin>
     <mnemonic-mixin>
     <labelled-gadget-mixin>
     <gadget-command-mixin>
     <value-gadget>)
  ()
  (:documentation
"
The class of all button gadgets.

The :accelerator initarg is used to specify a keyboard accelerator for
the button. This is a key press that gives the user a method of
activating the button using a short key sequence rather than by
clicking the button itself. Keyboard accelerators usually combine the
CONTROL and possibly SHIFT keys with an alphanumeric character.

When choosing accelerators, you should be aware of the style
guidelines that might be applicable for the operating system you are
developing for. For example, a common accelerator for the command
'File > Open' in Windows is CTRL+O.

Keyboard accelerators are mostly used in menu buttons, though they can
be applied to other forms of button as well.

The :mnemonic initarg is used to specify a keyboard mnemonic for the
button. This is a key press that involves pressing the ALT key
followed by a number of alphanumeric keys.

Note that the choice of keys is more restrictive than for keyboard
accelerators. They are determined in part by the names of the button
itself (and, in the case of menu buttons, the menu that contains it),
as well as by any appropriate style guidelines. For example, a common
mnemonic for the 'File > Open' command is ALT, F, O.

Mnemonics have the advantage that the letters forming the mnemonic are
automatically underlined in the button label on the screen (and, for
menu buttons, the menu itself). This means that they do not have to be
remembered. In addition, when the user makes use of a mnemonic in a
menu, the menu itself is displayed on screen, as if the command had
been chosen using the mouse. This does not happen if the keyboard
accelerator is used.

Buttons are intrinsically \"non-stretchy\" gadgets. That is, the width
and height of a button is generally calculated on the basis of the
button's label, and the button will be sized so that it fits the label
accordingly. Sometimes, however, you want a button to occupy all the
available space that is given it, however large that space may be. To
force a button to use all the available width and height,
specify :max-width +fill+ or :max-height +fill+ accordingly in the
button definition. See the second example below to see how this is
done.

Example:

    (contain
      (make-pane '<button> :label \"Hello\"
                 :activate-callback
                 #'(lambda (gadget)
                     (notify-user
                       (format nil \"Pressed button ~a\" gadget)
                       :owner gadget))))

The following example creates a column layout that contains two
elements.

    + The first is a row layout that itself contains two buttons
      with short labels.
    + The second is a button with a long label.

The use of :equalize-widths? in the call to VERTICALLY ensures that
these two elements have the same width.

The interesting part of this example is the use of :max-width +fill+
in the definition of the buttons with shorter labels. If this was not
used, then each button would be sized such that it just fit its own
label, and there would be empty space in the row layout. However,
using :max-width +fill+ ensures that each button is made as large as
possible, so as to fit the entire width of the row layout.

    (contain
      (vertically (:equalize-widths? t)
        (horizontally ()
          (make-pane '<button> :label \"Red\" :max-width +fill+)
          (make-pane '<button> :label \"Ultraviolet\"
                     :max-width +fill+))
        (make-pane '<button> :label
                   \"A button with a really really long label\")))
")
  (:metaclass <abstract-metaclass>))


#||
define method handle-button-gadget-click
    (gadget :: <basic-gadget>, #key double-click? = #f)
 => (handled? :: <boolean>)
  select (gadget-selection-mode(gadget))
    #"none" =>
      distribute-activate-callback(gadget);
    #"single" =>
      // You can only turn single-selection buttons on by clicking
      // on them; they go off when you click on some other button
      unless (gadget-value(gadget))
	distribute-value-changed-callback(gadget, #t);
	when (double-click?)
	  distribute-activate-callback(gadget)
	end;
      end;
    #"multiple" =>
      //---*** 'gadget-value' won't be up to date if we distribute
      //---*** two of these before handling any of them
      distribute-value-changed-callback(gadget, ~gadget-value(gadget));
      when (double-click?)
	distribute-activate-callback(gadget)
      end;
  end;
  #t
end method handle-button-gadget-click;
||#

(defmethod handle-button-gadget-click ((gadget <basic-gadget>) &key (double-click? nil))
  (ecase (gadget-selection-mode gadget)
    (:none (distribute-activate-callback gadget))
    (:single
     ;; You can only turn single-selection buttons on by clicking
     ;; on them; they go off when you click on some other button
     (unless (gadget-value gadget)
       (distribute-value-changed-callback gadget t)
       (when double-click?
	 (distribute-activate-callback gadget))))
    (:multiple
     ;;---*** 'gadget-value' won't be up to date if we distribute
     ;;---*** two of these before handling any of them
     (distribute-value-changed-callback gadget (not (gadget-value gadget)))
     (when double-click?
       (distribute-activate-callback gadget))))
  t)


#||
define function button-selection-mode-class
    (selection-mode :: <selection-mode>) => (class :: <class>)
  select (selection-mode)
    #"none"     => <push-button>;
    #"single"   => <radio-button>;
    #"multiple" => <check-button>;
  end
end function button-selection-mode-class;
||#

(defun button-selection-mode-class (selection-mode) ;; <selection-mode>))
  (check-type selection-mode <selection-mode>)
  (ecase selection-mode
    (:none     (find-class '<push-button>))
    (:single   (find-class '<radio-button>))
    (:multiple (find-class '<check-button>))))


#||
define sealed inline method make
    (class == <button>, #rest initargs,
     #key selection-mode :: <selection-mode> = #"none", #all-keys)
 => (button :: <button>)
  apply(make, button-selection-mode-class(selection-mode), initargs)
end method make;
||#

(defmethod make-pane ((class (eql (find-class '<button>)))
		      &rest initargs &key (selection-mode :none) &allow-other-keys)
  (apply #'make-pane (button-selection-mode-class selection-mode) initargs))


#||
// Inside of things like menus, we want to be able to get the value of
// a button.  When the button is inside some kind of a button box, its
// value is the value of the client box.
define method button-gadget-value (button :: <button>) => (value)
  select (gadget-selection-mode(button))
    #"none" =>
      gadget-value(button);
    #"single", #"multiple" =>
      let client = gadget-client(button);
      when (client)
        gadget-value(client)
      end
  end
end method button-gadget-value;
||#

(defmethod button-gadget-value ((button <button>))
  (ecase (gadget-selection-mode button)
    (:none (gadget-value button))
    ((:single :multiple)
     (let ((client (gadget-client button)))
       (when client
	 (gadget-value client))))))


#||
/// Push buttons

define open abstract class <push-button>
    (<button>, <default-gadget-mixin>, <basic-value-gadget>)
end class <push-button>;
||#

(defclass <push-button>
    (<button>
     <default-gadget-mixin>
     <basic-value-gadget>)
  ()
  (:documentation
"
The class of push buttons. The push button gadget provides
press-to-activate switch behaviour.

When the button is activated (by releasing the pointer button over
it), its activate callback is invoked.

If you supply a GADGET-VALUE for a push button, this can be used by
any callback defined on the push button. This is especially useful in
the case of push boxes, where this value can be used to test which
button in the push box has been pressed.

The :default? initarg sets the default property for the push button
gadget. When true, the push button is drawn with a heavy border,
indicating that it is the \"default operation\" for that
frame. Usually, this means that pressing the Return key invokes the
activate callback.

Internally, this class maps into the push button Windows control.

Example:

The following code creates a push button which, when clicked, displays
a message showing the label of the button.

    (contain
      (make-pane '<push-button>
                 :label \"Hello\"
                 :activate-callback
                 #'(lambda (gadgt)
                     (notify-user
                       (format nil \"Pressed button ~a\"
                               (gadget-label gadget))
                       :owner gadget))))
")
  (:metaclass <abstract-metaclass>))

#||
define method initialize
    (button :: <push-button>, #key) => ()
  next-method();
  when (gadget-command(button) & ~gadget-activate-callback(button))
    gadget-activate-callback(button) := callback-for-command(gadget-command(button))
  end
end method initialize;
||#

(defmethod initialize-instance :after ((button <push-button>) &key &allow-other-keys)
  (when (and (gadget-command button) (not (gadget-activate-callback button)))
    (setf (gadget-activate-callback button) (callback-for-command (gadget-command button)))))


#||
define sealed method gadget-selection-mode
    (button :: <push-button>) => (selection-mode :: <selection-mode>)
  #"none"
end method gadget-selection-mode;
||#

(defmethod gadget-selection-mode ((button <push-button>))
  :none)


#||
/// Radio buttons and check buttons

// A button in a "one-of" panel
define open abstract class <radio-button>
    (<button>, <action-gadget-mixin>, <basic-value-gadget>)
end class <radio-button>;
||#

(defclass <radio-button>
    (<button>
     <action-gadget-mixin>
     <basic-value-gadget>)
  ()
  (:documentation
"

The class of radio buttons. Isolated radio buttons are of limited use;
you will normally want to combine several instances of such buttons
using the <RADIO-BOX> gadget.

Internally, this class maps into the radio button Windows control.

Example:

    (contain (make-pane '<radio-button> :label \"Hello\"))
")
  (:metaclass <abstract-metaclass>))


#||
define sealed method gadget-selection-mode
    (button :: <radio-button>) => (selection-mode :: <selection-mode>)
  #"single"
end method gadget-selection-mode;
||#

(defmethod gadget-selection-mode ((button <radio-button>))
  :single)


#||
// A button in a "some-of" panel
define open abstract class <check-button>
    (<button>, <action-gadget-mixin>, <basic-value-gadget>)
end class <check-button>;
||#

(defclass <check-button>
    (<button>
     <action-gadget-mixin>
     <basic-value-gadget>)
  ()
  (:documentation
"

The class of check buttons. The value of a check button is either T or
NIL, depending on whether or not it is currently selected.

Internally, this class maps into the check box Windows control. (!)

Example:

    (contain (make-pane '<check-button> :label \"Check button\"))
")
  (:metaclass <abstract-metaclass>))


#||
define sealed method gadget-selection-mode
    (button :: <check-button>) => (selection-mode :: <selection-mode>)
  #"multiple"
end method gadget-selection-mode;
||#

(defmethod gadget-selection-mode ((button <check-button>))
  :multiple)



#||

/// Sliders

// Sliders generate a value-changing callback as the slider is being moved,
// and a value-changed callback once the slider is released
define open abstract class <slider>
    (<oriented-gadget-mixin>,
     <range-gadget-mixin>,
     <labelled-gadget-mixin>,
     <bordered-gadget-mixin>,
     <changing-value-gadget-mixin>,
     <basic-value-gadget>)
  sealed constant slot slider-tick-marks :: false-or(<integer>) = #f,
    init-keyword: tick-marks:;
  // These ones aren't natively supported on Windows
  sealed slot slider-decimal-places :: <integer> = 0,
    init-keyword: decimal-places:;
  sealed slot slider-min-label = #f,
    init-keyword: min-label:;
  sealed slot slider-max-label = #f,
    init-keyword: max-label:;
end class <slider>;
||#

(defclass <slider>
    (<oriented-gadget-mixin>
     <range-gadget-mixin>
     <labelled-gadget-mixin>
     <bordered-gadget-mixin>
     <changing-value-gadget-mixin>
     <basic-value-gadget>)
  ((slider-tick-marks :type (or null integer) :initarg :tick-marks :initform nil :reader slider-tick-marks)
   ;; These ones aren't natively supported on Windows
   (slider-decimal-places :type integer :initarg :decimal-places :initform 0 :accessor slider-decimal-places)
   (slider-min-label :initarg :min-label :initform nil :accessor slider-min-label)
   (slider-max-label :initarg :max-label :initform nil :accessor slider-max-label))
  (:documentation
"
The class of slider gadgets. This is a gadget used for setting or
adjusting the value on a continuous range of values, such as a volume
or brightness control.

You can specify a number of attributes for the labels in a
slider. The :min-label and :max-label initargs let you specify a label
to be displayed at the minimum and maximum points of the slider bar,
respectively. In addition, the :range-label-text-style initarg lets
you specify a text style for these labels.

The :borders initarg lets you specify a border around the slider. If
specified, a border of the appropriate type is drawn around the
gadget.

The :tick-marks initarg specifies the number of tick-marks that are
shown on the slider. Displaying tick marks gives the user a better
notion of the position of the slug at any time.

The :orientation initarg specifies whether the slider is horizontal or
vertical.

The :value-changing-callback initarg is the callback that is invoked
when the slider slug is dragged.

Internally, this class maps onto the Windows trackbar control.

When designing a user interface, you will find that sliders are a
suitable alternative to spin boxes in many situations.

Example:

    (contain (make-pane '<slider>
                        :value-range (range :from -20 :to 20 :by 5)))
")
  (:metaclass <abstract-metaclass>))


#||
define method initialize
    (slider :: <slider>,
     #key decimal-places = $unsupplied, show-value? = #t) => ()
  next-method();
  let bit = if (show-value?) %show_value else 0 end;
  gadget-flags(slider)
    := logior(logand(gadget-flags(slider), lognot(%show_value)), bit);
  when (unsupplied?(decimal-places))
    let slider-range = range-values(gadget-value-range(slider));
    case
      slider-range <=  1 =>
	slider-decimal-places(slider) := 2;
      slider-range <= 10 =>
	slider-decimal-places(slider) := 1;
    end
  end
end method initialize;
||#

;; FIXME: DOES THIS NEED DECIMAL-PLACES = 0 SPECIFYING FOR RANGES THAT ARE LARGER THAN 10?
(defmethod initialize-instance :after ((slider <slider>)
				       &key (decimal-places :unsupplied) (show-value? t)
				       &allow-other-keys)
  (let ((bit (if show-value? %show_value 0)))
    (setf (gadget-flags slider)
	  (logior (logand (gadget-flags slider) (lognot %show_value)) bit))
    (when (unsupplied? decimal-places)
      (let ((slider-range (range-values (gadget-value-range slider))))
        (cond ((<= slider-range 1) (setf (slider-decimal-places slider) 2))
              ((<= slider-range 10) (setf (slider-decimal-places slider) 1)))))))


#||
define sealed inline method slider-show-value?
    (slider :: <slider>) => (default? :: <boolean>)
  logand(gadget-flags(slider), %show_value) = %show_value
end method slider-show-value?;
||#

(defmethod slider-show-value? ((slider <slider>))
  (= (logand (gadget-flags slider) %show_value) %show_value))



#||

/// Progress controls

define open abstract class <progress-bar> 
    (<oriented-gadget-mixin>, 
     <range-gadget-mixin>,
     <basic-value-gadget>)
  keyword accepts-focus?: = #f;
  keyword tab-stop?:      = #f;
end class <progress-bar>;
||#

(defclass <progress-bar>
    (<oriented-gadget-mixin>
     <range-gadget-mixin>
     <basic-value-gadget>)
  ()
  (:default-initargs :accepts-focus? nil :tab-stop? nil)
  (:documentation
"
The class of progress bar windows.

The :orientation initarg lets you specify whether the progress bar
should be horizontal or vertical.

Internally, this class maps into the Windows progress indicator
control.

Example:

The following code creates an \"empty\" progress bar:

    (defparameter *prog*
                  (contain (make-pane '<progress-bar>
                                      :value-range
                                      (range :from 0 :to 100))))

By setting the gadget value of the progress bar, the progress of a
task can be monitored as follows:

    (loop for i from 0 to 100
          do (setf (gadget-value *prog*) i))
")
  (:metaclass <abstract-metaclass>))


#||
define method initialize (pane :: <progress-bar>, #key value-range)
  next-method();
  unless (value-range)
    gadget-value-range(pane) := range(from: 0, to: 100)
  end;
  unless (gadget-value(pane))
    gadget-value(pane) := gadget-value-range(pane)[0]
  end
end method initialize;
||#

(defmethod initialize-instance :after ((pane <progress-bar>)
				       &key value-range &allow-other-keys)
  (unless value-range
    (setf (gadget-value-range pane)
	  (range :from 0 :to 100)))
  (unless (gadget-value pane)
    (setf (gadget-value pane) (elt (gadget-value-range pane) 0))))



#||

/// Separator panes

define open abstract class <separator> 
    (<oriented-gadget-mixin>, 
     <no-value-gadget-mixin>,
     <basic-gadget>)
  keyword accepts-focus?: = #f;
  keyword tab-stop?:      = #f;
end class <separator>;
||#

(defclass <separator>
    (<oriented-gadget-mixin>
     <no-value-gadget-mixin>
     <basic-gadget>)
  ()
  (:default-initargs :accepts-focus? nil :tab-stop? nil)
  (:documentation
"
The class of gadgets used as a visual separator.

The :orientation initarg specifies whether the separator is vertical
or horizontal.

Example:

The following example creates a column layout and places two buttons
into it, separated with a separator.

    (contain (vertically ()
               (make-pane '<button> :label \"Hello\")
               (make-pane '<separator>)
               (make-pane '<button> :label \"World\")))
")
  (:metaclass <abstract-metaclass>))


#||
define sealed class <separator-pane> 
    (<separator>,
     <drawing-pane>)
end class <separator-pane>;
||#

(defclass <separator-pane> (<separator> <drawing-pane>) ())


#||
define method class-for-make-pane
    (framem :: <frame-manager>, class == <separator>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<separator-pane>, #f)
end method class-for-make-pane;

define sealed domain make (singleton(<separator-pane>));
define sealed domain initialize (<separator-pane>);
||#

(defmethod class-for-make-pane ((framem <frame-manager>) (class (eql (find-class '<separator>))) &key)
  (values (find-class '<separator-pane>) nil))


#||
define sealed method do-compose-space
    (pane :: <separator-pane>, #key width, height)
 => (space-requirement :: <space-requirement>)
  select (gadget-orientation(pane))
    #"horizontal" =>
      make(<space-requirement>,
	   min-width: 1, width: width | 1, max-width: $fill,
	   height: 2);
    #"vertical" =>
      make(<space-requirement>,
	   width: 2,
	   min-height: 1, height: height | 1, max-height: $fill);
  end
end method do-compose-space;
||#

(defmethod do-compose-space ((pane <separator-pane>) &key width height)
  (ecase (gadget-orientation pane)
    (:horizontal (make-space-requirement :min-width 1
					 :width (or width 1)
					 :max-width +fill+
					 :height 2))
    (:vertical (make-space-requirement :width 2
				       :min-height 1
				       :height (or height 1)
				       :max-height +fill+))))


#||
define sealed method handle-repaint
    (pane :: <separator-pane>, medium :: <medium>, region :: <region>) => ()
  ignore(region);	// not worth checking
  let (left, top, right, bottom) = box-edges(pane);
  draw-separator(pane, medium, gadget-orientation(pane), left, top, right, bottom)
end method handle-repaint;
||#

(defmethod handle-repaint ((pane <separator-pane>) (medium <medium>) (region <region>))
  (declare (ignore region))	;; not worth checking
  (multiple-value-bind (left top right bottom)
      (box-edges pane)
    (draw-separator pane medium (gadget-orientation pane) left top right bottom)))


#||
define open generic draw-separator
    (sheet :: <abstract-sheet>, medium :: <abstract-medium>, orientation :: <gadget-orientation>,
     left  :: <integer>, top    :: <integer>,
     right :: <integer>, bottom :: <integer>,
     #key type :: <border-type>) => ();
||#

(defgeneric draw-separator (sheet medium orientation left top right bottom &key type))


#||
define method draw-separator
    (sheet :: <sheet>, medium :: <medium>, orientation :: <gadget-orientation>,
     left  :: <integer>, top    :: <integer>,
     right :: <integer>, bottom :: <integer>, #key type :: <border-type> = #f) => ()
  ignore(type);
  with-drawing-options (medium, brush: default-foreground(sheet))
    select (orientation)
      #"horizontal" =>
        let bottom = top + 1;
	draw-rectangle(medium, left, top, right, bottom, filled?: #t);
      #"vertical" =>
        let right = left + 1;
	draw-rectangle(medium, left, top, right, bottom, filled?: #t);
    end
  end
end method draw-separator;
||#

(defmethod draw-separator ((sheet <sheet>) (medium <medium>) orientation
                           (left integer) (top integer)
			   (right integer) (bottom integer)
                           &key (type nil))
  (declare (ignore type))
  (check-type orientation <gadget-orientation>)
  (with-drawing-options (medium :brush (default-foreground sheet))
    (ecase orientation
      (:horizontal (let ((bottom (+ top 1)))
		     (draw-rectangle medium left top right bottom :filled? t)))
      (:vertical (let ((right (+ left 1)))
		   (draw-rectangle medium left top right bottom :filled? t))))))



#||

/// Pages

define open abstract class <page> (<gadget>)
end class <page>;
||#

(defclass <page> (<gadget>)
  ()
  (:documentation
"
The class that represents a page in a multi-page frame, such as a tab
control or wizard frame or property frame.

The :label initarg specifies a string or icon that is to be used as a
label for the gadget. Pages typically appear inside a tab control,
where the label for the page becomes the label on the tab for the
page.
")
  (:metaclass <abstract-metaclass>))


#||
define open abstract class <basic-page>
    (<labelled-gadget-mixin>, 
     <page>,
     <basic-gadget>)
  sealed slot page-initial-focus :: false-or(<sheet>) = #f,
    init-keyword: input-focus:;
end class <basic-page>;
||#

(defclass <basic-page>
    (<labelled-gadget-mixin>
     <page>
     <basic-gadget>)
  ((page-initial-focus :type (or null <sheet>) :initarg :input-focus :initform nil :accessor page-initial-focus))
  (:metaclass <abstract-metaclass>))


#||
// Default method, because we allow property sheets to contain non-<page> objects
define method page-initial-focus
    (sheet :: <sheet>) => (focus :: false-or(<sheet>))
  #f
end method page-initial-focus;
||#

(defmethod page-initial-focus ((sheet <sheet>))
  nil)

