;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GADGETS-INTERNALS -*-
(in-package #:duim-gadgets-internals)

#||
/// List controls

define constant <list-control-view>
    = one-of(#"list", #"small-icon", #"large-icon");
||#

(deftype <list-control-view> ()
"
This type represents the acceptable values for the view arguments to
operators of <LIST-CONTROL>. You should not attempt to redefine this
type in any way.

There are three possible values, corresponding to the view options
that will be familiar to most users of GUI-based operating systems:

  :small-icon   Displays each item in the list control using a small
                icon to the left of the item. Items are arranged
                horizontally.

  :large-icon   Displays each item in the list control using a large
                icon to the left of the item. Items are arranged
                horizontally.

  :list         Displays each item in the list control using a small
                icon to the left of the item. Items are arranged
                vertically in one column.
"
  '(member :list :small-icon :large-icon))


#||
// The items in a list control are layed out in one or more columns
// Note that items are not modelled as sheets!
define open abstract class <list-control>
    (<bordered-gadget-mixin>,
     <scrolling-gadget-mixin>,
     <action-gadget-mixin>,
     <key-press-gadget-mixin>,
     <popup-menu-gadget-mixin>,
     <basic-choice-gadget>)
  sealed constant slot gadget-lines :: false-or(<integer>) = #f,
    init-keyword: lines:;
  sealed slot list-control-view :: <list-control-view> = #"list",
    init-keyword: view:,
    setter: %list-control-view-setter;
  // Takes an object, produces two icons -- a small and a large icon
  sealed constant slot list-control-icon-function :: false-or(<function>) = #f,
    init-keyword: icon-function:;
end class <list-control>;
||#

;; The items in a list control are layed out in one or more columns
;; Note that items are not modelled as sheets!
(defclass <list-control>
    (<bordered-gadget-mixin>
     <scrolling-gadget-mixin>
     <action-gadget-mixin>
     <key-press-gadget-mixin>
     <popup-menu-gadget-mixin>
     <basic-choice-gadget>)
  ((gadget-lines :type (or null integer) :initarg :lines :initform nil :reader gadget-lines)
   (list-control-view :type <list-control-view> :initarg :view :initform :list :reader list-control-view
		      :writer %list-control-view-setter)
   ;; Takes an object, produces two icons -- a small and a large icon
   (list-control-icon-function :type (or null function) :initarg :icon-function :initform nil :reader list-control-icon-function))
  (:documentation
"
The class of list controls. These are controls that can list items in
a number of different ways, using a richer format than the <LIST-BOX>
class. Examples of list controls are the main panels in the Windows
Explorer, or the Macintosh Finder. List controls can also be seen in
the standard Windows 95 Open File dialog box.

The :icon-function initarg lets you specify a function to supply icons
for display in the control. The function is called with the item that
needs an icon as its argument, and it should return an instance of
<IMAGE> as its result. Typically, you might want to define an icon
function that returns a different icon for each kind of item in the
control. For example, if the control is used to display the files and
directories on a hard disk, you would want to return the appropriate
icon for each registered file type.

The :view initarg can be used to specify the way in which the items in
the list box are displayed. There are three options, corresponding to
the view options that will be most familiar to most users of GUI-based
operating systems.

The :borders initarg lets you specify a border around the list
control. If specified, a border of the appropriate type is drawn
around the gadget.

The :scroll-bars initarg lets you specify the presence of scroll bars
around the gadget. By default, both horizontal and vertical scroll
bars are created. You can also force the creation of only horizontal
or vertical scroll bars, or you can create scroll bars dynamically;
that is, have them created only if necessary, dependent on the size of
the gadget. If :scroll-bars is NIL, no scroll bars are added to the
gadget.

You can use the :popup-menu-callback initarg to specify a
context-sensitive menu to display for one or more selected items in
the list control. In Windows 95, for instance, such a
context-sensitive menu can be displayed by right-clicking on any item
or group of selected items in the list control.

The :key-press-callback initarg lets you specify a key-press
callback. This type of callback is invoked whenever a key on the
keyboard is pressed while the gadget has focus. See
GADGET-KEY-PRESS-CALLBACK for a fuller description of key-press
callbacks.

Internally, this class maps into the Windows list view control.
")
  (:metaclass <abstract-metaclass>))

;;; TODO: Where to add the documentation for 'list-control-icon-function':
#||
Returns the icon-function for _list-control_. This function lets you
specify which icon to display for each item in the control. The
function is called with the item that needs an icon as its argument,
and it should return an instance of <IMAGE> as its result. Typically,
you might want to define an icon function that returns a different
icon for each kind of item in the control. For example, if the control
is used to display the files and directories on a hard disk, you would
want to return the appropriate icon for each registered file type.

Note that, unlike tree controls, the icon function for a list control
can be changed once the list control has been created.

TODO: Actually, the icon function *can't* be changed. There's no
setter and the slot is defined as constant! If this changes, need to
lift the documentation on LIST-CONTROL-ICON-FUNCTION-SETTER out of the
DUIM library reference.
||#

;;; TODO: Where should the documentation for 'list-control-view' go?
#||
Returns the view for _list-control_. The view defines how items in the
list control are displayed. Three views are available; items are
accompanied either by a small icon or a large icon. In addition, items
can be listed vertically, and additional details can be displayed for
each item. For more details, see the description for
<LIST-CONTROL-VIEW>.

Example:

Given a list control created with the following code:

    (defparameter *list*
                  (contain (make-pane '<list-control>
                                      :items #(#(\"One\" :one)
                                               #(\"Two\" :two)
                                               #(\"Three\" :three))
                                      :view :list
                                      :scroll-bars nil)))

The list control view may be returned with:

    (list-control-view *list*)
||#
#|| define method initialize
    (pane :: <list-control>,
     #rest initargs, #key items = #[]) => ()
  // Ensure the items are in a stretchy vector so we can use 'add!' and 'remove!'
  apply(next-method, pane, items: as(<stretchy-vector>, items), initargs)
end method initialize;
||#

;; This is kept on the PRIMARY method also. We want the items to be in a
;; stretchy vector (array-with-fill-pointer) for all the :after methods
;; on init-instance.
(defmethod initialize-instance ((pane <list-control>)
				&rest initargs &key (items #()) &allow-other-keys)
  ;; Ensure the items are in a stretchy vector so we can use 'add!'
  ;; and 'remove!'
  (apply #'call-next-method  pane :items (MAKE-STRETCHY-VECTOR :contents items) initargs))


#||
define method gadget-items-setter
    (items :: <sequence>, gadget :: <list-control>)
 => (items :: <sequence>)
  // Ensure the items are in a stretchy vector so we can use 'add!' and 'remove!'
  next-method(as(<stretchy-vector>, items), gadget)
end method gadget-items-setter;
||#

(defmethod (setf gadget-items) ((items sequence) (gadget <list-control>))
  ;; Ensure the items are in a stretchy vector so we can use 'add!' and 'remove!'
  (call-next-method (MAKE-STRETCHY-VECTOR :contents items) gadget))


#||
// The superclass of list and table items and tree nodes...
define open abstract class <item> (<object>)
end class <item>;
||#

(defclass <item> ()
  ()
  (:metaclass <abstract-metaclass>))


#||
define protocol <<item>> ()
  getter item-object
    (item :: <item>) => (object);
  getter item-label
    (item :: <item>) => (label :: false-or(<string>));
  setter item-label-setter
    (label :: false-or(<string>), item :: <item>) => (label :: false-or(<string>));
  getter item-icon
    (item :: <item>) => (icon :: false-or(<image>));
  setter item-icon-setter
    (icon :: false-or(<image>), item :: <item>) => (icon :: false-or(<image>));
end protocol <<item>>;
||#
#||
(define-protocol <<item>> ()
  (:getter item-object (item))
  (:getter item-label (item))
  (:setter (setf item-label) (label item))
  (:getter item-icon (item))
  (:setter (setf item-icon) (icon item)))
||#


#||
define open abstract class <list-item> (<item>)
  sealed constant slot item-object = #f,
    init-keyword: object:;
end class <list-item>;
||#

(defclass <list-item> (<item>)
  ((item-object :initarg :object :initform nil :reader item-object))
  (:documentation
"
The class that represents an item in a list control.
")
  (:metaclass <abstract-metaclass>))


#||
// This protocol is shared by list controls and table controls
define protocol <<list-control>> ()
  function make-item (control, object, #key, #all-keys) => (item);
  function find-item (control, object, #key, #all-keys) => (item);
  function add-item  (control, item, #key after) => ();
  function remove-item (control, item) => ();
  // Back-end function to make and find an item
  function do-make-item (control, item-class, #key, #all-keys) => (item);
  function do-find-item (control, object, #key, #all-keys) => (item);
  // The concrete implementation should add the list item to the
  // list control, and then re-layout and redisplay the list control
  function do-add-item (control, item, #key after) => ();
  // The concrete implementation should remove the list item from the
  // list control, and then re-layout and redisplay the list control
  function do-remove-item (control, item) => ();
  // Views, etc
  getter list-control-view
    (list-control :: <list-control>) => (view :: <list-control-view>);
  setter list-control-view-setter
    (view :: <list-control-view>, list-control :: <list-control>)
 => (view :: <list-control-view>);
end protocol <<list-control>>;
||#

#||
(define-protocol <<list-control>> ()
  (:function make-item (control object &key &allow-other-keys))
  (:function find-item (control object &key &allow-other-keys))
  (:function add-item (control item &key after))
  (:function remove-item (control item))
  ;; Back-end function to make and find an item
  (:function do-make-item (control item-class &key &allow-other-keys))
  (:function do-find-item (control object &key &allow-other-keys))
  ;; The concrete implementation should add the list item to the
  ;; list control, and then re-layout and redisplay the list control
  (:function do-add-item (control item &key after))
  ;; The concrete implementation should remove the list item from the
  ;; list control, and then re-layout and redisplay the list control
  (:function do-remove-item (control item))
  ;; Views, etc
  ;; The next one is defined as an accessor on <LIST-CONTROL>.
  ;; TODO: Move all these protocols out of the source files and put
  ;; them in a protocol file that gets built early.
;;  (:getter list-control-view (list-control))
  (:setter (setf list-control-view) (view list-control)))
||#


#||
define method list-control-view-setter
    (view :: <list-control-view>, pane :: <list-control>)
 => (view :: <list-control-view>)
  pane.%list-control-view := view
end method list-control-view-setter;
||#

(defmethod (setf list-control-view) (view (pane <list-control>))
  (check-type view <list-control-view>)
  (%list-control-view-setter view pane))


#||
define sealed method make-item
    (pane :: <list-control>, object, #rest initargs, #key)
 => (list-item :: <list-item>)
  apply(do-make-item, pane, <list-item>, object: object, initargs)
end method make-item;
||#

(defmethod make-item ((pane <list-control>) object &rest initargs &key)
  (apply #'do-make-item pane (find-class '<list-item>) :object object initargs))


#||
define sealed method find-item
    (pane :: <list-control>, object, #key)
 => (node :: false-or(<list-item>))
  do-find-item(pane, object)
end method find-item;
||#

(defmethod find-item ((pane <list-control>) object &key)
  (do-find-item pane object))


#||
// AFTER indicates which item to place the new item after
define sealed method add-item
    (pane :: <list-control>, item :: <list-item>, #key after) => ()
  // Update the set of items, bypassing 'note-gadget-items-changed'
  pane.%items
    := add!(as(<stretchy-vector>, gadget-items(pane)), item-object(item));
  // Update the selection by incrementing the indices of anything
  // after the newly added item
  when (after)
    let index = position(gadget-items(pane), item-object(after));
    when (index)
      let selection = gadget-selection(pane);
      for (i :: <integer> from 0 below size(selection))
	when (selection[i] > index)
	  selection[i] := selection[i] + 1
	end
      end;
      pane.%selection := selection
    end
  end;
  do-add-item(pane, item, after: after)
end method add-item;
||#

(defmethod add-item ((pane <list-control>) (item <list-item>) &key after)
"
_after_ indicates which item to place the new item after.
"
  ;; Update the set of items, bypassing 'note-gadget-items-changed'
  (%items-setter (add! (MAKE-STRETCHY-VECTOR :contents (gadget-items pane)) (item-object item)) pane)
  ;; Update the selection by incrementing the indices of anything
  ;; after the newly added item
  (when after
    (let ((index (position (gadget-items pane) (item-object after))))
      (when index
        (let ((selection (gadget-selection pane)))
          (loop for i from 0 below (length selection)
	     when (> (aref selection i) index)
	     do (setf (aref selection i) (1+ (aref selection i))))
	  (%selection-setter selection pane)))))
  (do-add-item pane item :after after))


#||
define sealed method remove-item
    (pane :: <list-control>, item :: <list-item>) => ()
  pane.%items
    := remove!(as(<stretchy-vector>, gadget-items(pane)), item-object(item));
  // Update the selection by decrementing the indices of anything
  // after the deleted item
  let index = position(gadget-items(pane), item-object(item));
  when (index)
    let selection = remove(gadget-selection(pane), index);
    for (i :: <integer> from 0 below size(selection))
      when (selection[i] > index)
	selection[i] := selection[i] - 1
      end
    end;
    pane.%selection := selection
  end;
  do-remove-item(pane, item)  
end method remove-item;
||#

(defmethod remove-item ((pane <list-control>) (item <list-item>))
  (%items-setter (delete  (item-object item) (coerce (gadget-items pane) 'array)) pane)
  ;; Update the selection by decrementing the indices of anything
  ;; after the deleted item
  (let ((index (position (gadget-items pane) (item-object item))))
    (when index
      (let ((selection (remove index (gadget-selection pane))))
        (loop for i from 0 below (length selection)
	   when (> (aref selection i) index)
	   do (setf (aref selection i) (1- (aref selection i))))
	(%selection-setter selection pane))))
  (do-remove-item pane item))

