;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GADGETS-INTERNALS -*-
(in-package #:duim-gadgets-internals)

#||
/// Active labels

define protocol <<active-label>> ()
  function handle-semantic-event
    (frame :: <frame>, gadget :: <active-label>, object :: <object>,
     event :: <pointer-button-event>,
     #key button, modifiers, x, y) => ();
  function handle-semantic-button-event
    (frame :: <frame>, gadget :: <active-label>, object :: <object>,
     event :: <pointer-button-event>, button :: <integer>,
     #key modifiers, x, y) => ();
end protocol <<active-label>>;
||#
#||
(define-protocol <<active-label>> ()
  (:function handle-semantic-event (frame gadget object event
                                          &key button modifiers x y))
  (:function handle-semantic-button-event (frame gadget object event button
                                                 &key modifiers x y)))
||#

(defgeneric label-selected? (gadget))
(defgeneric (setf label-selected?) (selected? gadget))
(defgeneric label-underline? (gadget))
(defgeneric (setf label-underline?) (underline? gadget))

#||
define constant %label_selected  :: <integer> = #o01;
define constant %label_underline :: <integer> = #o02;
||#

(defconstant %label_selected  #o01)
(defconstant %label_underline #o02)


#||
define open abstract class <active-label>
    (<popup-menu-gadget-mixin>,
     <labelled-gadget-mixin>,
     <value-gadget>,
     <basic-action-gadget>)
  sealed slot gadget-value :: <object> = #f,
    setter: %gadget-value-setter,
    init-keyword: value:;
  sealed slot %label-flags :: <integer> = 0;
end class <active-label>;
||#

(defclass <active-label>
    (<popup-menu-gadget-mixin>
     <labelled-gadget-mixin>
     <value-gadget>
     <basic-action-gadget>)
  ((gadget-value :initform nil :reader gadget-value :writer %gadget-value-setter :initarg :value)
   (%label-flags :type integer :initform 0 :accessor %label-flags))
  (:documentation
"
A clickable and activatable label. TODO: What does that mean? ;)
")
  (:metaclass <abstract-metaclass>))


#||
define method initialize
    (gadget :: <active-label>, #key underline? = #f)
  next-method();
  label-underline?(gadget) := underline?
end method initialize;
||#

(defmethod initialize-instance :after ((gadget <active-label>) &key (underline? nil) &allow-other-keys)
  (setf (label-underline? gadget) underline?))


#||
define method gadget-value-setter
    (value, gadget :: <active-label>, #key do-callback? = #f)
 => (value)
  ignore(do-callback?);
  gadget.%gadget-value := value
end method gadget-value-setter;
||#

(defmethod (setf gadget-value) (value (gadget <active-label>) &key (do-callback? nil))
  (declare (ignore do-callback?))
  (%gadget-value-setter value gadget))


#||
define method gadget-value-type
    (gadget :: <active-label>) => (type :: <type>)
  object-class(gadget-value(gadget))
end method gadget-value-type;
||#

(defmethod gadget-value-type ((gadget <active-label>))
  (class-of (gadget-value gadget)))


#||
define method label-selected?
    (gadget :: <active-label>) => (selected? :: <boolean>)
  logand(gadget.%label-flags, %label_selected) = %label_selected
end method label-selected?;
||#

(defmethod label-selected? ((gadget <active-label>))
  (eql (logand (%label-flags gadget) %label_selected) %label_selected))


#||
define method label-selected?-setter
    (selected? :: <boolean>, gadget :: <active-label>) => (selected? :: <boolean>)
  gadget.%label-flags
    := logior(logand(gadget.%label-flags, lognot(%label_selected)),
	      if (selected?) %label_selected else 0 end);
  selected?
end method label-selected?-setter;
||#

(defmethod (setf label-selected?) (selected? (gadget <active-label>))
  (setf (%label-flags gadget)
	(logior (logand (%label-flags gadget) (lognot %label_selected))
		(if selected? %label_selected 0)))
  selected?)


#||
define method label-underline?
    (gadget :: <active-label>) => (underline? :: <boolean>)
  logand(gadget.%label-flags, %label_underline) = %label_underline
end method label-underline?;
||#

(defmethod label-underline? ((gadget <active-label>))
  (eql (logand (%label-flags gadget) %label_underline) %label_underline))


#||
define method label-underline?-setter
    (underline? :: <boolean>, gadget :: <active-label>) => (underline? :: <boolean>)
  gadget.%label-flags
    := logior(logand(gadget.%label-flags, lognot(%label_underline)),
	      if (underline?) %label_underline else 0 end);
  underline?
end method label-underline?-setter;
||#

(defmethod (setf label-underline?) (underline? (gadget <active-label>))
  (setf (%label-flags gadget)
	(logior (logand (%label-flags gadget) (lognot %label_underline))
		(if underline? %label_underline 0)))
  underline?)


#||
define method handle-semantic-event
    (frame :: <frame>, gadget :: <active-label>, object :: <object>,
     event :: <pointer-button-event>,
     #key x, y, button = $left-button, modifiers = 0) => ()
  handle-semantic-button-event(frame, gadget, object, event, button,
			       modifiers: modifiers, x: x, y: y)
end method handle-semantic-event;
||#

(defmethod handle-semantic-event ((frame <frame>) (gadget <active-label>) object (event <pointer-button-event>)
				  &key x y (button +left-button+) (modifiers 0))
  (handle-semantic-button-event frame gadget object event button
				:modifiers modifiers :x x :y y))


#||
define method handle-semantic-button-event
    (frame :: <frame>, gadget :: <active-label>, object :: <object>,
     event :: <pointer-button-event>, button :: <integer>,
     #key modifiers, x, y) => ()
  ignore(modifiers, x, y);
  #f
end method handle-semantic-button-event;
||#

(defmethod handle-semantic-button-event ((frame <frame>) (gadget <active-label>) object (event <pointer-button-event>)
					 (button integer)
					 &key modifiers x y)
  (declare (ignore object modifiers x y))
  nil)



#||

/// Concrete implementation of active labels

define sealed class <active-label-pane>
    (<active-label>,
     <standard-input-mixin>,
     <standard-repainting-mixin>,
     <cached-space-requirement-mixin>,
     <leaf-layout-mixin>,
     <basic-sheet>)
  keyword cursor: = #"hand";
end class <active-label-pane>;
||#

(defclass <active-label-pane>
    (<active-label>
     <standard-input-mixin>
     <standard-repainting-mixin>
     <cached-space-requirement-mixin>
     <leaf-layout-mixin>
     <basic-sheet>)
  ()
  (:default-initargs :cursor :hand))


#||
define method class-for-make-pane
    (framem :: <frame-manager>, class == <active-label>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<active-label-pane>, #f)
end method class-for-make-pane;

define sealed domain make (singleton(<active-label-pane>));
define sealed domain initialize (<active-label-pane>);
||#

(defmethod class-for-make-pane ((framem <frame-manager>) (class (eql (find-class '<active-label>))) &key)
  (values (find-class '<active-label-pane>) nil))


#||
/// Layout

define method do-compose-space
    (gadget :: <active-label-pane>, #key width, height)
 => (space-req :: <space-requirement>)
  ignore(width, height);
  let offset :: <integer> = 1;
  let (width, height) = gadget-label-size(gadget);
  make(<space-requirement>,
       width:  width + offset * 2,
       height: height)
end method do-compose-space;
||#

(defmethod do-compose-space ((gadget <active-label-pane>) &key width height)
  (declare (ignore width height))
  (let ((offset 1))
    (multiple-value-bind (width height)
	(gadget-label-size gadget)
      (make-space-requirement :width (+ width (* offset 2))
			      :height height))))


#||
// Draw the active label, highlighting it in inverse video if it's selected
define method handle-repaint
    (gadget :: <active-label-pane>, medium :: <medium>, region :: <region>) => ()
  let offset :: <integer> = 1;
  let selected? = label-selected?(gadget);
  let (left, top, right, bottom) = box-edges(gadget);
  let brush = if (selected?) $foreground else $background end;
  with-drawing-options (medium, brush: brush)
    draw-rectangle(medium, left, top, right, bottom, filled?: #t)
  end;
  let brush = if (selected?) $background else $foreground end;
  draw-gadget-label(gadget, medium, 0 + offset, 0,
                    brush: selected? & $background,
		    underline?: label-underline?(gadget))
end method handle-repaint;
||#

(defmethod handle-repaint ((gadget <active-label-pane>) (medium <medium>) (region <region>))
  (let ((offset 1)
	(selected? (label-selected? gadget)))
    (multiple-value-bind (left top right bottom)
	(box-edges gadget)
      (let ((brush (if selected? *foreground* *background*)))
	(with-drawing-options (medium :brush brush)
          (draw-rectangle medium left top right bottom :filled? t))
	(let ((brush (if selected? *background* *foreground*)))
	  (with-drawing-options (medium :brush brush)
	    (draw-gadget-label gadget medium (+ 0 offset) 0
			       :brush (and selected? *background*)
			       :underline? (label-underline? gadget))))))))


#||
/// Event handling

define method handle-event
    (gadget :: <active-label-pane>, event :: <button-press-event>) => ()
  when (gadget-enabled?(gadget))
    label-selected?(gadget) := #t;
    repaint-sheet(gadget, $everywhere);
    select (event-button(event))
      $left-button, $middle-button =>
	// Left and middle button generate the semantic event
	// when the button is released
	#f;
      $right-button =>
	// Right button generates the semantic event when the
	// button is pressed, since it gets used for pop-up menus
	let x = event-x(event);
	let y = event-y(event);
	if (gadget-popup-menu-callback(gadget))
	  execute-popup-menu-callback
	    (gadget, gadget-client(gadget), gadget-id(gadget), gadget-value(gadget),
	     x: x, y: y)
	else
	  handle-semantic-event
	    (sheet-frame(gadget), gadget, gadget-value(gadget), event,
	     button: event-button(event), modifiers: event-modifier-state(event),
	     x: x, y: y)
	end;
    end
  end
end method handle-event;
||#

(defmethod handle-event ((gadget <active-label-pane>) (event <button-press-event>))
  (when (gadget-enabled? gadget)
    (setf (label-selected? gadget) t)
    (repaint-sheet gadget *everywhere*)
    (cond ((or (eql (event-button event) +left-button+)
	       (eql (event-button event) +middle-button+))
	   ;; Left and middle button generate the semantic event
	   ;; when the button is released
	   nil)
	  ((eql (event-button event) +right-button+)
	   ;; Right button generates the semantic event when the
	   ;; button is pressed, since it gets used for pop-up menus
	   (let ((x (event-x event))
		 (y (event-y event)))
	     (if (gadget-popup-menu-callback gadget)
		 (execute-popup-menu-callback gadget (gadget-client gadget) (gadget-id gadget) (gadget-value gadget)
					      :x x :y y)
		 (handle-semantic-event (sheet-frame gadget) gadget (gadget-value gadget) event
					:button (event-button event) :modifiers (event-modifier-state event)
					:x x :y y)))))))


#||
define method handle-event 
    (gadget :: <active-label-pane>, event :: <double-click-event>) => ()
  when (gadget-enabled?(gadget))
    label-selected?(gadget) := #t;
    repaint-sheet(gadget, $everywhere);
    // Double click generates the semantic event immediately
    handle-semantic-event
      (sheet-frame(gadget), gadget, gadget-value(gadget), event,
       button: event-button(event), modifiers: event-modifier-state(event),
       x: event-x(event), y: event-y(event));
    // Deselect the label immediately, too
    label-selected?(gadget) := #f;
    repaint-sheet(gadget, $everywhere)
  end
end method handle-event;
||#

(defmethod handle-event ((gadget <active-label-pane>) (event <double-click-event>))
  (when (gadget-enabled? gadget)
    (setf (label-selected? gadget) t)
    (repaint-sheet gadget *everywhere*)
    ;; Double click generates the semantic event immediately
    (handle-semantic-event (sheet-frame gadget) gadget (gadget-value gadget) event
			   :button (event-button event) :modifiers (event-modifier-state event)
			   :x (event-x event) :y (event-y event))
    ;; Deselect the label immediately, too
    (setf (label-selected? gadget) nil)
    (repaint-sheet gadget *everywhere*)))


#||
define method handle-event 
    (gadget :: <active-label-pane>, event :: <button-release-event>) => ()
  when (gadget-enabled?(gadget) & label-selected?(gadget))
    label-selected?(gadget) := #f;
    repaint-sheet(gadget, $everywhere);
    select (event-button(event))
      $left-button, $middle-button =>
	// Generate the semantic event for these now
	let x = event-x(event);
	let y = event-y(event);
	if (gadget-activate-callback(gadget))
	  execute-activate-callback
	    (gadget, gadget-client(gadget), gadget-id(gadget))
	else
	  handle-semantic-event
	    (sheet-frame(gadget), gadget, gadget-value(gadget), event,
	     button: event-button(event), modifiers: event-modifier-state(event),
	     x: x, y: y)
	end;
      $right-button =>
	// The semantic event has already been generated for this
	#f;
    end
  end
end method handle-event;
||#

(defmethod handle-event ((gadget <active-label-pane>) (event <button-release-event>))
  (when (and (gadget-enabled? gadget) (label-selected? gadget))
    (setf (label-selected? gadget) nil)
    (repaint-sheet gadget *everywhere*)
    (cond ((or (eql (event-button event) +left-button+)
	       (eql (event-button event) +middle-button+))
	   ;; Generate the semantic event for these now
	   (let ((x (event-x event))
		 (y (event-y event)))
	     (if (gadget-activate-callback gadget)
		 (execute-activate-callback gadget (gadget-client gadget) (gadget-id gadget))
		 (handle-semantic-event (sheet-frame gadget) gadget (gadget-value gadget) event
					:button (event-button event) :modifiers (event-modifier-state event)
					:x x :y y))))
	  ((eql (event-button event) +right-button+)
	   ;; The semantic event has already been generated for this
	   nil))))


#||
define method handle-event 
    (gadget :: <active-label-pane>, event :: <pointer-enter-event>) => ()
  when (gadget-enabled?(gadget))
    let pointer = port-pointer(port(gadget));
    when (pointer-button-state(pointer) ~= 0)
      label-selected?(gadget) := #t;
      repaint-sheet(gadget, $everywhere)
    end
  end
end method handle-event;
||#

(defmethod handle-event ((gadget <active-label-pane>) (event <pointer-enter-event>))
  (when (gadget-enabled? gadget)
    (let ((pointer (port-pointer (port gadget))))
      (when (not (= (pointer-button-state pointer) 0))
	(setf (label-selected? gadget) t)
	(repaint-sheet gadget *everywhere*)))))


#||
define method handle-event 
    (gadget :: <active-label-pane>, event :: <pointer-exit-event>) => ()
  when (gadget-enabled?(gadget))
    let pointer = port-pointer(port(gadget));
    when (pointer-button-state(pointer) ~= 0)
      label-selected?(gadget) := #f;
      repaint-sheet(gadget, $everywhere)
    end
  end
end method handle-event;
||#

(defmethod handle-event ((gadget <active-label-pane>) (event <pointer-exit-event>))
  (when (gadget-enabled? gadget)
    (let ((pointer (port-pointer (port gadget))))
      (when (not (= (pointer-button-state pointer) 0))
	(setf (label-selected? gadget) nil)
	(repaint-sheet gadget *everywhere*)))))



