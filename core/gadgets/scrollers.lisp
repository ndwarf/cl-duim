;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GADGETS-INTERNALS -*-
(in-package #:duim-gadgets-internals)

#||
/// Abstract scroller pane

// Scroller panes just serve to lay out all of the things that make up
// a scrollable pane -- the viewport, the pane being scrolled, and the
// viewport into the pane being scrolled.
define open abstract class <scroller> 
    (<scrolling-gadget-mixin>, <basic-gadget>)
  sealed slot scroller-sheet :: false-or(<scrolling-sheet-mixin>) = #f,
    setter: %sheet-setter;
  sealed slot scroller-gadget-supplies-scroll-bars? = #f;
end class <scroller>;
||#

(defclass <scroller>
    (<scrolling-gadget-mixin>
     <basic-gadget>)
  ((scroller-sheet :type (or null <scrolling-sheet-mixin>) :initform nil :reader scroller-sheet :writer %sheet-setter)
   (scroller-gadget-supplies-scroll-bars? :initform nil :accessor scroller-gadget-supplies-scroll-bars?))
  (:documentation
"
Scroller panes just serve to lay out all of the things that make up
a scrollable pane -- the viewport, the pane being scrolled, and the
viewport into the pane being scrolled.
")
  (:metaclass <abstract-metaclass>))


#||
define open generic make-scrolling-layout
    (framem :: <frame-manager>, sheet :: false-or(<sheet>),
     #key scroller, horizontal?, vertical?,
          border-type, foreground, background)
 => (layout :: <sheet>);
||#


(defgeneric make-scrolling-layout (framem sheet
				   &key scroller horizontal? vertical?
				   border-type foreground background))


#||
// Options can be any of the pane sizing options
define macro scrolling
  { scrolling (#rest ?options:expression)
      ?contents:body
    end }
    => { begin
           let _scrolled-sheet = #f;
           let ?=scrolled-sheet = method (p) _scrolled-sheet := p end;
           let _contents = ?contents;	// contents is a single expression
           ignore(?=scrolled-sheet);
           // Separate the "contents" from the "scrolled pane" so that folks
           // can do things like wrap margins around the scrolled pane.
	   // "Contents" is the child of the scroller and serves to control
	   // layout, whereas "scrolled-sheet" is the sheet being controlled
	   // by the scroll bars.
           make(<scroller>,
		contents: _contents,
		scrolled-sheet: _scrolled-sheet,
                ?options)
         end }
end macro scrolling;
||#

;; the ?=.. syntax tells Dylan to ignore hygiene for the value 'scrolled-sheet'
;; so that it will be available in the macro body.

(defmacro scrolling ((&rest options) &body contents)
"
Places scroll bars around the DUIM panes created by _pane_, if
required. It is useful to use this macro if you are unsure that the
panes created can be displayed on the screen successfully without
scroll bars; this macro only adds scroll bars when it is necessary.

Creates _pane_ with scroll bars attached to it, taking into account
any of the specified _options_.

The _pane_ is an expression whose return value is the sheet to which
the scroll bars should be attached.

The options can be used to specify the properties of the scroll
bars. As well as all the properties of <GADGET>, these include
a :scroll-bars initarg, which may take one of the following values:
NIL, :none, :horizontal, :vertical, :both, :dynamic. If no options are
specified, then both vertical and horizontal scroll bars are used.

The pane is a body of code whose return value is the sheet to which
the label should be assigned.

Example:

    (scrolling (:scroll-bars :vertical)
      (make-pane '<radio-box>
                 :orientation :vertical
                 :items (range :from 1 :to 50)))
"
  (let ((_scrolled-sheet (gensym "SCROLLED-SHEET"))
	(_contents (gensym "CONTENTS"))
	(_p (gensym "P")))
  `(let* ((,_scrolled-sheet nil)
          (scrolled-sheet #'(lambda (,_p) (setf ,_scrolled-sheet ,_p)))
	  (,_contents ,@contents))
     (declare (ignore scrolled-sheet))
     ;; Separate the "contents" from the "scrolled pane" so that folks
     ;; can do things like wrap margins around the scrolled pane.
     ;; "Contents" is the child of the scroller and serves to control
     ;; layout, whereas "scrolled-sheet" is the sheet being controlled
     ;; by the scroll bars.
     (duim-debug-message "GADGETS;SCROLLERS:SCROLLING:making <scroller> with contents=~a and scrolled-sheet=~a" ,_contents ,_scrolled-sheet)
     (make-pane '<scroller>
		:contents ,_contents
		:scrolled-sheet ,_scrolled-sheet
		,@options))))



#||

/// Concrete scroller panes

define sealed class <scroller-pane>
    (<scroller>, <single-child-wrapping-pane>)
end class <scroller-pane>;
||#

(defclass <scroller-pane>
    (<scroller> <single-child-wrapping-pane>)
  ())


#||
define method class-for-make-pane 
    (framem :: <frame-manager>, class == <scroller>, #key)
 => (class :: <class>, options :: false-or(<sequence>));
  values(<scroller-pane>, #f)
end method class-for-make-pane;

define sealed domain make (singleton(<scroller-pane>));
define sealed domain initialize (<scroller-pane>);
||#

(defmethod class-for-make-pane ((framem <frame-manager>) (class (eql (find-class '<scroller>))) &key)
  (values (find-class '<scroller-pane>) nil))


#||
define constant $vertical-scroll-bar-values
    = #[#t, #"both", #"vertical", #"dynamic"];
||#

(defvar *vertical-scroll-bar-values* '(t :both :vertical :dynamic))


#||
define constant $horizontal-scroll-bar-values
     = #[#t, #"both", #"horizontal", #"dynamic"];
||#

(defvar *horizontal-scroll-bar-values* '(t :both :horizontal :dynamic))


#||
// We could use a toolkit scrolling window, but that's actually a lot harder
// and we give up flexibility, too.  This mimics the look and feel properly
// anyway, so it's no big deal.
define sealed method initialize
    (scroller :: <scroller-pane>,
     #key contents, scrolled-sheet, frame-manager: framem,
          child-width, child-height, border-type: borders = #"sunken") => ()
  next-method();
  let scroll-bars = gadget-scroll-bars(scroller);
  let scrolled-sheet = scrolled-sheet | contents;
  let gadget-scroller?
    = instance?(scrolled-sheet, <gadget>)
      & gadget-supplies-scroll-bars?
          (framem, scrolled-sheet, scroll-bars: scroll-bars);
  if (gadget-scroller?)
    add-child(scroller, contents);
    scroller-gadget-supplies-scroll-bars?(scroller) := #t
  else
    check-type(scroll-bars, <scroll-bar-type>);
    let foreground = default-foreground(scroller);
    let background = default-background(scroller);
    let horizontal? = member?(scroll-bars, $horizontal-scroll-bar-values);
    let vertical?   = member?(scroll-bars, $vertical-scroll-bar-values);
    // 'contents' will be the sheet that gets put into the layout with
    // scroll bars etc, and 'scrolled-sheet' is the sheet that the scroll
    // bars are attached to.
    let (contents, scrolled-sheet)
      = if (instance?(scrolled-sheet, <scrolling-sheet-mixin>))
          values(contents, scrolled-sheet)
        else
          let viewport
            = make-sheet-viewport(scrolled-sheet,
                                  width: child-width,
                                  height: child-height);
          // The children of the scroller pane are the contents, which may
          // be a "superset" of the pane we are actually scrolling.
          values(if (contents == scrolled-sheet) viewport else contents end,
                 viewport)
        end;
    let layout
      = make-scrolling-layout
	  (framem, contents,
	   scroller: scroller,
	   horizontal?: horizontal?, vertical?: vertical?,
	   border-type: borders,
	   foreground: foreground, background: background);
    scroller.%sheet := scrolled-sheet;
    add-child(scroller, layout)
  end;
  initialize-scrolling(scroller, scrolled-sheet)
end method initialize;
||#

(defmethod initialize-instance :after ((scroller <scroller-pane>)
				       &key contents scrolled-sheet ((:frame-manager framem))
				       child-width child-height (border-type :sunken)
				       &allow-other-keys)
  (let* ((scroll-bars      (gadget-scroll-bars scroller))
         (scrolled-sheet   (or scrolled-sheet contents))
         (gadget-scroller? (and (typep scrolled-sheet '<gadget>)
                                (gadget-supplies-scroll-bars? framem scrolled-sheet :scroll-bars scroll-bars))))
    (if gadget-scroller?
        (progn
          (add-child scroller contents)
          (setf (scroller-gadget-supplies-scroll-bars? scroller) t))
	;; else
        (progn
          (check-type scroll-bars <scroll-bar-type>)
          (let ((foreground  (default-foreground scroller))
                (background  (default-background scroller))
                (horizontal? (member scroll-bars *horizontal-scroll-bar-values*))
                (vertical?   (member scroll-bars *vertical-scroll-bar-values*)))
            ;; 'contents' will be the sheet that gets put into the layout with
            ;; scroll bars etc, and 'scrolled-sheet' is the sheet that the scroll
            ;; bars are attached to.
            (multiple-value-bind (contents scrolled-sheet)
                (if (typep scrolled-sheet '<scrolling-sheet-mixin>)
                    (values contents scrolled-sheet)
		    ;; else
                    (let ((viewport (make-sheet-viewport scrolled-sheet
                                                         :width child-width
                                                         :height child-height)))
                      ;; The children of the scroller pane are the contents, which may
                      ;; be a "superset" of the pane we are actually scrolling.
                      (values (if (equal? contents scrolled-sheet) viewport contents)
                              viewport)))
              (let ((layout (make-scrolling-layout framem contents
                                                   :scroller scroller
                                                   :horizontal? horizontal? :vertical? vertical?
                                                   :border-type border-type
                                                   :foreground foreground :background background)))
		(%sheet-setter scrolled-sheet scroller)
                (add-child scroller layout))))))
    (initialize-scrolling scroller scrolled-sheet)))


#||
define open generic initialize-scrolling
    (scroller :: <scroller>, scrolled-sheet :: false-or(<abstract-sheet>)) => ();
||#

(defgeneric initialize-scrolling (scroller scrolled-sheet))


#||
define method initialize-scrolling
    (scroller :: <scroller>, scrolled-sheet :: false-or(<sheet>)) => ()
  #f
end method initialize-scrolling;
||#

(defmethod initialize-scrolling ((scroller <scroller>) scrolled-sheet)
  (check-type scrolled-sheet (or null <sheet>))
  nil)


#||
define function make-sheet-viewport
    (sheet :: false-or(<sheet>), #key width, height)
 => (viewport :: <viewport>)
  let viewport = make(<viewport>, width: width, height: height);
  // Splice the viewport between the pane we are scrolling and its parent
  when (sheet)
    let parent = sheet-parent(sheet);
    when (parent)
      replace-child(parent, sheet, viewport)
    end;
    add-child(viewport, sheet)
  end;
  viewport
end function make-sheet-viewport;
||#

(defun make-sheet-viewport (sheet &key width height)
  (check-type sheet (or null <sheet>))
  (let ((viewport (make-pane '<viewport> :width width :height height)))
    ;; Splice the viewport between the pane we are scrolling and its parent
    (when sheet
      (let ((parent (sheet-parent sheet)))
        (when parent
          (replace-child parent sheet viewport)))
      (add-child viewport sheet))
    viewport))


#||
define method make-scrolling-layout
    (framem :: <frame-manager>, sheet :: false-or(<sheet>),
     #key scroller,
	  horizontal? = #t, vertical? = #t,
          border-type: borders = #"sunken", foreground, background)
 => (layout :: <sheet>)
  with-frame-manager (framem)
    let vertical-bar
      = vertical? & make(<scroll-bar>,
			 orientation: #"vertical",
			 id: #"vertical",
			 client: scroller,
			 background: background,
			 foreground: foreground);
    let horizontal-bar
      = horizontal? & make(<scroll-bar>,
			   orientation: #"horizontal",
			   id: #"horizontal",
			   client: scroller,
			   background: background,
			   foreground: foreground);
    sheet-horizontal-scroll-bar(sheet) := horizontal-bar;
    sheet-vertical-scroll-bar(sheet)   := vertical-bar;
    let layout
      = case
	  horizontal? & vertical? =>
	    make(<table-layout>,
		 contents: vector(vector(sheet, vertical-bar),
				  vector(horizontal-bar, #f)));
	  vertical? =>
	    make(<row-layout>,
		 children: vector(sheet, vertical-bar));
	  horizontal? =>
	    make(<column-layout>, 
		 children: vector(sheet, horizontal-bar));
	  otherwise =>
	    sheet;
	end;
    if (borders)
      with-border (type: borders) layout end
    else
      layout
    end
  end
end method make-scrolling-layout;
||#

(defmethod make-scrolling-layout ((framem <frame-manager>) (sheet <sheet>)
				  &key scroller (horizontal? t) (vertical? t)
				    ((:border-type borders) :sunken) foreground background)
  (with-frame-manager (framem)
    (let ((vertical-bar (and vertical? (make-pane '<scroll-bar>
						  :orientation :vertical
						  :id :vertical
						  :client scroller
						  :background background
						  :foreground foreground)))
	  (horizontal-bar (and horizontal? (make-pane '<scroll-bar>
						      :orientation :horizontal
						      :id :horizontal
						      :client scroller
						      :background background
						      :foreground foreground))))
      (setf (sheet-horizontal-scroll-bar sheet) horizontal-bar)
      (setf (sheet-vertical-scroll-bar sheet) vertical-bar)
      (let ((layout (cond ((and horizontal? vertical?) (make-pane '<table-layout>
								  :contents (vector (vector sheet vertical-bar)
										    (vector horizontal-bar nil))))
			  (vertical? (make-pane '<row-layout> :children (vector sheet vertical-bar)))
			  (horizontal? (make-pane '<column-layout> :children (vector sheet horizontal-bar)))
			  (t sheet))))
	(if borders
	    (with-border (:type borders)
	      layout)
	    ;; else
	    layout)))))

(defmethod make-scrolling-layout ((framem <frame-manager>) (sheet null)
				  &key scroller (horizontal? t) (vertical? t)
				    ((:border-type borders) :sunken) foreground background)
  (with-frame-manager (framem)
    (let ((vertical-bar (and vertical? (make-pane '<scroll-bar>
						  :orientation :vertical
						  :id :vertical
						  :client scroller
						  :background background
						  :foreground foreground)))
	  (horizontal-bar (and horizontal? (make-pane '<scroll-bar>
						      :orientation :horizontal
						      :id :horizontal
						      :client scroller
						      :background background
						      :foreground foreground))))
      (setf (sheet-horizontal-scroll-bar sheet) horizontal-bar)
      (setf (sheet-vertical-scroll-bar sheet) vertical-bar)
      (let ((layout (cond ((and horizontal? vertical?) (make-pane '<table-layout>
								  :contents (vector (vector sheet vertical-bar)
										    (vector horizontal-bar nil))))
			  (vertical? (make-pane '<row-layout> :children (vector sheet vertical-bar)))
			  (horizontal? (make-pane '<column-layout> :children (vector sheet horizontal-bar)))
			  (t sheet))))
	(if borders
	    (with-border (:type borders)
	      layout)
	    ;; else
	    layout)))))
