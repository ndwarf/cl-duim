;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GADGETS-INTERNALS -*-
(in-package #:duim-gadgets-internals)

#||
/// Tab controls

define open abstract class <tab-control>
    (<oriented-gadget-mixin>,
     <key-press-gadget-mixin>,
     <value-gadget-mixin>,
     <basic-gadget>)
  // Are the tabs at the top or the bottom (tab-control or workspace?)
  sealed constant slot tab-control-tabs-position :: <vertical-position> = #"top",
    init-keyword: tabs-position:;
  // The single visible sheet corresponding to the selected "tab"
  sealed slot tab-control-current-page :: false-or(<sheet>) = #f,
    init-keyword: current-page:,
    setter: %current-page-setter;
  // All of sheets that we can select between
  sealed slot tab-control-pages :: <sequence> = #[],
    init-keyword: pages:,
    setter: %pages-setter;
  sealed slot gadget-label-key :: <function> = gadget-label,
    init-keyword: label-key:;
  sealed slot gadget-value-key :: <function> = tab-control-default-value-key,
    init-keyword: value-key:;
end class <tab-control>;
||#

(defclass <tab-control>
    (<oriented-gadget-mixin>
     <key-press-gadget-mixin>
     <value-gadget-mixin>
     <basic-gadget>)
  ;; Are the tabs at the top or the bottom (tab-control or workspace?)
  ((tab-control-tabs-position :type <vertical-position> :initarg :tabs-position :initform :top :reader tab-control-tabs-position)
   ;; The single visible sheet corresponding to the selected "tab"
   (tab-control-current-page :type (or null <sheet>) :initarg :current-page :initform nil
			     :reader tab-control-current-page :writer %current-page-setter)
   ;; All of the sheets that we can select between
   (tab-control-pages :type sequence :initarg :pages :initform #() :reader tab-control-pages :writer %pages-setter)
   (gadget-label-key :type function :initarg :label-key :initform #'gadget-label :accessor gadget-label-key)
   (gadget-value-key :type function :initarg :value-key :initform #'tab-control-default-value-key :accessor gadget-value-key))
  (:documentation
"
The class of tab controls. These controls let you implement a
multi-page environment in a window or dialog. Each page in a tab
control has its own associated layout of sheets and gadgets, and an
accompanying tab (usually displayed at the top of the page, rather
like the tab dividers commonly used in a filing cabinet). Each page in
a tab control can be displayed by clicking on the appropriate tab.

The :pages initarg is used to define the pages that the tab control
contains. Each page in the tab control is an instance of the class
<page>.

The :tabs-position initarg is used to specify where the tabs are
drawn. This can be either :top or :bottom. If this initarg is not
provided, the tabs position defaults to :top.

The :current-page initarg specifies which tab is visible when the tab
control is first displayed.

The :key-press-callback initarg lets you specify a key-press
callback. This type of callback is invoked whenever a key on the
keyboard is pressed while the gadget has focus. In a tab control, a
key-press callback might be used as a quick way to display each page
in the tab control. See GADGET-KEY-PRESS-CALLBACK for a fuller
description of key-press callbacks.

The GADGET-ID of a tab control is particularly useful, because it is
returned by GADGET-VALUE.

Internally, this class maps into the Windows tab control.

Example:

The following example creates a tab control that has two pages. The
first page contains a button, and the second page contains a list.

    (contain (make-pane '<tab-control>
                        :pages
                        (vector (make-pane '<tab-control-page>
                                           :label \"First\"
                                           :child
                                           (make-pane '<push-button>
                                                      :label
                                                      \"One\"))
                                (make-pane '<tab-control-page>
                                           :label \"Second\"
                                           :child
                                           (make-pane '<list-box>
                                                      :items
                                                      '(1 2 3))))))
")
  (:metaclass <abstract-metaclass>))

;;; TODO: where should the documentation for 'tab-control-current-page' go?
;;; -- create a gf above the class definition and attach the documentation
;;;    there, or can it be specified in the class slot? It could go in the
;;;    class slot, but that would document the slot not the gf. Is that
;;;    different? I suspect it is.

#||
Returns the current visible page of _tab-control_.

Example:

The following example creates a tab control that has two pages:

    (defparameter *tab*
    (contain (make-pane '<tab-control>
                        :pages
                        (vector (make-pane '<tab-control-page>
                                           :label \"First\"
                                           :child
                                           (make-pane '<push-button>
                                                      :label
                                                      \"One\"))
                                (make-pane '<tab-control-page>
                                           :label \"Second\"
                                           :child
                                           (make-pane '<list-box>
                                                      :items
                                                      '(1 2 3)))))))

The current page of the tab control can be returned with the following
code:

    (tab-control-current-page *tab*)
||#
;;; TODO: where should documentation for 'tab-control-pages' go?
#||
Returns the tab pages of _pane_.

Example:

Given the tab control created by the code below:

    (defparameter *tab*
      (contain (make-pane '<tab-control>
                          :pages
                          (vector (make-pane '<tab-control-page>
                                             :label \"First\")
                                  (make-pane '<tab-control-page>
                                             :label \"Second\")
                                  (make-pane '<tab-control-page>
                                             :label \"Third\")
                                  (make-pane '<tab-control-page>
                                             :label \"Fourth\")
                                  (make-pane '<tab-control-page>
                                             :label \"Fifth\")))))

You can return a list of the pages as follows:

    (tab-control-pages *tab*)
||#

#||
define sealed class <tab-control-page> 
    (<basic-page>, <single-child-wrapping-pane>)
end class <tab-control-page>;
||#

(defclass <tab-control-page>
    (<basic-page> <single-child-wrapping-pane>)
  ()
  (:documentation
"
The class that represents a page in a tab control.
"))


(defgeneric (setf tab-control-current-page) (child pane)
  (:documentation
"
Sets the current visible page of _tab-control_.

Example:

The following example creates a tab control that has two pages.

    (defparameter *tab*
    (contain (make-pane '<tab-control>
                        :pages
                        (vector (make-pane '<tab-control-page>
                                           :label \"First\"
                                           :child
                                           (make-pane '<push-button>
                                                      :label
                                                      \"One\"))
                                (make-pane '<tab-control-page>
                                           :label \"Second\"
                                           :child
                                           (make-pane '<list-box>
                                                      :items
                                                      '(1 2 3)))))))

Assign a variable to the current page of the tab control as follows:

    (defparameter *page* (tab-control-current-page *tab*))

Next, change the current page of the tab control by clicking on the
tab for the hidden page. Then, set the current page to be the original
current page as follows:

    (setf (tab-control-current-page *tab*) *page*)
"))


(defgeneric (setf tab-control-pages) (pages pane &key page)
  (:documentation
"

Sets the tab pages available to _tab-control_, optionally setting
_page_ to the default page to be displayed. The pages argument is an
instance of 'limited(<sequence> of: <page>)'. The _page_ argument is
an instance of <page> and, moreover, must be one of the pages
contained in _pages_.

Example:

    (setf (tab-control-pages my-tab-control :page my-page) my-pages)
"))

(defgeneric tab-control-labels (pane)
  (:documentation
"
Returns the tab labels of _tab-control_, as a sequence. Each element
in _labels_ is an instance of <label>.

Example:

Given the tab control created by the code below:

    (defparameter *tab*
      (contain (make-pane '<tab-control>
                          :pages
                          (vector (make-pane '<tab-control-page>
                                             :label \"First\")
                                  (make-pane '<tab-control-page>
                                             :label \"Second\")
                                  (make-pane '<tab-control-page>
                                             :label \"Third\")
                                  (make-pane '<tab-control-page>
                                             :label \"Fourth\")
                                  (make-pane '<tab-control-page>
                                             :label \"Fifth\")))))

You can return a list of the labels as follows:

    (tab-control-labels *tab*)
"))
                        

(defgeneric tab-control-named-child (pane name))
(defgeneric tab-control-default-value-key (page))
(defgeneric find-tab-control-page (pane value))


#||
define method initialize (pane :: <tab-control>, #key value) => ()
  next-method();
  unless (tab-control-current-page(pane))
    unless (empty?(tab-control-pages(pane)))
      pane.%current-page := tab-control-pages(pane)[0]
    end
  end
end method initialize;

define sealed domain make (singleton(<tab-control-page>));
define sealed domain initialize (<tab-control-page>);
||#

(defmethod initialize-instance :after ((pane <tab-control>) &key value &allow-other-keys)
  (declare (ignore value))
  (unless (tab-control-current-page pane)
    (unless (empty? (tab-control-pages pane))
      (%current-page-setter (aref (tab-control-pages pane) 0) pane))))


#||
define method tab-control-current-page-setter
    (child :: false-or(<sheet>), pane :: <tab-control>)
 => (child :: false-or(<sheet>))
  pane.%current-page := child;
  when (child)
    let frame = sheet-frame(pane);
    when (frame & page-initial-focus(child))
      frame-input-focus(frame) := page-initial-focus(child)
    end
  end;
  child
end method tab-control-current-page-setter;
||#

(defmethod (setf tab-control-current-page) ((child null) (pane <tab-control>))
  (%current-page-setter child pane)
  (when child
    (let ((frame (sheet-frame pane)))
      (when (and frame (page-initial-focus child))
	(setf (frame-input-focus frame) (page-initial-focus child)))))
  child)

(defmethod (setf tab-control-current-page) ((child <sheet>) (pane <tab-control>))
  (%current-page-setter child pane)
  (when child
    (let ((frame (sheet-frame pane)))
      (when (and frame (page-initial-focus child))
	(setf (frame-input-focus frame) (page-initial-focus child)))))
  child)


#||
define method tab-control-current-page-setter
    (name :: <string>, pane :: <tab-control>)
 => (name :: <string>)
  let child = tab-control-named-child(pane, name);
  if (child)
    tab-control-current-page(pane) := child
  else
    error("No child named %s in tab control %=", name, pane)
  end
end method tab-control-current-page-setter;
||#

(defmethod (setf tab-control-current-page) ((name string) (pane <tab-control>))
  (let ((child (tab-control-named-child pane name)))
    (if child
	(setf (tab-control-current-page pane) child)
	(error "No child named ~s in tab control ~a" name pane))))


#||
define method tab-control-pages-setter
    (pages :: <sequence>, pane :: <tab-control>, #key page)
 => (pages :: <sequence>)
  let old-pages = tab-control-pages(pane);
  let old-page  = tab-control-current-page(pane);
  let page
    = select (page by instance?)
	<sheet>   => page;
	otherwise => find-tab-control-page(pane, page);
      end
      | old-page;	// Keep old page current if none specified
  let new-page
    = if (member?(page, pages))
	page
      else
	unless (empty?(pages)) pages[0] end
      end;
  case
    old-pages ~= pages =>
      pane.%pages := pages;
      pane.%current-page := new-page;
      note-pages-changed(pane);
    old-page ~= new-page =>
      tab-control-current-page(pane) := new-page;
  end;
  pages
end method tab-control-pages-setter;
||#

(defmethod (setf tab-control-pages) ((pages sequence) (pane <tab-control>) &key page)
  (let* ((old-pages (tab-control-pages pane))
	 (old-page  (tab-control-current-page pane))
	 (page      (or (typecase page
			  (<sheet> page)
			  (t       (find-tab-control-page pane page)))
			old-page))  ; Keep old page current if none specified
	 (new-page (if (find page pages)
		       page
		       (unless (empty? pages) (aref pages 0)))))
    (cond ((not (equal? old-pages pages))
	   (%pages-setter pages pane)
	   (%current-page-setter new-page pane)
	   (note-pages-changed pane))
	  ((not (equal? old-page new-page))
	   (setf (tab-control-current-page pane) new-page))))
  pages)


#||
define open generic note-pages-changed (pane :: <tab-control>) => ();
||#

(defgeneric note-pages-changed (pane))


#||
define constant $tab-control-default-label :: <string> = "{no-label}";
||#

(defparameter *tab-control-default-label* "{no-label}")


#||
define method tab-control-labels
    (pane :: <tab-control>) => (labels :: <sequence>)
  let label-key = gadget-label-key(pane);
  map-as(<simple-vector>,
         method (gadget) label-key(gadget) | $tab-control-default-label end,
         tab-control-pages(pane))
end method tab-control-labels;
||#

(defmethod tab-control-labels ((pane <tab-control>))
  (let ((label-key (gadget-label-key pane)))
    (map 'vector
	 #'(lambda (gadget)
	     (or (funcall label-key gadget) *tab-control-default-label*))
	 (tab-control-pages pane))))


#||
define method tab-control-named-child
    (pane :: <tab-control>, name :: <string>)
 => (child :: false-or(<sheet>))
  let key = position(tab-control-labels(pane), name, test: \=);
  when (key)
    tab-control-pages(pane)[key];
  end
end method tab-control-named-child;
||#

(defmethod tab-control-named-child ((pane <tab-control>) (name string))
  (let ((key (position (tab-control-labels pane) name :test #'string=)))
    (when key
      (aref (tab-control-pages pane) key))))



#||

/// Gadget value handling

define method tab-control-default-value-key
    (page :: <sheet>) => (value)
  #f
end method tab-control-default-value-key;
||#

(defmethod tab-control-default-value-key ((page <sheet>))
  nil)


#||
define method tab-control-default-value-key
    (page :: <labelled-gadget-mixin>) => (value)
  gadget-id(page) | gadget-label(page)
end method tab-control-default-value-key;
||#

(defmethod tab-control-default-value-key ((page <labelled-gadget-mixin>))
  (or (gadget-id page) (gadget-label page)))


#||
define method gadget-value
    (pane :: <tab-control>) => (value)
  let page = tab-control-current-page(pane);
  when (page)
    gadget-value-key(pane)(page)
  end
end method gadget-value;
||#

(defmethod gadget-value ((pane <tab-control>))
  (let ((page (tab-control-current-page pane)))
    (when page
      (funcall (gadget-value-key pane) page))))


#||
define method find-tab-control-page
    (pane :: <tab-control>, value) => (page :: false-or(<sheet>))
  let value-key = gadget-value-key(pane);
  block (return)
    for (page in tab-control-pages(pane))
      when (value-key(page) = value)
        return(page)
      end
    end
  end
end method find-tab-control-page;
||#

(defmethod find-tab-control-page ((pane <tab-control>) value)
  (let ((value-key (gadget-value-key pane)))
    (loop for page across (tab-control-pages pane)
       when (equal? (funcall value-key page) value)
       do (return page))))


#||
define method do-gadget-value-setter
    (pane :: <tab-control>, value) => ()
  let page = find-tab-control-page(pane, value);
  tab-control-current-page(pane) := page
end method do-gadget-value-setter;
||#

(defmethod do-gadget-value-setter ((pane <tab-control>) value)
  (let ((page (find-tab-control-page pane value)))
    (setf (tab-control-current-page pane) page)))

