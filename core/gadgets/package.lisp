s;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

;;; Two packages are defined in this file, a public and private API.

(defpackage :duim-gadgets-internals
  (:use #:common-lisp
	#:dylan-commands
	#:duim-utilities
	#:duim-geometry-internals
	#:duim-DCs-internals
	#:duim-sheets-internals
	#:duim-graphics-internals
	#:duim-layouts-internals)
  (:export

   ;; Gadgets, excluding collection gadgets

   #:<accelerator>  ;; ???
   #:<accelerator-mixin>
   #:<action-gadget-mixin>
   #:<basic-action-gadget>
   #:<basic-gadget>
   #:<basic-page>
   #:<basic-value-gadget>
   #:<border-type>  ;; type?
   #:<bordered-gadget-mixin>
   #:<callback-client>  ;; ???
   #:<callback-type>  ;; ???
   #:<changing-value-gadget-mixin>
   #:<collection-gadget-state>
   #:<default-gadget-mixin>
   #:<gadget-command-mixin>
   #:<gadget-state>
   #:<key-press-gadget-mixin>
   #:<label-pane>
   #:<labelled-gadget-mixin>
   #:<mnemonic>  ;; ???
   #:<mnemonic-mixin>
   #:<no-value-gadget-mixin>
   #:<oriented-gadget-mixin>
   #:<popup-menu-gadget-mixin>
   #:<range-gadget-mixin>
   #:<range-gadget-state>
   #:<scroll-bar-type>  ;; type?
   #:<scrolling-gadget-mixin>
   #:<separator-pane>
   #:<slug-gadget-mixin>
   #:<slug-gadget-state>
   #:<text-gadget-state>
   #:<tree-control-state>
   #:<updatable-gadget-mixin>
   #:<value-gadget-mixin>
   #:<value-gadget-state>
   #:border-type
   ;;   #:border-type-setter
   ;;   #:(setf border-type)
   #:callback-for-command
   #:compute-mnemonic-from-label
   #:defaulted-gadget-accelerator
   #:defaulted-gadget-mnemonic
   #:do-gadget-value-setter
   #:draw-border
   #:draw-gadget-label
   #:draw-separator
   #:distribute-activate-callback
   #:distribute-column-click-callback
   #:distribute-key-press-callback
   #:distribute-popup-menu-callback
   #:distribute-text-changed-callback
   #:distribute-text-changing-callback
   #:distribute-update-callback
   #:distribute-value-changed-callback
   #:distribute-value-changing-callback
   #:execute-activate-callback
   #:do-execute-activate-callback
   #:execute-callback
   #:execute-key-press-callback
   #:do-execute-key-press-callback
   #:execute-popup-menu-callback
   #:do-execute-popup-menu-callback
   #:execute-update-callback
   #:do-execute-update-callback
   #:execute-value-changed-callback
   #:do-execute-value-changed-callback
   #:execute-value-changing-callback
   #:do-execute-value-changing-callback
   #:gadget-flags
   ;;   #:gadget-flags-setter
   ;;   #:(setf gadget-flags)
   #:gadget-keep-selection-visible?
   #:gadget-label-size
   #:gadget-lines
   #:gadget-columns
   #:gadget-scroll-bars
   ;;   #:gadget-scroll-bars-setter
   ;;   #:(setf gadget-scroll-bars)
   #:handle-button-gadget-click
   #:multi-line-label?
   #:note-gadget-disabled
   #:note-gadget-enabled
   #:note-gadget-label-changed
   #:note-gadget-slug-size-changed
   #:note-gadget-text-changed
   #:note-gadget-value-changed
   #:note-gadget-value-range-changed
   #:push-button-like?

   ;; Gadget events

   #:<gadget-event>
   #:<activate-gadget-event>
   #:<key-press-gadget-event>
   #:<popup-menu-gadget-event>
   #:<text-changed-gadget-event>
   #:<text-changing-gadget-event>
   #:<update-gadget-event>
   #:<value-changed-gadget-event>
   #:<value-changing-gadget-event>
   #:event-gadget

   ;; Slider properties

   #:slider-decimal-places
   #:slider-max-label
   #:slider-min-label
   #:slider-show-value?
   #:slider-tick-marks

   ;; Text editing gadgets

   #:<protected-gadget-mixin>
   #:<selection-change-mixin>
   #:execute-protection-callback
   #:do-execute-protection-callback
   #:execute-text-selection-changed-callback
   #:do-execute-text-selection-changed-callback
   #:gadget-text-buffer
   ;;   #:gadget-text-buffer-setter
   ;;   #:(setf gadget-text-buffer)
   #:text-field-auto-scroll?
   #:text-field-case
   #:text-field-maximum-size

   ;; Collection gadgets (including menus)

   #:<basic-choice-gadget>
   #:<collection-gadget-mixin>
   #:<gadget-box>
   #:<gadget-selection-mixin>
   #:<selection-changed-gadget-event>
   #:<selection-mode>  ;; type?
   #:collection-gadget-default-label-key
   #:collection-gadget-default-value-key
   #:collection-gadget-item-label
   #:distribute-selection-changed-callback
   #:gadget-item-label
   #:gadget-item-value
   #:help-menu?
   ;;   #:menu-owner-setter
   ;;   #:(setf menu-owner)
   #:note-gadget-items-changed
   #:note-gadget-selection-changed
   #:note-menu-attached
   #:note-menu-detached
   #:tear-off-menu?

   ;; Scrolling

   #:<scroller-pane>
   #:<scrolling-sheet-mixin>
   #:attach-scroll-bar
   #:detach-scroll-bar
   #:initialize-scrolling
   #:line-scroll-amount
   #:make-scrolling-layout
   #:note-scroll-bar-changed
   #:page-scroll-amount
   #:scroll-down-line
   #:scroll-down-page
   #:scroll-to-position
   #:scroll-up-line
   #:scroll-up-page
   #:scroll-viewport
   #:scroll-viewport-child
   #:sheet-horizontal-scroll-bar
   ;;   #:sheet-horizontal-scroll-bar-setter
   ;;   #:(setf sheet-horizontal-scroll-bar)
   #:sheet-vertical-scroll-bar
   ;;   #:sheet-vertical-scroll-bar-setter
   ;;   #:(setf sheet-vertical-scroll-bar)
   #:sheet-scrolls-horizontally?
   #:sheet-scrolls-vertically?
   #:sheet-scroll-range
   #:sheet-visible-range
   #:set-sheet-visible-range
   #:shift-visible-region
   #:update-dynamic-scroll-bars
   #:update-scroll-bar
   #:update-scroll-bars
   ;;   #:viewport-region-setter
   ;;   #:(setf viewport-region)

   ;; Borders

   #:border-thickness
   ;;   #:border-thickness-setter
   ;;   #:(setf border-thickness)
   #:group-box-label-position

   ;; Splitters, tab controls, etc.

   #:execute-split-box-callback
   #:do-execute-split-box-callback
   #:execute-split-bar-moved-callback
   #:do-execute-split-bar-moved-callback

   ;; List and table control panes

   #:<list-control-view>  ;;???
   #:<table-control-view>  ;;???
   #:do-add-column
   #:do-add-item
   #:do-find-item
   #:do-make-item
   #:do-remove-column
   #:do-remove-item
   #:list-control-icon-function
   #:table-column-callback
   #:table-column-heading
   #:table-column-width
   #:table-column-alignment
   #:table-column-generator
   #:table-control-columns
   ;;   #:table-control-columns-setter
   ;;   #:(setf table-control-columns)
   #:table-control-icon-function

   ;; Tree control panes

   #:<node-state-changed-gadget-event>
   #:distribute-node-state-changed-callback
   #:execute-node-state-changed-callback
   #:do-execute-node-state-changed-callback
   #:do-add-node
   #:do-add-nodes
   #:do-contract-node
   #:do-expand-node
   #:do-find-node
   #:do-make-node
   #:do-remove-node
   #:note-tree-control-roots-changed
   #:tree-control-icon-function
   #:tree-control-root-nodes
   ;;   #:tree-control-root-nodes-setter
   ;;   #:(setf tree-control-root-nodes)
   #:tree-control-show-buttons?
   #:tree-control-show-edges?
   #:tree-control-show-root-edges?
   #:node-generation
   ;;   #:node-generation-setter
   ;;   #:(setf node-generation)
   ;;   #:node-state-setter
   ;;   #:(setf node-state)
   ;;   #:node-parents-setter
   ;;   #:(setf node-parents)

   ;; Button box implementation classes

   #:<check-box-pane>
   #:<push-box-pane>
   #:<radio-box-pane>
   #:button-box-spacing
   #:button-class-for-gadget-box
   #:button-first-in-group?
   #:button-gadget-box
   #:button-in-tool-bar?
   #:gadget-box-button-class
   #:gadget-box-buttons
   #:make-button-for-gadget-box
   #:make-buttons-for-gadget-box

   ;; Menu component implementation classes

   #:<menu-box-pane>
   #:<push-menu-box-pane>
   #:<radio-menu-box-pane>
   #:<check-menu-box-pane>


   ;;;
   ;;; Public API
   ;;;

   ;; Gadgets, excluding collection gadgets

   #:<abstract-gadget>
   #:<action-gadget>
   #:<compound-label>
   #:<gadget>
   #:<label>
   #:<labelled-gadget>
   #:<page>
   #:<progress-bar>
   #:<slider>
   #:<value-gadget>
   #:<value-range-gadget>
   #:<gadget-orientation>
   #:gadget-accelerator
   ;;   #:gadget-accelerator-setter
   ;;   #:(setf gadget-accelerator)
   #:gadget-activate-callback
   ;;   #:gadget-activate-callback-setter
   ;;   #:(setf gadget-activate-callback)
   #:gadget-client
   ;;   #:gadget-client-setter
   ;;   #:(setf gadget-client)
   #:gadget-command
   ;;   #:gadget-command-setter
   ;;   #:(setf gadget-command)
   #:gadget-documentation
   ;;   #:gadget-documentation-setter
   ;;   #:(setf gadget-documentation)
   #:gadget-enabled?
   ;;   #:gadget-enabled?-setter
   ;;   #:(setf gadget-enabled?)
   #:gadget-end-value
   #:gadget-id
   ;;   #:gadget-id-setter
   ;;   #:(setf gadget-id)
   #:gadget-key-press-callback
   ;;   #:gadget-key-press-callback-setter
   ;;   #:(setf gadget-key-press-callback)
   #:gadget-label
   ;;   #:gadget-label-setter
   ;;   #:(setf gadget-label)
   #:gadget-mnemonic
   ;;   #:gadget-mnemonic-setter
   ;;   #:(setf gadget-mnemonic)
   #:gadget-orientation
   #:gadget-popup-menu-callback
   ;;   #:gadget-popup-menu-callback-setter
   ;;   #:(setf gadget-popup-menu-callback)
   #:gadget-protection-callback
   ;;   #:gadget-protection-callback-setter
   ;;   #:(setf gadget-protection-callback)
   #:gadget-read-only?
   #:gadget-scrolling?
   #:gadget-scrolling-horizontally?
   #:gadget-scrolling-vertically?
   #:gadget-state
   ;;   #:gadget-state-setter
   ;;   #:(setf gadget-state)
   #:gadget-text-selection-changed-callback
   ;;   #:gadget-text-selection-changed-callback-setter
   ;;   #:(setf gadget-text-selection-changed-callback)
   #:gadget-slug-size
   ;;   #:gadget-slug-size-setter
   ;;   #:(setf gadget-slug-size)
   #:gadget-start-value
   #:gadget-supplies-scroll-bars?
   #:gadget-update-callback
   ;;   #:gadget-update-callback-setter
   ;;   #:(setf gadget-update-callback)
   #:gadget-value
   ;;   #:gadget-value-setter
   ;;   #:(setf gadget-value)
   #:gadget-value-changed-callback
   ;;   #:gadget-value-changed-callback-setter
   ;;   #:(setf gadget-value-changed-callback)
   #:gadget-value-changing-callback
   ;;   #:gadget-value-changing-callback-setter
   ;;   #:(setf gadget-value-changing-callback)
   #:gadget-value-increment
   #:gadget-value-range
   ;;   #:gadget-value-range-setter
   ;;   #:(setf gadget-value-range)
   #:gadget-value-type
   #:gadget-x-alignment
   #:gadget-y-alignment
   #:gadgetp
   #:labeling
   #:labelling		;; everyone always spells this "wrong"
   #:horizontal-line-scroll-amount
   #:normalize-gadget-value
   #:vertical-line-scroll-amount
   #:activate-gadget
   #:update-gadget

   ;; Text editing gadgets

   #:<active-text-range>
   #:<password-field>
   #:<rich-text-editor>
   #:<simple-text-range>
   #:<text-editor>
   #:<text-field>
   #:<text-gadget>
   #:<text-range>
   #:character-format
   ;;   #:character-format-setter
   ;;   #:(setf character-format)
   #:character-position
   #:position-character
   #:current-line
   #:find-text
   #:gadget-text
   ;;   #:gadget-text-setter
   ;;   #:(setf gadget-text)
   #:gadget-text-parser
   #:gadget-value-printer
   #:get-line
   #:line-index
   #:index-line
   #:line-length
   #:make-text-range
   #:paragraph-format
   ;;   #:paragraph-format-setter
   ;;   #:(setf paragraph-format)
   #:selected-text
   ;;   #:selected-text-setter
   ;;   #:(setf selected-text)
   #:text-caret-position
   ;;   #:text-caret-position-setter
   ;;   #:(setf text-caret-position)
   #:text-field-modified?
   ;;   #:text-field-modified?-setter
   ;;   #:(setf text-field-modified?)
   #:text-field-size
   #:text-field-text
   #:text-field-word-wrap?
   #:text-range-end
   ;;   #:text-range-end-setter
   ;;   #:(setf text-range-end)
   #:text-range-object
   ;;   #:text-range-object-setter
   ;;   #:(setf text-range-object)
   #:text-range-protected?
   ;;   #:text-range-protected?-setter
   ;;   #:(setf text-range-protected?)
   #:text-range-start
   ;;   #:text-range-start-setter
   ;;   #:(setf text-range-start)
   #:text-selection
   ;;   #:text-selection-setter
   ;;   #:(setf text-selection)

   ;; Collection gadgets (including menus)

   #:<button-box>
   #:<button>
   #:<check-box>
   #:<check-button>
   #:<check-menu-box>
   #:<check-menu-button>
   #:<collection-gadget>
   #:<combo-box>
   #:<list-box>
   #:<menu-bar>
   #:<menu-box>
   #:<menu-button>
   #:<menu>
   #:<option-box>
   #:<push-box>
   #:<push-button>
   #:<push-menu-box>
   #:<push-menu-button>
   #:<radio-box>
   #:<radio-button>
   #:<radio-menu-box>
   #:<radio-menu-button>
   #:<spin-box>
   #:<status-bar>
   #:<tool-bar>
   #:button-gadget-value
   #:display-menu
   #:gadget-default?
   ;;   #:gadget-default?-setter
   ;;   #:(setf gadget-default?)
   #:gadget-item-selected?
   #:gadget-items
   ;;   #:gadget-items-setter
   ;;   #:(setf gadget-items)
   #:gadget-label-key
   #:gadget-selection
   ;;   #:gadget-selection-setter
   ;;   #:(setf gadget-selection)
   #:gadget-selection-mode
   #:gadget-test
   #:gadget-value-key
   #:make-menu-from-items
   #:menu-owner
   #:status-bar-label-pane
   ;;   #:status-bar-label-pane-setter
   ;;   #:(setf status-bar-label-pane)
   #:status-bar-progress-bar
   ;;   #:status-bar-progress-bar-setter
   ;;   #:(setf status-bar-progress-bar)

   ;; Scrolling

   #:<scroll-bar>
   #:<scroller>
   #:<viewport>
   #:inhibit-updating-scroll-bars
   #:note-viewport-position-changed
   #:note-viewport-region-changed
   #:scrolling
   #:scroll-position
   #:set-scroll-position
   #:sheet-viewport
   #:sheet-viewport-region
   #:viewport-region
   #:viewport?

   ;; Borders

   #:<border>
   #:<group-box>
   #:<separator>
   #:<spacing>
   #:grouping
   #:with-border
   #:with-spacing

   ;; Splitters, tab controls, etc.

   #:<column-splitter>
   #:<splitter>
   #:<row-splitter>
   #:<tab-control>
   #:<tab-control-page>
   #:gadget-ratios
   ;;   #:gadget-ratios-setter
   ;;   #:(setf gadget-ratios)
   #:splitter-split-box-callback
   ;;   #:splitter-split-box-callback-setter
   ;;   #:(setf splitter-split-box-callback)
   #:splitter-split-bar-moved-callback
   ;;   #:splitter-split-bar-moved-callback-setter
   ;;   #:(setf splitter-split-bar-moved-callback)
   #:note-pages-changed
   #:page-initial-focus
   ;;   #:page-initial-focus-setter
   ;;   #:(setf page-initial-focus)
   #:tab-control-current-page
   ;;   #:tab-control-current-page-setter
   ;;   #:(setf tab-control-current-page)
   #:tab-control-labels
   #:tab-control-pages
   ;;   #:tab-control-pages-setter
   ;;   #:(setf tab-control-pages)
   #:tab-control-tabs-position

   ;; List and table control panes

   #:<item>
   #:<list-control>
   #:<list-item>
   #:<table-column>
   #:<table-control>
   #:<table-item>
   #:add-column
   #:add-item
   #:find-item
   #:item-icon
   ;;   #:item-icon-setter
   ;;   #:(setf item-icon)
   #:item-label
   ;;   #:item-label-setter
   ;;   #:(setf item-label)
   #:item-object
   #:list-control-view
   ;;   #:list-control-view-setter
   ;;   #:(setf list-control-view)
   #:make-item
   #:remove-column
   #:remove-item
   #:table-control-view
   ;;   #:table-control-view-setter
   ;;   #:(setf table-control-view)

   #:<graph-control>
   #:<graph-edge>
   #:<graph-node>
   #:<graph-orientation>
   #:draw-edges
   #:graph-center-nodes?
   #:graph-edge-class
   #:graph-edge-initargs
   #:graph-edge-generator
   #:graph-edge-from-node
   #:graph-edge-to-node
   #:graph-edge-object
   #:graph-inter-generation-spacing
   #:graph-intra-generation-spacing
   #:graph-orientation
   #:node-x
   ;;   #:node-x-setter
   ;;   #:(setf node-x)
   #:node-y
   ;;   #:node-y-setter
   ;;   #:(setf node-y)

   ;; Tree control pane

   #:<tree-control>
   #:<tree-node>
   #:<node-state>
   #:add-node
   #:contract-node
   #:ensure-node-visible
   #:expand-node
   #:find-node
   #:make-node
   #:node-object
   #:remove-node
   #:tree-control-children-generator
   ;;   #:tree-control-children-generator-setter
   ;;   #:(setf tree-control-children-generator)
   #:tree-control-children-predicate
   ;;   #:tree-control-children-predicate-setter
   ;;   #:(setf tree-control-children-predicate)
   #:tree-control-initial-depth
   ;;   #:tree-control-initial-depth-setter
   ;;   #:(setf tree-control-initial-depth)
   #:tree-control-roots
   ;;   #:tree-control-roots-setter
   ;;   #:(setf tree-control-roots)
   #:tree-control-expanded-objects
   ;;   #:tree-control-expanded-objects-setter
   ;;   #:(setf tree-control-expanded-objects)
   #:tree-control-expanded-object-count
   #:tree-node-state-changed-callback
   ;;   #:tree-node-state-changed-callback-setter
   ;;   #:(setf tree-node-state-changed-callback)
   #:node-children
   ;;   #:node-children-setter
   ;;   #:(setf node-children)
   #:node-icon
   ;;   #:node-icon-setter
   ;;   #:(setf node-icon)
   #:node-label
   ;;   #:node-label-setter
   ;;   #:(setf node-label)
   #:node-parents
   #:node-state

   ;; Active labels

   #:<active-label>
   #:handle-semantic-event
   #:handle-semantic-button-event))



(defpackage :duim-gadgets
  (:use #:common-lisp
	#:dylan-commands
	#:duim-utilities
	#:duim-geometry
	#:duim-DCs
	#:duim-sheets
	#:duim-graphics
	#:duim-layouts
	#:duim-gadgets-internals)
  (:export

   ;; Gadgets, excluding collection gadgets

   #:<abstract-gadget>
   #:<action-gadget>
   #:<compound-label>
   #:<gadget>
   #:<label>
   #:<labelled-gadget>
   #:<page>
   #:<progress-bar>
   #:<slider>
   #:<value-gadget>
   #:<value-range-gadget>
   #:<gadget-orientation>  ;; type
   #:gadget-accelerator
   ;;   #:gadget-accelerator-setter
   ;;   #:(setf gadget-accelerator)
   #:gadget-activate-callback
   ;;   #:gadget-activate-callback-setter
   ;;   #:(setf gadget-activate-callback)
   #:gadget-client
   ;;   #:gadget-client-setter
   ;;   #:(setf gadget-client)
   #:gadget-command
   ;;   #:gadget-command-setter
   ;;   #:(setf gadget-command)
   #:gadget-documentation
   ;;   #:gadget-documentation-setter
   ;;   #:(setf gadget-documentation)
   #:gadget-enabled?
   ;;   #:gadget-enabled?-setter
   ;;   #:(setf gadget-enabled?)
   #:gadget-end-value
   #:gadget-id
   ;;   #:gadget-id-setter
   ;;   #:(setf gadget-id)
   #:gadget-key-press-callback
   ;;   #:gadget-key-press-callback-setter
   ;;   #:(setf gadget-key-press-callback)
   #:gadget-label
   ;;   #:gadget-label-setter
   ;;   #:(setf gadget-label)
   #:gadget-mnemonic
   ;;   #:gadget-mnemonic-setter
   ;;   #:(setf gadget-mnemonic)
   #:gadget-orientation
   #:gadget-popup-menu-callback
   ;;   #:gadget-popup-menu-callback-setter
   ;;   #:(setf gadget-popup-menu-callback)
   #:gadget-protection-callback
   ;;   #:gadget-protection-callback-setter
   ;;   #:(setf gadget-protection-callback)
   #:gadget-read-only?
   #:gadget-scrolling?
   #:gadget-scrolling-horizontally?
   #:gadget-scrolling-vertically?
   #:gadget-state
   ;;   #:gadget-state-setter
   ;;   #:(setf gadget-state)
   #:gadget-text-selection-changed-callback
   ;;   #:gadget-text-selection-changed-callback-setter
   ;;   #:(setf gadget-text-selection-changed-callback)
   #:gadget-slug-size
   ;;   #:gadget-slug-size-setter
   ;;   #:(setf gadget-slug-size)
   #:gadget-start-value
   #:gadget-supplies-scroll-bars?
   #:gadget-update-callback
   ;;   #:gadget-update-callback-setter
   ;;   #:(setf gadget-update-callback)
   #:gadget-value
   ;;   #:gadget-value-setter
   ;;   #:(setf gadget-value)
   #:gadget-value-changed-callback
   ;;   #:gadget-value-changed-callback-setter
   ;;   #:(setf gadget-value-changed-callback)
   #:gadget-value-changing-callback
   ;;   #:gadget-value-changing-callback-setter
   ;;   #:(setf gadget-value-changing-callback)
   #:gadget-value-increment
   #:gadget-value-range
   ;;   #:gadget-value-range-setter
   ;;   #:(setf gadget-value-range)
   #:gadget-value-type
   #:gadget-x-alignment
   #:gadget-y-alignment
   #:gadgetp
   #:labeling
   #:labelling		;; everyone always spells this "wrong"
   #:horizontal-line-scroll-amount
   #:normalize-gadget-value
   #:vertical-line-scroll-amount
   #:activate-gadget
   #:update-gadget

   ;; Text editing gadgets

   #:<active-text-range>
   #:<password-field>
   #:<rich-text-editor>
   #:<simple-text-range>
   #:<text-editor>
   #:<text-field>
   #:<text-gadget>
   #:<text-range>
   #:character-format
   ;;   #:character-format-setter
   ;;   #:(setf character-format)
   #:character-position
   #:position-character
   #:current-line
   #:find-text
   #:gadget-text
   ;;   #:gadget-text-setter
   ;;   #:(setf gadget-text)
   #:gadget-text-parser
   #:gadget-value-printer
   #:get-line
   #:line-index
   #:index-line
   #:line-length
   #:make-text-range
   #:paragraph-format
   ;;   #:paragraph-format-setter
   ;;   #:(setf paragraph-format)
   #:selected-text
   ;;   #:selected-text-setter
   ;;   #:(setf selected-text)
   #:text-caret-position
   ;;   #:text-caret-position-setter
   ;;   #:(setf text-caret-position)
   #:text-field-modified?
   ;;   #:text-field-modified?-setter
   ;;   #:(setf text-field-modified?)
   #:text-field-size
   #:text-field-text
   #:text-field-word-wrap?
   #:text-range-end
   ;;   #:text-range-end-setter
   ;;   #:(setf text-range-end)
   #:text-range-object
   ;;   #:text-range-object-setter
   ;;   #:(setf text-range-object)
   #:text-range-protected?
   ;;   #:text-range-protected?-setter
   ;;   #:(setf text-range-protected?)
   #:text-range-start
   ;;   #:text-range-start-setter
   ;;   #:(setf text-range-start)
   #:text-selection
   ;;   #:text-selection-setter
   ;;   #:(setf text-selection)

   ;; Collection gadgets (including menus)

   #:<button-box>
   #:<button>
   #:<check-box>
   #:<check-button>
   #:<check-menu-box>
   #:<check-menu-button>
   #:<collection-gadget>
   #:<combo-box>
   #:<list-box>
   #:<menu-bar>
   #:<menu-box>
   #:<menu-button>
   #:<menu>
   #:<option-box>
   #:<push-box>
   #:<push-button>
   #:<push-menu-box>
   #:<push-menu-button>
   #:<radio-box>
   #:<radio-button>
   #:<radio-menu-box>
   #:<radio-menu-button>
   #:<spin-box>
   #:<status-bar>
   #:<tool-bar>
   #:button-gadget-value
   #:display-menu
   #:gadget-default?
   ;;   #:gadget-default?-setter
   ;;   #:(setf gadget-default?)
   #:gadget-item-selected?
   #:gadget-items
   ;;   #:gadget-items-setter
   ;;   #:(setf gadget-items)
   #:gadget-label-key
   #:gadget-selection
   ;;   #:gadget-selection-setter
   ;;   #:(setf gadget-selection)
   #:gadget-selection-mode
   #:gadget-test
   #:gadget-value-key
   #:make-menu-from-items
   #:menu-owner
   #:status-bar-label-pane
   ;;   #:status-bar-label-pane-setter
   ;;   #:(setf status-bar-label-pane)
   #:status-bar-progress-bar
   ;;   #:status-bar-progress-bar-setter
   ;;   #:(setf status-bar-progress-bar)

   ;; Scrolling

   #:<scroll-bar>
   #:<scroller>
   #:<viewport>
   #:inhibit-updating-scroll-bars
   #:note-viewport-position-changed
   #:note-viewport-region-changed
   #:scrolling
   #:scroll-position
   #:set-scroll-position
   #:sheet-viewport
   #:sheet-viewport-region
   #:viewport-region
   #:viewport?

   ;; Borders

   #:<border>
   #:<group-box>
   #:<separator>
   #:<spacing>
   #:grouping
   #:with-border
   #:with-spacing

   ;; Splitters, tab controls, etc.

   #:<column-splitter>
   #:<splitter>
   #:<row-splitter>
   #:<tab-control>
   #:<tab-control-page>
   #:gadget-ratios
   ;;   #:gadget-ratios-setter
   ;;   #:(setf gadget-ratios)
   #:splitter-split-box-callback
   ;;   #:splitter-split-box-callback-setter
   ;;   #:(setf splitter-split-box-callback)
   #:splitter-split-bar-moved-callback
   ;;   #:splitter-split-bar-moved-callback-setter
   ;;   #:(setf splitter-split-bar-moved-callback)
   #:note-pages-changed
   #:page-initial-focus
   ;;   #:page-initial-focus-setter
   ;;   #:(setf page-initial-focus)
   #:tab-control-current-page
   ;;   #:tab-control-current-page-setter
   ;;   #:(setf tab-control-current-page)
   #:tab-control-labels
   #:tab-control-pages
   ;;   #:tab-control-pages-setter
   ;;   #:(setf tab-control-pages)
   #:tab-control-tabs-position

   ;; List and table control panes

   #:<item>
   #:<list-control>
   #:<list-item>
   #:<table-column>
   #:<table-control>
   #:<table-item>
   #:add-column
   #:add-item
   #:find-item
   #:item-icon
   ;;   #:item-icon-setter
   ;;   #:(setf item-icon)
   #:item-label
   ;;   #:item-label-setter
   ;;   #:(setf item-label)
   #:item-object
   #:list-control-view
   ;;   #:list-control-view-setter
   ;;   #:(setf list-control-view)
   #:make-item
   #:remove-column
   #:remove-item
   #:table-control-view
   ;;   #:table-control-view-setter
   ;;   #:(setf table-control-view)

   #:<graph-control>
   #:<graph-edge>
   #:<graph-node>
   #:<graph-orientation>  ;; type
   #:draw-edges
   #:graph-center-nodes?
   #:graph-edge-class
   #:graph-edge-initargs
   #:graph-edge-generator
   #:graph-edge-from-node
   #:graph-edge-to-node
   #:graph-edge-object
   #:graph-inter-generation-spacing
   #:graph-intra-generation-spacing
   #:graph-orientation
   #:node-x
   ;;   #:node-x-setter
   ;;   #:(setf node-x)
   #:node-y
   ;;   #:node-y-setter
   ;;   #:(setf node-y)

   ;; Tree control pane

   #:<tree-control>
   #:<tree-node>
   #:<node-state>  ;; type?
   #:add-node
   #:contract-node
   #:ensure-node-visible
   #:expand-node
   #:find-node
   #:make-node
   #:node-object
   #:remove-node
   #:tree-control-children-generator
   ;;   #:tree-control-children-generator-setter
   ;;   #:(setf tree-control-children-generator)
   #:tree-control-children-predicate
   ;;   #:tree-control-children-predicate-setter
   ;;   #:(setf tree-control-children-predicate)
   #:tree-control-initial-depth
   ;;   #:tree-control-initial-depth-setter
   ;;   #:(setf tree-control-initial-depth)
   #:tree-control-roots
   ;;   #:tree-control-roots-setter
   ;;   #:(setf tree-control-roots-setter)
   #:tree-control-expanded-objects
   ;;   #:tree-control-expanded-objects-setter
   ;;   #:(setf tree-control-expanded-objects)
   #:tree-control-expanded-object-count
   #:tree-node-state-changed-callback
   ;;   #:tree-node-state-changed-callback-setter
   ;;   #:(setf tree-node-state-changed-callback)
   #:node-children
   ;;   #:node-children-setter
   ;;   #:(setf node-children)
   #:node-icon
   ;;   #:node-icon-setter
   ;;   #:(setf node-icon)
   #:node-label
   ;;   #:node-label-setter
   ;;   #:(setf node-label)
   #:node-parents
   #:node-state

   ;; Active labels

   #:<active-label>
   #:handle-semantic-event
   #:handle-semantic-button-event))


#||
define library duim-gadgets
  use dylan;
  use commands;

  use duim-utilities;
  use duim-geometry;
  use duim-DCs;
  use duim-sheets;
  use duim-graphics;  
  use duim-layouts;

  export duim-gadgets;
  export duim-gadgets-internals;
end library duim-gadgets;

define module duim-gadgets
  // Gadgets, excluding collection gadgets
  create <abstract-gadget>,
	 <action-gadget>,
         <compound-label>,
	 <gadget>,
         <label>,
         <labelled-gadget>,
         <page>,
         <progress-bar>,
         <slider>,
         <value-gadget>,
         <value-range-gadget>,
         <gadget-orientation>,
         gadget-accelerator, gadget-accelerator-setter,
         gadget-activate-callback, gadget-activate-callback-setter,
         gadget-client, gadget-client-setter,
         gadget-command, gadget-command-setter,
         gadget-documentation, gadget-documentation-setter,
         gadget-enabled?, gadget-enabled?-setter,
         gadget-end-value,
         gadget-id, gadget-id-setter,
         gadget-key-press-callback, gadget-key-press-callback-setter,
         gadget-label, gadget-label-setter,
         gadget-mnemonic, gadget-mnemonic-setter,
         gadget-orientation,
         gadget-popup-menu-callback, gadget-popup-menu-callback-setter,
         gadget-protection-callback, gadget-protection-callback-setter,
         gadget-read-only?,
         gadget-scrolling?,
         gadget-scrolling-horizontally?,
         gadget-scrolling-vertically?,
         gadget-state, gadget-state-setter,
	 gadget-text-selection-changed-callback, gadget-text-selection-changed-callback-setter,
         gadget-slug-size, gadget-slug-size-setter,
         gadget-start-value,
         gadget-supplies-scroll-bars?,
         gadget-update-callback, gadget-update-callback-setter,
         gadget-value, gadget-value-setter,
         gadget-value-changed-callback, gadget-value-changed-callback-setter,
         gadget-value-changing-callback, gadget-value-changing-callback-setter,
         gadget-value-increment,
         gadget-value-range, gadget-value-range-setter,
	 gadget-value-type,
         gadget-x-alignment, gadget-y-alignment,
         gadget?,
	 \labeling, \labelling,		// everyone always spells this "wrong"
         horizontal-line-scroll-amount,
         normalize-gadget-value,
         vertical-line-scroll-amount,
         activate-gadget,
         update-gadget;

  // Text editing gadgets
  create <active-text-range>,
         <password-field>,
         <rich-text-editor>,
         <simple-text-range>,
         <text-editor>,
         <text-field>,
         <text-gadget>,
         <text-range>,
	 character-format, character-format-setter,
	 character-position, position-character,
	 current-line,
	 find-text,
         gadget-text, gadget-text-setter,
	 gadget-text-parser, gadget-value-printer,
	 get-line,
	 line-index, index-line,
	 line-length,
	 paragraph-format, paragraph-format-setter,
	 selected-text, selected-text-setter,
	 text-caret-position, text-caret-position-setter,
	 text-field-modified?, text-field-modified?-setter,
	 text-field-size,
	 text-field-text,
	 text-field-word-wrap?,
	 text-range-end, text-range-end-setter,
	 text-range-object, text-range-object-setter,
	 text-range-protected?, text-range-protected?-setter,
	 text-range-start, text-range-start-setter,
	 text-selection, text-selection-setter;

  // Collection gadgets (including menus)
  create <button-box>,
         <button>,
         <check-box>,
         <check-button>,
         <check-menu-box>,
         <check-menu-button>,
         <collection-gadget>,
         <combo-box>,
         <list-box>,
         <menu-bar>,
         <menu-box>,
         <menu-button>,
         <menu>,
         <option-box>,
         <push-box>,
         <push-button>,
         <push-menu-box>,
         <push-menu-button>,
         <radio-box>,
         <radio-button>,
         <radio-menu-box>,
         <radio-menu-button>,
         <spin-box>,
         <status-bar>,
         <tool-bar>,
         button-gadget-value,
         display-menu,
         gadget-default?, gadget-default?-setter,
         gadget-item-selected?,
         gadget-items, gadget-items-setter,
         gadget-label-key,
         gadget-selection, gadget-selection-setter,
         gadget-selection-mode,
         gadget-test,
         gadget-value-key,
         make-menu-from-items,
         menu-owner,
         status-bar-label-pane, status-bar-label-pane-setter,
         status-bar-progress-bar, status-bar-progress-bar-setter;

  // Scrolling
  create <scroll-bar>,
         <scroller>,
         <viewport>,
         \inhibit-updating-scroll-bars,
         note-viewport-position-changed,
         note-viewport-region-changed,
         \scrolling,
         scroll-position, set-scroll-position,
         sheet-viewport,
         sheet-viewport-region,
         viewport-region,
         viewport?;

  // Borders
  create <border>,
         <group-box>,
         <separator>,
         <spacing>,
         \grouping,
         \with-border,
         \with-spacing;

  // Splitters, tab controls, etc.
  create <column-splitter>,
	 <splitter>,
         <row-splitter>,
	 <tab-control>,
         <tab-control-page>,
         gadget-ratios, gadget-ratios-setter,
         splitter-split-box-callback, splitter-split-box-callback-setter,
         splitter-split-bar-moved-callback, splitter-split-bar-moved-callback-setter,
         note-pages-changed,
         page-initial-focus, page-initial-focus-setter,
	 tab-control-current-page, tab-control-current-page-setter,
         tab-control-labels,
         tab-control-pages, tab-control-pages-setter,
         tab-control-tabs-position;

  // List and table control panes
  create <item>,
         <list-control>,
         <list-item>,
         <table-column>,
	 <table-control>,
         <table-item>,
         add-column,
         add-item,
         find-item,
	 item-icon,  item-icon-setter,
	 item-label, item-label-setter,
         item-object,
	 list-control-view, list-control-view-setter,
         make-item,
         remove-column,
         remove-item,
         table-control-view, table-control-view-setter;

  create <graph-control>,
  	 <graph-edge>,
	 <graph-node>,
	 <graph-orientation>,
	 draw-edges,
	 graph-center-nodes?,
	 graph-edge-class,
	 graph-edge-initargs,
	 graph-edge-generator,
	 graph-edge-from-node,
	 graph-edge-to-node,
	 graph-edge-object,
	 graph-inter-generation-spacing,
	 graph-intra-generation-spacing,
	 graph-orientation,
	 node-x, node-x-setter,
	 node-y, node-y-setter;

  // Tree control panes
  create <tree-control>,
         <tree-node>,
         <node-state>,
         add-node,
         contract-node,
         ensure-node-visible,
	 expand-node,
         find-node,
         make-node,
         node-object,
         remove-node,
         tree-control-children-generator, tree-control-children-generator-setter,
         tree-control-children-predicate, tree-control-children-predicate-setter,
         tree-control-initial-depth, tree-control-initial-depth-setter,
	 tree-control-roots, tree-control-roots-setter,
	 tree-control-expanded-objects, tree-control-expanded-objects-setter,
	 tree-control-expanded-object-count,
         tree-node-state-changed-callback, tree-node-state-changed-callback-setter,
	 node-children, node-children-setter,
	 node-icon,  node-icon-setter,
	 node-label, node-label-setter,
         node-parents,
	 node-state;

  // Active labels
  create <active-label>,
	 handle-semantic-event, handle-semantic-button-event;
end module duim-gadgets;

define module duim-gadgets-internals
  use dylan;
  use commands;
  use duim-imports;
  use duim-utilities;
  use duim-geometry-internals;
  use duim-DCs-internals;
  use duim-sheets-internals;
  use duim-graphics-internals;
  use duim-layouts-internals;
  use duim-gadgets, export: all;

  // Gadgets, excluding collection gadgets
  export <accelerator>,
	 <accelerator-mixin>,
	 <action-gadget-mixin>,
	 <basic-action-gadget>,
	 <basic-gadget>,
	 <basic-page>,
	 <basic-value-gadget>,
	 <border-type>,
	 <bordered-gadget-mixin>,
	 <callback-client>,
	 <callback-type>,
	 <changing-value-gadget-mixin>,
	 <collection-gadget-state>,
	 <default-gadget-mixin>,
	 <gadget-command-mixin>,
	 <gadget-state>,
	 <key-press-gadget-mixin>,
	 <label-pane>,
	 <labelled-gadget-mixin>,
	 <mnemonic>,
	 <mnemonic-mixin>,
	 <no-value-gadget-mixin>,
	 <oriented-gadget-mixin>,
	 <popup-menu-gadget-mixin>,
	 <range-gadget-mixin>,
	 <range-gadget-state>,
	 <scroll-bar-type>,
	 <scrolling-gadget-mixin>,
	 <separator-pane>,
	 <slug-gadget-mixin>,
	 <slug-gadget-state>,
	 <text-gadget-state>,
	 <tree-control-state>,
	 <updatable-gadget-mixin>,
	 <value-gadget-mixin>,
	 <value-gadget-state>,
	 border-type, border-type-setter,
         callback-for-command,
	 compute-mnemonic-from-label,
         defaulted-gadget-accelerator,
	 defaulted-gadget-mnemonic,
	 do-gadget-value-setter,
	 draw-border,
	 draw-gadget-label,
         draw-separator,
         distribute-activate-callback,
	 distribute-column-click-callback,
	 distribute-key-press-callback,
	 distribute-popup-menu-callback,
         distribute-text-changed-callback,
         distribute-text-changing-callback,
         distribute-update-callback,
         distribute-value-changed-callback,
         distribute-value-changing-callback,
         execute-activate-callback, do-execute-activate-callback,
         execute-callback,
         execute-key-press-callback, do-execute-key-press-callback,
         execute-popup-menu-callback, do-execute-popup-menu-callback,
         execute-update-callback, do-execute-update-callback,
         execute-value-changed-callback, do-execute-value-changed-callback,
         execute-value-changing-callback, do-execute-value-changing-callback,
         gadget-flags, gadget-flags-setter,
	 gadget-keep-selection-visible?,
	 gadget-label-size,
         gadget-lines, gadget-columns,
         gadget-scroll-bars, gadget-scroll-bars-setter,
         handle-button-gadget-click,
         multi-line-label?,
	 note-gadget-disabled,
         note-gadget-enabled,
         note-gadget-label-changed,
         note-gadget-slug-size-changed,
         note-gadget-text-changed,
         note-gadget-value-changed,
         note-gadget-value-range-changed,
         push-button-like?;

  // Gadget events
  export <gadget-event>,
         <activate-gadget-event>,
         <key-press-gadget-event>,
         <popup-menu-gadget-event>,
 	 <text-changed-gadget-event>,
 	 <text-changing-gadget-event>,
         <update-gadget-event>,
 	 <value-changed-gadget-event>,
         <value-changing-gadget-event>,
         event-gadget;

  // Slider properties
  export slider-decimal-places,
         slider-max-label,
         slider-min-label,
	 slider-show-value?,
	 slider-tick-marks;

  // Text editing gadgets
  export <protected-gadget-mixin>,
	 <selection-change-mixin>,
	 execute-protection-callback, do-execute-protection-callback,
	 execute-text-selection-changed-callback, do-execute-text-selection-changed-callback,
         gadget-text-buffer, gadget-text-buffer-setter,
	 text-field-auto-scroll?,
         text-field-case,
         text-field-maximum-size;

  // Collection gadgets (including menus)
  export <basic-choice-gadget>,
         <collection-gadget-mixin>,
	 <gadget-box>,
         <gadget-selection-mixin>,
         <selection-changed-gadget-event>,
	 <selection-mode>,
         collection-gadget-default-label-key,
         collection-gadget-default-value-key,
         collection-gadget-item-label,
	 distribute-selection-changed-callback,
         gadget-item-label,
         gadget-item-value,
         help-menu?,
	 menu-owner-setter,
         note-gadget-items-changed,
         note-gadget-selection-changed,
         note-menu-attached,
         note-menu-detached,
         tear-off-menu?;

  // Scrolling
  export <scroller-pane>,
         <scrolling-sheet-mixin>,
         attach-scroll-bar,
         detach-scroll-bar,
         initialize-scrolling,
         line-scroll-amount,
         make-scrolling-layout,
         note-scroll-bar-changed,
	 page-scroll-amount,
         scroll-down-line,
         scroll-down-page,
         scroll-to-position,
         scroll-up-line,
         scroll-up-page,
         scroll-viewport, scroll-viewport-child,
	 sheet-horizontal-scroll-bar, sheet-horizontal-scroll-bar-setter,
         sheet-vertical-scroll-bar, sheet-vertical-scroll-bar-setter,
	 sheet-scrolls-horizontally?, sheet-scrolls-vertically?,
         sheet-scroll-range,
         sheet-visible-range, set-sheet-visible-range,
         shift-visible-region,
	 update-dynamic-scroll-bars,
	 update-scroll-bar, update-scroll-bars,
         viewport-region-setter;
  
  // Borders
  export border-thickness, border-thickness-setter,
         group-box-label-position;

  // Splitters, tab controls, etc.
  export execute-split-box-callback, do-execute-split-box-callback,
         execute-split-bar-moved-callback, do-execute-split-bar-moved-callback;

  // List and table control panes
  export <list-control-view>,
	 <table-control-view>,
	 do-add-column,
         do-add-item,
         do-find-item,
         do-make-item,
         do-remove-column,
         do-remove-item,
	 list-control-icon-function,
	 table-column-callback,
	 table-column-heading,
	 table-column-width,
	 table-column-alignment,
	 table-column-generator,
	 table-control-columns, table-control-columns-setter,
	 table-control-icon-function;

  // Tree control panes
  export <node-state-changed-gadget-event>,
	 distribute-node-state-changed-callback,
	 execute-node-state-changed-callback, do-execute-node-state-changed-callback,
	 do-add-node,
	 do-add-nodes,
         do-contract-node,
         do-expand-node,
         do-find-node,
         do-make-node,
         do-remove-node,
         note-tree-control-roots-changed,
	 tree-control-icon-function,
         tree-control-root-nodes, tree-control-root-nodes-setter,
	 tree-control-show-buttons?,
	 tree-control-show-edges?,
	 tree-control-show-root-edges?,
	 node-generation, node-generation-setter,
         node-state-setter,
         node-parents-setter;

  // Button box implementation classes
  export <check-box-pane>,
         <push-box-pane>,
         <radio-box-pane>,
         button-box-spacing,
         button-class-for-gadget-box,
	 button-first-in-group?,
         button-gadget-box,
	 button-in-tool-bar?,
         gadget-box-button-class,
         gadget-box-buttons,
         make-button-for-gadget-box,
         make-buttons-for-gadget-box;

  // Menu component implementation classes
  export <menu-box-pane>,
         <push-menu-box-pane>,
         <radio-menu-box-pane>,
         <check-menu-box-pane>;
end module duim-gadgets-internals;
||#
