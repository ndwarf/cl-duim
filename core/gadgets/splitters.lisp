;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GADGETS-INTERNALS -*-
(in-package #:duim-gadgets-internals)

#||
/// Splitters

define protocol <<splitter-protocol>> ()
  getter gadget-ratios
    (gadget :: <abstract-gadget>) => (ratios :: false-or(<sequence>));
  setter gadget-ratios-setter
    (ratios :: false-or(<sequence>), gadget :: <abstract-gadget>)
 => (ratios :: false-or(<sequence>));
end protocol <<splitter-protocol>>;
||#
#||
(define-protocol <<splitter-protocol>> ()
  (:getter gadget-ratios (gadget)
	   (:documentation
"
Returns the ratios of the children of _gadget_.
Defined by: <<SPLITTER-PROTOCOL>>
"))
  (:setter (setf gadget-ratios) (ratios gadget)
	   (:documentation
"
Sets the rations allocated to the children of _gadget_
to _ratios_.
Defined by: <<SPLITTER-PROTOCOL>>
")))
||#

(defgeneric execute-split-box-callback (gadget client id box))
(defgeneric do-execute-split-box-callback (gadget client id box))
(defgeneric execute-split-bar-moved-callback (gadget client id pane1 pane2))
(defgeneric do-execute-split-bar-moved-callback (gadget client id pane1 pane2))


#||
define open abstract class <splitter> (<layout>, <basic-gadget>)
  // Initial set of layout ratios for the splitter
  slot gadget-ratios :: false-or(<sequence>) = #f,
    init-keyword: ratios:;
  // This callback gets called when the split box is clicked on;
  // the callback is responsible for doing the actual splitting
  sealed slot splitter-split-box-callback :: <callback-type> = #f,
    init-keyword: split-box-callback:;
  // This callback gets called after a draggable border has been
  // moved and after the relayout has been done
  sealed slot splitter-split-bar-moved-callback :: <callback-type> = #f,
    init-keyword: split-bar-moved-callback:;
  sealed slot %horizontal-split-box? = #t,
    init-keyword: horizontal-split-box?:;
  sealed slot %vertical-split-box?   = #t,
    init-keyword: vertical-split-box?:;
end class <splitter>;
||#

(defclass <splitter>
    (<layout> <basic-gadget>)
  ;; Initial set of layout ratios for the splitter
  ((gadget-ratios :type (or null sequence) :initarg :ratios :initform nil :accessor gadget-ratios)
   ;; This callback gets called when the split box is clicked on;
   ;; the callback is responsible for doing the actual splitting
   (splitter-split-box-callback :type <callback-type> :initarg :split-box-callback :initform nil
				:accessor splitter-split-box-callback)
   ;; This callback gets called after a draggable border has been
   ;; moved and after the relayout has been done
   (splitter-split-bar-moved-callback :type <callback-type> :initarg :split-bar-moved-callback :initform nil
				      :accessor splitter-split-bar-moved-callback)
   (%horizontal-split-box? :initarg :horizontal-split-box? :initform t :accessor %horizontal-split-box?)
   (%vertical-split-box? :initarg :vertical-split-box? :initform t :accessor %vertical-split-box?))
  (:documentation
"
The class of splitter gadgets. Splitters are subclasses of both
<GADGET> and <LAYOUT>. Splitters (sometimes referred to as split bars
in Microsoft documentation) are gadgets that allow you to split a pane
into two resizable portions. For example, you could create a splitter
that would allow more than one view of a single document. In a word
processor, this may be used to let the user edit disparate pages on
screen at the same time.

A splitter consists of two components: a button that is used to create
the splitter component itself (referred to as a split box), and the
splitter component (referred to as the split bar). The split box is
typically placed adjacent to the scroll bar. When the user clicks on
the split box, a movable line is displayed in the associated pane
which, when clicked, creates the split bar.

The :split-box-callback initarg is an instance of type
'false-or(<function>)', and specifies the callback that is invoked
when the split box is clicked.

The :split-bar-moved-callback initarg is an instance of type
'false-or(<function>)', and specifies a callback that is invoked when
the user moves the split bar.

The :horizontal-split-box? initarg is an instance of type <boolean>,
and if true a horizontal split bar is created.

The :vertical-split-box? initarg is an instance of type <boolean>, and
if true a vertical split bar is created.
")
  (:metaclass <abstract-metaclass>))

;;; TODO: where to put the documentation for 'splitter-split-bar-moved-callback'?
#||
Returns the function invoked when the split bar of _splitter_ is
moved.

The _splitter_ argument is an instance of type <SPLITTER>. The
_function_ argument is an instance of type <function>.
||#
;;; TODO: where to put the documentation for '(setf splitter-split-bar-moved-callback)'?
#||
Sets the callback to be invoked when the split bar of _splitter_ is
moved.

The _splitter_ argument is an instance of type <SPLITTER>. The
_function_ argument is an instance of type <function>.
||#
;;; TODO: where to put the documentation for 'splitter-split-box-callback'?
#||
Returns the callback invoked when the split box of _splitter_ is
clicked.

The _splitter_ argument is an instance of type <SPLITTER>. The
_function_ argument is an instance of type <function>.
||#
;;; TODO: where to put the documentation for '(setf splitter-split-box-callback)'?
#||
Sets the callback invoked when the split box of splitter is clicked.

The splitter argument is an instance of type <splitter>. The function
argument is an instance of type <function>.
||#


#||
define method initialize
    (pane :: <splitter>, #key split-box? = $unsupplied) => ()
  next-method();
  when (supplied?(split-box?))
    pane.%horizontal-split-box? := split-box?;
    pane.%vertical-split-box?   := split-box?
  end
end method initialize;
||#

(defmethod initialize-instance :after ((pane <splitter>) &key (split-box? :unsupplied) &allow-other-keys)
  (when (supplied? split-box?)
    (setf (%horizontal-split-box? pane) split-box?)
    (setf (%vertical-split-box? pane) split-box?)))


#||
// Arranges kids in a row, allows a vertical split box
define open abstract class <row-splitter> (<splitter>)
  keyword horizontal-split-box?: = #f;
  keyword vertical-split-box?:   = #t;
end class <row-splitter>;
||#

(defclass <row-splitter> (<splitter>)
  ()
  (:default-initargs :horizontal-split-box? nil
                     :vertical-split-box? t)
  (:documentation
"
Arranges kids in a row, allows a vertical split box
")
  (:metaclass <abstract-metaclass>))


#||
// Arranges kids in a column, allows a horizontal split box
define open abstract class <column-splitter> (<splitter>)
  keyword horizontal-split-box?: = #t;
  keyword vertical-split-box?:   = #f;
end class <column-splitter>;
||#

(defclass <column-splitter> (<splitter>)
  ()
  (:default-initargs :horizontal-split-box? t
                     :vertical-split-box? nil)
  (:documentation
"
Arranges kids in a column, allows a horizontal split box
")
  (:metaclass <abstract-metaclass>))


#||
define method execute-split-box-callback
    (gadget :: <splitter>, client, id, box) => ()
  ignore(client, id);
  let callback = splitter-split-box-callback(gadget);
  if (callback)
    execute-callback(gadget, callback, gadget, box)
  else
    do-execute-split-box-callback(gadget, client, id, box)
  end
end method execute-split-box-callback;
||#

(defmethod execute-split-box-callback ((gadget <splitter>) client id box)
  (let ((callback (splitter-split-box-callback gadget)))
    (if callback
	(execute-callback gadget callback gadget box)
	(do-execute-split-box-callback gadget client id box))))


#||
define method do-execute-split-box-callback
    (gadget :: <splitter>, client, id, box) => ()
  ignore(client, id, box);
  #f
end method do-execute-split-box-callback;
||#

(defmethod do-execute-split-box-callback ((gadget <splitter>) client id box)
  (declare (ignore client id box))
  nil)


#||
define method execute-split-bar-moved-callback
    (gadget :: <splitter>, client, id, pane1, pane2) => ()
  ignore(client, id);
  let callback = splitter-split-bar-moved-callback(gadget);
  if (callback)
    execute-callback(gadget, callback, gadget, pane1, pane2)
  else
    do-execute-split-bar-moved-callback(gadget, client, id, pane1, pane2)
  end
end method execute-split-bar-moved-callback;
||#

(defmethod execute-split-bar-moved-callback ((gadget <splitter>) client id pane1 pane2)
  (let ((callback (splitter-split-bar-moved-callback gadget)))
    (if callback
	(execute-callback gadget callback gadget pane1 pane2)
	(do-execute-split-bar-moved-callback gadget client id pane1 pane2))))


#||
define method do-execute-split-bar-moved-callback
    (gadget :: <splitter>, client, id, pane1, pane2) => ()
  ignore(client, id);
  #f
end method do-execute-split-bar-moved-callback;
||#

(defmethod do-execute-split-bar-moved-callback ((gadget <splitter>) client id pane1 pane2)
  (declare (ignore client id pane1 pane2))
  nil)

