;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GADGETS-INTERNALS -*-
(in-package #:duim-gadgets-internals)

#||
/// Borders

// Outline panes draw a dark (foreground) border around a pane
define open abstract class <border> 
    // This stuff acts like <layout-pane>, but manages only a single child
    (<cached-space-requirement-mixin>,
     <client-overridability-mixin>,
     <wrapping-layout-mixin>,
     <single-child-mixin>,
     <bordered-gadget-mixin>,
     <basic-gadget>)
  sealed slot border-thickness :: <integer> = 1,
    init-keyword: thickness:;
end class <border>;
||#

(defclass <border>
    ;; This stuff acts like <layout-pane>, but manages only a single child
    (<cached-space-requirement-mixin>
     <client-overridability-mixin>
     <wrapping-layout-mixin>
     <single-child-mixin>
     <bordered-gadget-mixin>
     <basic-gadget>)
  ((border-thickness :type integer :initform 1 :initarg :thickness :accessor border-thickness))
  (:documentation
"
The base class of gadgets that provide borders for their children.

The thickness of the border is specified by the :thickness initarg,
and is given in pixels.

The :type initarg represents the kind of border to be created. Borders
may appear raised from the area they surround, or lowered with respect
to it. Alternatively, a border may be displayed as a thin ridge or
groove. Input and output borders represent \"logical\" borders.

Borders are usually created using the WITH-BORDER macro, rather than
by making direct instances of this class.
")
  (:metaclass <abstract-metaclass>))


#||
define method initialize
    (gadget :: <border>, #key type: borders = #f)
  next-method();
  border-type(gadget) := borders
end method initialize;
||#

(defmethod initialize-instance :after ((gadget <border>) &key ((:type borders) nil) &allow-other-keys)
  (setf (border-type gadget) borders))


#||
// Options can be any of the pane sizing options, plus THICKNESS: and TYPE:
define macro with-border
  { with-border (#rest ?options:expression)
      ?child:body
    end }
    => { let _child = ?child;		// child is a single expression
	 make(<border>, child: _child, ?options) }
end macro with-border;
||#

(defmacro with-border ((&rest options) &body child)
"
Creates _pane_ with a border around it, taking into account any of the
specified _options_.

The options specified may be any of the legal initargs used to specify
an instance of <border>. If no options are specified, then the default
border is used.

The pane is an expression whose return value is the sheet around which
a border should be placed.

Example:

To create a button in a border:

    (contain (with-border (:type :raised)
               (make-pane '<button> :label \"Hello\")))
"
  ;; :fixme: gensym is unnecessary here...
  (let ((_child (gensym "CHILD-")))
  `(let ((,_child ,@child))    ;; child is a single expression
     (make-pane '<border> :child ,_child ,@options))))


#||
define open generic draw-border
    (sheet :: <abstract-sheet>, medium :: <abstract-medium>, type :: <border-type>,
     left  :: <integer>, top    :: <integer>,
     right :: <integer>, bottom :: <integer>) => ();
||#

(defgeneric draw-border (sheet medium type left top right bottom))


#||
define method draw-border
    (sheet :: <sheet>, medium :: <medium>, type :: <border-type>,
     left  :: <integer>, top    :: <integer>,
     right :: <integer>, bottom :: <integer>) => ()
  with-drawing-options (medium, brush: default-foreground(sheet))
    draw-rectangle(medium, left, top, right, bottom, filled?: #f)
  end
end method draw-border;
||#

(defmethod draw-border ((sheet <sheet>) (medium <medium>) type
			(left integer) (top integer)
			(right integer) (bottom integer))
  (check-type type <border-type>)
  (with-drawing-options (medium :brush (default-foreground sheet))
    (draw-rectangle medium left top right bottom :filled? nil)))


#||
/// Spacing

// Spacing panes leaves whitespace (background) around a pane
define open abstract class <spacing> 
    //--- Like <layout-pane>, but manages only a single child
    (<cached-space-requirement-mixin>,
     <client-overridability-mixin>,
     <wrapping-layout-mixin>,
     <single-child-mixin>,
     <basic-gadget>)
  sealed slot border-thickness :: <integer> = 1,
    init-keyword: thickness:;
end class <spacing>;
||#

(defclass <spacing>
    (<cached-space-requirement-mixin>
     <client-overridability-mixin>
     <wrapping-layout-mixin>
     <single-child-mixin>
     <basic-gadget>)
  ((border-thickness :type integer :initform 1 :initarg :thickness :accessor border-thickness))
  (:documentation
"
The class of gadgets that can be used to provide spacing around a
sheet.

The :child initarg is the sheet or sheets that you are adding spacing
around.

The :thickness initarg specifies the thickness of the spacing
required.

It is usually clearer to use the WITH-SPACING macro, rather than to
create an instance of <SPACING> explicitly.

Example:

The following creates a vertical layout containing two buttons
separated by a text field that has spacing added to it.

    (contain (vertically ()
               (make-pane '<button> :label \"Hello\")
               (make-pane '<spacing>
                          :child (make-pane '<text-field>)
                          :thickness 10)
               (make-pane '<button> :label \"World\")))
")
  (:metaclass <abstract-metaclass>))


#||
define method border-type
    (pane :: <spacing>) => (type :: singleton(#f))
  #f
end method border-type;
||#

(defmethod border-type ((pane <spacing>))
  nil)


#||
define method initialize (sheet :: <spacing>, #key spacing) => ()
  next-method();
  when (spacing) 
    border-thickness(sheet) := spacing
  end
end method initialize;
||#

(defmethod initialize-instance :after ((sheet <spacing>) &key spacing &allow-other-keys)
  (when spacing
    (setf (border-thickness sheet) spacing)))


#||
// Options can be any of the pane sizing options, plus THICKNESS:
define macro with-spacing
  { with-spacing (#rest ?options:expression)
      ?child:body
    end }
    => { let _child = ?child;		// child is a single expression
	 make(<spacing>, child: _child, ?options) }
end macro with-spacing;
||#


(defmacro with-spacing ((&rest options) &body child)
"
Creates _pane_ with spacing around it, taking into account any of the
specified _options_.

The options specified may be any of the legal initargs used to specify
an instance of <spacing>. If no options are specified, then the
default spacing is used.

The pane is an expression whose return value is the sheet around which
spacing should be placed.

Example:

    (contain (with-spacing (:thickness 10)
               (vertically ()
                 (make-pane '<button> :label \"Hello\")
                 (make-pane '<button> :label \"World\"))))
"
  ;; :fixme: gensym is unnecessary here I think
  (let ((_child (gensym "CHILD-")))
  `(let ((,_child ,@child))    ;; child is a single expression
     (make-pane '<spacing> :child ,_child ,@options))))



#||

/// Group boxes, aka labelled borders

define open abstract class <group-box>
    (<labelled-gadget-mixin>,
     <border>)
  sealed slot group-box-label-position :: <vertical-position> = #"top",
    init-keyword: label-position:;
end class <group-box>;
||#

(defclass <group-box>
    (<labelled-gadget-mixin> <border>)
  ((group-box-label-position :type <vertical-position> :initform :top :initarg :label-position :accessor group-box-label-position))
  (:documentation
"
The class of gadgets that group their children using a labelled
border. You can use this gadget class to group together a number of
related items visually.

The :label initarg specifies a string or icon that is to be used as a
label for the gadget.

The :label-position initarg is used to specify whether the label
should be displayed along the top or the bottom edge of the border.

Internally, this class maps into the Windows group box control.

Example:

    (contain (make-pane '<group-box>
                        :child
                        (make-pane '<radio-box>
                                   :items #(1 2 3 4)
                                   :orientation :vertical)
                        :label \"Select integer:\"))
")
  (:metaclass <abstract-metaclass>))


#||
define macro grouping
  { grouping (?label:expression)
      ?child:body
    end }
    => { let _child = ?child;		// child is a single expression
	 make(<group-box>, child: _child, label: ?label) }
  { grouping (?label:expression, #rest ?options:expression)
      ?child:body
    end }
    => { let _child = ?child;		// child is a single expression
	 make(<group-box>, ?options, child: _child, label: ?label) }
end macro grouping;
||#

(defmacro grouping ((label &optional &rest options) &body child)
  (let ((_child (gensym "CHILD-")))
  `(let ((,_child ,@child))    ;; child is a single expression
     ,(if options
	  `(make-pane '<group-box> ,@options :child ,_child :label ,label)
	  `(make-pane '<group-box> :child ,_child :label ,label)))))



