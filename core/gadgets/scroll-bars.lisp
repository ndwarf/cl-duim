;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GADGETS-INTERNALS -*-
(in-package :duim-gadgets-internals)

#||
/// Scroll bars

define protocol <<scrolling-protocol>> ()
  function scroll-down-line   (gadget :: <abstract-gadget>) => ();
  function scroll-up-line     (gadget :: <abstract-gadget>) => ();
  function scroll-down-page   (gadget :: <abstract-gadget>) => ();
  function scroll-up-page     (gadget :: <abstract-gadget>) => ();
  function scroll-to-position (gadget :: <abstract-gadget>, position) => ();
  function note-scroll-bar-changed (scroll-bar :: <scroll-bar>) => ();
end protocol <<scrolling-protocol>>;
||#

;; DR: this is here because Dylan uses $MAXIMUM-INTEGER (and therefore
;;     so too does DUIM) so we define it too; it can be removed once
;; the porting is finished and Lispification is further advanced.

(defconstant +maximum-integer+ most-positive-fixnum)

#||
(define-protocol <<scrolling-protocol>> ()
  (:function scroll-down-line (gadget)
      (:documentation " Cause _gadget_ to be scrolled down by one line. Defined by: <<SCROLLING-PROTOCOL>> "))
  (:function scroll-up-line (gadget)
      (:documentation " Cause _gadget_ to be scrolled up by one line. Defined by: <<SCROLLING-PROTOCOL>> "))
  (:function scroll-down-page (gadget)
      (:documentation " Cause _gadget_ to be scrolled down by one page. Defined by: <<SCROLLING-PROTOCOL>> "))
  (:function scroll-up-page (gadget)
      (:documentation " Cause _gadget_ to be scrolled up by one page. Defined by: <<SCROLLING-PROTOCOL>> "))
  (:function scroll-to-position (gadget position)
      (:documentation " Cause _gadget_ to be scrolled to _position_. Defined by: <<SCROLLING-PROTOCOL>> "))
  (:function note-scroll-bar-changed (scroll-bar)
      (:documentation " Notification that _scroll-bar_ has been changed. Defined by: <<SCROLLING-PROTOCOL>> ")))
||#

(defgeneric attach-scroll-bar (sheet scroll-bar))
(defgeneric detach-scroll-bar (sheet scroll-bar))
(defgeneric (setf sheet-horizontal-scroll-bar) (scroll-bar sheet))
(defgeneric (setf sheet-vertical-scroll-bar) (scroll-bar sheet))
(defgeneric update-scroll-bar (scroll-bar contents-start contents-end visible-start visible-end))
(defgeneric scroll-attached-sheets (scroll-bar))
(defgeneric scroll-by-pixels (scroll-bar pixels))

#||
// Scroll bar definition
define open abstract class <scroll-bar>
  (<oriented-gadget-mixin>,
   <slug-gadget-mixin>,
   <changing-value-gadget-mixin>,
   <basic-value-gadget>)
  sealed slot %scrolling-sheets :: <list> = #();
  keyword value-changing-callback: = scroll-attached-sheets;
  keyword value-changed-callback:  = scroll-attached-sheets;
end class <scroll-bar>;
||#

(defclass <scroll-bar>
    (<oriented-gadget-mixin>
     <slug-gadget-mixin>
     <changing-value-gadget-mixin>
     <basic-value-gadget>)
  ((%scrolling-sheets :type list :initform () :accessor %scrolling-sheets))
  (:default-initargs :value-changing-callback #'scroll-attached-sheets
                     :value-changed-callback  #'scroll-attached-sheets)
  (:documentation
"
The instantiable class that implements an abstract scroll bar.

The :orientation initarg defines whether the scroll bar is horizontal
or vertical.

The :value-changing-callback initarg is the callback that is invoked
when the gadget value is in the process of changing, such as when the
scroll bar slug is dragged.

The :value-changed-callback initarg is the callback that is invoked
when the gadget value has changed, such as when the scroll bar slug
has come to rest after being dragged. You could use this callback, for
example, to refresh the screen in your application to show a different
part of a sheet, after the scroll bar has been moved.

The :slug-size initarg defines the size of the slug in the scroll bar,
as a proportion of :value-range. For example, if :value-range is from
0 to 100 and :slug-size is 25, then the slug occupies a quarter of the
total length of the scroll bar. The slug is the part of the scroll bar
that can be dragged up and down, and represents how much of the sheet
being scrolled is visible.

Note: the Microsoft Windows Interface Guidelines refer to the slug as
a 'scroll-box', and the area in which the slug can slide as the
'scroll-shaft'. You should be aware of this difference if you are
using those guidelines as a reference.

Internally, this class maps into the Windows scroll bar control.

Example:

As an example of how the :slug-size initarg operates, compare the two
examples of scroll bars below. The second scroll bar has a slug that
is twice the size of the first.

    (contain (make-pane '<scroll-bar>
                        :value-range (range :from 0 :to 100)
                        :slug-size 10))

    (contain (make-pane '<scroll-bar>
                        :value-range (range :from 0 :to 100)
                        :slug-size 20))
")
  (:metaclass <abstract-metaclass>))


#||
define method attach-scroll-bar
    (sheet :: <scrolling-sheet-mixin>, scroll-bar :: <scroll-bar>) => ()
  scroll-bar.%scrolling-sheets := add-new!(scroll-bar.%scrolling-sheets, sheet)
end method attach-scroll-bar;
||#

(defmethod attach-scroll-bar ((sheet <scrolling-sheet-mixin>) (scroll-bar <scroll-bar>))
  (setf (%scrolling-sheets scroll-bar) (add-new! (%scrolling-sheets scroll-bar) sheet)))


#||
define method detach-scroll-bar 
    (sheet :: <scrolling-sheet-mixin>, scroll-bar :: <scroll-bar>) => ()
  scroll-bar.%scrolling-sheets := remove!(scroll-bar.%scrolling-sheets, sheet)
end method detach-scroll-bar;
||#

(defmethod detach-scroll-bar ((sheet <scrolling-sheet-mixin>) (scroll-bar <scroll-bar>))
  (setf (%scrolling-sheets scroll-bar) (delete sheet (%scrolling-sheets scroll-bar))))


#||
/// The rest of <scrolling-sheet-mixin>...

define method sheet-horizontal-scroll-bar-setter
    (scroll-bar :: false-or(<scroll-bar>), sheet :: <scrolling-sheet-mixin>)
 => (scroll-bar :: false-or(<scroll-bar>))
  let old-scroll-bar = sheet-horizontal-scroll-bar(sheet);
  unless (scroll-bar == old-scroll-bar)
    old-scroll-bar & detach-scroll-bar(sheet, old-scroll-bar);
    sheet.%horizontal-scroll-bar := scroll-bar;
    scroll-bar & attach-scroll-bar(sheet, scroll-bar)
  end;
  scroll-bar
end method sheet-horizontal-scroll-bar-setter;
||#

(defmethod (setf sheet-horizontal-scroll-bar) ((scroll-bar <scroll-bar>) (sheet <scrolling-sheet-mixin>))
  (let ((old-scroll-bar (sheet-horizontal-scroll-bar sheet)))
    (unless (eql scroll-bar old-scroll-bar)
      (and old-scroll-bar (detach-scroll-bar sheet old-scroll-bar))
      (%horizontal-scroll-bar-setter scroll-bar sheet)
      (and scroll-bar (attach-scroll-bar sheet scroll-bar))))
  scroll-bar)

(defmethod (setf sheet-horizontal-scroll-bar) ((scroll-bar null) (sheet <scrolling-sheet-mixin>))
  (let ((old-scroll-bar (sheet-horizontal-scroll-bar sheet)))
    (unless (eql scroll-bar old-scroll-bar)
      (and old-scroll-bar (detach-scroll-bar sheet old-scroll-bar))
      (%horizontal-scroll-bar-setter scroll-bar sheet)
      (and scroll-bar (attach-scroll-bar sheet scroll-bar))))
  scroll-bar)


#||
define method sheet-vertical-scroll-bar-setter
    (scroll-bar :: false-or(<scroll-bar>), sheet :: <scrolling-sheet-mixin>)
 => (scroll-bar :: false-or(<scroll-bar>))
  let old-scroll-bar = sheet-vertical-scroll-bar(sheet);
  unless (scroll-bar == old-scroll-bar)
    old-scroll-bar & detach-scroll-bar(sheet, old-scroll-bar);
    sheet.%vertical-scroll-bar := scroll-bar;
    scroll-bar & attach-scroll-bar(sheet, scroll-bar)
  end;
  scroll-bar
end method sheet-vertical-scroll-bar-setter;
||#

(defmethod (setf sheet-vertical-scroll-bar) ((scroll-bar <scroll-bar>) (sheet <scrolling-sheet-mixin>))
  (let ((old-scroll-bar (sheet-vertical-scroll-bar sheet)))
    (unless (eql scroll-bar old-scroll-bar)
      (and old-scroll-bar (detach-scroll-bar sheet old-scroll-bar))
      (%vertical-scroll-bar-setter scroll-bar sheet)
      (and scroll-bar (attach-scroll-bar sheet scroll-bar))))
  scroll-bar)

(defmethod (setf sheet-vertical-scroll-bar) ((scroll-bar null) (sheet <scrolling-sheet-mixin>))
  (let ((old-scroll-bar (sheet-vertical-scroll-bar sheet)))
    (unless (eql scroll-bar old-scroll-bar)
      (and old-scroll-bar (detach-scroll-bar sheet old-scroll-bar))
      (%vertical-scroll-bar-setter scroll-bar sheet)
      (and scroll-bar (attach-scroll-bar sheet scroll-bar))))
  scroll-bar)



#||

/// Scroll bar updating

define sealed method update-scroll-bar
    (scroll-bar :: <scroll-bar>,
     contents-start :: <integer>, contents-end :: <integer>,
     visible-start :: <integer>, visible-end :: <integer>) => ()
  let (scroll-range, scroll-start, scroll-end, increment)
    = range-values(gadget-value-range(scroll-bar));
  ignore(scroll-range);
  let new-value = visible-start;
  let new-size  = visible-end - visible-start + increment;
  let new-range
    = if (scroll-start = contents-start
	  & scroll-end = contents-end)
	gadget-value-range(scroll-bar)
      else
	range(from: contents-start, to: contents-end)
      end;
  // We do this "the internal way" to avoid flashing the scroll bar
  // in the back end, which would happen if we individually updated
  // the range, then the value, then the slug size
  when (  gadget-value(scroll-bar)       ~= new-value
	| gadget-slug-size(scroll-bar)   ~= new-size
	| gadget-value-range(scroll-bar) ~= new-range)
    scroll-bar.%value       := new-value;
    scroll-bar.%slug-size   := new-size;
    scroll-bar.%value-range := new-range;
    note-scroll-bar-changed(scroll-bar)
  end
end method update-scroll-bar;
||#

(defmethod update-scroll-bar ((scroll-bar <scroll-bar>)
			      (contents-start integer) (contents-end integer)
                              (visible-start integer) (visible-end integer))
  (multiple-value-bind (scroll-range scroll-start scroll-end increment)
      (range-values (gadget-value-range scroll-bar))
    (declare (ignore scroll-range))
    (let ((new-value visible-start)
          (new-size  (+ (- visible-end visible-start) increment))   ; new slug size
          (new-range (if (and (= scroll-start contents-start)
                              (= scroll-end contents-end))
                         (gadget-value-range scroll-bar)
			 (range :from contents-start :to contents-end))))
      ;; We do this "the internal way" to avoid flashing the scroll bar
      ;; in the back end, which would happen if we individually updated
      ;; the range, then the value, then the slug size
      (when (or (not (= (gadget-value scroll-bar)       new-value))
                (not (= (gadget-slug-size scroll-bar)   new-size))
                (not (equal? (gadget-value-range scroll-bar) new-range)))
        (%value-setter new-value scroll-bar)
        (%slug-size-setter new-size scroll-bar)
        (%value-range-setter new-range scroll-bar)
        (note-scroll-bar-changed scroll-bar)))))


#||
define method note-scroll-bar-changed
    (gadget :: <scroll-bar>) => ()
  #f
end method note-scroll-bar-changed;
||#

(defmethod note-scroll-bar-changed ((gadget <scroll-bar>))
  nil)


#||
define method scroll-attached-sheets
    (scroll-bar :: <scroll-bar>) => ()
  for (sheet in scroll-bar.%scrolling-sheets)
    let _size  = gadget-slug-size(scroll-bar);
    let _start = gadget-value(scroll-bar);
    let _end   = _start + _size;
    let (vleft, vtop, vright, vbottom) = sheet-visible-range(sheet);
    select (gadget-orientation(scroll-bar))
      #"vertical" =>
	set-sheet-visible-range(sheet, vleft, _start, vright, _end);
      #"horizontal" =>
	set-sheet-visible-range(sheet, _start, vtop, _end, vbottom);
    end
  end
end method scroll-attached-sheets;
||#

(defmethod scroll-attached-sheets ((scroll-bar <scroll-bar>))
  (map nil #'(lambda (sheet)
	       (let* ((_size  (gadget-slug-size scroll-bar))
		      (_start (gadget-value scroll-bar))
		      (_end   (+ _start _size)))
		 (multiple-value-bind (vleft vtop vright vbottom)
		     (sheet-visible-range sheet)
		   (ecase (gadget-orientation scroll-bar)
		     (:vertical (set-sheet-visible-range sheet vleft _start vright _end))
		     (:horizontal (set-sheet-visible-range sheet _start vtop _end vbottom))))))
       (%scrolling-sheets scroll-bar)))


;;; TODO: Totally the wrong place for these, but the frames documentation in the DUIM reference
;;; indicates 2 things of interest... firstly, that when the "application" frame is exited (this
;;; would be the "Tests" frame for the GUI tests), the whole application exits, which addresses
;;; one of my open queries, and secondly that of the two syntaxes for specifying commands in
;;; command tables:
;;;
;;;    menu-item "My command" = make(<command>, function: my-command)
;;; and
;;;    menu-item "My command" = my-command
;;;
;;; only the second is valid. This might clean up the command table defining macros a bit.

#||
define method line-scroll-amount
    (scroll-bar :: <scroll-bar>, orientation :: <gadget-orientation>)
 => (amount :: <integer>)
  let sheets = scroll-bar.%scrolling-sheets;
  case
    ~empty?(sheets) =>
      let amount :: <integer> = $maximum-integer;
      for (sheet in sheets)
	let sheet-amount = line-scroll-amount(sheet, orientation);
	amount := min(amount, sheet-amount)
      end;
      amount;
    otherwise =>
      // Scroll bar is not attached through a scroller,
      // so see if we can find a plausible client sheet
      let client = gadget-client(scroll-bar);
      if (sheet?(client) & ~instance?(client, <scroller>))
	line-scroll-amount(client, orientation)
      else
	max(floor(gadget-value-increment(scroll-bar) * 3), 10)
      end;
  end
end method line-scroll-amount;
||#

(defmethod line-scroll-amount ((scroll-bar <scroll-bar>) orientation)
  (check-type orientation <gadget-orientation>)
  (let ((sheets (%scrolling-sheets scroll-bar)))
    (cond ((not (empty? sheets))
           (let ((amount +maximum-integer+))
             (loop for sheet in sheets
		do (let ((sheet-amount (line-scroll-amount sheet orientation)))
		     (setf amount (min amount sheet-amount))))
             amount))
          (t
           ;; Scroll bar is not attached through a scroller,
           ;; so see if we can find a plausible client sheet
           (let ((client (gadget-client scroll-bar)))
             (if (and (sheetp client) (not (typep client '<scroller>)))
                 (line-scroll-amount client orientation)
                 (max (floor (* (gadget-value-increment scroll-bar) 3)) 10)))))))


#||
define method page-scroll-amount 
    (scroll-bar :: <scroll-bar>, orientation :: <gadget-orientation>)
 => (amount :: <integer>)
  let sheets = scroll-bar.%scrolling-sheets;
  case
    ~empty?(sheets) =>
      let amount :: <integer> = $maximum-integer;
      for (sheet in sheets)
	let sheet-amount = page-scroll-amount(sheet, orientation);
	amount := min(amount, sheet-amount)
      end;
      amount;
    otherwise =>
      let client = gadget-client(scroll-bar);
      if (sheet?(client) & ~instance?(client, <scroller>))
	page-scroll-amount(client, orientation)
      else
	floor(gadget-slug-size(scroll-bar))
      end;
  end
end method page-scroll-amount;
||#

;; FIXME: IS 'TYPEP CLIENT <SCROLLER>' REALLY THE SAME AS 'INSTANCE? CLIENT <SCROLLER>'?
;; Yes, yes it is

(defmethod page-scroll-amount ((scroll-bar <scroll-bar>) orientation)
  (check-type orientation <gadget-orientation>)
  (let ((sheets (%scrolling-sheets scroll-bar)))
    (cond ((not (empty? sheets))
           (let ((amount +maximum-integer+))
             (loop for sheet in sheets
		do (let ((sheet-amount (page-scroll-amount sheet orientation)))
		     (setf amount (min amount sheet-amount))))
             amount))
          (t
           (let ((client (gadget-client scroll-bar)))
             (if (and (sheetp client) (not (typep client '<scroller>)))
                 (page-scroll-amount client orientation)
                 (floor (gadget-slug-size scroll-bar))))))))


#||
define method scroll-by-pixels 
    (scroll-bar :: <scroll-bar>, pixels :: <integer>) => ()
  let old-value = gadget-value(scroll-bar);
  let increment = gadget-value-increment(scroll-bar);
  let new-pixel-position
    = floor/(old-value, if (increment = 0) 1 else increment end) + pixels;
  let new-value
    = select (increment by instance?)
	<integer> => floor(new-pixel-position * increment);
	<real>    => new-pixel-position * increment;
      end;
  gadget-value(scroll-bar, do-callback?: #t) := new-value
end method scroll-by-pixels;
||#

(defmethod scroll-by-pixels ((scroll-bar <scroll-bar>) (pixels integer))
  (let* ((old-value (gadget-value scroll-bar))
         (increment (gadget-value-increment scroll-bar))
         (new-pixel-position (+ (floor old-value (if (= increment 0) 1 increment)) pixels))
         (new-value (etypecase increment
                      (integer (floor (* new-pixel-position increment)))
                      (real    (* new-pixel-position increment)))))
    (setf (gadget-value scroll-bar :do-callback? t) new-value)))


#||
define method scroll-up-line (scroll-bar :: <scroll-bar>) => ()
  scroll-by-pixels(scroll-bar, -gadget-line-scroll-amount(scroll-bar))
end method scroll-up-line;
||#

(defmethod scroll-up-line ((scroll-bar <scroll-bar>))
  (scroll-by-pixels scroll-bar (- (gadget-line-scroll-amount scroll-bar))))


#||
define method scroll-down-line (scroll-bar :: <scroll-bar>) => ()
  scroll-by-pixels(scroll-bar, gadget-line-scroll-amount(scroll-bar))
end method scroll-down-line;
||#

(defmethod scroll-down-line ((scroll-bar <scroll-bar>))
  (scroll-by-pixels scroll-bar (gadget-line-scroll-amount scroll-bar)))


#||
define method scroll-up-page (scroll-bar :: <scroll-bar>) => ()
  scroll-by-pixels(scroll-bar, -gadget-page-scroll-amount(scroll-bar))
end method scroll-up-page;
||#

(defmethod scroll-up-page ((scroll-bar <scroll-bar>))
  (scroll-by-pixels scroll-bar (- (gadget-page-scroll-amount scroll-bar))))


#||
define method scroll-down-page (scroll-bar :: <scroll-bar>) => ()
  scroll-by-pixels(scroll-bar, gadget-page-scroll-amount(scroll-bar))
end method scroll-down-page;
||#

(defmethod scroll-down-page ((scroll-bar <scroll-bar>))
  (scroll-by-pixels scroll-bar (gadget-page-scroll-amount scroll-bar)))


#||
define method scroll-to-position
    (scroll-bar :: <scroll-bar>, value :: <real>) => ()
  gadget-value(scroll-bar, do-callback?: #t) := value
end method scroll-to-position;
||#

(defmethod scroll-to-position ((scroll-bar <scroll-bar>) (value real))
  (setf (gadget-value scroll-bar :do-callback? t) value))


#||
define method scroll-to-position
    (scroll-bar :: <scroll-bar>, value == #"top") => ()
  gadget-value(scroll-bar, do-callback?: #t) := gadget-start-value(scroll-bar)
end method scroll-to-position;
||#

(defmethod scroll-to-position ((scroll-bar <scroll-bar>) (value (eql :top)))
  (setf (gadget-value scroll-bar :do-callback? t) (gadget-start-value scroll-bar)))


#||
define method scroll-to-position 
    (scroll-bar :: <scroll-bar>, value == #"bottom") => ()
  let size = gadget-slug-size(scroll-bar);
  gadget-value(scroll-bar, do-callback?: #t)
    := gadget-end-value(scroll-bar) - size
end method scroll-to-position;
||#

(defmethod scroll-to-position ((scroll-bar <scroll-bar>) (value (eql :bottom)))
  (let ((size (gadget-slug-size scroll-bar)))
    (setf (gadget-value scroll-bar :do-callback? t)
          (- (gadget-end-value scroll-bar) size))))


