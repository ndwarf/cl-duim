;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GADGETS-INTERNALS -*-
(in-package #:duim-gadgets-internals)

#||
/// Text editing gadgets

// Selection change notifications
define protocol <<selection-change-protocol>> (<<gadget-protocol>>)
  function execute-text-selection-changed-callback
    (gadget :: <abstract-gadget>, client, id) => ();
  function do-execute-text-selection-changed-callback
    (gadget :: <abstract-gadget>, client, id) => ();
  getter gadget-text-selection-changed-callback
    (gadget :: <abstract-gadget>) => (callback :: <callback-type>);
  setter gadget-text-selection-changed-callback-setter
    (callback :: <callback-type>, gadget :: <abstract-gadget>)
 => (callback :: <callback-type>);
end protocol <<selection-change-protocol>>;
||#
#||
(define-protocol <<selection-change-protocol>> (<<gadget-protocol>>)
  (:function execute-text-selection-changed-callback (gadget client id) (:documentation " Defined by: <<SELECTION-CHANGE-PROTOCOL>> "))
  (:function do-execute-text-selection-changed-callback (gadget client id) (:documentation " Defined by: <<SELECTION-CHANGE-PROTOCOL>> "))
  (:getter gadget-text-selection-changed-callback (gadget) (:documentation " Defined by: <<SELECTION-CHANGE-PROTOCOL>> "))
  (:setter (setf gadget-text-selection-changed-callback) (callback gadget) (:documentation " Defined by: <<SELECTION-CHANGE-PROTOCOL>> ")))
||#

(defgeneric text-field-case (gadget))
(defgeneric text-field-auto-scroll? (gadget))


#||
define open abstract class <selection-change-mixin> (<abstract-gadget>)
  sealed slot gadget-text-selection-changed-callback :: <callback-type> = #f,
    init-keyword: text-selection-changed-callback:;
end class <selection-change-mixin>;
||#

(defclass <selection-change-mixin>
    (<abstract-gadget>)
  ((gadget-text-selection-changed-callback :type <callback-type> :initarg :text-selection-changed-callback
					   :initform nil :accessor gadget-text-selection-changed-callback))
  (:metaclass <abstract-metaclass>))


#||
define method execute-text-selection-changed-callback
    (gadget :: <selection-change-mixin>, client, id) => ()
  ignore(client, id);
  let callback = gadget-text-selection-changed-callback(gadget);
  if (callback)
    execute-callback(gadget, callback, gadget)
  else
    do-execute-text-selection-changed-callback(gadget, client, id)
  end
end method execute-text-selection-changed-callback;
||#

(defmethod execute-text-selection-changed-callback ((gadget <selection-change-mixin>) client id)
  (let ((callback (gadget-text-selection-changed-callback gadget)))
    (if callback
	(execute-callback gadget callback gadget)
	(do-execute-text-selection-changed-callback gadget client id))))


#||
define method do-execute-text-selection-changed-callback
    (gadget :: <selection-change-mixin>, client, id) => ()
  ignore(client, id);
  #f
end method do-execute-text-selection-changed-callback;
||#

(defmethod do-execute-text-selection-changed-callback ((gadget <selection-change-mixin>) client id)
  (declare (ignore client id))
  nil)


#||
// Protection notifications
define protocol <<protected-gadget-protocol>> (<<gadget-protocol>>)
  function execute-protection-callback
    (gadget :: <abstract-gadget>, client, id, range) => ();
  function do-execute-protection-callback
    (gadget :: <abstract-gadget>, client, id, range) => ();
  getter gadget-protection-callback
    (gadget :: <abstract-gadget>) => (callback :: <callback-type>);
  setter gadget-protection-callback-setter
    (callback :: <callback-type>, gadget :: <abstract-gadget>)
 => (callback :: <callback-type>);
end protocol <<protected-gadget-protocol>>;
||#
#||
(define-protocol <<protected-gadget-protocol>> (<<gadget-protocol>>)
  (:function execute-protection-callback (gadget client id range)
	     (:documentation
"
Defined by: <<PROTECTED-GADGET-PROTOCOL>>
"))
  (:function do-execute-protection-callback (gadget client id range)
	     (:documentation
"
Defined by: <<PROTECTED-GADGET-PROTOCOL>>
"))
  (:getter gadget-protection-callback (gadget)
	   (:documentation
"
Defined by: <<PROTECTED-GADGET-PROTOCOL>>
"))
  (:setter (setf gadget-protection-callback) (callback gadget)
	   (:documentation
"
Defined by: <<PROTECTED-GADGET-PROTOCOL>>
")))
||#


#||
define open abstract class <protected-gadget-mixin> (<abstract-gadget>)
  sealed slot gadget-protection-callback :: <callback-type> = #f,
    init-keyword: protection-callback:;
end class <protected-gadget-mixin>;
||#

(defclass <protected-gadget-mixin>
    (<abstract-gadget>)
  ((gadget-protection-callback :type <callback-type> :initarg :protection-callback :initform nil
			       :accessor gadget-protection-callback))
  (:metaclass <abstract-metaclass>))


#||
define method execute-protection-callback
    (gadget :: <protected-gadget-mixin>, client, id, range) => ()
  ignore(client, id);
  let callback = gadget-protection-callback(gadget);
  if (callback)
    execute-callback(gadget, callback, gadget, range)
  else
    do-execute-protection-callback(gadget, client, id, range)
  end
end method execute-protection-callback;
||#

(defmethod execute-protection-callback ((gadget <protected-gadget-mixin>) client id range)
  (let ((callback (gadget-protection-callback gadget)))
    (if callback
	(execute-callback gadget callback gadget range)
	(do-execute-protection-callback gadget client id range))))


#||
define method do-execute-protection-callback
    (gadget :: <protected-gadget-mixin>, client, id, range) => ()
  ignore(client, id, range);
  #f
end method do-execute-protection-callback;
||#

(defmethod do-execute-protection-callback ((gadget <protected-gadget-mixin>) client id range)
  (declare (ignore client id range))
  nil)



#||

/// Text gadget protocols and support classes

define open abstract class <text-gadget> (<value-gadget>) end;
||#

(defclass <text-gadget> (<value-gadget>)
  ()
  (:documentation
"
The class of all text gadgets. You should not create a direct instance
of this class.

The :text initarg specifies a text value for the text gadget.

The :value-type initarg specifies the type of the gadget value of the
text gadget, which by default is string. Other supported types are
integer and symbol. The string entered in the text gadget is parsed,
and converted to the appropriate type automatically.

Text gadgets have a method on 'gadget-value' that converts the
'gadget-text' based on the 'gadget-value-type', for example converting
the string to an integer for ':value-type (find-class 'integer)'.

The 'gadget-text' generic function always returns the exact text typed
into a text gadget. However, 'gadget-value' always returns a
\"parsed\" value of the appropriate type, depending on the value of
'gadget-value-type'. If the string contains any characters that are
not appropriate to the 'gadget-value-type' (for example, if the string
contains any non-integers, and the 'gadget-value-type' is integer),
then 'gadget-value' returns nil.

Setting the gadget value \"prints\" the value and inserts the
appropriate text into the text field.

The :value-changing-callback initarg allows you to specify a callback
that is invoked as the value of the text gadget is changing during the
course of \"casual\" typing. Generally, this means when the user is
typing text, but before the text is committed (usually by pressing the
RETURN key).

Conversely, the value-changed callback of a text gadget is invoked
when the change to the gadget value is committed (again, usually by
pressing the RETURN key).

The action required to \"commit\" a text change is defined by the
back-end for the platform that you are writing for, and is not
configurable.

Example:

    (contain (make-pane '<text-field>
                        :value-type (find-class 'integer)
                        :text \"1234\"))
")
  (:metaclass <abstract-metaclass>))

#||
define open abstract class <text-range> (<object>) end;
||#

(defclass <text-range> ()
  ()
  (:metaclass <abstract-metaclass>))


#||
define protocol <<text-field-protocol>> (<<value-gadget-protocol>>)
  getter gadget-text
    (gadget :: <text-gadget>) => (text :: <string>);
  setter gadget-text-setter
    (text :: <string>, gadget :: <text-gadget>, #key do-callback?)
 => (text :: <string>);
  getter gadget-text-buffer
    (gadget :: <text-gadget>) => (text :: <string>);
  setter gadget-text-buffer-setter
    (text :: <string>, gadget :: <text-gadget>)
 => (text :: <string>);
  function note-gadget-text-changed
    (gadget :: <text-gadget>) => ();
  function gadget-text-parser
    (type :: <type>, text :: <string>) => (value);
  function gadget-value-printer
    (type :: <type>, value) => (text :: <string>);
  // Get and set the current selection
  function text-selection
    (gadget :: <text-gadget>) => (range :: type-union(<text-range>, one-of(#f)));
  function text-selection-setter
    (range :: type-union(<text-range>, one-of(#t, #f)), gadget :: <text-gadget>)
 => (range :: type-union(<text-range>, one-of(#t, #f)));
  // Return or replace the current selection.  Note that this works
  // on all sorts of sheets, not just text gadgets
  function selected-text
    (gadget :: <abstract-sheet>) => (string :: false-or(<string>));
  function selected-text-setter
    (string :: false-or(<string>), gadget :: <abstract-sheet>)
 => (string :: false-or(<string>));
  function text-field-modified?
    (gadget :: <text-gadget>) => (modified? :: <boolean>);
  function text-field-modified?-setter
    (modified? :: <boolean>, gadget :: <text-gadget>)
 => (modified? :: <boolean>);
  // These can be more efficient than using 'gadget-value'
  function text-field-size
    (gadget :: <text-gadget>) => (size :: <integer>);
  function text-field-text
    (gadget :: <text-gadget>, range :: <text-range>)
 => (string :: false-or(<string>));
  // Get and set the current caret position
  function text-caret-position
    (gadget :: <text-gadget>) => (index :: false-or(<integer>));
  function text-caret-position-setter
    (index :: false-or(<integer>), gadget :: <text-gadget>)
 => (index :: false-or(<integer>));
  // Mapping indices to caret positions, and vice-versa
  function character-position
    (gadget :: <text-gadget>, x, y) => (index :: <integer>);
  function position-character
    (gadget :: <text-gadget>, index :: <integer>) => (x, y);
end protocol <<text-field-protocol>>;
||#
#||
(define-protocol <<text-field-protocol>> (<<value-gadget-protocol>>)
  (:getter gadget-text (gadget) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  (:setter (setf gadget-text) (text gadget &key do-callback?) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  (:getter gadget-text-buffer (gadget) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  (:setter (setf gadget-text-buffer) (text gadget) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  (:function note-gadget-text-changed (gadget) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  (:function gadget-text-parser (type text) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  (:function gadget-value-printer (type value) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  ;; Get and set the current selection
  (:function text-selection (gadget) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  (:function (setf text-selection) (range gadget) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  ;; Return or replace the current selection.  Note that this works
  ;; on all sorts of sheets, not just text gadgets
  (:function selected-text (gadget) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  (:function (setf selected-text) (string gadget) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  (:function text-field-modified? (gadget) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  (:function (setf text-field-modified?) (modified? gadget) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  ;; These can be more efficient than using 'gadget-value'
  (:function text-field-size (gadget) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  (:function text-field-text (gadget range) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  ;; Get and set the current caret position
  (:function text-caret-position (gadget) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  (:function (setf text-caret-position) (index gadget) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  ;; Mapping indices to caret positions, and vice-versa
  (:function character-position (gadget x y) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  (:function position-character (gadget index) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> ")))
||#


#||
// Default methods for these are no-ops
define method selected-text
    (sheet :: <sheet>) => (string :: false-or(<string>))
  #f
end method selected-text;
||#

(defmethod selected-text ((sheet <sheet>))
  nil)


#||
define method selected-text-setter
    (string :: false-or(<string>), sheet :: <sheet>)
 => (string :: false-or(<string>))
  string
end method selected-text-setter;
||#

(defmethod (setf selected-text) ((string string) (sheet <sheet>))
  string)

(defmethod (setf selected-text) ((string null) (sheet <sheet>))
  string)


#||
define sealed class <simple-text-range> (<text-range>)
  sealed slot text-range-start :: <integer>,
    required-init-keyword: start:;
  sealed slot text-range-end :: <integer>,
    required-init-keyword: end:;
end class <simple-text-range>;

define sealed domain make (singleton(<simple-text-range>));
define sealed domain initialize (<simple-text-range>);
||#

(defclass <simple-text-range> (<text-range>)
  ((text-range-start :type integer :initarg :start :initform (required-slot ":start" "<simple-text-range>") :accessor text-range-start)
   (text-range-end :type integer :initarg :end :initform (required-slot ":end" "<simple-text-range>") :accessor text-range-end)))


#||
define sealed inline method make
    (class == <text-range>, #rest initargs, #key)
 => (range :: <simple-text-range>)
  dynamic-extent(initargs);
  apply(make, <simple-text-range>, initargs)
end method make;
||#

(defun make-text-range (&rest initargs &key &allow-other-keys)
  (declare (dynamic-extent initargs))
  (apply #'make-instance '<simple-text-range> initargs))


#||
define sealed class <active-text-range> (<simple-text-range>)
  sealed slot text-range-object = #f,
    init-keyword: object:;
end class <active-text-range>;

define sealed domain make (singleton(<active-text-range>));
define sealed domain initialize (<active-text-range>);
||#

(defclass <active-text-range>
    (<simple-text-range>)
  ((text-range-object :initarg :object :initform nil :accessor text-range-object)))


#||
/// Text gadget state

define sealed class <text-gadget-state> (<gadget-state>)
  sealed constant slot %state-text,
    required-init-keyword: text:;
end class <text-gadget-state>;

define sealed domain make (singleton(<text-gadget-state>));
define sealed domain initialize (<text-gadget-state>);
||#

(defclass <text-gadget-state>
    (<gadget-state>)
  ((%state-text :initarg :text :initform (required-slot ":text" "<text-gadget-state>") :reader %state-text)))


#||
define method gadget-state
    (gadget :: <text-gadget>) => (state :: <text-gadget-state>)
  make(<text-gadget-state>,
       text: gadget-text(gadget))
end method gadget-state;
||#

(defmethod gadget-state ((gadget <text-gadget>))
  (make-instance '<text-gadget-state>
		 :text (gadget-text gadget)))


#||
define method gadget-state-setter
    (state :: <text-gadget-state>, gadget :: <text-gadget>)
 => (state :: <text-gadget-state>)
  gadget-text(gadget) := state.%state-text;
  state
end method gadget-state-setter;
||#

(defmethod (setf gadget-state) ((state <text-gadget-state>) (gadget <text-gadget>))
  (setf (gadget-text gadget) (%state-text state))
  state)



#||

/// Text gadget classes

// A single-line text editing field
// The callbacks are as follows:
//  - value-changing callback during "casual" typing
//  - value-changed callback when the change is "committed"
//  - activate callback when some sort of activation gesture is seen
// The "commit" and "activation" gestures are defined by the back-end
define open abstract class <text-field>
    (<bordered-gadget-mixin>,
     <action-gadget-mixin>,
     <changing-value-gadget-mixin>,
     <value-gadget-mixin>,
     <text-gadget>,
     <basic-gadget>)
  // We maintain the text buffer separately from 'gadget-text' in case
  // the internal representation is different from the external represenation,
  // as is the case with Windows multi-line text editors
  slot gadget-text-buffer :: <string> = "",
    init-keyword: text:;
  constant slot gadget-value-type :: <type> = <string>,
    init-keyword: value-type:;
  sealed slot text-field-maximum-size :: false-or(<integer>) = #f,
    init-keyword: maximum-size:;
end class <text-field>;
||#

(defclass <text-field>
    (<bordered-gadget-mixin>
     <action-gadget-mixin>
     <changing-value-gadget-mixin>
     <value-gadget-mixin>
     <text-gadget>
     <basic-gadget>)
  ;; We maintain the text buffer separately from 'gadget-text' in case
  ;; the internal representation is different from the external representation,
  ;; as is the case with Windows multi-line text editors
  ((gadget-text-buffer :type string :initarg :text :initform "" :accessor gadget-text-buffer)
   (gadget-value-type :type class :initarg :value-type :initform (find-class 'string) :reader gadget-value-type)
   (text-field-maximum-size :type (or null integer) :initarg :maximum-size :initform nil
			    :accessor text-field-maximum-size))
  (:documentation
"
The class of single line text fields.

The :x-alignment initarg is used to align the text in the text field.

The :case initarg lets you specify which case is used to display the
text in the text field. You can specify either upper or lower
case. The default is to display letters of either case.

If :auto-scroll? is true, then text scrolls off to the left of the
text field if more text is typed than can be displayed in the text
field itself.

Internally, this class maps into the single-line edit control Windows
control.

Example:

To make a text field with a fixed width:

    (make-pane '<text-field> :width 200 :fixed-width? t)

The following example creates a text field which, after pressing
Return, invokes a callback that displays the gadget value in a dialog
box.

    (defparameter *text*
      (contain (make-pane '<text-field>
                          :value-changed-callback
                          #'(lambda (gadget)
                              (notify-user
                                (format nil \"Changed to ~a\"
                                        (gadget-value gadget))
                                :owner gadget)))))
")
  (:metaclass <abstract-metaclass>))


#||
define method initialize
    (gadget :: <text-field>,
     #key x-alignment = #"left", case: text-case = #f, auto-scroll? = #f)
  next-method();
  let xa = select (x-alignment)
	     #"left"  => %x_alignment_left;
	     #"right" => %x_alignment_right;
	     #"center", #"centre" => %x_alignment_center;
	   end;
  let c = select (text-case)
	    #f       => %text_case_false;
	    #"lower" => %text_case_lower;
	    #"upper" => %text_case_upper;
	  end;
  let scroll = if (auto-scroll?) %auto_scroll else 0 end;
  gadget-flags(gadget)
    := logior(logand(gadget-flags(gadget), lognot(%x_alignment_mask)),
	      xa + c + scroll)
end method initialize;
||#

(defmethod initialize-instance :after ((gadget <text-field>)
				       &key (x-alignment :left) ((:case text-case) nil) (auto-scroll? nil)
				       &allow-other-keys)
  (let ((xa (ecase x-alignment
	      (:left   %x_alignment_left)
	      (:right  %x_alignment_right)
	      ((:center :centre) %x_alignment_center)))
	(c (ecase text-case
	     ((nil)   %text_case_false)
	     (:lower  %text_case_lower)
	     (:upper  %text_case_upper)))
	(scroll (if auto-scroll? %auto_scroll 0)))
    (setf (gadget-flags gadget)
	  (logior (logand (gadget-flags gadget) (lognot %x_alignment_mask))
		  (+ xa c scroll)))))


#||
define constant $text-field-cases :: <simple-object-vector>
    = #[#f, #"lower", #"upper"];
||#

(defvar *text-field-cases* #(nil :lower :upper))


#||
define sealed inline method text-field-case
    (gadget :: <text-field>) => (text-case)
  let index = ash(logand(gadget-flags(gadget), %text_case_mask),
		  -%text_case_shift);
  $text-field-cases[index]
end method text-field-case;
||#

(defmethod text-field-case ((gadget <text-field>))
  (let ((index (ash (logand (gadget-flags gadget) %text_case_mask)
		    (- %text_case_shift))))
    (aref *text-field-cases* index)))


#||
define sealed inline method text-field-auto-scroll?
    (gadget :: <text-field>) => (auto-scroll? :: <boolean>)
  logand(gadget-flags(gadget), %auto_scroll) = %auto_scroll
end method text-field-auto-scroll?;
||#

(defmethod text-field-auto-scroll? ((gadget <text-field>))
  (= (logand (gadget-flags gadget) %auto_scroll) %auto_scroll))


#||
define method viewport-fencepost? (sheet :: <text-field>) => (true? :: <boolean>)
  #t
end method viewport-fencepost?;
||#

(defmethod viewport-fencepost? ((sheet <text-field>))
  t)


#||
// Back-ends where 'gadget-text' and 'gadget-text-buffer' use different
// representations should specialize 'gadget-text' and 'gadget-text-setter'
define method gadget-text
    (gadget :: <text-gadget>) => (text :: <string>)
  gadget-text-buffer(gadget)
end method gadget-text;
||#

(defmethod gadget-text ((gadget <text-gadget>))
  (gadget-text-buffer gadget))


#||
define method gadget-text-setter
    (text :: <string>, gadget :: <text-gadget>, #key do-callback? = #f)
 => (text :: <string>)
  gadget-text-buffer(gadget) := text;
  when (do-callback?)
    execute-value-changed-callback(gadget, gadget-client(gadget), gadget-id(gadget))
  end;
  note-gadget-text-changed(gadget);
  note-gadget-value-changed(gadget);
  text
end method gadget-text-setter;
||#

(defmethod (setf gadget-text) ((text string) (gadget <text-gadget>) &key (do-callback? nil))
  (setf (gadget-text-buffer gadget) text)
  (when do-callback?
    (execute-value-changed-callback gadget (gadget-client gadget) (gadget-id gadget)))
  (note-gadget-text-changed gadget)
  (note-gadget-value-changed gadget)
  text)


#||
define method note-gadget-text-changed
    (gadget :: <text-gadget>) => ()
  #f
end method note-gadget-text-changed;
||#

(defmethod note-gadget-text-changed ((gadget <text-gadget>))
  nil)


#||
define method gadget-value
    (gadget :: <text-gadget>) => (value)
  gadget-text-parser(gadget-value-type(gadget), gadget-text(gadget))
end method gadget-value;
||#

(defmethod gadget-value ((gadget <text-gadget>))
  (gadget-text-parser (gadget-value-type gadget) (gadget-text gadget)))


#||
define method do-gadget-value-setter
    (gadget :: <text-gadget>, value) => ()
  let text = gadget-value-printer(gadget-value-type(gadget), value);
  unless (text = gadget-text(gadget))
    gadget-text(gadget) := text
  end
end method do-gadget-value-setter;
||#

(defmethod do-gadget-value-setter ((gadget <text-gadget>) value)
  (let ((text (gadget-value-printer (gadget-value-type gadget) value)))
    (unless (equal? text (gadget-text gadget))
      (setf (gadget-text gadget) text))))


#||
/// 'accept' and 'present', ha ha ha

define method gadget-text-parser
    (type :: subclass(<string>), text :: <string>) => (value :: <string>)
  text
end method gadget-text-parser;
||#

(defmethod gadget-text-parser ((type (eql (find-class 'string))) (text string))
  text)


#||
define method gadget-value-printer
    (type :: subclass(<string>), value :: <string>) => (text :: <string>)
  value
end method gadget-value-printer;
||#

(defmethod gadget-value-printer ((type (eql (find-class 'string))) (value string))
  value)


#||
define method gadget-text-parser
    (type :: subclass(<symbol>), text :: <string>) => (value :: <symbol>)
  as(<symbol>, text)
end method gadget-text-parser;
||#

(defmethod gadget-text-parser ((type (eql (find-class 'symbol))) (text string))
  (make-symbol text))


#||
define method gadget-value-printer
    (type :: subclass(<symbol>), value :: <symbol>) => (text :: <string>)
  as(<string>, value)
end method gadget-value-printer;
||#

(defmethod gadget-value-printer ((type (eql (find-class 'symbol))) (value symbol))
  (symbol-name value))


#||
define method gadget-text-parser
    (type :: subclass(<integer>), text :: <string>) => (value :: false-or(<integer>))
  block ()
    let (value, next) = string-to-integer(text);
    next = size(text) & value
  exception (<error>)
    #f
  end
end method gadget-text-parser;
||#

;; XXX: These could be extended to accept real, compex, etc.
(defmethod gadget-text-parser ((type (eql (find-class 'integer))) (text string))
  ;; FIXME: PUT IN A HANDLER CASE?
  (multiple-value-bind (value next)
      (parse-integer text :junk-allowed t)
    (and (equal? next (length text))
	 value)))


#||
define method gadget-value-printer
    (type :: subclass(<integer>), value :: false-or(<integer>)) => (text :: <string>)
  if (value)
    integer-to-string(value)
  else
    ""
  end
end method gadget-value-printer;
||#

(defmethod gadget-value-printer ((type (eql (find-class 'integer))) (value integer))
  (format nil "~d" value))

(defmethod gadget-value-printer ((type (eql (find-class 'integer))) (value null))
  "")


#||
/*
define method gadget-text-parser
    (type :: subclass(<float>), text :: <string>) => (value :: false-or(<float>))
  block ()
    let (value, next) = string-to-float(text);
    next = size(text) & value
  exception (<error>)
    #f
  end
end method gadget-text-parser;

define method gadget-value-printer
    (type :: subclass(<float>), value :: false-or(<float>)) => (text :: <string>)
  if (value)
    float-to-string(value)
  else
    ""
  end
end method gadget-value-printer;
*/
||#


#||

/// Password fields

define open abstract class <password-field> (<text-field>)
end class <password-field>;
||#

(defclass <password-field> (<text-field>)
  ()
  (:documentation
"
The class of text fields that do not echo typed text. This class of
gadgets are very similar in appearance to the <TEXT-FIELD> gadget,
except that any text typed by the user is hidden in some way, rather
than being echoed to the screen in the normal way.

Internally, this class maps into the Windows single-line edit control
with ES-PASSWORD style.

Example:

    (defparameter *pass* (contain (make-pane '<password-field>)))
")
  (:metaclass <abstract-metaclass>))



#||

/// Multi-line text editors

define protocol <<text-editor-protocol>> (<<text-field-protocol>>)
  getter text-field-word-wrap?
    (gadget :: <text-gadget>) => (word-wrap? :: <boolean>);
  // Hacking lines
  function current-line
    (gadget :: <text-gadget>) => (line :: false-or(<integer>));
  function line-length
    (gadget :: <text-gadget>, line :: <integer>) => (length :: false-or(<integer>));
  function get-line
    (gadget :: <text-gadget>, line :: <integer>) => (line :: false-or(<string>));
  // Mapping indices to lines, and vice-versa
  function index-line
    (gadget :: <text-gadget>, index :: <integer>) => (line :: false-or(<integer>));
  function line-index
    (gadget :: <text-gadget>, line :: <integer>) => (index :: false-or(<integer>));
  // Protection
  function text-range-protected?
    (gadget :: <text-gadget>, range :: <text-range>) => (protected? :: <boolean>);
  function text-range-protected?-setter
    (protected? :: <boolean>, gadget :: <text-gadget>, range :: <text-range>)
 => (protected? :: <boolean>);
  // Searching
  function find-text (gadget :: <text-gadget>, string :: <string>)
 => (index :: false-or(<integer>));
end protocol <<text-editor-protocol>>;
||#
#||
(define-protocol <<text-editor-protocol>> (<<text-field-protocol>>)
  (:getter text-field-word-wrap? (gadget) (:documentation " Defined by: <<TEXT-EDITOR-PROTOCOL>> "))
  ;; Hacking lines
  (:function current-line (gadget) (:documentation " Defined by: <<TEXT-EDITOR-PROTOCOL>> "))
  (:function line-length (gadget line) (:documentation " Defined by: <<TEXT-EDITOR-PROTOCOL>> "))
  (:function get-line (gadget line) (:documentation " Defined by: <<TEXT-EDITOR-PROTOCOL>> "))
  ;; Mapping indices to lines, and vice-versa
  (:function index-line (gadget index) (:documentation " Defined by: <<TEXT-EDITOR-PROTOCOL>> "))
  (:function line-index (gadget line) (:documentation " Defined by: <<TEXT-EDITOR-PROTOCOL>> "))
  ;; Protection
  (:function text-range-protected? (gadget range) (:documentation " Defined by: <<TEXT-EDITOR-PROTOCOL>> "))
  (:function (setf text-range-protected?) (protected? gadget range) (:documentation " Defined by: <<TEXT-EDITOR-PROTOCOL>> "))
  ;; Searching
  (:function find-text (gadget string) (:documentation " Defined by: <<TEXT-EDITOR-PROTOCOL>> ")))
||#


#||
// A multi-line text editing field
define open abstract class <text-editor>
    (<scrolling-gadget-mixin>, <text-field>)
  sealed constant slot gadget-lines   :: false-or(<integer>) = #f,	// i.e., bottomless
    init-keyword: lines:;
  sealed constant slot gadget-columns :: false-or(<integer>) = #f,
    init-keyword: columns:;
  sealed slot text-field-word-wrap? :: <boolean> = #t,
    init-keyword: word-wrap?:;
end class <text-editor>;
||#

(defclass <text-editor>
    (<scrolling-gadget-mixin>
     <text-field>)
  ((gadget-lines :type (or null integer) :initarg :lines :initform nil :reader gadget-lines)   ;; nil == bottomless
   (gadget-columns :type (or null integer) :initarg :columns :initform nil :reader gadget-columns)  ;; "sideless"?
   (text-field-word-wrap? :type boolean :initarg :word-wrap? :initform t :accessor text-field-word-wrap?))
  (:documentation
"
The class of multiple line text editors.

The :columns and :lines initargs specify the number of columns and
lines of characters visible in the text editor, respectively.

The :scroll-bars initarg specifies whether the text editor has scroll
bars or not.

Internally, this class maps into the multi-line edit control Windows
control.

Examples:

To constrain the number of lines and columns when an editor is first
displayed:

    (defparameter *editor*
                  (contain (make-pane '<text-editor>
                                      :lines 20 :columns 80)))

To make a text editor that is fixed at 10 lines high:

    (make-pane '<text-editor> :lines 10 :fixed-height? t)
")
  (:metaclass <abstract-metaclass>))



#||

/// "Rich" text editors

define protocol <<rich-text-editor-protocol>> (<<text-editor-protocol>>)
  getter character-format
    (gadget :: <text-gadget>) => (sd :: false-or(<style-descriptor>));
  // Set the default character format, or set the format for the indicated range
  setter character-format-setter
    (sd :: false-or(<style-descriptor>), gadget :: <text-gadget>,
     #key range :: false-or(<text-range>))
 => (sd :: false-or(<style-descriptor>));
  getter paragraph-format
    (gadget :: <text-gadget>, #key range) => (paragraph-format);
  setter paragraph-format-setter
    (paragraph-format, gadget :: <text-gadget>, #key range) => (paragraph-format);
end protocol <<rich-text-editor-protocol>>;
||#
#||
(define-protocol <<rich-text-editor-protocol>> (<<text-editor-protocol>>)
  (:getter character-format (gadget) (:documentation " Defined by: <<RICH-TEXT-EDITOR-PROTOCOL>> "))
  ;; Set the default character format, or set the format for the
  ;; indicated range
  (:setter (setf character-format) (sd gadget &key range)
	   (:documentation
"
Set the default character format, or set the format for the
indicated range.
Defined by: <<RICH-TEXT-EDITOR-PROTOCOL>>
"))
  (:getter paragraph-format (gadget &key range) (:documentation " Defined by: <<RICH-TEXT-EDITOR-PROTOCOL>> "))
  (:setter (setf paragraph-format) (paragraph-format gadget &key range) (:documentation " Defined by: <<RICH-TEXT-EDITOR-PROTOCOL>> ")))
||#

#||
define open abstract class <rich-text-editor> (<text-editor>)
  sealed slot %paragraph-format = #f,
    init-keyword: paragraph-format:;
  sealed slot %word-break-policy = #f,
    init-keyword: word-break-policy:;
  sealed slot %line-break-policy = #f,
    init-keyword: line-break-policy:
end class <rich-text-editor>;
||#

(defclass <rich-text-editor> (<text-editor>)
  ((%paragraph-format :initarg :paragraph-format :initform nil :accessor %paragraph-format)
   (%word-break-policy :initarg :word-break-policy :initform nil :accessor %word-break-policy)
   (%line-break-policy :initarg :line-break-policy :initform nil :accessor %line-break-policy))
  (:metaclass <abstract-metaclass>))


#||
define method paragraph-format
    (gadget :: <rich-text-editor>, #key range) => (paragraph-format)
  //---*** What to do about the range?
  gadget.%paragraph-format
end method paragraph-format;
||#

(defmethod paragraph-format ((gadget <rich-text-editor>) &key range)
  ;;---*** What to do about the range?
  (declare (ignorable range))
  (%paragraph-format gadget))


#||
define method paragraph-format-setter
    (paragraph-format, gadget :: <rich-text-editor>, #key range) => (paragraph-format)
  //---*** What to do about the range?
  gadget.%paragraph-format := paragraph-format
end method paragraph-format-setter;
||#

(defmethod (setf paragraph-format) (paragraph-format (gadget <rich-text-editor>) &key range)
  ;;---*** What to do about the range?
  (declare (ignorable range))
  (setf (%paragraph-format gadget) paragraph-format))



#||

/// Text changed events

define sealed class <text-changing-gadget-event> (<gadget-event>)
  sealed constant slot event-text :: <string>,
    required-init-keyword: text:;
end class <text-changing-gadget-event>;

define sealed domain make (singleton(<text-changing-gadget-event>));
define sealed domain initialize (<text-changing-gadget-event>);
||#

(defclass <text-changing-gadget-event>
    (<gadget-event>)
  ((event-text :type string :initarg :text :initform (required-slot ":text" "<text-changing-gadget-event>") :reader event-text)))


#||
define method handle-event
    (gadget :: <text-gadget>, event :: <text-changing-gadget-event>) => ()
  gadget-text-buffer(gadget) := event-text(event);
  execute-value-changing-callback(gadget, gadget-client(gadget), gadget-id(gadget))
end method handle-event;
||#

(defmethod handle-event ((gadget <text-gadget>) (event <text-changing-gadget-event>))
  (setf (gadget-text-buffer gadget) (event-text event))
  (execute-value-changing-callback gadget
				   (gadget-client gadget)
				   (gadget-id gadget)))


#||
define function distribute-text-changing-callback
    (gadget :: <text-gadget>, text :: <string>) => ()
  distribute-event(port(gadget),
		   make(<text-changing-gadget-event>,
			gadget: gadget,
			text: text))
end function distribute-text-changing-callback;
||#

(defun distribute-text-changing-callback (gadget text)
  (declare (type <text-gadget> gadget)
	   (type string text))
  (distribute-event (port gadget)
		    (make-instance '<text-changing-gadget-event>
				   :gadget gadget
				   :text text)))


#||
define sealed class <text-changed-gadget-event> (<text-changing-gadget-event>)
end class <text-changed-gadget-event>;

define sealed domain make (singleton(<text-changed-gadget-event>));
define sealed domain initialize (<text-changed-gadget-event>);
||#


(defclass <text-changed-gadget-event>
    (<text-changing-gadget-event>)
  ())


#||
define method handle-event
    (gadget :: <text-gadget>, event :: <text-changed-gadget-event>) => ()
  gadget-text-buffer(gadget) := event-text(event);
  execute-value-changed-callback(gadget, gadget-client(gadget), gadget-id(gadget))
end method handle-event;
||#

(defmethod handle-event ((gadget <text-gadget>) (event <text-changed-gadget-event>))
  (setf (gadget-text-buffer gadget) (event-text event))
  (execute-value-changed-callback gadget
				  (gadget-client gadget)
				  (gadget-id gadget)))


#||
define function distribute-text-changed-callback
    (gadget :: <text-gadget>, text :: <string>) => ()
  distribute-event(port(gadget),
		   make(<text-changed-gadget-event>,
			gadget: gadget,
			text: text))
end function distribute-text-changed-callback;
||#

(defun distribute-text-changed-callback (gadget text)
  (check-type gadget <text-gadget>)
  (check-type text string)
  (distribute-event (port gadget)
		    (make-instance '<text-changed-gadget-event>
				   :gadget gadget
				   :text text)))


