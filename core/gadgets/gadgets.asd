;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

(asdf:defsystem :gadgets
    :description "DUIM gadgets"
    :long-description "Provides all the gadgets available for use in the DUIM library. Gadgets are the sheet objects that make up a user interface, and the DUIM library supplies all the gadgets you will need in your applications."
    :author      "Scott McKay, Andy Armstrong (Lisp port: Duncan Rose <duncan@robotcat.demon.co.uk>)"
    :version     (:read-file-form "version.sexp")
    :licence     "BSD-2-Clause"
    :depends-on (#:dylan-commands
		 #:duim-utilities
		 #:geometry
		 #:dcs
		 #:sheets
		 #:graphics
		 #:layouts)
    :serial t
    :components
    ((:file "package")
     (:file "protocols")
     (:file "gadget-mixins")
     (:file "gadgets")
     (:file "text-gadgets")
     (:file "collection-gadgets")
     (:file "menus")
     (:file "borders")
     (:file "viewports")
     (:file "scroll-bars")
     (:file "scrollers")
     (:file "splitters")
     (:file "tab-controls")
     (:file "list-controls")
     (:file "table-controls")
     (:file "tree-controls")
     (:file "graph-controls")
     (:file "button-box-panes")
     (:file "menu-panes")
     (:file "active-labels")))

