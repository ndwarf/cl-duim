;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GADGETS-INTERNALS -*-
(in-package #:duim-gadgets-internals)

#||
/// Viewports, for scrolling

// The single child of the viewport is the pane being scrolled.
// Usually mirrored at the back-end
//--- This should be split into an abstract and concrete class...
define open abstract class <viewport> 
    (<basic-gadget>,
     <standard-input-mixin>,		// so we can pass on events to the child...
     <client-overridability-mixin>,
     <scrolling-sheet-mixin>,
     <single-child-mixin>)
  // This describes the region that we are displaying
  sealed slot viewport-region :: false-or(<region>) = #f;
end class <viewport>;
||#

(defgeneric viewport-region (viewport)
  (:documentation
"
Returns the region for _viewport_.

Example:

To return the region for a viewport *viewer*:

    (viewport-region *viewer*)
"))

(defclass <viewport>
    (<basic-gadget>
     <standard-input-mixin>         ; so we can pass on events to the child...
     <client-overridability-mixin>
     <scrolling-sheet-mixin>
     <single-child-mixin>)
  ;; This describes the region that we are displaying
  ((viewport-region :type (or null <region>) :initform nil :accessor viewport-region))
  (:documentation
"
The class of viewports. A viewport is a sheet \"through\" which other
sheets are visible; they are used to implement a clipping region for
scrolling.

The :horizontal-scroll-bar and :vertical-scroll-bar initargs specify
whether the viewport has horizontal and vertical scroll bars,
respectively.

In most applications, you should not need to use a viewport
yourself. However, there are some circumstances in which defining your
own viewports is invaluable. In particular, if you need to use a
single scroll bar to scroll more than one window at the same time, you
should define each window as a viewport, and use the same scroll bar
to scroll each window. There are two situations where this behaviour
is quite common:

  + In applications which have vertical or horizontal rulers around a
    document window, such as a drawing application. In this case, the
    rulers must scroll with the drawing itself.

  + In applications such as spreadsheets, where row and column
    headings need to scroll with the document. Note that you may also
    choose to implement this kind of functionality using a table
    control.
")
  (:metaclass <abstract-metaclass>))


#||
define protocol-predicate viewport;
||#

(define-protocol-predicate viewport)
#||
    (:documentation
"
Returns true if _object_ is a viewport.

Example:

To test whether the gadget *gadget* is a viewport:

    (viewport? *gadget*)
"))
||#
#||
define protocol <<viewport-protocol>> ()
  function sheet-viewport
    (sheet :: type-union(<abstract-sheet>, <abstract-gadget>))
 => (viewport :: false-or(<viewport>));
  function sheet-viewport-region
    (sheet :: type-union(<abstract-sheet>, <abstract-gadget>))
 => (region :: <region>);
  function scroll-viewport
    (viewport :: <viewport>, x :: <integer>, y :: <integer>,
     #key update-scroll-bars?) => ();
  function scroll-viewport-child
    (viewport :: <viewport>, sheet :: <abstract-sheet>, x :: <integer>, y :: <integer>,
     #key update-scroll-bars?) => ();
  function shift-visible-region
    (sheet :: <abstract-sheet>,
     oleft :: <integer>, otop :: <integer>, oright :: <integer>, obottom :: <integer>,
     nleft :: <integer>, ntop :: <integer>, nright :: <integer>, nbottom :: <integer>) => ();
  // Users can define methods on the next two to do things such as
  // linking two scrollable panes together
  function note-viewport-position-changed
    (frame, sheet :: <abstract-sheet>, x, y) => ();
  function note-viewport-region-changed
    (sheet :: <abstract-sheet>, viewport :: <viewport>) => ();
  // These two ask of a sheet, are you capable of scrolling in this direction?
  // The default methods just query the containing viewport as to whether there
  // is a scroll bar in that direction
  function sheet-scrolls-horizontally?
    (sheet :: <abstract-sheet>) => (true? :: <boolean>);
  function sheet-scrolls-vertically?
    (sheet :: <abstract-sheet>) => (true? :: <boolean>);
end protocol <<viewport-protocol>>;
||#
#||
(define-protocol <<viewport-protocol>> ()
  (:function sheet-viewport (sheet) (:documentation " Defined by: <<VIEWPORT-PROTOCOL>> "))
  (:function sheet-viewport-region (sheet) (:documentation " Defined by: <<VIEWPORT-PROTOCOL>> "))
  (:function scroll-viewport (viewport x y &key update-scroll-bars?) (:documentation " Defined by: <<VIEWPORT-PROTOCOL>> "))
  (:function scroll-viewport-child (viewport sheet x y
					     &key update-scroll-bars?) (:documentation " Defined by: <<VIEWPORT-PROTOCOL>> "))
  (:function shift-visible-region (sheet oleft otop oright obottom
					 nleft ntop nright nbottom) (:documentation " Defined by: <<VIEWPORT-PROTOCOL>> "))
  ;; Users can define methods on the next two to do things such as
  ;; linking two scrollable panes together
  (:function note-viewport-position-changed (frame sheet x y) (:documentation " Defined by: <<VIEWPORT-PROTOCOL>> "))
  (:function note-viewport-region-changed (sheet viewport) (:documentation " Defined by: <<VIEWPORT-PROTOCOL>> "))
  ;; These two ask of a sheet, are you capable of scrolling in this direction?
  ;; The default methods just query the containing viewport as to whether there
  ;; is a scroll bar in that direction
  (:function sheet-scrolls-horizontally? (sheet) (:documentation " Defined by: <<VIEWPORT-PROTOCOL>> "))
  (:function sheet-scrolls-vertically? (sheet) (:documentation " Defined by: <<VIEWPORT-PROTOCOL>> ")))
||#


#||
define method initialize 
    (viewport :: <viewport>, #key) => ()
  next-method();
  let (width, height) = box-size(viewport);
  viewport-region(viewport) := make-bounding-box(0, 0, width, height)
end method initialize;
||#

(defmethod initialize-instance :after ((viewport <viewport>) &key &allow-other-keys)
  (multiple-value-bind (width height)
      (box-size viewport)
    (setf (viewport-region viewport) (make-bounding-box 0 0 width height))))


#||
// This is where we pass on events to the right place -- the child
//---*** Maybe this should only happen when the child is unmirrored.
//---*** Also, couldn't it behave the same as <drawing-pane>?
define method handle-event
    (viewport :: <viewport>, event :: <event>) => ()
  let child = sheet-child(viewport);
  when (child)
    handle-event(event-handler(child), event)
  end
end method handle-event;
||#

(defmethod handle-event ((viewport <viewport>) (event <event>))
  (let ((child (sheet-child viewport)))
    (when child
      (handle-event (event-handler child) event))))


#||
// The input focus of a viewport gets redirected to the sheet the
// viewport is looking at
define method sheet-input-focus (sheet :: <viewport>) => (focus :: <sheet>)
  sheet-child(sheet)
end method sheet-input-focus;
||#

(defmethod sheet-input-focus ((sheet <viewport>))
  (sheet-child sheet))


#||
// The idea here is that we allow the viewport to be infinitely
// stretchable and shrinkable.  If no size is specified, we make the 
// viewport large enough to contain the entire child.
// We also allow scrolling in only one direction, in which case the
// other direction just picks up everything from the child.
define method do-compose-space
    (viewport :: <viewport>, #key width, height)
 => (space-req :: <space-requirement>)
  let child = sheet-child(viewport);
  let horizontal? = sheet-scrolls-horizontally?(child);
  let vertical?   = sheet-scrolls-vertically?(child);
  let vertical-only?   = vertical?   & ~horizontal?;
  let horizontal-only? = horizontal? & ~vertical?;
  let default-space-req
    = default-space-requirement(viewport, width: width, height: height);
  let space-req
    = if (child)
	compose-space(child, width: width, height: height);
      else
	default-space-req;
      end;
  let ( w,  w-,  w+,  h,  h-,  h+)
    = space-requirement-components(child | viewport, space-req);
  let (dw, dw-, dw+, dh, dh-, dh+)
    = space-requirement-components(viewport, default-space-req);
  let min-width  :: <integer> = if (vertical-only?)   w- else dw- end;
  let max-width  :: <integer> = if (vertical-only?)   w+ else dw+ end;
  let min-height :: <integer> = if (horizontal-only?) h- else dh- end;
  let max-height :: <integer> = if (horizontal-only?) h+ else dh+ end;
  //--- I'm not sure about this.  This says if no size is specified then
  //--- we make it the size of the child... but the child could be huge
  //--- so the initial window comes up huge (bounded only by the size of
  //--- the screen).  An alternative might be to make it some reasonable
  //--- minimum by default (100 pixels), but this could be just as wrong.
  let best-width  :: <integer>
    = constrain-size(width  | if (horizontal?) w else dw end, min-width, max-width);
  let best-height :: <integer>
    = constrain-size(height | if (vertical?)   h else dh end, min-height, max-height);
  make(<space-requirement>,
       width:  best-width,  min-width:  min-width,  max-width:  max-width,
       height: best-height, min-height: min-height, max-height: max-height)
end method do-compose-space;
||#

(defmethod do-compose-space ((viewport <viewport>) &key width height)
  (let* ((child (sheet-child viewport))
	 (horizontal? (and child (sheet-scrolls-horizontally? child)))
	 (vertical?   (and child (sheet-scrolls-vertically? child)))
	 (vertical-only?   (and vertical? (not horizontal?)))
	 (horizontal-only? (and horizontal? (not vertical?)))
	 (default-space-req (default-space-requirement viewport :width width :height height))
	 (space-req (if child
			(compose-space child :width width :height height)
			default-space-req)))
    (multiple-value-bind (w  w-  w+  h  h-  h+)
	(space-requirement-components (or child viewport) space-req)
      (multiple-value-bind (dw dw- dw+ dh dh- dh+)
	  (space-requirement-components viewport default-space-req)
	(declare (ignore dw dh))
	(let* ((min-width (if vertical-only? w- dw-))
	       (max-width (if vertical-only? w+ dw+))
	       (min-height (if horizontal-only? h- dh-))
	       (max-height (if horizontal-only? h+ dh+))
	       ;;--- I'm not sure about this.  This says if no size is
	       ;;--- specified then we make it the size of the child... but
	       ;;--- the child could be huge so the initial window comes up
	       ;;--- huge (bounded only by the size of the screen).  An
	       ;;--- alternative might be to make it some reasonable
	       ;;--- minimum by default (100 pixels), but this could be just
	       ;;--- as wrong.
	       (best-width (constrain-size (or width w) min-width max-width))
	       (best-height (constrain-size (or height h) min-height max-height)))
	  (make-space-requirement :width  best-width
				  :min-width  min-width
				  :max-width  max-width
				  :height best-height
				  :min-height min-height
				  :max-height max-height))))))


#||
// Yes, we really do mean to put this wrapper on 'allocate-space', not
// on 'DO-allocate-space'.  The idea is that subclasses of <viewport> might
// want their own 'do-allocate-space' method, but the stuff here should
// be done no matter what.
define method allocate-space
    (viewport :: <viewport>, width :: <integer>, height :: <integer>) => ()
  next-method();
  // Now reposition the viewport
  let contents = sheet-child(viewport);		// i.e., the scrollable contents
  when (contents)
    let (owidth, oheight) = box-size(viewport-region(viewport));
    viewport-region(viewport)			// might cons a new region...
      := set-box-size(viewport-region(viewport), width, height);
    // If previously it was too small to display the entire contents
    // but now it is large enough, scroll the window
    //--- Rethink this sometime...
    let (ox, oy) = box-position(viewport-region(viewport));
    let (cleft, ctop, cright, cbottom) = box-edges(contents);
    let cw = cright - cleft;
    let ch = cbottom - ctop;
    let x = ox;
    let y = oy;
    when (owidth < cw & cw <= width)
      x := cleft
    end;
    when (oheight < ch & ch <= height)
      y := ctop
    end;
    if (x ~= ox | y ~= oy)
      scroll-viewport(viewport, x, y)
    else
      update-scroll-bars(viewport);
      note-viewport-region-changed(sheet-child(viewport), viewport)
    end
  end
end method allocate-space;
||#

;; Yes, we really do mean to put this wrapper on 'allocate-space', not
;; on 'DO-allocate-space'. The idea is that subclasses of <viewport> might
;; want their own 'do-allocate-space' method, but the stuff here should
;; be done no matter what.
(defmethod allocate-space ((viewport <viewport>) (width integer) (height integer))
  (call-next-method)
  ;; Now reposition the viewport
  (let ((contents (sheet-child viewport)))  ;; i.e., the scrollable contents
    (when contents
      (multiple-value-bind (owidth oheight)
	  (box-size (viewport-region viewport))
	(setf (viewport-region viewport)		;; might cons a new region...
	      (set-box-size (viewport-region viewport) width height))
	;; If previously it was too small to display the entire contents
	;; but now it is large enough, scroll the window
	;;--- Rethink this sometime...
	(multiple-value-bind (ox oy)
	    (box-position (viewport-region viewport))
	  (multiple-value-bind (cleft ctop cright cbottom)
	      (box-edges contents)
	    (let ((cw (- cright cleft))
		  (ch (- cbottom ctop))
		  (x ox)
		  (y oy))
	      (when (and (< owidth cw) (<= cw width))
		(setf x cleft))
	      (when (and (< oheight ch) (<= ch height))
		(setf y ctop))
	      (if (or (not (= x ox)) (not (= y oy)))
		  (scroll-viewport viewport x y)
		  ;; else
		  (progn
		    (update-scroll-bars viewport)
		    (note-viewport-region-changed (sheet-child viewport) viewport))))))))))


#||
define method do-allocate-space
    (viewport :: <viewport>, width :: <integer>, height :: <integer>) => ()
  // Resize the child if it is happy to take on the new size
  let child = sheet-child(viewport);
  let horizontal? = (sheet-horizontal-scroll-bar(viewport) ~== #f);
  let vertical?   = (sheet-vertical-scroll-bar(viewport) ~== #f);
  when (child)
    let (old-width, old-height) = box-size(child);
    let width  = if (horizontal?) max(width,  old-width)  else width  end;
    let height = if (vertical?)   max(height, old-height) else height end;
    let space-req = compose-space(child, width:  width, height: height);
    let (w, w-, w+, h, h-, h+) = space-requirement-components(child, space-req);
    ignore(w-, w+, h-, h+);
    let child-width  :: <integer> = w;
    let child-height :: <integer> = h;
    unless (child-width = old-width & child-height = old-height)
      set-sheet-size(child, child-width, child-height)
    end
  end
end method do-allocate-space;
||#

(defmethod do-allocate-space ((viewport <viewport>) (width integer) (height integer))
  ;; Resize the child if it is happy to take on the new size
  (let ((child       (sheet-child viewport))
	(horizontal? (sheet-horizontal-scroll-bar viewport))  ;; will be nil if not present and t otherwise
	(vertical?   (sheet-vertical-scroll-bar viewport)))   ;; ditto
    (when child
      (multiple-value-bind (old-width old-height)
	  (box-size child)
	(let* ((width  (if horizontal? (max width  old-width)  width))
	       (height (if vertical?   (max height old-height) height))
	       (space-req (compose-space child :width  width :height height)))
	  (multiple-value-bind (w w- w+ h h- h+)
	      (space-requirement-components child space-req)
	    (declare (ignore w- w+ h- h+))
	    (let ((child-width w)
		  (child-height h))
	      (unless (and (= child-width old-width) (= child-height old-height))
		(set-sheet-size child child-width child-height)))))))))



#||

/// Back end and user hooks

define method note-viewport-region-changed
    (sheet :: <sheet>, viewport :: <viewport>) => ()
  #f
end method note-viewport-region-changed;
||#

(defmethod note-viewport-region-changed ((sheet <sheet>) (viewport <viewport>))
  nil)


#||
// Note that the frame might be #f, which is why we don't specialize on it
define method note-viewport-position-changed
    (frame, sheet :: <sheet>, x, y) => ()
  ignore(frame, sheet, x, y);
  #f
end method note-viewport-position-changed;
||#

;; Note that the frame might be #f, which is why we don't specialize on it
(defmethod note-viewport-position-changed (frame (sheet <sheet>) x y)
  (declare (ignore frame sheet x y))
  nil)



#||

/// User-level control of scrolling

// Returns the viewport of the pane, if there is one
define method sheet-viewport
    (sheet :: <sheet>) => (viewport :: false-or(<viewport>))
  let parent = sheet-parent(sheet);
  when (parent)
    case
      viewport?(parent) => parent;
      viewport-fencepost?(parent) => #f;
      otherwise => sheet-viewport(parent)
    end
  end
end method sheet-viewport;
||#

(defmethod sheet-viewport ((sheet <sheet>))
  (let ((parent (sheet-parent sheet)))
    (when parent
      (cond ((viewport? parent) parent)          ;; parent is a viewport - return it
	    ((viewport-fencepost? parent) nil)   ;; parent is a 'viewport-fencepost'; pretend sheet is not in a viewport
	    (t (sheet-viewport parent))))))      ;; return the parent's sheet-viewport (ascend the sheet tree)


#||
// Given a sheet, return the region of it's viewport.  If the sheet is not
// being scrolled, the viewport region is effectively just the sheet region.
define method sheet-viewport-region
    (sheet :: <sheet>) => (region :: <region>)
  let viewport = sheet-viewport(sheet);
  (viewport & viewport-region(viewport))
  // Not a scrolling pane, so the sheet's region is the viewport region
  | sheet-region(sheet)
end method sheet-viewport-region;
||#

(defmethod sheet-viewport-region ((sheet <sheet>))
  (let ((viewport (sheet-viewport sheet)))
    (or (and viewport (viewport-region viewport))
	;; Not a scrolling pane, so the sheet's region is the viewport region
	(sheet-region sheet))))


#||
define method scroll-position
    (sheet :: <sheet>) => (x :: <integer>, y :: <integer>)
  box-position(sheet-viewport-region(sheet))
end method scroll-position;
||#

(defmethod scroll-position ((sheet <sheet>))
  (box-position (sheet-viewport-region sheet)))


#||
define method set-scroll-position
    (sheet :: <sheet>, x :: <integer>, y :: <integer>) => ()
  let viewport = sheet-viewport(sheet);
  when (viewport)
    scroll-viewport(viewport, x, y)
  end
end method set-scroll-position;
||#

(defmethod set-scroll-position ((sheet <sheet>) (x integer) (y integer))
  (let ((viewport (sheet-viewport sheet)))
    (when viewport
      (scroll-viewport viewport x y))))


#||
define method sheet-scrolls-horizontally?
    (sheet :: <sheet>) => (true? :: <boolean>)
  let viewport = sheet-viewport(sheet);
  viewport & sheet-horizontal-scroll-bar(viewport) ~== #f
end method sheet-scrolls-horizontally?;
||#

(defmethod sheet-scrolls-horizontally? ((sheet <sheet>))
  (let ((viewport (sheet-viewport sheet)))
    (and viewport
	 (sheet-horizontal-scroll-bar viewport))))


#||
define method sheet-scrolls-vertically?
    (sheet :: <sheet>) => (true? :: <boolean>)
  let viewport = sheet-viewport(sheet);
  viewport & sheet-vertical-scroll-bar(viewport) ~== #f
end method sheet-scrolls-vertically?;
||#

(defmethod sheet-scrolls-vertically? ((sheet <sheet>))
  (let ((viewport (sheet-viewport sheet)))
    (and viewport
	 (sheet-vertical-scroll-bar viewport))))


#||
// We need to handle this specially for viewports, because viewports don't
// change size when their kid changes size.  We need to (1) be sure the kid
// really changes size, and (2) update the scroll bars.
//--- Andy still doesn't buy this, and SWM doesn't know for sure
define method relayout-parent 
    (viewport :: <viewport>, #key width, height) => (did-layout? :: <boolean>)
  when (sheet-attached?(viewport))	// be forgiving
    let child = sheet-child(viewport);
    when (child)
      let space-req = compose-space(child, width: width, height: height);
      let (w, w-, w+, h, h-, h+) = space-requirement-components(child, space-req);
      ignore(w-, w+, h-, h+);
      let new-width  :: <integer> = w;
      let new-height :: <integer> = h;
      unless (sheet-layed-out-to-size?(child, new-width, new-height))
	set-sheet-size(child, new-width, new-height)
      end;
      update-scroll-bars(viewport);
      #t
    end
  end
end method relayout-parent;
||#

(defmethod relayout-parent ((viewport <viewport>) &key width height)
  (when (sheet-attached? viewport)	;; be forgiving
    (let ((child (sheet-child viewport)))
      (when child
	(let ((space-req (compose-space child :width width :height height)))
	  (multiple-value-bind (w w- w+ h h- h+)
	      (space-requirement-components child space-req)
	    (declare (ignore w- w+ h- h+))
	    (let ((new-width w)
		  (new-height h))
	      (unless (sheet-layed-out-to-size? child new-width new-height)
		(set-sheet-size child new-width new-height))
	      (update-scroll-bars viewport)
	      t)))))))


#||
/*
//--- This is closer to what we really want, but it doesn't manage to
//--- propagate geometry changes to the scroll bars themselves.  Why not?
define method relayout-parent 
    (viewport :: <viewport>, #key width, height) => (did-layout? :: <boolean>)
  ignore(width, height);
  when (sheet-attached?(viewport))	// be forgiving
    block ()
      next-method()
    cleanup
      update-scroll-bars(viewport)
    end
  end
end method relayout-parent;
*/
||#


#||

/// Scrolling protocol

define method sheet-scroll-range
    (viewport :: <viewport>)
 => (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>);
  box-edges(sheet-child(viewport))
end method sheet-scroll-range;
||#

(defmethod sheet-scroll-range ((viewport <viewport>))
  (box-edges (sheet-child viewport)))


#||
define method sheet-scroll-range
    (sheet :: <sheet>)
 => (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>);
  box-edges(sheet)
end method sheet-scroll-range;
||#

(defmethod sheet-scroll-range ((sheet <sheet>))
  (box-edges sheet))


#||
define method sheet-visible-range
    (viewport :: <viewport>)
 => (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>);
  box-edges(viewport-region(viewport))
end method sheet-visible-range;
||#

(defmethod sheet-visible-range ((viewport <viewport>))
  (box-edges (viewport-region viewport)))


#||
define method sheet-visible-range
    (sheet :: <sheet>)
 => (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>);
  box-edges(sheet)
end method sheet-visible-range;
||#

(defmethod sheet-visible-range ((sheet <sheet>))
  (box-edges sheet))


#||
define method set-sheet-visible-range
    (viewport :: <viewport>, 
     left :: <real>, top :: <real>, right :: <real>, bottom :: <real>) => ()
  ignore(right, bottom);
  let x :: <integer> = truncate(left);
  let y :: <integer> = truncate(top);
  scroll-viewport(viewport, x, y, update-scroll-bars?: #f)
end method set-sheet-visible-range;
||#

(defmethod set-sheet-visible-range ((viewport <viewport>)
				    (left real) (top real) (right real) (bottom real))
  (declare (ignore right bottom))
  (let ((x (truncate left))
	(y (truncate top)))
    (scroll-viewport viewport x y :update-scroll-bars? nil)))


#||
define method line-scroll-amount
    (viewport :: <viewport>, orientation :: <gadget-orientation>)
 => (amount :: <integer>)
  line-scroll-amount(sheet-child(viewport), orientation) | next-method()
end method line-scroll-amount;
||#

(defmethod line-scroll-amount ((viewport <viewport>) orientation)
  (check-type orientation <gadget-orientation>)
  (or (line-scroll-amount (sheet-child viewport) orientation)
      (call-next-method)))


#||
define method page-scroll-amount
    (viewport :: <viewport>, orientation :: <gadget-orientation>)
 => (amount :: <integer>)
  page-scroll-amount(sheet-child(viewport), orientation) | next-method()
end method page-scroll-amount;
||#

(defmethod page-scroll-amount ((viewport <viewport>) orientation)
  (check-type orientation <gadget-orientation>)
  (or (page-scroll-amount (sheet-child viewport) orientation)
      (call-next-method)))



#||

/// BITBLT

define method scroll-viewport
    (viewport :: <viewport>, x :: <integer>, y :: <integer>,
     #key update-scroll-bars? = #t) => ()
  let sheet = sheet-child(viewport);
  when (sheet)
    scroll-viewport-child(viewport, sheet, x, y,
			  update-scroll-bars?: update-scroll-bars?)
  end
end method scroll-viewport;
||#

(defmethod scroll-viewport ((viewport <viewport>) (x integer) (y integer)
			    &key (update-scroll-bars? t))
  (let ((sheet (sheet-child viewport)))
    (when sheet
      (scroll-viewport-child viewport sheet x y
			     :update-scroll-bars? update-scroll-bars?))))


#||
// For non-mirrored children, we have to do BITBLT and repaint by hand
define method scroll-viewport-child
    (viewport :: <viewport>, sheet :: <sheet>, x :: <integer>, y :: <integer>,
     #key update-scroll-bars? = #t) => ()
  let (left, top, right, bottom) = box-edges(sheet-viewport-region(sheet));
  // Optimize this case, since the rest of this code can be quite
  // expensive, especially on servers that require 'copy-area' to
  // be synchronous
  unless (x = left & y = top)
    //--- This could actually bash the sheet-transform
    sheet-transform(sheet) := make-translation-transform(-x, -y);
    viewport-region(viewport)		// might cons a new region...
      := set-box-position(viewport-region(viewport), x, y);
    // Must go after 'set-box-position'
    when (update-scroll-bars?)
      update-scroll-bars(viewport)
    end;
    // When we scroll, we've got to move all mirrored children
    update-all-mirror-positions(sheet);
    let (nleft, ntop, nright, nbottom) = box-edges(sheet-viewport-region(sheet));
    case
      // If some of the stuff that was previously on display is still on
      // display, BITBLT it into the proper place and redraw the rest
      ltrb-intersects-ltrb?(left, top, right, bottom,
			    nleft, ntop, nright, nbottom) =>
	// Move the old stuff to the new position
	shift-visible-region(sheet, left, top, right, bottom,
				    nleft, ntop, nright, nbottom);
	let rectangles
	  = ltrb-difference(nleft, ntop, nright, nbottom,
			    left, top, right, bottom);
	with-sheet-medium (medium = sheet)
	  for (region in rectangles)
	    with-clipping-region (medium, region)
	      clear-box*(medium, region);
	      repaint-sheet(sheet, region)
	    end
	  end
	end;
      // Otherwise, just redraw the whole visible viewport.
      // Adjust for the left and top margins by hand so 'clear-box'
      // doesn't erase the margin components.
      otherwise =>
	let region = viewport-region(viewport);
	with-sheet-medium (medium = sheet)
	  clear-box*(medium, region)
	end;
	repaint-sheet(sheet, region)
    end;
    let frame = sheet-frame(sheet);
    when ((left ~= x | top ~= y) & frame)
      note-viewport-position-changed(frame, sheet, x, y)
    end
  end
end method scroll-viewport-child;
||#

;; For non-mirrored children, we have to do BITBLT and repaint by hand
(defmethod scroll-viewport-child ((viewport <viewport>) (sheet <sheet>) (x integer) (y integer)
				  &key (update-scroll-bars? t))
  (multiple-value-bind (left top right bottom)
      (box-edges (sheet-viewport-region sheet))
    ;; Optimize this case, since the rest of this code can be quite
    ;; expensive, especially on servers that require 'copy-area' to
    ;; be synchronous
    (unless (and (= x left) (= y top))
      ;;--- This could actually bash the sheet-transform
      (setf (sheet-transform sheet) (make-translation-transform (- x) (- y)))
      (setf (viewport-region viewport)    ;; might cons a new region...
	    (set-box-position (viewport-region viewport) x y))
      ;; Must go after 'set-box-position'
      (when update-scroll-bars?
	(update-scroll-bars viewport))
      ;; When we scroll, we've got to move all mirrored children
      (update-all-mirror-positions sheet)
      (multiple-value-bind (nleft ntop nright nbottom)
	  (box-edges (sheet-viewport-region sheet))
	(cond
	 ;; If some of the stuff that was previously on display is still on
	 ;; display, BITBLT it into the proper place and redraw the rest
	 ((ltrb-intersects-ltrb? left top right bottom
				 nleft ntop nright nbottom)
	  ;; Move the old stuff to the new position
	  (shift-visible-region sheet left top right bottom
				nleft ntop nright nbottom)
	  (let ((rectangles (ltrb-difference nleft ntop nright nbottom
					     left top right bottom)))
	    (with-sheet-medium (medium = sheet)
	      (map nil #'(lambda (region)
			   (with-clipping-region (medium region)
			     (clear-box* medium region)
			     (repaint-sheet sheet region)))
		   rectangles))))
	 ;; Otherwise, just redraw the whole visible viewport.
	 ;; Adjust for the left and top margins by hand so 'clear-box'
	 ;; doesn't erase the margin components.
	 (t
	  (let ((region (viewport-region viewport)))
	    (with-sheet-medium (medium = sheet)
              (clear-box* medium region))
	    (repaint-sheet sheet region)))))
      (let ((frame (sheet-frame sheet)))
	(when (and (or (/= left x) (/= top y)) frame)
	  (note-viewport-position-changed frame sheet x y))))))


#||
// For mirrored children, the act of setting the sheet's transform is
// typically enough to trigger repainting, etc.  Back-ends are, of course,
// free to specialize this behavior further
define method scroll-viewport-child
    (viewport :: <viewport>, sheet :: <mirrored-sheet-mixin>, x :: <integer>, y :: <integer>,
     #key update-scroll-bars? = #t) => ()
  let (left, top, right, bottom) = box-edges(sheet-viewport-region(sheet));
  ignore(right, bottom);
  unless (x = left & y = top)
    sheet-transform(sheet) := make-translation-transform(-x, -y);
    viewport-region(viewport)
      := set-box-position(viewport-region(viewport), x, y);
    when (update-scroll-bars?)
      update-scroll-bars(viewport)
    end;
    update-all-mirror-positions(sheet);
    let frame = sheet-frame(sheet);
    when ((left ~= x | top ~= y) & frame)
      note-viewport-position-changed(frame, sheet, x, y)
    end
  end
end method scroll-viewport-child;
||#

(defmethod scroll-viewport-child ((viewport <viewport>) (sheet <mirrored-sheet-mixin>) (x integer) (y integer)
				  &key (update-scroll-bars? t))
  (multiple-value-bind (left top right bottom)
      (box-edges (sheet-viewport-region sheet))
    (declare (ignore right bottom))
    (unless (and (= x left) (= y top))
      (setf (sheet-transform sheet) (make-translation-transform (- x) (- y)))
      (setf (viewport-region viewport)
	    (set-box-position (viewport-region viewport) x y))
      (when update-scroll-bars?
	(update-scroll-bars viewport))
      (update-all-mirror-positions sheet)
      (let ((frame (sheet-frame sheet)))
	(when (and (or (/= left x) (/= top y)) frame)
	  (note-viewport-position-changed frame sheet x y))))))


#||
// This shifts a region of the "host screen" that's visible to some other visible
// location using 'copy-area'.  It does _not_ do any cleaning up after itself.
// It does not side-effect the output history of an output-recording stream.
// Back-ends can specialize this themselves based on the concrete gadget
// class.  For example, Windows uses 'ScrollWindowEx' to do this.
define method shift-visible-region
    (sheet :: <sheet>,
     oleft :: <integer>, otop :: <integer>, oright :: <integer>, obottom :: <integer>,
     nleft :: <integer>, ntop :: <integer>, nright :: <integer>, nbottom :: <integer>) => ()
  ignore(oright, obottom, nright, nbottom);
  let dx = oleft - nleft;
  let dy = otop  - ntop;
  let (sheet-width, sheet-height) = box-size(sheet-viewport-region(sheet));
  let from-x = #f;
  let from-y = #f;
  case
    dx >= 0 & dy >= 0 =>	// shifting down and to the right
      from-x := 0;
      from-y := 0;
    dx >= 0 & dy <= 0 =>	// shifting up and to the right
      from-x := 0;
      from-y := -dy;
    dy >= 0 =>			// shifting down and to the left
      from-x := -dx;
      from-y := 0;
    otherwise =>		// shifting up and to the left
      from-x := -dx;
      from-y := -dy
  end;
  let width  = sheet-width  - abs(dx);
  let height = sheet-height - abs(dy);
  let transform = sheet-transform(sheet);
  let (to-x, to-y)
    = untransform-position(transform, from-x + dx, from-y + dy);
  let (width, height) = untransform-distance(transform, width, height);
  let (from-x, from-y) = untransform-position(transform, from-x, from-y);
  copy-area(sheet, from-x, from-y, width, height, to-x, to-y)
end method shift-visible-region;
||#

(defmethod shift-visible-region ((sheet <sheet>)
				 (oleft integer) (otop integer) (oright integer) (obottom integer)
				 (nleft integer) (ntop integer) (nright integer) (nbottom integer))
  (declare (ignore oright obottom nright nbottom))
  (let ((dx (- oleft nleft))
	(dy (- otop ntop)))
    (multiple-value-bind (sheet-width sheet-height)
	(box-size (sheet-viewport-region sheet))
      (let ((from-x nil)
	    (from-y nil))
	(cond ((and (>= dx 0) (>= dy 0))    ;; shifting down and to the right
	       (setf from-x 0
		     from-y 0))
	      ((and (>= dx 0) (<= dy 0))    ;; shifting up and to the right
	       (setf from-x 0
		     from-y (- dy)))
	      ((>= dy 0)		    ;; shifting down and to the left
	       (setf from-x (- dx)
		     from-y 0))
	      (t		            ;; shifting up and to the left
	       (setf from-x (- dx)
		     from-y (- dy))))
	(let ((width     (- sheet-width  (abs dx)))
	      (height    (- sheet-height (abs dy)))
	      (transform (sheet-transform sheet)))
	  (multiple-value-bind (to-x to-y)
	      (untransform-position transform (+ from-x dx) (+ from-y dy))
	    (multiple-value-bind (width height)
		(untransform-distance transform width height)
	      (multiple-value-bind (from-x from-y)
		  (untransform-position transform from-x from-y)
		(copy-area sheet from-x from-y width height to-x to-y)))))))))

