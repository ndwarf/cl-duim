;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GADGETS-INTERNALS -*-
(in-package #:duim-gadgets-internals)

#||
/// Menus

define open abstract class <menu-bar>
    (<no-value-gadget-mixin>,
     <gadget-bar-mixin>,
     <basic-gadget>)
end class <menu-bar>;
||#

(defclass <menu-bar>
    (<no-value-gadget-mixin>
     <gadget-bar-mixin>
     <basic-gadget>)
  ()
  (:documentation
"
The class of menu bar gadgets.

Internally, this class maps into the Windows menu control.

Example:

The following example is similar to the example for <MENU>, except
that here, the menu bar object is explicitly defined. In the example
for <MENU>, it is created automatically by using CONTAIN:

    (defparameter *menu*
      (make-pane '<menu-bar>
                 :children
                 (vector (make-pane '<menu>
                                    :label \"Hello\"
                                    :children
                                    (vector (make-pane '<menu-button>
                                                       :label
                                                       \"World\"))))))
")
  (:metaclass <abstract-metaclass>))


#||
//--- Maybe this should just be <sheet>, but it's often convenient 
//--- to specify a frame
define constant <menu-owner> = false-or(type-union(<sheet>, <frame>));
||#

(deftype <menu-owner> () '(or null <sheet> <frame>))


#||
// A menu will have three kinds of children:
//  - one of the subclasses of <menu-button>
//  - one of the subclasses of <menu-box>
//  - other <menu>'s
define open abstract class <menu>
    (<no-value-gadget-mixin>,
     <gadget-command-mixin>,
     <gadget-bar-mixin>,
     <mnemonic-mixin>,
     <updatable-gadget-mixin>,
     <labelled-gadget-mixin>,
     <basic-gadget>)
  sealed slot menu-owner :: <menu-owner> = #f,		// for pop-up menus
    init-keyword: owner:;
end class <menu>;
||#

(defclass <menu>
    (<no-value-gadget-mixin>
     <gadget-command-mixin>
     <gadget-bar-mixin>
     <mnemonic-mixin>
     <updatable-gadget-mixin>
     <labelled-gadget-mixin>
     <basic-gadget>)
  ((menu-owner :type <menu-owner> :initarg :owner :initform nil :accessor menu-owner))        ;; for pop-up menus
  (:documentation
"
The class of menu gadgets.

Support for dynamically modifying the contents of a menu is provided
in the form of an update callback. If this is supplied using
the :update-callback initarg, then it is invoked just before the menu
is displayed. This callback is free to make changes to the contents of
the menu, which will then appear when the update callback is
complete. Note that you can also supply an update callback to any menu
box which forms a part of the menu, using the relevent initarg to
<MENU-BOX>.

The :owner argument is used to specify which sheet owns the menu. If
you fail to supply this, then the menu will be owned by the entire
screen.

The :mnemonic initarg is used to specify a keyboard mnemonic for the
button. This is a key press that involves pressing the ALT key
followed by a number of alphanumeric keys.

The :command initarg specifies a command that is invoked when the menu
is chosen. For most menus, you should not specify a command; instead,
you assign menu buttons as children to the menu, and the menu buttons
themselves have commands specified. However, in rare cases where the
menu has no children, and you want the menu itself to invoke a
command, you can use this initarg.

Internally, this class maps into the menu Windows control.

Example:

The following code creates a menu, 'Hello', that contains a single
button, 'World'. Notice how using CONTAIN creates a menu bar for you
automatically. You should note that using DISPLAY-MENU would not have
this effect.

    (defparameter *menu*
                  (contain (make-pane '<menu>
                                      :label \"Hello\"
                                      :children
                                      (vector (make-pane '<menu-button>
                                                         :label
                                                         \"World\")))))
")
  (:metaclass <abstract-metaclass>))

;;; TODO: Where to put the documentation for 'menu-owner'?
#||
Returns the sheet that owns _menu_, that is, the sheet in which _menu_
is displayed.

Every menu should specify which sheet it is owned by. If this is not
specified, then the menu will be owned by the entire screen.
||#

(defgeneric tear-off-menu? (menu))
(defgeneric help-menu? (menu))
(defgeneric display-menu (menu &key x y)
  (:documentation
"
Displays the specified menu, optionally at a precise position on the
screen, specified by _x_ and _y_, where _x_ and _y_ are both relative
to the owner of the menu.

The function returns when the menu has been popped down again.

FIXME: Test that the last line is not telling porkies...
"))

(defgeneric make-menu-from-items (framem items &key owner title label-key value-key width height
					 foreground background text-style multiple-sets?)
  (:documentation
"
Returns a menu object created from the specified _items_.

The _framem_ argument lets you specify a frame manager.

The _owner_ argument is used to specify which sheet owns the menu. If
you fail to supply this, then the menu will be owned by the entire
screen.

You can specify a _title_, if desired.

The _label-key_ and _value-key_ can be functions used to compute the
label and the value for each item in the menu, respectively. For more
information, see GADGET-LABEL-KEY or GADGET-VALUE-KEY. In general, the
label key can be trusted to \"do the right thing\" by default.

The _text-style_ argument specifies a text style for the menu. The
_foreground_ and _background_ arguments specify foreground and
background colours for the menu; _foreground_ being used for the text
in the menu, and _background_ for the menu itself.
"))


#||
define method initialize
    (menu :: <menu>,
     #key tear-off? = #f, help-menu? = #f,
	  x-alignment = #"left", y-alignment = #"top")
  next-method();
  let bits = logior(if (tear-off?) %tear_off_menu else 0 end,
		    if (help-menu?) %help_menu else 0 end);
  // <labelled-gadget-mixin> also does this, but we do it again
  // here because the defaults are different for menus...
  let xa = select (x-alignment)
	     #"left"  => %x_alignment_left;
	     #"right" => %x_alignment_right;
	     #"center", #"centre" => %x_alignment_center;
	   end;
  let ya = select (y-alignment)
	     #"top"      => %y_alignment_top;
	     #"bottom"   => %y_alignment_bottom;
	     #"baseline" => %y_alignment_baseline;
	     #"center", #"centre" => %y_alignment_center;
	   end;
  gadget-flags(menu)
    := logior(logand(gadget-flags(menu),
		     lognot(logior(%tear_off_menu, %help_menu,
				   %x_alignment_mask, %y_alignment_mask))),
	      bits, xa, ya)
end method initialize;
||#

(defmethod initialize-instance :after ((menu <menu>)
				       &key (tear-off? nil) (help-menu? nil) (x-alignment :left) (y-alignment :top)
				       &allow-other-keys)
  (let ((bits (logior (if tear-off?  %tear_off_menu 0)
                      (if help-menu? %help_menu     0)))
        ;; <labelled-gadget-mixin> also does this, but we do it again
        ;; here because the defaults are different for menus...
        (xa (ecase x-alignment
	      (:left   %x_alignment_left)
	      (:right  %x_alignment_right)
	      ((:center :centre) %x_alignment_center)))
        (ya (ecase y-alignment
	      (:top      %y_alignment_top)
	      (:bottom   %y_alignment_bottom)
	      (:baseline %y_alignment_baseline)
	      ((:center :centre) %y_alignment_center))))
    (setf (gadget-flags menu)
          (logior (logand (gadget-flags menu)
                          (lognot (logior %tear_off_menu %help_menu
                                          %x_alignment_mask %y_alignment_mask)))
                  bits xa ya))))


#||
define sealed inline method tear-off-menu?
    (menu :: <menu>) => (tear-off? :: <boolean>)
  logand(gadget-flags(menu), %tear_off_menu) = %tear_off_menu
end method tear-off-menu?;
||#

(defmethod tear-off-menu? ((menu <menu>))
  (= (logand (gadget-flags menu) %tear_off_menu) %tear_off_menu))


#||
define sealed inline method help-menu?
    (menu :: <menu>) => (help? :: <boolean>)
  logand(gadget-flags(menu), %help_menu) = %help_menu
end method help-menu?;
||#

(defmethod help-menu? ((menu <menu>))
  (= (logand (gadget-flags menu) %help_menu) %help_menu))


#||
define sealed method top-level-sheet 
    (menu :: <menu>) => (sheet :: false-or(<sheet>))
  let owner = menu-owner(menu);
  if (owner)
    top-level-sheet(owner)
  else
    next-method()
  end
end method top-level-sheet;
||#

(defmethod top-level-sheet ((menu <menu>))
  (let ((owner (menu-owner menu)))
    (if owner
        (top-level-sheet owner)
        (call-next-method))))


#||
define sealed method display
    (sheet :: <menu>) => (display :: false-or(<display>))
  if (menu-owner(sheet))
    display(menu-owner(sheet))
  else
    next-method()
  end
end method display;
||#

(defmethod display ((sheet <menu>))
  (if (menu-owner sheet)
      (display (menu-owner sheet))
      (call-next-method)))


#||
define sealed method gadget-selection-mode
    (menu :: <menu>) => (selection-mode :: <selection-mode>)
  #"none"
end method gadget-selection-mode;
||#

(defmethod gadget-selection-mode ((menu <menu>))
  :none)


#||
define open generic note-menu-attached 
    (frame :: <abstract-frame>, menu :: <menu>) => ();
||#

(defgeneric note-menu-attached (frame menu))


#||
define open generic note-menu-detached
    (frame :: <abstract-frame>, menu :: <menu>) => ();
||#

(defgeneric note-menu-detached (frame menu))


#||
// If this is a pop-up menu, ensure that it's mirrored before we map it.
// Then unmap it when we're done.
define method sheet-mapped?-setter 
    (mapped? == #t, menu :: <menu>, 
     #key do-repaint? = #t, clear? = do-repaint?)
 => (mapped? :: <boolean>)
  ignore(do-repaint?, clear?);
  let owner     = menu-owner(menu);
  let top-sheet = owner & top-level-sheet(owner);
  if (top-sheet)
    block ()
      unless (sheet-attached?(menu))
        //--- We should add this to the owner's top-level sheet,
        //--- but <top-level-sheet> only allows a single child
        add-child(display(top-sheet), menu);	// ensure the menu is attached
	note-menu-attached(sheet-frame(menu), menu)
      end;
      next-method()
    cleanup
      sheet-mapped?(menu) := #f
    end;
    mapped?
  else
    next-method()
  end
end method sheet-mapped?-setter;
||#

(defmethod (setf sheet-mapped?) ((mapped? (eql t)) (menu <menu>)
				 &key (do-repaint? t) (clear? do-repaint?))
  ;; do-repaint? is ignored in the body...
  (declare (ignore #||do-repaint?||# clear?))
  (let* ((owner     (menu-owner menu))
         (top-sheet (and owner (top-level-sheet owner))))
    (if top-sheet
	(progn
	  (unwind-protect
	       (progn
		 (unless (sheet-attached? menu)
		   ;;--- We should add this to the owner's top-level sheet,
		   ;;--- but <top-level-sheet> only allows a single child
		   (add-child (display top-sheet) menu)  ;; ensure the menu is attached
		   (note-menu-attached (sheet-frame menu) menu))
		 (call-next-method))
	    ;; cleanup
	    (setf (sheet-mapped? menu) nil))
          mapped?)
	;; else
        (call-next-method))))


#||
define method note-sheet-detached
    (menu :: <menu>) => ()
  next-method();
  let owner     = menu-owner(menu);
  let top-sheet = owner & top-level-sheet(owner);
  when (top-sheet)
    note-menu-detached(sheet-frame(top-sheet), menu)
  end
end method note-sheet-detached;
||#

(defmethod note-sheet-detached ((menu <menu>))
  (call-next-method)
  (let* ((owner     (menu-owner menu))
         (top-sheet (and owner (top-level-sheet owner))))
    (when top-sheet
      (note-menu-detached (sheet-frame top-sheet) menu))))


#||
define method note-menu-attached
    (frame :: <frame>, menu :: <menu>) => ()
  #f
end method note-menu-attached;
||#

(defmethod note-menu-attached ((frame <frame>) (menu <menu>))
  nil)


#||
define method note-menu-detached
    (frame :: <frame>, menu :: <menu>) => ()
  #f
end method note-menu-detached;
||#

(defmethod note-menu-detached ((frame <frame>) (menu <menu>))
  nil)


#||
define method display-menu
    (menu :: <menu>,
     #key x :: false-or(<integer>), y :: false-or(<integer>)) => ()
  let owner = menu-owner(menu);
  assert(owner,
	 "Cannot display a popup menu without an owner: %=", menu);
  if (x & y)
    set-sheet-position(menu, x, y)
  else
    let _port   = port(owner);
    let pointer = port-pointer(_port);
    //--- It would be much simpler if the owner was always a sheet
    let sheet
      = select (owner by instance?)
	  <frame> => top-level-sheet(owner);
	  <sheet> => owner;
	end;
    when (sheet)
      let (x, y) = pointer-position(pointer, sheet: sheet);
      set-sheet-position(menu, x, y)
    end
  end;
  sheet-mapped?(menu) := #t
end method display-menu;
||#

(defmethod display-menu ((menu <menu>) &key x y)
  (check-type x (or null integer))
  (check-type y (or null integer))
  (let ((owner (menu-owner menu)))
    (unless owner
      (error "Cannot display a popup menu without an owner: ~a" menu))
    (if (and x y)
        (set-sheet-position menu x y)
        (let* ((_port   (port owner))
               (pointer (port-pointer _port))
               ;;--- It would be much simpler if the owner was always a sheet
               (sheet (etypecase owner
                        (<frame> (top-level-sheet owner))
                        (<sheet> owner))))
          (when sheet
            (multiple-value-bind (x y)
		(pointer-position pointer :sheet sheet)
              (set-sheet-position menu x y))))))
  (setf (sheet-mapped? menu) t))



#||

/// Menu boxes

define open abstract class <menu-box>
    (<gadget-box>,
     <basic-gadget>)
end class <menu-box>;
||#

(defclass <menu-box>
    (<gadget-box> <basic-gadget>)
  ()
  (:documentation
"
A class that groups menu buttons. Like the <BUTTON-BOX> class, you can
use this class to create groups of menu buttons that are related in
some way. A visual separator is displayed in the menu in which a menu
box is inserted, separating the menu buttons defined in the menu box
from other menu buttons or menu boxes in the menu.

An example of the way in which a menu box may be used is to implement
the clipboard menu commands usually found in applications. A menu box
containing items that represent the 'Cut', 'Copy', and 'Paste'
commands can be created and inserted into the 'Edit' menu.

Internally, this class maps into the menu Windows control.

Support for dynamically modifying the contents of a menu box is
provided in the form of an update callback. If this is supplied using
the :update-callback initarg, then it is invoked just before the menu
box is displayed (this usually occurs at the same time that the menu
of which the menu box is a part is displayed). This callback is free
to make changes to the contents of the menu box, which will then
appear when the update callback is complete.

Example:

    (defparameter *menu-box*
                  (contain (make-pane '<menu-box>
                                      :items
                                      (range :from 0 :to 5))))
")
  (:metaclass <abstract-metaclass>))


#||
define function menu-box-selection-mode-class
    (selection-mode :: <selection-mode>) => (class :: <class>)
  select (selection-mode)
    #"none"     => <push-menu-box>;
    #"single"   => <radio-menu-box>;
    #"multiple" => <check-menu-box>;
  end
end function menu-box-selection-mode-class;
||#

(defun menu-box-selection-mode-class (selection-mode)
  (check-type selection-mode <selection-mode>)
  (ecase selection-mode
    (:none     (find-class '<push-menu-box>))
    (:single   (find-class '<radio-menu-box>))
    (:multiple (find-class '<check-menu-box>))))


#||
define sealed inline method make
    (class == <menu-box>, #rest initargs,
     #key selection-mode :: <selection-mode> = #"none", #all-keys)
 => (menu-box :: <menu-box>)
  apply(make, menu-box-selection-mode-class(selection-mode), initargs)
end method make;
||#

(defmethod make-pane ((class (eql (find-class '<menu-box>)))
		      &rest initargs &key (selection-mode :none) &allow-other-keys)
  (apply #'make-pane (menu-box-selection-mode-class selection-mode) initargs))


#||
define open abstract class <push-menu-box>
    (<action-gadget-mixin>,
     <menu-box>,
     <basic-value-gadget>)
end class <push-menu-box>;
||#

(defclass <push-menu-box>
    (<action-gadget-mixin>
     <menu-box>
     <basic-value-gadget>)
  ()
  (:documentation
"
The class of grouped push buttons in menus.

Internally, this class maps into the menu Windows control.

Example:

    (contain (make-pane '<push-menu-box>
                        :items (range :from 0 :to 5)))
")
  (:metaclass <abstract-metaclass>))


#||
define sealed method gadget-selection-mode
    (menu :: <push-menu-box>) => (selection-mode :: <selection-mode>)
  #"none"
end method gadget-selection-mode;
||#

(defmethod gadget-selection-mode ((menu <push-menu-box>))
  :none)


#||
define open abstract class <radio-menu-box> 
    (<gadget-selection-mixin>, <menu-box>)
end class <radio-menu-box>;
||#

(defclass <radio-menu-box>
    (<gadget-selection-mixin>
     <menu-box>)
  ()
  (:documentation
"
The class of grouped radio buttons that can appear in menus.

Internally, this class maps into the menu Windows control.

Example:

The following example creates a menu that shows an example of a radio
menu box, as well as several other menu gadgets.

    (contain
      (make-pane '<menu>
                 :label \"Hello\"
                 :children
                 (vector (make-pane '<menu-button>
                                    :label \"World\")
                         (make-pane '<menu-button>
                                    :label \"Bonzo\")
                         (make-pane '<radio-menu-box>
                                    :items
                                    (list \"You\" \"All\"
                                          \"Everyone\"))
                         (make-pane '<menu>
                                    :label \"Others\"
                                    :children
                                    (vector
                                      (make-pane '<check-menu-box>
                                                 :items
                                                 (list 1 2 3)))))))
")
  (:metaclass <abstract-metaclass>))


#||
define sealed method gadget-selection-mode
    (menu :: <radio-menu-box>) => (selection-mode :: <selection-mode>)
  #"single"
end method gadget-selection-mode;
||#

(defmethod gadget-selection-mode ((menu <radio-menu-box>))
  :single)


#||
define open abstract class <check-menu-box> 
    (<gadget-selection-mixin>, <menu-box>)
end class <check-menu-box>;
||#

(defclass <check-menu-box>
    (<gadget-selection-mixin>
     <menu-box>)
  ()
  (:documentation
"
The class of groups of check buttons displayed in a menu.

Internally, this class maps into the menu Windows control. (!)

Example:

The following example creates a menu that shows an example of a check
menu box.

    (contain
      (make-pane '<menu> :label \"Hello\"
                 :children
                 (vector (make-pane '<radio-menu-box>
                                    :items #(\"You\" \"All\"
                                             \"Everyone\")))))

 FIXME: This example doesn't work... no applicable method found for
 (NOTE-CHILD-ADDED <COLUMN-LAYOUT-PANE> <MENU-BAR>).
")
  (:metaclass <abstract-metaclass>))


#||
define sealed method gadget-selection-mode
    (menu :: <check-menu-box>) => (selection-mode :: <selection-mode>)
  #"multiple"
end method gadget-selection-mode;
||#

(defmethod gadget-selection-mode ((menu <check-menu-box>))
  :multiple)



#||

/// Menu buttons

define open abstract class <menu-button>
    (<updatable-gadget-mixin>,
     <button>)
end class <menu-button>;
||#

(defclass <menu-button>
    (<updatable-gadget-mixin>
     <button>)
  ()
  (:documentation
"
The class of buttons that can appear on menus.

You should take special care to define keyboard accelerators and
keyboard mnemonics for any menu buttons that you create. For a full
discussion on this, see the entry for <BUTTON>.

Internally, this class maps into the menu item Windows control.

Example:

    (contain (make-pane '<menu-button> :label \"Hello\"
                        :activate-callback
                        #'(lambda (gadget)
                            (notify-user
                              (format nil \"Pressed button ~a\"
                                      gadget)
                              :owner gadget))))

TODO: Check example works...
")
  (:metaclass <abstract-metaclass>))


#||
define open abstract class <push-menu-button> 
    (<menu-button>, <default-gadget-mixin>, <basic-value-gadget>)
end class <push-menu-button>;
||#

(defclass <push-menu-button>
    (<menu-button>
     <default-gadget-mixin>
     <basic-value-gadget>)
  ()
  (:documentation
"
The class of push buttons that appear on menus.

The :default? initarg sets the default value for the push menu button
gadget.

Internally, this class maps into the menu item Windows control.
")
  (:metaclass <abstract-metaclass>))


#||
define method initialize
    (button :: <push-menu-button>, #key) => ()
  next-method();
  when (gadget-command(button) & ~gadget-activate-callback(button))
    gadget-activate-callback(button) := callback-for-command(gadget-command(button))
  end
end method initialize;
||#

(defmethod initialize-instance :after ((button <push-menu-button>) &key
				       &allow-other-keys)
  (when (and (gadget-command button) (not (gadget-activate-callback button)))
    (setf (gadget-activate-callback button) (callback-for-command (gadget-command button)))))


#||
define sealed method gadget-selection-mode
    (button :: <push-menu-button>) => (selection-mode :: <selection-mode>)
  #"none"
end method gadget-selection-mode;
||#

(defmethod gadget-selection-mode ((button <push-menu-button>))
  :none)


#||
define open abstract class <radio-menu-button>
    (<menu-button>, <basic-value-gadget>)
end class <radio-menu-button>;
||#

(defclass <radio-menu-button>
    (<menu-button>
     <basic-value-gadget>)
  ()
  (:documentation
"
The class of radio buttons that can appear in menus. Isolated radio
menu buttons are of limited use; you will normally want to combine
several instances of such buttons using the <RADIO-MENU-BOX> gadget.

Internally, this class maps into the menu radio item Windows control.

Example:

    (contain (make-pane '<radio-menu-button> :label \"Hello\"))
")
  (:metaclass <abstract-metaclass>))


#||
define sealed method gadget-selection-mode
    (button :: <radio-menu-button>) => (selection-mode :: <selection-mode>)
  #"single"
end method gadget-selection-mode;
||#

(defmethod gadget-selection-mode ((button <radio-menu-button>))
  :single)


#||
// Because we don't have <action-gadget-mixin> in radio menu buttons
define sealed method activate-gadget
    (gadget :: <radio-menu-button>) => ()
  execute-value-changed-callback(gadget, gadget-client(gadget), gadget-id(gadget))
end method activate-gadget;
||#

;; Because we don't have <action-gadget-mixin> in radio menu buttons
(defmethod activate-gadget ((gadget <radio-menu-button>))
  (execute-value-changed-callback gadget (gadget-client gadget) (gadget-id gadget)))


#||
define open abstract class <check-menu-button>
    (<menu-button>, <basic-value-gadget>)
end class <check-menu-button>;
||#

(defclass <check-menu-button>
    (<menu-button>
     <basic-value-gadget>)
  ()
  (:documentation
"
The class of check buttons that can be displayed in a menu. The values
of a menu button is either T or NIL.

Internally, this class maps into the menu item Windows control. (!)

Example:

    (contain
      (make-pane '<check-menu-button>
                 :label \"Menu button\"
                 :activate-callback
                 #'(lambda (gadget)
                     (notify-user
                       (format nil \"Toggled button ~a\" gadget)))))

FIXME: This example doesn't work.
")
  (:metaclass <abstract-metaclass>))


#||
define sealed method gadget-selection-mode
    (button :: <check-menu-button>) => (selection-mode :: <selection-mode>)
  #"multiple"
end method gadget-selection-mode;
||#

(defmethod gadget-selection-mode ((button <check-menu-button>))
  :multiple)


#||
// Because we don't have <action-gadget-mixin> in check menu buttons
define sealed method activate-gadget
    (gadget :: <check-menu-button>) => ()
  execute-value-changed-callback(gadget, gadget-client(gadget), gadget-id(gadget))
end method activate-gadget;
||#

;; Because we don't have <action-gadget-mixin> in check menu buttons
;; XXX: WHY NOT?
(defmethod activate-gadget ((gadget <check-menu-button>))
  (execute-value-changed-callback gadget (gadget-client gadget) (gadget-id gadget)))


#||
define function menu-button-selection-mode-class
    (selection-mode :: <selection-mode>) => (class :: <class>)
  select (selection-mode)
    #"none"     => <push-menu-button>;
    #"single"   => <radio-menu-button>;
    #"multiple" => <check-menu-button>;
  end;
end function menu-button-selection-mode-class;
||#

(defun menu-button-selection-mode-class (selection-mode)
  (check-type selection-mode <selection-mode>)
  (ecase selection-mode
    (:none     (find-class '<push-menu-button>))
    (:single   (find-class '<radio-menu-button>))
    (:multiple (find-class '<check-menu-button>))))


#||
define sealed inline method make
    (class == <menu-button>, #rest initargs,
     #key selection-mode :: <selection-mode> = #"none", #all-keys)
 => (button :: <menu-button>)
  apply(make, menu-button-selection-mode-class(selection-mode), initargs)
end method make;
||#

(defmethod make-pane ((class (eql (find-class '<menu-button>)))
		      &rest initargs &key (selection-mode :none) &allow-other-keys)
  (apply #'make-pane (menu-button-selection-mode-class selection-mode) initargs))



#||

/// Popup menus

// Default implementation of menu chooser when handed a sequence of items.
// This is "sideways" because it is a forward reference from DUIM-Sheets.
define sideways method do-choose-from-menu
    (framem :: <frame-manager>, owner :: <sheet>, items :: <sequence>,
     #rest keys,
     #key title = #f, value,
          label-key = collection-gadget-default-label-key,
          value-key = collection-gadget-default-value-key,
          width, height, foreground, background, text-style,
	  multiple-sets? = #f,
     #all-keys)
 => (value, success? :: <boolean>)
  dynamic-extent(keys);
  ignore(value);
  let menu
    = make-menu-from-items(framem, items,
			   label-key: label-key,
			   value-key: value-key,
			   width:  width,
			   height: height,
			   foreground: foreground,
			   background: background,
			   text-style: text-style,
			   title: title,
			   owner: owner,
			   multiple-sets?: multiple-sets?);
  block ()
    apply(do-choose-from-menu, framem, owner, menu, keys)
  cleanup
    // We're done with it, so get rid of all back-end resources
    destroy-sheet(menu)
  end  
end method do-choose-from-menu;
||#

(defmethod do-choose-from-menu ((framem <frame-manager>) (owner <sheet>) (items sequence)
                                &rest keys
                                &key (title nil) value
                                (label-key #'collection-gadget-default-label-key)
                                (value-key #'collection-gadget-default-value-key)
                                width height foreground background text-style
                                (multiple-sets? nil)
                                &allow-other-keys)
  (declare (dynamic-extent keys)
           (ignore value))
  (let ((menu (make-menu-from-items framem items
                                    :label-key label-key
                                    :value-key value-key
                                    :width  width
                                    :height height
                                    :foreground foreground
                                    :background background
                                    :text-style text-style
                                    :title title
                                    :owner owner
                                    :multiple-sets? multiple-sets?)))
    (unwind-protect
         (apply #'do-choose-from-menu framem owner menu keys)
      ;; We're done with it, so get rid of all back-end resources
      (destroy-sheet menu))))


#||
define method make-menu-from-items
    (framem :: <frame-manager>, items :: <sequence>,
     #key owner, title = "Menu",
          label-key = collection-gadget-default-label-key,
          value-key = collection-gadget-default-value-key,
          width, height, foreground, background, text-style, multiple-sets? = #f)
 => (menu :: <menu>)
  with-frame-manager (framem)
    let menu-boxes :: <stretchy-object-vector> = make(<stretchy-vector>);
    local method make-menu-box (items)
	    add!(menu-boxes, make(<menu-box>,
				  items: items,
				  label-key: label-key,
				  value-key: value-key,
				  foreground: foreground,
				  background: background,
				  text-style: text-style))
	  end method;
    if (multiple-sets?)
      do(make-menu-box, items)
    else
      make-menu-box(items)
    end;
    make(<menu>,
	 owner: owner,
	 label: title,
	 width:  width,
	 height: height,
	 children:   menu-boxes, 
	 foreground: foreground,
	 background: background,
	 text-style: text-style)
  end
end method make-menu-from-items;
||#

(defmethod make-menu-from-items ((framem <frame-manager>) (items sequence)
                                 &key owner (title "Menu")
                                 (label-key #'collection-gadget-default-label-key)
                                 (value-key #'collection-gadget-default-value-key)
                                 width height foreground background text-style
                                 (multiple-sets? nil))
  (with-frame-manager (framem)
    (let ((menu-boxes (make-array 0 :adjustable t :fill-pointer t)))
      (labels ((make-menu-box (items)
                 (add! menu-boxes
                       (make-pane '<menu-box>
				  :items items
				  :label-key label-key
				  :value-key value-key
				  :foreground foreground
				  :background background
				  :text-style text-style))))
        (if multiple-sets?
            (map nil #'make-menu-box items)
            (make-menu-box items))
        (make-pane '<menu>
		   :owner owner
		   :label title
		   :width  width
		   :height height
		   :children   menu-boxes
		   :foreground foreground
		   :background background
		   :text-style text-style)))))



#||

/// Menu creation macros

/*--- The problem with this is that we lose the variable 'menu'!
define macro menu
  { menu (?label:expression, #rest ?options:expression)
      ?entries:*
    end }
    => { make(<menu>, label: ?label, children: vector(?entries), ?options) }
end macro menu;

define macro menu-box
  { menu-box (?label:expression, #rest ?options:expression)
      ?entries:*
    end }
    => { make(<menu-box>, label: ?label, children: vector(?entries), ?options) }
 entries:
  { } => { }
  { item ?label:expression => ?callback:expression; ... }
    => { make(<menu-button>, label: ?label, activate-callback: ?callback), ... }
end macro menu-box;
*/
||#

