;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GADGETS-INTERNALS -*-
(in-package #:duim-gadgets-internals)

#||
/// Table controls

define constant <table-control-view>
    = one-of(#"table", #"list", #"small-icon", #"large-icon");
||#

(deftype <table-control-view> ()
"
This type represents the acceptable values for the view arguments to
operators of <table-control>.

There are four possible values, corresponding to the view options that
will be familiar to most users of GUI-based operating systems:

:small-icon
    Displays each item in the table with a small icon to the left of
    the item. Items are arranged horizontally.

:large-icon
    Displays each item in the table with a large icon to the left of
    the item. Items are arranged horizontally.

:list
    Displays each item in the table with a small icon to the left of
    the item. Items are arranged vertically in one column.

:table
    Displays each item in the list with a small icon to the left of
    the item. Items are arranged vertically in one column. Additional
    details not available in other views are also displayed. The details
    that are displayed depend on the nature of the items in the table
    control. For example, if filenames are displayed in the table
    control, additional details may include the size, modification date,
    and creation date of each file. If e-mail messages are displayed in
    the table control, additional details may include the author of the
    e-mail, its subject, and the data and time it was sent.
"
  '(member :table :list :small-icon :large-icon))


#||
// The items in a table control are layed out as a sets of rows,
// and the rows can have multiple columns
// Note that items are not modelled as sheets!
define open abstract class <table-control>
    (<bordered-gadget-mixin>,
     <scrolling-gadget-mixin>,
     <action-gadget-mixin>,
     <key-press-gadget-mixin>,
     <popup-menu-gadget-mixin>,
     <basic-choice-gadget>)
  sealed constant slot gadget-lines :: false-or(<integer>) = #f,
    init-keyword: lines:;
  sealed slot table-control-view :: <table-control-view> = #"table",
    init-keyword: view:,
    setter: %table-control-view-setter;
  // Takes an object, produces two icons -- a small and a large icon
  sealed constant slot table-control-icon-function :: false-or(<function>) = #f,
    init-keyword: icon-function:;
  // Describes each column in the table
  sealed slot table-control-columns :: <sequence> = make(<stretchy-vector>),
    init-keyword: columns:;
end class <table-control>;
||#

;; The items in a table control are layed out as sets of rows,
;; and the rows can have multiple columns
;; Note that the items are not modelled as sheets!
(defclass <table-control>
    (<bordered-gadget-mixin>
     <scrolling-gadget-mixin>
     <action-gadget-mixin>
     <key-press-gadget-mixin>
     <popup-menu-gadget-mixin>
     <basic-choice-gadget>)
  ((gadget-lines :type (or null integer) :initarg :lines :initform nil :reader gadget-lines)
   (table-control-view :type <table-control-view> :initarg :view :initform :table
		       :reader table-control-view :writer %table-control-view-setter)
   ;; Takes an object, produces two icons -- a small and a large icon
   (table-control-icon-function :type (or null function) :initarg :icon-function :initform nil :reader table-control-icon-function)
   ;; Describes each column in the table
   (table-control-columns :type sequence :initarg :columns :initform (make-array 0 :adjustable t :fill-pointer t)
			  :accessor table-control-columns))
  (:documentation
"
The class of table controls.

The :view initarg can be used to specify how the items in the table
control are displayed. See <table-control-view> for more details.

The :borders initarg lets you specify a border around the table
control. If specified, a border of the appropriate type is drawn
around the gadget.

The :scroll-bars initarg defines the scroll bar behaviour for the
gadget.

You can use the :popup-menu-callback initarg to specify a
context-sensitive menu to display for one or more selected items in
the table control. In Windows 95, for instance, such a
context-sensitive menu can be displayed by right-clicking on any item
or group of selected items in the list control.

The :key-press-callback initarg lets you specify a key-press
callback. This type of callback is invoked whenever a key on the
keyboard is pressed while the gadget has focus. In a table control, a
key-press callback might be used as a quick way to select an item in
the control. See 'gadget-key-press-callback' for a fuller description
of key-press callbacks.

The :headings and :generators initargs can be used to specify the
titles of each column in the control, and a sequence of functions that
are used to generate the contents of each column. The headings should
be a sequence of strings, and the generators should be a sequence of
functions.

The first item in the sequence of headings is used as the title for
the first column, the second is used as the title of the second
column, and so on. Similarly, the first function in the sequence of
generators is invoked on each item in the control, thereby generating
the contents of the first column, the second is used to generate the
contents of the second column by invoking it on each item in the
control, and so on.

If you do not specify both of these initargs, you must supply columns
for the table control, using the <table-column> class.

The :widths initarg lets you specify the width of each column in the
table control. It takes a sequence of integers, each of which
represents the width, in pixels, of the respective column in the
control. Note that there must be as many widths as there are columns.

Internally, this class maps into the Windows list view control with
LVS-REPORT style.
")
  (:metaclass <abstract-metaclass>))


#||
// 'columns' is a vector of <table-column> objects...
// ...or your must supply 'headings' and 'generators'
define method initialize
    (pane :: <table-control>,
     #rest initargs,
     #key items = #[], columns,
     headings, generators, widths, alignments, callbacks) => ()
  // Ensure the items are in a stretchy vector so we can use 'add!' and 'remove!'
  apply(next-method, pane, items: as(<stretchy-vector>, items), initargs);
  assert(columns | (headings & generators),
	 "You must supply either columns, or headings and generators");
  unless (columns)
    assert(size(headings) = size(generators),
	   "There must be as many generators as there are headings");
    when (widths)
      assert(size(headings) = size(widths),
	     "There must be as many widths as there are headings")
    end;
    when (alignments)
      assert(size(headings) = size(alignments),
	     "There must be as many alignments as there are headings")
    end;
    when (callbacks)
      assert(size(headings) = size(callbacks),
	     "There must be as many callbacks as there are headings")
    end;
    let columns = make(<stretchy-vector>);
    for (i :: <integer> from 0 below size(headings))
      let heading   = headings[i];
      let generator = generators[i];
      let width     = (widths & widths[i]) | 100;
      let alignment = (alignments & alignments[i]) | #"left";
      let callback  = (callbacks & callbacks[i]) | #f;
      add!(columns, make(<table-column>,
			 heading:   heading,
			 width:     width,
			 alignment: alignment,
			 generator: generator,
			 callback:  callback))
    end;
    table-control-columns(pane) := columns
  end
end method initialize;
||#

;; Yes, this is an overloaded PRIMARY method; we want to be able to deal
;; with the items as a stretchy-vector in all the :after methods and this
;; is the place to permit that.
(defmethod initialize-instance ((pane <table-control>)
				&rest initargs
				&key (items #())  ; this will be made stretchy...
				columns headings generators widths alignments callbacks
				&allow-other-keys)
  ;; The following are used only in the :AFTER method...
  (declare (ignore columns headings generators widths alignments callbacks))
  ;; Ensure the items are in a stretchy vector so we can use 'add!'
  ;; and 'remove!'
  (apply #'call-next-method pane :items (MAKE-STRETCHY-VECTOR :contents items) initargs))

;; + an :after method. We want to do this stuff after the built-in CL method
;; has a chance to shared-initialize the object.
(defmethod initialize-instance :after ((pane <table-control>)
				       &rest initargs
				       &key (items #()) columns
				       headings generators widths alignments callbacks
				       &allow-other-keys)
  (declare (ignorable items initargs))
  (unless (or columns (and headings generators))
    (error "You must supply either columns, or headings and generators"))
  (unless columns
    (unless (= (length headings) (length generators))
      (error "There must be as many generators as there are headings"))
    (when widths
      (unless (= (length headings) (length widths))
	(error "There must be as many widths as there are headings")))
    (when alignments
      (unless (= (length headings) (length alignments))
	(error "There must be as many alignments as there are headings")))
    (when callbacks
      (unless (= (length headings) (length callbacks))
	(error "There must be as many callbacks as there are headings")))
    (let ((columns (MAKE-STRETCHY-VECTOR)))
      (loop for i from 0 below (length headings)
            ;; :FIXME: -- elt is sub-optimal, need to decide if these
            ;; should be sequences, or lists / vectors specifically.
	    do (let ((heading (elt headings i))
		     (generator (elt generators i))
		     (width (or (and widths (elt widths i)) 100))
		     (alignment (or (and alignments (elt alignments i)) :left))
		     (callback (or (and callbacks (elt callbacks i)) nil)))
		 (add! columns (make-instance '<table-column>
					      :heading heading
					      :width width
					      :alignment alignment
					      :generator generator
					      :callback callback))))
      (setf (table-control-columns pane) columns))))


#||
define method gadget-items-setter
    (items :: <sequence>, gadget :: <table-control>)
 => (items :: <sequence>)
  // Ensure the items are in a stretchy vector so we can use 'add!' and 'remove!'
  next-method(as(<stretchy-vector>, items), gadget)
end method gadget-items-setter;
||#

(defmethod (setf gadget-items) ((items sequence) (gadget <table-control>))
  ;; Ensure the items are in a stretchy vector so we can use 'add!' and 'remove!'
  (call-next-method (MAKE-STRETCHY-VECTOR :contents items) gadget))


#||
define sealed class <table-column> (<object>)
  constant slot table-column-heading :: <string>,
    required-init-keyword: heading:;
  constant slot table-column-width :: <integer> = 100,
    init-keyword: width:;
  constant slot table-column-alignment :: <x-alignment> = #"left",
    init-keyword: alignment:;
  constant slot table-column-generator :: <function>,
    required-init-keyword: generator:;
  constant slot table-column-callback :: false-or(<function>) = #f,
    init-keyword: callback:;
end class <table-column>;

define sealed domain make (singleton(<table-column>));
define sealed domain initialize (<table-column>);
||#

(defclass <table-column> ()
  ((table-column-heading :type string :initarg :heading :initform (required-slot ":heading" "<table-column>")
			 :reader table-column-heading)
   (table-column-width :type integer :initarg :width :initform 100 :reader table-column-width)
   (table-column-alignment :type <x-alignment> :initarg :alignment :initform :left :reader table-column-alignment)
   (table-column-generator :type function :initarg :generator :initform (required-slot ":generator" "<table-column>")
			   :reader table-column-generator)
   (table-column-callback :type (or null function) :initarg :callback :initform nil :reader table-column-callback))
  (:documentation
"
The class of columns in table controls.

The :width initarg lets you specify the width of the
column. The :alignment initarg is used to specify how the column
should be aligned in the table.

To populate the table column, the function specified by :generator is
invoked. This function is called for each item in the table control,
and the value returned is placed at the appropriate place in the
column.

In addition, you can also specify a callback that can be used for
sorting the items in the table column, using the :callback initarg.
"))


#||
define open abstract class <table-item> (<item>)
  sealed constant slot item-object = #f,
    init-keyword: object:;
end class <table-item>;
||#

(defclass <table-item> (<item>)
  ((item-object :initarg :object :initform nil :reader item-object))
  (:documentation
"
The class that represents an item in a table control.

The :object initarg describes the object that an instance of table
item represents.
")
  (:metaclass <abstract-metaclass>))


#||
define protocol <<table-control>> (<<list-control>>)
  function add-column
    (control :: <table-control>, column :: <table-column>, index :: <integer>) => ();
  function remove-column
    (control :: <table-control>, index :: <integer>) => ();
  function do-add-column
    (control :: <table-control>, column :: <table-column>, index :: <integer>) => ();
  function do-remove-column
    (control :: <table-control>, index :: <integer>) => ();
  // Views, etc
  getter table-control-view
    (table-control :: <table-control>) => (view :: <table-control-view>);
  setter table-control-view-setter
    (view :: <table-control-view>, table-control :: <table-control>)
 => (view :: <table-control-view>);
end protocol <<table-control>>;
||#
#||
(define-protocol <<table-control>> (<<list-control>>)
  (:function add-column (control column index) (:documentation " Defined by: <<TABLE-CONTROL>> protocol "))
  (:function remove-column (control index) (:documentation " Defined by: <<TABLE-CONTROL>> protocol "))
  (:function do-add-column (control column index) (:documentation " Defined by: <<TABLE-CONTROL>> protocol "))
  (:function do-remove-column (control index) (:documentation " Defined by: <<TABLE-CONTROL>> protocol "))
  ;; Views, etc
  (:getter table-control-view (table-control) (:documentation " Defined by: <<TABLE-PROTOCOL>> "))
  (:setter (setf table-control-view) (view table-control) (:documentation " Defined by: <<TABLE-PROTOCOL>> ")))
||#

#||
define method table-control-view-setter
    (view :: <table-control-view>, pane :: <table-control>)
 => (view :: <table-control-view>)
  pane.%table-control-view := view
end method table-control-view-setter;
||#

(defmethod (setf table-control-view) (view (pane <table-control>))
  (check-type view <table-control-view>)
  (%table-control-view-setter view pane))


#||
define sealed method make-item
    (pane :: <table-control>, object, #rest initargs, #key)
 => (table-item :: <table-item>)
  apply(do-make-item, pane, <table-item>, object: object, initargs)
end method make-item;
||#

(defmethod make-item ((pane <table-control>) object &rest initargs &key)
  (apply #'do-make-item pane (find-class '<table-item>) :object object initargs))


#||
define sealed method find-item
    (pane :: <table-control>, object, #key)
 => (node :: false-or(<table-item>))
  do-find-item(pane, object)
end method find-item;
||#

(defmethod find-item ((pane <table-control>) object &key)
  (do-find-item pane object))


#||
// AFTER indicates which item to place the new item after
define sealed method add-item
    (pane :: <table-control>, item :: <table-item>, #key after) => ()
  // Update the set of items, bypassing 'note-gadget-items-changed'
  pane.%items
    := add!(as(<stretchy-vector>, gadget-items(pane)), item-object(item));
  // Update the selection by incrementing the indices of anything
  // after the newly added item
  when (after)
    let index = position(gadget-items(pane), item-object(after));
    when (index)
      let selection = gadget-selection(pane);
      for (i :: <integer> from 0 below size(selection))
	when (selection[i] > index)
	  selection[i] := selection[i] + 1
	end
      end;
      pane.%selection := selection
    end
  end;
  do-add-item(pane, item, after: after)
end method add-item;
||#

(defmethod add-item ((pane <table-control>) (item <table-item>) &key after)
  ;; Update the set of items, bypassing 'note-gadget-items-changed'
  (%items-setter (add! (MAKE-STRETCHY-VECTOR :contents (gadget-items pane)) (item-object item)) pane)
  ;; Update the selection by incrementing the indices of anything
  ;; after the newly added item
  (when after
    (let ((index (position (gadget-items pane) (item-object after))))
      (when index
	(let ((selection (gadget-selection pane)))
	  (loop for i from 0 below (length selection)
	     when (> (aref selection i) index)
	     do (setf (aref selection i) (+ (aref selection i) 1)))
	  (%selection-setter selection pane)))))
  (do-add-item pane item :after after))


#||
define sealed method remove-item
    (pane :: <table-control>, item :: <table-item>) => ()
  pane.%items
    := remove!(as(<stretchy-vector>, gadget-items(pane)), item-object(item));
  // Update the selection by decrementing the indices of anything
  // after the deleted item
  let index = position(gadget-items(pane), item-object(item));
  when (index)
    let selection = remove(gadget-selection(pane), index);
    for (i :: <integer> from 0 below size(selection))
      when (selection[i] > index)
	selection[i] := selection[i] - 1
      end
    end;
    pane.%selection := selection
  end;
  do-remove-item(pane, item)  
end method remove-item;
||#

(defmethod remove-item ((pane <table-control>) (item <table-item>))
  (%items-setter (delete (item-object item) (coerce (gadget-items pane) 'array)) pane)
  ;; Update the selection by decrementing the indices of anything
  ;; after the deleted item
  (let ((index (position (gadget-items pane) (item-object item))))
    (when index
      (let ((selection (remove index (gadget-selection pane))))
	(loop for i from 0 below (length selection)
	   when (> (aref selection i) index)
	   do (setf (aref selection i) (- (aref selection i) 1)))
	(%selection-setter selection pane))))
  (do-remove-item pane item))


#||
define sealed method add-column
    (pane :: <table-control>, column :: <table-column>, index :: <integer>) => ()
  insert-at!(table-control-columns(pane), column, index);
  do-add-column(pane, column, index)
end method add-column;
||#

(defmethod add-column ((pane <table-control>) (column <table-column>) (index integer))
  (insert-at! (table-control-columns pane) column index)
  (do-add-column pane column index))


#||
define sealed method remove-column
    (pane :: <table-control>, index :: <integer>) => ()
  do-remove-column(pane, index);
  remove-at!(table-control-columns(pane), index)
end method remove-column;
||#

(defmethod remove-column ((pane <table-control>) (index integer))
  (do-remove-column pane index)
  (remove-at! (table-control-columns pane) index))



#||

/// Column-click callbacks

define sealed class <column-click-gadget-event> (<gadget-event>)
  sealed constant slot event-column :: <table-column>,
    required-init-keyword: column:;
end class <column-click-gadget-event>;

define sealed domain make (singleton(<column-click-gadget-event>));
define sealed domain initialize (<column-click-gadget-event>);
||#

(defclass <column-click-gadget-event> (<gadget-event>)
  ((event-column :type <table-column> :initarg :column :initform (required-slot ":column" "<column-click-gadget-event>")
		 :reader event-column)))


#||
define sealed method handle-event
    (gadget :: <table-control>, event :: <column-click-gadget-event>) => ()
  let function = table-column-callback(event-column(event));
  //--- Perhaps this should pass both the gadget and the column
  when (function)
    function(gadget)
  end
end method handle-event;
||#

(defmethod handle-event ((gadget <table-control>) (event <column-click-gadget-event>))
  (let ((function (table-column-callback (event-column event))))
    ;;--- Perhaps this should pass both the gadget and the column
    (when function
      (funcall function gadget))))


#||
define function distribute-column-click-callback
    (gadget :: <table-control>, column :: <table-column>) => ()
  distribute-event(port(gadget),
		   make(<column-click-gadget-event>,
			gadget: gadget,
			column: column))
end function distribute-column-click-callback;
||#

(defun distribute-column-click-callback (gadget column)
  (check-type gadget <table-control>)
  (check-type column <table-column>)
  (distribute-event (port gadget)
		    (make-instance '<column-click-gadget-event>
				   :gadget gadget
				   :column column)))

