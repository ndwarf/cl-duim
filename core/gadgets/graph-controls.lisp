;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GADGETS-INTERNALS -*-
(in-package #:duim-gadgets-internals)

#||
/// Graph controls

define constant <graph-orientation>
  = one-of(#"horizontal", #"vertical", #"up", #"down", #"left", #"right");
||#

(deftype <graph-orientation> ()
  '(member :horizontal :vertical :up :down :left :right))


#||
// The nodes in a tree control are layed out in a genuine graph
// Note that nodes and edges are not modelled as sheets in the front end!
//--- In a perfect world, <tree-control> would be a subclass of <graph-control>
define open abstract class <graph-control> (<tree-control>)
  // Various user-settable properties
  sealed constant slot graph-edge-class :: false-or(subclass(<graph-edge>)) = #f,
    init-keyword: edge-class:;
  sealed constant slot graph-edge-initargs :: <sequence> = #[],
    init-keyword: edge-initargs:;
  sealed constant slot graph-edge-generator :: false-or(<function>) = #f,
    init-keyword: edge-generator:;
  sealed constant slot graph-orientation :: <graph-orientation> = #"horizontal",
    init-keyword: orientation:;
  sealed constant slot graph-center-nodes? :: <boolean> = #f,
    init-keyword: center-nodes?:;
  sealed constant slot graph-inter-generation-spacing :: <integer> = 20,
    init-keyword: inter-generation-spacing:;
  sealed constant slot graph-intra-generation-spacing :: <integer> =  8,
    init-keyword: intra-generation-spacing:;
end class <graph-control>;
||#

;; FIXME: CAN DO THIS FOR ALL SLOTS THAT ARE SUBCLASS SLOTS IN THE DYLAN... STILL
;; CAN'T DEAL WELL WITH METHODS THOUGH :(
;; FIXME: FOR LISTS, WHILST THE TYPE MIGHT BE LIST, THE CLASS IS CONS

(defun subclass-graph-edge-p (object)
  (let ((class-name (class-name object)))
    (subtypep class-name '<graph-edge>)))

;; The nodes in a tree control are layed out in a genuine graph
;; Node that nodes and edges are not modelled as sheets in the front end!
;;--- In a perfect world, <tree-control> would be a subclass of <graph-control>
(defclass <graph-control> (<tree-control>)
  ;; Various user-settable properties
  ((graph-edge-class :initarg :edge-class :type (or null (satisfies subclass-graph-edge-p)) :initform nil :reader graph-edge-class)
   (graph-edge-initargs :type list #||sequence||# :initarg :edge-initargs :initform '()  #|| #()||# :reader graph-edge-initargs)
   (graph-edge-generator :type (or null function) :initarg :edge-generator :initform nil :reader graph-edge-generator)
   (graph-orientation :type <graph-orientation> :initarg :orientation :initform :horizontal :reader graph-orientation)
   (graph-center-nodes? :type boolean :initarg :center-nodes? :initform nil :reader graph-center-nodes?)
   (graph-inter-generation-spacing :type integer :initarg :inter-generation-spacing :initform 20 :reader graph-inter-generation-spacing)
   (graph-intra-generation-spacing :type integer :initarg :intra-generation-spacing :initform 8 :reader graph-intra-generation-spacing))
  (:documentation
"
The nodes in a tree control are layed out in a genuine graph.
Note that nodes and edges are not modelled as sheets in the front end!
")
  (:metaclass <abstract-metaclass>))


#||
// Nodes within a graph control
//--- In a perfect world, <tree-node> would be a subclass of <graph-node>
define open abstract class <graph-node> (<tree-node>)
end class <graph-node>;
||#

;; Nodes within a graph control
;;--- In a perfect world, <tree-node> would be a subclass of <graph-node>
(defclass <graph-node> (<tree-node>)
  ()
  (:metaclass <abstract-metaclass>))


#||
define protocol <<graph-node>> (<<tree-node>>)
  getter node-x
    (node :: <tree-node>) => (x :: <integer>);
  getter node-x-setter
    (x :: <integer>, node :: <tree-node>) => (x :: <integer>);
  getter node-y
    (node :: <tree-node>) => (y :: <integer>);
  getter node-y-setter
    (y :: <integer>, node :: <tree-node>) => (y :: <integer>);
end protocol <<graph-node>>;
||#
#||
(define-protocol <<graph-node>> (<<tree-node>>)
  (:getter node-x (node))
  (:getter (setf node-x) (x node))
  (:getter node-y (node))
  (:getter (setf nody-y) (y node)))
||#

#||
define sealed method make-node
    (graph :: <graph-control>, object, #rest initargs, #key)
 => (node :: <graph-node>)
  apply(do-make-node, graph, <graph-node>, object: object, initargs)
end method make-node;
||#

(defmethod make-node ((graph <graph-control>) object &rest initargs &key)
  (apply #'do-make-node graph (find-class '<graph-node>) :object object initargs))


#||
define sealed method find-node
    (graph :: <graph-control>, object, #key node: parent-node)
 => (node :: false-or(<graph-node>))
  do-find-node(graph, object, node: parent-node)
end method find-node;
||#

(defmethod find-node ((graph <graph-control>) object &key node)
  (let ((parent-node node))
    (do-find-node graph object :node parent-node)))


#||
// Edges within a graph control
define open abstract primary class <graph-edge> (<object>)
  sealed constant slot graph-edge-from-node :: <graph-node>,
    required-init-keyword: from-node:;
  sealed constant slot graph-edge-to-node   :: <graph-node>,
    required-init-keyword: to-node:;
  sealed constant slot graph-edge-object    :: <object> = #f,
    init-keyword: object:;
end class <graph-edge>;
||#

(defclass <graph-edge> ()
  ((graph-edge-from-node :type <graph-node> :initarg :from-node :initform (required-slot ":from-node" "<graph-edge>")
			 :reader graph-edge-from-node)
   (graph-edge-to-node :type <graph-node> :initarg :to-node :initform (required-slot ":to-node" "<graph-edge>")
		       :reader graph-edge-to-node)
   (graph-edge-object :initarg :object :initform nil :reader graph-edge-object))
  (:metaclass <abstract-metaclass>))


#||
define protocol <<graph-edge>> ()
  getter draw-edges
    (edge :: <graph-edge>, medium :: <abstract-medium>, region :: <region>) => ();
end protocol <<graph-edge>>;
||#
#||
(define-protocol <<graph-edge>> ()
  (:getter draw-edges (edge medium region)))
||#


