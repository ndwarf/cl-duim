;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GADGETS-INTERNALS -*-
(in-package #:duim-gadgets-internals)

#||
/// Tree controls

// The nodes in a tree control are layed out in a column (with indentation)
// Note that nodes are not modelled as sheets!
define open abstract class <tree-control>
    (<bordered-gadget-mixin>,
     <scrolling-gadget-mixin>,
     <action-gadget-mixin>,
     <key-press-gadget-mixin>,
     <popup-menu-gadget-mixin>,
     <basic-choice-gadget>)
  sealed constant slot gadget-lines :: false-or(<integer>) = #f,
    init-keyword: lines:;
  sealed slot tree-control-roots :: <stretchy-object-vector> = make(<stretchy-vector>),
    setter: %roots-setter;
  sealed slot tree-control-root-nodes :: <stretchy-object-vector> = make(<stretchy-vector>);
  // How deep should the tree be initially?
  sealed slot tree-control-initial-depth :: <integer> = 0,
    init-keyword: depth:;
  // Takes an object, produces child objects
  sealed slot tree-control-children-generator :: <function>,
    required-init-keyword: children-generator:;
  // Takes an object, returns #t iff there are any child objects
  sealed slot tree-control-children-predicate :: <function> = default-children-predicate,
    init-keyword: children-predicate:;
  // Takes an object, produces two icons -- a normal and a selected icon
  sealed constant slot tree-control-icon-function :: false-or(<function>) = #f,
    init-keyword: icon-function:;
  // Compressed tree control flag word
  sealed slot tree-control-flags :: <integer> = $initial-tree-control-flags;
  // Callback for node-state changed
  sealed slot tree-node-state-changed-callback :: <callback-type> = #f,
    init-keyword: node-state-changed-callback:;
end class <tree-control>;
||#

;; ^^ has been moved lower... fixme, check if that was necessary.

(defgeneric tree-control-show-edges? (tree))
(defgeneric (setf tree-control-show-edges?) (show-edges? tree))
(defgeneric tree-control-show-root-edges? (tree))
(defgeneric (setf tree-control-show-root-edges?) (show-root-edges? tree))
(defgeneric tree-control-show-buttons? (tree))
(defgeneric (setf tree-control-show-buttons?) (show-buttons? tree))
(defgeneric ensure-node-visible (tree node))
(defgeneric execute-node-state-changed-callback (tree client id node))
(defgeneric do-execute-node-state-changed-callback (tree client id node))


#||
// Bits 0..2 are some basic boolean flags
define constant %tree_show_edges      :: <integer> = #o01;
define constant %tree_show_root_edges :: <integer> = #o02;
define constant %tree_show_buttons    :: <integer> = #o04;
||#

(defconstant %tree_show_edges      #o01)
(defconstant %tree_show_root_edges #o02)
(defconstant %tree_show_buttons    #o04)

#||
define constant $initial-tree-control-flags :: <integer>
    = logior(%tree_show_edges, %tree_show_root_edges, %tree_show_buttons);
||#

(defconstant +initial-tree-control-flags+ (logior %tree_show_edges
						  %tree_show_root_edges
						  %tree_show_buttons))


;;; These are accessors in <tree-control>; defining the GFs here gives somewhere
;;; to attach the documentation.
(defgeneric tree-control-children-predicate (tree-control)
  (:documentation
"
Returns the children predicate function of _tree-control_.
"))

(defgeneric (setf tree-control-children-predicate) (children-predicate tree-control)
  (:documentation
"
Sets the children predicate function of _tree-control_.
"))

(defgeneric tree-control-children-generator (tree-control)
  (:documentation
"
Returns the function that generates the children of
_tree-control_. This is the function that is used to generate the
children below the root of _tree-control_.
"))

(defgeneric (setf tree-control-children-generator) (children-generator tree-control)
  (:documentation
"
Sets the function that generates the children of _tree-control_. This
is the function that is used to generate the children below the root
of _tree-control_.
"))

(defgeneric tree-control-icon-function (tree-control)
  (:documentation
"
Returns the icon function for _tree-control_. This function lets you
specify which icon to display for each item in the control. The
function is called with the item that needs an icon as its argument,
and it should return an instance of <image> as its result. Typically,
you might want to define an icon function that returns a different
icon for each kind of item in the control. For example, if the control
is used to display the files and directories on a hard disk, you would
want to return the appropriate icon for each registered file type.

Note that, unlike list controls, the icon function for a tree control
cannot be changed once the list control has been created.

TODO: I'm pretty sure the icon function in list controls cannot be
changed either...
"))

(defgeneric tree-control-initial-depth (tree-control)
  (:documentation
"
Returns the initial depth of _tree-control_. This is the number of
levels of outline that are visible in the tree control when it is
first displayed. A return value of 0 indicates that only the top level
of the outline is displayed initially. A return value of 1 indicates
that the outline is expanded to a depth of one (that is, any direct
subnodes of the top level are displayed, but no others).
"))

(defgeneric (setf tree-control-initial-depth) (initial-depth tree-control)
  (:documentation
"
Sets the initial depth of _tree-control_. This is the number of levels
of outline that are visible in the tree control when it is first
displayed. A return value of 0 indicates that only the top level of
the outline is displayed initially. A return value of 1 indicates that
the outline is expanded to a depth of one (that is, any direct
subnodes of the top level are displayed, but no others).
"))


;; The nodes in a tree control are layed out in a column (with indentation)
;; Note that nodes are not modelled as sheets!
(defclass <tree-control>
    (<bordered-gadget-mixin>
     <scrolling-gadget-mixin>
     <action-gadget-mixin>
     <key-press-gadget-mixin>
     <popup-menu-gadget-mixin>
     <basic-choice-gadget>)
  ((gadget-lines :type (or null integer) :initarg :lines :initform nil :reader gadget-lines)
   (tree-control-roots :type vector :initform (make-array 0 :adjustable t :fill-pointer t)
		       :reader tree-control-roots :writer %roots-setter)
   (tree-control-root-nodes :type vector :initform (make-array 0 :adjustable t :fill-pointer t)
			    :accessor tree-control-root-nodes)
   ;; How deep should the tree be initially?
   (tree-control-initial-depth :type integer :initarg :depth :initform 0 :accessor tree-control-initial-depth)
   ;; Takes an object, produces child objects
   (tree-control-children-generator :type function :initarg :children-generator
				    :initform (required-slot ":children-generator" "<tree-control>")
				    :accessor tree-control-children-generator)
   ;; Takes an object, returns #t iff there are any child objects
   (tree-control-children-predicate :type function :initarg :children-predicate :initform #'default-children-predicate
				    :accessor tree-control-children-predicate)
   ;; Takes an object, produces two icons -- a normal and a selected icon
   (tree-control-icon-function :type (or null function) :initarg :icon-function :initform nil
			       :reader tree-control-icon-function)
   ;; Compressed tree control flag word
   (tree-control-flags :type integer :initform +initial-tree-control-flags+ :accessor tree-control-flags)
   ;; Callback for node-state changed
   (tree-node-state-changed-callback :type <callback-type> :initarg :node-state-changed-callback :initform nil
				     :accessor tree-node-state-changed-callback))
  (:documentation
"
The class of tree controls.

The :children-generator is the function that is used to generate the
children below the root of the tree control. It is called with one
argument, an object.

The :icon-function initarg lets you specify a function to supply icons
for display in the control. The function is called with the item that
needs an icon as its argument, and it should return an instance of
<image> as its result. Typically, you might want to define an icon
function that returns a different icon for each kind of item in the
control. For example, if the control is used to display the files and
directories on a hard disk, you would want to return the appropriate
icon for each registered file type.

The :show-edges?, :show-root-edges?, and :show-buttons? initargs
define whether lines are displayed for the edges of items in the tree
control, the roots in the tree control, and whether the icons of items
in the tree control are displayed, respectively. By default, all three
are visible.

The number of levels of outline that are shown when the tree control
is first displayed is controlled by the :initial-depth initarg. The
default value of this is 0, meaning that only the top level of the
outline is shown, with no nodes expanded.

The :scroll-bars initarg specifies whether the tree control has scroll
bars or not.

You can use the :popup-menu-callback initarg to specify a
context-sensitive menu to display for one or more selected items in
the tree control. In Windows 95, for instance, such a
context-sensitive menu can be displayed by right-clicking on any item
or group of selected items in the list control.

The :key-press-callback initarg lets you specify a key-press
callback. This type of callback is invoked whenever a key on the
keyboard is pressed while the gadget has focus. For tree controls, a
typical key-press callback might select an item in the control. See
'gadget-key-press-callback' for a fuller description of key-press
callbacks.

The :roots initarg is used to specify any roots for the tree
control. It is a sequence.

Internally, this class maps into the Windows tree view control.

Example:

    (make-pane '<tree-control>
               :roots (vector 1)
               :children-generator
               #'(lambda (x)
                   (vector (* x 2) (1+ (* x 2))))
               :icon-function
               #'(lambda (item)
                   (ecase ((oddp item)  *odd-icon*)
                          ((evenp item) *even-icon*))))
")
  (:metaclass <abstract-metaclass>))

#|| Moved up...

;; Bits 0..2 are some basic boolean flags
(defconstant %tree_show_edges      #o01)
(defconstant %tree_show_root_edges #o02)
(defconstant %tree_show_buttons    #o04)

(defconstant $initial-tree-control-flags (logior %tree_show_edges
						 %tree_show_root_edges
						 %tree_show_buttons))
||#

#||
define method initialize
    (tree :: <tree-control>,
     #key roots = #[], show-edges? = #t, show-root-edges? = #t, show-buttons? = #t)
  next-method();
  let bits = logior(if (show-edges?)      %tree_show_edges      else 0 end,
		    if (show-root-edges?) %tree_show_root_edges else 0 end,
		    if (show-buttons?)    %tree_show_buttons    else 0 end);
  tree-control-flags(tree) := bits;
  tree.%roots := as(<stretchy-vector>, roots);
  tree.%items := make(<stretchy-vector>)
end method initialize;
||#

(defmethod initialize-instance :after ((tree <tree-control>)
				       &key (roots #()) (show-edges? t) (show-root-edges? t) (show-buttons? t)
				       &allow-other-keys)
  (let ((bits (logior (if show-edges?      %tree_show_edges 0)
		      (if show-root-edges? %tree_show_root_edges 0)
		      (if show-buttons?    %tree_show_buttons 0))))
    (setf (tree-control-flags tree) bits)
    (%roots-setter (MAKE-STRETCHY-VECTOR :contents roots) tree)
    (%items-setter (MAKE-STRETCHY-VECTOR) tree)))


#||
define sealed inline method tree-control-show-edges?
    (tree :: <tree-control>) => (show-edges? :: <boolean>)
  logand(tree-control-flags(tree), %tree_show_edges) = %tree_show_edges
end method tree-control-show-edges?;
||#

(defmethod tree-control-show-edges? ((tree <tree-control>))
  (= (logand (tree-control-flags tree) %tree_show_edges) %tree_show_edges))


#||
define sealed inline method tree-control-show-edges?-setter
    (show-edges? :: <boolean>, tree :: <tree-control>) => (show-edges? :: <boolean>)
  tree-control-flags(tree)
    := logior(logand(tree-control-flags(tree), lognot(%tree_show_edges)),
	      if (show-edges?) %tree_show_edges else 0 end);
  show-edges?
end method tree-control-show-edges?-setter;
||#

(defmethod (setf tree-control-show-edges?) (show-edges? (tree <tree-control>))
  (check-type show-edges? boolean)
  (setf (tree-control-flags tree)
	(logior (logand (tree-control-flags tree) (lognot %tree_show_edges))
		(if show-edges? %tree_show_edges 0)))
  show-edges?)


#||
define sealed inline method tree-control-show-root-edges?
    (tree :: <tree-control>) => (show-root-edges? :: <boolean>)
  logand(tree-control-flags(tree), %tree_show_root_edges) = %tree_show_root_edges
end method tree-control-show-root-edges?;
||#

(defmethod tree-control-show-root-edges? ((tree <tree-control>))
  (= (logand (tree-control-flags tree) %tree_show_root_edges) %tree_show_root_edges))


#||
define sealed inline method tree-control-show-root-edges?-setter
    (show-root-edges? :: <boolean>, tree :: <tree-control>) => (show-root-edges? :: <boolean>)
  tree-control-flags(tree)
    := logior(logand(tree-control-flags(tree), lognot(%tree_show_root_edges)),
	      if (show-root-edges?) %tree_show_root_edges else 0 end);
  show-root-edges?
end method tree-control-show-root-edges?-setter;
||#

(defmethod (setf tree-control-show-root-edges?) (show-root-edges? (tree <tree-control>))
  (check-type show-root-edges? boolean)
  (setf (tree-control-flags tree)
       (logior (logand (tree-control-flags tree) (lognot %tree_show_root_edges))
	       (if show-root-edges? %tree_show_root_edges 0)))
  show-root-edges?)


#||
define sealed inline method tree-control-show-buttons?
    (tree :: <tree-control>) => (show-buttons? :: <boolean>)
  logand(tree-control-flags(tree), %tree_show_buttons) = %tree_show_buttons
end method tree-control-show-buttons?;
||#

(defmethod tree-control-show-buttons? ((tree <tree-control>))
  (= (logand (tree-control-flags tree) %tree_show_buttons) %tree_show_buttons))


#||
define sealed inline method tree-control-show-buttons?-setter
    (show-buttons? :: <boolean>, tree :: <tree-control>) => (show-buttons? :: <boolean>)
  tree-control-flags(tree)
    := logior(logand(tree-control-flags(tree), lognot(%tree_show_buttons)),
	      if (show-buttons?) %tree_show_buttons else 0 end);
  show-buttons?
end method tree-control-show-buttons?-setter;
||#

(defmethod (setf tree-control-show-buttons?) (show-buttons? (tree <tree-control>))
  (check-type show-buttons? boolean)
  (setf (tree-control-flags tree)
	(logior (logand (tree-control-flags tree) (lognot %tree_show_buttons))
		(if show-buttons? %tree_show_buttons 0)))
  show-buttons?)


#||
// #f means that no items have ever been added to the node
define constant <node-state> = one-of(#"expanded", #"contracted", #f);
||#

;; FIXME: MAKE SURE OTHER DEFTYPE FORMS REFER TO NIL RATHER THAN NULL!
(deftype <node-state> () '(member :expanded :contracted nil))


#||
define open abstract class <tree-node> (<item>)
  sealed constant slot node-object = #f,
    init-keyword: object:;
  sealed slot node-parents :: <sequence> = make(<stretchy-vector>),
    init-keyword: node-parents:;
  sealed slot node-children :: <sequence> = make(<stretchy-vector>),
    init-keyword: node-children:,
    setter: %node-children-setter;
  sealed slot node-state :: <node-state> = #f;
  sealed slot node-generation :: <integer> = 0,
    init-keyword: generation:;
end class <tree-node>;
||#

(defclass <tree-node> (<item>)
  ((node-object :initarg :object :initform nil :reader node-object)
   (node-parents :type sequence :initarg :node-parents :initform (make-array 0 :adjustable t :fill-pointer t) :accessor node-parents)
   (node-children :type sequence :initarg :node-children :initform (make-array 0 :adjustable t :fill-pointer t)
		  :reader node-children :writer %node-children-setter)
   (node-state :type <node-state> :initform nil :accessor node-state)
   (node-generation :type integer :initarg :generation :initform 0 :accessor node-generation))
  (:documentation
"
The class of nodes in tree controls. A tree node represents an object,
and is displayed as a text label accompanied by an icon. Tree nodes
are analagous to list items in a list control or table items in a
table control.

To the left of a tree node is a small plus or minus sign. If a plus
sign is displayed, this indicates that the node contains subnodes that
are currently not visible. If a minus sign is displayed, this
indicates either that the node does not contain any subnodes, or that
the subnodes are already visible.

The :parent-nodes and :child-nodes initargs let you specify any
parents and children that the node has.

The :object initarg specifies the object that is represented by the
tree node. For example, in the case of a file manager application, the
might be a directory on disk.
")
  (:metaclass <abstract-metaclass>))

#||
define protocol <<tree-control>> ()
  getter tree-control-roots
    (tree :: <tree-control>) => (roots :: <sequence>);
  setter tree-control-roots-setter
    (roots :: <sequence>, tree :: <tree-control>)
 => (roots :: <sequence>);
  getter tree-control-root-nodes
    (tree :: <tree-control>) => (nodes :: false-or(<sequence>));
  setter tree-control-root-nodes-setter
    (nodes :: false-or(<sequence>), tree :: <tree-control>)
 => (nodes :: false-or(<sequence>));
  function note-tree-control-roots-changed
    (tree :: <tree-control>, #key value) => ();
  getter tree-control-expanded-objects
    (tree :: <tree-control>)
 => (objects :: <sequence>, depth :: <integer>);
  setter tree-control-expanded-objects-setter
    (objects :: <sequence>, tree :: <tree-control>, #key depth)
 => (objects :: <sequence>);
  getter tree-control-expanded-object-count
    (tree :: <tree-control>)
 => (count :: <integer>, depth :: <integer>);
  function make-node (tree-control, object, #key, #all-keys) => (node);
  function find-node (tree-control, object, #key, #all-keys) => (node);
  function add-node (tree-control, parent, node, #key after, setting-roots?) => ();
  function remove-node (tree-control, node) => ();
  function expand-node (tree-control, node) => ();
  function contract-node (tree-control, node) => ();
  function do-make-node (tree-control, node-class, #key, #all-keys) => (node);
  function do-find-node (tree-control, object, #key, #all-keys) => (node);
  function do-add-node (tree-control, parent, node, #key after) => ();
  function do-add-nodes (tree-control, parent, nodes :: <sequence>, #key after) => ();
  function do-remove-node (tree-control, node) => ();
  function do-expand-node (tree-control, node) => ();
  function do-contract-node (tree-control, node) => ();
  // Node state changed notification callback
  getter tree-node-state-changed-callback
    (tree :: <tree-control>) => (callback :: <callback-type>);
  setter tree-node-state-changed-callback-setter
    (callback :: <callback-type>, tree :: <tree-control>)
 => (callback :: <callback-type>);
end protocol <<tree-control>>;
||#
#||
(define-protocol <<tree-control>> ()
  (:getter tree-control-roots (tree) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:setter (setf tree-control-roots) (roots tree) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:getter tree-control-root-nodes (tree) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:setter (setf tree-control-root-nodes) (nodes tree) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:function note-tree-control-roots-changed (tree &key value) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:getter tree-control-expanded-objects (tree) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:setter (setf tree-control-expanded-objects) (objects tree &key depth) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:getter tree-control-expanded-object-count (tree) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:function make-node (tree-control object &key &allow-other-keys) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:function find-node (tree-control object &key &allow-other-keys) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:function add-node (tree-control parent node &key after setting-roots?) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:function remove-node (tree-control node) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:function expand-node (tree-control node) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:function contract-node (tree-control node) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:function do-make-node (tree-control node-class &key &allow-other-keys) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:function do-find-node (tree-control object &key &allow-other-keys) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:function do-add-node (tree-control parent node &key after) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:function do-add-nodes (tree-control parent nodes &key after) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:function do-remove-node (tree-control node) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:function do-expand-node (tree-control node) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:function do-contract-node (tree-control node) (:documentation " Defined by: <<TREE-CONTROL>> "))
  ;; Node state changed notification callback
  (:getter tree-node-state-changed-callback (tree) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:setter (setf tree-node-state-changed-callback) (callback tree) (:documentation " Defined by: <<TREE-CONTROL>> ")))
||#
#||
define protocol <<tree-node>> ()
  getter node-object
    (node :: <tree-node>) => (object);
  getter node-parents
    (node :: <tree-node>) => (parents :: <sequence>);
  setter node-parents-setter
    (parents :: <sequence>, node :: <tree-node>) => (parents :: <sequence>);
  getter node-children
    (node :: <tree-node>) => (children :: <sequence>);
  setter node-children-setter
    (children :: <sequence>, node :: <tree-node>) => (children :: <sequence>);
  getter node-label
    (node :: <tree-node>) => (label :: false-or(<string>));
  setter node-label-setter
    (label :: false-or(<string>), node :: <tree-node>) => (label :: false-or(<string>));
  getter node-icon
    (node :: <tree-node>) => (icon :: false-or(<image>));
  setter node-icon-setter
    (icon :: false-or(<image>), node :: <tree-node>) => (icon :: false-or(<image>));
end protocol <<tree-node>>;
||#
#||
(define-protocol <<tree-node>> ()
  (:getter node-object (node) (:documentation " Defined by: <<TREE-NODE>> "))
  (:getter node-parents (node) (:documentation " Defined by: <<TREE-NODE>> "))
  (:setter (setf node-parents) (parents node) (:documentation " Defined by: <<TREE-NODE>> "))
  (:getter node-children (node) (:documentation " Defined by: <<TREE-NODE>> "))
  (:setter (setf node-children) (children node) (:documentation " Defined by: <<TREE-NODE>> "))
  (:getter node-label (node) (:documentation " Defined by: <<TREE-NODE>> "))
  (:setter (setf node-label) (label node) (:documentation " Defined by: <<TREE-NODE>> "))
  (:getter node-icon (node) (:documentation " Defined by: <<TREE-NODE>> "))
  (:setter (setf node-icon) (icon node) (:documentation " Defined by: <<TREE-NODE>> ")))
||#

#||
// For convenience...
define sealed inline method item-object
    (node :: <tree-node>) => (object)
  node-object(node)
end method item-object;
||#

(defmethod item-object ((node <tree-node>))
  (node-object node))


#||
define function default-children-predicate (node) => (true? :: <boolean>)
  ignore(node);
  #t
end function default-children-predicate;
||#

(defun default-children-predicate (node)
  (declare (ignore node))
  t)


#||
define method tree-control-roots-setter
    (roots :: <sequence>, tree :: <tree-control>)
 => (roots :: <sequence>)
  let value = gadget-value(tree);
  tree.%roots := as(<stretchy-vector>, roots);
  note-tree-control-roots-changed(tree, value: value);
  roots
end method tree-control-roots-setter;
||#

(defmethod (setf tree-control-roots) ((roots sequence) (tree <tree-control>))
  (let ((value (gadget-value tree)))
    (%roots-setter (MAKE-STRETCHY-VECTOR :contents roots) tree)
    (note-tree-control-roots-changed tree :value value))
  roots)


#||
define method note-tree-control-roots-changed
     (tree :: <tree-control>, #key value = $unsupplied) => ()
  ignore(value);
  // The back end needs to fill these in...
  gadget-items(tree).size := 0;
  tree-control-root-nodes(tree).size := 0;
end method note-tree-control-roots-changed;
||#

(defmethod note-tree-control-roots-changed ((tree <tree-control>) &key (value :unsupplied))
  (declare (ignore value))
  ;; The back end needs to fill these in...
  ;; FIXME: REUSE THE EXISTING STRETCHY RATHER THAN CONSING A NEW ONE - this will only
  ;; work if (gadget-items tree) and (tree-control-root-nodes tree) are in fact
  ;; vectors with fill pointers...
  (setf (fill-pointer (gadget-items tree)) 0)
;;  (%items-setter (make-array 0 :adjustable t :fill-pointer t) tree)
  (setf (fill-pointer (tree-control-root-nodes tree)) 0)
;;  (setf (tree-control-root-nodes tree) (make-array 0 :adjustable t :fill-pointer t))
  )


#||
define method gadget-items-setter
    (items :: <sequence>, gadget :: <tree-control>)
 => (items :: <sequence>)
  error("Use 'tree-control-roots-setter' to set the items of a tree control");
  items
end method gadget-items-setter;
||#

(defmethod (setf gadget-items) ((items sequence) (gadget <tree-control>))
  (error "Use 'tree-control-roots-setter' to set the items of a tree control"))


#||
/// Gadget state

define method update-gadget
    (tree :: <tree-control>) => ()
  let state = gadget-state(tree);
  tree-control-roots(tree) := #[];
  gadget-state(tree) := state
end method update-gadget;
||#

(defmethod update-gadget ((tree <tree-control>))
  (let ((state (gadget-state tree)))
    (setf (tree-control-roots tree) (MAKE-STRETCHY-VECTOR))
    (setf (gadget-state tree) state)))


#||
define method tree-control-expanded-objects
    (tree :: <tree-control>)
 => (objects :: <stretchy-vector>, depth :: <integer>)
  let objects :: <stretchy-object-vector> = make(<stretchy-vector>);
  let depth :: <integer> = 0;
  local method walk-nodes (node :: <tree-node>) => ()
	  when (node-state(node) == #"expanded")
	    add!(objects, node-object(node));
	    max!(depth, node-generation(node));
	    do(walk-nodes, node-children(node))
	  end
	end method;
  do(walk-nodes, tree-control-root-nodes(tree));
  values(objects, depth)
end method tree-control-expanded-objects;
||#

(defmethod tree-control-expanded-objects ((tree <tree-control>))
  (let ((objects (MAKE-STRETCHY-VECTOR))
	(depth 0))
    (labels ((walk-nodes (node)
	         (when (eql (node-state node) :expanded)
		   (add! objects (node-object node))
		   (setf depth (max depth (node-generation node)))
		   (map nil #'walk-nodes (node-children node)))))
      (map nil #'walk-nodes (tree-control-root-nodes tree))
      (values objects depth))))


#||
define method tree-control-expanded-objects-setter
    (objects :: <sequence>, tree :: <tree-control>, #key depth = 1)
 => (objects :: <sequence>)
  local method expand-one (node :: <tree-node>) => ()
	  when (member?(node-object(node), objects, test: gadget-test(tree)))
	    expand-node(tree, node);
	    when (node-generation(node) <= depth)
	      do(expand-one, node-children(node))
	    end
	  end
	end method;
  do(expand-one, tree-control-root-nodes(tree));
  objects
end method tree-control-expanded-objects-setter;
||#

(defmethod (setf tree-control-expanded-objects) ((objects sequence) (tree <tree-control>) &key (depth 1))
  (labels ((expand-one (node)
               (when (member (node-object node) objects :test (gadget-test tree))
		 (expand-node tree node)
		 (when (<= (node-generation node) depth)
		   (map nil #'expand-one (node-children node))))))
    (map nil #'expand-one (tree-control-root-nodes tree))
    objects))


#||
define method tree-control-expanded-object-count
    (tree :: <tree-control>)
 => (count :: <integer>, depth :: <integer>)
  let count :: <integer> = 0;
  let depth :: <integer> = 0;
  local method walk-nodes (node :: <tree-node>) => ()
	  when (node-state(node) == #"expanded")
	    inc!(count, 1);
	    max!(depth, node-generation(node));
	    do(walk-nodes, node-children(node))
	  end
	end method;
  do(walk-nodes, tree-control-root-nodes(tree));
  values(count, depth)
end method tree-control-expanded-object-count;
||#

(defmethod tree-control-expanded-object-count ((tree <tree-control>))
  (let ((count 0)
	(depth 0))
    (labels ((walk-nodes (node)
	         (when (eql (node-state node) :expanded)
		   (incf count)
		   (setf depth (max depth (node-generation node)))
		   (map nil #'walk-nodes (node-children node)))))
      (map nil #'walk-nodes (tree-control-root-nodes tree))
      (values count depth))))


#||
define sealed class <tree-control-state> (<value-gadget-state>)
  sealed constant slot %state-roots :: <sequence>,
    required-init-keyword: roots:;
  sealed constant slot %state-expanded-objects :: <sequence>,
    required-init-keyword: expanded-objects:;
  sealed constant slot %state-expanded-depth :: <integer>,
    required-init-keyword: expanded-depth:;
end class <tree-control-state>;

define sealed domain make (singleton(<tree-control-state>));
define sealed domain initialize (<tree-control-state>);
||#

(defclass <tree-control-state> (<value-gadget-state>)
  ((%state-roots :type sequence :initarg :roots :initform (required-slot ":roots" "<tree-control-state>") :reader %state-roots)
   (%state-expanded-objects :type sequence :initarg :expanded-objects :initform (required-slot ":expanded-objects" "<tree-control-state>")
			    :reader %state-expanded-objects)
   (%state-expanded-depth :type integer :initarg :expanded-depth :initform (required-slot ":expanded-depth" "<tree-control-state>")
			  :reader %state-expanded-depth)))


#||
define method gadget-state
    (tree :: <tree-control>) => (state :: <tree-control-state>)
  let (objects, depth) = tree-control-expanded-objects(tree);
  make(<tree-control-state>,
       value: gadget-value(tree),
       roots: tree-control-roots(tree),
       expanded-objects: objects,
       expanded-depth: depth)
end method gadget-state;
||#

(defmethod gadget-state ((tree <tree-control>))
  (multiple-value-bind (objects depth)
      (tree-control-expanded-objects tree)
    (make-instance '<tree-control-state>
		   :value (gadget-value tree)
		   :roots (tree-control-roots tree)
		   :expanded-objects objects
		   :expanded-depth depth)))


#||
define method gadget-state-setter
    (state :: <tree-control-state>, tree :: <tree-control>)
 => (state :: <tree-control-state>)
  with-busy-cursor (tree)
    tree-control-roots(tree) := state.%state-roots;
    tree-control-expanded-objects(tree, depth: state.%state-expanded-depth)
      := state.%state-expanded-objects;
    next-method()
  end
end method gadget-state-setter;
||#

(defmethod (setf gadget-state) ((state <tree-control-state>) (tree <tree-control>))
  (with-busy-cursor (tree)
    (setf (tree-control-roots tree) (%state-roots state))
    (setf (tree-control-expanded-objects tree :depth (%state-expanded-depth state))
	  (%state-expanded-objects state))
    (call-next-method)))


#||
/// Tree controls nodes

define sealed method make-node
    (tree :: <tree-control>, object, #rest initargs, #key)
 => (node :: <tree-node>)
  apply(do-make-node, tree, <tree-node>, object: object, initargs)
end method make-node;
||#

(defmethod make-node ((tree <tree-control>) object &rest initargs &key)
  (apply #'do-make-node tree (find-class '<tree-node>) :object object initargs))


#||
define sealed method find-node
    (tree :: <tree-control>, object, #key node: parent-node)
 => (node :: false-or(<tree-node>))
  do-find-node(tree, object, node: parent-node)
end method find-node;
||#

(defmethod find-node ((tree <tree-control>) object &key node)
  (let ((parent-node node))
    (do-find-node tree object :node parent-node)))


#||
// AFTER indicates which root to place the new root after
define sealed method add-node
    (tree :: <tree-control>, parent :: <tree-control>, node :: <tree-node>,
     #key after, setting-roots?) => ()
  let roots      = tree-control-roots(tree);
  let root-nodes = tree-control-root-nodes(tree);
  node-generation(node) := 0;
  node-parents(node) := #[];
  add!(gadget-items(tree), node-object(node));
  do-add-node(tree, parent, node, after: after);
  let index = after & position(root-nodes, after);
  // Insert the new node into the set of roots
  unless (setting-roots?)	//--- I'm ashamed...
    insert-at!(roots, node-object(node), index | #"end")
  end;
  insert-at!(root-nodes, node, index | #"end")
end method add-node;
||#

(defmethod add-node ((tree <tree-control>) (parent <tree-control>) (node <tree-node>)
		     &key after setting-roots?)
  (let ((roots      (tree-control-roots tree))
	(root-nodes (tree-control-root-nodes tree)))
    (setf (node-generation node) 0)
    (setf (node-parents node) (MAKE-STRETCHY-VECTOR))
    (add! (gadget-items tree) (node-object node))  ;; FIXME: MAKE SURE (GADGET-ITEMS TREE) IS STRETCHY!
    (do-add-node tree parent node :after after)
    (let ((index (and after (position root-nodes after))))
      ;; Insert the new node into the set of roots
      (unless setting-roots?	;;--- I'm ashamed...
	(insert-at! roots (node-object node) (or index :end)))
      (insert-at! root-nodes node (or index :end)))))


#||
// AFTER indicates which of NODE's children to place the new node after
define sealed method add-node
    (tree :: <tree-control>, parent :: <tree-node>, node :: <tree-node>,
     #key after, setting-roots?) => ()
  ignore(setting-roots?);
  let children = node-children(parent);
  node-generation(node) := node-generation(parent) + 1;
  node-parents(node) := make(<stretchy-vector>, size: 1, fill: parent);
  add!(gadget-items(tree), node-object(node));
  do-add-node(tree, parent, node, after: after);
  let index = after & position(children, after);
  insert-at!(children, node, index | #"end");
  node-state(parent) := node-state(parent) | #"contracted"
end method add-node;
||#

(defmethod add-node ((tree <tree-control>) (parent <tree-node>) (node <tree-node>)
		     &key after setting-roots?)
  (declare (ignore setting-roots?))
  ;; FIXME: Children is a SEQUENCE; can't rely on being able to mutate
  ;; it with INSERT-AT! without destroying the sequence structure.
  ;; SHOULD WE MAKE THEM MUTABLE ARRAYS?
  ;; FIXME: MAKE THE NODE CHILDREN BE A STRETCHY
  (let ((children (node-children parent)))
    (setf (node-generation node) (+ (node-generation parent) 1))
    (setf (node-parents node) (make-array 1 :initial-element parent))
    ;; FIXME: MAKE SURE (GADGET-ITEMS TREE) IS STRETCHY
    (add! (gadget-items tree) (node-object node))
    (do-add-node tree parent node :after after)
    (let ((index (and after (position children after))))
      ;; FIXME: MAKE SURE CHILDREN IS STRETCHY
      (insert-at! children node (or index :end)))
    (setf (node-state parent) (or (node-state parent) :contracted))))


#||
define sealed method remove-node
    (tree :: <tree-control>, node :: <tree-node>) => ()
  let roots      = tree-control-roots(tree);
  let root-nodes = tree-control-root-nodes(tree);
  remove!(gadget-items(tree), node-object(node));
  remove!(roots, node-object(node));	// just in case...
  remove!(root-nodes, node);
  do-remove-node(tree, node)
end method remove-node;
||#

;; FIXME -- ALL THE SEQUENCES IN THE TREE CONTROL NEED TO BE ADJUSTABLE ARRAYS WITH
;; FILL POINTERS....

(defmethod remove-node ((tree <tree-control>) (node <tree-node>))
  (let ((roots      (tree-control-roots tree))
	(root-nodes (tree-control-root-nodes tree)))
    ;; FIXME: (GADGET-ITEMS TREE) returns a sequence, it needs to be a STRETCHY
    (%items-setter (delete (node-object node) (gadget-items tree)) tree)
    ;; ROOTS is a vector. We can't rely on DELETE to return the
    ;; same vector it was passed! XXX: fixme
    (%roots-setter (delete (node-object node) roots) tree)   ;; just in case...
    ;; ROOT-NODES is a vector...
    (setf (tree-control-root-nodes tree) (delete node root-nodes))
    (do-remove-node tree node)))


#||
define method node-children-setter
    (children :: <sequence>, node :: <tree-node>)
 => (children :: <sequence>)
  node-state(node) := #f;
  node.%node-children := as(<stretchy-vector>, children)
end method node-children-setter;
||#

(defmethod (setf node-children) ((children sequence) (node <tree-node>))
  (setf (node-state node) nil)
  ;; CHANGE STUFF LIKE "LENGTH"/"SIZE" TO BE "COLLECTION-SIZE" OR
  ;; "SEQUENCE-SIZE", ALSO AREF/ELT ETC TO "SEQUENCE-ELT" OR
  ;; "COLLECTION-ELT".
  (%node-children-setter (MAKE-STRETCHY-VECTOR :contents children) node))


#||
define sealed method expand-node
    (tree :: <tree-control>, node :: <tree-node>) => ()
  unless (node-state(node))
    with-busy-cursor (tree)
      // If no items have ever been added, do it now
      let children-predicate = tree-control-children-predicate(tree);
      when (children-predicate(node-object(node)))
	let children-generator = tree-control-children-generator(tree);  
	let objects = children-generator(node-object(node));
	let nodes = map-as(<simple-vector>,
			   method (object) make-node(tree, object) end, objects);
	do-add-nodes(tree, node, nodes)
      end;
      node-state(node) := #"contracted"
    end
  end;
  when (node-state(node) == #"contracted")
    node-state(node) := #"expanded";
    do-expand-node(tree, node)
  end
end method expand-node;
||#

(defmethod expand-node ((tree <tree-control>) (node <tree-node>))
  (unless (node-state node)
    (with-busy-cursor (tree)
      ;; If no items have ever been added, do it now
      (let ((children-predicate (tree-control-children-predicate tree)))
	(when (funcall children-predicate (node-object node))
	  (let* ((children-generator (tree-control-children-generator tree))
		 (objects (funcall children-generator (node-object node)))
		 (nodes (map 'vector
			     #'(lambda (object)
				 (make-node tree object))
			     objects)))
	    (do-add-nodes tree node nodes)))
	(setf (node-state node) :contracted))))
  (when (eql (node-state node) :contracted)
    (setf (node-state node) :expanded)
    (do-expand-node tree node)))


#||
define sealed method contract-node
    (tree :: <tree-control>, node :: <tree-node>) => ()
  when (node-state(node) == #"expanded")
    node-state(node) := #"contracted";
    do-contract-node(tree, node)
  end
end method contract-node;
||#

(defmethod contract-node ((tree <tree-control>) (node <tree-node>))
  (when (eql (node-state node) :expanded)
    (setf (node-state node) :contracted)
    (do-contract-node tree node)))


#||
define method ensure-node-visible
    (tree :: <tree-control>, node :: <tree-node>) => ()
  // The back-end is expected to fill this in
  #f
end method ensure-node-visible;
||#

(defmethod ensure-node-visible ((tree <tree-control>) (node <tree-node>))
  ;; The back-end is expected to fill this in
  nil)


#||
/// Node state changed callback

define sealed method execute-node-state-changed-callback
    (tree :: <tree-control>, client, id, node :: <tree-node>) => ()
  ignore(client, id);
  let callback = tree-node-state-changed-callback(tree);
  if (callback)
    execute-callback(tree, callback, tree, node)
  else
    do-execute-node-state-changed-callback(tree, client, id, node)
  end
end method execute-node-state-changed-callback;
||#

(defmethod execute-node-state-changed-callback ((tree <tree-control>) client id (node <tree-node>))
  (let ((callback (tree-node-state-changed-callback tree)))
    (if callback
	(execute-callback tree callback tree node)
	(do-execute-node-state-changed-callback tree client id node))))


#||
define method do-execute-node-state-changed-callback
    (tree :: <tree-control>, client, id, node :: <tree-node>) => ()
  ignore(client, id, node);
  #f
end method do-execute-node-state-changed-callback;
||#

(defmethod do-execute-node-state-changed-callback ((tree <tree-control>) client id (node <tree-node>))
  (declare (ignore client id node))
  nil)


#||
define sealed class <node-state-changed-gadget-event> (<gadget-event>)
  sealed constant slot event-tree-node,
    required-init-keyword: node:;
end class <node-state-changed-gadget-event>;

define sealed domain make (singleton(<node-state-changed-gadget-event>));
define sealed domain initialize (<node-state-changed-gadget-event>);
||#

(defclass <node-state-changed-gadget-event>
    (<gadget-event>)
  ((event-tree-node :initarg :node :initform (required-slot ":node" "<node-state-changed-gadget-event>")
		    :reader event-tree-node)))


#||
define sealed method handle-event
    (tree :: <tree-control>, event :: <node-state-changed-gadget-event>) => ()
  execute-node-state-changed-callback
    (tree, gadget-client(tree), gadget-id(tree), event-tree-node(event))
end method handle-event;
||#

(defmethod handle-event ((tree <tree-control>) (event <node-state-changed-gadget-event>))
  (execute-node-state-changed-callback tree (gadget-client tree) (gadget-id tree) (event-tree-node event)))


#||
define function distribute-node-state-changed-callback
    (tree :: <tree-control>, node :: <tree-node>) => ()
  distribute-event(port(tree),
		   make(<node-state-changed-gadget-event>,
			gadget: tree,
			node: node))
end function distribute-node-state-changed-callback;
||#

(defun distribute-node-state-changed-callback (tree node)
  (check-type tree <tree-control>)
  (check-type node <tree-node>)
  (distribute-event (port tree)
		    (make-instance '<node-state-changed-gadget-event>
				   :gadget tree
				   :node node)))

