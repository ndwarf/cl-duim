;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GADGETS-INTERNALS -*-
(in-package #:duim-gadgets-internals)

;;; The following are from 'gadget-mixins.lisp'

(define-protocol <<gadget-protocol>> ()
  (:getter gadget-enabled? (gadget)
	   (:documentation
"
Returns true if _gadget_ is enabled.

If the gadget is enabled, the user can interact with it in an
appropriate way. If the gadget is not enabled, then the user cannot
interact with it. For example, if a push button is not enabled, it
cannot be clicked, or if a check box is not enabled, its setting
cannot be switched on or off. Gadgets that are not enabled are
generally grayed out on the screen.

Example:

    (defparameter *gadget*
                  (contain (make-pane '<radio-box>
                                      :items
                                      (range :from 0 :to 20))))
    (gadget-enabled? *gadget*)

Defined by: <<gadget-protocol>>
"))
  (:setter (setf gadget-enabled?) (enabled? gadget)
	   (:documentation
"
Causes _gadget_ to become active (that is, available for input) or
inactive, by toggling its enabled state. If _enabled?_ is true, then
_gadget_ is enabled, otherwise _gadget_ is not enabled.

If the gadget is enabled, the user can interact with it in an
appropriate way. If the gadget is not enabled, then the user cannot
interact with it. For example, if a push button is not enabled, it
cannot be clicked, or if a check box is not enabled, its setting
cannot be switched on or off. Gadgets that are not enabled are
generally grayed out on the screen.

Example:

    (defparameter *gadget*
                  (contain (make-pane '<radio-box>
                                      :items
                                      (range :from 0 :to 20))))
    (setf (gadget-enabled? *gadget*) nil)

Defined by: <<gadget-protocol>>
"))
  (:function note-gadget-disabled (client gadget) (:documentation " Defined by: <<GADGET-PROTOCOL>> "))
  (:function note-gadget-enabled (client gadget) (:documentation " Defined by: <<GADGET-PROTOCOL>> "))
  (:getter gadget-id (gadget)
	   (:documentation
"
Returns the identifier of _gadget_. The identifier is typically a
simple Dylan object that uniquely identifies the gadget. For most
gadgets, it is usually not necessary. Making use of a gadget ID
provides you with an additional way of controlling execution of your
code, allowing you to create simple branching statements such as:

    (case gadget-id
      (:modify (do-modify))
      (:add    (do-add))
      (:remove (do-remove))
      (:done   (do-done)))

In the specific case of tab controls, it is more important that you
specify an ID. The gadget ID for a tab control is returned as the
gadget value for that tab control.

Example:

    (defparameter *gadget*
                  (contain (make-pane '<button> :id :test
                                      :label \"Test\")))
    (gadget-id *gadget*)

Defined by: <<gadget-protocol>>
"))
  (:setter (setf gadget-id) (id gadget)
	   (:documentation
"

Sets the identifier of _gadget_. The identifier is typically a simple
Dylan object that uniquely identifies the gadget. For most gadgets, it
is usually not necessary, though it does provide you with an
additional way of controlling execution of your code based on the
gadget returned.

In the specific case of tab controls, it is more important that you
specify an ID. The gadget ID for a tab control is returned as the
gadget value for that tab control.

Example:

    (defparameter *gadget*
                  (contain (make-pane '<button> :id :test
                                      :label \"Test\")))
    (setf (gadget-id *gadget*) :test-two)
    (gadget-id *gadget*)

Defined by: <<gadget-protocol>>
"))
  (:getter gadget-client (gadget)
	   (:documentation
"
Returns the client of _gadget_. The client is the gadget or frame that
_gadget_ should look to for callback information.

In any sheet hierarchy, the client is usually the immediate parent of
_gadget_. This often means that the client is a frame, but it can also
be another gadget. In the majority of cases, you need not be concerned
with the client of a gadget. However, rather like the gadget-id, you
are free to assign your own client to a given gadget whenever it is
necessary for your code.

In less obvious cases, the client may not be the immediate parent: for
example, in the case of a radio box, the client of each button in the
radio box is the radio box itself. At the implementation level, the
radio box is not the immediate parent of the buttons that it contains,
since there is an intervening layout object that arranges the buttons
within the box. See <action-gadget> for more details.

Gadget clients enable you to pass messages between the gadget and its
client when a callback is received.

Defined by: <<gadget-protocol>>
"))
  (:setter (setf gadget-client) (client gadget)
	   (:documentation
"
Sets the _client_ of the specified _gadget_.

The client is often a frame, but it could be another gaget (for
example, in the case of a push button that is contained in a radio
box, the client of the button could be the radio box). See
<action-gadget> for more details.

Gadget clients enable you to pass messages between the gadget and its
client when a callback is received.

Defined by: <<gadget-protocol>>
"))
  (:getter gadget-documentation (gadget)
	   (:documentation
"
Returns the documentation string for _gadget_.

The documentation string can be used to specify a short piece of
online help text describing the action performed by the gadget. This
text can then be displayed in a number of different ways. On Windows,
for example, the documentation for a menu button might be displayed in
the status bar of the application, and the documentation for a button
might be displayed as a tooltip (a piece of pop-up text that appears
next to the mouse pointer when the pointer is inside the region
occupied by the gadget).

You are strongly encouraged to supply documentation strings for
significant gadgets in your application. Because of the nature of
their presentation, you should keep them as short as possible.

Defined by: <<gadget-protocol>>
"))
  (:setter (setf gadget-documentation) (documentation gadget)
	   (:documentation
"
Sets the documentation string for _gadget_ to _documentation_.

The documentation string can be used to specify a short piece of
online help text describing the action performed by the gadget. This
text can then be displayed in a number of different ways. On Windows,
for example, the documentation for a menu button might be displayed in
the status bar of the application, and the documentation for a button
might be displayed as a tooltip (a piece of pop-up text that appears
next to the mouse pointer when the pointer is inside the region
occupied by the gadget).

You are strongly encouraged to supply documentation strings for
significant gadgets in your application. Because of the nature of
their presentation, you should keep them as short as possible.

Defined by: <<gadget-protocol>>
"))
  (:getter gadget-selection-mode (gadget)
	   (:documentation
"

Returns the selection mode for _gadget_. Typically, gadgets are either
single or multiple selection (that is, either only one item can be
selected at a time, or any number of items can be selected), or there
is no selection behaviour (items cannot be selected). Some gadgets,
such as list boxes and button boxes, can choose a selection mode at
initialization time using the :selection-mode initarg.

Example:

Create a radio box as follows:

    (defparameter *radio*
                  (contain (make-pane '<radio-box>
                                      :items (range :from 0 :to 5))))

The selection mode of the radio box is returned with:

    (gadget-selection-mode *radio*)

Defined by: <<gadget-protocol>>
"))
  (:getter gadget-default? (gadget)
	   (:documentation
"
Returns true if the specified gadget is the default gadget for the
frame it is part of.

It is generally useful to set a default gadget in a frame, or a
default menu if there is no suitable gadget.

When a default gadget is specified, using the default keyboard gesture
in the frame invokes the activate callback for the default gadget. The
default gesture is usually pressing the RETURN button.

Defined by: <<gadget-protocol>>
"))
  (:getter (setf gadget-default?) (default? gadget)
	   (:documentation
"

If _default?_ is true, _gadget_ becomes the default gadget for the
current frame. If _default?_ is NIL, _gadget_ is not the default
gadget for the current frame, regardless of any previous value the
'gadget-default?' slot may have had.

It is generally useful to set a default gadget in a frame, or a
default menu if there is no suitable gadget.

When a default gadget is specified, using the default keyboard gesture
in the frame invokes the activate callback for the default gadget. The
default gesture is usually pressing the RETURN button.

Defined by: <<gadget-protocol>>
"))
  (:getter gadget-read-only? (gadget)
	   (:documentation
"
Returns true if _gadget_ is read-only. The read-only attribute of a
gadget is of most use within text gadgets.

Defined by: <<gadget-protocol>>
"))
  (:getter gadget-slug-size (gadget)
	   (:documentation
"
Returns the slug size of _gadget_. The slug is the part of _gadget_
that can be dragged using the mouse. The value returned uses the same
units as those used for 'gadget-value-range'.

Note: The Microsoft Windows Interface Guidelines refer to the slug as
a \"scroll-box\", and the area in which the slug can slide as the
\"scroll-shaft\". You should be aware of this difference if you are
using those guidelines as a reference.

Defined by: <<gadget-protocol>>
"))
  (:getter (setf gadget-slug-size) (slug-size gadget)
	   (:documentation
"
Sets the slug size of _gadget_. The value should use the same units as
those used for 'gadget-value-range'.

Note: The Microsoft Windows Interface Guidelines refer to the slug as
a \"scroll-box\", and the area in which the slug can slide as the
\"scroll-shaft\". You should be aware of this difference if you are
using those guidelines as a reference.

Defined by: <<gadget-protocol>>
"))
  (:function note-gadget-slug-size-changed (gadget) (:documentation " Defined by: <<gadget-protocol>> "))
  ;; Orientation and labels
  (:getter gadget-orientation (gadget)
	   (:documentation
"
Returns the orientation of _gadget_: either horizontal or vertical.

Example:

The following code creates a vertical row of buttons:

    (defparameter *buttons*
                  (contain (make-pane '<button-box>
                                      :selection-mode :multiple
                                      :orientation :vertical
                                      :items
                                      (range :from 0 :to 5))))

The orientation can be returned as follows:

    (gadget-orientation *buttons*)

Defined by: <<gadget-protocol>>
"))
  (:getter gadget-label (gadget)
	   (:documentation
"
Returns the label for _gadget_.

Example:

    (defparameter *gadget*
                  (contain (make-pane '<button>
                                      :label \"Hello\")))
    (gadget-label *gadget*)

Defined by: <<gadget-protocol>>
"))
  (:setter (setf gadget-label) (label gadget)
	   (:documentation
"
Sets the label for _gadget_ to _label_. The _label_ must be nil, a
string, or an instance of <image>. Changing the label of a gadget may
result in invoking the layout protocol on the gadget and its ancestor
sheets, if the new label occupies a different amount of space than the
old label.

Example:

    (defparameter *gadget*
                  (contain (make-pane '<button> :label \"Hello\")))
    (setf (gadget-label *gadget*) \"Hello world\")

Defined by: <<gadget-protocol>>
"))
  (:function note-gadget-label-changed (gadget) (:documentation " Defined by: <<gadget-protocol>> "))
  (:function gadget-label-size (gadget &key do-newlines? do-tabs?) (:documentation " Defined by: <<gadget-protocol>> "))
  (:function draw-gadget-label (gadget medium x y
			       &key align-x align-y state
			       do-tabs? brush underline?) (:documentation " Defined by: <<gadget-protocol>> "))
  ;; Accelerators and mnemonics
  (:getter gadget-accelerator (gadget)
	   (:documentation
"
Returns the keyboard accelerator of the specified gadget. An
accelerator is a keyboard gesture that activates a gadget (that is, it
invokes the activate callback for the gadget) without needing to use
the mouse.

Accelerators are of most use with button gadgets, and in particular
menu button gadgets.

Defined by: <<gadget-protocol>>
"))
  (:setter (setf gadget-accelerator) (accelerator gadget)
	   (:documentation
"
Sets the keyboard accelerator of the specified gadget. An accelerator
is a keyboard gesture that invokes the activate callback of a gadget
without needing to use the mouse.

Accelerators are of most use with button gadgets, and in particular
menu button gadgets.

Defined by: <<gadget-protocol>>
"))
  (:getter defaulted-gadget-accelerator (framem gadget) (:documentation " Defined by: <<gadget-protocol>> "))
  (:getter gadget-mnemonic (gadget)
	   (:documentation
"
Returns the mnemonic for _gadget_. On Windows, the mnemonic is
displayed as an underlined character in the label of the gadget, and
pressing the key for that character activates the gadget or gives it
the focus.

Defined by: <<gadget-protocol>>
"))
  (:setter (setf gadget-mnemonic) (mnemonic gadget)
	   (:documentation
"
Sets the mnemonic for _gadget_ to _mnemonic_. On Windows, the mnemonic
is displayed as an underlined character in the label of the gadget,
and pressing the key for that character activates the gadget or gives
it the focus.

Defined by: <<gadget-protocol>>
"))
  (:getter defaulted-gadget-mnemonic (framem gadget) (:documentation " Defined by: <<gadget-protocol>> "))
  (:function compute-mnemonic-from-label (sheet label
                                                &key remove-ampersand?) (:documentation " Defined by: <<gadget-protocol>> "))
  (:getter gadget-scrolling? (gadget) (:documentation " Defined by: <<gadget-protocol>> "))
  (:getter gadget-scrolling-horizontally? (gadget)
	   (:documentation
"
Returns true if _gadget_ has an associated horizontal scroll bar, and
false otherwise.

Defined by: <<gadget-protocol>>
"))
  (:getter gadget-scrolling-vertically? (gadget)
	   (:documentation
"
Returns true if _gadget_ has an associated vertical scroll bar, and
false otherwise.

Defined by: <<gadget-protocol>>
"))
  (:getter gadget-lines (gadget) (:documentation " Defined by: <<gadget-protocol>> "))
  (:getter gadget-columns (gadget) (:documentation " Defined by: <<gadget-protocol>> "))
  (:getter gadget-state (gadget) (:documentation " Defined by: <<gadget-protocol>> "))
  (:setter (setf gadget-state) (state gadget) (:documentation " Defined by: <<gadget-protocol>> ")))

;; TODO: Where should the documentation for 'gadget-command' be put?
#||
Returns the command associated with _gadget_.

A command is typically associated with a gadget if that gadget has
been created using a command table. For example, the command
associated with a menu button would represent the callback that is
invoked when the user chooses the relevant menu command.
||#
;; TODO: Also for (setf gadget-command):
#||
Sets the command of the specified _gadget_.

A command is typically associated with a gadget if that gadget has
been created using a command table. For example, the command
associated with a menu button would represent the callback that is
invoked when the user chooses the relevant menu command.
||#

(define-protocol <<value-gadget-protocol>> (<<gadget-protocol>>)
  (:getter gadget-value (gadget)
	   (:documentation
"
Returns the gadget value of the specified gadget.

The interpretation of the value varies from gadget to gadget. Most
gadgets conceptually have \"raw\" values that can be determined
directly using the generic function appropriate to the gadget class
concerned (GADGET-TEXT for and instance of <TEXT-GADGET>,
GADGET-SELECTION for an instance of <COLLECTION-GADGET>, and so
on). These gadget classes also have a convenience method on
GADGET-VALUE that wraps up the raw value in some useful way. So, text
gadgets have a method on GADGET-VALUE that converts the GADGET-TEXT
based on the GADGET-VALUE-TYPE, for example converting the string to
an integer for :value-type (find-class 'integer).

The GADGET-VALUE method for collection gadgets is different for single
and multiple selection gadgets. For single selection, the item that is
selected is returned. For multiple selection, a sequence of the
selected items is returned.

Note: if the gadget ID has been specified for a tab control, then this
is returned as the gadget value.

Example:

Create a radio button:

    (defparameter *radio*
                  (contain (make-pane '<radio-button>
                                      :label \"Radio\")))

The gadget value of *radio* can be returned as follows:

    (gadget-value *radio*)

If the radio button is selected, GADGET-VALUE returns T. If not
selected, GADGET-VALUE returns NIL.

Defined by: <<VALUE-GADGET-PROTOCOL>>
"))
  (:setter (setf gadget-value) (value gadget &key do-callback?)
	   (:documentation
"
Sets the gadget value of _gadget_.

The _value_ that you need to specify varies from gadget to gadget. For
example, for a scroll bar, _value_ might be a number between 0 and 1,
while for a radio button, _value_ is either true or false.

If _do-callback?_ is true, the value-changed callback for _gadget_ is
invoked.

Example:

Create a radio button:

    (defparameter *radio*
                  (contain (make-pane '<radio-button>
                                      :label \"Radio\")))

The gadget value of *radio* can be set with either of the following:

    (setf (gadget-value *radio*) T)
    (setf (gadget-value *radio*) NIL)

Setting the gadget value to T selects the button, and setting it to
NIL deselects it.

Defined by: <<VALUE-GADGET-PROTOCOL>>
"))
  (:function do-gadget-value-setter (gadget normalized-value) (:documentation " Defined by: <<VALUE-GADGET-PROTOCOL>> "))
  (:getter gadget-value-type (gadget)
	   (:documentation
"
Returns the type of the gadget value for _gadget_.

Example:

The following code creates a text field, the contents of which are
constrained to be an integer.

    (defparameter *numeric*
                  (contain (make-pane '<text-field>
                                      :value-type
                                      (find-class 'integer))))

Evaluating the following code confirms the gadget value type to be the
class INTEGER.

    (gadget-value-type *numeric*)

Defined by: <<VALUE-GADGET-PROTOCOL>>
"))
  (:getter gadget-label-key (gadget)
	   (:documentation
"
Returns the function that is used to compute the labels for the items
in _gadget_. Using a label key can be a useful way of consistently
specifying labels that are a mapping of, but not directly equivalent
to, the item names. As shown in the example, it is possible to force
the case of item labels, and this is useful if the items are specified
as symbol names, rather than strings.

Example:

The following code creates a list box whose items are the lower-cased
equivalents of the symbols stated.

    (defparameter *gadget*
                  (contain
                    (make-pane '<list-box>
                               :items (list \"One\" \"Two\" \"Three\")
                               :label-key
                               #'(lambda (symbol)
                                   (string-downcase
                                     (symbol-name symbol))))))

The label key function can be returned as follows:

    (gadget-label-key *gadget*)

Defined by: <<VALUE-GADGET-PROTOCOL>>
"))
  (:getter gadget-value-key (gadget)
	   (:documentation
"
Returns the function that is used to calculate the gadget value of
_gadget_, given the selected items. The function takes an item and
returns a value.

Example:

The list box defined below has three items, each of which is a pair of
two symbols. A label-key and a value-key is defined such that the
label for each item is calculated from the first symbol in each pair,
and the gadget value is calculated from the second.

    (defparameter *list*
                  (contain (make-pane '<list-box>
                                      :items
                                      (list (list \"One\" :one)
                                            (list \"Two\" :two)
                                            (list \"Three\" :three))
                                      :label-key #'first
                                      :value-key #'second)))

This ensures that while the label of the first item is displayed
on-screen as 'One', the value returned from that item is :one, and
similarly for the other items in the list.

The gadget value key function can be returned with:

    (gadget-value-key *list*)

Defined by: <<VALUE-GADGET-PROTOCOL>>
"))
  (:function normalize-gadget-value (gadget value) (:documentation " Defined by: <<VALUE-GADGET-PROTOCOL>> "))
  (:function note-gadget-value-changed (gadget) (:documentation " Defined by: <<VALUE-GADGET-PROTOCOL>> "))
  (:function execute-value-changed-callback (gadget client id) (:documentation " Defined by: <<VALUE-GADGET-PROTOCOL>> "))
  (:function do-execute-value-changed-callback (gadget client id) (:documentation " Defined by: <<VALUE-GADGET-PROTOCOL>> "))
  (:getter gadget-value-changed-callback (gadget)
	   (:documentation
"
Returns the value-changed callback of _gadget_. This is the callback
function that is called once the gadget value of _gadget_ has been
changed.

The value-changed callback is invoked with one argument, the gadget.

If GADGET-VALUE-CHANGED-CALLBACK returns NIL, there is no value
changed callback for _gadget_.

Defined by: <<VALUE-GADGET-PROTOCOL>>
"))
  (:setter (setf gadget-value-changed-callback) (callback gadget)
	   (:documentation
"
Sets the value-changed callback of _gadget_ to _callback_. This is the
callback function that is called once the gadget value of _gadget_ has
been changed.

The value-changed callback function is invoked with one argument, the
gadget.

Defined by: <<VALUE-GADGET-PROTOCOL>>
"))
  (:getter gadget-value-range (gadget)
	   (:documentation
"
Returns the range of values for _gadget_. The value range is the
elements represented by the range specified for the gadget.

Note: the value range is not simply the difference between the maximum
and minimum values in the range. Consider the following range:

    (range :from 10 :to 0 :by -2)

In this case, the value range is the elements 10, 8, 6, 4, 2, 0.

The units in which the range is specified are also used for
GADGET-SLUG-SIZE.

TODO: Ensure the RANGE function will deal with the example range!

Example:

You can create a slider with a given range as follows:

    (defparameter *slider*
                  (contain (make-pane '<slider>
                                      :value-range
                                      (range :from -20
                                             :to 20
                                             :by 5))))

You can return the range of this gadget by executing the following:

    (gadget-value-range *slider*)

which in this case returns {range -20 through 20 by 5}.

Defined by: <<VALUE-GADGET-PROTOCOL>>
"))
  (:getter (setf gadget-value-range) (value-range gadget)
	   (:documentation
"
Sets the range of values for _gadget_. The value range is the elements
represented by the range specified for gadget.

Example:

Create a slider without specifying a range:

    (defparameter *slider* (contain (make-pane '<slider>)))

You can specify the range of this gadget by executing the following:

    (setf (gadget-value-range *slider*)
          (range :from -20 :to 20 :by 5))

TODO: Check this example works!

Defined by: <<VALUE-GADGET-PROTOCOL>>
"))
  (:function note-gadget-value-range-changed (gadget) (:documentation " Defined by: <<VALUE-GADGET-PROTOCOL>> "))
  (:getter gadget-start-value (gadget) (:documentation " Defined by: <<VALUE-GADGET-PROTOCOL>> "))
  (:getter gadget-end-value (gadget) (:documentation " Defined by: <<VALUE-GADGET-PROTOCOL>> "))
  (:getter gadget-value-increment (gadget) (:documentation " Defined by: <<VALUE-GADGET-PROTOCOL>> "))
  ;; This one is a little odd, but it is exported...
  (:getter button-gadget-value (button) (:documentation " Defined by: <<VALUE-GADGET-PROTOCOL>> ")))


(define-protocol <<changing-value-gadget-protocol>> ()
  (:getter gadget-value-changing-callback (gadget)
	   (:documentation
"
Returns the function that will be called when the value of _gadget_ is
in the process of changing, such as when a slider is being
dragged. The function will be invoked with two arguments, _gadget_ and
the new value.

Defined by: <<CHANGING-VALUE-GADGET-PROTOCOL>>
"))
  (:getter (setf gadget-value-changing-callback) (callback gadget)
	   (:documentation
"
Sets the function that will be called when the value of _gadget_ is in
the process of changing, such as when a slider is being dragged. The
_callback_ will be invoked with two arguments, _gadget_ and the new
value.

Defined by: <<CHANGING-VALUE-GADGET-PROTOCOL>>
"))
  (:function execute-value-changing-callback (gadget client id) (:documentation " Defined by: <<CHANGING-VALUE-GADGET-PROTOCOL>> "))
  (:function do-execute-value-changing-callback (gadget client id) (:documentation " Defined by: <<CHANGING-VALUE-GADGET-PROTOCOL>> ")))


(define-protocol <<action-gadget-protocol>> (<<gadget-protocol>>)
  (:function execute-activate-callback (gadget client id) (:documentation " Defined by: <<ACTION-GADGET-PROTOCOL>> "))
  (:function do-execute-activate-callback (gadget client id) (:documentation " Defined by: <<ACTION-GADGET-PROTOCOL>> "))
  (:getter gadget-activate-callback (gadget)
	   (:documentation
"
Returns the function that will be called when _gadget_ is
activated. This function will be invoked with one argument, the gadget
itself.

When this function returns NIL, this indicates that there is no
activate callback for the gadget.

Defined by: <<ACTION-GADGET-PROTOCOL>>
"))
  (:setter (setf gadget-activate-callback) (callback gadget)
	   (:documentation
"
Sets the activate callback for _gadget_ to _callback_. _callback_ may
be NIL to indicate that any existing activate callback for _gadget_
should be removed.

Defined by: <<ACTION-GADGET-PROTOCOL>>
")))


(define-protocol <<updatable-gadget-protocol>> (<<gadget-protocol>>)
  (:function update-gadget (gadget)
	     (:documentation
"
Forces _gadget_ to be redrawn. This can be useful if a number of
changes have been made which have not been reflected in the gadget
automatically (for example, by using pixmaps to perform image
operations).

Defined by: <<UPDATABLE-GADGET-PROTCOL>>
"))
  (:function execute-update-callback (gadget client id) (:documentation " Defined by: <<UPDATABLE-GADGET-PROTCOL>> "))
  (:function do-execute-update-callback (gadget client id) (:documentation " Defined by: <<UPDATABLE-GADGET-PROTCOL>> "))
  (:getter gadget-update-callback (gadget) (:documentation " Defined by: <<UPDATABLE-GADGET-PROTCOL>> "))
  (:setter (setf gadget-update-callback) (callback gadget) (:documentation " Defined by: <<UPDATABLE-GADGET-PROTCOL>> ")))


(define-protocol <<key-press-gadget-protocol>> (<<gadget-protocol>>)
  (:function execute-key-press-callback (gadget client id keysym &rest args
                                        &key &allow-other-keys)
	     (:documentation
"
Returns the key-press callback for _gadget_. The key-press callback is
the callback invoked when a key on the keyboard is pressed while the
gadget has focus. They are of most use in tab controls, list controls,
table controls, and tree controls.

In Windows, a good use for the key-press callback would be to mirror
the behaviour of Windows Explorer, where typing a filename, or part of
a filename, selects the first file in the current folder whose name
matches that typed.

Defined by: <<KEY-PRESS-GADGET-PROTOCOL>>
"))
  (:function do-execute-key-press-callback (gadget client id keysym &rest args
                                           &key &allow-other-keys) (:documentation " Defined by: <<KEY-PRESS-GADGET-PROTOCOL>> "))
  (:getter gadget-key-press-callback (gadget) (:documentation " Defined by: <<KEY-PRESS-GADGET-PROTOCOL>> "))
  (:setter (setf gadget-key-press-callback) (callback gadget)
	   (:documentation
"
Sets the key-press callback for _gadget_. The key-press callback is
the callback invoked when a key on the keyboard is pressed while the
gadget has focus. They are of most use in tab controls, list controls,
table controls, and tree controls.

In Windows, a good use for the key-press callback would be to mirror
the behaviour of Windows Explorer, where typing a filename, or part of
a filename, selects the first file in the current folder whose name
matches that typed.

Defined by: <<KEY-PRESS-GADGET-PROTOCOL>>
")))


(define-protocol <<popup-menu-gadget-protocol>> (<<gadget-protocol>>)
  (:function execute-popup-menu-callback (gadget client id target &rest args &key &allow-other-keys)
     (:documentation " Defined by: <<POPUP-MENU-GADGET-PROTOCOL>> "))
  (:function do-execute-popup-menu-callback (gadget client id target &rest args &key &allow-other-keys)
     (:documentation " Defined by: <<POPUP-MENU-GADGET-PROTOCOL>> "))
  (:getter gadget-popup-menu-callback (gadget)
	   (:documentation
"
Returns the popup menu callback of _gadget_. This is typically a
function that is used to create a context-sensitive menu of available
commands. It is generally invoked when the user right clicks on the
gadget.

Defined by: <<POPUP-MENU-GADGET-PROTOCOL>>
"))
  (:setter (setf gadget-popup-menu-callback) (callback gadget)
	   (:documentation
"
Sets the popup menu callback of _gadget_ to _callback_. The function
should typically create a menu of commands suited to the context in
which the function is called. The function is generally invoked by
right-clicking on the gadget.

Defined by: <<POPUP-MENU-GADGET-PROTOCOL>>
")))


(define-protocol <<sheet-scrolling-protocol>> ()
  (:function line-scroll-amount (sheet orientation) (:documentation " Defined by: <<SHEET-SCROLLING-PROTOCOL>> "))
  (:function page-scroll-amount (sheet orientation) (:documentation " Defined by: <<SHEET-SCROLLING-PROTOCOL>> "))
  (:function horizontal-line-scroll-amount (sheet) (:documentation " Defined by: <<SHEET-SCROLLING-PROTOCOL>> "))
  (:function vertical-line-scroll-amount (sheet) (:documentation " Defined by: <<SHEET-SCROLLING-PROTOCOL>> "))
  (:function sheet-scroll-range (sheet) (:documentation " Defined by: <<SHEET-SCROLLING-PROTOCOL>> "))
  (:function sheet-visible-range (sheet) (:documentation " Defined by: <<SHEET-SCROLLING-PROTOCOL>> "))
  (:function set-sheet-visible-range (sheet left top right bottom) (:documentation " Defined by: <<SHEET-SCROLLING-PROTOCOL>> "))
  ;; User-level scrolling functionality
  (:function scroll-position (sheet)
	     (:documentation
"
Returns the position of the scroll bar slug in _sheet_. Note that this
generic function only returns the position of scroll bar slugs that
have been created using the SCROLLING macro. It does not work on
gadgets with scroll bars defined explicitly.

Note: the Microsoft Windows Interface Guidelines refer to the slug as
a 'scroll-box', and the area in which the slug can slide as the
'scroll-shaft'. You should be aware of this difference if you are
using those guidelines as a reference.

Defined by: <<SHEET-SCROLLING-PROTOCOL>>
"))
  (:function set-scroll-position (sheet x y)
	     (:documentation
"
Scrolls the window on _sheet_ by setting the position of the scroll
bar slug. Note that this generic function only sets the position of
scroll bar slugs that have been created using the SCROLLING macro. It
does not work on gadgets with scroll bars defined explicitly.

Note: the Microsoft Windows Interface Guidelines refer to the slug as
a 'scroll-box', and the area in which the slug can slide as the
'scroll-shaft'. You should be aware of this difference if you are
using those guidelines as a reference.

Defined by: <<SHEET-SCROLLING-PROTOCOL>>
"))
  ;; For back-end tinkering...
  (:function gadget-supplies-scroll-bars? (framem gadget &key &allow-other-keys)
      (:documentation " Defined by: <<SHEET-SCROLLING-PROTOCOL>> ")))


;;; The following are from 'table-controls.lisp'

(define-protocol <<table-control>> (<<list-control>>)
  (:function add-column (control column index)
	     #-(and)
	     (:documentation
"
Adds a column to _table_, with a table heading given by _heading_. The
contents of the column are generated by calling the _generator_
function on the item for each row of _table_. The _index_ specifies
where in the column order the new column should be added.

FIXME: This description isn't right for this GF definition!

Defined by: <<TABLE-CONTROL>> protocol
"))
  (:function remove-column (control index)
	     (:documentation
"
Removes a column from _control_.

Defined by: <<TABLE-CONTROL>> protocol
"))
  (:function do-add-column (control column index) (:documentation " Defined by: <<TABLE-CONTROL>> protocol "))
  (:function do-remove-column (control index) (:documentation " Defined by: <<TABLE-CONTROL>> protocol "))
  ;; Views, etc
  (:getter table-control-view (table-control)
	   (:documentation
"
Returns the current view of _table-control_. The available views are
described in the entry of <table-control-view>.

Defined by: <<TABLE-PROTOCOL>>
"))
  (:setter (setf table-control-view) (view table-control)
	   (:documentation
"
Sets the current view of _table-control_.

The _view_ argument is used to specify the way in which the items in
the table control are displayed.

Defined by: <<TABLE-PROTOCOL>>
")))


;;; The following are from 'tree-controls.lisp'

(define-protocol <<tree-control>> ()
  (:getter tree-control-roots (tree)
	   (:documentation
"
Returns the roots of _tree-control_.

Example:

Create a tree control as follows:

    (defparameter *tree*
                  (contain (make-pane '<tree-control>
                                      :roots '(1 2 3)
                                      :children-generator
                                      #'(lambda (x)
                                          (vector x (1+ x))))))

You can return the roots of this tree control as follows:

    (tree-control-roots *tree*)

Defined by: <<TREE-CONTROL>>
"))
  (:setter (setf tree-control-roots) (roots tree)
	   (:documentation
"
Sets the roots of _tree_.

Example:

Create a tree control without specifying any roots as follows:

    (defparameter *tree*
                  (contain (make-pane '<tree-control>
                                      :children-generator
                                      #'(lambda (x)
                                          (vector x (1+ x))))))

You can set the roots of this tree control as follows:

    (setf (tree-control-roots *tree*) '(1 2 3))

The tree control is updated on the screen to reflect this change.

Defined by: <<TREE-CONTROL>>
"))
  (:getter tree-control-root-nodes (tree) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:setter (setf tree-control-root-nodes) (nodes tree) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:function note-tree-control-roots-changed (tree &key value) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:getter tree-control-expanded-objects (tree) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:setter (setf tree-control-expanded-objects) (objects tree &key depth) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:getter tree-control-expanded-object-count (tree) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:function make-node (tree-control object &key &allow-other-keys)
	     (:documentation
"
Creates a node that represents _object_ which can be inserted in the
specified _tree-control_. To insert the item in the tree control,
ADD-NODE is used. You would not normally call MAKE-NODE explicitly;
just use ADD-NODE and the node is created automatically before it is
added to the tree control.

Defined by: <<TREE-CONTROL>>
"))
  (:function find-node (tree-control object &key &allow-other-keys)
	     (:documentation
"
Finds the item in a tree control that corresponds to _object_.

Defined by: <<TREE-CONTROL>>
"))
  (:function add-node (tree-control parent node &key after setting-roots?)
	     (:documentation
"
Adds _node_ to the specified _tree-control_ with the specified
_parent_. The new item is created via a call to MAKE-NODE.

The _after_ argument indicates which existing node to place the new
node after. If _setting-roots?_ is T, then the new node is added at
the root of _tree-control_.

Defined by: <<TREE-CONTROL>>
"))
  (:function remove-node (tree-control node)
	     (:documentation
"
Removes _node_ from _tree-control_.

Defined by: <<TREE-CONTROL>>
"))
  (:function expand-node (tree-control node)
	     (:documentation
"
Expands the specified node in a _tree-control_, thereby displaying any
children that the node has.

If no children have been explicitly added to the node before it is
expanded, they are generated by calling the tree's children generating
function on the node.

Defined by: <<TREE-CONTROL>>
"))
  (:function contract-node (tree-control node)
	     (:documentation
"
Contracts the specified _node_ in _tree-control_, thereby hiding any
children of the node that were displayed.

Defined by: <<TREE-CONTROL>>
"))
  (:function do-make-node (tree-control node-class &key &allow-other-keys) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:function do-find-node (tree-control object &key &allow-other-keys) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:function do-add-node (tree-control parent node &key after) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:function do-add-nodes (tree-control parent nodes &key after) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:function do-remove-node (tree-control node) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:function do-expand-node (tree-control node) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:function do-contract-node (tree-control node) (:documentation " Defined by: <<TREE-CONTROL>> "))
  ;; Node state changed notification callback
  (:getter tree-node-state-changed-callback (tree) (:documentation " Defined by: <<TREE-CONTROL>> "))
  (:setter (setf tree-node-state-changed-callback) (callback tree) (:documentation " Defined by: <<TREE-CONTROL>> ")))


(define-protocol <<tree-node>> ()
  (:getter node-object (node)
	   (:documentation
"
Returns the object that _node_ represents.

Defined by: <<TREE-NODE>>
"))
  (:getter node-parents (node)
	   (:documentation
"
Returns the parents of _node_ in a tree control.

Defined by: <<TREE-NODE>>
"))
  (:setter (setf node-parents) (parents node) (:documentation " Defined by: <<TREE-NODE>> "))
  (:getter node-children (node)
	   (:documentation
"
Returns the children of _node_ in a tree control.

Defined by: <<TREE-NODE>>
"))
  (:setter (setf node-children) (children node)
	   (:documentation
"
Sets the children of _node_ in a tree control.

Defined by: <<TREE-NODE>>
"))
  (:getter node-label (node) (:documentation " Defined by: <<TREE-NODE>> "))
  (:setter (setf node-label) (label node) (:documentation " Defined by: <<TREE-NODE>> "))
  (:getter node-icon (node) (:documentation " Defined by: <<TREE-NODE>> "))
  (:setter (setf node-icon) (icon node) (:documentation " Defined by: <<TREE-NODE>> ")))

;;; TODO: Where is 'node-state'?
#||
Returns the state of _tree-node_ in a tree control, that is, whether
it is currently expanded or contracted. This function returns NIL if
_tree-node_ does not exist.
||#

;;; TODO: Where is 'node-expanded?'?
#||
Returns true if _tree-node_ is expanded in a tree control, so that its
children are displayed in the tree control.
||#

;;; The following are from 'active-labels.lisp'

(define-protocol <<active-label>> ()
  (:function handle-semantic-event (frame gadget object event
                                          &key button modifiers x y))
  (:function handle-semantic-button-event (frame gadget object event button
                                                 &key modifiers x y)))


;;; The following are from 'collection-gadgets.lisp'

(define-protocol <<collection-gadget-protocol>> ()
  (:getter gadget-items (gadget)
	   (:documentation
"
Returns the items for _gadget_. The items of any collection gadget is
the collection of items that the collection gadget contains. In a list
box, for example, the items are the list items themselves.

Example:

The following code creates a list box whose items are the lower-cased
equivalents of the symbols stated. Note that the label key for a
gadget is a function that computes the label for the items in that
gadget.

    (defparameter *gadget*
                  (contain
                    (make-pane '<list-box>
                               :items (list \"One\" \"Two\" \"Three\")
                               :label-key
                               #'(lambda (symbol)
                                   (string-downcase
                                     (symbol-name symbol))))))

You can return the items in the gadget as follows:

    (gadget-items *gadget*)

This returns the value (\"one\" \"two\" \"three\").

Defined by: <<COLLECTION-GADGET-PROTOCOL>>
"))
  (:getter (setf gadget-items) (items gadget)
	   (:documentation
"
Sets the items for _gadget_ to the items specified by _items_.

Example:

    (defparameter *gadget*
                  (contain (make-pane '<radio-box>
                                      :items
                                      (range :from 0 :to 20))))
    (setf (gadget-items *gadget*) (range :from 0 :to 15))

Defined by: <<COLLECTION-GADGET-PROTOCOL>>
"))
  (:function note-gadget-items-changed (gadget))
  (:getter gadget-test (gadget)
	   (:documentation
"
Returns the test function for the specified gadget. This function is
used to test whether two items of the collection are considered
identical.

Defined by: <<COLLECTION-GADGET-PROTOCOL>>
"))
  (:getter gadget-selection (gadget)
	   (:documentation
"
Returns the keys for the currently selected items of
_gadget_. Generally, you should use GADGET-VALUE to return the
selected item, rather than GADGET-SELECTION, which is best used for
handling repeated items.

Single selection gadgets (such as radio boxes) always have exactly one
key selected. Multiple selection gadgets (such as check boxes) have
zero or more keys selected. The value of a collection gadget is
determined by calling the value key of the gadget on each selected
item in that gadget.

Example:

Create a radio box as follows:

    (defparameter *radio*
                  (contain (make-pane '<radio-box>
                                      :items (range :from 0 :to 5))))

Select one of the items in the radio box. This selection can be
returned with:

    (gadget-selection *radio*)

Defined by: <<COLLECTION-GADGET-PROTOCOL>>
"))
  (:getter (setf gadget-selection) (selection-specifier gadget &key do-callback?)
	   (:documentation
"
Sets the selection of _gadget_. When setting the selection, you need
be wary of the selection mode for _gadget_. It is an error to try to
set multiple items in a single selection mode gadget.

If _do-callback?_ is true, the selection changed callback for _gadget_
is invoked.

As with GADGET-SELECTION, you should usually use (SETF GADGET-VALUE)
to set the selected item, rather than (SETF GADGET-SELECTION), which
is best used for handling repeated items. See GADGET-SELECTION for
more details.

Example:

Create a radio box as follows:

    (defparameter *radio*
                  (contain (make-pane '<radio-box>
                                      :items (range :from 0 :to 5))))

You can select the third item with:

    (setf (gadget-selection *radio* :do-callback? t) #(3))

This sets the appropriate item, and invokes the callback that would
have been invoked had the item been set manually, rather than
programmatically (assuming that such a callback has been defined).

Defined by: <<COLLECTION-GADGET-PROTOCOL>>
"))
  (:function note-gadget-selection-changed (gadget)))


;;; The following are from 'graph-controls.lisp'

(define-protocol <<graph-node>> (<<tree-node>>)
  (:getter node-x (node))
  (:getter (setf node-x) (x node))
  (:getter node-y (node))
  (:getter (setf node-y) (y node)))


(define-protocol <<graph-edge>> ()
  (:getter draw-edges (edge medium region)))


;;; The following are from 'list-controls.lisp'

(define-protocol <<item>> ()
  (:getter item-object (item)
	   (:documentation
"
Returns the Dylan object representing an item in a list or table
control.

Defined by: <<ITEM>>
"))
  (:getter item-label (item))
  (:setter (setf item-label) (label item))
  (:getter item-icon (item))
  (:setter (setf item-icon) (icon item)))


(define-protocol <<list-control>> ()
  (:function make-item (control object &key &allow-other-keys)
	     (:documentation
"
Creates an item that represents _object_ which can be inserted in the
specified list or table. To insert the item in the list control or
table control, ADD-ITEM is used. You would not normally call MAKE-ITEM
explicitly; just use ADD-ITEM and the item is created automatically
before it is added to the list or table control.

If the _frame-manager_ argument is specified, then this is used
instead of the default frame manager. (?)

Defined by: <<LIST-CONTROL>>
"))
  (:function find-item (control object &key &allow-other-keys)
	     (:documentation
"
Finds the item in a list control or a table control that corresponds
to _object_.

Defined by: <<LIST-CONTROL>>
"))
  (:function add-item (control item &key after)
	     (:documentation
"
Adds _item_ to the specified _list-or-table_. The new item is created
via a call to MAKE-ITEM.

The _after_ argument indicates which existing item to place the new
item after.

Defined by: <<LIST-CONTROL>>
"))
  (:function remove-item (control item)
	     (:documentation
"
Removes _item_ from _control_.

Defined by: <<LIST-CONTROL>>
"))
  ;; Back-end function to make and find an item
  (:function do-make-item (control item-class &key &allow-other-keys))
  (:function do-find-item (control object &key &allow-other-keys))
  ;; The concrete implementation should add the list item to the
  ;; list control, and then re-layout and redisplay the list control
  (:function do-add-item (control item &key after))
  ;; The concrete implementation should remove the list item from the
  ;; list control, and then re-layout and redisplay the list control
  (:function do-remove-item (control item))
  ;; Views, etc
  (:getter list-control-view (list-control))
  (:setter (setf list-control-view) (view list-control)
	   (:documentation
"
Sets the view for _list-control_. The view defines how items in the
list control are displayed. Three views are available; items are
accompanied either by a small icon or a large icon. In addition, items
can be listed vertically, and additional details can be displayed for
each item. For more details, see the description for
<LIST-CONTROL-VIEW>.

Example:

Given a list control created with the following code:

    (defparameter *list*
                  (contain (make-pane '<list-control>
                                      :items
                                      (list \"One\" \"Two\" \"Three\"))))

The list control view may be specified with:

    (setf (list-control-view *list*) :list)
")))


;;; The following are from 'scroll-bars.lisp'

(define-protocol <<scrolling-protocol>> ()
  (:function scroll-down-line (gadget)
      (:documentation " Cause _gadget_ to be scrolled down by one line. Defined by: <<SCROLLING-PROTOCOL>> "))
  (:function scroll-up-line (gadget)
      (:documentation " Cause _gadget_ to be scrolled up by one line. Defined by: <<SCROLLING-PROTOCOL>> "))
  (:function scroll-down-page (gadget)
      (:documentation " Cause _gadget_ to be scrolled down by one page. Defined by: <<SCROLLING-PROTOCOL>> "))
  (:function scroll-up-page (gadget)
      (:documentation " Cause _gadget_ to be scrolled up by one page. Defined by: <<SCROLLING-PROTOCOL>> "))
  (:function scroll-to-position (gadget position)
      (:documentation " Cause _gadget_ to be scrolled to _position_. Defined by: <<SCROLLING-PROTOCOL>> "))
  (:function note-scroll-bar-changed (scroll-bar)
      (:documentation " Notification that _scroll-bar_ has been changed. Defined by: <<SCROLLING-PROTOCOL>> ")))


;;; The following are from 'splitters.lisp'

(define-protocol <<splitter-protocol>> ()
  (:getter gadget-ratios (gadget)
	   (:documentation
"
Returns the ratios of the windows in _splitter_. This generic function
lets you query the position of a splitter.

The _splitter_ argument is an instance of type <SPLITTER>. The
_ratios_ argument is an instance of false-or(<sequence>).

TODO: Do better!

Defined by: <<SPLITTER-PROTOCOL>>
"))
  (:setter (setf gadget-ratios) (ratios gadget)
	   (:documentation
"
Sets the ratios of the windows in _splitter_ to _ratios_. This generic
function lets you set the position of a splitter.

The _splitter_ argument is an instance of type <SPLITTER>. The
_ratios_ argument is an instance of false-or(<sequence>). Set _ratios_
to NIL if you do not care what ratios are used.

TODO: Do better!

Defined by: <<SPLITTER-PROTOCOL>>
")))


;;; The following are from 'text-gadgets.lisp'

(define-protocol <<selection-change-protocol>> (<<gadget-protocol>>)
  (:function execute-text-selection-changed-callback (gadget client id) (:documentation " Defined by: <<SELECTION-CHANGE-PROTOCOL>> "))
  (:function do-execute-text-selection-changed-callback (gadget client id) (:documentation " Defined by: <<SELECTION-CHANGE-PROTOCOL>> "))
  (:getter gadget-text-selection-changed-callback (gadget) (:documentation " Defined by: <<SELECTION-CHANGE-PROTOCOL>> "))
  (:setter (setf gadget-text-selection-changed-callback) (callback gadget) (:documentation " Defined by: <<SELECTION-CHANGE-PROTOCOL>> ")))


(define-protocol <<protected-gadget-protocol>> (<<gadget-protocol>>)
  (:function execute-protection-callback (gadget client id range)
	     (:documentation
"
Defined by: <<PROTECTED-GADGET-PROTOCOL>>
"))
  (:function do-execute-protection-callback (gadget client id range)
	     (:documentation
"
Defined by: <<PROTECTED-GADGET-PROTOCOL>>
"))
  (:getter gadget-protection-callback (gadget)
	   (:documentation
"
Defined by: <<PROTECTED-GADGET-PROTOCOL>>
"))
  (:setter (setf gadget-protection-callback) (callback gadget)
	   (:documentation
"
Defined by: <<PROTECTED-GADGET-PROTOCOL>>
")))


(define-protocol <<text-field-protocol>> (<<value-gadget-protocol>>)
  (:getter gadget-text (gadget)
	   (:documentation
"
Returns the text for the specified gadget.

Example:

First, create and display a text field by typing the following into an
interactor:

    (defparaneter *g*
                  (contain (make-pane '<text-field>
                                      :value-type
                                      (find-class 'integer))))

Next, type something into the text field. You can return the text
string that you just typed with the following form:

    (gadget-text *g*)

Defined by: <<TEXT-FIELD-PROTOCOL>>
"))
  (:setter (setf gadget-text) (text gadget &key do-callback?)
	   (:documentation
"
Sets the text for the specified gadget.

Example:

First, create and display a text field by typing the following into an
interactor:

    (defparaneter *g*
                  (contain (make-pane '<text-field>
                                      :value-type
                                      (find-class 'integer))))

Next, set the value of the text field with the following form:

    (setf (gadget-text *g*) \"Hello World\")

TODO: Test this example, setting a string into an integer text field
shouldn't work...

Defined by: <<TEXT-FIELD-PROTOCOL>>
"))
  (:getter gadget-text-buffer (gadget) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  (:setter (setf gadget-text-buffer) (text gadget) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  (:function note-gadget-text-changed (gadget) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  (:function gadget-text-parser (type text) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  (:function gadget-value-printer (type value) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  ;; Get and set the current selection
  (:function text-selection (gadget) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  (:function (setf text-selection) (range gadget) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  ;; Return or replace the current selection.  Note that this works
  ;; on all sorts of sheets, not just text gadgets
  (:function selected-text (gadget) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  (:function (setf selected-text) (string gadget) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  (:function text-field-modified? (gadget) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  (:function (setf text-field-modified?) (modified? gadget) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  ;; These can be more efficient than using 'gadget-value'
  (:function text-field-size (gadget) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  (:function text-field-text (gadget range) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  ;; Get and set the current caret position
  (:function text-caret-position (gadget) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  (:function (setf text-caret-position) (index gadget) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  ;; Mapping indices to caret positions, and vice-versa
  (:function character-position (gadget x y) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> "))
  (:function position-character (gadget index) (:documentation " Defined by: <<TEXT-FIELD-PROTOCOL>> ")))


(define-protocol <<text-editor-protocol>> (<<text-field-protocol>>)
  (:getter text-field-word-wrap? (gadget) (:documentation " Defined by: <<TEXT-EDITOR-PROTOCOL>> "))
  ;; Hacking lines
  (:function current-line (gadget) (:documentation " Defined by: <<TEXT-EDITOR-PROTOCOL>> "))
  (:function line-length (gadget line) (:documentation " Defined by: <<TEXT-EDITOR-PROTOCOL>> "))
  (:function get-line (gadget line) (:documentation " Defined by: <<TEXT-EDITOR-PROTOCOL>> "))
  ;; Mapping indices to lines, and vice-versa
  (:function index-line (gadget index) (:documentation " Defined by: <<TEXT-EDITOR-PROTOCOL>> "))
  (:function line-index (gadget line) (:documentation " Defined by: <<TEXT-EDITOR-PROTOCOL>> "))
  ;; Protection
  (:function text-range-protected? (gadget range) (:documentation " Defined by: <<TEXT-EDITOR-PROTOCOL>> "))
  (:function (setf text-range-protected?) (protected? gadget range) (:documentation " Defined by: <<TEXT-EDITOR-PROTOCOL>> "))
  ;; Searching
  (:function find-text (gadget string) (:documentation " Defined by: <<TEXT-EDITOR-PROTOCOL>> ")))


(define-protocol <<rich-text-editor-protocol>> (<<text-editor-protocol>>)
  (:getter character-format (gadget) (:documentation " Defined by: <<RICH-TEXT-EDITOR-PROTOCOL>> "))
  ;; Set the default character format, or set the format for the
  ;; indicated range
  (:setter (setf character-format) (sd gadget &key range)
	   (:documentation
"
Set the default character format, or set the format for the
indicated range.
Defined by: <<RICH-TEXT-EDITOR-PROTOCOL>>
"))
  (:getter paragraph-format (gadget &key range) (:documentation " Defined by: <<RICH-TEXT-EDITOR-PROTOCOL>> "))
  (:setter (setf paragraph-format) (paragraph-format gadget &key range) (:documentation " Defined by: <<RICH-TEXT-EDITOR-PROTOCOL>> ")))


;;; The following are from 'viewports.lisp'

(define-protocol <<viewport-protocol>> ()
  (:function sheet-viewport (sheet)
	     (:documentation
"
Returns the viewport that is clipping _sheet_.

Defined by: <<VIEWPORT-PROTOCOL>>
"))
  (:function sheet-viewport-region (sheet)
	     (:documentation
"
Returns the sheet region of _sheet_'s viewport, if it has one. If
_sheet_ has no viewport, it returns _sheet_'s own region.

Defined by: <<VIEWPORT-PROTOCOL>>
"))
  (:function scroll-viewport (viewport x y &key update-scroll-bars?) (:documentation " Defined by: <<VIEWPORT-PROTOCOL>> "))
  (:function scroll-viewport-child (viewport sheet x y
					     &key update-scroll-bars?) (:documentation " Defined by: <<VIEWPORT-PROTOCOL>> "))
  (:function shift-visible-region (sheet oleft otop oright obottom
					 nleft ntop nright nbottom) (:documentation " Defined by: <<VIEWPORT-PROTOCOL>> "))
  ;; Users can define methods on the next two to do things such as
  ;; linking two scrollable panes together
  (:function note-viewport-position-changed (frame sheet x y) (:documentation " Defined by: <<VIEWPORT-PROTOCOL>> "))
  (:function note-viewport-region-changed (sheet viewport) (:documentation " Defined by: <<VIEWPORT-PROTOCOL>> "))
  ;; These two ask of a sheet, are you capable of scrolling in this direction?
  ;; The default methods just query the containing viewport as to whether there
  ;; is a scroll bar in that direction
  (:function sheet-scrolls-horizontally? (sheet) (:documentation " Defined by: <<VIEWPORT-PROTOCOL>> "))
  (:function sheet-scrolls-vertically? (sheet) (:documentation " Defined by: <<VIEWPORT-PROTOCOL>> ")))
