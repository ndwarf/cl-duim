;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GADGETS-INTERNALS -*-
(in-package :duim-gadgets-internals)

#||
/// Useful constants

//---*** These should really be computed by the backend
define constant $progress-bar-best-width    :: <integer> = 120;
define constant $status-bar-label-min-width :: <integer> = 150;
||#

(defconstant +progress-bar-best-width+    120)
(defconstant +status-bar-label-min-width+ 150)



#||

/// Collection gadgets

define constant <selection-specifier> = type-union(one-of(#"all"), <sequence>);
||#

(deftype <selection-specifier> () '(or sequence (member :all)))


#||
define open abstract class <collection-gadget> (<value-gadget>)
end class <collection-gadget>;
||#

(defclass <collection-gadget> (<value-gadget>)
  ()
  (:documentation
"
The class of all gadgets that can contain collections.

The :items initarg is used to specify the collection of items that the
collection gadget contains.

The :label-key and :value-key initargs are functions that are used to
calculate the labels and the value of the gadget respectively.

The value of a collection gadget is determined by calling the value
key of the gadget on each selected item in the gadget. The \"printed
representation\" of a collection gadget is determined by calling the
label key of the gadget on each item.

By default, the label key returns the numeric label of the gadget
items (for example, the buttons in a button box would be labeled 1, 2,
3, and so on). In general, the label key can be trusted to \"do the
right thing\" by default.

By default, the value key returns the collection gadget itself.

Note also that the GADGET-VALUE method for collection gadgets is
different for single and multiple selection gadgets. For single
selection, the item that is selected is returned. For multiple
selection, a sequence of the selected items is returned.

The :test initarg is the function used to test whether two items of
the collection are considered identical.

The :selection initarg is available only to those subclasses of
<COLLECTION-GADGET> that contain items that may be selected. The
selection is a collection containing the selected keys from the items
collection.

Subclasses of <COLLECTION-GADGET> that can have selections are:
<LIST-BOX>, <OPTION-BOX>, <LIST-CONTROL>, <TREE-CONTROL>,
<TABLE-CONTROL>, <RADIO-BOX>, <CHECK-BOX>, <CHECK-MENU-BOX>,
<RADIO-MENU-BOX>, <COMBO-BOX>.

The :key-press-callback initarg lets you specify a key-press
callback. This type of callback is invoked whenever a key on the
keyboard is pressed while the gadget has focus. It applies only to
graph controls, list controls, tab controls, and table controls. See
GADGET-KEY-PRESS-CALLBACK for a fuller description of key-press
callbacks.
")
  (:metaclass <abstract-metaclass>))

#||
define protocol <<collection-gadget-protocol>> ()
  getter gadget-items
    (gadget :: <collection-gadget>) => (items :: <sequence>);
  getter gadget-items-setter 
    (items :: <sequence>, gadget :: <collection-gadget>) => (items :: <sequence>);
  function note-gadget-items-changed
    (gadget :: <collection-gadget>) => ();
  getter gadget-test
    (gadget :: <collection-gadget>) => (test :: <function>);
  getter gadget-selection
    (gadget :: <collection-gadget>) => (selection :: <sequence>);
  getter gadget-selection-setter 
    (selection-specifier :: <selection-specifier>, gadget :: <collection-gadget>,
     #key do-callback?)
 => (selection :: <sequence>);
  function note-gadget-selection-changed
    (gadget :: <collection-gadget>) => ();
end protocol <<collection-gadget-protocol>>;
||#
#||
(define-protocol <<collection-gadget-protocol>> ()
  (:getter gadget-items (gadget))
  (:getter (setf gadget-items) (items gadget))
  (:function note-gadget-items-changed (gadget))
  (:getter gadget-test (gadget))
  (:getter gadget-selection (gadget))
  (:getter (setf gadget-selection) (selection-specifier gadget
                                                        &key do-callback?))
  (:function note-gadget-selection-changed (gadget)))
||#

(defgeneric gadget-item-value (gadget item))
(defgeneric gadget-item-label (gadget item))
(defgeneric collection-gadget-item-label (gadget item))
(defgeneric gadget-value-index (gadget value))
(defgeneric gadget-selection-for-value (gadget value))
(defgeneric gadget-keep-selection-visible? (gadget))
(defgeneric gadget-item-selected? (gadget item))

(defgeneric collection-gadget-default-label-key (object))
(defgeneric collection-gadget-default-value-key (object))


#||
// Mix-in for gadgets that operate upon collections (e.g. button-boxes)
define open abstract class <collection-gadget-mixin> 
    (<value-gadget-mixin>,
     <collection-gadget>)
  slot gadget-items :: <sequence> = #[],
    setter: %items-setter,
    init-keyword: items:;
  sealed slot gadget-label-key :: <function> = collection-gadget-default-label-key,
    init-keyword: label-key:;
  sealed slot gadget-value-key :: <function> = collection-gadget-default-value-key,
    init-keyword: value-key:;
  sealed slot gadget-test :: <function> = \==,
    init-keyword: test:;
end class <collection-gadget-mixin>;
||#

(defclass <collection-gadget-mixin>
    (<value-gadget-mixin> <collection-gadget>)
  ((gadget-items :type sequence :initform (make-array 0 :adjustable t :fill-pointer t)
		 :reader gadget-items :writer %items-setter :initarg :items)
   (gadget-label-key :type function :initform #'collection-gadget-default-label-key :initarg :label-key :accessor gadget-label-key)
   (gadget-value-key :type function :initform #'collection-gadget-default-value-key :initarg :value-key :accessor gadget-value-key)
   (gadget-test :type function :initform #'eql :initarg :test :accessor gadget-test))
  (:documentation
"
Mixin for gadgets that operate upon collections (e.g. button-boxes).
")
  (:metaclass <abstract-metaclass>))


#||
define method initialize
    (gadget :: <collection-gadget-mixin>, #key name-key) => ()
  //---*** Hack to remove when name-key has really gone
  when (name-key)
    gadget-label-key(gadget) := name-key
  end;
  next-method()
end method initialize;


define method gadget-items-setter
    (items :: <sequence>, gadget :: <collection-gadget-mixin>)
 => (items :: <sequence>)
  unless (items = gadget-items(gadget))
    gadget.%items := items;
    note-gadget-items-changed(gadget)
  end;
  items
end method gadget-items-setter;
||#

(defmethod (setf gadget-items) ((items sequence) (gadget <collection-gadget-mixin>))
  (unless (equal? items (gadget-items gadget))
    (%items-setter items gadget)
    (note-gadget-items-changed gadget))
  items)

;; FIXME: SHOULD ALSO ACCEPT A <RANGE>...
;;; (defmethod (setf gadget-items) ((items <range>)
;;; 				(gadget <collection-gadget-mixin>))
;;;   (unless (equivalent items (gadget-items gadget))
;;;     (%items-setter items gadget)
;;;     (note-gadget-items-changed gadget))
;;;   items)


#||
define method note-gadget-items-changed
    (gadget :: <collection-gadget>) => ()
  #f
end method note-gadget-items-changed;
||#

(defmethod note-gadget-items-changed ((gadget <collection-gadget>))
  nil)


#||
define method gadget-item-value
    (gadget :: <collection-gadget-mixin>, item) => (value)
  let value-key = gadget-value-key(gadget);
  value-key(item)
end method gadget-item-value;
||#

(defmethod gadget-item-value ((gadget <collection-gadget-mixin>) item)
  (let ((value-key (gadget-value-key gadget)))
    (funcall value-key item)))


#||
define method gadget-item-label
    (gadget :: <collection-gadget-mixin>, item)
 => (label)
  let label-key = gadget-label-key(gadget);
  label-key(item)
end method gadget-item-label;
||#

(defmethod gadget-item-label ((gadget <collection-gadget-mixin>) item)
  (let ((label-key (gadget-label-key gadget)))
    (funcall label-key item)))


#||
define method collection-gadget-item-label
    (gadget :: <collection-gadget-mixin>, item)
 => (label :: <string>)
  let label = gadget-label-key(gadget)(item);
  assert(instance?(label, <string>),
	 "'gadget-label-key' returned non-string %= for gadget %=", label, gadget);
  let ampersand = position(label, '&');
  if (ampersand & (ampersand < size(label) - 1))
    remove(label, '&', count: 1)
  else
    label
  end
end method collection-gadget-item-label;
||#

(defmethod collection-gadget-item-label ((gadget <collection-gadget-mixin>) item)
  (let ((label (funcall (gadget-label-key gadget) item)))
    (unless (typep label 'string)
      (error "'gadget-label-key' returned non-string ~a for gadget ~a" label gadget))
    (let ((ampersand (position #\& label)))
      (if (and ampersand (< ampersand (- (length label) 1)))
	  (remove #\& label :count 1)
	label))))


#||
define macro with-preserved-selection
  { with-preserved-selection (?gadget:expression)
      ?body:body
    end }
 => { let gadget = ?gadget;
      let value = gadget-value(gadget);
      // Clear the selection, since it may no longer be valid
      gadget-selection(gadget) := #[];
      ?body;
      when (value)
        gadget-value(gadget) := value
      end }
end macro with-preserved-selection;
||#

(defmacro with-preserved-selection ((gadget) &body body)
  (let ((_gadget (gensym "GADGET-"))
	(_value (gensym "VALUE-")))
  `(let* ((,_gadget ,gadget)
	  (,_value  (gadget-value ,_gadget)))
     ;; Clear the selection, since it may no longer be valid
     (setf (gadget-selection ,_gadget) (make-stretchy-vector))  ; XXX: MAKE-SIMPLE-VECTOR
     (progn
       ,@body)
     (when ,_value
       (setf (gadget-value ,_gadget) ,_value)))))


#||
define method update-gadget
    (gadget :: <collection-gadget-mixin>) => ()
  with-preserved-selection (gadget)
    note-gadget-items-changed(gadget)
  end
end method update-gadget;
||#

(defmethod update-gadget ((gadget <collection-gadget-mixin>))
  (with-preserved-selection (gadget)
    (note-gadget-items-changed gadget)))


#||
//--- Can we do anything better than this?
define method gadget-value-type
    (gadget :: <collection-gadget-mixin>) => (type :: <type>)
  select (gadget-selection-mode(gadget))
    #"multiple" => <sequence>;
    otherwise   => <object>;
  end
end method gadget-value-type;
||#

(defmethod gadget-value-type ((gadget <collection-gadget-mixin>))
  (case (gadget-selection-mode gadget)
    (:multiple (find-class 'sequence))
    (t         t)))



#||

// Mix-in to allow a selection of zero or more items
// Note: This must come before <collection-gadget-mixin> in any CPL,
// as it overrides some of the behavior.
define open abstract class <gadget-selection-mixin> (<collection-gadget>)
  sealed slot gadget-selection :: <sequence> = #[],
    init-keyword: selection:,
    setter: %selection-setter;
end class <gadget-selection-mixin>;
||#

(defclass <gadget-selection-mixin>
    (<collection-gadget>)
  ((gadget-selection :type sequence :initform #() :initarg :selection :reader gadget-selection :writer %selection-setter))
  (:documentation
"
Mixin to allow a selection of zero or more items.
Note: this must come before <COLLECTION-GADGET-MIXIN> in any CPL,
as it overrides some of the behaviour.
")
  (:metaclass <abstract-metaclass>))


#||
define method initialize 
    (gadget :: <gadget-selection-mixin>,
     #key keep-selection-visible? = #t, value = $unsupplied) => ()
  next-method();
  gadget-flags(gadget)
    := logior(logand(gadget-flags(gadget), lognot(%keep_selection_visible)),
	      if (keep-selection-visible?) %keep_selection_visible else 0 end);
  if (unsupplied?(value))
    let selection = gadget-selection(gadget);
    let items = gadget-items(gadget);
    when (empty?(selection)
	  & items & size(items) > 0
	  & gadget-selection-mode(gadget) = #"single")
      gadget-selection(gadget) := #[0]
    end
  else
    gadget-selection(gadget) := gadget-selection-for-value(gadget, value)
  end
end method initialize;
||#

(defmethod initialize-instance :after ((gadget <gadget-selection-mixin>)
				       &key (keep-selection-visible? t) (value :unsupplied)
				       &allow-other-keys)
  (setf (gadget-flags gadget)
	(logior (logand (gadget-flags gadget) (lognot %keep_selection_visible))
		(if keep-selection-visible? %keep_selection_visible 0)))
  (if (unsupplied? value)
      (let ((selection (gadget-selection gadget))
	    (items     (gadget-items gadget)))
	(when (and (empty? selection)
		   items
		   ;; items could be a sequence or a <range>
		   (> (size items) 0)
		   (eql (gadget-selection-mode gadget) :single))
	  ;; XXX: Make a simple vector?
	  (setf (gadget-selection gadget) (make-array 1 :adjustable t :fill-pointer t :initial-element 0))))
      ;; else
      (setf (gadget-selection gadget) (gadget-selection-for-value gadget value))))


#||
define sealed method gadget-selection-setter
    (selection :: <sequence>, gadget :: <gadget-selection-mixin>,
     #key do-callback? = #f)
 => (selection :: <sequence>)
  unless (selection = gadget-selection(gadget))
    let items-size = size(gadget-items(gadget));
    for (key in selection)
      assert(instance?(key, <integer>) & key >= 0 & key < items-size,
	     "Invalid key %= in selection for %=",
	     key, gadget)
    end;
    gadget.%selection := selection;
    when (do-callback?)
      execute-value-changed-callback
	(gadget, gadget-client(gadget), gadget-id(gadget))
    end;
    note-gadget-value-changed(gadget);
    note-gadget-selection-changed(gadget)
  end;
  selection
end method gadget-selection-setter;
||#

(defmethod (setf gadget-selection) ((selection sequence) (gadget <gadget-selection-mixin>)
				    &key (do-callback? nil))
  (unless (equal? selection (gadget-selection gadget))
    (let ((items-size (size (gadget-items gadget))))
      (map nil #'(lambda (key)
		   (unless (and (integerp key) (>= key 0) (< key items-size))
		     (error "Invalid key ~a in selection for ~a" key gadget)))
	   selection))
    (%selection-setter selection gadget)
    (when do-callback?
      (execute-value-changed-callback gadget (gadget-client gadget) (gadget-id gadget)))
    (note-gadget-value-changed gadget)
    (note-gadget-selection-changed gadget))
  selection)


#||
define sealed method gadget-selection-setter
    (selection == #"all", gadget :: <gadget-selection-mixin>, 
     #key do-callback? = #f)
 => (new-selection :: <sequence>)
  assert(gadget-selection-mode(gadget) == #"multiple",
         "Cannot set selection of non-multiple selection gadget to %=: %=",
         selection, gadget);
  let new-selection = range(from: 0, below: size(gadget-items(gadget)));
  gadget-selection-setter(new-selection, gadget, do-callback?: do-callback?)
end method gadget-selection-setter;
||#

(defmethod (setf gadget-selection) ((selection (eql :all))
				    (gadget <gadget-selection-mixin>)
				    &key
				    (do-callback? nil))
  (unless (eql (gadget-selection-mode gadget) :multiple)
    (error "Cannot set selection of non-multiple selection gadget to ~a: ~a"
	   selection gadget))
  (let ((new-selection (range :from 0 :below (length (gadget-items gadget)))))
    (setf (gadget-selection gadget :do-callback? do-callback?) new-selection)))


#||
define method gadget-items-setter
    (items :: <sequence>, gadget :: <gadget-selection-mixin>)
 => (items :: <sequence>)
  with-preserved-selection (gadget)
    next-method()
  end;
  items
end method gadget-items-setter;
||#

(defmethod (setf gadget-items) ((items sequence) (gadget <gadget-selection-mixin>))
  (with-preserved-selection (gadget)
    (call-next-method))
  items)


#||
define method note-gadget-selection-changed
    (gadget :: <gadget-selection-mixin>) => ()
  #f
end method note-gadget-selection-changed;
||#

(defmethod note-gadget-selection-changed ((gadget <gadget-selection-mixin>))
  nil)


#||
define method gadget-value (gadget :: <gadget-selection-mixin>) => (value)
  let items = gadget-items(gadget);
  let selection = gadget-selection(gadget);
  when (selection)
    select (gadget-selection-mode(gadget))
      #"single" =>
	unless (empty?(selection))
	  gadget-item-value(gadget, items[selection[0]])
	end;
      #"multiple" =>
	map-as(<simple-vector>,
	       method (index :: <integer>)
		 gadget-item-value(gadget, items[index])
	       end,
	       selection);
      #"none" =>
	#f;
    end
  end
end method gadget-value;
||#

;; FIXME: DECIDE WHETHER THINGS LIKE THE SELECTION AND THE ITEMS ARE ARRAYS,
;; LISTS, RANGES, ANY, OR....

(defmethod gadget-value ((gadget <gadget-selection-mixin>))
  (let ((items (gadget-items gadget))
	(selection (gadget-selection gadget)))
    (when selection
      (ecase (gadget-selection-mode gadget)
	(:single (unless (empty? selection)
		   ;; was (aref items (aref ...))
		   (gadget-item-value gadget (elt items (elt selection 0)))))
	(:multiple (map 'vector
			#'(lambda (index)
			    ;; was (aref items index)...
			    (gadget-item-value gadget (elt items index)))
			selection))
	(:none nil)))))


#||
define sealed method gadget-value-index
    (gadget :: <gadget-selection-mixin>, value)
 => (index :: false-or(<integer>))
  let test = gadget-test(gadget);
  find-key(gadget-items(gadget),
           method (item)
             test(gadget-item-value(gadget, item), value)
           end)
end method gadget-value-index;
||#

(defmethod gadget-value-index ((gadget <gadget-selection-mixin>) value)
  (let ((test (gadget-test gadget)))
    (find-key (gadget-items gadget)
              #'(lambda (item)
		  (funcall test (gadget-item-value gadget item) value)))))


#||
define sealed method gadget-selection-for-value 
    (gadget :: <gadget-selection-mixin>, value) => (selection :: <sequence>)
  select (gadget-selection-mode(gadget))
    #"single" =>
      let index = gadget-value-index(gadget, value);
      if (index) vector(index) else #[] end;
    #"multiple" =>
      error("Non-sequence %= supplied as value for multiple selection gadget %=",
	    value, gadget);
  end
end method gadget-selection-for-value;
||#

(defmethod gadget-selection-for-value ((gadget <gadget-selection-mixin>) value)
  (ecase (gadget-selection-mode gadget)
    (:single (let ((index (gadget-value-index gadget value)))
	       (if index (vector index) #())))
    (:multiple
     (error "Non-sequence ~a supplied as value for multiple selection gadget ~a"
	    value gadget))))


#||  
define sealed method gadget-selection-for-value
    (gadget :: <gadget-selection-mixin>, value :: <collection>)
 => (selection :: <sequence>)
  select (gadget-selection-mode(gadget))
    #"single" =>
      next-method();
    #"multiple" =>
      let indices
	= map-as(<simple-vector>,
		 method (subvalue)
		   gadget-value-index(gadget, subvalue)
		 end,
		 value);
      remove!(indices, #f)
  end
end method gadget-selection-for-value;
||#

;; XXX: All methods that specialize <collection> should handle sequences, hash-tables and ranges.
(defmethod gadget-selection-for-value ((gadget <gadget-selection-mixin>) (value hash-table))
  (error "IMPLEMENT ME: COLLECTION-GADGETS GADGET-SELECTION-FOR-VALUE on HASH-TABLE"))

(defmethod gadget-selection-for-value ((gadget <gadget-selection-mixin>) (value sequence))
  (ecase (gadget-selection-mode gadget)
    (:single (call-next-method))
    (:multiple (let ((indices (map 'vector
				   #'(lambda (subvalue)
				       (gadget-value-index gadget subvalue))
				   value)))
		 (delete nil indices)))))


#||
define sealed method do-gadget-value-setter 
    (gadget :: <gadget-selection-mixin>, value) => ()
  unless (value = gadget-value(gadget))
    let selection = gadget-selection-for-value(gadget, value);
    gadget.%selection := selection;
    note-gadget-selection-changed(gadget)
  end
end method do-gadget-value-setter;
||#

(defmethod do-gadget-value-setter ((gadget <gadget-selection-mixin>) value)
  (unless (eql value (gadget-value gadget))
    (let ((selection (gadget-selection-for-value gadget value)))
      (%selection-setter selection gadget)
      (note-gadget-selection-changed gadget))))


#||  
// Returns #t if the item is selected in the collection gadget
define sealed method gadget-item-selected?
    (gadget :: <collection-gadget-mixin>, item) => (true? :: <boolean>)
  let value = gadget-value(gadget);
  let test = gadget-test(gadget);
  let mode = gadget-selection-mode(gadget);
  select (mode)
    #"single"   => test(gadget-item-value(gadget, item), value);
    #"multiple" => member?(gadget-item-value(gadget, item), value, test: test);
    #"none"     => #f;
  end
end method gadget-item-selected?;
||#
  
(defmethod gadget-item-selected? ((gadget <collection-gadget-mixin>) item)
"
Returns T if the item is selected in the collection gadget.
"
  (let ((value (gadget-value gadget))
	(test  (gadget-test gadget))
	(mode  (gadget-selection-mode gadget)))
    (ecase mode
      (:single (funcall test (gadget-item-value gadget item) value))
      (:multiple (member (gadget-item-value gadget item) value :test test))
      (:none nil))))


#||
define sealed inline method gadget-keep-selection-visible?
    (gadget :: <collection-gadget-mixin>) => (true? :: <boolean>)
  logand(gadget-flags(gadget), %keep_selection_visible) = %keep_selection_visible
end method gadget-keep-selection-visible?;
||#

(defmethod gadget-keep-selection-visible? ((gadget <collection-gadget-mixin>))
  (eql (logand (gadget-flags gadget) %keep_selection_visible) %keep_selection_visible))


#||
/// Collection gadget state

define sealed class <collection-gadget-state> (<value-gadget-state>)
  sealed constant slot %state-items :: <sequence>,
    required-init-keyword: items:;
end class <collection-gadget-state>;

define sealed domain make (singleton(<collection-gadget-state>));
define sealed domain initialize (<collection-gadget-state>);
||#

(defclass <collection-gadget-state> (<value-gadget-state>)
  ((%state-items :type sequence :initarg :items
		 :initform (required-slot ":items" "<collection-gadget-state>")
		 :reader %state-items)))


#||
define method gadget-state
    (gadget :: <collection-gadget>) => (state :: <collection-gadget-state>)
  make(<collection-gadget-state>,
       value: gadget-value(gadget),
       items: gadget-items(gadget))
end method gadget-state;
||#

(defmethod gadget-state ((gadget <collection-gadget>))
  (make-instance '<collection-gadget-state>
                 :value (gadget-value gadget)
                 :items (gadget-items gadget)))


#||
define method gadget-state-setter
    (state :: <collection-gadget-state>, gadget :: <collection-gadget>)
 => (state :: <collection-gadget-state>)
  gadget-items(gadget) := state.%state-items;
  next-method()
end method gadget-state-setter;
||#

(defmethod (setf gadget-state) ((state <collection-gadget-state>) (gadget <collection-gadget>))
  (setf (gadget-items gadget) (slot-value state '%state-items))
  (call-next-method))



#||
// The base class for hairy controls like list and tree controls, etc
define open abstract class <basic-choice-gadget> 
    (<gadget-selection-mixin>,
     <collection-gadget-mixin>,
     <basic-gadget>)
  sealed slot gadget-selection-mode :: <selection-mode> = #"single",
    init-keyword: selection-mode:;
end class <basic-choice-gadget>;
||#

(defclass <basic-choice-gadget>
    (<gadget-selection-mixin>
     <collection-gadget-mixin>
     <basic-gadget>)
  ((gadget-selection-mode :type <selection-mode> :initform :single :initarg :selection-mode :accessor gadget-selection-mode))
  (:documentation
"
 The base class for hairy controls like list and tree controls, etc.
")
  (:metaclass <abstract-metaclass>))



#||

/// List boxes

// This is both an action gadget and a value gadget. Changing the
// selection invokes the value-changed-callback, and double clicking
// invokes the activate-callback.
// When 'read-only?: #t', this is like a Windows list box.
// When 'read-only?: #f', this is like a Windows combo box, if one existed.
define open abstract class <list-box>
    (<bordered-gadget-mixin>,
     <scrolling-gadget-mixin>,
     <action-gadget-mixin>,
     <basic-choice-gadget>)
  sealed constant slot gadget-lines :: false-or(<integer>) = #f,
    init-keyword: lines:;
  keyword read-only?: = #t;
end class <list-box>;
||#

(defclass <list-box>
    (<bordered-gadget-mixin>
     <scrolling-gadget-mixin>
     <action-gadget-mixin>
     <basic-choice-gadget>)
  ((gadget-lines :type (or null integer) :initform nil :initarg :lines :reader gadget-lines))
  (:default-initargs :read-only? t)
  (:documentation
"
The class of list boxes.

The :borders initarg lets you specify a border around the list box. If
specified, a border of the appropriate type is drawn around the
gadget.

The :scroll-bars initarg lets you specify the presence of scroll bars
around the gadget. By default, both horizontal and vertical scroll
bars are created. You can also force the creation of only horizontal
or vertical scroll bars, or you can create scroll bars dynamically;
that is, have them created only if necessary, dependent on the size of
the gadget. If :scroll-bars is NIL, no scroll bars are added to the
gadget.

Internally, this class maps into the Windows list box control.

TODO: Check that DUIM actually works this way for list boxes!

Example:

The following creates a list of three items, without scroll bars.

    (defparameter *list*
                  (contain (make-pane '<list-box>
                                      :items #(#(\"One\" :one)
                                               #(\"Two\" :two)
                                               #(\"Three\" :three))
                                      :label-key #'first
                                      :value-key #'second
                                      :scroll-bars nil)))

TODO: Fix example so it works (label, value keys are wrong).
")
  (:metaclass <abstract-metaclass>))


#||
/// Option boxes

// An option box is like a list box, except that it only displays a single
// selection, and you pull it down to make a different selection.
// When 'read-only?: #t', this is like a Windows drop-down list box.
// When 'read-only?: #f', this is like a Windows drop-down combo box.
define open abstract class <option-box>
    (<bordered-gadget-mixin>,
     <scrolling-gadget-mixin>,
     // No <action-gadget-mixin> because you can't double click!
     <basic-choice-gadget>)
  keyword read-only?: = #t;
end class <option-box>;
||#

;;; Note: GTK calls this a "combo box". It calls what DUIM refers to as
;;; a combo box a "combo box entry".

(defclass <option-box>
    (<bordered-gadget-mixin>
     <scrolling-gadget-mixin>
     ;; No <action-gadget-mixin> because you can't double click!
     <basic-choice-gadget>)
  ()
  (:default-initargs :read-only? t)
  (:documentation
"
The class of option boxes.

The :borders initarg lets you specify a border around the option
box. If specified, a border of the appropriate type is drawn around
the gadget.

The :scroll-bars initarg lets you specify the scroll bar behaviour for
the gadget.

Internally, this class maps into the Windows drop-down list control.
")
  (:metaclass <abstract-metaclass>))


#||
define sealed method gadget-selection-mode 
    (pane :: <option-box>) => (selection-mode :: <selection-mode>)
  #"single"
end method gadget-selection-mode;
||#

(defmethod gadget-selection-mode ((pane <option-box>))
  :single)


#||
/// Combo boxes

// As it turns out, in real life we don't actually model 'read-only?: #f'
// list and option boxes as combo boxes, because we need all the text gadget
// protocols as well.  So we have <combo-box>, too...
define open abstract class <combo-box>
    (<bordered-gadget-mixin>,
     <scrolling-gadget-mixin>,
     <action-gadget-mixin>,
     <changing-value-gadget-mixin>,
     <text-gadget>,
     <basic-choice-gadget>)
  // Same deal as <text-field>...
  sealed slot gadget-text-buffer :: <string> = "",
    init-keyword: text:;
  constant slot gadget-value-type :: <type> = <string>,
    init-keyword: value-type:;
  sealed slot text-field-maximum-size :: false-or(<integer>) = #f,
    init-keyword: maximum-size:;
  keyword read-only?: = #f;
end class <combo-box>;
||#

;; As it turns out, in real life we don't actually model 'read-only?: #f'
;; list and option boxes as combo boxes, because we need all the text gadget
;; protocols as well. So we have <combo-box>, too...
(defclass <combo-box>
    (<bordered-gadget-mixin>
     <scrolling-gadget-mixin>
     <action-gadget-mixin>
     <changing-value-gadget-mixin>
     <text-gadget>
     <basic-choice-gadget>)
  ;; Same deal as <text-field>...
  ((gadget-text-buffer :type string :initform "" :initarg :text :accessor gadget-text-buffer)
   (gadget-value-type :type class :initform (find-class 'string) :initarg :value-type :reader gadget-value-type)
   (text-field-maximum-size :type (or null integer) :initform nil :initarg :maximum-size :accessor text-field-maximum-size))
  (:default-initargs :read-only? nil)
  (:documentation
"
The class of combo boxes. Combo boxes are similar to option boxes,
except that the text field is editable, so that new values can be
specified in addition to those already provided in the drop-down
list. Users may either choose an existing option from the list, or
type in their own.

It is common for additional items typed by the user to be added to the
list of options available. A combo box is often used to specify text
in a Find dialog box, for example, and any previous search terms can
be recalled by choosing them from the list. If you wish to provide
this functionality, then you can do so using a combination of ADD-ITEM
and FIND-ITEM, to search for the presence of an item and add it if it
does not already exist.

The :borders initarg lets you specify a border around the combo
box. If specified, a border of the appropriate type is drawn around
the gadget.

The :scroll-bars initarg lets you specify the scroll bar behaviour for
the gadget.

Internall, this class maps onto the Windows combo box control.

Example:

    (contain (make-pane '<combo-box>
                        :value-type (find-class 'integer)
                        :items (range :from 1 :to 5)))
")
  (:metaclass <abstract-metaclass>))



#||
define sealed method gadget-selection-mode 
    (pane :: <combo-box>) => (selection-mode :: <selection-mode>)
  #"single"
end method gadget-selection-mode;
||#

(defmethod gadget-selection-mode ((pane <combo-box>))
  :single)


#||
define method gadget-value
    (gadget :: <combo-box>) => (value)
  gadget-text-parser(gadget-value-type(gadget), gadget-text(gadget))
end method gadget-value;
||#

(defmethod gadget-value ((gadget <combo-box>))
  (gadget-text-parser (gadget-value-type gadget) (gadget-text gadget)))


#||
// Back-ends where 'gadget-text' and 'gadget-text-buffer' use different
// representations should specialize this method
define method do-gadget-value-setter
    (gadget :: <combo-box>, value) => ()
  let text = gadget-value-printer(gadget-value-type(gadget), value);
  unless (text = gadget-text(gadget))
    gadget-text-buffer(gadget) := text;
    note-gadget-text-changed(gadget)
  end
end method do-gadget-value-setter;
||#

(defmethod do-gadget-value-setter ((gadget <combo-box>) value)
"
Back-ends where 'gadget-text' and 'gadget-text-buffer' use different
representations should specialize this method.
"
  (let ((text (gadget-value-printer (gadget-value-type gadget) value)))
    (unless (eql text (gadget-text gadget))
      (setf (gadget-text-buffer gadget) text)
      (note-gadget-text-changed gadget))))



#||

/// Spin boxes

// You can get "integer spin boxes" by specifying 'items: range(...)',
// or <option-pane>-like spin boxes by specifying a set of items
define open abstract class <spin-box>
    (<bordered-gadget-mixin>,
     <gadget-selection-mixin>,
     <collection-gadget-mixin>,
     <basic-gadget>)
end class <spin-box>;
||#

(defclass <spin-box>
    (<bordered-gadget-mixin>
     <gadget-selection-mixin>
     <collection-gadget-mixin>
     <basic-gadget>)
  ()
  (:documentation
"
The class of spin box gadgets. A spin box gadget is a text box that
only accepts a limited range of values that form an ordered loop. As
well as typing a value directly into the text box, small buttons are
placed on its right hand side (usually with up and down arrow icons as
labels). You can use these buttons to increase or decrease the value
shown in the text box.

A spin box may be used when setting a percentage value, for
example. In this case, on the values between 0 and 100 are value, and
a spin box is a suitable way of ensuring that only valid values are
specified by the user.

The :borders initarg lets you specify a border around the spin box. If
specified, a border of the appropriate type is drawn around the
gadget.

When designing a user interface, you will find that sliders are a
suitable alternative to spin boxes in many situations.

Example:

    (contain (make-pane '<spin-box>
                        :items (range :from 1 :to 10)))
")
  (:metaclass <abstract-metaclass>))


#||
define sealed method gadget-selection-mode 
    (pane :: <spin-box>) => (selection-mode :: <selection-mode>)
  #"single"
end method gadget-selection-mode;
||#

(defmethod gadget-selection-mode ((pane <spin-box>))
  :single)


#||
define method gadget-label
    (pane :: <spin-box>) => (label :: <string>)
  let selection = gadget-selection(pane);
  case
    empty?(selection) =>
      "";
    otherwise =>
      let item = gadget-items(pane)[selection[0]];
      gadget-label-key(pane)(item)
  end
end method gadget-label;
||#

(defmethod gadget-label ((pane <spin-box>))
  (let ((selection (gadget-selection pane)))
    (cond ((empty? selection) "")
	  (t (let ((item (elt (gadget-items pane) (elt selection 0))))
	       (funcall (gadget-label-key pane) item))))))



#||
/// Gadget Boxes

// A gadget box is a value gadget whose value is given by a selection
// of zero of more buttons contained with the box.  For example, radio
// and check boxes.
define open abstract class <gadget-box>
    (<oriented-gadget-mixin>,
     <gadget-command-mixin>,
     <collection-gadget-mixin>,
     <updatable-gadget-mixin>)
end class <gadget-box>;
||#

(defclass <gadget-box>
    (<oriented-gadget-mixin>
     <gadget-command-mixin>
     <collection-gadget-mixin>
     <updatable-gadget-mixin>)
  ()
  (:documentation
"
A gadget box is a value gadget whose value is given by a selection
of zero or more buttons contained within the box. For example, radio
and check boxes.
")
  (:metaclass <abstract-metaclass>))



#||
/// Tool bars

define abstract class <gadget-bar-mixin>
    (<updatable-gadget-mixin>)
end class <gadget-bar-mixin>;
||#

(defclass <gadget-bar-mixin>
    (<updatable-gadget-mixin>)
  ()
  (:metaclass <abstract-metaclass>))


#||
define open abstract class <tool-bar>
    (<no-value-gadget-mixin>,
     <gadget-bar-mixin>,
     <basic-gadget>)
end class <tool-bar>;
||#

(defclass <tool-bar>
    (<no-value-gadget-mixin>
     <gadget-bar-mixin>
     <basic-gadget>)
  ()
  (:documentation
"
The class of tool bars. A tool bar is a gadget that contains, as
children, a number of buttons that give the user quick access to the
more common commands in an application. Typically, the label for each
button is an icon that pictorially represents the operation that
clicking the button performs.

A tool bar is often placed underneath the menu bar of an application,
although its position is very often configurable, and a tool bar may
often be \"docked\" against any edge of the application's frame. In
addition, a tool bar can sometimes be displayed as a free-floating
window inside the application.

Internally, this class maps into the Windows toolbar control.
")
  (:metaclass <abstract-metaclass>))


#||
define sealed class <tool-bar-pane>
    (<tool-bar>, <single-child-wrapping-pane>)
end class <tool-bar-pane>;
||#

(defclass <tool-bar-pane>
    (<tool-bar> <single-child-wrapping-pane>)
  ())


#||
define method class-for-make-pane 
    (framem :: <frame-manager>, class == <tool-bar>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<tool-bar-pane>, #f)
end method class-for-make-pane;

define sealed domain make (singleton(<tool-bar-pane>));
define sealed domain initialize (<tool-bar-pane>);
||#

(defmethod class-for-make-pane ((framem <frame-manager>) (class (eql (find-class '<tool-bar>))) &key)
  (values (find-class '<tool-bar-pane>) nil))



#||
/// Status bars

define open abstract class <status-bar>
    (<value-gadget-mixin>,
     <range-gadget-mixin>,
     <bordered-gadget-mixin>,
     <labelled-gadget-mixin>,
     <gadget-bar-mixin>,
     <basic-gadget>)
  sealed slot status-bar-label-pane :: false-or(<labelled-gadget-mixin>) = #f,
    init-keyword: label-pane:;
  sealed slot status-bar-progress-bar :: false-or(<progress-bar>) = #f,
    init-keyword: progress-bar:;
end class <status-bar>;
||#

;;; Moved from lower down in file.

;;; The idea is that a status bar can hold at least a label pane
;;; and a progress control...

(defgeneric status-bar-label-pane (status-bar)
  (:documentation
"
Returns the gadget that displays the label of _status-bar_.

Example:

Create a status bar with a label as follows:

    (defparameter *status*
                  (contain (make-pane '<status-bar>
                                      :value-range
                                      (range :from 0 :to 100)
                                      :label \"Status\")))

The pane that the label of the status bar is displayed in can be
returned with the following call:

    (status-bar-label-pane *status*)
"))

(defgeneric status-bar-progress-bar (status-bar)
  (:documentation
"
Returns the progress bar for _status-bar_, if there is one.
"))


(defclass <status-bar>
    (<value-gadget-mixin>
     <range-gadget-mixin>
     <bordered-gadget-mixin>
     <labelled-gadget-mixin>
     <gadget-bar-mixin>
     <basic-gadget>)
  ((status-bar-label-pane :type (or null <labelled-gadget-mixin>) :initform nil :initarg :label-pane :accessor status-bar-label-pane)
   (status-bar-progress-bar :type (or null <progress-bar>) :initform nil :initarg :progress-bar :accessor status-bar-progress-bar))
  (:documentation
"
The class of status bars. Status bars are often used at the bottom of
an application window, and can provide a range of feedback on the
current state of an application. Some examples of information that is
often placed in a status bar are:

  + Documentation strings for the currently selected menu button.
  + Progress indicators to show the state of operations such as
    loading and saving files.
  + The current position of the caret on the screen.
  + Currently selected configurable values (such as the current
    font family, size, and style in a word processor).
  + The current time.

In particular, it is trivial to add an in-built progress bar to a
status bar. Any documentation strings specified for menu buttons in a
frame are automatically displayed in the label pane of a status bar
when the mouse pointer is placed over the menu button itself.

The :label initarg specifies a string or icon that is to be used as a
label for the gadget. Alternatively, the :label-pane initarg specifies
a pane that should be used as the label. You should only use one of
these initargs; see the discussion about creating status bars below.

If :progress-bar? is true, then the status bar has a progress
bar. Alternatively, the :progress-bar initarg specifies a pane that
should be used as the label. You should only use one of these
initargs; see the discussion about creating status bars below.

The :value initarg specifies the gadget value of the progress bar, if
there is one.

The :value-range initarg is the range of values across which the
progress bar can vary, if there is one.

Internally, this class maps into the Windows status window control.

There are two ways that you can create a status bar:

  + The simple way is to provide a simple status bar that only has
    a label and, optionally, a progress bar.
  + The more complicated way is to define all the elements of a
    status bar from scratch, as children of the status bar.

If you want to create a simple status bar, then use the :label initarg
to specify the text to be displayed in the status bar. In addition,
you can set or check the label using GADGET-LABEL once the status bar
has been created.

You can create a basic progress bar by setting :progress-bar? to
true. If you create a progress bar in this way, then it will respond
to the GADGET-VALUE and GADGET-VALUE-RANGE protocols; you can use
GADGET-VALUE to set the position of the progress bar explicitly, or to
check it, and you can use GADGET-VALUE-RANGE to define the range of
values that the progress bar can take, just like any other value
gadget. By default, the range of possible values is 0 to 100.

The more complicated way to create a status bar is to define all its
children from scratch. You need to do this if you want to provide the
user with miscellaneous feedback about the application state, such as
online documentation for menu commands, or the current position of the
cursor. Generally speaking, if you need to provide a pane in which to
display information, you should define instances of <label> for each
piece of information you want to use. However, if you wish you can add
any type of gadget to your status bar in order to create a more
interactive status bar. For instance, many word processors include
gadgets in the status bar that let you select the zoom level at which
to view the current document from a drop-down list of options.

If you define the children of a status bar from scratch in this way,
you should make appropriate use of the :label-pane and :progress-bar
initargs. The :label-pane initarg lets you specify the pane that is to
act as the label for the status bar; that is, the pane that responds
to the GADGET-LABEL protocol. The :progress-bar initarg lets you
define a progress bar to add to the status bar. If you create a status
bar from scratch, you should not use either the :label
or :progress-bar? initargs.

Example:

The following creates a basic status bar with the given label, and a
progress bar with the given range of values.

    (contain (make-pane '<status-bar>
                        :progress-bar? t
                        :value-range (range :from 0 :to 50)
                        :label \"Status\"))
")
  (:metaclass <abstract-metaclass>))


#||
define method initialize 
    (pane :: <status-bar>,
     #key frame-manager: framem, label, value, value-range, progress-bar?)
 => ()
  next-method();
  when (empty?(sheet-children(pane)))
    with-frame-manager (framem)
      let label-pane
	= make(<label>, label: label | "", 
	       min-width: $status-bar-label-min-width, max-width: $fill);
      status-bar-label-pane(pane) := label-pane;
      let children
	= if (progress-bar? | (value | value-range))
	    let best-width  = $progress-bar-best-width;
	    let value-range = value-range | range(from: 0, to: 100);
            let progress-bar
              = make(<progress-bar>, 
		     value-range: value-range,
		     value:       value | value-range[0],
		     withdrawn?:  ~value,
		     min-width: best-width, max-width: best-width);
            status-bar-progress-bar(pane) := progress-bar;
            vector(label-pane, progress-bar)
	  else
	    vector(label-pane)
	  end;
      sheet-children(pane) := children
    end
  end;
  gadget-label(pane) := label
end method initialize;
||#

(defmethod initialize-instance :after ((pane <status-bar>)
				       &key ((:frame-manager framem)) label value
				       value-range progress-bar?)
  (when (empty? (sheet-children pane))
    (with-frame-manager (framem)
      (let ((label-pane (make-pane '<label> :label (or label "")
				   :min-width +status-bar-label-min-width+ :max-width +fill+)))
	(setf (status-bar-label-pane pane) label-pane)
	(let ((children (if (or progress-bar? (or value value-range))
			    (let* ((best-width +progress-bar-best-width+)
				   (value-range (or value-range (range :from 0 :to 100)))
				   (progress-bar (make-pane '<progress-bar>
							    :value-range value-range
							    :value (or value (elt value-range 0))
							    :withdrawn? (not value)
							    :min-width best-width
							    :max-width best-width)))
			      (setf (status-bar-progress-bar pane) progress-bar)
			      (vector label-pane progress-bar))
			    ;; else
			    (vector label-pane))))
	  (setf (sheet-children pane) children))
	(setf (gadget-label pane) label)))))


#||
// The idea is that a status bar can hold at least a label pane
// and a progress control...
define open generic status-bar-label-pane
    (status-bar :: <status-bar>)
 => (label-pane :: false-or(<labelled-gadget-mixin>));
define open generic status-bar-progress-bar
    (status-bar :: <status-bar>)
 => (progress-bar :: false-or(<progress-bar>));
||#
#||
Moved higher...
(defgeneric status-bar-label-pane (status-bar))
(defgeneric status-bar-progress-bar (status-bar))
||#


#||
// The label of a status bar is the label of the label pane in the
// status bar, if it has one.  Setting the label updates the label pane.
define method gadget-label 
    (status-bar :: <status-bar>) => (message :: false-or(<string>))
  let label-pane = status-bar-label-pane(status-bar);
  label-pane & gadget-label(label-pane)
end method gadget-label;
||#

(defmethod gadget-label ((status-bar <status-bar>))
  (let ((label-pane (status-bar-label-pane status-bar)))
    (and label-pane (gadget-label label-pane))))


#||
define method gadget-label-setter
    (message :: <string>, status-bar :: <status-bar>) => (message :: <string>)
  let label-pane = status-bar-label-pane(status-bar);
  label-pane & (gadget-label(label-pane) := message);
  message
end method gadget-label-setter;
||#

(defmethod (setf gadget-label) ((message string) (status-bar <status-bar>))
  (let ((label-pane (status-bar-label-pane status-bar)))
    (and label-pane (setf (gadget-label label-pane) message)))
  message)


#||
// The value of a status bar returns the value of the progress
// control in the status bar, if it has one.  Setting the value
// updates the progress control.
define method gadget-value
    (status-bar :: <status-bar>) => (value :: false-or(<real>))
  let progress-bar = status-bar-progress-bar(status-bar);
  progress-bar
    & ~sheet-withdrawn?(progress-bar)
    & gadget-value(progress-bar)
end method gadget-value;
||#

(defmethod gadget-value ((status-bar <status-bar>))
"
The value of a status bar returns the value of the progress
control in the status bar, if it has one. Setting the value
updates the progress control.
"
  (let ((progress-bar (status-bar-progress-bar status-bar)))
    (and progress-bar
	 (not (sheet-withdrawn? progress-bar))
	 (gadget-value progress-bar))))


#||
// Allow #f meaning don't display the progress bar if possible
define method normalize-gadget-value
    (status-bar :: <status-bar>, value :: false-or(<real>))
 => (value :: false-or(<real>))
  let progress-bar = status-bar-progress-bar(status-bar);
  if (progress-bar)
    value & normalize-gadget-value(progress-bar, value)
  else
    next-method()
  end
end method normalize-gadget-value;
||#

;; Allow #f meaning don't display the progress bar if possible
(defmethod normalize-gadget-value ((status-bar <status-bar>) (value null))
  (let ((progress-bar (status-bar-progress-bar status-bar)))
    (if progress-bar
	(and value (normalize-gadget-value progress-bar value))
	(call-next-method))))

(defmethod normalize-gadget-value ((status-bar <status-bar>) (value real))
  (let ((progress-bar (status-bar-progress-bar status-bar)))
    (if progress-bar
	(and value (normalize-gadget-value progress-bar value))
	(call-next-method))))


#||
define method do-gadget-value-setter
    (status-bar :: <status-bar>, value :: false-or(<real>)) => ()
  let progress-bar = status-bar-progress-bar(status-bar);
  when (progress-bar)
    if (value)
      sheet-withdrawn?(progress-bar) := #f;
      gadget-value(progress-bar) := value
    else
      sheet-withdrawn?(progress-bar) := #t
    end;
    if (sheet-mapped?(status-bar))
      relayout-children(status-bar);
      sheet-mapped?(progress-bar) := true?(value)
    end
  end
end method do-gadget-value-setter;
||#

(defmethod do-gadget-value-setter ((status-bar <status-bar>) (value null))
  (let ((progress-bar (status-bar-progress-bar status-bar)))
    (when progress-bar
      (setf (sheet-withdrawn? progress-bar) t)
      (when (sheet-mapped? status-bar)
	(relayout-children status-bar)
	(setf (sheet-mapped? progress-bar) nil)))))

(defmethod do-gadget-value-setter ((status-bar <status-bar>) (value real))
  (let ((progress-bar (status-bar-progress-bar status-bar)))
    (when progress-bar
      (setf (sheet-withdrawn? progress-bar) nil
	    (gadget-value progress-bar) value)
      (when (sheet-mapped? status-bar)
	(relayout-children status-bar)
	(setf (sheet-mapped? progress-bar) t)))))


#||
define method gadget-value-range-setter 
    (range :: <range>, status-bar :: <status-bar>)
 => (range :: <range>)
  next-method();
  let progress-bar = status-bar-progress-bar(status-bar);
  when (progress-bar)
    gadget-value-range(progress-bar) := range
  end;
  range
end method gadget-value-range-setter;
||#

(defmethod (setf gadget-value-range) ((range sequence) (status-bar <status-bar>))
  (call-next-method)
  (let ((progress-bar (status-bar-progress-bar status-bar)))
    (when progress-bar
      (setf (gadget-value-range progress-bar) range)))
  range)


#||
/// Status bar pane

define sealed class <status-bar-pane>
    (<status-bar>, <row-layout>)
  inherited slot layout-x-spacing = 4;
  inherited slot layout-y-alignment = #"center";
end class <status-bar-pane>;
||#

;;;---*** It looks to me like this just enables new defaults to
;;;---*** be specified for these slots; they default to 0 and
;;;---*** :top in <horizontal-layout-mixin>. Maybe this should be
;;;---*** done in INITIALIZE-INSTANCE?

;;; FIXME: WORK OUT WHAT'S HAPPENING HERE AND MAKE IT RIGHT

(defclass <status-bar-pane>
    (<status-bar> <row-layout>)
  ((layout-x-spacing :initform 4)             ;; FIXME:INHERITED?
   (layout-y-alignment :initform :center)))   ;; FIXME:INHERITED?


#||
define method class-for-make-pane 
    (framem :: <frame-manager>, class == <status-bar>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<status-bar-pane>, #f)
end method class-for-make-pane;

define sealed domain make (singleton(<status-bar-pane>));
define sealed domain initialize (<status-bar-pane>);
||#

(defmethod class-for-make-pane ((framem <frame-manager>) (class (eql (find-class '<status-bar>))) &key)
  (values (find-class '<status-bar-pane>) nil))



#||
/// Button box [exclusive-choice] .. [inclusive-choice]

define open abstract class <button-box>
    (<gadget-box>, <basic-gadget>)
end class <button-box>;
||#

(defclass <button-box> (<gadget-box> <basic-gadget>)
  ()
  (:documentation
"
The class of grouped buttons; the superclass of <CHECK-BOX> and
<RADIO-BOX>.

The :rows and :columns initargs allow you to specify how many rows or
columns should be used to lay out the buttons. In addition, you can
set the orientation of the button box by specifying the :orientation
initarg.

An instance of the class that is specified by :layout-class is used to
parent the buttons that are created, and any extra arguments that are
specified, such as :x-alignment and :x-spacing, are passed along to
this layout.

You can use the :child initarg to specify a sheet hierarchy to be used
in place of a list of items. Under normal circumstances, the items
defined for any button box are realized in terms of their \"natural\"
gadget class. For example, if you create a radio button box, DUIM
creates a radio button for each item that you specify. By using
the :child initarg, you can define sheet hierarchies that override
these \"natural\" gadget classes, letting you specify more complex
arrangements of gadgets: in this way, you could create a check button
box where each check button is itself surrounded by a group box. For
an example of the use of the :child initarg, look at the initial
dialog box that is displayed when you first start the Dylan
environment. In this dialog, a number of radio buttons are
presented, each delineated by its own group box. In fact, this dialog
is implemented as a radio button box in which the :child initarg has
been used rather than the :items initarg.

If you use :child, then the GADGET-VALUE returned by the gadget is the
GADGET-ID of the selected button. Contrast this with :items, where the
selected item is returned as the GADGET-VALUE.

Examples:

    (contain (make-pane '<button-box>
                        :selection-mode :multiple
                        :items (range :from 0 :to 20)))

The following examples illustrate the use of some of the initargs
described. They each create an instance of a subclass of
<BUTTON-BOX>. Note that the :selection-mode initarg may be used
instead, rather than creating a direct instance of one of the
subclasses.

    (contain (make-pane '<check-box> :items (range :from 1 :to 9)
                        :columns 3))

    (contain (make-pane '<radio-box> :items #(\"Yes\" \"No\")
                        :orientation :vertical))

    (contain (make-pane '<check-box> :items #(1 2 3 4)
                        :layout-class (find-class '<table-layout>)
                        :rows 2))
")
  (:metaclass <abstract-metaclass>))


#||
define function button-box-selection-mode-class
    (selection-mode :: <selection-mode>) => (class :: <class>)
  select (selection-mode)
    #"none"     => <push-box>;
    #"single"   => <radio-box>;
    #"multiple" => <check-box>;
  end;
end function button-box-selection-mode-class;
||#

(defun button-box-selection-mode-class (selection-mode)
  (check-type selection-mode <selection-mode>)
  (ecase selection-mode
    (:none     (find-class '<push-box>))
    (:single   (find-class '<radio-box>))
    (:multiple (find-class '<check-box>))))


#||
define sealed inline method make
    (class == <button-box>, #rest initargs, 
     #key selection-mode :: <selection-mode> = #"none", #all-keys)
 => (button :: <button-box>)
  apply(make, button-box-selection-mode-class(selection-mode), initargs)
end method make;
||#

(defmethod make-pane ((class (eql (find-class '<button-box>)))
		      &rest initargs &key (selection-mode :none) &allow-other-keys)
  (apply #'make-pane (button-box-selection-mode-class selection-mode) initargs))


#||
/// Push box

define open abstract class <push-box> 
    (<action-gadget-mixin>,
     <button-box>,
     <basic-value-gadget>)
end class <push-box>;
||#

(defclass <push-box>
    (<action-gadget-mixin>
     <button-box>
     <basic-value-gadget>)
  ()
  (:documentation
"
The class of grouped push buttons.

The GADGET-VALUE of a push box is always the gadget value of the last
push button in the box to be pressed. You should use the gadget value
of a push box as the way of determining which button has been pressed
in a callback for the push box.

Example:

    (defparameter *push-box*
                  (contain (make-pane '<push-box>
                                      :items (range :from 0 :to 5))))
")
  (:metaclass <abstract-metaclass>))


#||
define sealed method gadget-selection-mode 
    (box :: <push-box>) => (selection-mode :: <selection-mode>)
  #"none"
end method gadget-selection-mode;
||#

(defmethod gadget-selection-mode ((box <push-box>))
  :none)


#||
/// Radio boxes

// Radio box [exclusive-choice] .. [inclusive-choice]
define open abstract class <radio-box>
    (<gadget-selection-mixin>,
     <action-gadget-mixin>,
     <button-box>)
end class <radio-box>;
||#


(defclass <radio-box>
    (<gadget-selection-mixin>
     <action-gadget-mixin>
     <button-box>)
  ()
  (:documentation
"
The instantiable class that implements an abstract radio box, that is,
a gadget that constrains a number of toggle buttons, only one of which
may be selected at any one time.

The value of the radio box is the value of the currently selected item
in the radio box.

Examples:

    (contain (make-pane '<radio-box> :items (list \"Yes\" \"No\")
                        :orientation :vertical))

The following example defines a label-key function which formats the
label of each item in the radio box, rather than just using the item
itself.

    (defparameter *radio-box*
                  (contain (make-pane '<radio-box>
                                      :items (list 1 2 3 4 5)
                                      :orientation :vertical
                                      :label-key
                                      #'(lambda (item)
                                          (format nil \"-- ~d --\"
                                                  item)))))
")
  (:metaclass <abstract-metaclass>))


#||
define sealed method gadget-selection-mode
    (box :: <radio-box>) => (selection-mode :: <selection-mode>)
  #"single"
end method gadget-selection-mode;
||#

(defmethod gadget-selection-mode ((box <radio-box>))
  :single)


#||
/// Check-box

define open abstract class <check-box>
    (<gadget-selection-mixin>,
     <action-gadget-mixin>,
     <button-box>)
end class <check-box>;
||#

(defclass <check-box>
    (<gadget-selection-mixin>
     <action-gadget-mixin>
     <button-box>)
  ()
  (:documentation
"

The instantiable class that implements an abstract check box, that is,
a gadget that constrains a number of toggle buttons, zero or more of
which may be selected at any one time.

The value of a check box is a sequence of all the currently selected
items in the check box.

Examples:

    (contain (make-pane '<check-box> :items #(1 2 3 4 5)))

    (contain (make-pane '<check-box> :items (range :from 1 :to 9)
                        :columns 3))

    (contain (make-pane '<check-box> :items #(1 2 3 4)
                        :layout-class (find-class '<table-layout>)
                        :rows 2))
")
  (:metaclass <abstract-metaclass>))


#||
define sealed method gadget-selection-mode
    (box :: <check-box>) => (selection-mode :: <selection-mode>)
  #"multiple"
end method gadget-selection-mode;
||#

(defmethod gadget-selection-mode ((box <check-box>))
  :multiple)



#||

/// Some useful default methods

define method collection-gadget-default-label-key
    (object) => (name :: <string>)
  format-to-string("%s", object)
end method collection-gadget-default-label-key;
||#

(defmethod collection-gadget-default-label-key (object)
  (format nil "~a" object))


#||
define method collection-gadget-default-label-key
    (string :: <string>) => (name :: <string>)
  string
end method collection-gadget-default-label-key;
||#

(defmethod collection-gadget-default-label-key ((string string))
  string)


#||
define method collection-gadget-default-label-key 
    (n :: <integer>) => (name :: <string>)
  integer-to-string(n)
end method collection-gadget-default-label-key;
||#

(defmethod collection-gadget-default-label-key ((n integer))
  (format nil "~d" n))


#||
define method collection-gadget-default-value-key
    (object) => (value)
  object
end method collection-gadget-default-value-key;
||#

(defmethod collection-gadget-default-value-key (object)
  object)



#||

/// Event handling

define sealed class <selection-changed-gadget-event> (<gadget-event>)
  sealed constant slot event-selection :: <sequence>,
    required-init-keyword: selection:;
end class <selection-changed-gadget-event>;

define sealed domain make (singleton(<selection-changed-gadget-event>));
define sealed domain initialize (<selection-changed-gadget-event>);
||#

(defclass <selection-changed-gadget-event> (<gadget-event>)
  ((event-selection :type sequence :initarg :selection
		    :initform (required-slot ":selection" "<selection-changed-gadget-event>")
		    :reader event-selection)))


#||
define method handle-event
    (gadget :: <gadget-selection-mixin>,
     event :: <selection-changed-gadget-event>) => ()
  gadget-selection(gadget, do-callback?: #t) := event-selection(event);
end method handle-event;
||#

(defmethod handle-event ((gadget <gadget-selection-mixin>) (event <selection-changed-gadget-event>))
  (setf (gadget-selection gadget :do-callback? t) (event-selection event)))


#||
define function distribute-selection-changed-callback
    (gadget :: <gadget-selection-mixin>, selection :: <sequence>) => ()
  distribute-event(port(gadget),
		   make(<selection-changed-gadget-event>,
                        selection: selection,
			gadget: gadget))
end function distribute-selection-changed-callback;
||#

;; FIXME: GO THROUGH LOOKING FOR DISTRIBUTE-* METHODS, CHANGE THEM BACK TO FUNs. OR NOT :)

(defun distribute-selection-changed-callback (gadget selection)
  (check-type gadget <gadget-selection-mixin>)
  (check-type selection sequence)
  (distribute-event (port gadget)
		    (make-instance '<selection-changed-gadget-event>
                                   :selection selection
                                   :gadget gadget)))

