;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-UTILITIES -*-
(in-package #:duim-gadgets-internals)

#||
/// Generic gadget protocols

// By the time we instantiate a gadget, it will be a <sheet> as well...
define open abstract class <abstract-gadget> (<abstract-sheet>) end;
||#

(defclass <abstract-gadget> (<abstract-sheet>)
  ()
  (:metaclass <abstract-metaclass>))


#||
// "Primary" gadget classes should inherit from <gadget>
// "Mixin" gadget classes should inherit from <abstract-gadget>
define protocol-class gadget (<abstract-gadget>)
  /*
  virtual slot gadget-enabled? :: <boolean>;
  virtual slot gadget-label;
  virtual slot gadget-value;
  virtual slot gadget-id;
  virtual slot gadget-client;
  virtual slot gadget-documentation;
 */
end protocol-class gadget;
||#

(define-protocol-class gadget (<abstract-gadget>)
  ()
;;  ((gadget-enabled? :type boolean)
;;   gadget-label
;;   gadget-value
;;   gadget-id
;;   gadget-client
;;   gadget-documentation)
  (:documentation
"
The class of all gadgets. You should not create a direct instance of
this class.

The :id initarg lets you specify a unique identifier for the action
gadget. This is a useful way of identifying gadgets, and provides you
with an additional way of controlling execution of your code, allowing
you to create simple branching statements such as:

    (case gadget-id
      (:ok     (do-okay))
      (:cancel (do-cancel)))

Note, however, that specifying :id is not generally necessary. The :id
initarg is useful in the case of tab controls, where it is returned by
GADGET-VALUE.

Every gadget has a :client that is specified when the gadget is
created. Typically, :client is a frame or a composite sheet.

The :label initarg lets you assign a label to any gadget. A label may
be any string, or an image of an appropriate size (usually a small
icon).

The :documentation initarg is used to provide a short piece of online
help for the gadget. Any documentation supplied for a gadget may be
used in a tooltip or a status bar. For example, moving the mouse over
a menu command may display the supplied documentation for that command
in the status bar of your application, or moving the mouse over any of
the buttons in a tool bar may display a tooltip (a piece of pop-up
text) that contains the supplied documentation.

If :enabled? is T, then the gadget is enabled; that is, the user can
interact with the gadget in an appropriate way. If the gadget is not
enabled, then the user cannot interact with it. For example, if a push
button is not enabled, it cannot be clicked, or if a check box is not
enabled, its setting cannot be switched on or off. Gadgets that are
not enabled are generally grayed out on the screen.

If :read-only? is T, then the user cannot alter any of the values
displayed in the gadget; this typically applies to text gadgets. Note
that this is not the same as disabling the gadget -- if a gadget is
set to read-only, it is not grayed out, and the user may still
interact with it: only the values cannot be changed.
"))


;;; TODO: Ditto for predicate 'gadget?':
#||
Returns true if _object_ is a gadget.

Example:

    (defparameter *gadget*
                  (contain (make-pane '<radio-menu-box>
                                      :items
                                      (range :from 0 :to 20))))
    (gadget? *gadget*)
||#


(defgeneric push-button-like? (gadget))
(defgeneric callback-for-command (command))
(defgeneric activate-gadget (gadget)
  (:documentation
"
Activates _gadget_ by calling the activate callback. For example, in
the case of a button, calling this generic function would be as if the
user had pressed the button.
"))

(defgeneric gadget-pane-default-layout-class (gadget))
(defgeneric gadget-x-alignment (gadget)
  (:documentation
"
Returns the horizontal alignment of _gadget_. You can only set the
horizontal alignment of a gadget when first initializing that gadget,
using the :x-alignment initarg.
"))
(defgeneric gadget-y-alignment (gadget)
  (:documentation
"
Returns the vertical alignment of _gadget_. You can only set the
vertical alignment of a gadget when first initializing that gadget,
using the :y-alignment initarg.
"))
(defgeneric compound-label-size (gadget label))
(defgeneric draw-compound-label (gadget label medium x y &key state brush))
(defgeneric draw-image-label (medium label x y &key state))
(defgeneric gadget-line-scroll-amount (gadget))
(defgeneric gadget-page-scroll-amount (gadget))
(defgeneric gadget-scroll-bars (gadget))
(defgeneric (setf gadget-scroll-bars) (scroll-bars gadget))
(defgeneric border-type (gadget))
(defgeneric (setf border-type) (borders gadget))
(defgeneric update-scroll-bars (sheet))
(defgeneric update-dynamic-scroll-bars (sheet &key relayout?))
(defgeneric compute-dynamic-scroll-bar-values (sheet))
(defgeneric sheet-scroller (sheet))
(defgeneric distribute-activate-callback (gadget))
(defgeneric distribute-value-changed-callback (gadget value))
(defgeneric distribute-value-changing-callback (gadget value))
(defgeneric distribute-key-press-callback (gadget keysym))
(defgeneric distribute-popup-menu-callback (gadget target &key x y))
(defgeneric distribute-update-callback (gadget))


#||

CL NOTE: JUST USE 'MAKE-PANE' AND BE DONE WITH IT

//--- If you change this method, change the one in sheets/frame-managers
define method make
    (pane-class :: subclass(<gadget>),
     #rest pane-options,
     #key port, frame-manager: framem, #all-keys)
 => (pane :: <gadget>)
  dynamic-extent(pane-options);
  let framem = framem
               | *current-frame-manager*
               | port-default-frame-manager(port | default-port())
               | error("Can't find a frame manager to use with 'make-pane'");
  let (concrete-class, concrete-options)
    = apply(class-for-make-pane, framem, pane-class, pane-options);
  // If there's a mapping from the abstract pane class to a concrete pane
  // class, then use it.  Otherwise just try to create a class named by the
  // abstract pane class.
  if (concrete-class == pane-class)
    apply(next-method, pane-class,
	  frame-manager: framem, pane-options)
  else
    //---*** Unfortunately, this recursive call to make will call
    //---*** 'class-for-make-pane' again.  How to speed this up?
    apply(make, concrete-class,
	  frame-manager: framem,
	  concrete-options | pane-options)
  end
end method make;


define constant <gadget-orientation>
    = one-of(#"horizontal", #"vertical", #"none");
||#

(deftype <gadget-orientation> () '(member :horizontal :vertical :none))


#||
define constant <selection-mode> = one-of(#"none", #"single", #"multiple");
||#

(deftype <selection-mode> () '(member :none :single :multiple))


#||
// Currently just for Windows "push-button-like" buttons...
define constant <button-style> = one-of(#f, #"push-button");
||#

(deftype <button-style> () '(member null :push-button))


#||
define constant <mnemonic> = type-union(<character>, <keyboard-gesture>);
||#

(deftype <mnemonic> () '(or character <keyboard-gesture>))


#||
define open abstract class <gadget-state> (<object>)
end class <gadget-state>;
||#

(defclass <gadget-state> ()
  ()
  (:metaclass <abstract-metaclass>))


#||
define protocol <<gadget-protocol>> ()
  getter gadget-enabled?
    (gadget :: <abstract-gadget>) => (enabled? :: <boolean>);
  setter gadget-enabled?-setter
    (enabled? :: <boolean>, gadget :: <abstract-gadget>) => (enabled? :: <boolean>);
  function note-gadget-disabled
    (client, gadget :: <abstract-gadget>) => ();
  function note-gadget-enabled
    (client, gadget :: <abstract-gadget>) => ();
  getter gadget-id
    (gadget :: <abstract-gadget>) => (id);
  setter gadget-id-setter
    (id, gadget :: <abstract-gadget>) => (id);
  getter gadget-client
    (gadget :: <abstract-gadget>) => (client);
  function gadget-client-setter
    (client, gadget :: <abstract-gadget>) => (client);
  getter gadget-documentation
    (gadget :: <abstract-gadget>) => (documentation);
  setter gadget-documentation-setter
    (documentation, gadget :: <abstract-gadget>) => (documentation);
  getter gadget-selection-mode 
    (gadget :: <abstract-gadget>) => (selection-mode :: <selection-mode>);
  getter gadget-default?
    (gadget :: <abstract-gadget>) => (default? :: <boolean>);
  getter gadget-default?-setter
    (default? :: <boolean>, gadget :: <abstract-gadget>)
 => (default? :: <boolean>);
  getter gadget-read-only?
    (gadget :: <abstract-gadget>) => (read-only? :: <boolean>);
  getter gadget-slug-size
    (gadget :: <abstract-gadget>) => (slug-size :: <real>);
  getter gadget-slug-size-setter
    (slug-size :: <real>, gadget :: <abstract-gadget>) => (slug-size :: <real>);
  function note-gadget-slug-size-changed
    (gadget :: <abstract-gadget>) => ();
  // Orientation and labels
  getter gadget-orientation
    (gadget :: <abstract-gadget>) => (orientation :: <gadget-orientation>);
  getter gadget-label
    (gadget :: <abstract-gadget>) => (label);
  setter gadget-label-setter
    (label, gadget :: <abstract-gadget>) => (label);
  function note-gadget-label-changed
    (gadget :: <abstract-gadget>) => ();
  function gadget-label-size 
    (gadget :: <abstract-gadget>, #key do-newlines?, do-tabs?)
 => (width :: <integer>, height :: <integer>);
  function draw-gadget-label
    (gadget :: <abstract-gadget>, medium :: <medium>, x, y,
     #key align-x, align-y, state, do-tabs?, brush, underline?) => ();
  // Accelerators and mnemonics
  getter gadget-accelerator
    (gadget :: <abstract-gadget>) => (accelerator);
  setter gadget-accelerator-setter
    (accelerator, gadget :: <abstract-gadget>) => (accelerator);
  getter defaulted-gadget-accelerator
    (framem :: <abstract-frame-manager>, gadget :: <abstract-gadget>)
 => (accelerator :: false-or(<accelerator>));
  getter gadget-mnemonic
    (gadget :: <abstract-gadget>) => (mnemonic);
  setter gadget-mnemonic-setter
    (mnemonic, gadget :: <abstract-gadget>) => (mnemonic);
  getter defaulted-gadget-mnemonic
    (framem :: <abstract-frame-manager>, gadget :: <abstract-gadget>)
 => (mnemonic :: false-or(<mnemonic>));
  function compute-mnemonic-from-label
    (sheet :: type-union(<abstract-sheet>, <abstract-gadget>), label, #key remove-ampersand?)
 => (label, mnemonic :: false-or(<mnemonic>), index :: false-or(<integer>));
  getter gadget-scrolling?
    (gadget :: <abstract-gadget>)
 => (horizontally? :: <boolean>, vertically? :: <boolean>);
  getter gadget-scrolling-horizontally?
    (gadget :: <abstract-gadget>) => (horizontally? :: <boolean>);
  getter gadget-scrolling-vertically?
    (gadget :: <abstract-gadget>) => (vertically? :: <boolean>);
  getter gadget-lines
    (gadget :: <abstract-gadget>) => (lines :: false-or(<integer>));
  getter gadget-columns
    (gadget :: <abstract-gadget>) => (columns :: false-or(<integer>));
  getter gadget-state
    (gadget :: <abstract-gadget>) => (state :: false-or(<gadget-state>));
  setter gadget-state-setter
    (state :: <gadget-state>, gadget :: <abstract-gadget>) => (state :: <gadget-state>);
end protocol <<gadget-protocol>>;
||#
#||
(define-protocol <<gadget-protocol>> ()
  (:getter gadget-enabled? (gadget))
  (:setter (setf gadget-enabled?) (enabled? gadget))
  (:function note-gadget-disabled (client gadget))
  (:function note-gadget-enabled (client gadget))
  (:getter gadget-id (gadget))
  (:setter (setf gadget-id) (id gadget))
  (:getter gadget-client (gadget))
  (:setter (setf gadget-client) (client gadget))
  (:getter gadget-documentation (gadget))
  (:setter (setf gadget-documentation) (documentation gadget))
  (:getter gadget-selection-mode (gadget))
  (:getter gadget-default? (gadget))
  (:getter (setf gadget-default?) (default? gadget))
  (:getter gadget-read-only? (gadget))
  (:getter gadget-slug-size (gadget))
  (:getter (setf gadget-slug-size) (slug-size gadget))
  (:function note-gadget-slug-size-changed (gadget))
  ;; Orientation and labels
  (:getter gadget-orientation (gadget))
  (:getter gadget-label (gadget))
  (:setter (setf gadget-label) (label gadget))
  (:function note-gadget-label-changed (gadget))
  (:function gadget-label-size (gadget &key do-newlines? do-tabs?))
  (:function draw-gadget-label (gadget medium x y
			       &key align-x align-y state
			       do-tabs? brush underline?))
  ;; Accelerators and mnemonics
  (:getter gadget-accelerator (gadget))
  (:setter (setf gadget-accelerator) (accelerator gadget))
  (:getter defaulted-gadget-accelerator (framem gadget))
  (:getter gadget-mnemonic (gadget))
  (:setter (setf gadget-mnemonic) (mnemonic gadget))
  (:getter defaulted-gadget-mnemonic (framem gadget))
  (:function compute-mnemonic-from-label (sheet label
                                                &key remove-ampersand?))
  (:getter gadget-scrolling? (gadget))
  (:getter gadget-scrolling-horizontally? (gadget))
  (:getter gadget-scrolling-vertically? (gadget))
  (:getter gadget-lines (gadget))
  (:getter gadget-columns (gadget))
  (:getter gadget-state (gadget))
  (:setter (setf gadget-state) (state gadget)))
||#



;;; Moved from lower-down...

;; Bits 0..5 are some basic boolean flags
(defconstant %gadget_enabled   #o01)
(defconstant %gadget_read_only #o02)
(defconstant %default_button   #o04)
(defconstant %show_value       #o10)
(defconstant %tear_off_menu    #o20)
(defconstant %help_menu        #o40)

;; Reuse this bit, since you can't be both a label and a menu
(defconstant %multi_line_label %tear_off_menu)

;; Bits 6..8 is the x-alignment field
(defconstant %x_alignment_shift  6)
(defconstant %x_alignment_mask   #o700)
(defconstant %x_alignment_left   #o000)
(defconstant %x_alignment_right  #o100)
(defconstant %x_alignment_center #o200)

;; Bits 9..11 is the y-alignment field
(defconstant %y_alignment_shift    9)
(defconstant %y_alignment_mask     #o7000)
(defconstant %y_alignment_top      #o0000)
(defconstant %y_alignment_bottom   #o1000)
(defconstant %y_alignment_center   #o2000)
(defconstant %y_alignment_baseline #o3000)

;; Bits 12..14 is the scroll bar field
(defconstant %scroll_bar_shift      12)
(defconstant %scroll_bar_mask       #o70000)
(defconstant %scroll_bar_false      #o00000)
(defconstant %scroll_bar_none       #o10000)
(defconstant %scroll_bar_horizontal #o20000)
(defconstant %scroll_bar_vertical   #o30000)
(defconstant %scroll_bar_both       #o40000)
(defconstant %scroll_bar_dynamic    #o50000)

;; Bits 15..17 is the orientation field
;; define constant %orientation_shift   :: <integer> = 15;
(defconstant %orientation_mask       #o700000)
(defconstant %orientation_none       #o000000)
(defconstant %orientation_horizontal #o100000)
(defconstant %orientation_vertical   #o200000)

;; Bits 18..20 are some text field flags
(defconstant %text_case_shift 18)
(defconstant %text_case_mask  #o3000000)
(defconstant %text_case_false #o0000000)
(defconstant %text_case_lower #o1000000)
(defconstant %text_case_upper #o2000000)
(defconstant %auto_scroll     #o4000000)

;; Bits 21..23 are for gadgets that do their own borders
(defconstant %border_shift   21)
(defconstant %border_mask    #o70000000)
(defconstant %border_default #o00000000)
(defconstant %border_none    #o10000000)
(defconstant %border_sunken  #o20000000)
(defconstant %border_raised  #o30000000)
(defconstant %border_ridge   #o40000000)
(defconstant %border_groove  #o50000000)
(defconstant %border_input   #o60000000)
(defconstant %border_output  #o70000000)

;; Bit 24 is for "push-button-like" gadgets in tool bars
(defconstant %push_button_like #o100000000)

;; Bit 25 tells the gadget to ensure that the selection is visible
(defconstant %keep_selection_visible #o200000000)

(defvar *initial-gadget-flags* (logior %gadget_enabled
				       %show_value
				       %x_alignment_center
				       %y_alignment_top
				       %scroll_bar_both
				       %orientation_horizontal
				       %border_default
				       %keep_selection_visible))



#||

/// Basic gadget definition

// Note that only the concrete gadget classes have a <style-descriptor>,
// which will typically come when <basic-sheet> gets included in the CPL
define open abstract class <basic-gadget> (<gadget>)
  sealed slot gadget-id = #f,
    init-keyword: id:;
  sealed slot gadget-client = #f,
    init-keyword: client:;
  slot gadget-documentation :: false-or(<string>) = #f,
    init-keyword: documentation:,
    setter: %documentation-setter;
  // This stores mostly "static" properties that get used at mirroring time
  sealed slot gadget-flags :: <integer> = $initial-gadget-flags;
end class <basic-gadget>;
||#

;; Note that only the concrete gadget classes have a <style-descriptor>,
;; which will typically come when <basic-sheet> gets included in the CPL
(defclass <basic-gadget> (<gadget>)
  ((gadget-id :initform nil :initarg :id :accessor gadget-id)
   (gadget-client :initform nil :initarg :client :accessor gadget-client)
   (gadget-documentation :type (or null string) :initform nil :initarg :documentation
			 :reader gadget-documentation :writer %documentation-setter)
   ;; This stores mostly "static" properties that get used at mirroring time
   (gadget-flags :type integer :initform *initial-gadget-flags* :accessor gadget-flags))
  (:metaclass <abstract-metaclass>))


#||
// Bits 0..5 are some basic boolean flags
define constant %gadget_enabled   :: <integer> = #o01;
define constant %gadget_read_only :: <integer> = #o02;
define constant %default_button   :: <integer> = #o04;
define constant %show_value       :: <integer> = #o10;
define constant %tear_off_menu    :: <integer> = #o20;
define constant %help_menu        :: <integer> = #o40;

// Reuse this bit, since you can't be both a label and a menu
define constant %multi_line_label :: <integer> = %tear_off_menu;

// Bits 6..8 is the x-alignment field
define constant %x_alignment_shift  :: <integer> = 6;
define constant %x_alignment_mask   :: <integer> = #o700;
define constant %x_alignment_left   :: <integer> = #o000;
define constant %x_alignment_right  :: <integer> = #o100;
define constant %x_alignment_center :: <integer> = #o200;

// Bits 9..11 is the y-alignment field
define constant %y_alignment_shift    :: <integer> = 9;
define constant %y_alignment_mask     :: <integer> = #o7000;
define constant %y_alignment_top      :: <integer> = #o0000;
define constant %y_alignment_bottom   :: <integer> = #o1000;
define constant %y_alignment_center   :: <integer> = #o2000;
define constant %y_alignment_baseline :: <integer> = #o3000;

// Bits 12..14 is the scroll bar field
define constant %scroll_bar_shift      :: <integer> = 12;
define constant %scroll_bar_mask       :: <integer> = #o70000;
define constant %scroll_bar_false      :: <integer> = #o00000;
define constant %scroll_bar_none       :: <integer> = #o10000;
define constant %scroll_bar_horizontal :: <integer> = #o20000;
define constant %scroll_bar_vertical   :: <integer> = #o30000;
define constant %scroll_bar_both       :: <integer> = #o40000;
define constant %scroll_bar_dynamic    :: <integer> = #o50000;

// Bits 15..17 is the orientation field
// define constant %orientation_shift   :: <integer> = 15;
define constant %orientation_mask       :: <integer> = #o700000;
define constant %orientation_none       :: <integer> = #o000000;
define constant %orientation_horizontal :: <integer> = #o100000;
define constant %orientation_vertical   :: <integer> = #o200000;

// Bits 18..20 are some text field flags
define constant %text_case_shift :: <integer> = 18;
define constant %text_case_mask  :: <integer> = #o3000000;
define constant %text_case_false :: <integer> = #o0000000;
define constant %text_case_lower :: <integer> = #o1000000;
define constant %text_case_upper :: <integer> = #o2000000;
define constant %auto_scroll     :: <integer> = #o4000000;

// Bits 21..23 are for gadgets that do their own borders
define constant %border_shift   :: <integer> = 21;
define constant %border_mask    :: <integer> = #o70000000;
define constant %border_default :: <integer> = #o00000000;
define constant %border_none    :: <integer> = #o10000000;
define constant %border_sunken  :: <integer> = #o20000000;
define constant %border_raised  :: <integer> = #o30000000;
define constant %border_ridge   :: <integer> = #o40000000;
define constant %border_groove  :: <integer> = #o50000000;
define constant %border_input   :: <integer> = #o60000000;
define constant %border_output  :: <integer> = #o70000000;

// Bit 24 is for "push-button-like" gadgets in tool bars
define constant %push_button_like :: <integer> = #o100000000;

// Bit 25 tells the gadget to ensure that the selection is visible
define constant %keep_selection_visible :: <integer> = #o200000000;

define constant $initial-gadget-flags :: <integer>
    = logior(%gadget_enabled, %show_value,
	     %x_alignment_center, %y_alignment_top,
	     %scroll_bar_both, %orientation_horizontal,
	     %border_default, %keep_selection_visible);
||#

;;; Moved higher-up

#||
define method initialize
    (gadget :: <basic-gadget>,
     #key enabled? = #t, read-only? = #f, button-style :: <button-style> = #f)
  next-method();
  let bits = logior(if (enabled?)   %gadget_enabled   else 0 end,
		    if (read-only?) %gadget_read_only else 0 end,
		    if (button-style == #"push-button") %push_button_like else 0 end);
  gadget-flags(gadget)
    := logior(logand(gadget-flags(gadget),
		     lognot(logior(%gadget_enabled, %gadget_read_only, %push_button_like))),
	      bits)
end method initialize;
||#

(defmethod initialize-instance :after ((gadget <basic-gadget>)
				       &key (enabled? t) (read-only? nil) (button-style nil)
				       &allow-other-keys)
  (let ((bits (logior (if enabled? %gadget_enabled 0)
		      (if read-only? %gadget_read_only 0)
		      (if (eql button-style :push-button) %push_button_like 0))))
    (setf (gadget-flags gadget)
	  (logior (logand (gadget-flags gadget)
			  (lognot (logior %gadget_enabled %gadget_read_only %push_button_like)))
		  bits))))


#||
define method gadget-state
    (gadget :: <basic-gadget>) => (state :: false-or(<gadget-state>))
  #f
end method gadget-state;
||#

(defmethod gadget-state ((gadget <basic-gadget>))
  nil)


#||
define method gadget-state-setter
    (state :: <gadget-state>, gadget :: <basic-gadget>) => (state :: <gadget-state>)
  state
end method gadget-state-setter;
||#

(defmethod (setf gadget-state) ((state <gadget-state>)
				(gadget <basic-gadget>))
  state)



#||

/// Basic gadget methods

// Make gadgets have no label by default
define method gadget-label (gadget :: <gadget>) => (label)
  #f
end method gadget-label;
||#


(defmethod gadget-label ((gadget <gadget>))
"
Gadgets have no label by default.
"
  nil)


#||
define method gadget-documentation-setter
    (documentation, gadget :: <basic-gadget>) => (documentation :: false-or(<string>))
  gadget.%documentation := documentation
end method gadget-documentation-setter;
||#

(defmethod (setf gadget-documentation) (documentation (gadget <basic-gadget>))
  (%documentation-setter documentation gadget))


#||
define sealed inline method gadget-read-only?
    (gadget :: <basic-gadget>) => (read-only? :: <boolean>)
  logand(gadget-flags(gadget), %gadget_read_only) = %gadget_read_only
end method gadget-read-only?;
||#

(defmethod gadget-read-only? ((gadget <basic-gadget>))
  (= (logand (gadget-flags gadget) %gadget_read_only) %gadget_read_only))


#||
define sealed inline method push-button-like?
    (gadget :: <basic-gadget>) => (push-button-like? :: <boolean>)
  logand(gadget-flags(gadget), %push_button_like) = %push_button_like
end method push-button-like?;
||#

(defmethod push-button-like? ((gadget <gadget>))
  (= (logand (gadget-flags gadget) %push_button_like) %push_button_like))


#||
define method update-gadget (gadget :: <gadget>) => ()
  #f
end method update-gadget;
||#

(defmethod update-gadget ((gadget <gadget>))
  nil)



#||

/// Callbacks

// Could be 'false-or(<class>, <command>, <function>, <list>)', but it's not worth it
define constant <callback-type>   = <object>;
define constant <callback-client> = type-union(<abstract-gadget>, <abstract-frame>);
||#

(deftype <callback-type> () t)
(deftype <callback-client> () '(or <abstract-gadget> <abstract-frame>))

#||
define open generic execute-callback
    (client :: <callback-client>, callback :: <callback-type>, #rest args) => ();
||#

(defgeneric execute-callback (client callback &rest args))

#||
define method execute-callback
    (client :: <callback-client>, function :: <function>, #rest args) => ()
  dynamic-extent(args);
  apply(function, args)
end method execute-callback;
||#

(defmethod execute-callback ((client <abstract-gadget>) (function function) &rest args)
  (declare (dynamic-extent args))
  (apply function args))

(defmethod execute-callback ((client <abstract-frame>) (function function) &rest args)
  (declare (dynamic-extent args))
  (apply function args))


#||
// A little bit of Lisp for you...
define method execute-callback
    (client :: <callback-client>, function :: <list>, #rest args) => ()
  dynamic-extent(args);
  apply(head(function), concatenate(args, tail(function)))
end method execute-callback;
||#

(defmethod execute-callback ((client <abstract-gadget>) (function cons)  ; list)
			     &rest args)
  (declare (dynamic-extent args))
  (apply (car function) (concatenate args (cdr function))))

(defmethod execute-callback ((client <abstract-frame>) (function cons)  ; list)
			     &rest args)
  (declare (dynamic-extent args))
  (apply (car function) (concatenate args (cdr function))))


#||
/// Command callbacks

define method callback-for-command 
    (command :: <function>) => (callback :: <function>, command)
  values(method (sheet)
	   command(sheet-frame(sheet))
	 end method,
	 command)
end method callback-for-command;
||#

(defmethod callback-for-command ((command function))
  (values #'(lambda (sheet)
	      (funcall command (sheet-frame sheet)))
	  command))


#||
define method callback-for-command
    (command :: <command>) => (callback :: <function>, command)
  values(method (sheet)
	   //--- This could copy the command and plug in the new server and client...
	   execute-command(command)
	 end method,
	 command)
end method callback-for-command;
||#

(defmethod callback-for-command ((command <command>))
  (values #'(lambda (sheet)
              (declare (ignore sheet))
              ;;--- This could copy the command and plug in the new server and client...
              (execute-command command))
          command))


#||
define method callback-for-command
    (command-type :: subclass(<command>)) => (callback :: <function>, command)
  values(method (sheet)
	   execute-command-type(command-type, server: sheet-frame(sheet), client: sheet)
	 end method,
	 command-type)
end method callback-for-command;
||#

;; FIXME: MAYBE THIS APPROACH (CATCH INSTANCES OF STANDARD-CLASS) SHOULD BE USED IN
;; OTHER PLACES WHERE THE TYPE OF AN ARG IS A DYLAN 'SUBCLASS' TYPE SPECIFIER?

(defmethod callback-for-command ((command-type standard-class))
  (assert (subtypep command-type (find-class '<command>)))
  (values #'(lambda (sheet)
              (execute-command-type command-type :server (sheet-frame sheet) :client sheet))
          command-type))


#||
define method callback-for-command
    (command-type :: <list>) => (callback :: <function>, command)
  values(method (sheet)
	   execute-command-type(command-type, server: sheet-frame(sheet), client: sheet)
	 end method,
	 head(command-type))
end method callback-for-command;
||#

(defmethod callback-for-command ((command-type cons))
  (values #'(lambda (sheet)
	      (execute-command-type command-type :server (sheet-frame sheet) :client sheet))
	  (car command-type)))



#||

/// Enabling and disabling of gadgets

define sealed inline method gadget-enabled?
    (gadget :: <basic-gadget>) => (enabled? :: <boolean>)
  logand(gadget-flags(gadget), %gadget_enabled) = %gadget_enabled
end method gadget-enabled?;
||#

(defmethod gadget-enabled? ((gadget <basic-gadget>))
  (= (logand (gadget-flags gadget) %gadget_enabled) %gadget_enabled))


#||
define sealed method gadget-enabled?-setter
    (enabled? :: <boolean>, gadget :: <basic-gadget>)
 => (enabled? :: <boolean>)
  let bit = if (enabled?) %gadget_enabled else 0 end;
  when (logand(gadget-flags(gadget), %gadget_enabled) ~= bit)
    gadget-flags(gadget)
      := logior(logand(gadget-flags(gadget), lognot(%gadget_enabled)), bit);
    if (enabled?)
      note-gadget-enabled(gadget-client(gadget), gadget)
    else
      note-gadget-disabled(gadget-client(gadget), gadget)
    end
  end;
  enabled?
end method gadget-enabled?-setter;
||#

(defmethod (setf gadget-enabled?) (enabled? (gadget <basic-gadget>))
  (let ((bit (if enabled? %gadget_enabled 0)))
    (when (not (= (logand (gadget-flags gadget) %gadget_enabled)
		  bit))
      (setf (gadget-flags gadget)
	    (logior (logand (gadget-flags gadget) (lognot %gadget_enabled)) bit))
      (if enabled?
	  (note-gadget-enabled (gadget-client gadget) gadget)
	  (note-gadget-disabled (gadget-client gadget) gadget)))
    enabled?))


#||
define method note-gadget-enabled (client, gadget :: <gadget>) => ()
  ignore(client);
  #f
end method note-gadget-enabled;
||#

(defmethod note-gadget-enabled (client (gadget <gadget>))
  (declare (ignore client))
  nil)


#||
define method note-gadget-disabled (client, gadget :: <gadget>) => ()
  ignore(client);
  #f
end method note-gadget-disabled;
||#

(defmethod note-gadget-disabled (client (gadget <gadget>))
  (declare (ignore client))
  nil)



#||

/// Value gadgets

define open abstract class <value-gadget> (<gadget>)
end class <value-gadget>;

define open abstract class <value-range-gadget> (<value-gadget>)
end class <value-range-gadget>;
||#

(defclass <value-gadget> (<gadget>)
  ()
  (:documentation
"
The class of gadgets that can take values.

The :value initarg specifies the current gadget value. For tab
controls, if the gadget ID is specified, then that is passed as the
gadget value whether or not :value is specified.

The :value-changed-callback initarg is the callback that is invoked
when the gadget value has changed, such as when a scroll bar slug has
come to rest after being dragged, or when the changes to text in a
text field have been committed by pressing the RETURN key.
")
  (:metaclass <abstract-metaclass>))


(defclass <value-range-gadget> (<value-gadget>)
  ()
  (:documentation
"
The class of all value gadgets with ranges. You should not create a
direct instance of this class.

The :value-range initarg is the range of values that the gadget value
of a value range gadget can take. This may be different in any given
situation; when downloading a file or compiling source code, you might
want to use a value range of 0-100, to indicate percentage done (this
is the default). When downloading e-mail messages from a mail server,
however, you may want to use a range equal to the number of messages
being downloaded.

Example:

    (contain (make-pane '<slider>
                        :value-range (range :from -20 :to 20 :by 5)))
")
  (:metaclass <abstract-metaclass>))


#||
define protocol <<value-gadget-protocol>> (<<gadget-protocol>>)
  getter gadget-value
    (gadget :: <value-gadget>) => (value);
  setter gadget-value-setter
    (value, gadget :: <value-gadget>, #key do-callback?) => (value);
  function do-gadget-value-setter
    (gadget :: <value-gadget>, normalized-value) => ();
  getter gadget-value-type
    (gadget :: <value-gadget>) => (type :: <type>);
  getter gadget-label-key
    (gadget :: <value-gadget>) => (label-key :: <function>);
  getter gadget-value-key
    (gadget :: <value-gadget>) => (value-key :: <function>);
  function normalize-gadget-value
    (gadget :: <value-gadget>, value) => (value);
  function note-gadget-value-changed
    (gadget :: <value-gadget>) => ();
  function execute-value-changed-callback
    (gadget :: <value-gadget>, client, id) => ();
  function do-execute-value-changed-callback
    (gadget :: <value-gadget>, client, id) => ();
  getter gadget-value-changed-callback
    (gadget :: <value-gadget>) => (callback :: <callback-type>);
  setter gadget-value-changed-callback-setter
    (callback :: <callback-type>, gadget :: <value-gadget>)
 => (callback :: <callback-type>);
  getter gadget-value-range
    (gadget :: <value-range-gadget>) => (value-range :: <range>);
  getter gadget-value-range-setter 
    (value-range :: <range>, gadget :: <value-range-gadget>)
 => (value-range :: <range>);
  function note-gadget-value-range-changed
    (gadget :: <value-range-gadget>) => ();
  getter gadget-start-value
    (gadget :: <value-range-gadget>) => (start-value :: <real>);
  getter gadget-end-value
    (gadget :: <value-range-gadget>) => (start-value :: <real>);
  getter gadget-value-increment
    (gadget :: <value-range-gadget>) => (increment :: <real>);
  // This one is a little odd, but it is exported...
  getter button-gadget-value (button :: <button>) => (value)
end protocol <<value-gadget-protocol>>;
||#
#||
(define-protocol <<value-gadget-protocol>> (<<gadget-protocol>>)
  (:getter gadget-value (gadget))
  (:setter (setf gadget-value) (value gadget &key do-callback?))
  (:function do-gadget-value-setter (gadget normalized-value))
  (:getter gadget-value-type (gadget))
  (:getter gadget-label-key (gadget))
  (:getter gadget-value-key (gadget))
  (:function normalize-gadget-value (gadget value))
  (:function note-gadget-value-changed (gadget))
  (:function execute-value-changed-callback (gadget client id))
  (:function do-execute-value-changed-callback (gadget client id))
  (:getter gadget-value-changed-callback (gadget))
  (:setter (setf gadget-value-changed-callback) (callback gadget))
  (:getter gadget-value-range (gadget))
  (:getter (setf gadget-value-range) (value-range gadget))
  (:function note-gadget-value-range-changed (gadget))
  (:getter gadget-start-value (gadget))
  (:getter gadget-end-value (gadget))
  (:getter gadget-value-increment (gadget))
  ;; This one is a little odd, but it is exported...
  (:getter button-gadget-value (button)))
||#

#||
// This class can be used for things that act like value gadgets, but whose
// value is computed on the fly
define open abstract class <value-gadget-mixin> (<value-gadget>)
  sealed slot gadget-value-changed-callback :: <callback-type> = #f,
    init-keyword: value-changed-callback:;
end class <value-gadget-mixin>;
||#

;; This class can be used for things that act like value gadgets, but whose
;; value is computed on the fly
(defclass <value-gadget-mixin> (<value-gadget>)
  ((gadget-value-changed-callback :type <callback-type> :initarg :value-changed-callback
				  :initform nil :accessor gadget-value-changed-callback))
  (:metaclass <abstract-metaclass>))


#||
define method initialize (gadget :: <value-gadget-mixin>, #key value)
  next-method();
  // Ensure that gadget value normalization takes place
  gadget-value(gadget) := value | gadget-value(gadget)
end method initialize;
||#

(defmethod initialize-instance :after ((gadget <value-gadget-mixin>)
				       &key value &allow-other-keys)
  ;; Ensure that gadget value normalization takes place
  (setf (gadget-value gadget) (or value (gadget-value gadget))))


#||
define method normalize-gadget-value
    (gadget :: <value-gadget>, value) => (value)
  value
end method normalize-gadget-value;
||#

(defmethod normalize-gadget-value ((gadget <value-gadget>) value)
  value)


#||
// By default, we don't call the value-changed callback
define method gadget-value-setter
    (value, gadget :: <value-gadget-mixin>, #key do-callback? = #f)
 => (value)
  let value = normalize-gadget-value(gadget, value);
  unless (value = gadget-value(gadget))
    do-gadget-value-setter(gadget, value);
    when (do-callback?)
      execute-value-changed-callback(gadget, gadget-client(gadget), gadget-id(gadget))
    end;
    note-gadget-value-changed(gadget)
  end;
  value
end method gadget-value-setter;
||#

;; By default, we don't call the value-changed callback
(defmethod (setf gadget-value) (value (gadget <value-gadget-mixin>) &key (do-callback? nil))
  (let ((value (normalize-gadget-value gadget value)))
    (unless (equal? value (gadget-value gadget))
      (do-gadget-value-setter gadget value)
      (when do-callback?
	(execute-value-changed-callback gadget (gadget-client gadget) (gadget-id gadget)))
      (note-gadget-value-changed gadget))
    value))


#||
define method note-gadget-value-changed
    (gadget :: <value-gadget>) => ()
  #f
end method note-gadget-value-changed;
||#

(defmethod note-gadget-value-changed ((gadget <value-gadget>))
  nil)


#||
// The value-changed callback gets invoked when the value is "definitively"
// changed, such as clicking on a radio button in a radio box or releasing
// the mouse button after dragging a slider
define sealed method execute-value-changed-callback
    (gadget :: <value-gadget-mixin>, client, id) => ()
  ignore(client, id);
  let callback = gadget-value-changed-callback(gadget);
  if (callback)
    execute-callback(gadget, callback, gadget)
  else
    do-execute-value-changed-callback(gadget, client, id)
  end
end method execute-value-changed-callback;
||#

(defmethod execute-value-changed-callback ((gadget <value-gadget-mixin>) client id)
"
The value-changed callback gets invoked when the value is 'definitively'
changed, such as clicking on a radio button in a radio box or releasing
the mouse button after dragging a slider.
"
  (let ((callback (gadget-value-changed-callback gadget)))
    (if callback
	(execute-callback gadget callback gadget)
	(do-execute-value-changed-callback gadget client id))))


#||
define method do-execute-value-changed-callback
    (gadget :: <value-gadget-mixin>, client, id) => ()
  ignore(client, id);
  #f
end method do-execute-value-changed-callback;
||#

(defmethod do-execute-value-changed-callback ((gadget <value-gadget-mixin>) client id)
  (declare (ignore client id))
  nil)


#||
// By default, valued gadgets have unconstrained type
define method gadget-value-type
    (gadget :: <value-gadget>) => (type :: <type>)
  <object>
end method gadget-value-type;
||#

;; By default, valued gadgets have unconstrained type
(defmethod gadget-value-type ((gadget <value-gadget>))
  t)


#||
// By default, it is an error to set a gadget's value... if it is legal,
// then there will be another method below it.
define method gadget-value-setter
    (value, gadget :: <value-gadget>, #key do-callback? = #f)
 => (value)
  ignore(do-callback?);
  error("Setting value for %= which is a class of gadget with no value: %=",
	gadget, value)
end method gadget-value-setter;
||#

;; By default, it is an error to set a gadget's value... if it is legal,
;; then there will be another method below it.
(defmethod (setf gadget-value) (value (gadget <value-gadget>) &key (do-callback? nil))
  (declare (ignore do-callback?))
  (error "Setting value for ~a which is a class of gadget with no value: ~a"
	 gadget value))


#||
define open abstract class <no-value-gadget-mixin> (<value-gadget>)
end class <no-value-gadget-mixin>;
||#

(defclass <no-value-gadget-mixin> (<value-gadget>)
  ()
  (:metaclass <abstract-metaclass>))


#||
define sealed method gadget-value
    (gadget :: <no-value-gadget-mixin>) => (value)
  #f
end method gadget-value;
||#

(defmethod gadget-value ((gadget <no-value-gadget-mixin>))
  nil)


#||
define open abstract class <basic-value-gadget>
    (<value-gadget-mixin>, 
     <basic-gadget>)
  slot gadget-value = #f,
    init-keyword: value:,
    setter: %value-setter;
end class <basic-value-gadget>;
||#

(defclass <basic-value-gadget>
    (<value-gadget-mixin>
     <basic-gadget>)
  ((gadget-value :initform nil :initarg :value :reader gadget-value :writer %value-setter))
  (:metaclass <abstract-metaclass>))


#||
define sealed method do-gadget-value-setter
    (gadget :: <basic-value-gadget>, value) => ()
  gadget.%value := value
end method do-gadget-value-setter;
||#

(defmethod do-gadget-value-setter ((gadget <basic-value-gadget>) value)
  (%value-setter value gadget))


#||
/// Value gadget state

define sealed class <value-gadget-state> (<gadget-state>)
  sealed constant slot %state-value,
    required-init-keyword: value:;
end class <value-gadget-state>;
||#

(defclass <value-gadget-state> (<gadget-state>)
  ((%state-value :initarg :value :initform (required-slot ":value" "<value-gadget-state>") :reader %state-value)))


#||
define sealed domain make (singleton(<value-gadget-state>));
define sealed domain initialize (<value-gadget-state>);

define method gadget-state
    (gadget :: <value-gadget>) => (state :: <value-gadget-state>)
  make(<value-gadget-state>,
       value: gadget-value(gadget))
end method gadget-state;
||#

(defmethod gadget-state ((gadget <value-gadget>))
  (make-instance '<value-gadget-state>
                 :value (gadget-value gadget)))


#||
define method gadget-state-setter
    (state :: <value-gadget-state>, gadget :: <value-gadget>)
 => (state :: <value-gadget-state>)
  gadget-value(gadget) := state.%state-value;
  state
end method gadget-state-setter;
||#

(defmethod (setf gadget-state) ((state <value-gadget-state>) (gadget <value-gadget>))
  (setf (gadget-value gadget) (%state-value state))
  state)



#||

/// Value-changing (formerly "dragging") gadgets

define protocol <<changing-value-gadget-protocol>> ()
  getter gadget-value-changing-callback
    (gadget :: <value-gadget>) => (callback :: <callback-type>);
  getter gadget-value-changing-callback-setter
    (callback :: <callback-type>, gadget :: <value-gadget>)
 => (callback :: <callback-type>);
  function execute-value-changing-callback
    (gadget :: <value-gadget>, client, id) => ();
  function do-execute-value-changing-callback
    (gadget :: <value-gadget>, client, id) => ();
end protocol <<changing-value-gadget-protocol>>;
||#
#||
(define-protocol <<changing-value-gadget-protocol>> ()
  (:getter gadget-value-changing-callback (gadget))
  (:getter (setf gadget-value-changing-callback) (callback gadget))
  (:function execute-value-changing-callback (gadget client id))
  (:function do-execute-value-changing-callback (gadget client id)))
||#


#||
define open abstract class <changing-value-gadget-mixin> (<value-gadget>)
  sealed slot gadget-value-changing-callback :: <callback-type> = #f,
    init-keyword: value-changing-callback:;
end class <changing-value-gadget-mixin>;
||#

(defclass <changing-value-gadget-mixin>
    (<value-gadget>)
  ((gadget-value-changing-callback :type <callback-type> :initform nil :initarg :value-changing-callback
				   :accessor gadget-value-changing-callback))
  (:metaclass <abstract-metaclass>))


#||
// The value-changing callback gets invoked when the value is in the
// process of changing, such as dragging a slider before releasing
// the mouse button or "casual" typing to a text field
define sealed method execute-value-changing-callback 
    (gadget :: <changing-value-gadget-mixin>, client, id) => ()
  ignore(client, id);
  let callback = gadget-value-changing-callback(gadget);
  if (callback)
    execute-callback(gadget, callback, gadget)
  else
    do-execute-value-changing-callback(gadget, client, id)
  end
end method execute-value-changing-callback;
||#

(defmethod execute-value-changing-callback ((gadget <changing-value-gadget-mixin>) client id)
"
The value-changing callback gets invoked when the value is in the
process of changing, such as dragging a slider before releasing
the mouse button or 'casual' typing to a text field.
"
  (let ((callback (gadget-value-changing-callback gadget)))
    (if callback
	(execute-callback gadget callback gadget)
	(do-execute-value-changing-callback gadget client id))))


#||
define method do-execute-value-changing-callback 
    (gadget :: <changing-value-gadget-mixin>, client, id) => ()
  ignore(client, id);
  #f
end method do-execute-value-changing-callback;
||#

(defmethod do-execute-value-changing-callback ((gadget <changing-value-gadget-mixin>) client id)
  (declare (ignore client id))
  nil)



#||

/// Action gadget mixin

define open abstract class <action-gadget> (<gadget>)
end class <action-gadget>;
||#

(defclass <action-gadget> (<gadget>)
  ()
  (:documentation
"
The class used by gadgets that have an action callback that allows
some type of action to be performed, such as a push button. Action
gadgets can only be activated when they are enabled.
")
  (:metaclass <abstract-metaclass>))


#||
define protocol <<action-gadget-protocol>> (<<gadget-protocol>>)
  function execute-activate-callback
    (gadget :: <action-gadget>, client, id) => ();
  function do-execute-activate-callback
    (gadget :: <action-gadget>, client, id) => ();
  getter gadget-activate-callback
    (gadget :: <action-gadget>) => (callback :: <callback-type>);
  setter gadget-activate-callback-setter
    (callback :: <callback-type>, gadget :: <action-gadget>)
 => (callback :: <callback-type>);
end protocol <<action-gadget-protocol>>;
||#
#||
(define-protocol <<action-gadget-protocol>> (<<gadget-protocol>>)
  (:function execute-activate-callback (gadget client id))
  (:function do-execute-activate-callback (gadget client id))
  (:getter gadget-activate-callback (gadget))
  (:setter (setf gadget-activate-callback) (callback gadget)))
||#


#||
define open abstract class <action-gadget-mixin> (<action-gadget>)
  sealed slot gadget-activate-callback :: <callback-type> = #f,
    init-keyword: activate-callback:;
end class <action-gadget-mixin>;
||#

(defclass <action-gadget-mixin> (<action-gadget>)
  ((gadget-activate-callback :type <callback-type> :initform nil :initarg :activate-callback :accessor gadget-activate-callback))
  (:metaclass <abstract-metaclass>))


#||
define sealed method activate-gadget
    (gadget :: <action-gadget-mixin>) => ()
  execute-activate-callback(gadget, gadget-client(gadget), gadget-id(gadget))
end method activate-gadget;
||#

(defmethod activate-gadget ((gadget <action-gadget-mixin>))
  (execute-activate-callback gadget (gadget-client gadget) (gadget-id gadget)))


#||
define sealed method execute-activate-callback
    (gadget :: <action-gadget-mixin>, client, id) => ()
  ignore(client, id);
  let callback = gadget-activate-callback(gadget);
  if (callback)
    execute-callback(gadget, callback, gadget)
  else
    do-execute-activate-callback(gadget, client, id)
  end
end method execute-activate-callback;
||#

(defmethod execute-activate-callback ((gadget <action-gadget-mixin>) client id)
  (let ((callback (gadget-activate-callback gadget)))
    (if callback
	(execute-callback gadget callback gadget)
	(do-execute-activate-callback gadget client id))))


#||
define method do-execute-activate-callback
    (gadget :: <action-gadget-mixin>, client, id) => ()
  ignore(client, id);
  #f
end method do-execute-activate-callback;
||#

(defmethod do-execute-activate-callback ((gadget <action-gadget-mixin>) client id)
  (declare (ignore client id))
  nil)


#||
define open abstract class <basic-action-gadget>
    (<action-gadget-mixin>, <basic-gadget>)
end class <basic-action-gadget>;
||#

(defclass <basic-action-gadget>
    (<action-gadget-mixin> <basic-gadget>)
  ()
  (:metaclass <abstract-metaclass>))



#||

/// "Updatable" gadgets

define protocol <<updatable-gadget-protocol>> (<<gadget-protocol>>)
  function update-gadget
    (gadget :: <abstract-gadget>) => ();
  function execute-update-callback
    (gadget :: <abstract-gadget>, client, id) => ();
  function do-execute-update-callback
    (gadget :: <abstract-gadget>, client, id) => ();
  getter gadget-update-callback
    (gadget :: <abstract-gadget>) => (callback :: <callback-type>);
  setter gadget-update-callback-setter
    (callback :: <callback-type>, gadget :: <abstract-gadget>)
 => (callback :: <callback-type>);
end protocol <<updatable-gadget-protocol>>;
||#
#||
(define-protocol <<updatable-gadget-protocol>> (<<gadget-protocol>>)
  (:function update-gadget (gadget))
  (:function execute-update-callback (gadget client id))
  (:function do-execute-update-callback (gadget client id))
  (:getter gadget-update-callback (gadget))
  (:setter (setf gadget-update-callback) (callback gadget)))
||#


#||
// This class provides an update callback that should cause the gadget to
// update itself based on the current environment.  Useful for popup menus
// to set their selection correctly, or whatever.
define open abstract class <updatable-gadget-mixin> (<abstract-gadget>)
  sealed slot gadget-update-callback :: <callback-type> = #f,
    init-keyword: update-callback:;
end class <updatable-gadget-mixin>;
||#

(defclass <updatable-gadget-mixin>
    (<abstract-gadget>)
  ((gadget-update-callback :type <callback-type> :initform nil :initarg :update-callback :accessor gadget-update-callback))
  (:documentation
"
This class provides an update callback that should cause the gadget to
update itself based on the current environment. Useful for popup menus
to set their selection correctly, or whatever.
")
  (:metaclass <abstract-metaclass>))


#||
define sealed method execute-update-callback
    (gadget :: <updatable-gadget-mixin>, client, id) => ()
  ignore(client, id);
  let callback = gadget-update-callback(gadget);
  if (callback)
    execute-callback(gadget, callback, gadget)
  else
    do-execute-update-callback(gadget, client, id)
  end
end method execute-update-callback;
||#

(defmethod execute-update-callback ((gadget <updatable-gadget-mixin>) client id)
  (let ((callback (gadget-update-callback gadget)))
    (if callback
	(execute-callback gadget callback gadget)
	(do-execute-update-callback gadget client id))))


#||
define method do-execute-update-callback
    (gadget :: <updatable-gadget-mixin>, client, id) => ()
  ignore(client, id);
  #f
end method do-execute-update-callback;
||#

(defmethod do-execute-update-callback ((gadget <updatable-gadget-mixin>) client id)
  (declare (ignore client id))
  nil)



#||

/// Key-press gadget mixins

define protocol <<key-press-gadget-protocol>> (<<gadget-protocol>>)
  function execute-key-press-callback
    (gadget :: <abstract-gadget>, client, id, keysym,
     #rest args, #key, #all-keys) => ();
  function do-execute-key-press-callback
    (gadget :: <abstract-gadget>, client, id, keysym,
     #rest args, #key, #all-keys) => ();
  getter gadget-key-press-callback
    (gadget :: <abstract-gadget>) => (callback :: <callback-type>);
  setter gadget-key-press-callback-setter
    (callback :: <callback-type>, gadget :: <abstract-gadget>)
 => (callback :: <callback-type>);
end protocol <<key-press-gadget-protocol>>;
||#
#||
(define-protocol <<key-press-gadget-protocol>> (<<gadget-protocol>>)
  (:function execute-key-press-callback (gadget client id keysym
				        &rest args
                                        &key &allow-other-keys))
  (:function do-execute-key-press-callback (gadget client id keysym
					   &rest args
                                           &key &allow-other-keys))
  (:getter gadget-key-press-callback (gadget))
  (:setter (setf gadget-key-press-callback) (callback gadget)))
||#


#||
define open abstract class <key-press-gadget-mixin> (<abstract-gadget>)
  sealed slot gadget-key-press-callback :: <callback-type> = #f,
    init-keyword: key-press-callback:;
end class <key-press-gadget-mixin>;
||#

(defclass <key-press-gadget-mixin> (<abstract-gadget>)
  ((gadget-key-press-callback :type <callback-type> :initform nil :initarg :key-press-callback :accessor gadget-key-press-callback))
  (:metaclass <abstract-metaclass>))


#||
// GADGET is the gadget that got key press, and KEYSYM is the keysym
// for the key that got pressed.
define sealed method execute-key-press-callback
    (gadget :: <key-press-gadget-mixin>, client, id, keysym,
     #rest args, #key, #all-keys) => ()
  ignore(client, id);
  let callback = gadget-key-press-callback(gadget);
  if (callback)
    apply(execute-callback, gadget, callback, gadget, keysym, args)
  else
    apply(do-execute-key-press-callback, gadget, client, id, keysym, args)
  end
end method execute-key-press-callback;
||#

(defmethod execute-key-press-callback ((gadget <key-press-gadget-mixin>) client id keysym
				       &rest args &key &allow-other-keys)
"
_gadget_ is the gadget that got the key press, and _keysym_ is the keysym
for the key that got pressed.
"
  (let ((callback (gadget-key-press-callback gadget)))
    (if callback
	(apply #'execute-callback gadget callback gadget keysym args)
	(apply #'do-execute-key-press-callback gadget client id keysym args))))


#||
define method do-execute-key-press-callback
    (gadget :: <key-press-gadget-mixin>, client, id, keysym,
     #rest args, #key, #all-keys) => ()
  ignore(client, id, keysym, args);
  #f
end method do-execute-key-press-callback;
||#

(defmethod do-execute-key-press-callback ((gadget <key-press-gadget-mixin>) client id keysym
					  &rest args &key &allow-other-keys)
  (declare (ignore client id keysym args))
  nil)



#||

/// Popup menu gadget mixins

define protocol <<popup-menu-gadget-protocol>> (<<gadget-protocol>>)
  function execute-popup-menu-callback
    (gadget :: <abstract-gadget>, client, id, target,
     #rest args, #key, #all-keys) => ();
  function do-execute-popup-menu-callback
    (gadget :: <abstract-gadget>, client, id, target,
     #rest args, #key, #all-keys) => ();
  getter gadget-popup-menu-callback
    (gadget :: <abstract-gadget>) => (callback :: <callback-type>);
  setter gadget-popup-menu-callback-setter
    (callback :: <callback-type>, gadget :: <abstract-gadget>)
 => (callback :: <callback-type>);
end protocol <<popup-menu-gadget-protocol>>;
||#
#||
(define-protocol <<popup-menu-gadget-protocol>> (<<gadget-protocol>>)
  (:function execute-popup-menu-callback (gadget client id target
					 &rest args &key &allow-other-keys))
  (:function do-execute-popup-menu-callback (gadget client id target
					    &rest args &key &allow-other-keys))
  (:getter gadget-popup-menu-callback (gadget))
  (:setter (setf gadget-popup-menu-callback) (callback gadget)))
||#


#||
define open abstract class <popup-menu-gadget-mixin> (<abstract-gadget>)
  sealed slot gadget-popup-menu-callback :: <callback-type> = #f,
    init-keyword: popup-menu-callback:;
end class <popup-menu-gadget-mixin>;
||#

(defclass <popup-menu-gadget-mixin>
    (<abstract-gadget>)
  ((gadget-popup-menu-callback :type <callback-type> :initform nil :initarg :popup-menu-callback :accessor gadget-popup-menu-callback))
  (:metaclass <abstract-metaclass>))


#||
// GADGET is the recipient of the callback, and TARGET is the gadget
// that actually got the event.  For example, clicking on a node in a
// tree or list control sends the callback to the control, but the
// target is the node
define sealed method execute-popup-menu-callback
    (gadget :: <popup-menu-gadget-mixin>, client, id, target,
     #rest args, #key, #all-keys) => ()
  ignore(client, id);
  let callback = gadget-popup-menu-callback(gadget);
  if (callback)
    apply(execute-callback, gadget, callback, gadget, target, args)
  else
    apply(do-execute-popup-menu-callback, gadget, client, id, target, args)
  end
end method execute-popup-menu-callback;
||#

(defmethod execute-popup-menu-callback ((gadget <popup-menu-gadget-mixin>) client id target
                                        &rest args &key &allow-other-keys)
"
_gadget_ is the recipient of the callback, and _target_ is the gadget
that actually got the event. For example, clicking on a node in a
tree or list control sends the callback to the control, but the
target is the node.
"
  (let ((callback (gadget-popup-menu-callback gadget)))
    (if callback
        (apply #'execute-callback gadget callback gadget target args)
        (apply #'do-execute-popup-menu-callback gadget client id target args))))


#||
define method do-execute-popup-menu-callback
    (gadget :: <popup-menu-gadget-mixin>, client, id, target,
     #rest args, #key, #all-keys) => ()
  ignore(client, id, target, args);
  #f
end method do-execute-popup-menu-callback;
||#

(defmethod do-execute-popup-menu-callback ((gadget <popup-menu-gadget-mixin>) client id target
                                           &rest args &key &allow-other-keys)
  (declare (ignore client id target args))
  nil)



#||

/// Oriented gadgets

define open abstract class <oriented-gadget-mixin> (<abstract-gadget>)
end class <oriented-gadget-mixin>;
||#

(defclass <oriented-gadget-mixin> (<abstract-gadget>)
  ()
  (:metaclass <abstract-metaclass>))


#||
define method initialize
    (gadget :: <oriented-gadget-mixin>, #key orientation = #"horizontal")
  next-method();
  let o = select (orientation)
	    #"horizontal" => %orientation_horizontal;
	    #"vertical"   => %orientation_vertical;
	    #"none"       => %orientation_none;
	   end;
  gadget-flags(gadget)
    := logior(logand(gadget-flags(gadget), lognot(%orientation_mask)), o)
end method initialize;
||#

(defmethod initialize-instance :after ((gadget <oriented-gadget-mixin>) &key (orientation :horizontal)
				       &allow-other-keys)
  (let ((o (ecase orientation
	     (:horizontal %orientation_horizontal)
	     (:vertical   %orientation_vertical)
	     (:none       %orientation_none))))
    (setf (gadget-flags gadget)
          (logior (logand (gadget-flags gadget) (lognot %orientation_mask)) o))))


#||
define sealed inline method gadget-orientation
    (gadget :: <oriented-gadget-mixin>)
 => (orientation :: <gadget-orientation>)
  select (logand(gadget-flags(gadget), %orientation_mask))
    %orientation_horizontal => #"horizontal";
    %orientation_vertical   => #"vertical";
    %orientation_none       => #"none";
  end
end method gadget-orientation;
||#

(defmethod gadget-orientation ((gadget <oriented-gadget-mixin>))
  (let ((val (logand (gadget-flags gadget) %orientation_mask)))
    (cond ((= val %orientation_horizontal) :horizontal)
          ((= val %orientation_vertical) :vertical)
          ((= val %orientation_none) :none)
          (t (error "The orientation of ~a must be one of horizontal, vertical or none" gadget)))))


#||
define method gadget-pane-default-layout-class
    (gadget :: <oriented-gadget-mixin>) => (class :: <class>)
  select (logand(gadget-flags(gadget), %orientation_mask))
    %orientation_horizontal => <row-layout>;
    %orientation_vertical   => <column-layout>;
    %orientation_none       => <pinboard-layout>;
  end
end method gadget-pane-default-layout-class;
||#

(defmethod gadget-pane-default-layout-class ((gadget <oriented-gadget-mixin>))
  (let ((val (logand (gadget-flags gadget) %orientation_mask)))
    (cond ((= val %orientation_horizontal) (find-class '<row-layout>))
          ((= val %orientation_vertical)   (find-class '<column-layout>))
          ((= val %orientation_none)       (find-class '<pinboard-layout>))
          (t (error "The orientation of ~a must be one of horizontal, vertical or none" gadget)))))



#||

/// Labelled gadgets

define open abstract class <labelled-gadget> (<abstract-gadget>)
end class <labelled-gadget>;
||#

(defclass <labelled-gadget> (<abstract-gadget>)
  ()
  (:metaclass <abstract-metaclass>))


#||
define open abstract class <labelled-gadget-mixin> (<labelled-gadget>)
  sealed slot gadget-label = "",
    init-keyword: label:,
    setter: %label-setter;
end class <labelled-gadget-mixin>;
||#

(defclass <labelled-gadget-mixin> (<labelled-gadget>)
  ((gadget-label :initform "" :initarg :label :reader gadget-label :writer %label-setter))
  (:metaclass <abstract-metaclass>))


#||
define method initialize
    (gadget :: <labelled-gadget-mixin>,
     #key label, pressed-label, armed-label, disabled-label,
          x-alignment = #"center", y-alignment = #"top") => ()
  next-method();
  let xa = select (x-alignment)
	     #"left"  => %x_alignment_left;
	     #"right" => %x_alignment_right;
	     #"center", #"centre" => %x_alignment_center;
	   end;
  let ya = select (y-alignment)
	     #"top"      => %y_alignment_top;
	     #"bottom"   => %y_alignment_bottom;
	     #"baseline" => %y_alignment_baseline;
	     #"center", #"centre" => %y_alignment_center;
	   end;
  gadget-flags(gadget)
    := logior(logand(gadget-flags(gadget),
		     lognot(logior(%x_alignment_mask, %y_alignment_mask))),
	      logior(xa, ya));
  when (pressed-label | armed-label | disabled-label)
    gadget.%label := make(<image-label>,
			  normal-label:   label,
			  pressed-label:  pressed-label  | label,
			  armed-label:    armed-label    | label,
			  disabled-label: disabled-label | label)
  end
end method initialize;
||#

(defmethod initialize-instance :after ((gadget <labelled-gadget-mixin>)
				       &key label pressed-label armed-label disabled-label
				       (x-alignment :center) (y-alignment :top)
				       &allow-other-keys)
  (let ((xa (ecase x-alignment
	      (:left %x_alignment_left)
	      (:right %x_alignment_right)
	      ((:center :centre) %x_alignment_center)))
        (ya (ecase y-alignment
	      (:top %y_alignment_top)
	      (:bottom %y_alignment_bottom)
	      (:baseline %y_alignment_baseline)
	      ((:center :centre) %y_alignment_center))))
    (setf (gadget-flags gadget)
          (logior (logand (gadget-flags gadget)
                          (lognot (logior %x_alignment_mask %y_alignment_mask)))
                  (logior xa ya))))
  (when (or pressed-label armed-label disabled-label)
    (%label-setter (make-pane '<image-label>
			      :normal-label label
			      :pressed-label (or pressed-label label)
			      :armed-label (or armed-label label)
			      :disabled-label (or disabled-label label))
		   gadget)))


#||
define sealed method gadget-label-setter 
    (label, gadget :: <labelled-gadget-mixin>) => (label)
  gadget.%label := label;
  note-gadget-label-changed(gadget);
  label
end method gadget-label-setter;
||#

(defmethod (setf gadget-label) (label (gadget <labelled-gadget-mixin>))
  (%label-setter label gadget)
  (note-gadget-label-changed gadget)
  label)


#||
define method note-gadget-label-changed (gadget :: <gadget>) => ()
  #f
end method note-gadget-label-changed;
||#

(defmethod note-gadget-label-changed ((gadget <gadget>))
  nil)


#||
// Must be in the same order as the x alignment constants
define constant $x-alignments :: <simple-object-vector>
    = #[#"left", #"right", #"center"];
||#

(defvar *x-alignments* #(:left :right :center))


#||
define sealed inline method gadget-x-alignment
    (gadget :: <labelled-gadget-mixin>) => (alignment :: <x-alignment>)
  let index = ash(logand(gadget-flags(gadget), %x_alignment_mask),
		  -%x_alignment_shift);
  $x-alignments[index]
end method gadget-x-alignment;
||#

(defmethod gadget-x-alignment ((gadget <labelled-gadget-mixin>))
  (let ((index (ash (logand (gadget-flags gadget) %x_alignment_mask)
		    (- %x_alignment_shift))))
    (aref *x-alignments* index)))


#||
// Must be in the same order as the y alignment constants
define constant $y-alignments :: <simple-object-vector>
    = #[#"top", #"bottom", #"center", #"baseline"];
||#

(defvar *y-alignments* #(:top :bottom :center :baseline))


#||
define sealed inline method gadget-y-alignment
    (gadget :: <labelled-gadget-mixin>) => (alignment :: <y-alignment>)
  let index = ash(logand(gadget-flags(gadget), %y_alignment_mask),
		  -%y_alignment_shift);
  $y-alignments[index]
end method gadget-y-alignment;
||#

(defmethod gadget-y-alignment ((gadget <labelled-gadget-mixin>))
  (let ((index (ash (logand (gadget-flags gadget) %y_alignment_mask)
		    (- %y_alignment_shift))))
    (aref *y-alignments* index)))


#||
define method gadget-label-size
    (gadget :: <labelled-gadget-mixin>,
     #key do-newlines? = #f, do-tabs? = #t)
 => (width :: <integer>, height :: <integer>)
  let label = gadget-label(gadget) | "";
  select (label by instance?)
    <string> =>
      let _port = port(gadget);
      let text-style = get-default-text-style(_port, gadget);
      // Use the computed width of the label, but the height of the font
      // Add a few pixels in each direction to keep the label from being squeezed
      values(ceiling(text-size(_port, label,
			       text-style: text-style,
			       do-newlines?: do-newlines?, do-tabs?: do-tabs?)) + 2,
	     ceiling(font-height(text-style, _port)) + 2);
    <image> =>
      values(image-width(label), image-height(label));
    <compound-label> =>
      compound-label-size(gadget, label);
    <image-label> =>
      values(image-width(label.%normal-label), image-height(label.%normal-label));
  end
end method gadget-label-size;
||#

(defmethod gadget-label-size ((gadget <labelled-gadget-mixin>)
                              &key (do-newlines? nil) (do-tabs? t))
  (let ((label (or (gadget-label gadget) "")))
    (etypecase label
      (string
       (let* ((_port (port gadget))
              (text-style (get-default-text-style _port gadget)))
         ;; Use the computed width of the label, but the height of the font
         ;; Add a few pixels in each direction to keep the label from being squeezed
         (values (+ 2 (ceiling (text-size _port label
					  :text-style text-style
					  :do-newlines? do-newlines? :do-tabs? do-tabs?)))
                 (+ 2 (ceiling (font-height text-style _port))))))
      (<image>
       (values (image-width label) (image-height label)))
      (<compound-label>
       (compound-label-size gadget label))
      (<image-label>
       (values (image-width (%normal-label label)) (image-height (%normal-label label)))))))


#||
define method draw-gadget-label
    (gadget :: <labelled-gadget-mixin>, medium :: <medium>, x, y,
     #key align-x = #"left", align-y = #"top", state = #"normal",
	  do-tabs? = #t, brush, underline? = #f) => ()
  let label = gadget-label(gadget);
  select (label by instance?)
    <string> =>
      with-drawing-options (medium,
			    brush: brush | default-foreground(gadget),
			    text-style: default-text-style(gadget))
	draw-text(medium, label, x, y,
		  align-x: align-x, align-y: align-y,
		  do-tabs?: do-tabs?);
	when (underline?)
	  let _port = port(gadget);
	  let text-style = get-default-text-style(_port, gadget);
	  let (width, height)
	    = text-size(_port, label,
			text-style: text-style,
			do-newlines?: #f, do-tabs?: do-tabs?);
	  let descent = font-descent(text-style, _port) - 1;
	  //---*** This needs to handle other alignments!
	  draw-line(medium, x, y + height - descent, x + width, y + height - descent)
	end
      end;
    <image> =>
      let width  = image-width(label);
      let height = image-height(label);
      select (align-x)
        #"left" => #f;
        #"right" => dec!(x, width);
        #"center", #"centre" => dec!(x, truncate/(width, 2))
      end;
      select (align-y)
        #"top", #"baseline" => #f;
        #"bottom" => dec!(x, height);
        #"center", #"centre" => dec!(x, truncate/(height, 2))
      end;
      draw-image(medium, label, x, y);
    <compound-label> =>
      draw-compound-label(gadget, label, medium, x, y, state: state, brush: brush);
    <image-label> =>
      draw-image-label(medium, label, x, y, state: state);
    singleton(#f) =>
      #f;
  end
end method draw-gadget-label;
||#

(defmethod draw-gadget-label ((gadget <labelled-gadget-mixin>) (medium <medium>) x y
                              &key (align-x :left) (align-y :top) (state :normal)
                              (do-tabs? t) brush (underline? nil))
  (let ((label (gadget-label gadget)))
    (typecase label
      (string (with-drawing-options (medium
				     :brush (or brush (default-foreground gadget))
				     :text-style (default-text-style gadget))
		(draw-text medium label x y
			   :align-x align-x :align-y align-y
			   :do-tabs? do-tabs?)
		(when underline?
		  (let* ((_port (port gadget))
			 (text-style (get-default-text-style _port gadget)))
		    (multiple-value-bind (width height)
			(text-size _port label
				   :text-style text-style
				   :do-newlines? nil :do-tabs? do-tabs?)
		      (let ((descent (- (font-descent text-style _port) 1)))
			;;---*** This needs to handle other alignments!
			(draw-line medium x (- (+ y height) descent) (+ x width) (- (+ y height) descent))))))))
      (<image> (let ((width (image-width label))
		     (height (image-height label)))
		 (case align-x
		   (:left nil)
		   (:right (decf x width))
		   ((:center :centre) (decf x (truncate width 2))))
		 (case align-y
		   (:top nil)
		   (:baseline nil)
		   (:bottom (decf x height))
		   ((:center :centre) (decf x (truncate height 2))))
		 (draw-image medium label x y)))
      (<compound-label> (draw-compound-label gadget label medium x y :state state :brush brush))
      (<image-label> (draw-image-label medium label x y :state state))
      (t nil))))


#||
/// Compound labels

define sealed class <compound-label> (<object>)
  sealed constant slot %text :: <string>,
    required-init-keyword: text:;
  sealed constant slot %image :: <image>,
    required-init-keyword: image:;
  sealed constant slot %orientation :: one-of(#"horizontal", #"vertical") = #"horizontal",
    init-keyword: orientation:;
end class <compound-label>;

define sealed domain make (singleton(<compound-label>));
define sealed domain initialize (<compound-label>);
||#

(defclass <compound-label> ()
  ((%text :type string :initarg :text :initform (required-slot ":text" "<compound-label>") :reader %text)
   (%image :type <image> :initarg :image :initform (required-slot ":image" "<compound-label>") :reader %image)
   (%orientation :type (member :horizontal :vertical) :initform :horizontal :initarg :orientation :reader %orientation)))


#||
define sealed method compound-label-size
    (gadget :: <labelled-gadget-mixin>, label :: <compound-label>)
 => (width :: <integer>, height :: <integer>)
  let text  = label.%text;
  let image = label.%image;
  let _port = port(gadget);
  let text-style = get-default-text-style(_port, gadget);
  let (tw, th) = text-size(_port, text, text-style: text-style);
  let (iw, ih) = values(image-width(image), image-height(image));
  if (label.%orientation == #"horizontal")
    // Add a little slop so that label doesn't get too squeezed
    values(ceiling(iw + tw) + 2 + 1,
	   ceiling(max(font-height(text-style, _port), ih)) + 1)
  else
    values(ceiling(max(iw, tw) + 2),
	   ceiling(ih + font-height(text-style, _port)) + 2 + 1)
  end
end method compound-label-size;
||#

(defmethod compound-label-size ((gadget <labelled-gadget-mixin>) (label <compound-label>))
  (let* ((text       (%text label))
         (image      (%image label))
         (_port      (port gadget))
         (text-style (get-default-text-style _port gadget)))
    (multiple-value-bind (tw th)
        (text-size _port text :text-style text-style)
      (declare (ignore th))
      (multiple-value-bind (iw ih)
          (values (image-width image) (image-height image))
        (if (eql (%orientation label) :horizontal)
            ;; Add a little slop so that label doesn't get too squeezed
            (values (+ (ceiling (+ iw tw)) 2 1)
                    (+ (ceiling (max (font-height text-style _port) ih)) 1))
	    ;; else
            (values (ceiling (+ (max iw tw) 2))
	            (+ (ceiling (+ ih (font-height text-style _port))) 2 1)))))))


#||
define sealed method draw-compound-label
    (gadget :: <labelled-gadget-mixin>, label :: <compound-label>, medium :: <medium>, x, y,
     #key state = #"normal", brush) => ()
  let text  = label.%text;
  let image = label.%image;
  let (iw, ih) = values(image-width(image), image-height(image));
  draw-image(medium, image, x, y);
  with-drawing-options (medium,
			brush: brush | default-foreground(gadget),
			text-style: default-text-style(gadget))
    if (label.%orientation == #"horizontal")
      draw-text(medium, text, x + iw + 2, y + floor/(ih, 2),
		align-x: #"left", align-y: #"center")
    else
      draw-text(medium, text, x + floor/(iw, 2) + 2, y + ih + 2,
		align-x: #"center", align-y: #"top")
    end
  end
end method draw-compound-label;
||#

(defmethod draw-compound-label ((gadget <labelled-gadget-mixin>) (label <compound-label>) (medium <medium>) x y
                                &key (state :normal) brush)
  (declare (ignore state))
  (let ((text  (%text label))
        (image (%image label)))
    (multiple-value-bind (iw ih)
        (values (image-width image) (image-height image))
      (draw-image medium image x y)
      (with-drawing-options (medium
                             :brush (or brush (default-foreground gadget))
                             :text-style (default-text-style gadget))
        (if (eql (%orientation label) :horizontal)
            (draw-text medium text (+ x iw 2) (+ y (floor ih 2))
                       :align-x :left :align-y :center)
            (draw-text medium text (+ x (floor iw 2) 2) (+ y ih 2)
                       :align-x :center :align-y :top))))))


#||
/// Image labels

define sealed class <image-label> (<object>)
  sealed constant slot %normal-label,
    required-init-keyword: normal-label:;
  sealed constant slot %pressed-label = #f,
    init-keyword: pressed-label:;
  sealed constant slot %armed-label = #f,
    init-keyword: armed-label:;
  sealed constant slot %disabled-label = #f,
    init-keyword: disabled-label:;
end class <image-label>;

define sealed domain make (singleton(<image-label>));
define sealed domain initialize (<image-label>);
||#

(defclass <image-label> ()
  ((%normal-label :initarg :normal-label :initform (required-slot ":normal-label" "<image-label>") :reader %normal-label)
   (%pressed-label :initarg :pressed-label :initform nil :reader %pressed-label)
   (%armed-label :initarg :armed-label :initform nil :reader %armed-label)
   (%disabled-label :initarg :disabled-label :initform nil :reader %disabled-label)))


#||
define method draw-image-label (medium, label :: <image-label>, x, y,
				#key state = #"normal") => ()
  let image = select (state)
		#"normal"   => label.%normal-label;
		#"pressed"  => label.%pressed-label;
		#"armed"    => label.%armed-label;
		#"disabled" => label.%disabled-label;
	      end;
  draw-image(medium, image, x, y)
end method draw-image-label;
||#

(defmethod draw-image-label (medium (label <image-label>) x y
                             &key (state :normal))
  (let ((image (ecase state
		 (:normal   (%normal-label label))
		 (:pressed  (%pressed-label label))
		 (:armed    (%armed-label label))
		 (:disabled (%disabled-label label)))))
    (draw-image medium image x y)))



#||

/// Mixins for accelerators and mnemonics

define constant <accelerator> = type-union(<character>, <keyboard-gesture>);
||#

(deftype <accelerator> () '(or character <keyboard-gesture>))


#||
define open abstract class <accelerator-mixin> (<abstract-gadget>)
  sealed slot gadget-accelerator = $unsupplied,
    init-keyword: accelerator:;
end class <accelerator-mixin>;
||#

(defclass <accelerator-mixin>
    (<abstract-gadget>)
  ((gadget-accelerator :initarg :accelerator :initform :unsupplied :accessor gadget-accelerator))
  (:metaclass <abstract-metaclass>))


#||
define method defaulted-gadget-accelerator
    (framem :: <frame-manager>, gadget :: <accelerator-mixin>)
 => (accelerator :: false-or(<accelerator>))
  supplied?(gadget-accelerator(gadget)) & gadget-accelerator(gadget)
end method defaulted-gadget-accelerator;
||#

(defmethod defaulted-gadget-accelerator ((framem <frame-manager>) (gadget <accelerator-mixin>))
  (and (supplied? (gadget-accelerator gadget)) (gadget-accelerator gadget)))


#||
define open abstract class <mnemonic-mixin> (<abstract-gadget>)
  sealed slot gadget-mnemonic = $unsupplied,
    init-keyword: mnemonic:;
end class <mnemonic-mixin>;
||#

(defclass <mnemonic-mixin> (<abstract-gadget>)
  ((gadget-mnemonic :initarg :mnemonic :initform :unsupplied :accessor gadget-mnemonic))
  (:metaclass <abstract-metaclass>))


#||
define method defaulted-gadget-mnemonic
    (framem :: <frame-manager>, gadget :: <mnemonic-mixin>)
 => (mnemonic :: false-or(<mnemonic>))
  supplied?(gadget-mnemonic(gadget)) & gadget-mnemonic(gadget)
end method defaulted-gadget-mnemonic;
||#

(defmethod defaulted-gadget-mnemonic ((framem <frame-manager>) (gadget <mnemonic-mixin>))
  (and (supplied? (gadget-mnemonic gadget)) (gadget-mnemonic gadget)))


#||
// This just gets called by the backend to compute its mnemonic
// The index is the position at which the mnemonic character is in the [new] label
define method compute-mnemonic-from-label
    (sheet :: <sheet>, label, #key remove-ampersand? = #f)
 => (label, mnemonic :: false-or(<mnemonic>), index :: false-or(<integer>))
  select (label by instance?)
    <string> =>
      let ampersand = position(label, '&');
      if (ampersand & (ampersand < size(label) - 1))
	if (remove-ampersand?)
	  values(remove(label, '&', count: 1), label[ampersand + 1], ampersand)
	else
	  values(label, label[ampersand + 1], ampersand + 1)
	end
      else
	values(label, #f, #f)
      end;
    otherwise =>
      values(label, #f, #f);
  end
end method compute-mnemonic-from-label;
||#

;; This just gets called by the backend to compute its mnemonic
;; The index is the position at which the mnemonic character is in the [new] label
(defmethod compute-mnemonic-from-label ((sheet <sheet>) label &key (remove-ampersand? nil))
  ;; FIXME: is there no way to permit an '&' in the label
  ;; without it being considered to be a mnemonic?
  ;; FIXME: TEST THIS ^^^
  (typecase label
    (string (let ((ampersand (position #\& label)))
	      (if (and ampersand (< ampersand (- (length label) 1)))
		  (if remove-ampersand?
		      (values (remove #\& label :count 1) (aref label (+ ampersand 1)) ampersand)
		      (values label (aref label (+ ampersand 1)) (+ ampersand 1)))
		  ;; else
		  (values label nil nil))))
    (t (values label nil nil))))


#||
// An explicitly supplied mnemonic wins out over "&", and
// effectively forces the "&" out of the label if there is one
define method compute-mnemonic-from-label
    (sheet :: <mnemonic-mixin>, label, #key remove-ampersand? = #f)
 => (label, mnemonic :: false-or(<mnemonic>), index :: false-or(<integer>))
  ignore(label, remove-ampersand?);
  let explicit-mnemonic
    = supplied?(gadget-mnemonic(sheet)) & gadget-mnemonic(sheet);
  let remove-ampersand?
    = (explicit-mnemonic & #t) | remove-ampersand?;
  let (label, mnemonic, index)
    = next-method(sheet, label, remove-ampersand?: remove-ampersand?);
  values(label, explicit-mnemonic | mnemonic, ~explicit-mnemonic & index)
end method compute-mnemonic-from-label;
||#

;; An explicitly supplied mnemonic wins out over "&", and
;; effectively forces the "&" out of the label if there is one
(defmethod compute-mnemonic-from-label ((sheet <mnemonic-mixin>) label &key (remove-ampersand? nil))
  (let* ((explicit-mnemonic (and (supplied? (gadget-mnemonic sheet)) (gadget-mnemonic sheet)))
	 (remove-ampersand? (or (and explicit-mnemonic t) remove-ampersand?)))
    (multiple-value-bind (label mnemonic index)
        (call-next-method sheet label :remove-ampersand? remove-ampersand?)
      (values label (or explicit-mnemonic mnemonic) (and (not explicit-mnemonic) index)))))



#||

/// Command table support

// Some things here get used in command table menus.  We need a back-pointer
// from the gadget to the command so that we can enabled/disable the gadget
// when, for example, a command gets enabled/disabled.  Note that the command
// can actually be a <class>, <command>, <function>, or <command-table>.
define open abstract class <gadget-command-mixin> (<abstract-gadget>)
  sealed slot gadget-command = #f,
    init-keyword: command:;
end class <gadget-command-mixin>;
||#

(defclass <gadget-command-mixin> (<abstract-gadget>)
  ((gadget-command :initarg :command :initform nil :accessor gadget-command))
  (:documentation
"
Some things here get used in command table menus. We need a back-pointer
from the gadget to the command so that we can enable/disable to gadget
when, for example, a command gets enabled/disabled. Note that the command
can actually be a <class>, <command>, <function>, or <command-table>.
")
  (:metaclass <abstract-metaclass>))


#||
define method gadget-command (gadget :: <gadget>) => (command)
  #f
end method gadget-command;
||#

(defmethod gadget-command ((gadget <gadget>))
  nil)



#||

/// Range gadgets

// In effect, the default is a continuous slider ranging from 0 to 1
//--- Maybe 0 to 100 by 1 would be more generally useful...
define constant $default-value-range = range(from: 0.0, to: 1.0, by: 0.001);
||#

(defparameter *default-value-range* (range :from 0 :to 100 :by 1))


#||
//--- We need a general way of changing the range and the value together
define open abstract class <range-gadget-mixin> (<value-range-gadget>)
  sealed slot gadget-value-range :: <range> = $default-value-range,
    init-keyword: value-range:,
    setter: %value-range-setter;
end class <range-gadget-mixin>;
||#

(defclass <range-gadget-mixin>
    (<value-range-gadget>)
  ((gadget-value-range :type (or sequence <range>) :initarg :value-range :initform *default-value-range*
		       :reader gadget-value-range :writer %value-range-setter))
  (:metaclass <abstract-metaclass>))


#||
//--- Can we compute this from the underlying range object?
define method gadget-value-type
    (gadget :: <range-gadget-mixin>) => (type :: <type>)
  <real>
end method gadget-value-type;
||#

(defmethod gadget-value-type ((gadget <range-gadget-mixin>))
  (find-class 'real))


#||
define sealed method gadget-value-range-setter
    (range :: <range>, gadget :: <range-gadget-mixin>) => (range :: <range>)
  unless (range = gadget-value-range(gadget))
    gadget.%value-range := range;
    note-gadget-value-range-changed(gadget)
  end;
  range
end method gadget-value-range-setter;
||#

(defmethod (setf gadget-value-range) ((range sequence) (gadget <range-gadget-mixin>))
  (unless (equal? range (gadget-value-range gadget))
    (%value-range-setter range gadget)
    (note-gadget-value-range-changed gadget))
  range)


#||
define method note-gadget-value-range-changed 
    (gadget :: <value-range-gadget>) => ()
  let value = gadget-value(gadget);
  let normalized-value = normalize-gadget-value(gadget, value);
  when (value ~= normalized-value)
    gadget-value(gadget) := normalized-value
  end
end method note-gadget-value-range-changed;
||#

(defmethod note-gadget-value-range-changed ((gadget <value-range-gadget>))
  (let* ((value (gadget-value gadget))
         (normalized-value (normalize-gadget-value gadget value)))
    ;; These values are sometimes NIL, so we can't use '=' on them.
    ;; TODO: Check that NIL values are acceptable values.
    (when (not (equal? value normalized-value))
      (setf (gadget-value gadget) normalized-value))))


#||
define method initialize (gadget :: <range-gadget-mixin>, #key) => ()
  next-method();
  let range-size = size(gadget-value-range(gadget));
  assert(range-size & range-size > 0,
	 "Unbounded or empty value range for %=", gadget)
end method initialize;
||#

;;; TODO: Changing from a Dylan-ish <range> to a CL sequence means we
;;; lose unbounded ranges. Does that matter?

(defmethod initialize-instance :after ((gadget <range-gadget-mixin>) &key &allow-other-keys)
  (let ((range-size (size (gadget-value-range gadget))))
    (unless (and range-size (> range-size 0))
      (error "Unbounded or empty value range from ~a" gadget))))


#||
define sealed method gadget-start-value 
    (gadget :: <range-gadget-mixin>) => (start-value :: <real>)
  let range = gadget-value-range(gadget);
  range[0]
end method gadget-start-value;
||#

(defmethod gadget-start-value ((gadget <range-gadget-mixin>))
  (let ((range (gadget-value-range gadget)))
    (elt range 0)))


#||
define sealed method gadget-end-value 
    (gadget :: <range-gadget-mixin>) => (end-value :: <real>)
  let range = gadget-value-range(gadget);
  range[size(range) - 1]
end method gadget-end-value;
||#

(defmethod gadget-end-value ((gadget <range-gadget-mixin>))
  (let ((range (gadget-value-range gadget)))
    (elt range (- (size range) 1))))


#||
define sealed method gadget-value-increment 
    (gadget :: <range-gadget-mixin>) => (increment :: <real>)
  let range = gadget-value-range(gadget);
  if (size(range) <= 1) 0 else range[1] - range[0] end
end method gadget-value-increment;
||#

(defmethod gadget-value-increment ((gadget <range-gadget-mixin>))
  (let ((range (gadget-value-range gadget)))
    (if (<= (size range) 1) 0 (- (elt range 1) (elt range 0)))))


#||
define method normalize-gadget-value
    (gadget :: <range-gadget-mixin>, value == #f) => (value :: <real>)
  gadget-start-value(gadget)
end method normalize-gadget-value;
||#

(defmethod normalize-gadget-value ((gadget <range-gadget-mixin>) (value null))
  (gadget-start-value gadget))


#||
define method normalize-gadget-value 
    (gadget :: <range-gadget-mixin>, value :: <number>) => (value :: <real>)
  let range-start = gadget-start-value(gadget);
  let range-end = gadget-end-value(gadget);
  let min-value = min(range-start, range-end);
  let max-value = max(range-start, range-end);
  let minimized-value = max(value, min-value);
  let maximized-value = min(minimized-value, max-value);
  maximized-value
end method normalize-gadget-value;
||#

(defmethod normalize-gadget-value ((gadget <range-gadget-mixin>) (value real))
  (let* ((range-start (gadget-start-value gadget))
         (range-end   (gadget-end-value gadget))
         (min-value   (min range-start range-end))
         (max-value   (max range-start range-end))
         (minimized-value (max value min-value))
         (maximized-value (min minimized-value max-value)))
    maximized-value))


#||
//--- This is unfortunate, but we don't really want to pass the
//--- orientation through at this point.  Any better ideas?
//--- There should maybe be a user-specifiable way to set this.
define method gadget-line-scroll-amount
    (gadget :: <range-gadget-mixin>) => (amount :: <integer>)
  line-scroll-amount(gadget, gadget-orientation(gadget))
end method gadget-line-scroll-amount;
||#

(defmethod gadget-line-scroll-amount ((gadget <range-gadget-mixin>))
  (line-scroll-amount gadget (gadget-orientation gadget)))


#||
//--- There should maybe be a user-specifiable way to set this
define method gadget-page-scroll-amount
    (gadget :: <range-gadget-mixin>) => (amount :: <integer>)
  page-scroll-amount(gadget, gadget-orientation(gadget))
end method gadget-page-scroll-amount;
||#

(defmethod gadget-page-scroll-amount ((gadget <range-gadget-mixin>))
  (page-scroll-amount gadget (gadget-orientation gadget)))


#||
// Why doesn't Dylan have this, I wonder?
define function range-values
    (range :: <range>)
 => (value-range :: <real>, start-value :: <real>, end-value :: <real>, increment :: <real>)
  let start-value = range[0];
  let end-value   = range[size(range) - 1];
  let value-range = abs(start-value - end-value);
  let increment   = if (size(range) <= 1) 1 else range[1] - start-value end;
  values(value-range, start-value, end-value, increment)
end function range-values;
||#

(defun range-values (range)
  (let* ((start-value (elt range 0))
         (end-value   (elt range (- (size range) 1)))
         (value-range (abs (- start-value end-value)))
         (increment   (if (<= (size range) 1) 1 (- (elt range 1) start-value))))
    (values value-range start-value end-value increment)))


#||
/// Range gadget state

define sealed class <range-gadget-state> (<value-gadget-state>)
  sealed constant slot %state-range :: <range>,
    required-init-keyword: value-range:;
end class <range-gadget-state>;

define sealed domain make (singleton(<range-gadget-state>));
define sealed domain initialize (<range-gadget-state>);
||#

;; FIXME: MAYBE WE NEED AN IMPLEMENTATION OF <RANGE> AFTER ALL... BUT HOW TO
;; FIT IT IN TO METHODS THAT ACCEPT SEQUENCES?

(defclass <range-gadget-state> (<value-gadget-state>)
  ((%state-range :type #||<range>||# sequence :initarg :value-range
		 :initform (required-slot ":value-range" "<range-gadget-state>")
		 :reader %state-range)))


#||
define method gadget-state
    (gadget :: <range-gadget-mixin>) => (state :: <range-gadget-state>)
  make(<range-gadget-state>,
       value: gadget-value(gadget),
       value-range: gadget-value-range(gadget))
end method gadget-state;
||#

(defmethod gadget-state ((gadget <range-gadget-mixin>))
  (make-instance '<range-gadget-state>
                 :value (gadget-value gadget)
                 :value-range (gadget-value-range gadget)))


#||
define method gadget-state-setter
    (state :: <range-gadget-state>, gadget :: <range-gadget-mixin>)
 => (state :: <range-gadget-state>)
  gadget-value-range(gadget) := state.%state-range;
  next-method()
end method gadget-state-setter;
||#

(defmethod (setf gadget-state) ((state <range-gadget-state>) (gadget <range-gadget-mixin>))
  (setf (gadget-value-range gadget) (%state-range state))
  (call-next-method))



#||

/// Slug gadget mixin

define open abstract class <slug-gadget-mixin> (<range-gadget-mixin>)
  // Note that the slug size really is a _size_.  That is, if a scroll bar
  // ranges from 0 to 10 and you want the slug to take the full range, the
  // size needs to be 11 (that is, it's the size of the value range, not
  // its maximum).
  //--- This isn't a very good default for scroll bars
  sealed slot gadget-slug-size :: <real> = 1,
    init-keyword: slug-size:,
    setter: %slug-size-setter;
end class <slug-gadget-mixin>;
||#

(defclass <slug-gadget-mixin>
    (<range-gadget-mixin>)
  ((gadget-slug-size :type real :initform 1 :initarg :slug-size :reader gadget-slug-size :writer %slug-size-setter))
  (:documentation
"
Note that the slug size really is a _size_. That is, if a scroll bar
ranges from 0 to 10 and you want the slug to take the full range, the
size needs to be 11 (that is, it's the size of the value range, not
its maximum.
")
  (:metaclass <abstract-metaclass>))


#||
define sealed method gadget-slug-size-setter
    (slug-size :: <real>, gadget :: <slug-gadget-mixin>) => (slug-size :: <real>)
  unless (slug-size = gadget-slug-size(gadget))
    gadget.%slug-size := slug-size;
    note-gadget-slug-size-changed(gadget)
  end;
  slug-size
end method gadget-slug-size-setter;
||#

(defmethod (setf gadget-slug-size) ((slug-size real) (gadget <slug-gadget-mixin>))
  (unless (= slug-size (gadget-slug-size gadget))
    (%slug-size-setter slug-size gadget)
    (note-gadget-slug-size-changed gadget))
  slug-size)


#||
define method note-gadget-slug-size-changed
    (gadget :: <slug-gadget-mixin>) => ()
  #f
end method note-gadget-slug-size-changed;
||#

(defmethod note-gadget-slug-size-changed ((gadget <slug-gadget-mixin>))
  nil)


#||
// The end value of a slug gadget is the end of the range - the slug size + 1
define sealed method gadget-end-value 
    (gadget :: <slug-gadget-mixin>) => (end-value :: <real>)
  let end-value = next-method();
  end-value - gadget-slug-size(gadget) + 1
end method gadget-end-value;
||#

(defmethod gadget-end-value ((gadget <slug-gadget-mixin>))
  (let ((end-value (call-next-method)))
    (+ 1 (- end-value (gadget-slug-size gadget)))))


#||
/// Slug gadget state

define sealed class <slug-gadget-state> (<value-gadget-state>)
  sealed constant slot %state-slug-size :: <real>,
    required-init-keyword: slug-size:;
end class <slug-gadget-state>;

define sealed domain make (singleton(<slug-gadget-state>));
define sealed domain initialize (<slug-gadget-state>);
||#

(defclass <slug-gadget-state> (<value-gadget-state>)
  ((%state-slug-size :type real :initarg :slug-size
		     :initform (required-slot ":slug-size" "<slug-gadget-state>")
		     :reader %state-slug-size)))


#||
define method gadget-state
    (gadget :: <slug-gadget-mixin>) => (state :: <slug-gadget-state>)
  make(<slug-gadget-state>,
       value: gadget-value(gadget),
       value-range: gadget-value-range(gadget),
       slug-size: gadget-slug-size(gadget))
end method gadget-state;
||#

(defmethod gadget-state ((gadget <slug-gadget-mixin>))
  (make-instance '<slug-gadget-state>
                 :value (gadget-value gadget)
                 :value-range (gadget-value-range gadget)
                 :slug-size (gadget-slug-size gadget)))


#||
define method gadget-state-setter
    (state :: <slug-gadget-state>, gadget :: <slug-gadget-mixin>)
 => (state :: <slug-gadget-state>)
  gadget-slug-size(gadget) := state.%state-slug-size;
  next-method()
end method gadget-state-setter;
||#

(defmethod (setf gadget-state) ((state <slug-gadget-state>) (gadget <slug-gadget-mixin>))
  (setf (gadget-slug-size gadget) (%state-slug-size state))
  (call-next-method))



#||

/// Scrollable gadgets

// Mix-in for things that might implement their own scrolling
// Valid values for 'scroll-bars' field are:
//    #f, #"none", #"horizontal", #"vertical", #"both", or #"dynamic"
define open abstract class <scrolling-gadget-mixin> (<abstract-gadget>)
end class <scrolling-gadget-mixin>;
||#

(defclass <scrolling-gadget-mixin> (<abstract-gadget>)
  ()
  (:documentation
"
Mixin for things that might implement their own scrolling.
Valid values for the 'scroll-bars' field are:
    NIL, :NONE, :HORIZONTAL, :VERTICAL, :BOTH or :DYNAMIC
")
  (:metaclass <abstract-metaclass>))


#||
define constant <scroll-bar-type>
    = one-of(#f, #t, #"none", #"horizontal", #"vertical", #"both", #"dynamic");
||#

(deftype <scroll-bar-type> () '(or boolean (member :none :horizontal :vertical :both :dynamic)))


#||
define method initialize
    (gadget :: <scrolling-gadget-mixin>, #key scroll-bars = #"both")
  next-method();
  gadget-scroll-bars(gadget) := scroll-bars
end method initialize;
||#

(defmethod initialize-instance  :after ((gadget <scrolling-gadget-mixin>) &key (scroll-bars :both)
					&allow-other-keys)
  (setf (gadget-scroll-bars gadget) scroll-bars))


#||
define constant $scroll-bars :: <simple-object-vector>
    = #[#f, #"none", #"horizontal", #"vertical", #"both", #"dynamic"];
||#

(defvar *scroll-bars* #(nil :none :horizontal :vertical :both :dynamic))


#||
define sealed inline method gadget-scroll-bars
    (gadget :: <scrolling-gadget-mixin>) => (scroll-bars :: <scroll-bar-type>)
  let index = ash(logand(gadget-flags(gadget), %scroll_bar_mask),
		  -%scroll_bar_shift);
  $scroll-bars[index]
end method gadget-scroll-bars;
||#

(defmethod gadget-scroll-bars ((gadget <scrolling-gadget-mixin>))
  (let ((index (ash (logand (gadget-flags gadget) %scroll_bar_mask)
                    (- %scroll_bar_shift))))
    (aref *scroll-bars* index)))


#||
define sealed method gadget-scroll-bars-setter
    (scroll-bars :: <scroll-bar-type>, gadget :: <scrolling-gadget-mixin>)
 => (scroll-bars :: <scroll-bar-type>)
  let sb = select (scroll-bars)
	     #f            => %scroll_bar_false;
	     #"none"       => %scroll_bar_none;
	     #"horizontal" => %scroll_bar_horizontal;
	     #"vertical"   => %scroll_bar_vertical;
	     #"both"       => %scroll_bar_both;
	     #"dynamic"    => %scroll_bar_dynamic;
	   end;
  gadget-flags(gadget)
    := logior(logand(gadget-flags(gadget), lognot(%scroll_bar_mask)), sb);
  scroll-bars
end method gadget-scroll-bars-setter;
||#

;; FIXME: REALLY SHOULD IMPLEMENT CLASSES FOR THE <TYPE> STUFF, BUT HOW TO
;; GET THE USEFUL KEYWORD ARGS INTO A SPECIFIC CLASS WITHOUT MAKING THE
;; REST OF THE CODE REALLY UGLY?
;; INSTEAD OF '(SETF (GADGET-SCROLL-BARS GADGET) :DYNAMIC)' WE'D NEED TO
;; DO '(SETF (GADGET-SCROLL-BARS GADGET) *DYNAMIC*)' -- MAYBE NOT SO BAD.
;; EVEN SO, THAT MIGHT NOT WORK FOR TYPES THAT ENCAPSULATE MORE THAN JUST
;; A FEW KEYWORDS...

(defmethod (setf gadget-scroll-bars) (scroll-bars (gadget <scrolling-gadget-mixin>))
  (check-type scroll-bars <scroll-bar-type>)
  (let ((sb (ecase scroll-bars
	      ((nil) %scroll_bar_false)
	      (:none %scroll_bar_none)
	      (:horizontal %scroll_bar_horizontal)
	      (:vertical %scroll_bar_vertical)
	      (:both %scroll_bar_both)
	      (:dynamic %scroll_bar_dynamic))))
    (setf (gadget-flags gadget)
          (logior (logand (gadget-flags gadget) (lognot %scroll_bar_mask)) sb))
    scroll-bars))


#||
define sealed method gadget-scrolling?
    (gadget :: <scrolling-gadget-mixin>)
 => (horizontally? :: <boolean>, vertically? :: <boolean>)
  let sb = logand(gadget-flags(gadget), %scroll_bar_mask);
  values(  sb = %scroll_bar_horizontal
	 | sb = %scroll_bar_both
	 | sb = %scroll_bar_dynamic,
	   sb = %scroll_bar_vertical
	 | sb = %scroll_bar_both
	 | sb = %scroll_bar_dynamic)
end method gadget-scrolling?;
||#

(defmethod gadget-scrolling? ((gadget <scrolling-gadget-mixin>))
  (let ((sb (logand (gadget-flags gadget) %scroll_bar_mask)))
    (values (or (= sb %scroll_bar_horizontal)
                (= sb %scroll_bar_both)
                (= sb %scroll_bar_dynamic))
            (or (= sb %scroll_bar_vertical)
                (= sb %scroll_bar_both)
                (= sb %scroll_bar_dynamic)))))


#||
define sealed method gadget-scrolling-horizontally?
    (gadget :: <scrolling-gadget-mixin>) => (horizontally? :: <boolean>)
  let (horizontally?, vertically?) = gadget-scrolling?(gadget);
  ignore(vertically?);
  horizontally?
end method gadget-scrolling-horizontally?;
||#

(defmethod gadget-scrolling-horizontally? ((gadget <scrolling-gadget-mixin>))
  (multiple-value-bind (horizontally? vertically?)
      (gadget-scrolling? gadget)
    (declare (ignore vertically?))
    horizontally?))


#||
define sealed method gadget-scrolling-vertically?
    (gadget :: <scrolling-gadget-mixin>) => (vertically? :: <boolean>)
  let (horizontally?, vertically?) = gadget-scrolling?(gadget);
  ignore(horizontally?);
  vertically?
end method gadget-scrolling-vertically?;
||#

(defmethod gadget-scrolling-vertically? ((gadget <scrolling-gadget-mixin>))
  (multiple-value-bind (horizontally? vertically?)
      (gadget-scrolling? gadget)
    (declare (ignore horizontally?))
    vertically?))



#||

/// Gadgets with borders

// Mix-in for things that might implement their own borders
define open abstract class <bordered-gadget-mixin> (<abstract-gadget>)
end class <bordered-gadget-mixin>;
||#

(defclass <bordered-gadget-mixin> (<abstract-gadget>)
  ()
  (:documentation
"
Mix-in for things that might implement their own borders.
")
  (:metaclass <abstract-metaclass>))


#||
define constant <border-type>
    = one-of(#f, #"default",		// use the default
	     #"none", #"flat",		// no border
	     #"sunken", #"raised", #"ridge", #"groove",
	     #"input", #"output");	// "logical" borders
||#

(deftype <border-type> ()
  '(or null (member :default            ; use the default
		    :none :flat         ; no border
		    :sunken :raised :ridge :groove
		    :input :output)))   ; "logical" borders


#||
define method initialize
    (gadget :: <bordered-gadget-mixin>, #key border-type: borders = #f)
  next-method();
  border-type(gadget) := borders
end method initialize;
||#

(defmethod initialize-instance :after ((gadget <bordered-gadget-mixin>) &key (border-type nil)
				       &allow-other-keys)
  (setf (border-type gadget) border-type))


#||
define constant $borders :: <simple-object-vector>
    = #[#f, #"none", #"sunken", #"raised", #"ridge", #"groove", #"input", #"output"];
||#

(defvar *borders* #(nil :none :sunken :raised :ridge :groove
		    :input :output))


#||
define sealed inline method border-type
    (gadget :: <bordered-gadget-mixin>) => (borders :: <border-type>)
  let index = ash(logand(gadget-flags(gadget), %border_mask),
		  -%border_shift);
  $borders[index]
end method border-type;
||#

(defmethod border-type ((gadget <bordered-gadget-mixin>))
  (let ((index (ash (logand (gadget-flags gadget) %border_mask)
                    (- %border_shift))))
    (aref *borders* index)))


#||
define sealed method border-type-setter
    (borders :: <border-type>, gadget :: <bordered-gadget-mixin>)
 => (borders :: <border-type>)
  let bt = select (borders)
	     #f         => %border_default;	// #f == #"default"
	     #"default" => %border_default;	// #"default" == #f
	     #"none"    => %border_none;	// #"none" == #"flat"
	     #"flat"    => %border_none;	// #"flat" == #"none"
	     #"sunken"  => %border_sunken;
	     #"raised"  => %border_raised;
	     #"ridge"   => %border_ridge;
	     #"groove"  => %border_groove;
	     #"input"   => %border_input;
	     #"output"  => %border_output;
	   end;
  gadget-flags(gadget)
    := logior(logand(gadget-flags(gadget), lognot(%border_mask)), bt);
  borders
end method border-type-setter;
||#

(defmethod (setf border-type) (borders (gadget <bordered-gadget-mixin>))
  (check-type borders <border-type>)
  (let ((bt (ecase borders
	      ((nil)    %border_default)	;; #f == #"default"
	      (:default %border_default)	;; #"default" == #f
	      (:none    %border_none)	;; #"none" == #"flat"
	      (:flat    %border_none)	;; #"flat" == #"none"
	      (:sunken  %border_sunken)
	      (:raised  %border_raised)
	      (:ridge   %border_ridge)
	      (:groove  %border_groove)
	      (:input   %border_input)
	      (:output  %border_output))))
    (setf (gadget-flags gadget)
          (logior (logand (gadget-flags gadget) (lognot %border_mask)) bt)))
  borders)



#||

/// Default button

define open abstract class <default-gadget-mixin> (<action-gadget-mixin>)
end class <default-gadget-mixin>;
||#

(defclass <default-gadget-mixin> (<action-gadget-mixin>)
  ()
  (:metaclass <abstract-metaclass>))


#||
define method initialize
    (gadget :: <default-gadget-mixin>, #key default? = #f) => ()
  next-method();
  let bit = if (default?) %default_button else 0 end;
  gadget-flags(gadget)
    := logior(logand(gadget-flags(gadget), lognot(%default_button)), bit)
end method initialize;
||#

(defmethod initialize-instance :after ((gadget <default-gadget-mixin>) &key (default? nil)
				       &allow-other-keys)
  (let ((bit (if default? %default_button 0)))
    (setf (gadget-flags gadget)
          (logior (logand (gadget-flags gadget) (lognot %default_button)) bit))))


#||
define sealed inline method gadget-default?
    (gadget :: <default-gadget-mixin>) => (default? :: <boolean>)
  logand(gadget-flags(gadget), %default_button) = %default_button
end method gadget-default?;
||#

(defmethod gadget-default? ((gadget <default-gadget-mixin>))
  (= (logand (gadget-flags gadget) %default_button) %default_button))


#||
define method gadget-default?-setter
    (default? :: <boolean>, gadget :: <default-gadget-mixin>)
 => (default? :: <boolean>)
  let bit = if (default?) %default_button else 0 end;
  gadget-flags(gadget)
    := logior(logand(gadget-flags(gadget), lognot(%default_button)), bit);
  default?
end method gadget-default?-setter;
||#

(defmethod (setf gadget-default?) (default? (gadget <default-gadget-mixin>))
  (declare (type boolean default?))
  (let ((bit (if default? %default_button 0)))
    (setf (gadget-flags gadget)
          (logior (logand (gadget-flags gadget) (lognot %default_button)) bit)))
  default?)



#||

/// Scrolling sheets

define open abstract class <scrolling-sheet-mixin> (<abstract-sheet>)
  // The setters for these are in gadgets/scroll-bars.dylan
  sealed slot sheet-horizontal-scroll-bar :: false-or(<scroll-bar>) = #f,
    setter: %horizontal-scroll-bar-setter,
    init-keyword: horizontal-scroll-bar:;
  sealed slot sheet-vertical-scroll-bar :: false-or(<scroll-bar>) = #f,
    setter: %vertical-scroll-bar-setter,
    init-keyword: vertical-scroll-bar:;
end class <scrolling-sheet-mixin>;
||#

(defclass <scrolling-sheet-mixin>
    (<abstract-sheet>)
  ;; The setters for these are in gadgets/scroll-bars.dylan
  ((sheet-horizontal-scroll-bar :type (or null <scroll-bar>)
				:initform nil
				:initarg :horizontal-scroll-bar
				:reader sheet-horizontal-scroll-bar
				:writer %horizontal-scroll-bar-setter)
   (sheet-vertical-scroll-bar :type (or null <scroll-bar>)
			      :initarg :vertical-scroll-bar
			      :initform nil
			      :reader sheet-vertical-scroll-bar
			      :writer %vertical-scroll-bar-setter))
  (:metaclass <abstract-metaclass>))


#||
define method initialize 
    (sheet :: <scrolling-sheet-mixin>,
     #key horizontal-scroll-bar: horizontal-bar,
          vertical-scroll-bar:   vertical-bar) => ()
  next-method();
  when (horizontal-bar)
    attach-scroll-bar(sheet, horizontal-bar)
  end;
  when (vertical-bar) 
    attach-scroll-bar(sheet, vertical-bar)
  end;
end method initialize;
||#

(defmethod initialize-instance :after ((sheet <scrolling-sheet-mixin>) &key horizontal-scroll-bar vertical-scroll-bar
				       &allow-other-keys)
  (when horizontal-scroll-bar
    (attach-scroll-bar sheet horizontal-scroll-bar))
  (when vertical-scroll-bar
    (attach-scroll-bar sheet vertical-scroll-bar)))


#||
/// Scrolling Protocols

define protocol <<sheet-scrolling-protocol>> ()
  function line-scroll-amount
    (sheet :: <abstract-sheet>, orientation :: <gadget-orientation>)
 => (amount :: false-or(<integer>));
  function page-scroll-amount
    (sheet :: <abstract-sheet>, orientation :: <gadget-orientation>)
 => (amount :: false-or(<integer>));
  function horizontal-line-scroll-amount
    (sheet :: <abstract-sheet>) => (amount :: <integer>);
  function vertical-line-scroll-amount
    (sheet :: <abstract-sheet>) => (amount :: <integer>);
  function sheet-scroll-range
    (sheet :: <abstract-sheet>)
 => (left :: <real>, top :: <real>, right :: <real>, bottom :: <real>);
  function sheet-visible-range
    (sheet :: <abstract-sheet>)
 => (left :: <real>, top :: <real>, right :: <real>, bottom :: <real>);
  function set-sheet-visible-range
    (sheet :: <abstract-sheet>,
     left :: <real>, top :: <real>, right :: <real>, bottom :: <real>) => ();
  // User-level scrolling functionality
  function scroll-position
    (sheet :: <abstract-sheet>) => (x :: <integer>, y :: <integer>);
  function set-scroll-position
    (sheet :: <abstract-sheet>, x :: <integer>, y :: <integer>) => ();
  // For back-end tinkering...
  function gadget-supplies-scroll-bars?
    (framem :: <frame-manager>, gadget :: false-or(<gadget>),
     #key, #all-keys)
 => (true? :: <boolean>);
end protocol <<sheet-scrolling-protocol>>;
||#
#||
(define-protocol <<sheet-scrolling-protocol>> ()
  (:function line-scroll-amount (sheet orientation))
  (:function page-scroll-amount (sheet orientation))
  (:function horizontal-line-scroll-amount (sheet))
  (:function vertical-line-scroll-amount (sheet))
  (:function sheet-scroll-range (sheet))
  (:function sheet-visible-range (sheet))
  (:function set-sheet-visible-range (sheet left top right bottom))
  ;; User-level scrolling functionality
  (:function scroll-position (sheet))
  (:function set-scroll-position (sheet x y))
  ;; For back-end tinkering...
  (:function gadget-supplies-scroll-bars? (framem gadget
                                                  &key
                                                  &allow-other-keys)))
||#


#||
define method line-scroll-amount
    (sheet :: <scrolling-sheet-mixin>, orientation :: <gadget-orientation>)
 => (amount :: <integer>)
  select (orientation)
    #"horizontal" => horizontal-line-scroll-amount(sheet);
    #"vertical"   => vertical-line-scroll-amount(sheet);
  end
end method line-scroll-amount;
||#

(defmethod line-scroll-amount ((sheet <scrolling-sheet-mixin>) orientation) ;;  <gadget-orientation>))
  (check-type orientation <gadget-orientation>)
  (ecase orientation
    (:horizontal (horizontal-line-scroll-amount sheet))
    (:vertical   (vertical-line-scroll-amount sheet))))


#||
define method line-scroll-amount
    (sheet :: <sheet>, orientation :: <gadget-orientation>)
 => (amount :: false-or(<integer>))
  #f
end method line-scroll-amount;
||#

(defmethod line-scroll-amount ((sheet <sheet>) orientation) ;; <gadget-orientation>))
  (check-type orientation <gadget-orientation>)
  nil)


#||
// Page scroll scroll amount is the viewport size minus one line
//--- Seems reasonable, but maybe this is a frame manager decision
define method page-scroll-amount
    (sheet :: <scrolling-sheet-mixin>, orientation :: <gadget-orientation>)
 => (amount :: <integer>)
  let (left, top, right, bottom) = sheet-visible-range(sheet);
  let line-amount = line-scroll-amount(sheet, orientation);
  select (orientation)
    #"horizontal" => right  - left - line-amount;
    #"vertical"   => bottom - top  - line-amount;
  end
end method page-scroll-amount;
||#

(defmethod page-scroll-amount ((sheet <scrolling-sheet-mixin>) orientation) ;; <gadget-orientation>))
  (check-type orientation <gadget-orientation>)
  (multiple-value-bind (left top right bottom)
      (sheet-visible-range sheet)
    (let ((line-amount (line-scroll-amount sheet orientation)))
      (ecase orientation
	(:horizontal (- right left line-amount))
	(:vertical   (- bottom top line-amount))))))


#||
define method page-scroll-amount
    (sheet :: <sheet>, orientation :: <gadget-orientation>)
 => (amount :: false-or(<integer>))
  #f
end method page-scroll-amount;
||#

(defmethod page-scroll-amount ((sheet <sheet>) orientation) ;; <gadget-orientation>))
  (check-type orientation <gadget-orientation>)
  nil)


#||
define method horizontal-line-scroll-amount
    (sheet :: <sheet>) => (amount :: <integer>)
  with-sheet-medium (medium = sheet)
    let text-style = medium-merged-text-style(medium);
    floor(font-width(text-style, port(medium)))
  end
end method horizontal-line-scroll-amount;
||#

(defmethod horizontal-line-scroll-amount ((sheet <sheet>))
  (with-sheet-medium (medium = sheet)
    (let ((text-style (medium-merged-text-style medium)))
      (floor (font-width text-style (port medium))))))


#||
define method vertical-line-scroll-amount
    (sheet :: <sheet>) => (amount :: <integer>)
  with-sheet-medium (medium = sheet)
    let text-style = medium-merged-text-style(medium);
    floor(font-height(text-style, port(medium)))
  end
end method vertical-line-scroll-amount;
||#

(defmethod vertical-line-scroll-amount ((sheet <sheet>))
  (with-sheet-medium (medium = sheet)
    (let ((text-style (medium-merged-text-style medium)))
      (floor (font-height text-style (port medium))))))


#||
define method gadget-supplies-scroll-bars?
    (framem :: <frame-manager>, gadget :: false-or(<gadget>), #key)
 => (true? :: <boolean>)
  #f
end method gadget-supplies-scroll-bars?;
||#

(defmethod gadget-supplies-scroll-bars? ((framem <frame-manager>) (gadget <gadget>) &key)
  nil)

(defmethod gadget-supplies-scroll-bars? ((framem <frame-manager>) (gadget null) &key)
  nil)


#||
/// Updating scrollbars

define thread variable *inhibit-updating-scroll-bars?* = #f;
define thread variable *inhibit-updating-scroll-bars-viewports* = #f;
||#

(define-thread-variable *inhibit-updating-scroll-bars?* nil)
(define-thread-variable *inhibit-updating-scroll-bars-viewports* nil)


#||
// Inhibit updating any scroll bars for the duration of the body
define macro inhibit-updating-scroll-bars
  { inhibit-updating-scroll-bars ?:body end }
    => { dynamic-bind (*inhibit-updating-scroll-bars-viewports* = #())
	   block ()
	     dynamic-bind (*inhibit-updating-scroll-bars?* = #t)
               ?body
             end
	   cleanup
	     for (_viewport in *inhibit-updating-scroll-bars-viewports*)
	       when (_viewport)
		 update-scroll-bars(_viewport)
	       end
	     end
	   end
	 end }
end macro inhibit-updating-scroll-bars;
||#

;; Inhibit updating any scroll bars for the duration of the body
(defmacro inhibit-updating-scroll-bars (&body body)
  `(dynamic-bind ((*inhibit-updating-scroll-bars-viewports* = ()))
    (unwind-protect
        (dynamic-bind ((*inhibit-updating-scroll-bars?* = t))
          ,@body)
      (loop for _viewport in *inhibit-updating-scroll-bars-viewports*
	    when _viewport
	    do (update-scroll-bars _viewport)))))


#||
define method update-scroll-bars
    (sheet :: <scrolling-sheet-mixin>) => ()
  if (*inhibit-updating-scroll-bars?*)
    // Who cares that this is not the most efficient thing in the world...
    unless (member?(sheet, *inhibit-updating-scroll-bars-viewports*))
      push!(*inhibit-updating-scroll-bars-viewports*, sheet)
    end
  else
    update-dynamic-scroll-bars(sheet);
    let horizontal-bar = sheet-horizontal-scroll-bar(sheet);
    let vertical-bar   = sheet-vertical-scroll-bar(sheet);
    let (left, top, right, bottom)     = sheet-scroll-range(sheet);
    let (vleft, vtop, vright, vbottom) = sheet-visible-range(sheet);
    when (vertical-bar)
      update-scroll-bar(vertical-bar, top, bottom, vtop, vbottom)
    end;
    when (horizontal-bar)
      update-scroll-bar(horizontal-bar, left, right, vleft, vright)
    end
  end
end method update-scroll-bars;
||#

(defmethod update-scroll-bars ((sheet <scrolling-sheet-mixin>))
  (if *inhibit-updating-scroll-bars?*
      ;; Who cares that this is not the most efficient thing in the world...
      (unless (member sheet *inhibit-updating-scroll-bars-viewports*)
        (setf *inhibit-updating-scroll-bars-viewports* (cons sheet *inhibit-updating-scroll-bars-viewports*)))
      ;; else
      (progn
        (update-dynamic-scroll-bars sheet)
        (let ((horizontal-bar (sheet-horizontal-scroll-bar sheet))
              (vertical-bar   (sheet-vertical-scroll-bar sheet)))
          (multiple-value-bind (left top right bottom)
              (sheet-scroll-range sheet)
            (multiple-value-bind (vleft vtop vright vbottom)
                (sheet-visible-range sheet)
              (when vertical-bar
                (update-scroll-bar vertical-bar top bottom vtop vbottom))
              (when horizontal-bar
                (update-scroll-bar horizontal-bar left right vleft vright))))))))


#||
/// Dynamic scroll bars

define method update-dynamic-scroll-bars
    (sheet :: <scrolling-sheet-mixin>, #key relayout? = #f) => ()
  let (changed?, hscroll-bar, hscroll-enabled?, vscroll-bar, vscroll-enabled?)
    = compute-dynamic-scroll-bar-values(sheet);
  when (changed?)
    //--- Really the back-end should take care of graying out (or hiding)
    //--- scroll bars when everything in the viewport is visible
    when (hscroll-bar)
      gadget-enabled?(hscroll-bar) := hscroll-enabled?
    end;
    when (vscroll-bar)
      gadget-enabled?(vscroll-bar) := vscroll-enabled?
    end;
    when (relayout?)
      let scroller = sheet-scroller(sheet);
      when (scroller)
        relayout-children(scroller)
      end
    end
  end
end method update-dynamic-scroll-bars;
||#

(defmethod update-dynamic-scroll-bars ((sheet <scrolling-sheet-mixin>) &key (relayout? nil))
  (multiple-value-bind (changed? hscroll-bar hscroll-enabled? vscroll-bar vscroll-enabled?)
      (compute-dynamic-scroll-bar-values sheet)
    (when changed?
      ;;--- Really the back-end should take care of graying out (or hiding)
      ;;--- scroll bars when everything in the viewport is visible
      (when hscroll-bar
        (setf (gadget-enabled? hscroll-bar) hscroll-enabled?))
      (when vscroll-bar
        (setf (gadget-enabled? vscroll-bar) vscroll-enabled?))
      (when relayout?
        (let ((scroller (sheet-scroller sheet)))
          (when scroller
            (relayout-children scroller)))))))


#||
define method compute-dynamic-scroll-bar-values
    (sheet :: <scrolling-sheet-mixin>)
 => (changed? :: <boolean>,
     horizontal-bar, hscroll-enabled? :: <boolean>,
     vertical-bar,   vscroll-enabled? :: <boolean>)
  let scroller = sheet-scroller(sheet);
  if (scroller & gadget-scroll-bars(scroller) == #"dynamic")
    let horizontal-bar = sheet-horizontal-scroll-bar(sheet);
    let vertical-bar   = sheet-vertical-scroll-bar(sheet);
    let (left, top, right, bottom)     = sheet-scroll-range(sheet);
    let (vleft, vtop, vright, vbottom) = sheet-visible-range(sheet);
    let oh-enabled? = gadget-enabled?(horizontal-bar);
    let ov-enabled? = gadget-enabled?(vertical-bar);
    let nh-enabled? = left ~= vleft | right ~= vright;
    let nv-enabled? = top ~= vtop | bottom ~= vbottom;
    values(~(oh-enabled? == nh-enabled?  & ov-enabled? == nv-enabled?),
	   ~(oh-enabled? == nh-enabled?) & horizontal-bar, nh-enabled?,
	   ~(ov-enabled? == nv-enabled?) & vertical-bar,   nv-enabled?)
  else
    values(#f, #f, #f, #f, #f)
  end
end method compute-dynamic-scroll-bar-values;
||#

(defmethod compute-dynamic-scroll-bar-values ((sheet <scrolling-sheet-mixin>))
;; => changed?, hbar, hscroll-enabled?, vbar, vscroll-enabled?
  (let ((scroller (sheet-scroller sheet)))
    (if (and scroller (eql (gadget-scroll-bars scroller) :dynamic))
        (let ((horizontal-bar (sheet-horizontal-scroll-bar sheet))
              (vertical-bar   (sheet-vertical-scroll-bar sheet)))
          (multiple-value-bind (left top right bottom)
              (sheet-scroll-range sheet)
            (multiple-value-bind (vleft vtop vright vbottom)
                (sheet-visible-range sheet)
              (let ((oh-enabled? (gadget-enabled? horizontal-bar))
                    (ov-enabled? (gadget-enabled? vertical-bar))
                    (nh-enabled? (or (not (equal? left vleft)) (not (equal? right vright))))
                    (nv-enabled? (or (not (equal? top vtop)) (not (equal? bottom vbottom)))))
                (values (not (and (eql oh-enabled? nh-enabled?) (eql ov-enabled? nv-enabled?)))
                        (and (not (eql oh-enabled? nh-enabled?)) horizontal-bar)
                        nh-enabled?
                        (and (not (eql ov-enabled? nv-enabled?)) vertical-bar)
                        nv-enabled?)))))
	;; else
        (values nil nil nil nil nil))))


#||
define method sheet-scroller
    (sheet :: <scrolling-sheet-mixin>) => (scroller :: false-or(<sheet>))
  let layout = sheet-parent(sheet);
  let scroller = layout & sheet-parent(layout);
  instance?(scroller, <scroller>) & scroller
end method sheet-scroller;
||#

(defmethod sheet-scroller ((sheet <scrolling-sheet-mixin>))
  (let* ((layout (sheet-parent sheet))
	 ;; If the scroller has a border, the parent of the layout will be
	 ;; the border, not the scroller.
	 ;; Navigate up an additional layer to get the scroller.
	 (border? (and layout (sheet-parent layout)))
         (scroller (or (and border?
			    (typep border? '<border>)
			    (sheet-parent border?))
		       border?)))
    (when (typep scroller '<scroller>)
	scroller)))

#||
/// Viewport fenceposts... don't ask

define generic viewport-fencepost? (object) => (true? :: <boolean>);
||#

(defgeneric viewport-fencepost? (object))


#||
define method viewport-fencepost? (sheet) => (true? :: <boolean>)
  #f
end method viewport-fencepost?;
||#

(defmethod viewport-fencepost? (sheet)
  (declare (ignore sheet))
  nil)



#||

/// Callbacks on gadgets generate these events

define open abstract class <gadget-event> (<basic-event>)
  sealed constant slot event-gadget :: false-or(<gadget>),
    required-init-keyword: gadget:;
end class <gadget-event>;
||#

(defclass <gadget-event> (<basic-event>)
  ((event-gadget :type (or null <gadget>) :initarg :gadget
		 :initform (required-slot ":gadget" "<gadget-event>")
		 :reader event-gadget))
  (:metaclass <abstract-metaclass>))


#||
define sealed method event-client
    (event :: <gadget-event>) => (gadget :: false-or(<gadget>))
  event-gadget(event)
end method event-client;
||#

(defmethod event-client ((event <gadget-event>))
  (event-gadget event))


#||
define sealed class <activate-gadget-event> (<gadget-event>)
end class <activate-gadget-event>;

define sealed domain make (singleton(<activate-gadget-event>));
define sealed domain initialize (<activate-gadget-event>);
||#

(defclass <activate-gadget-event> (<gadget-event>) ())


#||
define sealed method handle-event
    (gadget :: <action-gadget-mixin>, event :: <activate-gadget-event>) => ()
  execute-activate-callback(gadget, gadget-client(gadget), gadget-id(gadget))
end method handle-event;
||#

(defmethod handle-event ((gadget <action-gadget-mixin>) (event <activate-gadget-event>))
  (execute-activate-callback gadget (gadget-client gadget) (gadget-id gadget)))


#||
define function distribute-activate-callback
    (gadget :: <action-gadget-mixin>) => ()
  distribute-event(port(gadget),
		   make(<activate-gadget-event>,
			gadget: gadget))
end function distribute-activate-callback;
||#

(defmethod distribute-activate-callback ((gadget <action-gadget-mixin>))
  (distribute-event (port gadget)
                    (make-instance '<activate-gadget-event>
                                   :gadget gadget)))


#||
define sealed class <value-changed-gadget-event> (<gadget-event>)
  sealed constant slot event-value,
    required-init-keyword: value:;
end class <value-changed-gadget-event>;

define sealed domain make (singleton(<value-changed-gadget-event>));
define sealed domain initialize (<value-changed-gadget-event>);
||#

(defclass <value-changed-gadget-event> (<gadget-event>)
  ((event-value :initarg :value :initform (required-slot ":value" "<value-changed-gadget-event>") :reader event-value)))


#||
define sealed method handle-event
    (gadget :: <value-gadget-mixin>, event :: <value-changed-gadget-event>) => ()
  gadget-value(gadget, do-callback?: #t) := event-value(event)
end method handle-event;
||#

(defmethod handle-event ((gadget <value-gadget-mixin>) (event <value-changed-gadget-event>))
  (setf (gadget-value gadget :do-callback? t) (event-value event)))


#||
define function distribute-value-changed-callback
    (gadget :: <value-gadget-mixin>, value) => ()
  distribute-event(port(gadget),
		   make(<value-changed-gadget-event>,
			gadget: gadget,
			value: value))
end function distribute-value-changed-callback;
||#

(defmethod distribute-value-changed-callback ((gadget <value-gadget-mixin>) value)
  (distribute-event (port gadget)
                    (make-instance '<value-changed-gadget-event>
                                   :gadget gadget
                                   :value value)))


#||
define sealed class <value-changing-gadget-event> (<gadget-event>)
  sealed constant slot event-value,
    required-init-keyword: value:;
end class <value-changing-gadget-event>;

define sealed domain make (singleton(<value-changing-gadget-event>));
define sealed domain initialize (<value-changing-gadget-event>);
||#

(defclass <value-changing-gadget-event> (<gadget-event>)
  ((event-value :initarg :value
		:initform (required-slot ":value" "<value-changing-gadget-event>")
		:reader event-value)))


#||
define sealed method handle-event
    (gadget :: <changing-value-gadget-mixin>, event :: <value-changing-gadget-event>) => ()
  gadget-value(gadget) := event-value(event);
  execute-value-changing-callback(gadget, gadget-client(gadget), gadget-id(gadget))
end method handle-event;
||#

(defmethod handle-event ((gadget <changing-value-gadget-mixin>) (event <value-changing-gadget-event>))
  (setf (gadget-value gadget) (event-value event))
  (execute-value-changing-callback gadget (gadget-client gadget) (gadget-id gadget)))


#||
define function distribute-value-changing-callback
    (gadget :: <changing-value-gadget-mixin>, value) => ()
  distribute-event(port(gadget),
		   make(<value-changing-gadget-event>,
			gadget: gadget,
			value: value))
end function distribute-value-changing-callback;
||#

(defmethod distribute-value-changing-callback ((gadget <changing-value-gadget-mixin>) value)
  (distribute-event (port gadget)
                    (make-instance '<value-changing-gadget-event>
                                   :gadget gadget
                                   :value value)))


#||
define sealed class <key-press-gadget-event> 
    (<gadget-event>)
  sealed constant slot event-key-name = #f,
    init-keyword: key-name:;
end class <key-press-gadget-event>;

define sealed domain make (singleton(<key-press-gadget-event>));
define sealed domain initialize (<key-press-gadget-event>);
||#

(defclass <key-press-gadget-event> (<gadget-event>)
  ((event-key-name :initarg :key-name
		   :initform nil
		   :reader event-key-name)))


#||
define sealed method handle-event
    (gadget :: <key-press-gadget-mixin>, event :: <key-press-gadget-event>) => ()
  execute-key-press-callback
    (gadget, gadget-client(gadget), gadget-id(gadget), event-key-name(event))
end method handle-event;
||#

(defmethod handle-event ((gadget <key-press-gadget-mixin>) (event <key-press-gadget-event>))
  (execute-key-press-callback gadget
			      (gadget-client gadget) (gadget-id gadget) (event-key-name event)))


#||
define function distribute-key-press-callback
    (gadget :: <key-press-gadget-mixin>, keysym) => ()
  distribute-event(port(gadget), 
		   make(<key-press-gadget-event>,
			gadget: gadget,
			key-name: keysym))
end function distribute-key-press-callback;
||#

(defmethod distribute-key-press-callback ((gadget <key-press-gadget-mixin>) keysym)
  (distribute-event (port gadget)
                    (make-instance '<key-press-gadget-event>
                                   :gadget gadget
                                   :key-name keysym)))


#||
define sealed class <popup-menu-gadget-event> 
    (<gadget-event>)
  sealed constant slot event-target,
    required-init-keyword: target:;
  sealed constant slot event-x :: false-or(<integer>) = #f,
    init-keyword: x:;
  sealed constant slot event-y :: false-or(<integer>) = #f,
    init-keyword: y:;
end class <popup-menu-gadget-event>;

define sealed domain make (singleton(<popup-menu-gadget-event>));
define sealed domain initialize (<popup-menu-gadget-event>);
||#

(defclass <popup-menu-gadget-event> (<gadget-event>)
  ((event-target :initarg :target :initform (required-slot ":target" "<popup-menu-gadget-event>") :reader event-target)
   (event-x :type (or null integer) :initarg :x :initform nil :reader event-x)
   (event-y :type (or null integer) :initarg :y :initform nil :reader event-y)))


#||
define sealed method handle-event
    (gadget :: <popup-menu-gadget-mixin>, event :: <popup-menu-gadget-event>) => ()
  execute-popup-menu-callback
    (gadget, gadget-client(gadget), gadget-id(gadget), event-target(event),
     x: event-x(event), y: event-y(event))
end method handle-event;
||#

(defmethod handle-event ((gadget <popup-menu-gadget-mixin>) (event <popup-menu-gadget-event>))
  (execute-popup-menu-callback gadget
                               (gadget-client gadget) (gadget-id gadget) (event-target event)
                               :x (event-x event) :y (event-y event)))


#||
define function distribute-popup-menu-callback
    (gadget :: <popup-menu-gadget-mixin>, target, #key x, y) => ()
  distribute-event(port(gadget), 
		   make(<popup-menu-gadget-event>,
			gadget: gadget,
			target: target,
			x: x, y: y))
end function distribute-popup-menu-callback;
||#

(defmethod distribute-popup-menu-callback ((gadget <popup-menu-gadget-mixin>) target &key x y)
  (distribute-event (port gadget)
                    (make-instance '<popup-menu-gadget-event>
                                   :gadget gadget
                                   :target target
                                   :x x :y y)))


#||
define sealed class <update-gadget-event> (<gadget-event>)
end class <update-gadget-event>;

define sealed domain make (singleton(<update-gadget-event>));
define sealed domain initialize (<update-gadget-event>);
||#

(defclass <update-gadget-event> (<gadget-event>) ())


#||
define sealed method handle-event
    (gadget :: <updatable-gadget-mixin>, event :: <update-gadget-event>) => ()
  execute-update-callback(gadget, gadget-client(gadget), gadget-id(gadget))
end method handle-event;
||#

(defmethod handle-event ((gadget <updatable-gadget-mixin>) (event <update-gadget-event>))
  (execute-update-callback gadget (gadget-client gadget) (gadget-id gadget)))


#||
define function distribute-update-callback
    (gadget :: <updatable-gadget-mixin>) => ()
  distribute-event(port(gadget), 
		   make(<update-gadget-event>,
			gadget: gadget))
end function distribute-update-callback;
||#

;; TODO: Should these be returned to basic DEFUNs?
(defmethod distribute-update-callback ((gadget <updatable-gadget-mixin>))
  (distribute-event (port gadget)
                    (make-instance '<update-gadget-event>
                                   :gadget gadget)))

