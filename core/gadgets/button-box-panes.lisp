;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GADGETS-INTERNALS -*-
(in-package #:duim-gadgets-internals)

#||
/// Gadget box pane implementation class

// Implements the adding of buttons to a gadget box to represent the items
define abstract class <gadget-box-pane-mixin> (<collection-gadget>)
  sealed slot frame-manager :: false-or(<frame-manager>) = #f,
    init-keyword: frame-manager:;
  sealed slot gadget-box-button-class :: false-or(<class>) = #f,
    init-keyword: button-class:;
  sealed slot gadget-box-buttons :: <sequence> = #[];
end class <gadget-box-pane-mixin>;
||#


(defgeneric gadget-box-button-value (box item))
(defgeneric make-button-for-gadget-box (box item button-class))
(defgeneric make-buttons-for-gadget-box (box items))
(defgeneric gadget-box-button-index (box button))
(defgeneric update-button-selections (box selection))
(defgeneric gadget-box-buttons-parent (box))
(defgeneric button-first-in-group? (gadget))
(defgeneric button-in-tool-bar? (button))
(defgeneric button-gadget-box (button))
(defgeneric update-button-enabled-states (box enabled?))
(defgeneric find-button-box-buttons (box child))

(defclass <gadget-box-pane-mixin>
    (<collection-gadget>)
  ((frame-manager :type (or null <frame-manager>) :initform nil :initarg :frame-manager :accessor frame-manager)
   (gadget-box-button-class :type (or null class) :initform nil :initarg :button-class :accessor gadget-box-button-class)
   (gadget-box-buttons :type sequence :initform (make-array 0 :adjustable t :fill-pointer t) :accessor gadget-box-buttons))
  (:documentation
"
Implements the adding of buttons to a gadget box to represent the items.
")
  (:metaclass <abstract-metaclass>))


#||
define open generic button-class-for-gadget-box (box);
||#

(defgeneric button-class-for-gadget-box (box))

#||
define method gadget-box-button-value 
    (box :: <gadget-box-pane-mixin>, item)
  let value-key = gadget-value-key(box);
  select (gadget-selection-mode(box))
    #"none"   => value-key(item);
    otherwise => #f;
  end
end method gadget-box-button-value;
||#

(defmethod gadget-box-button-value ((box <gadget-box-pane-mixin>) item)
  (let ((value-key (gadget-value-key box)))
    (case (gadget-selection-mode box)
      (:none (funcall value-key item))
      (t nil))))


#||
define method make-button-for-gadget-box
    (box :: <gadget-box-pane-mixin>, item, button-class)
 => (button :: <sheet>)
  let selection-mode = gadget-selection-mode(box);
  let documentation = gadget-documentation(box);
  let label = gadget-item-label(box, item);
  let framem = frame-manager(box);
  with-frame-manager (framem)
    make-pane(button-class,
              selection-mode: selection-mode,
	      button-style: push-button-like?(box) & #"push-button",
              enabled?: gadget-enabled?(box),
              client: box,
              label: label,
	      documentation: documentation,
              value: gadget-box-button-value(box, item),
              foreground: default-foreground(box),
              background: default-background(box),
              text-style: default-text-style(box))
  end
end method make-button-for-gadget-box;
||#

(defmethod make-button-for-gadget-box ((box <gadget-box-pane-mixin>) item button-class)
  (let ((selection-mode (gadget-selection-mode box))
	(documentation  (gadget-documentation box))
	(label          (gadget-item-label box item))
	(framem         (frame-manager box)))
    (with-frame-manager (framem)
      (make-pane button-class
		 :selection-mode selection-mode
		 :button-style (and (push-button-like? box) :push-button)
		 :enabled? (gadget-enabled? box)
		 :client box
		 :label label
		 :documentation documentation
		 :value (gadget-box-button-value box item)
		 :foreground (default-foreground box)
		 :background (default-background box)
		 :text-style (default-text-style box)))))


#||
define function make-buttons-for-gadget-box 
    (box :: <gadget-box-pane-mixin>, items :: <sequence>)
 => (buttons :: <vector>)
  let button-class 
    = gadget-box-button-class(box)
      | button-class-for-gadget-box(box);
  // Make a stretchy vector of the buttons because these are going
  // to go into the children of some lucky sheet...
  let buttons
    = map-as(<stretchy-vector>,
	     method (item)
	       make-button-for-gadget-box(box, item, button-class)
	     end,
	     items);
  gadget-box-buttons(box) := buttons
end function make-buttons-for-gadget-box;
||#

(defmethod make-buttons-for-gadget-box ((box <gadget-box-pane-mixin>) (items sequence))
  (let* ((button-class (or (gadget-box-button-class box)
			   (button-class-for-gadget-box box)))
	 ;; Make a stretchy vector of the buttons because these are going
	 ;; to go into the children of some lucky sheet...
	 (buttons (make-array 0 :adjustable t :fill-pointer t)))
    (map nil #'(lambda (item)
		 (setf buttons (add! buttons (make-button-for-gadget-box box item button-class))))
	 items)
    (setf (gadget-box-buttons box) buttons)))


#||
define method gadget-box-button-index 
    (box :: <gadget-box-pane-mixin>, button :: <button>)
 => (index :: false-or(<integer>))
  position(gadget-box-buttons(box), button)
end method gadget-box-button-index;
||#

(defmethod gadget-box-button-index ((box <gadget-box-pane-mixin>) (button <button>))
  (position button (gadget-box-buttons box)))


#||
define method update-button-selections 
    (box :: <gadget-box-pane-mixin>, selection) => ()
  let buttons = gadget-box-buttons(box);
  when (buttons)
    for (index :: <integer> from 0 below size(buttons))
      let button = buttons[index];
      gadget-value(button) := (selection & member?(index, selection))
    end  
  end
end method update-button-selections;
||#

(defmethod update-button-selections ((box <gadget-box-pane-mixin>) selection)
  (let ((buttons (gadget-box-buttons box)))
    (when buttons
      (loop for index from 0 below (length buttons)
	 do (let ((button (aref buttons index)))
	      (setf (gadget-value button)
		    (and selection (find index selection))))))))


#||
//--- Could we make this more general via 'layout-default-spacing' or something?
define open generic button-box-spacing
    (framem :: <frame-manager>, box :: <button-box>)
 => (spacing :: <integer>);
||#

(defgeneric button-box-spacing (framem box))


#||
define method button-box-spacing
    (framem :: <frame-manager>, box :: <button-box>)
 => (spacing :: <integer>)
  2
end method button-box-spacing;
||#

(defmethod button-box-spacing ((framem <frame-manager>) (box <button-box>))
  2)


#||
define method initialize
    (box :: <gadget-box-pane-mixin>, #rest initargs,
     #key layout-class, spacing, rows, columns) => ()
  dynamic-extent(initargs);
  next-method();
  when (empty?(sheet-children(box)))
    let framem = frame-manager(box);
    with-frame-manager (framem)
      let items = gadget-items(box);
      let layout-class
        = layout-class
          | ((rows | columns) & <table-layout>)
          | gadget-pane-default-layout-class(box);
      let layout
        = if (layout-class)
            with-keywords-removed (pane-args = initargs,
				   #[layout-class:, border:, x:, y:])
              apply(make-pane,
                    layout-class,
                    spacing: spacing | button-box-spacing(framem, box),
                    parent: box,
                    pane-args)
            end
          else
            box
          end;
      let buttons = make-buttons-for-gadget-box(box, items);
      sheet-children(layout) := buttons;
      unless (gadget-selection-mode(box) = #"none")
	update-button-selections(box, gadget-selection(box))
      end
    end
  end
end method initialize;
||#

(defmethod initialize-instance :after ((box <gadget-box-pane-mixin>) &rest initargs
				       &key layout-class spacing rows columns &allow-other-keys)
  (declare (dynamic-extent initargs))
  (when (empty? (sheet-children box))
    (let ((framem (frame-manager box)))
      (with-frame-manager (framem)
        (let* ((items (gadget-items box))
	       (layout-class (or layout-class
				 (and (or rows columns) (find-class '<table-layout>))
				 (gadget-pane-default-layout-class box)))
	       (layout (if layout-class
			   (with-keywords-removed (pane-args = initargs (:layout-class :border :x :y))
                             (apply #'make-pane
				    layout-class
				    :spacing (or spacing (button-box-spacing framem box))
				    :parent box
				    pane-args))
			   box))
	       (buttons (make-buttons-for-gadget-box box items)))
	  (setf (sheet-children layout) buttons)
	  (unless (eql (gadget-selection-mode box) :none)
	    (update-button-selections box (gadget-selection box))))))))


#||
define method gadget-box-buttons-parent
    (box :: <gadget-box-pane-mixin>) => (sheet :: <sheet>)
  sheet-child(box)
end method gadget-box-buttons-parent;
||#

(defmethod gadget-box-buttons-parent ((box <gadget-box-pane-mixin>))
  (sheet-child box))


#||
define method button-first-in-group?
    (gadget :: <button>) => (first? :: <boolean>)
  let box = button-gadget-box(gadget);
  select (box by instance?)
    <gadget-box-pane-mixin> =>
      gadget == first(gadget-box-buttons(box));
    otherwise =>
      //--- This is not a very good approximation, and maybe the first
      //--- select case above is enough on its own.
      let parent = sheet-parent(gadget);
      let children = sheet-children(parent);
      let index = position(children, gadget);
      index == 0
	| (index > 0
	     & object-class(children[index - 1]) ~== object-class(gadget));
  end
end method button-first-in-group?;
||#

(defmethod button-first-in-group? ((gadget <button>))
  (let ((box (button-gadget-box gadget)))
    (typecase box
      (<gadget-box-pane-mixin>
       (eql gadget (aref (gadget-box-buttons box) 0)))
      (t
       ;;--- This is not a very good approximation, and maybe the first
       ;;--- select case above is enough on its own.
       (let* ((parent (sheet-parent gadget))
	      (children (sheet-children parent))
	      (index (position gadget children)))
	 (or (eql index 0)
	     (and (> index 0)
		  (not (eql (class-of (aref children (- index 1))) (class-of gadget))))))))))


#||
define method button-in-tool-bar?
    (gadget :: <button>) => (in-tool-bar? :: <boolean>)
  block (return)
    for (parent = sheet-parent(gadget) then sheet-parent(parent),
	 while: parent)
      when (instance?(parent, <tool-bar>))
	return(#t)
      end
    end;
    #f
  end
end method button-in-tool-bar?;
||#

(defmethod button-in-tool-bar? ((gadget <button>))
  (loop for parent = (sheet-parent gadget) then (sheet-parent parent)
     while parent
     do (when (typep parent '<tool-bar>)
	  (return t)))
  nil)


#||
define method button-gadget-box
    (gadget :: <button>) => (box :: false-or(<gadget-box>))
  let client = gadget-client(gadget);
  instance?(client, <gadget-box>) & client
end method button-gadget-box;
||#

(defmethod button-gadget-box ((gadget <button>))
  (let ((client (gadget-client gadget)))
    (and (typep client '<gadget-box>) client)))


#||
define method note-gadget-items-changed
    (box :: <gadget-box-pane-mixin>) => ()
  next-method();
  let layout = gadget-box-buttons-parent(box);
  let buttons = make-buttons-for-gadget-box(box, gadget-items(box));
  //--- 'sheet-children-setter' doesn't do relayout so that it remains
  //--- inexpensive.  Is this really what we want?
  //---*** Also, won't the layout be the wrong size if box isn't mapped
  //---*** (hence we don't call 'relayout-parent')?  But we can't call it
  //---*** because nothing will have been mirrored!
  sheet-children(layout) := buttons;
  relayout-parent(layout);
  when (sheet-mapped?(layout))
    do(curry(sheet-mapped?-setter, #t), buttons)
  end
end method note-gadget-items-changed;
||#

(defmethod note-gadget-items-changed ((box <gadget-box-pane-mixin>))
  (call-next-method)
  (let ((layout (gadget-box-buttons-parent box))
	(buttons (make-buttons-for-gadget-box box (gadget-items box))))
    ;;--- 'sheet-children-setter' doesn't do relayout so that it remains
    ;;--- inexpensive.  Is this really what we want?
    ;;---*** Also, won't the layout be the wrong size if box isn't mapped
    ;;---*** (hence we don't call 'relayout-parent')?  But we can't call it
    ;;---*** because nothing will have been mirrored!
    (setf (sheet-children layout) buttons)
    (relayout-parent layout)
    (when (sheet-mapped? layout)
      (map nil #'(lambda (button)
		   (setf (sheet-mapped? button) t))
	   buttons))))


#||
define method note-gadget-selection-changed
    (box :: <gadget-box-pane-mixin>) => ()
  next-method();
  update-button-selections(box, gadget-selection(box))
end method note-gadget-selection-changed;
||#

(defmethod note-gadget-selection-changed ((box <gadget-box-pane-mixin>))
  (call-next-method)
  (update-button-selections box (gadget-selection box)))


#||
define method update-button-enabled-states 
    (box :: <gadget-box-pane-mixin>, enabled? :: <boolean>) => ()
  let buttons = gadget-box-buttons(box);
  for (button in buttons)
    gadget-enabled?(button) := enabled?
  end
end method update-button-enabled-states;
||#

(defmethod update-button-enabled-states ((box <gadget-box-pane-mixin>) enabled?)
  (declare (type boolean enabled?))
  (let ((buttons (gadget-box-buttons box)))
    (map nil #'(lambda (button)
		 (setf (gadget-enabled? button) enabled?))
	 buttons)))


#||
define method note-gadget-enabled
    (client, box :: <gadget-box-pane-mixin>) => ()
  ignore(client);
  update-button-enabled-states(box, #t)
end method note-gadget-enabled;
||#

(defmethod note-gadget-enabled (client (box <gadget-box-pane-mixin>))
  (declare (ignore client))
  (update-button-enabled-states box t))


#||
define method note-gadget-disabled
    (client, box :: <gadget-box-pane-mixin>) => ()
  ignore(client);
  update-button-enabled-states(box, #f)
end method note-gadget-disabled;
||#

(defmethod note-gadget-disabled (client (box <gadget-box-pane-mixin>))
  (declare (ignore client))
  (update-button-enabled-states box nil))



#||

/// Generic implementation of button-box using just buttons

define sealed class <button-box-pane-mixin> (<gadget-box-pane-mixin>)
end class <button-box-pane-mixin>;
||#

(defclass <button-box-pane-mixin> (<gadget-box-pane-mixin>) ())


#||
define method initialize
    (box :: <button-box-pane-mixin>, #key child) => ()
  when (child)
    // Reverse engineer the buttons from the sheet hierarchy
    //--- One flaw is that we can't notice any new buttons that get added
    let buttons = find-button-box-buttons(box, child);
    gadget-box-buttons(box) := buttons;
    box.%items := map-as(<vector>, gadget-id, buttons)
  end;
  next-method()
end method initialize;
||#

;; Yes, this is done AFTER the default initialization, but before any
;; actual :after method. Specifically we want this done before the
;; :AFTER method of the superclass is run (but after all "standard"
;; init is finished)
(defmethod initialize-instance ((box <button-box-pane-mixin>)
				&key
				child
				&allow-other-keys)
  (call-next-method)
  (when child
    ;; Reverse engineer the buttons from the sheet hierarchy
    ;;--- One flaw is that we can't notice any new buttons that get added
    (let ((buttons (find-button-box-buttons box child)))
      (setf (gadget-box-buttons box) buttons)
      ;; FIXME: Ugh, way too much consing and messing about... sort out
      ;; code that references ITEMS so it works with arbitrary sequences
      ;; rather than array-with-fill-pointers.
      (%items-setter (MAP-AS (MAKE-STRETCHY-VECTOR) #'gadget-id buttons) box))))


#||
define method find-button-box-buttons
    (box :: <button-box-pane-mixin>, child :: <sheet>)
 => (buttons :: <sequence>)
  let button-class = button-class-for-gadget-box(box);
  let buttons = make(<stretchy-vector>);
  do-sheet-tree(method (sheet)
		  when (instance?(sheet, button-class))
                    gadget-client(sheet) := box;
		    add!(buttons, sheet)
		  end
		end,
		child);
  buttons
end method find-button-box-buttons;
||#

(defmethod find-button-box-buttons ((box <button-box-pane-mixin>) (child <sheet>))
  (let ((button-class (button-class-for-gadget-box box))
	(buttons (make-array 0 :adjustable t :fill-pointer t)))
    (do-sheet-tree #'(lambda (sheet)
		       (when (typep sheet button-class)
			 (setf (gadget-client sheet) box)
			 (setf buttons (add! buttons sheet))))
      child)
    buttons))


#||
/// Push box panes

define sealed class <push-box-pane> 
    (<button-box-pane-mixin>,
     <push-box>,
     <single-child-wrapping-pane>)
end class <push-box-pane>;
||#

(defclass <push-box-pane>
    (<button-box-pane-mixin>
     <push-box>
     <single-child-wrapping-pane>)
  ())


#||
define method class-for-make-pane 
    (framem :: <frame-manager>, class == <push-box>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<push-box-pane>, #f)
end method class-for-make-pane;

define sealed domain make (singleton(<push-box-pane>));
define sealed domain initialize (<push-box-pane>);
||#

(defmethod class-for-make-pane ((framem <frame-manager>) (class (eql (find-class '<push-box>))) &key)
  (values (find-class '<push-box-pane>) nil))


#||
define sealed method button-class-for-gadget-box
    (box :: <push-box-pane>) => (class :: <class>)
  <push-button>
end method button-class-for-gadget-box;
||#

(defmethod button-class-for-gadget-box ((box <push-box-pane>))
  (find-class '<push-button>))


#||
define method do-execute-activate-callback
    (button :: <push-button>, box :: <push-box-pane>, id) => ()
  ignore(id);
  gadget-value(box) := gadget-value(button);
  execute-activate-callback(box, gadget-client(box), gadget-id(box));
  next-method()
end method do-execute-activate-callback;
||#

(defmethod do-execute-activate-callback ((button <push-button>) (box <push-box-pane>) id)
  (declare (ignore id))
  (setf (gadget-value box) (gadget-value button))
  (execute-activate-callback box (gadget-client box) (gadget-id box))
  (call-next-method))


#||
/// Radio box panes

define sealed class <radio-box-pane>
    (<button-box-pane-mixin>,
     <radio-box>,
     <single-child-wrapping-pane>)
end class <radio-box-pane>;
||#

(defclass <radio-box-pane>
    (<button-box-pane-mixin>
     <radio-box>
     <single-child-wrapping-pane>)
  ())


#||
define method class-for-make-pane 
    (framem :: <frame-manager>, class == <radio-box>, #key)
 => (class :: <class>, options :: false-or(<sequence>));
  values(<radio-box-pane>, #f)
end method class-for-make-pane;

define sealed domain make (singleton(<radio-box-pane>));
define sealed domain initialize (<radio-box-pane>);
||#

(defmethod class-for-make-pane ((framem <frame-manager>) (class (eql (find-class '<radio-box>))) &key)
  (values (find-class '<radio-box-pane>) nil))


#||
define sealed method button-class-for-gadget-box
    (box :: <radio-box-pane>) => (class :: <class>)
  <radio-button>
end method button-class-for-gadget-box;
||#

(defmethod button-class-for-gadget-box ((box <radio-box-pane>))
  (find-class '<radio-button>))


#||
define method do-execute-value-changed-callback
    (button :: <radio-button>, box :: <radio-box-pane>, id) => ()
  ignore(id);
  gadget-selection(box, do-callback?: #t)
    := if (gadget-value(button) == #t)
	 vector(gadget-box-button-index(box, button))
       else
	 #[]
       end;
  next-method()
end method do-execute-value-changed-callback;
||#

(defmethod do-execute-value-changed-callback ((button <radio-button>) (box <radio-box-pane>) id)
  (declare (ignore id))
  (setf (gadget-selection box :do-callback? t)
	(if (eql (gadget-value button) t)
	    (vector (gadget-box-button-index box button))
	    #()))
  (call-next-method))


#||
define method do-execute-activate-callback
    (button :: <radio-button>, box :: <radio-box-pane>, id) => ()
  ignore(id);
  execute-activate-callback(box, gadget-client(box), gadget-id(box));
  next-method()
end method do-execute-activate-callback;
||#

(defmethod do-execute-activate-callback ((button <radio-button>) (box <radio-box-pane>) id)
  (declare (ignore id))
  (execute-activate-callback box (gadget-client box) (gadget-id box))
  (call-next-method))


#||
/// Check box panes

define sealed class <check-box-pane>
    (<button-box-pane-mixin>,
     <check-box>,
     <single-child-wrapping-pane>)
end class <check-box-pane>;
||#

(defclass <check-box-pane>
    (<button-box-pane-mixin>
     <check-box>
     <single-child-wrapping-pane>)
  ())


#||
define method class-for-make-pane 
    (framem :: <frame-manager>, class == <check-box>, #key)
 => (class :: <class>, options :: false-or(<sequence>));
  values(<check-box-pane>, #f)
end method class-for-make-pane;

define sealed domain make (singleton(<check-box-pane>));
define sealed domain initialize (<check-box-pane>);
||#

(defmethod class-for-make-pane ((framem <frame-manager>) (class (eql (find-class '<check-box>))) &key)
  (values (find-class '<check-box-pane>) nil))


#||
define sealed method button-class-for-gadget-box
    (box :: <check-box-pane>) => (class :: <class>)
  <check-button>
end method button-class-for-gadget-box;
||#

(defmethod button-class-for-gadget-box ((box <check-box-pane>))
  (find-class '<check-button>))


#||
define method do-execute-value-changed-callback
    (button :: <check-button>, box :: <check-box-pane>, id) => ()
  ignore(id);
  let index = gadget-box-button-index(box, button);
  let new-selection
    = if (gadget-value(button) == #t)
        add-new(gadget-selection(box), index)
      else
	remove(gadget-selection(box), index)
      end;
  gadget-selection(box, do-callback?: #t) := new-selection;
  next-method()
end method do-execute-value-changed-callback;
||#

(defmethod do-execute-value-changed-callback ((button <check-button>) (box <check-box-pane>) id)
  (declare (ignore id))
  (let* ((index (gadget-box-button-index box button))
	 (new-selection (if (gadget-value button)
			    ;; MAKE A NEW VECTOR WITH THE OLD SELECTION +
			    ;; THE NEW SELECTION.
			    ;; FIXME: THIS LOOKS HORRIBLY INEFFICIENT. IS THERE
			    ;; A BETTER WAY?
			    ;; DOES IT MATTER THAT WE LOSE ANY FILL POINTER (FROM
			    ;; (GADGET-SELECTION BOX)) HERE?
			    (apply #'vector index (coerce (gadget-selection box) 'list))
			    ;; FIXME: CHECK FOR OCCURRENCES OF "DELETE" AND
			    ;; ENSURE THEY DO WHAT WE WANT!
			    (remove index (gadget-selection box) :test #'=))))
    (setf (gadget-selection box :do-callback? t) new-selection))
  (call-next-method))


#||
define method do-execute-activate-callback
    (button :: <check-button>, box :: <check-box-pane>, id) => ()
  ignore(id);
  execute-activate-callback(box, gadget-client(box), gadget-id(box));
  next-method()
end method do-execute-activate-callback;
||#

(defmethod do-execute-activate-callback ((button <check-button>) (box <check-box-pane>) id)
  (declare (ignore id))
  (execute-activate-callback box (gadget-client box) (gadget-id box))
  (call-next-method))




