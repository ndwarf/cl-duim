;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GADGETS-INTERNALS -*-
(in-package #:duim-gadgets-internals)

(defmethod print-object ((gadget <labelled-gadget-mixin>) stream)
  (let ((label (gadget-label gadget)))
    (if (typep (type-of label) 'string)
	(print-unreadable-object (gadget stream :type t :identity t)
	  (format stream "~a" label))
      (call-next-method))))

#||
define method print-object
    (gadget :: <labelled-gadget-mixin>, stream :: <stream>) => ()
  let label = gadget-label(gadget);
  if (instance?(label, <string>))
    printing-object (gadget, stream, type?: #t, identity?: #t)
      format(stream, "%=", label)
    end
  else
    next-method()
  end
end method print-object;
||#
