;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

(asdf:defsystem :frames
    :description "DUIM frames"
    :long-description "Provides support for frames. A DUIM frame is a combination of a set of nested sheets, together with an event loop that describes the behavior of the elements in those sheets. DUIM frames can be used to specify whether a given user interface is displayed in an application as a dialog box, or a more straightforward window, or as a task wizard, and so on."
    :author      "Scott McKay, Andy Armstrong (Lisp port: Duncan Rose <duncan@robotcat.demon.co.uk>)"
    :version     (:read-file-form "version.sexp")
    :licence     "BSD-2-Clause"
    :depends-on (#:duim-utilities
		 #:geometry
		 #:dcs
		 #:sheets
		 #:graphics
		 #:layouts
		 #:gadgets)
    :serial t
    :components
    ((:file "package")
     (:file "protocols")
     (:file "classes")
     (:file "events")
     (:file "frames")
     (:file "embedded-frames")
     (:file "dialogs")
     (:file "commands")
     (:file "decorators")
     (:file "help")
     (:file "command-tables")
     (:file "standard-commands")
     (:file "completer")
     (:file "progress-notes")
     (:file "contain")
     (:file "debug")
     (:file "printers")))

