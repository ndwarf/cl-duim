;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-FRAMES-INTERNALS -*-
(in-package #:duim-frames-internals)

;;; Note: Genera claims there are duplicate type declarations for
;;; ACCELERATORS, without pointing out which definitions have the
;;; duplicates.

#||
/// Frames

define constant <frame-state>
    = one-of(#"detached", #"unmapped", #"mapped", #"destroyed");
||#

;;; TODO: introduce "keyword-type" and make these deftypes be subtypes
;;;       of it... since constants, would be +detached+, +unmapped+, ...

(deftype <frame-state> ()
  '(member :detached :unmapped :mapped :destroyed))


#||
define constant <frame-mode>
    = one-of(#"modeless", #"modal", #"system-modal");
||#

(deftype <frame-mode> ()
  '(member :modeless :modal :system-modal))


#||
define protocol <<frame-protocol>> ()
  getter frame-layout
    (frame :: <abstract-frame>) => (layout :: false-or(<abstract-sheet>));
  setter frame-layout-setter
    (layout :: false-or(<abstract-sheet>), frame :: <abstract-frame>)
 => (layout :: false-or(<abstract-sheet>));
  function layout-frame
    (frame :: <abstract-frame>, #key width, height) => ();
  getter frame-owner
    (frame :: <abstract-frame>) => (owner :: false-or(<abstract-frame>));
  getter frame-owned-frames
    (frame :: <abstract-frame>) => (owned-frames :: <sequence>);
  getter frame-owned-menus
    (frame :: <abstract-frame>) => (owned-menus :: <sequence>);
  getter frame-menu-bar
    (frame :: <abstract-frame>) => (menu-bar :: false-or(<menu-bar>));
  setter frame-menu-bar-setter
    (menu-bar :: false-or(<menu-bar>), frame :: <abstract-frame>)
 => (menu-bar :: false-or(<menu-bar>));
  getter frame-tool-bar
    (frame :: <abstract-frame>) => (tool-bar :: false-or(<tool-bar>));
  setter frame-tool-bar-setter
    (tool-bar :: false-or(<tool-bar>), frame :: <abstract-frame>)
 => (tool-bar :: false-or(<tool-bar>));
  getter frame-status-bar
    (frame :: <abstract-frame>) => (status-bar :: false-or(<status-bar>));
  setter frame-status-bar-setter
    (status-bar :: false-or(<status-bar>), frame :: <abstract-frame>)
 => (status-bar :: false-or(<status-bar>));
  getter frame-status-message
    (frame :: <abstract-frame>) => (message :: false-or(<string>));
  setter frame-status-message-setter
    (message :: false-or(<string>), frame :: <abstract-frame>)
 => (message :: false-or(<string>));
//  getter frame-input-focus		// defined in DUIM-sheets
//    (frame :: <abstract-frame>) => (sheet :: false-or(<abstract-sheet>));
//  setter frame-input-focus-setter	// defined in DUIM-sheets
//    (sheet :: false-or(<abstract-sheet>), frame :: <abstract-frame>)
// => (sheet :: false-or(<abstract-sheet>));
  getter frame-command-table
    (frame :: <abstract-frame>) => (command-table :: false-or(<command-table>));
  setter frame-command-table-setter
    (command-table :: false-or(<command-table>), frame :: <abstract-frame>)
 => (command-table :: false-or(<command-table>));
  function note-command-table-changed
    (framem :: <abstract-frame-manager>, frame :: <abstract-frame>) => ();
  function handle-id-activation
    (frame :: <abstract-frame>, id) => (handled? :: <boolean>);
  function frame-top-level-sheet-class
    (frame :: <abstract-frame>, #key, #all-keys) => (class :: <class>);
  function frame-wrapper
    (framem :: <abstract-frame-manager>, frame :: <abstract-frame>,
     sheet :: false-or(<abstract-sheet>))
 => (wrapper :: false-or(<abstract-sheet>));
  getter frame-resource-id
    (frame :: <abstract-frame>) => (resource-id);
  function start-frame
    (frame :: <abstract-frame>) => (status-code :: false-or(<integer>));
  function port-start-frame
    (port :: <abstract-port>, frame :: <abstract-frame>)
 => (status-code :: false-or(<integer>));
  function make-event-queue
    (framem :: <abstract-frame-manager>, frame :: <abstract-frame>)
 => (event-queue :: false-or(<event-queue>));
  function frame-needs-event-queue?
    (framem :: <abstract-frame-manager>, frame :: <abstract-frame>, #key mode)
 => (needs-event-queue? :: <boolean>);
  function frame-can-exit?
    (frame :: <abstract-frame>) => (can-exit? :: <boolean>);
  function frame-top-level
    (frame :: <abstract-frame>) => (#rest values);
  // Enabling and disabling a whole frame (for modal dialog parents)
  getter frame-enabled?
    (frame :: <abstract-frame>) => (enabled? :: <boolean>);
  setter frame-enabled?-setter
    (enabled? :: <boolean>, frame :: <abstract-frame>) => (enabled? :: <boolean>);
  function note-frame-enabled
    (framem :: <abstract-frame-manager>, frame :: <abstract-frame>) => ();
  function note-frame-disabled
    (framem :: <abstract-frame-manager>, frame :: <abstract-frame>) => ();
  // Iconifying, etc
  function frame-iconified?
    (frame :: <abstract-frame>) => (iconified? :: <boolean>);
  function frame-iconified?-setter
    (iconified? :: <boolean>, frame :: <abstract-frame>) => (iconified? :: <boolean>);
  function frame-maximized?
    (frame :: <abstract-frame>) => (maximized? :: <boolean>);
  function frame-maximized?-setter
    (maximized? :: <boolean>, frame :: <abstract-frame>) => (maximized? :: <boolean>);
  function raise-frame (frame :: <abstract-frame>, #key activate?) => ();
  function lower-frame (frame :: <abstract-frame>) => ();
  // For exit buttons, etc
  getter frame-default-button
    (frame :: <abstract-frame>) => (button :: false-or(<button>));
  getter frame-default-button-setter
    (button :: false-or(<button>), frame :: <abstract-frame>)
 => (button :: false-or(<button>));
  getter frame-accelerators
    (frame :: <abstract-frame>) => (accelerators :: <sequence>);
  getter frame-accelerators-setter
    (accelerators :: false-or(<sequence>), frame :: <abstract-frame>)
 => (accelerators :: false-or(<sequence>));
  function note-accelerators-changed
    (framem :: <abstract-frame-manager>, frame :: <abstract-frame>) => ();
  // User frame classes can use this to watch transition in 'frame-state',
  // 'frame-iconified?', and 'frame-maximized?'
  function note-frame-state-changed
    (frame :: <abstract-frame>, old-state, new-state) => ();
  // User frame classes can add methods to these, provided the methods
  // start with a call to 'next-method'
  function note-frame-mapped
    (framem :: <abstract-frame-manager>, frame :: <abstract-frame>) => ();
  function note-frame-unmapped
    (framem :: <abstract-frame-manager>, frame :: <abstract-frame>) => ();
  function note-frame-iconified
    (framem :: <abstract-frame-manager>, frame :: <abstract-frame>) => ();
  function note-frame-deiconified
    (framem :: <abstract-frame-manager>, frame :: <abstract-frame>) => ();
  function note-frame-maximized
    (framem :: <abstract-frame-manager>, frame :: <abstract-frame>) => ();
  function note-frame-unmaximized
    (framem :: <abstract-frame-manager>, frame :: <abstract-frame>) => ();
  // These communicate to the back-end to do some layout work
  function update-frame-layout
    (framem :: <abstract-frame-manager>, frame :: <abstract-frame>) => ();
  function update-frame-wrapper
    (framem :: <abstract-frame-manager>, frame :: <abstract-frame>) => ();
  function note-frame-title-changed
    (framem :: <abstract-frame-manager>, frame :: <abstract-frame>) => ();
  function note-frame-icon-changed
    (framem :: <abstract-frame-manager>, frame :: <abstract-frame>) => ();
  // The most basic support for documents
  getter frame-document
    (frame :: <abstract-frame>) => (document :: false-or(<object>));
  setter frame-document-setter
    (document :: false-or(<object>), frame :: <abstract-frame>)
 => (document :: false-or(<object>));
  // Is this frame covered by any other window?
  function frame-occluded?
    (frame :: <abstract-frame>) => (occluded? :: <boolean>);
  function do-frame-occluded?
    (framem :: <abstract-frame-manager>, frame :: <abstract-frame>) => (occluded? :: <boolean>);
end protocol <<frame-protocol>>;
||#

;;; <<frame-protocol>> definition moved to 'protocols.lisp' file.

(defgeneric frame-state (frame)
  (:documentation
"
Returns the visible state of the specified frame. The return value
from this function indicates whether frame is currently iconified,
whether it is mapped or unmapped, whether it has been destroyed, or
whether it has become detached from the thread of which it was a part.

Example

The following example creates a simple frame, then displays it and
tests its position. You should run this code in the interactor,
pressing the RETURN key at the points indicated.

    (defparameter *frame*
                  (make-instance '<simple-frame>
                                 :title \"A frame\"
                                 :layout
                                 (make-pane '<button>))) RETURN

    (start-frame *frame*) RETURN
    (frame-state *frame*) RETURN
    => :mapped
"))

(defgeneric (setf frame-state) (state frame))
(defgeneric frame-mode (frame)
  (:documentation
"
Returns the mode of _frame_. This is the same value as was specified
for the :mode init-keyword when the frame was created.

If _frame_ is modal, such as a dialog, then it must be dismissed
before the user can interact with the user interface of an
application (for instance, before a menu can be displayed).

If _frame_ is modeless, then the user can interact with its parent
frame while the frame is still visible. Typically, the user will move
the frame to a convenient position on the screen and continue work,
keeping the frame on screen for as long as is desired. For example it
is often useful to make the Find dialog box in an application
modeless, so that the user can keep it on screen while performing
other tasks.

If _frame_ is system-modal, then it prevents the user from interacting
with *any* other running applications, such as the Shutdown dialog in
Windows 95. System modal frames are rarely used, and should be used
with caution.

Note: You can only set the mode of a frame when it is first
created. The mode cannot subsequently be changed.
"))

(defgeneric (setf frame-mode) (mode frame))
(defgeneric frame-fixed-width? (frame)
  (:documentation
"
Returns true if the width of _frame_ is not resizable.
"))

(defgeneric frame-fixed-height? (frame)
  (:documentation
"
Returns true if the height of _frame_ is not resizable.
"))

(defgeneric frame-resizable? (frame)
  (:documentation
"
Returns true if _frame_ is resizable, that is can have one or both of
its width and height modified by the user.
"))

(defgeneric frame-alt-key-is-meta? (frame))
(defgeneric (setf frame-alt-key-is-meta?) (alt-is-meta? frame))
(defgeneric frame-allow-control-alt? (frame))
(defgeneric (setf frame-allow-control-alt?) (allow-control-alt? frame))
(defgeneric frame-save-under? (frame))
(defgeneric frame-minimize-box? (frame))
(defgeneric frame-maximize-box? (frame))
(defgeneric frame-always-on-top? (frame))
(defgeneric frame-centered? (frame))
(defgeneric frame-keyboard-interrupt? (frame))
(defgeneric frame-controlling-frame (frame)
  (:documentation
"
Go up the 'frame-owner' path until we find a frame with a thread.
"))

(defgeneric frame-palette (frame)
  (:documentation
"
Returns the palette used in _frame_.
"))

(defgeneric (setf frame-palette) (palette frame)
  (:documentation
"
Sets the palette used in _frame_.
"))

(defgeneric frame-position (frame)
  (:documentation
"
Returns the position on the screen of _frame_. Coordinates are
expressed relative to the top left corner of the screen, measured in
pixels.

Example

The following example creates a simple frame, then displays it and
tests its position. You should run this code in the interactor,
pressing the RETURN key at the points indicated.

    (defparameter *frame*
                  (make-instance '<simple-frame>
                                 :title \"A frame\"
                                 :layout
                                 (make-pane '<button>))) RETURN

    (start-frame *frame*) RETURN

    (frame-position *frame*) RETURN
"))

(defgeneric set-frame-position (frame x y &key constrain?)
  (:documentation
"
Sets the position of _frame_. The coordinates _x_ and _y_ are measured
from the top left of the screen, measured in pixels.
"))

(defgeneric constrained-frame-position (frame x y)
  (:documentation
"
Put _frame_ at _x_, _y_, ensuring its visibility.

Returns (VALUES X Y)

This method provdes coordinates that would put the frame as close
to _x_, _y_ as possible whilst ensuring the frame is fully visible
on the display.
"))

(defgeneric frame-size (frame)
  (:documentation
"
Returns the size of _frame_, measured in pixels.

Example

The following example creates a simple frame, then displays it and
tests its size. You should run this code in the interactor, pressing
the RETURN key at the points indicated.

    (defparameter *frame*
                  (make-instance '<simple-frame>
                                 :title \"A frame\"
                                 :layout
                                 (make-pane '<button>))) RETURN

    (start-frame *frame*) RETURN

    (frame-size *frame*) RETURN
"))

(defgeneric set-frame-size (frame width height)
  (:documentation
"
Sets the size of _frame_.

Example:

The following example creates and displays a simple frame, then
resizes it. You should run this code in the interactor, pressing the
RETURN key at the points indicated.

    (defparameter *frame*
                  (make-instance '<simple-frame> :title \"A frame\"
                                 :layout
                                 (make-pane '<button>))) RETURN

    TODO: Doesn't this need a '(start-frame *frame*)'?

    (set-frame-size *frame* 100 500) RETURN
"))

(defgeneric do-with-background-cursor (sheet cursor continuation))
(defgeneric note-background-operation-started (frame &key cursor))
(defgeneric note-background-operation-finished (frame))
(defgeneric find-frame (frame-class &rest initargs &key create? activate? own-thread? port frame-manager test z-order &allow-other-keys)
  (:documentation
"
This function creates a frame of the specified type if one does not
already exist, and then runs it, possibly in its own thread. If one
already exists, then it is selected.

The _frame-class_ argument specifies the class of frame that is being
searched for. By default, if a match is not found, then an instance of
this class will be created.

The _init-args_ supplied are the slot values that should be passed to
the instance of frame-class. Either an existing frame must be found
that has the specified slot values, or a new one will be created.

If _create?_ is nil, then a new frame will not be created if it does
not already exist.

If _own-thread?_ is true, the frame will run in its own thread if one
is created.

The _port_ and _frame-manager_ arguments specify a port and frame
manager which control the frame being searched for, or under the
control of which a new frame should be created.

If desired, you can supply a _test_ which must evaluate to true for a
frame to match successfully.
"))

(defgeneric attach-frame (frame-manager frame)
  (:documentation
"
Attach the frame to a frame manager.
"))

(defgeneric detach-frame (frame-manager frame))
(defgeneric (setf frame-title) (title frame)
  (:documentation
"
Sets the title of _frame_. The title of a frame is displayed in the
title bar of the frame. If title is #f, then the platform attempts to
remove the title bar from the frame, if possible.
"))

(defgeneric (setf frame-icon) (icon frame)
  (:documentation
"
Specifies the icon associated with _frame_. This icon is used when the
frame is iconified, and in Windows 95 and Windows NT 4.0 is also
visible on the left hand side of the title bar of the frame.
"))

(defgeneric frame-top-level-sheet-size (frame-manager frame width height))
(defgeneric frame-mapped? (frame)
  (:documentation
"
Returns true if _frame_ is mapped, that is, is currently displayed
on-screen. Note that a frame is considered to be mapped if it is
anywhere on the screen, even if it is not completely visible because
other windows are covering it either partially or completely, or if it
is iconized.

Example

The following example creates a simple frame, then displays it and
exits it. In between starting and exiting the frame, frame-mapped? is
called. You should run this code in the interactor, pressing the
RETURN key at the points indicated.

    (defparameter *frame*
                  (make-instance '<simple-frame>
                                 :title \"A frame\" 
                                 :layout
                                 (make-pane '<button>))) RETURN

    (start-frame *frame*) RETURN

    (frame-mapped? *frame*) RETURN
    => t

    (exit-frame *frame*) RETURN

    (frame-mapped? *frame*) RETURN
    => nil
"))

(defgeneric (setf frame-mapped?) (mapped? frame)
  (:documentation
"
Maps or unmaps _frame_, that is, displays frame on the screen or
removes it from the screen, depending on whether _mapped?_ is true or
false. Note that a frame is considered to be mapped if it is anywhere
on the screen, even if it is not completely visible because other
windows are covering it either partially or completely, or if it is
iconized.

Example

The following example creates a simple frame, then displays it and
unmaps it using frame-mapped?-setter rather than start-frame and
exit-frame. You should run this code in the interactor, pressing the
RETURN key at the points indicated.

    (defparameter *frame*
                  (make-instance '<simple-frame> :title \"A frame\" 
                                 :layout
                                 (make-pane '<button>))) RETURN

    (setf (frame-mapped? *frame*) t) RETURN
    (setf (frame-mapped? *frame*) nil) RETURN

TODO: The frame is not displayed when mappedp is set to t in this
example! Why not? The event loop isn't started, but that's to be
expected...
"))

(defgeneric map-frame (frame))
(defgeneric unmap-frame (frame))
(defgeneric update-frame-settings (frame-manager frame))
(defgeneric iconify-frame (frame)
  (:documentation
"
Iconifies _frame_. The appearance of the iconified frame depends on
the behavior of the operating system in which the application is
running. For instance, in Windows 95 or Windows NT 4.0, the icon is
displayed in the task bar at the bottom of the screen.

Example

The following example creates and displays a simple frame, then
iconifies it. You should run this code in the interactor, pressing the
RETURN key at the points indicated.

    (defparameter *frame*
                  (make-instance '<simple-frame>
                                 :title \"A frame\"
                                 :layout
                                 (make-pane '<button>))) RETURN

    (start-frame *frame*) RETURN
    (iconify-frame *frame*) RETURN
"))

(defgeneric deiconify-frame (frame)
  (:documentation
"
Displays a frame that has previously been iconified on screen.

Example:

The following example creates and displays a simple frame, then
iconifies it and deiconifies it.

    (defparameter *frame*
                  (make-instance '<simple-frame>
                                 :title \"A frame\"
                                 :layout
                                 (make-pane '<button>)))

    (start-frame *frame*)
    (iconify-frame *frame*)
    (deiconify-frame *frame*)
"))

(defgeneric maximize-frame (frame))
(defgeneric unmaximize-frame (frame))
(defgeneric in-frame-thread? (frame))
(defgeneric queue-call-in-frame (frame function &rest args))
(defgeneric queue-apply-in-frame (frame function arg &rest more-args))
(defgeneric call-in-frame (frame function &rest args)
  (:documentation
"
Calls _function_ with the given arguments in the main thread of
_frame_.
"))

(defgeneric apply-in-frame (frame function arg &rest more-args)
  (:documentation
"
Applies _function_ to the given arguments in the main thread of
_frame_. You must supply at least one argument (_arg_), though you can
optionally supply as many additional arguments as you like.
"))

(defgeneric execute-frame-top-level (frame))
(defgeneric note-frame-top-level-finished (frame))
(defgeneric exit-frame (frame &key destroy?)
  (:documentation
"
Unmaps _frame_, removing the associated sheet and its children from
the screen. If _destroy?_ is true, then the frame is destroyed
completely, via a call to 'destroy-frame'.

If _destroy?_ is t, then dialog is destroyed.

Example

The following example creates a simple frame, then displays it and
exits it. You should run this code in the interactor, pressing the
RETURN key at the points indicated.

    (defparameter *frame*
                  (make-instance '<simple-frame> :title \"A frame\" 
                                 :layout
                                 (make-pane '<button>))) RETURN

    (start-frame *frame*) RETURN

    (exit-frame *frame*) RETURN
"))

(defgeneric display-pointer-documentation (frame sheet string))
(defgeneric do-display-pointer-documentation (frame-manager frame sheet string))
(defgeneric pointer-documentation-sheet (frame-manager frame sheet)
  (:documentation
"
This method allows a frame to redirect the pointer documentation.
"))


#||
/// Decoding the frame flags

// Bits 0..2 are the frames's state
define constant %frame_state_mask :: <integer> = #o07;
define constant %frame_detached   :: <integer> = #o00;
define constant %frame_unmapped   :: <integer> = #o01;
define constant %frame_mapped     :: <integer> = #o02;
define constant %frame_destroyed  :: <integer> = #o03;
||#

(defconstant %frame_state_mask #o07)
(defconstant %frame_detached   #o00)
(defconstant %frame_unmapped   #o01)
(defconstant %frame_mapped     #o02)
(defconstant %frame_destroyed  #o03)


#||
define constant $frame-states :: <simple-object-vector>
    = #[#"detached", #"unmapped", #"mapped", #"destroyed"];
||#

(defvar +frame-states+ #(:detached :unmapped :mapped :destroyed))


#||
// Bits 3..5 are the frames's mode
define constant %frame_mode_shift   :: <integer> = 3;
define constant %frame_mode_mask    :: <integer> = #o70;
define constant %frame_modeless     :: <integer> = #o00;
define constant %frame_modal        :: <integer> = #o10;
define constant %frame_system_modal :: <integer> = #o20;
||#

(defconstant %frame_mode_shift   3)
(defconstant %frame_mode_mask    #o70)
(defconstant %frame_modeless     #o00)
(defconstant %frame_modal        #o10)
(defconstant %frame_system_modal #o20)


#||
define constant $frame-modes :: <simple-object-vector>
    = #[#"modeless", #"modal", #"system-modal", #"unknown"];
||#

(defvar +frame-modes+ #(:modeless :modal :system-modal :unknown))


#||
// Bit 6 is the enabled flag
define constant %frame_enabled :: <integer> = #o100;
||#

(defconstant %frame_enabled #o100)


#||
// Bits 7 and 8 are the iconified and maximized flags
define constant %frame_iconified :: <integer> = #o200;
define constant %frame_maximized :: <integer> = #o400;
||#

(defconstant %frame_iconified #o200)
(defconstant %frame_maximized #o400)


#||
// Bits 9 and 10 are the fixed width/height flags
define constant %frame_fixed_width  :: <integer> = #o1000;
define constant %frame_fixed_height :: <integer> = #o2000;
||#

(defconstant %frame_fixed_width  #o1000)
(defconstant %frame_fixed_height #o2000)


#||
// Bit 11 is the save-under flag
define constant %frame_save_under   :: <integer> = #o4000;
||#

(defconstant %frame_save_under #o4000)


#||
// Bits 12 and 13 are alt=meta and allow-control-meta flags
define constant %frame_alt_is_meta       :: <integer> = #o10000;
define constant %frame_allow_control_alt :: <integer> = #o20000;
||#

(defconstant %frame_alt_is_meta       #o10000)
(defconstant %frame_allow_control_alt #o20000)


#||
// Bit 14 is the "always on top" flag
define constant %frame_on_top :: <integer> = #o40000;
||#

(defconstant %frame_on_top #o40000)


#||
// Bits 15 and 16 are the minimize/maximize box flags
define constant %frame_minimize_box :: <integer> = #o100000;
define constant %frame_maximize_box :: <integer> = #o200000;
||#

(defconstant %frame_minimize_box #o100000)
(defconstant %frame_maximize_box #o200000)


#||
// Bit 17 is the "center over parent" flag
define constant %frame_centered :: <integer> = #o400000;
||#

(defconstant %frame_centered #o400000)


#||
// Bit 18 is the "handle keyboard interrupt" flag
define constant %keyboard_interrupt :: <integer> = #o1000000;
||#

(defconstant %keyboard_interrupt #o1000000)


#||
define constant $initial-frame-flags :: <integer>
    = logior(%frame_detached, %frame_modeless, %frame_enabled,
	     %frame_minimize_box, %frame_maximize_box, %keyboard_interrupt);
||#

(defvar +initial-frame-flags+ (logior %frame_detached       ; #o00
				      %frame_modeless       ; #o00
				      %frame_enabled        ; #o100
				      %frame_minimize_box   ; #o100000
				      %frame_maximize_box   ; #o200000
				      %keyboard_interrupt)) ; #o1000000


(defun decode-frame-flags (flags)
"
Return a sequence of keywords representing the frame
flags.
"
  (let* ((state (logand flags %frame_state_mask))
	 (mode (ash (logand flags %frame_mode_mask) (- %frame_mode_shift)))
	 (enabled? (not (zerop (logand flags %frame_enabled))))
	 (iconified? (not (zerop (logand flags %frame_iconified))))
	 (maximized? (not (zerop (logand flags %frame_maximized))))
	 (fixed-width? (not (zerop (logand flags %frame_fixed_width))))
	 (fixed-height? (not (zerop (logand flags %frame_fixed_height))))
	 (alt-is-meta? (not (zerop (logand flags %frame_alt_is_meta))))
	 (allow-control-alt? (not (zerop (logand flags %frame_allow_control_alt))))
	 (save-under? (not (zerop (logand flags %frame_save_under))))
	 (minimize-box? (not (zerop (logand flags %frame_minimize_box))))
	 (maximize-box? (not (zerop (logand flags %frame_maximize_box))))
	 (always-on-top? (not (zerop (logand flags %frame_on_top))))
	 (centered? (not (zerop (logand flags %frame_centered))))
	 (allow-kb-interrupt? (not (zerop (logand flags %keyboard_interrupt))))
	 (decoded-state (make-array 0 :adjustable t :fill-pointer t)))
    (vector-push-extend (aref +frame-states+ state) decoded-state)
    (vector-push-extend (aref +frame-modes+ mode) decoded-state)
    (when enabled? (vector-push-extend :enabled decoded-state))
    (when iconified? (vector-push-extend :iconified decoded-state))
    (when maximized? (vector-push-extend :maximized decoded-state))
    (when fixed-width? (vector-push-extend :fixed-width decoded-state))
    (when fixed-height? (vector-push-extend :fixed-height decoded-state))
    (when alt-is-meta? (vector-push-extend :alt-is-meta decoded-state))
    (when allow-control-alt? (vector-push-extend :allow-control-alt decoded-state))
    (when save-under? (vector-push-extend :save-under decoded-state))
    (when minimize-box? (vector-push-extend :minimize-box decoded-state))
    (when maximize-box? (vector-push-extend :maximize-box decoded-state))
    (when always-on-top? (vector-push-extend :always-on-top decoded-state))
    (when centered? (vector-push-extend :centered decoded-state))
    (when allow-kb-interrupt? (vector-push-extend :keyboard-interrupt decoded-state))
    decoded-state))


#||
define open abstract primary class <basic-frame> (<frame>)
  slot frame-title :: false-or(<string>) = #f,
    init-keyword: title:,
    setter: %title-setter;
  slot frame-icon :: false-or(<image>) = #f,
    init-keyword: icon:,
    setter: %icon-setter;
  sealed slot frame-manager :: false-or(<frame-manager>) = #f;
  sealed slot frame-owner   :: false-or(<frame>) = #f,
    init-keyword: owner:;
  sealed slot frame-flags :: <integer> = $initial-frame-flags;
  sealed constant slot frame-owned-frames :: <stretchy-object-vector>
    = make(<stretchy-vector>);
  sealed constant slot frame-owned-menus  :: <stretchy-object-vector>
    = make(<stretchy-vector>);
  sealed slot top-level-sheet :: false-or(<sheet>) = #f,
    init-keyword: top-level-sheet:;
  sealed slot frame-default-button :: false-or(<button>) = #f,
    init-keyword: default-button:,
    setter: %default-button-setter;
  sealed constant slot frame-geometry :: <simple-object-vector>
    = vector(#f, #f, #f, #f),		// x, y, width, height
    init-keyword: geometry:;
  sealed constant slot frame-properties :: <stretchy-object-vector>
    = make(<stretchy-vector>);
  sealed slot frame-thread = #f,
    init-keyword: thread:;
  sealed slot frame-event-queue :: false-or(<event-queue>) = #f,
    init-keyword: event-queue:;
  sealed slot event-handler :: false-or(<event-handler>) = #f,
    init-keyword: event-handler:;
  sealed slot %input-focus :: false-or(<sheet>) = #f,
    init-keyword: input-focus:;
  sealed slot frame-cursor-override :: false-or(<cursor>) = #f,
    setter: %cursor-override-setter,
    init-keyword: cursor-override:;
  sealed slot frame-background-operations :: <integer> = 0;
  sealed slot %style-descriptor :: false-or(<style-descriptor>) = #f,
    init-keyword: style-descriptor:;
  sealed slot %palette :: false-or(<palette>) = #f,
    init-keyword: palette:;
  sealed slot %exit-function :: false-or(<function>) = #f;
  sealed slot %accelerators  :: false-or(<sequence>) = #f;
  // Documents
  slot frame-document :: false-or(<object>) = #f,
    init-keyword: document:;
  // Resources
  // Use 'keyword resource-id: = $mumble-frame-id' to initialize this
  sealed constant slot frame-resource-id = #f,
    init-keyword: resource-id:;
  // For handing to top level sheet in case this is an embedded frame...
  sealed slot %container = #f,
    init-keyword: container:;
  sealed slot %container-region = #f,
    init-keyword: container-region:;
end class <basic-frame>;
||#

(defgeneric frame-event-queue (frame)
  (:documentation
"
Returns the event queue that _frame_ is running in.
"))

(defgeneric frame-icon (frame)
  (:documentation
"
Returns the icon associated with _frame_. This is the icon used to
represent the frame when it has been iconized. In Windows 95 and
Windows NT 4.0, this icon is also visible in the left hand corner of
the title bar of the frame when it is not iconized.
"))

(defgeneric frame-thread (frame)
  (:documentation
"
Returns the thread with which _frame_ is associated.

For more information about threads, refer to the manual Library
Reference: Core Features.
"))

(defgeneric frame-title (frame)
  (:documentation
"
Returns the title of _frame_. If this is #f, then the title bar is
removed from the frame, if this is possible. If this is not possible,
then a default message is displayed. Whether the title bar can be
removed from the frame or not is platform dependent.
"))

(defclass <basic-frame> (<frame>)
  ((frame-title :type (or null string) :initarg :title :initform nil :reader frame-title :writer %title-setter)
   (frame-icon :type (or null <image>) :initarg :icon :initform nil :reader frame-icon :writer %icon-setter)
   (frame-manager :type (or null <frame-manager>) :initform nil :accessor frame-manager)
   (frame-owner :type (or null <frame>) :initarg :owner :initform nil :accessor frame-owner)
   (frame-flags :type integer :initform +initial-frame-flags+ :accessor frame-flags)
   (frame-owned-frames :type vector :initform (make-array 0 :adjustable t :fill-pointer t) :reader frame-owned-frames)
   (frame-owned-menus :type vector :initform (make-array 0 :adjustable t :fill-pointer t) :reader frame-owned-menus)
   (top-level-sheet :type (or null <sheet>) :initarg :top-level-sheet :initform nil :accessor top-level-sheet)
   (frame-default-button :type (or null <button>) :initarg :default-button :initform nil
			 :reader frame-default-button :writer %default-button-setter)
   (frame-geometry :type vector :initarg :geometry :initform (vector nil nil nil nil) :reader frame-geometry)    ;; x, y, width, height
   (frame-properties :type vector :initform (make-array 0 :adjustable t :fill-pointer t) :reader frame-properties)
   (frame-thread :initform nil :initarg :thread :accessor frame-thread)
   (frame-event-queue :type (or null <event-queue>) :initarg :event-queue :initform nil :accessor frame-event-queue)
   (event-handler :type (or null <event-handler>) :initarg :event-handler :initform nil :accessor event-handler)
   (%input-focus :type (or null <sheet>) :initarg :input-focus :initform nil :accessor %input-focus)
   (frame-cursor-override :type (or null <cursor>) :initarg :cursor-override :initform nil
			  :reader frame-cursor-override :writer %cursor-override-setter)
   (frame-background-operations :type integer :initform 0 :accessor frame-background-operations)
   (%style-descriptor :type (or null <style-descriptor>) :initarg :style-descriptor :initform nil :accessor %style-descriptor)
   (%palette :type (or null <palette>) :initarg :palette :initform nil :accessor %palette)
   (%exit-function :type (or null function) :initform nil :accessor %exit-function)
   (%accelerators :type (or null sequence) :initform nil :accessor %accelerators)
   ;; Documents
   (frame-document :initarg :document :initform nil :accessor frame-document) ;; false-or(<object>)
   ;; Resources
   ;; Use 'keyword resource-id: = $mumble-frame-id' to initialize this
   (frame-resource-id :initarg :resource-id :initform nil :reader frame-resource-id)
   ;; For handing to top level sheet in case this is an embedded frame...
   (%container :initarg :container :initform nil :accessor %container)
   (%container-region :initarg :container-region :initform nil :accessor %container-region))
  (:metaclass <abstract-metaclass>))


#||
// Users can make an instance of this class, but it's better to subclass it
define open abstract primary class <simple-frame> (<basic-frame>)
  sealed slot frame-command-queue :: <event-queue> = make(<event-queue>),
    init-keyword: command-queue:;
  sealed slot frame-pointer-documentation :: false-or(<sheet>) = #f,
    init-keyword: pointer-documentation:;
  sealed slot %command-table :: false-or(<command-table>) = #f,
    init-keyword: command-table:;
  sealed slot frame-disabled-commands :: <object-table> = make(<table>);
  // The next four slots have accessors defined for them by 'define frame'
  // for every user-defined subclass of <simple-frame>
  sealed slot %layout :: false-or(<sheet>) = #f,
    init-keyword: layout:;
  sealed slot %menu-bar :: false-or(<menu-bar>) = #f,
    init-keyword: menu-bar:;
  sealed slot %tool-bar :: false-or(<tool-bar>) = #f,
    init-keyword: tool-bar:;
  sealed slot %status-bar :: false-or(<status-bar>) = #f,
    init-keyword: status-bar:;
end class <simple-frame>;
||#

;; Users can make an instance of this class, but it's better to subclass it
(defclass <simple-frame> (<basic-frame>)
  ((frame-command-queue :type <event-queue> :initarg :command-queue :initform (make-instance '<event-queue>)
			:accessor frame-command-queue)
   (frame-pointer-documentation :type (or null <sheet>) :initarg :pointer-documentation :initform nil
				:accessor frame-pointer-documentation)
   (%command-table :type (or null <command-table>) :initarg :command-table :initform nil :accessor %command-table)
   (frame-disabled-commands :type hash-table
			    ;; The test in command-table-commands is #'equalp; this one is too.
			    :initform (make-hash-table :test #'equalp) :accessor frame-disabled-commands)
   ;; The next four slots have accessors defined for them by 'define frame'
   ;; for every user-defined subclass of <simple-frame>
   (%layout :type (or null <sheet>) :initarg :layout :initform nil :accessor %layout)
   (%menu-bar :type (or null <menu-bar>) :initarg :menu-bar :initform nil :accessor %menu-bar)
   (%tool-bar :type (or null <tool-bar>) :initarg :tool-bar :initform nil :accessor %tool-bar)
   (%status-bar :type (or null <status-bar>) :initarg :status-bar :initform nil :accessor %status-bar))
  (:documentation
"
The class of simple frames.

The :command-queue init-keyword specifies a command-queue for the
frame.

The :layout init-keyword specifies a layout for the frame.

The :command-table init-keyword specifies a command table for the
frame.

The :menu-bar init-keyword specifies a menu bar for the frame.

The :tool-bar init-keyword specifies a tool bar for the frame.

The :status-bar init-keyword specifies a status bar for the frame.
")
  (:metaclass <abstract-metaclass>))


#||
define sealed class <concrete-simple-frame> (<simple-frame>)
end class <concrete-simple-frame>;

define sealed domain make (singleton(<concrete-simple-frame>));
define sealed domain initialize (<concrete-simple-frame>);
||#

(defclass <concrete-simple-frame> (<simple-frame>) ())


#||
//--- 'sideways' because <frame> is defined in DUIM-Sheets
define sealed inline sideways method make
    (class == <frame>, #rest initargs, #key, #all-keys)
 => (pane :: <concrete-simple-frame>)
  apply(make, <concrete-simple-frame>, initargs)
end method make;
||#

(defmethod make-frame (&rest initargs &key &allow-other-keys)
  (apply #'make-instance '<concrete-simple-frame> initargs))


;;; TODO: Where should this documentation for 'make' go?
#||
Creates and returns an instance of <frame> or one of its subclasses.

The _top-level_ argument specifies the top-level command-loop in which
the frame runs.

The _command-queue_ argument specifies a command-queue for the frame.

The _layout_ argument specifies a layout for the frame.

The _icon_ argument specifies an icon that will be used when the frame
is iconized. In all current versions of Windows, this icon is also
visible in the left hand corner of the title bar of the frame when it
is not iconized.

The _pointer-documentation_ argument specifies pointer-documentation
for the frame.

The _command-table_ argument specifies a command table for the frame.

The _menu-bar_ argument specifies a menu bar for the frame.

The _tool-bar_ argument specifies a tool bar for the frame.

The _status-bar_ argument specifies a status bar for the frame.

The _title_ argument specifies a title for the frame.

The _calling-frame_ argument specifies a calling frame for the frame.

The _state_ argument specifies a frame-state. The frame can be mapped
or unmapped (that is, visible on the screen, or not), iconified, or
detached.

The _geometry_ argument specifies a position for the frame. The four
components of this keyword represent the x and y position of the
frame, and the width and height of the frame, respectively.

The _resizable?_ argument specifies whether or not the frame is
resizable.

The _properties_ argument specifies properties for the frame.

The _thread_ argument specifies the thread in which the frame will
run. See the Library Reference: Core Features manual for full details
about how threads are handled.

The _event-queue_ specifies an event-queue for the frame.

The arguments _foreground_ and _background_ specify a foreground color
for the frame. In addition, _text-style_ specifies a text style for
the frame, and _palette_ specifies a color palette for the frame.
||#


#||
define sealed inline method make
    (class == <simple-frame>, #rest initargs, #key, #all-keys)
 => (pane :: <concrete-simple-frame>)
  apply(make, <concrete-simple-frame>, initargs)
end method make;
||#

(defmethod make-simple-frame (&rest initargs &key &allow-other-keys)
  (apply #'make-instance '<concrete-simple-frame> initargs))


#||
define sealed method port (frame :: <basic-frame>) => (port :: false-or(<port>))
  port(frame-manager(frame))
end method port;
||#

(defmethod port ((frame <basic-frame>))
"
Returns PORT instance associated with FRAME.

Returns NIL if FRAME is not ATTACHED to a FRAME MANAGER.
"
  (port (frame-manager frame)))


#||
define sealed method display (frame :: <basic-frame>) => (display :: false-or(<display>))
  let framem = frame-manager(frame);
  framem & display(framem)
end method display;
||#

(defmethod display ((frame <basic-frame>))
"
Returns DISPLAY instance associated with FRAME.

Returns NIL if FRAME is not ATTACHED to a FRAME MANAGER, or is ATTACHED
to a FRAME MANAGER that is not associated with a DISPLAY.
"
  (let ((framem (frame-manager frame)))
    (and framem (display framem))))


#||
define sealed inline method frame-state
    (frame :: <basic-frame>) => (state :: <frame-state>)
  let state = logand(frame-flags(frame), %frame_state_mask);
  $frame-states[state]
end method frame-state;
||#

(defmethod frame-state ((frame <basic-frame>))
"
Returns one of the frame states :DETACHED, :UNMAPPED, :MAPPED,
or :DESTROYED
"
  (let ((state (logand (frame-flags frame) %frame_state_mask)))
    (aref +frame-states+ state)))


#||
define sealed method frame-state-setter
    (state :: <frame-state>, frame :: <basic-frame>)
 => (state :: <frame-state>)
  let old-state = frame-state(frame);
  unless (state == old-state)
    let new-state :: <integer>
      = select (state)
	  #"detached"  => %frame_detached;
	  #"unmapped"  => %frame_unmapped;
	  #"mapped"    => %frame_mapped;
	  #"destroyed" => %frame_destroyed;
	end;
    frame-flags(frame)
      := logior(logand(frame-flags(frame), lognot(%frame_state_mask)), new-state);
    note-frame-state-changed(frame, old-state, state)
  end;
  state
end method frame-state-setter;
||#

(defmethod (setf frame-state) (state (frame <basic-frame>))
"
Updates FRAME STATE to be _state_.

If this results in a state change (i.e., frame's existing state is
not the same as _state_), causes a NOTE-FRAME-STATE-CHANGED call.
"
  (check-type state <frame-state>)
  (let ((old-state (frame-state frame)))
    (unless (eql state old-state)
      (let ((new-state (ecase state
			 (:detached  %frame_detached)
			 (:unmapped  %frame_unmapped)
			 (:mapped    %frame_mapped)
			 (:destroyed %frame_destroyed))))
        (setf (frame-flags frame) (logior (logand (frame-flags frame)
						  (lognot %frame_state_mask))
					  new-state))
	(note-frame-state-changed frame old-state state)))
    state))


#||
define method note-frame-state-changed
    (frame :: <basic-frame>, old-state, new-state) => ()
  #f
end method note-frame-state-changed;
||#

(defmethod note-frame-state-changed ((frame <basic-frame>) old-state new-state)
  (declare (ignore old-state new-state))
  nil)


#||
define sealed inline method frame-mode
    (frame :: <basic-frame>) => (mode :: <frame-mode>)
  let mode = ash(logand(frame-flags(frame), %frame_mode_mask), -%frame_mode_shift);
  $frame-modes[mode]
end method frame-mode;
||#

(defmethod frame-mode ((frame <basic-frame>))
"
Returns one of the frame states :MODELESS, :MODAL, or :SYSTEM-MODAL
"
  (let ((mode (ash (logand (frame-flags frame) %frame_mode_mask) (- %frame_mode_shift))))
    (aref +frame-modes+ mode)))


#||
define sealed method frame-mode-setter
    (mode :: <frame-mode>, frame :: <basic-frame>)
 => (mode :: <frame-mode>)
  let new-mode :: <integer>
    = select (mode)
	#"modeless"     => %frame_modeless;
	#"modal"        => %frame_modal;
	#"system-modal" => %frame_system_modal;
      end;
  frame-flags(frame)
    := logior(logand(frame-flags(frame), lognot(%frame_mode_mask)), new-mode);
  mode
end method frame-mode-setter;
||#

(defmethod (setf frame-mode) (mode (frame <basic-frame>))
  (check-type mode <frame-mode>)
  (let ((new-mode (ecase mode
		    (:modeless     %frame_modeless)
		    (:modal        %frame_modal)
		    (:system-modal %frame_system_modal))))
    (setf (frame-flags frame)
	  (logior (logand (frame-flags frame) (lognot %frame_mode_mask)) new-mode))
    mode))

#||
define sealed inline method frame-enabled?
    (frame :: <basic-frame>) => (enabled? :: <boolean>)
  ~zero?(logand(frame-flags(frame), %frame_enabled))
end method frame-enabled?;
||#

(defmethod frame-enabled? ((frame <basic-frame>))
  (not (zerop (logand (frame-flags frame) %frame_enabled))))


#||
define sealed method frame-enabled?-setter
    (enabled? :: <boolean>, frame :: <basic-frame>)
 => (enabled? :: <boolean>)
  when (frame-enabled?(frame) ~= enabled?)
    frame-flags(frame)
      := logior(logand(frame-flags(frame), lognot(%frame_enabled)),
		if (enabled?) %frame_enabled else 0 end);
    if (enabled?)
      note-frame-enabled(frame-manager(frame), frame)
    else
      note-frame-disabled(frame-manager(frame), frame)
    end
  end;
  enabled?
end method frame-enabled?-setter;
||#

(defmethod (setf frame-enabled?) (enabled? (frame <basic-frame>))
"
Updates frame enabled then calls NOTE-FRAME-ENABLED or
NOTE-FRAME-DISABLED accordingly.
"
  (when (not (eql (frame-enabled? frame) enabled?))
    (setf (frame-flags frame)
	  (logior (logand (frame-flags frame) (lognot %frame_enabled))
		  (if enabled? %frame_enabled 0)))
    (if enabled?
	(note-frame-enabled (frame-manager frame) frame)
	(note-frame-disabled (frame-manager frame) frame)))
  enabled?)


#||
define method note-frame-enabled
    (framem :: <frame-manager>, frame :: <frame>) => ()
  #f
end method note-frame-enabled;
||#

(defmethod note-frame-enabled ((framem <frame-manager>) (frame <frame>))
  nil)


#||
define method note-frame-disabled
    (framem :: <frame-manager>, frame :: <frame>) => ()
  #f
end method note-frame-disabled;
||#

(defmethod note-frame-disabled ((framem <frame-manager>) (frame <frame>))
  nil)


#||
define sealed inline method frame-iconified?
    (frame :: <basic-frame>) => (iconified? :: <boolean>)
  ~zero?(logand(frame-flags(frame), %frame_iconified))
end method frame-iconified?;
||#

(defmethod frame-iconified? ((frame <basic-frame>))
  (not (zerop (logand (frame-flags frame) %frame_iconified))))


#||
define sealed method frame-iconified?-setter
    (iconified? :: <boolean>, frame :: <basic-frame>)
 => (iconified? :: <boolean>)
  when (frame-iconified?(frame) ~= iconified?)
    frame-flags(frame)
      := logior(logand(frame-flags(frame), lognot(%frame_iconified)),
		if (iconified?) %frame_iconified else 0 end);
    if (iconified?)
      note-frame-state-changed(frame, #"deiconified", #"iconified");
      note-frame-iconified(frame-manager(frame), frame)
    else
      note-frame-state-changed(frame, #"iconified", #"deiconified");
      note-frame-deiconified(frame-manager(frame), frame)
    end
  end;
  iconified?
end method frame-iconified?-setter;
||#

(defmethod (setf frame-iconified?) (iconified? (frame <basic-frame>))
"
Change iconified state of _frame_.

If there is a change in state, invokes NOTE-FRAME-STATE-CHANGED followed
by one of NOTE-FRAME-ICONIFIED or NOTE-FRAME-DEICONIFIED appropriately.
"
  (when (not (eql (frame-iconified? frame) iconified?))
    (setf (frame-flags frame)
	  (logior (logand (frame-flags frame) (lognot %frame_iconified))
		  (if iconified? %frame_iconified 0)))
    (if iconified?
	(progn
	  (note-frame-state-changed frame :deiconified :iconified)
	  (note-frame-iconified (frame-manager frame) frame))
	;; else
	(progn
	  (note-frame-state-changed frame :iconified :deiconified)
	  (note-frame-deiconified (frame-manager frame) frame))))
  iconified?)


#||
define method note-frame-iconified
    (framem :: <frame-manager>, frame :: <frame>) => ()
  #f
end method note-frame-iconified;
||#

(defmethod note-frame-iconified ((framem <frame-manager>) (frame  <frame>))
  nil)


#||
define method note-frame-deiconified
    (framem :: <frame-manager>, frame :: <frame>) => ()
  #f
end method note-frame-deiconified;
||#

(defmethod note-frame-deiconified ((framem <frame-manager>) (frame  <frame>))
  nil)


#||
define sealed inline method frame-maximized?
    (frame :: <basic-frame>) => (maximized? :: <boolean>)
  ~zero?(logand(frame-flags(frame), %frame_maximized))
end method frame-maximized?;
||#

(defmethod frame-maximized? ((frame <basic-frame>))
  (not (zerop (logand (frame-flags frame) %frame_maximized))))


#||
define sealed method frame-maximized?-setter
    (maximized? :: <boolean>, frame :: <basic-frame>)
 => (maximized? :: <boolean>)
  when (frame-maximized?(frame) ~= maximized?)
    frame-flags(frame)
      := logior(logand(frame-flags(frame), lognot(%frame_maximized)),
		if (maximized?) %frame_maximized else 0 end);
    if (maximized?)
      note-frame-state-changed(frame, #"unmaximized", #"maximized");
      note-frame-maximized(frame-manager(frame), frame)
    else
      note-frame-state-changed(frame, #"maximized", #"unmaximized");
      note-frame-unmaximized(frame-manager(frame), frame)
    end
  end;
  maximized?
end method frame-maximized?-setter;
||#

(defmethod (setf frame-maximized?) (maximized? (frame <basic-frame>))
  ;; FIXME: These comparisons are going to fail unless maximized really is
  ;; T/NIL.
  (when (not (eql (frame-maximized? frame) maximized?))
    (setf (frame-flags frame)
	  (logior (logand (frame-flags frame) (lognot %frame_maximized))
		  (if maximized? %frame_maximized 0)))
    (if maximized?
	(progn
	  (note-frame-state-changed frame :unmaximized :maximized)
	  (note-frame-maximized (frame-manager frame) frame))
	(progn
	  (note-frame-state-changed frame :maximized :unmaximized)
	  (note-frame-unmaximized (frame-manager frame) frame))))
  maximized?)


#||
define method note-frame-maximized
    (framem :: <frame-manager>, frame :: <frame>) => ()
  #f
end method note-frame-maximized;
||#

(defmethod note-frame-maximized ((framem <frame-manager>) (frame <frame>))
  nil)


#||
define method note-frame-unmaximized
    (framem :: <frame-manager>, frame :: <frame>) => ()
  #f
end method note-frame-unmaximized ;
||#

(defmethod note-frame-unmaximized ((framem <frame-manager>) (frame <frame>))
  nil)


#||
define sealed inline method frame-fixed-width?
    (frame :: <basic-frame>) => (fixed-width? :: <boolean>)
  ~zero?(logand(frame-flags(frame), %frame_fixed_width))
end method frame-fixed-width?;
||#

(defmethod frame-fixed-width? ((frame <basic-frame>))
  (not (zerop (logand (frame-flags frame) %frame_fixed_width))))


#||
define sealed inline method frame-fixed-height?
    (frame :: <basic-frame>) => (fixed-height? :: <boolean>)
  ~zero?(logand(frame-flags(frame), %frame_fixed_height))
end method frame-fixed-height?;
||#

(defmethod frame-fixed-height? ((frame <basic-frame>))
  (not (zerop (logand (frame-flags frame) %frame_fixed_height))))


#||
define function frame-resizable?
    (frame :: <frame>) => (resizable? :: <boolean>)
  ~frame-fixed-width?(frame) | ~frame-fixed-height?(frame)
end function frame-resizable?;
||#

(defmethod frame-resizable? ((frame <frame>))
  (or (not (frame-fixed-width? frame)) (not (frame-fixed-height? frame))))


#||
define sealed inline method frame-alt-key-is-meta?
    (frame :: <basic-frame>) => (alt-is-meta? :: <boolean>)
  ~zero?(logand(frame-flags(frame), %frame_alt_is_meta))
end method frame-alt-key-is-meta?;
||#

(defmethod frame-alt-key-is-meta? ((frame <basic-frame>))
  (not (zerop (logand (frame-flags frame) %frame_alt_is_meta))))


#||
define sealed method frame-alt-key-is-meta?-setter
    (alt-is-meta? :: <boolean>, frame :: <basic-frame>) => (alt-is-meta? :: <boolean>)
  frame-flags(frame)
    := logior(logand(frame-flags(frame), lognot(%frame_alt_is_meta)),
	      if (alt-is-meta?) %frame_alt_is_meta else 0 end);
  alt-is-meta?
end method frame-alt-key-is-meta?-setter;
||#

(defmethod (setf frame-alt-key-is-meta?) (alt-is-meta? (frame <basic-frame>))
  (check-type alt-is-meta? boolean)
  (setf (frame-flags frame)
	(logior (logand (frame-flags frame) (lognot %frame_alt_is_meta))
		(if alt-is-meta? %frame_alt_is_meta 0)))
  alt-is-meta?)


#||
define sealed inline method frame-allow-control-alt?
    (frame :: <basic-frame>) => (alt-is-meta? :: <boolean>)
  ~zero?(logand(frame-flags(frame), %frame_alt_is_meta))
end method frame-allow-control-alt?;
||#

(defmethod frame-allow-control-alt? ((frame <basic-frame>))
  (not (zerop (logand (frame-flags frame) %frame_allow_control_alt))))


#||
define sealed method frame-allow-control-alt?-setter
    (alt-is-meta? :: <boolean>, frame :: <basic-frame>) => (alt-is-meta? :: <boolean>)
  frame-flags(frame)
    := logior(logand(frame-flags(frame), lognot(%frame_alt_is_meta)),
	      if (alt-is-meta?) %frame_alt_is_meta else 0 end);
  alt-is-meta?
end method frame-allow-control-alt?-setter;
||#

(defmethod (setf frame-allow-control-alt?) (alt-is-meta? (frame <basic-frame>))
  (check-type alt-is-meta? boolean)
  (setf (frame-flags frame)
	(logior (logand (frame-flags frame) (lognot %frame_alt_is_meta))
		(if alt-is-meta? %frame_alt_is_meta 0)))
  alt-is-meta?)


#||
define sealed inline method frame-save-under?
    (frame :: <basic-frame>) => (save-under? :: <boolean>)
  ~zero?(logand(frame-flags(frame), %frame_save_under))
end method frame-save-under?;
||#

(defmethod frame-save-under? ((frame <basic-frame>))
  (not (zerop (logand (frame-flags frame) %frame_save_under))))


#||
define sealed inline method frame-minimize-box?
    (frame :: <basic-frame>) => (minimize-box? :: <boolean>)
  ~zero?(logand(frame-flags(frame), %frame_minimize_box))
end method frame-minimize-box?;
||#

(defmethod frame-minimize-box? ((frame <basic-frame>))
  (not (zerop (logand (frame-flags frame) %frame_minimize_box))))


#||
define sealed inline method frame-maximize-box?
    (frame :: <basic-frame>) => (maximize-box? :: <boolean>)
  ~zero?(logand(frame-flags(frame), %frame_maximize_box))
end method frame-maximize-box?;
||#

(defmethod frame-maximize-box? ((frame <basic-frame>))
  (not (zerop (logand (frame-flags frame) %frame_maximize_box))))


#||
define sealed inline method frame-always-on-top?
    (frame :: <basic-frame>) => (always-on-top? :: <boolean>)
  ~zero?(logand(frame-flags(frame), %frame_on_top))
end method frame-always-on-top?;
||#

(defmethod frame-always-on-top? ((frame <basic-frame>))
  (not (zerop (logand (frame-flags frame) %frame_on_top))))


#||
define sealed inline method frame-centered?
    (frame :: <basic-frame>) => (centered? :: <boolean>)
  ~zero?(logand(frame-flags(frame), %frame_centered))
end method frame-centered?;
||#

(defmethod frame-centered? ((frame <basic-frame>))
  (not (zerop (logand (frame-flags frame) %frame_centered))))


#||
define sealed inline method frame-keyboard-interrupt?
    (frame :: <basic-frame>) => (keyboard-interrupt? :: <boolean>)
  ~zero?(logand(frame-flags(frame), %keyboard_interrupt))
end method frame-keyboard-interrupt?;
||#

(defmethod frame-keyboard-interrupt? ((frame <basic-frame>))
  (not (zerop (logand (frame-flags frame) %keyboard_interrupt))))


#||
// Go up the 'frame-owner' path until we find a frame with a thread
define method frame-controlling-frame
    (frame :: <basic-frame>) => (frame :: false-or(<frame>))
  for (f = frame then frame-owner(f),
       until: ~f | frame-thread(f))
  finally f
  end
end method frame-controlling-frame;
||#

;; Go up the 'frame-owner' path until we find a frame with a thread
(defmethod frame-controlling-frame ((frame <basic-frame>))
  (loop for f = frame then (frame-owner f)
     until (or (not f) (frame-thread f))
     finally (return-from frame-controlling-frame f)))


#||
define method find-color
    (name, frame :: <basic-frame>, #key error? = #t) => (color :: <color>)
  find-color(name, frame-palette(frame), error?: error?)
end method find-color;
||#

(defmethod find-color (name (frame <basic-frame>) &key (error? t))
"
Returns <COLOR>
"
  (find-color name (frame-palette frame) :error? error?))


#||
define method frame-palette
    (frame :: <basic-frame>) => (palette :: false-or(<palette>))
  let framem = frame-manager(frame);
  frame.%palette
  | if (framem)
      frame-manager-palette(framem)
    else
      port(frame) & port-default-palette(port(frame))
    end
end method frame-palette;
||#

(defmethod frame-palette ((frame <basic-frame>))
  (let ((framem (frame-manager frame)))
    (or (%palette frame)
	(if framem
	    (frame-manager-palette framem)
            (and (port frame) (port-default-palette (port frame)))))))


#||
define method frame-palette-setter
    (palette :: false-or(<palette>), frame :: <basic-frame>)
 => (palette :: false-or(<palette>))
  frame.%palette := palette
end method frame-palette-setter;
||#

(defmethod (setf frame-palette) ((palette null) (frame <basic-frame>))
  (setf (%palette frame) palette))

(defmethod (setf frame-palette) ((palette <palette>) (frame <basic-frame>))
  (setf (%palette frame) palette))


#||
define sealed method default-foreground
    (frame :: <basic-frame>) => (fg :: false-or(<ink>))
  let style = frame.%style-descriptor;
  style & default-foreground(style)
end method default-foreground;
||#

(defmethod default-foreground ((frame <basic-frame>))
  (let ((style (%style-descriptor frame)))
    (and style (default-foreground style))))


#||
define sealed method default-foreground-setter
    (fg :: false-or(<ink>), frame :: <basic-frame>) => (fg :: false-or(<ink>))
  let style = frame.%style-descriptor
	      | begin
		  let style = make(<style-descriptor>);
		  frame.%style-descriptor := style;
		  style
		end;
  default-foreground(style) := fg;
  fg
end method default-foreground-setter;
||#

(defmethod (setf default-foreground) ((fg null) (frame <basic-frame>))
"
Returns NIL or instance of <INK>
"
  (let ((style (or (%style-descriptor frame)
		   (let ((style (make-instance '<style-descriptor>)))
                     (setf (%style-descriptor frame) style)
		     style))))
    (setf (default-foreground style) fg)
    fg))

(defmethod (setf default-foreground) ((fg <ink>) (frame <basic-frame>))
"
Returns NIL or instance of <INK>
"
  (let ((style (or (%style-descriptor frame)
		   (let ((style (make-instance '<style-descriptor>)))
                     (setf (%style-descriptor frame) style)
		     style))))
    (setf (default-foreground style) fg)
    fg))


#||
define sealed method default-background
    (frame :: <basic-frame>) => (fg :: false-or(<ink>))
  let style = frame.%style-descriptor;
  style & default-background(style)
end method default-background;
||#

(defmethod default-background ((frame <basic-frame>))
"
Returns NIL or instance of <INK>
"
  (let ((style (%style-descriptor frame)))
    (and style (default-background style))))


#||
define sealed method default-background-setter
    (bg :: false-or(<ink>), frame :: <basic-frame>) => (bg :: false-or(<ink>))
  let style = frame.%style-descriptor
	      | begin
		  let style = make(<style-descriptor>);
		  frame.%style-descriptor := style;
		  style
		end;
  default-background(style) := bg;
  bg
end method default-background-setter;
||#

(defmethod (setf default-background) ((bg null) (frame <basic-frame>))
  (let ((style (or (%style-descriptor frame)
		   (let ((style (make-instance '<style-descriptor>)))
                     (setf (%style-descriptor frame) style)
		     style))))
    (setf (default-background style) bg)
    bg))

(defmethod (setf default-background) ((bg <ink>) (frame <basic-frame>))
  (let ((style (or (%style-descriptor frame)
		   (let ((style (make-instance '<style-descriptor>)))
                     (setf (%style-descriptor frame) style)
		     style))))
    (setf (default-background style) bg)
    bg))


#||
define sealed method default-text-style
    (frame :: <basic-frame>) => (ts :: false-or(<text-style>))
  let style = frame.%style-descriptor;
  style & default-text-style(style)
end method default-text-style;
||#

(defmethod default-text-style ((frame <basic-frame>))
"
Returns NIL or instance of <TEXT-STYLE>
"
  (let ((style (%style-descriptor frame)))
    (and style (default-text-style style))))


#||
define sealed method default-text-style-setter
    (ts :: false-or(<text-style>), frame :: <basic-frame>) => (ts :: false-or(<text-style>))
  let style = frame.%style-descriptor
	      | begin
		  let style = make(<style-descriptor>);
		  frame.%style-descriptor := style;
		  style
		end;
  default-text-style(style) := ts;
  ts
end method default-text-style-setter;
||#

(defmethod (setf default-text-style) ((ts null) (frame <basic-frame>))
  (let ((style (or (%style-descriptor frame)
		   (let ((style (make-instance '<style-descriptor>)))
                     (setf (%style-descriptor frame) style)
		     style))))
    (setf (default-text-style style) ts)
    ts))

(defmethod (setf default-text-style) ((ts <text-style>) (frame <basic-frame>))
  (let ((style (or (%style-descriptor frame)
		   (let ((style (make-instance '<style-descriptor>)))
                     (setf (%style-descriptor frame) style)
		     style))))
    (setf (default-text-style style) ts)
    ts))



#||
/// Accelerators

// The idea here is to allow the accelerators to be set explicitly,
// or computed from the all the gadgets in the frame
define method frame-accelerators
    (frame :: <basic-frame>) => (accelerators :: <sequence>)
  frame.%accelerators
  | begin
      let top-sheet = top-level-sheet(frame);
      let framem    = frame-manager(frame);
      if (top-sheet)
	local method do-accelerators (function :: <function>)
		do-sheet-tree(method (sheet)
				when (instance?(sheet, <accelerator-mixin>))
				  let accelerator
				    = defaulted-gadget-accelerator(framem, sheet);
				  when (accelerator)
				    function(sheet, accelerator)
				  end
				end
			      end method, top-sheet)
	      end method;
	let n :: <integer> = 0;
	do-accelerators(method (gadget, accelerator)
			  ignore(gadget, accelerator);
			  inc!(n)
			end method);
	let accelerators :: <simple-object-vector> = make(<vector>, size: n);
	let i :: <integer> = 0;
	do-accelerators(method (gadget, accelerator)
			  accelerators[i] := vector(gadget, accelerator);
			  inc!(i)
			end method);
	frame.%accelerators := accelerators
      else
	#[]
      end
    end
end method frame-accelerators;
||#

(defmethod frame-accelerators ((frame <basic-frame>))
"
The idea here is to allow the accelerators to be set explicitly,
or computed from all the gadgets in the frame.

Returns array where each element is #[<GADGET>, <ACCELERATOR>]
"
  (or (%accelerators frame)
      (let ((top-sheet (top-level-sheet frame))
	    (framem    (frame-manager frame)))
	(if top-sheet
	    (labels ((do-accelerators (function)
                         (declare (type function function))
			 (do-sheet-tree #'(lambda (sheet)
					    (when (typep sheet '<accelerator-mixin>)
					      (let ((accelerator (defaulted-gadget-accelerator framem sheet)))
						(when accelerator
						  (funcall function sheet accelerator)))))
					top-sheet)))
	      (let ((n 0))
		(do-accelerators #'(lambda (gadget accelerator)
				     (declare (ignore gadget accelerator))
				     (incf n)))
		(let ((accelerators (make-array n))
		      (i 0))
		  (do-accelerators #'(lambda (gadget accelerator)
				       (setf (aref accelerators i) (vector gadget accelerator))
				       (incf i)))
                  (setf (%accelerators frame) accelerators))))
	  #()))))


#||
define method frame-accelerators-setter
    (accelerators :: false-or(<sequence>), frame :: <basic-frame>)
 => (accelerators :: false-or(<sequence>))
  note-accelerators-changed(frame-manager(frame), frame);
  frame.%accelerators := accelerators
end method frame-accelerators-setter;
||#

(defmethod (setf frame-accelerators) ((accelerators null) (frame <basic-frame>))
"
Calls NOTE-ACCELERATORS-CHANGED
"
  ;;; FIXME: SHOULD THE NOTE-ACCELERATORS-CHANGED METHOD BE CALLED AFTER
  ;;; THE ACCELERATORS HAVE BEEN MODIFIED ON THE FRAME?
  (note-accelerators-changed (frame-manager frame) frame)
  (setf (%accelerators frame) accelerators))

(defmethod (setf frame-accelerators) ((accelerators sequence) (frame <basic-frame>))
"
Calls NOTE-ACCELERATORS-CHANGED
"
  ;;; FIXME: SHOULD THE NOTE-ACCELERATORS-CHANGED METHOD BE CALLED AFTER
  ;;; THE ACCELERATORS HAVE BEEN MODIFIED ON THE FRAME?
  (declare (type (or null sequence) accelerators))
  (note-accelerators-changed (frame-manager frame) frame)
  (setf (%accelerators frame) accelerators))


#||
define method note-accelerators-changed
    (framem :: <frame-manager>, frame :: <basic-frame>) => ()
  #f
end method note-accelerators-changed;
||#

(defmethod note-accelerators-changed ((framem <frame-manager>) (frame  <basic-frame>))
  nil)



#||
/// Frame geometry

// If the frame has not been layed out yet, return the default geometry,
// otherwise return the live value from the top level sheet
define method frame-position
    (frame :: <basic-frame>)
 => (x :: false-or(<integer>), y :: false-or(<integer>))
  let top-sheet = top-level-sheet(frame);
  let container-region = frame.%container-region;
  case
    container-region =>
      box-position(container-region);
    top-sheet & sheet-layed-out?(top-sheet) =>
      sheet-position(top-sheet);
    otherwise =>
      let geometry = frame-geometry(frame);
      values(geometry[0], geometry[1]);
  end
end method frame-position;
||#

(defmethod frame-position ((frame <basic-frame>))
"
If the frame has not been layed out yet, return the default geometry,
otherwise return the live value from the top level sheet.

Returns x and y as 'false-or(<integer>)' values.
"
  (let ((top-sheet (top-level-sheet frame))
	(container-region (%container-region frame)))
    ;; container-region deals with embedded frames
    (cond (container-region
	   (box-position container-region))
	  ((and top-sheet (sheet-layed-out? top-sheet))
	   (sheet-position top-sheet))
	  (t
	   (let ((geometry (frame-geometry frame)))
	     (values (aref geometry 0) (aref geometry 1)))))))


#||
// Always update the geometry hint.  If the frame has a top level sheet,
// update it as well.
define method set-frame-position
    (frame :: <basic-frame>, x :: <integer>, y :: <integer>,
     #key constrain? = #t) => ()
  let (x, y)
    = if (constrain?) constrained-frame-position(frame, x, y)
      else values(x, y) end;
  let top-sheet = top-level-sheet(frame);
  let geometry  = frame-geometry(frame);
  geometry[0] := x;
  geometry[1] := y;
  when (top-sheet & sheet-layed-out?(top-sheet))
    set-sheet-position(top-sheet, x, y)
  end
end method set-frame-position;
||#

;; Always update the geometry hint. If the frame has a top level sheet,
;; update it as well.
(defmethod set-frame-position ((frame <basic-frame>) (x integer) (y integer)
			       &key (constrain? t))
  (multiple-value-bind (x y)
      (if constrain?
	  (constrained-frame-position frame x y)
	  (values x y))
    (let ((top-sheet (top-level-sheet frame))
	  (geometry  (frame-geometry frame)))
      (setf (aref geometry 0) x)
      (setf (aref geometry 1) y)
      (when (and top-sheet (sheet-layed-out? top-sheet))
	(set-sheet-position top-sheet x y)))))


#||
define method constrained-frame-position
    (frame :: <frame>, x :: <integer>, y :: <integer>)
 => (x :: <integer>, y :: <integer>)
  let _display = display(frame);
  if (_display)
    let (width, height)   = frame-size(frame);
    let (dright, dbottom) = box-size(_display);
    let width  :: <integer> = width  | $default-sheet-size;
    let height :: <integer> = height | $default-sheet-size;
    let left   :: <integer> = x;
    let top    :: <integer> = y;
    let right  :: <integer> = left + width;
    let bottom :: <integer> = top  + height;
    when (right > dright)
      left := dright - width
    end;
    when (bottom > dbottom)
      top := dbottom - height
    end;
    values(max(0, left), max(0, top))
  else
    values(x, y)
  end
end method constrained-frame-position;
||#

(defmethod constrained-frame-position ((frame <frame>) (x integer) (y integer))
  (let ((_display (display frame)))
    (if _display
	(multiple-value-bind (width height)
	    (frame-size frame)
	  (multiple-value-bind (dright dbottom)
	      (box-size _display)
	    (let* ((width  (or width +default-sheet-size+))
		   (height (or height +default-sheet-size+))
		   (left   x)
		   (top    y)
		   (right  (+ left width))
		   (bottom (+ top height)))
	      (when (> right dright)
		(setf left (- dright width)))
	      (when (> bottom dbottom)
		(setf top (- dbottom height)))
	      (values (max 0 left) (max 0 top)))))
	;; else
	(values x y))))


#||
define method frame-size 
    (frame :: <basic-frame>)
 => (width :: false-or(<integer>), height :: false-or(<integer>))
  let top-sheet = top-level-sheet(frame);
  let container-region = frame.%container-region;
  case
    container-region =>
      box-size(container-region);
    top-sheet & sheet-layed-out?(top-sheet) =>
      sheet-size(top-sheet);
    otherwise =>
      let geometry = frame-geometry(frame);
      values(geometry[2], geometry[3]);
  end
end method frame-size;
||#

(defmethod frame-size ((frame <basic-frame>))
  (let ((top-sheet (top-level-sheet frame))
	(container-region (%container-region frame)))
    ;; container-region deals with embedded frames
    (cond (container-region
	   (box-size container-region))
	  ((and top-sheet (sheet-layed-out? top-sheet))
	   (sheet-size top-sheet))
	  (t
	   (let ((geometry (frame-geometry frame)))
	     (values (aref geometry 2) (aref geometry 3)))))))


#||
define method set-frame-size
    (frame :: <basic-frame>, width :: <integer>, height :: <integer>) => ()
  let top-sheet = top-level-sheet(frame);
  let geometry  = frame-geometry(frame);
  geometry[2] := width;
  geometry[3] := height;
  when (top-sheet & sheet-layed-out?(top-sheet))
    set-sheet-size(top-sheet, width, height)
  end
end method set-frame-size;
||#

(defmethod set-frame-size ((frame <basic-frame>) (width integer) (height integer))
  (let ((top-sheet (top-level-sheet frame))
	(geometry  (frame-geometry frame)))
    (setf (aref geometry 2) width)
    (setf (aref geometry 3) height)
    (when (and top-sheet (sheet-layed-out? top-sheet))
      (set-sheet-size top-sheet width height))))



#||
/// Input focus

define method frame-input-focus
    (frame :: <simple-frame>) => (sheet :: false-or(<sheet>))
  frame.%input-focus
end method frame-input-focus;
||#

(defmethod frame-input-focus ((frame <simple-frame>))
  (%input-focus frame))


#||
define method frame-input-focus-setter
    (new :: false-or(<sheet>), frame :: <basic-frame>)
 => (new :: false-or(<sheet>))
  let old = frame.%input-focus;
  when (new ~== old)
    let _port = port(frame);
    frame.%input-focus := new;
    if (port-input-focus(_port) == old)
      port-input-focus(_port) := new
    end;
    when (frame-mapped?(frame))
      distribute-event(_port,
		       make(<frame-input-focus-changed-event>,
			    frame: frame,
			    old-focus: old,
			    new-focus: new))
    end
  end;
  new
end method frame-input-focus-setter;
||#

(defmethod (setf frame-input-focus) ((new null) (frame <basic-frame>))
  (%frame-input-focus-setter new frame))

(defmethod (setf frame-input-focus) ((new <sheet>) (frame <basic-frame>))
  (%frame-input-focus-setter new frame))

(defun %frame-input-focus-setter (new frame)
  (let ((old (%input-focus frame)))
    (when (not (eql new old))
      (let ((_port (port frame)))
        (setf (%input-focus frame) new)
	(if (eql (port-input-focus _port) old)
            (setf (port-input-focus _port) new))
	(when (frame-mapped? frame)
	  (distribute-event _port
			    (make-instance '<frame-input-focus-changed-event>
					   :frame frame
					   :old-focus old
					   :new-focus new)))))
    new))



#||
/// Pointer cursors

define method frame-cursor-override-setter
    (new :: false-or(<cursor>), frame :: <basic-frame>)
 => (new :: false-or(<cursor>))
  let old = frame-cursor-override(frame);
  when (new ~== old)
    frame.%cursor-override := new;
    let _port = port(frame);
    if (_port)
      let pointer = port-pointer(_port);
      update-pointer-cursor(pointer, frame: frame)
    end
  end;
  new
end method frame-cursor-override-setter;
||#

(defmethod (setf frame-cursor-override) (new (frame <basic-frame>))
  (check-type new (or null <cursor>))
  (let ((old (frame-cursor-override frame)))
    (when (not (eql new old))
      (%cursor-override-setter new frame)
      (let ((_port (port frame)))
	(when _port
	  (let ((pointer (port-pointer _port)))
	    (update-pointer-cursor pointer :frame frame)))))
    new))


#||
define macro with-background-cursor
  { with-background-cursor (?frame:expression, ?cursor:expression) ?:body end }
    => { begin
	   let body = method () ?body end;
	   do-with-background-cursor(?frame, ?cursor, body)
         end }
  { with-background-cursor (?frame:expression) ?:body end }
    => { begin
	   let body = method () ?body end;
	   do-with-background-cursor(?frame, #"starting", body)
         end }
end macro with-background-cursor;
||#

(defmacro with-background-cursor ((frame &optional cursor) &body body)
  (let ((_cursor (gensym "CURSOR-"))
	(_frame  (gensym "FRAME-")))
    `(let ((body #'(lambda () ,@body))
	   (,_cursor ,cursor)
	   (,_frame  ,frame))
       (if ,_cursor
	   (do-with-background-cursor ,_frame ,_cursor body)
	   (do-with-background-cursor ,_frame :starting body)))))


#||
define method do-with-background-cursor
    (sheet :: <sheet>, cursor, continuation :: <function>) => (#rest values)
  let frame = sheet-frame(sheet);
  if (frame)
    do-with-background-cursor(frame, cursor, continuation)
  else
    continuation()		// sheet is not grafted
  end
end method do-with-background-cursor;
||#

(defmethod do-with-background-cursor ((sheet <sheet>) cursor (continuation function))
  (let ((frame (sheet-frame sheet)))
    (if frame
	(do-with-background-cursor frame cursor continuation)
	(funcall continuation))))   ;; sheet is not grafted


#||
define method do-with-background-cursor
    (frame :: <frame>, cursor, continuation :: <function>) => (#rest values)
  block ()
    call-in-frame(frame, 
		  note-background-operation-started, frame, cursor: cursor);
    continuation()
  cleanup
    call-in-frame(frame, note-background-operation-finished, frame)
  end
end method do-with-background-cursor;
||#

(defmethod do-with-background-cursor ((frame <frame>) cursor (continuation function))
  (unwind-protect
       (progn
	 (call-in-frame frame #'note-background-operation-started frame :cursor cursor)
	 (funcall continuation))
    ;; cleanup
    (call-in-frame frame #'note-background-operation-finished frame)))


#||
define method note-background-operation-started
    (frame :: <frame>, #key cursor = #"starting") => ()
  inc!(frame-background-operations(frame));
  frame-cursor-override(frame) := cursor
end method note-background-operation-started;
||#

(defmethod note-background-operation-started ((frame <frame>) &key (cursor :starting))
  (incf (frame-background-operations frame))
  (setf (frame-cursor-override frame) cursor))


#||
define method note-background-operation-finished
    (frame :: <frame>) => ()
  let operations = dec!(frame-background-operations(frame));
  when (zero?(operations))
    frame-cursor-override(frame) := #f
  end
end method note-background-operation-finished;
||#

(defmethod note-background-operation-finished ((frame <frame>))
  (let ((operations (decf (frame-background-operations frame))))
    (when (zerop operations)
      (setf (frame-cursor-override frame) nil))))



#||
/// Command table handling

define method frame-command-table
    (frame :: <simple-frame>) => (command-table :: false-or(<command-table>))
  frame.%command-table
end method frame-command-table;
||#

(defmethod frame-command-table ((frame <simple-frame>))
  (%command-table frame))


#||
define method frame-command-table-setter
    (command-table :: false-or(<command-table>), frame :: <simple-frame>)
 => (command-table :: false-or(<command-table>))
  //---*** How does this interact with command loops?
  //---*** What if there was an explicit <menu> given as the menu bar?
  frame.%command-table := command-table;
  let framem = frame-manager(frame);
  when (framem)
    note-command-table-changed(framem, frame)
  end;
  command-table
end method frame-command-table-setter;
||#

;; TODO: I think we should error if there's an explicit <menu> already
;; associated with the frame. Either that or supress the creation of
;; a menu for the command-table. I guess a 3rd option is just to add
;; the command table to the existing menu. Hrm.
;; The NOTE-COMMAND-TABLE-CHANGED method just replaces any children
;; of a frame's menu-bar with newly-generated menus for the command
;; table.

(defmethod (setf frame-command-table) ((command-table null) (frame <simple-frame>))
  ;;---*** How does this interact with command loops?
  ;;---*** What if there was an explicit <menu> given as the menu bar?
  (setf (%command-table frame) command-table)
  (let ((framem (frame-manager frame)))
    (when framem
      (note-command-table-changed framem frame)))
  command-table)

(defmethod (setf frame-command-table) ((command-table <command-table>) (frame <simple-frame>))
  ;;---*** How does this interact with command loops?
  ;;---*** What if there was an explicit <menu> given as the menu bar?
  (setf (%command-table frame) command-table)
  (let ((framem (frame-manager frame)))
    (when framem
      (note-command-table-changed framem frame)))
  command-table)


#||
define method note-command-table-changed
    (framem :: <frame-manager>, frame :: <simple-frame>) => ()
  let menu-bar = frame-menu-bar(frame);
  when (menu-bar)
    let command-table = frame-command-table(frame);
    let menus = make-menus-from-command-table(command-table, frame, framem);
    let old-menus = sheet-children(menu-bar);
    sheet-children(menu-bar) := menus;
    do(destroy-sheet, old-menus)
  end
end method note-command-table-changed;
||#

(defmethod note-command-table-changed ((framem <frame-manager>) (frame <simple-frame>))
  (let ((menu-bar (frame-menu-bar frame)))
    (when menu-bar
      (let* ((command-table (frame-command-table frame))
	     (menus         (make-menus-from-command-table command-table frame framem))
	     (old-menus     (sheet-children menu-bar)))
        (setf (sheet-children menu-bar) menus)
	;; FIXME: DO-SEQUENCE? DO-COLLECTION?
	(map nil #'destroy-sheet old-menus)))))


#||
define method handle-id-activation
    (frame :: <frame>, id) => (handled? :: <boolean>)
  let command-table = frame-command-table(frame);
  when (command-table)
    block (return)
      do-command-table-menu-items
	(method (decorator, command-table)
	   ignore(command-table);
	   when (decorator-resource-id(decorator) = id)
	     let object = decorator-object(decorator);
	     let type   = decorator-type(decorator);
	     select (type)
	       <function>, <command> =>
		 distribute-command-event(frame, object);
		 return(#t);
	       otherwise =>
		 #f;
	     end
	   end
	 end,
	 command-table,
	 do-inherited?: #t)
    end
  end
end method handle-id-activation;
||#

(defmethod handle-id-activation ((frame <frame>) id)
"
Invoke the command associated with the id provided.
"
  (let ((command-table (frame-command-table frame)))
    (when command-table
      (block ret
	(do-command-table-menu-items
	    #'(lambda (decorator command-table)
		(declare (ignore command-table))
		(when (eql (decorator-resource-id decorator) id)
		  (let ((object (decorator-object decorator))
			(type   (decorator-type decorator)))
		    (typecase type
		      ((or function <command>)
		       (distribute-command-event frame object)
		       (return-from ret t))
		      (t
		       nil)))))
	  command-table
	  :do-inherited? t)))))



#||

/// Menu bar handling

// Back-ends specialize this to install sensible mnemonics for
// the particular platform (e.g. 'x' for 'Exit' in MS Windows)
define open generic install-frame-mnemonics (frame :: <abstract-frame>) => ();
||#

(defgeneric install-frame-mnemonics (frame)
  (:documentation
"
Back-ends specialize this to install sensible mnemonics for the
particular platform (e.g. 'x' for 'Exit' in MS Windows)
"))


#||
define method frame-menu-bar
    (frame :: <simple-frame>) => (menu-bar :: false-or(<menu-bar>))
  frame.%menu-bar
end method frame-menu-bar;
||#

(defmethod frame-menu-bar ((frame <simple-frame>))
  (%menu-bar frame))


#||
define method frame-menu-bar-setter
    (menu-bar :: false-or(<menu-bar>), frame :: <simple-frame>)
 => (menu-bar :: false-or(<menu-bar>))
  unless (menu-bar = frame-menu-bar(frame))
    frame.%menu-bar := menu-bar;
    let framem = frame-manager(frame);
    framem & update-frame-wrapper(framem, frame)
  end;
  menu-bar
end method frame-menu-bar-setter;
||#

(defmethod (setf frame-menu-bar) ((menu-bar null) (frame <simple-frame>))
  (unless (eql menu-bar (frame-menu-bar frame))
    (setf (%menu-bar frame) menu-bar)
    (let ((framem (frame-manager frame)))
      (and framem (update-frame-wrapper framem frame))))
  menu-bar)

(defmethod (setf frame-menu-bar) ((menu-bar <menu-bar>) (frame <simple-frame>))
  (unless (eql menu-bar (frame-menu-bar frame))
    (setf (%menu-bar frame) menu-bar)
    (let ((framem (frame-manager frame)))
      (and framem (update-frame-wrapper framem frame))))
  menu-bar)


#||
define method frame-tool-bar
    (frame :: <simple-frame>) => (tool-bar :: false-or(<tool-bar>))
  frame.%tool-bar
end method frame-tool-bar;
||#

(defmethod frame-tool-bar ((frame <simple-frame>))
  (%tool-bar frame))


#||
define method frame-tool-bar-setter
    (tool-bar :: false-or(<tool-bar>), frame :: <simple-frame>)
 => (tool-bar :: false-or(<tool-bar>))
  unless (tool-bar = frame-tool-bar(frame))
    frame.%tool-bar := tool-bar;
    let framem = frame-manager(frame);
    framem & update-frame-wrapper(framem, frame)
  end;
  tool-bar
end method frame-tool-bar-setter;
||#

(defmethod (setf frame-tool-bar) ((tool-bar null) (frame <simple-frame>))
  (unless (eql tool-bar (frame-tool-bar frame))
    (setf (%tool-bar frame) tool-bar)
    (let ((framem (frame-manager frame)))
      (and framem (update-frame-wrapper framem frame))))
  tool-bar)

(defmethod (setf frame-tool-bar) ((tool-bar <tool-bar>) (frame <simple-frame>))
  (unless (eql tool-bar (frame-tool-bar frame))
    (setf (%tool-bar frame) tool-bar)
    (let ((framem (frame-manager frame)))
      (and framem (update-frame-wrapper framem frame))))
  tool-bar)


#||
define method frame-status-bar
    (frame :: <simple-frame>) => (status-bar :: false-or(<status-bar>))
  frame.%status-bar
end method frame-status-bar;
||#

(defmethod frame-status-bar ((frame <simple-frame>))
  (%status-bar frame))


#||
define method frame-status-bar-setter
    (status-bar :: false-or(<status-bar>), frame :: <simple-frame>)
 => (status-bar :: false-or(<status-bar>))
  unless (status-bar = frame-status-bar(frame))
    frame.%status-bar := status-bar;
    let framem = frame-manager(frame);
    update-frame-wrapper(framem, frame)
  end;
  status-bar
end method frame-status-bar-setter;
||#

(defmethod (setf frame-status-bar) ((status-bar null) (frame <simple-frame>))
  (unless (eql status-bar (frame-status-bar frame))
    (setf (%status-bar frame) status-bar)
    (let ((framem (frame-manager frame)))
      (update-frame-wrapper framem frame)))
  status-bar)

(defmethod (setf frame-status-bar) ((status-bar <status-bar>) (frame <simple-frame>))
  (unless (eql status-bar (frame-status-bar frame))
    (setf (%status-bar frame) status-bar)
    (let ((framem (frame-manager frame)))
      (update-frame-wrapper framem frame)))
  status-bar)


#||
// A little more support for OLE, which has a simplistic notion of status bars
define method frame-status-message
    (frame :: <simple-frame>) => (message :: false-or(<string>))
  let status-bar = frame-status-bar(frame);
  when (status-bar)
    gadget-label(status-bar)
  end
end method frame-status-message;
||#

(defmethod frame-status-message ((frame <simple-frame>))
"
A little more support for OLE, which has a simplistic notion of status bars.
"
  (let ((status-bar (frame-status-bar frame)))
    (when status-bar
      (gadget-label status-bar))))


#||
define method frame-status-message-setter
    (message :: false-or(<string>), frame :: <simple-frame>)
 => (message :: false-or(<string>))
  let status-bar = frame-status-bar(frame);
  when (status-bar)
    gadget-label(status-bar) := message
  end;
  message
end method frame-status-message-setter;
||#

(defmethod (setf frame-status-message) ((message null) (frame <simple-frame>))
  (let ((status-bar (frame-status-bar frame)))
    (when status-bar
      (setf (gadget-label status-bar) message)))
  message)

(defmethod (setf frame-status-message) ((message string) (frame <simple-frame>))
  (let ((status-bar (frame-status-bar frame)))
    (when status-bar
      (setf (gadget-label status-bar) message)))
  message)



#||
/// Popup menu handling

define method note-menu-attached
    (frame :: <basic-frame>, menu :: <menu>) => ()
  next-method();
  add!(frame-owned-menus(frame), menu)
end method note-menu-attached;
||#

(defmethod note-menu-attached ((frame <basic-frame>) (menu <menu>))
  (call-next-method)
  ;; Note: the slot is CONSTANT, but the contents are not
  (add! (frame-owned-menus frame) menu))


#||
define method note-menu-detached
    (frame :: <basic-frame>, menu :: <menu>) => ()
  next-method();
  remove!(frame-owned-menus(frame), menu)
end method note-menu-detached;
||#

(defmethod note-menu-detached ((frame <basic-frame>) (menu <menu>))
  (call-next-method)
  (delete menu (frame-owned-menus frame)))



#||
/// Frame initialization

define method initialize
    (frame :: <basic-frame>,
     #key frame-manager: framem, parent = $unsupplied,
          state = #"detached", mode = #"modeless",
	  enabled? = #t, iconified? = #f, maximized? = #f,
	  fixed-width? = #f, fixed-height? = #f, resizable? = #t,
	  always-on-top? = #f,
     	  alt-key-is-meta? = #f, allow-control-alt? = $unsupplied,
	  x, y, width, height, disabled-commands = #[],
          foreground, background, text-style,
          dialog-for = $unsupplied, save-under? = #f,
	  minimize-box? = #t, maximize-box? = #t,
	  center? = #f, keyboard-interrupt? = #t)
  next-method();
  // Initialize the event handler very early
  unless (event-handler(frame))
    event-handler(frame) := frame
  end;
  let state :: <integer>
    = select (state)
	#"detached"  => %frame_detached;
	#"unmapped"  => %frame_unmapped;
	#"mapped"    => %frame_mapped;
	#"destroyed" => %frame_destroyed;
      end;
  let mode :: <integer>
    = select (mode)
	#"modeless"     => %frame_modeless;
	#"modal"        => %frame_modal;
	#"system-modal" => %frame_system_modal;
      end;
  let fixed-width?  = if (resizable?) fixed-width?  else #t end;
  let fixed-height? = if (resizable?) fixed-height? else #t end;
  let allow-control-alt?
    = if (supplied?(allow-control-alt?)) allow-control-alt? else alt-key-is-meta? end;
  frame-flags(frame)
    := logior(state, mode,
	      if (enabled?) %frame_enabled else 0 end,
	      if (iconified?) %frame_iconified else 0 end,
	      if (maximized?) %frame_maximized else 0 end,
	      if (fixed-width?) %frame_fixed_width else 0 end,
	      if (fixed-height?) %frame_fixed_height else 0 end,
	      if (always-on-top?) %frame_on_top else 0 end,
	      if (alt-key-is-meta?) %frame_alt_is_meta else 0 end,
	      if (allow-control-alt?) %frame_allow_control_alt else 0 end,
	      if (save-under?) %frame_save_under else 0 end,
	      if (minimize-box?) %frame_minimize_box else 0 end,
	      if (maximize-box?) %frame_maximize_box else 0 end,
	      if (center?) %frame_centered else 0 end,
	      if (keyboard-interrupt?) %keyboard_interrupt else 0 end);
  when (foreground | background | text-style)
    frame.%style-descriptor
      := make(<style-descriptor>,
	      foreground: foreground,
	      background: background,
	      text-style: text-style)
  end;
  // Save various other frame properties
  let properties :: <stretchy-object-vector> = frame-properties(frame);
  when (supplied?(dialog-for))
    put-property!(properties, dialog-for:, dialog-for)
  end;
  // Save the requested geometry
  let geometry = frame-geometry(frame);
  geometry[0] := x;
  geometry[1] := y;
  geometry[2] := width;
  geometry[3] := height;
  // Initially disable some commands
  let table :: <object-table> = frame-disabled-commands(frame);
  for (command in disabled-commands)
    gethash(table, command) := #t
  end;
  // The frame doesn't get attached and mapped until 'start-frame',
  // but we still want to figure out where to attach the frame for later
  let parent-supplied? = supplied?(parent);
  let parent = if (parent-supplied?) parent else framem end;
  let framem
    = select (parent by instance?)
	singleton(#f) =>
	  unless (parent-supplied?)
	    current-frame-manager() | find-frame-manager()
	  end;
	<frame-manager> => parent;
	<frame>   => frame-manager(parent);
	<port>    => find-frame-manager(port: parent);
	<display> => find-frame-manager(port: port(parent));
	<sheet>   => frame-manager(sheet-frame(parent))
      end;
  frame-manager(frame) := framem;
  // Now initialize the event queue
  unless (frame-event-queue(frame))
    let owner = frame-owner(frame);
    frame-event-queue(frame)
      := if (owner & ~frame-needs-event-queue?(framem, frame))
	   frame-event-queue(owner)
	 else
	   make-event-queue(framem, frame)
	 end
  end
end method initialize;
||#

(defmethod initialize-instance :after ((frame <basic-frame>)
				       &key ((:frame-manager framem)) (parent :unsupplied)
				       (state :detached) (mode :modeless)
				       (enabled? t) (iconified? nil) (maximized? nil)
				       (fixed-width? nil) (fixed-height? nil) (resizable? t)
				       (always-on-top? nil)
				       (alt-key-is-meta? nil) (allow-control-alt? :unsupplied)
				       x y width height (disabled-commands #())
				       foreground background text-style
				       (dialog-for :unsupplied) (save-under? nil)
				       (minimize-box? t) (maximize-box? t)
				       (center? nil) (keyboard-interrupt? t)
				       &allow-other-keys)
  ;; Initialize the event handler very early
  (unless (event-handler frame)
    (setf (event-handler frame) frame))
  (let ((state (ecase state
                 (:detached  %frame_detached)
                 (:unmapped  %frame_unmapped)
                 (:mapped    %frame_mapped)
                 (:destroyed %frame_destroyed)))
	(mode (ecase mode
                (:modeless     %frame_modeless)
                (:modal        %frame_modal)
                (:system-modal %frame_system_modal)))
	(fixed-width?       (if resizable? fixed-width? t))
	(fixed-height?      (if resizable? fixed-height? t))
	(allow-control-alt? (if (supplied? allow-control-alt?) allow-control-alt? alt-key-is-meta?)))
    (setf (frame-flags frame)
	  (logior state mode
		  (if enabled? %frame_enabled 0)
		  (if iconified? %frame_iconified 0)
		  (if maximized? %frame_maximized 0)
		  (if fixed-width? %frame_fixed_width 0)
		  (if fixed-height? %frame_fixed_height 0)
		  (if always-on-top? %frame_on_top 0)
		  (if alt-key-is-meta? %frame_alt_is_meta 0)
		  (if allow-control-alt? %frame_allow_control_alt 0)
		  (if save-under? %frame_save_under 0)
		  (if minimize-box? %frame_minimize_box 0)
		  (if maximize-box? %frame_maximize_box 0)
		  (if center? %frame_centered 0)
		  (if keyboard-interrupt? %keyboard_interrupt 0)))
    (when (or foreground background text-style)
      (setf (%style-descriptor frame)
	    (make-instance '<style-descriptor>
			   :foreground foreground
			   :background background
			   :text-style text-style)))
    ;; Save various other frame properties
    (let ((properties (frame-properties frame)))
      (when (supplied? dialog-for)
	(PUT-PROPERTY! properties :dialog-for dialog-for)))
    ;; Save the requested geometry
    (let ((geometry (frame-geometry frame)))
      (setf (aref geometry 0) x)
      (setf (aref geometry 1) y)
      (setf (aref geometry 2) width)
      (setf (aref geometry 3) height))
    ;; Initially disable some commands
    (let ((table (frame-disabled-commands frame)))
      ;; FIXME: THIS IS A HASH-TABLE, NOT A SEQUENCE
      (loop for command across disabled-commands
	 do (setf (gethash table command) t)))
    ;; The frame doesn't get attached and mapped until 'start-frame',
    ;; but we still want to figure out where to attach the frame for later
    (let* ((parent-supplied? (supplied? parent))
	   (parent (if parent-supplied? parent framem))
	   (framem (typecase parent
		     (null (unless parent-supplied?
			     (or (current-frame-manager) (find-frame-manager))))
		     (<frame-manager> parent)
		     (<frame>   (frame-manager parent))
		     (<port>    (find-frame-manager :port parent))
		     (<display> (find-frame-manager :port (port parent)))
		     (<sheet>   (frame-manager (sheet-frame parent))))))
      (setf (frame-manager frame) framem)
      ;; Now initialize the event queue
      (unless (frame-event-queue frame)
	(let ((owner (frame-owner frame)))
	  (setf (frame-event-queue frame)
		(if (and owner (not (frame-needs-event-queue? framem frame)))
		    (frame-event-queue owner)
		    (make-event-queue framem frame))))))))


#||
// Will return either <top-level-sheet> or <top-level-stream>
define method frame-top-level-sheet-class
    (frame :: <basic-frame>, #key) => (class :: <class>)
  <top-level-sheet>
end method frame-top-level-sheet-class;
||#

(defmethod frame-top-level-sheet-class ((frame <basic-frame>) &key)
"
Will return either <TOP-LEVEL-SHEET> or <TOP-LEVEL-STREAM>.
"
  (find-class '<top-level-sheet>))



#||
/// Creating frames

// Create a frame of the specified type if one does not already exist, 
// and then run it, possibly in its own thread.  If one already exists,
// just select it.
define method find-frame
    (frame-class, #rest initargs,
     #key create? = #t, activate? = #t, own-thread? = #t,
          port: _port, frame-manager: framem, test = identity,
	  z-order :: one-of(#"top-down", #"bottom-up") = #"top-down",
     #all-keys)
 => (frame :: <frame>)
  dynamic-extent(initargs);
  let frame
    = unless (create? == #"force")
        block (return)
          do-frames(method (frame)
		      when (instance?(frame, frame-class) & test(frame))
			return(frame)
		      end
		    end method,
		    // Restrict the search if these are supplied
		    port: _port, frame-manager: framem, z-order: z-order)
        end
      end;
  when (create? & ~frame)
    with-keywords-removed
        (initargs = initargs,
	 #[create?:, activate?:, own-thread?:, port:, frame-manager:, z-order:])
      case
        framem => #f;
        _port =>
          framem := find-frame-manager(port: _port);
        otherwise =>
          framem := find-frame-manager()
      end;
      frame := apply(make, frame-class, frame-manager: framem, initargs)
    end
  end;
  when (frame & activate?)
    case
      frame-thread(frame) =>
        raise-frame(frame, activate?: #t);
      own-thread? =>
	make(<thread>,
	     function: method () start-frame(frame) end,
	     name: frame-title(frame) | "Unnamed frame");
      otherwise =>
        start-frame(frame)
    end
  end;
  frame
end method find-frame;
||#

(defmethod find-frame (frame-class &rest initargs
		       &key (create? t) (activate? t) (own-thread? t)
		       ((:port _port)) ((:frame-manager framem)) (test #'identity)
			 (z-order :top-down)
			 &allow-other-keys)
"
Create a frame of the specified type if one does not already exist, 
and then run it, possibly in its own thread.  If one already exists,
just select it.
"
  (declare (dynamic-extent initargs))
  (check-type z-order (member :top-down :bottom-up))
  (let ((frame (unless (eql create? :force)
		 (block return
		   (do-frames #'(lambda (frame)
				  (when (and (typep frame frame-class) (funcall test frame))
				    (return-from return frame)))
		     ;; Restrict the search if these are supplied
		     :port _port :frame-manager framem :z-order z-order)))))
    (when (and create? (not frame))
      (with-keywords-removed (initargs = initargs (:create? :activate? :own-thread? :port :frame-manager :z-order))
        (cond (framem nil)
	      (_port  (setf framem (find-frame-manager :port _port)))
	      (t      (setf framem (find-frame-manager))))
	(setf frame (apply #'make-instance frame-class :frame-manager framem initargs))))
    (when (and frame activate?)
      (cond ((frame-thread frame)
	     (duim-debug-message "FRAME HAS THREAD, RAISING FRAME")
	     (raise-frame frame :activate? t))
	    (own-thread?
	     (duim-debug-message "OWN-THREAD? IS T, CREATING THREAD AND INVOKING START-FRAME")
	     (bordeaux-threads:make-thread #'(lambda () (start-frame frame))
					   :name (or (frame-title frame) "Unnamed frame")))
	    (t
	     (duim-debug-message "INVOKING START-FRAME IN CURRENT THREAD")
	     (start-frame frame))))
    frame))



#||
/// Attaching and detaching frames

// Attach the frame to a frame manager
define method attach-frame
    (framem :: <frame-manager>, frame :: <basic-frame>) => (frame :: <frame>)
  assert(frame-state(frame) ~== #"destroyed",
         "You cannot attach frame %= because it has been destroyed", frame);
  assert(frame-state(frame) ==  #"detached",
         "There is already a frame manager for %=", frame);
  frame-manager(frame) := framem;
  dynamic-bind (*current-frame* = frame)
    let panes = generate-panes(framem, frame);
    // If there's a command table, install it now
    let command-table = frame-command-table(frame);
    when (command-table)
      let menu-bar = frame-menu-bar(frame);
      if (menu-bar)
	unless (sheet-resource-id(menu-bar))
	  error("You can't supply both a menu bar and a command table for %=",
		frame)
	end
      else
	frame-menu-bar(frame) := make-command-menu-bar(framem, frame)
      end
    end;
    // Note that we always generate a frame wrapper, even when there are no
    // panes in the layout.  This means, for example, that we can get a frame
    // that has only a menu-bar.
    //--- Do we really want to do this?  Of course, 'frame-wrapper' is free
    //--- to return #f if it really wants a frame with no UI component...
    let layout = frame-wrapper(framem, frame, panes);
    when (layout)
      //---*** How do we specify a different display than the default
      //---*** one for the port?
      let top-sheet
	= attach-sheet(find-display(port: port(frame)), layout,
		       sheet-class: frame-top-level-sheet-class(frame),
		       fixed-width?:  frame-fixed-width?(frame),
		       fixed-height?: frame-fixed-height?(frame),
		       frame-manager: framem, frame: frame,
		       event-queue: frame-event-queue(frame),
		       container: frame.%container,
		       container-region: frame.%container-region,
		       foreground: default-foreground(frame),
		       background: default-background(frame),
		       text-style: default-text-style(frame),
		       resource-id: frame-resource-id(frame));
      top-level-sheet(frame) := top-sheet
    end
  end;
  frame-state(frame) := #"unmapped";
  let owner = frame-owner(frame);
  owner & add!(frame-owned-frames(owner), frame);
  add!(frame-manager-frames(framem), frame);
  // This uses 'handle-event' rather than 'distribute-event' because 
  // the user needs to see this before the event loop starts, since by
  // that time everything has already happened (mapping etc).
  handle-event(event-handler(frame),
	       make(<frame-created-event>, frame: frame));
  frame
end method attach-frame;
||#

;; Attach the frame to a frame manager
;; Note that this method disallows having both a menu bar and a command
;; table in the same frame.
(defmethod attach-frame ((framem <frame-manager>) (frame <basic-frame>))
  (when (eql (frame-state frame) :destroyed)
    (error "You cannot attach frame ~a because it has been destroyed" frame))
  (unless (eql (frame-state frame) :detached)
    (error "There is already a frame manager for ~a" frame))
  (setf (frame-manager frame) framem)
  (dynamic-let ((*current-frame* frame))
    (let ((panes (generate-panes framem frame))
	  ;; If there's a command table, install it now
	  (command-table (frame-command-table frame)))
      (when command-table
	(let ((menu-bar (frame-menu-bar frame)))
	  (if menu-bar
	      (unless (sheet-resource-id menu-bar)
		(error "You can't supply both a menu bar and a command table for ~a" frame))
	      ;; else
	      (setf (frame-menu-bar frame) (make-command-menu-bar framem frame)))))
      ;; Note that we always generate a frame wrapper, even when there are no
      ;; panes in the layout.  This means, for example, that we can get a frame
      ;; that has only a menu-bar.
      ;;--- Do we really want to do this?  Of course, 'frame-wrapper' is free
      ;;--- to return #f if it really wants a frame with no UI component...
      (let ((layout (frame-wrapper framem frame panes)))
	(when layout
	  ;;---*** How do we specify a different display than the default
	  ;;---*** one for the port?
	  (let ((top-sheet (attach-sheet (find-display :port (port frame)) layout
					 :sheet-class (frame-top-level-sheet-class frame)
					 :fixed-width? (frame-fixed-width? frame)
					 :fixed-height? (frame-fixed-height? frame)
					 :frame-manager framem :frame frame
					 :event-queue (frame-event-queue frame)
					 :container (%container frame)
					 :container-region (%container-region frame)
					 :foreground (default-foreground frame)
					 :background (default-background frame)
					 :text-style (default-text-style frame)
					 :resource-id (frame-resource-id frame))))
            (setf (top-level-sheet frame) top-sheet))))))
  (setf (frame-state frame) :unmapped)
  (let ((owner (frame-owner frame)))
    (and owner (add! (frame-owned-frames owner) frame)))
  ;; FIXME: FRAME-MANAGER-FRAMES SHOULD BE A STRETCHY VECTOR
  (setf (frame-manager-frames framem) (add! (frame-manager-frames framem) frame))
  ;; This uses 'handle-event' rather than 'distribute-event' because 
  ;; the user needs to see this before the event loop starts, since by
  ;; that time everything has already happened (mapping etc).
  (handle-event (event-handler frame)
		(make-instance '<frame-created-event> :frame frame))
  frame)


#||
// Detach the frame from its frame manager
define method detach-frame
    (framem :: <frame-manager>, frame :: <basic-frame>) => (frame :: <frame>)
  unless (frame-state(frame) == #"destroyed")
    assert(frame-manager(frame) == framem,
	   "The frame %= does not belong to this frame manager", frame);
    select (frame-state(frame))
      #"mapped" =>
	frame-mapped?(frame) := #f;
      otherwise => #f
    end;
    // Detach the frame's top-level sheet from the display
    let top-sheet = top-level-sheet(frame);
    when (top-sheet)
      // Update the frame's geometry
      let geometry = frame-geometry(frame);
      let (x, y) = sheet-position(top-sheet);
      let (width, height) = sheet-size(top-sheet);
      geometry[0] := x;
      geometry[1] := y;
      geometry[2] := width;
      geometry[3] := height;
      let _display = display(top-sheet);
      when (_display)
	detach-sheet(_display, top-sheet)
      end
    end;
    top-level-sheet(frame) := #f;
    // If there are any menus still attached, detach them, too
    let owned-menus = frame-owned-menus(frame);
    for (menu in owned-menus)
      //--- Can we make 'detach-sheet' remove the owned menu?
      let _display = display(menu);
      when (_display)
	detach-sheet(_display, menu)
      end
    end;
    size(owned-menus) := 0;
    // Now detach the frame from the frame manager
    frame-state(frame) := #"detached";
    let owner = frame-owner(frame);
    owner & remove!(frame-owned-frames(owner), frame);
    remove!(frame-manager-frames(framem), frame)
  end;
  frame
end method detach-frame;
||#

(defmethod detach-frame ((framem <frame-manager>) (frame <basic-frame>))
  (unless (eql (frame-state frame) :destroyed)
    (unless (eql (frame-manager frame) framem)
      (error "The frame ~a does not belong to this frame manager" frame))
    (case (frame-state frame)
      (:mapped (setf (frame-mapped? frame) nil))
      (t nil))
    ;; Detach the frame's top-level sheet from the display
    (let ((top-sheet (top-level-sheet frame)))
      (when top-sheet
	;; Update the frame's geometry
	(let ((geometry (frame-geometry frame)))
	  (multiple-value-bind (x y)
	      (sheet-position top-sheet)
	    (multiple-value-bind (width height)
		(sheet-size top-sheet)
	      (setf (aref geometry 0) x)
	      (setf (aref geometry 1) y)
	      (setf (aref geometry 2) width)
	      (setf (aref geometry 3) height)
	      (let ((_display (display top-sheet)))
		(when _display
		  (detach-sheet _display top-sheet))))))))
    (setf (top-level-sheet frame) nil)
    ;; If there are any menus still attached, detach them, too
    (let ((owned-menus (frame-owned-menus frame)))
      (loop for menu across owned-menus
	   do (duim-debug-message "DETACH-FRAME: DETACHING MENUS")
	 ;;--- Can we make 'detach-sheet' remove the owned menu?
	 do (let ((_display (display menu)))
	      (when _display
		(detach-sheet _display menu))))
      ;; (SETF (SEQUENCE-SIZE OWNED-MENUS) 0)
      (setf (fill-pointer owned-menus) 0))
    ;; Now detach the frame from the frame manager
    (setf (frame-state frame) :detached)
    (let ((owner (frame-owner frame)))
      (and owner (SEQUENCE-REMOVE! (frame-owned-frames owner) frame)))
    (setf (frame-manager-frames framem)
	  (SEQUENCE-REMOVE! (frame-manager-frames framem) frame)))
  frame)


#||
// The default wrapper does nothing -- it just returns the user-generated
// "top level" sheet...
define method frame-wrapper
    (framem :: <frame-manager>, frame :: <frame>, sheet :: false-or(<sheet>))
 => (wrapper :: false-or(<sheet>))
  sheet
end method frame-wrapper;
||#

;; The default wrapper does nothing -- it just returns the user-generated
;; "top level" sheet...
(defmethod frame-wrapper ((framem <frame-manager>) (frame  <frame>) (sheet  null))
  sheet)

(defmethod frame-wrapper ((framem <frame-manager>) (frame  <frame>) (sheet  <sheet>))
  sheet)



#||
/// Frame-defining macrology

define macro frame-definer
  { define ?modifiers:* frame ?:name (?superclasses:*) ?slots:* end }
    => { define ?modifiers frame-class ?name (?superclasses) ?slots end;
         define frame-panes ?name (?superclasses) ?slots end;
         define frame-gadget-bars ?name (?superclasses) ?slots end; 
	 define frame-layout ?name (?superclasses) ?slots end; }
end macro frame-definer;
||#

;; TODO: Revisit all this macrology

(defmacro define-frame (name superclasses slots &optional class-options)
"
Defines a new class of frame called _name_ with the specified
properties. This macro is equivalent to 'defclass', but with
additional options.

The _supers_ argument lets you specify any classes from which the
frame you are creating should inherit. You must include at least one
concrete frame class, such as <simple-frame> or <dialog-frame>.

The _slots-panes-options_ supplied describe the state variables of the
frame class; that is, the total composition of the frame. This
includes, but is not necessarily limited to, any panes, layouts, tool
bar, menus, and status bar contained in the frame. You can specify
arbitrary slots in the definition of the frame. You may specify any of
the following:

  + A number of slots for defining per-instance values of the frame
    state.
  + A number of named panes. Each pane defines a sheet of some sort.
  + A single layout.
  + A tool bar.
  + A status bar.
  + A menu bar.
  + A command table.
  + A number of sequential pages for inclusion in a multi-page frame
    such as a wizard or property dialog.

Note: If the frame has a menu bar, either define the menu bar and its
panes, or a command table, but not both. See the discussion below for
more details.

The syntax for each of these options is described below.

The _slot_ option allows you to define any slot values that the new
frame class should allow. This option has the same syntax as slot
specifiers in 'defclass', allowing you to define initargs, accessors,
and so on for the frame class.

For each of the remaining options, the syntax is as follows:

    (_option_ _name_ (_owner_) _body_)

The argument _option_ is the name of the option used, taken from the
list described below, _name_ is the name you assign to the option for
use within your code, _owner_ is the owner of the option, usually the
frame itself, and _body_ contains the definition value returned by the
option.

_pane_ specifies a single pane in the frame. The default is nil,
meaning that there is no single pane. This is the simplest way to
define a pane hierarchy.

_layout_ specifies the layout of the frame. The default is to lay out
all of the named panes in horizontal strips. The value of this option
must evaluate to an instance of a layout.

_command-table_ defines a command table for the frame. The default is
to create a command table with the same name as the frame. The value
of this option must evaluate to an instance of <command-table>.

_menu-bar_ is used to specify the commands that will be shown in the
menu bar of the frame. The default is T. If used, it typically
specifies the top-level commands of the frame. The value of this
option can evaluate to any of the following:

nil
    The frame has no menu bar.

T
    The menu bar for the frame is defined by the value of the
    _command-table_ option.

A command table
    The menu bar for the frame is defined by this command table.

A body of code
    This is interpreted the same way as the 'menu-item' options to
    'define-command-table'.

_disabled-commands_ is used to specify a list of command names that
are initially disabled in the application frame. The default is
#(). The set of enabled and disabled commands can be modified via
'(setf command-enabled?)'.

_tool-bar_ is used to specify a tool bar for the frame. The default is
nil. The value of this option must evaluate to an instance of
<tool-bar>.

_top-level_ specifies a function that executes the top level loop of
the frame. It has as its argument a list whose first element is the
name of a function to be called to execute the top-level loop. The
function must take at least one argument, which is the frame
itself. The rest of the list consists of additional arguments to be
passed to the function.

_icon_ specifies an <image> to be used in the window decoration for
the frame. This icon may be used in the title bar of the frame, or
when the frame is iconified, for example.

_geometry_ specifies the geometry for the frame.

_pages_ is used to define the pages of a wizard or property
frame. This evaluates to a list of pages, each of which can be defined
as panes within the frame definition itself. For example:

    (define-frame <wizard-type> (<wizard-frame>)
      ...
      (pages (frame)
        (vector (page-1 frame) (page-2 frame) (page-3 frame)))))

The _name_, _supers_, and slot arguments are not evaluated. The values
of each of the options are evaluated.

Example:

    (define-frame <multiple-values-dialog> (<dialog-frame>)
      ((pane label-pane (frame)
         (make-pane '<option-box>
                    :items (list \"&Red\" \"&Green\" \"&Blue\")))
       (pane check-one (frame)
         (make-pane '<check-button>
                    :label \"Check box 1 test text\"))
       (pane check-two (frame)
         (make-pane '<check-button>
                    :label \"Check box 2 test text\"))
       (pane radio-box (frame)
         (make-pane '<radio-box>
                    :items (list \"Option &1\" \"Option &2\"
                                 \"Option &3\" \"Option &4\")
                    :orientation :vertical))
       (pane first-group-box (frame)
         (grouping (\"Group box\" :max-width +fill+)
           (vertically (:spacing 4)
             (make-pane '<label> :label \"Label:\")
             (horizontally (:spacing 4 :y-alignment :center)
               (label-pane frame)
               (make-pane '<button> :label \"Button\"))
             (check-one frame)
             (check-two frame))))
       (pane second-group-box (frame)
         (grouping (\"Group box\" :max-width +fill+)
           (radio-box frame)))
       (layout (frame)
         (vertically (:spacing 4)
           (first-group-box frame)
           (second-group-box frame)))))

TODO: check all the syntax here is actually the way the Lisp version
does it. It looks like it is, pretty much.
"
  (let ((panes  (define-frame-panes       name superclasses slots))
	(bars   (define-frame-gadget-bars name superclasses slots))
	(layout (define-frame-layout      name superclasses slots)))
    `(progn
       (define-frame-class       ,name ,superclasses ,slots ,class-options)
       ,@panes
       ,@bars
       ,@layout)))


#||
define macro frame-class-definer
  { define ?modifiers:* frame-class ?:name (?superclasses:*) ?slots:* end }
    => { define ?modifiers class ?name (?superclasses) ?slots end }
 slots:
  { } => { }
  { ?slot:*; ... } => { ?slot ... }
 slot:
  { ?modifiers:* pane ?:name (?frame:variable) ?:body }
    => { ?modifiers slot ?name ## "-pane" :: false-or(<abstract-sheet>) = #f; }
  { ?modifiers:* resource ?:name :: ?type:name = ?resource-id:expression }
    => { ?modifiers slot ?name ## "-pane" :: false-or(<abstract-sheet>) = #f; }
  // The next seven feel like the 'exception' clause in 'block', in that
  // they take an argument (the frame) and a body, but no 'end'
  { layout (?frame:variable) ?:body } => { }		// uses %layout slot
  { menu-bar (?frame:variable) ?:body } => { }		// uses %menu-bar slot
  { tool-bar (?frame:variable) ?:body } => { }		// uses %tool-bar slot
  { status-bar (?frame:variable) ?:body } => { }	// uses %status-bar slot
  { command-table (?frame:variable) ?:body } => { }	// uses %command-table slot
  { input-focus (?frame:variable) ?:body } => { }	// uses %input-focus slot
  { pages  (?frame:variable) ?:body } => { }		// uses %pages slot
  // Catch 'slot', 'keyword', and so forth
  { ?other:* } => { ?other; }
end macro frame-class-definer;
||#


(defun %process-frame-slot (slot)
  (let ((indicator (%STRING-DESIGNATOR->STRING (first slot))))
    ;; These are dealt with elsewhere.
    (if (member indicator
		(list (symbol-name :LAYOUT)         ;; uses %layout slot
		      (symbol-name :MENU-BAR)       ;; uses %menu-bar slot
		      (symbol-name :TOOL-BAR)       ;; uses %tool-bar slot
		      (symbol-name :STATUS-BAR)     ;; uses %status-bar slot
		      (symbol-name :COMMAND-TABLE)  ;; uses %command-table slot
		      (symbol-name :INPUT-FOCUS)    ;; uses %input-focus slot
		      (symbol-name :PAGES))         ;; uses %pages slot
		:test #'string=)
	nil
	;; Create a slot for :PANE + :RESOURCE syntactic sugar...
	;; Note these are also dealt with in 'DEFINE-FRAME-PANES',
	;; in addition to here.
	(if (or (string= indicator (symbol-name :PANE))
		(string= indicator (symbol-name :RESOURCE)))
	    (let ((slot-name (intern (concatenate 'string
						  (symbol-name (second slot))
						  "-PANE"))))
	      `(,slot-name :type (or null <abstract-sheet>)
			   :initform nil
			   :accessor ,slot-name))
	    ;; else - just a 'normal' slot...
	    slot))))


;;; FIXME: Is this right?

(defmacro define-frame-class (name superclasses slots &optional class-options)
  (if class-options
      `(defclass ,name ,superclasses
	 ,(loop for slot in slots
	     for processed-slot = (%process-frame-slot slot)
	     unless (zerop (length processed-slot))
	     collect processed-slot)
	 ,class-options)
      ;; else
      `(defclass ,name ,superclasses
	 ,(loop for slot in slots
	     for processed-slot = (%process-frame-slot slot)
	     unless (zerop (length processed-slot))
	     collect processed-slot))))


#||
define macro frame-panes-definer
  { define frame-panes ?class:name (?superclasses:*) end }
    => { }
  { define frame-panes ?class:name (?superclasses:*) 
      pane ?:name (?frame:variable) ?:body; ?more-slots:*
    end }
    => { define method ?name (?frame :: ?class) => (pane :: <sheet>)
	   let _framem = frame-manager(?frame);
	   ?frame.?name ## "-pane"
	   | (?frame.?name ## "-pane"
                := with-frame-manager (_framem)
                     ?body
                   end)
	 end method ?name; 
         define frame-panes ?class (?superclasses) ?more-slots end; }
  { define frame-panes ?class:name (?superclasses:*) 
      resource ?:name :: ?type:name = ?resource-id:expression; ?more-slots:*
    end }
    => { define method ?name (_frame :: ?class) => (pane :: <sheet>)
	   let _framem = frame-manager(_frame);
	   _frame.?name ## "-pane"
	   | (_frame.?name ## "-pane"
                := with-frame-manager (_framem)
                     make(?type, resource-id: ?resource-id)
                   end)
	 end method ?name; 
         define frame-panes ?class (?superclasses) ?more-slots end; }
  { define frame-panes ?class:name (?superclasses:*) 
      ?non-pane-slot:*; ?more-slots:*
    end }
    => { define frame-panes ?class (?superclasses) ?more-slots end; }
end macro frame-panes-definer;
||#

;;; FIXME: TIDY THIS UP, LIKE THE ONES IN PANES HAVE BEEN TIDIED...

(defun %process-frame-pane (class-name slot)
  (let ((indicator (and (listp slot) (%STRING-DESIGNATOR->STRING (first slot)))))
    (cond ((string= indicator (symbol-name :PANE))
	   ;; (:PANE <NAME> (<FRAME>) . BODY)
	   (let* ((name  (cadr slot))
		  (frame (car (caddr slot)))
		  (body  (cdddr slot))
		  (mname (intern (concatenate 'string
					      (symbol-name name)
					      "-PANE"))))
	     `(defmethod ,name ((,frame ,class-name))
		(let ((_framem (frame-manager ,frame)))
		  (or (,mname ,frame)
		      (setf (,mname ,frame)
			    (with-frame-manager (_framem)
			      ,@body)))))))
	  ((string= indicator (symbol-name :RESOURCE))
	   ;; (:RESOURCE <NAME> :OF <TYPE> = <RESOURCE-ID>)
	   (let* ((res-name    (cadr slot))
		  (res-type    (cadddr slot))
		  (resource-id (cadr (cddddr slot)))
		  (mname       (intern (concatenate 'string
						    (symbol-name res-name)
						    "-PANE"))))
	     `(defmethod ,res-name ((_frame ,class-name))
		(let ((_framem (frame-manager _frame)))
		  (or (,mname _frame)
		      (setf (,mname _frame)
			    (with-frame-manager (_framem)
			      (make-pane ',res-type
					 :resource-id ,resource-id))))))))
	  ;; wasn't a :PANE or :RESOURCE 'slot'; ignore
	  (t nil))))

;; This deals with :PANE + :RESOURCE 'slots'.

;; (defmacro define-frame-panes (name superclasses slots)
;;   `(progn
;;      ,@(loop for slot in slots
;; 	  for processed-pane = (%process-frame-pane name slot)
;; 	  unless (zerop (length processed-pane))
;; 	  collect processed-pane)))

(defun define-frame-panes (name superclasses slots)
  (declare (ignore superclasses))
  (loop for slot in slots
     for processed-pane = (%process-frame-pane name slot)
     unless (zerop (length processed-pane))
     collect processed-pane))



#||
define macro frame-gadget-bars-definer
  { define frame-gadget-bars ?class:name (?superclasses:*) end }
    => { }
  { define frame-gadget-bars ?class:name (?superclasses:*) 
      layout (?frame:variable) ?:body; ?more-slots:*
    end }
    => { define method frame-layout
	     (?frame :: ?class) => (sheet :: false-or(<sheet>))
	   let framem = frame-manager(?frame);
	   ?frame.%layout
	   | (?frame.%layout
                := with-frame-manager (framem)
                     ?body
                   end)
	 end method frame-layout; 
         define frame-gadget-bars ?class (?superclasses) ?more-slots end; }
  { define frame-gadget-bars ?class:name (?superclasses:*) 
      menu-bar (?frame:variable) ?:body; ?more-slots:*
    end }
    => { define method frame-menu-bar
	     (?frame :: ?class) => (sheet :: false-or(<menu-bar>))
	   let framem = frame-manager(?frame);
	   ?frame.%menu-bar
	   | (?frame.%menu-bar
                := with-frame-manager (framem)
                     ?body
                   end)
         end method frame-menu-bar; 
         define frame-gadget-bars ?class (?superclasses) ?more-slots end; }
  { define frame-gadget-bars ?class:name (?superclasses:*) 
      tool-bar (?frame:variable) ?:body; ?more-slots:*
    end }
    => { define method frame-tool-bar 
	     (?frame :: ?class) => (sheet :: false-or(<tool-bar>))
	   let framem = frame-manager(?frame);
	   ?frame.%tool-bar
	   | (?frame.%tool-bar
                := with-frame-manager (framem)
                     ?body
                   end)
         end method frame-tool-bar; 
         define frame-gadget-bars ?class (?superclasses) ?more-slots end; }
  { define frame-gadget-bars ?class:name (?superclasses:*) 
      status-bar (?frame:variable) ?:body; ?more-slots:*
    end }
    => { define method frame-status-bar 
	     (?frame :: ?class) => (sheet :: false-or(<status-bar>))
	   let framem = frame-manager(?frame);
	   ?frame.%status-bar
	   | (?frame.%status-bar
                := with-frame-manager (framem)
                     ?body
                   end)
         end method frame-status-bar; 
         define frame-gadget-bars ?class (?superclasses) ?more-slots end; }
  { define frame-gadget-bars ?class:name (?superclasses:*) 
      command-table (?frame:variable) ?:body; ?more-slots:*
    end }
    => { define method frame-command-table
	     (?frame :: ?class) => (command-table :: false-or(<command-table>))
	   ?frame.%command-table
	   | (?frame.%command-table
                := begin
                     ?body
                   end)
         end method frame-command-table; 
         define frame-gadget-bars ?class (?superclasses) ?more-slots end; }
  { define frame-gadget-bars ?class:name (?superclasses:*) 
      input-focus (?frame:variable) ?:body; ?more-slots:*
    end }
    => { define method frame-input-focus
	     (?frame :: ?class) => (sheet :: false-or(<sheet>))
	   ?frame.%input-focus
	   | (?frame.%input-focus
                := begin
                     ?body
                   end)
         end method frame-input-focus; 
         define frame-gadget-bars ?class (?superclasses) ?more-slots end; }
  { define frame-gadget-bars ?class:name (?superclasses:*) 
      pages (?frame:variable) ?:body; ?more-slots:*
    end }
    => { define method dialog-pages 
	     (?frame :: ?class) => (pages :: <sequence>)
	   let framem = frame-manager(?frame);
	   ?frame.%pages
	   | (?frame.%pages
                := with-frame-manager (framem)
                     ?body
                   end)
	 end method dialog-pages; 
         define frame-gadget-bars ?class (?superclasses) ?more-slots end; }
  { define frame-gadget-bars ?class:name (?superclasses:*) 
      ?non-bar-slot:*; ?more-slots:*
    end }
    => { define frame-gadget-bars ?class (?superclasses) ?more-slots end; }
end macro frame-gadget-bars-definer;
||#

;; (defmacro define-frame-gadget-bars (class superclasses slots)
;;   (declare (ignorable superclasses))
;;   `(progn
;;   ,@(loop for slot in slots
;; 	for form =
;; 	(cond ((eql (first slot) :LAYOUT)
;; 	       (let ((frame-var (second slot))
;; 		     (body (third slot)))
;; 		 `(defmethod frame-layout ((,@frame-var ,class))
;; 		    (let ((framem (frame-manager ,@frame-var)))
;; 		      (or (%layout ,@frame-var)
;; 			  (setf (%layout ,@frame-var)
;; 				(with-frame-manager (framem)
;; 				  ,body)))))))
;; 	      ((eql (first slot) :MENU-BAR)
;; 	       (let ((frame-var (second slot))
;; 		     (body (third slot)))
;; 		 `(defmethod frame-menu-bar ((,@frame-var ,class))
;; 		    (let ((framem (frame-manager ,@frame-var)))
;; 		      (or (%menu-bar ,@frame-var)
;; 			  (setf (%menu-bar ,@frame-var)
;;                                 (with-frame-manager (framem)
;;                                   ,body)))))))
;; 	      ((eql (first slot) :TOOL-BAR)
;; 	       (let ((frame-var (second slot))
;; 		     (body (third slot)))
;; 		 `(defmethod frame-tool-bar ((,@frame-var ,class))
;; 		    (let ((framem (frame-manager ,@frame-var)))
;; 		      (or (%tool-bar ,@frame-var)
;; 			  (setf (%tool-bar ,@frame-var)
;;                                 (with-frame-manager (framem)
;;                                   ,body)))))))
;; 	      ((eql (first slot) :STATUS-BAR)
;; 	       (let ((frame-var (second slot))
;; 		     (body (third slot)))
;; 		 `(defmethod frame-status-bar ((,@frame-var ,class))
;; 		    (let ((framem (frame-manager ,@frame-var)))
;; 		      (or (%status-bar ,@frame-var)
;; 			  (setf (%status-bar ,@frame-var)
;;                                 (with-frame-manager (framem)
;;                                   ,body)))))))
;; 	      ((eql (first slot) :COMMAND-TABLE)
;; 	       (let ((frame-var (second slot))
;; 		     (body (third slot)))
;; 		 `(defmethod frame-command-table ((,@frame-var ,class))
;; 		    (duim-debug-message "[FRAME-COMMAND-TABLE] entered for frame ~a" ,@frame-var)
;; 		    (or (%command-table ,@frame-var)
;;                         (setf (%command-table ,@frame-var) (progn ,body))))))
;; 	      ((eql (first slot) :INPUT-FOCUS)
;; 	       (let ((frame-var (second slot))
;; 		     (body (third slot)))
;; 		 `(defmethod frame-input-focus ((,@frame-var ,class))
;; 		    (or (%input-focus ,@frame-var)
;;                         (setf (%input-focus ,@frame-var) (progn ,body))))))
;; 	      ((eql (first slot) :PAGES)
;; 	       (let ((frame-var (second slot))
;; 		     (body (third slot)))
;; 		 `(defmethod dialog-pages ((,@frame-var ,class))
;; 		    (let ((framem (frame-manager ,@frame-var)))
;; 		      (or (%pages ,@frame-var)
;; 			  (setf (%pages ,@frame-var)
;; 				(with-frame-manager (framem)
;; 				  ,body)))))))
;;               ;; Ignore unrecognised slots
;; 	      (t nil))
;; 	unless (zerop (length form)) collect form)))

(defun define-frame-gadget-bars (class superclasses slots)
  (declare (ignorable superclasses))
  (loop for slot in slots
	for form =
	(cond ((string= (%STRING-DESIGNATOR->STRING (first slot))
			(symbol-name :LAYOUT))
	       (let ((frame-var (second slot))
		     (body (third slot)))
		 `(defmethod frame-layout ((,@frame-var ,class))
		    (let ((framem (frame-manager ,@frame-var)))
		      (or (%layout ,@frame-var)
			  (setf (%layout ,@frame-var)
				(with-frame-manager (framem)
				  ,body)))))))
	      ((string= (%STRING-DESIGNATOR->STRING (first slot))
			(symbol-name :MENU-BAR))
	       (let ((frame-var (second slot))
		     (body (third slot)))
		 `(defmethod frame-menu-bar ((,@frame-var ,class))
		    (let ((framem (frame-manager ,@frame-var)))
		      (or (%menu-bar ,@frame-var)
			  (setf (%menu-bar ,@frame-var)
                                (with-frame-manager (framem)
                                  ,body)))))))
	      ((string= (%STRING-DESIGNATOR->STRING (first slot))
			(symbol-name :TOOL-BAR))
	       (let ((frame-var (second slot))
		     (body (third slot)))
		 `(defmethod frame-tool-bar ((,@frame-var ,class))
		    (let ((framem (frame-manager ,@frame-var)))
		      (or (%tool-bar ,@frame-var)
			  (setf (%tool-bar ,@frame-var)
                                (with-frame-manager (framem)
                                  ,body)))))))
	      ((string= (%STRING-DESIGNATOR->STRING (first slot))
			(symbol-name :STATUS-BAR))
	       (let ((frame-var (second slot))
		     (body (third slot)))
		 `(defmethod frame-status-bar ((,@frame-var ,class))
		    (let ((framem (frame-manager ,@frame-var)))
		      (or (%status-bar ,@frame-var)
			  (setf (%status-bar ,@frame-var)
                                (with-frame-manager (framem)
                                  ,body)))))))
	      ((string= (%STRING-DESIGNATOR->STRING (first slot))
			(symbol-name :COMMAND-TABLE))
	       (let ((frame-var (second slot))
		     (body (third slot)))
		 `(defmethod frame-command-table ((,@frame-var ,class))
		    (let ((ct (or (%command-table ,@frame-var)
				  (setf (%command-table ,@frame-var) (progn ,body)))))
		      ct))))
	      ((string= (%STRING-DESIGNATOR->STRING (first slot))
			(symbol-name :INPUT-FOCUS))
	       (let ((frame-var (second slot))
		     (body (third slot)))
		 `(defmethod frame-input-focus ((,@frame-var ,class))
		    (or (%input-focus ,@frame-var)
                        (setf (%input-focus ,@frame-var) (progn ,body))))))
	      ((string= (%STRING-DESIGNATOR->STRING (first slot))
			(symbol-name :PAGES))
	       (let ((frame-var (second slot))
		     (body (third slot)))
		 `(defmethod dialog-pages ((,@frame-var ,class))
		    (let ((framem (frame-manager ,@frame-var)))
		      (or (%pages ,@frame-var)
			  (setf (%pages ,@frame-var)
				(with-frame-manager (framem)
				  ,body)))))))
              ;; Ignore unrecognised slots
	      (t nil))
	unless (zerop (length form)) collect form))

#||
define macro frame-layout-definer
  { define frame-layout ?class:name (?superclasses:*) end }
    => { }
  { define frame-layout ?class:name (?superclasses:*) 
      resource ?:name :: ?type:name = ?resource-id:expression; ?more-slots:*
    end }
    => { //---*** How do we now get this pane into a pinboard layout?
         define frame-layout ?class (?superclasses) ?more-slots end; }
  { define frame-layout ?class:name (?superclasses:*) 
      ?non-resource-slot:*; ?more-slots:*
    end }
    => { define frame-layout ?class (?superclasses) ?more-slots end; }
end macro frame-layout-definer;
||#

;; (defmacro define-frame-layout (class superclasses slots)
;;   ;; FIXME: I'm not sure exactly what this is supposed to do. Is it
;;   ;;        expected to pick up the layout from some resource file
;;   ;; or something?
;;   (declare (ignorable class superclasses))
;;   (loop for slot in slots
;; 	;; Not sure the following line is doing what we want in the loop...
;; 	for form =
;; 	(cond ((eql (first slot) :RESOURCE)
;; 	       ;;---*** How do we now get this pane into a pinboard layout?
;; 	       nil)
;;               ;; Ignore non-:RESOURCE 'slots'
;; 	      (t nil))
;; 	unless (zerop (length form)) collect form))

(defun define-frame-layout (class superclasses slots)
  ;; FIXME: I'm not sure exactly what this is supposed to do. Is it
  ;;        expected to pick up the layout from some resource file
  ;; or something?
  (declare (ignorable class superclasses))
  (loop for slot in slots
	;; Not sure the following line is doing what we want in the loop...
	for form =
	(cond ((string= (%STRING-DESIGNATOR->STRING (first slot))
			(symbol-name :RESOURCE))
	       ;;---*** How do we now get this pane into a pinboard layout?
	       nil)
              ;; Ignore non-:RESOURCE 'slots'
	      (t nil))
	unless (zerop (length form)) collect form))


#||
define open generic generate-panes
    (framem :: <abstract-frame-manager>, frame :: <abstract-frame>)
 => (panes :: false-or(<sheet>));
||#

(defgeneric generate-panes (framem frame))


#||
// Note that this assumes that 'current-frame()' will return the right thing...
define method generate-panes
    (framem :: <frame-manager>, frame :: <simple-frame>) => (panes :: false-or(<sheet>))
  frame-layout(frame)
end method generate-panes;
||#

;; Note that this assumes that '(current-frame)' will return the right thing...
(defmethod generate-panes ((framem <frame-manager>) (frame  <simple-frame>))
  (frame-layout frame))



#||

/// Frame titles and icons

define method frame-title-setter
    (title :: false-or(<string>), frame :: <basic-frame>) => (title :: false-or(<string>))
  frame.%title := title;
  let framem = frame-manager(frame);
  when (framem)
    note-frame-title-changed(framem, frame)
  end;
  title
end method frame-title-setter;
||#

(defmethod (setf frame-title) ((title null) (frame <basic-frame>))
  (%title-setter title frame)
  (let ((framem (frame-manager frame)))
    (when framem
      (note-frame-title-changed framem frame)))
  title)

(defmethod (setf frame-title) ((title string) (frame <basic-frame>))
  (%title-setter title frame)
  (let ((framem (frame-manager frame)))
    (when framem
      (note-frame-title-changed framem frame)))
  title)


#||
define method note-frame-title-changed
    (framem :: <frame-manager>, frame :: <frame>) => ()
  #f
end method note-frame-title-changed;
||#

(defmethod note-frame-title-changed ((framem <frame-manager>) (frame  <frame>))
  nil)


#||
define method frame-icon-setter
    (icon :: false-or(<image>), frame :: <basic-frame>) => (icon :: false-or(<image>))
  frame.%icon := icon;
  let framem = frame-manager(frame);
  when (framem)
    note-icon-changed(framem, frame)
  end;
  icon
end method frame-icon-setter;
||#

(defmethod (setf frame-icon) ((icon null) (frame <basic-frame>))
  (%icon-setter icon frame)
  (let ((framem (frame-manager frame)))
    (when framem
      ;; XXX: Changed from NOTE-ICON-CHANGED -- is this right?
      (note-frame-icon-changed framem frame)))
  icon)

(defmethod (setf frame-icon) ((icon <image>) (frame <basic-frame>))
  (%icon-setter icon frame)
  (let ((framem (frame-manager frame)))
    (when framem
      (note-frame-icon-changed framem frame)))
  icon)


#||
define method note-icon-changed
    (framem :: <frame-manager>, frame :: <frame>) => ()
  #f
end method note-icon-changed;
||#

(defmethod note-frame-icon-changed ((framem <frame-manager>) (frame <frame>))
  nil)



#||
/// Layout

define method layout-frame
    (frame :: <basic-frame>, #key width, height) => ()
  dynamic-bind (*current-frame* = frame)
    let top-sheet = top-level-sheet(frame);
    when (top-sheet)
      let (new-width, new-height) 
        = frame-top-level-sheet-size(frame-manager(frame), frame, width, height);
      when (width & new-width ~= width | height & new-height ~= height)
	warn("Frame %= rejected size %dX%d, using %dX%d instead",
	     frame, width, height, new-width, new-height)
      end;
      // Setting the top level sheets edges will call 'allocate-space'.
      //--- Don't bother with this if the size didn't change?
      //--- Really 'set-sheet-size' should handle this kludge
      let mirror = sheet-direct-mirror(top-sheet);
      if (mirror)
        let (x, y, right, bottom)
          = mirror-edges(port(frame), top-sheet, mirror);
        ignore(right, bottom);
        // Do this instead of just 'set-sheet-size' to ensure that the
        // frame does not get moved by a window manager to (0,0)
        set-sheet-edges(top-sheet, x, y, x + new-width, y + new-height)
      else
        set-sheet-size(top-sheet, new-width, new-height)
      end
    end
  end
end method layout-frame;
||#

(defmethod layout-frame ((frame <basic-frame>) &key width height)
  (dynamic-let ((*current-frame* frame))
    (let ((top-sheet (top-level-sheet frame)))
      (when top-sheet
	(multiple-value-bind (new-width new-height)
	    (frame-top-level-sheet-size (frame-manager frame) frame width height)
	  (when (or (and width (not (equal? new-width width)))
		    (and height (not (equal? new-height height))))
	    (warn "Frame ~a rejected size ~dX~d, using ~dX~d instead"
		  frame width height new-width new-height))
	  ;; Setting the top level sheet's edges will call 'allocate-space'.
	  ;;--- Don't bother with this if the size didn't change?
	  ;;--- Really 'set-sheet-size' should handle this kludge
	  (let ((mirror (sheet-direct-mirror top-sheet)))
	    (if mirror
		(multiple-value-bind (x y right bottom)
		    (mirror-edges (port frame) top-sheet mirror)
		  (declare (ignore bottom right))
		  ;; Do this instead of just 'set-sheet-size' to ensure that the
		  ;; frame does not get moved by a window manager to (0,0)
		  (set-sheet-edges top-sheet x y (+ x new-width) (+ y new-height)))
		;; else
		(set-sheet-size top-sheet new-width new-height))))))))


#||
define method frame-layout (frame :: <simple-frame>) => (layout :: false-or(<sheet>))
  frame.%layout
end method frame-layout;
||#

(defmethod frame-layout ((frame <simple-frame>))
  (%layout frame))


#||
define method frame-layout-setter 
    (layout :: false-or(<sheet>), frame :: <simple-frame>)
 => (layout :: false-or(<sheet>))
  let old-layout = frame-layout(frame);
  unless (layout == old-layout)
    dynamic-bind (*old-layout* = old-layout)
      frame.%layout := layout;
      when (old-layout)
	sheet-mapped?(old-layout) := #f
      end;
      let framem = frame-manager(frame);
      framem & update-frame-layout(framem, frame)
    end
  end;
  layout
end method frame-layout-setter;
||#

(defmethod (setf frame-layout) ((layout null) (frame  <simple-frame>))
  (let ((old-layout (frame-layout frame)))
    (unless (eql layout old-layout)
      (dynamic-let ((*old-layout* old-layout))
	(setf (%layout frame) layout)
	(when old-layout
          (setf (sheet-mapped? old-layout) nil))
	(let ((framem (frame-manager frame)))
	  (and framem (update-frame-layout framem frame))))))
  layout)

(defmethod (setf frame-layout) ((layout <sheet>) (frame  <simple-frame>))
  (let ((old-layout (frame-layout frame)))
    (unless (eql layout old-layout)
      (dynamic-let ((*old-layout* old-layout))
	(setf (%layout frame) layout)
	(when old-layout
          (setf (sheet-mapped? old-layout) nil))
	(let ((framem (frame-manager frame)))
	  (and framem (update-frame-layout framem frame))))))
  layout)


#||
define method frame-top-level-sheet-size
    (framem :: <frame-manager>, frame :: <basic-frame>,
     width :: false-or(<integer>), height :: false-or(<integer>))
 => (width, height)
  let top-sheet = top-level-sheet(frame);
  let resizable = frame-resizable?(frame);
  when (top-sheet)
    invalidate-space-requirements(top-sheet);
    // Don't let the frame get bigger than the whole screen
    let (gwidth, gheight) = box-size(display(frame));
    when (width)
      min!(width, gwidth)
    end;
    when (height)
      min!(height, gheight)
    end;
    when (~width | ~height | resizable)
      let space-req = compose-space(top-sheet, width: width, height: height);
      let (w, w-, w+, h, h-, h+) = space-requirement-components(top-sheet, space-req);
      ignore(w-, w+, h-, h+);
      when (~width  | resizable) width  := w end;
      when (~height | resizable) height := h end;
    end
  end;
  values(width, height)
end method frame-top-level-sheet-size;
||#

(defmethod frame-top-level-sheet-size ((framem <frame-manager>) (frame <basic-frame>) width height)
  (let ((top-sheet (top-level-sheet frame))
	(resizable (frame-resizable? frame)))
    (when top-sheet
      (invalidate-space-requirements top-sheet)
      ;; Don't let the frame get bigger than the whole screen
      (multiple-value-bind (gwidth gheight)
	  (box-size (display frame))
	(when width
	  (setf width (min width gwidth)))
	(when height
	  (setf height (min height gheight))))
      (when (or (not width) (not height) resizable)
	(let ((space-req (compose-space top-sheet :width width :height height)))
	  (multiple-value-bind (w w- w+ h h- h+)
	      (space-requirement-components top-sheet space-req)
	    (declare (ignore w- w+ h- h+))
	    (when (or (not width) resizable)
	      (setf width w))
            (when (or (not height) resizable)
	      (setf height h)))))))
  (values width height))



#||
/// Default button handling

define method frame-default-button-setter
    (new-button :: false-or(<button>), frame :: <basic-frame>)
 => (new-button :: false-or(<button>))
  let old-button = frame-default-button(frame);
  frame.%default-button := new-button;
  when (old-button) gadget-default?(old-button) := #f end;
  when (new-button) gadget-default?(new-button) := #t end;
  new-button
end method frame-default-button-setter;
||#

(defmethod (setf frame-default-button) ((new-button null) (frame <basic-frame>))
  (let ((old-button (frame-default-button frame)))
    (%default-button-setter new-button frame)
    (when old-button (setf (gadget-default? old-button) nil))
    (when new-button (setf (gadget-default? new-button) t))
    new-button))

(defmethod (setf frame-default-button) ((new-button <button>) (frame <basic-frame>))
  (let ((old-button (frame-default-button frame)))
    (%default-button-setter new-button frame)
    (when old-button (setf (gadget-default? old-button) nil))
    (when new-button (setf (gadget-default? new-button) t))
  new-button))



#||
/// Mapping and unmapping

define method frame-mapped?
    (frame :: <basic-frame>) => (mapped? :: <boolean>)
  frame-state(frame) == #"mapped"
end method frame-mapped?;
||#

(defmethod frame-mapped? ((frame <basic-frame>))
  (eql (frame-state frame) :mapped))


#||
define method frame-mapped?-setter
    (mapped? :: <boolean>, frame :: <basic-frame>) => (mapped? :: <boolean>)
  if (mapped?)
    map-frame(frame)
  else
    unmap-frame(frame)
  end;
  mapped?
end method frame-mapped?-setter;
||#

(defmethod (setf frame-mapped?) (mapped? (frame <basic-frame>))
  (check-type mapped? boolean)
  (if mapped?
      (map-frame frame)
      (unmap-frame frame))
  mapped?)


#||
define method map-frame (frame :: <basic-frame>) => ()
  assert(frame-state(frame) ~== #"destroyed",
         "You cannot map frame %= because it has been destroyed", frame);
  select (frame-state(frame))
    #"mapped" =>
      #f;
    #"detached" =>
      attach-frame(frame-manager(frame), frame);
      let top-sheet = top-level-sheet(frame);
      when (top-sheet)
	let geometry = frame-geometry(frame);
	let x      = geometry[0];
	let y      = geometry[1];
	let width  = geometry[2];
	let height = geometry[3];
	layout-frame(frame, width: width, height: height);
	// This uses 'handle-event' rather than 'distribute-event' because 
	// the user needs to see this before the event loop starts
	handle-event(event-handler(frame),
		     make(<frame-layed-out-event>, frame: frame));
	// Position the frame on the screen
	case 
	  frame-centered?(frame) =>
	    let (width, height) = frame-size(frame);
	    let (screen-width, screen-height) = sheet-size(display(frame));
	    let x = max(floor/(screen-width  - width,  2), 0);
	    let y = max(floor/(screen-height - height, 2), 0);
	    set-frame-position(frame, x, y);
	  x & y => 
	    set-frame-position(frame, x, y);
	  otherwise => #f;
	end
      end;
      map-frame(frame);
    #"unmapped" =>
      frame-state(frame) := #"mapped";
      note-frame-mapped(frame-manager(frame), frame);
  end
end method map-frame;
||#

(defmethod map-frame ((frame <basic-frame>))
  (when (eql (frame-state frame) :destroyed)
    (error "You cannot map frame ~a because it has been destroyed" frame))
  (ecase (frame-state frame)
    (:mapped nil)
    (:detached
     (attach-frame (frame-manager frame) frame)
     (let ((top-sheet (top-level-sheet frame)))
       (when top-sheet
         (let* ((geometry (frame-geometry frame))
                (x (aref geometry 0))
                (y (aref geometry 1))
                (width (aref geometry 2))
                (height (aref geometry 3)))
           (layout-frame frame :width width :height height)
           ;; This uses 'handle-event' rather than 'distribute-event' because 
           ;; the user needs to see this before the event loop starts
           (handle-event (event-handler frame)
			 (make-instance '<frame-layed-out-event> :frame frame))
           ;; Position the frame on the screen
           (cond ((frame-centered? frame)
                  (multiple-value-bind (width height)
		      (frame-size frame)
                    (multiple-value-bind (screen-width screen-height)
                        (sheet-size (display frame))
                      (let ((x (max (floor (- screen-width width) 2) 0))
                            (y (max (floor (- screen-height height) 2) 0)))
                        (set-frame-position frame x y)))))
                 ((and x y)
                  (set-frame-position frame x y))
                 (t nil)))))
     (map-frame frame))
    (:unmapped
     (setf (frame-state frame) :mapped)
     (note-frame-mapped (frame-manager frame) frame))))


#||
define method note-frame-mapped
    (framem :: <frame-manager>, frame :: <basic-frame>) => ()
  update-frame-settings(framem, frame);
  let top-sheet = top-level-sheet(frame);
  //--- Perhaps we want to resize the top level sheet if there is one
  when (top-sheet)
    sheet-mapped?(top-sheet) := #t
  end;
  distribute-event(port(frame), 
		   make(<frame-mapped-event>, frame: frame))
end method note-frame-mapped;
||#

(defmethod note-frame-mapped ((framem <frame-manager>) (frame <basic-frame>))
  (update-frame-settings framem frame)
  (let ((top-sheet (top-level-sheet frame)))
    ;;--- Perhaps we want to resize the top level sheet if there is one
    (when top-sheet
      (setf (sheet-mapped? top-sheet) t))
    (distribute-event (port frame)
		      (make-instance '<frame-mapped-event> :frame frame))))


#||
define method unmap-frame (frame :: <basic-frame>) => ()
  unless (frame-state(frame) == #"destroyed")
    select (frame-state(frame))
      #"detached", #"unmapped" => 
	// Nothing to do
	#f;
      #"mapped" =>
	frame-state(frame) := #"unmapped";
	note-frame-unmapped(frame-manager(frame), frame);
	// 'force-display' to ensure that the frame disappears immediately
	force-display(frame);
	let _port = port(frame);
	// Distribute an exit event from the top level sheet so that none
	// of the frame's sheets appear in the port's trace stack
	distribute-event(_port, make(<pointer-exit-event>,
				     sheet: top-level-sheet(frame),
				     x: -1, y: -1,
				     modifier-state: port-modifier-state(_port),
				     pointer: port-pointer(_port)));
	distribute-event(_port, make(<frame-unmapped-event>,
				     frame: frame))
    end
  end
end method unmap-frame;
||#

(defmethod unmap-frame ((frame <basic-frame>))
  (unless (eql (frame-state frame) :destroyed)
    (ecase (frame-state frame)
      ((:detached :unmapped)
       ;; Nothing to do
       nil)
      (:mapped
       (setf (frame-state frame) :unmapped)
       (note-frame-unmapped (frame-manager frame) frame)
       ;; 'force-display' to ensure that the frame disappears immediately
       (force-display frame)
       (let ((_port (port frame)))
         ;; Distribute an exit event from the top level sheet so that none
         ;; of the frame's sheets appear in the port's trace stack
         (distribute-event _port (make-instance '<pointer-exit-event>
						:sheet (top-level-sheet frame)
						:x -1 :y -1
						:modifier-state (port-modifier-state _port)
						:pointer (port-pointer _port)))
         (distribute-event _port (make-instance '<frame-unmapped-event>
						:frame frame)))))))


#||
define method note-frame-unmapped
    (framem :: <frame-manager>, frame :: <basic-frame>) => ()
  let top-sheet = top-level-sheet(frame);
  when (top-sheet)			// might be a pane-less frame
    sheet-mapped?(top-sheet) := #f
  end
end method note-frame-unmapped;
||#

(defmethod note-frame-unmapped ((framem <frame-manager>) (frame <basic-frame>))
  (let ((top-sheet (top-level-sheet frame)))
    (when top-sheet   ;; might be a pane-less frame
      (setf (sheet-mapped? top-sheet) nil))))


#||
// This is "sideways" because it is a forward reference from DUIM-Sheets.
define sideways method destroy-frame (frame :: <frame>) => ()
  unless (frame-state(frame) == #"destroyed")
    when (frame-mapped?(frame))
      frame-mapped?(frame) := #f
    end;
    // NB: copy the sequence because it will get changed from under us
    for (owned-frame in copy-sequence(frame-owned-frames(frame)))
      //--- Maybe use 'exit-frame'?
      destroy-frame(owned-frame)
    end;
    let top-sheet = top-level-sheet(frame);
    when (frame-manager(frame))		// just in case
      detach-frame(frame-manager(frame), frame)
    end;
    when (top-sheet)
      destroy-sheet(top-sheet)
    end;
    // Note that we use 'handle-event' instead of 'distribute-event',
    // because the event loop and event queue may well already be gone
    handle-event(event-handler(frame),
		 make(<frame-destroyed-event>, frame: frame));
    frame-state(frame) := #"destroyed"
  end
end method destroy-frame;
||#

(defmethod destroy-frame ((frame <frame>))
  (unless (eql (frame-state frame) :destroyed)
    (when (frame-mapped? frame)
      (setf (frame-mapped? frame) nil))
    ;; NB: copy the sequence because it will get changed from under us
    (let ((owned-frames (SEQUENCE-COPY (frame-owned-frames frame))))
      (loop for owned-frame #||in||# across owned-frames
	    do (destroy-frame owned-frame)))
    (let ((top-sheet (top-level-sheet frame)))
      (when (frame-manager frame)   ;; just in case
	;; minimises but does not unmap...
	(detach-frame (frame-manager frame) frame))
      (when top-sheet
	(destroy-sheet top-sheet)))
    ;; Note that we use 'handle-event' instead of 'distribute-event',
    ;; because the event loop and event queue may well already be gone
    (handle-event (event-handler frame)
		  (make-instance '<frame-destroyed-event> :frame frame))
    (setf (frame-state frame) :destroyed)))


#||
define method raise-frame
    (frame :: <frame>, #key activate? = #t) => ()
  let top-sheet = top-level-sheet(frame);
  assert(top-sheet & sheet-mapped?(top-sheet),
         "Attempted to raise %=, which isn't mapped",
         frame);
  raise-sheet(top-sheet, activate?: activate?)
end method raise-frame;
||#

(defmethod raise-frame ((frame <frame>) &key (activate? t))
  (let ((top-sheet (top-level-sheet frame)))
    (unless (and top-sheet (sheet-mapped? top-sheet))
      (error "Attempted to raise ~a, which isn't mapped" frame))
    (raise-sheet top-sheet :activate? activate?)))


#||
define method lower-frame
    (frame :: <frame>) => ()
  let top-sheet = top-level-sheet(frame);
  assert(top-sheet & sheet-mapped?(top-sheet),
         "Attempted to lower %=, which isn't mapped",
         frame);
  lower-sheet(top-sheet)
end method lower-frame;
||#

(defmethod lower-frame ((frame <frame>))
  (let ((top-sheet (top-level-sheet frame)))
    (unless (and top-sheet (sheet-mapped? top-sheet))
      (error "Attempted to lower ~a, which isn't mapped" frame))
    (lower-sheet top-sheet)))


#||
define method frame-occluded?
    (frame :: <frame>) => (occluded? :: <boolean>)
  let framem = frame-manager(frame);
  // If it's not attached to a frame manager, just pretend it's occluded
  ~framem | do-frame-occluded?(framem, frame)
end method frame-occluded?;
||#

(defmethod frame-occluded? ((frame <frame>))
  (let ((framem (frame-manager frame)))
    ;; If it's not attached to a frame manager, just pretend it's occluded
    (or (not framem)
	(do-frame-occluded? framem frame))))


#||
define method do-frame-occluded?
    (framem :: <frame-manager>, frame :: <frame>)
 => (occluded? :: <boolean>)
  #f
end method do-frame-occluded?;
||#

(defmethod do-frame-occluded? ((framem <frame-manager>) (frame <frame>))
  nil)


#||
define method update-frame-settings
    (framem :: <frame-manager>, frame :: <frame>) => ()
  ignore(frame);
  #f
end method update-frame-settings;
||#

(defmethod update-frame-settings ((framem <frame-manager>) (frame <frame>))
  (declare (ignore frame))
  nil)


#||
// For compatibility...
define sealed method iconify-frame (frame :: <frame>) => ()
  frame-iconified?(frame) := #t
end method iconify-frame;
||#

;; For compatibility...
(defmethod iconify-frame ((frame <frame>))
  (setf (frame-iconified? frame) t))


#||
define sealed method deiconify-frame (frame :: <frame>) => ()
  frame-iconified?(frame) := #f
end method deiconify-frame;
||#

(defmethod deiconify-frame ((frame <frame>))
  (setf (frame-iconified? frame) nil))


#||
define sealed method maximize-frame (frame :: <frame>) => ()
  frame-maximized?(frame) := #t
end method maximize-frame;
||#

(defmethod maximize-frame ((frame <frame>))
  (setf (frame-maximized? frame) t))


#||
define sealed method unmaximize-frame (frame :: <frame>) => ()
  frame-maximized?(frame) := #f
end method unmaximize-frame;
||#

(defmethod unmaximize-frame ((frame <frame>))
  (setf (frame-maximized? frame) nil))


#||
define method beep (frame :: <basic-frame>) => ();
  let _port = port(frame);
  _port & beep(_port)
end method beep;
||#

(defmethod beep ((frame <basic-frame>))
  (let ((_port (port frame)))
    (and _port (beep _port))))


#||
define method force-display (frame :: <basic-frame>) => ()
  let _port = port(frame);
  _port & force-display(_port)
end method force-display;
||#

(defmethod force-display ((frame <basic-frame>))
  (let ((_port (port frame)))
    (and _port (force-display _port))))


#||
define method synchronize-display (frame :: <basic-frame>) => ()
  let _port = port(frame);
  _port & synchronize-display(_port)
end method synchronize-display;
||#

(defmethod synchronize-display ((frame <basic-frame>))
  (let ((_port (port frame)))
    (and _port (synchronize-display _port))))



#||
/// Frame event distribution and queueing

define method do-dispatch-event
    (frame :: <basic-frame>, event :: <event>) => ()
  queue-event(frame, event)
end method do-dispatch-event;
||#

(defmethod do-dispatch-event ((frame <basic-frame>) (event <event>))
  (queue-event frame event))


#||
define method queue-event
    (frame :: <basic-frame>, event :: <event>) => ()
  // Push the new event onto the tail of the event queue,
  // or handle the event directly if there is no queue
  let queue = frame-event-queue(frame);
  if (queue)
    event-queue-push-last(queue, event)
  else
    handle-event(event-handler(frame), event)
  end
end method queue-event;
||#

(defmethod queue-event ((frame <basic-frame>) (event <event>))
"
Push the new event onto the tail of the event queue,
or handle the event directly if there is no queue.
"
  (let ((queue (frame-event-queue frame)))
    (if queue
	(event-queue-push-last queue event)
	(handle-event (event-handler frame) event))))


#||
// Returns #t if this frame needs its very own event queue
define method frame-needs-event-queue?
    (framem :: <frame-manager>, frame :: <frame>, #key mode)
 => (needs-event-queue? :: <boolean>)
  select (mode | frame-mode(frame))
    #"modeless" =>
      // Modeless frames get their own event queue only when they have no owner
      ~(frame-owner(frame) & port-event-processor-type(port(framem)) = #"n");
    #"modal", #"system-modal" =>
      // Modal frames always get their own event queue
      #t;
  end
end method frame-needs-event-queue?;
||#

(defmethod frame-needs-event-queue? ((framem <frame-manager>) (frame <frame>) &key mode)
"
Returns NIL if events for `frame` are handled on its behalf by an
owning frame.
"
  (case (or mode (frame-mode frame))
    (:modeless
     ;; Modeless frames get their own event queue only when they have no owner
     (not (and (frame-owner frame) (eql (port-event-processor-type (port framem)) :n))))
    ((:modal :system-modal)
     ;; Modal frames always get their own event queue
     t)))


#||
define method make-event-queue
    (framem :: <frame-manager>, frame :: <frame>)
 => (event-queue :: <event-queue>)
  make(<event-queue>)
end method make-event-queue;
||#

(defmethod make-event-queue ((framem <frame-manager>) (frame <frame>))
  (make-instance '<event-queue>))



#||
/// Thread handling

// Users can use these functions to make a function call in the frame's
// thread. If calling from a different thread, it queues a function event
// onto the frame's event loop.

define inline function in-frame-thread?
    (frame :: <frame>) => (in-frame-thread? :: <boolean>)
  current-thread() == frame-thread(frame)
end function in-frame-thread?;
||#

(defmethod in-frame-thread? ((frame <frame>))
  (eql (bordeaux-threads:current-thread) (frame-thread frame)))


#||
define method queue-call-in-frame
    (frame :: <frame>, function :: <function>, #rest args) => ()
  let _function = method () apply(function, args) end;
  distribute-function-event(frame, _function)
end method queue-call-in-frame;
||#

(defmethod queue-call-in-frame ((frame <frame>) (function function) &rest args)
  (let ((_function #'(lambda () (apply function args))))
    (distribute-function-event frame _function)))


#||
define method queue-apply-in-frame
    (frame :: <frame>, function :: <function>, arg, #rest more-args) => ()
  let _function = method () apply(apply, function, arg, more-args) end;
  distribute-function-event(frame, _function)
end method queue-apply-in-frame;
||#

(defmethod queue-apply-in-frame ((frame <frame>) (function function) arg &rest more-args)
  (let ((_function #'(lambda () (apply #'apply function arg more-args))))
    (distribute-function-event frame _function)))


#||
define method call-in-frame
    (frame :: <frame>, function :: <function>, #rest args) => ()
  if (in-frame-thread?(frame))
    apply(function, args)
  else
    let _function = method () apply(function, args) end;
    distribute-function-event(frame, _function)
  end
end method call-in-frame;
||#

(defmethod call-in-frame ((frame <frame>) (function function) &rest args)
  (if (in-frame-thread? frame)
      (apply function args)
      (let ((_function #'(lambda () (apply function args))))
	(distribute-function-event frame _function))))


#||
define method apply-in-frame
    (frame :: <frame>, function :: <function>, arg, #rest more-args) => ()
  if (in-frame-thread?(frame))
    apply(apply, function, arg, more-args)
  else
    let _function = method () apply(apply, function, arg, more-args) end;
    distribute-function-event(frame, _function)
  end
end method apply-in-frame;
||#

(defmethod apply-in-frame ((frame <frame>) (function function) arg &rest more-args)
  (if (in-frame-thread? frame)
      (apply #'apply function arg more-args)
      (let ((_function #'(lambda () (apply #'apply function arg more-args))))
	(distribute-function-event frame _function))))



#||
/// Starting frames

// Realizes and displays the frame's UI, then starts the frame's event loop.
// Note that when 'start-frame' is called on modeless, owned frames, it
// will return immediately with a single value of #f
define method start-frame
    (frame :: <simple-frame>)
 => (status-code :: false-or(<integer>))
  assert(frame-state(frame) ~== #"destroyed",
         "You cannot start frame %= because it has been destroyed", frame);
  port-start-frame(port(frame), frame)
end method start-frame;
||#

(defmethod start-frame ((frame <simple-frame>))
"
Realizes and displays the frame's UI, then starts the frame's event loop.
Note that when 'START-FRAME' is called on modeless, owned frames, it
will return immediately with a single value of NIL.
"
  (when (eql (frame-state frame) :destroyed)
    (error "You cannot start frame ~a because it has been destroyed" frame))
  (port-start-frame (port frame) frame))


#||
define method port-start-frame
    (_port :: <port>, frame :: <simple-frame>)
 => (status-code :: false-or(<integer>))
  block (return)
    let framem = frame-manager(frame);
    dynamic-bind (*current-frame* = frame)
      // Map the frame now if it's not already mapped
      unless (frame-mapped?(frame))
        frame-mapped?(frame) := #t
      end;
      // If we are starting an "owned" frame, we need to be careful with
      // multiple threads -- in an "in thread" event processor, we mustn't
      // start a new event loop since we already have one. 
      if (frame-needs-event-queue?(framem, frame, mode: frame-mode(frame)))
	block ()
	  let status-code = execute-frame-top-level(frame);
	  return(status-code);
	cleanup
	  note-frame-top-level-finished(frame)
	end
      else
	// Modeless, owned frame sharing a thread and event queue!
	// Just return #f
        return(#f)
      end
    end
  end
end method port-start-frame;
||#

(defmethod port-start-frame ((_port <port>) (frame <simple-frame>))
  (let ((framem (frame-manager frame)))
    (dynamic-let ((*current-frame* frame))
      ;; Map the frame now if it's not already mapped
      (unless (frame-mapped? frame)
        (setf (frame-mapped? frame) t))
      ;; If we are starting an "owned" frame, we need to be careful with
      ;; multiple threads -- in an "in thread" event processor, we mustn't
      ;; start a new event loop since we already have one.
      (if (frame-needs-event-queue? framem frame :mode (frame-mode frame))
	  (unwind-protect
	       (let ((status-code (execute-frame-top-level frame)))
		 (return-from port-start-frame status-code))
	    (note-frame-top-level-finished frame))
	  ;; Modeless, owned frame sharing a thread and event queue!
	  ;; Just return #f
	  nil))))


#||
define method execute-frame-top-level
    (frame :: <simple-frame>)
 => (status-code :: false-or(<integer>))
  duim-debug-message("Starting up frame %=", frame);
  when (frame-thread(frame))
    cerror("Bludgeon ahead, assuming the risk",
	   "The thread %= is already running the top-level function for frame %=",
	   frame-thread(frame), frame)
  end;
  frame-thread(frame) := current-thread();
  block (return)
    // Set up the exit function for use by the exit event handler
    frame.%exit-function := return;
    duim-debug-message("Starting top level loop for frame %=", frame);
    with-abort-restart-loop ("Exit frame %s",
			     frame-title(frame) | "Unnamed frame")
      // This will return when a <frame-exited-event> comes in
      frame-top-level(frame)
    end;
  cleanup
    duim-debug-message("Finished top level loop for frame %=", frame);
  end
end method execute-frame-top-level;
||#

(defmethod execute-frame-top-level ((frame <simple-frame>))
  (when (frame-thread frame)
    (cerror "Bludgeon ahead, assuming the risk"
	    "The thread ~a is already running the top-level function for frame ~a; about to change frame-thread to ~a"
	    (frame-thread frame) frame (bordeaux-threads:current-thread)))
  (setf (frame-thread frame) (bordeaux-threads:current-thread))
  ;; Set up the exit function for use by the exit event handler
  (unwind-protect
       (block exit-duim-frame
	 (setf (%exit-function frame)
	       #'(lambda (&optional result)
		   (return-from exit-duim-frame (or result t))))
	 (duim-debug-message "======= Starting top level loop for frame ~A" frame)
	 (with-abort-restart-loop ("Exit frame ~s" frame)
	   ;; This will return when a <frame-exited-event> comes in
	   (frame-top-level frame)))
    ;; cleanup
    (duim-debug-message "======= Finished top level loop for frame ~A" frame)))


;;; TODO: Where should the documentation for 'execute-command command frame' go?
#||
Executes _command_ for _frame_. The values returned are those values
returned as a result of evaluating the command function of _command_.
||#

#||
//--- Frames really need to have a first-class "command loop" object
//---  - It implements 'read-command', 'do-execute-command', and 'redisplay'
//---  - It has notification protocols to install/deinstall menu bars, etc.
define method frame-top-level
    (frame :: <simple-frame>) => (#rest values)
  // The "read-eval-print" loop for event-driven applications...
  let top-sheet = top-level-sheet(frame);
  assert(top-sheet,
	 "The frame %= does not have a top-level sheet after it was started", frame);
  while (#t)
    // Get an event and then handle it.  Note that the user
    // program is responsible for managing its own redisplay.
    let event  = read-event(top-sheet);
    let client = event-client(event);
    unless (instance?(event, <pointer-motion-event>))
      duim-debug-message("  Handling event %= for %=", event, client)
    end;
    // Note that if a <frame-exited-event> comes in, the
    // event handler for it will exit this loop by calling the
    // frame.%exit-function thunk
    handle-event(event-handler(client), event)
  end
end method frame-top-level;
||#

;;--- Frames really need to have a first-class "command loop" object
;;---  - It implements 'read-command', 'do-execute-command', and 'redisplay'
;;---  - It has notification protocols to install/deinstall menu bars, etc.
(defmethod frame-top-level ((frame <simple-frame>))
  ;; The "read-eval-print" loop for event-driven applications...
  (let ((top-sheet (top-level-sheet frame)))
    (unless top-sheet
      (error "The frame ~a does not have a top-level sheet after it was started" frame))
    (loop while t
       ;; Get an event and then handle it.  Note that the user
       ;; program is responsible for managing its own redisplay.
       do (let* ((event  (read-event top-sheet))
		 (client (event-client event)))
	    (unless (INSTANCE? event '<pointer-motion-event>)
	      (duim-debug-message "    Handling event ~a for ~a" event client))
	    ;; Note that if a <frame-exited-event> comes in, the
	    ;; event handler for it will exit this loop by calling the
	    ;; frame.%exit-function thunk
	    (handle-event (event-handler client) event)))))


#||
define method note-frame-top-level-finished
    (frame :: <simple-frame>) => ()
  event-queue-clear(frame-command-queue(frame));
  when (top-level-sheet(frame))		// some hackers do this themselves
    let queue = sheet-event-queue(top-level-sheet(frame));
    when (queue)
      event-queue-clear(queue)
    end
  end;
  frame-thread(frame)  := #f
end method note-frame-top-level-finished;
||#

(defmethod note-frame-top-level-finished ((frame <simple-frame>))
  (event-queue-clear (frame-command-queue frame))
  (when (top-level-sheet frame)   ;; some hackers do this themselves
    (let ((queue (sheet-event-queue (top-level-sheet frame))))
      (when queue
	(event-queue-clear queue))))
  (setf (frame-thread frame) nil))


#||
/// Stopping frames

define method exit-frame
    (frame :: <frame>, #key destroy? = #t) => ()
  let _port = port(frame);
  when (_port)
    distribute-event(_port, make(<frame-exit-event>,
				 frame: frame,
				 destroy-frame?: destroy?));
    let top-sheet = top-level-sheet(frame);
    when (top-sheet)
      generate-trigger-event(_port, top-sheet)
    end
  end
end method exit-frame;
||#

(defmethod exit-frame ((frame <frame>) &key (destroy? t))
"
Distribute a <frame-exit-event> then generate a trigger
event.

Called by the back end in response to a DELETE-EVENT.
"
  (let ((_port (port frame)))
    (when _port
      (distribute-event _port (make-instance '<frame-exit-event>
					     :frame frame
					     :destroy-frame? destroy?))
      (let ((top-sheet (top-level-sheet frame)))
	(when top-sheet
	  (generate-trigger-event _port top-sheet))))))


#||
define method exit-frame
    (sheet :: <sheet>, #key destroy? = #t) => ()
  let frame = sheet-frame(sheet);
  exit-frame(frame, destroy?: destroy?)
end method exit-frame;
||#

(defmethod exit-frame ((sheet <sheet>) &key (destroy? t))
  (let ((frame (sheet-frame sheet)))
    (exit-frame frame :destroy? destroy?)))


#||
// Intended to be specialized by users
define method frame-can-exit? 
    (frame :: <frame>) => (can-exit? :: <boolean>)
  #t
end method frame-can-exit?;
||#

;; Intended to be specialized by users
(defmethod frame-can-exit? ((frame <frame>))
  t)


#||
define method handle-event
    (frame :: <frame>, event :: <frame-exit-event>) => ()
  when (frame-can-exit?(frame))
    do-exit-frame(frame-manager(frame), frame, destroy?: event-destroy-frame?(event))
  end
end method handle-event;
||#

(defmethod handle-event ((frame <frame>) (event <frame-exit-event>))
  (when (frame-can-exit? frame)
    (do-exit-frame (frame-manager frame) frame :destroy? (event-destroy-frame? event))))


#||
define open generic do-exit-frame 
    (framem :: <abstract-frame-manager>, frame :: <abstract-frame>,
     #key status-code, destroy?) => ();
||#

(defgeneric do-exit-frame (framem frame &key status-code destroy?))


#||
define method do-exit-frame
    (framem :: <frame-manager>, frame :: <frame>,
     #key status-code, destroy? = #t) => ()
  // First get rid of the windows
  if (destroy?)
    // 'destroy-frame' will arrange to handle a <frame-destroyed-event>...
    destroy-frame(frame)
  else
    frame-mapped?(frame) := #f
  end;
  // Now we can claim that the frame has exited
  // Note that we use 'handle-event' instead of 'distribute-event',
  // because the event loop and event queue may well already be gone
  handle-event(event-handler(frame),
	       make(<frame-exited-event>,
		    frame: frame,
		    status-code: status-code))
end method do-exit-frame;
||#

(defmethod do-exit-frame ((framem <frame-manager>) (frame <frame>) &key status-code (destroy? t))
  ;; First get rid of the windows
  (if destroy?
      ;; 'destroy-frame' will arrange to handle a <frame-destroyed-event>...
      (destroy-frame frame)
      (setf (frame-mapped? frame) nil))
  ;; Now we can claim that the frame has exited
  ;; Note that we use 'handle-event' instead of 'distribute-event',
  ;; because the event loop and event queue may well already be gone
  (handle-event (event-handler frame)
		(make-instance '<frame-exited-event>
			       :frame frame
			       :status-code status-code)))


#||
define method handle-event
    (frame :: <frame>, event :: <frame-exited-event>) => ()
  let exit-function = frame.%exit-function;
  when (exit-function)
    frame.%exit-function := #f;
    duim-debug-message("Shutting down event loop for frame %=", frame);
    exit-function(event-status-code(event))
  end
end method handle-event;
||#

(defmethod handle-event :around ((frame <frame>) (event <frame-exited-event>))
  (call-next-method)
  (let ((exit-function (%exit-function frame)))
    (when exit-function
      (setf (%exit-function frame) nil)
      (funcall exit-function (event-status-code event)))))



#||
/// Pointer documentation

define method display-pointer-documentation
    (frame :: <frame>, sheet :: false-or(<sheet>),
     string :: false-or(<string>)) => ()
  let framem = frame-manager(frame) | find-frame-manager();
  do-display-pointer-documentation(framem, frame, sheet, string)
end method display-pointer-documentation;
||#

(defmethod display-pointer-documentation ((frame <frame>) (sheet null) (string null))
  (let ((framem (or (frame-manager frame) (find-frame-manager))))
    (do-display-pointer-documentation framem frame sheet string)))

(defmethod display-pointer-documentation ((frame <frame>) (sheet null) (string string))
  (let ((framem (or (frame-manager frame) (find-frame-manager))))
    (do-display-pointer-documentation framem frame sheet string)))

(defmethod display-pointer-documentation ((frame <frame>) (sheet <sheet>) (string null))
  (let ((framem (or (frame-manager frame) (find-frame-manager))))
    (do-display-pointer-documentation framem frame sheet string)))

(defmethod display-pointer-documentation ((frame <frame>) (sheet <sheet>) (string string))
  (let ((framem (or (frame-manager frame) (find-frame-manager))))
    (do-display-pointer-documentation framem frame sheet string)))


#||
define method do-display-pointer-documentation
    (framem :: <frame-manager>, frame :: <frame>, sheet :: false-or(<sheet>),
     string :: false-or(<string>)) => ()
  let sheet = pointer-documentation-sheet(framem, frame, sheet);
  when (sheet)
    with-sheet-medium (medium = sheet)
      clear-box*(medium, sheet-viewport-region(sheet));
      when (string)
	draw-text(medium, string, 0, 0,
		  align-x: #"left", align-y: #"top")
      end
    end
  end
end method do-display-pointer-documentation;
||#

(defmethod do-display-pointer-documentation ((framem <frame-manager>) (frame <frame>) (sheet null) (string null))
  (let ((sheet (pointer-documentation-sheet framem frame sheet)))
    (when sheet
      (with-sheet-medium (medium = sheet)
        (clear-box* medium (sheet-viewport-region sheet))
	(when string
	  (draw-text medium string 0 0
		     :align-x :left :align-y :top))))))

(defmethod do-display-pointer-documentation ((framem <frame-manager>) (frame <frame>) (sheet null) (string string))
  (let ((sheet (pointer-documentation-sheet framem frame sheet)))
    (when sheet
      (with-sheet-medium (medium = sheet)
        (clear-box* medium (sheet-viewport-region sheet))
	(when string
	  (draw-text medium string 0 0
		     :align-x :left :align-y :top))))))

(defmethod do-display-pointer-documentation ((framem <frame-manager>) (frame <frame>) (sheet <sheet>) (string null))
  (let ((sheet (pointer-documentation-sheet framem frame sheet)))
    (when sheet
      (with-sheet-medium (medium = sheet)
        (clear-box* medium (sheet-viewport-region sheet))
	(when string
	  (draw-text medium string 0 0
		     :align-x :left :align-y :top))))))

(defmethod do-display-pointer-documentation ((framem <frame-manager>) (frame <frame>) (sheet <sheet>) (string string))
  (let ((sheet (pointer-documentation-sheet framem frame sheet)))
    (when sheet
      (with-sheet-medium (medium = sheet)
        (clear-box* medium (sheet-viewport-region sheet))
	(when string
	  (draw-text medium string 0 0
		     :align-x :left :align-y :top))))))


#||
// This method allows a frame to redirect the pointer documentation
define method pointer-documentation-sheet
    (framem :: <frame-manager>, frame :: <frame>, sheet :: false-or(<sheet>))
 => (sheet :: false-or(<sheet>))
  sheet
end method pointer-documentation-sheet;
||#

(defmethod pointer-documentation-sheet ((framem <frame-manager>) (frame <frame>) (sheet null))
  sheet)

(defmethod pointer-documentation-sheet ((framem <frame-manager>) (frame <frame>) (sheet <sheet>))
  sheet)


#||
/*
///---*** This all belongs in the presentation layer

define variable *pointer-documentation-interval*
    = max(truncate($internal-time-units-per-second * 0.10), 1);

define variable *last-pointer-documentation-time* = 0;

define variable *last-pointer-documentation-modifier-state* = 0;

define method document-highlighted-presentation
    (frame :: <frame>, sheet :: false-or(<sheet>),
     presentation, input-context, window, x, y) => ()
  let framem = frame-manager(frame) | find-frame-manager();
  display-presentation-documentation
    (framem, frame, sheet, presentation, input-context, window, x, y)
end method document-highlighted-presentation;

define method display-presentation-documentation
    (framem :: <frame-manager>, frame :: <frame>, sheet :: false-or(<sheet>),
     presentation, input-context, window, x, y) => ()
  ignore(input-context, x, y);
  block (return)
    let framem = frame-manager(frame) | find-frame-manager();
    when (pointer-documentation-sheet(framem, frame, sheet))
      // The documentation should never say anything if we're not over a presentation
      unless (presentation)
        display-pointer-documentation(frame, sheet, #f)
      end;
      // Cheap test to not do this work too often
      let old-modifier-state = *last-pointer-documentation-modifier-state*;
      let modifier-state = port-modifier-state(port(window));
      let last-time = *last-pointer-documentation-time*;
      let time = get-internal-real-time();
      *last-pointer-documentation-modifier-state* := modifier-state;
      when (time < last-time + *pointer-documentation-interval*
	    & modifier-state = old-modifier-state)
	return(#f)
      end;
      *last-pointer-documentation-time* := time;
      //---*** See frame-manager-display-pointer-documentation
      //---*** See frame-document-highlighted-presentation-1
      //---*** See find-applicable-translators-for-documentation
      do-display-presentation-documentation
        (framem, frame, sheet, presentation, input-context, window, x, y)
    end
  end
end method display-presentation-documentation;
*/
||#

