;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-FRAMES-INTERNALS -*-
(in-package #:duim-frames-internals)

#||
/// Containers

define protocol <<contain-protocol>> ()
  function contain
    (object, #key, #all-keys) => (object, frame :: <frame>);
  function make-container
    (object, #key, #all-keys) => (frame :: <frame>);
end protocol <<contain-protocol>>;
||#
#||
(define-protocol <<contain-protocol>> ()
  (:function contain (object &key &allow-other-keys))
  (:function make-container (object &key &allow-other-keys)))
||#
#||
define variable *contain-uses-own-thread?* :: <boolean> = #f;
||#

(defparameter *contain-uses-own-thread?* nil)


#||
define method contain
    (pane, #rest initargs, #key own-thread? = *contain-uses-own-thread?*, #all-keys)
 => (pane :: <sheet>, frame :: <frame>) 
  dynamic-extent(initargs);
  with-keywords-removed (initargs = initargs, #[owner:])
    let container = apply(make-container, pane, initargs);
    local method start-container-frame () => ()
	    with-abort-restart ()
	      start-frame(container)
	    end
	  end method;
    if (own-thread?)
      make(<thread>,
	   name: frame-title(container),
	   function: start-container-frame)
    else
      start-container-frame()
    end;
    values(pane, container)
  end
end method contain;
||#

(defmethod contain (pane &rest initargs &key (own-thread? *contain-uses-own-thread?*) &allow-other-keys)
  (declare (dynamic-extent initargs))
  (with-keywords-removed (initargs = initargs (:owner))
    (let ((container (apply #'make-container pane initargs)))
      (labels ((start-container-frame () (with-abort-restart ()
					   (start-frame container))))
	(if own-thread?
	    (bordeaux-threads:make-thread #'start-container-frame :name (frame-title container))
	    (start-container-frame))
	(values pane container)))))


#||
define method contain
    (class :: <class>, #rest initargs, #key)
 => (pane :: <sheet>, frame :: <frame>)
  dynamic-extent(initargs);
  apply(contain, make(class), initargs)
end method contain;
||#

(defmethod contain ((class standard-class) &rest initargs &key)
  (declare (dynamic-extent initargs))
  (apply #'contain (make-pane class) initargs))


#||
define method make-container
    (frame :: <frame>, #rest initargs, #key) => (frame :: <frame>)
  ignore(initargs);
  frame
end method make-container;
||#

(defmethod make-container ((frame <frame>) &rest initargs &key)
  (declare (ignore initargs))
  frame)


#||
/// Container frames

define sealed class <container-frame> (<simple-frame>)
  sealed constant slot container-uses-own-thread? :: <boolean> = *contain-uses-own-thread?*,
    init-keyword: own-thread?:;
end class <container-frame>;

define sealed domain make (singleton(<container-frame>));
define sealed domain initialize (<container-frame>);
||#

(defclass <container-frame> (<simple-frame>)
  ((container-uses-own-thread? :type boolean :initarg :own-thread?
			       :initform *contain-uses-own-thread?*
			       :reader container-uses-own-thread?))
  (:documentation
"
A <FRAME> subclass for 'contain'ed widgets.
"))


#||
// Wraps an object into a container frame
define method make-container-frame
    (object, #rest initargs,
     #key own-thread? = *contain-uses-own-thread?*, #all-keys) => (frame :: <frame>)
  dynamic-extent(initargs);
  apply(make, <container-frame>, title: "Container", initargs)
end method make-container-frame;
||#

(defgeneric make-container-frame (object &rest initargs &key own-thread? &allow-other-keys))

(defmethod make-container-frame (object &rest initargs &key (own-thread? *contain-uses-own-thread?*) &allow-other-keys)
  (declare (dynamic-extent initargs)
	   (ignore object own-thread?))
  (apply #'make-instance '<container-frame> :title "Container" initargs))



#||
/// General containers

define method make-container
    (layout :: <layout-pane>, #rest initargs, #key) => (frame :: <frame>)
  dynamic-extent(initargs);
  apply(make-container-frame, layout, layout: layout, initargs)
end method make-container;
||#

(defmethod make-container ((layout <layout-pane>) &rest initargs &key)
  (declare (dynamic-extent initargs))
  (apply #'make-container-frame layout :layout layout initargs))


#||
define method make-container 
    (sheet :: <sheet>, #rest initargs, #key) => (frame :: <frame>)
  dynamic-extent(initargs);
  apply(make-container, 
        make(<column-layout>, children: vector(sheet)),
        initargs)
end method make-container;
||#

(defmethod make-container ((sheet <sheet>) &rest initargs &key)
  (declare (dynamic-extent initargs))
  (apply #'make-container
	 (make-pane '<column-layout> :children (vector sheet))
	 initargs))



#||
/// Menu containers

define method make-container 
    (menu-bar :: <menu-bar>, #rest initargs, #key) => (frame :: <frame>)
  dynamic-extent(initargs);
  apply(make-container-frame, menu-bar, menu-bar: menu-bar, initargs)
end method make-container;
||#

(defmethod make-container ((menu-bar <menu-bar>) &rest initargs &key)
  (declare (dynamic-extent initargs))
  (apply #'make-container-frame menu-bar :menu-bar menu-bar initargs))


#||
define method make-container
    (menu :: <menu>, #rest initargs, #key) => (frame :: <frame>)
  dynamic-extent(initargs);
  apply(make-container, make(<menu-bar>, children: vector(menu)), initargs)
end method make-container;
||#

(defmethod make-container ((menu <menu>) &rest initargs &key)
  (declare (dynamic-extent initargs))
  (apply #'make-container
	 (make-pane '<menu-bar> :children (vector menu))
	 initargs))


#||
define method make-container
    (component :: <menu-box>, #rest initargs, #key) => (frame :: <frame>)
  dynamic-extent(initargs);
  apply(make-container, 
        make(<menu>, label: "Menu", children: vector(component)), initargs)
end method make-container;
||#

(defmethod make-container ((component <menu-box>) &rest initargs &key)
  (declare (dynamic-extent initargs))
  (apply #'make-container
	 (make-pane '<menu> :label "Menu" :children (vector component))
	 initargs))


#||
define method make-container 
    (button :: <menu-button>, #rest initargs, #key) => (frame :: <frame>)
  dynamic-extent(initargs);
  apply(make-container, 
        make(<menu>, label: "Menu", children: vector(button)), initargs)
end method make-container;
||#

(defmethod make-container ((button <menu-button>) &rest initargs &key)
  (declare (dynamic-extent initargs))
  (apply #'make-container
	 (make-pane '<menu> :label "Menu" :children (vector button))
	 initargs))


#||
/// Command table containers

define method make-container 
    (command-table :: <command-table>, #rest initargs, #key) => (frame :: <frame>)
  dynamic-extent(initargs);
  apply(make-container-frame, command-table, 
        command-table: command-table,
        initargs)
end method make-container;
||#

(defmethod make-container ((command-table <command-table>) &rest initargs &key)
  (declare (dynamic-extent initargs))
  (apply #'make-container-frame command-table
	 :command-table command-table
	 initargs))


#||
/// Status bar and tool bar containers

define method make-container 
    (status-bar :: <status-bar>, #rest initargs, #key) => (frame :: <frame>)
  dynamic-extent(initargs);
  apply(make-container-frame, status-bar, status-bar: status-bar, initargs)
end method make-container;
||#

(defmethod make-container ((status-bar <status-bar>) &rest initargs &key)
  (declare (dynamic-extent initargs))
  (apply #'make-container-frame status-bar :status-bar status-bar initargs))


#||
define method make-container 
    (tool-bar :: <tool-bar>, #rest initargs, #key) => (frame :: <frame>)
  dynamic-extent(initargs);
  apply(make-container-frame, tool-bar, tool-bar: tool-bar, initargs)
end method make-container;
||#

(defmethod make-container ((tool-bar <tool-bar>) &rest initargs &key)
  (declare (dynamic-extent initargs))
  (apply #'make-container-frame tool-bar :tool-bar tool-bar initargs))


