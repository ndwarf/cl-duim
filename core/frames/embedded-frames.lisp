;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-FRAMES-INTERNALS -*-
(in-package #:duim-frames-internals)

#||
/// Embedded frames

define open abstract primary class <basic-embedded-frame> (<basic-frame>)
  sealed slot %layout :: false-or(<sheet>) = #f,
    init-keyword: layout:;
end class <basic-embedded-frame>;
||#


(defclass <basic-embedded-frame> (<basic-frame>)
  ((%layout :type (or null <sheet>) :initarg :layout :initform nil :accessor %layout))
  (:documentation
"
Embedded frames provide support for things like OLE embedded objects. Frame
decorations are provided by the embeddING frame, the embeddED frame has none.
")
  (:metaclass <abstract-metaclass>))


#||
define method frame-menu-bar
    (frame :: <basic-embedded-frame>) => (menu-bar :: singleton(#f))
  #f
end method frame-menu-bar;
||#

(defmethod frame-menu-bar ((frame <basic-embedded-frame>))
  nil)


#||
define method frame-tool-bar
    (frame :: <basic-embedded-frame>) => (menu-bar :: singleton(#f))
  #f
end method frame-tool-bar;
||#

(defmethod frame-tool-bar ((frame <basic-embedded-frame>))
  nil)


#||
define method frame-status-bar
    (frame :: <basic-embedded-frame>) => (menu-bar :: singleton(#f))
  #f
end method frame-status-bar;
||#

(defmethod frame-status-bar ((frame <basic-embedded-frame>))
  nil)


#||
define open abstract class <embedded-top-level-sheet>
    (<top-level-sheet>)
end class <embedded-top-level-sheet>;
||#


(defclass <embedded-top-level-sheet>
    (<top-level-sheet>)
  ()
  (:metaclass <abstract-metaclass>))


#||
define method frame-top-level-sheet-class
    (frame :: <basic-embedded-frame>, #key) => (class :: <class>)
  <embedded-top-level-sheet>
end method frame-top-level-sheet-class;
||#

(defmethod frame-top-level-sheet-class ((frame <basic-embedded-frame>) &key)
  (find-class '<embedded-top-level-sheet>))


#||
// Embedded frames don't have any decorations
define method frame-wrapper
    (framem :: <frame-manager>, frame :: <basic-embedded-frame>, sheet :: false-or(<sheet>))
 => (wrapper :: false-or(<sheet>))
  sheet
end method frame-wrapper;
||#

(defmethod frame-wrapper ((framem <frame-manager>) (frame <basic-embedded-frame>) (sheet null))
  sheet)

(defmethod frame-wrapper ((framem <frame-manager>) (frame <basic-embedded-frame>) (sheet <sheet>))
  sheet)

