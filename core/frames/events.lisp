;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-FRAMES-INTERNALS -*-
(in-package #:duim-frames-internals)

#||
/// Frame events

// This event is the first event a frame will see
// It gets put at the head of the event queue, before any other events
define sealed class <frame-created-event> (<frame-event>)
end class <frame-created-event>;
||#

(defclass <frame-created-event> (<frame-event>) ()
  (:documentation
"
The class of events that indicate a frame has been created. An
instance of this class is distributed to the frame when it is
created. Only one of these events is passed during the lifetime of any
frame.
"))


#||
// This event gets sent when the UI for a frame gets destroyed by
// a call to 'destroy-frame'
define sealed class <frame-destroyed-event> (<frame-event>)
end class <frame-destroyed-event>;
||#

(defclass <frame-destroyed-event> (<frame-event>) ()
  (:documentation
"
The class of events that indicate a frame has been destroyed. An
instance of this class is distributed to the frame when it is
destroyed. Only one of these events is passed during the lifetime of
any frame.
"))


#||
// This event is sent the very first time a frame gets layed out,
// after it gets created but before it gets destroyed
define sealed class <frame-layed-out-event> (<frame-event>)
end class <frame-layed-out-event>;
||#

(defclass <frame-layed-out-event> (<frame-event>) ()
  (:documentation
"
This event is sent the very first time a frame gets layed out,
after it gets created but before it gets destroyed.
"))


#||
// This event is sent after a frame gets mapped
define sealed class <frame-mapped-event> (<frame-event>)
end class <frame-mapped-event>;
||#

(defclass <frame-mapped-event> (<frame-event>) ()
  (:documentation
"
The class of events that indicate a frame has been mapped, that is,
displayed on screen. An instance of this class is distributed whenever
a frame is mapped.

Example

The following example defines a method that can inform you when an
instance of a class of frame you have defined is mapped.

    (defmethod handle-event
        ((frame <my-frame>) (event <frame-mapped-event>))
      (notify-user (format nil \"Frame ~a mapped\" frame)))
"))


#||
// This event is sent after a frame gets unmapped
define sealed class <frame-unmapped-event> (<frame-event>)
end class <frame-unmapped-event>;
||#

(defclass <frame-unmapped-event> (<frame-event>) ()
  (:documentation
"
The class of events that indicate a frame has been unmapped, that is,
removed from the screen. An instance of this class is distributed
whenever a frame is unmapped. A frame may be unmapped by either
iconifying it, or by exiting or destroying the frame completely, so
that it no longer exists.

Example

The following example defines a method that can inform you when an
instance of a class of frame you have defined is unmapped.

    (defmethod handle-event
        ((frame <my-frame>) (event <frame-unmapped-event>))
      (notify-user
        (format nil \"Frame ~a unmapped\" frame)))
"))


#||
define abstract class <frame-focus-event> (<frame-event>)
end class <frame-focus-event>;
||#

(defclass <frame-focus-event> (<frame-event>)
  ()
  (:documentation
"
The class of events distributed when a frame receives the mouse focus.
")
  (:metaclass <abstract-metaclass>))


#||
// This event is sent every time a frame gets the input focus
define sealed class <frame-focus-in-event> (<frame-focus-event>)
end class <frame-focus-in-event>;
||#

(defclass <frame-focus-in-event> (<frame-focus-event>) ()
  (:documentation
"
This event is sent every time a frame gets the input focus.
"))


#||
// This event is sent every time a frame loses the input focus
define sealed class <frame-focus-out-event> (<frame-focus-event>)
end class <frame-focus-out-event>;
||#

(defclass <frame-focus-out-event> (<frame-focus-event>) ()
  (:documentation
"
This event is sent every time a frame loses the input focus.
"))


#||
// This event is sent every time the input focus in a frame changes
define sealed class <frame-input-focus-changed-event> (<frame-event>)
  sealed constant slot event-old-focus :: false-or(<sheet>),
    required-init-keyword: old-focus:;
  sealed constant slot event-new-focus :: false-or(<sheet>),
    required-init-keyword: new-focus:;
end class <frame-input-focus-changed-event>;
||#

(defclass <frame-input-focus-changed-event> (<frame-event>)
  ((event-old-focus :type (or null <sheet>) :initarg :old-focus
		    :initform (required-slot ":old-focus" "<frame-input-focus-changed-event>")
		    :reader event-old-focus)
   (event-new-focus :type (or null <sheet>) :initarg :new-focus
		    :initform (required-slot ":new-focus" "<frame-input-focus-changed-event>")
		    :reader event-new-focus))
  (:documentation
"
This event is sent every time the input focus in a frame changes.
"))


#||
// This event gets sent when someone calls 'frame-exit'.  The default
// 'handle-event' method just exits the frame, but hackers can write
// their own method that queries the user, for example.
define sealed class <frame-exit-event> (<frame-event>)
  sealed constant slot event-destroy-frame? = #f,
    init-keyword: destroy-frame?:;
end class <frame-exit-event>;
||#

(defgeneric event-destroy-frame? (event)
  (:documentation
"
Returns information about the frame that was destroyed in _event_.
"))

(defclass <frame-exit-event> (<frame-event>)
  ((event-destroy-frame? :initarg :destroy-frame? :initform nil :reader event-destroy-frame?))
  (:documentation
"
The class of events distributed when a frame is about to
exit. Contrast this with <frame-exited-event>, which is passed after
the frame is exited.

The default method uses 'frame-can-exit?' to decide whether or not to
exit.

If :destroy-frame? is t, then the frame is destroyed.
"))


#||
// This event get sent when a dialog exits normally.
define sealed class <dialog-exit-event> (<frame-exit-event>)
end class <dialog-exit-event>;
||#

(defclass <dialog-exit-event> (<frame-exit-event>) ()
  (:documentation
"
This event is sent when a dialog exits normally.
"))


#||
// This event get sent when a dialog is cancelled.
define sealed class <dialog-cancel-event> (<frame-exit-event>)
end class <dialog-cancel-event>;
||#

(defclass <dialog-cancel-event> (<frame-exit-event>) ()
  (:documentation
"
This event is sent when a dialog is cancelled.
"))


#||
// This event gets sent when the UI for a frame has been shut down
define sealed class <frame-exited-event> (<frame-event>)
  sealed constant slot event-status-code :: false-or(<integer>) = #f,
    init-keyword: status-code:;
end class <frame-exited-event>;
||#

(defgeneric event-status-code (event)
  (:documentation
"
Returns the status code of _event_.
"))

(defclass <frame-exited-event> (<frame-event>)
  ((event-status-code :type (or null integer) :initarg :status-code :initform nil :reader event-status-code))
  (:documentation
"
The class of events that indicate a frame has been exited. An instance
of this class is distributed to the frame when it is exited. Only one
of these events is passed during the lifetime of any frame.

The :status-code initarg is used to pass a status code, if
desired. This code can be used to pass the reason that the frame was
exited.
"))


#||
// This event gets sent when a whole application is shut down,
// kind of analogous to the Windows 'PostQuitMessage'
define sealed class <application-exited-event> (<frame-exited-event>)
end class <application-exited-event>;
||#

(defclass <application-exited-event> (<frame-exited-event>) ()
  (:documentation
"
The class of events signalled when an application exits. An instance
of this class is distributed when your application is exited, for
instance by choosing 'File > Exit' from its main menu bar.
"))


#||
/// Seal some domains for the concrete event classes

define sealed domain make (singleton(<frame-created-event>));
define sealed domain initialize (<frame-created-event>);

define sealed domain make (singleton(<frame-destroyed-event>));
define sealed domain initialize (<frame-destroyed-event>);

define sealed domain make (singleton(<frame-mapped-event>));
define sealed domain initialize (<frame-mapped-event>);

define sealed domain make (singleton(<frame-unmapped-event>));
define sealed domain initialize (<frame-unmapped-event>);

define sealed domain make (singleton(<frame-focus-in-event>));
define sealed domain initialize (<frame-focus-in-event>);

define sealed domain make (singleton(<frame-focus-out-event>));
define sealed domain initialize (<frame-focus-out-event>);

define sealed domain make (singleton(<frame-input-focus-changed-event>));
define sealed domain initialize (<frame-input-focus-changed-event>);

define sealed domain make (singleton(<frame-exit-event>));
define sealed domain initialize (<frame-exit-event>);

define sealed domain make (singleton(<frame-exited-event>));
define sealed domain initialize (<frame-exited-event>);

define sealed domain make (singleton(<application-exited-event>));
define sealed domain initialize (<application-exited-event>);
||#


