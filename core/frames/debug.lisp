;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-FRAMES-INTERNALS -*-
(in-package #:duim-frames-internals)

#||
/// Sheet debugging

define generic print-sheet-layout (sheet, #key, #all-keys) => ();
||#

(defgeneric print-sheet-layout (sheet &key &allow-other-keys))

#||
define method print-sheet-layout
    (sheet :: <sheet>, #key indentation = "") => ()
  let (left, top, right, bottom) = sheet-edges(sheet);
  format-out("\n%s%=: %dx%d at %d,%d %s",
             indentation, sheet, right - left, bottom - top, left, top,
             case
               sheet-withdrawn?(sheet) => "[withdrawn]";
               sheet-mapped?(sheet)    => "[mapped]";
               otherwise               => "[unmapped]";
             end);
  do(rcurry(print-sheet-layout, indentation: concatenate(indentation, "  ")),
     sheet-children(sheet))
end method print-sheet-layout;
||#

(defmethod print-sheet-layout ((sheet <sheet>) &key (indentation ""))
  (multiple-value-bind (left top right bottom)
      (sheet-edges sheet)
    (format t "~a~a: ~dx~d at ~d,~d ~a~%"
	    indentation
	    sheet
	    (- right left)
	    (- bottom top)
	    left
	    top
	    (cond ((sheet-withdrawn? sheet)
		   "[withdrawn]")
		  ((sheet-mapped? sheet)
		   "[mapped]")
		  (t
		   "[unmapped]")))
    (map nil
	 (alexandria:rcurry #'print-sheet-layout
		 :indentation
		 (concatenate 'string indentation "  "))
	 (sheet-children sheet))))

#||
define method print-sheet-layout
    (frame :: <frame>, #rest args, #key) => ()
  dynamic-extent(args);
  apply(print-sheet-layout, top-level-sheet(frame), args)
end method print-sheet-layout;
||#

(defmethod print-sheet-layout ((frame <frame>) &rest args &key)
  (declare (dynamic-extent args))
  (apply #'print-sheet-layout (top-level-sheet frame) args))


