;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-FRAMES-INTERNALS -*-
(in-package :duim-frames-internals)

;;; Command tables are a way of grouping a set of related command
;;; objects or "command functions" and providing user interfaces to
;;; the commands.  Interaction styles include pull-down menus,
;;; keystroke accelerators, direct manipulations (such as presentation
;;; translations and drag&drop), and command lines.

;;;; XXX: many of the MAKE-INSTANCE invocations in here should be
;;;;      changed to be MAKE-PANE invocations.
;;;; Is there any way of protecting the unwary from this pitfall?

;;; TODO: What to do about doc for 'menu-item-accelerator' which appears not
;;; to exist?
#||
Returns the keyboard accelerator for _menu-item_. Note that
_menu-item_ must be defined in a command table.
||#

;;; TODO: What to do about doc for 'menu-item-mnemonic' which appears not to exist?
#||
Returns the keyboard mnemonic for _menu-item_.
||#

;;; TODO: What to do about doc for 'menu-item-name' which appears not to exist?
#||
Returns the name of 'menu-item'
||#

;;; TODO: what to do about doc for 'menu-item-options' which appears not to exist?
#||
Returns the options for _menu-item_.
||#

;;; TODO: what to do about doc for 'menu-item-type' which appears not to exist?
#||
Returns the type of _menu-item_.
||#

;;; TODO: what to do about doc for 'menu-item-value' which appears not to exist?
#||
Returns the value of _menu-item_.
||#

;;; TODO: What to do about the documentation for <command-table-menu-item>, which
;;; seems to not exist as a class? Note that this documentation is the same as
;;; that for the 'add-command-table-menu-item' method, so the need for this class
;;; may not exist (it obviously doesn't exist).
#||
# Command Table Menu Item
The class of menu items in command tables. This class models menu
items, tool bar items, accelerators, and mnemonics for a command table
entry.

The :type initarg denotes what type of menu item has been
created. This is either :command, :function, :menu, or :divider.

When :type is :command, :value must be one of the following:

  + A command (a list consisting of a command name followed by a
    list of the arguments for the command).
  + A command name. In this case, _value_ behaves as though a command
    with no arguments was supplied.

When all the required arguments for the command are supplied, clicking
on an item in the menu invokes the command immediately. Otherwise, the
user is prompted for the remaining required arguments.

When :type is :function, :value must be a function having indefinite
extent that, when called, returns a command. The function is called
with two arguments:

  + The gesture used to select the item (either a keyboard or button
    press event).
  + A \"numeric argument\".

When :type is :menu, this indicates that a sub-menu is required, and
_value_ must be another command table or the name of another command
table.

When :type is :divider, some sort of dividing line is displayed in the
menu at that point. If a string is supplied using the :options
initarg, it will be drawn as the divider instead of a line. If the
look and feel provided by the underlying window system has no
corresponding concept, :divider items may be ignored. When :type
is :divider, :value is ignored.

The :accelerator and :mnemonic initargs let you specify a keyboard
accelerator and mnemonic for the menu item.
||#


;;; Command Tables

(defparameter *command-tables* (make-hash-table :test #'equal)
  "Command table registry, maps command table names to command tables")

(defgeneric (setf command-table-inherit-from) (inherit-from command-table))
(defgeneric install-inherited-menus (command-table inherit-menu))
(defgeneric remove-command-table (command-table)
  (:documentation "Removes _command-table_."))

(defgeneric do-command-table-inheritance (function command-table &key do-inherited-p))
(defgeneric command-present-p (command-table command))
(defgeneric command-accessible-p (command-table command))
(defgeneric add-command (command-table command &key label name menu image accelerator mnemonic resource-id errorp)
  (:documentation  "Adds _command_ to the specified _command-table_.

You can supply a keyboard accelerator or a mnemonic using the
_accelerator_ and _mnemonic_ arguments respectively.

The argument _name_ is the command-line name for the command.

  + When _name_ is nil, the command is not available via
    command-line interactions.
  + When _name_ is a string, that string is the command-line name for
    the command.

For the purposes of command-line name lookup, the character case of
_name_ is ignored.

The argument _menu_ is a menu for _command_.

  + When _menu_ is nil, _command_ is not available via menus.
  + When _menu_ is a string, the string is used as the menu name.
  + When _menu_ is t and _name_ is a string, then _name_ is used as
    the menu name.
  + When _menu_ is t and _name_ is not a string, a menu name is
    automatically generated.
  + When _menu_ is a list of the form (string menu-options), _string_
    is the menu name and _menu-options_ consists of a list of
    keyword-value pairs. Each keyword-value pair is itself a list.
    The valid keywords are :after, :documentation, and :text-style,
    which are interpreted as for 'add-command-table-menu-item'.

You can supply an image that will appear on the menu next to the
command name using the _image_ argument. When supplying an image, bear
in mind the size of the menu; you should only supply a small
icon-sized image for a menu command. There may also be other interface
guidelines that you wish to follow when using images in menu items.

The value for _accelerator_ is either a keyboard gesture or nil. When
it is a gesture, the gesture represents the keystroke accelerator for
the command; otherwise the command is not available via keystroke
accelerators. Similarly, if _mnemonic_ is supplied, this gesture is
used as a mnemonic for the command.

If _command_ is already present in the command table and _errorp_ is
t, an error is signalled. When _command_ is already present in the
command table and _errorp_ is nil, then the old command-line name,
menu, and keystroke accelerator are removed from the command table
before creating the new one."))

(defgeneric remove-command (command-table command)
  (:documentation "Removes _command_ from _command-table_."))

(defgeneric do-command-table-commands (function command-table &key do-inherited-p))
(defgeneric add-command-table-menu-item (command-table label type object &rest keys &key documentation after
						       accelerator mnemonic resource-id image text-style errorp
						       items label-key value-key test callback update-callback)
  (:documentation
   "Adds a command menu item to the menu in _command-table_. The _string_
argument is the name of the command menu item; its character case is
ignored. The _type_ of the item is either :command, :function, :menu,
or :divider.

When _type_ is :command, _value_ must be one of the following:

  + A command (a list consisting of a command name followed by a
    list of the arguments for the command).
  + A command name. In this case, _value_ behaves as though a command
    with no arguments was supplied.

When all the required arguments for the command are supplied, clicking
on an item in the menu invokes the command immediately. Otherwise, the
user is prompted for the remaining required arguments.

When _type_ is :function, _value_ must be a function having indefinite
extent that, when called, returns a command. The function is called
with two arguments:

  + The gesture used to select the item (either a keyboard or button
    press event).
  + A \"numeric argument\".

When _type_ is :menu, this indicates that a sub-menu is required, and
_value_ must be another command table or the name of another command
table.

When _type_ is :divider, some sort of a dividing line is displayed in
the menu at that point. If _string_ is supplied, it will be drawn as
the divider instead of a line. If the look and feel provided by the
underlying window system has no corresponding concept, :divider items
may be ignored. When _type_ is :divider, _value_ is ignored.

The argument _documentation_ specifies a documentation string. This
can be used to provide the user with some online documentation for the
menu item. Documentation strings are often displayed in a status bar
at the bottom of an application; highlighting the menu item using the
mouse pointer displays the documentation string in the status bar.

The _text-style_ argument, if supplied, represents text style. This
specifies the font family, style and weight with which to display the
menu item in the menu. For most menu items, you should just use the
default text style (that is, the one that the user chooses for all
applications). However, in certain cases, some variation is allowed.

The _text-style_ argument is of most use in context sensitive pop-up
menus, which often have a default menu item. This is usually the
command that is invoked by pressing the RETURN key on the current
selection; for example, in a list of files, the default command
usually opens the selected file in the application associated with
it. In Windows 95, the default command is displayed using a bold font,
to differentiate it from other commands in the menu, and you should
use the text-style argument to specify this.

When altering the text style of a menu item, you should always try to
stick to any relevant interface guidelines.

The _items_ argument is used to specify the gadgets that are to be
supplied to the command table as menu items. You can supply either
push boxes, check boxes, or radio boxes.

The _after_ argument denotes where in the menu the new item is to be
added. It must be one of the following:

:start
    Adds the new item to the beginning of the menu.

:end
    Adds the new item to the end of the menu.

A string naming an existing entry
    Adds the new item after that entry.

:sort
    Insert the item in such a way as to maintain the menu in
    alphabetical order.

If _mnemonic_ is supplied, the item is added to the keyboard mnemonic
table for the command table. The value of _mnemonic_ must be a
keyboard gesture name.

When _mnemonic_ is supplied and _type_ is :command or :function,
typing a key on the keyboard that matches the mnemonic invokes the
command specified by _value_.

When _type_ is :menu, the command is read from the submenu indicated
by _value_ in a window system specific manner. This usually means that
the submenu itself is displayed, allowing the user to see the
available options at that point.

If the item named by _string_ is already present in the command table
and _errorp_ is t, then an error is signalled. When the item is
already present in the command table and _errorp_ is nil, the old item
is removed from the menu before adding the new item. Note that the
character case of _string_ is ignored when searching the command
table."))

(defgeneric remove-command-table-menu-item (command-table label)
  (:documentation "Removes the menu item identified by _label_ from _command-table_."))

(defgeneric do-command-table-menu-items (function command-table &key label name do-inherited-p))
(defgeneric do-command-table-menu-commands (function command-table))
(defgeneric make-command-menu-bar (frame-manager frame &key command-table))
(defgeneric make-menus-from-command-table (command-table frame frame-manager &key label)
  (:documentation "Returns a set of menus from the menu definitions in _command-table_.

The _frame_ and _framem_ arguments specify the frame and frame manager
in which the menus are to be placed.

The _label_ argument lets you specify a label for the set of menus."))

(defgeneric make-menu-from-command-table-menu (decorators frame frame-manager &rest keys &key owner command-table
							  label documentation resource-id mnemonic item-callback
							  use-accelerators-p)
  (:documentation
   "Returns a menu from the menu definition in the specified command
table. This function is used by 'make-menus-from-command-table' to
individually create each menu defined in the command table. The
function 'make-menus-from-command-table' then puts each of the menus
created together in the appropriate way.

The _command-table-menu-items_ argument defines the items that are to
be placed in the menu. It is a sequence of instances of
<command-table-menu-item>.

The _frame_ and _framem_ arguments define the frame and the frame
manager in which the menu created is to be placed.

The _command-table_ argument specifies the command table in which the
definition of the menu created can be found.

The _label_ argument defines a label for the menu created.

The _mnemonic_ argument defines a keyboard mnemonic for the menu
created.
"))

(defgeneric make-command-tool-bar (frame-manager frame &key command-table))
(defgeneric do-frame-commands (function frame &key test menu-bar-p tool-bar-p owned-menus-p))
(defgeneric do-command-table-accelerators (function command-table &key accelerator do-inherited-p))
(defgeneric command-table-accelerators (command-table)
  (:documentation "Returns the keyboard accelerators for _command-table_."))


#||

define sealed class <standard-command-table> (<command-table>)
  // A symbol that names the command table
  sealed constant slot command-table-name,
    required-init-keyword: name:;
  // A sequence of command tables from which this one inherits
  sealed slot command-table-inherit-from,
    required-init-keyword: inherit-from:,
    setter: %inherit-from-setter;
  // A table of all the commands in this command table; the key is a
  // <command-oid> object, the value is the command line name string
  // or a boolean
  sealed slot command-table-commands :: <object-table> = make(<table>);
  // This is a sequence of decorators used to build menu and tool bars, etc.
  sealed slot command-table-menu :: <stretchy-object-vector> = make(<stretchy-vector>);
  // A cache of all the keystroke accelerators for this command table
  sealed slot %accelerators = #f;
  /*
  //--- This belongs in a command-line layer
  // A sorted sequence that maps from command-line names to <command-oid>
  // objects for this command table; entries are a pair consisting of the
  // command line name string and the <command-oid>
  sealed slot %command-line-names :: false-or(<stretchy-object-vector>) = #f;
  // A completion alist of command line names for this command table and
  // all the command tables it inherits from
  sealed slot %completion-alist = #f;
  sealed slot %completion-alist-tick = 0; 
  */
  /*
  //--- This belongs in a presentation layer
  // The set of presentation translators for this command table
  sealed slot command-table-translators :: false-or(<stretchy-object-vector>) = #f;
  sealed slot %translators-cache :: false-or(<object-table>) = #f;
  */
  // A resource id for creating menus from this command table
  sealed constant slot command-table-resource-id = #f,
    init-keyword: resource-id:;
end class <standard-command-table>;

define sealed domain make (singleton(<standard-command-table>));
define sealed domain initialize (<standard-command-table>);
||#

(defgeneric command-table-commands (command-table)
  (:documentation "Returns the commands defined for _command-table_."))

(defgeneric command-table-menu (command-table)
  (:documentation "Returns the menu items in _command-table_."))

(defgeneric command-table-name (command-table)
  (:documentation "Returns the name of _command-table_, as defined by the :name initarg for <command-table>."))

(defun find-command-table (name &key (errorp t))
  "Returns the command table named by _name_. If _name_ is itself a command table, it is returned. If the command table is not found and _errorp_ is t , the *command-table-not-found* error will be signaled"
  (cond ((command-table-p name)
         name)
        ((symbolp name)
;         (or (gethash name *all-command-tables*) ; FIXME: what is the equivalent of *all-command-tables* in DUIM?
         (or (gethash name *command-tables*)
	 (ecase errorp
               ((t)
                (error 'command-table-not-found
                       :format-string "Command table ~S not found"
                       :format-args (list name)))
               ((nil)
                nil))))
        (t
	 (error "~S is not a symbol or command table" name))))


(defclass <standard-command-table> (<command-table>)
  ;; A symbol that names the command table
  ((command-table-name :initarg :name
		       :initform (required-slot ":name" "<standard-command-table>")
		       :reader command-table-name)
   ;; A sequence of command tables from which this one inherits
   (command-table-inherit-from :initarg :inherit-from
			       :initform (required-slot ":inherit-from" "<standard-command-table>")
			       :reader command-table-inherit-from :writer %inherit-from-setter)
   ;; A table of all the commands in this command table; the key is a
   ;; <command-oid> object, the value is the command line name string
   ;; or a boolean
   ;; FIXME: is this a reasonable :test? If this is changed, also change the one
   ;; for frame-disabled-commands in FRAMES;FRAMES.LISP.
   (command-table-commands :type hash-table :initform (make-hash-table :test #'equalp) :accessor command-table-commands)
   ;; This is a sequence of decorators used to build menu and tool bars, etc.
   (command-table-menu :type vector :initform (make-array 0 :adjustable t :fill-pointer t) :accessor command-table-menu)
   ;; A cache of all the keystroke accelerators for this command table
   (%accelerators :initform nil :accessor %accelerators)
#|| /*
  //--- This belongs in a command-line layer
  // A sorted sequence that maps from command-line names to <command-oid>
  // objects for this command table; entries are a pair consisting of the
  // command line name string and the <command-oid>
  (%command-line-names :type (or null sequence) :initform nil :accessor %command-line-names)
  // A completion alist of command line names for this command table and
  // all the command tables it inherits from
  (%completion-alist :initform nil :accessor %completion-alist)
  (%completion-alist-tick :initform 0 :accessor %completion-alist-tick)
*/ ||#
#|| /*
  //--- This belongs in a presentation layer
  // The set of presentation translators for this command table
  (command-table-translators :type (or null sequence) :initform nil :accessor command-table-translators)
  (%translators-cache :type (or null hash-table) :initform nil :accessor %translators-cache)
*/ ||#
   ;; A resource id for creating menus from this command table
   (command-table-resource-id :initarg :resource-id :initform nil :reader command-table-resource-id))
  (:documentation
   "Command tables are a way of grouping a set of related command objects or
'command functions' and providing user interfaces to the commands.
Interaction styles include via pull-down menus, keystroke accelerators,
direct manipulations (such as presentation translations and drag&drop),
and command lines."))


#||
define method command-table-inherit-from-setter
    (inherit-from, command-table :: <standard-command-table>)
 => (command-tables :: <sequence>)
  unless (inherit-from == command-table-inherit-from(command-table))
    command-table.%inherit-from := inherit-from;
    /* command-table.%completion-alist := #f; */
    /* when (command-table.%translators-cache)
	 remove-all-keys!(command-table.%translators-cache)
       end */
  end;
  inherit-from
end method command-table-inherit-from-setter;
||#

(defmethod (setf command-table-inherit-from) (inherit-from (command-table <standard-command-table>))
  (unless (eql inherit-from (command-table-inherit-from command-table))
    (%inherit-from-setter inherit-from command-table)
#|| /*    command-table.%completion-alist := #f; */
    /*    when (command-table.%translators-cache)
	 remove-all-keys!(command-table.%translators-cache)
       end */ ||#
    )
  inherit-from)



#||

/// Making command tables

// define command-table *compiler* (*file*, *edit*)
//   command-table {a command table decorator}
//   menu      {a menu decorator}
//   command   {a command or function decorator}
//   menu-item "Compile" [of <command>] = compile-file
//   menu-item "Load"    [of <command>] = make(<command, function: load-file);
//   menu-item "Help"    [of <command>] = <help-command>;
//   menu-item "About"   [of <command>] = (<help-command>, about?: #t);
// end
define macro command-table-definer
  { define command-table ?:name (?supers:*) ?options:* end }
    => { define command-table-variable ?name (?supers) ?options end;
         define command-table-menu ?name (?supers) ?options end }
end macro command-table-definer;
||#

(defmacro define-command-table (name supers &optional options)
  "Defines a new class of command table with the specified name and
properties. This macro is equivalent to 'defclass', but with
additional options.

The _supers_ argument specifies a list of command tables from which
the command table you are creating should inherit. If you are not
explicitly inheriting the behaviour of other command tables, then
_supers_ should have the value '*global-command-table*'.

Each one of the _options_ supplied describes a command for the command
table. This can be either a menu item, a separator, or another command
table to be included in the command table. You can supply any number
of options. Each option takes one of the following forms:

    (menu-item _menu-item-descriptor_)
    (include _command-table-name_)
    (separator)

TODO: check this... pretty sure 'separator' doesn't need parenthesis,
for example.

To add a menu item of menu to a command table, include an option of
the following form:

    (menu-item _label_ = _command-function_
        &key _accelerator_ _documentation_)

_label_
    An instance of <string>. This is the label that appears in the
    menu.

_command-function_
    An instance of type 'type-union(<command>, <command-table>,
    <function>)'. The command function is the callback that is
    invoked to perform the intended operation for the menu item.
    Note that this can itself be a command table.

_accelerator_
    An instance of 'false-or(<gesture>)'. Default value: nil. This
    defines a keyboard accelerator that can be used to invoke
    _command-function_ in preference to the menu item itself.

_documentation_
    An instance of 'false-or(<string>)'. Default value: nil. This
    specifies a documentation string for the menu item that can
    be used to provide online help to the user. For menu items,
    documentation strings are usually displayed in the status bar
    of your application, when the mouse pointer is placed over
    the menu item itself.

To add a separator to a menu, just include the following option at the
point you want the separator to appear:

    (separator)

TODO: Pretty sure this doesn't need to be in parenthesis, and also it
can take a string I think (when in parenthesis).

To include another command table in the current table, include the
following option at the point you want the command table to appear:

    (include _command-table-name_)

The commands defined in _command-table-name_ are added to the current
command table at the appropriate point.

Example:

The following example shows how you might create a command table for
the standard Windows File menu, a how this could be integrated into
the menu bar for an application. The example assumes that the
appropriate command functions have already been defined for each
command in the command table.

    (define-command-table *file-menu-command-table*
        (*global-command-table*)
      ((menu-item \"New...\" = #'frame-new-file
                               :accelerator
                               (make-keyboard-gesture :n :control)
                               :documentation
                               \"Creates a new document.\")
       (menu-item \"Open...\" = #'frame-open-file
                                :accelerator
                                (make-keyboard-gesture :o :control)
                                :documentation
                                \"Opens an existing document.\")
       (menu-item \"Close\" = #'frame-close-file
                              :documentation
                              \"Closes an open document.\")
       (separator)
       (include *save-files-command-table*)
       (separator)
       (menu-item \"Exit\" = (make-command '<command>
                                           :function
                                           #'exit-frame))))

    (define-command-table *application-command-table*
        (*global-command-table*)
      ((menu-item \"File\" = *file-menu-command-table*)
       (menu-item \"Edit\" = *edit-menu-command-table*)
       (menu-item \"View\" = *view-menu-command-table*)
       (menu-item \"Windows\" = *windows-menu-command-table*)
       (menu-item \"Help\" = *help-menu-command-table*)))

TODO: In standard commands, the *standard-help-command-table* also
uses the syntax:

    (menu-item \"Topics...\" = (find-class '<help-on-topics>))

... so need to work out what that's doing and document it. Also
separators are *not* surrounded by parenthesis.
"
  (let ((menu (define-command-table-menu name supers options)))   ; list of calls to add menus to the command table
    `(progn
       (define-command-table-variable ,name ,supers ,options)     ; returns a single form defining the command table
       ,@menu)))

#||
define macro command-table-variable-definer
  { define command-table-variable ?:name (?supers:*) ?options:* end }
    => { define variable ?name :: <standard-command-table>
           = apply(make, <command-table>,
		   name: ?#"name",
		   inherit-from: vector(?supers), 
                   ?options) }
 supers:
  { } => { }
  { ?super:*, ... } => { ?super, ... }
 options:
  { } => { #[] }
  { ?option:*; ... } => { concatenate(?option, ...) }
 option:
  { inherit-menu  ?:expression } => { vector(inherit-menu:, ?expression) }
  { resource-id   ?:expression } => { vector(resource-id:,  ?expression) }
  { command-table ?:expression } => { #[] }
  { command   ?:expression } => { #[] }
  { menu      ?:expression } => { #[] }
  { menu-item ?item:* } => { #[] }
  { include   ?item:* } => { #[] }
  { separator ?item:* } => { #[] }
  { separator } => { #[] }
end macro command-table-variable-definer;

/*
//--- Used to be this...
define macro command-table-variable-definer
  { define command-table-variable ?:name (?supers:*) ?options:* end }
    => { define variable ?name :: <standard-command-table>
           = make(<command-table>,
		  name: ?#"name",
		  inherit-from: vector(?supers), ?options) }
 supers:
  { } => { }
  { ?super:*, ... } => { ?super, ... }
 options:
  { } => { }
  { ?option:*; ... } => { ?option, ... }
 option:
  { inherit-menu ?:expression } => { inherit-menu: ?expression }
  { resource-id  ?:expression } => { resource-id:  ?expression }
  { command-table ?:expression } => { }
  { command   ?:expression } => { }
  { menu      ?:expression } => { }
  { menu-item ?item:* } => { }
  { include   ?item:* } => { }
  { separator ?item:* } => { }
  { separator } => { }
end macro command-table-variable-definer;
*/
||#

  ;; Must return a LIST of evaluated options...
  ;; Walk over the 'options', looking for specific words; if found,
  ;; process. A pattern matching macro system would be nice for
  ;; converting these.
  ;; Each option is either a LIST, or a SYMBOL. In the latter case,
  ;; only the symbol :SEPARATOR is valid:

  ;; (:inherit-menu ?:expression) => (:inherit-menu ?expression)
  ;; (:resource-id  ?:expression) => (:resource-id  ?expression)
  ;; (:command-table ?:expression) => { }
  ;; (:command   ?:expression) => { }
  ;; (:menu      ?:expression) => { }
  ;; (:menu-item ?item:*) => { }
  ;; (:include   ?item:*) => { }
  ;; (:separator ?item:*) => { }
  ;; :separator => { }

;;; FIXME: go through these again, check they work for all cases, and tidy them up.

(defmacro define-command-table-variable (name supers options)
  `(defparameter ,name
     (apply #'make-command-table
	    :name ',name
	    :inherit-from (list ,@supers)
	    ;; XXX: this is not right; we NEVER want the list of supers
	    ;; to be the last arg, since if it is APPLY unrolls it and
	    ;; we get a coercion error. We want the following form to
	    ;; return either a list, or nil. Maybe changing the apply to
	    ;; a FUNCALL would suffice, I'm not sure.
	    ;; XXX: The following WAS ,@(loop...)
	    ,(loop for option in options
		   appending (cond ((atom option)
				    ;; :SEPARATOR
				    nil)
				   (t
				    (cond ((string= (%STRING-DESIGNATOR->STRING (first option))
						    (symbol-name :inherit-menu))
					   (list :inherit-menu (cadr option)))
					  ((string= (%STRING-DESIGNATOR->STRING (first option))
						    (symbol-name :resource-id))
					   (list :resource-id (cadr option)))
					  (t
					   nil))))))))

#||
define macro command-table-menu-definer
  { define command-table-menu ?:name (?supers:*) end }
    => { }
  // The decorator cases
  { define command-table-menu ?:name (?supers:*)
      command-table ?decorator:expression;
      ?more-items:*
    end }
    => { add-command(?name, ?decorator);
	 define command-table-menu ?name (?supers) ?more-items end }
  { define command-table-menu ?:name (?supers:*)
      menu ?decorator:expression;
      ?more-items:*
    end }
    => { add-command(?name, ?decorator);
	 define command-table-menu ?name (?supers) ?more-items end }
  { define command-table-menu ?:name (?supers:*)
      command ?decorator:expression;
      ?more-items:*
    end }
    => { add-command(?name, ?decorator);
	 define command-table-menu ?name (?supers) ?more-items end }
  // The 'menu-item' cases
  { define command-table-menu ?:name (?supers:*)
      menu-item ?label of ?type = ?command, #rest ?options:expression;
      ?more-items:*
    end }
    => { add-command-table-menu-item
	   (?name, ?label, ?type, ?command, ?options);
	 define command-table-menu ?name (?supers) ?more-items end }
  { define command-table-menu ?:name (?supers:*)
      menu-item ?label of ?type = ?command;
      ?more-items:*
    end }
    => { add-command-table-menu-item
	   (?name, ?label, ?type, ?command);
	 define command-table-menu ?name (?supers) ?more-items end }
  { define command-table-menu ?:name (?supers:*)
      menu-item ?label = ?command, #rest ?options:expression;
      ?more-items:*
    end }
    => { add-command-table-menu-item
	   (?name, ?label, #f, ?command, ?options);
	 define command-table-menu ?name (?supers) ?more-items end }
  { define command-table-menu ?:name (?supers:*)
      menu-item ?label = ?command;
      ?more-items:*
    end }
    => { add-command-table-menu-item
	   (?name, ?label, #f, ?command);
	 define command-table-menu ?name (?supers) ?more-items end }
  // The 'separator' cases
  { define command-table-menu ?:name (?supers:*)
      separator;
      ?more-items:*
    end }
    => { add-command-table-menu-item
	   (?name, #f, <separator>, #f);
	 define command-table-menu ?name (?supers) ?more-items end }
  { define command-table-menu ?:name (?supers:*)
      separator ?label;
      ?more-items:*
    end }
    => { add-command-table-menu-item
	   (?name, ?label, <separator>, #f);
	 define command-table-menu ?name (?supers) ?more-items end }
  // The 'include' cases
  { define command-table-menu ?:name (?supers:*)
      include ?command-table;
      ?more-items:*
    end }
    => { add-command-table-menu-item
	   (?name, #f, <separator>, ?command-table);
	 define command-table-menu ?name (?supers) ?more-items end }
  { define command-table-menu ?:name (?supers:*)
      include ?label = ?command-table;
      ?more-items:*
    end }
    => { add-command-table-menu-item
	   (?name, ?label, <separator>, ?command-table);
	 define command-table-menu ?name (?supers) ?more-items end }
  // Fall-through...
  { define command-table-menu ?:name (?supers:*)
      ?non-menu-item:*; ?more-items:*
    end }
    => { define command-table-menu ?name (?supers) ?more-items end }
 // We use these secondary patterns instead of '?label:expression' in order
 // to prevent the whole '"label" of <type> = command' input from matching
 label:
  { ?:expression }
    => { ?expression }
 type:
  { ?:expression }
    => { ?expression }
 command:
  { (?class:expression, #rest ?options:expression) }
    => { list(?class, ?options) }
  { ?:expression }
    => { ?expression }
 command-table:
  { ?:expression }
    => { ?expression }
end macro command-table-menu-definer;
||#

#||

Decorator cases:

    (COMMAND-TABLE ?decorator:expression) => (add-command name decorator)
    (MENU ?decorator:expression)          => (add-command name decorator)
    (COMMAND ?decorator:expression)       => (add-command name decorator)

Menu-item cases:

    (MENU-ITEM ?label OF ?type = ?command #rest ?options:expression) => (add-command-table-menu-item name label type command options)
    (MENU-ITEM ?label OF ?type = ?command)                           => (add-command-table-menu-item name label type command)))
    (MENU-ITEM ?label = ?command #rest ?options:expression)          => (add-command-table-menu-item name label nil  command options)
    (MENU-ITEM ?label = ?command)                                    => (add-command-table-menu-item name label nil  command))))))

 command:
  { (?class:expression, #rest ?options:expression) }
    => { list(?class, ?options) }
  { ?:expression }
    => { ?expression }

    An example of this usage is:-

        (menu-item "Show All Subclasses" = (list #m<show-subclasses> :all? t)
                   :image "v!"
                   :documentation "Show all subclasses")

    Where the <show-subclasses> class takes an ":ALL?" initarg.

Separator cases:

    SEPARATOR          => (add-command-table-menu-item name nil   #m<separator> nil)
    (SEPARATOR ?label) => (add-command-table-menu-item name label #m<separator> nil)

Include cases:

    (INCLUDE ?command-table)          => (add-command-table-menu-item name nil   #m<separator> command-table)
    (INCLUDE ?label = ?command-table) => (add-command-table-menu-item name label #m<separator> command-table)
||#
(defun %process-command-table-menu-item (name item)
  (let ((result (cond
		  ;;
		  ;; The decorator cases
		  ;;
		  ((and (listp item)
			(string= (%STRING-DESIGNATOR->STRING (first item))
				 (symbol-name :command-table)))
		   ;; (COMMAND-TABLE ?decorator:expression)
		   `(add-command ,name ,(rest item)))
		  ((and (listp item)
			(string= (%STRING-DESIGNATOR->STRING (first item))
				 (symbol-name :menu)))
		   ;; (MENU ?decorator:expression)
		   `(add-command ,name ,(rest item)))
		  ((and (listp item)
			(string= (%STRING-DESIGNATOR->STRING (first item))
				 (symbol-name :command)))
		   ;; (COMMAND ?decorator:expression)
		   `(add-command ,name ,(rest item)))
		  ;;
		  ;; The 'menu-item' cases
		  ;;
		  ((and (listp item)
			(string= (%STRING-DESIGNATOR->STRING (first item))
				 (symbol-name :menu-item)))
		   (let ((label (second item)))
		     ;; 3rd element is either 'OF' or '='....
		     (if (string= (%STRING-DESIGNATOR->STRING (third item)) (symbol-name :of))
			 ;; Deal with 'OF' cases...
			 (let ((type    (find-class (fourth item)))
			       (command (sixth item))
			       (options (if (> (length item) 6)
					    (cddddr (cddr item))
					    nil)))
			   ;; command is either:
			   ;;    an atom             [*some-command-table*]
			   ;;    a list              [(function 'fn) or (find-class 'class)]
			   ;;    or another list (!) [(class:expression #rest options)]  =>  ((function <class>) :all? t)
			   (if options
			       ;; (MENU-ITEM ?label OF ?type = ?command #rest ?options:expression)
			       `(add-command-table-menu-item ,name ,label ,type ,command ,@options)
			       ;; (MENU-ITEM ?label OF ?type = ?command)
			       `(add-command-table-menu-item ,name ,label ,type ,command)))
			 ;; else
			 ;; Deal with '=' cases...
			 (let ((command (fourth item))
			       (options (if (> (length item) 4)
					    (cddddr item)
					    nil)))
			   (if options
			       ;; (MENU-ITEM ?label = ?command #rest ?options:expression)
			       `(add-command-table-menu-item ,name ,label nil ,command ,@options)
			       ;; (MENU-ITEM ?label = ?command)
			       `(add-command-table-menu-item ,name ,label nil ,command))))))
		  ;;
		  ;; The 'separator' cases
		  ;;
		  ((and (not (listp item))
			(string= (%STRING-DESIGNATOR->STRING item)
				 (symbol-name :separator)))
		   ;; SEPARATOR
		   `(add-command-table-menu-item ,name nil (find-class '<separator>) nil))
		  ((and (listp item)
			(string= (%STRING-DESIGNATOR->STRING (first item))
				 (symbol-name :separator)))
		   ;; (SEPARATOR ?label)
		   (let ((label (second item)))
		     `(add-command-table-menu-item ,name ,label (find-class '<separator>) nil)))
		  ;;
		  ;; The 'include' cases
		  ;;
		  ((and (listp item)
			(string= (%STRING-DESIGNATOR->STRING (first item))
				 (symbol-name :include)))
		   (if (= (length item) 2)
		       ;; (INCLUDE ?command-table)
		       (let ((command-table (second item)))
			 `(add-command-table-menu-item ,name nil (find-class '<separator>) ,command-table))
		       (let ((label (second item))
			     ;; 3rd is '='...
			     (command-table (fourth item)))
			 ;; (INCLUDE ?label = ?command-table)
			 `(add-command-table-menu-item ,name ,label (find-class '<separator>) ,command-table))))
		  ;;
		  ;; Fall-through...
		  ;;
		  (t
		   (values)))))
    result))


(defun define-command-table-menu (name supers options)
  (declare (ignore supers))
  (let ((items ()))
    (map nil #'(lambda (option)
		 (let ((item (%process-command-table-menu-item name option)))
		   (when item
		     (push item items))))
	 options)
    (nreverse items)))

#||
define sealed method make
    (class == <command-table>,
     #key name, inherit-from = #[], inherit-menu = #f, resource-id = #f)
 => (command-table :: <standard-command-table>)
  let inherit-from = as(<simple-vector>, inherit-from);
  let command-table
    = make(<standard-command-table>,
	   name: name,
	   inherit-from: inherit-from,
	   resource-id:  resource-id);
  when (inherit-menu)
    install-inherited-menus(command-table, inherit-menu)
  end;
  gethash(*command-tables*, name) := command-table;
  command-table
end method make;
||#

(defmethod make-command-table (&key name (inherit-from #()) (inherit-menu nil) (resource-id nil) &allow-other-keys)
  (check-type name symbol)
  (check-type inherit-from sequence)
  (let* ((inherit-from (coerce inherit-from 'vector))
	 (command-table (make-instance '<standard-command-table>
				       :name name
				       :inherit-from inherit-from
				       :resource-id resource-id)))
    (when inherit-menu
      (install-inherited-menus command-table inherit-menu))
    ;;; FIXME:
    ;;; Raise a command-table-already-exists condition here instead of
    ;;; a warning. This is what CLIM does. It is a subclass of
    ;;; command-table-error. See section 11.6 of the Lispworks CLIM
    ;;; docs and section 27.2 of the CLIM specification. See:
    ;;; https://github.com/franzinc/clim2/blob/4d4712f28b397523d8b30059de60d5b48f52a1b7/clim/command.lisp#L111
    ;;; for the CLIM implementation of make-command-table and how to do this
    (when (gethash name *command-tables*)
      (warn "Redefining command table ~a which already exists as a command table in *COMMAND-TABLES*" name))
    (setf (gethash name *command-tables*) command-table)
    command-table))


#||
define method install-inherited-menus
    (command-table :: <standard-command-table>, inherit-menu) => ()
  check-type(inherit-menu, type-union(one-of(#f, #t, #"menu", #"accelerators"), <list>));
  for (comtab in command-table-inherit-from(command-table))
    when (~instance?(inherit-menu, <sequence>)
	  | member?(comtab, inherit-menu))
      let menu = command-table-menu(comtab);
      when (menu)
        for (decorator :: <command-decorator> in menu)
	  let object  = decorator-object(decorator);
	  let type    = decorator-type(decorator);
	  let label   = decorator-label(decorator);
	  let image   = decorator-image(decorator);
	  let options = decorator-options(decorator);
	  let accelerator   = decorator-accelerator(decorator);
	  let mnemonic      = decorator-mnemonic(decorator);
	  let documentation = decorator-documentation(decorator);
	  let resource-id   = decorator-resource-id(decorator);
	  select (inherit-menu)
	    #"menu" => 
	      accelerator := $unsupplied;
	      mnemonic    := $unsupplied;
	    #"accelerators" =>
	      label := #f;
	    otherwise => #f
	  end;
	  apply(add-command-table-menu-item,
		command-table, label, type, object,
		image: image,
		accelerator: accelerator, mnemonic: mnemonic,
		documentation: documentation, resource-id: resource-id,
		error?: #f, options)
	end
      end
    end
  end
end method install-inherited-menus;
||#

(defmethod install-inherited-menus ((command-table <standard-command-table>) inherit-menu)
  (check-type inherit-menu (or boolean (member :menu :accelerators) list))
  (loop for comtab across (command-table-inherit-from command-table)
     when (or (not (typep inherit-menu 'sequence))
	      (member comtab inherit-menu))
     do (let ((menu (command-table-menu comtab)))
	  (when menu
	    (loop for decorator in menu
	       do (let ((object        (decorator-object decorator))
			(type          (decorator-type decorator))
			(label         (decorator-label decorator))
			(image         (decorator-image decorator))
			(options       (decorator-options decorator))
			(accelerator   (decorator-accelerator decorator))
			(mnemonic      (decorator-mnemonic decorator))
			(documentation (decorator-documentation decorator))
			(resource-id   (decorator-resource-id decorator)))
		    (case inherit-menu
		      (:menu
		       (setf accelerator :unsupplied)
		       (setf mnemonic    :unsupplied))
		      (:accelerators
		       (setf label nil))
		      (t nil))
		    (apply #'add-command-table-menu-item
			   command-table label type object
			   :image image
			   :accelerator accelerator :mnemonic mnemonic
			   :documentation documentation :resource-id resource-id
			   :errorp nil options)))))))


(defmethod remove-command-table (name)
  "Removes command-table from *command-tables*. Returns _true_ if successful, _false_ otherwise"
  ;;; This should hack inheritance, too, but doesn't.
  (cond ((command-table-p name)
         (remhash (command-table-name name) *command-tables*))
        ((symbolp name)
         (or (remhash name *command-tables*)
             (ecase errorp
               ((t)
                (error 'command-table-not-found
                       :format-string "Command table ~S not found"
                       :format-args (list name)))
               ((nil)
                nil))))
        (t
	 (error "~S is not a symbol or command table" name))))


#||
// The global command table
// Things like the Mouse-Right (menu) translator live here.
define variable *global-command-table* :: <standard-command-table>
    = make(<command-table>,
	   name: #"global-command-table",
	   inherit-from: #[]);
||#

(defparameter *global-command-table* (make-command-table :name :global-command-table
							 :inherit-from (vector))
  "This is the command table from which all other command tables inherit
by default. You should not explicitly add anything to or remove
anything from this command table. DUIM can use this command to store
internals or system-wide commands. You should not casually install any
commands or translators into this command table.")


#||
// CLIM's general "user" command table
define variable *user-command-table* :: <standard-command-table>
    = make(<command-table>,
	   name: #"user-command-table",
	   inherit-from: vector(*global-command-table*));
||#

(defparameter *user-command-table* (make-command-table :name :user-command-table
						       :inherit-from (vector *global-command-table*))
  "This is a command table that can be used by the programmer for any
purpose. DUIM does not use it for anything, and its contents are
completely undefined.

If desired, all new command tables can inherit the command table
specified by this variable.")



#||

/// Inheritance

//--- This should be more careful to hit each command table only once
//--- (see the new version in CLIM for how to do this)
define method do-command-table-inheritance
    (function :: <function>, command-table :: <standard-command-table>,
     #key do-inherited? = #t) => ()
  function(command-table);
  when (do-inherited?)
    for (comtab in command-table-inherit-from(command-table))
      do-command-table-inheritance(function, comtab, do-inherited?: #t)
    end
  end
end method do-command-table-inheritance;
||#

;;; This should be more careful to hit each command table only once
;;; (see the new version in CLIM for how to do this)
(defmethod do-command-table-inheritance ((function function) (command-table <standard-command-table>)
					 &key (do-inherited-p t))
  (funcall function command-table)
  (when do-inherited-p
    (map nil #'(lambda (comtab)
		 (do-command-table-inheritance function comtab :do-inherited-p t))
	 (command-table-inherit-from command-table))))


(defmethod command-present-p ((command-table <standard-command-table>) command)
  (check-type command <command-oid>)
  (gethash command (command-table-commands command-table)))


#||
define method command-accessible?
    (command-table :: <standard-command-table>, command :: <command-oid>)
 => (command-table :: false-or(<standard-command-table>))
  block (return)
    do-command-table-inheritance
      (method (comtab)
	 when (command-present?(comtab, command))
	   return(comtab)
	 end
       end method,
       command-table);
    #f
  end
end method command-accessible?;
||#

(defmethod command-accessible-p ((command-table <standard-command-table>) command)
  (check-type command <command-oid>)
  (do-command-table-inheritance #'(lambda (comtab)
				    (when (command-present-p comtab command)
				      (return-from command-accessible-p comtab)))
    command-table)
  nil)



#||
/// Adding and removing commands

define method add-command
    (command-table :: <standard-command-table>, command :: <command-oid>,
     #key label, name, menu, image,
	  accelerator = $unsupplied, mnemonic = $unsupplied, resource-id,
	  error? = #t) => ()
  let label = label | name;		// for compatibility
  check-type(label, false-or(<string>));
  when (command-present?(command-table, command))
    when (error?)
      cerror("Remove the command and proceed",
	     "The command %= is already present in %=", command, command-table)
    end;
    remove-command(command-table, command)
  end;
  let menu-label   = #f;
  let menu-options = #();
  when (menu)
    menu-label   := if (instance?(menu, <list>)) head(menu) else menu end;
    menu-options := if (instance?(menu, <list>)) tail(menu) else #()  end
  end;
  check-type(menu-label, false-or(<string>));
  let commands = command-table-commands(command-table);
  if (label)
    add-command-line-name(command-table, command, label)
  else
    gethash(commands, command) := #t
  end;
  case
    menu =>
      let type = if (instance?(command, <function>)) <function> else <command> end;
      apply(add-command-table-menu-item,
	    command-table, menu-label, type, command,
	    image: image,
	    accelerator: accelerator, mnemonic: mnemonic, resource-id: resource-id,
	    error?: error?, menu-options);
    (supplied?(accelerator) & accelerator)
    | (supplied?(mnemonic) & mnemonic) =>
      let type = if (instance?(command, <function>)) <function> else <command> end;
      add-command-table-menu-item
	(command-table, #f, type, command,
	 accelerator: accelerator, mnemonic: mnemonic, resource-id: resource-id,
	 error?: error?);
  end
end method add-command;
||#

(defmethod add-command ((command-table <standard-command-table>) command
			&key label name menu image
			(accelerator :unsupplied) (mnemonic :unsupplied) resource-id
			(errorp t))
  (check-type command <command-oid>)
  (let ((label (or label name)))  ;; for compatability
    (check-type label (or null string))
    (when (command-present-p command-table command)
      (when error
	(cerror "Remove the command and proceed"
		"The command ~a is already present in ~a" command command-table))
      (remove-command command-table command))
    (let ((menu-label nil)
	  (menu-options (list)))
      (when menu
	(setf menu-label (if (listp menu) (first menu) menu))
	(setf menu-options (if (listp menu) (cdr menu) (list))))
      (let ((commands (command-table-commands command-table)))
	(if label
	    (add-command-line-name command-table command label)
	    (setf (gethash command commands) t))
	(cond (menu (let ((type (if (typep command 'function) (find-class 'function) (find-class '<command>))))
		      (apply #'add-command-table-menu-item
			     command-table menu-label type command
			     :image image
			     :accelerator accelerator :mnemonic mnemonic :resource-id resource-id
			     :errorp errorp menu-options)))
	      ((or (and (supplied? accelerator) accelerator)
		   (and (supplied? mnemonic) mnemonic))
	       (let ((type (if (typep command 'function) (find-class 'function) (find-class '<command>))))
		 (add-command-table-menu-item command-table nil type command
					      :accelerator accelerator :mnemonic mnemonic :resource-id resource-id
					      :errorp errorp))))))))


#||
define method add-command
    (command-table :: <standard-command-table>, decorator :: <command-decorator>,
     #key label, name, menu, image,
	  accelerator = $unsupplied, mnemonic = $unsupplied, resource-id,
	  error? = #t) => ()
  ignore(menu);
  let object = decorator-object(decorator);
  let type   = decorator-type(decorator);
  let label  = label | name | decorator-label(decorator);
  let image  = image        | decorator-image(decorator);
  let accelerator
    = if (supplied?(accelerator)) accelerator else decorator-accelerator(decorator) end;
  let mnemonic
    = if (supplied?(mnemonic)) mnemonic else decorator-mnemonic(decorator) end;
  let resource-id = resource-id | decorator-resource-id(decorator);
  add-command-table-menu-item
    (command-table, label, type, object,
     image: image,
     accelerator: accelerator, mnemonic: mnemonic, resource-id: resource-id,
     error?: error?)
end method add-command;
||#

(defmethod add-command ((command-table <standard-command-table>) (decorator <command-decorator>)
			&key label name menu image
			  (accelerator :unsupplied) (mnemonic :unsupplied) resource-id
			  (errorp t))
  (declare (ignore menu))
  (let ((object (decorator-object decorator))
	(type   (decorator-type decorator))
	(label  (or label name (decorator-label decorator)))
	(image  (or image      (decorator-image decorator)))
	(accelerator (if (supplied? accelerator) accelerator (decorator-accelerator decorator)))
	(mnemonic (if (supplied? mnemonic) mnemonic (decorator-mnemonic decorator)))
	(resource-id (or resource-id (decorator-resource-id decorator))))
    (add-command-table-menu-item command-table label type object
				 :image image
				 :accelerator accelerator :mnemonic mnemonic :resource-id resource-id
				 :errorp errorp)))


#||
define method remove-command
    (command-table :: <standard-command-table>, command :: <command-oid>) => ()
  // Remove old command names
  /* let names = command-table.%command-line-names;
     block (break)
       while (names)
	 let index = find-key(names, method (entry) second(entry) = command end);
	 case
	   index =>
	     names := remove-at!(names, index);
	     inc!(*completion-cache-tick*);
	   otherwise =>
	     break()
	 end
       end
     end; */
  // Remove old menu items
  let menu       = command-table-menu(command-table);
  let decorators = #();
  when (menu)
    for (decorator :: <command-decorator> in menu)
      when ((  decorator-type(decorator) == <command>
	     | decorator-type(decorator) == <function>)
	    & decorator-object(decorator) = command)
	push!(decorators, decorator)
      end
    end;
    for (decorator in decorators)
      remove!(menu, decorator)
    end
  end;
  remhash(command-table-commands(command-table), command)
end method remove-command;
||#

(defmethod remove-command ((command-table <standard-command-table>) command)
  (check-type command <command-oid>)
  ;; Remove old command names
  #|| /* let names = command-table.%command-line-names;
     block (break)
       while (names)
	 let index = find-key(names, method (entry) second(entry) = command end);
	 case
	   index =>
	     names := remove-at!(names, index);
	     inc!(*completion-cache-tick*);
	   otherwise =>
	     break()
	 end
       end
     end; */ ||#
  ;; Remove old menu items
  (let ((menu (command-table-menu command-table))
	(decorators (list)))
    (when menu
      (map nil #'(lambda (decorator)
		   (when (and (or (eql (decorator-type decorator) (find-class '<command>))
				  (eql (decorator-type decorator) (find-class 'function)))
			      (eql (decorator-object decorator) command))
		     (setf decorators (push decorator decorators))))
	   menu)
      (map nil #'(lambda (decorator)
		   (remove decorator menu))
	   decorators))
    (remhash command (command-table-commands command-table))))


#||
define method remove-command
    (command-table :: <standard-command-table>, decorator :: <command-decorator>) => ()
  remove-command(command-table, decorator-object(decorator))
end method remove-command;
||#

(defmethod remove-command ((command-table <standard-command-table>) (decorator <command-decorator>))
  (remove-command command-table (decorator-object decorator)))

#||
/*
define method remove-command-entirely (command :: <command-oid>) => ()
  for (command-table in *command-tables*)
    remove-command(command-table, command)
  end
end method remove-command-entirely;
*/

define method do-command-table-commands
    (function :: <function>, command-table :: <standard-command-table>,
     #key do-inherited? = #f) => ()
  dynamic-extent(function);
  do-command-table-inheritance
    (method (comtab)
       for (command in key-sequence(command-table-commands(comtab)))
	 function(command, comtab)
       end
     end method,
     command-table,
     do-inherited?: do-inherited?)
end method do-command-table-commands;
||#

(defmethod do-command-table-commands ((function function)
				      (command-table <standard-command-table>)
				      &key (do-inherited-p nil))
  (declare (dynamic-extent function))
  (do-command-table-inheritance
      #'(lambda (comtab)
          (loop for command being the hash-keys in (command-table-commands comtab)
	     do (funcall function command comtab)))
    command-table
    :do-inherited-p do-inherited-p))



#||
/// Command table menus

// The radio box and check box types are a bit odd:
//  add-command-table-menu-item
//    (command-table, symbol, <radio-box>, #t,	;or <check-box>
//     items: #[#["True", #t], #["False", #f]],
//     label-key: first, value-key: second,
//     callback: method (g) format-out("Value changed to %=", gadget-value(g)) end)
define method add-command-table-menu-item
    (command-table :: <standard-command-table>, label, type, object,
     #rest keys,
     #key documentation, after = #"end",
	  accelerator = $unsupplied, mnemonic = $unsupplied, resource-id,
          image, text-style, error? = #t,
          items, label-key, value-key, test, callback, update-callback)
 => (decorator :: <command-decorator>)
  dynamic-extent(keys);
  ignore(items, text-style, label-key, value-key, test, callback, update-callback);
  let type
    = type
      | select (object by instance?)
	  <function> =>		 // menu-item "New"  = new-file
	    <function>;
	  <command> =>		 // menu-item "New"  = make(<command>, function: new-file)
	    <command>;
	  <command-table> =>	 // menu-item "File" = *file-command-table*
	    <command-table>;
	  subclass(<command>) => // menu-item "New"  = <new-file-command>
	    <command>;
	  <list> =>		 // menu-item "New"  = (<file-command>, new?: #t)
	    <command>;
	end;
  select (type)
    <command>, <function>, <command-table> =>
      check-type(label, false-or(<string>));
    <menu>, <separator>, <push-box>, <radio-box>, <check-box> =>
      check-type(label, false-or(type-union(<string>, <symbol>)));
  end;
  when (supplied?(accelerator) & accelerator)
    assert(instance?(accelerator, <accelerator>),
	   "%= is not a character or keyboard gesture", accelerator)
  end;
  when (supplied?(mnemonic) & mnemonic)
    assert(instance?(mnemonic, <mnemonic>),
	   "%= is not a character or keyboard gesture", mnemonic)
  end;
  check-type(documentation, false-or(<string>));
  let menu = command-table-menu(command-table);
  let old-item
    = label
      & find-value(menu, method (decorator :: <command-decorator>)
			   label-equal-p(decorator-label(decorator), label)
			 end method);
  when (old-item)
    when (error?)
      cerror("Remove the menu item and proceed",
	     "The menu item %= is already present in %=", label, command-table)
    end;
    remove-command-table-menu-item(command-table, label)
  end;
  command-table.%accelerators := #f;		// decache
  with-keywords-removed (options = keys, #[after:, accelerator:, mnemonic:, error?:])
    let menu      = command-table-menu(command-table);
    let decorator = make(<command-decorator>,
			 object:  object,
			 type:    type,
			 label:   label,
			 image:   image,
			 options: options,
			 accelerator:   accelerator,
			 mnemonic:      mnemonic,
			 documentation: documentation,
			 resource-id:   resource-id);
    select (after)
      #"start" =>
	menu := insert-at!(menu, decorator, #"start");
      #"end", #f =>
	menu := insert-at!(menu, decorator, #"end");
      #"sort" =>
	add!(menu, decorator);
        local method label-less? (x, y) => (less? :: <boolean>)
		case
		  ~x => #t;
		  ~y => #f;
		  otherwise => string-less?(x, y);
		end
	      end method;
	command-table-menu(command-table)
	  := sort!(menu,
		   test: method (x :: <command-decorator>, y :: <command-decorator>)
			   label-less?(decorator-label(x), decorator-label(y))
			 end);
      otherwise =>
	if (instance?(after, <string>))
	  let index
	    = find-key(menu,
		       method (decorator :: <command-decorator>)
			 label-equal-p(decorator-label(decorator), after)
		       end method);
	  if (index)
	    menu := insert-at!(menu, decorator, index)
	  else
	    error("There is no menu item named %= for 'after:'", after)
	  end
	else
          error("The value for 'after:' is not a string, #\"start\", #\"end\", or #\"sort\"")
	end
    end;
    // Might have done something destructive above, so be careful
    command-table-menu(command-table) := menu;
    // Now that the command is accessible via a menu (or accelerator),
    // make sure that we've really imported it
    when (type == <command> | type == <function>)
      let commands = command-table-commands(command-table);
      let old-name = gethash(commands, object);
      gethash(commands, object) := old-name | #t
    end;
    decorator
  end
end method add-command-table-menu-item;
||#

;; The radio box and check box types are a bit odd:
;;  add-command-table-menu-item
;;    (command-table, symbol, <radio-box>, #t,   ;or <check-box>
;;     items: #[#["True", #t], #["False", #f]],
;;     label-key: first, value-key: second,
;;     callback: method(g) format-out("Value changed to %=", gadget-value(g)) end)
(defmethod add-command-table-menu-item ((command-table <standard-command-table>) label type object
					&rest keys
					&key documentation (after :end)
					(accelerator :unsupplied) (mnemonic :unsupplied) resource-id
					image text-style (errorp t)
					items label-key value-key test callback update-callback)
  (declare (dynamic-extent keys)
	   (ignore items text-style
		   label-key value-key
		   test callback
		   update-callback))
  (let ((type 
	 ;; XXX: COND rearranged; if (listp object) = T, then (subtypep object) blows up...
         (or type (cond
		    ((functionp object)                              ;; menu-item "New" = #'new-file
		     (find-class 'function))
                    ((eql (class-of object) (find-class '<command>)) ;; menu-item "New" = (make-instance '<command> :function #'new-file)
		     (find-class '<command>))
		    ((typep object '<command-table>)                 ;; menu-item "File" = *file-command-table*
		     (find-class '<command-table>))
                    ((listp object)                                  ;; menu-item "New" = '(#m<file-command> :new? t)
		     (find-class '<command>))
		    ((subtypep object (find-class '<command>))       ;; menu-item "New" = (find-class '<new-file-command>)
		     (find-class '<command>))))))
    (cond
      ((or (eql type (find-class '<command>))
           (eql type (find-class 'function))
           (eql type (find-class '<command-table>)))
       (check-type label (or null string)))
      ((or (eql type (find-class '<menu>))
           (eql type (find-class '<separator>))
           (eql type (find-class '<push-box>))
           (eql type (find-class '<radio-box>))
           (eql type (find-class '<check-box>)))
       (check-type label (or null string symbol))))
    (when (and (supplied? accelerator) accelerator)
      (unless (typep accelerator '<accelerator>)
        (error "~a is not a character or keyboard gesture" accelerator)))
    (when (and (supplied? mnemonic) mnemonic)
      (unless (typep mnemonic '<mnemonic>)
        (error "~a is not a character or keyboard gesture" mnemonic)))
    (check-type documentation (or null string))
    (let* ((menu (command-table-menu command-table))
	   (old-item (and label (find-value menu #'(lambda (decorator) (label-equal-p (decorator-label decorator) label))))))
      (when old-item
	(when errorp
	  (cerror "Remove the menu item and proceed"
		  "The menu item ~a is already present in ~a" label command-table))
	(remove-command-table-menu-item command-table label))
      (setf (%accelerators command-table) nil)    ; decache
      (with-keywords-removed (options = keys (:after :accelerator :mnemonic :errorp))
        (let ((menu      (command-table-menu command-table))
	      (decorator (make-instance '<command-decorator>
					:object object
					:type   type
					:label  label
					:image  image
					:options options
					:accelerator accelerator
					:mnemonic mnemonic
					:documentation documentation
					:resource-id resource-id)))
	  (case after
	    (:start     (setf menu (insert-at! menu decorator :start)))
	    ((:end nil) (setf menu (insert-at! menu decorator :end)))
	    (:sort      (add! menu decorator)
			(labels ((label-less? (x y)
				   (cond ((not x) t)
					 ((not y) nil)
					 (t       (string-lessp x y)))))
			  (setf (command-table-menu command-table)
				(sort menu
				      #'(lambda (x y)
					  (label-less? (decorator-label x) (decorator-label y)))))))
	    (t          (if (stringp after)
			    (let ((index (find-key menu
						   #'(lambda (decorator)
						       (label-equal-p (decorator-label decorator) after)))))
			      (if index
				  (setf menu (insert-at! menu decorator index))
				  ;; else
				  (error "There is no menu item named ~a for ':after'" after)))
			    ;; else
			    (error "The value for ':after' is not nil, a string, :start, :end, or :sort"))))
	  ;; Might have done something destructive above, so be careful
	  (setf (command-table-menu command-table) menu)
	  ;; Now that the command is accessible via a menu (or accelerator),
	  ;; make sure that we've really imported it
	  (when (or (eql type (find-class '<command>))
		    (eql type (find-class 'function)))
	    (let* ((commands (command-table-commands command-table))
		   (old-name (gethash object commands)))
	      (setf (gethash object commands) (or old-name t))))
	  decorator)))))


#||
define method remove-command-table-menu-item
    (command-table :: <standard-command-table>, label) => ()
  check-type(label, false-or(type-union(<string>, <symbol>)));
  let menu = command-table-menu(command-table);
  let index = find-key(menu,
		       method (decorator :: <command-decorator>)
			 label-equal-p(decorator-label(decorator), label)
		       end method);
  when (index)
    //--- Is it right for this to remove the whole item even
    //--- if there is still a keystroke accelerator?
    menu := remove-at!(menu, index);
    command-table.%accelerators := #f	// decache
  end
end method remove-command-table-menu-item;
||#

(defmethod remove-command-table-menu-item ((command-table <standard-command-table>) label)
  (check-type label (or nil string symbol))
  (let* ((menu  (command-table-menu command-table))
	 (index (position-if #'(lambda (decorator)
				 (label-equal-p (decorator-label decorator) label))
			     menu)))
    (when index
      ;;--- Is it right for this to remove the whole item even
      ;;--- if there is still a keystroke accelerator?
      (setf menu (remove-at! menu index))
      (setf (%accelerators command-table) nil))))  ;  decache


#||
define method do-command-table-menu-items
    (function :: <function>, command-table :: <standard-command-table>,
     #key label, name, do-inherited? = #f) => ()
  dynamic-extent(function);
  let label = label | name;		// for compatibility
  do-command-table-inheritance
    (method (comtab)
       for (decorator :: <command-decorator> in command-table-menu(comtab))
	 when (~label
	       | label-equal-p(decorator-label(decorator), label))
	   function(decorator, comtab)
	 end
       end
     end method,
     command-table,
     do-inherited?: do-inherited?)
end method do-command-table-menu-items;
||#

(defmethod do-command-table-menu-items ((function function) (command-table <standard-command-table>)
					&key label name (do-inherited-p nil))
  (declare (dynamic-extent function))
  (let ((label (or label name)))	;; for compatibility
    (do-command-table-inheritance #'(lambda (comtab)
				      (loop for decorator in (command-table-menu comtab)
					 when (or (not label)
						  (label-equal-p (decorator-label decorator) label))
					 do (funcall function decorator comtab)))
      command-table
      :do-inherited-p do-inherited-p)))


#||
// Starting from the given command table, calls 'function' on all
// of the commands in the command table's menu and all its sub-menus
define method do-command-table-menu-commands
    (function :: <function>, command-table :: <standard-command-table>)
  dynamic-extent(function);
  local method do-one (decorator :: <command-decorator>, comtab)
	  let object = decorator-object(decorator);
	  let type   = decorator-type(decorator);
	  select (type)
	    <function>      => function(object, comtab);
	    <command>       => function(object, comtab);
	    <command-table> => do-command-table-menu-commands(function, object);
	    otherwise       => #f;
	  end
	end method;
  do-command-table-menu-items(do-one, command-table)
end method do-command-table-menu-commands;
||#

;; Starting from the given command table, calls 'function' on all
;; of the commands in the command table's menu and all its sub-menus
(defmethod do-command-table-menu-commands ((function function) (command-table <standard-command-table>))
  (declare (dynamic-extent function))
  (labels ((do-one (decorator comtab)
	     (let ((object (decorator-object decorator))
		   (type   (decorator-type decorator)))
	       (cond
		 ((eql type (find-class 'function))
		  (funcall function object comtab))
		 ((eql type (find-class '<command>))
		  (funcall function object comtab))
		 ((eql type (find-class '<command-table>))
		  (do-command-table-menu-commands function object))
		 (t nil)))))
    (do-command-table-menu-items #'do-one command-table)))


#||
// Menu labels can be #f, but we don't want that to match the string "#f"
define function label-equal? (s1, s2) => (true? :: <boolean>)
  s1 & s2
  & if (instance?(s1, <symbol>) | instance?(s2, <symbol>))
      s1 == s2
    else
      string-equal?(s1, s2)
    end
end function label-equal?;
||#

;; Menu labels can be #f, but we don't want that to match the string "#f"
;; SN:20200702 Is this true in common lisp?
(defun label-equal-p (s1 s2)
  (and s1
       s2
       (if (or (symbolp s1) (symbolp s2))
	   (eql s1 s2)
	   (string-equal s1 s2))))



#||
/// Command menu bars and tool bars

define method make-command-menu-bar
    (framem :: <frame-manager>, frame :: <simple-frame>,
     #key command-table = frame-command-table(frame))
 => (menu-bar :: false-or(<menu-bar>))
  when (command-table)
    with-frame-manager (framem)
      let menus = make-menus-from-command-table(command-table, frame, framem);
      when (menus)
	make(<menu-bar>, children: menus)
      end
    end
  end
end method make-command-menu-bar;
||#

(defmethod make-command-menu-bar ((framem <frame-manager>) (frame <simple-frame>)
				  &key (command-table (frame-command-table frame)))
  (when command-table
    (with-frame-manager (framem)
      (let ((menus (make-menus-from-command-table command-table frame framem)))
	(when menus
	  (make-pane '<menu-bar> :children menus))))))


#||
//---*** If the command table has a resource ID, do we skip all this?
define method make-menus-from-command-table
    (command-table :: <standard-command-table>,
     frame :: false-or(<frame>), framem :: <frame-manager>,
     #key label = "Misc") => (menus :: <sequence>)
  let misc-items :: <stretchy-object-vector> = make(<stretchy-vector>);
  let menus      :: <stretchy-object-vector> = make(<stretchy-vector>);
  for (decorator :: <command-decorator> in command-table-menu(command-table))
    let type = decorator-type(decorator);
    select (type)
      <command-table> =>
	let comtab = decorator-object(decorator);
	let menu   = make-menu-from-command-table-menu
	               (command-table-menu(comtab), frame, framem,
			label:         as(<string>, decorator-label(decorator)),
			mnemonic:      decorator-mnemonic(decorator),
			documentation: decorator-documentation(decorator),
			resource-id:   decorator-resource-id(decorator),
			command-table: comtab,
			use-accelerators?: #t);
	add!(menus, menu);
      <menu> =>
	// Call the menu creation function to create the menu contents
	let menu = make(<menu>,
			children:      decorator-object(decorator)(frame),
			label:         as(<string>, decorator-label(decorator)),
			mnemonic:      decorator-mnemonic(decorator),
			documentation: decorator-documentation(decorator),
			resource-id:   decorator-resource-id(decorator));
	add!(menus, menu);
      otherwise =>
	add!(misc-items, decorator);
    end
  end;
  unless (empty?(misc-items))
    let misc-menu = make-menu-from-command-table-menu(misc-items, frame, framem,
						      label: label,
						      use-accelerators?: #t);
    add!(menus, misc-menu)
  end;
  menus
end method make-menus-from-command-table;
||#

;;---*** If the command table has a resource ID, do we skip all this?
(defmethod make-menus-from-command-table ((command-table <standard-command-table>)
					  (frame null) (framem <frame-manager>)
					  &key (label "Misc"))
  (let ((misc-items (MAKE-STRETCHY-VECTOR))
	(menus      (MAKE-STRETCHY-VECTOR)))
    (loop for decorator across (command-table-menu command-table)
       do (let ((type (decorator-type decorator)))
	    (cond
	      ((eql type (find-class '<command-table>))
	       (let* ((comtab (decorator-object decorator))
		      (menu (make-menu-from-command-table-menu (command-table-menu comtab) frame framem
							       :label (coerce (decorator-label decorator) 'string)
							       :mnemonic (decorator-mnemonic decorator)
							       :documentation (decorator-documentation decorator)
							       :resource-id (decorator-resource-id decorator)
							       :command-table comtab
							       :use-accelerators-p t)))
		 (add! menus menu)))
	      ((eql type (find-class '<menu>))
	       ;; Call the menu creation function to create the menu contents
	       (let ((menu (make-pane '<menu>
				      :children (funcall (decorator-object decorator) frame)
				      :label (coerce (decorator-label decorator) 'string)
				      :mnemonic (decorator-mnemonic decorator)
				      :documentation (decorator-documentation decorator)
				      :resource-id (decorator-resource-id decorator))))
		 (add! menus menu)))
	      (t (add! misc-items decorator)))))
    (unless (empty? misc-items)
      (let ((misc-menu (make-menu-from-command-table-menu misc-items frame framem
							  :label label
							  :use-accelerators-p t)))
	(add! menus misc-menu)))
    menus))

(defmethod make-menus-from-command-table ((command-table <standard-command-table>)
					  (frame <frame>) (framem <frame-manager>)
					  &key (label "Misc"))
  (let ((misc-items (MAKE-STRETCHY-VECTOR))
	(menus      (MAKE-STRETCHY-VECTOR)))
    (loop for decorator across (command-table-menu command-table)
       do (let ((type (decorator-type decorator)))
	    (cond
	      ((eql type (find-class '<command-table>))
	       (let* ((comtab (decorator-object decorator))
		      (menu (make-menu-from-command-table-menu (command-table-menu comtab) frame framem
							       :label (coerce (decorator-label decorator) 'string)
							       :mnemonic (decorator-mnemonic decorator)
							       :documentation (decorator-documentation decorator)
							       :resource-id (decorator-resource-id decorator)
							       :command-table comtab
							       :use-accelerators-p t)))
		 (add! menus menu)))
	      ((eql type (find-class '<menu>))
	       ;; Call the menu creation function to create the menu contents
	       (let ((menu (make-pane '<menu>
				      :children (funcall (decorator-object decorator) frame)
				      :label (coerce (decorator-label decorator) 'string)
				      :mnemonic (decorator-mnemonic decorator)
				      :documentation (decorator-documentation decorator)
				      :resource-id (decorator-resource-id decorator))))
		 (add! menus menu)))
	      (t (add! misc-items decorator)))))
    (unless (empty? misc-items)
      (let ((misc-menu (make-menu-from-command-table-menu misc-items frame framem
							  :label label
							  :use-accelerators-p t)))
	(add! menus misc-menu)))
    menus))


#||
define method make-menu-from-command-table-menu
    (decorators :: <sequence>,
     frame :: false-or(<frame>), framem :: <frame-manager>,
     #key owner, command-table, label, documentation, resource-id,
	  mnemonic = $unsupplied, item-callback = $unsupplied,
	  use-accelerators? = #f)
 => (menu :: <menu>)
  let menu-items :: <stretchy-object-vector> = make(<stretchy-vector>);
  with-frame-manager (framem)
    local method button-for-command
	      (decorator :: <command-decorator>) => (button :: <menu-button>)
	    let command = decorator-object(decorator);
	    let (callback, command) = callback-for-command(command);
            make(<push-menu-button>,
		 command:       command,
		 label:         as(<string>, decorator-label(decorator)),
		 value:         command,
		 accelerator:   use-accelerators? & decorator-accelerator(decorator),
		 mnemonic:      decorator-mnemonic(decorator),
		 documentation: decorator-documentation(decorator),
		 resource-id:   decorator-resource-id(decorator),
		 enabled?: ~frame | command-enabled?(command, frame),
		 activate-callback:
		   if (supplied?(item-callback)) item-callback
		   else callback end)
          end method,
	  method menu-for-command-table
	      (decorator :: <command-decorator>) => (menu :: <menu>)
	    let comtab = decorator-object(decorator);
            make-menu-from-command-table-menu
              (command-table-menu(comtab), frame, framem,
               label:         as(<string>, decorator-label(decorator)),
	       mnemonic:      decorator-mnemonic(decorator),
	       documentation: decorator-documentation(decorator),
               resource-id:   decorator-resource-id(decorator),
	       command-table: comtab,
	       use-accelerators?: use-accelerators?)
	  end method,
	  // Example:
          //  add-command-table-menu-item
          //    (command-table, symbol, <radio-box>, #f,
          //     items: #[#["Direct methods", #f], #["All methods", #t]],
          //     label-key: first, value-key: second,
          //     callback: method (g) sheet-frame(g).all-methods? := gadget-value(g) end)
	  method component-for-box
	      (decorator :: <command-decorator>, selection-mode)
	   => (menu-box :: <menu-box>)
	    let options  = decorator-options(decorator);
	    let items    = get-property(options, items:, default: #());
	    let callback = get-property(options, callback:);
	    let label-key
	      = get-property(options, label-key:, default: collection-gadget-default-label-key);
	    let value-key
	      = get-property(options, value-key:, default: collection-gadget-default-value-key);
	    let test = get-property(options, test:, default: \==);
	    let update-callback = get-property(options, update-callback:);
	    let callback-args
	      = if (selection-mode == #"none")
		  vector(activate-callback: callback)
		else
		  vector(value-changed-callback: callback)
		end;
	    apply(make, <menu-box>,
		  command:         callback,
		  items:           items,
		  selection-mode:  selection-mode,
		  resource-id:     decorator-resource-id(decorator),
		  value:           decorator-object(decorator),
		  label-key:       label-key,
		  value-key:       value-key, test: test,
		  update-callback: update-callback,
		  callback-args)
	  end method,
          method collect-buttons
	      (buttons :: <stretchy-object-vector>) => ()
            unless (empty?(buttons))
              add!(menu-items,
                   make(<push-menu-box>,
			children: as(<simple-vector>, buttons)));
              buttons.size := 0
            end
          end method;
    let buttons :: <stretchy-object-vector> = make(<stretchy-vector>);
    local method do-decorators (decorators :: <sequence>) => ()
	    for (decorator :: <command-decorator> in decorators)
	      let type = decorator-type(decorator);
	      select (type)
		<function>, <command> =>
		  add!(buttons, button-for-command(decorator));
		<command-table> =>
		  add!(buttons, menu-for-command-table(decorator));
		<menu> =>
		  // Call the menu creation function to create the menu contents
		  let menu = make(<menu>,
				  children:      decorator-object(decorator)(frame),
				  label:         as(<string>, decorator-label(decorator)),
				  mnemonic:      decorator-mnemonic(decorator),
				  documentation: decorator-documentation(decorator),
				  resource-id:   decorator-resource-id(decorator));
		  add!(buttons, menu);
		<separator> =>
		  collect-buttons(buttons);
                  // If the separator specifies a command table, that means we
                  // should "inline" it in the current menu right after the separator
                  when (instance?(decorator-object(decorator), <command-table>))
                    do-decorators(command-table-menu(decorator-object(decorator)))
                  end;
		<radio-box> =>
		  collect-buttons(buttons);
		  add!(menu-items, component-for-box(decorator, #"single"));
		<check-box> =>
		  collect-buttons(buttons);
		  add!(menu-items, component-for-box(decorator, #"multiple"));
		<push-box> =>
		  collect-buttons(buttons);
		  add!(menu-items, component-for-box(decorator, #"none"));
	      end
	    end
          end method;
    do-decorators(decorators);
    make(<menu>,
         owner: owner,
	 label: label,
	 command: command-table,
	 mnemonic: mnemonic,
	 documentation: documentation,
	 resource-id: resource-id,
	 // No need to group the last buttons...
	 children: concatenate(menu-items, buttons))
  end
end method make-menu-from-command-table-menu;
||#

(defmethod make-menu-from-command-table-menu ((decorators sequence)
					      (frame null) (framem <frame-manager>)
					      &key owner command-table label documentation resource-id
					      (mnemonic :unsupplied) (item-callback :unsupplied)
					      (use-accelerators-p nil))
  (declare (ignorable owner command-table label
		      documentation resource-id
		      mnemonic item-callback
		      use-accelerators-p))
  (let ((menu-items (MAKE-STRETCHY-VECTOR)))
    (with-frame-manager (framem)
      (labels ((button-for-command (decorator)
		 (check-type decorator <command-decorator>)
		 (let ((command (decorator-object decorator)))
		   (multiple-value-bind (callback command)
		       (callback-for-command command)
		     (make-pane '<push-menu-button>
				:command command
				:label (coerce (decorator-label decorator) 'string)
				:value command
				:accelerator (and use-accelerators-p (decorator-accelerator decorator))
				:mnemonic (decorator-mnemonic decorator)
				:documentation (decorator-documentation decorator)
				:resource-id (decorator-resource-id decorator)
				:enabledp (or (not frame) (command-enabled-p command frame))
				:activate-callback (if (supplied? item-callback) item-callback callback)))))
	       (menu-for-command-table (decorator)
		   (check-type decorator <command-decorator>)
		   (let ((comtab (decorator-object decorator)))
		     (make-menu-from-command-table-menu (command-table-menu comtab) frame framem
							:label (coerce (decorator-label decorator) 'string)
							:mnemonic (decorator-mnemonic decorator)
							:documentation (decorator-documentation decorator)
							:resource-id (decorator-resource-id decorator)
							:command-table comtab
							:use-accelerators-p use-accelerators-p)))
	       ;; Example:
	       ;;  add-command-table-menu-item
	       ;;    (command-table, symbol, <radio-box>, #f,
	       ;;     items: #[#["Direct methods", #f], #["All methods", #t]],
	       ;;     label-key: first, value-key: second,
	       ;;     callback: method (g) sheet-frame(g).all-methods? := gadget-value(g) end)
	       (component-for-box (decorator selection-mode)
		 (check-type decorator <command-decorator>)
		 (let* ((options (decorator-options decorator))
			(items (getf options :items (list)))
			(callback (getf options :callback))
			(label-key (getf options :label-key #'collection-gadget-default-label-key))
			(value-key (getf options :value-key #'collection-gadget-default-value-key))
			(test (getf options :test #'eql))
			(update-callback (getf options :update-callback))
			(callback-args (if (eql selection-mode :none)
					   (list :activate-callback callback)
					   (list :value-changed-callback callback))))
		   (apply #'make-pane '<menu-box>
			  :command         callback
			  :items           items
			  :selection-mode  selection-mode
			  :resource-id     (decorator-resource-id decorator)
			  :value           (decorator-object decorator)
			  :label-key       label-key
			  :value-key       value-key
			  :test            test
			  :update-callback update-callback
			  callback-args)))
	       (collect-buttons (buttons)
		 (check-type buttons array)   ;; <stretchy-vector>
		 (unless (empty? buttons)
		   (add! menu-items (make-pane '<push-menu-box>
					       :children (MAKE-SIMPLE-VECTOR :contents buttons)))
		   (setf (size buttons) 0))))
	(let ((buttons (MAKE-STRETCHY-VECTOR)))
	  (labels ((do-decorators (decorators)
		     (check-type decorators array)
		     (loop for decorator across decorators
			do (let ((type (decorator-type decorator)))
			     (cond
			       ((or (eql type (find-class 'function))
				    (eql type (find-class '<command>)))
				(setf buttons (add! buttons (button-for-command decorator))))
			       ((eql type (find-class '<command-table>))
				(add! buttons (menu-for-command-table decorator)))
			       ((eql type (find-class '<menu>))
				;; Call the menu creation function to create the menu contents
				(let ((menu (make-pane '<menu>
						       :children (funcall (decorator-object decorator) frame)
						       :label (coerce (decorator-label decorator) 'string)
						       :mnemonic (decorator-mnemonic decorator)
						       :documentation (decorator-documentation decorator)
						       :resource-id (decorator-resource-id decorator))))
				  (add! buttons menu)))
			       ((eql type (find-class '<separator>))
				(collect-buttons buttons)
				;; If the separator specifies a command table, that means we
				;; should "inline" it in the current menu right after the separator
				(when (typep (decorator-object decorator) '<command-table>)
				  (do-decorators (command-table-menu (decorator-object decorator)))))
			       ((eql type (find-class '<radio-box>))
				(collect-buttons buttons)
				(add! menu-items (component-for-box decorator :single)))
			       ((eql type (find-class '<check-box>))
				(collect-buttons buttons)
				(add! menu-items (component-for-box decorator :multiple)))
			       ((eql type (find-class '<push-box>))
				(collect-buttons buttons)
				(add! menu-items (component-for-box decorator :none))))))))
	    (do-decorators decorators)
	    (let ((result (make-pane '<menu>
				     :owner         owner
				     :label         label
				     :command       command-table
				     :mnemonic      mnemonic
				     :documentation documentation
				     :resource-id   resource-id
				     ;; No need to group the last buttons...
				     :children (concatenate 'vector menu-items buttons))))
	      result)))))))

(defmethod make-menu-from-command-table-menu ((decorators sequence)
					      (frame <frame>) (framem <frame-manager>)
					      &key owner command-table label documentation resource-id
					      (mnemonic :unsupplied) (item-callback :unsupplied)
					      (use-accelerators-p nil))
  (declare (ignorable owner command-table label
		      documentation resource-id
		      mnemonic item-callback
		      use-accelerators-p))
  (let ((menu-items (MAKE-STRETCHY-VECTOR)))
    (with-frame-manager (framem)
      (labels ((button-for-command (decorator)
		 (check-type decorator <command-decorator>)
		 (let ((command (decorator-object decorator)))
		   (multiple-value-bind (callback command)
		       (callback-for-command command)
		     (make-pane '<push-menu-button>
				:command command
				:label (coerce (decorator-label decorator) 'string)
				:value command
				:accelerator (and use-accelerators-p (decorator-accelerator decorator))
				:mnemonic (decorator-mnemonic decorator)
				:documentation (decorator-documentation decorator)
				:resource-id (decorator-resource-id decorator)
				:enabledp (or (not frame) (command-enabled-p command frame))
				:activate-callback (if (supplied? item-callback) item-callback callback)))))
	       (menu-for-command-table (decorator)
		   (check-type decorator <command-decorator>)
		   (let ((comtab (decorator-object decorator)))
		     (make-menu-from-command-table-menu (command-table-menu comtab) frame framem
							:label (coerce (decorator-label decorator) 'string)
							:mnemonic (decorator-mnemonic decorator)
							:documentation (decorator-documentation decorator)
							:resource-id (decorator-resource-id decorator)
							:command-table comtab
							:use-accelerators-p use-accelerators-p)))
	       ;; Example:
	       ;;  add-command-table-menu-item
	       ;;    (command-table, symbol, <radio-box>, #f,
	       ;;     items: #[#["Direct methods", #f], #["All methods", #t]],
	       ;;     label-key: first, value-key: second,
	       ;;     callback: method (g) sheet-frame(g).all-methods? := gadget-value(g) end)
	       (component-for-box (decorator selection-mode)
		 (check-type decorator <command-decorator>)
		 (let* ((options (decorator-options decorator))
			(items (getf options :items (list)))
			(callback (getf options :callback))
			(label-key (getf options :label-key #'collection-gadget-default-label-key))
			(value-key (getf options :value-key #'collection-gadget-default-value-key))
			(test (getf options :test #'eql))
			(update-callback (getf options :update-callback))
			(callback-args (if (eql selection-mode :none)
					   (list :activate-callback callback)
					   (list :value-changed-callback callback))))
		   (apply #'make-pane '<menu-box>
			  :command         callback
			  :items           items
			  :selection-mode  selection-mode
			  :resource-id     (decorator-resource-id decorator)
			  :value           (decorator-object decorator)
			  :label-key       label-key
			  :value-key       value-key
			  :test            test
			  :update-callback update-callback
			  callback-args)))
	       (collect-buttons (buttons)
		 (check-type buttons array)   ;; <stretchy-vector>
		 (unless (empty? buttons)
		   (add! menu-items (make-pane '<push-menu-box>
					       :children (MAKE-SIMPLE-VECTOR :contents buttons)))
		   (setf (size buttons) 0))))
	(let ((buttons (MAKE-STRETCHY-VECTOR)))
	  (labels ((do-decorators (decorators)
		     (check-type decorators array)
		     (loop for decorator across decorators
			do (let ((type (decorator-type decorator)))
			     (cond
			       ((or (eql type (find-class 'function))
				    (eql type (find-class '<command>)))
				(setf buttons (add! buttons (button-for-command decorator))))
			       ((eql type (find-class '<command-table>))
				(add! buttons (menu-for-command-table decorator)))
			       ((eql type (find-class '<menu>))
				;; Call the menu creation function to create the menu contents
				(let ((menu (make-pane '<menu>
						       :children (funcall (decorator-object decorator) frame)
						       :label (coerce (decorator-label decorator) 'string)
						       :mnemonic (decorator-mnemonic decorator)
						       :documentation (decorator-documentation decorator)
						       :resource-id (decorator-resource-id decorator))))
				  (add! buttons menu)))
			       ((eql type (find-class '<separator>))
				(collect-buttons buttons)
				;; If the separator specifies a command table, that means we
				;; should "inline" it in the current menu right after the separator
				(when (typep (decorator-object decorator) '<command-table>)
				  (do-decorators (command-table-menu (decorator-object decorator)))))
			       ((eql type (find-class '<radio-box>))
				(collect-buttons buttons)
				(add! menu-items (component-for-box decorator :single)))
			       ((eql type (find-class '<check-box>))
				(collect-buttons buttons)
				(add! menu-items (component-for-box decorator :multiple)))
			       ((eql type (find-class '<push-box>))
				(collect-buttons buttons)
				(add! menu-items (component-for-box decorator :none))))))))
	    (do-decorators decorators)
	    (let ((result (make-pane '<menu>
				     :owner         owner
				     :label         label
				     :command       command-table
				     :mnemonic      mnemonic
				     :documentation documentation
				     :resource-id   resource-id
				     ;; No need to group the last buttons...
				     :children (concatenate 'vector menu-items buttons))))
	      result)))))))


#||
define method make-command-tool-bar
    (framem :: <frame-manager>, frame :: <simple-frame>,
     #key command-table = frame-command-table(frame))
 => (tool-bar :: false-or(<tool-bar>))
  when (command-table)
    // An alist of #[command-table buttons]
    // We do this in order to flatten out any command table inheritance,
    // since tool bars are not hierarchical
    let comtab-buttons :: <stretchy-object-vector> = make(<stretchy-vector>);
    with-frame-manager (framem)
      local method button-for-command
		(decorator :: <command-decorator>) => (button :: <button>)
	      let command = decorator-object(decorator);
	      let (callback, command) = callback-for-command(command);
	      make(<push-button>,
		   command:     command,
		   value:       command,
		   label:       decorator-image(decorator),
		   accelerator: decorator-accelerator(decorator),
		   mnemonic:    decorator-mnemonic(decorator),
		   // The documentation is typically used as a "tool tip",
		   // so we use the item's label instead of the item's
		   // documentation since it looks so much better
		   documentation:     as(<string>, decorator-label(decorator)),
		   enabled?:          ~frame | command-enabled?(command, frame),
		   activate-callback: callback)
	    end method,
	    method component-for-box
		(decorator :: <command-decorator>, selection-mode)
	     => (menu-box :: <menu-box>)
	      let options  = decorator-options(decorator);
	      let items    = get-property(options, items:, default: #());
	      let callback = get-property(options, callback:);
	      let label-key
		= get-property(options, label-key:,
			       default: collection-gadget-default-label-key);
	      let value-key
		= get-property(options, value-key:, 
			       default: collection-gadget-default-value-key);
	      let test = get-property(options, test:, default: \==);
	      make(<menu-box>,
		   command:     callback,
		   items:       items,
		   selection-mode: selection-mode,
		   resource-id: decorator-resource-id(decorator),
		   value:       decorator-object(decorator),
		   label-key:   label-key,
		   value-key:   value-key,
		   test:        test,
		   value-changed-callback: callback)
	    end method,
	    method collect-buttons (command-table :: <standard-command-table>) => ()
	      let entry = find-pair(comtab-buttons, command-table);
	      unless (entry)		// do each command table only once
		let buttons = make(<stretchy-vector>);
		add!(comtab-buttons, vector(command-table, buttons));
		for (decorator :: <command-decorator> in command-table-menu(command-table))
		  let type  = decorator-type(decorator);
		  select (type)
		    <function>, <command> =>
		      when (decorator-image(decorator))
			add!(buttons, button-for-command(decorator))
		      end;
		    <command-table> =>
		      collect-buttons(decorator-object(decorator));
		    <menu> =>
		      error("Menus not supported in command-table tool bars");
		    <separator> =>
		      when (instance?(decorator-object(decorator), <command-table>))
			collect-buttons(decorator-object(decorator))
		      end;
		    <radio-box> =>
		      when (decorator-image(decorator))
			add!(buttons, component-for-box(decorator, #"single"))
		      end;
		    <check-box> =>
		      when (decorator-image(decorator))
			add!(buttons, component-for-box(decorator, #"multiple"))
		      end;
		    <push-box> =>
		      when (decorator-image(decorator))
			add!(buttons, component-for-box(decorator, #"none"))
		      end;
		  end
	        end
	      end
	    end method;
      collect-buttons(command-table);
      // Each command table gets its own closely spaced vbox
      let boxes = make(<stretchy-vector>);
      for (entry in comtab-buttons)
        let buttons = entry[1];
        unless (empty?(buttons))
          add!(boxes, make(<row-layout>,
			   children: buttons,
      			   x-spacing: 0))
        end
      end;
      // Now make the tool bar with a little space between vboxes
      make(<tool-bar>,
	   child: make(<row-layout>,
		       children: boxes,
		       x-spacing: 2))
    end
  end
end method make-command-tool-bar;
||#

(defmethod make-command-tool-bar ((framem <frame-manager>) (frame <simple-frame>)
				  &key (command-table (frame-command-table frame)))
  (when command-table
    ;; An alist of #[command-table buttons]
    ;; We do this in order to flatten out any command table inheritance,
    ;; since tool bars are not hierarchical
    (let ((comtab-buttons (MAKE-STRETCHY-VECTOR)))
      (with-frame-manager (framem)
        (labels ((button-for-command (decorator)
		   (let ((command (decorator-object decorator)))
		     (multiple-value-bind (callback command)
			 (callback-for-command command)
		       (make-pane '<push-button>
				  :command     command
				  :value       command
				  :label       (decorator-image decorator)
				  :accelerator (decorator-accelerator decorator)
				  :mnemonic (decorator-mnemonic decorator)
				  ;; The documentation is typically used as a "tool tip",
				  ;; so we use the item's label instead of the item's
				  ;; documentation since it looks so much better
				  :documentation (coerce (decorator-label decorator) 'string)
				  :enabledp (or (not frame) (command-enabled-p command frame))
				  :activate-callback callback))))
		 (component-for-box (decorator selection-mode)
		   (let* ((options (decorator-options decorator))
			  (items (getf options :items (list)))
			  (callback (getf options :callback))
			  (label-key (getf options :label-key #'collection-gadget-default-label-key))
			  (value-key (getf options :value-key #'collection-gadget-default-value-key))
			  (test (getf options :test 'eql)))
		     (make-pane '<menu-box>
				:command        callback
				:items          items
				:selection-mode selection-mode
				:resource-id    (decorator-resource-id decorator)
				:value          (decorator-object decorator)
				:label-key      label-key
				:value-key      value-key
				:test           test
				:value-changed-callback callback)))
		 (collect-buttons (command-table)
		   (let ((entry (find-pair comtab-buttons command-table)))
		     (unless entry      ;; do each command table only once
		       (let ((buttons (MAKE-STRETCHY-VECTOR)))
			 (add! comtab-buttons (vector command-table buttons))
			 (loop for decorator across (command-table-menu command-table)
			    do (let ((type (decorator-type decorator)))
				 (cond
				   ((or (eql type (find-class 'function))
					(eql type (find-class '<command>)))
				    (when (decorator-image decorator)
				      (add! buttons (button-for-command decorator))))
				   ((eql type (find-class '<command-table>))
				    (collect-buttons (decorator-object decorator)))
				   ((eql type (find-class '<menu>))
				    (error "Menus not supported in command-table tool bars"))
				   ((eql type (find-class '<separator>))
				    (when (typep (decorator-object decorator) '<command-table>)
				      (collect-buttons (decorator-object decorator))))
				   ((eql type (find-class '<radio-box>))
				    (when (decorator-image decorator)
				      (add! buttons (component-for-box decorator :single))))
				   ((eql type (find-class '<check-box>))
				    (when (decorator-image decorator)
				      (add! buttons (component-for-box decorator :multiple))))
				   ((eql type (find-class '<push-box>))
				    (when (decorator-image decorator)
				      (add! buttons (component-for-box decorator :none))))))))))))
	  (collect-buttons command-table)
	  ;; Each command table gets its own closely spaced vbox
	  (let ((boxes (MAKE-STRETCHY-VECTOR)))
	    (loop for entry across comtab-buttons
	       do (let ((buttons (aref entry 1)))
		    (unless (empty? buttons)
		      (add! boxes (make-pane '<row-layout>
					     :children buttons
					     :x-spacing 0)))))
	    ;; Now make the tool bar with a little space between vboxes
	    (make-pane '<tool-bar>
		       :child (make-pane '<row-layout>
					 :children boxes
					 :x-spacing 2))))))))


#||
define method do-frame-commands
    (function :: <function>, frame :: <frame>,
     #key test :: <function> = identity,
          menu-bar? = #t, tool-bar? = #t, owned-menus? = #t)
 => ()
  local method maybe-do-sheet-command (sheet) => ()
	  when (gadget?(sheet))
	    let command = gadget-command(sheet);
	    if (command & test(command))
	      function(sheet, command)
	    end
	  end
	end method maybe-do-sheet-command;
  when (menu-bar?)
    let menu-bar = frame-menu-bar(frame);
    when (menu-bar)
      do-sheet-tree(maybe-do-sheet-command, menu-bar)
    end
  end;
  when (tool-bar?)
    let tool-bar = frame-tool-bar(frame);
    when (tool-bar)
      do-sheet-tree(maybe-do-sheet-command, tool-bar)
    end
  end;
  when (owned-menus?)
    for (menu in frame-owned-menus(frame))
      do-sheet-tree(maybe-do-sheet-command, menu)
    end
  end
end method do-frame-commands;
||#

(defmethod do-frame-commands ((function function) (frame <frame>)
			      &key (test #'identity)
			      (menu-bar-p t) (tool-bar-p t) (owned-menus-p t))
  (labels ((maybe-do-sheet-command (sheet)
	     (when (gadgetp sheet)
	       (let ((command (gadget-command sheet)))
		 (when (and command (funcall test command))
		   (funcall function sheet command))))))
    (when menu-bar-p
      (let ((menu-bar (frame-menu-bar frame)))
	(when menu-bar
	  (do-sheet-tree #'maybe-do-sheet-command menu-bar))))
    (when tool-bar-p
      (let ((tool-bar (frame-tool-bar frame)))
	(when tool-bar
	  (do-sheet-tree #'maybe-do-sheet-command tool-bar))))
    (when owned-menus-p
      (map nil #'(lambda (menu)
		   (do-sheet-tree #'maybe-do-sheet-command menu))
	   (frame-owned-menus frame)))))


#||
// Map over all gadgets corresponding to a command.  Command might be a
// command, a command function, or even a command table.  Note that this
// won't find sub-menus that were added with a type of <menu>.
define method do-command-menu-gadgets
    (function :: <function>, frame :: <frame>,
     command :: type-union(<command>, <function>, <command-table>),
     #key menu-bar? = #t, tool-bar? = #t, owned-menus? = #t)
 => ()
  do-frame-commands
    (method (gadget :: <gadget>, gcommand)
       if (gcommand = command
	     | (instance?(gcommand, <functional-command>)
		  & command-function(gcommand) = command))
	 function(gadget)
       end
     end, frame,
     menu-bar?: menu-bar?, tool-bar?: tool-bar?, owned-menus?: owned-menus?)
end method do-command-menu-gadgets;
||#

;; Map over all gadgets corresponding to a command. Command might be a
;; command, a command function, or even a command table. Note that this
;; won't find sub-menus that were added with a type of <menu>.
(defgeneric do-command-menu-gadgets (function frame command
				     &key menu-bar-p tool-bar-p owned-menus-p))

(defmethod do-command-menu-gadgets ((function function) (frame <frame>) (command <command>)
				    &key (menu-bar-p t) (tool-bar-p t) (owned-menus-p t))
  (do-frame-commands #'(lambda (gadget gcommand)
			 (check-type gadget <gadget>)
			 (when (or (eql gcommand command)
				   (and (typep gcommand '<functional-command>)
					(eql (command-function gcommand) command)))
			   (funcall function gadget)))
    frame :menu-bar-p menu-bar-p :tool-bar-p tool-bar-p :owned-menus-p owned-menus-p))

(defmethod do-command-menu-gadgets ((function function) (frame <frame>) (command function)
				    &key (menu-bar-p t) (tool-bar-p t) (owned-menus-p t))
  (do-frame-commands #'(lambda (gadget gcommand)
			 (check-type gadget <gadget>)
			 (when (or (eql gcommand command)
				   (and (typep gcommand '<functional-command>)
					(eql (command-function gcommand) command)))
			   (funcall function gadget)))
    frame :menu-bar-p menu-bar-p :tool-bar-p tool-bar-p :owned-menus-p owned-menus-p))

(defmethod do-command-menu-gadgets ((function function) (frame <frame>) (command <command-table>)
				    &key (menu-bar-p t) (tool-bar-p t) (owned-menus-p t))
  (do-frame-commands #'(lambda (gadget gcommand)
			 (check-type gadget <gadget>)
			 (when (or (eql gcommand command)
				   (and (typep gcommand '<functional-command>)
					(eql (command-function gcommand) command)))
			   (funcall function gadget)))
    frame :menu-bar-p menu-bar-p :tool-bar-p tool-bar-p :owned-menus-p owned-menus-p))


#||
define method do-command-menu-gadgets
    (function :: <function>, frame :: <frame>,
     command :: subclass(<command>),
     #key menu-bar? = #t, tool-bar? = #t, owned-menus? = #t)
 => ()
  do-frame-commands
    (method (gadget :: <gadget>, gcommand)
       if (gcommand = command
	     | (instance?(gcommand, <class>)
		  & subtype?(gcommand, command))
	     | (instance?(gcommand, <list>)
		  & subtype?(head(gcommand), command)))
	 function(gadget)
       end
     end, frame,
     menu-bar?: menu-bar?, tool-bar?: tool-bar?, owned-menus?: owned-menus?)
end method do-command-menu-gadgets;
||#

(defmethod do-command-menu-gadgets ((function function) (frame <frame>)
				    (command class)
				    &key (menu-bar-p t) (tool-bar-p t) (owned-menus-p t))
  (if (subtypep command (find-class '<command>))
      (do-frame-commands #'(lambda (gadget gcommand)
			     (check-type gadget <gadget>)
			     (when (or (eql gcommand command)
				       (and (typep gcommand 'class)
					    (subtypep gcommand command))
				       (and (listp gcommand)
					    (subtypep (car gcommand) command)))
			       (funcall function gadget)))
	frame :menu-bar-p menu-bar-p :tool-bar-p tool-bar-p :owned-menus-p owned-menus-p)
      ;; else
      (error "No applicable method for DO-COMMAND-MENU-GADGETS when invoked with 'command' ~a" command)))



#||

/// Keystroke accelerators

define method do-command-table-accelerators
    (function :: <function>, command-table :: <standard-command-table>,
     #key accelerator, do-inherited? = #f) => ()
  dynamic-extent(function);
  do-command-table-inheritance
    (method (comtab)
       for (decorator :: <command-decorator> in command-table-menu(comtab))
	 when (decorator-accelerator(decorator)
	       & (~accelerator
		  | decorator-accelerator(decorator) = accelerator))
	   function(decorator, comtab)
	 end
       end
     end method,
     command-table,
     do-inherited?: do-inherited?)
end method do-command-table-accelerators;
||#

(defmethod do-command-table-accelerators ((function function) (command-table <standard-command-table>)
					  &key accelerator (do-inherited-p nil))
  (declare (dynamic-extent function))
  (do-command-table-inheritance #'(lambda (comtab)
				    (loop for decorator in (command-table-menu comtab)
				       when (and (decorator-accelerator decorator)
						 (or (not accelerator)
						     (eql (decorator-accelerator decorator) accelerator)))
				       do (funcall function decorator comtab)))
    command-table :do-inherited-p do-inherited-p))


#||
define method command-table-accelerators
    (command-table :: <standard-command-table>)
 => (accelerators :: <vector>)
  command-table.%accelerators
  | begin
      let accelerators :: <stretchy-object-vector> = make(<stretchy-vector>);
      for (decorator :: <command-decorator> in command-table-menu(command-table))
	let accelerator = decorator-accelerator(decorator);
	when (supplied?(accelerator) & accelerator)
	  add!(accelerators, accelerator)
	end
      end;
      command-table.%accelerators := accelerators;
      accelerators
    end
end method command-table-accelerators;
||#

(defmethod command-table-accelerators ((command-table <standard-command-table>))
  (or (%accelerators command-table)
      (let ((accelerators (MAKE-STRETCHY-VECTOR)))
	(map nil #'(lambda (decorator)
		     (let ((accelerator (decorator-accelerator decorator)))
		       (when (and (supplied? accelerator) accelerator)
			 (add! accelerators accelerator))))
	     (command-table-menu command-table))
	(setf (%accelerators command-table) accelerators)
	accelerators)))



#||
/// Stubs for command line names

define open generic add-command-line-name
    (command-table :: <command-table>, command, command-name) => ();
||#

(defgeneric add-command-line-name (command-table command command-name))


#||
define open generic remove-command-line-name
    (command-table :: <command-table>, command-name) => ();
||#

(defgeneric remove-command-line-name (command-table command-name))


#||
define open generic do-command-line-names
    (function :: <function>, command-table :: <command-table>, #key do-inherited?) => ();
||#

(defgeneric do-command-line-names (function command-table &key do-inherited-p))


#||
/*
//--- All of this belongs in a command-line layer

define variable *completion-cache-tick*  :: <integer> = 0;

define method add-command-line-name
    (command-table :: <standard-command-table>, command :: <command-oid>, command-name) => ()
  let commands = command-table-commands(command-table);
  // Make the command directly accessible in the command table, but
  // don't change the primary name if there already is one
  unless (instance?(gethash(commands, command), <string>))
    gethash(commands, command) := command-name
  end;
  //--- Use binary insertion on command line names (completion aarrays)
  remove-command-line-name(command-table, command-name);
  let names
    = command-table.%command-line-names
      | (command-table.%command-line-names := make(<stretchy-vector>));
  add!(names, list(command-name, command));
  command-table.%command-line-names
    := sort!(names,
             test: method (x, y)
                     string-less?(first(x), first(y))
                   end);
  inc!(*completion-cache-tick*)
end method add-command-line-name;

define method remove-command-line-name
    (command-table :: <standard-command-table>, command-name) => ()
  //--- Use binary deletion on command line names (completion aarrays)
  let names = command-table.%command-line-names;
  let index = names
	      & find-key(names,
			 method (entry) string-equal?(first(entry), command-name) end);
  when (index)
    let command = second(command-table.names[index]);
    let commands = command-table-commands(command-table);
    when (string-equal?(command-name, gethash(commands, command)))
      // Remove the primary command-line name for this command,
      // but don't claim that the command does not exist
      gethash(commands, command) := #t
    end;
    remove-at!(names, index);
    inc!(*completion-cache-tick*)
  end
end method remove-command-line-name;

define method do-command-line-names
    (function :: <function>, command-table :: <standard-command-table>,
     #key label, name, do-inherited? = #f) => ()
  dynamic-extent(function);
  let label = label | name;		// for compatibility
  do-command-table-inheritance
    (method (comtab)
       let names = command-table.%command-line-names;
       when (names)
	 for (entry in names)
	   when (~label
		 | method (entry) string-equal?(first(entry), label))
	     function(first(entry), second(entry), comtab)
	   end
	 end
       end
     end method,
     command-table,
     do-inherited?: do-inherited?)
end method do-command-line-names;

define method command-line-name-for-command
    (command, command-table :: <standard-command-table>, #key error? = #t)
 => (command-name)
  block (return)
    do-command-table-inheritance
      (method (comtab)
	 let command-name = gethash(command-table-commands(comtab), command);
	 when (instance?(command-name, <string>))
	   return(command-name)
	 end
       end method,
       command-table);
    select (error?)
      #f => #f;
      #"create" => command-name-from-command(command);	//---*** this can't work
      otherwise =>
	error("The command %= has no command line name in %=", command, command-table)
    end
  end
end method command-line-name-for-command;

define method command-table-completion-alist
    (command-table :: <standard-command-table>) => (alist :: <vector>)
  when (~command-table.%completion-alist
	| (*completion-cache-tick* > command-table.%completion-alist-tick))
    command-table.%completion-alist := make(<stretchy-vector>);
    do-command-table-inheritance
      (method (comtab)
	 let names = command-table.%command-line-names;
	 when (names)
	   for (entry in names)
	     add!(command-table.%completion-alist, entry)
	   end
	 end
       end method,
       command-table);
    command-table.%completion-alist
      := remove-duplicates!(command-table.%completion-alist,
			    test: method (x, y) second(x) == second(y) end);
    command-table.%completion-alist
      := sort!(command-table.%completion-alist,
	       test: method (x, y) string-less?(first(x), first(y)) end);
    command-table.%completion-alist-tick := *completion-cache-tick*
  end;
  command-table.%completion-alist
end command-table-completion-alist;
*/
||#


#||
/// Stubs for presentation translators

define open generic add-presentation-translator
    (command-table :: <command-table>, translator, #key error?) => ();
||#

(defgeneric add-presentation-translator (command-table translator &key error?))


#||
define open generic remove-presentation-translator
    (command-table :: <command-table>, translator-name) => ();
||#

(defgeneric remove-presentation-translator (command-table translator-name))


#||
define open generic do-presentation-translators
    (function :: <function>, command-table :: <command-table>, #key do-inherited?) => ();
||#

(defgeneric do-presentation-translators (function command-table &key do-inherited-p))


#||
/*
//--- All of this belongs in a presentation layer

define variable *translators-cache-tick* :: <integer> = 0;

define method add-presentation-translator
    (command-table :: <standard-command-table>, translator,
     #key error? = #t) => ()
  let translator-name = presentation-translator-name(translator);
  let translators
    = command-table-translators(command-table)
      | (command-table-translators(command-table) := make(<stretchy-vector>));
  let place 
    = find-key(translators,
	       method (entry) presentation-translator-name(entry) == translator-name end);
  case
    place =>
      when (error?)
	cerror("Remove the translator and proceed",
	       "The translator %= is already present in %=", 
	       presentation-translator-name(translator), command-table)
      end;
      translators[place] := translator;
    otherwise =>
      add!(translators, translator)
  end;
  // Maybe one day we'll do incremental updating of the cache.  That day
  // is not today.
  inc!(*translators-cache-tick*)
end method add-presentation-translator;

define method remove-presentation-translator
    (command-table :: <standard-command-table>, translator-name) => ()
  let translators = command-table-translators(command-table);
  when (translators)
    let translator
      = if (instance?(translator-name, <symbol>))
	  find-value(command-table-translators(comtab),
		     method (entry) 
		       presentation-translator-name(entry) == translator-name
		     end)
	else
	  translator-name
	end;
    remove!(translators, translator);
    inc!(*translators-cache-tick*)
  end
end method remove-presentation-translator;

define method do-presentation-translators
    (function :: <function>, command-table :: <standard-command-table>,
     #key name, do-inherited? = #f) => ()
  dynamic-extent(function);
  do-command-table-inheritance
    (method (comtab)
       let translators = command-table-translators(comtab);
       when (translators)
	 for (translator in translators)
	   when (~name
		 | translator-name(translator) == name)
	     function(translator, comtab)
	   end
	 end
       end
     end method,
     command-table,
     do-inherited?: do-inherited?)
end method do-presentation-translators;

define method remove-presentation-translator-entirely (name) => ()
  for (command-table in *command-tables*)
    remove-presentation-translator(command-table, name)
  end
end method remove-presentation-translator-entirely;

define method clear-presentation-translator-caches () => ()
  for (command-table in *command-tables*)
    when (command-table.%translators-cache)
      remove-all-keys!(command-table.%translators-cache)
    end
  end;
  inc!(*translators-cache-tick*)
end method clear-presentation-translator-caches;
*/
||#


#||
/// Simple command-defining macrology

/*
//---*** Not yet...

define sealed class <command-argument> (<object>)
  sealed slot command-argument-name, required-init-keyword: name:;
  sealed slot command-argument-type, required-init-keyword: type:;
  sealed slot command-argument-default = #f, init-keyword: default:;
  sealed slot command-argument-provide-default? = #t, init-keyword: provide-default?:;
  sealed slot command-argument-prompt = #f, init-keyword: prompt:;
end class <command-argument>;

define sealed domain make (singleton(<command-argument>));
define sealed domain initialize (<command-argument>);

// For example,
//   define command hardcopy-file
//       (file :: <locator>;
//        printer :: <printer> = $unsupplied, prompt: "printer";
//        copies :: <integer> = 1, prompt: "copies")
//       (command-table: *print-comtab*)
//     do-hardcopy-file(file, printer: printer, copies: copies)
//   end command hardcopy-file;
define macro command-definer
  { define command ?:name (?arguments:*) (#rest ?options:expression) ?:body end }
    => { define command-method ?name (?arguments) ?body end;
         define command-parser ?name (?arguments) (?options) end; }
end macro command-definer;

// Define the method for the command, discarding argument options
define macro command-method-definer
  { define command-method ?:name (?arguments:*) ?:body end }
    => { define method ?name (?arguments) ?body end }
 arguments:
  { } => { }
  // Map over semicolon-separated arguments, reducing each to a
  // parameter list entry.  When we get to the first argument that
  // has a default, insert '#key' into the parameter list
  { ?:variable = ?default:expression, #rest ?options:expression; ?key-arguments }
    => { #key ?variable = ?default, ?key-arguments }
  { ?argument; ... } => { ?argument, ... }
 key-arguments:
  { } => { }
  { ?key-argument; ... } => { ?key-argument, ... }
 argument:
  { ?:variable, #rest ?options:expression }
    => { ?variable }
 key-argument:
  { ?:variable = ?default:expression, #rest ?options:expression }
    => { ?variable = ?default }
end macro command-method-definer;

// Define the method for the command, discarding argument options
define macro command-parser-definer
  { define command-parser ?:name (?arguments:*) (#rest ?options:expression) end }
    => { install-command(?name, vector(?arguments), ?options) }
 arguments:
  { } => { }
  { ?argument; ... } => { ?argument, ... }
 argument:
  { ?:name :: ?type:expression = ?default:expression, #rest ?options:expression }
    => { make(<command-argument>,
	      name: ?"name", type: ?type,
	      default: ?default, ?options) }
  { ?:name :: ?type:expression, #rest ?options:expression }
    => { make(<command-argument>,
	      name: ?"name", type: ?type,
	      ?options) }
end macro command-parser-definer;

define method install-command
    (command, argument-info,
     #key command-table, name, menu, image,
	  accelerator = $unsupplied, mnemonic = $unsupplied) => ()
  when (command-table)
    add-command(command-table, command,
		name: name, menu: menu,
		image: image,
		accelerator: accelerator, mnemonic: mnemonic,
		error?: #f)
  end
end method install-command;
*/
||#

