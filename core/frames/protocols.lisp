;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-FRAMES-INTERNALS -*-
(in-package #:duim-frames-internals)

;;; From 'contain.lisp'

(define-protocol <<contain-protocol>> ()
  (:function contain (object &key &allow-other-keys)
	     (:documentation
"
Creates and returns a frame containing _object_. This function is
intended to be used as a convenience function when testing sections of
code in development; you are not recommended to use it in your final
source code. The function wraps a set of DUIM objects in a frame and
displays them on screen, without you needing to worry about the
creation, management, or display of frames on the computer screen. The
'contain' function is most useful when testing code interactively
using the Dylan interactor.

If _own-thread?_ is t, then the window that is created by 'contain'
runs in its own thread. If not supplied, _own-thread?_ is nil.

Consider the following expression that calls 'contain':

    (contain (make-pane '<button>))

This is equivalent to the fuller expression:

    (let ((frame (make-instance '<simple-frame>
                                :title \"Container\"
                                :layout
                                (make-pane '<button>))))
      (start-frame frame))

As can be seen, when testing short pieces of code interactively in the
environment, the former section of code is easier to use than the
latter.

Example:

Assigning the result of a contain expression allows you to manipulate
the DUIM objects being contained interactively, as shown in the
example below.

You should assume the following code is typed into the Dylan
Interactor, and that each expression is evaluated by pressing the
RETURN key at the points indicated.

    (defparameter *g*
                  (contain (make-pane '<list-box>
                                      :items (list :one :two :three)
                                      :label-key
                                      #'(lambda (symbol)
                                          (string-downcase
                                            (symbol-name symbol))))))
    RETURN

    (gadget-items *g*) RETURN

As you would expect, evaluating the call to 'gadget-items' returns the
following result:

    (:one :two :three)

TODO: check this example works

In a similar way, you can destructively modify the slot values of any
contained DUIM objects.
"))
  (:function make-container (object &key &allow-other-keys)))


;;; From 'dialogs.lisp'

(define-protocol <<dialog-protocol>> ()
  (:getter dialog-image (dialog))
  (:getter (setf dialog-image) (image dialog))
  (:getter dialog-exit-button (dialog)
	   (:documentation
"
Returns the Exit button in _dialog_. The Exit button is commonly found
in multi-page dialogs, where the user is given the option to exit the
sequence at any point (as well as navigate through the sequence using
Next and Back buttons).

Defined by: <<dialog-protocol>>
"))
  (:setter (setf dialog-exit-button) (button dialog)
	   (:documentation
"
Sets the Exit button in _dialog_. The Exit button is commonly found in
muti-page dialogs, where the user is given the option to exit the
sequence at any point (as well as navigate through the sequence using
Next and Back buttons).

Example:

In the following example, a simple dialog frame is created, and then
its exit button is redefined before the dialog is displayed on screen.

    (defparameter *dialog*
                  (make-instance '<dialog-frame>
                                 :exit-button? t
                                 :cancel-button? t
                                 :help-callback
                                 #'(lambda (gadget)
                                     (notify-user
                                       (format nil
                                               \"Here is some help\")
                                       :owner gadget))))

    (setf (dialog-exit-button *dialog*)
          (make-pane '<push-button> :label \"Yes\"
                     :activate-callback #'exit-dialog
                     :max-width +fill+))

    (start-frame *dialog*)

Defined by: <<dialog-protocol>>
"))
  (:getter dialog-cancel-button (dialog)
	   (:documentation
"
Returns the Cancel button in _dialog_.

Defined by: <<dialog-protocol>>
"))
  (:setter (setf dialog-cancel-button) (button dialog)
	   (:documentation
"
Specifies the Cancel button in _dialog_.

Example:

In the following example, a simple dialog frame is created, and then
its cancel button is redefined before the dialog is displayed on
screen.

    (defparameter *dialog*
                  (make-instance '<dialog-frame>
                                 :exit-button? t
                                 :cancel-button? t
                                 :help-callback
                                 #'(lambda (gadget)
                                     (notify-user
                                       (format nil
                                               \"Here is some help\")
                                       :owner gadget))))

    (setf (dialog-cancel-button *dialog*)
          (make-pane '<push-button> :label \"No\"
                     :activate-callback #'cancel-dialog
                     :max-width +fill+))

    (start-frame *dialog*)

Defined by: <<dialog-protocol>>
"))
  (:getter dialog-apply-callback (dialog)
	   (:documentation
"
Returns the callback invoked when the Apply button is clicked in
_dialog_. As well as having OK and Cancel buttons, many dialogs also
have an Apply button that lets the user apply the changes that have
been made in the dialog, without removing the dialog from the screen
itself.

Note: If you supply nil as the callback, then the button does not
appear.

Defined by: <<dialog-protocol>>
"))
  (:getter dialog-apply-button (dialog)
	   (:documentation
"
Returns the Apply button in _dialog_. As well as having OK and Cancel
buttons, many dialogs also have an Apply button that lets the user
apply the changes that have been made in the dialog, without removing
the dialog from the screen itself.

Defined by: <<dialog-protocol>>
"))
  (:setter (setf dialog-apply-button) (button dialog)
	   (:documentation
"
Specifies the Apply button in _dialog_. As well as having OK and Cancel
buttons, many dialogs also have an Apply button that lets the user
apply the changes that have been made in the dialog, without removing
the dialog from the screen itself.

Defined by: <<dialog-protocol>>
"))
  (:getter dialog-back-callback (dialog)
	   (:documentation
"
Returns the callback invoked when the Back button is clicked in
_dialog_. This is most useful in wizard frames, which typically have
Back and Next buttons that let the user navigate forward and backward
through the sequence of pages that comprise the dialog.

Note: If you do not explicitly supply this callback, the previous page
in the sequence for the multi-page dialog is displayed when the Back
button is clicked. Specifying your own callback gives you flexibility
in describing how the user can navigate through the sequence of pages
in the dialog.

Defined by: <<dialog-protocol>>
"))
  (:getter dialog-back-button (dialog)
	   (:documentation
"
Returns the Back button in _dialog_. This is most useful in multi-page
dialogs such as property frames and wizard frames, which typically
have Back and Next buttons that let the user navigate forward and
backward through the sequence of pages that comprise the dialog.

Defined by: <<dialog-protocol>>
"))
  (:setter (setf dialog-back-button) (button dialog)
	   (:documentation
"
Sets the Back button in _dialog_. This is most useful in multi-page
dialogs such as property frames and wizard frames, which typically
have Back and Next buttons that let the user navigate forward and
backward through the sequence of pages that comprise the dialog.

Defined by: <<dialog-protocol>>
"))
  (:getter dialog-next-callback (dialog)
	   (:documentation
"
Returns the callback invoked when the Next button is clicked in
dialog. This is most useful in multi-page dialogs such as property
frames and wizard frames, which typically have Back and Next buttons
that let the user navigate forward and backward through the sequence
of pages that comprise the dialog.

Note: If you do not explicitly supply this callback, the next page in
the sequence for the multi-page dialog is displayed when the Next
button is clicked. Specifying your own callback gives you flexibility
in describing how the user can navigate through the sequence of pages
in the dialog.

The default value for this callback is 'move-to-next-page'.

Defined by: <<dialog-protocol>>
"))
  (:getter dialog-next-button (dialog)
	   (:documentation
"
Returns the Next button in _dialog_. This is most useful in multi-page
dialogs such as property frames and wizard frames, which typically
have Back and Next buttons that let the user navigate forward and
backward through the sequence of pages that comprise the dialog.

Defined by: <<dialog-protocol>>
"))
  (:setter (setf dialog-next-button) (button dialog)
	   (:documentation
"
Specifies the Next button in dialog. This is most useful in multi-page
dialogs such as property frames and wizard frames, which typically
have Back and Next buttons that let the user navigate forward and
backward through the sequence of pages that comprise the dialog.

Defined by: <<dialog-protocol>>
"))
  (:getter dialog-help-callback (dialog)
	   (:documentation
"
Returns the callback invoked when the Help button is clicked in
_dialog_. Many dialogs contain a Help button that, when clicked,
displays a relevant topic from the online help system for the
application.

Note: You must specify this callback in order to create a Help button
in any dialog. If the callback is nil, then there will be no Help
button present in the dialog.

Defined by: <<dialog-protocol>>
"))
  (:getter dialog-help-button (dialog)
	   (:documentation
"
Returns the Help button in _dialog_. Many dialogs contain a Help
button that, when clicked, displays a relevant topic from the online
help system for the application.

Defined by: <<dialog-protocol>>
"))
  (:setter (setf dialog-help-button) (button dialog)
	   (:documentation
"
Specifies the Help button in _dialog_. Many dialogs contain a Help
button that, when clicked, displays a relevant topic from the online
help system for the application.

Example:

In the following example, a simple dialog frame is created, and then
its help button is redefined before the dialog is displayed on screen.

    (defparameter *dialog*
                  (make-instance '<dialog-frame>
                                 :exit-button? t
                                 :cancel-button? t
                                 :help-callback
                                 #'(lambda (gadget)
                                     (notify-user
                                       \"Here is some help\"
                                       :owner gadget))))

    (setf (dialog-help-button *dialog*)
          (make-pane '<push-button> :label \"Help Me!\"
                     :activate-callback
                     #'(lambda (gadget)
                         (notify-user
                           \"Here is some other help\"
                           :owner gadget))
                     :max-width +fill+))

    (start-frame *dialog*)

Defined by: <<dialog-protocol>>
"))
  (:getter dialog-current-page (frame)
	   (:documentation
"
Returns the current page in _dialog_.

Defined by: <<dialog-protocol>>
"))
  (:setter (setf dialog-current-page) (page frame)
	   (:documentation
"
Sets the current page in _dialog_.

Defined by: <<dialog-protocol>>
"))
  (:getter dialog-next-page (frame)
	   (:documentation
"
Returns the next page in sequence for dialog. This is for use in
multi-page dialogs such as property frames and wizard frames, which
typically have Back and Next buttons that let the user navigate
forward and backward through the sequence of pages that comprise the
dialog.

The default method for the Next button in dialog uses the value of
this function. When the Next button is clicked, the current page is
set to the next logical page in the sequence, but you are free to
dynamically change it as the state of the dialog changes.

Defined by: <<dialog-protocol>>
"))
  (:setter (setf dialog-next-page) (page frame)
	   (:documentation
"
Specifies the next page in sequence for dialog. This is for use in
multi-page dialogs such as property frames and wizard frames, which
typically have Back and Next buttons that let the user navigate
forward and backward through the sequence of pages that comprise the
dialog.

The default method for the Next button in dialog uses the value of
this function. When the Next button is clicked, the current page is
set to the next logical page in the sequence, but you are free to
dynamically change it as the state of the dialog changes.

Defined by: <<dialog-protocol>>
"))
  (:getter dialog-previous-page (frame)
	   (:documentation
"
Returns the previous page in sequence for _dialog_. This is for use in
multi-page dialogs such as property frames and wizard frames, which
typically have Back and Next buttons that let the user navigate
forward and backward through the sequence of pages that comprise the
dialog.

The default method for the Back button in _dialog_ uses the value of
this function. When the Back button is clicked, the current page is
set to the previous logical page in the sequence, but you are free to
dynamically change it as the state of the dialog changes.

Defined by: <<dialog-protocol>>
"))
  (:setter (setf dialog-previous-page) (page frame)
	   (:documentation
"
Specifies the previous page in sequence for _dialog_. This is for use
in multi-page dialogs such as property frames and wizard frames, which
typically have Back and Next buttons that let the user navigate
forward and backward through the sequence of pages that comprise the
dialog.

The default method for the Back button in _dialog_ uses the value of
this function. When the Back button is clicked, the current page is
set to the previous logical page in the sequence, but you are free to
dynamically change it as the state of the dialog changes.

Defined by: <<dialog-protocol>>
"))
  (:getter dialog-pages (frame)
	   (:documentation
"
Returns the pages of _dialog_. Each of the items in sequence is an
instance of <page>.

Defined by: <<dialog-protocol>>
"))
  (:setter (setf dialog-pages) (pages frame)
	   (:documentation
"
Sets the pages of _dialog_. Each of the items in sequence must be an
instance of <page>.

Defined by: <<dialog-protocol>>
"))
  (:getter dialog-page-complete? (frame)
	   (:documentation
"
Returns true if all the information required on the current page in
_dialog_ has been specified by the user. This generic function has two
uses:

    * It can be used within wizards to test whether all the necessary
      information has been supplied, before moving on to the next page
      of the wizard.
    * It can be used within property pages to test whether all the
      necessary information has been supplied, before allowing the
      user to apply any changes. 

Defined by: <<dialog-protocol>>
"))
  (:setter (setf dialog-page-complete?) (complete? frame)
	   (:documentation
"
Sets the slot that indicates all the information required on the
current page in _dialog_ has been specified by the user. This generic
function has two uses:

    * It can be used within wizards to indicate that the necessary
      information has been supplied, so that the next page of the
      wizard can be displayed safely.
    * It can be used within property pages to indicate that the
      necessary information has been supplied, so that the user can
      apply any changes. 

Defined by: <<dialog-protocol>>
")))

(define-protocol <<wizard-protocol>> (<<dialog-protocol>>)
  (:function compute-next-page (dialog)
	     (:documentation
"
Returns the next page in _dialog_, which must be a wizard.
"))
  (:function compute-previous-page (dialog)
	     (:documentation
"
Returns the previous page in _dialog_, which must be a wizard.
")))


;;; From 'frames.lisp'

(define-protocol <<frame-protocol>> ()
  (:getter frame-layout (frame)
	   (:documentation
"
Returns the layout used in _frame_.

Defined by: <<frame-protocol>>
"))
  (:setter (setf frame-layout) (layout frame)
	   (:documentation
"
Specifies the layout used in _frame_.

Defined by: <<frame-protocol>>
"))
  (:function layout-frame (frame &key width height)
	     (:documentation
"
Resizes the frame and lays out the current pane hierarchy according to
the layout protocol. This function is automatically invoked on a frame
when it is adopted, after its pane hierarchy has been generated.

If _width_ and _height_ are provided, then this function resizes the
frame to the specified size. It is an error to provide just _width_.

If no optional arguments are provided, this function resizes the frame
to the preferred size of the top-level pane as determined by the space
composition pass of the layout protocol.

In either case, after the frame is resized, the space allocation pass
of the layout protocol is invoked on the top-level pane.

Defined by: <<frame-protocol>>
"))
  (:getter frame-owner (frame)
	   (:documentation
"
Returns the controlling frame for _frame_. The controlling frame for
any hierarchy of existing frames is the one that owns the thread in
which the frames are running. Thus, the controlling frame for _frame_
is not necessarily its direct owner: it may be the owner of _frame_'s
owner, and so on, depending on the depth of the hierarchy.

Defined by: <<frame-protocol>>
"))
  (:getter frame-owned-frames (frame))
  (:getter frame-owned-menus (frame))
  (:getter frame-menu-bar (frame)
	   (:documentation
"
Returns the menu bar used in _frame_.

Defined by: <<frame-protocol>>
"))
  (:setter (setf frame-menu-bar) (menu-bar frame)
	   (:documentation
"
Sets the menu bar used in _frame_.

Defined by: <<frame-protocol>>
"))
  (:getter frame-tool-bar (frame)
	   (:documentation
"
Returns the tool bar used in _frame_.

Defined by: <<frame-protocol>>
"))
  (:setter (setf frame-tool-bar) (tool-bar frame)
	   (:documentation
"
Sets the tool bar used in _frame_.

Defined by: <<frame-protocol>>
"))
  (:getter frame-status-bar (frame)
	   (:documentation
"
Returns the status bar used in _frame_.

Defined by: <<frame-protocol>>
"))
  (:setter (setf frame-status-bar) (status-bar frame)
	   (:documentation
"
Sets the status bar used in _frame_.

Defined by: <<frame-protocol>>
"))
  (:getter frame-status-message (frame)
	   (:documentation
"
Returns the status message for _frame_. This is the label in the
status bar for the frame. If the frame has no status bar, or if the
label is not set, this this function returns false.

Defined by: <<frame-protocol>>
"))
  (:setter (setf frame-status-message) (message frame)
	   (:documentation
"
Sets the status message for _frame_. This is the label in the status
bar for the frame. If the frame has no status bar, then attempting to
set the label fails silently.

Defined by: <<frame-protocol>>
"))
;;  getter frame-input-focus		// defined in DUIM-sheets
;;    (frame :: <abstract-frame>) => (sheet :: false-or(<abstract-sheet>));
;;  setter frame-input-focus-setter	// defined in DUIM-sheets
;;    (sheet :: false-or(<abstract-sheet>), frame :: <abstract-frame>)
;; => (sheet :: false-or(<abstract-sheet>));
  (:getter frame-command-table (frame)
	   (:documentation
"
Returns the command table associated with _frame_.

Defined by: <<frame-protocol>>
"))
  (:setter (setf frame-command-table) (command-table frame)
	   (:documentation
"
Specifies the command table associated with _frame_.

Defined by: <<frame-protocol>>
"))
  (:function note-command-table-changed (framem frame))
  (:function handle-id-activation (frame id))
  (:function frame-top-level-sheet-class (frame &key &allow-other-keys))
  (:function frame-wrapper (framem frame sheet))
  (:getter frame-resource-id (frame))
  (:function start-frame (frame)
	     (:documentation
"
Starts _frame_, optionally setting the _owner_ of the frame and the
_mode_ in which it will run.

The function 'start-frame' dynamically binds an <abort> restart around
the event loop for the frame that is started. The restart allows the
event loop to be re-entered, and enables any callbacks run from the
frame to signal an <abort> (via the 'abort' function, for instance),
in order to terminate execution of the current callback and return to
event processing. This facility is useful for implementing operations
that cancel gestures and for debugging DUIM applications from Dylan
debuggers.

Example

The following example creates a simple frame, then displays it. You
should run this code in the interactor, pressing the RETURN key at the
points indicated.

    (defparameter *frame*
                  (make-instance '<simple-frame> :title \"A frame\"
                                 :layout
                                 (make-pane '<button>))) RETURN

    (start-frame *frame*) RETURN
"))
  (:function port-start-frame (port frame))
  (:function make-event-queue (framem frame))
  (:function frame-needs-event-queue? (framem frame &key mode))
  (:function frame-can-exit? (frame)
	     (:documentation
"
Returns true if _frame_ can be exited dynamically. You can add methods
to this generic function in order to allow the user to make a dynamic
decision about whether a frame should exit.

Example

    (defmethod frame-can-exit? ((frame <abstract-test-frame>))
      (notify-user \"Really exit?\"
                   :frame frame
                   :style :question))

Defined by: <<frame-protocol>>
"))
  (:function frame-top-level (frame)
	     (:documentation
"
Returns the top level loop function for _frame_. The top level loop
function for a frame is the \"command loop\" for the frame.

The default method for 'frame-top-level' calls 'read-event' and then
'handle-event'.
"))
  ;; Enabling and disabling a whole frame (for modal dialog parents)
  (:getter frame-enabled? (frame))
  (:setter (setf frame-enabled?) (enabled? frame))
  (:function note-frame-enabled (framem frame))
  (:function note-frame-disabled (framem frame))
  ;; Iconifying, etc
  (:function frame-iconified? (frame))
  (:function (setf frame-iconified?) (iconified? frame))
  (:function frame-maximized? (frame))
  (:function (setf frame-maximized?) (maximized? frame))
  (:function raise-frame (frame &key activate?)
	     (:documentation
"
Raises _frame_ to the top of the stack of visible windows. After
calling this function, _frame_ will appear above any occluding windows
that may be on the screen.

Example

The following example creates and displays a simple frame, then lowers
and raises it. You should run this code in the interactor, pressing
the RETURN key at the points indicated.

    (defparameter *frame*
                  (make-instance '<simple-frame> :title \"A frame\"
                                 :layout
                                 (make-pane '<button>))) RETURN

    (start-frame *frame*) RETURN
    (lower-frame *frame*) RETURN
    (raise-frame *frame*) RETURN
"))
  (:function lower-frame (frame)
	     (:documentation
"
Lowers _frame_ to the bottom of the stack of visible windows. After
calling this function, _frame_ will appear beneath any occluding
windows that may be on the screen.

Example

The following example creates and displays a simple frame, then lowers
it.

    (defparameter *frame*
                  (make-instance '<simple-frame>
                                 :title \"A frame\"
                                 :layout
                                 (make-pane '<button>)))

    (start-frame *frame*)
    (lower-frame *frame*)

Defined by <<frame-protocol>>
"))
  ;; For exit buttons, etc
  (:getter frame-default-button (frame)
	   (:documentation
"
Returns the default button associated with _frame_.

Defined by: <<frame-protocol>>
"))
  (:function (setf frame-default-button) (button frame)
	     (:documentation
"
Sets the default button associated with _frame_.

Defined by: <<frame-protocol>>
"))
  (:getter frame-accelerators (frame)
	   (:documentation
"
Returns the keyboard accelerators defined for _frame_.

Defined by: <<frame-protocol>>
"))
  (:function (setf frame-accelerators) (accelerators frame)
	     (:documentation
"
Defines the keyboard accelerators for _frame_.

Defined by: <<frame-protocol>>
"))
  (:function note-accelerators-changed (framem frame))
  ;; User frame classes can use this to watch transition in 'frame-state',
  ;; 'frame-iconified?', and 'frame-maximized?'
  (:function note-frame-state-changed (frame old-state new-state))
  ;; User frame classes can add methods to these, provided the methods
  ;; start with a call to 'next-method'
  (:function note-frame-mapped (framem frame))
  (:function note-frame-unmapped (framem frame))
  (:function note-frame-iconified (framem frame))
  (:function note-frame-deiconified (framem frame))
  (:function note-frame-maximized (framem frame))
  (:function note-frame-unmaximized (framem frame))
  ;; These communicate to the back-end to do some layout work
  (:function update-frame-layout (framem frame))
  (:function update-frame-wrapper (framem frame))
  (:function note-frame-title-changed (framem frame))
  (:function note-frame-icon-changed (framem frame))
  ;; The most basic support for documents
  (:getter frame-document (frame))
  (:setter (setf frame-document) (document frame))
  ;; Is this frame covered by any other window?
  (:function frame-occluded? (frame))
  (:function do-frame-occluded? (framem frame)))


;;; From 'help.lisp'

(define-protocol <<help-protocol>> ()
  (:getter help-source (command))
  (:getter help-secondary-window (command))
  (:getter help-pane (command))
  (:getter help-context (command))
  (:getter help-topic-id (command))
  (:getter help-popup? (command))
  (:getter help-keyword (command))
  (:getter help-macro (command))
  (:getter help-window-region (command))
  ;; Initialization
  (:function initialize-help-pane (command frame))
  (:function initialize-help (command frame))
  ;; Glue to commands
  (:function display-help (framem frame command))
  ;; Glue to frames
  (:function frame-help-source-locator (frame source))
  (:function frame-help-source (frame command))
  (:function frame-help-context (frame command))
  (:function frame-help-topic-id (frame command))
  (:function frame-help-keyword (frame command)))


;;; From 'standard-commands.lisp'

(define-protocol <<document>> ()
  (:function new-document (frame))
  (:function open-document (frame &key document))
  (:function close-document (frame &key document))
  (:function save-document (frame &key document))
  (:function save-document-as (frame &key document))
  (:function save-all-documents (frame))
  (:function note-document-changed (frame document)))
