;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-FRAMES-INTERNALS -*-
(in-package :duim-frames-internals)

;;; Decorators for commands, command tables, menus, etc.

#||
define sealed class <command-decorator> (<command>)
  // The object being decorated and its intentional type
  sealed constant slot decorator-object,
    required-init-keyword: object:;
  sealed constant slot decorator-type,
    required-init-keyword: type:;
  // The label to be used in a menu or menu bar...
  sealed constant slot decorator-label :: false-or(<string>) = #f,
    init-keyword: label:;
  // The label to be used in a tool bar...
  sealed constant slot decorator-image :: false-or(type-union(<string>, <image>)) = #f,
    init-keyword: image:;
  // Other properties
  sealed constant slot decorator-accelerator = $unsupplied,
    init-keyword: accelerator:;
  sealed constant slot decorator-mnemonic = $unsupplied,
    init-keyword: mnemonic:;
  sealed constant slot decorator-documentation :: false-or(<string>) = #f,
    init-keyword: documentation:;
  sealed constant slot decorator-options = #(),
    init-keyword: options:;
  sealed constant slot decorator-resource-id = #f,
    init-keyword: resource-id:;
end class <command-decorator>;
||#


(defclass <command-decorator> (<command>)
  ;; The object being decorated and its intentional type
  ((decorator-object :initarg :object :initform (required-slot ":object" "<command-decorator>") :reader decorator-object)
   ;; Note: this is the appropriate built-in or standard class for the
   ;; type of decorator...
   (decorator-type :initarg :type :initform (required-slot ":type" "<command-decorator>") :reader decorator-type)
   ;; The label to be used in a menu or menu bar...
   (decorator-label :type (or null string) :initarg :label :initform nil :reader decorator-label)
   ;; The label to be used in a tool bar...
   (decorator-image :type (or null string <image>) :initarg :image :initform nil :reader decorator-image)
   ;; Other properties
   (decorator-accelerator :initarg :accelerator :initform :unsupplied :reader decorator-accelerator)
   (decorator-mnemonic :initarg :mnemonic :initform :unsupplied :reader decorator-mnemonic)
   (decorator-documentation :type (or null string) :initarg :documentation :initform nil :reader decorator-documentation)
   (decorator-options :initarg :options :initform () :reader decorator-options)
   (decorator-resource-id :initarg :resource-id :initform nil :reader decorator-resource-id))
  (:documentation "Adds UI information to decorate a command object."))


#||
/// Trampolines from decorators to commands

define method execute-command-type
    (decorator :: <command-decorator>, #rest initargs,
     #key client, server, invoker, results-to)
 => (results :: false-or(<command-results>))
  ignore(client, server, invoker, results-to);
  ensure-command-decorator(decorator);
  apply(execute-command-type, decorator-object(decorator), initargs)
end method execute-command-type;
||#

(defmethod execute-command-type ((decorator <command-decorator>) &rest initargs
				 &key client server invoker results-to)
  (declare (ignore client server invoker results-to))
  (ensure-command-decorator decorator)
  (apply #'execute-command-type (decorator-object decorator) initargs))


#||
define sealed method execute-command
    (decorator :: <command-decorator>) => (#rest values)
  ensure-command-decorator(decorator);
  execute-command(decorator-object(decorator))
end method execute-command;
||#

(defmethod execute-command ((decorator <command-decorator>))
  (ensure-command-decorator decorator)
  (execute-command (decorator-object decorator)))


#||
define sealed method do-execute-command
    (server, decorator :: <command-decorator>) => (#rest values)
  ensure-command-decorator(decorator);
  do-execute-command(server, decorator-object(decorator))
end method do-execute-command;
||#

(defmethod do-execute-command (server (decorator <command-decorator>))
  (ensure-command-decorator decorator)
  (do-execute-command server (decorator-object decorator)))


#||
define sealed method command-undoable?
    (decorator :: <command-decorator>) => (undoable? :: <boolean>)
  ensure-command-decorator(decorator);
  command-undoable?(decorator-object(decorator))
end method command-undoable?;
||#

(defmethod command-undoable-p ((decorator <command-decorator>))
  (ensure-command-decorator decorator)
  (command-undoable-p (decorator-object decorator)))


#||
define sealed method undo-command
    (decorator :: <command-decorator>) => (#rest values)
  ensure-command-decorator(decorator);
  undo-command(decorator-object(decorator))
end method undo-command;
||#

(defmethod undo-command ((decorator <command-decorator>))
  (ensure-command-decorator decorator)
  (undo-command (decorator-object decorator)))


#||
define sealed method redo-command
    (decorator :: <command-decorator>) => (#rest values)
  ensure-command-decorator(decorator);
  redo-command(decorator-object(decorator))
end method redo-command;
||#

(defmethod redo-command ((decorator <command-decorator>))
  (ensure-command-decorator decorator)
  (redo-command (decorator-object decorator)))


#||
define inline function ensure-command-decorator
    (decorator :: <command-decorator>) => ()
  let type = decorator-type(decorator);
  assert(type == <command> | type == <function>,
	 "Trying to treat a non-command decorator as a command")
end function ensure-command-decorator;
||#

(defun ensure-command-decorator (decorator)
  (check-type decorator <command-decorator>)
  (let ((type (decorator-type decorator)))
    (unless (or (eql type (find-class '<command>)) (eql type (find-class 'function)))
      (error "Trying to treat a non-command decorator as a command"))))
