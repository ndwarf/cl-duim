;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-FRAMES-INTERNALS -*-
(in-package #:duim-frames-internals)

#||
/// Standard commands and command tables

/// "File"

// The document protocol, such as it is
define protocol <<document>> ()
  function new-document
    (frame :: <abstract-frame>) => (#rest values);
  function open-document
    (frame :: <abstract-frame>, #key document) => (#rest values);
  function close-document
    (frame :: <abstract-frame>, #key document) => (#rest values);
  function save-document
    (frame :: <abstract-frame>, #key document) => (#rest values);
  function save-document-as
    (frame :: <abstract-frame>, #key document) => (#rest values);
  function save-all-documents
    (frame :: <abstract-frame>) => (#rest values);
  function note-document-changed
    (frame :: <abstract-frame>, document :: <object>) => ();
end protocol <<document>>;
||#
#||
(define-protocol <<document>> ()
  (:function new-document (frame))
  (:function open-document (frame &key document))
  (:function close-document (frame &key document))
  (:function save-document (frame &key document))
  (:function save-document-as (frame &key document))
  (:function save-all-documents (frame))
  (:function note-document-changed (frame document)))
||#
#||
define command-table *standard-file-command-table* (*global-command-table*)
  menu-item "New..."     = new-document;
  menu-item "Open..."    = open-document;
  menu-item "Close"      = close-document;
  separator;
  menu-item "Save"       = save-document;
  menu-item "Save As..." = save-document-as;
  menu-item "Save All"   = save-all-documents;
  separator;
  menu-item "Exit"       = exit-frame;
end command-table *standard-file-command-table*;
||#

(define-command-table *standard-file-command-table* (*global-command-table*)
  ((:menu-item "New..."     = #'new-document)
   (:menu-item "Open..."    = #'open-document)
   (:menu-item "Close"      = #'close-document)
   :separator
   (:menu-item "Save"       = #'save-document)
   (:menu-item "Save As..." = #'save-document-as)
   (:menu-item "Save All"   = #'save-all-documents)
   :separator
   (:menu-item "Exit"       = #'exit-frame)))


#||
/// "Edit"

define open generic command-undo
    (frame :: <abstract-frame>) => (#rest values);
||#

(defgeneric command-undo (frame))


#||
define open generic command-redo
    (frame :: <abstract-frame>) => (#rest values);
||#

(defgeneric command-redo (frame))


#||
define open generic clipboard-cut
    (frame :: <abstract-frame>) => (#rest values);
||#

(defgeneric clipboard-cut (frame))


#||
define open generic clipboard-copy
    (frame :: <abstract-frame>) => (#rest values);
||#

(defgeneric clipboard-copy (frame))


#||
define open generic clipboard-paste
    (frame :: <abstract-frame>) => (#rest values);
||#

(defgeneric clipboard-paste (frame))


#||
define open generic clipboard-clear
    (frame :: <abstract-frame>) => (#rest values);
||#

(defgeneric clipboard-clear (frame))


#||
define command-table *standard-edit-command-table* (*global-command-table*)
  menu-item "Undo"  = command-undo;
  menu-item "Redo"  = command-redo;
  separator;
  menu-item "Cut"   = clipboard-cut;
  menu-item "Copy"  = clipboard-copy;
  menu-item "Paste" = clipboard-paste;
  menu-item "Clear" = clipboard-clear;
end command-table *standard-edit-command-table*;
||#

(define-command-table *standard-edit-command-table* (*global-command-table*)
  ((:menu-item "Undo"  = #'command-undo)
   (:menu-item "Redo"  = #'command-redo)
   :separator
   (:menu-item "Cut"   = #'clipboard-cut)
   (:menu-item "Copy"  = #'clipboard-copy)
   (:menu-item "Paste" = #'clipboard-paste)
   (:menu-item "Clear" = #'clipboard-clear)))


#||
/// "View"

define command-table *standard-view-command-table* (*global-command-table*)
end command-table *standard-view-command-table*;
||#

(define-command-table *standard-view-command-table* (*global-command-table*))


#||
/// "Windows"

define command-table *standard-windows-command-table* (*global-command-table*)
end command-table *standard-windows-command-table*;
||#

(define-command-table *standard-windows-command-table* (*global-command-table*))


#||
/// "Help"

define command-table *standard-help-command-table* (*global-command-table*)
  menu-item "Topics..." = <help-on-topics>;
  separator;
  menu-item "About..."  = <help-on-version>;
end command-table *standard-help-command-table*;
||#

(define-command-table *standard-help-command-table* (*global-command-table*)
  ((:menu-item "Topics..." = (find-class '<help-on-topics>))
   :separator
   (:menu-item "About..."  = (find-class '<help-on-version>))))

