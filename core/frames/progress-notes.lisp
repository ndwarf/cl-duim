;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-FRAMES-INTERNALS -*-
(in-package #:duim-frames-internals)

#||
/// Progress notes

// The current progress note
define thread variable *progress-note* = #f;
||#

(define-thread-variable *progress-note* nil)
#-(and)
(defparameter *progress-note* nil
"
This variable is used to supply a default progress note to use if no
progress note is explicitly specified.
")


#||
define open generic clear-progress-note
    (framem :: <abstract-frame-manager>, note) => ();
define open generic display-progress-note
    (framem :: <abstract-frame-manager>, note) => ();
define open generic raise-progress-note
    (framem :: <abstract-frame-manager>, frame :: <abstract-frame>)
 => (sheet :: false-or(<sheet>));
define open generic lower-progress-note
    (framem :: <abstract-frame-manager>, frame :: <abstract-frame>, sheet) => ();
||#

(defgeneric clear-progress-note (framem note)
  (:documentation
"
Clears the specified progress note.
"))

(defgeneric display-progress-note (framem note)
  (:documentation
"
Displays the specified _progress-note_ in the frame managed by
_framem_.
"))

(defgeneric raise-progress-note (framem frame))
(defgeneric lower-progress-note (framem frame sheet))


(defgeneric (setf progress-note-label) (label note))
(defgeneric do-noting-progress (sheet label continuation &key frame cursor))
(defgeneric note-progress (numerator denominator &key note label cursor)
  (:documentation
"
Note the progress of an event in _note_.

If a _numerator_ and _denominator_ are supplied, then the progress is
displayed in terms of those figures. For example, if _numerator_ is 1,
and _denominator_ is 10, then the progress is displayed in tenths.

If supplied, _pointer-cursor_ is used as a cursor when the mouse
pointer is placed over the owner frame.
"))

(defgeneric note-progress-in-phases (numerator denominator &key note label phase-number n-phases))


#||
define sealed class <progress-note> (<object>)
  sealed slot progress-note-sheet = #f,
    init-keyword: sheet:;
  sealed slot progress-note-frame = #f,
    init-keyword: frame:;
  sealed slot progress-note-label = #f,
    init-keyword: label:,
    setter: %label-setter;
  sealed slot %numerator = 0;
  sealed slot %denominator = 1;
end class <progress-note>;
    
define sealed domain make (singleton(<progress-note>));
define sealed domain initialize (<progress-note>);
||#

(defclass <progress-note> ()
  ((progress-note-sheet :initarg :sheet :initform nil :accessor progress-note-sheet)
   (progress-note-frame :initarg :frame :initform nil :accessor progress-note-frame)
   (progress-note-label :initarg :label :initform nil :reader progress-note-label :writer %label-setter)
   (%numerator :initform 0 :accessor %numerator)
   (%denominator :initform 1 :accessor %denominator)))


#||
define method progress-note-label-setter (label, note :: <progress-note>) => (label)
  note.%label := label;
  let frame = progress-note-frame(note);
  when (frame)
    display-progress-note(frame-manager(frame), note)
  end;
  label
end method progress-note-label-setter;
||#

(defmethod (setf progress-note-label) (label (note <progress-note>))
  (%label-setter label note)
  (let ((frame (progress-note-frame note)))
    (when frame
      (display-progress-note (frame-manager frame) note))
    label))


#||
define macro noting-progress
  { noting-progress (?label:expression) ?:body end }
    => { begin
	   let noting-progress-body = method () ?body end;
	   do-noting-progress(#f, ?label, noting-progress-body)
	 end }
  { noting-progress (?sheet:expression, ?label:expression) ?:body end }
    => { begin
	   let noting-progress-body = method () ?body end;
	   do-noting-progress(?sheet, ?label, noting-progress-body)
	 end }
end macro noting-progress;
||#

(defmacro noting-progress ((arg1 &optional arg2) &body body)
"
Performs a body of code, noting its progress, for the specified sheet.

The sheet argument is an expression that evaluates to an instance of
<sheet>. The label argument is an expression that evaluates to an
instance of <string>.
"
  (let ((noting-progress-body (gensym "NOTING-PROGRESS-BODY-"))
	(sheet (gensym "SHEET-"))
	(label (gensym "LABEL-"))
	(macro-arg1  (gensym "ARG1-"))
	(macro-arg2  (gensym "ARG2-")))
    `(let* ((,noting-progress-body #'(lambda () ,@body))
	    (,macro-arg1 ,arg1)
	    (,macro-arg2 ,arg2)
	    (,sheet (if ,macro-arg2 ,macro-arg1 nil))
	    (,label (if ,macro-arg2 ,macro-arg2 ,macro-arg1)))
       (do-noting-progress ,sheet ,label ,noting-progress-body))))


#||
define method do-noting-progress
    (sheet :: <sheet>, label, continuation :: <function>,
     #key frame = sheet-frame(sheet), cursor)
 => (#rest values)
  let old-note = *progress-note*;
  let new-note = make(<progress-note>,
		      label: label,
		      sheet: sheet,
		      frame: frame);
  let pointer = port(sheet) & port-pointer(port(frame));
  let old-cursor = frame-cursor-override(frame);
  let framem = frame-manager(frame);
  local
    method initialize-progress
	() => ()
      when (cursor)
	frame-cursor-override(frame) := cursor
      end;
      display-progress-note(framem, new-note)
    end method initialize-progress,

    method finish-progress
	() => ()
      // If there was an old note, restore it, otherwise get rid
      // of the progress note display
      when (cursor)
	frame-cursor-override(frame) := old-cursor
      end;
      if (old-note)
	display-progress-note(framem, old-note);
      else
	lower-progress-note(framem, frame, sheet)
      end
    end method finish-progress;

  dynamic-bind (*progress-note* = new-note)
    block ()
      call-in-frame(frame, initialize-progress);
      continuation()
    cleanup
      call-in-frame(frame, finish-progress)
    end
  end
end method do-noting-progress;
||#


(defmethod do-noting-progress ((sheet <sheet>) label (continuation function)
			       &key (frame (sheet-frame sheet)) cursor)
  (let ((old-note   *progress-note*)
	(new-note   (make-instance '<progress-note>
				   :label label
				   :sheet sheet
				   :frame frame))
	(pointer    (and (port sheet) (port-pointer (port frame))))
	(old-cursor (frame-cursor-override frame))
	(framem     (frame-manager frame)))
    (declare (ignore pointer))
    (labels ((initialize-progress ()
	       (when cursor
		 (setf (frame-cursor-override frame) cursor))
	       (display-progress-note framem new-note))
	     (finish-progress ()
	       ;; If there was an old note, restore it, otherwise get rid
	       ;; of the progress note display
	       (when cursor
		 (setf (frame-cursor-override frame) old-cursor))
	       (if old-note
		   (display-progress-note framem old-note)
		   (lower-progress-note framem frame sheet))))
      (dynamic-let ((*progress-note* new-note))
        (unwind-protect
	     (progn
	       (call-in-frame frame #'initialize-progress)
	       (funcall continuation))
	  (call-in-frame frame #'finish-progress))))))


#||
define method do-noting-progress
    (sheet == #f, label, continuation :: <function>,
     #key frame = current-frame(), cursor)
 => (#rest values)
  let sheet = raise-progress-note(frame-manager(frame), frame);
  if (sheet)
    do-noting-progress(sheet, label, continuation,
                       frame: frame, cursor: cursor)
  else
    continuation()
  end
end method do-noting-progress;
||#

(defmethod do-noting-progress ((sheet (eql nil)) label (continuation function)
			       &key (frame (current-frame)) cursor)
  (let ((sheet (raise-progress-note (frame-manager frame) frame)))
    (if sheet
	(do-noting-progress sheet label continuation
			    :frame frame :cursor cursor)
	;; else
	(funcall continuation))))


#||
define method do-noting-progress
    (frame :: <frame>, label, continuation :: <function>,
     #key frame: _frame = frame, cursor) => (#rest values)
  ignore(_frame);
  do-noting-progress(#f, label, continuation,
                     frame: frame, cursor: cursor)
end method do-noting-progress;
||#

(defmethod do-noting-progress ((frame <frame>) label (continuation function)
			       &key ((:frame _frame) frame) cursor)
  (declare (ignore _frame))
  (do-noting-progress nil label continuation
		      :frame frame :cursor cursor))


#||
define method note-progress
    (numerator, denominator,
     #key note = *progress-note*, label, cursor) => ()
  when (note)
    let frame = progress-note-frame(note);
    let framem = frame-manager(frame);
    let pointer = port-pointer(port(frame));
    when (pointer & cursor)
      //--- This won't 'stick', should we use frame-override-cursor?
      pointer-cursor(pointer) := cursor
    end;
    when (label)
      note.%label := label
    end;
    note.%numerator := numerator;
    note.%denominator := denominator;
    call-in-frame(frame, display-progress-note, framem, note)
  end
end method note-progress;
||#

(defmethod note-progress (numerator denominator
			  &key (note *progress-note*) label cursor)
  (when note
    (let* ((frame   (progress-note-frame note))
	   (framem  (frame-manager frame))
	   (pointer (port-pointer (port frame))))
      (when (and pointer cursor)
	;;--- This won't 'stick', should we use frame-override-cursor?
        (setf (pointer-cursor pointer) cursor))
      (when label
        (%label-setter label note))
      (setf (%numerator note) numerator)
      (setf (%denominator note) denominator)
      (call-in-frame frame #'display-progress-note framem note))))


#||
define method note-progress-in-phases
    (numerator, denominator,
     #key note = *progress-note*, label, phase-number = 0, n-phases = 1) => ()
  when (note)
    note-progress(denominator * phase-number + numerator,
		  denominator * n-phases,
		  note: note, label: label)
  end
end method note-progress-in-phases;
||#

(defmethod note-progress-in-phases (numerator denominator
				    &key (note *progress-note*) label (phase-number 0) (n-phases 1))
  (when note
    (note-progress (+ (* denominator phase-number) numerator)
		   (* denominator n-phases)
		   :note note :label label)))



#||
/// Default implementation of progress notes, using the status bar

define method raise-progress-note
    (framem :: <basic-frame-manager>, frame :: <frame>)
 => (sheet :: false-or(<sheet>))
  frame-status-bar(frame)
end method raise-progress-note;
||#

(defmethod raise-progress-note ((framem <basic-frame-manager>) (frame <frame>))
  (frame-status-bar frame))


#||
define method clear-progress-note
    (framem :: <basic-frame-manager>, note :: <progress-note>) => ()
  let sheet = progress-note-sheet(note);
  when (sheet)
    gadget-value(sheet) := #f
  end
end method clear-progress-note;
||#

(defmethod clear-progress-note ((framem <basic-frame-manager>) (note <progress-note>))
  (let ((sheet (progress-note-sheet note)))
    (when sheet
      (setf (gadget-value sheet) nil))))


#||
define method display-progress-note
    (framem :: <basic-frame-manager>, note :: <progress-note>) => ()
  let sheet = progress-note-sheet(note);
  let denominator = note.%denominator;
  when (denominator > 0)
    gadget-value-range(sheet) := range(from: 0, to: denominator)
  end;
  gadget-label(sheet) := progress-note-label(note);
  gadget-value(sheet) := note.%numerator
end method display-progress-note;
||#

(defmethod display-progress-note ((framem <basic-frame-manager>) (note <progress-note>))
  (let ((sheet       (progress-note-sheet note))
	(denominator (%denominator note)))
    (when (> denominator 0)
      (setf (gadget-value-range sheet) (range :from 0 :to denominator)))
    (setf (gadget-label sheet) (progress-note-label note))
    (setf (gadget-value sheet) (%numerator note))))


#||
define method lower-progress-note
    (framem :: <basic-frame-manager>, frame :: <frame>,
     gadget :: <status-bar>)
 => ()
  // Switch off the progress control, but leave the last message
  gadget-value(gadget) := #f
end method lower-progress-note;
||#

(defmethod lower-progress-note ((framem <basic-frame-manager>) (frame <frame>) (gadget <status-bar>))
  ;; Switch off the progress control, but leave the last message
  (setf (gadget-value gadget) nil))

