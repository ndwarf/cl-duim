;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-FRAMES-INTERNALS -*-
(in-package :duim-frames-internals)

#||
/// Command table protocol class

define protocol-class command-table (<object>) end;
||#

(define-protocol-class command-table () ()
  (:documentation
   "The class of command tables. The command table for an application
gives a complete specification of the commands available to that
application, through its menus, tool bars, mnemonics, and
accelerators.

The :name initarg is a symbol that names the current command table.

The :inherit-from initarg is a sequence of command tables whose
behaviour the current command table should inherit. All command tables
inherit the behaviour of the command table specified by
*global-command-table* and can also inherit the behaviour specified by
*user-command-table*.

You do not normally need to specify a unique :resource-id yourself. As
with most other DUIM classes, the :name initarg serves as a sufficient
unique identifier.

Example:

    (define-command-table *clipboard-command-table*
        (*global-command-table*)
      ((menu-item \"Cut\" = #'cut-selection :documentation $cut-doc)
       (menu-item \"Copy\" = #'copy-selection :documentation $copy-doc)
       (menu-item \"Paste\" = #'paste-from-clipboard
                                     :documentation $paste-doc)
       (menu-item \"Delete\" = #'delete-selection
                                     :documentation $delete-doc)))

TODO: check this example is right...
"))

;;; TODO: where should the documentation for 'command-table?' go?
;;; Automatically generated in define-protocol-class. Do not use this.
; (defun command-table-p (o)
;   "Returns t if object is a command table; otherwise, it returns nil.")
