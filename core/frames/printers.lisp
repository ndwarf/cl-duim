;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-FRAMES-INTERNALS -*-
(in-package #:duim-frames-internals)

(defmethod print-object ((command-table <command-table>) stream)
  (print-unreadable-object (command-table stream :type t :identity t)
    (format stream "~a [~d commands]"
	    (command-table-name command-table)
	    (hash-table-size (command-table-commands command-table)))))

#||
define method print-object
    (command-table :: <command-table>, stream :: <stream>) => ()
  printing-object (command-table, stream, type?: #t, identity?: #t)
    format(stream, "%=", command-table-name(command-table))
  end
end method print-object;
||#
