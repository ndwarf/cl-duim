;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-FRAMES-INTERNALS -*-
(in-package :duim-frames-internals)

#||
/// Help systems

define open abstract class <help-system> (<object>)
  sealed constant slot help-system-name :: <string>,
    required-init-keyword: name:;
end class <help-system>;
||#

(defclass <help-system> ()
  ((help-system-name :type string :initarg :name :initform (required-slot ":name" "<help-system>") :reader help-system-name))
  (:metaclass <abstract-metaclass>))

;;(defgeneric as ())  ;; TODO: This needs to go, so leaving as a style-warning.
(defgeneric find-pane-by-gesture (frame))
(defgeneric find-pane-by-focus (frame))
(defgeneric default-from-sheet (sheet getter-fn))

#||
define variable *default-help-system* :: false-or(<help-system>) = #f;
||#

(defparameter *default-help-system* nil)


#||
// NB: This is for testing the installation of a Help System (viewer) and
// not any particular form of Help content
define open generic help-system-installed?
    (system :: <help-system>) => (installed? :: <boolean>);
||#

(defgeneric help-system-installed? (system)
  (:documentation
"
NB: This is for testing the installation of a Help System (viewer) and
not any particular form of Help content.
"))


#||
define method help-system-installed?
    (system :: <help-system>) => (installed? :: <boolean>)
  #t
end method help-system-installed?;
||#

(defmethod help-system-installed? ((system <help-system>))
  t)


#||
define open generic frame-manager-help-system
    (framem :: <abstract-frame-manager>)
 => (system :: false-or(<help-system>));
||#

(defgeneric frame-manager-help-system (framem))


#||
define open generic frame-manager-help-system-setter
    (system :: false-or(<help-system>), framem :: <abstract-frame-manager>)
 => (system :: false-or(<help-system>));
||#

(defgeneric (setf frame-manager-help-system) (system framem))


#||
define method frame-manager-help-system
    (framem :: <frame-manager>)
 => (system :: false-or(<help-system>))
  *default-help-system*
end method frame-manager-help-system;
||#

(defmethod frame-manager-help-system ((framem <frame-manager>))
  *default-help-system*)


#||
define method frame-manager-help-system-setter
    (system :: false-or(<help-system>), framem :: <frame-manager>)
 => (system :: false-or(<help-system>))
  *default-help-system* := system
end method frame-manager-help-system-setter;
||#

(defmethod (setf frame-manager-help-system) ((system null) (framem <frame-manager>))
  (setf *default-help-system* system))

(defmethod (setf frame-manager-help-system) ((system <help-system>) (framem <frame-manager>))
  (setf *default-help-system* system))



#||

/// Help systems errors

define class <help-system-error> (<simple-error>)
end class <help-system-error>;
||#

(define-condition <help-system-error> (simple-error) ())


#||
define class <help-system-not-installed> (<help-system-error>)
  keyword format-string: = "%s is not installed";
end class <help-system-not-installed>;
||#

(define-condition <help-system-not-installed> (<help-system-error>) ())
;; FIXME: KEYWORD FORMAT-STRING:...?


#||
define method make
    (class == <help-system-not-installed>, #key system)
 => (condition :: <help-system-not-installed>)
  next-method(class, format-arguments: vector(help-system-name(system)))
end method make;
||#

#||
;;; :fixme: this is creating a condition type; not sure this
;;; is valid syntax...
(defmethod make-instance ((class (eql #m<help-system-not-installed>))
                          &rest
                          initargs
                          &key
			  system)
  (declare (ignore initargs))
  (call-next-method class :format-arguments (vector (help-system-name system))))
||#

#||
define class <no-help-system> (<help-system-error>)
  keyword format-string: = "There is no help system defined.";
end class <no-help-system>;
||#

(define-condition <no-help-system> (<help-system-error>)
  ;; FIXME: FORMAT-STRING?
  ((format-string :initform "There is no help system defined"
                  :initarg :format-string)))



#||

/// Help

define constant <help-pane-type>
    = false-or(type-union(<abstract-sheet>, one-of(#"by-gesture", #"by-focus")));
||#

(deftype <help-pane-type> () '(or null <abstract-sheet> (member :by-gesture :by-focus)))


#||
define constant <named-help-source> = type-union(<symbol>, <help-source>);
||#

(deftype <named-help-source> () '(or symbol <help-source>))


#||
define protocol <<help-protocol>> ()
  getter help-source (command :: <help-command>)
 => (source :: false-or(<named-help-source>));
  getter help-secondary-window (command :: <help-command>)
 => (window-name :: false-or(<string>));
  getter help-pane (command :: <help-command>)
 => (pane :: <help-pane-type>);
  getter help-context (command :: <help-command>)
 => (context :: false-or(<symbol>));
  getter help-topic-id (command :: <help-command>)
 => (topic-id);
  getter help-popup? (command :: <help-command>)
 => (pop-up? :: <boolean>);
  getter help-keyword (command :: <help-command>)
 => (keyword :: false-or(<string>));
  getter help-macro (command :: <help-command>)
 => (_macro :: <string>);
  getter help-window-region (command :: <help-command>)
 => (region :: <region>);
  // Initialization
  function initialize-help-pane
    (command :: <help-command>, frame :: <abstract-frame>) => ();
  function initialize-help
    (command :: <help-command>, frame :: <abstract-frame>) => ();
  // Glue to commands
  function display-help
    (framem :: <abstract-frame-manager>, frame :: <abstract-frame>,
     command :: <help-command>) => ();
  // Glue to frames
  function frame-help-source-locator
    (frame :: <abstract-frame>, source :: <help-source>)
 => (locator);
  function frame-help-source
    (frame :: <abstract-frame>, command :: <help-command>)
 => (source :: false-or(<named-help-source>));
  function frame-help-context
    (frame :: <abstract-frame>, command :: <help-command>)
 => (context :: false-or(<symbol>));
  function frame-help-topic-id
    (frame :: <abstract-frame>, command :: <help-command>)
 => (topic-id);
  function frame-help-keyword
    (frame :: <abstract-frame>, command :: <help-command>)
 => (keyword :: false-or(<string>));
end protocol <<help-protocol>>;
||#
#||
(define-protocol <<help-protocol>> ()
  (:getter help-source (command))
  (:getter help-secondary-window (command))
  (:getter help-pane (command))
  (:getter help-context (command))
  (:getter help-topic-id (command))
  (:getter help-popup? (command))
  (:getter help-keyword (command))
  (:getter help-macro (command))
  (:getter help-window-region (command))
  ;; Initialization
  (:function initialize-help-pane (command frame))
  (:function initialize-help (command frame))
  ;; Glue to commands
  (:function display-help (framem frame command))
  ;; Glue to frames
  (:function frame-help-source-locator (frame source))
  (:function frame-help-source (frame command))
  (:function frame-help-context (frame command))
  (:function frame-help-topic-id (frame command))
  (:function frame-help-keyword (frame command)))
||#



#||

/// Help Sources

// Reify entity for refering to a help source file and its mapping
// from symbolic help contexts to whatever identifiers are required
// by the help system: eg integers, strings, urls, or whatever.
define constant $help-sources :: <object-table> = make(<table>);
||#

(defparameter *help-sources* (make-hash-table :test #'eql))


#||
define sealed class <help-source> (<object>)
  sealed constant slot help-source-name :: <symbol>,
    required-init-keyword: name:;
  sealed slot help-source-locator :: false-or(<string>) = #f,
    init-keyword: locator:;
  sealed constant slot help-source-context-map :: false-or(<object-table>) = #f,
    init-keyword: contexts:;
end class <help-source>;
||#


(defclass <help-source> ()
  ((help-source-name :type symbol :initarg :name :initform (required-slot ":name" "<help-source>") :reader help-source-name)
   (help-source-locator :type (or null string) :initarg :locator :initform nil :accessor help-source-locator)
   (help-source-context-map :type (or null hash-table) :initarg :contexts :initform nil :reader help-source-context-map)))


#||
define sealed method initialize
    (source :: <help-source>, #key name :: <symbol>)
  next-method();
  $help-sources[name] := source
end method initialize;
||#

(defmethod initialize-instance :after ((source <help-source>) &key name &allow-other-keys)
  (setf (gethash name *help-sources*) source))


#||
define macro help-source-definer
  { define help-source ?name:name ?locator:expression ?entries:* end }
    => { make(<help-source>,
	      name: ?#"name",
	      locator: ?locator,
	      contexts: initialize-table (make(<table>)) ?entries end) }
end macro help-source-definer;
||#

(defmacro define-help-source ((name locator entries))
  `(make-instance '<help-source>
		  :name ',name         ; was ?#"name" - should be a keyword? :fixme:
		  :locator ,locator
		  :contexts (initialize-table (make-hash-table) ,entries)))


#||
define macro initialize-table
  { initialize-table (?table-name:expression) ?entries:* end }
    => { let the-table = ?table-name; ?entries; the-table }
 entries:
  { } => { }
  { ?key:expression => ?value:expression; ... }
    => { the-table[?key] := ?value; ... }
end macro initialize-table;
||#

(defmacro initialize-table (expression entries)
  ;; Since there's no BODY these gensyms probably aren't needed...
  (let ((the-table (gensym "THE-TABLE-"))
	(the-list  (gensym "THE-LIST-"))
	(key       (gensym "KEY-"))
	(value     (gensym "VALUE-")))
  `(let ((,the-table ,expression)
	 (,the-list ,entries))
     (unless (endp ,the-list)
       (loop for ,key   = (pop ,the-list)
	     and ,value = (pop ,the-list)
	     do (setf (gethash ,key ,the-table) ,value)
	     until (null ,the-list)))
     ,the-table)))


#||
define method as
    (class == <help-source>, source :: <help-source>) => (source :: <help-source>)
  source
end method as;
||#

(defmethod as ((class (eql (find-class '<help-source>))) (source <help-source>))
  source)


#||
define method as
    (class == <help-source>, name :: <symbol>) => (source :: <help-source>)
  $help-sources[name]
end method as;
||#

(defmethod as ((class (eql (find-class '<help-source>))) (name symbol))
  (gethash name *help-sources*))



#||

/// Help Commands

// <help-command> and its subclasses model the standard kinds of help that
// users are normally able to invoke.  Most classes have a 'help-source'
// associated with them.  Some have additional state, such as a 'help-context'.
define open abstract primary class <help-command> (<basic-command>)
end class <help-command>;
||#

(defclass <help-command> (<basic-command>)
  ()
  (:metaclass <abstract-metaclass>))


#||
// Help on a specific subject
define open abstract primary class <help-on-subject> (<help-command>)
end class <help-on-subject>;
||#

(defclass <help-on-subject> (<help-command>)
  ()
  (:documentation
"
Help on a specific subject.
")
  (:metaclass <abstract-metaclass>))


#||
// Help about using the help system itself
define sealed class <help-on-help> (<help-on-subject>)
end class <help-on-help>;
||#

(defclass <help-on-help> (<help-on-subject>) ()
  (:documentation
"
Help about using the help system itself.
"))


#||
// "About" box help
define sealed class <help-on-version> (<help-on-subject>)
end class <help-on-version>;
||#

(defclass <help-on-version> (<help-on-subject>) ()
  (:documentation
"
'About' box help.
"))


#||
// Help on something with an explicit source
define open abstract primary class <help-from-source> (<help-on-subject>)
  sealed slot help-source :: false-or(<named-help-source>) = #f,
    init-keyword: source:;
  sealed slot help-secondary-window :: false-or(<string>) = #f,
    init-keyword: secondary-window:;
end class <help-from-source>;
||#

(defclass <help-from-source> (<help-on-subject>)
  ((help-source :type (or null <named-help-source>) :initarg :source :initform nil :accessor help-source)
   (help-secondary-window :type (or null string) :initarg :secondary-window :initform nil :accessor help-secondary-window))
  (:documentation
"
Help on something with an explicit source.
")
  (:metaclass <abstract-metaclass>))


#||
// Help topics page (supersedes index and contents in Win95)
define sealed class <help-on-topics> (<help-from-source>)
end class <help-on-topics>;
||#

(defclass <help-on-topics> (<help-from-source>) ()
  (:documentation
"
Help topics page (supersedes index and contents in Win95).
"))


#||
// Help index page
define sealed class <help-on-index> (<help-from-source>)
end class <help-on-index>;
||#

(defclass <help-on-index> (<help-from-source>) ()
  (:documentation
"
Help index page.
"))


#||
// Help contents page
define sealed class <help-on-contents> (<help-from-source>)
end class <help-on-contents>;
||#

(defclass <help-on-contents> (<help-from-source>) ()
  (:documentation
"
Help contents page.
"))


#||
// Help on some UI element
define open abstract primary class <help-on-pane> (<help-from-source>)
  sealed slot help-pane :: false-or(<help-pane-type>) = #f,
    init-keyword: pane:;
end class <help-on-pane>;
||#

(defclass <help-on-pane> (<help-from-source>)
  ((help-pane :type (or null <help-pane-type>) :initarg :pane :initform nil :accessor help-pane))
  (:documentation
"
Help on some UI element.
")
  (:metaclass <abstract-metaclass>))


#||
define method find-pane-by-gesture
    (frame :: <basic-frame>) => (pane :: false-or(<sheet>))
  // Go into special mode so user can select pane they are interested in
  //---*** Do this
  error("Not implemented yet!")
end method find-pane-by-gesture;
||#


(defmethod find-pane-by-gesture ((frame <basic-frame>))
  ;; Go into special mode so user can select pane they are interested in
  ;;---*** Do this
  (error "Not implemented yet!"))


#||
define method find-pane-by-focus
    (frame :: <basic-frame>) => (pane :: false-or(<sheet>))
  frame-mapped?(frame) & frame-input-focus(frame)
end method find-pane-by-focus;
||#

(defmethod find-pane-by-focus ((frame <basic-frame>))
  (and (frame-mapped? frame) (frame-input-focus frame)))


#||
define method default-from-sheet
    (sheet :: false-or(<sheet>), getter :: <function>) => (default)
  block (return)
    for (sh = sheet then sheet-parent(sh),
	 until: ~sh)
      let value = getter(sh);
      when (value)
	return(value)
      end
    end;
    #f
  end
end method default-from-sheet;
||#

(defmethod default-from-sheet ((sheet null) (getter function))
  (loop for sh = sheet then (sheet-parent sh)
     until (not sh)
     do (let ((value (funcall getter sh)))
	  (when value
	    (return value))))
  nil)

(defmethod default-from-sheet ((sheet <sheet>) (getter function))
  (loop for sh = sheet then (sheet-parent sh)
     until (not sh)
     do (let ((value (funcall getter sh)))
	  (when value
	    (return value))))
  nil)


#||
// Help on predefined topic
// Sometimes called "What is this?"
define sealed class <help-on-context> (<help-on-pane>)
  sealed slot help-context :: false-or(<symbol>) = #f,
    init-keyword: context:;
  sealed slot help-topic-id = #f,
    init-keyword: topic-id:;
  sealed slot help-popup? :: <boolean> = #f,
    init-keyword: popup?:;
end class <help-on-context>;
||#


(defclass <help-on-context> (<help-on-pane>)
  ((help-context :type (or null symbol) :initarg :context :initform nil :accessor help-context)
   (help-topic-id :initarg :topic-id :initform nil :accessor help-topic-id)
   (help-popup? :type boolean :initarg :popup? :initform nil :accessor help-popup?))
  (:documentation
"
Help on predefined topic.
"))


#||
// Help on arbitrary string
define sealed class <help-on-keyword> (<help-on-pane>)
  sealed slot help-keyword :: false-or(<string>) = #f,
    init-keyword: keyword:;
end class <help-on-keyword>;
||#

(defclass <help-on-keyword> (<help-on-pane>)
  ((help-keyword :type (or null string) :initarg :keyword :initform nil :accessor help-keyword))
  (:documentation
"
Help on arbitrary string.
"))


#||
// Help system macro
define sealed class <help-run-macro> (<help-from-source>)
  sealed slot help-macro :: <string>,
    required-init-keyword: macro:;
end class <help-run-macro>;
||#

(defclass <help-run-macro> (<help-from-source>)
  ((help-macro :type string :initarg :macro :initform (required-slot ":macro" "<help-run-macro>") :accessor help-macro))
  (:documentation
"
Help system macro.
"))


#||
// Help window can be repositioned
define sealed class <help-reposition> (<help-from-source>)
  sealed slot help-window-region :: <bounding-box>,
    required-init-keyword: region:;
end class <help-reposition>;
||#

(defclass <help-reposition> (<help-from-source>)
  ((help-window-region :type <bounding-box> :initarg :region :initform (required-slot ":region" "<help-reposition>")
		       :accessor help-window-region))
  (:documentation
"
Help window can be repositioned.
"))


#||
// Help system is no longer needed
//---*** Needs to register a callback with DUIM exiting mechanism
define sealed class <help-quit> (<help-command>)
end class <help-quit>;
||#

(defclass <help-quit> (<help-command>) ()
  (:documentation
"
Help system is no longer needed.
"))


#||
// Initializes command pane
define method initialize-help-pane
    (command :: <help-command>, frame :: <basic-frame>) => ()
  #f
end method initialize-help-pane;
||#

(defmethod initialize-help-pane ((command <help-command>) (frame <basic-frame>))
  nil)


#||
define method initialize-help-pane
    (command :: <help-on-pane>, frame :: <basic-frame>) => ()
  select (help-pane(command))
    #"by-gesture" =>
      help-pane(command) := find-pane-by-gesture(frame);
    #"by-focus" =>
      help-pane(command) := find-pane-by-focus(frame);
    otherwise => #f;
  end
end method initialize-help-pane;
||#

(defmethod initialize-help-pane ((command <help-on-pane>) (frame <basic-frame>))
  (case (help-pane command)
    (:by-gesture
     (setf (help-pane command) (find-pane-by-gesture frame)))
    (:by-focus
     (setf (help-pane command) (find-pane-by-focus frame)))
    (t nil)))


#||
// Initializes command from frame
define method initialize-help
    (command :: <help-command>, frame :: <basic-frame>) => ()
  #f
end method initialize-help;
||#

(defmethod initialize-help ((command <help-command>) (frame <basic-frame>))
  nil)


#||
define method initialize-help
    (command :: <help-from-source>, frame :: <basic-frame>) => ()
  next-method();
  help-source(command)
    := as(<help-source>,
	  help-source(command)
	  | frame-help-source(frame, command));
  help-source-locator(help-source(command))
    := help-source-locator(help-source(command))
       | frame-help-source-locator(frame, help-source(command));
end method initialize-help;
||#

(defmethod initialize-help ((command <help-from-source>) (frame <basic-frame>))
  (call-next-method)
  (setf (help-source command)
	(as '<help-source>
	    (or (help-source command)
		(frame-help-source frame command))))
  (setf (help-source-locator (help-source command))
	(or (help-source-locator (help-source command))
	    (frame-help-source-locator frame (help-source command)))))


#||
define method initialize-help
    (command :: <help-on-context>, frame :: <basic-frame>) => ()
  next-method();
  help-context(command)
    := help-context(command)
       | frame-help-context(frame, command);
  help-topic-id(command)
    := help-topic-id(command)
       | frame-help-topic-id(frame, command);
end method initialize-help;
||#

(defmethod initialize-help ((command <help-on-context>) (frame <basic-frame>))
  (call-next-method)
  (setf (help-context command)
	(or (help-context command)
	    (frame-help-context frame command)))
  (setf (help-topic-id command)
	(or (help-topic-id command)
	    (frame-help-topic-id frame command))))


#||
define method initialize-help
    (command :: <help-on-keyword>, frame :: <basic-frame>) => ()
  next-method();
  help-keyword(command)
    := help-keyword(command)
       | frame-help-keyword(frame, command);
end method initialize-help;
||#

(defmethod initialize-help ((command <help-on-keyword>) (frame <basic-frame>))
  (call-next-method)
  (setf (help-keyword command)
	(or (help-keyword command)
	    (frame-help-keyword frame command))))



#||
/// Glue to Commands

define method do-execute-command
    (frame :: <basic-frame>, command :: <help-command>) => ()
  initialize-help-pane(command, frame);
  initialize-help(command, frame);
  display-help(frame-manager(frame), frame, command)
end method do-execute-command;
||#

(defmethod do-execute-command ((frame <basic-frame>) (command <help-command>))
  (initialize-help-pane command frame)
  (initialize-help command frame)
  (display-help (frame-manager frame) frame command))


#||
define method do-execute-command
    (frame :: <basic-frame>, command :: <help-on-version>) => ()
  // The idea here is that hackers write their own method for this...
  #f
end method do-execute-command;
||#

(defmethod do-execute-command ((frame <basic-frame>) (command <help-on-version>))
  ;; The idea here is that hackers write their own method for this...
  nil)



#||
/// Glue to Frames

define method frame-help-source-locator
    (frame :: <basic-frame>, source :: <help-source>)
 => (locator :: singleton(#f))
  #f
end method frame-help-source-locator;
||#

(defmethod frame-help-source-locator ((frame <basic-frame>) (source <help-source>))
  nil)


#||
define method frame-help-source
    (frame :: <basic-frame>, command :: <help-from-source>)
 => (source :: singleton(#f))
  #f
end method frame-help-source;
||#

(defmethod frame-help-source ((frame <basic-frame>) (command <help-from-source>))
  nil)


#||
define method frame-help-source
    (frame :: <basic-frame>, command :: <help-on-pane>)
 => (source :: false-or(<named-help-source>))
  default-from-sheet(help-pane(command), sheet-help-source)
end method frame-help-source;
||#

(defmethod frame-help-source ((frame <basic-frame>) (command <help-on-pane>))
  (default-from-sheet (help-pane command) #'sheet-help-source))


#||
define method frame-help-context
    (frame :: <basic-frame>, command :: <help-on-context>)
 => (context :: false-or(<symbol>))
  default-from-sheet(help-pane(command), sheet-help-context)
end method frame-help-context;
||#

(defmethod frame-help-context ((frame <basic-frame>) (command <help-on-context>))
  (default-from-sheet (help-pane command) #'sheet-help-context))


#||
define method frame-help-keyword
    (frame :: <basic-frame>, command :: <help-on-keyword>)
 => (keyword :: false-or(<string>))
  default-from-sheet(help-pane(command), selected-text)
end method frame-help-keyword;
||#

(defmethod frame-help-keyword ((frame <basic-frame>) (command <help-on-keyword>))
  (default-from-sheet (help-pane command) #'selected-text))


#||
define method frame-help-topic-id
    (frame :: <basic-frame>, command :: <help-on-context>)
 => (object)
  help-source-context-map(help-source(command))[help-context(command)]
end method frame-help-topic-id;
||#

(defmethod frame-help-topic-id ((frame <basic-frame>) (command <help-on-context>))
  ;; ::fixme:: should be a gethash...
  (aref (help-source-context-map (help-source command)) (help-context command)))

