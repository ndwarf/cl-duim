;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-FRAMES-INTERNALS -*-
(in-package :duim-frames-internals)

;;; Enabling and disabling of commands

(defun command-subclass-p (object)
  ;; XXX: Pretty sure this will catch instances of <command> as well as
  ;; subclasses of it... so the <command> part of <command-oid> is
  ;; a bit superfluous.
  (instance? object (find-class '<command>)))

;; FIXME: this is *also* defined in DYLAN-COMPAT;COMMANDS;COMMANDS - do we need both?
;; SN:20200702 It appears we need both, for now. I added
;; dylan-commands as a dependency in the package definitions and asd
;; files, but anything using <command-oid> will fail as undefined if
;; this isn't here. More than likely it's a dependency or
;; import/export issue that needs to be worked out. Also note that the
;; definitions differ in the two files.

(deftype <command-oid> ()
  " A <command-oid> is something that acts like a command:
  - a command class, or
  - a command object, or
  - a function that acts like a command"
  '(or (satisfies command-subclass-p) <command> function))


;;; 'command' can be a <command-oid> or a <command-table>
(defgeneric command-enabled-p (command frame &key command-table)
  (:documentation "Returns true if _command_ in _frame_ is enabled."))

(defgeneric (setf command-enabled-p) (enabledp command frame &key do-inherited-p)
  (:documentation
   "Enables or disables _command_ in _frame_. If _enabledp_ is true, the
_command_ is enabled, otherwise it is disabled. Enabling and disabling
a command enables and disables all the gadgets that are associated
with the command, such as menu items and tool bar buttons.

This function is useful when manipulating the disabled commands in
_frame_. For example, it is common to disable the Save menu command
immediately after saving a file, enabling it again only when the file
has been modified."))


(defgeneric distribute-command-event (sheet command))


#||
define method command-enabled?
    (command :: <command-oid>, frame :: <basic-frame>,
     #key command-table = frame-command-table(frame))
 => (enabled? :: <boolean>)
  let disabled-commands :: <object-table> = frame-disabled-commands(frame);
  ~gethash(disabled-commands, command)
  //--- CLIM used to do this, but do we need to?
  //  & command-accessible?(command-table, command)
end method command-enabled?;
||#

(defmethod command-enabled-p (command (frame <basic-frame>) &key (command-table (frame-command-table frame)))
  (declare (ignore command-table))
  (check-type command <command-oid>)
  (let ((disabled-commands (frame-disabled-commands frame)))
    (not (gethash command disabled-commands)))
  ;;--- CLIM used to do this, but do we need to?
  ;;  & command-accessible?(command-table, command)
  )


#||
define method command-enabled?-setter
    (enabled? == #t, command :: <command-oid>, frame :: <basic-frame>,
     #key do-inherited? = #f)
 => (enabled? :: <boolean>)
  ignore(do-inherited?);
  let disabled-commands :: <object-table> = frame-disabled-commands(frame);
  remhash(disabled-commands, command);
  note-command-enabled(frame-manager(frame), frame, command);
  enabled?
end method command-enabled?-setter;
||#

(defmethod (setf command-enabled-p) ((enabledp (eql t)) command (frame <basic-frame>) &key (do-inherited-p nil))
  (declare (ignore do-inherited-p))
  (check-type command <command-oid>)
  (let ((disabled-commands (frame-disabled-commands frame)))
    (remhash command disabled-commands)
    (note-command-enabled (frame-manager frame) frame command)
    enabledp))


#||
define method command-enabled?-setter
    (enabled? == #f, command :: <command-oid>, frame :: <basic-frame>,
     #key do-inherited? = #f)
 => (enabled? :: <boolean>)
  ignore(do-inherited?);
  let disabled-commands :: <object-table> = frame-disabled-commands(frame);
  gethash(disabled-commands, command) := #t;
  note-command-disabled(frame-manager(frame), frame, command);
  enabled?
end method command-enabled?-setter;
||#

(defmethod (setf command-enabled-p) ((enabledp (eql nil)) command (frame <basic-frame>) &key (do-inherited-p nil))
  (declare (ignore do-inherited-p))
  (check-type command <command-oid>)
  (let ((disabled-commands (frame-disabled-commands frame)))
    (setf (gethash command disabled-commands) t)
    (note-command-disabled (frame-manager frame) frame command)
    enabledp))


#||
define method command-enabled?
    (command :: <functional-command>, frame :: <basic-frame>,
     #key command-table = frame-command-table(frame))
 => (enabled? :: <boolean>)
  let disabled-commands :: <object-table> = frame-disabled-commands(frame);
  let command :: <function> = command-function(command);
  ~gethash(disabled-commands, command)
  //--- CLIM used to do this, but do we need to?
  //  & command-accessible?(command-table, command)
end method command-enabled?;
||#

(defmethod command-enabled-p ((command <functional-command>)
			      (frame <basic-frame>)
			      &key (command-table (frame-command-table frame)))
  (declare (ignore command-table))
  (let ((disabled-commands (frame-disabled-commands frame))
	(command (command-function command)))
    (not (gethash command disabled-commands)))
    ;;--- CLIM used to do this in addition to the below, but do we need to?
    ;;  & command-accessible?(command-table, command)
  )


#||
define method command-enabled?-setter
    (enabled? == #t, command :: <functional-command>, frame :: <basic-frame>,
     #key do-inherited? = #f)
 => (enabled? :: <boolean>)
  ignore(do-inherited?);
  let disabled-commands :: <object-table> = frame-disabled-commands(frame);
  let command :: <function> = command-function(command);
  remhash(disabled-commands, command);
  note-command-enabled(frame-manager(frame), frame, command);
  enabled?
end method command-enabled?-setter;
||#

(defmethod (setf command-enabled-p) ((enabledp (eql t))
				     (command <functional-command>)
				     (frame <basic-frame>)
				     &key (do-inherited-p nil))
  (declare (ignore do-inherited-p))
  (let ((disabled-commands (frame-disabled-commands frame))
	(command (command-function command)))
    (remhash command disabled-commands)
    (note-command-enabled (frame-manager frame) frame command)
    enabledp))


#||
define method command-enabled?-setter
    (enabled? == #f, command :: <functional-command>, frame :: <basic-frame>,
     #key do-inherited? = #f)
 => (enabled? :: <boolean>)
  ignore(do-inherited?);
  let disabled-commands :: <object-table> = frame-disabled-commands(frame);
  let command :: <function> = command-function(command);
  gethash(disabled-commands, command) := #t;
  note-command-disabled(frame-manager(frame), frame, command);
  enabled?
end method command-enabled?-setter;
||#

(defmethod (setf command-enabled-p) ((enabledp (eql nil))
				     (command <functional-command>)
				     (frame <basic-frame>)
				     &key (do-inherited-p nil))
  (declare (ignore do-inherited-p))
  (let ((disabled-commands (frame-disabled-commands frame))
	(command (command-function command)))
    (setf (gethash command disabled-commands) t)
    (note-command-disabled (frame-manager frame) frame command)
    enabledp))


#||
// 'command-enabled?' and friends also work on command tables...
define method command-enabled?
    (comtab :: <command-table>, frame :: <basic-frame>,
     #key command-table)
 => (enabled? :: <boolean>)
  ignore(command-table);
  let disabled-commands :: <object-table> = frame-disabled-commands(frame);
  ~gethash(disabled-commands, comtab)
end method command-enabled?;
||#

;; 'command-enabled-p' and friends also work on command tables...
;; TODO: Make work when comtab is something other than the :name of the command table
(defmethod command-enabled-p ((comtab <command-table>) (frame <basic-frame>) &key command-table)
  (declare (ignore command-table))
  (let ((disabled-commands (frame-disabled-commands frame)))
    (not (gethash comtab disabled-commands))))


#||
define method command-enabled?-setter
    (enabled? == #t, comtab :: <command-table>, frame :: <basic-frame>,
     #key do-inherited? = #f)
 => (enabled? :: <boolean>)
  let disabled-commands :: <object-table> = frame-disabled-commands(frame);
  remhash(disabled-commands, comtab);
  when (do-inherited?)
    do-command-table-commands(method (c, ct)
				ignore(ct);
				command-enabled?(c, frame) := #t
			      end method, comtab)
  end;
  note-command-enabled(frame-manager(frame), frame, comtab);
  enabled?
end method command-enabled?-setter;
||#

(defmethod (setf command-enabled-p) ((enabledp (eql t))
				     (comtab <command-table>)
				     (frame <basic-frame>)
				     &key (do-inherited-p nil))
  (let ((disabled-commands (frame-disabled-commands frame)))
    (remhash comtab disabled-commands)
    (when do-inherited-p
      (do-command-table-commands #'(lambda (c ct)
				     (declare (ignore ct))
				     (setf (command-enabled-p c frame) t))
	comtab))
    (note-command-enabled (frame-manager frame) frame comtab)
    enabledp))


#||
define method command-enabled?-setter
    (enabled? == #f, comtab :: <command-table>, frame :: <basic-frame>,
     #key do-inherited? = #f)
 => (enabled? :: <boolean>)
  let disabled-commands :: <object-table> = frame-disabled-commands(frame);
  gethash(disabled-commands, comtab) := #t;
  when (do-inherited?)
    do-command-table-commands(method (c, ct)
				ignore(ct);
				command-enabled?(c, frame) := #f
			      end method, comtab)
  end;
  note-command-disabled(frame-manager(frame), frame, comtab);
  enabled?
end method command-enabled?-setter;
||#

(defmethod (setf command-enabled-p) ((enabledp (eql nil))
				     (comtab <command-table>)
				     (frame <basic-frame>)
				     &key (do-inherited-p nil))
  (let ((disabled-commands (frame-disabled-commands frame)))
    (setf (gethash comtab disabled-commands) t)
    (when do-inherited-p
      (do-command-table-commands #'(lambda (c ct)
				     (declare (ignore ct))
				     (setf (command-enabled-p c frame) nil))
	comtab))
    (note-command-disabled (frame-manager frame) frame comtab)
    enabledp))



;;; This is where a command button might get ungrayed
;;; Note that COMMAND can be a <command>, <function>, or <command-table>
(defgeneric note-command-enabled (framem frame command))

#||
define method note-command-enabled
    (framem :: <frame-manager>, frame :: <frame>, command) => ()
  // Enable all the gadgets in this frame corresponding to the command
  do-command-menu-gadgets
    (method(gadget) gadget-enabled?(gadget) := #t end,
     frame, command)
end method note-command-enabled;
||#

;;; TODO: Figure out why <frame-manager> is in the function arguments
(defmethod note-command-enabled ((framem <frame-manager>) (frame <frame>) command)
  ;; Enable all the gadgets in this frame corresponding to the command
  (do-command-menu-gadgets #'(lambda (gadget)
			       (setf (gadget-enabled? gadget) t))
    frame command))


#||
// This is where a command button might get grayed out
// 'command' can be a <command-oid> or a <command-table>
define open generic note-command-disabled
    (framem :: <abstract-frame-manager>, frame :: <abstract-frame>, command) => ();
||#

(defgeneric note-command-disabled (framem frame command))


#||
define method note-command-disabled
    (framem :: <frame-manager>, frame :: <frame>, command) => ()
  // Disable all the gadgets in this frame corresponding to the command
  do-command-menu-gadgets
    (method(gadget) gadget-enabled?(gadget) := #f end,
     frame, command)
end method note-command-disabled;
||#

(defmethod note-command-disabled ((framem <frame-manager>) (frame <frame>) command)
  ;; Disable all the gadgets in this frame corresponding to the command
  (do-command-menu-gadgets #'(lambda (gadget)
			       (setf (gadget-enabled? gadget) nil))
    frame command))





;;; Callbacks on command menus generate these events

;;; Command events
#||
define sealed class <command-event> (<frame-event>)
  sealed constant slot event-command :: <command-oid>,
    required-init-keyword: command:;
end class <command-event>;

define sealed domain make (singleton(<command-event>));
define sealed domain initialize (<command-event>);
||#

(defclass <command-event> (<frame-event>)
  ((event-command :type <command-oid>
		  :initarg :command
		  :initform (required-slot ":command" "<command-event>")
		  :reader event-command)))


#||
define method handle-event
    (handler :: <event-handler>, event :: <command-event>) => ()
  let command = event-command(event);
  execute-command-type(command, server: event-frame(event), client: handler)
end method handle-event;
||#

(defmethod handle-event ((handler <event-handler>) (event <command-event>))
  (let ((command (event-command event)))
    (execute-command-type command :server (event-frame event) :client handler)))


#||
define method distribute-command-event
    (sheet :: <sheet>, command :: <command-oid>) => ()
  let frame = sheet-frame(sheet);
  when (frame)
    distribute-command-event(frame, command)
  end
end method distribute-command-event;
||#

(defmethod distribute-command-event ((sheet <sheet>) command)
  (check-type command <command-oid>)
  (let ((frame (sheet-frame sheet)))
    (when frame
      (distribute-command-event frame command))))


#||
define method distribute-command-event
    (frame :: <frame>, command :: <command-oid>) => ()
  let _port = port(frame);
  when (_port)
    distribute-event(_port, make(<command-event>,
				 frame:   frame,
				 command: command));
    let top-sheet = top-level-sheet(frame);
    when (top-sheet)
      generate-trigger-event(_port, top-sheet)
    end
  end
end method distribute-command-event;
||#

(defmethod distribute-command-event ((frame <frame>) command)
  (check-type command <command-oid>)
  (let ((_port (port frame)))
    (when _port
      (distribute-event _port (make-instance '<command-event>
					     :frame   frame
					     :command command))
      (let ((top-sheet (top-level-sheet frame)))
	(when top-sheet
	  (generate-trigger-event _port top-sheet))))))



;;; Make commands be legal callbacks for gadgets and frames
#||
//--- Should we be worried that the arguments are being ignored?
//--- It means that putting commands in for value callbacks will lose...
define sideways method execute-callback
    (gadget :: <gadget>, command :: <command>, #rest args) => ()
  ignore(args);
  //--- This could copy the command and plug in the new server and client...
  execute-command(command)
end method execute-callback;
||#

;;--- Should we be worried that the arguments are being ignored?
;;--- It means that putting commands in for value callbacks will lose...
(defmethod execute-callback ((gadget <gadget>) (command <command>) &rest args)
  (declare (ignore args))
  ;;--- This could copy the command and plug in the new server and client...
  (execute-command command))


#||
define sideways method execute-callback
    (gadget :: <gadget>, command-type :: subclass(<command>), #rest args) => ()
  ignore(args);
  execute-command-type(command-type, server: sheet-frame(gadget), client: gadget)
end method execute-callback;
||#

(defmethod execute-callback ((gadget <gadget>) (command-type class) &rest args)
  (declare (ignore args))
  ;; TODO: Is there anything that can be done to sort this out better?
  (assert (subtypep command-type (find-class '<command>)))
  (execute-command-type command-type :server (sheet-frame gadget) :client gadget))


#||
define sideways method execute-callback
    (gadget :: <gadget>, command-type :: <list>, #rest args) => ()
  ignore(args);
  execute-command-type(command-type, server: sheet-frame(gadget), client: gadget)
end method execute-callback;
||#

(defmethod execute-callback ((gadget <gadget>) (command-type list) &rest args)
  (declare (ignore args))
  (execute-command-type command-type :server (sheet-frame gadget) :client gadget))


#||
define sideways method execute-callback
    (frame :: <frame>, command :: <command>, #rest args) => ()
  ignore(args);
  //--- This could copy the command and plug in the new server and client...
  execute-command(command)
end method execute-callback;
||#

(defmethod execute-callback ((frame <frame>) (command <command>) &rest args)
  (declare (ignore args))
  ;;--- This could copy the command and plug in the new server and client...
  (execute-command command))


#||
define sideways method execute-callback
    (frame :: <frame>, command-type :: subclass(<command>), #rest args) => ()
  ignore(args);
  execute-command-type(command-type, server: frame, client: frame)
end method execute-callback;
||#

(defmethod execute-callback ((frame <frame>) (command-type class) &rest args)
  (declare (ignore args))
  ;; TODO: Can this be done more elegantly?
  (assert (subtypep command-type (find-class '<command>)))
  (execute-command-type command-type :server frame :client frame))


#||
define sideways method execute-callback
    (frame :: <frame>, command-type :: <list>, #rest args) => ()
  ignore(args);
  execute-command-type(command-type, server: frame, client: frame)
end method execute-callback;
||#

(defmethod execute-callback ((frame <frame>) (command-type list) &rest args)
  (declare (ignore args))
  (execute-command-type command-type :server frame :client frame))

