;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-FRAMES-INTERNALS -*-
(in-package #:duim-frames-internals)

#||
//// Dialog protocols

define protocol <<dialog-protocol>> ()
  getter dialog-image
    (dialog :: <frame>) => (image :: false-or(<image>));
  getter dialog-image-setter
    (image :: false-or(<image>), dialog :: <frame>)
 => (image :: false-or(<image>));
  getter dialog-exit-button
    (dialog :: <frame>) => (button :: false-or(<button>));
  setter dialog-exit-button-setter
    (button :: false-or(<button>), dialog :: <frame>)
 => (button :: false-or(<button>));
  getter dialog-cancel-button
    (dialog :: <frame>) => (button :: false-or(<button>));
  setter dialog-cancel-button-setter
    (button :: false-or(<button>), dialog :: <frame>)
 => (button :: false-or(<button>));
  getter dialog-apply-callback
    (dialog :: <frame>) => (callback :: false-or(<callback-type>));
  getter dialog-apply-button
    (dialog :: <frame>) => (button :: false-or(<button>));
  setter dialog-apply-button-setter
    (button :: false-or(<button>), dialog :: <frame>)
 => (button :: false-or(<button>));
  getter dialog-back-callback
    (dialog :: <frame>) => (callback :: false-or(<callback-type>));
  getter dialog-back-button
    (dialog :: <frame>) => (button :: false-or(<button>));
  setter dialog-back-button-setter
    (button :: false-or(<button>), dialog :: <frame>)
 => (button :: false-or(<button>));
  getter dialog-next-callback
    (dialog :: <frame>) => (callback :: false-or(<callback-type>));
  getter dialog-next-button
    (dialog :: <frame>) => (button :: false-or(<button>));
  setter dialog-next-button-setter
    (button :: false-or(<button>), dialog :: <frame>)
 => (button :: false-or(<button>));
  getter dialog-help-callback
    (dialog :: <frame>) => (callback :: false-or(<callback-type>));
  getter dialog-help-button
    (dialog :: <frame>) => (button :: false-or(<button>));
  setter dialog-help-button-setter
    (button :: false-or(<button>), dialog :: <frame>)
 => (button :: false-or(<button>));
  getter dialog-current-page
    (frame :: <frame>) => (page :: false-or(<abstract-sheet>));
  setter dialog-current-page-setter
    (page :: false-or(<abstract-sheet>), frame :: <frame>) => (page :: false-or(<abstract-sheet>));
  getter dialog-next-page
    (frame :: <frame>) => (page :: false-or(<abstract-sheet>));
  setter dialog-next-page-setter
    (page :: false-or(<abstract-sheet>), frame :: <frame>) => (page :: false-or(<abstract-sheet>));
  getter dialog-previous-page
    (frame :: <frame>) => (page :: false-or(<abstract-sheet>));
  setter dialog-previous-page-setter
    (page :: false-or(<abstract-sheet>), frame :: <frame>) => (page :: false-or(<abstract-sheet>));
  getter dialog-pages
    (frame :: <frame>) => (pages :: <sequence>);
  setter dialog-pages-setter
    (pages :: <sequence>, frame :: <frame>) => (pages :: <sequence>);
  getter dialog-page-complete?
    (frame :: <frame>) => (complete? :: <boolean>);
  setter dialog-page-complete?-setter
    (complete? :: <boolean>, frame :: <frame>) => (complete? :: <boolean>);
end protocol <<dialog-protocol>>;
||#
#||
(define-protocol <<dialog-protocol>> ()
  (:getter dialog-image (dialog))
  (:getter (setf dialog-image) (image dialog))
  (:getter dialog-exit-button (dialog))
  (:setter (setf dialog-exit-button) (button dialog))
  (:getter dialog-cancel-button (dialog))
  (:setter (setf dialog-cancel-button) (button dialog))
  (:getter dialog-apply-callback (dialog))
  (:getter dialog-apply-button (dialog))
  (:setter (setf dialog-apply-button) (button dialog))
  (:getter dialog-back-callback (dialog))
  (:getter dialog-back-button (dialog))
  (:setter (setf dialog-back-button) (button dialog))
  (:getter dialog-next-callback (dialog))
  (:getter dialog-next-button (dialog))
  (:setter (setf dialog-next-button) (button dialog))
  (:getter dialog-help-callback (dialog))
  (:getter dialog-help-button (dialog))
  (:setter (setf dialog-help-button) (button dialog))
  (:getter dialog-current-page (frame))
  (:setter (setf dialog-current-page) (page frame))
  (:getter dialog-next-page (frame))
  (:setter (setf dialog-next-page) (page frame))
  (:getter dialog-previous-page (frame))
  (:setter (setf dialog-previous-page) (page frame))
  (:getter dialog-pages (frame))
  (:setter (setf dialog-pages) (pages frame))
  (:getter dialog-page-complete? (frame))
  (:setter (setf dialog-page-complete?) (complete? frame)))
||#
#||
define protocol <<wizard-protocol>> (<<dialog-protocol>>)
  function compute-next-page
    (dialog :: <wizard-frame>) => (next-page :: false-or(<sheet>));
  function compute-previous-page
    (dialog :: <wizard-frame>) => (prev-page :: false-or(<sheet>));
end protocol <<wizard-protocol>>;
||#
#||
(define-protocol <<wizard-protocol>> (<<dialog-protocol>>)
  (:function compute-next-page (dialog))
  (:function compute-previous-page (dialog)))
||#

(defgeneric start-dialog (dialog)
  (:documentation
"
Displays a DUIM frame as a dialog box.

The function 'start-dialog' dynamically binds an <abort> restart
around the event loop for the dialog that is started. The restart
allows the event loop to be re-entered, and enables any callbacks run
from the dialog to signal an <abort> (via the 'abort' function, for
instance), in order to terminate execution of the current callback and
return to event processing. This facility is useful for implementing
operations that cancel gestures and for debugging DUIM applications
from Dylan debuggers.
"))

(defgeneric exit-dialog (dialog &key destroy?)
  (:documentation
"
Exits _dialog_, recording any changes to the information displayed in
the dialog that have been made by the user.

This is the default callback used for the exit button in a
dialog. This is the button that is typically labeled OK.

If _destroy?_ is t, then _dialog_ is destroyed.

Example

The following example defines a button, *yes-button*, that calls
exit-dialog as its activate-callback. This button is then used in a
dialog that simply replaces the standard exit button for the newly
defined dialog. Note that the example assumes the existence of a
similar *no-button* to replace the cancel button.

    (defparameter *yes-button*
                  (make-pane '<push-button> :label \"Yes\"
                             :activate-callback #'exit-dialog
                             :max-width $fill))

    (defparameter *dialog* 
                  (make-pane '<dialog-frame>,
                             :exit-button? nil
                             :cancel-button? nil
                             layout:
                             (vertically 
                                 (:x-alignment :center
                                  :y-spacing 5)
                               (make-pane '<label>, 
                                          :label \"Here is a label\")
                               (horizontally (:x-spacing 2)
                                 *yes-button*
                                 *no-button*))))

    (start-frame *dialog*)

"))

(defgeneric cancel-dialog (dialog &key destroy?)
  (:documentation
"
Cancels _dialog_ and removes it from the screen. Any changes that the
user has made to information displayed in the dialog is discarded.

If _destroy?_ is t then the dialog is unmapped from the screen.

This is the default callback used for the cancel button in a dialog.

Example:

The following example defines a button, *no-button*, that calls
'cancel-dialog' as its activate-callback. This button is then used in
a dialog that simply replaces the standard cancel button for the newly
defined dialog. Note that the example assumes the existence of a
similar *yes-button* to replace the exit button.

    (defparameter *no-button*
                  (make-pane '<push-button> :label \"No\"
                             :activate-callback #'cancel-dialog
                             :max-width +fill+))

    (defparameter *dialog*
                  (make-pane '<dialog-frame>
                             :exit-button nil
                             :cancel-button nil
                             :layout
                             (vertically ()
                               (make-pane '<label>
                                          :label \"Simple dialog\")
                               (horizontally ()
                                 *yes-button*
                                 *no-button*))))

    (start-frame *dialog*)
"))

(defgeneric dialog-next-button-visible? (dialog))
(defgeneric dialog-exit-button-visible? (dialog))
(defgeneric note-property-frame-current-page-changed (gadget))
(defgeneric (setf dialog-next-enabled?) (next-enabled? dialog)
  (:documentation
"
Enables or disables the Next button for dialog. This button is most
useful in multi-page dialogs such as property frames and wizard
frames, which typically have Back and Next buttons that let the user
navigate forward and backward through the sequence of pages that
comprise the dialog.

It is useful to be able to enable and disable the Next button at any
point in order to ensure that the user supplies all necessary
information before proceeding to the next page of the dialog. You can
do this by testing to see if the information on the page has been
specified with dialog-page-complete?, and then enabling or disabling
the Next button as appropriate.
"))

(defgeneric dialog-current-page-number (dialog))
(defgeneric move-to-next-page (dialog)
  (:documentation
"
Moves to the next page in sequence of _wizard_. This is the default
callback for the Next button in a wizard frame.
"))

(defgeneric move-to-previous-page (dialog)
  (:documentation
"
Moves to the previous page in sequence of _wizard_. This is the
default callback for the Back button in a wizard frame.
"))


#||
/// Dialogs

define open abstract primary class <dialog-frame> (<simple-frame>)
  // Exit button
  sealed slot dialog-exit-callback :: false-or(<function>) = exit-dialog,
    init-keyword: exit-callback:;
  sealed slot dialog-exit-button :: false-or(<button>) = #f,
    init-keyword: exit-button:;
  sealed slot dialog-exit-enabled? :: <boolean> = #t,
    init-keyword: exit-enabled?:,
    setter: %exit-enabled?-setter;
  // Cancel button
  sealed slot dialog-cancel-callback :: false-or(<function>) = cancel-dialog,
    init-keyword: cancel-callback:;
  sealed slot dialog-cancel-button :: false-or(<button>) = #f,
    init-keyword: cancel-button:;
  // Help button
  sealed slot dialog-help-callback :: false-or(<function>) = #f,
    init-keyword: help-callback:;
  sealed slot dialog-help-button :: false-or(<button>) = #f,
    init-keyword: help-button:;
  // Position, or #f meaning "don't lay out any exit buttons"
  sealed slot dialog-exit-buttons-position :: false-or(<symbol>) = #"bottom",
    init-keyword: exit-buttons-position:;
  // By default, dialogs are modal
  keyword mode: = #"modal";
end class <dialog-frame>;
||#

(defgeneric dialog-cancel-callback (dialog)
  (:documentation
"
Returns the function invoked when the cancel button is clicked in
_dialog_. This defaults to 'cancel-dialog'.
"))

(defgeneric (setf dialog-cancel-callback) (callback dialog)
  (:documentation
"
Sets the function invoked when the cancel button is clicked in
_dialog_.
"))

(defgeneric dialog-exit-callback (dialog)
  (:documentation
"
Returns the callback invoked when the Exit button is clicked in
_dialog_. The Exit button is commonly found in multi-page dialogs,
where the user is given the option to exit the sequence at any
point (as well as navigate through the sequence using Next and Back
buttons).
"))

(defgeneric (setf dialog-exit-callback) (callback dialog)
  (:documentation
"
Sets the callback invoked when the Exit button is clicked in
_dialog_. The Exit button is commonly found in multi-page dialogs,
where the user is given the option to exit the sequence at any
point (as well as navigate through the sequence using Next and Back
buttons).

If you do not supply this callback, then the default behaviour is to
quit the dialog when the Exit button is clicked. This is normally the
action that you will want. Specifying your own callback gives you
flexibility in describing other actions to be performed when the
dialog is exited. In addition, supplying nil means that no Exit button
is displayed at all.
"))

(defgeneric dialog-exit-enabled? (dialog)
  (:documentation
"
Returns true if the Exit button has been enabled for _dialog_. The
Exit button is commonly found in multi-page dialogs, where the user is
given the option to exit the sequence at any point (as well as
navigate through the sequence using Next and Back buttons).
"))

(defgeneric (setf dialog-exit-enabled?) (enabled? dialog)
  (:documentation
"
Enables or disables the Exit button for _dialog_. The Exit button is
commonly found in multi-page dialogs, where the user is given the
option to exit the sequence at any point (as well as navigate through
the sequence using Next and Back buttons).

Example:

In this example, a dialog is created, and then its exit button is
disabled. When displayed on the screen, the exit button is grayed out.

    (defparameter *dialog*
                  (make-instance '<dialog-frame>
                                 :exit-button? t
                                 :cancel-button? t
                                 :help-callback
                                 #'(lambda (gadget)
                                     (notify-user
                                       (format nil
                                               \"Here is some help\")
                                       :owner gadget))))

    (setf (dialog-exit-enabled? *dialog*) nil)
    (start-frame *dialog*)
"))

(defclass <dialog-frame> (<simple-frame>)
  ;; Exit button
  ((dialog-exit-callback :type (or null function) :initarg :exit-callback :initform #'exit-dialog :accessor dialog-exit-callback)
   (dialog-exit-button :type (or null <button>) :initarg :exit-button :initform nil :accessor dialog-exit-button)
   ;; Allow any value here; restricting to t or nil is too limiting
   (dialog-exit-enabled? #||:type boolean||# :initarg :exit-enabled? :initform t :reader dialog-exit-enabled? :writer %exit-enabled?-setter)
   ;; Cancel button
   (dialog-cancel-callback :type (or null function) :initarg :cancel-callback :initform #'cancel-dialog :accessor dialog-cancel-callback)
   (dialog-cancel-button :type (or null <button>) :initarg :cancel-button :initform nil :accessor dialog-cancel-button)
   ;; Help button
   (dialog-help-callback :type (or null function) :initarg :help-callback :initform nil :accessor dialog-help-callback)
   (dialog-help-button :type (or null <button>) :initarg :help-button :initform nil :accessor dialog-help-button)
   ;; Position, or nil meaning "don't lay out any exit buttons"
   (dialog-exit-buttons-position :type (or null symbol) :initarg :exit-buttons-position
				 :initform :bottom :accessor dialog-exit-buttons-position))
  ;; By default, dialogs are modal
  (:default-initargs :mode :modal)
  (:documentation
"
The class of dialog frames. These frames let you create dialog boxes
for use in your applications. All buttons in a dialog frame are
automatically made the same size, and are placed at the bottom of the
dialog by default. When at the bottom of the dialog, buttons are
right-aligned.

By default, all dialogs are modal, that is, when displayed, they take
over the entire application thread, preventing the user from using any
other part of the application until the dialog has been removed from
the screen. To create a modeless dialog (that is, one that can remain
displayed on the screen while the user interacts with the application
in other ways) you should set the :mode keyword to :modeless. Note,
however, that you should not normally need to do this; if you need to
create a modeless dialog, then you should consider using a normal DUIM
frame, rather than a dialog frame.

The initargs :exit-button and :cancel-button specify the exit and
cancel buttons in the dialog. The user clicks on the exit button to
dismiss the dialog and save any changes that have been made as a
result of editing the information in the dialog. The user clicks on
the cancel button in order to dismiss the dialog and discard any
changes that have been made.

In addition, the :exit-callback and :cancel-callback initargs specify
the callback that is invoked when the Exit or Cancel buttons in the
dialog are clicked on. These both default to the appropriate function
for each button, but you have the flexibility to specify an
alternative if you wish. If you do not require a Cancel button in your
dialog, sepcify :cancel-callback nil. Similarly,
specify :exit-callback nil if you do not require and Exit button.

All dialogs should have an exit button, and most dialogs should have a
cancel button too. You should only omit the cancel button in cases
where the information being displayed in the dialog cannot be changed
by the user. For example, a dialog containing an error message can
have only and exit button, but any dialog that contains information
the user can edit should have both exit and cancel buttons.

Two initargs are available for each button so that a given button may
be specified for a particular dialog, but need only be displayed under
certain circumstances. This lets you define subtly different behaviour
in different situations.

The :exit-enabled? initarg is used to specify whether the exit button
on the dialog is enabled or not. If nil, then the exit button is
displayed on the dialog, but it is grayed out.

The :help-button initarg specifies the help button in the dialog. Note
that, in contrast to the exit and cancel buttons, specifying the
button gadget to use in a dialog determines its presence in the
dialog: it is not possible to define a help button and then only
display it in certain circumstances. You are strongly encouraged to
provide a help button in all but the most trivial dialogs.

The :help-callback initarg defines a callback function that is invoked
when the help button is clicked. This should normally display a
context-sensitive help topic from the help file supplied with the
application, although you might also choose to display an alert box
with the relevant information.

The :exit-buttons-position initarg defines the position in the dialog
that the exit and cancel buttons occupy (and any other standard
buttons, if they have been specified). By default, buttons are placed
where the interface guidelines for the platform recommend, and this
position is encouraged in most interface design guidelines. Usually,
this means that buttons are placed at the bottom of the dialog. Less
commonly, buttons may also be placed on the right side of the
dialog. Buttons are not normally placed at the top or on the left of
the dialog, although this is possible if desired.

The :pages initarg is used for multi-page dialogs such as property
frames and wizard frames. If used, it should be a sequence of
elements, each of which evaluates to an instance of a page.

The :page-changed-callback is a callback function that is invoked when
a different page in a multi-page dialog is displayed.

Example:

The following example creates and displays a simple dialog that
contains only an exit button, cancel button, and help button, and
assigns a callback to the help button.

    (defparameter *dialog*
                  (make-instance '<dialog-frame>
                                 :exit-button? t
                                 :cancel-button? t
                                 :help-callback
                                 #'(lambda (gadget)
                                     (notify-user
                                       (format nil
                                               \"Here is some help\")
                                       :owner gadget))))

    (start-frame *dialog*)
")
  (:metaclass <abstract-metaclass>))


#||
define sealed class <concrete-dialog-frame> (<dialog-frame>)
end class <concrete-dialog-frame>;

define sealed domain make (singleton(<concrete-dialog-frame>));
define sealed domain initialize (<concrete-dialog-frame>);
||#

(defclass <concrete-dialog-frame> (<dialog-frame>) ())


#||
define sealed inline method make
    (class == <dialog-frame>, #rest initargs, #key, #all-keys)
 => (pane :: <concrete-dialog-frame>)
  apply(make, <concrete-dialog-frame>, initargs)
end method make;
||#

(defmethod make-dialog-frame (&rest initargs &key &allow-other-keys)
  (apply #'make-instance '<concrete-dialog-frame> initargs))


#||
define method initialize
    (dialog :: <dialog-frame>,
     #key exit-buttons? = #t, save-under? = #f,
	  minimize-box? = #f, maximize-box? = #f) => ()
  next-method();
  frame-flags(dialog)
    := logior(logand(frame-flags(dialog),
		     lognot(logior(%frame_minimize_box, %frame_maximize_box))),
	      if (minimize-box?) %frame_minimize_box else 0 end,
	      if (maximize-box?) %frame_maximize_box else 0 end);
  unless (exit-buttons?)
    // If the user asked for no exit buttons, just claim their position is #f.
    // We do this because the user might want to lay out this own exit buttons,
    // but still need 'dialog-exit-button' and 'dialog-cancel-button' to be set up.
    dialog-exit-buttons-position(dialog) := #f
  end
end method initialize;
||#


(defmethod initialize-instance :after ((dialog <dialog-frame>)
				       &key (exit-buttons? t) (save-under? nil)
				       (minimize-box? nil) (maximize-box? nil)
				       &allow-other-keys)
  (declare (ignore save-under?))
  (setf (frame-flags dialog)
	(logior (logand (frame-flags dialog)
			(lognot (logior %frame_minimize_box %frame_maximize_box)))
		(if minimize-box? %frame_minimize_box 0)
		(if maximize-box? %frame_maximize_box 0)))
  (unless exit-buttons?
    ;; If the user asked for no exit buttons, just claim their position is #f.
    ;; We do this because the user might want to lay out his own exit buttons,
    ;; but still need 'dialog-exit-button' and 'dialog-cancel-button' to be
    ;; set up.
    (setf (dialog-exit-buttons-position dialog) nil)))



#||

/// Starting dialogs

define method start-dialog
    (dialog :: <dialog-frame>)
 => (#rest values :: <object>)
  start-frame(dialog)
end method start-dialog;
||#

(defmethod start-dialog ((dialog <dialog-frame>))
  (start-frame dialog))


#||
define method start-frame
    (dialog :: <dialog-frame>)
 => (status-code :: false-or(<integer>))
  let owner  = frame-owner(dialog);
  let modal? = (frame-mode(dialog) == #"modal");
  if (owner & modal?)
    // If this is a modal dialog, the owning frame needs to be disabled
    // for the duration of the dialog
    let old-state = frame-enabled?(owner);
    block ()
      frame-enabled?(owner) := #f;
      next-method();
    cleanup
      frame-enabled?(owner) := old-state
    end
  else
    next-method()
  end
end method start-frame;
||#

(defmethod start-frame ((dialog <dialog-frame>))
  (let ((owner  (frame-owner dialog))
	(modal? (eql (frame-mode dialog) :modal)))
    (if (and owner modal?)
	;; If this is a modal dialog, the owning frame needs to be disabled
	;; for the duration of the dialog
	(let ((old-state (frame-enabled? owner)))
	  (unwind-protect
	       (progn
		 (setf (frame-enabled? owner) nil)
		 (call-next-method))
	    ;; cleanup
            (setf (frame-enabled? owner) old-state)))
	;; else (not modal, no owner)
	(call-next-method))))


#||
/// Exiting dialogs

define method exit-dialog
    (dialog :: <dialog-frame>, #key destroy? = #t) => ()
  let _port = port(dialog);
  when (_port)
    distribute-event(_port, make(<frame-exit-event>,
				 frame: dialog,
				 destroy-frame?: destroy?));
    let top-sheet = top-level-sheet(dialog);
    when (top-sheet)
      generate-trigger-event(_port, top-sheet)
    end
  end
end method exit-dialog;
||#

(defmethod exit-dialog ((dialog <dialog-frame>) &key (destroy? t))
  (let ((_port (port dialog)))
    (when _port
      (distribute-event _port (make-instance '<frame-exit-event>
					     :frame dialog
					     :destroy-frame? destroy?))
      (let ((top-sheet (top-level-sheet dialog)))
	(when top-sheet
	  (generate-trigger-event _port top-sheet))))))


#||
define method exit-dialog
    (sheet :: <sheet>, #key destroy? = #t) => ()
  let dialog = sheet-frame(sheet);
  exit-dialog(dialog, destroy?: destroy?)
end method exit-dialog;
||#

(defmethod exit-dialog ((sheet <sheet>) &key (destroy? t))
  (let ((dialog (sheet-frame sheet)))
    (exit-dialog dialog :destroy? destroy?)))


#||
define open generic do-exit-dialog 
    (framem :: <abstract-frame-manager>, dialog :: <dialog-frame>, #key destroy?) => ();
||#

(defgeneric do-exit-dialog (framem dialog &key destroy?))


#||
define method handle-event
    (dialog :: <dialog-frame>, event :: <frame-exit-event>) => ()
  when (dialog-exit-enabled?(dialog))
    do-exit-dialog(frame-manager(dialog), dialog, destroy?: event-destroy-frame?(event))
  end
end method handle-event;
||#

(defmethod handle-event ((dialog <dialog-frame>) (event <frame-exit-event>))
  (when (dialog-exit-enabled? dialog)
    (do-exit-dialog (frame-manager dialog) dialog :destroy? (event-destroy-frame? event))))


#||
define method handle-event
    (dialog :: <dialog-frame>, event :: <dialog-exit-event>) => ()
  do-exit-frame(frame-manager(dialog), dialog,
		destroy?: event-destroy-frame?(event), status-code: 0)
end method handle-event;
||#

(defmethod handle-event ((dialog <dialog-frame>) (event <dialog-exit-event>))
  (do-exit-frame (frame-manager dialog) dialog
		 :destroy? (event-destroy-frame? event) :status-code 0))


#||
define method dialog-exit-enabled?-setter
    (enabled? :: <boolean>, dialog :: <dialog-frame>) => (true? :: <boolean>)
  let exit-button = dialog-exit-button(dialog);
  when (exit-button)
    let default-button = frame-default-button(dialog);
    case
      enabled? & ~default-button =>
        frame-default-button(dialog) := exit-button;
      ~enabled? & default-button = exit-button =>
        frame-default-button(dialog) := #f;
      otherwise =>
        #f
    end;
    gadget-enabled?(exit-button) := enabled?
  end;
  dialog.%exit-enabled? := enabled?
end method dialog-exit-enabled?-setter;
||#

(defmethod (setf dialog-exit-enabled?) (enabled? (dialog <dialog-frame>))
  (let ((exit-button (dialog-exit-button dialog)))
    (when exit-button
      (let ((default-button (frame-default-button dialog)))
	(cond ((and enabled? (not default-button))
               (setf (frame-default-button dialog) exit-button))
	      ((and (not enabled?) (eql default-button exit-button))
               (setf (frame-default-button dialog) nil))
	      (t
	       nil))
        (setf (gadget-enabled? exit-button) enabled?)))
    (%exit-enabled?-setter enabled? dialog)))


#||
/// Canceling dialogs

define method cancel-dialog
    (dialog :: <dialog-frame>, #key destroy? = #t) => ()
  do-cancel-dialog(frame-manager(dialog), dialog, destroy?: destroy?)
end method cancel-dialog;
||#

(defmethod cancel-dialog ((dialog <dialog-frame>) &key (destroy? t))
  (do-cancel-dialog (frame-manager dialog) dialog :destroy? destroy?))


#||
define method cancel-dialog
    (sheet :: <sheet>, #key destroy? = #t) => ()
  let dialog = sheet-frame(sheet);
  cancel-dialog(dialog, destroy?: destroy?)
end method cancel-dialog;
||#

(defmethod cancel-dialog ((sheet <sheet>) &key (destroy? t))
  (let ((dialog (sheet-frame sheet)))
    (cancel-dialog dialog :destroy? destroy?)))


#||
define open generic do-cancel-dialog 
    (framem :: <abstract-frame-manager>, dialog :: <dialog-frame>, #key destroy?) => ();
||#

(defgeneric do-cancel-dialog (framem dialog &key destroy?))


#||
define method handle-event
    (dialog :: <dialog-frame>, event :: <dialog-cancel-event>) => ()
  do-exit-frame(frame-manager(dialog), dialog,
		destroy?: event-destroy-frame?(event))
end method handle-event;
||#

(defmethod handle-event ((dialog <dialog-frame>) (event <dialog-cancel-event>))
  (do-exit-frame (frame-manager dialog) dialog
		 :destroy? (event-destroy-frame? event)))



#||
/// Simple chooser

define sealed class <simple-chooser-dialog> (<dialog-frame>)
end class <simple-chooser-dialog>;

define sealed domain make (singleton(<simple-chooser-dialog>));
define sealed domain initialize (<simple-chooser-dialog>);
||#

(defclass <simple-chooser-dialog> (<dialog-frame>) ())


#||
// Default implementation of dialog chooser when handed a sequence of items.
// This is "sideways" because it is a forward reference from DUIM-Sheets.
define sideways method do-choose-from-dialog
    (framem :: <frame-manager>, owner :: <sheet>, items :: <sequence>,
     #key title, value,
          selection-mode = #"single",
	  gadget-class = <list-box>, gadget-options = #[],
          label-key = collection-gadget-default-label-key,
          value-key = collection-gadget-default-value-key,
          width, height, foreground, background, text-style,
     #all-keys)
 => (value, success? :: <boolean>,
     width :: false-or(<integer>), height :: false-or(<integer>))
  let value
    = if (selection-mode == #"single") value | (~empty?(items) & items[0]) else value end;
  with-frame-manager (framem)
    let gadget
      = apply(make-pane, gadget-class,
	      items: items,
	      selection-mode: selection-mode,
	      title: title,
	      value: value,
	      label-key: label-key,
	      value-key: value-key,
	      activate-callback: exit-dialog,
	      foreground: foreground,
	      background: background,
	      text-style: text-style,
	      gadget-options);
    let dialog
      = make(<simple-chooser-dialog>,
	     mode: #"modal",
	     owner: sheet-frame(owner),
	     title: title,
	     layout: gadget,		// the gadget is the layout
	     input-focus: gadget,	// ensure the gadget has the focus
	     width:  if (width  = $fill) #f else width  | 300 end,
	     height: if (height = $fill) #f else height | 200 end);
    let status-code     = start-dialog(dialog);
    let (width, height) = frame-size(dialog);
    values(status-code & gadget-value(gadget),
	   status-code & #t,
	   width, height)
  end
end method do-choose-from-dialog;
||#

;; Default implementation of dialog chooser when handed a sequence of items.
;; This is "sideways" because it is a forward reference from DUIM-Sheets.
(defmethod do-choose-from-dialog ((framem <frame-manager>) (owner <sheet>) (items sequence)
				  &key title value
				    (selection-mode :single)
				    (gadget-class (find-class '<list-box>)) (gadget-options (vector))
				    (label-key #'collection-gadget-default-label-key)
				    (value-key #'collection-gadget-default-value-key)
				    width height foreground background text-style
				    &allow-other-keys)
  (let ((value (if (eql selection-mode :single)
		   (or value (and (not (empty? items)) (SEQUENCE-ELT items 0)))
		   value)))
    (with-frame-manager (framem)
      (let* ((gadget (apply #'make-pane gadget-class
			    :items items
			    :selection-mode selection-mode
			    :title title
			    :value value
			    :label-key label-key
			    :value-key value-key
			    :activate-callback #'exit-dialog
			    :foreground foreground
			    :background background
			    :text-style text-style
			    gadget-options))
	     (dialog (make-instance '<simple-chooser-dialog>
			   :mode :modal
			   :owner (sheet-frame owner)
			   :title title
			   :layout gadget        ;; the gadget is the layout
			   :input-focus gadget   ;; ensure the gadget has the focus
			   :width (if (eql width +fill+) nil (or width 300))
			   :height (if (eql height +fill+) nil (or height 200))))
	     (status-code (start-dialog dialog)))
	(multiple-value-bind (width height)
	    (frame-size dialog)
	  (values (and status-code (gadget-value gadget))
		  (and status-code t)
		  width height))))))



#||

/// Multi-page dialog

define open abstract primary class <multi-page-dialog-frame> (<dialog-frame>)
  // #f means the pages have yet to be initialized, so that
  // the 'pages' section of 'define frame' knows when it is done
  slot %pages :: false-or(<sequence>) = #f,
    init-keyword: pages:;
  slot dialog-page-changed-callback :: false-or(<callback-type>) = #f,
    init-keyword: page-changed-callback:;
end class <multi-page-dialog-frame>;
||#

(defgeneric dialog-page-changed-callback (dialog)
  (:documentation
"
Returns the page-changed-callback of _dialog_. This is the callback
function used to test whether the information in the current page of
_dialog_ has changed. This callback is useful when using multi-page
dialogs, as a test that can be performed before the next page of the
dialog is displayed.
"))

(defgeneric (setf dialog-page-changed-callback) (callback dialog)
  (:documentation
"
Sets the page-changed-callback of _dialog_. This is the callback
function used to test whether the information in the current page of
_dialog_ has changed. This callback is useful when using multi-page
dialogs, as a test that can be performed before the next page of the
dialog is displayed.
"))

(defclass <multi-page-dialog-frame> (<dialog-frame>)
  ;; nil means the pages have yet to be initialized, so that
  ;; the 'pages' section of 'define-frame' knows when it is done
  ((%pages :type (or null sequence) :initarg :pages :initform nil :accessor %pages)
   (dialog-page-changed-callback :type (or null <callback-type>)
				 :initarg :page-changed-callback :initform nil
				 :accessor dialog-page-changed-callback))
  (:metaclass <abstract-metaclass>))


#||  
define open generic update-dialog-buttons 
    (dialog :: <multi-page-dialog-frame>) => ();
||#

(defgeneric update-dialog-buttons (dialog))


#||
define open generic note-dialog-page-changed
    (dialog :: <multi-page-dialog-frame>) => ();
||#

(defgeneric note-dialog-page-changed (dialog))


#||
define method initialize
    (dialog :: <multi-page-dialog-frame>, #key page) => ()
  next-method();
  let pages = dialog-pages(dialog);
  when (~dialog-current-page(dialog) & ~empty?(pages))
    let new-page = page | pages[0];
    dialog-current-page(dialog) := new-page
  end;
  note-dialog-page-changed(dialog)
end method initialize;
||#

(defmethod initialize-instance :after ((dialog <multi-page-dialog-frame>) &key page &allow-other-keys)
  (let ((pages (dialog-pages dialog)))
    (when (and (not (dialog-current-page dialog)) (not (empty? pages)))
      (let ((new-page (or page (SEQUENCE-ELT pages 0))))
        (setf (dialog-current-page dialog) new-page)))
    (note-dialog-page-changed dialog)))


#||
define method dialog-pages
    (dialog :: <multi-page-dialog-frame>) => (pages :: <sequence>)
  dialog.%pages | #[]
end method dialog-pages;
||#

(defmethod dialog-pages ((dialog <multi-page-dialog-frame>))
  (or (%pages dialog) #()))


#||
define method dialog-pages-setter
    (pages :: <sequence>, dialog :: <multi-page-dialog-frame>)
 => (pages :: <sequence>)
  unless (pages = dialog-pages(dialog))
    dialog.%pages := pages
  end;
  pages
end method dialog-pages-setter;
||#

(defmethod (setf dialog-pages) ((pages sequence) (dialog <multi-page-dialog-frame>))
  (unless (equal? pages (dialog-pages dialog))
    (setf (%pages dialog) pages))
  pages)


#||
define method note-dialog-page-changed
    (dialog :: <multi-page-dialog-frame>) => ()
  #f
end method note-dialog-page-changed;
||#

(defmethod note-dialog-page-changed ((dialog <multi-page-dialog-frame>))
  nil)


#||
define method dialog-next-button-visible?
    (dialog :: <multi-page-dialog-frame>) => (visible? :: <boolean>)
  let next-button = dialog-next-button(dialog);
  next-button & ~sheet-withdrawn?(next-button)
end method dialog-next-button-visible?;
||#

(defmethod dialog-next-button-visible? ((dialog <multi-page-dialog-frame>))
  (let ((next-button (dialog-next-button dialog)))
    (and next-button (not (sheet-withdrawn? next-button)))))


#||
define method dialog-exit-button-visible?
    (dialog :: <multi-page-dialog-frame>) => (visible? :: <boolean>)
  let exit-button = dialog-exit-button(dialog);
  exit-button & ~sheet-withdrawn?(exit-button)
end method dialog-exit-button-visible?;
||#

(defmethod dialog-exit-button-visible? ((dialog <multi-page-dialog-frame>))
  (let ((exit-button (dialog-exit-button dialog)))
    (and exit-button (not (sheet-withdrawn? exit-button)))))


#||
define method dialog-page-complete?
    (dialog :: <multi-page-dialog-frame>) => (complete? :: <boolean>)
  case
    dialog-next-button-visible?(dialog) => dialog-next-enabled?(dialog);
    dialog-exit-button-visible?(dialog) => dialog-exit-enabled?(dialog);
    otherwise                           => #f;
  end
end method dialog-page-complete?;
||#

(defmethod dialog-page-complete? ((dialog <multi-page-dialog-frame>))
  (cond ((dialog-next-button-visible? dialog) (dialog-next-enabled? dialog))
	((dialog-exit-button-visible? dialog) (dialog-exit-enabled? dialog))
	(t nil)))


#||
define method dialog-page-complete?-setter
    (complete? :: <boolean>, dialog :: <multi-page-dialog-frame>)
 => (complete? :: <boolean>)
  let next-button-visible? = sheet-mapped?(dialog-next-button(dialog));
  let exit-button-visible? = sheet-mapped?(dialog-exit-button(dialog));
  when (dialog-next-button-visible?(dialog))
    dialog-next-enabled?(dialog) := complete?
  end;
  when (dialog-exit-button-visible?(dialog))
    dialog-exit-enabled?(dialog) := complete?
  end;
  complete?
end method dialog-page-complete?-setter;
||#

(defmethod (setf dialog-page-complete?) (complete? (dialog <multi-page-dialog-frame>))
  (let ((next-button-visible? (sheet-mapped? (dialog-next-button dialog)))
	(exit-button-visible? (sheet-mapped? (dialog-exit-button dialog))))
    (declare (ignorable next-button-visible? exit-button-visible?))
    (when (dialog-next-button-visible? dialog)
      (setf (dialog-next-enabled? dialog) complete?))
    (when (dialog-exit-button-visible? dialog)
      (setf (dialog-exit-enabled? dialog) complete?))
    complete?))



#||
/// Page changed callback

define sealed class <page-changed-dialog-event> (<frame-event>)
end class <page-changed-dialog-event>;

define sealed domain make (singleton(<page-changed-dialog-event>));
define sealed domain initialize (<page-changed-dialog-event>);
||#

(defclass <page-changed-dialog-event> (<frame-event>) ())


#||
define sealed method handle-event
    (dialog :: <multi-page-dialog-frame>, event :: <page-changed-dialog-event>) => ()
  execute-page-changed-callback(dialog)
end method handle-event;
||#

(defmethod handle-event ((dialog <multi-page-dialog-frame>) (event <page-changed-dialog-event>))
  (execute-page-changed-callback dialog))


#||
define function execute-page-changed-callback
    (dialog :: <dialog-frame>) => ()
  let callback = dialog-page-changed-callback(dialog);
  callback & execute-callback(dialog, callback, dialog)
end function execute-page-changed-callback;
||#

(defun execute-page-changed-callback (dialog)
  (check-type dialog <dialog-frame>)
  (let ((callback (dialog-page-changed-callback dialog)))
    (and callback (execute-callback dialog callback dialog))))


#||
define function distribute-page-changed-callback
    (dialog :: <multi-page-dialog-frame>) => ()
  distribute-event(port(dialog), 
		   make(<page-changed-dialog-event>, frame: dialog))
end function distribute-page-changed-callback;
||#

(defun distribute-page-changed-callback (dialog)
  (check-type dialog <multi-page-dialog-frame>)
  (distribute-event (port dialog)
		    (make-instance '<page-changed-dialog-event> :frame dialog)))



#||
/// Property frames

define open primary class <property-frame> 
    (<multi-page-dialog-frame>)
  sealed slot dialog-apply-callback :: false-or(<callback-type>) = #f,
    init-keyword: apply-callback:;
  sealed slot dialog-apply-button :: false-or(<button>) = #f,
    init-keyword: apply-button:;
  // By default, property sheets are modeless
  keyword mode: = #"modeless";
end class <property-frame>;
||#


(defclass <property-frame> (<multi-page-dialog-frame>)
  ((dialog-apply-callback :type (or null <callback-type>) :initarg :apply-callback :initform nil :accessor dialog-apply-callback)
   (dialog-apply-button :type (or null <button>) :initarg :apply-button :initform nil :accessor dialog-apply-button))
  ;; By default, property sheets are modeless
  (:default-initargs :mode :modeless)
  (:documentation
"
The class of property frames. These are dialogs that can contain
property sheets of some description. This is the class of dialogs with
several pages, each presented as a label in a tab control.

The :pages initarg defines the pages available for the property frame.

The apply callback and button define an additional Apply button
available in property frames. The Apply button applies any changes
made in the current page of the dialog, but does not dismiss the
dialog from the screen. By default, there is no Apply button defined.

The page-changed callback lets you specify a callback that should be
invoked if the current page in the property frame is changed by
clicking on a different page tab.
"))


#||
define method initialize
    (frame :: <property-frame>, #key frame-manager: framem) => ()
  let tab-control
    = make(<tab-control>,
	   pages: dialog-pages(frame),
	   value-changed-callback: note-property-frame-current-page-changed,
	   frame-manager: framem);
  frame-layout(frame) := tab-control;
  next-method()
end method initialize; 
||#

;; Yes, this is on the PRIMARY method. We want this to run before any
;; usual :after methods, but after the shared-initialize (specifically,
;; BEFORE the init-instance :after method on <multi-page-dialog-frame>
;; which is a superclass...
(defmethod initialize-instance ((frame <property-frame>) &key ((:frame-manager framem)) &allow-other-keys)
  (call-next-method)
  (let ((tab-control (make-pane '<tab-control>
				:pages (dialog-pages frame)
				:value-changed-callback #'note-property-frame-current-page-changed
				:frame-manager framem)))
    (setf (frame-layout frame) tab-control)))


#||
define method note-property-frame-current-page-changed
    (gadget :: <tab-control>) => ()
  let dialog = sheet-frame(gadget);
  execute-page-changed-callback(dialog)
end method note-property-frame-current-page-changed;
||#

(defmethod note-property-frame-current-page-changed ((gadget <tab-control>))
  (let ((dialog (sheet-frame gadget)))
    (execute-page-changed-callback dialog)))



#||
/// Property pages

define open abstract class <property-page> (<basic-page>)
end class <property-page>;
||#


(defclass <property-page> (<basic-page>)
  ()
  (:documentation
"
The class of property pages. These are pages that can be displayed in
an instance of <property-frame>.

Internally, this class maps into the Windows property page control.
")
  (:metaclass <abstract-metaclass>))


#||
define sealed class <property-page-pane>
    (<property-page>, <single-child-wrapping-pane>)
end class <property-page-pane>;
||#


(defclass <property-page-pane>
    (<property-page> <single-child-wrapping-pane>)
  ())


#||
define method class-for-make-pane
    (framem :: <frame-manager>, class == <property-page>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<property-page-pane>, #f)
end method class-for-make-pane;

define sealed domain make (singleton(<property-page-pane>));
define sealed domain initialize (<property-page-pane>);
||#

(defmethod class-for-make-pane ((framem <frame-manager>) (class (eql (find-class '<property-page>))) &key)
  (values (find-class '<property-page-pane>) nil))



#||
/// Wizard frames

define open primary class <wizard-frame> 
    (<multi-page-dialog-frame>)
  slot dialog-image :: false-or(<image>) = #f,
    init-keyword: image:;
  slot dialog-back-button :: false-or(<button>) = #f,
    init-keyword: back-button:;
  slot dialog-next-button :: false-or(<button>) = #f,
    init-keyword: next-button:;
  slot dialog-next-enabled? :: <boolean> = #f,
    init-keyword: next-enabled?:,
    setter: %next-enabled?-setter;
  constant slot dialog-next-callback :: false-or(<callback-type>) = move-to-next-page,
    init-keyword: next-callback:;
  constant slot dialog-back-callback :: false-or(<callback-type>) = move-to-previous-page,
    init-keyword: back-callback:;
  slot dialog-next-page :: false-or(<sheet>) = #f;
  slot dialog-previous-page :: false-or(<sheet>) = #f;
  // By default, wizards are modal
  keyword mode: = #"modal";
  keyword exit-enabled?: = #f;
end class <wizard-frame>;
||#

(defgeneric dialog-next-enabled? (dialog)
  (:documentation
"
Returns true if the Next button has been enabled for dialog. This
button is most useful in multi-page dialogs such as property frames
and wizard frames, which typically have Back and Next buttons that let
the user navigate forward and backward through the sequence of pages
that comprise the dialog.
"))

(defclass <wizard-frame> (<multi-page-dialog-frame>)
  ((dialog-image :type (or null <image>) :initarg :image :initform nil :accessor dialog-image)
   (dialog-back-button :type (or null <button>) :initarg :back-button :initform nil :accessor dialog-back-button)
   (dialog-next-button :type (or null <button>) :initarg :next-button :initform nil :accessor dialog-next-button)
   (dialog-next-enabled? :type boolean :initarg :next-enabled? :initform nil :reader dialog-next-enabled?
			 :writer %next-enabled?-setter)
   (dialog-next-callback :type (or null <callback-type>) :initarg :next-callback :initform #'move-to-next-page
			 :reader dialog-next-callback)
   (dialog-back-callback :type (or null <callback-type>) :initarg :back-callback :initform #'move-to-previous-page
			 :reader dialog-back-callback)
   (dialog-next-page :type (or null <sheet>) :initform nil :accessor dialog-next-page)
   (dialog-previous-page :type (or null <sheet>) :initform nil :accessor dialog-previous-page))
  ;; By default, wizards are modal
  (:default-initargs :mode :modal :exit-enabled? nil)
  (:documentation
"
The class of wizard frames. These are frames that are used to create
wizards (series of connected dialogs) that are used to guide the user
through a structured task, such as installing an application.

A wizard frame is a multi-page dialog, in which the user specifies
requested information before proceeding to the next page in the
sequence. At the end of the sequence, the user exits the dialog to
send the relevant information back to the controlling application.

When a wizard frame is created, each page in the frame automatically
has a Next and Back button to let the user navigate forward and
backward through the sequence of pages.

In addition, if :apply-button is specified, an Apply button is
displayed in the frame. By default, clicking on this button lets the
user apply the changes made so far without dismissing the frame from
the screen. If specified, the :apply-callback function is invoked when
the Apply button is clicked.

The layout of a wizard frame is controlled using a <stack-layout>.

Example

    (define-frame <my-wizard> (<wizard-frame>)
      ((:pane name-pane (frame)
         (make-pane '<text-field>))
       (:pane organization-pane (frame)
         (make-pane '<text-field>))
       (:pane job-description-pane (frame)
         (make-pane '<text-field>))
       (:pane years-employed-pane (frame)
         (make-pane '<text-field>
                    :value-type (find-class 'integer)))
       (:pane first-page-layout (frame)
         (make-pane '<table-layout>
                    :columns 2
                    :x-alignment #(:right :left)
                    :children
                    (vector (make-pane '<label>,
                                       :label \"Name:\")
                            (name-pane frame)
                            (make-pane '<label>
                                       :label \"Organization:\")
                            (organization-pane frame))))
       (:pane second-page-layout (frame)
         (make-pane '<table-layout>
                    :columns 2
                    :x-alignment #(:right :left)
                    :children
                    (vector (make-pane '<label>
                                       :label \"Job Description:\")
                            (job-description-pane frame)
                            (make-pane '<label>
                                       :label \"Years Employed:\")
                            (years-employed-pane frame))))
       (:pane first-page (frame)
         (make-pane '<wizard-page>
                    :child (first-page-layout frame)))
       (:pane second-page (frame)
         (make-pane '<wizard-page>
                    :child (second-page-layout frame)))
       (:pages (frame)
         (vector (first-page frame) (second-page frame)))
       (:default-initargs :title \"My Wizard\")))
"))

#||
define method dialog-next-enabled?-setter
    (enabled? :: <boolean>, dialog :: <wizard-frame>) => (true? :: <boolean>)
  let next-button = dialog-next-button(dialog);
  when (next-button)
    let default-button = frame-default-button(dialog);
    let back-button = dialog-back-button(dialog);
    case
      enabled? & ~default-button =>
        frame-default-button(dialog) := next-button;
      ~enabled? & default-button = next-button =>
        frame-default-button(dialog) := back-button;
      otherwise =>
        #f
    end;
    gadget-enabled?(next-button) := enabled?
  end;
  dialog.%next-enabled? := enabled?
end method dialog-next-enabled?-setter;
||#

(defmethod (setf dialog-next-enabled?) (enabled? (dialog <wizard-frame>))
  (let ((next-button (dialog-next-button dialog)))
    (when next-button
      (let ((default-button (frame-default-button dialog))
	    (back-button    (dialog-back-button dialog)))
	(cond ((and enabled? (not default-button))
               (setf (frame-default-button dialog) next-button))
	      ((and (not enabled?) (eql default-button next-button))
               (setf (frame-default-button dialog) back-button))
	      (t
	       nil))
        (setf (gadget-enabled? next-button) enabled?)))
    (%next-enabled?-setter enabled? dialog)))


#||
define method dialog-current-page-number
    (dialog :: <wizard-frame>)
 => (page-number :: false-or(<integer>))
  let page = dialog-current-page(dialog);
  when (page)
    let pages = dialog-pages(dialog);
    position(pages, page)
    | error("Wizard page %= not found within pages of %=", page, dialog)
  end
end method dialog-current-page-number;
||#


(defmethod dialog-current-page-number ((dialog <wizard-frame>))
  (let ((page (dialog-current-page dialog)))
    (when page
      (let ((pages (dialog-pages dialog)))
	(or (position page pages)
	    (error "Wizard page ~a not found within pages of ~a" page dialog))))))


#||
define method update-dialog-buttons
    (dialog :: <wizard-frame>) => ()
  let back-button = dialog-back-button(dialog);
  let next-button = dialog-next-button(dialog);
  let exit-button = dialog-exit-button(dialog);
  let back-available? = (dialog-previous-page(dialog) ~= #f);
  let next-available? = (dialog-next-page(dialog) ~= #f);
  let exit-enabled? = (~next-available? | dialog-exit-enabled?(dialog));
  let exit-box = sheet-parent(back-button | next-button | exit-button);
  when (back-button)
    gadget-enabled?(back-button) := back-available?
  end;
  // We go through this rigamarole for the Next and Exit buttons
  // because they are stacked on top of each other, that is, you
  // never see them both at the same time
  when (next-button)
    if (next-available?)
      sheet-withdrawn?(next-button) := #f;
      sheet-mapped?(next-button)    := #t
    else
      sheet-withdrawn?(next-button) := #t
    end
  end;
  when (exit-button)
    if (exit-enabled?)
      sheet-withdrawn?(exit-button) := #f;
      sheet-mapped?(exit-button)    := #t
    else
      sheet-withdrawn?(exit-button) := #t
    end
  end;
  frame-default-button(dialog)
    := (next-available? & next-button) | (exit-enabled? & exit-button);
  // Re-layout the exit buttons, because if some were withdrawn before
  // we did any layout, they will be in the wrong place
  invalidate-space-requirements(exit-box);
  relayout-children(exit-box)
end method update-dialog-buttons;
||#

(defmethod update-dialog-buttons ((dialog <wizard-frame>))
  (let* ((back-button (dialog-back-button dialog))
	 (next-button (dialog-next-button dialog))
	 (exit-button (dialog-exit-button dialog))
	 (back-available? (dialog-previous-page dialog))
	 (next-available? (dialog-next-page dialog))
	 (exit-enabled? (or (not next-available?) (dialog-exit-enabled? dialog)))
	 (exit-box (sheet-parent (or back-button next-button exit-button))))
    (when back-button
      (setf (gadget-enabled? back-button) back-available?))
    ;; We go through this rigamarole for the Next and Exit buttons
    ;; because they are stacked on top of each other, that is, you
    ;; never see them both at the same time
    (when next-button
      (if next-available?
	  (progn
            (setf (sheet-withdrawn? next-button) nil)
            (setf (sheet-mapped? next-button) t))
          (setf (sheet-withdrawn? next-button) t)))
    (when exit-button
      (if exit-enabled?
	  (progn
            (setf (sheet-withdrawn? exit-button) nil)
            (setf (sheet-mapped? exit-button) t))
          (setf (sheet-withdrawn? exit-button) t)))
    (setf (frame-default-button dialog)
	  (or (and next-available? next-button) (and exit-enabled? exit-button)))
    ;; Re-layout the exit buttons, because if some were withdrawn before
    ;; we did any layout, they will be in the wrong place
    (invalidate-space-requirements exit-box)
    (relayout-children exit-box)))


#||
define method note-dialog-page-changed
    (dialog :: <wizard-frame>) => ()
  let next-page = compute-next-page(dialog);
  dialog-next-page(dialog)     := next-page;
  dialog-previous-page(dialog) := compute-previous-page(dialog);
  dialog-exit-enabled?(dialog) := (next-page == #f)
end method note-dialog-page-changed;
||#

(defmethod note-dialog-page-changed ((dialog <wizard-frame>))
  (let ((next-page (compute-next-page dialog)))
    (setf (dialog-next-page dialog) next-page)
    (setf (dialog-previous-page dialog) (compute-previous-page dialog))
    (setf (dialog-exit-enabled? dialog) (eql next-page nil))))


#||
define method compute-next-page
    (dialog :: <wizard-frame>) => (next-page :: false-or(<sheet>))
  let current-page-number = dialog-current-page-number(dialog);
  when (current-page-number)
    let next-page-number = current-page-number + 1;
    let pages = dialog-pages(dialog);
    when (next-page-number < size(pages))
      pages[next-page-number]
    end
  end
end method compute-next-page;
||#

(defmethod compute-next-page ((dialog <wizard-frame>))
  (let ((current-page-number (dialog-current-page-number dialog)))
    (when current-page-number
      (let ((next-page-number (+ 1 current-page-number))
	    (pages            (dialog-pages dialog)))
	(when (< next-page-number (length pages))
	  (SEQUENCE-ELT pages next-page-number))))))


#||
define method compute-previous-page
    (dialog :: <wizard-frame>) => (previous-page :: false-or(<sheet>))
  let current-page-number = dialog-current-page-number(dialog);
  when (current-page-number)
    let previous-page-number = current-page-number - 1;
    let pages = dialog-pages(dialog);
    when (previous-page-number >= 0)
      pages[previous-page-number]
    end
  end
end method compute-previous-page;
||#

(defmethod compute-previous-page ((dialog <wizard-frame>))
  (let ((current-page-number (dialog-current-page-number dialog)))
    (when current-page-number
      (let ((previous-page-number (- current-page-number 1))
	    (pages                (dialog-pages dialog)))
	(when (>= previous-page-number 0)
	  (SEQUENCE-ELT pages previous-page-number))))))


#||
define method move-to-next-page
    (dialog :: <wizard-frame>) => ()
  let new-page = dialog-next-page(dialog);
  dialog-current-page(dialog) := new-page;
  note-dialog-page-changed(dialog);
  update-dialog-buttons(dialog);
  distribute-page-changed-callback(dialog)
end method move-to-next-page;
||#

(defmethod move-to-next-page ((dialog <wizard-frame>))
  (let ((new-page (dialog-next-page dialog)))
    (setf (dialog-current-page dialog) new-page)
    (note-dialog-page-changed dialog)
    (update-dialog-buttons dialog)
    (distribute-page-changed-callback dialog)))


#||
define method move-to-previous-page
    (dialog :: <wizard-frame>) => ()
  let new-page = dialog-previous-page(dialog);
  dialog-current-page(dialog) := new-page;
  note-dialog-page-changed(dialog);
  update-dialog-buttons(dialog);
  distribute-page-changed-callback(dialog)
end method move-to-previous-page;
||#

(defmethod move-to-previous-page ((dialog <wizard-frame>))
  (let ((new-page (dialog-previous-page dialog)))
    (setf (dialog-current-page dialog) new-page)
    (note-dialog-page-changed dialog)
    (update-dialog-buttons dialog)
    (distribute-page-changed-callback dialog)))



#||
/// Wizard pages

define open abstract class <wizard-page> (<basic-page>)
end class <wizard-page>;
||#


(defclass <wizard-page> (<basic-page>)
  ()
  (:documentation
"
The class of wizard pages. These are pages that can be displayed in an
instance of <wizard-frame>, and are used for a single dialog in the
structured task that the wizard guides the user through.
")
  (:metaclass <abstract-metaclass>))


#||
define sealed class <wizard-page-pane>
    (<wizard-page>, <single-child-wrapping-pane>)
end class <wizard-page-pane>;
||#


(defclass <wizard-page-pane>
    (<wizard-page> <single-child-wrapping-pane>)
  ())


#||
define method class-for-make-pane
    (framem :: <frame-manager>, class == <wizard-page>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<wizard-page-pane>, #f)
end method class-for-make-pane;

define sealed domain make (singleton(<wizard-page-pane>));
define sealed domain initialize (<wizard-page-pane>);
||#

(defmethod class-for-make-pane ((framem <frame-manager>) (class (eql (find-class '<wizard-page>))) &key)
  (values (find-class '<wizard-page-pane>) nil))



