;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GUI-TEST-SUITE -*-
(in-package #:gui-test-suite)

#||
Module:       duim-gui-test-suite
Author:       Andy Armstrong
Synopsis:     DUIM example code

/// Dynamic layouts

define frame <dynamic-layout-frame> (<simple-frame>)
  pane add-button (frame)
    make(<button>,
         label: "Add sheet",
         activate-callback: dynamic-layout-add-sheet);
  pane replace-button (frame)
    make(<button>,
	 label: "Replace sheet",
	 activate-callback: dynamic-layout-replace-sheet);
  pane remove-button (frame)
    make(<button>,
	 label: "Remove sheet",
	 activate-callback: dynamic-layout-remove-sheet);
  pane dynamic-layout (frame)
    vertically (spacing: 2)
      make(<text-field>)
    end;
  pane main-layout (frame)
    vertically (spacing: 5, x-alignment: #"centre")
      horizontally (spacing: 2)
        frame.add-button;
        frame.replace-button;
        frame.remove-button;
      end;
      frame.dynamic-layout
    end;
  layout (frame) frame.main-layout;
end frame <dynamic-layout-frame>;
||#

(define-frame <dynamic-layout-frame> (<simple-frame>)
  ((:pane add-button (frame)
     (make-pane '<button>
		:label "Add sheet"
		:activate-callback #'dynamic-layout-add-sheet))
   (:pane replace-button (frame)
     (make-pane '<button>
		:label "Replace sheet"
		:activate-callback #'dynamic-layout-replace-sheet))
   (:pane remove-button (frame)
     (make-pane '<button>
		:label "Remove sheet"
		:activate-callback #'dynamic-layout-remove-sheet))
   (:pane dynamic-layout (frame)
     (vertically (:spacing 2)
       (make-pane '<text-field>)))
   (:pane main-layout (frame)
     (vertically (:spacing 5 :x-alignment :centre)
       (horizontally (:spacing 2)
	 (add-button frame)
	 (replace-button frame)
	 (remove-button frame))
       (dynamic-layout frame)))
   (:layout (frame)
     (main-layout frame))))

#||
define method dynamic-layout-add-sheet
    (sheet :: <sheet>) => ()
  let frame = sheet-frame(sheet);
  let layout = frame.dynamic-layout;
  let new-child = make(<text-field>, parent: layout);
  relayout-parent(layout);
  sheet-mapped?(new-child) := #t
end method dynamic-layout-add-sheet;
||#

(defmethod dynamic-layout-add-sheet ((sheet <sheet>))
  (let* ((frame (sheet-frame sheet))
	 (layout (dynamic-layout frame))
	 (new-child (make-pane '<text-field> :parent layout)))
    (relayout-parent layout)
    (setf (sheet-mapped? new-child) t)))

#||
define function find-child-of-class
    (sheet :: <sheet>, class :: <class>)
 => (child :: false-or(<sheet>))
  block (return)
    for (child in sheet-children(sheet))
      if (instance?(child, class))
        return(child)
      end
    end
  end
end function find-child-of-class;
||#

(defmethod find-child-of-class ((sheet <sheet>) (class class))
  (block return
    (loop for child across (sheet-children sheet)
	  do (when (INSTANCE? child class)
	       (return-from return child)))))

#||
define method dynamic-layout-replace-sheet 
    (sheet :: <sheet>) => ()
  let frame = sheet-frame(sheet);
  let layout = frame.dynamic-layout;
  let child = find-child-of-class(layout, <text-field>);
  if (child)
    let new-child = make(<scroll-bar>);
    replace-child(layout, child, new-child);
    relayout-parent(layout);
    sheet-mapped?(new-child) := #t;
  end
end method dynamic-layout-replace-sheet;
||#

(defmethod dynamic-layout-replace-sheet ((sheet <sheet>))
  (let* ((frame (sheet-frame sheet))
	 (layout (dynamic-layout frame))
	 (child (find-child-of-class layout (find-class '<text-field>))))
    (when child
      (let ((new-child (make-pane '<scroll-bar>)))
	(replace-child layout child new-child)
	(relayout-parent layout)
	(setf (sheet-mapped? new-child) t)))))

#||
define method dynamic-layout-remove-sheet
    (sheet :: <sheet>) => ()
  let frame = sheet-frame(sheet);
  let layout = frame.dynamic-layout;
  let children = sheet-children(layout);
  unless (empty?(children))
    remove-child(layout, children[0]);
    relayout-parent(layout)
  end
end method dynamic-layout-remove-sheet;
||#

(defmethod dynamic-layout-remove-sheet ((sheet <sheet>))
  (let* ((frame (sheet-frame sheet))
	 (layout (dynamic-layout frame))
	 (children (sheet-children layout)))
    (unless (empty? children)
      (remove-child layout (aref children 0))
      (relayout-parent layout))))

#||
install-test(<dynamic-layout-frame>, "Dynamic layouts");
||#

(install-test (find-class '<dynamic-layout-frame>) "Dynamic layouts")
