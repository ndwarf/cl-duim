;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

(defsystem :gui-tests
  :description "An interactive test suite for DUIM objects"
  :author      "Scott McKay, Andy Armstrong (Lisp port: Duncan Rose <duncan@robotcat.demon.co.uk>)"
  :version     "2.1"
  :licence     "BSD-2-Clause"
  :depends-on (#:duim/backend)
  :serial t
  :components
  ((:file "package")
   (:file "harness")
   (:file "graphics")
   (:file "recording")
   (:file "sheet-mapping")
   (:file "gadgets")
   (:file "borders")
   (:file "colors")
   (:file "frames")
   (:file "input-focus")
   (:file "tab-controls")
   (:file "graph-controls")
   (:file "dialogs")
   (:file "standard-dialogs")
   (:file "dynamic-layouts")
   (:file "scrolling")
   (:file "keyboard")
   (:file "cursors")
   (:file "menus")
   (:file "command-tables")
   #-(and)
   (:file "comtab-surgery")
   (:file "clipboard")
   (:file "random-rectangles")
   (:file "gtk-tests")
   (:file "start-tests")))
