;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GUI-TEST-SUITE -*-
(in-package #:gui-test-suite)

#||
Module:    duim-gui-test-suite
author:    Andy Armstrong
Synopsis:  An interactive test-suite for DUIM objects

/// Test input sheet
define class <test-input-sheet> (<drawing-pane>)
  keyword height: = 50;
  keyword fixed-height?: = #t;
end class <test-input-sheet>;
||#

(defclass <test-input-sheet> (<drawing-pane>)
  ()
  (:default-initargs :height 50 :fixed-height? t))


#||
define method handle-repaint
    (sheet :: <test-input-sheet>, medium :: <medium>, region :: <region>) => ()
  let color
    = if (sheet == frame-input-focus(sheet-frame(sheet)))
        $red
      else
        $blue
      end;
  with-drawing-options (medium, brush: color)
    let (left, top, right, bottom) = box-edges(region);
    draw-rectangle(medium, left, top, right, bottom)
  end
end method handle-repaint;
||#

(defmethod handle-repaint ((sheet <test-input-sheet>) (medium <medium>) (region <region>))
  (let ((color (if (eql sheet (frame-input-focus (sheet-frame sheet)))
		   *red*
		   *blue*)))
    (with-drawing-options (medium :brush color)
      (multiple-value-bind (left top right bottom)
	  (box-edges region)
	(draw-rectangle medium left top right bottom)))))


#||
define method handle-event
    (sheet :: <test-input-sheet>, event :: <input-focus-in-event>) => ()
  repaint-sheet(sheet, $everywhere)
end method handle-event;
||#

(defmethod handle-event ((sheet <test-input-sheet>) (event <input-focus-in-event>))
  (repaint-sheet sheet *everywhere*))


#||
define method handle-event
    (sheet :: <test-input-sheet>, event :: <input-focus-out-event>) => ()
  repaint-sheet(sheet, $everywhere)
end method handle-event;
||#

(defmethod handle-event ((sheet <test-input-sheet>) (event <input-focus-out-event>))
  (repaint-sheet sheet *everywhere*))


#||
define method sheet-handles-keyboard?
    (sheet :: <test-input-sheet>) => (handles-keyboard? :: <boolean>)
  #f
end method sheet-handles-keyboard?;
||#

(defmethod sheet-handles-keyboard? ((sheet <test-input-sheet>))
  nil)



#||
/// Input focus frame
define frame <input-focus-frame> (<simple-frame>)
  pane text-pane (frame)
    make(<text-field>);
  pane button (frame)
    make(<push-button>, label: "Press me!");
  pane test-sheet (frame)
    make(<test-input-sheet>);
  layout (frame)
    vertically (spacing: 2)
      horizontally (spacing: 2)
        make(<label>, label: "Text:");
        frame.text-pane
      end;
      frame.test-sheet;
      frame.button;
    end;
  status-bar (frame) 
    make(<status-bar>);
end frame <input-focus-frame>;
||#

(define-frame <input-focus-frame> (<simple-frame>)
  ((:pane text-pane (frame)
     (make-pane '<text-field>))
   (:pane button (frame)
     (make-pane '<push-button> :label "Press me!"))
   (:pane test-sheet (frame)
     (make-pane '<test-input-sheet>))
   (:layout (frame)
     (vertically (:spacing 2)
       (horizontally (:spacing 2)
	 (make-pane '<label> :label "Text:")
	 (text-pane frame))
       (test-sheet frame)
       (button frame)))
   (:status-bar (frame)
     (make-pane '<status-bar>))))


#||
define method handle-event
    (frame :: <input-focus-frame>, event :: <frame-input-focus-changed-event>)
 => ()
  frame-status-message(frame)
    := format-to-string("Focus now in %=", event.event-new-focus)
end method handle-event;
||#

(defmethod handle-event ((frame <input-focus-frame>) (event <frame-input-focus-changed-event>))
  (setf (frame-status-message frame)
	(format nil "Focus now in ~a" (event-new-focus event))))


#||
install-test(<input-focus-frame>, "Input focus");
||#

(install-test (find-class '<input-focus-frame>) "Input focus")
