;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GUI-TEST-SUITE -*-
(in-package #:gui-test-suite)

#||
Module:       duim-gui-test-suite
Author:       Andy Armstrong
Synopsis:     DUIM example code

/// Tab control tests

define frame <tab-control-frame> (<simple-frame>)
  slot first-tab-pages :: false-or(<sequence>) = #f;
  slot second-tab-pages :: false-or(<sequence>) = #f;
  pane which-pages (frame)
    make(<radio-box>,
	 items: #[#["First pages",  1],
		  #["Second pages", 2]],
         label-key: first,
         value-key: second,
	 value-changed-callback: method (gadget)
                                   let frame = sheet-frame(gadget);
                                   update-tab-control-pages(frame, gadget-value(gadget))
                                 end);
  pane button (frame)
    make(<button>, 
         activate-callback: tab-control-activate-callback,
         label: "Press Me");
  pane list-box (frame)
    make(<list-box>, 
         activate-callback: tab-control-activate-callback,
         items: range(from: 1, to: 10));
  pane tree-control (frame)
    make(<tree-control>,
         roots: #(1),
	 children-generator: method (x) vector(x * 2, 1 + (x * 2)) end,
         activate-callback: tab-control-activate-callback);
  pane table-control (frame)
    make(<table-control>,
         items: range(from: 1, to: 10),
         activate-callback: tab-control-activate-callback,
         headings: #("Identity", "Doubled", "Squared"),
         generators: vector(identity,
                            method (x) x + x end,
                            method (x) x * x end));
  pane ellipse-pane (frame)
    make(<ellipse-pane>);
  pane table-layout-pane (frame)
    make(<table-layout>,
         columns: 2,
	 x-alignment: #[#"right", #"left"],
	 y-alignment: #"center",
	 children: vector(make(<label>, label: "Name:"),
			  make(<text-field>),
			  make(<label>, label: "Organization:"),
			  make(<text-field>)));
  pane tab-control (frame)
    begin
      let first-pages
	= vector(make(<tab-control-page>,
		      label: "Button",   child: frame.button),
		 make(<tab-control-page>,
		      label: "List Box", child: frame.list-box),
		 make(<tab-control-page>,
		      label: "Layout",   child: frame.table-layout-pane));
      let second-pages
	= vector(make(<tab-control-page>,
		      label: "Tree",     child: frame.tree-control),
		 make(<tab-control-page>,
		      label: "Table",    child: frame.table-control),
		 make(<tab-control-page>,
		      label: "Ellipse",  child: frame.ellipse-pane));
      frame.first-tab-pages  := first-pages;
      frame.second-tab-pages := second-pages;
      make(<tab-control>, pages: first-pages)
    end;
  pane main-layout (frame)
    vertically (y-spacing: 4, height: 300)
      make(<table-layout>,
           columns: 2,
           x-alignment: #(#"right", #"left"),
           children: vector(make(<label>, label: "Pages:"),
                            frame.which-pages));
      make(<separator>);
      frame.tab-control
    end;
  layout (frame) frame.main-layout;
  status-bar (frame) make(<status-bar>);
end frame <tab-control-frame>;
||#

(define-frame <tab-control-frame> (<simple-frame>)
  ((first-tab-pages :type (or null sequence)
		    :initform nil
		    :accessor first-tab-pages)
   (second-tab-pages :type (or null sequence)
		     :initform nil
		     :accessor second-tab-pages)
   (:pane which-pages (frame)
     (make-pane '<radio-box>
		:items (vector (vector "First pages" 1)
			       (vector "Second pages" 2))
		:label-key (alexandria:rcurry #'aref 0)  ;;#'first
		:value-key (alexandria:rcurry #'aref 1)  ;;#'second
		:value-changed-callback
		#'(lambda (gadget)
		    (let ((frame (sheet-frame gadget)))
		      (update-tab-control-pages frame
						(gadget-value gadget))))))
   (:pane button (frame)
     (make-pane '<button>
		:activate-callback #'tab-control-activate-callback
		:label "Press Me"))
   (:pane list-box (frame)
     (make-pane '<list-box>
		:activate-callback #'tab-control-activate-callback
		:items (range :from 1 :to 10)))
   (:pane tree-control (frame)
     (make-pane '<tree-control>
		:roots '(1)
		:children-generator
		#'(lambda (x)
		    (vector (* x 2) (+ 1 (* x 2))))
		:activate-callback #'tab-control-activate-callback))
   (:pane table-control (frame)
     (make-pane '<table-control>
		;; TODO: Maybe we should write a "range" method that looks like
		;; the old one but that generates values via loop. Could also
		;; make it return a sequence of a configurable type...
		:items (range :from 1 :to 10)
		:activate-callback #'tab-control-activate-callback
		:headings '("Identity" "Doubled" "Squared")
		:generators
		(vector #'identity
			#'(lambda (x) (+ x x))
			#'(lambda (x) (* x x)))))
   (:pane ellipse-pane (frame)
     (make-pane '<ellipse-pane>))
   (:pane table-layout-pane (frame)
     (make-pane '<table-layout>
		:columns 2
		:x-alignment #(:right :left)
		:y-alignment :center
		:children
		(vector (make-pane '<label> :label "Name:")
			(make-pane '<text-field>)
			(make-pane '<label> :label "Organization:")
			(make-pane '<text-field>))))
   (:pane tab-control (frame)
     (let ((first-pages
	    (vector (make-pane '<tab-control-page>
			       :label "Button"
			       :child (button frame))
		    (make-pane '<tab-control-page>
			       :label "List Box"
			       :child (list-box frame))
		    (make-pane '<tab-control-page>
			       :label "Layout"
			       :child (table-layout-pane frame))))
	   (second-pages
	    (vector (make-pane '<tab-control-page>
			       :label "Tree"
			       :child (tree-control frame))
		    (make-pane '<tab-control-page>
			       :label "Table"
			       :child (table-control frame))
		    (make-pane '<tab-control-page>
			       :label "Ellipse"
			       :child (ellipse-pane frame)))))
       (setf (first-tab-pages frame) first-pages)
       (setf (second-tab-pages frame) second-pages)
       (make-pane '<tab-control> :pages first-pages)))
   (:pane main-layout (frame)
     (vertically (:y-spacing 4 :height 300)
       (make-pane '<table-layout>
		  :columns 2
		  :x-alignment '(:right :left)
		  :children
		  (vector (make-pane '<label>
				     :label "Pages:")
			  (which-pages frame)))
       (make-pane '<separator>)
       (tab-control frame)))
   (:layout (frame)
     (main-layout frame))
   (:status-bar (frame)
     (make-pane '<status-bar>))))

#||
define function tab-control-activate-callback
    (gadget :: <gadget>) => ()
  let frame = sheet-frame(gadget);
  frame-status-message(frame)
    := format-to-string("Activated gadget: value %=", gadget-value(gadget))
end function tab-control-activate-callback;
||#

(defun tab-control-activate-callback (gadget)
  (let ((frame (sheet-frame gadget)))
    (setf (frame-status-message frame)
	  (format nil "Activated gadget: value ~a"
		  (gadget-value gadget)))))

#||
define method update-tab-control-pages
    (frame :: <tab-control-frame>, value :: <integer>) => ()
  let gadget = tab-control(frame);
  tab-control-pages(gadget)
    := select (value)
	 1 => frame.first-tab-pages;
	 2 => frame.second-tab-pages;
       end;
end method update-tab-control-pages;
||#

(defmethod update-tab-control-pages ((frame <tab-control-frame>)
				     (value integer))
  (let ((gadget (tab-control frame)))
    (setf (tab-control-pages gadget)
	  (ecase value
	    (1 (first-tab-pages frame))
	    (2 (second-tab-pages frame))))))


#||
/// Install the test

install-test(<tab-control-frame>, "Tab controls");
||#

(install-test (find-class '<tab-control-frame>) "Tab controls")
