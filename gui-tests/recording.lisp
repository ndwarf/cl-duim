;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GUI-TEST-SUITE -*-
(in-package #:gui-test-suite)

#||
Module:       duim-gui-test-suite
Author:       Scott McKay
Synopsis:     An interactive test-suite for DUIM objects

/// Simple recording drawing tests

define class <graphics-recording-pane> (<recording-pane>)
  constant slot graphics-to-test :: <list>,
    init-keyword: graphics:;
  constant slot graphic-width  :: <integer> = $default-graphic-width;
  constant slot graphic-height :: <integer> = $default-graphic-height;
end class <graphics-recording-pane>;
||#

(defclass <graphics-recording-pane> (<recording-pane>)
  ((graphics-to-test :type list
		     :initarg :graphics
		     :reader graphics-to-test)
   (graphic-width :type integer
		  :initform +default-graphic-width+
		  :reader graphic-width)
   (graphic-height :type integer
		   :initform +default-graphic-height+
		   :reader graphic-height)))


#||
define method do-compose-space
    (pane :: <graphics-recording-pane>, #key width, height)
 => (space-requirement :: <space-requirement>)
  ignore(width, height);
  let width   = graphic-width(pane);
  let height  = graphic-height(pane);
  let spacing = $default-graphic-spacing;
  let border  = $default-graphic-border;
  let no-of-items = size(graphics-to-test(pane));
  let _port = port(pane);
  let text-height = font-height(get-default-text-style(_port, pane), _port);
  make(<space-requirement>,
       width:  (2 * (border + width)) + spacing,
       height: text-height + ((no-of-items - 1) * (height + spacing))
                 + height + 4 + 3 * border)
end method do-compose-space;
||#

(defmethod do-compose-space ((pane <graphics-recording-pane>) &key width height)
  (declare (ignore width height))
  (let* ((width   (graphic-width pane))
	 (height  (graphic-height pane))
	 (spacing +default-graphic-spacing+)
	 (border  +default-graphic-border+)
	 (no-of-items (size (graphics-to-test pane)))
	 (_port   (port pane))
	 (text-height (font-height (get-default-text-style _port pane) _port)))
    (make-space-requirement
		   :width (+ (* 2 (+ border width)) spacing)
		   :height (+ text-height
			      (* (- no-of-items 1) (+ height spacing))
			      height
			      4
			      (* 3 border)))))


#||
define method handle-event
    (sheet :: <graphics-recording-pane>, 
     event :: <pointer-motion-event>) => ()
  let old = sheet-highlighted-record(sheet);
  let new = child-containing-position(sheet-output-history(sheet), event-x(event), event-y(event));
  when (old & old ~== new)
    highlight-output-record(old, sheet, #"unhighlight")
  end;
  when (new)
    highlight-output-record(new, sheet, #"highlight")
  end;
  sheet-highlighted-record(sheet) := new
end method handle-event;
||#

(defmethod handle-event ((sheet <graphics-recording-pane>)
			 (event <pointer-motion-event>))
  (let ((old (sheet-highlighted-record sheet))
	(new (child-containing-position (sheet-output-history sheet)
					(event-x event)
					(event-y event))))
    (when (and old (not (eql old new)))
      (highlight-output-record old sheet :unhighlight))
    (when new
      (highlight-output-record new sheet :highlight))
    (setf (sheet-highlighted-record sheet) new)))


#||
define method draw-test-graphics
    (sheet :: <graphics-recording-pane>, medium :: <medium>, region :: <region>) => ()
  ignore(region);
  clear-output-history(sheet);
  let frame  = sheet-frame(sheet);
  let color = frame.%color;
  let brush = select (color by \==)
		#"Blue"  => $blue;
		#"Red"   => $red;
		#"Black" => $black;
	      end;
  let thickness = frame.%thickness;
  let pen       = make(<pen>, width: thickness);
  let width     = graphic-width(sheet);
  let height    = graphic-height(sheet);
  let spacing   = $default-graphic-spacing;
  let border    = $default-graphic-border;
  let x1 = border;
  let y1 = border;
  let x2 = x1 + width;
  let y2 = y1 + height;
  let x3 = x2 + spacing;
  let x4 = x3 + width;
  with-drawing-options (sheet, brush: brush, pen: pen)
    let text-style = medium-merged-text-style(medium);
    let text-height = font-height(text-style, port(sheet));
    draw-text(sheet, "Unfilled", x1, y1, align-y: #"top");
    draw-text(sheet, "Filled",   x3, y1, align-y: #"top");
    draw-line(sheet, x1, y1 + text-height + 2, x4, y1 + text-height + 2);
    y1 := y1 + text-height + 4 + border;
    y2 := y2 + text-height + 4 + border;
    for (graphic in sheet.graphics-to-test)
      draw-graphic(graphic, sheet, #f, x1, x2, y1, y2);
      draw-graphic(graphic, sheet, #t, x3, x4, y1, y2);
      y1 := y1 + height + spacing;
      y2 := y2 + height + spacing;
    end
  end
end method draw-test-graphics;
||#

(defmethod draw-test-graphics ((sheet <graphics-recording-pane>)
			       (medium <medium>)
			       (region <region>))
  (declare (ignore region))
  (clear-output-history sheet)
  (let* ((frame (sheet-frame sheet))
	 (color (%color frame))
	 (brush (cond ((string= color "Blue")  *blue*)
		      ((string= color "Red")   *red*)
		      ((string= color "Black") *black*)
		      (t (error "Unknown color ~a" color))))
	 (thickness (%thickness frame))
	 (pen (make-pen :width thickness))
	 (width (graphic-width sheet))
	 (height (graphic-height sheet))
	 (spacing +default-graphic-spacing+)
	 (border  +default-graphic-border+)
	 (x1 border)
	 (y1 border)
	 (x2 (+ x1 width))
	 (y2 (+ y1 height))
	 (x3 (+ x2 spacing))
	 (x4 (+ x3 width)))
    (with-drawing-options (sheet :brush brush :pen pen)
      (let* ((text-style (medium-merged-text-style medium))
	     (text-height (font-height text-style (port sheet))))
	(draw-text sheet "Unfilled" x1 y1 :align-y :top)
	(draw-text sheet "Filled" x3 y1 :align-y :top)
	(draw-line sheet x1 (+ y1 text-height 2)
		   x4 (+ y1 text-height 2))
	(setf y1 (+ y1 text-height 4 border))
	(setf y2 (+ y2 text-height 4 border))
	(loop for graphic in (graphics-to-test sheet)
	      do (draw-graphic graphic sheet nil x1 x2 y1 y2)
	      do (draw-graphic graphic sheet t   x3 x4 y1 y2)
	      do (setf y1 (+ y1 height spacing))
	      do (setf y2 (+ y2 height spacing)))))))

#||
define method draw-graphic
    (graphic-type == #"rectangle", sheet :: <graphics-recording-pane>, filled? :: <boolean>, 
     x1 :: <integer>, x2 :: <integer>, y1 :: <integer>, y2 :: <integer>) => () 
  draw-rectangle(sheet, x1, y1, x2, y2, filled?: filled?)
end method draw-graphic;
||#

(defmethod draw-graphic ((graphic-type (eql :rectangle))
			 (sheet <graphics-recording-pane>)
			 filled?
			 (x1 integer) (x2 integer)
			 (y1 integer) (y2 integer))
  (draw-rectangle sheet x1 y1 x2 y2 :filled? filled?))

#||
define method draw-graphic
    (graphic-type == #"arrow", sheet :: <graphics-recording-pane>, filled? :: <boolean>, 
     x1 :: <integer>, x2 :: <integer>, y1 :: <integer>, y2 :: <integer>) => ()
  let xinc = round/(x2 - x1, 5);
  let yinc = round/(y2 - y1, 5);
  for (i :: <integer> from x1 to x2 by xinc, j :: <integer> from x2 to x1 by -xinc)
    draw-arrow(sheet, i, y1, j, y2, from-head?: filled?, to-head?: filled?);
  end;
  for (i :: <integer> from y1 to y2 by yinc, j :: <integer> from y2 to y1 by -yinc)
    draw-arrow(sheet, x1, i, x2, j, from-head?: filled?, to-head?: filled?);
  end;
end method draw-graphic;
||#

(defmethod draw-graphic ((graphic-type (eql :arrow))
			 (sheet <graphics-recording-pane>)
			 filled?
			 (x1 integer) (x2 integer)
			 (y1 integer) (y2 integer))
  (let ((xinc (round (- x2 x1) 5))
	(yinc (round (- y2 y1) 5)))
    (loop for i from x1 to x2 by xinc
	  for j downfrom x2 to x1 by xinc
	  do (draw-arrow sheet i y1 j y2
			 :from-head? filled?
			 :to-head? filled?))
    (loop for i from y1 to y2 by yinc
	  for j downfrom y2 to y1 by yinc
	  do (draw-arrow sheet x1 i x2 j
			 :from-head? filled?
			 :to-head? filled?))))

#||
define method draw-graphic
    (graphic-type == #"polygon", sheet :: <graphics-recording-pane>, filled? :: <boolean>, 
     x1 :: <integer>, x2 :: <integer>, y1 :: <integer>, y2 :: <integer>) => () 
  let xcen = x1 + (round/(x2 - x1, 2));
  let ycen = y1 + (round/(y2 - y1, 2));
  let x3 = xcen + (round/(x2 - xcen, 2));
  draw-polygon(sheet, list(xcen, y2, x1, ycen, xcen, y1, x3, ycen),
               closed?: filled?, filled?: filled?);
end method draw-graphic;
||#

(defmethod draw-graphic ((graphic-type (eql :polygon))
			 (sheet <graphics-recording-pane>)
			 filled?
			 (x1 integer) (x2 integer)
			 (y1 integer) (y2 integer))
  (let* ((xcen (+ x1 (round (- x2 x1) 2)))
	 (ycen (+ y1 (round (- y2 y1) 2)))
	 (x3   (+ xcen (round (- x2 xcen) 2))))
    (draw-polygon sheet (list xcen y2 x1 ycen xcen y1 x3 ycen)
		  :closed? filled? :filled? filled?)))

#||
define method draw-graphic
    (graphic-type == #"regular-polygon", sheet :: <graphics-recording-pane>, filled? :: <boolean>, 
     x1 :: <integer>, x2 :: <integer>, y1 :: <integer>, y2 :: <integer>) => ()
  let xcen = floor/(x1 + x2, 2);
  let line-size = floor/(x2 - x1, 3);
  let x3 = xcen - floor/(line-size, 2);
  let x4 = xcen + floor/(line-size, 2);
  draw-regular-polygon(sheet, x3, y1, x4, y1, 8, closed?: filled?, filled?: filled?);
end method draw-graphic;
||#

(defmethod draw-graphic ((graphic-type (eql :regular-polygon))
			(sheet <graphics-recording-pane>)
			filled?
			(x1 integer) (x2 integer)
			(y1 integer) (y2 integer))
  (let* ((xcen (floor (+ x1 x2) 2))
	 (line-size (floor (- x2 x1) 3))
	 (x3 (- xcen (floor line-size 2)))
	 (x4 (+ xcen (floor line-size 2))))
    (draw-regular-polygon sheet x3 y1 x4 y1 8
			  :closed? filled?
			  :filled? filled?)))

#||
define method draw-graphic
    (graphic-type == #"triangle", sheet :: <graphics-recording-pane>, filled? :: <boolean>, 
     x1 :: <integer>, x2 :: <integer>, y1 :: <integer>, y2 :: <integer>) => () 
  let xcen = x1 + (round/(x2 - x1, 2));
  draw-triangle(sheet, xcen, y1, x1, y2, x2, y2, filled?: filled?);
end method draw-graphic;
||#

(defmethod draw-graphic ((graphic-type (eql :triangle))
			 (sheet <graphics-recording-pane>)
			 filled?
			 (x1 integer) (x2 integer)
			 (y1 integer) (y2 integer))
  (let ((xcen (+ x1 (round (- x2 x1) 2))))
    (draw-triangle sheet xcen y1 x1 y2 x2 y2 :filled? filled?)))

#||
define method draw-graphic
    (graphic-type == #"circle", sheet :: <graphics-recording-pane>, filled? :: <boolean>, 
     x1 :: <integer>, x2 :: <integer>, y1 :: <integer>, y2 :: <integer>) => () 
  let xcen = x1 + (round/(x2 - x1, 2));
  let ycen = y1 + (round/(y2 - y1, 2));
  draw-circle(sheet, xcen, ycen, (ycen - y1), filled?: filled?)
end method draw-graphic;
||#

(defmethod draw-graphic ((graphic-type (eql :circle))
			 (sheet <graphics-recording-pane>)
			 filled?
			 (x1 integer) (x2 integer)
			 (y1 integer) (y2 integer))
  (let ((xcen (+ x1 (round (- x2 x1) 2)))
	(ycen (+ y1 (round (- y2 y1) 2))))
    (draw-circle sheet xcen ycen (- ycen y1) :filled? filled?)))

#||
define method draw-graphic
    (graphic-type == #"ellipse", sheet :: <graphics-recording-pane>, filled? :: <boolean>, 
     x1 :: <integer>, x2 :: <integer>, y1 :: <integer>, y2 :: <integer>) => () 
  let xcen = x1 + (round/(x2 - x1, 2));
   let ycen = y1 + (round/(y2 - y1, 2));
   let xrad = round/(xcen - x1, 2);
   let yrad = ycen - y1;
   draw-ellipse(sheet, xcen, ycen, xrad, 0, 0, yrad, filled?: filled?);
end method draw-graphic;
||#

(defmethod draw-graphic ((graphic-type (eql :ellipse))
			 (sheet <graphics-recording-pane>)
			 filled?
			 (x1 integer) (x2 integer)
			 (y1 integer) (y2 integer))
  (let* ((xcen (+ x1 (round (- x2 x1) 2)))
	 (ycen (+ y1 (round (- y2 y1) 2)))
	 (xrad (round (- xcen x1) 2))
	 (yrad (- ycen y1)))
    (draw-ellipse sheet xcen ycen xrad 0 0 yrad :filled? filled?)))

#||
define method draw-graphic
    (graphic-type == #"oval", sheet :: <graphics-recording-pane>, filled? :: <boolean>, 
     x1 :: <integer>, x2 :: <integer>, y1 :: <integer>, y2 :: <integer>) => () 
  let xcen = x1 + (round/(x2 - x1, 2));
  let ycen = y1 + (round/(y2 - y1, 2));
  let yrad = round/(ycen - y1, 2);
  with-new-output-record (sheet)
    draw-oval(sheet, xcen, ycen, xcen - x1, yrad, filled?: filled?)
  end;
end method draw-graphic;
||#

(defmethod draw-graphic ((graphic-type (eql :oval))
			 (sheet <graphics-recording-pane>)
			 filled?
			 (x1 integer) (x2 integer)
			 (y1 integer) (y2 integer))
  (let* ((xcen (+ x1 (round (- x2 x1) 2)))
	 (ycen (+ y1 (round (- y2 y1) 2)))
	 (yrad (round (- ycen y1) 2)))
    (with-new-output-record (sheet)
      (draw-oval sheet xcen ycen (- xcen x1) yrad :filled? filled?))))

#||
define method draw-graphic
    (graphic-type == #"bezier-curve", sheet :: <graphics-recording-pane>, filled? :: <boolean>, 
     x1 :: <integer>, x2 :: <integer>, y1 :: <integer>, y2 :: <integer>) => () 
  let xcen = x1 + (round/(x2 - x1, 2));
  let ycen = y1 + (round/(y2 - y1, 2));
  draw-bezier-curve(sheet, list(x1, ycen, xcen, y1, x2, y2, x2, ycen), filled?: filled?);
end method draw-graphic;
||#

(defmethod draw-graphic ((graphic-type (eql :bezier-curve))
			 (sheet <graphics-recording-pane>)
			 filled?
			 (x1 integer) (x2 integer)
			 (y1 integer) (y2 integer))
  (let ((xcen (+ x1 (round (- x2 x1) 2)))
	(ycen (+ y1 (round (- y2 y1) 2))))
    (draw-bezier-curve sheet (list x1 ycen xcen y1 x2 y2 x2 ycen)
		       :filled? filled?)))

#||
define method draw-graphic
    (graphic-type == #"image", sheet :: <graphics-recording-pane>, filled? :: <boolean>, 
     x1 :: <integer>, x2 :: <integer>, y1 :: <integer>, y2 :: <integer>) => ()
  let xcen = x1 + (round/(x2 - x1, 2));
  let ycen = y1 + (round/(y2 - y1, 2));
  draw-image(sheet, $pixmap-to-test, xcen, ycen);
end method draw-graphic;
||#

(defmethod draw-graphic ((graphic-type (eql :image))
			 (sheet <graphics-recording-pane>)
			 filled?
			 (x1 integer) (x2 integer)
			 (y1 integer) (y2 integer))
  (let ((xcen (+ x1 (round (- x2 x1) 2)))
	(ycen (+ y1 (round (- y2 y1) 2))))
    (draw-image sheet *pixmap-to-test* xcen ycen)))

#||
define command-table *graphics-recording-file-comtab* (*global-command-table*)
  menu-item "E&xit" = exit-frame,
    documentation: "Exit";
end command-table *graphics-recording-file-comtab*;
||#

(define-command-table *graphics-recording-file-comtab* (*global-command-table*)
  ((menu-item "E&xit" = #'exit-frame
	      :documentation "Exit")))

#||
define command-table *graphics-recording-comtab* (*global-command-table*)
  menu-item "File" = *graphics-recording-file-comtab*;
end command-table *graphics-recording-comtab*;
||#

(define-command-table *graphics-recording-comtab* (*global-command-table*)
  ((menu-item "File" = *graphics-recording-file-comtab*)))

#||
define frame <recording-test-frame> (<simple-frame>)
  slot %color = $test-graphics-color[0],
    init-keyword: color:;
  slot %thickness = $test-graphics-thickness[0],
    init-keyword: thickness:;
  pane color-radio-box (frame)
    make(<radio-box>,
	 items: $test-graphics-color,
	 value: frame.%color,
         label-key: curry(as, <string>),
 	 value-changed-callback:
	   method (gadget)
	     let frame = sheet-frame(gadget);
	     frame.%color := gadget-value(gadget);
	     repaint-panes(frame)
	   end method);
  pane thickness-list-box (frame)
    make(<spin-box>,
         items: $test-graphics-thickness,
         value: frame.%thickness,
         value-changed-callback:
	   method (gadget)
	     let frame = sheet-frame(gadget);
	     frame.%thickness := gadget-value(gadget);
	     repaint-panes(frame)
	   end method);
  pane drawable-left (frame)
    make(<graphics-recording-pane>,
         graphics: list(#"rectangle", #"circle", #"arrow", #"polygon", #"regular-polygon"));
  pane drawable-right (frame)
    make(<graphics-recording-pane>,
         graphics: list(#"triangle", #"ellipse", #"oval", #"bezier-curve", #"image"));
  pane main-layout (frame)
    vertically (spacing: 5)
      make(<separator>);
      make(<table-layout>,
	   x-spacing: 10, y-spacing: 2,
           x-alignment: #[#"right", #"left"],
           contents: vector(vector(make(<label>, label: "Color:"),
				   frame.color-radio-box),
			    vector(make(<label>, label: "Thickness:"),
				   frame.thickness-list-box)));
      make(<separator>);
      horizontally (spacing: 10)
        with-border (type: #"sunken")
	  frame.drawable-left
	end;
        with-border (type: #"sunken")
	  frame.drawable-right
	end
      end
    end;
  layout (frame) frame.main-layout;
  command-table (frame) *graphics-recording-comtab*;
end frame <recording-test-frame>;
||#

(define-frame <recording-test-frame> (<simple-frame>)
  ((%color :initarg :color
	   :initform (nth 0 *test-graphics-color*)
	   :accessor %color)
   (%thickness :initarg :thickness
	       :initform (nth 0 *test-graphics-thickness*)
	       :accessor %thickness)
   (:pane color-radio-box (frame)
     (make-pane '<radio-box>
		:items *test-graphics-color*
		:value (%color frame)
		:label-key #'(lambda (x) (format nil "~a" x))
		:value-changed-callback
		#'(lambda (gadget)
		    (let ((frame (sheet-frame gadget)))
		      (setf (%color frame) (gadget-value gadget))
		      (repaint-panes frame)))))
   (:pane thickness-list-box (frame)
     (make-pane '<spin-box>
		:items *test-graphics-thickness*
		:value (%thickness frame)
		:value-changed-callback
		#'(lambda (gadget)
		    (let ((frame (sheet-frame gadget)))
		      (setf (%thickness frame) (gadget-value gadget))
		      (repaint-panes frame)))))
   (:pane drawable-left (frame)
     (make-pane '<graphics-recording-pane>
		:graphics (list :rectangle :circle :arrow
				:polygon :regular-polygon)))
   (:pane drawable-right (frame)
     (make-pane '<graphics-recording-pane>
		:graphics (list :triangle :ellipse :oval
				:bezier-curve :image)))
   (:pane main-layout (frame)
     (vertically (:spacing 5)
       (make-pane '<separator>)
       (make-pane '<table-layout>
		  :x-spacing 10 :y-spacing 2
		  :x-alignment #(:right :left)
		  :contents
		  (vector (vector (make-pane '<label> :label "Color:")
				  (color-radio-box frame))
			  (vector (make-pane '<label> :label "Thickness:")
				  (thickness-list-box frame))))
       (make-pane '<separator>)
       (horizontally (:spacing 10)
	 (with-border (:type :sunken)
	   (drawable-left frame))
	 (with-border (:type :sunken)
	   (drawable-right frame)))))
   (:layout (frame) (main-layout frame))
   (:command-table (frame) *graphics-recording-comtab*)))

#||
// Redraw methods for new color and thickness, etc
define method repaint-panes
    (frame :: <recording-test-frame>) => ()
  draw-test-graphics(frame.drawable-left,  sheet-medium(frame.drawable-left),  $everywhere);
  draw-test-graphics(frame.drawable-right, sheet-medium(frame.drawable-right), $everywhere);
end method repaint-panes;
||#

(defmethod repaint-panes ((frame <recording-test-frame>))
  (duim-debug-message "[gui-tests] recording - repaint-panes entered")
  (draw-test-graphics (drawable-left frame)
		      (sheet-medium (drawable-left frame))
		      *everywhere*)
  (draw-test-graphics (drawable-right frame)
		      (sheet-medium (drawable-right frame))
		      *everywhere*))

#||
install-test(<recording-test-frame>, "Recording -- graphics");
||#

(install-test (find-class '<recording-test-frame>) "Recording -- graphics")


#||
/// Simple drawing tests with scrolling

define frame <scrolling-recording-test-frame> (<simple-frame>)
  slot %color = $test-graphics-color[0],
    init-keyword: color:;
  slot %thickness = $test-graphics-thickness[0],
    init-keyword: thickness:;
  pane color-radio-box (frame)
    make(<radio-box>,
	 items: $test-graphics-color,
	 value: frame.%color,
         label-key: curry(as, <string>),
 	 value-changed-callback:
	   method (gadget)
	     let frame = sheet-frame(gadget);
	     frame.%color := gadget-value(gadget);
	     repaint-panes(frame)
	   end method);
  pane thickness-list-box (frame)
    make(<spin-box>,
         items: $test-graphics-thickness,
         value: frame.%thickness,
         value-changed-callback:
	   method (gadget)
	     let frame = sheet-frame(gadget);
	     frame.%thickness := gadget-value(gadget);
	     repaint-panes(frame)
	   end method);
  pane drawable-left (frame)
    make(<graphics-recording-pane>,
         graphics: list(#"rectangle", #"circle", #"arrow", #"polygon", #"regular-polygon"),
         height: 200);
  pane drawable-right (frame)
    make(<graphics-recording-pane>,
         graphics: list(#"triangle", #"ellipse", #"oval", #"bezier-curve", #"image"),
         height: 200);
  pane main-layout (frame)
    vertically (spacing: 5)
      make(<separator>);
      make(<table-layout>,
	   x-spacing: 10, y-spacing: 2,
           x-alignment: #[#"right", #"left"],
           contents: vector(vector(make(<label>, label: "Color:"),
				   frame.color-radio-box),
			    vector(make(<label>, label: "Thickness:"),
				   frame.thickness-list-box)));
      make(<separator>);
      horizontally (spacing: 10)
        with-border (type: #"sunken")
	  scrolling () frame.drawable-left end
	end;
        with-border (type: #"sunken")
	  scrolling () frame.drawable-right end
	end
      end
    end;
  layout (frame) frame.main-layout;
  command-table (frame) *graphics-recording-comtab*;
end frame <scrolling-recording-test-frame>;
||#

(define-frame <scrolling-recording-test-frame> (<simple-frame>)
  ((%color :initarg :color
	   :initform (nth 0 *test-graphics-color*)
	   :accessor %color)
   (%thickness :initarg :thickness
	       :initform (nth 0 *test-graphics-thickness*)
	       :accessor %thickness)
   (:pane color-radio-box (frame)
     (make-pane '<radio-box>
		:items *test-graphics-color*
		:value (%color frame)
		:label-key #'(lambda (x) (format nil "~a" x))
		:value-changed-callback
		#'(lambda (gadget)
		    (duim-debug-message "[gui-tests;recording] - entered value-changed-callback for scrolling recorder")
		    (let ((frame (sheet-frame gadget)))
		      (setf (%color frame) (gadget-value gadget))
		      (repaint-panes frame)))))
   (:pane thickness-list-box (frame)
     (make-pane '<spin-box>
		:items *test-graphics-thickness*
		:value (%thickness frame)
		:value-changed-callback
		#'(lambda (gadget)
		    (let ((frame (sheet-frame gadget)))
		      (setf (%thickness frame) (gadget-value gadget))
		      (repaint-panes frame)))))
   (:pane drawable-left (frame)
     (make-pane '<graphics-recording-pane>
		:graphics (list :rectangle :circle :arrow :polygon
				:regular-polygon)
		:height 200))
   (:pane drawable-right (frame)
     (make-pane '<graphics-recording-pane>
		:graphics (list :triangle :ellipse :oval
				:bezier-curve :image)
		:height 200))
   (:pane main-layout (frame)
     (vertically (:spacing 5)
       (make-pane '<separator>)
       (make-pane '<table-layout>
		  :x-spacing 10 :y-spacing 2
		  :x-alignment #(:right :left)
		  :contents
		  (vector (vector (make-pane '<label> :label "Color:")
				  (color-radio-box frame))
			  (vector (make-pane '<label> :label "Thickness:")
				  (thickness-list-box frame))))
       (make-pane '<separator>)
       (horizontally (:spacing 10)
	 (with-border (:type :sunken)
	   (scrolling () (drawable-left frame)))
	 (with-border (:type :sunken)
	   (scrolling () (drawable-right frame))))))
   (:layout (frame) (main-layout frame))
   (:command-table (frame) *graphics-recording-comtab*)))

#||
// Redraw methods for new color and thickness, etc
define method repaint-panes
    (frame :: <scrolling-recording-test-frame>) => ()
  draw-test-graphics(frame.drawable-left,  sheet-medium(frame.drawable-left),  $everywhere);
  draw-test-graphics(frame.drawable-right, sheet-medium(frame.drawable-right), $everywhere);
end method repaint-panes;
||#

(defmethod repaint-panes ((frame <scrolling-recording-test-frame>))
  (draw-test-graphics (drawable-left frame)
		      (sheet-medium (drawable-left frame))
		      *everywhere*)
  (draw-test-graphics (drawable-right frame)
		      (sheet-medium (drawable-right frame))
		      *everywhere*))

#||
install-test(<scrolling-recording-test-frame>, "Recording -- scrolling");
||#

(install-test (find-class '<scrolling-recording-test-frame>) "Recording -- scrolling")


#||
/// Scribble, using output recording

define class <scribble-recording-pane> (<recording-pane>)
  slot scribble-segment = #f;
  constant slot scribble-popup-menu-callback = #f,
    init-keyword: popup-menu-callback:;
end class <scribble-recording-pane>;
||#

(defclass <scribble-recording-pane> (<recording-pane>)
  ((scribble-segment :initform nil
		     :accessor scribble-segment)
   (scribble-popup-menu-callback :initarg :popup-menu-callback
				 :initform nil
				 :reader scribble-popup-menu-callback)))

#||
define method handle-button-event
    (sheet :: <scribble-recording-pane>, 
     event :: <button-press-event>, 
     button == $left-button) => ()
  sheet.scribble-segment := make(<stretchy-vector>);
  add-scribble-segment(sheet, event.event-x, event.event-y);
  block (break)
    tracking-pointer (event, sheet)
      <pointer-drag-event> =>
        add-scribble-segment(sheet, event.event-x, event.event-y);
      <button-release-event> =>
        begin
          when (sheet.scribble-segment)
            with-output-recording-options (sheet, record?: #t)
              draw-polygon(sheet, sheet.scribble-segment, closed?: #f, filled?: #f)
            end
          end;
          sheet.scribble-segment := #f;
          break()
        end;
    end
  end
end method handle-button-event;
||#

(defmethod handle-button-event ((sheet <scribble-recording-pane>)
				(event <button-press-event>)
				(button (eql +left-button+)))
  (setf (scribble-segment sheet)
	(make-array 0 :adjustable t :fill-pointer t))
  (add-scribble-segment sheet (event-x event) (event-y event))
  (block break
    (tracking-pointer (event sheet)
      ;;(#m<pointer-drag-event>
      (<pointer-drag-event>
       (progn
	 (add-scribble-segment sheet
			       (event-x event)
			       (event-y event))))
      ;;(#m<button-release-event>
      (<button-release-event>
       (progn
	 (when (scribble-segment sheet)
	   (with-output-recording-options (sheet :record? t)
	     (draw-polygon sheet (scribble-segment sheet)
			   :closed? nil :filled? nil)))
	 (setf (scribble-segment sheet) nil)
	 (return-from break nil))))))

#||
define method handle-button-event
    (sheet :: <scribble-recording-pane>, 
     event :: <button-release-event>, 
     button == $right-button) => ()
  let popup-menu-callback = scribble-popup-menu-callback(sheet);
  when (popup-menu-callback)
    popup-menu-callback(sheet, event.event-x, event.event-y)
  end
end method handle-button-event;
||#

(defmethod handle-button-event ((sheet <scribble-recording-pane>)
				(event <button-release-event>)
				(button (eql +right-button+)))
  (let ((popup-menu-callback (scribble-popup-menu-callback sheet)))
    (when popup-menu-callback
      (funcall popup-menu-callback sheet (event-x event) (event-y event)))))

#||
define method add-scribble-segment
    (sheet :: <scribble-recording-pane>, x, y) => ()
  let segment = sheet.scribble-segment;
  // The app can generate drag and release events before it has ever
  // seen a press event, so be careful
  when (segment)
    add!(segment, x);
    add!(segment, y);
    with-output-recording-options (sheet, record?: #f)
      draw-polygon(sheet, segment, closed?: #f, filled?: #f)
    end
  end
end method add-scribble-segment;
||#

(defmethod add-scribble-segment ((sheet <scribble-recording-pane>)
				 x y)
  (let ((segment (scribble-segment sheet)))
    (when segment
      (add! segment x)
      (add! segment y)
      (with-output-recording-options (sheet :record? nil)
	(draw-polygon sheet segment :closed? nil :filled? nil)))))

#||
define frame <scribble-recording-frame> (<simple-frame>)
  pane surface (frame)
    make(<scribble-recording-pane>, 
	 popup-menu-callback: method (sheet, x, y)
				let frame = sheet-frame(sheet);
				popup-scribble-menu(frame, x, y)
			      end,
	 width:  300, max-width:  $fill,
	 height: 200, max-height: $fill);
  pane file-menu (frame)
    make(<menu>,
	 label: "File",
	 children: vector(make(<menu-button>,
			       label: "&Clear",
			       selection-mode: #"none",
			       activate-callback: method (button)
						    clear-output-history(frame.surface)
						  end),
			  make(<menu-button>,
			       label: "E&xit",
			       selection-mode: #"none",
			       activate-callback: method (button)
						    exit-frame(sheet-frame(button))
						  end)));
  pane scribble-popup-menu (frame)
    make(<menu>,
	 owner: frame.surface,
	 children: vector(make(<menu-button>,
			       label: "&Clear",
			       selection-mode: #"none",
			       activate-callback: method (button)
						    ignore(button);
						    clear-output-history(frame.surface)
						  end)));
  layout (frame) frame.surface;
  menu-bar (frame)
    make(<menu-bar>, children: vector(frame.file-menu));
end frame <scribble-recording-frame>;
||#

(define-frame <scribble-recording-frame> (<simple-frame>)
  ((:pane surface (frame)
     (make-pane '<scribble-recording-pane>
		:popup-menu-callback
		#'(lambda (sheet x y)
		    (let ((frame (sheet-frame sheet)))
		      (popup-scribble-menu frame x y)))
		:width 300 :max-width +fill+
		:height 200 :max-height +fill+))
   (:pane file-menu (frame)
     (make-pane '<menu>
		:label "File"
		:children
		(vector (make-pane '<menu-button>
				   :label "&Clear"
				   :selection-mode :none
				   :activate-callback
				   #'(lambda (button)
				       (clear-output-history (surface frame))))
			(make-pane '<menu-button>
				   :label "E&xit"
				   :selection-mode :none
				   :activate-callback
				   #'(lambda (button)
				       (exit-frame (sheet-frame button)))))))
   (:pane scribble-popup-menu (frame)
     (make-pane '<menu>
		:owner (surface frame)
		:children
		(vector (make-pane '<menu-button>
				   :label "&Clear"
				   :selection-mode :none
				   :activate-callback
				   #'(lambda (button)
				       (declare (ignore button))
				       (clear-output-history (surface frame)))))))
   (:layout (frame) (surface frame))
   (:menu-bar (frame)
     (make-pane '<menu-bar>
		:children (vector (file-menu frame))))))

#||
define method popup-scribble-menu
    (frame :: <scribble-recording-frame>, x :: <integer>, y :: <integer>) => ()
  let menu = frame.scribble-popup-menu;
  display-menu(menu, x: x, y: y)
end method popup-scribble-menu;
||#

(defmethod popup-scribble-menu ((frame <scribble-recording-frame>)
				(x integer) (y integer))
  (let ((menu (scribble-popup-menu frame)))
    (display-menu menu :x x :y y)))

#||
install-test(<scribble-recording-frame>, "Recording -- Scribble");
||#

(install-test (find-class '<scribble-recording-frame>) "Recording -- Scribble")


#||
/// 'with-room-for-graphics' test

define class <wrfg-pane> (<recording-pane>)
end class <wrfg-pane>;
||#

(defclass <wrfg-pane> (<recording-pane>) ())

#||
define method handle-button-event
    (sheet :: <wrfg-pane>, 
     event :: <button-press-event>, 
     button == $left-button) => ()
  let medium = sheet-medium(sheet);
  let x = event-x(event);
  let y = event-y(event);
  with-drawing-options (medium, pen: $dotted-pen)
    draw-line(sheet, x, y, x + 20, y + 20)
  end;
  with-drawing-options (medium, pen: $solid-pen)
    with-room-for-graphics (sheet, x: x, y: y)
      draw-line(sheet, 0, 0, 20, 20)
    end
  end;
end method handle-button-event;
||#

(defmethod handle-button-event ((sheet <wrfg-pane>)
				(event <button-press-event>)
				(button (eql +left-button+)))
  (let ((medium (sheet-medium sheet))
	(x (event-x event))
	(y (event-y event)))
    (with-drawing-options (medium :pen *dotted-pen*)
      (draw-line sheet x y (+ x 20) (+ y 20)))
    (with-drawing-options (medium :pen *solid-pen*)
      (with-room-for-graphics (sheet :x x :y y)
	(draw-line sheet 0 0 20 20)))))

#||
define frame <wrfg-frame> (<simple-frame>)
  pane surface (frame)
    make(<wrfg-pane>,
	 width:  300, max-width:  $fill,
	 height: 200, max-height: $fill);
  pane file-menu (frame)
    make(<menu>,
	 label: "File",
	 children: vector(make(<menu-button>,
			       label: "&Clear",
			       selection-mode: #"none",
			       activate-callback: method (button)
						    clear-output-history(frame.surface)
						  end),
			  make(<menu-button>,
			       label: "E&xit",
			       selection-mode: #"none",
			       activate-callback: method (button)
						    exit-frame(sheet-frame(button))
						  end)));
  layout (frame) frame.surface;
  menu-bar (frame)
    make(<menu-bar>, children: vector(frame.file-menu));
end frame <wrfg-frame>;
||#

(define-frame <wrfg-frame> (<simple-frame>)
  ((:pane surface (frame)
     (make-pane '<wrfg-pane>
		:width  300 :max-width  +fill+
		:height 200 :max-height +fill+))
   (:pane file-menu (frame)
     (make-pane '<menu>
		:label "File"
		:children
		(vector (make-pane '<menu-button>
				   :label "&Clear"
				   :selection-mode :none
				   :activate-callback
				   #'(lambda (button)
				       (clear-output-history (surface frame))))
			(make-pane '<menu-button>
				   :label "E&xit"
				   :selection-mode :none
				   :activate-callback
				   #'(lambda (button)
				       (exit-frame (sheet-frame button)))))))
   (:layout (frame) (surface frame))
   (:menu-bar (frame)
     (make-pane '<menu-bar>
		:children (vector (file-menu frame))))))

#||
install-test(<wrfg-frame>, "Recording -- Room...");
||#

(install-test (find-class '<wrfg-frame>) "Recording -- Room...")

