;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GUI-TEST-SUITE -*-
(in-package #:gui-test-suite)

#||
define method frame-add-items (frame :: <frame>)
  unless (frame.%proceed-buttons)
    frame.%proceed-buttons
      := add-proceed-items(frame, "One", "Two", "Three");
    command-enabled?(frame-add-items, frame)    := #f;
    command-enabled?(frame-remove-items, frame) := #t
  end
end method frame-add-items;
||#

(defmethod frame-add-items ((frame <frame>))
  (unless (%proceed-buttons frame)
    (setf (%proceed-buttons frame)
	  (add-proceed-items frame "One" "Two" "Three"))
    (setf (command-enabled-p frame-add-items frame) nil)
    (setf (command-enabled-p frame-remove-items frame) t)))

#||
define method frame-remove-items (frame :: <frame>)
  when (frame.%proceed-buttons)
    remove-proceed-items(frame, frame.%proceed-buttons);
    frame.%proceed-buttons := #f;
    command-enabled?(frame-add-items, frame)    := #t;
    command-enabled?(frame-remove-items, frame) := #f
  end
end method frame-remove-items;
||#

(defmethod frame-remove-items ((frame <frame>))
  (when (%proceed-buttons frame)
    (remove-proceed-items frame (%proceed-buttons frame))
    (setf (%proceed-buttons frame) nil)
    (setf (command-enabled-p frame-add-items frame) t)
    (setf (command-enabled-p frame-remove-items frame) nil)))

#||
define command-table *file-command-table* (*global-command-table*)
  menu-item "Add"    = frame-add-items;
  menu-item "Remove" = frame-remove-items;
  separator;
  menu-item "Exit"   = exit-frame;
end command-table *file-command-table*;
||#

(define-command-table *file-command-table* (*global-command-table*)
  (menu-item "Add"    = frame-add-items)
  (menu-item "Remove" = frame-remove-items)
  separator
  (menu-item "Exit"   = exit-frame))

#||
define method frame-abort  (frame :: <frame>) end;
define method frame-resume (frame :: <frame>) end;
||#

(defmethod frame-abort ((frame <frame>)))
(defmethod frame-resume ((frame <frame>)))

#||
define command-table *proceed-command-table* (*global-command-table*)
  menu-item "Abort"  = frame-abort;
  menu-item "Resume" = frame-resume;
end command-table *proceed-command-table*;
||#

(define-command-table *proceed-command-table* (*global-command-table*)
  (menu-item "Abort"  = frame-abort)
  (menu-item "Resume" = frame-resume))

#||
define command-table *surgery-command-table* (*global-command-table*)
  menu-item "File"    = *file-command-table*;
  menu-item "Proceed" = *proceed-command-table*;
end command-table *test-command-table*;
||#

(define-command-table *surgery-command-table* (*global-command-table*)
  (menu-item "File"    = *file-command-table*)
  (menu-item "Proceed" = *proceed-command-table*))

#|
define frame <surgery-frame> (<simple-frame>)
  slot %proceed-buttons = #f;
  command-table (frame) *surgery-command-table*;
end frame <surgery-frame>;
||#

(define-frame <surgery-frame> (<simple-frame>)
  ((%proceed-buttons :initform nil
		     :accessor %proceed-buttons)
   (:command-table (frame)
		   *surgery-command-table*)))

#||
/// Adding and removing proceed items

define method add-proceed-items
    (frame :: <frame>, #rest items) => (buttons :: false-or(<menu-box>))
  let menu
    = block (return)
	do-command-menu-gadgets(method (menu) return(menu) end,
				frame, *proceed-command-table*,
				tool-bar?: #f);
	#f
      end;
  when (menu)
    let buttons = make(<vector>, size: size(items));
    for (item in items,
         i :: <integer> from 0)
      let label    = item;			//---*** name of handler here
      let callback = method (sheet) #f end;	//---*** invoke the handler here
      let button   = make(<push-menu-button>,
			  label:   label,
			  activate-callback: callback);
      buttons[i] := button
    end;
    let menu-box = make(<push-menu-box>,
			children: as(<simple-vector>, buttons));
    add-child(menu, menu-box);
    menu-box
  end
end method add-proceed-items;
||#

(defmethod add-proceed-items ((frame <frame>)
			      &rest items)
  (let ((menu (block return
		(do-command-menu-gadgets #'(lambda (menu)
					     (return-from return menu))
		  frame
		  *proceed-command-table*
		  :tool-bar? nil)
		nil)))
    (when menu
      (let ((buttons (make-array (size-items))))
	(loop for item in items
              for i from 0
              do (let* ((label item)            ;;---*** name of handler here
			(callback #'(lambda (sheet)
				      nil))     ;;---*** invoke the handler here
			(button (make-pane '<push-menu-button>
					   :label label
					   :activate-callback callback)))
		   (setf (aref buttons i) button)))
	(let ((menu-box (make-pane '<push-menu-box>
				   :children buttons)))
	  (add-child menu menu-box)
	  menu-box)))))

#||
define method remove-proceed-items
    (frame :: <frame>, buttons :: <menu-box>) => ()
  let menu
    = block (return)
	do-command-menu-gadgets(method (menu) return(menu) end,
				frame, *proceed-command-table*,
				tool-bar?: #f);
	#f
      end;
  when (menu)
    remove-child(menu, buttons)
  end
end method remove-proceed-items;
||#

(defmethod remove-proceed-items ((frame <frame>)
				 (buttons <menu-box>))
  (let ((menu (block return
		(do-command-menu-gadgets #'(lambda (menu)
					     (return-from return menu))
		  frame
		  *proceed-command-table*
		  :tool-bar? nil)
		nil)))
    (when menu
      (remove-child menu buttons))))


#||
install-test(<surgery-frame>, "Dynamic Command Tables");
||#

(install-test (find-class '<surgery-frame>) "Dynamic Command Tables")
