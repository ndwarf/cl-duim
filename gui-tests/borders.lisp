;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GUI-TEST-SUITE -*-
(in-package #:gui-test-suite)

#||
Module:       duim-gui-test-suite
Author:       Andy Armstrong
Synopsis:     An interactive test-suite for DUIM objects

/// Border tests

define frame <border-test-frame> (<simple-frame>)
  pane flat-border (frame)
    with-border (type: #"flat")   make-ellipse-with-space() end;
  pane raised-border (frame)
    with-border (type: #"raised") make-ellipse-with-space() end;
  pane sunken-border (frame)
    with-border (type: #"sunken") make-ellipse-with-space() end;
  pane ridge-border (frame)
    with-border (type: #"ridge")  make-ellipse-with-space() end;
  pane groove-border (frame)
    with-border (type: #"groove") make-ellipse-with-space() end;
  pane group-box-border (frame) 
    grouping ("Test") make(<ellipse-pane>) end;
  layout (frame)
    make(<table-layout>,
         columns: 2,
         height: 500,
         x-alignment: #[#"right", #"left"], y-alignment: #"center",
	 x-spacing: 4, y-spacing: 4,
         children: vector(make(<label>, label: "Flat:"),
                          frame.flat-border,
                          make(<label>, label: "Raised:"),
                          frame.raised-border,
                          make(<label>, label: "Sunken:"),
                          frame.sunken-border,
                          make(<label>, label: "Ridged:"),
                          frame.ridge-border,
                          make(<label>, label: "Groove:"),
                          frame.groove-border,
                          make(<label>, label: "Group:"),
                          frame.group-box-border))
end frame <border-test-frame>;
||#

(define-frame <border-test-frame> (<simple-frame>)
  ((:pane flat-border (frame)
     (with-border (:type :flat)
       (make-ellipse-with-space)))
   (:pane raised-border (frame)
     (with-border (:type :raised)
       (make-ellipse-with-space)))
   (:pane sunken-border (frame)
     (with-border (:type :sunken)
       (make-ellipse-with-space)))
   (:pane ridge-border (frame)
     (with-border (:type :ridge)
       (make-ellipse-with-space)))
   (:pane groove-border (frame)
     (with-border (:type :groove)
       (make-ellipse-with-space)))
   (:pane group-box-border (frame)
     (grouping ("Test")
       (make-pane '<ellipse-pane>)))
   (:layout (frame)
     (make-pane '<table-layout>
		:columns 2
		:height 500
		:x-alignment #(:right :left) :y-alignment :center
		:x-spacing 4 :y-spacing 4
		:children
		(vector (make-pane '<label> :label "Flat:")
			(flat-border frame)
			(make-pane '<label> :label "Raised:")
			(raised-border frame)
			(make-pane '<label> :label "Sunken:")
			(sunken-border frame)
			(make-pane '<label> :label "Ridged:")
			(ridge-border frame)
			(make-pane '<label> :label "Groove:")
			(groove-border frame)
			(make-pane '<label> :label "Group:")
			(group-box-border frame))))))

#||
define function make-ellipse-with-space
    () => (sheet :: <sheet>)
  with-spacing (spacing: 4)
    make(<ellipse-pane>)
  end
end function make-ellipse-with-space;
||#

(defun make-ellipse-with-space ()
  (with-spacing (:spacing 4)
    (make-pane '<ellipse-pane>)))

#||
install-test(<border-test-frame>, "Borders");
||#

(install-test (find-class '<border-test-frame>) "Borders")
