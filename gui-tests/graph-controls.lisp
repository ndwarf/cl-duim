;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GUI-TEST-SUITE -*-
(in-package #:gui-test-suite)

#||
Module:       duim-gui-test-suite
Author:       Andy Armstrong
Synopsis:     DUIM example code

/// Number graph controls

define frame <number-graph-frame> (<simple-frame>)
  pane roots-pane (frame)
    make(<check-box>, 
         items: #[1, 2, 3, 4],
         value: #[1],
	 value-changed-callback: method (gadget)
                                   tree-control-roots(frame.tree-pane)
                                     := sort(gadget-value(gadget));
                                   tree-control-roots(frame.tree-graph-pane)
                                     := sort(gadget-value(gadget));
                                   tree-control-roots(frame.dag-graph-pane)
                                     := sort(gadget-value(gadget));
                                 end);
  pane tree-pane (frame)
    make(<tree-control>,
         roots: #[1],
         depth: 2,
	 children-generator: method (x) vector(x * 2, x * 2 + 1) end,
	 min-height: 150, min-width: 250);
  pane tree-graph-pane (frame)
    make(<graph-control>,
	 graph-type: #"tree",
         roots: #[1],
         depth: 2,
         children-generator: method (x) vector(x * 2, x * 2 + 1) end,
	 min-height: 150, min-width: 250);
  pane dag-graph-pane (frame)
    make(<graph-control>,
	 graph-type: #"dag",
         roots: #[1],
         depth: 2,
         children-generator: method (x) vector(x * 2, x * 2 + 1) end,
	 min-height: 150, min-width: 250);
  pane main-layout (frame)
    vertically (spacing: 2)
      frame.roots-pane;
      make(<separator>);
      frame.tree-pane;
      frame.tree-graph-pane;
      frame.dag-graph-pane;
    end;
  layout (frame) frame.main-layout;
end frame <number-graph-frame>;
||#

(define-frame <number-graph-frame> (<simple-frame>)
  ((:pane roots-pane (frame)
     (make-pane '<check-box>
		:items #(1 2 3 4)
		:value #(1)
		:value-changed-callback
		#'(lambda (gadget)
		    (setf (tree-control-roots (tree-pane frame))
			  (sort (gadget-value gadget) #'<))
		    (setf (tree-control-roots (tree-graph-pane frame))
			  (sort (gadget-value gadget) #'<))
		    (setf (tree-control-roots (dag-graph-pane frame))
			  (sort (gadget-value gadget) #'<)))))
   (:pane tree-pane (frame)
     (make-pane '<tree-control>
		:roots #(1)
		:depth 2
		:children-generator #'(lambda (x)
					(vector (* x 2) (+ 1 (* x 2))))
		:min-height 150 :min-width 250))
   (:pane tree-graph-pane (frame)
     (make-pane '<graph-control>
		:graph-type :tree
		:roots #(1)
		:depth 2
		:children-generator #'(lambda (x)
					(vector (* x 2) (+ 1 (* x 2))))
		:min-height 150 :min-width 250))
   (:pane dag-graph-pane (frame)
     (make-pane '<graph-control>
		:graph-type :dag
		:roots #(1)
		:depth 2
		:children-generator #'(lambda (x)
					(vector (* x 2) (+ 1 (* x 2))))
		:min-height 150 :min-width 250))
   (:pane main-layout (frame)
     (vertically (:spacing 2)
       (roots-pane frame)
       (make-pane '<separator>)
       (tree-pane frame)
       (tree-graph-pane frame)
       (dag-graph-pane frame)))
   (:layout (frame)
     (main-layout frame))))


#||
/// Class graph controls

define frame <class-graph-frame> (<simple-frame>)
  pane tree-pane (frame)
    make(<tree-control>,
         roots: vector(<graph-control>),
         depth: 2,
         activate-callback: tab-control-activate-callback,
	 children-generator: direct-superclasses,
	 label-key: method (c) as(<string>, debug-name(c)) end,
	 min-height: 150, min-width: 250);
  pane tree-graph-pane (frame)
    make(<graph-control>,
	 graph-type: #"tree",
         roots: vector(<graph-control>),
         depth: 2,
         activate-callback: tab-control-activate-callback,
	 children-generator: direct-superclasses,
	 label-key: method (c) as(<string>, debug-name(c)) end,
	 min-height: 150, min-width: 250);
  pane dag-graph-pane (frame)
    make(<graph-control>,
	 graph-type: #"dag",
         roots: vector(<graph-control>),
         depth: 2,
         activate-callback: tab-control-activate-callback,
	 children-generator: direct-superclasses,
	 label-key: method (c) as(<string>, debug-name(c)) end,
	 min-height: 150, min-width: 250);
  pane main-layout (frame)
    vertically (spacing: 2)
      frame.tree-pane;
      frame.tree-graph-pane;
      frame.dag-graph-pane;
    end;
  layout (frame) frame.main-layout;
end frame <class-graph-frame>;
||#

(defun class-direct-supers (class)
  #+sbcl (sb-mop::class-direct-superclasses)
  #+ccl  (ccl:class-direct-superclasses class))

(defun class-direct-subs (class)
  #+sbcl (sb-mop:class-direct-subclasses)
  #+ccl  (ccl:class-direct-subclasses class))

(define-frame <class-graph-frame> (<simple-frame>)
  ((:pane tree-pane (frame)
     (make-pane '<tree-control>
		:roots (vector (find-class '<graph-control>))
		:depth 2
		:activate-callback #'tab-control-activate-callback
		:children-generator #'class-direct-supers
		:label-key #'(lambda (c)
			       (format nil "~a" c))
		:min-height 150 :min-width 250))
   (:pane tree-graph-pane (frame)
     (make-pane '<graph-control>
		:graph-type :tree
		:roots (vector (find-class '<graph-control>))
		:depth 2
		:activate-callback #'tab-control-activate-callback
		:children-generator #'class-direct-supers
		:label-key #'(lambda (c)
			       (format nil "~a" c))
		:min-height 150 :min-width 250))
   (:pane dag-graph-pane (frame)
     (make-pane '<graph-control>
		:graph-type :dag
		:roots (vector (find-class '<graph-control>))
		:depth 2
		:activate-callback #'tab-control-activate-callback
		:children-generator #'class-direct-supers
		:label-key #'(lambda (c)
			       (format nil "~a" c))
		:min-height 150 :min-width 250))
   (:pane main-layout (frame)
     (vertically (:spacing 2)
       (tree-pane frame)
       (tree-graph-pane frame)
       (dag-graph-pane frame)))
   (:layout (frame)
     (main-layout frame))))


#||
/// Install the test

define variable $graph-control-tests
  = vector(vector("Number graphs", <number-graph-frame>),
	   vector("Class graphs",  <class-graph-frame>));
||#

(defparameter *graph-control-tests*
  (vector (vector "Number graphs" (find-class '<number-graph-frame>))
	  (vector "Class graphs"  (find-class '<class-graph-frame>))))

#||
define frame <graph-control-frame> (<simple-frame>)
  pane examples (frame)
    make(<list-control>,
         scroll-bars: #"none",
	 documentation: "Double-click on a test name to run it",
	 items: $graph-control-tests,
	 lines: size($graph-control-tests),
	 label-key: first,
	 activate-callback: method (gadget :: <gadget>)
                              let frame = sheet-frame(gadget);
                              let value = gadget-value(gadget);
                              let title = first(value);
                              let class = second(value);
                              let test-frame = make(class, title: title, owner: frame);
                              start-frame(test-frame)
			    end);
  pane main-layout (frame)
    frame.examples;
  layout (frame) frame.main-layout;
end frame <graph-control-frame>;
||#

(define-frame <graph-control-frame> (<simple-frame>)
  ((:pane examples (frame)
     (make-pane '<list-control>
		:scroll-bars :none
		:documentation "Double-click on a test name to run it"
		:items *graph-control-tests*
		:lines (size *graph-control-tests*)
		:label-key (alexandria:rcurry #'aref 0)  ;; #'first
		:activate-callback #'(lambda (gadget)
				       (let* ((frame (sheet-frame gadget))
					      (value (gadget-value gadget))
					      (title (aref value 0))  ;;first value))
					      (class (aref value 1))  ;;second value))
					      (test-frame (make-instance class :title title :owner frame)))
					 (start-frame test-frame)))))
   (:pane main-layout (frame)
     (examples frame))
   (:layout (frame)
     (main-layout frame))))


#||
install-test(<graph-control-frame>, "Graph controls");
||#

(install-test (find-class '<graph-control-frame>) "Graph controls")
