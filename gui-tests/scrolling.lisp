;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GUI-TEST-SUITE -*-
(in-package #:gui-test-suite)

#||
Module:       duim-gui-test-suite
Author:       Andy Armstrong
Synopsis:     DUIM test code

/// Ruler pane

define sealed class <ruler-pane>
    (<oriented-gadget-mixin>,
     <basic-gadget>,
     <drawing-pane>)
  keyword orientation: = #"vertical";
end class <ruler-pane>;
||#

(defclass <ruler-pane>
    (<oriented-gadget-mixin>
     <basic-gadget>
     <drawing-pane>)
  ()
  (:default-initargs :orientation :vertical))


#||
define method handle-repaint 
    (pane :: <ruler-pane>, medium :: <medium>, region :: <region>) => ()
  select (gadget-orientation(pane))
    #"vertical" =>
      let (left, top, right, bottom) = box-edges(pane);
      let (width, height) = values(right - left, bottom - top);
      let gradations = 10;
      let right-x = width - 50;
      let half-x = floor/(right-x, 2);
      let quarter-x = floor/(right-x, 4);
      for (y from top to bottom by gradations)
        case
	  modulo(y, 100) = 0 =>
            draw-line(medium, 0, y, right-x, y);
            draw-text(medium, format-to-string("%d", y), 
                      right-x, y, 
                      align-x: #"left", 
                      align-y: case
                                 y == top                => #"top";
                                 y > bottom - gradations => #"bottom";
                                 otherwise               => #"center"
                               end);
          modulo(y, 50) = 0 =>
            draw-line(medium, 0, y, half-x, y);
          otherwise =>
            draw-line(medium, 0, y, quarter-x, y);
	end
      end;
    #"horizontal" =>
      let (left, top, right, bottom) = box-edges(pane);
      let (width, height) = values(right - left, bottom - top);
      let gradations = 10;
      let bottom-y = height - 50;
      let half-y = floor/(bottom-y, 2);
      let quarter-y = floor/(bottom-y, 4);
      for (x from left to right by gradations)
        case
	  modulo(x, 100) = 0 =>
            draw-line(medium, x, 0, x, bottom-y);
            draw-text(medium, format-to-string("%d", x),
                      x, bottom-y, 
                      align-x: case
                                 x == left              => #"left";
                                 x > right - gradations => #"right";
                                 otherwise              => #"center";
                               end,
                      align-y: #"top");
          modulo(x, 50) = 0 =>
            draw-line(medium, x, 0, x, half-y);
          otherwise =>
            draw-line(medium, x, 0, x, quarter-y);
	end
      end;
  end
end method handle-repaint;
||#

(defmethod handle-repaint ((pane <ruler-pane>) (medium <medium>) (region <region>))
  (ecase (gadget-orientation pane)
    (:vertical
     (multiple-value-bind (left top right bottom)
	 (box-edges pane)
       (multiple-value-bind (width height)
	   (values (- right left) (- bottom top))
	 (let* ((graduations 10)
		(right-x (- width 50))
		(half-x (floor right-x 2))
		(quarter-x (floor right-x 4)))
	   (loop for y from top to bottom by graduations
		 do (cond ((= (mod y 100) 0)
			   (draw-line medium 0 y right-x y)
			   (draw-text medium (format nil "~d" y)
				      right-x y
				      :align-x :left
				      :align-y
				      (cond ((= y top)  :top)
					    ((> y (- bottom graduations))
					     :bottom)
					    (t :center))))
		      ((= (mod y 50) 0)
		       (draw-line medium 0 y half-x y))
		      (t
		       (draw-line medium 0 y quarter-x y))))))))
    (:horizontal
     (multiple-value-bind (left top right bottom)
	 (box-edges pane)
       (multiple-value-bind (width height)
	   (values (- right left) (- bottom top))
	 (let* ((graduations 10)
		(bottom-y (- height 50))
		(half-y (floor bottom-y 2))
		(quarter-y (floor bottom-y 4)))
	   (loop for x from left to right by graduations
		 do (cond ((= (mod x 100) 0)
			   (draw-line medium x 0 x bottom-y)
			   (draw-text medium (format nil "~d" x)
				      x bottom-y
				      :align-x
				      (cond ((eq x left)   :left)
					    ((> x (- right graduations)) :right)
					    (t :center))
				      :align-y :top))
			  ((= (mod x 50) 0)
			   (draw-line medium x 0 x half-y))
			  (t
			   (draw-line medium x 0 x quarter-y))))))))))

#||
define method do-compose-space
    (pane :: <ruler-pane>, #key width, height)
 => (space-requirement :: <space-requirement>)
  let vertical? = gadget-orientation(pane) = #"vertical";
  let width  = if (vertical?) 100 else 401 end;
  let height = if (vertical?) 401 else 100 end;
  let max-width  = if (vertical?) 100 else $fill end;
  let max-height = if (vertical?) $fill else 100 end;
  make(<space-requirement>,
       width:  width,  min-width:  100, max-width:  max-width,
       height: height, min-height: 100, max-height: max-height)
end method do-compose-space;
||#

(defmethod do-compose-space ((pane <ruler-pane>) &key width height)
  (let* ((vertical?  (equal? (gadget-orientation pane) :vertical))
	 (width      (if vertical? 100 401))
	 (height     (if vertical? 401 100))
	 (max-width  (if vertical? 100 +fill+))
	 (max-height (if vertical? +fill+ 100)))
    (make-space-requirement
		   :width  width  :min-width  100 :max-width  max-width
		   :height height :min-height 100 :max-height max-height)))


#||
/// grid display pane

define sealed class <checkboard-pane> (<drawing-pane>)
end class <checkboard-pane>;
||#

(defclass <checkboard-pane> (<drawing-pane>) ())

#||
define method handle-repaint 
    (pane :: <checkboard-pane>, medium :: <medium>, region :: <region>) => ()
  let first-filled? = #f;
  let (width, height) = sheet-size(pane);
  for (x from 0 below width by 50)
    let filled? = first-filled?;
    for (y from 0 below height by 50)
      draw-rectangle(medium, x, y, x + 50, y + 50, filled?: filled?);
      filled? := ~filled?;
    end;
    first-filled? := ~first-filled?
  end
end method handle-repaint;
||#

(defmethod handle-repaint ((pane <checkboard-pane>) (medium <medium>) (region <region>))
  (let ((first-filled? nil))
    (multiple-value-bind (width height)
	(sheet-size pane)
      (loop for x from 0 below width by 50
	    do (let ((filled? first-filled?))
		 (loop for y from 0 below height by 50
		       do (draw-rectangle medium x y (+ x 50) (+ y 50) :filled? filled?)
		       do (setf filled? (not filled?))))
	    do (setf first-filled? (not first-filled?))))))

#||
define method do-compose-space
    (pane :: <checkboard-pane>, #key width, height)
 => (space-requirement :: <space-requirement>)
  make(<space-requirement>,
       width:  800, min-width:  1, max-width:  $fill,
       height: 800, min-height: 1, max-height: $fill)
end method do-compose-space;
||#

(defmethod do-compose-space ((pane <checkboard-pane>)
			     &key width height)
  (make-space-requirement
		 :width  800 :min-width  1 :max-width  +fill+
		 :height 800 :min-height 1 :max-height +fill+))

#||
/// scroll testing

define method test-scrolling 
    (frame :: <frame>) => ()
  contain(scrolling ()
	    make(<ruler-pane>, height: 800)
	  end,
          title: "Simple Scrolling Test",
          height: 400)
end method test-scrolling;
||#

(defmethod test-scrolling ((frame <frame>))
  (contain (scrolling ()
	     (make-pane '<ruler-pane> :height 800))
	   :title "Simple Scrolling Test"
	   :height 400))

#||
define method test-two-pane-scrolling 
    (frame :: <frame>) => ()
  let scroll-bar = make(<scroll-bar>, orientation: #"vertical");
  let viewport1
    = make(<viewport>, 
	   child: make(<ruler-pane>),
	   min-width: 100,
	   min-height: 100,
	   height: 300,
	   vertical-scroll-bar: scroll-bar);
  let viewport2
    = make(<viewport>, 
	   child: make(<ruler-pane>),
	   min-width: 100,
	   min-height: 100,
	   height: 300,
	   vertical-scroll-bar: scroll-bar);
  contain(with-border (type: #"sunken")
	    horizontally ()
	      viewport1; 
	      viewport2;
	      scroll-bar
            end
	  end,
          title: "Two Pane Scrolling Test")
end method test-two-pane-scrolling;
||#

(defmethod test-two-pane-scrolling ((frame <frame>))
  (let* ((scroll-bar (make-pane '<scroll-bar> :orientation :vertical))
 	 (viewport1
 	  (make-pane '<viewport>
 		     :child (make-pane '<ruler-pane>)
 		     :min-width  100
 		     :min-height 100
 		     :height 300
 		     :vertical-scroll-bar scroll-bar))
 	 (viewport2
 	  (make-pane '<viewport>
 		     :child (make-pane '<ruler-pane>)
 		     :min-width  100
 		     :min-height 100
 		     :height 300
 		     :vertical-scroll-bar scroll-bar)))
    (contain (with-border (:type :sunken)
	       (horizontally ()
	         viewport1
		 viewport2
		 scroll-bar))
	     :title "Two Pane Scrolling Test")))

#||
define method test-three-pane-scrolling
    (frame :: <frame>) => ()
  let horizontal-bar = make(<scroll-bar>, orientation: #"horizontal");
  let vertical-bar = make(<scroll-bar>, orientation: #"vertical");
  let horizontal-ruler
    = make(<viewport>, 
	   child: make(<ruler-pane>, orientation: #"horizontal"),
	   horizontal-scroll-bar: horizontal-bar);
  let vertical-ruler
    = make(<viewport>, 
	   child: make(<ruler-pane>, orientation: #"vertical"),
	   vertical-scroll-bar:   vertical-bar);
  let viewport
    = make(<viewport>,
	   child: make(<checkboard-pane>),
	   min-width: 300,
	   min-height: 300,
	   horizontal-scroll-bar: horizontal-bar,
	   vertical-scroll-bar:   vertical-bar);
  let table-contents
    = vector(vector(#f, horizontal-ruler, #f),
             vector(vertical-ruler, viewport, vertical-bar),
             vector(#f, horizontal-bar, #f));
  contain(with-border (type: #"sunken", width: 300, height: 300)
	    make(<table-layout>, contents: table-contents)
          end,
	  title: "Three Pane Scrolling Test")
end method test-three-pane-scrolling;
||#

(defmethod test-three-pane-scrolling ((frame <frame>))
  (let* ((horizontal-bar (make-pane '<scroll-bar> :orientation :horizontal))
	 (vertical-bar   (make-pane '<scroll-bar> :orientation :vertical))
	 (horizontal-ruler
	  (make-pane '<viewport>
		     :child (make-pane '<ruler-pane> :orientation :horizontal)
		     :horizontal-scroll-bar horizontal-bar))
	 (vertical-ruler
	  (make-pane '<viewport>
		     :child (make-pane '<ruler-pane> :orientation :vertical)
		     :vertical-scroll-bar vertical-bar))
	 (viewport
	  (make-pane '<viewport>
		     :child (make-instance '<checkboard-pane>)
		     :min-width  300
		     :min-height 300
		     :horizontal-scroll-bar horizontal-bar
		     :vertical-scroll-bar vertical-bar))
	 (table-contents
	  (vector (vector nil horizontal-ruler nil)
		  (vector vertical-ruler viewport vertical-bar)
		  (vector nil horizontal-bar nil))))
    (contain (with-border (:type :sunken :width 300 :height 300)
	       (make-pane '<table-layout> :contents table-contents))
	     :title "Three Pane Scrolling Test")))

#||
define method test-gadget-scrolling
    (frame :: <frame>, #key orientation = #"vertical") => ()
  contain(scrolling (width: 100, height: 100)
	    make(<radio-box>,
		 items: range(from: 1, to: 20),
		 label-key: method (i) format-to-string("%d", i) end,
		 orientation: orientation)
	  end,
          title: "Gadget Scrolling Test")
end method test-gadget-scrolling;
||#

(defmethod test-gadget-scrolling ((frame <frame>) &key (orientation :vertical))
  (contain (scrolling (:width 100 :height 100)
	     (make-pane '<radio-box>
			:items (range :from 1 :to 20)
			:label-key #'(lambda (i) (format nil "~d" i))
			:orientation orientation))
	   :title "Gadget Scrolling Test"))



#||
/// Advanced scrolling

define method make-spreadsheet 
    (frame :: <frame>) => (table :: <table-layout>)
  with-frame-manager (frame-manager(frame))
    let children = make(<vector>, size: 100);
    for (x from 0 below 10)
      for (y from 0 below 10)
        let label = format-to-string("Position %d, %d", x, y);
        children[x + y * 10] := make(<push-button>, label: label)
      end
    end;
    make(<table-layout>,
	 columns: 10,
	 x-spacing: 4,
	 y-spacing: 4,
	 children: children)
  end
end method make-spreadsheet;
||#

(defmethod make-spreadsheet ((frame <frame>))
  (with-frame-manager ((frame-manager frame))
    (let ((children (make-array 100)))
      (loop for x from 0 below 10
	    do (loop for y from 0 below 10
		     do (let ((label (format nil "Position ~d, ~d" x y)))
			  (setf (aref children (+ x (* y 10)))
				(make-pane '<push-button> :label label)))))
      (make-pane '<table-layout>
		 :columns 10
		 :x-spacing 4
		 :y-spacing 4
		 :children children))))


#||
define method test-advanced-scrolling 
    (frame :: <frame>) => ()
  contain(scrolling (width: 300, height: 200)
            make-spreadsheet(frame)
	  end,
          title: "Advanced Scrolling Test")
end method test-advanced-scrolling;
||#

(defmethod test-advanced-scrolling ((frame <frame>))
  (contain (scrolling (:width 300 :height 200)
	     (make-spreadsheet frame))
	   :title "Advanced Scrolling Test"))



#||
/// Hand-rolled scrolling

define sealed class <hand-rolled-scrolling-pane> 
    (<scrolling-sheet-mixin>, 
     <drawing-pane>)
  slot top-line-number :: <integer> = 0;
  constant slot last-line-number :: <integer> = 300;
end class <hand-rolled-scrolling-pane>;
||#

(defclass <hand-rolled-scrolling-pane>
    (<scrolling-sheet-mixin>
     <drawing-pane>)
  ((top-line-number :type integer
		    :initform 0
		    :accessor top-line-number)
   (last-line-number :type integer
		     :initform 300
		     :reader last-line-number)))

#||
define method do-allocate-space
    (pane :: <hand-rolled-scrolling-pane>, width :: <integer>, height :: <integer>) => ()
  next-method();
  update-scroll-bars(pane)
end method do-allocate-space;
||#

(defmethod do-allocate-space ((pane <hand-rolled-scrolling-pane>)
			      (width integer)
			      (height integer))
  (call-next-method)
  (update-scroll-bars pane))

#||
define function visible-lines
    (pane :: <hand-rolled-scrolling-pane>) => (lines :: <integer>)
  let _port = port(pane);
  let (width, height) = sheet-size(pane);
  ignore(width);
  let text-style = get-default-text-style(_port, pane);
  let text-height = font-height(text-style, _port);
  let top-line = top-line-number(pane);
  floor/(height, text-height) + top-line;
end function visible-lines;
||#

(defmethod visible-lines ((pane <hand-rolled-scrolling-pane>))
  (let ((_port (port pane)))
    (multiple-value-bind (width height)
	(sheet-size pane)
      (declare (ignore width))
      (let* ((text-style (get-default-text-style _port pane))
	     (text-height (font-height text-style _port))
	     (top-line (top-line-number pane)))
	(+ (floor height text-height) top-line)))))

#||
define method line-scroll-amount
    (pane :: <hand-rolled-scrolling-pane>, orientation == #"vertical")
 => (amount :: <integer>)
  1
end method line-scroll-amount;
||#

(defmethod line-scroll-amount ((pane <hand-rolled-scrolling-pane>) (orientation (eql :vertical)))
  1)

#||
define method page-scroll-amount
    (pane :: <hand-rolled-scrolling-pane>, orientation == #"vertical")
 => (amount :: <integer>)
  visible-lines(pane) - 1
end method page-scroll-amount;
||#

(defmethod page-scroll-amount ((pane <hand-rolled-scrolling-pane>) (orientation (eql :vertical)))
  (- (visible-lines pane) 1))

#||
define method sheet-scroll-range
    (pane :: <hand-rolled-scrolling-pane>)
 => (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
  values(0, 0, 1, last-line-number(pane))
end method sheet-scroll-range;
||#

(defmethod sheet-scroll-range ((pane <hand-rolled-scrolling-pane>))
  (values 0 0 1 (last-line-number pane)))

#||
define method sheet-visible-range
    (pane :: <hand-rolled-scrolling-pane>)
 => (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
  let top-line = top-line-number(pane);
  values(0, top-line, 1, top-line + visible-lines(pane))
end method sheet-visible-range;
||#

(defmethod sheet-visible-range ((pane <hand-rolled-scrolling-pane>))
  (let ((top-line (top-line-number pane)))
    (values 0 top-line 1 (+ top-line (visible-lines pane)))))

#||
define method set-sheet-visible-range
    (pane :: <hand-rolled-scrolling-pane>,
     left :: <real>, top :: <real>, right :: <real>, bottom :: <real>) => ()
  ignore(left, right, bottom);
  top-line-number(pane) := round(top);
  clear-box*(pane, $everywhere);
  repaint-sheet(pane, $everywhere)
end method set-sheet-visible-range;
||#

(defmethod set-sheet-visible-range ((pane <hand-rolled-scrolling-pane>)
				    (left real) (top real) (right real) (bottom real))
  (declare (ignore left right bottom))
  (setf (top-line-number pane) (round top))
  (clear-box* pane *everywhere*)
  (repaint-sheet pane *everywhere*))

#||
define method handle-repaint
    (pane :: <hand-rolled-scrolling-pane>, medium :: <medium>, region :: <region>) => ()
  let _port = port(pane);
  let text-style = get-default-text-style(_port, pane);
  let text-height = font-height(text-style, _port);
  let top-line = top-line-number(pane);
  let bottom-line = visible-lines(pane);
  for (i from top-line to bottom-line,
       count from 0)
    draw-text(medium, format-to-string("%d", i), 0, count * text-height, align-y: #"top")
  end;
end method handle-repaint;
||#

(defmethod handle-repaint ((pane <hand-rolled-scrolling-pane>) (medium <medium>) (region <region>))
  (let* ((_port (port pane))
	 (text-style (get-default-text-style _port pane))
	 (text-height (font-height text-style _port))
	 (top-line (top-line-number pane))
	 (bottom-line (visible-lines pane)))
    (loop for i from top-line to bottom-line
	  for count from 0
	  do (draw-text medium (format nil "~d" i) 0 (* count text-height) :align-y :top))))


#||
define frame <hand-rolled-scrolling-test-frame> (<simple-frame>)
  pane scrolling-pane (frame)
    make(<hand-rolled-scrolling-pane>);
  layout (frame)
    scrolling (scroll-bars: #"vertical")
      frame.scrolling-pane
    end;
end frame <hand-rolled-scrolling-test-frame>;
||#

(define-frame <hand-rolled-scrolling-test-frame> (<simple-frame>)
  ((:pane scrolling-pane (frame)
     (make-pane '<hand-rolled-scrolling-pane>))
   (:layout (frame)
     (scrolling (:scroll-bars :vertical)
       (scrolling-pane frame)))))

#||
define method test-hand-rolled-scrolling 
    (frame :: <frame>) => ()
  let test-frame
    = make(<hand-rolled-scrolling-test-frame>,
           owner: frame,
           title: "Hand Rolled Scrolling Test");
  start-frame(test-frame)
end method test-hand-rolled-scrolling;
||#

(defmethod test-hand-rolled-scrolling ((frame <frame>))
  (let ((test-frame
	 (make-pane '<hand-rolled-scrolling-test-frame>
		    :owner frame
		    :title "Hand Rolled Scrolling Test")))
    (start-frame test-frame)))


#||
/// Scrolling frame

define variable $scrolling-test-frame-tests
  = vector(vector("Simple",      test-scrolling),
	   vector("Two-Pane",    test-two-pane-scrolling),
	   vector("Three-Pane",  test-three-pane-scrolling),
	   vector("Gadget",      test-gadget-scrolling),
	   vector("Advanced",    test-advanced-scrolling),
	   vector("Hand Rolled", test-hand-rolled-scrolling));
||#

(defparameter *scrolling-test-frame-tests*
  (vector (vector "Simple"      #'test-scrolling)
	  (vector "Two-Pane"    #'test-two-pane-scrolling)
	  (vector "Three-Pane"  #'test-three-pane-scrolling)
	  (vector "Gadget"      #'test-gadget-scrolling)
	  (vector "Advanced"    #'test-advanced-scrolling)
	  (vector "Hand Rolled" #'test-hand-rolled-scrolling)))

#||
define frame <scrolling-tests-frame> (<simple-frame>)
  pane tests (frame)
    make(<list-control>,
	 items: $scrolling-test-frame-tests,
	 lines: size($scrolling-test-frame-tests),
	 label-key: first,
	 value-key: second,
	 activate-callback: method (sheet :: <sheet>)
			      gadget-value(sheet)(sheet-frame(sheet))
			    end);
  pane main-layout (frame)
    frame.tests;
  layout (frame) frame.main-layout;
end frame <scrolling-tests-frame>;
||#

(define-frame <scrolling-tests-frame> (<simple-frame>)
  ((:pane tests (frame)
     (make-pane '<list-control>
		:items *scrolling-test-frame-tests*
		:lines (size *scrolling-test-frame-tests*)
		:label-key (alexandria:rcurry #'aref 0)  ;;#'first
		:value-key (alexandria:rcurry #'aref 1)  ;;#'second
		:activate-callback
		#'(lambda (sheet)
		    (funcall (gadget-value sheet) (sheet-frame sheet)))))
   (:pane main-layout (frame)
     (tests frame))
   (:layout (frame)
     (main-layout frame))))

#||
install-test(<scrolling-tests-frame>, "Scrolling");
||#

(install-test (find-class '<scrolling-tests-frame>) "Scrolling")
