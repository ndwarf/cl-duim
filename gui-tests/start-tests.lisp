;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GUI-TEST-SUITE -*-
(in-package #:gui-test-suite)

#||
Module:       duim-gui-test-suite
Author:       Andy Armstrong
Synopsis:     DUIM example code

define method main
    (arguments :: <sequence>) => (status-code :: <integer>)
  debug-message("Starting DUIM GUI tests with arguments %=", arguments);
  let status-code = start-tests();
  debug-message("Exiting DUIM GUI tests with status code %d", status-code);
  status-code
end method main;
||#

(defmethod main ((arguments sequence))
  (duim-debug-message "Starting DUIM GUI tests with arguments ~a" arguments)
  (let ((status-code (start-tests)))
    (duim-debug-message "Exiting DUIM GUI tests with status code ~d"
			status-code)
    status-code))

#||
main(application-arguments());
||#

;; FIXME: SORT OUT THE APPLICATION-ARGUMENTS
;; Run this in a new thread so that it doesn't block the lisp listener
;; when it goes wrong...

(bordeaux-threads:make-thread #'(lambda ()
				  (main nil))
			      :name "GUI TEST THREAD")

