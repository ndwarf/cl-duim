;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GUI-TEST-SUITE -*-
(in-package #:gui-test-suite)

#||
Module:       duim-gui-test-suite
Author:       Andy Armstrong
Synopsis:     DUIM testing code

/// Frame tests

define abstract frame <abstract-test-frame> (<frame>)
  constant slot frame-destroyed-callback :: false-or(<function>) = #f,
    init-keyword: destroyed-callback:;
end frame <abstract-test-frame>;
||#

(define-frame <abstract-test-frame> (<frame>)  ;; FIXME:ABSTRACT?
  ((frame-destroyed-callback :type (or null function)
			     :initarg :destroyed-callback
			     :initform nil
			     :reader frame-destroyed-callback)))


#||
define method frame-can-exit?
    (frame :: <abstract-test-frame>) => (can-exit? :: <boolean>)
  notify-user("Really exit?", frame: frame, style: #"question")
end method frame-can-exit?;
||#

(defmethod frame-can-exit? ((frame <abstract-test-frame>))
  (notify-user "Really exit?" :frame frame :style :question))


#||
define method handle-event
    (frame :: <abstract-test-frame>, event :: <frame-created-event>) => ()
  notify-user(format-to-string("Frame %= created", frame))
end method handle-event;
||#

(defmethod handle-event ((frame <abstract-test-frame>) (event <frame-created-event>))
  (notify-user (format nil "Frame ~a created" frame)))


#||
define method handle-event
    (frame :: <abstract-test-frame>, event :: <frame-mapped-event>) => ()
  notify-user(format-to-string("Frame %= mapped", frame))
end method handle-event;
||#

(defmethod handle-event ((frame <abstract-test-frame>) (event <frame-mapped-event>))
  (notify-user (format nil "Frame ~a mapped" frame)))


#||
define method handle-event
    (frame :: <abstract-test-frame>, event :: <frame-unmapped-event>) => ()
  notify-user(format-to-string("Frame %= unmapped", frame))
end method handle-event;
||#

(defmethod handle-event ((frame <abstract-test-frame>) (event <frame-unmapped-event>))
  (notify-user (format nil "Frame ~a unmapped" frame)))


#||
define method handle-event
    (frame :: <abstract-test-frame>, event :: <frame-exited-event>) => ()
  notify-user(format-to-string("Frame %= exited", frame))
end method handle-event;
||#

(defmethod handle-event ((frame <abstract-test-frame>) (event <frame-exited-event>))
  (notify-user (format nil "Frame ~a exited" frame)))


#||
define method handle-event
    (frame :: <abstract-test-frame>, event :: <application-exited-event>) => ()
  notify-user(format-to-string("Application %= exited", frame))
end method handle-event;
||#

(defmethod handle-event ((frame <abstract-test-frame>) (event <application-exited-event>))
  (notify-user (format nil "Application ~a exited" frame)))


#||
define method handle-event
    (frame :: <abstract-test-frame>, event :: <frame-destroyed-event>) => ()
  let callback = frame.frame-destroyed-callback;
  notify-user(format-to-string("Frame %= destroyed", frame));
  if (callback)
    callback(frame)
  end
end method handle-event;
||#

(defmethod handle-event ((frame <abstract-test-frame>) (event <frame-destroyed-event>))
  (let ((callback (frame-destroyed-callback frame)))
    (notify-user (format nil "Frame ~a destroyed" frame))
    (when callback
      (funcall callback frame))))


#||
define frame <test-simple-frame> (<abstract-test-frame>, <simple-frame>)
end frame <test-simple-frame>;
||#

(define-frame <test-simple-frame>
    (<abstract-test-frame>
     <simple-frame>)
  ())


#||
define frame <test-dialog-frame> (<abstract-test-frame>, <dialog-frame>)
end frame <test-dialog-frame>;
||#

(define-frame <test-dialog-frame>
    (<abstract-test-frame>
     <dialog-frame>)
  ())



#||
/// Frame tests

define frame <frame-test-frame> (<simple-frame>)
  slot frame-test-frame :: false-or(<frame>) = #f;
  pane frame-type-pane (frame)
    make(<radio-box>,
         orientation: #"vertical",
         items: vector(pair("Simple Frame", <test-simple-frame>),
                       pair("Dialog",       <test-dialog-frame>)),
         value-changed-callback: method (gadget)
                                   let enabled? = (gadget-value(gadget) == <test-simple-frame>);
                                   gadget-enabled?(frame.frame-extras-pane) := enabled?
                                 end,
         label-key:  head,
         value-key: tail);
  pane frame-title-pane (frame)
    make(<text-field>, text: "My Frame");
  pane frame-extras-pane (frame)
    make(<check-box>,
         orientation: #"vertical",
         items: vector(pair("Menu Bar",   #"menu-bar"),
                       pair("Tool Bar",   #"tool-bar"),
                       pair("Status Bar", #"status-bar")),
         label-key:  head,
         value-key: tail);
  pane frame-layout-pane (frame)
    make(<radio-box>,
         orientation: #"vertical",
         items: vector(pair("None", #f),
                       pair("Button", <button>),
                       pair("Tree", <tree-control>)),
         label-key:  head,
         value-key: tail);
  pane frame-x-pane (frame)
    make(<text-field>, value-type: <integer>, value: 0);
  pane frame-y-pane (frame)
    make(<text-field>, value-type: <integer>, value: 100);
  pane frame-width-pane (frame)
    make(<text-field>, value-type: <integer>, value: 200);
  pane frame-height-pane (frame)
    make(<text-field>, value-type: <integer>, value: 300);
  pane make-frame-button (frame)
    make(<push-button>, 
         label: "Make the frame",
         activate-callback: method (gadget)
                              frame-test-make-frame(sheet-frame(gadget))
                            end);
  pane update-frame-button (frame)
    make(<push-button>, 
         label: "Update it",
         enabled?: #f,
         activate-callback: method (gadget)
                              frame-test-update-frame(sheet-frame(gadget))
                            end,
         default?: #t);
  pane main-layout (frame)
    vertically (y-spacing: 4)
      make(<table-layout>,
           columns: 2,
           x-alignment: #(#"right", #"left"),
           y-alignment: #"center",
           y-spacing: 5,
           children: vector(make(<label>, label: "Frame type:"),
                            frame.frame-type-pane,
                            make(<label>, label: "Title:"),
                            frame.frame-title-pane,
                            make(<label>, label: "Gadgets:"),
                            frame.frame-extras-pane,
                            make(<label>, label: "Layout:"),
                            frame.frame-layout-pane,
			    make(<label>, label: "X:"),
			    frame.frame-x-pane,
			    make(<label>, label: "Y:"),
			    frame.frame-y-pane,
			    make(<label>, label: "Width:"),
			    frame.frame-width-pane,
			    make(<label>, label: "Height:"),
			    frame.frame-height-pane));
      make(<separator>);
      horizontally (spacing: 4)
        frame.make-frame-button;
        frame.update-frame-button;
      end
    end;
  layout (frame) frame.main-layout;
  status-bar (frame) make(<status-bar>);
end frame <frame-test-frame>;
||#

(define-frame <frame-test-frame> (<simple-frame>)
  ((frame-test-frame :type (or null <frame>)
		     :initform nil
		     :accessor frame-test-frame)
   (:pane frame-type-pane (frame)
     (make-pane '<radio-box>
		:orientation :vertical
		:items (vector (cons "Simple Frame" (find-class '<test-simple-frame>))
			       (cons "Dialog" (find-class '<test-dialog-frame>)))
		:value-changed-callback
		#'(lambda (gadget)
		    (let ((enabled? (eql (gadget-value gadget)
					 (find-class '<test-simple-frame>))))
		      (setf (gadget-enabled? (frame-extras-pane frame))
			    enabled?)))
		:label-key #'car
		:value-key #'cdr))
   (:pane frame-title-pane (frame)
     (make-pane '<text-field>
		:text "My Frame"))
   (:pane frame-extras-pane (frame)
     (make-pane '<check-box>
		:orientation :vertical
		:items
		(vector (cons "Menu Bar" :menu-bar)
			(cons "Tool Bar" :tool-bar)
			(cons "Status Bar" :status-bar))
		:label-key #'car
		:value-key #'cdr))
   (:pane frame-layout-pane (frame)
     (make-pane '<radio-box>
		:orientation :vertical
		:items
		(vector (cons "None" nil)
			(cons "Button" (find-class '<button>))
			(cons "Tree" (find-class '<tree-control>)))
		:label-key #'car
		:value-key #'cdr))
   (:pane frame-x-pane (frame)
     (make-pane '<text-field> :value-type (find-class 'integer) :value 0))
   (:pane frame-y-pane (frame)
     (make-pane '<text-field> :value-type (find-class 'integer) :value 100))
   (:pane frame-width-pane (frame)
     (make-pane '<text-field> :value-type (find-class 'integer) :value 200))
   (:pane frame-height-pane (frame)
     (make-pane '<text-field> :value-type (find-class 'integer) :value 300))
   (:pane make-frame-button (frame)
     (make-pane '<push-button>
		:label "Make the frame"
		:activate-callback
		#'(lambda (gadget)
		    (frame-test-make-frame (sheet-frame gadget)))))
   (:pane update-frame-button (frame)
     (make-pane '<push-button>
		:label "Update it"
		:enabled? nil
		:activate-callback
		#'(lambda (gadget)
		    (frame-test-update-frame (sheet-frame gadget)))
		:default? t))
   (:pane main-layout (frame)
     (vertically (:y-spacing 4)
       (make-pane '<table-layout>
		  :columns 2
		  :x-alignment '(:right :left)
		  :y-alignment :center
		  :y-spacing 5
		  :children
		  (vector (make-pane '<label> :label "Frame type:")
			  (frame-type-pane frame)
			  (make-pane '<label> :label "Title:")
			  (frame-title-pane frame)
			  (make-pane '<label> :label "Gadgets:")
			  (frame-extras-pane frame)
			  (make-pane '<label> :label "Layout:")
			  (frame-layout-pane frame)
			  (make-pane '<label> :label "X:")
			  (frame-x-pane frame)
			  (make-pane '<label> :label "Y:")
			  (frame-y-pane frame)
			  (make-pane '<label> :label "Width:")
			  (frame-width-pane frame)
			  (make-pane '<label> :label "Height:")
			  (frame-height-pane frame)))
       (make-pane '<separator>)
       (horizontally (:spacing 4)
	 (make-frame-button frame)
	 (update-frame-button frame))))
   (:layout (frame)
     (main-layout frame))
   (:status-bar (frame)
     (make-pane '<status-bar>))))

#||
define method frame-test-make-frame (frame :: <frame-test-frame>) => ()
  let x             = gadget-value(frame.frame-x-pane);
  let y             = gadget-value(frame.frame-y-pane);
  let width         = gadget-value(frame.frame-width-pane);
  let height        = gadget-value(frame.frame-height-pane);
  let title         = gadget-value(frame.frame-title-pane);
  let class         = gadget-value(frame.frame-type-pane);
  let extras        = gadget-value(frame.frame-extras-pane);
  let layout-option = gadget-value(frame.frame-layout-pane);
  let status-bar = member?(#"status-bar", extras) & make(<status-bar>);
  let tool-bar
    = member?(#"tool-bar", extras)
        & make(<tool-bar>,
               child: horizontally ()
                        make(<button>, label: "Shortcut...");
                      end);
  let menu-bar
    = if (member?(#"menu-bar", extras))
	let menu
	  = make(<menu>, 
		 label: "Menu",
		 children: vector(make(<menu-box>, items: #(1, 2, 3))));
        make(<menu-bar>, children: vector(menu))
      end;
  let layout
    = select (layout-option)
        <button> =>
          make(<button>, label: "Press Me!");
        <tree-control> =>
          make(<tree-control>,
	       roots: #(1), 
               children-generator: method (x) 
                                    vector(x * 2, 1 + (x * 2))
                                  end);
        #f =>
          #f;
      end;
  let new-frame
    = make(class,
           owner: frame,
           title: title,
           layout: layout,
           menu-bar: menu-bar, tool-bar: tool-bar, status-bar: status-bar,
           x: x, y: y, width: width, height: height,
           destroyed-callback: method (test-frame)
                                 gadget-enabled?(frame.update-frame-button) := #f
                               end);
  frame-test-frame(frame) := new-frame;
  gadget-enabled?(frame.update-frame-button) := #t;
  start-frame(new-frame)
end method frame-test-make-frame;
||#

(defmethod frame-test-make-frame ((frame <frame-test-frame>))
  (let* ((x             (gadget-value (frame-x-pane frame)))
	 (y             (gadget-value (frame-y-pane frame)))
	 (width         (gadget-value (frame-width-pane frame)))
	 (height        (gadget-value (frame-height-pane frame)))
	 (title         (gadget-value (frame-title-pane frame)))
	 (class         (gadget-value (frame-type-pane frame)))
	 (extras        (gadget-value (frame-extras-pane frame)))
	 (layout-option (gadget-value (frame-layout-pane frame)))
	 (status-bar    (and (find :status-bar extras) (make-pane '<status-bar>)))
	 (tool-bar (and (find :tool-bar extras)
			(make-pane '<tool-bar>
				   :child
				   (horizontally ()
				     (make-pane '<button> :label "Shortcut...")))))
	 (menu-bar (if (find :menu-bar extras)
		       (let ((menu (make-pane '<menu>
					      :label "Menu"
					      :children
					      (vector (make-pane '<menu-box>
								 :items '(1 2 3))))))
			 (make-pane '<menu-bar>
				    :children (vector menu)))))
	 (layout
	  (cond ((eql layout-option (find-class '<button>))
		 (make-pane '<button> :label "Press Me!"))
		((eql layout-option (find-class '<tree-control>))
		 (make-pane '<tree-control>
			    :roots #(1)  ;; XXX: WAS: '(1) - roots might need an array instead of a sequence :/
			    :children-generator
			    #'(lambda (x)
				(vector (* x 2) (+ 1 (* x 2))))))
		(t nil)))
	 (new-frame (make-instance class
				   :owner frame
				   :title title
				   :layout layout
				   :menu-bar menu-bar
				   :tool-bar tool-bar
				   :status-bar status-bar
				   :x x :y y :width width :height height
				   :destroyed-callback
				   #'(lambda (test-frame)
				       (setf (gadget-enabled? (update-frame-button frame)) nil)))))
    (setf (frame-test-frame frame) new-frame)
    (setf (gadget-enabled? (update-frame-button frame)) t)
    (start-frame new-frame)))

#||
define method frame-test-update-frame (frame :: <frame-test-frame>) => ()
  let x             = gadget-value(frame.frame-x-pane);
  let y             = gadget-value(frame.frame-y-pane);
  let width         = gadget-value(frame.frame-width-pane);
  let height        = gadget-value(frame.frame-height-pane);
  let title         = gadget-value(frame.frame-title-pane);
  let class         = gadget-value(frame.frame-type-pane);
  let extras        = gadget-value(frame.frame-extras-pane);
  let layout-option = gadget-value(frame.frame-layout-pane);
  let test-frame = frame.frame-test-frame;
  frame-title(test-frame) := title;
  let current-layout-option
    = select (frame-layout(test-frame) by instance?)
	<button>       => <button>;
	<tree-control> => <tree-control>;
	otherwise      => #f;
      end;
  if (layout-option ~= current-layout-option)
    let layout
      = select (layout-option)
	  <button> =>
	    make(<button>, label: "Press Me!");
	  <tree-control> =>
	    make(<tree-control>,
		 roots: #(1), 
		 children-generator: method (x) 
				      vector(x * 2, 1 + (x * 2))
				    end);
	  #f =>
	    #f;
	end;
    frame-layout(test-frame) := layout;
  end;
  if (x & y)
    set-frame-position(test-frame, x, y);
  end;
  if (width & height)
    set-frame-size(test-frame, width, height)
  end;
end method frame-test-update-frame;
||#

(defmethod frame-test-update-frame ((frame <frame-test-frame>))
  (let ((x             (gadget-value (frame-x-pane frame)))
	(y             (gadget-value (frame-y-pane frame)))
	(width         (gadget-value (frame-width-pane frame)))
	(height        (gadget-value (frame-height-pane frame)))
	(title         (gadget-value (frame-title-pane frame)))
	(layout-option (gadget-value (frame-layout-pane frame)))
	(test-frame    (frame-test-frame frame)))
    (setf (frame-title test-frame) title)
    (let ((current-layout-option (typecase (frame-layout test-frame)
				   (<button> (find-class '<button>))
				   (<tree-control> (find-class '<tree-control>))
				   (otherwise nil))))
      (if (not (equal? layout-option current-layout-option))
	  (let ((layout (cond ((eql layout-option (find-class '<button>))
			       (make-pane '<button> :label "Press Me!"))
			      ((eql layout-option (find-class '<tree-control>))
			       (make-pane '<tree-control>
					  :roots #(1)   ;; XXX: WAS: '(1)
					  :children-generator
					  #'(lambda (x)
					      (vector (* x 2)
						      (+ 1 (* x 2))))))
			      (t nil))))
	    (setf (frame-layout test-frame) layout)))
      (if (and x y)
	  (set-frame-position test-frame x y))
      (if (and width height)
	  (set-frame-size test-frame width height)))))



#||
/// Multiple layout frame

define frame <multiple-layout-frame> (<simple-frame>)
  slot current-layout = #"debugging";
  pane file-menu (frame)
    make(<menu>,
	 label: "File",
	 children: vector(frame.switch-button,
			  frame.exit-button));
  pane switch-button (frame)
    make(<menu-button>,
	 label: "Switch layouts",
	 activate-callback: method (button)
			      select (frame.current-layout)
         			#"debugging" =>
         			  frame.current-layout := #"interacting";
         			  frame-layout(frame)  := frame.interacting-layout;
         			#"interacting" =>
         			  frame.current-layout := #"debugging";
         			  frame-layout(frame)  := frame.debugging-layout;
			      end
			    end);
  pane exit-button (frame)
    make(<menu-button>,
	 label: "Exit",
	 activate-callback: method (button)
			      exit-frame(sheet-frame(button))
			    end);
  pane context-pane (frame)
    make(<drawing-pane>,
	 display-function:
	   method (pane, medium, region)
	     draw-text(medium, "[Error message]", 0, 0,
		       align-x: #"left", align-y: #"top")
	   end);
  pane stack-pane (frame)
    make(<drawing-pane>,
	 display-function:
	   method (pane, medium, region)
	     draw-text(medium, "[Stack trace]", 0, 0,
		       align-x: #"left", align-y: #"top")
	   end);
  pane source-pane (frame)
    make(<drawing-pane>,
	 display-function:
	   method (pane, medium, region)
	     draw-text(medium, "[Source code]", 0, 0,
		       align-x: #"left", align-y: #"top")
	   end);
  pane interactor-pane (frame)
    make(<drawing-pane>,
	 display-function:
	   method (pane, medium, region)
	     draw-text(medium, "[Interactor]", 0, 0,
		       align-x: #"left", align-y: #"top")
	   end,
         height: $fill);
  pane message-pane (frame)
    make(<drawing-pane>,
	 display-function:
	   method (pane, medium, region)
	     draw-text(medium, "[Other messages]", 0, 0,
		       align-x: #"left", align-y: #"top")
	   end);
  pane debugging-layout (frame)
    vertically ()
      with-border (type: #"sunken") frame.context-pane end;
      horizontally ()
	with-border (type: #"sunken") frame.stack-pane end;
	with-border (type: #"sunken") frame.source-pane end;
      end;
      with-border (type: #"sunken") frame.interactor-pane end;
      with-border (type: #"sunken") frame.message-pane end;
    end;
  pane interacting-layout (frame)
    vertically ()
      with-border (type: #"sunken") frame.interactor-pane end;
      with-border (type: #"sunken") frame.message-pane end;
    end;
  layout (frame)
    frame.debugging-layout;
  menu-bar (frame) 
    make(<menu-bar>,
	 children: vector(frame.file-menu));
  keyword width:  = 300;
  keyword height: = 400;
end frame <multiple-layout-frame>;
||#

(define-frame <multiple-layout-frame> (<simple-frame>)
  ((current-layout :initform :debugging :accessor current-layout)
   (:pane file-menu (frame)
     (make-pane '<menu>
		:label "File"
		:children
		(vector (switch-button frame)
			(exit-button frame))))
   (:pane switch-button (frame)
     (make-pane '<menu-button>
		:label "Switch layouts"
		:activate-callback
		#'(lambda (button)
		    (declare (ignored button))
		    (ecase (current-layout frame)
		      (:debugging
		       (setf (current-layout frame) :interacting)
		       (setf (frame-layout frame) (interacting-layout frame)))
		      (:interacting
		       (setf (current-layout frame) :debugging)
		       (setf (frame-layout frame) (debugging-layout frame)))))))
   (:pane exit-button (frame)
     (make-pane '<menu-button>
		:label "Exit"
		:activate-callback
		#'(lambda (button)
		    (exit-frame (sheet-frame button)))))
   (:pane context-pane (frame)
     (make-pane '<drawing-pane>
		:display-function
		#'(lambda (pane medium region)
		    (declare (ignored pane region))
		    (draw-text medium "[Error message]"
			       0 0
			       :align-x :left
			       :align-y :top))))
   (:pane stack-pane (frame)
     (make-pane '<drawing-pane>
		:display-function
		#'(lambda (pane medium region)
		    (declare (ignored pane region))
		    (draw-text medium "[Stack trace]"
			       0 0
			       :align-x :left
			       :align-y :top))))
   (:pane source-pane (frame)
     (make-pane '<drawing-pane>
		:display-function
		#'(lambda (pane medium region)
		    (declare (ignored pane region))
		    (draw-text medium "[Source code]"
			       0 0
			       :align-x :left
			       :align-y :top))))
   (:pane interactor-pane (frame)
     (make-pane '<drawing-pane>
		:display-function
		#'(lambda (pane medium region)
		    (declare (ignored pane region))
		    (draw-text medium "[Interactor]"
			       0 0
			       :align-x :left
			       :align-y :top))
		:height +fill+))
   (:pane message-pane (frame)
     (make-pane '<drawing-pane>
		:display-function
		#'(lambda (pane medium region)
		    (declare (ignored pane region))
		    (draw-text medium "[Other messages]"
			       0 0
			       :align-x :left
			       :align-y :top))))
   (:pane debugging-layout (frame)
     (vertically ()
       (with-border (:type :sunken) (context-pane frame))
       (horizontally ()
	 (with-border (:type :sunken) (stack-pane frame))
	 (with-border (:type :sunken) (source-pane frame)))
       (with-border (:type :sunken) (interactor-pane frame))
       (with-border (:type :sunken) (message-pane frame))))
   (:pane interacting-layout (frame)
     (vertically ()
       (with-border (:type :sunken) (interactor-pane frame))
       (with-border (:type :sunken) (message-pane frame))))
   (:layout (frame)
     (debugging-layout frame))
   (:menu-bar (frame)
     (make-pane '<menu-bar>
		:children
		(vector (file-menu frame)))))
  (:default-initargs :width 300 :height 400))


#||
/// Install the tests

install-test(<frame-test-frame>, "Frames");
install-test(<multiple-layout-frame>, "Multiple layout frame");
||#

(install-test (find-class '<frame-test-frame>) "Frames")
(install-test (find-class '<multiple-layout-frame>) "Multiple layout frame")
