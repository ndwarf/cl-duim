;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GUI-TEST-SUITE -*-
(in-package #:gui-test-suite)

#||
Module:    duim-gui-test-suite
Author:    Andy Armstrong
Synopsis:  DUIM example code

// Multiple threads not supported in GTK yet...
*use-threads?* := #f;
||#

(setf *use-threads?* nil)
