;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GUI-TEST-SUITE -*-
(in-package #:gui-test-suite)

#||
Module:       duim-gui-test-suite
Author:       Andy Armstrong
Synopsis:     DUIM example code

/// Clipboard test

define frame <clipboard-test-frame> (<simple-frame>)
  pane clipboard-text-field (frame)
    make(<text-field>);
  pane main-layout (frame)
    vertically ()
      frame.clipboard-text-field
    end;
  pane file-menu (frame)
    make(<menu>,
	 label: "File",
	 children: vector(frame.exit-button));
  pane exit-button (frame)
    make(<menu-button>,
	 label: "Exit",
	 accelerator: make(<gesture>, keysym: #"z", modifiers: #[#"control"]),
	 activate-callback: method (button)
			      exit-frame(sheet-frame(button))
			    end);
  pane edit-menu (frame)
    make(<menu>,
	 label: "Edit",
	 children: vector(frame.cut-menu-button, 
			  frame.copy-menu-button,
			  frame.paste-menu-button));
  pane cut-menu-button (frame)
    make(<menu-button>,
	 label: "Cut",
	 accelerator: make(<gesture>, keysym: #"x", modifiers: #[#"control"]),
	 activate-callback: method (button)
			      frame-cut(sheet-frame(button))
			    end);
  pane copy-menu-button (frame)
    make(<menu-button>,
	 label: "Copy",
	 accelerator: make(<gesture>, keysym: #"c", modifiers: #[#"control"]),
	 activate-callback: method (button)
			      frame-copy(sheet-frame(button))
			    end);
  pane paste-menu-button (frame)
    make(<menu-button>,
	 label: "Paste",
	 accelerator: make(<gesture>, keysym: #"v", modifiers: #[#"control"]),
	 activate-callback: method (button)
			      frame-paste(sheet-frame(button))
			    end);

  pane cut-button (frame)
    make(<push-button>,
	 label: "X",
	 activate-callback: method (button)
			      frame-cut(sheet-frame(button))
			    end);
  pane copy-button (frame)
    make(<push-button>,
	 label: "C",
	 activate-callback: method (button)
			      frame-copy(sheet-frame(button))
			    end);
  pane paste-button (frame)
    make(<push-button>,
	 label: "P",
	 activate-callback: method (button)
			      frame-paste(sheet-frame(button))
			    end);
  layout (frame) frame.main-layout;
  menu-bar (frame) 
    make(<menu-bar>,
	 children: vector(frame.file-menu, frame.edit-menu));
  tool-bar (frame)
    make(<tool-bar>,
         child: make(<row-layout>,
		     children: vector(frame.cut-button,
				      frame.copy-button,
				      frame.paste-button)));
end frame <clipboard-test-frame>;
||#

(define-frame <clipboard-test-frame> (<simple-frame>)
  ((:pane clipboard-text-field (frame)
     (make-pane '<text-field>))
   (:pane main-layout (frame)
     (vertically ()
       (clipboard-text-field frame)))
   (:pane file-menu (frame)
     (make-pane '<menu>
		:label "File"
		:children (vector (exit-button frame))))
   (:pane exit-button (frame)
     (make-pane '<menu-button>
		:label "Exit"
		:accelerator (make-gesture :keysym :z :modifiers #(:control))
		:activate-callback #'(lambda (button)
				       (exit-frame (sheet-frame button)))))
   (:pane edit-menu (frame)
     (make-pane '<menu>
		:label "Edit"
		:children (vector (cut-menu-button frame)
				  (copy-menu-button frame)
				  (paste-menu-button frame))))
   (:pane cut-menu-button (frame)
     (make-pane '<menu-button>
		:label "Cut"
		:accelerator (make-gesture :keysym :x :modifiers #(:control))
		:activate-callback #'(lambda (button)
				       (frame-cut (sheet-frame button)))))
   (:pane copy-menu-button (frame)
     (make-pane '<menu-button>
		:label "Copy"
		:accelerator (make-gesture :keysym :c :modifiers #(:control))
		:activate-callback #'(lambda (button)
				       (frame-copy (sheet-frame button)))))
   (:pane paste-menu-button (frame)
     (make-pane '<menu-button>
		:label "Paste"
		:accelerator (make-gesture :keysym :v :modifiers #(:control))
		:activate-callback #'(lambda (button)
				       (frame-paste (sheet-frame button)))))
   (:pane cut-button (frame)
     (make-pane '<push-button>
		:label "X"
		:activate-callback
		#'(lambda (button)
		    (frame-cut (sheet-frame button)))))
   (:pane copy-button (frame)
     (make-pane '<push-button>
		:label "C"
		:activate-callback
		#'(lambda (button)
		    (frame-copy (sheet-frame button)))))
   (:pane paste-button (frame)
     (make-pane '<push-button>
		:label "P"
		:activate-callback
		#'(lambda (button)
		    (frame-paste (sheet-frame button)))))
   (:layout (frame)
     (main-layout frame))
   (:menu-bar (frame)
     (make-pane '<menu-bar>
		:children (vector (file-menu frame) (edit-menu frame))))
   (:tool-bar (frame)
     (make-pane '<tool-bar>
		:child (make-pane '<row-layout>
				  :children
				  (vector (cut-button frame)
					  (copy-button frame)
					  (paste-button frame)))))))

#||
define method frame-cut
    (frame :: <clipboard-test-frame>) => (success? :: <boolean>)
  let gadget = clipboard-text-field(frame);
  if (frame-copy(frame))
    gadget-value(gadget) := "";
    #t
  end
end method frame-cut;
||#

(defmethod frame-cut ((frame <clipboard-test-frame>))
  (let ((gadget (clipboard-text-field frame)))
    (when (frame-copy frame)
      (setf (gadget-value gadget) "")
      t)))

#||
define method frame-copy 
    (frame :: <clipboard-test-frame>) => (success? :: <boolean>)
  let gadget = clipboard-text-field(frame);
  with-clipboard (clipboard = gadget)
    if (clipboard)
      let text = selected-text(gadget) | gadget-value(gadget);
      if (add-clipboard-data(clipboard, text))
	#t
      else
	notify-user("Failed to put text onto clipboard", owner: gadget)
      end
    else
      notify-user("Clipboard not available", owner: gadget)
    end
  end
end method frame-copy;
||#

(defmethod frame-copy ((frame <clipboard-test-frame>))
  (let ((gadget (clipboard-text-field frame)))
    (with-clipboard (clipboard = gadget)
      (if clipboard
	  (let ((text (or (selected-text gadget) (gadget-value gadget))))
	    (if (add-clipboard-data clipboard text)
		t
		(notify-user "Failed to put text onto clipboard" :owner gadget)))
	  (notify-user "Clipboard not available" :owner gadget)))))

#||
define method frame-paste 
    (frame :: <clipboard-test-frame>) => (success? :: <boolean>)
  let gadget = clipboard-text-field(frame);
  with-clipboard (clipboard = gadget)
    if (clipboard)
      let text = get-clipboard-data-as(<string>, clipboard);
      if (text)
	gadget-value(gadget) := text;
	#t
      else
	notify-user("No text on clipboard", owner: gadget)
      end
    else
      notify-user("Clipboard not available", owner: gadget)
    end
  end
end method frame-paste;
||#

(defmethod frame-paste ((frame <clipboard-test-frame>))
  (let ((gadget (clipboard-text-field frame)))
    (with-clipboard (clipboard = gadget)
      (if clipboard
	  (let ((text (get-clipboard-data-as (find-class 'string) clipboard)))
	    (if text
		(progn
		  (setf (gadget-value gadget) text)
		  t)
		(notify-user "No text on clipboard" :owner gadget)))
	  (notify-user "Clipboard not available" :owner gadget)))))


#||
/// Install the test
install-test(<clipboard-test-frame>, "Clipboard");
||#

(install-test (find-class '<clipboard-test-frame>) "Clipboard")
