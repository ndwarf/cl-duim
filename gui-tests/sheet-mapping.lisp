;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GUI-TEST-SUITE -*-
(in-package #:gui-test-suite)

#||
Module:       duim-gui-test-suite
Author:       Andy Armstrong
Synopsis:     DUIM example code

/// Sheet mapping test

define constant $initial-selection = #[1];
||#

;; FIXME: really we should allow the user to give any sequence for
;; selections and what have you, and write appropriate methods that
;; convert into adjustable vectors as appropriate.

(defparameter *initial-selection* (make-array 1 :adjustable t
					      :fill-pointer t
					      :initial-element 1))

#||
define frame <sheet-mapping-frame> (<simple-frame>)
  pane mapping-buttons (frame)
    make(<check-box>,
         items: #("One", "Two", "Three", "Four", "Five"),
         selection: $initial-selection,
         value-changed-callback: method (gadget)
                                   update-sheet-mapping(sheet-frame(gadget))
                                 end);
  pane sheet-one (frame)
    make(<radio-box>, items: #(1, 2, 3),
         x:  50,   y: 50, withdrawn?: #t);
  pane sheet-two (frame)
    make(<text-field>, text: "Sheet Two",
         x: 250,  y: 50, withdrawn?: #f);
  pane sheet-three (frame)
    make(<label>, label: "Sheet Three",
         x:  50,  y: 150,  withdrawn?: #t);
  pane sheet-four (frame)
    make(<progress-bar>, value: 10, value-range: range(from: 0, to: 100),
         x: 250,  y: 150,  withdrawn?: #t);
  pane sheet-five (frame)
    make(<push-button>, label: "Sheet Five",
         x:  50,  y: 250,  withdrawn?: #t);
  pane pinboard (frame)
    make(<pinboard-layout>,
         width: 400, height: 300,
         children: vector(frame.sheet-one,
                          frame.sheet-two,
                          frame.sheet-three,
                          frame.sheet-four,
                          frame.sheet-five));
  pane main-layout (frame)
    vertically (spacing: 5, x-alignment: #"centre")
      frame.mapping-buttons;
      with-border (type: #"sunken")
        frame.pinboard
      end;
    end;
  layout (frame) frame.main-layout;
end frame <sheet-mapping-frame>;
||#

(define-frame <sheet-mapping-frame> (<simple-frame>)
  ((:pane mapping-buttons (frame)
     (make-pane '<check-box>
		:items '("One" "Two" "Three" "Four" "Five")
		:selection *initial-selection*
		:value-changed-callback
		#'(lambda (gadget)
		    (update-sheet-mapping (sheet-frame gadget)))))
   (:pane sheet-one (frame)
     (make-pane '<radio-box>
		:items '(1 2 3)
		:x 50 :y 50
		:withdrawn? t))
   (:pane sheet-two (frame)
     (make-pane '<text-field>
		:text "Sheet Two"
		:x 250 :y 50
		:withdrawn? nil))
   (:pane sheet-three (frame)
     (make-pane '<label>
		:label "Sheet Three"
		:x 50 :y 150
		:withdrawn? t))
   (:pane sheet-four (frame)
     (make-pane '<progress-bar>
		:value 10
		:value-range (range :from 0 :to 100)
		:x 250 :y 150
		:withdrawn? t))
   (:pane sheet-five (frame)
     (make-pane '<push-button>
		:label "Sheet Five"
		:x 50 :y 250
		:withdrawn? t))
   (:pane pinboard (frame)
     (make-pane '<pinboard-layout>
		:width 400 :height 300
		:children (vector (sheet-one frame)
				  (sheet-two frame)
				  (sheet-three frame)
				  (sheet-four frame)
				  (sheet-five frame))))
   (:pane main-layout (frame)
     (vertically (:spacing 5 :x-alignment :center)
       (mapping-buttons frame)
       (with-border (:type :sunken)
	 (pinboard frame))))
   (:layout (frame) (main-layout frame))))

#||
define method update-sheet-mapping
    (frame :: <sheet-mapping-frame>) => ()
  let buttons = frame.mapping-buttons;
  let pinboard = frame.pinboard;
  let selection = gadget-selection(buttons);
  for (child in sheet-children(pinboard),
       index from 0)
    let mapped? = member?(index, selection);
    sheet-withdrawn?(child) := ~mapped?;
  end;
  relayout-children(pinboard);
  for (child in sheet-children(pinboard),
       index from 0)
    let mapped? = member?(index, selection);
    if (mapped?)
      sheet-mapped?(child) := #t
    end;
  end;
end method update-sheet-mapping;
||#

(defmethod update-sheet-mapping ((frame <sheet-mapping-frame>))
  (let* ((buttons (mapping-buttons frame))
	 (pinboard (pinboard frame))
	 (selection (gadget-selection buttons)))
    (loop for child across (sheet-children pinboard)
	  for index from 0
	  do (let ((mapped? (find index selection)))
	       (setf (sheet-withdrawn? child) (not mapped?))))
    (relayout-children pinboard)
    (loop for child across (sheet-children pinboard)
	  for index from 0
	  do (let ((mapped? (find index selection)))
	       (if mapped?
		   (setf (sheet-mapped? child) t))))))

#||
install-test(<sheet-mapping-frame>, "Sheet mapping");
||#

(install-test (find-class '<sheet-mapping-frame>) "Sheet mapping")
