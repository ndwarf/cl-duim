;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GUI-TEST-SUITE -*-
(in-package #:gui-test-suite)

#||
Module:       duim-gui-test-suite
Author:       Andy Armstrong
Synopsis:     DUIM example code

/// Keyboard handling

define class <keyboard-pane> (<drawing-pane>)
end class <keyboard-pane>;
||#

(defclass <keyboard-pane> (<drawing-pane>) ())

#||
define method handle-event
    (pane :: <keyboard-pane>, event :: <key-press-event>) => ()
  let frame = sheet-frame(pane);
  if (frame.pending-key-presses = 0)
    clear-events(frame)
  end;
  frame.pending-key-presses := frame.pending-key-presses + 1;
  let character = event-character(event);
  record-event(frame, 
	       format-to-string("Pressed key %= %s, %s",
				event-key-name(event),
				if (character)
				  format-to-string("(char %=)", character)
				else
				  format-to-string("(no char)", character)
				end,
				modifier-names(event-modifier-state(event))))
end method handle-event;
||#

(defmethod handle-event ((pane <keyboard-pane>) (event <key-press-event>))
  (let ((frame (sheet-frame pane)))
    (if (zerop (pending-key-presses frame))
	(clear-events frame))
    (setf (pending-key-presses frame) (+ (pending-key-presses frame) 1))
    (let ((character (event-character event)))
      (record-event frame
		    (format nil "Pressed key ~a ~a, ~a"
			    (event-key-name event)
			    (if character
				(format nil "(char ~a)" character)
				(format nil "(no char)"))
			    (modifier-names (event-modifier-state event)))))))


#||
define method handle-event
    (pane :: <keyboard-pane>, event :: <key-release-event>) => ()
  let frame = sheet-frame(pane);
  frame.pending-key-presses := frame.pending-key-presses - 1;
  record-event(frame,
               format-to-string("Released key %=, %s",
				event-key-name(event),
				modifier-names(event-modifier-state(event))))
end method handle-event;
||#

(defmethod handle-event ((pane <keyboard-pane>) (event <key-release-event>))
  (let ((frame (sheet-frame pane)))
    (setf (pending-key-presses frame) (- (pending-key-presses frame) 1))
    (record-event frame
		  (format nil "Released key ~a, ~a"
			  (event-key-name event)
			  (modifier-names (event-modifier-state event))))))


#||
define method modifier-names
    (modifier :: <integer>) => (names :: <string>)
  let shift?   = ~zero?(logand(modifier, $shift-key));
  let control? = ~zero?(logand(modifier, $control-key));
  let alt?     = ~zero?(logand(modifier, $alt-key));
  if (shift? | control? | alt?)
    format-to-string("modifiers are: %s%s%s",
		     if (alt?) "alt " else "" end,
		     if (shift?) "shift " else "" end,
		     if (control?) "control " else "" end)
  else
    "no modifiers"
  end
end method modifier-names;
||#

(defmethod modifier-names ((modifier integer))
  (let ((shift?   (not (zerop (logand modifier +shift-key+))))
	(control? (not (zerop (logand modifier +control-key+))))
	(alt?     (not (zerop (logand modifier +alt-key+)))))
    ;; super / hyper / meta?
    (if (or shift? control? alt?)
	(format nil "modifiers are: ~a~a~a"
		(if alt? "alt " "")
		(if shift? "shift " "")
		(if control? "control " ""))
	;; else
	"no modifiers")))


#||
define method handle-event
    (pane :: <keyboard-pane>, event :: <button-press-event>) => ()
  let frame = sheet-frame(pane);
  frame.pending-key-presses := 0;
  clear-events(frame)
end method handle-event;
||#

(defmethod handle-event ((pane <keyboard-pane>) (event <button-press-event>))
  (let ((frame (sheet-frame pane)))
    (setf (pending-key-presses frame) 0)
    (clear-events frame)))


#||
define frame <keyboard-handling-frame> (<simple-frame>)
  slot pending-key-presses :: <integer> = 0;
  pane keyboard-pane (frame)
    make(<keyboard-pane>, caret: #t);
  pane events-pane (frame)
    make(<list-control>, lines: 12);
  pane main-layout (frame)
    vertically (width: 400, height: 500)
      make(<label>, label: "Press some keys:");
      with-border (type: #"sunken")
        frame.keyboard-pane
      end;
      frame.events-pane
    end;
  layout (frame) frame.main-layout;
  status-bar (frame) make(<status-bar>);
  keyword alt-key-is-meta?: = #t;
end frame <keyboard-handling-frame>;
||#

(define-frame <keyboard-handling-frame> (<simple-frame>)
  ((pending-key-presses :type integer :initform 0 :accessor pending-key-presses)
   (:pane keyboard-pane (frame)
     (make-pane '<keyboard-pane> :caret t))
   (:pane events-pane (frame)
     (make-pane '<list-control> :lines 12))
   (:pane main-layout (frame)
     (vertically (:width 400 :height 500)
       (make-pane '<label> :label "Press some keys:")
       (with-border (:type :sunken)
	 (keyboard-pane frame))
       (events-pane frame)))
   (:layout (frame)
     (main-layout frame))
   (:status-bar (frame)
     (make-pane '<status-bar>)))
  (:default-initargs :alt-key-is-meta? t))


#||
define method initialize
    (frame :: <keyboard-handling-frame>, #key) => ()
  next-method();
  frame-input-focus(frame) := frame.keyboard-pane
end method initialize;
||#

(defmethod initialize-instance :after ((frame <keyboard-handling-frame>) &key
				       &allow-other-keys)
  (setf (frame-input-focus frame) (keyboard-pane frame)))


#||
define method clear-events
    (frame :: <keyboard-handling-frame>) => ()
  gadget-items(frame.events-pane) := #[]
end method clear-events;
||#

(defmethod clear-events ((frame <keyboard-handling-frame>))
  (setf (gadget-items (events-pane frame)) #()))


#||
define method record-event
    (frame :: <keyboard-handling-frame>, message :: <string>) => ()
  let events-pane = frame.events-pane;
  gadget-items(events-pane)
    := concatenate-as(<vector>, gadget-items(events-pane), vector(message))
end method record-event;
||#

(defmethod record-event ((frame <keyboard-handling-frame>) (message string))
  (let ((events-pane (events-pane frame)))
    (setf (gadget-items events-pane)
	  (concatenate 'vector
		       (gadget-items events-pane)
		       (vector message)))))


#||
install-test(<keyboard-handling-frame>, "Keyboard");
||#

(install-test (find-class '<keyboard-handling-frame>) "Keyboard")
