;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GUI-TEST-SUITE -*-
(in-package #:gui-test-suite)

#||
Module:       duim-gui-test-suite
Author:       Andy Armstrong
Synopsis:     DUIM testing code

/// Cursors

define frame <cursor-test-frame> (<simple-frame>)
  pane drawing-pane (frame)
    make(<drawing-pane>,		// mirrored
	 cursor: #"i-beam");
  pane simple-pane (frame)
    make(<simple-pane>,			// unmirrored
	 cursor: #"cross",
	 display-function: ignore);
  pane button (frame)
    make(<push-button>,
	 label: "Get Busy...",
	 activate-callback: test-busy-cursor);
  pane background-button (frame)
    make(<push-button>,
	 label: "Background it...",
	 activate-callback: test-background-busy-cursor);
  layout (frame)
    vertically (y-spacing: 4)
      horizontally (min-width: 400)
        with-border (type: #"sunken")
          frame.drawing-pane
	end;
        with-border (type: #"sunken")
          frame.simple-pane
	end;
      end;
      horizontally (spacing: 4)
        frame.button;
        frame.background-button;
      end
    end;
  status-bar (frame)
    make(<status-bar>, progress-bar?: #t);
end frame <cursor-test-frame>; 
||#

(define-frame <cursor-test-frame> (<simple-frame>)
  ((:pane drawing-pane (frame)
     (make-pane '<drawing-pane>              ;; mirrored
		:cursor :i-beam))
   (:pane simple-pane (frame)
     (make-pane '<simple-pane>               ;; unmirrored
		:cursor :cross
		:display-function (constantly nil))) ;;#'ignore))
   (:pane button (frame)
     (make-pane '<push-button>
		:label "Get Busy..."
		:activate-callback #'test-busy-cursor))
   (:pane background-button (frame)
     (make-pane '<push-button>
		:label "Background it..."
		:activate-callback #'test-background-busy-cursor))
   (:layout (frame)
     (vertically (:y-spacing 4)
       (horizontally (:min-width 400)
	 (with-border (:type :sunken)
	   (drawing-pane frame))
	 (with-border (:type :sunken)
	   (simple-pane frame)))
       (horizontally (:spacing 4)
	 (button frame)
	 (background-button frame))))
   (:status-bar (frame)
     (make-pane '<status-bar> :progress-bar? t))))

#||
define method note-operation-progress 
    (frame :: <cursor-test-frame>, value :: <integer>, max :: <integer>) => ()
  note-progress(value, max,
		label: format-to-string("%d%% of the way through",
					floor/(value * 100, max)))
end method note-operation-progress;
||#

(defmethod note-operation-progress ((frame <cursor-test-frame>) (value integer) (max integer))
  (note-progress value max
		 :label (format nil "~d% of the way through"
				(floor (* value 100) max))))

#||
define method test-busy-cursor (sheet :: <sheet>) => ()
  let frame = sheet-frame(sheet);
  with-busy-cursor (frame)
    noting-progress (frame, "Test")
      for (i from 0 to 100)
	note-operation-progress(frame, i, 100);
	sleep(0.1)
      end
    end
  end
end method test-busy-cursor;
||#

(defmethod test-busy-cursor ((sheet <sheet>))
  (let ((frame (sheet-frame sheet)))
    (with-busy-cursor (frame)
      (noting-progress (frame "Test")
        (loop for i from 0 to 100
	      do (note-operation-progress frame i 100)
	      do (sleep 0.1))))))

#||
define method test-background-busy-cursor (sheet :: <sheet>) => ()
  let frame = sheet-frame(sheet);
  make(<thread>,
       function: 
	 method ()
	   with-background-cursor (frame)
	     noting-progress (frame, "Test")
	       for (i from 0 to 100)
		 note-operation-progress(frame, i, 100);
		 sleep(0.1)
	       end
	     end
	   end
	 end)
end method test-background-busy-cursor;
||#

(defmethod test-background-busy-cursor ((sheet <sheet>))
  (let ((frame (sheet-frame sheet)))
    (bordeaux-threads:make-thread #'(lambda ()
				      (with-background-cursor (frame)
					(noting-progress (frame "Test")
							 (loop for i from 0 to 100
							    do (note-operation-progress frame i 100)
							    do (sleep 0.1))))))))

#||
/// Install the test

install-test(<cursor-test-frame>, "Cursors");
||#

(install-test (find-class '<cursor-test-frame>) "Cursors")
