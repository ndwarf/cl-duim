;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GUI-TEST-SUITE -*-
(in-package #:gui-test-suite)

#||
Module:       duim-gui-test-suite
Author:       Andy Armstrong
Synopsis:     An interactive test-suite for DUIM objects

/// Color tests

define frame <color-test-frame> (<simple-frame>)
  pane red-ellipse (frame)
    grouping ("Red Ellipse")
      make(<ellipse-pane>, foreground: $red)
    end;
  pane green-ellipse (frame)
    grouping ("Green On Blue Ellipse")
      make(<ellipse-pane>, foreground: $green, background: $blue)
    end;
  pane red-button (frame)
    grouping ("Red Button")
      make(<push-button>, label: "Hello", foreground: $red)
    end;
  layout (frame)
    vertically (spacing: 4)
      frame.red-ellipse;
      frame.green-ellipse;
      frame.red-button;
    end;
end frame <color-test-frame>;
||#

(define-frame <color-test-frame> (<simple-frame>)
  ((:pane red-ellipse (frame)
     (grouping ("Red Ellipse")
       (make-pane '<ellipse-pane> :foreground *red*)))
   (:pane green-ellipse (frame)
     (grouping ("Green On Blue Ellipse")
       (make-pane '<ellipse-pane> :foreground *green* :background *blue*)))
   (:pane red-button (frame)
     (grouping ("Red Button")
       (make-pane '<push-button> :label "Hello" :foreground *red*)))
   (:layout (frame)
     (vertically (:spacing 4)
       (red-ellipse frame)
       (green-ellipse frame)
       (red-button frame)))))

#||
install-test(<color-test-frame>, "Colors");
||#

(install-test (find-class '<color-test-frame>) "Colors")
