;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-TESTS -*-
(in-package #:duim-tests)

(defclass <test-port> (<basic-port>) ())

#||
define sideways method class-for-make-port
    (type == #"test", #rest initargs, #key)
 => (class :: <class>, initargs :: false-or(<sequence>))
  ignore(initargs);
  values(<test-port>, #f)
end method class-for-make-port;
||#

(defmethod class-for-make-port ((type (eql :test)) &rest initargs &key)
  (declare (ignore initargs))
  (values (find-class '<test-port>) nil))


#||
define method port-type (port :: <test-port>) => (type :: <symbol>)
  #"test"
end method port-type;
||#

(defmethod port-type ((port <test-port>))
  :test)


#||
define method port-name (port :: <test-port>) => (name :: false-or(<string>))
  #f
end method port-name;
||#

(defmethod port-name ((port <test-port>))
  nil)


#||
define variable *test-port* = #f;

define method find-test-port ()
  *test-port* | (*test-port* := find-port(server-path: vector(#"test")))
end method find-test-port;
||#

(defparameter *test-port* nil)

(defmethod find-test-port ()
  (or *test-port* (setf *test-port* (find-port :server-path '(:test)))))


#||
define method find-test-pointer ()
  port-pointer(find-test-port())
end method find-test-pointer;
||#

(defmethod find-test-pointer ()
  (port-pointer (find-test-port)))


#||
define method find-test-frame-manager ()
  find-frame-manager(port: find-test-port())
end method find-test-frame-manager;
||#

(defmethod find-test-frame-manager ()
  (find-frame-manager :port (find-test-port)))


#||
define class <test-palette> (<basic-palette>)
end class <test-palette>;
||#

(defclass <test-palette> (<basic-palette>) ())


#||
define method make-palette
    (port :: <test-port>, #key color?, dynamic?) => (palette :: <palette>)
  make(<test-palette>, port: port, color?: color?, dynamic?: dynamic?)
end method make-palette;
||#

(defmethod make-palette ((port <test-port>) &key color? dynamic?)
  (make-instance (find-class '<test-palette>)
		 :port port
		 :color? color?
		 :dynamic? dynamic?))


#||
define method do-pointer-position
    (port :: <test-port>, pointer :: <pointer>, sheet :: <sheet>)
 => (x :: <integer>, y :: <integer>)
  let (px, py) = pointer-position(pointer);
  values(px, py)
end method do-pointer-position;
||#

(defmethod do-pointer-position ((port <test-port>) (pointer <pointer>) (sheet <sheet>))
  (multiple-value-bind (px py)
      (pointer-position pointer)
    (values px py)))


#||
define method do-set-pointer-position
    (port :: <test-port>, pointer :: <pointer>, sheet :: <sheet>,
     x :: <integer>, y :: <integer>) => ()
end method do-set-pointer-position;
||#

(defmethod do-set-pointer-position ((port <test-port>) (pointer <pointer>) (sheet <sheet>)
                                    (x integer) (y integer))
  nil)


#||
define method do-set-pointer-cursor
    (port :: <test-port>, pointer :: <pointer>, cursor :: <cursor>) => ()
  cursor
end method do-set-pointer-cursor;
||#

(defmethod do-set-pointer-cursor ((port <test-port>) (pointer <pointer>) cursor)
  (check-type cursor <cursor>)
  cursor)


#||
define method do-set-sheet-cursor
    (port :: <test-port>, sheet :: <sheet>, cursor :: <cursor>) => ()
  cursor
end method do-set-sheet-cursor;
||#

(defmethod do-set-sheet-cursor ((port <test-port>) (sheet <sheet>) cursor)
  (check-type cursor <cursor>)
  cursor)


#||

/// Test Display

define method initialize-display 
    (port :: <test-port>, _display :: <display>) => ()
  let region = make-bounding-box(0, 0, 1000, 800);
  sheet-region(_display) := region;
  sheet-direct-mirror(_display) := make(<test-mirror>,
					sheet: _display,
					region: region);
end method initialize-display;
||#

(defmethod initialize-display ((port <test-port>) (_display <display>))
  (let ((result
	 (let ((region (make-bounding-box 0 0 1000 800)))
	   (setf (sheet-region _display) region)
	   (setf (sheet-direct-mirror _display)
		 (make-instance (find-class '<test-mirror>)
				:sheet _display
				:region region)))))
    result))


#||

/// Test Medium

define class <test-medium> (<basic-medium>)
  constant slot graphic-operations = make(<stretchy-vector>);
end class <test-medium>;
||#

(defclass <test-medium>
    (<basic-medium>)
  ((graphic-operations :initform (make-array 0 :adjustable t :fill-pointer t)
		       :reader graphic-operations)))


#||
define method make-medium
    (_port :: <test-port>, sheet :: <sheet>) => (medium :: <test-medium>)
  make(<test-medium>,
       port: _port, sheet: sheet)
end method make-medium;
||#

(defmethod make-medium ((_port <test-port>) (sheet <sheet>))
  (make-instance (find-class '<test-medium>)
                 :port _port
                 :sheet sheet))


#||
define method destroy-medium (medium :: <test-medium>) => ()
  //--- deallocate all window system resources
  next-method()
end method destroy-medium;
||#

(defmethod destroy-medium ((medium <test-medium>))
  (call-next-method))


#||
define method clear-graphic-operations (medium :: <test-medium>)
  graphic-operations(medium).size := 0;
end method clear-graphic-operations;
||#

(defmethod clear-graphic-operations ((medium <test-medium>))
  (setf (fill-pointer (graphic-operations medium)) 0))


#||

/// Pixmaps

define class <test-pixmap> (<pixmap>)
end class <test-pixmap>;
||#

(defclass <test-pixmap> (<pixmap>) ())


#||
define method do-make-pixmap 
    (port :: <test-port>, medium :: <test-medium>, width, height)
 => (pixmap :: <test-pixmap>)
  make(<test-pixmap>, width: width, height: height)
end method do-make-pixmap;
||#

(defmethod do-make-pixmap ((port <test-port>) (medium <test-medium>) width height)
  (declare (ignore width height))
  (make-instance (find-class '<test-pixmap>)))


#||
define method glyph-for-character
    (_port :: <test-port>, char :: <character>, text-style :: <text-style>,
     #key font)
 => (index :: <integer>, font,
     escapement-x :: <real>, escapement-y :: <real>,
     origin-x :: <real>, origin-y :: <real>, bb-x :: <real>, bb-y :: <real>)
  ignore(font);
  let index = as(<integer>, char);
  values(index, text-style,
	 8, 0, 0, 10, 8, 12)
end method glyph-for-character;
||#

(defmethod glyph-for-character ((_port <test-port>) (char character) (text-style <text-style>) &key font)
  (declare (ignore font))
  (let ((index (char-code char)))
    (values index text-style 8 0 0 10 8 12)))


#||
define method font-width
    (text-style :: <text-style>, _port :: <test-port>, #key character-set)
 => (width :: <integer>)
  ignore(character-set);
  10
end method font-width;
||#

(defmethod font-width ((text-style <text-style>) (_port <test-port>) &key character-set)
  (declare (ignore character-set))
  10)


#||
define method font-height
    (text-style :: <text-style>, _port :: <test-port>, #key character-set)
 => (height :: <integer>)
  ignore(character-set);
  14
end method font-height;
||#

(defmethod font-height ((text-style <text-style>) (_port <test-port>) &key character-set)
  (declare (ignore character-set))
  14)


#||
define method font-ascent
    (text-style :: <text-style>, _port :: <test-port>, #key character-set)
 => (ascent :: <integer>)
  ignore(character-set);
 12
end method font-ascent;
||#

(defmethod font-ascent ((text-style <text-style>) (_port <test-port>) &key character-set)
  (declare (ignore character-set))
  12)


#||
define method font-descent
    (text-style :: <text-style>, _port :: <test-port>, #key character-set)
 => (descent :: <integer>)
  ignore(character-set);
  2
end method font-descent;
||#

(defmethod font-descent ((text-style <text-style>) (_port <test-port>) &key character-set)
  (declare (ignore character-set))
  2)


#||
define method text-size
    (port :: <test-port>, string :: <string>,
     #key text-style :: <text-style> = $default-text-style,
          start: _start = 0, end: _end = size(string), do-newlines?, do-tabs?)
 => (largest-x :: <real>, largest-y :: <real>, 
     cursor-x :: <real>, cursor-y :: <real>, baseline :: <real>)
  ignore(text-style, do-newlines?, do-tabs?);
  let length = _end - _start;
  values(length * 8, 12, length * 8, 0, 10)
end method text-size;
||#

(defmethod text-size ((port <test-port>) (string string)
                      &key (text-style *default-text-style*)
			((:start _start) 0) ((:end _end) (length string)) do-newlines? do-tabs?)
  (declare (ignore text-style do-newlines? do-tabs?))
  (let ((length (- _end _start)))
    (values (* length 8) 12 (* length 8) 0 10)))


#||
define method text-size
    (port :: <test-port>, string :: <character>,
     #key text-style :: <text-style> = $default-text-style,
          start: _start = 0, end: _end = size(string), do-newlines?, do-tabs?)
 => (largest-x :: <real>, largest-y :: <real>, 
     cursor-x :: <real>, cursor-y :: <real>, baseline :: <real>)
  ignore(text-style, _start, _end, do-newlines?);
  values(8, 12, 8, 0, 10)
end method text-size;
||#

(defmethod text-size ((port <test-port>) (string character)
                      &key (text-style *default-text-style*)
                      ((:start _start) 0) ((:end _end) 1) do-newlines? do-tabs?)
  (declare (ignore text-style _start _end do-newlines? do-tabs?))
  (values 8 12 8 0 10))


#||
define method beep (medium :: <test-port>) => ()
  format-out("\nBeep!")
end method beep;
||#

(defmethod beep ((medium <test-port>))
  (format t "~%Beep!~%"))


#||

/// Test Mirrors

define class <test-mirror> (<mirror>)
  //---*** slot mirror-sheet, init-keyword: sheet:;
  slot %region, init-keyword: region:;
end class <test-mirror>;
||#

(defclass <test-mirror> (<mirror>)
  ;; XXX: mirror-sheet? It *was* removed, but then none of the
  ;; tests would run...
  ((mirror-sheet :initarg :sheet :accessor mirror-sheet)
   (%region :initarg :region :accessor %region)))


#||
define method do-make-mirror
    (_port :: <test-port>, sheet :: <mirrored-sheet-mixin>)
 => (mirror :: <test-mirror>)
  // We're not using a real window system to back this up, so
  // just jiggle the coordinates so that it looks like we are
  let (left, top, right, bottom) = box-edges(sheet);
  let parent    = if (sheet-direct-mirror(sheet)) sheet else sheet-device-parent(sheet) end;
  let transform = sheet-delta-transform(sheet, parent);
  let (left, top, right, bottom)
    = transform-box(transform, left, top, right, bottom);
  let mirror
    = make(<test-mirror>,
	   sheet: sheet,
	   region: make-bounding-box(left, top, right, bottom));
  mirror
end method do-make-mirror;
||#

(defmethod do-make-mirror ((_port <test-port>) (sheet <mirrored-sheet-mixin>))
  (multiple-value-bind (left top right bottom)
      (box-edges sheet)
    (let* ((parent (if (sheet-direct-mirror sheet) sheet (sheet-device-parent sheet)))
           (transform (sheet-delta-transform sheet parent)))
      (multiple-value-bind (left top right bottom)
          (transform-box transform left top right bottom)
        (make-instance (find-class '<test-mirror>)
		       :sheet sheet
                       :region (make-bounding-box left top right bottom))))))


#||
define method destroy-mirror
    (_port :: <test-port>, sheet :: <sheet>, mirror :: <mirror>) => ()
  sheet-direct-mirror(sheet) := #f
end method destroy-mirror;
||#

(defmethod destroy-mirror ((port <test-port>) (sheet <sheet>) (mirror <mirror>))
  (setf (sheet-direct-mirror sheet) nil))


#||
define method map-mirror
    (_port :: <test-port>, sheet :: <sheet>, mirror :: <mirror>) => ()
  #t
end method map-mirror;
||#

(defmethod map-mirror ((_port <test-port>) (sheet <sheet>) (mirror <mirror>))
  t)


#||
define method unmap-mirror
    (_port :: <test-port>, sheet :: <sheet>, mirror :: <mirror>) => ()
  #f
end method unmap-mirror;
||#

(defmethod unmap-mirror ((_port <test-port>) (sheet <sheet>) (mirror <mirror>))
  nil)


#||
define method raise-mirror
    (_port :: <test-port>, sheet :: <sheet>, mirror :: <mirror>,
     #key activate? = #t) => ()
  ignore(activate?);
  #t
end method raise-mirror;
||#

(defmethod raise-mirror ((_port <test-port>) (sheet <sheet>) (mirror <mirror>)
                         &key (activate? t))
  (declare (ignore activate?))
  t)


#||
define method lower-mirror
    (_port :: <test-port>, sheet :: <sheet>, mirror :: <mirror>) => ()
  #t
end method lower-mirror;
||#

(defmethod lower-mirror ((_port <test-port>) (sheet <sheet>) (mirror <mirror>))
  t)


#||
define method mirror-visible?
    (_port :: <test-port>, sheet :: <sheet>, mirror :: <mirror>)
 => (visible? :: <boolean>)
  #t
end method mirror-visible?;
||#

(defmethod mirror-visible? ((_port <test-port>) (sheet <sheet>) (mirror <mirror>))
  t)


#||
define method mirror-edges
    (_port :: <test-port>, sheet :: <sheet>, mirror :: <mirror>)
 => (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
  box-edges(mirror.%region)
end method mirror-edges;
||#

(defmethod mirror-edges ((_port <test-port>) (sheet <sheet>) (mirror <mirror>))
  (box-edges (%region mirror)))


#||
define method set-mirror-edges
    (_port :: <test-port>, sheet :: <sheet>, mirror :: <mirror>,
     left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>) => ()
  mirror.%region := set-box-edges(mirror.%region, left, top, right, bottom)
end method set-mirror-edges;
||#

(defmethod set-mirror-edges ((_port <test-port>) (sheet <sheet>) (mirror <mirror>)
                             left top right bottom)
  (setf (%region mirror)
	(set-box-edges (%region mirror) left top right bottom)))


#||

/// Test Frame Manager

define class <test-frame-manager> (<basic-frame-manager>)
end class <test-frame-manager>;
||#

(defclass <test-frame-manager> (<basic-frame-manager>) ())


#||
define method frame-wrapper
    (framem :: <test-frame-manager>, frame :: <simple-frame>,
     layout :: false-or(<sheet>))
 => (sheet :: false-or(<column-layout>))
  let menu-bar   = frame-menu-bar(frame);
  let tool-bar   = frame-tool-bar(frame);
  let status-bar = frame-status-bar(frame);
  let children = make(<stretchy-vector>);
  when (menu-bar)   add!(children, menu-bar)   end;
  when (tool-bar)   add!(children, tool-bar)   end;
  when (layout)     add!(children, layout)     end;
  when (status-bar) add!(children, status-bar) end;
  unless (empty?(children))
    make-test-pane(<column-layout>, children: children)
  end
end method frame-wrapper;
||#

(defmethod frame-wrapper ((framem <test-frame-manager>) (frame <simple-frame>) (layout null))
  (let ((menu-bar (frame-menu-bar frame))
        (tool-bar (frame-tool-bar frame))
        (status-bar (frame-status-bar frame))
        (children (DUIM-UTILITIES:MAKE-STRETCHY-VECTOR)))
    (when menu-bar
      (add! children menu-bar))
    (when tool-bar
      (add! children tool-bar))
    (when layout
      (add! children layout))
    (when status-bar
      (add! children status-bar))
    (unless (empty? children)
      (make-test-pane (find-class '<column-layout>) :children children))))

(defmethod frame-wrapper ((framem <test-frame-manager>) (frame <simple-frame>) (layout <sheet>))
  (let ((menu-bar (frame-menu-bar frame))
        (tool-bar (frame-tool-bar frame))
        (status-bar (frame-status-bar frame))
        (children (DUIM-UTILITIES:MAKE-STRETCHY-VECTOR)))
    (when menu-bar
      (add! children menu-bar))
    (when tool-bar
      (add! children tool-bar))
    (when layout
      (add! children layout))
    (when status-bar
      (add! children status-bar))
    (unless (empty? children)
      (make-test-pane (find-class '<column-layout>) :children children))))


#||
define method make-frame-manager
    (_port :: <test-port>,
     #key palette, class = <test-frame-manager>, #all-keys)
 => (framem :: <test-frame-manager>)
  make(class, port: _port, palette: palette)
end method make-frame-manager;
||#

(defmethod make-frame-manager ((_port <test-port>)
                               &key palette (class (find-class '<test-frame-manager>)) &allow-other-keys)
  (make-instance class :port _port :palette palette))


#||

/// Test panes

define class <test-pane-mixin>
    (<standard-input-mixin>,
     <standard-repainting-mixin>,
     <mirrored-sheet-mixin>)
end class <test-pane-mixin>;
||#

(defclass <test-pane-mixin>
    (<standard-input-mixin>
     <standard-repainting-mixin>
     <mirrored-sheet-mixin>)
  ())


#||
// The test port uses phony mirrors, so we need to repaint them ourself
define method port-handles-repaint?
    (_port :: <test-port>, sheet :: <test-pane-mixin>) => (true? :: <boolean>)
  #f
end method port-handles-repaint?;
||#

(defmethod port-handles-repaint? ((_port <test-port>) (sheet <test-pane-mixin>))
  nil)


#||
// The test port uses phony mirrors, so we need to repaint them ourself
define method port-handles-repaint?
    (_port :: <test-port>, sheet :: <drawing-pane>) => (true? :: <boolean>)
  #f
end method port-handles-repaint?;
||#

(defmethod port-handles-repaint? ((_port <test-port>) (sheet <drawing-pane>))
  nil)


#||
define class <test-gadget-mixin>
    (<sheet-with-medium-mixin>,		// here to support kludge 'handle-repaint' methods
     <test-pane-mixin>)
  constant slot handled-events :: <stretchy-vector>  = make(<stretchy-vector>);
end class <test-gadget-mixin>;
||#

(defclass <test-gadget-mixin>
    (<sheet-with-medium-mixin>
     <test-pane-mixin>)
  ((handled-events :initform (make-array 0 :adjustable t :fill-pointer t) :reader handled-events)))


#||
define method record-event
    (sheet :: <test-gadget-mixin>, event :: <event>) => ()
  add!(handled-events(sheet), event)
end method record-event;
||#

(defmethod record-event ((sheet <test-gadget-mixin>) (event <event>))
  (add! (handled-events sheet) event))


#||
define class <test-top-level-sheet>
    (<permanent-medium-mixin>,
     <test-pane-mixin>,
     <top-level-sheet>)
  slot top-level-sheet-region = #f;
end class <test-top-level-sheet>;
||#

(defclass <test-top-level-sheet>
    (<permanent-medium-mixin>
     <test-pane-mixin>
     <top-level-sheet>)
  ((top-level-sheet-region :initform nil :accessor top-level-sheet-region)))


#||
define method mirror-edges
    (port :: <test-port>, sheet :: <test-top-level-sheet>, mirror :: <mirror>)
 => (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
  let region = top-level-sheet-region(sheet);
  if (region)
    box-edges(region);
  else
    next-method()
  end
end method mirror-edges;
||#

(defmethod mirror-edges ((port <test-port>) (sheet <test-top-level-sheet>) (mirror <mirror>))
  (let ((region (top-level-sheet-region sheet)))
    (if region
        (box-edges region)
        (call-next-method))))


#||
define method class-for-make-pane
    (framem :: <test-frame-manager>, class == <top-level-sheet>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<test-top-level-sheet>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <test-frame-manager>) (class (eql (find-class '<top-level-sheet>))) &key)
  (values (find-class '<test-top-level-sheet>) nil))


#||
define method record-event
    (sheet :: <test-top-level-sheet>, event :: <event>) => ()
  let frame = sheet-frame(sheet);
  add!(handled-events(frame), event)
end method record-event;
||#

(defmethod record-event ((sheet <test-top-level-sheet>) (event <event>))
  (let ((frame (sheet-frame sheet)))
    (add! (handled-events frame) event)))


#||
define class <test-viewport>
    (<test-pane-mixin>,
     <viewport>,
     <single-child-composite-pane>)
  constant slot horizontal-scroll-bar = #f,
    init-keyword: horizontal-scroll-bar:;
  constant slot vertical-scroll-bar   = #f,
    init-keyword: vertical-scroll-bar:;
end class <test-viewport>;
||#


(defclass <test-viewport>
    (<test-pane-mixin>
     <viewport>
     <single-child-composite-pane>)
  ;; XXX: SHOULDN'T THESE BE PICKED UP FROM THE <SCROLLING-SHEET> MIXIN?
  ;; NOTE THAT THE TESTS PASS IF THEY ARE INSTEAD OF FROM HERE
  ;; IT APPEARS THAT THE INITARGS ARE NEVER SUPPLIED, SO NO BARS ARE
  ;; EVER ASSOCIATED.
  ((horizontal-scroll-bar :initarg :horizontal-scroll-bar
			  :initform nil
			  :reader horizontal-scroll-bar)
   (vertical-scroll-bar :initarg :vertical-scroll-bar
			:initform nil
			:reader vertical-scroll-bar)))


#||
define method class-for-make-pane
    (framem :: <test-frame-manager>, class == <viewport>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<test-viewport>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <test-frame-manager>) (class (eql (find-class '<viewport>))) &key)
  (values (find-class '<test-viewport>) nil))


#||
define class <test-scroll-bar>
  (<test-gadget-mixin>,
   <scroll-bar>,
   <leaf-pane>)
end class <test-scroll-bar>;
||#

(defclass <test-scroll-bar>
    (<test-gadget-mixin>
     <scroll-bar>
     <leaf-pane>)
  ())

#||
define method class-for-make-pane
    (framem :: <test-frame-manager>, class == <scroll-bar>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<test-scroll-bar>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <test-frame-manager>) (class (eql (find-class '<scroll-bar>))) &key)
  (values (find-class '<test-scroll-bar>) nil))


#||
define method do-compose-space
    (pane :: <test-scroll-bar>, #key width, height)
 => (space-req :: <space-requirement>)
  select (gadget-orientation(pane))
    #"horizontal" =>
      make(<space-requirement>,
	   width: width | 50, min-width: 50, max-width: $fill,
	   height: 10);
    #"vertical" =>
      make(<space-requirement>,
	   width: 10,
	   height: height | 50, min-height: 50, max-height: $fill);
  end
end method do-compose-space;
||#

(defmethod do-compose-space ((pane <test-scroll-bar>) &key width height)
  (ecase (gadget-orientation pane)
    (:horizontal (make-space-requirement :width (or width 50)
					 :min-width 50
					 :max-width +fill+
					 :height 10))
    (:vertical (make-space-requirement :width 10
				       :height (or height 50)
				       :min-height 50
				       :max-height +fill+))))


#||
define class <test-slider-pane>
  (<test-gadget-mixin>,
   <slider>,
   <leaf-pane>)
end class <test-slider-pane>;
||#

(defclass <test-slider-pane>
    (<test-gadget-mixin>
     <slider>
     <leaf-pane>)
  ())

#||
define method class-for-make-pane
    (framem :: <test-frame-manager>, class == <slider>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<test-slider-pane>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <test-frame-manager>) (class (eql (find-class '<slider>))) &key)
  (values (find-class '<test-slider-pane>) nil))


#||
define class <basic-test-button>
    (<test-gadget-mixin>,
     <leaf-pane>)
end class <basic-test-button>;
||#

(defclass <basic-test-button>
    (<test-gadget-mixin>
     <leaf-pane>)
  ())

#||
define method do-compose-space
    (pane :: <basic-test-button>, #key width, height)
 => (space-req :: <space-requirement>)
  ignore(width, height);
  make(<space-requirement>,
       width: 40,
       height: 15)
end method do-compose-space;
||#

(defmethod do-compose-space ((pane <basic-test-button>) &key width height)
  (declare (ignore width height))
  (make-space-requirement :width 40 :height 15))


#||
define method allocate-space
    (pane :: <basic-test-button>, width :: <integer>, height :: <integer>) => ()
end method allocate-space;
||#

;; FIXME: IS THIS RIGHT?
(defmethod allocate-space ((pane <basic-test-button>) width height)
  (declare (ignore width height))
  nil)


#||
define class <test-push-button-pane>
    (<push-button>,
     <basic-test-button>)
end class <test-push-button-pane>;
||#

(defclass <test-push-button-pane>
    (<push-button>
     <basic-test-button>)
  ())

#||
define method class-for-make-pane
    (framem :: <test-frame-manager>, class == <push-button>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<test-push-button-pane>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <test-frame-manager>) (class (eql (find-class '<push-button>))) &key)
  (values (find-class '<test-push-button-pane>) nil))


#||
define method do-compose-space
    (pane :: <test-push-button-pane>, #key width, height)
 => (space-req :: <space-requirement>)
  ignore(width, height);
  //---*** Shouldn't be calling medium functions until we have a port
  if (#f) // (gadget-label(pane) & port(pane))
    let (lw, lh) = gadget-label-size(pane);
    let x-margin = 2;
    let y-margin = 2;
    make(<space-requirement>,
         width:  x-margin + lw + x-margin,
         height: y-margin + lh + y-margin)
  else
    make(<space-requirement>,
         width:  40, height: 15)
  end
end method do-compose-space;
||#

(defmethod do-compose-space ((pane <test-push-button-pane>) &key width height)
  (declare (ignore width height))
  ;;---*** Shouldn't be calling medium functions until we have a port
  ;;  (if nil   ; (and (gadget-label pane) (port pane))
  ;;      (multiple-value-bind (lw lh)
  ;;          (gadget-label-size pane)
  ;;        (let ((x-margin 2)
  ;;              (y-margin 2))
  ;;          (make-instance #m<space-requirement>
  ;;                         :width (+ x-margin lw x-margin)
  ;;                         :height (+ y-margin lh y-margin))))
  ;; else
  (make-space-requirement :width 40 :height 15))


#||
define method handle-event
    (pane :: <test-push-button-pane>, event :: <button-release-event>) => ()
  execute-activate-callback(pane, gadget-client(pane), gadget-id(pane))
end method handle-event;
||#

(defmethod handle-event ((pane <test-push-button-pane>) (event <button-release-event>))
  (execute-activate-callback pane
                             (gadget-client pane)
                             (gadget-id pane)))


#||
define method handle-repaint
    (pane :: <test-push-button-pane>, medium :: <medium>, region :: <region>) => ()
  let (left, top, right, bottom) = box-edges(sheet-device-region(pane));
  draw-rectangle(medium, left, top, right, bottom, filled?: #f);
  //--- This isn't drawing at the right place
  when (gadget-label(pane))
    let x-margin = 2;
    let y-margin = 2;
    let x = right - left;
    let y = bottom + y-margin;
    draw-gadget-label(pane, medium, x, y,
		      align-x: #"center", align-y: #"baseline");
  end;
  draw-rectangle(medium, left, top, right, bottom, filled?: #f)
end method handle-repaint;
||#

(defmethod handle-repaint ((pane <test-push-button-pane>) (medium <medium>) (region <region>))
  (multiple-value-bind (left top right bottom)
      (box-edges (sheet-device-region pane))
    (draw-rectangle medium left top right bottom :filled? nil)
    ;;--- This isn't drawing at the right place
    (when (gadget-label pane)
      (let* ((x-margin 2)
             (y-margin 2)
             (x (- right left))
             (y (+ bottom y-margin)))
	(declare (ignore x-margin))
        (draw-gadget-label pane
                           medium
                           x y
                           :align-x :center
                           :align-y :baseline)))
    (draw-rectangle medium left top right bottom :filled? nil)))


#||
define class <test-radio-button-pane>
    (<radio-button>,
     <basic-test-button>)
end class <test-radio-button-pane>;
||#

(defclass <test-radio-button-pane>
    (<radio-button>
     <basic-test-button>)
  ())

#||
define method class-for-make-pane
    (framem :: <test-frame-manager>, class == <radio-button>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<test-radio-button-pane>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <test-frame-manager>) (class (eql (find-class '<radio-button>))) &key)
  (values (find-class '<test-radio-button-pane>) nil))


#||
define method handle-event
    (pane :: <test-radio-button-pane>, event :: <button-release-event>) => ()
  gadget-value(pane, do-callback?: #t) := ~gadget-value(pane)
end method handle-event;
||#

(defmethod handle-event ((pane <test-radio-button-pane>) (event <button-release-event>))
  (setf (gadget-value pane :do-callback? t) (not (gadget-value pane))))


#||
//--- Use drawing code from CLIM's db-button.lisp
define method handle-repaint
    (pane :: <test-radio-button-pane>, medium :: <medium>, region :: <region>) => ()
  let (left, top, right, bottom) = box-edges(sheet-device-region(pane));
  //--- square, selected/deselected
  draw-rectangle(medium, left, top, right, bottom,
		 filled?: gadget-value(pane))
end method handle-repaint;
||#

(defmethod handle-repaint ((pane <test-radio-button-pane>) (medium <medium>) (region <region>))
  (multiple-value-bind (left top right bottom)
      (box-edges (sheet-device-region pane))
    (draw-rectangle medium
                    left
                    top
                    right
                    bottom
                    :filled? (gadget-value pane))))


#||
define class <test-check-button-pane>
    (<check-button>,
     <basic-test-button>)
end class <test-check-button-pane>;
||#

(defclass <test-check-button-pane>
    (<check-button>
     <basic-test-button>)
  ())

#||
define method class-for-make-pane
    (framem :: <test-frame-manager>, class == <check-button>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<test-check-button-pane>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <test-frame-manager>) (class (eql (find-class '<check-button>))) &key)
  (values (find-class '<test-check-button-pane>) nil))


#||
define method handle-event
    (pane :: <test-check-button-pane>, event :: <button-release-event>) => ()
  gadget-value(pane, do-callback?: #t) := ~gadget-value(pane)
end method handle-event;
||#

(defmethod handle-event ((pane <test-check-button-pane>) (event <button-release-event>))
  (setf (gadget-value pane :do-callback? t) (not (gadget-value pane))))


#||
//--- Use drawing code from CLIM's db-button.lisp
define method handle-repaint
    (pane :: <test-check-button-pane>, medium :: <medium>, region :: <region>) => ()
  let (left, top, right, bottom) = box-edges(sheet-device-region(pane));
  //--- diamond, selected/deselected
  draw-rectangle(medium, left, top, right, bottom,
		 filled?: gadget-value(pane))
end method handle-repaint;
||#

(defmethod handle-repaint ((pane <test-check-button-pane>) (medium <medium>) (region <region>))
  (multiple-value-bind (left top right bottom)
      (box-edges (sheet-device-region pane))
    ;;--- diamond, selected/deselected
    (draw-rectangle medium
                    left
                    top
                    right
                    bottom
                    :filled? (gadget-value pane))))


#||
define class <test-list-box>
    (<test-gadget-mixin>,
     <list-box>,
     <leaf-pane>)
end class <test-list-box>;
||#

(defclass <test-list-box>
    (<test-gadget-mixin>
     <list-box>
     <leaf-pane>)
  ())


#||
define method do-compose-space
    (pane :: <test-list-box>, #key width, height)
 => (space-req :: <space-requirement>)
  make(<space-requirement>,
       width: width | 30,
       height: height | 50,
       min-width: 30,
       min-height: 50,
       max-width: $fill,
       max-height: $fill)
end method do-compose-space;
||#

(defmethod do-compose-space ((pane <test-list-box>) &key width height)
  (make-space-requirement :width (or width 30)
			  :height (or height 50)
			  :min-width 30
			  :min-height 50
			  :max-width +fill+
			  :max-height +fill+))


#||
define method class-for-make-pane
    (framem :: <test-frame-manager>, class == <list-box>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<test-list-box>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <test-frame-manager>) (class (eql (find-class '<list-box>))) &key)
  (values (find-class '<test-list-box>) nil))


#||
define method handle-repaint
    (pane :: <test-list-box>, medium :: <medium>, region :: <region>) => ()
  let (left, top, right, bottom) = box-edges(sheet-device-region(pane));
  //--- square, selected/deselected
  draw-rectangle(medium, left, top, right, bottom, filled?: #f)
end method handle-repaint;
||#

(defmethod handle-repaint ((pane <test-list-box>) (medium <medium>) (region <region>))
  (multiple-value-bind (left top right bottom)
      (box-edges (sheet-device-region pane))
    ;;--- square, selected/deselected
    (draw-rectangle medium
                    left
                    top
                    right
                    bottom
                    :filled? nil)))


#||
define class <test-option-box>
    (<test-gadget-mixin>,
     <option-box>,
     <leaf-pane>)
end class <test-option-box>;
||#

(defclass <test-option-box>
    (<test-gadget-mixin>
     <option-box>
     <leaf-pane>)
  ())


#||
define method do-compose-space
    (pane :: <test-option-box>, #key width, height)
 => (space-req :: <space-requirement>)
  make(<space-requirement>,
       width: width | 30,
       height: height | 50,
       min-width: 30,
       min-height: 50,
       max-width: $fill,
       max-height: $fill)
end method do-compose-space;
||#

(defmethod do-compose-space ((pane <test-option-box>) &key width height)
  (make-space-requirement :width (or width 30)
			  :height (or height 50)
			  :min-width 30
			  :min-height 50
			  :max-width +fill+
			  :max-height +fill+))


#||
define method class-for-make-pane
    (framem :: <test-frame-manager>, class == <option-box>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<test-option-box>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <test-frame-manager>) (class (eql (find-class '<option-box>))) &key)
  (values (find-class '<test-option-box>) nil))


#||
define class <test-combo-box>
    (<test-gadget-mixin>,
     <combo-box>,
     <leaf-pane>)
end class <test-combo-box>;
||#

(defclass <test-combo-box>
    (<test-gadget-mixin>
     <combo-box>
     <leaf-pane>)
  ())


#||
define method do-compose-space
    (pane :: <test-combo-box>, #key width, height)
 => (space-req :: <space-requirement>)
  make(<space-requirement>,
       width: width | 30,
       height: height | 50,
       min-width: 30,
       min-height: 50,
       max-width: $fill,
       max-height: $fill)
end method do-compose-space;
||#

(defmethod do-compose-space ((pane <test-combo-box>) &key width height)
  (make-space-requirement :width (or width 30)
			  :height (or height 50)
			  :min-width 30
			  :min-height 50
			  :max-width +fill+
			  :max-height +fill+))


#||
define method class-for-make-pane
    (framem :: <test-frame-manager>, class == <combo-box>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<test-combo-box>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <test-frame-manager>) (class (eql (find-class '<combo-box>))) &key)
  (values (find-class '<test-combo-box>) nil))


#||
define class <test-spin-box>
    (<test-gadget-mixin>,
     <spin-box>,
     <leaf-pane>)
end class <test-spin-box>;
||#

(defclass <test-spin-box>
    (<test-gadget-mixin>
     <spin-box>
     <leaf-pane>)
  ())


#||
define method class-for-make-pane
    (framem :: <test-frame-manager>, class == <spin-box>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<test-spin-box>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <test-frame-manager>) (class (eql (find-class '<spin-box>))) &key)
  (values (find-class '<test-spin-box>) nil))


#||
define class <test-menu-bar-pane>
    (<test-gadget-mixin>,
     <multiple-child-composite-pane>,
     <menu-bar>)
end class <test-menu-bar-pane>;
||#

(defclass <test-menu-bar-pane>
    (<test-gadget-mixin>
     <multiple-child-composite-pane>
     <menu-bar>)
  ())


#||
define method do-compose-space
    (menu :: <test-menu-bar-pane>, #key width, height)
 => (space-req :: <space-requirement>)
  ignore(width, height);
  let children = sheet-children(menu);
  make(<space-requirement>,
       width: width | 20 + 40 * size(children),
       height: 15)
end method do-compose-space;
||#

;; FIXME REVIEW AFTER MAKE-INSTANCE -> MAKE-PROTOCOL-TYPE CHANGE

;; SN: The callers of this method, at least in frames tests, do not
;; provide height/width arguments, so do-compose-space receives nil if
;; we don't set it here.
(defmethod do-compose-space ((menu <test-menu-bar-pane>) &key width height)
  (let ((children (sheet-children menu)))
    (make-space-requirement :width (or width (+ 20 (* 40 (length children))))
			    :height (or height 15))))

#||
define method class-for-make-pane
    (framem :: <test-frame-manager>, class == <menu-bar>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<test-menu-bar-pane>, #f)
end method class-for-make-pane;
||#
(defmethod class-for-make-pane ((framem <test-frame-manager>) (class (eql (find-class '<menu-bar>))) &key)
  (values (find-class '<test-menu-bar-pane>) nil))


#||
define class <test-menu-pane>
    (<test-gadget-mixin>,
     <multiple-child-composite-pane>,
     <menu>)
end class <test-menu-pane>;
||#

(defclass <test-menu-pane>
    (<test-gadget-mixin>
     <multiple-child-composite-pane>
     <menu>)
  ())


#||
define method class-for-make-pane
    (framem :: <test-frame-manager>, class == <menu>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<test-menu-pane>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <test-frame-manager>) (class (eql (find-class '<menu>))) &key)
  (values (find-class '<test-menu-pane>) nil))


#||
//--- Relaying-out a menu doesn't make any sense.
define method relayout-parent
    (sheet :: <test-menu-pane>, #key width, height)
 => (did-layout? :: <boolean>)
  ignore(width, height);
  #f
end method relayout-parent;
||#

(defmethod relayout-parent ((sheet <test-menu-pane>) &key width height)
  (declare (ignore width height))
  nil)


#||
define class <test-push-menu-button-pane>
    (<push-menu-button>,
     <basic-test-button>)
end class <test-push-menu-button-pane>;
||#

(defclass <test-push-menu-button-pane>
    (<push-menu-button>
     <basic-test-button>)
  ())


#||
define method class-for-make-pane
    (framem :: <test-frame-manager>, class == <push-menu-button>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<test-push-menu-button-pane>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <test-frame-manager>) (class (eql (find-class '<push-menu-button>))) &key)
  (values (find-class '<test-push-menu-button-pane>) nil))


#||
define class <test-radio-menu-button-pane>
    (<radio-menu-button>,
     <basic-test-button>)
end class <test-radio-menu-button-pane>;
||#

(defclass <test-radio-menu-button-pane>
    (<radio-menu-button>
     <basic-test-button>)
  ())


#||
define method class-for-make-pane 
    (framem :: <test-frame-manager>, class == <radio-menu-button>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<test-radio-menu-button-pane>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <test-frame-manager>) (class (eql (find-class '<radio-menu-button>))) &key)
  (values (find-class '<test-radio-menu-button-pane>) nil))



#||
define class <test-check-menu-button-pane>
    (<check-menu-button>,
     <basic-test-button>)
end class <test-check-menu-button-pane>;
||#

(defclass <test-check-menu-button-pane>
    (<check-menu-button>
     <basic-test-button>)
  ())


#||
define method class-for-make-pane 
    (framem :: <test-frame-manager>, class == <check-menu-button>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<test-check-menu-button-pane>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <test-frame-manager>) (class (eql (find-class '<check-menu-button>))) &key)
  (values (find-class '<test-check-menu-button-pane>) nil))


#||
define class <test-text-field-pane>
    (<test-gadget-mixin>,
     <text-field>,
     <leaf-pane>)
end class <test-text-field-pane>;
||#

(defclass <test-text-field-pane>
    (<test-gadget-mixin>
     <text-field>
     <leaf-pane>)
  ())


#||
define method do-compose-space 
    (pane :: <test-text-field-pane>, #key width, height)
 => (space-req :: <space-requirement>)
  make(<space-requirement>,
       width: width | 100,
       height: height | 16,
       min-width: 40,
       min-height: 12,
       max-width: $fill,
       max-height: $fill)
end method do-compose-space;
||#

(defmethod do-compose-space ((pane <test-text-field-pane>) &key width height)
  (make-space-requirement :width (or width 100)
			  :height (or height 16)
			  :min-width 40
			  :min-height 12
			  :max-width +fill+
			  :max-height +fill+))


#||
define method class-for-make-pane 
    (framem :: <test-frame-manager>, class == <text-field>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<test-text-field-pane>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <test-frame-manager>) (class (eql (find-class '<text-field>))) &key)
  (values (find-class '<test-text-field-pane>) nil))


#||
define class <test-password-field-pane>
    (<test-gadget-mixin>,
     <password-field>,
     <leaf-pane>)
end class <test-password-field-pane>;
||#

(defclass <test-password-field-pane>
    (<test-gadget-mixin>
     <password-field>
     <leaf-pane>)
  ())


#||
define method do-compose-space 
    (pane :: <test-password-field-pane>, #key width, height)
 => (space-req :: <space-requirement>)
  make(<space-requirement>,
       width: width | 100,
       height: height | 16,
       min-width: 40,
       min-height: 12,
       max-width: $fill,
       max-height: $fill)
end method do-compose-space;
||#

(defmethod do-compose-space ((pane <test-password-field-pane>) &key width height)
  (make-space-requirement :width (or width 100)
			  :height (or height 16)
			  :min-width 40
			  :min-height 12
			  :max-width +fill+
			  :max-height +fill+))


#||
define method class-for-make-pane 
    (framem :: <test-frame-manager>, class == <password-field>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<test-password-field-pane>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <test-frame-manager>) (class (eql (find-class '<password-field>))) &key)
  (values (find-class '<test-password-field-pane>) nil))


#||
define class <test-text-editor-pane>
    (<test-gadget-mixin>,
     <text-editor>,
     <leaf-pane>)
end class <test-text-editor-pane>;
||#

(defclass <test-text-editor-pane>
    (<test-gadget-mixin>
     <text-editor>
     <leaf-pane>)
  ())


#||
define method class-for-make-pane 
    (framem :: <test-frame-manager>, class == <text-editor>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<test-text-editor-pane>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <test-frame-manager>) (class (eql (find-class '<text-editor>))) &key)
  (values (find-class '<test-text-editor-pane>) nil))


#||
define class <test-splitter-pane>
    (<test-gadget-mixin>,
     <splitter>,
     <leaf-pane>)
end class <test-splitter-pane>;
||#

(defclass <test-splitter-pane>
    (<test-gadget-mixin>
     <splitter>
     <leaf-pane>)
  ())


#||
define method class-for-make-pane 
    (framem :: <test-frame-manager>, class == <splitter>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<test-splitter-pane>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <test-frame-manager>) (class (eql (find-class '<splitter>))) &key)
  (values (find-class '<test-splitter-pane>) nil))


#||
define class <test-progress-bar> 
    (<test-gadget-mixin>,
     <progress-bar>,
     <leaf-pane>)
end class <test-tree-control>;
||#

(defclass <test-progress-bar>
    (<test-gadget-mixin>
     <progress-bar>
     <leaf-pane>)
  ())


#||
define method class-for-make-pane 
    (framem :: <test-frame-manager>, class == <progress-bar>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<test-progress-bar>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <test-frame-manager>) (class (eql (find-class '<progress-bar>))) &key)
  (values (find-class '<test-progress-bar>) nil))


#||
define class <test-status-bar> 
    (<test-gadget-mixin>,
     <status-bar>,
     <leaf-pane>)
end class <test-tree-control>;
||#

(defclass <test-status-bar>
    (<test-gadget-mixin>
     <status-bar>
     <leaf-pane>

     ;; <status-bar> does not provide a specialized method for
     ;; (setf sheet-children), even though this method is invoked
     ;; in that class' INITIALIZE-INSTANCE method.
     ;; Add something that does include such a method to get the
     ;; tests to pass.
     
     <multiple-child-mixin>)
  ())


#||
define method class-for-make-pane 
    (framem :: <test-frame-manager>, class == <status-bar>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<test-status-bar>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <test-frame-manager>) (class (eql (find-class '<status-bar>))) &key)
  (values (find-class '<test-status-bar>) nil))


#||
define class <test-list-control> 
    (<test-gadget-mixin>,
     <list-control>,
     <leaf-pane>)
end class <test-list-control>;
||#

(defclass <test-list-control>
    (<test-gadget-mixin>
     <list-control>
     <leaf-pane>)
  ())


#||
define method class-for-make-pane 
    (framem :: <test-frame-manager>, class == <list-control>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<test-list-control>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <test-frame-manager>) (class (eql (find-class '<list-control>))) &key)
  (values (find-class '<test-list-control>) nil))


#||
define class <test-table-control> 
    (<test-gadget-mixin>,
     <table-control>,
     <leaf-pane>)
end class <test-table-control>;
||#

(defclass <test-table-control>
    (<test-gadget-mixin>
     <table-control>
     <leaf-pane>)
  ())


#||
define method class-for-make-pane 
    (framem :: <test-frame-manager>, class == <table-control>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<test-table-control>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <test-frame-manager>) (class (eql (find-class '<table-control>))) &key)
  (values (find-class '<test-table-control>) nil))


#||
define class <test-tree-control> 
    (<test-gadget-mixin>,
     <tree-control>,
     <leaf-pane>)
end class <test-tree-control>;
||#

(defclass <test-tree-control>
    (<test-gadget-mixin>
     <tree-control>
     <leaf-pane>)
  ())


#||
define method class-for-make-pane 
    (framem :: <test-frame-manager>, class == <tree-control>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<test-tree-control>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <test-frame-manager>) (class (eql (find-class '<tree-control>))) &key)
  (values (find-class '<test-tree-control>) nil))


#||
define class <test-tab-control> 
    (<test-gadget-mixin>,
     <tab-control>,
     <leaf-pane>)
end class <test-tab-control>;
||#

(defclass <test-tab-control>
    (<test-gadget-mixin>
     <tab-control>
     <leaf-pane>)
  ())


#||
define method class-for-make-pane 
    (framem :: <test-frame-manager>, class == <tab-control>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<test-tab-control>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <test-frame-manager>) (class (eql (find-class '<tab-control>))) &key)
  (values (find-class '<test-tab-control>) nil))


;;; Add to stop GADGETS.LISP <TAB-CONTROL>-CLASS-TEST blowing out
(defmethod note-pages-changed ((pane <test-tab-control>))
  nil)



#||
/// Make functions

define method make-test-pane
    (class :: <class>, #rest initargs)
  with-frame-manager (find-test-frame-manager())
    apply(make, class, initargs)
  end
end method make-test-pane;
||#

(defmethod make-test-pane (class &rest initargs)
  (with-frame-manager ((find-test-frame-manager))
     (apply #'make-pane class initargs)))


#||
define class <test-frame> (<simple-frame>)
  constant slot handled-events :: <stretchy-vector> = make(<stretchy-vector>);
end class <test-frame>;
||#

(defclass <test-frame> (<simple-frame>)
  ((handled-events :initform (DUIM-UTILITIES:MAKE-STRETCHY-VECTOR) :reader handled-events)))


#||
define method record-event
    (frame :: <test-frame>, event :: <event>) => ()
  add!(handled-events(frame), event)
end method record-event;
||#

(defmethod record-event ((frame <test-frame>) (event <event>))
  (add! (handled-events frame) event))


#||
define method make-test-frame
    (class :: subclass(<frame>),
     #rest args,
     #key frame-manager: framem, #all-keys)
 => (frame :: <frame>)
  let framem = framem | find-test-frame-manager();
  with-frame-manager (framem)
    let frame
      = apply(make, class,
	      title: "Test Frame",
	      args);
    frame-mapped?(frame) := #t;
    frame
  end
end method make-test-frame;
||#
(defmethod make-test-frame (class &rest args &key ((:frame-manager framem)) &allow-other-keys)
  (assert (instance? class (find-class '<frame>)))
  (let ((framem (or framem
                    (find-test-frame-manager))))
    (with-frame-manager (framem)
      (let ((frame (apply #'make-instance
			  class
			  :title "Test Frame"
			  args)))
	(setf (frame-mapped? frame) t)
	frame))))


#||

/// Debugging support

define table $gadget-class-names :: <object-table>
  = { <push-button>          => "push button",
      <radio-button>         => "radio button",
      <check-button>         => "check button",

      <button-box>           => "button box",
      <push-box>             => "push box",
      <radio-box>            => "radio box",
      <check-box>            => "check box",
      <push-box-pane>        => "push box pane",
      <radio-box-pane>       => "radio box pane",
      <check-box-pane>       => "check box pane",

      <list-box>             => "list box",
      <option-box>           => "option box",
      <combo-box>            => "combo box",
      <spin-box>             => "spin box",
      <slider>               => "slider",
      <separator>            => "separator",
      <text-field>           => "text field",
      <password-field>       => "password field",
      <text-editor>          => "text editor",

      <scroll-bar>           => "scroll-bar",
      <viewport>             => "viewport",
      <scroller>             => "scroller",

      <splitter>             => "splitter",
      <progress-bar>         => "progress control",
      <list-control>         => "list control",
      <status-bar>           => "status bar",
      <table-control>        => "table control",
      <tool-bar>             => "tool bar",
      <tree-control>         => "tree control",

      <menu-bar>             => "menu bar",
      <menu>                 => "menu",
      <menu-box>             => "menu box",
      <push-menu-box>        => "push menu component",
      <radio-menu-box>       => "radio menu component",
      <check-menu-box>       => "check menu component",
      <push-menu-box-pane>   => "push menu component pane",
      <radio-menu-box-pane>  => "radio menu component pane",
      <check-menu-box-pane>  => "check menu component pane",
      <push-menu-button>     => "push menu button",
      <radio-menu-button>    => "radio menu button",
      <check-menu-button>    => "check menu button",

      <row-layout>           => "row layout",
      <row-layout-pane>      => "row layout pane",
      <column-layout>        => "column layout",
      <column-layout-pane>   => "column layout pane",
      <stack-layout>         => "stack layout",
      <stack-layout-pane>    => "stack layout pane",
      <table-layout>         => "table layout",
      <table-layout-pane>    => "table layout pane",
      <grid-layout>          => "grid layout",
      <grid-layout-pane>     => "grid layout pane",
      <border>               => "border",
      <border-pane>          => "border pane",
      <group-box>            => "group box",
      <spacing>              => "spacing",
      <spacing-pane>         => "spacing pane",
      <single-child-wrapping-pane>   => "single child wrapping layout pane",
      <multiple-child-wrapping-pane> => "multiple child wrapping layout pane",
      <pinboard-layout>      => "pinboard layout",
      <pinboard-layout-pane> => "pinboard layout pane",

      <test-list-box>        => "test list box",

      <top-level-sheet>      => "top level sheet",
      <test-top-level-sheet> => "test top level sheet",

      <simple-frame>         => "simple frame",
      <frame>                => "frame"

      /*---*** Removed
      <spin-box-pane>        => "spin box pane",
      <list-control-pane>    => "list control pane",
      <table-control-pane>   => "table control pane",
      <tree-control-pane>    => "tree control pane"
      */
      };
||#

(defparameter *gadget-class-names* (make-hash-table))

(macrolet ((frob (key value)
             `(setf (gethash (find-class ,key) *gadget-class-names*) ,value)))

  (frob '<push-button>                  "push button")
  (frob '<radio-button>                 "radio button")
  (frob '<check-button>                 "check button")

  (frob '<button-box>                   "button box")
  (frob '<push-box>                     "push box")
  (frob '<radio-box>                    "radio box")
  (frob '<check-box>                    "check box")
  (frob '<push-box-pane>                "push box pane")
  (frob '<radio-box-pane>               "radio box pane")
  (frob '<check-box-pane>               "check box pane")

  (frob '<list-box>                     "list box")
  (frob '<option-box>                   "option box")
  (frob '<combo-box>                    "combo box")
  (frob '<spin-box>                     "spin box")
  (frob '<slider>                       "slider")
  (frob '<separator>                    "separator")
  (frob '<text-field>                   "text field")
  (frob '<password-field>               "password field")
  (frob '<text-editor>                  "text editor")

  (frob '<scroll-bar>                   "scroll-bar")
  (frob '<viewport>                     "viewport")
  (frob '<scroller>                     "scroller")

  (frob '<splitter>                     "splitter")
  (frob '<progress-bar>                 "progress control")
  (frob '<list-control>                 "list control")
  (frob '<status-bar>                   "status bar")
  (frob '<table-control>                "table control")
  (frob '<tool-bar>                     "tool bar")
  (frob '<tree-control>                 "tree control")

  (frob '<menu-bar>                     "menu bar")
  (frob '<menu>                         "menu")
  (frob '<menu-box>                     "menu box")
  (frob '<push-menu-box>                "push menu component")
  (frob '<radio-menu-box>               "radio menu component")
  (frob '<check-menu-box>               "check menu component")
  (frob '<push-menu-box-pane>           "push menu component pane")
  (frob '<radio-menu-box-pane>          "radio menu component pane")
  (frob '<check-menu-box-pane>          "check menu component pane")
  (frob '<push-menu-button>             "push menu button")
  (frob '<radio-menu-button>            "radio menu button")
  (frob '<check-menu-button>            "check menu button")

  (frob '<row-layout>                   "row layout")
  (frob '<row-layout-pane>              "row layout pane")
  (frob '<column-layout>                "column layout")
  (frob '<column-layout-pane>           "column layout pane")
  (frob '<stack-layout>                 "stack layout")
  (frob '<stack-layout-pane>            "stack layout pane")
  (frob '<table-layout>                 "table layout")
  (frob '<table-layout-pane>            "table layout pane")
  (frob '<grid-layout>                  "grid layout")
  (frob '<grid-layout-pane>             "grid layout pane")
  (frob '<border>                       "border")
  (frob '<border-pane>                  "border pane")
  (frob '<group-box>                    "group box")
  (frob '<spacing>                      "spacing")
  (frob '<spacing-pane>                 "spacing pane")
  (frob '<single-child-wrapping-pane>   "single child wrapping layout pane")
  (frob '<multiple-child-wrapping-pane> "multiple child wrapping layout pane")
  (frob '<pinboard-layout>              "pinboard layout")
  (frob '<pinboard-layout-pane>         "pinboard layout pane")

  (frob '<test-list-box>                "test list box")

  (frob '<top-level-sheet>              "top level sheet")
  (frob '<test-top-level-sheet>         "test top level sheet")

  (frob '<simple-frame>                 "simple frame")
  (frob '<frame>                        "frame"))

#||
      /*---*** Removed
      <spin-box-pane>        => "spin box pane",
      <list-control-pane>    => "list control pane",
      <table-control-pane>   => "table control pane",
      <tree-control-pane>    => "tree control pane"
      */
||#

#||
// gadget class name
define method gadget-class-name
    (class :: <class>) => (name :: <string>)
  element($gadget-class-names, class, default: #f)
    | format-to-string("unknown gadget %=", class)
end method gadget-class-name;
||#

(defmethod gadget-class-name ((class class))
  (or (gethash class *gadget-class-names* nil)  ;; :default nil)
      (format nil "unknown gadget ~a" class)))


#||
define method gadget-class-name
    (class) => (name :: <string>)
  gadget-class-name(object-class(class))
end method gadget-class-name;
||#

(defmethod gadget-class-name (class)
  (gadget-class-name (class-of class)))
