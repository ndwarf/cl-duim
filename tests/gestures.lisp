;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-TESTS -*-
(in-package #:duim-tests)

(def-suite duim-gestures
    :description "DUIM-GESTURES test suite"
    :in all-tests)
(in-suite duim-gestures)

#||
define method test-mouse-gesture
    (button :: <symbol>, #key modifiers = #[]) => ()
  let gesture = #f;
  let pointer = find-test-pointer();
  check-true("make(<gesture>) returns instance of <gesture>",
             begin
               gesture := make(<gesture>, button: button, modifiers: modifiers);
               instance?(gesture, <gesture>)
             end);
  check-true("event matches gesture",
             event-matches-gesture?(make(<button-release-event>,
                                         sheet: make-test-pane(<null-pane>),
                                         pointer: pointer,
                                         button: select (button)
                                                   #"left"   => $left-button;
                                                   #"right"  => $right-button;
                                                   #"middle" => $middle-button;
                                                 end,
                                         modifier-state: apply(make-modifier-state, modifiers)),
                                    gesture))
end method test-mouse-gesture;
||#

(defmethod test-mouse-gesture ((button symbol) &key (modifiers #()))
  (let ((gesture nil)
	(pointer (find-test-pointer)))
    (is (and (setf gesture (make-gesture :button button :modifiers modifiers))
	     (instance? gesture '<gesture>))
	"make(<gesture>) returns instance of <gesture>")
    (is (event-matches-gesture? (make-instance '<button-release-event>
					       :sheet (make-test-pane (find-class '<null-pane>))
					       :pointer pointer
					       :button (ecase button
							 (:left +left-button+)
							 (:right +right-button+)
							 (:middle +middle-button+))
					       :modifier-state (apply #'make-modifier-state (map-as (list) #'identity modifiers)))
				gesture)
	"event matches gesture")))


#||
define test mouse-gestures-test ()
  do(method (modifiers)
       test-mouse-gesture(#"left",   modifiers: modifiers);
       test-mouse-gesture(#"middle", modifiers: modifiers);
       test-mouse-gesture(#"right",  modifiers: modifiers);
     end,
     vector(#[],
            #[#"shift"],
            #[#"control"],
            #[#"alt"]))
end test mouse-gestures-test;
||#

(test mouse-gestures-test
  :description "mouse gestures test"
  (map nil #'(lambda (modifiers)
	       (test-mouse-gesture :left   :modifiers modifiers)
	       (test-mouse-gesture :middle :modifiers modifiers)
	       (test-mouse-gesture :right  :modifiers modifiers))
       (vector #() #(:shift) #(:control) #(:alt))))


#||
define test keyboard-gestures-test ()
end test keyboard-gestures-test;
||#

(test keyboard-gestures-test
  :description "keyboard gestures test"
  nil)


#||

/// Define the gestures test suite

define suite duim-gestures-suite ()
  test mouse-gestures-test;
  test keyboard-gestures-test;
end suite duim-gestures-suite;
||#

(defun test-gestures ()
  (run! 'duim-gestures))
