;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

(defpackage :duim-tests
  (:use #:common-lisp
        #:fiveam
	#:dylan-commands
	#:duim-utilities
	#:duim-internals
	#:duim-extended-geometry
	#:duim-gadget-panes)
  (:export #:run!))

