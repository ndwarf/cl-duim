;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-TESTS -*-
(in-package #:duim-tests)

(def-suite duim-frames
    :description "DUIM-FRAMES test suite"
    :in all-tests)
(in-suite duim-frames)

#||
define sideways method make-test-instance
    (class :: subclass(<frame>)) => (frame :: <frame>)
  //--- Be careful, since <frame> isn't instantiable!
  if (class == <frame>)
    next-method()
  else
    make(class, frame-manager: find-test-frame-manager())
  end
end method make-test-instance;

define duim-sheets class-test <frame> ()
  //---*** Fill this in...
end class-test <frame>;

define duim-sheets class-test <frame-manager> ()
  //---*** Fill this in...
end class-test <frame-manager>;

define duim-frames class-test <simple-frame> ()
  //---*** Fill this in...
end class-test <simple-frame>;


/// Frame layout tests

define method expected-named-size
    (frame :: <frame>, name, #rest args, #key width, height)
 => (size :: <integer>)
  ignore(width, height);
  apply(expected-named-size, top-level-sheet(frame), name, args)
end method expected-named-size;
||#

(defmethod expected-named-size ((frame <frame>) name &rest args &key width height)
  (declare (ignore width height))
  (apply #'expected-named-size (top-level-sheet frame) name args))


#||
define method sheet-alternative-size
    (sheet) => (width :: false-or(<integer>), height :: false-or(<integer>))
  let (old-width, old-height) = box-size(sheet);
  let (width, height) = expected-constrained-size(sheet,
                                                  old-width + 100,
                                                  old-height + 100);
  unless (width = old-width & height = old-height)
    values(width, height)
  end
end method sheet-alternative-size;
||#

(defmethod sheet-alternative-size (sheet)
  (multiple-value-bind (old-width old-height)
      (box-size sheet)
    (multiple-value-bind (width height)
        (expected-constrained-size sheet
                                   (+ old-width 100)
                                   (+ old-height 100))
      (unless (and (= width old-width)
                   (= height old-height))
        (values width height)))))


#||
define method check-frame-resize (frame, name) => ()
  let top-sheet = top-level-sheet(frame);
  let (width, height) = sheet-alternative-size(top-sheet);
  if (width & height)
    let new-region = make-bounding-box(0, 0, width, height);
    top-level-sheet-region(top-sheet) := new-region;
    distribute-event(port(frame),
                     make(<window-configuration-event>,
                          sheet: top-sheet,
                          region: new-region));
    check-layout-pane-layout(top-sheet, concatenate("resized ", name),
                             allocate-space?: #f,
                             width: width,
                             height: height)
  end;
end method check-frame-resize;
||#

(defmethod check-frame-resize (frame name)
  (let ((top-sheet (top-level-sheet frame)))
    (multiple-value-bind (width height)
        (sheet-alternative-size top-sheet)
      (when (and width height)
        (let ((new-region (make-bounding-box 0 0 width height)))
          (setf (top-level-sheet-region top-sheet) new-region)
          (distribute-event (port frame)
                            (make-instance '<window-configuration-event>
                                           :sheet top-sheet
                                           :region new-region))
          (check-layout-pane-layout top-sheet
				    (concatenate 'string "resized " name)
                                    :allocate-space? nil
                                    :width width
                                    :height height))))))


#||
define method check-frame-layout 
    (frame, layout, name, #key width, height) => ()
  let top-sheet = top-level-sheet(frame);
  check-equal(concatenate(name, " layout"), frame-layout(frame), layout);
  if (layout)
    check-true(concatenate(name, " has top-level-sheet"), 
               instance?(top-sheet, <top-level-sheet>));
    check-layout-pane-layout(top-sheet, name,
                             width: width | expected-width(top-sheet), 
                             height: height | expected-height(top-sheet),
                             allocate-space?: #f);
    check-frame-resize(frame, name);
  else
    check-false(concatenate(name, " has no top-level-sheet"), top-sheet);
  end;
  frame
end method check-frame-layout;
||#

(defmethod check-frame-layout (frame layout name &key width height)
  (let ((top-sheet (top-level-sheet frame)))
    (is (equal? (frame-layout frame) layout) "~a layout" name)
    (if layout
	(progn
	  (is (instance? top-sheet '<top-level-sheet>)
	      "~a has top-level-sheet" name)
	  (check-layout-pane-layout top-sheet
				    name
				    :width (or width (expected-width top-sheet))
				    :height (or height (expected-height top-sheet))
				    :allocate-space? nil)
	  (check-frame-resize frame name))
	;; else
	(is-false top-sheet "~a has no top-level-sheet" name))))

#||
define method test-frame-layout
    (name, layout, #key width, height) => (frame :: <frame>)
  let frame
    = make-test-frame(<test-frame>,
		      layout: layout, width: width, height: height);
  let name = concatenate(name, " ", gadget-class-name(<simple-frame>));
  check-frame-layout(frame, layout, name,
                     width: width, height: height);
  frame
end method test-frame-layout;
||#

(defmethod test-frame-layout (name layout &key width height)
  (let ((frame (make-test-frame (find-class '<test-frame>)
                                :layout layout
                                :width width
                                :height height))
	(name (concatenate 'string name
			   " "
			   (gadget-class-name (find-class '<simple-frame>)))))
    (check-frame-layout frame layout name :width width :height height)))


#||
define test frame-layouts-test ()
  test-frame-layout("empty", #f);
  test-frame-layout("fixed layout",
		    make-test-pane(<spacing>, child: make-test-pane(<button>)));
  test-frame-layout("non-fixed layout",
		    make-test-pane(<spacing>, child: make-test-pane(<list-box>)));
  test-frame-layout("explicit width",
		    make-test-pane(<spacing>, child: make-test-pane(<list-box>)),
		    width: 500);
  test-frame-layout("explicit height",
		    make-test-pane(<spacing>, child: make-test-pane(<list-box>)),
		    height: 600);
  test-frame-layout("explicit size",
		    make-test-pane(<spacing>, child: make-test-pane(<list-box>)),
		    width: 400,
		    height: 500);
end test frame-layouts-test;
||#

(test test-frame-layouts-test
  :description "test-frame-layouts-test"
  (test-frame-layout "empty" nil)
  (test-frame-layout "fixed layout"
		     (make-test-pane (find-class '<spacing>)
				     :child (make-test-pane (find-class '<button>))))
  (test-frame-layout "non-fixed layout"
		     (make-test-pane (find-class '<spacing>)
				     :child (make-test-pane (find-class '<list-box>))))
  (test-frame-layout "explicit width"
		     (make-test-pane (find-class '<spacing>)
				     :child (make-test-pane (find-class '<list-box>)))
		     :width 500)
  (test-frame-layout "explicit height"
		     (make-test-pane (find-class '<spacing>)
				     :child (make-test-pane (find-class '<list-box>)))
		     :height 600)
  (test-frame-layout "explicit size"
		     (make-test-pane (find-class '<spacing>)
				     :child (make-test-pane (find-class '<list-box>)))
		     :width 400
		     :height 500))


#||

/// Frame wrapper tests

define test frame-wrappers-test ()
  let layout = make-test-pane(<border-pane>, child: make-test-pane(<button>));
  let tool-bar = make-test-pane(<tool-bar>);
  let menu-bar = make-test-pane(<menu-bar>);
  let status-bar = make-test-pane(<status-bar>);
  let frame
    = make-test-frame(<test-frame>,
		      layout: layout,
		      tool-bar: tool-bar,
		      menu-bar: menu-bar,
		      status-bar: status-bar);
  check-equal("Frame menu bar installed",
	      frame-menu-bar(frame), menu-bar);
  check-equal("Frame tool bar installed",
	      frame-tool-bar(frame), tool-bar);
  check-equal("Frame layout installed",
	      frame-layout(frame), layout);
  check-equal("Frame status bar installed",
	      frame-status-bar(frame), status-bar);
end test frame-wrappers-test;
||#

(test frame-wrappers-test
  :description "frame wrappers test"
  (let* ((layout     (make-test-pane  (find-class '<border-pane>)
                                      :child (make-test-pane (find-class '<button>))))
	 (tool-bar   (make-test-pane  (find-class '<tool-bar>)))
	 (menu-bar   (make-test-pane  (find-class '<menu-bar>)))
	 (status-bar (make-test-pane  (find-class '<status-bar>)))
	 (frame      (make-test-frame (find-class '<test-frame>)
                                      :layout layout
                                      :tool-bar tool-bar
                                      :menu-bar menu-bar
                                      :status-bar status-bar)))
    (is (equal? (frame-menu-bar frame) menu-bar) "Frame menu bar installed")
    (is (equal? (frame-tool-bar frame) tool-bar) "Frame tool bar installed")
    (is (equal? (frame-layout frame) layout) "Frame layout installed")
    (is (equal? (frame-status-bar frame) status-bar)
	"Frame status bar installed")))

#||

/// Define the frames test suite

define suite duim-frames-suite ()
  test frame-layouts-test;
  test frame-wrappers-test;
end suite duim-frames-suite;
||#

(defun test-frames ()
  (run! 'duim-frames))

