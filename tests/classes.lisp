;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-TESTS -*-
(in-package :duim-tests)

(def-suite duim-classes
    :description "DUIM-CLASSES test suite"
    :in all-tests)
(in-suite duim-classes)

;;; DC classes
;;; https://github.com/dylan-lang/opendylan/blob/30b4a9192eecf4f5458c88f3901866af9dc1cf90/sources/duim/tests/core/classes.dylan#L9

(test make-test-<contrasting-color>-instance
  :description "make-test-instance <contrasting-color>"
  (is-true (make-instance '<contrasting-color>
			  :how-many 5
			  :which-one 1)))

(test make-test-<color-not-found>-instance
  :description "make-test-instance <color-not-found>"
  (is-true (make-instance '<color-not-found>
			  :color *red*)))

(test make-test-<palette-full>-instance
  :description "make-test-instance <palette-full>"
  (is-true (make-instance '<palette-full>
			  :palette (make-palette (find-test-port)))))

(test make-test-<palette>-instance
  :description "make-test-instance <palette>"
  (is-true (make-palette (find-test-port))))

(test make-test-<dynamic-color>-instance
  :description "make-test-instance <dynamic-color>"
  (is-true (make-instance '<dynamic-color> :color *red*)))

(test make-test-<pattern>-instance
  :description "make-test-instance <pattern>"
  (is-true (make-instance '<pattern>
			  :array (make-array '(2 2))
			  ;; XXX: MUST BE A LIST, AT THE MOMENT
			  ;;:colors #())))
			  :colors '())))

(test make-test-<stencil>-instance
  :description "make-test-instance <stencil>"
  (is-true (make-instance '<stencil> :array (make-array '(2 2)))))

(test make-test-<device-font>-instance
 :description "make-test-instance <device-font>"
 (is-true (make-instance '<device-font> :port (find-test-port)
			 :font :test-font)))




;;; DUIM-Sheets classes
;;; https://github.com/dylan-lang/opendylan/blob/30b4a9192eecf4f5458c88f3901866af9dc1cf90/sources/duim/tests/core/classes.dylan#L52

(defvar *dummy-sheet*  (make-test-pane  (find-class '<simple-pane>)))
(defvar *dummy-gadget* (make-test-pane  (find-class '<push-button>)))
(defvar *dummy-frame*  (make-test-frame (find-class '<test-frame>)))

(test make-test-<undefined-text-style-mapping>-instance
  :descrption "make-test-instance <undefined-text-style-mapping>"
  (is-true (make-instance '<undefined-text-style-mapping>
			  :port (find-test-port)
			  :text-style *default-text-style*)))

(test make-test-<application-exited-event>-instance
  :descrption "make-test-instance <application-exited-event>"
  (is-true (make-instance '<application-exited-event>
			  :frame *dummy-frame*)))

(test make-test-<button-press-event>-instance
  :descrption "make-test-instance <button-press-event>"
  (is-true (make-instance '<button-press-event>
			  :sheet *dummy-sheet*
			  :pointer (find-test-pointer))))

(test make-test-<button-release-event>-instance
  :descrption "make-test-instance <button-release-event>"
  (is-true (make-instance '<button-release-event>
			  :sheet *dummy-sheet*
			  :pointer (find-test-pointer))))

(test make-test-<double-click-event>-instance
  :descrption "make-test-instance <double-click-event>"
  (is-true (make-instance '<double-click-event>
			  :sheet *dummy-sheet*
			  :pointer (find-test-pointer))))

(test make-test-<key-press-event>-instance
  :description "make-test-instance <key-press-event>"
  (is-true (make-instance '<key-press-event> :sheet *dummy-sheet*)))

(test make-test-<key-release-event>-instance
  :description "make-test-instance <key-release-event>"
  (is-true (make-instance '<key-release-event> :sheet *dummy-sheet*)))

(test make-test-<frame-created-event>-instance
  :description "make-test-instance <frame-created-event>"
  (is-true (make-instance '<frame-created-event> :frame *dummy-frame*)))

(test make-test-<frame-destroyed-event>-instance
  :description "make-test-instance <frame-destroyed-event>"
  (is-true (make-instance '<frame-destroyed-event> :frame *dummy-frame*)))

(test make-test-<frame-exit-event>-instance
  :description "make-test-instance <frame-exit-event>"
  (is-true (make-instance '<frame-exit-event> :frame *dummy-frame*)))

(test make-test-<frame-exited-event>-instance
  :description "make-test-instance <frame-exited-event>"
  (is-true (make-instance '<frame-exited-event> :frame *dummy-frame*)))

(test make-test-<pointer-boundary-event>-instance
  :description "make-test-instance <pointer-boundary-event>"
  (is-true (make-instance '<pointer-boundary-event>
			  :sheet *dummy-sheet*
			  :pointer (find-test-pointer))))

(test make-test-<pointer-drag-event>-instance
  :description "make-test-instance <pointer-drag-event>"
  (is-true (make-instance '<pointer-drag-event>
			  :sheet *dummy-sheet*
			  :pointer (find-test-pointer))))

(test make-test-<pointer-enter-event>-instance
  :description "make-test-instance <pointer-enter-event>"
  (is-true (make-instance '<pointer-enter-event>
			  :sheet *dummy-sheet*
			  :pointer (find-test-pointer))))

(test make-test-<pointer-exit-event>-instance
  :description "make-test-instance <pointer-exit-event>"
  (is-true (make-instance '<pointer-exit-event>
			  :sheet *dummy-sheet*
			  :pointer (find-test-pointer))))

(test make-test-<pointer-motion-event>-instance
  :description "make-test-instance <pointer-motion-event>"
  (is-true (make-instance '<pointer-motion-event>
			  :sheet *dummy-sheet*
			  :pointer (find-test-pointer))))

(test make-test-<port-terminated-event>-instance
  :description "make-test-instance <port-terminated-event>"
  (is-true (make-instance '<port-terminated-event>
			  :frame *dummy-frame*
			  :condition (make-instance 'type-error
						    :expected-type 'character
						    :datum 10))))

(test make-test-<timer-event>-instance
  :description "make-test-instance <timer-event>"
  (is-true (make-instance '<timer-event> :frame *dummy-frame*)))

(test make-test-<window-configuration-event>-instance
  :description "make-test-instance <window-configuration-event>"
  (is-true (make-instance '<window-configuration-event>
			  :sheet *dummy-sheet*
			  :region *everywhere*)))

(test make-test-<window-repaint-event>-instance
  :description "make-test-instance <window-repaint-event>"
  (is-true (make-instance '<window-repaint-event>
			  :sheet *dummy-sheet*
			  :region *everywhere*)))


#|| These tests are not in Dylan either as of July 2020
define duim-sheets class-test <caret> ()
  //---*** Fill this in...
end class-test <caret>;

define duim-sheets class-test <clipboard> ()
  //---*** Fill this in...
end class-test <clipboard>;

define duim-sheets class-test <display> ()
  //---*** Fill this in...
end class-test <display>;

define duim-sheets class-test <medium> ()
  //---*** Fill this in...
end class-test <medium>;

define duim-sheets class-test <pointer> ()
  //---*** Fill this in...
end class-test <pointer>;

define duim-sheets class-test <port> ()
  //---*** Fill this in...
end class-test <port>;

define duim-sheets class-test <sheet> ()
  //---*** Fill this in...
end class-test <sheet>;

define duim-sheets class-test <undefined-text-style-mapping> ()
  //---*** Fill this in...
end class-test <undefined-text-style-mapping>;
||#

;;; Frame instances
;;; https://github.com/dylan-lang/opendylan/blob/30b4a9192eecf4f5458c88f3901866af9dc1cf90/sources/duim/tests/core/classes.dylan#L200

(test make-test-<functional-command>-instance
  :description "make-test-instance <functional-command>"
  (is-true (make-instance '<functional-command>
			  :function #'identity
			  :server nil)))


;;; Extended Geometry classes
;;; https://github.com/dylan-lang/opendylan/blob/30b4a9192eecf4f5458c88f3901866af9dc1cf90/sources/duim/tests/core/classes.dylan#L213

(test make-test-<ellipse>-instance
  :description "make-test-instance <ellipse>"
  (is-true (make-ellipse* (make-point 100 100) 100 100 100 100)))

(test make-test-<elliptical-arc>-instance
  :description "make-test-instance <elliptical-arc>"
  (is-true (make-elliptical-arc 100 100 100 100 100 100)))

(test make-test-<line>-instance
  :description "make-test-instance <line>"
  (is-true (make-line* (make-point 0 0) (make-point 100 100))))

(test make-test-<polygon>-instance
  :description "make-test-instance <polygon>"
  (is-true (make-polygon* #())))

(test make-test-<polyline>-instance
  :description "make-test-instance <polyline>"
  (is-true (make-polyline* #())))

(test make-test-<rectangle>-instance
  :description "make-test-instance <rectangle>"
  (is-true (make-rectangle* (make-point 0 0) (make-point 100 100))))


;;; Gadget pane classes
#||
/*---*** Removed duim-gadget-panes tests
define sideways method make-test-instance
    (class == <table-control-pane>) => (instance :: <table-control-pane>)
  make(<table-control-pane>,
       headings: #["Heading"], generators: vector(identity))
end method make-test-instance;

define sideways method make-test-instance
    (class == <tree-control-pane>) => (instance :: <tree-control-pane>)
  make(<tree-control-pane>, children-generator: always(#[]))
end method make-test-instance;
*/
||#

;;; Could use * "(5am:run! 'duim-tests::duim-geometry)" instead...
(defun test-classes ()
  (run! 'duim-classes))
