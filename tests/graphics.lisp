;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-TESTS -*-
(in-package #:duim-tests)

(def-suite duim-graphics
    :description "DUIM-GRAPHICS test suite"
    :in all-tests)
(in-suite duim-graphics)

#||
define duim-graphics class-test <pixmap> ()
  //---*** Fill this in...
end class-test <pixmap>;

define duim-graphics class-test <pixmap-medium> ()
  //---*** Fill this in...
end class-test <pixmap-medium>;


/// Graphic modelling

define method record-operation 
    (medium :: <test-medium>, type, #rest args) => ()
  add!(graphic-operations(medium), apply(list, type, args))
end method record-operation;
||#

(defmethod record-operation ((medium <test-medium>) type &rest args)
  (add! (graphic-operations medium) (apply #'list type args)))


#||
define method draw-point
    (medium :: <test-medium>, x, y) => (record)
  record-operation(medium, #"point", x, y);
  #f
end method draw-point;
||#

(defmethod draw-point ((medium <test-medium>) x y)
  (record-operation medium :point x y)
  nil)


#||
define method draw-points 
    (medium :: <test-medium>, coord-seq :: <coordinate-sequence>) => (record)
  record-operation(medium, #"points", coord-seq);
  #f
end method draw-points;
||#

(defmethod draw-points ((medium <test-medium>) coord-seq)
  (check-type coord-seq <coordinate-sequence>)
  (record-operation medium :points coord-seq)
  nil)


#||
define method draw-line
    (medium :: <test-medium>, x1, y1, x2, y2) => (record)
  record-operation(medium, #"line", x1, y1, x2, y2);
  #f
end method draw-line;
||#

(defmethod draw-line ((medium <test-medium>) x1 y1 x2 y2)
  (record-operation medium :line x1 y1 x2 y2)
  nil)


#||
define method draw-lines 
    (medium :: <test-medium>, coord-seq :: <coordinate-sequence>) => (record)
  record-operation(medium, #"lines", coord-seq);
  #f
end method draw-lines;
||#

(defmethod draw-lines ((medium <test-medium>) coord-seq)
  (check-type coord-seq <coordinate-sequence>)
  (record-operation medium :lines coord-seq)
  nil)


#||
define method draw-rectangle
    (medium :: <test-medium>, left, top, right, bottom,
     #key filled? = #t) => (record)
  record-operation(medium, #"rectangle", left, top, right, bottom, filled?);
  #f
end method draw-rectangle;
||#

(defmethod draw-rectangle ((medium <test-medium>) left top right bottom &key (filled? t))
  (record-operation medium :rectangle left top right bottom filled?)
  nil)


#||
define method draw-rectangles
    (medium :: <test-medium>, coord-seq :: <coordinate-sequence>,
     #key filled? = #t) => (record)
  record-operation(medium, #"rectangles", coord-seq, filled?);
  #f
end method draw-rectangles;
||#

(defmethod draw-rectangles ((medium <test-medium>) coord-seq &key (filled? t))
  (check-type coord-seq <coordinate-sequence>)
  (record-operation medium :rectangles coord-seq filled?)
  nil)


#||
define method draw-rounded-rectangle
    (medium :: <test-medium>, left, top, right, bottom, 
     #key filled? = #t, radius) => (record)
  record-operation
    (medium, #"rectangle", left, top, right, bottom, filled?, radius);
  #f
end method draw-rounded-rectangle;
||#

(defmethod draw-rounded-rectangle ((medium <test-medium>) left top right bottom
				   &key (filled? t) radius)
  (record-operation medium :rectangle left top right bottom filled? radius)
  nil)


#||
define method draw-polygon
    (medium :: <test-medium>, coord-seq :: <coordinate-sequence>,
     #key closed? = #t, filled? = #t) => (record)
  record-operation(medium, #"polygon", coord-seq, closed?, filled?);
  #f
end method draw-polygon;
||#

(defmethod draw-polygon ((medium <test-medium>) coord-seq
			 &key (closed? t) (filled? t))
  (check-type coord-seq <coordinate-sequence>)
  (record-operation medium :polygon coord-seq closed? filled?)
  nil)


#||
define method draw-ellipse
    (medium :: <test-medium>, center-x, center-y, radius-1-dx,
     radius-1-dy, radius-2-dx, radius-2-dy,
     #key start-angle, end-angle, filled? = #t) => (record)
  record-operation(medium, #"ellipse", center-x, center-y, 
                   radius-1-dx, radius-1-dy, radius-2-dx, radius-2-dy,
                   start-angle | 0.0, end-angle | $2pi, filled?);
  #f
end method draw-ellipse;
||#

(defmethod draw-ellipse ((medium <test-medium>) center-x center-y
			 radius-1-dx radius-1-dy radius-2-dx radius-2-dy
			 &key start-angle end-angle (filled? t))
  (record-operation medium :ellipse center-x center-y
		    radius-1-dx radius-1-dy radius-2-dx radius-2-dy
		    (or start-angle 0.0) (or end-angle +2pi+) filled?)
  nil)


#||
define method draw-pixmap
    (medium :: <test-medium>, pixmap :: <pixmap>, x :: <real>, y :: <real>,
     #key function = $boole-1) => (record)
  ignore(function);
  record-operation(medium, #"pixmap", pixmap, x, y);
  #f
end method draw-pixmap;
||#

(defmethod draw-pixmap ((medium <test-medium>) (pixmap <pixmap>) (x real) (y real)
			&key (function +boole-1+))
  (declare (ignore function))
  (record-operation medium :pixmap pixmap x y)
  nil)


#||
define method draw-text
    (medium :: <test-medium>, string :: <string>, x, y,
     #key start, end: _end, align-x = #"left", align-y = #"baseline", do-tabs?,
          towards-x, towards-y, transform-glyphs?) => (record)
  record-operation(medium, #"text", string, x, y);
  #f
end method draw-text;
||#

(defmethod draw-text ((medium <test-medium>) (string string) x y
		      &key start end (align-x :left) (align-y :baseline) do-tabs?
		      towards-x towards-y transform-glyphs?)
  (declare (ignore start end align-x align-y do-tabs? towards-x towards-y
		   transform-glyphs?))
  (record-operation medium :text string x y))


#||
define method draw-text
    (medium :: <test-medium>, character :: <character>, x, y,
     #key start, end: _end, align-x = #"left", align-y = #"baseline", do-tabs?,
          towards-x, towards-y, transform-glyphs?) => (record)
  record-operation(medium, #"character", character, x, y);
  #f
end method draw-text;
||#

(defmethod draw-text ((medium <test-medium>) (character character) x y
		      &key start end (align-x :left) (align-y :baseline) do-tabs?
			towards-x towards-y transform-glyphs?)
  (declare (ignore start end align-x align-y do-tabs? towards-x towards-y
		   transform-glyphs?))
  (record-operation medium :character character x y))


#||
define method start-path (medium :: <test-medium>) => (record)
  record-operation(medium, #"start-path")
end method start-path;
||#

(defmethod start-path ((medium <test-medium>))
  (record-operation medium :start-path))


#||
define method end-path (medium :: <test-medium>) => (record)
  record-operation(medium, #"end-path")
end method end-path;
||#

(defmethod end-path ((medium <test-medium>))
  (record-operation medium :end-path))


#||
define method abort-path (medium :: <test-medium>) => (record)
  record-operation(medium, #"abort-path")
end method abort-path;
||#

(defmethod abort-path ((medium <test-medium>))
  (record-operation medium :abort-path))


#||
define method close-path (medium :: <test-medium>) => (record)
  record-operation(medium, #"close-path")
end method close-path;
||#

(defmethod close-path ((medium <test-medium>))
  (record-operation medium :close-path))


#||
define method stroke-path (medium :: <test-medium>, #key filled?) => (record)
  ignore(filled?);
  record-operation(medium, #"stroke-path")
end method stroke-path;
||#

(defmethod stroke-path ((medium <test-medium>) &key filled?)
  (declare (ignore filled?))
  (record-operation medium :stroke-path))


#||
define method fill-path (medium :: <test-medium>) => (record)
  record-operation(medium, #"fill-path")
end method fill-path;
||#

(defmethod fill-path ((medium <test-medium>))
  (record-operation medium :fill-path))


#||
define method clip-from-path
    (medium :: <test-medium>, #key function = $boole-and) => (record)
  record-operation(medium, #"clip-from-path")
end method clip-from-path;
||#

(defmethod clip-from-path ((medium <test-medium>)
			   &key (function +boole-and+))
  (declare (ignore function))
  (record-operation medium :clip-from-path))


#||
define method save-clipping-region (medium :: <test-medium>) => (record)
  record-operation(medium, #"save-clipping-region")
end method save-clipping-region;
||#

(defmethod save-clipping-region ((medium <test-medium>))
  (record-operation medium :save-clipping-region))


#||
define method restore-clipping-region (medium :: <test-medium>) => (record)
  record-operation(medium, #"restore-clipping-region")
end method restore-clipping-region;
||#

(defmethod restore-clipping-region ((medium <test-medium>))
  (record-operation medium :restore-clipping-region))


#||
define method move-to (medium :: <test-medium>, x, y) => (record)
  record-operation(medium, #"move-to", x, y)
end method move-to;
||#

(defmethod move-to ((medium <test-medium>) x y)
  (record-operation medium :move-to x y))


#||
define method line-to (medium :: <test-medium>, x, y) => (record)
  record-operation(medium, #"line-to", x, y)
end method line-to;
||#

(defmethod line-to ((medium <test-medium>) x y)
  (record-operation medium :line-to x y))


#||
define method arc-to 
    (medium :: <test-medium>, center-x, center-y,
     radius-1-dx, radius-1-dy, radius-2-dx, radius-2-dy,
     #key start-angle, end-angle) => (record)
  record-operation(medium, #"arc-to", center-x, center-y,
		   radius-1-dx, radius-1-dy, radius-2-dx, radius-2-dy)
end method arc-to;
||#

(defmethod arc-to ((medium <test-medium>) center-x center-y
		   radius-1-dx radius-1-dy radius-2-dx radius-2-dy
		   &key start-angle end-angle)
  (declare (ignore start-angle end-angle))
  (record-operation medium :arc-to center-x center-y
		    radius-1-dx radius-1-dy radius-2-dx radius-2-dy))


#||
define method curve-to 
    (medium :: <test-medium>, x1, y1, x2, y2, x3, y3) => (record)
  record-operation(medium, #"curve-to", x1, y1, x2, y2, x3, y3)
end method curve-to;
||#

(defmethod curve-to ((medium <test-medium>) x1 y1 x2 y2 x3 y3)
  (record-operation medium :curve-to x1 y1 x2 y2 x3 y3))


#||
/// Graphic tests

define method make-graphics-test-sheet ()
  let sheet = make-test-pane(<drawing-pane>);
  let frame = make-test-frame(<test-frame>, layout: sheet);
  sheet
end method make-graphics-test-sheet;
||#

(defun make-graphics-test-sheet ()
  (let* ((sheet (make-test-pane (find-class '<drawing-pane>)))
	 (frame (make-test-frame (find-class '<test-frame>) :layout sheet)))
    (declare (ignore frame))
    sheet))


#||
define method check-operation 
    (name, medium :: <test-medium>, type, #rest args)
  let operations = graphic-operations(medium);
  check(concatenate(name, " caused correct number of operations"),
        \=, size(operations), 1);
  check(name, \=,
        size(operations) > 0 & operations[0], apply(list, type, args))
end method check-operation;
||#

(defmethod check-operation (name (medium <test-medium>) type &rest args)
  (let ((operations (graphic-operations medium)))
    (is (equal? (length operations) 1)
	(concatenate 'string name " caused correct number of operations"))
    (is (equal? (and (> (length operations) 0) (aref operations 0))
		(apply #'list type args))
	name)))


#||
define method check-operations-happened (name, medium)
  check-true(name, size(graphic-operations(medium)) > 0)
end method check-operations-happened;
||#

(defmethod check-operations-happened (name medium)
  (is (> (length (graphic-operations medium)) 0) name))


#||
define method check-ellipse-operation 
    (name, medium :: <test-medium>, type, #rest args)
  let operations = graphic-operations(medium);
  let operation = operations[0];
  check(name, \=,
        normalize-ellipse-operation(operation),
        normalize-ellipse-operation(apply(list, type, args)))
end method check-ellipse-operation;
||#

(defmethod check-ellipse-operation (name (medium <test-medium>) type &rest args)
  (let* ((operations (graphic-operations medium))
	 (operation  (aref operations 0)))
    (is (equal? (normalize-ellipse-operation operation)
		(normalize-ellipse-operation (apply #'list type args)))
	name)))


#||
define method mod (value, divisor)
  let (integer, modulo) = floor/(value, divisor);
  ignore(integer);
  modulo
end method mod;
||#

(defun dmod (value divisor)
  (multiple-value-bind (integer modulo)
      (floor value divisor)
    (declare (ignore integer))
    modulo))


#||
define method normalize-ellipse-angle (angle)
  let new-angle = mod(angle, $pi);
  floor(new-angle * 10000)
end method normalize-ellipse-angle;
||#

(defmethod normalize-ellipse-angle (angle)
  (let ((new-angle (dmod angle +pi+)))
    (floor (* new-angle 10000))))


#||
define method normalize-ellipse-operation (operation)
  if (operation[0] = #"ellipse")
    operation[7] := normalize-ellipse-angle(operation[7]);
    operation[8] := normalize-ellipse-angle(operation[8]);
  end;
  operation
end method normalize-ellipse-operation;
||#

(defun normalize-ellipse-operation (operation)
  (when (equal? (SEQUENCE-ELT operation 0) :ellipse)
    (setf (SEQUENCE-ELT operation 7)
	  (normalize-ellipse-angle (SEQUENCE-ELT operation 7))
	  (SEQUENCE-ELT operation 8)
	  (normalize-ellipse-angle (SEQUENCE-ELT operation 8))))
  operation)


#||
define method test-draw-rectangle (#key filled?)
  let sheet = make-graphics-test-sheet();
  with-sheet-medium (medium = sheet)
    let name
      = select (filled?)
          #t => "Draw Filled Rectangle";
          #f => "Draw Rectangle";
        end;
    draw-rectangle*(medium, make-point(100, 100), make-point(200, 200),
		    filled?: filled?);
    check-operation(name, medium,
                    #"rectangle", 100, 100, 200, 200, filled?);
    clear-graphic-operations(medium);
    draw-rectangle(medium, 100, 100, 200, 200, filled?: filled?);
    check-operation(concatenate("Split ", name), medium,
                    #"rectangle", 100, 100, 200, 200, filled?);
    medium
  end
end method test-draw-rectangle;
||#

(defun test-draw-rectangle (&key filled?)
  (let ((sheet (make-graphics-test-sheet)))
    (with-sheet-medium (medium = sheet)
      (let ((name (if filled?
		      "Draw Filled Rectangles"
		      "Draw Rectangle")))
	(draw-rectangle* medium (make-point 100 100) (make-point 200 200) :filled? filled?)
	(check-operation name medium :rectangle 100 100 200 200 filled?)
	(clear-graphic-operations medium)
	(draw-rectangle medium 100 100 200 200 :filled? filled?)
	(check-operation (concatenate 'string "Split " name) medium
			 :rectangle 100 100 200 200 filled?))
      medium)))


#||
define method test-draw-rectangles (#key filled?) => ()
  let sheet = make-graphics-test-sheet();
  with-sheet-medium (medium = sheet)
    let name
      = select (filled?)
          #t => "Draw Filled Rectangles";
          #f => "Draw Rectangles";
        end;
    let points
      = vector(make-point(100, 150), make-point(200, 250),
	       make-point(300, 350), make-point(400, 450));
    draw-rectangles*(medium, points, filled?: filled?);
    check-operation(name, medium,
                    #"rectangles", 
                    vector(100, 150, 200, 250, 300, 350, 400, 450),
                    filled?);
    clear-graphic-operations(medium);
    let coordinates = vector(100, 150, 200, 250, 300, 350, 400, 450);
    draw-rectangles(medium, coordinates, filled?: filled?);
    check-operation(concatenate("Split ", name), medium,
                    #"rectangles", 
                    coordinates,
                    filled?);
  end
end method test-draw-rectangles;
||#

(defun test-draw-rectangles (&key filled?)
  (let ((sheet (make-graphics-test-sheet)))
    (with-sheet-medium (medium = sheet)
      (let ((name (if filled?
		      "Draw Filled Rectangles"
		      "Draw Rectangles"))
	    (points (vector (make-point 100 150) (make-point 200 250)
			    (make-point 300 350) (make-point 400 450))))
	(draw-rectangles* medium points :filled? filled?)
	(check-operation name medium
			 :rectangles
			 (vector 100 150 200 250 300 350 400 450)
			 filled?)
	(clear-graphic-operations medium)
	(let ((coordinates (vector 100 150 200 250 300 350 400 450)))
	  (draw-rectangles medium coordinates :filled? filled?)
	  (check-operation (concatenate 'string "Split " name)
			   medium
			   :rectangles
			   coordinates
			   filled?))))))


#||
define method test-polygons (#key filled?, closed?) => ()
  let sheet = make-graphics-test-sheet();
  with-sheet-medium (medium = sheet)
    let name = "Polygon";
    if (filled?) name := concatenate("Filled ", name) end;
    if (closed?) name := concatenate("Closed ", name) end; 
    draw-polygon*(medium,
		  vector(make-point(100, 100),
			 make-point(200, 200),
			 make-point(100, 300)),
		  closed?: closed?,
		  filled?: filled?);
    check-operation(name, medium,
                    #"polygon", vector(100, 100, 200, 200, 100, 300),
                    closed?, filled?);
    clear-graphic-operations(medium);
    draw-polygon(medium,
		 vector(100, 100,
			200, 200,
			100, 300),
		 closed?: closed?,
		 filled?: filled?);
    check-operation(concatenate("Split ", name), medium,
                    #"polygon", vector(100, 100, 200, 200, 100, 300),
                    closed?, filled?);
    medium
  end
end method test-polygons;
||#

(defmethod test-polygons (&key filled? closed?)
  (let ((sheet (make-graphics-test-sheet)))
    (with-sheet-medium (medium = sheet)
      (let ((name "Polygon"))
	(when filled? (setf name (concatenate 'string "Filled " name)))
	(when closed? (setf name (concatenate 'string "Closed " name)))
	(draw-polygon* medium (vector (make-point 100 100)
				      (make-point 200 200)
				      (make-point 100 300))
		       :closed? closed?
		       :filled? filled?)
	(check-operation name medium :polygon
			 (vector 100 100 200 200 100 300) closed? filled?)
	(clear-graphic-operations medium)
	(draw-polygon medium (vector 100 100 200 200 100 300)
		      :closed? closed? :filled? filled?)
	(check-operation (concatenate 'string "Split " name) medium
			 :polygon (vector 100 100 200 200 100 300)
			 closed? filled?))
      medium)))


#||
define method test-ellipses (#key filled?, start-angle, end-angle) => ()
  let sheet = make-graphics-test-sheet();
  with-sheet-medium (medium = sheet)
    let name = "Ellipse";
    if (filled?) name := concatenate("Filled ", name) end;
    if (start-angle) name := concatenate(name, " [start]") end;
    if (end-angle) name := concatenate(name, " [end]") end;
    draw-ellipse*(medium,
		  make-point(100, 100),
		  100, 150, 200, 250,
		  start-angle: start-angle,
		  end-angle: end-angle,
		  filled?: filled?);
    check-ellipse-operation(name, medium,
			    #"ellipse", 100, 100, 100, 150, 200, 250,
			    start-angle | 0.0, end-angle | $2pi, filled?);
    clear-graphic-operations(medium);
    draw-ellipse(medium,
		 100, 100,
		 100, 150, 200, 250,
		 start-angle: start-angle,
		 end-angle: end-angle,
		 filled?: filled?) ;
    check-ellipse-operation(concatenate("Split ", name), medium,
			    #"ellipse", 100, 100, 100, 150, 200, 250,
			    start-angle | 0.0, end-angle | $2pi, filled?);
    medium
  end
end method test-ellipses;
||#

(defmethod test-ellipses (&key filled? start-angle end-angle)
  (let ((sheet (make-graphics-test-sheet)))
    (with-sheet-medium (medium = sheet)
      (let ((name "Ellipse"))
        (when filled? (setf name (concatenate 'string "Filled " name)))
	(when start-angle (setf name (concatenate 'string name " [start]")))
	(when end-angle (setf name (concatenate 'string name " [end]")))
	(draw-ellipse* medium
                       (make-point 100 100)
		       100 150 200 250
		       :start-angle start-angle
		       :end-angle end-angle
		       :filled? filled?)
	(check-ellipse-operation name medium
				 :ellipse 100 100 100 150 200 250
				 (or start-angle 0.0) (or end-angle +2pi+) filled?)
	(clear-graphic-operations medium)
	(draw-ellipse medium 100 100
		      100 150 200 250
		      :start-angle start-angle
		      :end-angle end-angle
		      :filled? filled?)
	(check-ellipse-operation (concatenate 'string "Split " name)
				 medium :ellipse
				 100 100 100 150 200 250
				 (or start-angle 0.0) (or end-angle +2pi+)
				 filled?))
      medium)))


#||
define method test-circles (#key filled?, start-angle, end-angle) => ()
  let sheet = make-graphics-test-sheet();
  with-sheet-medium (medium = sheet)
    let name = "Circle";
    if (filled?) name := concatenate("Filled ", name) end;
    if (start-angle) name := concatenate(name, " [start]") end;
    if (end-angle) name := concatenate(name, " [end]") end;
    draw-circle*(medium,
		 make-point(50, 100),
		 150,
		 start-angle: start-angle,
		 end-angle: end-angle,
		 filled?: filled?);
    check-ellipse-operation(name, medium,
			    #"ellipse", 50, 100, 150, 0, 0, 150,
			    start-angle | 0.0, end-angle | $2pi, filled?);
    clear-graphic-operations(medium);
    draw-circle(medium,
		50, 100, 150,
		start-angle: start-angle,
		end-angle: end-angle,
		filled?: filled?);
  check-ellipse-operation(concatenate("Split ", name), medium,
			  #"ellipse", 50, 100, 150, 0, 0, 150,
			  start-angle | 0.0, end-angle | $2pi, filled?);
  medium
end
  end method test-circles;
||#

(defmethod test-circles (&key filled? start-angle end-angle)
  (let ((sheet (make-graphics-test-sheet)))
    (with-sheet-medium (medium = sheet)
      (let ((name "Circle"))
	(when filled? (setf name (concatenate 'string "Filled " name)))
	(when start-angle (setf name (concatenate 'string name " [start]")))
	(when end-angle (setf name (concatenate 'string name " [end]")))
	(draw-circle* medium (make-point 50 100) 150
		      :start-angle start-angle
		      :end-angle end-angle
		      :filled? filled?)
	(check-ellipse-operation name medium :ellipse 50 100 150 0 0 150
				 (or start-angle 0.0) (or end-angle +2pi+)
				 filled?)
	(clear-graphic-operations medium)
	(draw-circle medium 50 100 150 :start-angle start-angle
		     :end-angle end-angle :filled? filled?)
	(check-ellipse-operation (concatenate 'string "Split " name) medium
				 :ellipse 50 100 150 0 0 150
				 (or start-angle 0.0) (or end-angle +2pi+)
				 filled?))
      medium)))


#||
define method test-ovals (#key filled?, start-angle, end-angle) => ()
  let sheet = make-graphics-test-sheet();
  with-sheet-medium (medium = sheet)
    let name = "Ellipse";
  if (filled?) name := concatenate("Filled ", name) end;
  if (start-angle) name := concatenate(name, " [start]") end;
  if (end-angle) name := concatenate(name, " [end]") end;
  draw-oval*(medium,
	     make-point(200, 200),
	     100, 150,
	     start-angle: start-angle,
	     end-angle: end-angle,
	     filled?: filled?) ;
    check-operations-happened(name, medium);
    clear-graphic-operations(medium);
    draw-oval(medium,
	      200, 200,
	      100, 150,
	      start-angle: start-angle,
	      end-angle: end-angle,
	      filled?: filled?);
    check-operations-happened(concatenate("Split ", name), medium);
    medium
  end
end method test-ovals;
||#

(defun test-ovals (&key filled? start-angle end-angle)
  (let ((sheet (make-graphics-test-sheet)))
    (with-sheet-medium (medium = sheet)
      (let ((name "Ellipse"))
	(when filled? (setf name (concatenate 'string "Filled " name)))
	(when start-angle (setf name (concatenate 'string name " [start]")))
	(when end-angle (setf name (concatenate 'string name " [end]")))
	(draw-oval* medium (make-point 200 200) 100 150
		    :start-angle start-angle
		    :end-angle end-angle
		    :filled? filled?)
	(check-operations-happened name medium)
	(clear-graphic-operations medium)
	(draw-oval medium 200 200 100 150 :start-angle start-angle
		   :end-angle end-angle :filled? filled?)
	(check-operations-happened (concatenate 'string "Split " name) medium))
      medium)))


#||
define method test-text-drawing 
    (string-or-char :: type-union(<string>, <character>)) => ()
  let sheet = make-graphics-test-sheet();
  with-sheet-medium (medium = sheet)
    let name
      = if (instance?(string-or-char, <string>))
          "Text"
        else
          "Character"
        end;
    let type
      = if (instance?(string-or-char, <string>))
          #"text"
        else
          #"character"
        end;
    draw-text*(medium, string-or-char, make-point(100, 100));
    check-operation(name, medium, type, string-or-char, 100, 100);
    clear-graphic-operations(medium);
    draw-text(medium, string-or-char, 100, 100);
    check-operation(concatenate("Split ", name), medium, type,
                    string-or-char, 100, 100);
    medium
  end
end method test-text-drawing;
||#

(defun test-text-drawing (string-or-char)
  (let ((sheet (make-graphics-test-sheet)))
    (with-sheet-medium (medium = sheet)
      (let ((name (typecase string-or-char
		    (string "Text")
		    (t "Character")))
	    (type (typecase string-or-char
		    (string :text)
		    (t :character))))
	(draw-text* medium string-or-char (make-point 100 100))
	(check-operation name medium type string-or-char 100 100)
	(clear-graphic-operations medium)
	(draw-text medium string-or-char 100 100)
	(check-operation (concatenate 'string "Split " name)
			 medium type string-or-char 100 100))
      medium)))


#||
define method test-pixmaps () => ()
  let sheet = make-graphics-test-sheet();
  with-sheet-medium (medium = sheet)
    let name = "Pixmap";
    let pixmap = make-pixmap(medium, 100, 100);
    draw-pixmap*(medium, pixmap, make-point(100, 100));
    check-operation(name, medium, #"pixmap", pixmap, 100, 100);
    clear-graphic-operations(medium);
    draw-pixmap(medium, pixmap, 100, 100);
    check-operation(concatenate("Split ", name), medium,
                    #"pixmap", pixmap, 100, 100);
    medium
  end
end method test-pixmaps;
||#

(defun test-pixmaps ()
  (let ((sheet (make-graphics-test-sheet)))
    (with-sheet-medium (medium = sheet)
      (let ((name "Pixmap")
	    (pixmap (make-pixmap medium 100 100)))
	(draw-pixmap* medium pixmap (make-point 100 100))
	(check-operation name medium :pixmap pixmap 100 100)
	(clear-graphic-operations medium)
	(draw-pixmap medium pixmap 100 100)
	(check-operation (concatenate 'string "Split " name) medium
			 :pixmap pixmap 100 100))
      medium)))


#||
define method test-arrow-drawing (name, #rest args) => ()
  let sheet = make-graphics-test-sheet();
  with-sheet-medium (medium = sheet)
    apply(draw-arrow*, medium, 
          make-point(100, 150), make-point(200, 250),
          args);
    check-operations-happened(name, medium);
    clear-graphic-operations(medium);
    apply(draw-arrow, medium, 100, 150, 200, 250, args);
    check-operations-happened(concatenate("Split ", name), medium);
    medium
  end
end method test-arrow-drawing;
||#

(defun test-arrow-drawing (name &rest args)
  (let ((sheet (make-graphics-test-sheet)))
    (with-sheet-medium (medium = sheet)
      (apply #'draw-arrow* medium (make-point 100 150) (make-point 200 250)
	     args)
      (check-operations-happened name medium)
      (clear-graphic-operations medium)
      (apply #'draw-arrow medium 100 150 200 250 args)
      (check-operations-happened (concatenate 'string "Split " name) medium)
      medium)))


#||
define method test-triangle-drawing (name, #rest args)
  let sheet = make-graphics-test-sheet();
  with-sheet-medium (medium = sheet)
    apply(draw-triangle*, medium, 
          make-point(100, 150), make-point(200, 250), make-point(300, 350),
          args);
    check-operations-happened(name, medium);
    clear-graphic-operations(medium);
    apply(draw-triangle, medium, 100, 150, 200, 250, 300, 350, args);
    check-operations-happened(concatenate("Split ", name), medium);
    medium
  end
end method test-triangle-drawing;
||#

(defun test-triangle-drawing (name &rest args)
  (let ((sheet (make-graphics-test-sheet)))
    (with-sheet-medium (medium = sheet)
      (apply #'draw-triangle* medium
	     (make-point 100 150) (make-point 200 250) (make-point 300 350)
	     args)
      (check-operations-happened name medium)
      (clear-graphic-operations medium)
      (apply #'draw-triangle medium 100 150 200 250 300 350 args)
      (check-operations-happened (concatenate 'string "Split " name) medium)
      medium)))


#||
define method test-bezier-curves (name, #rest args)
  let sheet = make-graphics-test-sheet();
  with-sheet-medium (medium = sheet)
    apply(draw-bezier-curve*, medium, 
          vector(make-point(100, 150), make-point(200, 250),
                 make-point(300, 350), make-point(400, 450)),
          args);
    check-operations-happened(name, medium);
    apply(draw-bezier-curve, medium,
          vector(100, 150, 200, 250, 300, 350, 400, 450),
          args);
    check-operations-happened(concatenate("Split ", name), medium);
    medium
  end
end method test-bezier-curves;
||#

(defun test-bezier-curves (name &rest args)
  (let ((sheet (make-graphics-test-sheet)))
    (with-sheet-medium (medium = sheet)
      (apply #'draw-bezier-curve* medium
	     (vector (make-point 100 150) (make-point 200 250)
		     (make-point 300 350) (make-point 400 450))
	     args)
      (check-operations-happened name medium)
      (apply #'draw-bezier-curve medium
	     (vector 100 150 200 250 300 350 400 450)
	     args)
      (check-operations-happened (concatenate 'string "Split " name) medium)
      medium)))


#||
define method test-draw-regular-polygon (name, edges, #rest args)
  let sheet = make-graphics-test-sheet();
  with-sheet-medium (medium = sheet)
    apply(draw-regular-polygon*, medium, 
          make-point(100, 150), make-point(200, 250),
          edges,
          args);
    check-operations-happened(name, medium);
    apply(draw-regular-polygon, medium,
          100, 150, 200, 250, edges,
          args);
    check-operations-happened(concatenate("Split ", name), medium);
    medium
  end
end method test-draw-regular-polygon;
||#

(defun test-draw-regular-polygon (name edges &rest args)
  (let ((sheet (make-graphics-test-sheet)))
    (with-sheet-medium (medium = sheet)
      (apply #'draw-regular-polygon* medium
	     (make-point 100 150) (make-point 200 250)
	     edges args)
      (check-operations-happened name medium)
      (apply #'draw-regular-polygon medium 100 150 200 250 edges args)
      (check-operations-happened (concatenate 'string "Split " name) medium)
      medium)))


#||
define method test-draw-lines ()
  let sheet = make-graphics-test-sheet();
  with-sheet-medium (medium = sheet)
    let name = "Draw Lines";
    draw-lines*(medium, 
		vector(make-point(100, 150), make-point(200, 250),
		       make-point(300, 350), make-point(400, 450)));
    check-operation(name, medium, #"lines",
                    vector(100, 150, 200, 250, 300, 350, 400, 450));
    clear-graphic-operations(medium);
    draw-lines(medium,
	       vector(100, 150, 200, 250, 300, 350, 400, 450));
    check-operation(concatenate("Split ", name), medium, #"lines",
                    vector(100, 150, 200, 250, 300, 350, 400, 450));
    medium
  end
end method test-draw-lines;
||#

(defun test-draw-lines ()
  (let ((sheet (make-graphics-test-sheet)))
    (with-sheet-medium (medium = sheet)
      (let ((name "Draw Lines"))
	(draw-lines* medium
		     (vector (make-point 100 150) (make-point 200 250)
			     (make-point 300 350) (make-point 400 450)))
	(check-operation name medium :lines
			 (vector 100 150 200 250 300 350 400 450))
	(clear-graphic-operations medium)
	(draw-lines medium
		    (vector 100 150 200 250 300 350 400 450))
	(check-operation (concatenate 'string "Split " name) medium :lines
			 (vector 100 150 200 250 300 350 400 450)))
      medium)))


#||
define method test-draw-points ()
  let sheet = make-graphics-test-sheet();
  with-sheet-medium (medium = sheet)
    let name = "Draw Points";
    draw-points*(medium,
		 vector(make-point(100, 150), make-point(200, 250),
			make-point(300, 350), make-point(400, 450)));
    check-operation(name, medium, #"points",
                    vector(100, 150, 200, 250, 300, 350, 400, 450));
    clear-graphic-operations(medium);
    draw-points(medium,
		vector(100, 150, 200, 250, 300, 350, 400, 450));
    check-operation(concatenate("Split ", name), medium, #"points",
                    vector(100, 150, 200, 250, 300, 350, 400, 450));
    medium
  end
end method test-draw-points;
||#

(defun test-draw-points ()
  (let ((sheet (make-graphics-test-sheet)))
    (with-sheet-medium (medium = sheet)
      (let ((name "Draw Points"))
	(draw-points* medium
		      (vector (make-point 100 150) (make-point 200 250)
			      (make-point 300 350) (make-point 400 450)))
	(check-operation name medium :points
			 (vector 100 150 200 250 300 350 400 450))
	(clear-graphic-operations medium)
	(draw-points medium
		     (vector 100 150 200 250 300 350 400 450))
	(check-operation (concatenate 'string "Split " name) medium :points
			 (vector 100 150 200 250 300 350 400 450)))
      medium)))


#||
define test simple-graphics-test ()
  let sheet = make-graphics-test-sheet();
  with-sheet-medium (medium = sheet)
    draw-point*(medium, make-point(100, 100));
    check-operation("Draw Point", medium, #"point", 100, 100);
    clear-graphic-operations(medium);
    draw-point(medium, 100, 100);
    check-operation("Split Draw Point", medium, #"point", 100, 100);
    clear-graphic-operations(medium);
    draw-line*(medium, make-point(100, 100), make-point(200, 200));
    check-operation("Draw Line", medium, #"line", 100, 100, 200, 200);
    clear-graphic-operations(medium);
    draw-line(medium, 100, 100, 200, 200);
    check-operation("Split Draw Line", medium, #"line", 100, 100, 200, 200);
    clear-graphic-operations(medium);
  end;
end test simple-graphics-test;
||#

(test simple-graphics-test
      :description "simple graphics test"
      (let ((sheet (make-graphics-test-sheet)))
	(with-sheet-medium (medium = sheet)
	  (draw-point* medium (make-point 100 100))
	  (check-operation "Draw Point" medium :point 100 100)
	  (clear-graphic-operations medium)
	  (draw-point medium 100 100)
	  (check-operation "Split Draw Point" medium :point 100 100)
	  (clear-graphic-operations medium)
	  (draw-line* medium (make-point 100 100) (make-point 200 200))
	  (check-operation "Draw Line" medium :line 100 100 200 200)
	  (clear-graphic-operations medium)
	  (draw-line medium 100 100 200 200)
	  (check-operation "Split Draw Line" medium :line 100 100 200 200)
	  (clear-graphic-operations medium))))


#||
define test arrow-drawing-test ()
  test-arrow-drawing("Draw Arrow");
  test-arrow-drawing("Draw Reverse Arrow",
                     from-head?: #t, to-head?: #f);
  test-arrow-drawing("Draw Bi-directional Arrow",
                     from-head?: #t, to-head?: #t);
  test-arrow-drawing("Draw Long Arrow", head-length: 50);
  test-arrow-drawing("Draw Wide Arrow", head-width: 50);
end test arrow-drawing-test;
||#

(test arrow-drawing-test
      :description "arrow drawing test"
      (test-arrow-drawing "Draw Arrow")
      (test-arrow-drawing "Draw Reverse Arrow" :from-head? t :to-head? nil)
      (test-arrow-drawing "Draw Bi-directional Arrow" :from-head? t :to-head? t)
      (test-arrow-drawing "Draw Long Arrow" :head-length 50)
      (test-arrow-drawing "Draw Wide Arrow" :head-width 50))


#||
define test bezier-curve-drawing-test ()
  test-bezier-curves("Draw Bezier Curve");
  test-bezier-curves("Draw Filled Bezier Curve", filled?: #t);
end test bezier-curve-drawing-test;
||#

(test bezier-curve-drawing-test
      :description "bezier curve drawing test"
      (test-bezier-curves "Draw Bezier Curve")
      (test-bezier-curves "Draw Filled Bezier Curve" :filled? t))


#||
define test circle-drawing-test ()
  test-circles();
  test-circles(filled?: #t);
  test-circles(start-angle: 0.0);
  test-circles(end-angle: $pi);
  test-circles(start-angle: 0.0, end-angle: $pi);
end test circle-drawing-test;
||#

(test circle-drawing-test
      :description "circle drawing test"
      (test-circles)
      (test-circles :filled? t)
      (test-circles :start-angle 0.0)
      (test-circles :end-angle +pi+)
      (test-circles :start-angle 0.0 :end-angle +pi+))


#||
define test ellipse-drawing-test ()
  test-ellipses();
  test-ellipses(filled?: #t);
  test-ellipses(start-angle: 0.0);
  test-ellipses(end-angle: $pi);
  test-ellipses(start-angle: 0.0, end-angle: $pi);
end test ellipse-drawing-test;
||#

(test ellipse-drawing-test
      :description "ellipse drawing test"
      (test-ellipses)
      (test-ellipses :filled? t)
      (test-ellipses :start-angle 0.0)
      (test-ellipses :end-angle +pi+)
      (test-ellipses :start-angle 0.0 :end-angle +pi+))


#||
define test line-drawing-test ()
  test-draw-lines();
end test line-drawing-test;
||#

(test line-drawing-test
      :description "line drawing test"
      (test-draw-lines))


#||
define test oval-drawing-test ()
  test-ovals();
  test-ovals(filled?: #t);
  test-ovals(start-angle: 0.0);
  test-ovals(end-angle: $pi);
  test-ovals(start-angle: 0.0, end-angle: $pi);
end test oval-drawing-test;
||#

(test oval-drawing-test
      :description "oval drawing test"
      (test-ovals)
      (test-ovals :filled? t)
      (test-ovals :start-angle 0.0)
      (test-ovals :end-angle +pi+)
      (test-ovals :start-angle 0.0 :end-angle +pi+))


#||
define test pixmap-drawing-test ()
  test-pixmaps();
end test pixmap-drawing-test;
||#

(test pixmap-drawing-test
      :description "pixmap drawing test"
      (test-pixmaps))


#||
define test points-drawing-test ()
  test-draw-points();
end test points-drawing-test;
||#

(test points-drawing-test
      :description "points drawing test"
      (test-draw-points))


#||
define test polygon-drawing-test ()
  test-polygons();
  test-polygons(filled?: #t);
  test-polygons(closed?: #t);
  test-polygons(closed?: #t, filled?: #t);
end test polygon-drawing-test;
||#

(test polygon-drawing-test
      :description "polygon drawing test"
      (test-polygons)
      (test-polygons :filled? t)
      (test-polygons :closed? t)
      (test-polygons :closed? t :filled? t))


#||
define test rectangle-drawing-test ()
  test-draw-rectangle();
  test-draw-rectangle(filled?: #t);
  test-draw-rectangles();
  test-draw-rectangles(filled?: #t);
end test rectangle-drawing-test;
||#

(test rectangle-drawing-test
      :description "rectangle drawing test"
      (test-draw-rectangle)
      (test-draw-rectangle :filled? t)
      (test-draw-rectangles)
      (test-draw-rectangles :filled? t))


#||
define test regular-polygon-drawing-test ()
  test-draw-regular-polygon("Regular Pentagon", 5);
  test-draw-regular-polygon("Regular Hexagon", 6);
  test-draw-regular-polygon("Right-handed Regular Hexagon", 6,
                            handedness: #"right");
end test regular-polygon-drawing-test;
||#

(test regular-polygon-drawing-test
      :description "regular polygon drawing test"
      (test-draw-regular-polygon "Regular Pentagon" 5)
      (test-draw-regular-polygon "Regular Hexagon" 6)
      (test-draw-regular-polygon "Right-handed Regular Hexagon" 6
				 :handedness :right))


#||
define test text-drawing-test ()
  test-text-drawing("Hello");
  test-text-drawing('A');
end test text-drawing-test;
||#

(test text-drawing-test
      :description "text drawing test"
      (test-text-drawing "Hello")
      (test-text-drawing #\A))


#||
define test triangle-drawing-test ()
  test-triangle-drawing("Draw Triangle", filled?: #f);
  test-triangle-drawing("Draw Filled Triangle", filled?: #t);
end test triangle-drawing-test;
||#

(test triangle-drawing-test
      :description "triangle drawing test"
      (test-triangle-drawing "Draw Triangle" :filled? nil)
      (test-triangle-drawing "Draw Filled Triangle" :filled? t))


#||

/// Define the graphics test suite

define suite duim-graphics-suite ()
  test simple-graphics-test;
  test arrow-drawing-test;
  test bezier-curve-drawing-test;
  test circle-drawing-test;
  test ellipse-drawing-test;
  test line-drawing-test;
  test oval-drawing-test;
  test pixmap-drawing-test;
  test points-drawing-test;
  test polygon-drawing-test;
  test rectangle-drawing-test;
  test regular-polygon-drawing-test;
  test text-drawing-test;
  test triangle-drawing-test;
end suite duim-graphics-suite;
||#

(defun test-graphics ()
  (run! 'duim-graphics))

