;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-TESTS -*-
(in-package #:duim-tests)

;;;; Some useful utilities

(defgeneric printable-value (object))

(defmethod printable-value ((object t))
  object)

#||
(defun strcat (&rest strings)
  (apply #'concatenate 'string strings))

;; bind to T to get masses of spam.
(defparameter *trace-tests* nil)
(defparameter *trace-indent* "")

(defun test-message (message &rest args)
  (when *trace-tests*
    (apply #'duim-debug-message (strcat *trace-indent* message "  {TEST-MSG}") args)))

(defmacro with-test-messages ((entry-message &rest args) &body body)
  `(let ((*trace-indent* (strcat *trace-indent* "   ")))
     (apply #'test-message ,entry-message (list ,@args))
     ,@body))

;;; Make sure we only report tests from this collection...

(test-framework:reset)
||#
