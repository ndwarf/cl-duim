;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-TESTS -*-
(in-package :duim-tests)

(def-suite duim-commands
    :description "COMMANDS test suite"
    :in all-tests)
(in-suite duim-commands)

;;; Command class tests

;;; The majority of the Dylan/DUIM tests for class functionality are
;;; stubs. Likely this is because, like Lisp, the implementation
;;; handles generation of the code and is assumed correct. I have
;;; added them here for the Commands classes both as an example and a
;;; way to learn the commands system.

;;; TODO: Add tests for <command-decorator> classes
#|| Dylan/DUIM's stubs below
define sideways method make-test-instance
    (class :: subclass(<command-decorator>)) => (instance :: <command-decorator>)
  make(class, object: "Dummy Object", type: <object>)
end method make-test-instance;

define duim-commands class-test <command-decorator> ()
  //---*** Fill this in...
end class-test <command-decorator>;
||#

(test command-classes
  :description "test class accessors"
  (let* ((frame (make-test-frame (find-class '<test-frame>)))
	 (bcommand (make-command (find-class '<basic-command>)
		     :client 'client
		     :server frame
		     :invoker 'invoker
		     :results-to 'results))
	 (fcommand (make-instance (find-class '<functional-command>)
		                  :function #'test-callback
		                  :arguments '(a b)
		                  :server frame)))

    (is (eq (command-client bcommand) 'client)
	"The ~A basic command did not return the correct client. Expected 'client, got ~A" (command-client bcommand))
    (is (eq (command-invoker bcommand) 'invoker)
	"The ~A basic command did not return the correct invoker. Expected 'invoker, got ~A" (command-invoker bcommand))
    (is (eq (command-results-to bcommand) 'results)
	"The ~A basic command did not return the correct client. Expected 'client, got ~A" (command-results-to bcommand))
    (is (eq (command-function fcommand) #'test-callback)
	"The ~A functional command did not return the correct function. Expected #'test-callback, got ~A" (command-function fcommand))
    (is (equal (command-arguments fcommand) '(a b))
	"The ~A functional command did not return the correct arguments. Expected '(a b), got ~A" (command-arguments fcommand))

    ;; Enable/disable commands; not strictly class tests, but the setup is already here
    (is (command-enabled-p bcommand frame)
    	"Basic command ~A was supposed to be enabled but isn't" bcommand)
    (setf (command-enabled-p bcommand frame) nil)
    (is-false (command-enabled-p bcommand frame)
    	"Basic command ~A was not supposed to be enabled" bcommand)
    (is (command-enabled-p fcommand frame)
    	"Functional command ~A was supposed to be enabled but isn't" fcommand)
    (setf (command-enabled-p fcommand frame) nil)
    (is-false (command-enabled-p fcommand frame)
    	"Functional command ~A was not supposed to be enabled" fcommand)))



;;; Command method tests

(test simple-commands
  :description "Test simple commands"

  ;; A simple command is either a <basic-command> (which is undoable)
  ;; or a <functional-command> (which is not). Only a functional
  ;; command can be executed, so it's the only one tested here. A
  ;; <basic-command> is more like a mixin.

  (let* ((frame (make-test-frame (find-class '<test-frame>)))
	 (command (make-instance (find-class '<functional-command>)
				 :function #'test-callback
				 :server frame)))
    (prepare-for-test-callback)
    (execute-command command)

    (is (test-callback-invoked-p)
	"~a was not executed as a test callback" command)
    (is (command-enabled-p command frame)
    	"~A was supposed to be enabled but isn't" fcommand)))


;;; Command table tests

(test command-tables
  :description "Manipulate command tables"
  (let* ((command-table   (make-command-table :name 'test-command-table))
	 (command-table-1 (make-command-table :name :test-command-table-1))
	 (command-table-2 (define-command-table test-command-table-2 (*global-command-table*))))

    (is (command-table-p command-table)
	"~a was supposed to be a command-table, but it isn't" command-table)
    (is (command-table-p command-table-1)
	"~a was supposed to be a command-table, but it isn't" command-table-1)

    ;; Add/remove command table from the global command table
    (remove-command-table command-table) ; Remove by command object instance
    (is-false (gethash (command-table-name command-table) DUIM-FRAMES-INTERNALS::*COMMAND-TABLES*)
	      "Command table ~a was not expected to be found" (command-table-name command-table))

    (remove-command-table :test-command-table-1) ; Remove by symbol (hash key)
    (is-false (find-command-table :test-command-table-1 :errorp nil)
	      "Command table ~a was not expected to be found" command-table)

    ;; enabling/disabling command-tables
    (let* ((frame (make-test-frame (find-class '<test-frame>)))
    	   (command (make-instance (find-class '<functional-command>)
    		      :function #'test-callback
    		      :server frame)))
      (is (command-enabled-p test-command-table-2 frame) "Command table ~A should be enabled" command-table-2)
      (setf (command-enabled-p test-command-table-2 frame) nil)
      (is-false (command-enabled-p test-command-table-2 frame)
		"~A was not supposed to be enabled" command-table-2))
    (remove-command-table command-table-2)))


(test commands
  :description "Test adding and removing various command types to/from a command table"
  (let* ((command-table    (make-command-table :name 'test-command-table))
	 (fcommand         #'test-callback)
	 (command-class    (find-class '<functional-command>))
	 (command-instance (make-instance (find-class '<functional-command>)
			     :function 'test-callback
			     :server nil)))

    ;; Add/remove command functions from a command table
    (add-command command-table fcommand)

    (is (command-present-p command-table fcommand)
    	"Command function ~a was not found in command table ~a" fcommand command-table)
    (is (command-accessible-p command-table fcommand)
	"Command function ~a was not accessible in command table ~a" fcommand command-table)

    (remove-command command-table fcommand)
    (is-false (command-present-p command-table fcommand)
    	      "Command function ~a was not expected to be in command-table ~a" fcommand command-table)
    (is-false (command-accessible-p command-table fcommand); TODO: put command in an inherited command table
	      "Command function ~a was not expected to be accessible in command table ~a" fcommand command-table)


    ;; Add/remove command classes from a command table
    (add-command command-table command-class)
    (is (command-present-p command-table command-class)
    	"Command class ~a was not found in command-table ~a" command-class command-table)

    (remove-command command-table command-class)
    (is-false (command-present-p command-table command-class)
    	      "Command class ~a was not expected to be in command-table ~a" command-class command-table)


    ;; Add/remove command object instances from a command table
    (add-command command-table command-instance)
    (is (command-present-p command-table command-instance)
    	"Command instance ~a was not found in command table ~a" command-instance command-table)

   (remove-command command-table command-instance)
    (is-false (command-present-p command-table command-instance)
    	      "Command instance ~a was not expected to be in command table ~a" command-instance command-table)


    ;; Clean up, since *command-tables* is a global variable
    (remove-command-table command-table)))


;;; Command table menu handling

(defparameter *test-command-menus*
  (vector (vector "Identity" #'identity)
	  (vector "First" #'first)))

(test command-table-menus
  :description "command table menus test"
  (let ((command-table (make-command-table :name 'test-command-table)))
    (loop for command-data across *test-command-menus*
       do (add-command-table-menu-item command-table
				       (aref command-data 0)
				       (find-class '<command>)
				       (aref command-data 1)))
    (let* ((framem (find-test-frame-manager))
	   (menus (make-menus-from-command-table command-table
						 nil
						 framem
						 :label "Test"))
	   (menu (and (= (length menus) 1) (aref menus 0))))

      (is-true (and menu (typep menu '<menu>)) "Command table menu creation failed")
      (is (string= (gadget-label menu) "Test") "Command table menu label ~a is not \"Test\"" (gadget-label menu))
      (let ((menu-items (sheet-children menu)))
      	(is (equal? (length menu-items)(length *test-command-menus*))
      	    "Command table menu is not correct size (~a != ~a)" (length menu-items)(length *test-command-menus*))
      	(loop for item across (sheet-children menu)
      	   for command-data across *test-command-menus*
      	   for count from 0
      	    do (is (string= (aref command-data 0)(gadget-label item))
      		   "Command table menu item ~d label, ~a != ~a" count (gadget-label item)(aref command-data 0))
      	   do (is (not (null (gadget-activate-callback item)))
      		  "Command table menu item ~d callback" count))
      	menus))
    (remove-command-table command-table)))


;;; Tests below this point in the file were not part of Dylan/DUIM

;;; Command enable/disable notification
(test command-notification
      :description "Test where a command button might get ungrayed"
      (let* ((frame (make-test-frame (find-class '<test-frame>)))
	     (framem (find-test-frame-manager))
	     (command (make-instance (find-class '<functional-command>)
				 :function #'test-callback
				 :server frame)))

	;; I don't think this works
	(note-command-enabled framem frame command)
	)
      )


;;; SN:20200704 Why aren't execute-callback* methods exported?
(test command-callbacks
  :description "Test commands are legal callbacks for gadgets and frames"
  (let* ((frame   (make-test-frame (find-class '<test-frame>)))
	 (command (make-instance (find-class '<functional-command>)
				 :function #'test-callback
				 :server frame)))

    ;;; Frames
    (prepare-for-test-callback)
    (duim-frames-internals::execute-callback frame command)
    (is (test-callback-invoked-p) "Command instance ~a was not executed as a test callback" command)

    ;; We don't have any command classes that can be executed
    ; (prepare-for-test-callback)
    ; (duim-frames-internals::execute-callback frame command-class)
    ; (is (test-callback-invoked-p) "Command class ~a was not executed as a test callback" command-class)

    ;; I can't find any examples of a command-type of list either.
    ; (prepare-for-test-callback)
    ; (duim-frames-internals::execute-callback frame command-list)
    ; (is (test-callback-invoked-p) "Command list ~a was not executed as a test callback" command-list)

    ;;; Gadgets
    ;; TODO: add tests here

    ))





(defun test-commands ()
  (run! 'duim-commands))

