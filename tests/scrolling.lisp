;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-TESTS -*-
(in-package #:duim-tests)

(def-suite duim-scrolling
    :description "DUIM-SCROLLING test suite"
    :in all-tests)
(in-suite duim-scrolling)

#||
// Viewports don't need to keep their children within their bounds
define method enforce-fully-within-parent?
    (pane :: <viewport>) => (enforce? :: <boolean>)
  #f
end method enforce-fully-within-parent?;
||#

(defmethod enforce-fully-within-parent? ((pane <viewport>))
  nil)

#||
define method horizontal-scroll-only?
    (pane :: <test-viewport>) => (horizontal-only? :: <boolean>)
  let horizontal? = horizontal-scroll-bar(pane);
  let vertical?   = vertical-scroll-bar(pane);
  horizontal? & ~vertical?
end method horizontal-scroll-only?;
||#

(defmethod horizontal-scroll-only? ((pane <test-viewport>))
  (let (;;(horizontal? (horizontal-scroll-bar pane))
	;;(vertical?   (vertical-scroll-bar pane))
	(HORIZONTAL? (SHEET-HORIZONTAL-SCROLL-BAR PANE))
	(VERTICAL? (SHEET-VERTICAL-SCROLL-BAR PANE)))
    (and horizontal? (not vertical?))))


#||
define method vertical-scroll-only?
    (pane :: <test-viewport>) => (horizontal-only? :: <boolean>)
  let horizontal? = horizontal-scroll-bar(pane);
  let vertical?   = vertical-scroll-bar(pane);
  vertical? & ~horizontal?
end method vertical-scroll-only?;
||#

(defmethod vertical-scroll-only? ((pane <test-viewport>))
  (let (;;(horizontal? (horizontal-scroll-bar pane))
	;;(vertical?   (vertical-scroll-bar pane))
	(HORIZONTAL? (SHEET-HORIZONTAL-SCROLL-BAR PANE))
	(VERTICAL? (SHEET-VERTICAL-SCROLL-BAR PANE)))
    (and vertical? (not horizontal?))))


#||
define method expected-width
    (pane :: <viewport>, #key width) => (width :: <integer>)
  let child = sheet-child(pane);
  if (child)
    if (vertical-scroll-only?(pane))
      if (width)
        expected-constrained-width(child, width)
      else
        expected-width(child)
      end
    else
      width | expected-width(child)
    end
  else
    expected-default-size(#"width", width: width)
  end
end method expected-width;
||#

(defmethod expected-width ((pane <viewport>) &key width)
  (let ((child (sheet-child pane)))
    (if child
	(if (vertical-scroll-only? pane)
	    (if width
		(expected-constrained-width child width)
		(expected-width child))
	    ;; else
	    (or width (expected-width child)))
	;; else
	(expected-default-size :width :width width))))


#||
define method expected-height
    (pane :: <viewport>, #key height) => (height :: <integer>)
  let child = sheet-child(pane);
  if (child)
    if (horizontal-scroll-only?(pane))
      if (height)
        expected-constrained-height(pane, height)
      else
        expected-height(child)
      end
    else
      height | expected-height(child)
    end
  else
    expected-default-size(#"height", height: height)
  end
end method expected-height;
||#

(defmethod expected-height ((pane <viewport>) &key height)
  (let ((child (sheet-child pane)))
    (if child
	(if (horizontal-scroll-only? pane)
	    (if height
		(expected-constrained-height pane height)
		(expected-height child))
	    (or height (expected-height child)))
	(expected-default-size :height :height height))))


#||
define method expected-min-width
    (pane :: <viewport>, #key) => (min-width :: <integer>)
  let child = sheet-child(pane);
  if (child & vertical-scroll-only?(pane))
    expected-min-width(child)
  else
    expected-default-size(#"min-width")
  end
end method expected-min-width;
||#

(defmethod expected-min-width ((pane <viewport>) &key)
  (let ((child (sheet-child pane)))
    (if (and child (vertical-scroll-only? pane))
	(expected-min-width child)
	(expected-default-size :min-width))))


#||
define method expected-min-height
    (pane :: <viewport>, #key) => (min-height :: <integer>)
  let child = sheet-child(pane);
  if (child & horizontal-scroll-only?(pane))
    expected-min-height(child)
  else
    expected-default-size(#"min-height")
  end
end method expected-min-height;
||#

(defmethod expected-min-height ((pane <viewport>) &key)
  (let ((child (sheet-child pane)))
    (if (and child (horizontal-scroll-only? pane))
	(expected-min-height child)
	(expected-default-size :min-height))))


#||
define method expected-max-width
    (pane :: <viewport>, #key) => (max-width :: <integer>)
  if (vertical-scroll-only?(pane))
    next-method()
  else
    $fill
  end
end method expected-max-width;
||#

(defmethod expected-max-width ((pane <viewport>) &key)
  (if (vertical-scroll-only? pane)
      (call-next-method)
      +fill+))


#||
define method expected-max-height
    (pane :: <viewport>, #key) => (max-height :: <integer>)
  if (horizontal-scroll-only?(pane))
    next-method()
  else
    $fill
  end
end method expected-max-height;
||#

(defmethod expected-max-height ((pane <viewport>) &key)
  (if (horizontal-scroll-only? pane)
      (call-next-method)
      +fill+))


#||
define method expected-space-allocation
    (pane :: <viewport>, #key width, height)
 => (space :: <sequence>)
  let child = sheet-child(pane);
  if (child)
    //---*** This should really take account of stretchable children
    //---*** which grow to fill the viewport
    let child-width  = expected-width(child);
    let child-height = expected-height(child);
    vector(vector(0, 0, child-width, child-height))
  else
    #()
  end
end method expected-space-allocation;
||#

(defmethod expected-space-allocation ((pane <viewport>) &key width height)
  (declare (ignore width height)) ; SN: CLHS says original arguments passed to next method
  (let ((child (sheet-child pane)))
    (if child
	(let ((child-width (expected-width child))
	      (child-height (expected-height child)))
	  (vector (vector 0 0 child-width child-height)))
	;; else -- XXX LIST OR VECTOR?
	'())))


#||
define method test-viewport-layout
    (name, class, horizontal?, vertical?,
     #rest args,
     #key width, height, #all-keys)
  let test-name = concatenate(name, " ", gadget-class-name(<viewport>));
  let child = class & apply(make-test-pane, class, args);
  let viewport
    = make-test-pane(<viewport>,
		     horizontal-scroll-bar: if (horizontal?) make-test-pane(<scroll-bar>) end,
		     vertical-scroll-bar:   if (vertical?)   make-test-pane(<scroll-bar>) end,
		     child: child);
  check-layout-pane-layout(viewport, test-name);
  let (new-width, new-height)
    = expected-constrained-size(viewport, 500, 500);
  set-sheet-size(viewport, new-width, new-height);
  check-layout-pane-layout(viewport, concatenate("resized ", test-name));
  viewport
end method test-viewport-layout;
||#

(defmethod test-viewport-layout (name class horizontal? vertical?
                                 &rest args
                                 &key width height &allow-other-keys)
  (declare (ignore width height))
  (let* ((test-name (concatenate 'string name " " (gadget-class-name (find-class '<viewport>))))
	 (child (and class (apply #'make-test-pane class args)))
	 (viewport (make-test-pane (find-class '<viewport>)
				   :horizontal-scroll-bar (if horizontal? (make-test-pane (find-class '<scroll-bar>)) nil)
				   :vertical-scroll-bar (if vertical? (make-test-pane (find-class '<scroll-bar>)) nil)
				   :child child)))
    (check-layout-pane-layout viewport test-name)
    (multiple-value-bind (new-width new-height)
	(expected-constrained-size viewport 500 500)
      (set-sheet-size viewport new-width new-height)
      (check-layout-pane-layout viewport (concatenate 'string "resized " test-name)))
    viewport))


#||
define method test-viewport-layouts (name, class, #rest args)
  apply(test-viewport-layout, concatenate("no-bar ", name),
        class, #f, #f, args);
  apply(test-viewport-layout, concatenate("vertical ", name),
        class, #f, #t, args);
  apply(test-viewport-layout, concatenate("horizontal ", name),
        class, #t, #f, args);
  apply(test-viewport-layout, concatenate("two-bar ", name),
        class, #t, #t, args);
end method test-viewport-layouts;
||#

(defmethod test-viewport-layouts (name class &rest args)
  (apply #'test-viewport-layout (concatenate 'string "no-bar " name) class nil nil args)
  (apply #'test-viewport-layout (concatenate 'string "vertical " name) class nil t args)
  (apply #'test-viewport-layout (concatenate 'string "horizontal " name) class t nil args)
  (apply #'test-viewport-layout (concatenate 'string "two-bar " name) class t t args))


#||
define duim-gadgets class-test <viewport> ()
  test-viewport-layouts("empty", #f);
  test-viewport-layouts("fixed", <push-button>);
  test-viewport-layouts("non-fixed", <list-box>);
  test-viewport-layouts("large child", <list-box>, width: 600, height: 800);
end class-test <viewport>;
||#

(test <viewport>-class-test
  :description "<viewport> class test"
  (test-viewport-layouts "empty" nil)
  (test-viewport-layouts "fixed" (find-class '<push-button>))
  (test-viewport-layouts "non-fixed" (find-class '<list-box>))
  (test-viewport-layouts "large-child" (find-class '<list-box>)
			 :width 600
			 :height 800))


#||

/// Scroller tests

define inline function border-thickness*2
    (border :: <border>) => (thickness :: <integer>)
  //---*** Why does this not work
  // border-thickness(border) * 2
  4
end function border-thickness*2;
||#

(defun border-thickness*2 (border)
  (* (border-thickness border) 2))


#||
define method expected-width
    (pane :: <scroller>, #key width) => (width :: <integer>)
  let child = sheet-child(pane);
  if (instance?(child, <border>))
    next-method() + border-thickness*2(child)
  else
    next-method()
  end
end method expected-width;
||#

(defmethod expected-width ((pane <scroller>) &key width)
  ;;; CHANGED FROM ORIGINAL TO AVOID DOUBLE-ADDING BORDER-THICKNESS
  (if width
      width
      (CALL-NEXT-METHOD)))


#||
define method expected-height
    (pane :: <scroller>, #key height) => (height :: <integer>)
  let child = sheet-child(pane);
  if (instance?(child, <border>))
    next-method() + border-thickness*2(child)
  else
    next-method()
  end
end method expected-height;
||#

(defmethod expected-height ((pane <scroller>) &key height)
  ;;; CHANGED FROM ORIGINAL TO AVOID DOUBLE-ADDING BORDER-THICKNESS
  (if height
      height
      (CALL-NEXT-METHOD)))


#||
define method expected-min-width
    (pane :: <scroller>, #key) => (min-width :: <integer>)
  let child = sheet-child(pane);
  if (instance?(child, <border>))
    next-method() + border-thickness*2(child)
  else
    next-method()
  end
end method expected-min-width;
||#

(defmethod expected-min-width ((pane <scroller>) &key)
  ;;; CHANGED FROM ORIGINAL TO AVOID DOUBLE-ADDING BORDER-THICKNESS
  ;;; BORDER ALREADY INCLUDED IN LESS SPECIFIC NEXT-METHOD
  (CALL-NEXT-METHOD))


#||
define method expected-min-height
    (pane :: <scroller>, #key) => (min-height :: <integer>)
  let child = sheet-child(pane);
  if (instance?(child, <border>))
    next-method() + border-thickness*2(child)
  else
    next-method()
  end
end method expected-min-height;
||#

(defmethod expected-min-height ((pane <scroller>) &key)
  ;;; CHANGED FROM ORIGINAL TO AVOID DOUBLE-ADDING BORDER-THICKNESS
  ;;; BORDER ALREADY INCLUDED IN LESS SPECIFIC NEXT-METHOD
  (CALL-NEXT-METHOD))


#||
define method expected-max-width
    (pane :: <scroller>, #key) => (max-width :: <integer>)
  let child = sheet-child(pane);
  if (instance?(child, <border>))
    next-method() + border-thickness*2(child)
  else
    next-method()
  end
end method expected-max-width;
||#

(defmethod expected-max-width ((pane <scroller>) &key)
  ;;; CHANGED FROM ORIGINAL TO AVOID DOUBLE-ADDING BORDER-THICKNESS
  ;;; BORDER ALREADY INCLUDED IN LESS SPECIFIC NEXT-METHOD
  (CALL-NEXT-METHOD))


#||
define method expected-max-height
    (pane :: <scroller>, #key) => (max-height :: <integer>)
  let child = sheet-child(pane);
  if (instance?(child, <border>))
    next-method() + border-thickness*2(child)
  else
    next-method()
  end
end method expected-max-height;
||#

(defmethod expected-max-height ((pane <scroller>) &key)
  ;;; CHANGED FROM ORIGINAL TO AVOID DOUBLE-ADDING BORDER-THICKNESS
  ;;; BORDER ALREADY INCLUDED IN LESS SPECIFIC NEXT-METHOD
  (CALL-NEXT-METHOD))


#||
define method expected-space-allocation 
    (pane :: <scroller>, #key width, height)
 => (space :: <sequence>)
  next-method()
end method expected-space-allocation;
||#
(defmethod expected-space-allocation ((pane <scroller>) &key width height)
  (declare (ignore width height)) ; SN: CLHS says original arguments passed to next method
  (call-next-method))

#||
define method test-scroller
    (name :: <string>, class :: false-or(<class>),
     scroll-bars :: one-of(#"none", #"horizontal", #"vertical", #"both"),
     borders :: <boolean>) => ()
  let child = class & make-test-pane(class);
  let scroller
     = make-test-pane(<scroller>,
		      contents: child,
		      scroll-bars: scroll-bars,
		      border-type: borders & #"sunken");
  check-layout-pane-layout(scroller, name);
  let (new-width, new-height)
    = expected-constrained-size(scroller, 1000, 1000);
  set-sheet-size(scroller, new-width, new-height);
  check-layout-pane-layout(scroller, concatenate("resized ", name));
end method test-scroller;
||#

(defmethod test-scroller ((name string) (class null) scroll-bars borders)
  (check-type scroll-bars (member :none :horizontal :vertical :both))
  (let* ((child (and class (make-test-pane class)))
	 (scroller (make-test-pane (find-class '<scroller>)
				   :contents child
				   :scroll-bars scroll-bars
				   :border-type (and borders :sunken))))
    (check-layout-pane-layout scroller name)
    (multiple-value-bind (new-width new-height)
	(expected-constrained-size scroller 1000 1000)
      (set-sheet-size scroller new-width new-height)
      (check-layout-pane-layout scroller (concatenate 'string "resized " name)))))

(defmethod test-scroller ((name string) (class class) scroll-bars borders)
  (check-type scroll-bars (member :none :horizontal :vertical :both))
  (let* ((child (and class (make-test-pane class)))
	 (scroller (make-test-pane (find-class '<scroller>)
				   :contents child
				   :scroll-bars scroll-bars
				   :border-type (and borders :sunken))))
    (check-layout-pane-layout scroller name)
    (multiple-value-bind (new-width new-height)
	(expected-constrained-size scroller 1000 1000)
      (set-sheet-size scroller new-width new-height)
      (check-layout-pane-layout scroller (concatenate 'string "resized " name)))))


#||
define method test-scroller-layout
    (name :: <string>, class :: false-or(<class>))
  let gadget-name = gadget-class-name(<scroller>);
  // Borders
  test-scroller(concatenate(name, " no-bar ", gadget-name),
                class, #"none", #t);
  test-scroller(concatenate(name, " horizontal ", gadget-name),
                class, #"horizontal", #t);
  test-scroller(concatenate(name, " vertical ", gadget-name),
                class, #"vertical", #t);
  test-scroller(concatenate(name, " two-bar ", gadget-name),
                class, #"both", #t);
  // No borders
  test-scroller(concatenate(name, " no-bar borderless ", gadget-name),
                class, #"none", #f);
  test-scroller(concatenate(name, " horizontal borderless ", gadget-name),
                class, #"horizontal", #f);
  test-scroller(concatenate(name, " vertical borderless ", gadget-name),
                class, #"vertical", #f);
  test-scroller(concatenate(name, " two-bar borderless ", gadget-name),
                class, #"both", #f);
end method test-scroller-layout;
||#

(defun test-scroller-layout (name class)
  (let ((gadget-name (gadget-class-name (find-class '<scroller>))))
    ;; Borders
    (test-scroller (concatenate 'string name " no-bar " gadget-name)
		   class :none t)
    (test-scroller (concatenate 'string name " horizontal " gadget-name)
		   class :horizontal t)
    (test-scroller (concatenate 'string name " vertical " gadget-name)
		   class :vertical t)
    (test-scroller (concatenate 'string name " two-bar " gadget-name)
		   class :both t)
    ;; No borders
    (test-scroller (concatenate 'string name " no-bar borderless "
				gadget-name) class :none nil)
    (test-scroller (concatenate 'string name " horizontal borderless "
				gadget-name) class :horizontal nil)
    (test-scroller (concatenate 'string name " vertical borderless "
				gadget-name) class :vertical nil)
    (test-scroller (concatenate 'string name " two-bar borderless "
				gadget-name) class :both nil)))

#||
define duim-gadgets class-test <scroller> ()
  test-scroller-layout("empty", #f);
  test-scroller-layout("fixed", <push-button>);
  test-scroller-layout("non-fixed", <list-box>);
end class-test <scroller>;
||#

(test <scroller>-class-test
  :description "<scroller> class test"
  (test-scroller-layout "empty" nil)
  (test-scroller-layout "fixed" (find-class '<push-button>))
  (test-scroller-layout "non-fixed" (find-class '<list-box>)))

(defun test-scrolling ()
  (run! 'duim-scrolling))
