;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-TESTS -*-
(in-package #:duim-tests)

(def-suite duim-transforms
    :description "DUIM-TRANSFORMS test suite"
    :in all-tests)
(in-suite duim-transforms)

;;; Transform test suites

(defvar *translate-up-10+10* (make-translation-transform +10 +10))
(defvar *translate-down-10+10* (make-translation-transform -10 -10))

(defvar *scale-up-4x4* (make-scaling-transform 4 4))
(defvar *scale-down-4x4* (make-scaling-transform 0.25 0.25))

(defvar *rotate-up-1/4* (make-rotation-transform +0.25))
(defvar *rotate-down-1/4* (make-rotation-transform -0.25))

(defvar *transforms-to-invert*
  (list (list *identity-transform* *identity-transform*)
	(list *translate-up-10+10* *translate-down-10+10*)
	(list *scale-up-4x4* *scale-down-4x4*)
	(list *rotate-up-1/4* *rotate-down-1/4*)))

(defun normalize-transform-component (component)
  (/ (floor (* component 10000)) 10000.0))

(defun normalize-transform (transform)
  (multiple-value-bind (mxx mxy myx myy tx ty)
      (transform-components transform)
    (make-transform (normalize-transform-component mxx)
		    (normalize-transform-component mxy)
		    (normalize-transform-component myx)
		    (normalize-transform-component myy)
		    (normalize-transform-component tx)
		    (normalize-transform-component ty))))

(defun test-invert-transform (tr1 tr2)
  (is (transform-equal (normalize-transform (invert-transform tr1))
		       (normalize-transform tr2))
      "(invert-transform ~a) = (transform ~a)"
      (printable-value tr1)
      (printable-value tr2)))

(defun check-transform-box (name transform l1 t1 r1 b1 l2 t2 r2 b2)
  (is (multiple-value-bind (left top right bottom)
	  (transform-box transform l1 t1 r1 b1)
	(and (= left l2)
	     (= top t2)
	     (= right r2)
	     (= bottom b2)))
      "~a transform-box"
      name))

(defun test-transform-position (name transform start-x start-y end-x end-y)
  (multiple-value-bind (new-x new-y)
      (transform-position transform start-x start-y)
    (is (and (= new-x end-x)
	     (= new-y end-y))
	"~a of ~d, ~d" name start-x start-y)))

(defun transform-position-test-ok? (data)
  (let* ((transform (nth 0 data))
	 (start-point (nth 1 data))
	 (end-point (nth 2 data))
	 (test-name (format nil "Transform ~a"
			    (printable-value transform))))
    (test-transform-position test-name transform
			     (point-x start-point) (point-y start-point)
			     (point-x end-point) (point-y end-point))))

(defmethod printable-value ((transform <transform>))
  (format nil "~a" transform))

(test invert-transforms-test
      :description "invert transforms test"
      (loop for elt in *transforms-to-invert*
	 do (let ((tr1 (elt elt 0))
		  (tr2 (elt elt 1)))
	      (test-invert-transform tr1 tr2)
	      (unless (eql tr1 tr2)
		(test-invert-transform tr2 tr1)))))

(test $identity-transform-constant-test
      :description "$identity-transform constant test"
  (test-transform-position "Identity transform"
			   *identity-transform*
			   10 20 10 20)
  (check-transform-box "Identity transform"
		       *identity-transform*
		       0 10 100 200
		       0 10 100 200))

(test transform-box-test
      :description "transform box test"
      (check-transform-box "Translation transform"
			   (make-translation-transform 10 20)
			   0 10 100 200
			   10 30 110 220)
      (check-transform-box "Scaling transform"
			   (make-scaling-transform 10 20)
			   0 10 100 200
			   0 200 1000 4000)
      (check-transform-box "Reflection transform"
			   (make-reflection-transform* (make-point 0 10)
						       (make-point 10 10))
			   0 10 100 200
			   0 -180 100 10))

(defvar *transform-position-tests*
  (list (list (make-translation-transform 10 10)
	      (make-point 0 0)
	      (make-point 10 10))
	(list (make-scaling-transform 10 10)
	      (make-point 10 10)
	      (make-point 100 100))
	(list (make-reflection-transform* (make-point 10 0)
					  (make-point 10 10))
	      (make-point 20 10)
	      (make-point 0 10))))

(test transform-position-test
      :description "transform position test"
      (loop for test in *transform-position-tests*
	 do (transform-position-test-ok? test)))

;;; Sheet delta transforms
(test sheet-delta-transform-test
      :description "sheet delta transform test"
      (let* ((sheet (make-test-pane (find-class '<simple-pane>) :x 50 :y 100))
	     (first-parent (make-test-pane (find-class '<pinboard-layout>)
					   :children (vector sheet)
					   :x 200 :y 300))
	     (second-parent (make-test-pane (find-class '<pinboard-layout>)
					    :children (vector first-parent))))
	(is (equal? (sheet-delta-transform sheet sheet) *identity-transform*)
	    "Sheet delta transform identity")
	(is (equal? (sheet-delta-transform sheet first-parent)
		    (make-translation-transform 50 100))
	    "Sheet delta transform one-level")
	(is (equal? (sheet-delta-transform sheet second-parent)
		    (make-translation-transform 250 400))
	    "Sheet delta transform two-levels")))

(defun test-transforms ()
  (run! 'duim-transforms))

