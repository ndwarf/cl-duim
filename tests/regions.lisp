;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-TESTS -*-
(in-package #:duim-tests)

(def-suite duim-regions
    :description "DUIM-REGIONS test suite"
    :in all-tests)
(in-suite duim-regions)

#||
define duim-geometry constant-test $everywhere ()
  check-false("$everywhere is not empty", region-empty?($everywhere))
end constant-test $everywhere;
||#

(test $everywhere-constant-test
      :description "$everywhere constant test"
      (is (not (region-empty? *everywhere*)) "$everywhere is not empty"))


#||
define duim-geometry constant-test $nowhere ()
  check-true("$nowhere is empty", region-empty?($nowhere))
end constant-test $nowhere;
||#

(test $nowhere-constant-test
      :description "$nowhere constant test"
      (is (region-empty? *nowhere*) "$nowhere is empty"))


#||

/// Region tests

define method printable-value (point :: <point>) => (value)
  vector("POINT", point-x(point), point-y(point))
end method printable-value;
||#

(defmethod printable-value ((point <point>))
  (vector "POINT" (point-x point) (point-y point)))


#||
define method printable-value (line :: <line>) => (value)
  let (x1, y1) = point-position(line-start-point(line));
  let (x2, y2) = point-position(line-end-point(line));
  vector("LINE", x1, y1, x2, y2)
end method printable-value;
||#

(defmethod printable-value ((line <line>))
  (multiple-value-bind (x1 y1)
      (point-position (line-start-point line))
    (multiple-value-bind (x2 y2)
	(point-position (line-end-point line))
      (vector "LINE" x1 y1 x2 y2))))


#||
define method printable-value (rectangle :: <rectangle>) => (value)
  let (left, right, top, bottom) = rectangle-edges(rectangle);
  vector("RECTANGLE", left, right, top, bottom)
end method printable-value;
||#

(defmethod printable-value ((rectangle <rectangle>))
  (multiple-value-bind (left right top bottom)
      (rectangle-edges rectangle)
    (vector "RECTANGLE" left right top bottom)))


#||
define constant *regions-for-region-equal-tests*
  = list($nowhere,
	 $everywhere,
	 make-point(0, 0),
	 make-point(1, 1),
         make-line(0, 0, 10, 10),
	 make-line(10, 10, 20, 20),
         make-rectangle(0, 0, 10, 10),
	 make-rectangle(10, 10, 20, 20),
         make-polygon(#[0, 0, 10, 10, 20, 20, 30, 30, 0, 0]),
         make-polygon(#[0, 0, 10, 10, 20, 20, 30, 30, 40, 40, 0, 0]),
         make-ellipse(20, 20, 10, 0, 0, 10),
         make-ellipse(20, 20, 20, 0, 0, 10));
||#

(defparameter *regions-for-region-equal-tests*
  (list *nowhere*
	*everywhere*
	(make-point 0 0)
	(make-point 1 1)
	(make-line 0 0 10 10)
	(make-line 10 10 20 20)
	(make-rectangle 0 0 10 10)
	(make-rectangle 10 10 20 20)
	(make-polygon #(0 0 10 10 20 20 30 30 0 0))
	(make-polygon #(0 0 10 10 20 20 30 30 40 40 0 0))
	(make-ellipse 20 20 10 0 0 10)
	(make-ellipse 20 20 20 0 0 10)))


#||
define test region-equal-test ()
  for (region1 in *regions-for-region-equal-tests*)
    for (region2 in *regions-for-region-equal-tests*)
      let name = format-to-string("%= = %=?", 
                                  printable-value(region1),
                                  printable-value(region2));
      check-equal(name, region-equal(region1, region2), region1 == region2)
    end
  end;
end test region-equal-test;
||#

(test region-equal-test
      :description "test if regions are equal"
      (loop for region1 in *regions-for-region-equal-tests*
	 do (loop for region2 in *regions-for-region-equal-tests*
	       do (let ((name (format nil "~a = ~a?"
				      (printable-value region1)
				      (printable-value region2))))
		    (is (eql (region-equal region1 region2)
			     (eql region1 region2))
			"region ~a equality test: ~a = ~a" name region1 region2)))))

	   
#||
// region, position, expected result
define constant *regions-for-region-contains-position-tests*
  = list(list($nowhere, make-point(0, 0), #f),
         list($nowhere, make-point(5, 5), #f),
         list($everywhere, make-point(0, 0), #t),
         list($everywhere, make-point(5, 5), #t),
         list(make-point(0, 0), make-point(0, 0), #t),
         list(make-point(0, 0), make-point(5, 5), #f),
         list(make-line(0, 0, 10, 10), make-point(0, 0), #t),
         list(make-line(0, 0, 10, 10), make-point(5, 5), #t),
         list(make-line(0, 0, 10, 10), make-point(10, 10), #t),
         list(make-line(5, 0, 5, 10), make-point(0, 0), #f),
         list(make-line(5, 0, 5, 10), make-point(5, 5), #t),
         list(make-rectangle(0, 0, 10, 10), make-point(0, 0), #t),
         list(make-rectangle(0, 0, 10, 10), make-point(5, 5), #t),
         list(make-rectangle(0, 0, 10, 10), make-point(10, 10), #t),
         list(make-rectangle(0, 0, 10, 10), make-point(20, 20), #f),
         list(make-rectangle(10, 10, 20, 20), make-point(5, 5), #f),
         list(make-rectangle(10, 10, 20, 20), make-point(10, 10), #t));
||#

(defparameter *regions-for-region-contains-position-tests*
  (list (list *nowhere* (make-point 0 0) nil)
	(list *nowhere* (make-point 5 5) nil)
	(list *everywhere* (make-point 0 0) t)
	(list *everywhere* (make-point 5 5) t)
	(list (make-point 0 0) (make-point 0 0) t)
	(list (make-point 0 0) (make-point 5 5) nil)
	(list (make-line 0 0 10 10) (make-point 0 0) t)
	(list (make-line 0 0 10 10) (make-point 5 5) t)
	(list (make-line 0 0 10 10) (make-point 10 10) t)
	(list (make-line 5 0 5 10) (make-point 0 0) nil)
	(list (make-line 5 0 5 10) (make-point 5 5) t)
	(list (make-rectangle 0 0 10 10) (make-point 0 0) t)
	(list (make-rectangle 0 0 10 10) (make-point 5 5) t)
	(list (make-rectangle 0 0 10 10) (make-point 10 10) t)
	(list (make-rectangle 0 0 10 10) (make-point 20 20) nil)
	(list (make-rectangle 10 10 20 20) (make-point 5 5) nil)
	(list (make-rectangle 10 10 20 20) (make-point 10 10) t)))


#||
define test region-contains-position-test ()
  for (x in *regions-for-region-contains-position-tests*)
    let region = x[0];
    let point = x[1];
    let expected = x[2];
    let result = region-contains-position?(region, point-x(point), point-y(point));
    let name = format-to-string("%= %s point %=",
                                printable-value(region),
                                case
                                  expected  => "contains";
                                  otherwise => "does not contain";
                                end,
                                printable-value(point));
    check-equal(name, result, expected)
  end;
end test region-contains-position-test;
||#

(test region-contains-position-test
      :description "region-contains-position checks"
      (loop for x in *regions-for-region-contains-position-tests*
	 do (let* ((region (nth 0 x))
		   (point (nth 1 x))
		   (expected (nth 2 x))
		   (result (region-contains-position? region (point-x point) (point-y point)))
		   (name (format nil "~a ~a point ~a"
				 (printable-value region)
				 (if expected
				     "contains"
				     "does not contain")
				 (printable-value point))))
	      (is (equal? result expected) name))))


#||
define constant *regions-for-region-contains-region-tests*
  = list(list($nowhere, $everywhere, #f, #t),
         list($nowhere, make-point(0, 0), #f, #t),
         list($everywhere, make-point(0, 0), #t, #f),
         list(make-point(0, 0), make-point(1, 1), #f, #f),
         list(make-point(0, 0), make-line(0, 0, 10, 10), #f, #t),
         list(make-point(5, 5), make-line(0, 0, 10, 10), #f, #t),
         list(make-point(5, 5), make-line(5, 0, 5, 10), #f, #t),
         list($nowhere, make-line(0, 0, 10, 10), #f, #t),
         list($everywhere, make-line(0, 0, 10, 10), #t, #f),
         list(make-line(0, 0, 10, 10), make-line(10, 10, 20, 20), #f, #f),
         list(make-rectangle(0, 0, 10, 10), make-point(0, 0), #t, #f),
         list(make-rectangle(0, 0, 10, 10), make-point(1, 1), #t, #f),
         list(make-rectangle(0, 0, 10, 10), make-line(0, 0, 10, 10), #t, #f),
         list(make-rectangle(0, 0, 10, 10), make-rectangle(2, 3, 6, 7), #t, #f),
         list(make-rectangle(0, 0, 10, 10), make-rectangle(10, 10, 20, 20), #f, #f));
||#

(defparameter *regions-for-region-contains-region-tests*
  (list (list *nowhere* *everywhere* nil t)
	(list *nowhere* (make-point 0 0) nil t)
	(list *everywhere* (make-point 0 0) t nil)
	(list (make-point 0 0) (make-point 1 1) nil nil)
	(list (make-point 0 0) (make-line 0 0 10 10) nil t)
	(list (make-point 5 5) (make-line 0 0 10 10) nil t)
	(list (make-point 5 5) (make-line 5 0 5 10) nil t)
	(list *nowhere* (make-line 0 0 10 10) nil t)
	(list *everywhere* (make-line 0 0 10 10) t nil)
	(list (make-line 0 0 10 10) (make-line 10 10 20 20) nil nil)
	(list (make-rectangle 0 0 10 10) (make-point 0 0) t nil)
	(list (make-rectangle 0 0 10 10) (make-point 1 1) t nil)
	(list (make-rectangle 0 0 10 10) (make-line 0 0 10 10) t nil)
	(list (make-rectangle 0 0 10 10) (make-rectangle 2 3 6 7) t nil)
	(list (make-rectangle 0 0 10 10) (make-rectangle 10 10 20 20) nil nil)))


#||
define test region-contains-region-test ()
  for (x in *regions-for-region-contains-region-tests*)
    let region1 = x[0];
    let region2 = x[1];
    let expected1 = x[2];
    let expected2 = x[3];
    let result = region-contains-region?(region1, region2);
    let name = format-to-string("%= %s %=",
                                printable-value(region1),
                                case
                                  expected1 => "contains";
                                  otherwise => "does not contain";
                                end,
                                printable-value(region2));
    check-equal(name, result, expected1);
    let result = region-contains-region?(region2, region1);
    let name = format-to-string("%= %s %=",
                                printable-value(region2),
                                case
                                  expected1 => "contains";
                                  otherwise => "does not contain";
                                end,
                                printable-value(region1));
    check-equal(name, result, expected2)
  end
end test region-contains-region-test;
||#

(test region-contains-region-test
      :description "region-contains-region checks"
      (loop for x in *regions-for-region-contains-region-tests*
	 do (let* ((region1 (nth 0 x))
		   (region2 (nth 1 x))
		   (expected1 (nth 2 x))
		   (expected2 (nth 3 x))
		   (result (region-contains-region? region1 region2))
		   (name (format nil "~a ~a ~a"
				 (printable-value region1)
				 (if expected1
				     "contains"
				     "does not contain")
				 (printable-value region2))))
	      (is (equal? result expected1) name)
	      (let ((result (region-contains-region? region2 region1))
		    (name (format nil "~a ~a ~a"
				  (printable-value region2)
				  (if expected2
				      "contains"
				      "does not contain")
				  (printable-value region1))))
		(is (equal? result expected2) name)))))


#||
// region1, region2, 1-contains-2, 2-contains-1
// region1, region2, expected result
define constant *regions-for-region-intersects-region-tests*
  = list(list(make-point(0, 0), make-point(1, 1), #f),
         list(make-point(0, 0), make-point(5, 5), #f),
         list(make-point(0, 0), make-line(0, 0, 10, 10), #t),
         list(make-point(5, 5), make-line(0, 0, 10, 10), #t),
         list(make-point(5, 5), make-line(5, 0, 5, 10), #t),
         list(make-line(0, 0, 10, 10), make-line(10, 10, 20, 20), #t),
         list(make-rectangle(0, 0, 10, 10), make-point(0, 0), #t),
         list(make-rectangle(0, 0, 10, 10), make-point(1, 1), #t),
         list(make-rectangle(0, 0, 10, 10), make-line(0, 0, 10, 10), #t),
         list(make-rectangle(0, 10, 20, 20), make-line(0, 0, 20, 30), #t),
         list(make-rectangle(0, 0, 10, 10), make-rectangle(2, 3, 6, 7), #t),
         list(make-rectangle(0, 0, 10, 10), make-rectangle(20, 20, 30, 30), #f),
         list(make-rectangle(0, 0, 10, 10), make-rectangle(10, 10, 20, 20), #t));
||#

(defparameter *regions-for-region-intersects-region-tests*
  (list (list (make-point 0 0) (make-point 1 1) nil)
	(list (make-point 0 0) (make-point 5 5) nil)
	(list (make-point 0 0) (make-line 0 0 10 10) t)
	(list (make-point 5 5) (make-line 0 0 10 10) t)
	(list (make-point 5 5) (make-line 5 0 5 10) t)
	(list (make-line 0 0 10 10) (make-line 10 10 20 20) t)
	(list (make-rectangle 0 0 10 10) (make-point 0 0) t)
	(list (make-rectangle 0 0 10 10) (make-point 1 1) t)
	(list (make-rectangle 0 0 10 10) (make-line 0 0 10 10) t)
	(list (make-rectangle 0 10 20 20) (make-line 0 0 20 30) t)
	(list (make-rectangle 0 0 10 10) (make-rectangle 2 3 6 7) t)
	(list (make-rectangle 0 0 10 10) (make-rectangle 20 20 30 30) nil)
	(list (make-rectangle 0 0 10 10) (make-rectangle 10 10 20 20) t)))


#||
define test region-intersects-region-test ()
  for (x in *regions-for-region-intersects-region-tests*)
    let region1 = x[0];
    let region2 = x[1];
    let expected = x[2];
    check-equal(format-to-string("intersects?(%=, %=)",
                                 printable-value(region1),
                                 printable-value(region2)),
                region-intersects-region?(region1, region2),
                expected);
    check-equal(format-to-string("intersects?(%=, %=)",
                                 printable-value(region2),
                                 printable-value(region1)),
                region-intersects-region?(region2, region1),
                expected);
    check-true(format-to-string("%= intersects $everywhere", 
                                printable-value(region1)),
               region-intersects-region?(region1, $everywhere));
    check-false(format-to-string("%= doesn't intersect $nowhere",
                                 printable-value(region1)),
                region-intersects-region?(region1, $nowhere));
  end
end test region-intersects-region-test;
||#

(test region-intersects-region-test
      :description "region-intersects-region checks"
      (loop for x in *regions-for-region-intersects-region-tests*
	 do (let* ((region1 (nth 0 x))
		   (region2 (nth 1 x))
		   (expected (nth 2 x)))
	      (is (equal? (region-intersects-region? region1 region2) expected)
		  "(intersects? ~a ~a)" (printable-value region1) (printable-value region2))
	      (is (equal? (region-intersects-region? region2 region1) expected)
		  "(intersects? ~a ~a)" (printable-value region2) (printable-value region1))
	      (is (region-intersects-region? region1 *everywhere*)
		  (format nil "~a intersects $everywhere" (printable-value region1)))
	      (is (not (region-intersects-region? region1 *nowhere*))
		  (format nil "~a doesn't intersect $nowhere" (printable-value region1))))))


#||
/// LTRB test harness

define method printable-value (box :: <bounding-box>) => (value)
  let (left, top, right, bottom) = box-edges(box);
  vector(floor(left), floor(top), floor(right), floor(bottom))
end method printable-value;
||#

(defmethod printable-value ((box <bounding-box>))
  (multiple-value-bind (left top right bottom)
      (box-edges box)
    (vector (floor left) (floor top) (floor right) (floor bottom))))


#||
define method printable-value (sequence :: <sequence>) => (value)
  map-as(<vector>, printable-value, sequence)
end method printable-value;
||#

(defmethod printable-value ((sequence sequence))
  (map 'vector #'printable-value sequence))


#||
define method expected-ltrb-equals-boxes? 
    (expected, boxes)
 => (equal? :: <boolean>)
  expected = boxes
end method expected-ltrb-equals-boxes?;
||#

(defmethod expected-ltrb-equals-boxes? (expected boxes)
  (equal? expected boxes))


#||
define method expected-ltrb-equals-boxes?
    (expected :: <bounding-box>, boxes :: <sequence>)
 => (equal? :: <boolean>)
  size(boxes) = 1 & boxes[0] = expected
end method expected-ltrb-equals-boxes?;
||#

(defmethod expected-ltrb-equals-boxes? ((expected <bounding-box>) (boxes sequence))
  (and (= (length boxes) 1)
       (equal? (nth 0 boxes) expected)))


(defmethod expected-ltrb-equals-boxes? ((expected <bounding-box>) (boxes array))
  (and (= (length boxes) 1)
       (equal? (aref boxes 0) expected)))


#||
// This method has to allow for the boxes to be in a different order
define method expected-ltrb-equals-boxes? 
    (expected :: <sequence>, boxes :: <sequence>)
 => (equal? :: <boolean>)
  size(expected) = size(boxes)
    & every?(method (box)
               member?(box, boxes, test: \=)
             end,
             expected)
end method expected-ltrb-equals-boxes?;
||#

(defmethod expected-ltrb-equals-boxes? ((expected sequence) (boxes sequence))
  (and (= (length expected) (length boxes))
       (every #'(lambda (box)
		  (find box boxes :test #'equal?))
	      expected)))

#||
define method ltrb-transform-result 
    (transform :: <transform>, value) => (value)
  value
end method ltrb-transform-result;
||#

(defmethod ltrb-transform-result ((transform <transform>) value)
  value)


#||
define method ltrb-transform-result
    (transform :: <transform>, box :: <bounding-box>) => (region)
  transform-region(transform, box)
end method ltrb-transform-result;
||#

(defmethod ltrb-transform-result ((transform <transform>) (box <bounding-box>))
  (transform-region transform box))


#||
define method ltrb-transform-result 
    (transform :: <transform>, value :: <sequence>) => (result)
  map-as(<vector>, curry(ltrb-transform-result, transform), value)
end method ltrb-transform-result;
||#

(defmethod ltrb-transform-result ((transform <transform>) (value sequence))
  (map 'vector (alexandria:curry #'ltrb-transform-result transform) value))


#||
define method ltrb-apply-with-transformed-box
    (function :: <function>, transform :: <transform>,
     l1, t1, r1, b1, l2, t2, r2, b2, 
     expected)
  let box1 = make-bounding-box(l1, t1, r1, b1);
  let box2 = make-bounding-box(l2, t2, r2, b2);
  let tbox1 = transform-region(transform, box1);
  let tbox2 = transform-region(transform, box2);
  let transformed-expected = expected
                               & ltrb-transform-result(transform, expected);
  let (l1, t1, r1, b1) = box-edges(tbox1);
  let (l2, t2, r2, b2) = box-edges(tbox2);
  function(l1, t1, r1, b1, l2, t2, r2, b2, transformed-expected)
end method ltrb-apply-with-transformed-box;
||#

(defmethod ltrb-apply-with-transformed-box ((fn function) (transform <transform>)
					    l1 t1 r1 b1 l2 t2 r2 b2
					    expected)
  (let* ((box1 (make-bounding-box l1 t1 r1 b1))
         (box2 (make-bounding-box l2 t2 r2 b2))
         (tbox1 (transform-region transform box1))
         (tbox2 (transform-region transform box2))
         (transformed-expected (and expected
                                    (ltrb-transform-result transform expected))))
    (multiple-value-bind (l1 t1 r1 b1)
	(box-edges tbox1)
      (multiple-value-bind (l2 t2 r2 b2)
	  (box-edges tbox2)
	(funcall fn l1 t1 r1 b1 l2 t2 r2 b2 transformed-expected)))))


#||
define method ltrb-apply-function
    (transform :: <transform>, function :: <function>, test :: <sequence>)
  ltrb-apply-with-transformed-box
     (function, transform, 
      test[0], test[1], test[2], test[3],
      test[4], test[5], test[6], test[7],
      test[8])
end method ltrb-apply-function;
||#

(defmethod ltrb-apply-function ((transform <transform>) (fn function) (test sequence))
  (ltrb-apply-with-transformed-box fn transform
				   (elt test 0) (elt test 1) (elt test 2) (elt test 3)
				   (elt test 4) (elt test 5) (elt test 6) (elt test 7)
				   (elt test 8)))


#||
define method ltrb-apply-to-x-and-y 
    (transform :: <transform>, function :: <function>, test :: <sequence>)
  ltrb-apply-with-transformed-box
     (function, transform, 
      test[0], test[1], test[2], test[3],
      test[4], test[5], test[6], test[7],
      test[8])
  & ltrb-apply-with-transformed-box
       (function, transform,
        test[4], test[5], test[6], test[7],
        test[0], test[1], test[2], test[3],
        test[8])
end method ltrb-apply-to-x-and-y;
||#

(defmethod ltrb-apply-to-x-and-y ((transform <transform>) (function function) (test sequence))
  (and (ltrb-apply-with-transformed-box function transform
					(elt test 0) (elt test 1) (elt test 2) (elt test 3)
					(elt test 4) (elt test 5) (elt test 6) (elt test 7)
					(elt test 8))
       (ltrb-apply-with-transformed-box function transform
					(elt test 4) (elt test 5) (elt test 6) (elt test 7)
					(elt test 0) (elt test 1) (elt test 2) (elt test 3)
					(elt test 8))))



#||
define variable *ltrb-transformations*
  = list($identity-transform,
         make-transform(0, 1, 1, 0, 0, 0),
         make-transform(-1, 0, 0, 1, 0, 0),
         make-transform(1, 0, 0, -1, 0, 0),
         make-translation-transform(20, 20));
||#

(defparameter *ltrb-transformations*
  (list *identity-transform*
	(make-transform 0 1 1 0 0 0)
	(make-transform -1 0 0 1 0 0)
	(make-transform 1 0 0 -1 0 0)
	(make-translation-transform 20 20)))


#||
define method do-ltrb-tests
    (function :: <function>, tests :: <sequence>, #key commutative? = #t) => ()
  do(method (test)
       do(rcurry(case
                   commutative? => ltrb-apply-to-x-and-y;
                   otherwise    => ltrb-apply-function;
                 end,
                 function, test),
              *ltrb-transformations*)
     end,
     tests)
end method do-ltrb-tests;
||#

(defmethod do-ltrb-tests ((fn function) (tests sequence) &key (commutative? t))
  (mapcar #'(lambda (test)
	      (map nil (alexandria:rcurry (if commutative?
					      #'ltrb-apply-to-x-and-y
					      #'ltrb-apply-function)
			       fn
			       test)
		   *ltrb-transformations*))
	  tests))


#||
/// <or-result>
/// An object that has a number of equally valid test results

define class <or-result> (<object>)
  constant slot or-values, init-keyword: values:;
end class <or-result>;
||#

(defclass <or-result> ()
  ((or-values :initarg :values :reader or-values)))


#||
define method or-result 
    (#rest values) => (or-result :: <or-result>)
  make(<or-result>, values: values)
end method or-result;
||#

(defmethod or-result (&rest values)
  (make-instance '<or-result> :values values))


#||
define method printable-value (value :: <or-result>) => (value)
  format-to-string("OR %=", 
                   map-as(<vector>, printable-value, or-values(value)))
end method printable-value;
||#

(defmethod printable-value ((value <or-result>))
  (format nil "OR ~a"
	  (map 'vector #'printable-value (or-values value))))


#||
define method expected-ltrb-equals-boxes? 
    (expected :: <or-result>, boxes) => (equal? :: <boolean>)
  any?(method (or-value)
         expected-ltrb-equals-boxes?(or-value, boxes)
       end,
       or-values(expected))
end method expected-ltrb-equals-boxes?;
||#

(defmethod expected-ltrb-equals-boxes? ((expected <or-result>) boxes)
  (some #'(lambda (or-value)
	    (expected-ltrb-equals-boxes? or-value boxes))
	(or-values expected)))


#||
define method ltrb-transform-result 
    (transform :: <transform>, value :: <or-result>)
 => (result :: <or-result>)
  make(<or-result>,
       values: map-as(<vector>,
                      curry(ltrb-transform-result, transform),
                      or-values(value)))
end method ltrb-transform-result;
||#

(defmethod ltrb-transform-result ((transform <transform>) (value <or-result>))
  (make-instance '<or-result>
		 :values (map 'vector
			      (alexandria:curry #'ltrb-transform-result transform)
			      (or-values value))))


#||
/// ltrb tests

// note that the or-result value here is because there are two equally
// valid possible values for the difference, and the one that gets chosen
// depends on how the ltrbs are transformed. Also it is better to have
// all possible values known by the test, even if one of them is never
// returned with the current algorithm.
define variable *ltrb-difference-tests*
  = list(list(0, 0, 0, 0, 0, 0, 0, 0,
              #f),
         list(0, 0, 10, 10, 20, 20, 30, 30,
              make-bounding-box(0, 0, 10, 10)),
         list(10, 0, 30, 20, 20, 10, 40, 30,
              or-result(vector(make-bounding-box(10, 10, 20, 20),
                               make-bounding-box(10, 0,  30, 10)),
                        vector(make-bounding-box(20, 0,  30, 10),
                               make-bounding-box(10, 0,  20, 20)))));
||#

(defparameter *ltrb-difference-tests*
  (list (list 0  0 0  0  0  0  0  0
	      nil)
	(list 0  0 10 10 20 20 30 30
	      (make-bounding-box 0 0 10 10))
	(list 10 0 30 20 20 10 40 30
	      (or-result (vector (make-bounding-box 10 10 20 20)
				 (make-bounding-box 10 0 30 10))
			 (vector (make-bounding-box 20 0 30 10)
				 (make-bounding-box 10 0 20 20))))))


#||
define method check-ltrb-test
    (name :: <string>, l1, t1, r1, b1, l2, t2, r2, b2, value, expected) => ()
  let args = format-to-string("(%d, %d, %d, %d, %d, %d, %d, %d)",
                              floor(l1), floor(t1), floor(r1), floor(b1),
                              floor(l2), floor(t2), floor(r2), floor(b2));
  check-equal(concatenate(name, args), value, expected)
end method check-ltrb-test;
||#

(defun check-ltrb-test (name l1 t1 r1 b1 l2 t2 r2 b2 value expected)
  (let ((args (format nil "(~a, ~a, ~a, ~a) (~a, ~a, ~a, ~a)"
		      (floor l1) (floor t1) (floor r1) (floor b1)
		      (floor l2) (floor t2) (floor r2) (floor b2))))
    (is (equal? value expected) (concatenate 'string name " " args))))


#||
define method expected-ltrb-difference?
    (l1, t1, r1, b1, l2, t2, r2, b2, expected)
  let difference = ltrb-difference(l1, t1, r1, b1, l2, t2, r2, b2);
  check-ltrb-test("ltrb-difference", l1, t1, r1, b1, l2, t2, r2, b2,
                  expected-ltrb-equals-boxes?(expected, difference),
                  #t)
end method expected-ltrb-difference?;
||#

(defun expected-ltrb-difference? (l1 t1 r1 b1 l2 t2 r2 b2 expected)
  (let ((difference (ltrb-difference l1 t1 r1 b1 l2 t2 r2 b2)))
    (check-ltrb-test "ltrb-difference"
		     l1 t1 r1 b1
		     l2 t2 r2 b2
		     (expected-ltrb-equals-boxes? expected difference)
		     t)))


#||
define test ltrb-difference-test ()
  do-ltrb-tests(expected-ltrb-difference?, *ltrb-difference-tests*,
                commutative?: #f)
end test ltrb-difference-test;
||#

(test ltrb-difference-test
      :description "ltrb-difference tests"
      (do-ltrb-tests #'expected-ltrb-difference?
	             *ltrb-difference-tests*
		     :commutative? nil))


#||
define variable *ltrb-equals-ltrb?-tests*
  = list(list(0, 0, 0, 0, 0, 0, 0, 0, #t),
         list(100, 0, 0, 0, 0, 0, 0, 0, #f),
         list(0, 100, 0, 0, 0, 0, 0, 0, #f),
         list(0, 0, 100, 0, 0, 0, 0, 0, #f),
         list(0, 0, 0, 100, 0, 0, 0, 0, #f),
         list(0, 0, 0, 0, 100, 0, 0, 0, #f),
         list(0, 0, 0, 0, 0, 100, 0, 0, #f),
         list(0, 0, 0, 0, 0, 0, 100, 0, #f),
         list(0, 0, 0, 0, 0, 0, 0, 100, #f),
         list(100, 100, 200, 200, 100, 100, 200, 200, #t));
||#

(defparameter *ltrb-equals-ltrb?-tests*
  (list (list 0 0 0 0 0 0 0 0 t)
	(list 100 0 0 0 0 0 0 0 nil)
	(list 0 100 0 0 0 0 0 0 nil)
	(list 0 0 100 0 0 0 0 0 nil)
	(list 0 0 0 100 0 0 0 0 nil)
	(list 0 0 0 0 100 0 0 0 nil)
	(list 0 0 0 0 0 100 0 0 nil)
	(list 0 0 0 0 0 0 100 0 nil)
	(list 0 0 0 0 0 0 0 100 nil)
	(list 100 100 200 200 100 100 200 200 t)))


#||
define test ltrb-equals-ltrb?-test ()
  for (test in *ltrb-equals-ltrb?-tests*)
    check-ltrb-test("ltrb-equals-ltrb?",
                    test[0], test[1], test[2], test[3],
                    test[4], test[5], test[6], test[7],
                    ltrb-equals-ltrb?(test[0], test[1], test[2], test[3],
                                      test[4], test[5], test[6], test[7]),
                    test[8])
  end;
end test ltrb-equals-ltrb?-test;
||#

(test ltrb-equals-ltrb?-test
      :description "ltrb-equals-ltrb? checks"
      (loop for test in *ltrb-equals-ltrb?-tests*
	 do (check-ltrb-test "ltrb-equals-ltrb?"
			     (nth 0 test) (nth 1 test) (nth 2 test) (nth 3 test)
			     (nth 4 test) (nth 5 test) (nth 6 test) (nth 7 test)
			     (ltrb-equals-ltrb? (nth 0 test) (nth 1 test) (nth 2 test) (nth 3 test)
						(nth 4 test) (nth 5 test) (nth 6 test) (nth 7 test))
			     (nth 8 test))))


#||
define variable *ltrb-intersection-tests*
  = list(list(0, 0, 0, 0, 0, 0, 0, 0, make-bounding-box(0, 0, 0, 0)),
         list(0, 0, 20, 20, 0, 0, 20, 20,
              make-bounding-box(0, 0, 20, 20)),
         list(0, 0, 40, 40, 10, 10, 30, 30,
              make-bounding-box(10, 10, 30, 30)),
         list(0, 0, 100, 100, 50, 50, 200, 200,
              make-bounding-box(50, 50, 100, 100)),
         list(0, 0, 100, 100, 200, 200, 300, 300,
              #f),
         list(0, 0, 20, 20, 10, 10, 30, 30,
              make-bounding-box(10, 10, 20, 20)),
         list(0, 10, 20, 30, 10, 0, 30, 20,
              make-bounding-box(10, 10, 20, 20)),
         list(0, 0, 40, 20, 10, 10, 30, 30,
              make-bounding-box(10, 10, 30, 20)),
         list(0, 10, 40, 30, 10, 0, 30, 20,
              make-bounding-box(10, 10, 30, 20)),
         list(0, 0, 100, 100, 200, 200, 300, 300,
              #f),
         list(0, 0, 100, 100, 100, 100, 200, 200,
              make-bounding-box(100, 100, 100, 100)));
||#

(defparameter *ltrb-intersection-tests*
  (list (list 0 0 0 0 0 0 0 0 (make-bounding-box 0 0 0 0))
	(list 0 0 20 20 0 0 20 20 (make-bounding-box 0 0 20 20))
	(list 0 0 40 40 10 10 30 30 (make-bounding-box 10 10 30 30))
	(list 0 0 100 100 50 50 200 200 (make-bounding-box 50 50 100 100))
	(list 0 0 100 100 200 200 300 300 nil)
	(list 0 0 20 20 10 10 30 30 (make-bounding-box 10 10 20 20))
	(list 0 10 20 30 10 0 30 20 (make-bounding-box 10 10 20 20))
	(list 0 0 40 20 10 10 30 30 (make-bounding-box 10 10 30 20))
	(list 0 10 40 30 10 0 30 20 (make-bounding-box 10 10 30 20))
	(list 0 0 100 100 200 200 300 300 nil)
	(list 0 0 100 100 100 100 200 200
	      (make-bounding-box 100 100 100 100))))


#||
define method expected-ltrb-intersection?
    (l1, t1, r1, b1, l2, t2, r2, b2, expected)
  let intersection = ltrb-intersection(l1, t1, r1, b1, l2, t2, r2, b2);
  check-ltrb-test("ltrb-intersection", l1, t1, r1, b1, l2, t2, r2, b2,
                  intersection, expected)
end method expected-ltrb-intersection?;
||#

(defun expected-ltrb-intersection? (l1 t1 r1 b1 l2 t2 r2 b2 expected)
  (let ((intersection (ltrb-intersection l1 t1 r1 b1 l2 t2 r2 b2)))
    (check-ltrb-test "ltrb-intersection" l1 t1 r1 b1 l2 t2 r2 b2
		     intersection
		     expected)))


#||
define test ltrb-intersection-test ()
  do-ltrb-tests(expected-ltrb-intersection?, *ltrb-intersection-tests*)
end test ltrb-intersection-test;
||#

(test ltrb-intersection-test
      :description "ltrb-intersection checks"
      (do-ltrb-tests #'expected-ltrb-intersection? *ltrb-intersection-tests*))


#||
define method expected-ltrb-intersects-ltrb?
    (l1, t1, r1, b1, l2, t2, r2, b2, expected)
  let expected = if (expected) #t else #f end;
  let intersection = ltrb-intersects-ltrb?(l1, t1, r1, b1, l2, t2, r2, b2);
  check-ltrb-test("ltrb-intersects-ltrb?", l1, t1, r1, b1, l2, t2, r2, b2,
                  expected, intersection)
end method expected-ltrb-intersects-ltrb?;
||#

(defun expected-ltrb-intersects-ltrb? (l1 t1 r1 b1 l2 t2 r2 b2 expected)
  (let ((expected (if expected t nil))
	(intersection (ltrb-intersects-ltrb? l1 t1 r1 b1 l2 t2 r2 b2)))
    (check-ltrb-test "ltrb-intersects-ltrb?" l1 t1 r1 b1 l2 t2 r2 b2
		     expected
		     intersection)))


#||
define test ltrb-intersects-ltrb?-test ()
  do-ltrb-tests(expected-ltrb-intersects-ltrb?, *ltrb-intersection-tests*)
end test ltrb-intersects-ltrb?-test;
||#

(test ltrb-intersects-ltrb?-test
      :description "LTRB-INTERSECTS-LTRB?-TEST"
      (do-ltrb-tests #'expected-ltrb-intersects-ltrb? *ltrb-intersection-tests*))


#||
define variable *ltrb-union-tests*
  = list(list(0,0,100,100, 0, 0, 200, 200,
              make-bounding-box(0, 0, 200, 200)),
         list(0,0,100,100, 100, 100, 200, 200,
              vector(make-bounding-box(0, 0, 100, 100),
                     make-bounding-box(100, 100, 200, 200))));
||#

(defparameter *ltrb-union-tests*
  ;; note: #'ltrb-union returns a LIST, not a VECTOR.
  ;; fixme: straight away, back to the lack of generic sequences being a pain. Maybe write a library?
  (list (list 0 0 100 100 0 0 200 200 (make-bounding-box 0 0 200 200))
	(list 0 0 100 100 100 100 200 200
	      (list (make-bounding-box 0 0 100 100)
		    (make-bounding-box 100 100 200 200)))))


#||
define method expected-ltrb-union?
    (l1, t1, r1, b1, l2, t2, r2, b2, expected-union)
  let union = ltrb-union(l1, t1, r1, b1, l2, t2, r2, b2);
  check-ltrb-test("ltrb-union", l1, t1, r1, b1, l2, t2, r2, b2,
                  expected-ltrb-equals-boxes?(expected-union, union),
                  #t)
end method expected-ltrb-union?;
||#

(defun expected-ltrb-union? (l1 t1 r1 b1 l2 t2 r2 b2 expected-union)
  ;; note: #'ltrb-union returns a LIST, not a VECTOR.
  ;; also: is 'expected-ltrb-equals-boxes? doing the right thing? - need to debug this!
  ;; I think LTRB-UNION is just wrong
  (let ((union (ltrb-union l1 t1 r1 b1 l2 t2 r2 b2)))
    (check-ltrb-test "ltrb-union" l1 t1 r1 b1 l2 t2 r2 b2
		     (expected-ltrb-equals-boxes? expected-union union)
		     t)))


#||
define test ltrb-union-test ()
  do-ltrb-tests(expected-ltrb-union?, *ltrb-union-tests*)
end test ltrb-union-test;
||#

(test ltrb-union-test
      :description "LTRB-UNION-TEST"
      (do-ltrb-tests #'expected-ltrb-union? *ltrb-union-tests*))


#||
define test regions-test ()
  check-true("region-empty? on an empty region",
             region-empty?(make-bounding-box(0, 0, 0, 0)));
  check-false("region-empty? false on a non-empty region",
              region-empty?(make-bounding-box(0, 0, 100, 100)));
end test regions-test;
||#

(test regions-test
      :description "REGIONS-TEST"
      (is (region-empty? (make-bounding-box 0 0 0 0)) "region-empty? on an empty region")
      (is (not (region-empty? (make-bounding-box 0 0 100 100))) "region-empty? false on a non-empty region"))


#||

/// Define the regions test suite

define suite duim-regions-suite ()
  test region-equal-test;
  test region-contains-position-test;
  test region-contains-region-test;
  test region-intersects-region-test;
  test ltrb-difference-test;
  test ltrb-equals-ltrb?-test;
  test ltrb-intersection-test;
  test ltrb-intersects-ltrb?-test;
  test ltrb-union-test;
  test regions-test;
end suite duim-regions-suite;
||#

(defun test-regions ()
  (run! 'duim-regions))

