;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-TESTS -*-
(in-package #:duim-tests)

(def-suite duim-dialogs
    :description "DUIM-DIALOGS test suite"
    :in all-tests)
(in-suite duim-dialogs)

#||
define duim-frames class-test <dialog-frame> ()
  //---*** Fill this in...
end class-test <dialog-frame>;

define duim-frames class-test <property-frame> ()
  //---*** Fill this in...
end class-test <property-frame>;

define duim-frames class-test <property-page> ()
  //---*** Fill this in...
end class-test <property-page>;

define duim-frames class-test <wizard-frame> ()
  test-wizards()
end class-test <wizard-frame>;

define duim-frames class-test <wizard-page> ()
  //---*** Fill this in...
end class-test <wizard-page>;


/// Dialog test suites

define test notify-user-test ()
  //---*** Fill this in!
  #f
end test notify-user-test;
||#

(test notify-user-test
  :description "notify user test"
  nil)


#||
define test choose-file-test ()
  //---*** Fill this in!
  #f
end test choose-file-test;
||#

(test choose-file-test
  :description "choose file test"
  nil)


#||
define test choose-directory-test ()
  //---*** Fill this in!
  #f
end test choose-directory-test;
||#

(test choose-directory-test
  :description "choose directory test"
  nil)


#||
define test choose-color-test ()
  //---*** Fill this in!
  #f
end test choose-color-test;
||#

(test choose-color-test
  :description "choose color test"
  nil)


#||
define test choose-text-style-test ()
  //---*** Fill this in!
  #f
end test choose-text-style-test;
||#

(test choose-text-style-test
  :description "choose text style test"
  nil)


#||
define test choose-from-dialog-test ()
  //---*** Fill this in!
  #f
end test choose-from-dialog-test;
||#

(test choose-from-dialog-test
  :description "choose from dialog test"
  nil)


#||
define test choose-from-menu-test ()
  //---*** Fill this in!
  #f
end test choose-from-menu-test;
||#

(test choose-from-menu-test
  :description "choose from menu test"
  nil)


#||

/// Wizard tests

define frame <test-wizard> (<wizard-frame>)
  pane first-button (frame)
    make(<button>, label: "One");
  pane second-button (frame)
    make(<button>, label: "Two");
  pane first-page (frame)
    vertically ()
      frame.first-button
    end;
  pane second-page (frame)
    vertically ()
      frame.second-button
    end;
end frame <test-wizard>;
||#

(define-frame <test-wizard> (<wizard-frame>)
  ((:pane first-button (frame) (make-test-pane (find-class '<button>)
					       :label "One"))
   (:pane second-button (frame) (make-test-pane (find-class '<button>)
						:label "Two"))
   (:pane first-page (frame) (vertically ()
			       (first-button frame)))
   (:pane second-page (frame) (vertically ()
				(second-button frame)))))


#||
define method initialize
    (wizard :: <test-wizard>, #key frame-manager: framem, #all-keys) => ()
  dialog-pages(wizard)
    := with-frame-manager (framem)
         vector(wizard.first-page, wizard.second-page)
       end;
  next-method();
end method initialize;
||#

;; Run this after shared-initialize but before any superclass :after
;; methods.

(defmethod initialize-instance ((wizard <test-wizard>) &key ((:frame-manager framem)) &allow-other-keys)
  (call-next-method)
  (setf (dialog-pages wizard)
	(with-frame-manager (framem)
	  (vector (first-page wizard) (second-page wizard)))))


#||
define method test-wizards
    () => ()
  let framem = find-test-frame-manager();
  let wizard = #f;
  check-true("wizard-frame first page is default",
	     begin
	       wizard := make-test-frame(<test-wizard>);
	       dialog-current-page(wizard) := wizard.first-page
	     end);
  when (wizard)
    check-false("wizard-frame back button is disabled and mapped",
	       begin
		 let back-button = dialog-back-button(wizard);
		 ~gadget-enabled?(back-button) & sheet-mapped?(back-button)
	       end);
    check-true("wizard-frame next button is enabled and mapped",
	       begin
		 let next-button = dialog-next-button(wizard);
		 gadget-enabled?(next-button) & sheet-mapped?(next-button)
	       end);
    check-true("wizard-frame exit button is enabled and unmapped",
	       begin
		 let exit-button = dialog-exit-button(wizard);
		 gadget-enabled?(exit-button) & ~sheet-mapped?(exit-button)
	       end);
    check-true("wizard-frame cancel button is enabled and mapped",
	       begin
		 let cancel-button = dialog-cancel-button(wizard);
		 gadget-enabled?(cancel-button) & sheet-mapped?(cancel-button)
	       end);
  end
end method test-wizards;
||#

(test test-wizards
  :description "test wizards"
  (let ((framem (find-test-frame-manager))
	(wizard nil))
    (declare (ignore framem))
    (is (progn
	  (setf wizard (make-test-frame (find-class '<test-wizard>)))
	  (setf (dialog-current-page wizard) (first-page wizard)))
	"wizard-frame first page is default")
    (when wizard
      (is-false (let ((back-button (dialog-back-button wizard)))
		  (and (not (gadget-enabled? back-button))
		       (sheet-mapped? back-button)))
		"wizard-frame back button is disabled and mapped")
      (let ((next-button (dialog-next-button wizard)))
	(is (and (gadget-enabled? next-button)
		 (sheet-mapped? next-button))
	    "wizard-frame next button is enabled and mapped")
	(let ((exit-button (dialog-exit-button wizard)))
	  (is (and (gadget-enabled? exit-button)
		   (not (sheet-mapped? exit-button)))
	      "wizard-frame exit button is enabled and unmapped")
	  (let ((cancel-button (dialog-cancel-button wizard)))
	    (is (and (gadget-enabled? cancel-button)
		     (sheet-mapped? cancel-button))
		"wizard-frame cancel button is enabled and mapped")))))))


#||

/// Define the menus test suite

define suite duim-dialogs-suite ()
  test notify-user-test;
  test choose-file-test;
  test choose-directory-test;
  test choose-color-test;
  test choose-text-style-test;
  test choose-from-dialog-test;
  test choose-from-menu-test;
end suite duim-dialogs-suite;
||#

(defun test-dialogs ()
  (run! 'duim-dialogs))
