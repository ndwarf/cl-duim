;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

(asdf:defsystem :duim-tests
  :description "Unit tests for DUIM utilities"
  :author "Steven Nunez <steve.nunez@symbolics.tech>"
  :license "Same as DUIM -- this is part of the DUIM-UTILS library."
  #+asdf-unicode :encoding #+asdf-unicode :utf-8
  :depends-on (#:fiveam
	       #:duim
	       #:duim-utilities/tests
	       #:duim-extended
	       #:extended-geometry)
  :serial t
  :components
  ((:file #:package)
   (:file #:main)
   (:file #:test-utilities)

   ;;; test-port + classes set up a test back-end; actual tests start at geometry
   (:file #:test-port)
   (:file #:classes)

   ;;; test suites + cases
   (:file #:geometry)
   (:file #:regions)
   (:file #:transforms)
   (:file #:graphics)
   (:file #:styles)
   (:file #:layouts)
   (:file #:frames)
   (:file #:gadgets)
   (:file #:menus)
   ;; (:file #:dialogs) ; No tests are currently implemented, nor were they in Dylan
   (:file #:scrolling)
   (:file #:events)
   (:file #:gestures)
   (:file #:commands))
  :perform (asdf:test-op (o s)
			 (uiop:symbol-call :fiveam :run-all-tests)))
