;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-TESTS -*-
(in-package #:duim-tests)

(def-suite duim-menus
    :description "DUIM-MENUS test suite"
    :in all-tests)
(in-suite duim-menus)

#||
define test frame-menu-bar-test ()
  let works-component
    = make-test-pane(<menu-box>, items: #("update", "clone"));
  let works-menu
    = make-test-pane(<menu>,
		     label: "works",
		     items: vector(works-component, "quit"));
  let help-menu
    = make-test-pane(<menu>,
		     label: "help",
		     items: #("about dylanworks"));
  let menu-bar = make-test-pane(<menu-bar>);
  add-child(menu-bar, works-menu);
  add-child(menu-bar, help-menu);
  let frame = make-test-frame(<test-frame>, menu-bar: menu-bar);
  frame
end test frame-menu-bar-test;
||#

(test frame-menu-bar-test
      :description "frame menu bar test"
      (let* ((works-component (make-test-pane (find-class '<menu-box>)
					      :items '("update" "clone")))
	     (works-menu (make-test-pane (find-class '<menu>)
					 :label "works"
					 :items (vector works-component "quit")))
	     (help-menu (make-test-pane (find-class '<menu>)
					:label "help"
					:items #("about dylanworks")))
	     (menu-bar (make-test-pane (find-class '<menu-bar>))))
	(add-child menu-bar works-menu)
	(add-child menu-bar help-menu)
	(let ((frame (make-test-frame (find-class '<test-frame>)
				      :menu-bar menu-bar)))
	  frame)))


#||
define test menu-parents-test ()
  let button = make-test-pane(<menu-button>, label: "test");
  let component
    = make-test-pane(<menu-box>,
		     selection-mode: #"single",
		     children: vector(button));
  check-equal("Initial menu button parent", sheet-parent(button), component);
  let menu = make-test-pane(<menu>, children: vector(component));
  check-equal("Initial menu component parent",
	      sheet-parent(component), menu);
  let menu-bar = make-test-pane(<menu-bar>, children: vector(menu));
  check-equal("Initial menu parent", sheet-parent(menu), menu-bar);
  let frame = make-test-frame(<test-frame>, menu-bar: menu-bar);
  check-true("Initial menu bar parent",
	     subchild?(top-level-sheet(frame), menu-bar));
  frame
end test menu-parents-test;
||#

(test menu-parents-test
      :description "menu parents test"
      (let* ((button (make-test-pane (find-class '<menu-button>)
				     :label "test"))
	     (component (make-test-pane (find-class '<menu-box>)
					:selection-mode :single
					:children (vector button))))
	(is (equal? (sheet-parent button) component)
	    "Initial menu button parent")
	(let ((menu (make-test-pane (find-class '<menu>)
				    :children (vector component))))
	  (is (equal? (sheet-parent component) menu)
	      "Initial menu component parent")
	  (let ((menu-bar (make-test-pane (find-class '<menu-bar>)
					  :children (vector menu))))
	    (is (equal? (sheet-parent menu) menu-bar)
		"Initial menu parent")
	    (let ((frame (make-test-frame (find-class '<test-frame>)
					  :menu-bar menu-bar)))
	      (is (subchild? (top-level-sheet frame) menu-bar)
		  "Initial menu bar parent")
	      frame)))))


#||

/// Menu box pane testing

define method test-menu-box-panes-buttons ()
  test-gadget-box-pane-buttons(<push-menu-box>);
  test-gadget-box-pane-buttons(<radio-menu-box>);
  test-gadget-box-pane-buttons(<check-menu-box>);
end method test-menu-box-panes-buttons;
||#

(defun test-menu-box-panes-buttons ()
  (test-gadget-box-pane-buttons (find-class '<push-menu-box>))
  (test-gadget-box-pane-buttons (find-class '<radio-menu-box>))
  (test-gadget-box-pane-buttons (find-class '<check-menu-box>)))


#||
define method test-menu-box-panes-button-selections ()
  let rbp = make-collection-gadget(<radio-menu-box>);
  verify-gadget-box-pane-button-selection(rbp);
  let cbp = make-collection-gadget(<check-menu-box>,
				   selection: #(0, 2));
  verify-gadget-box-pane-button-selection(cbp);
  cbp
end method test-menu-box-panes-button-selections;
||#

(defun test-menu-box-panes-button-selections ()
  (let ((rbp (make-collection-gadget (find-class '<radio-menu-box>))))
    (verify-gadget-box-pane-button-selection rbp))
  (let ((cbp (make-collection-gadget (find-class '<check-menu-box>)
				     :selection '(0 2))))
    (verify-gadget-box-pane-button-selection cbp)
    cbp))


#||
define test menu-box-panes-test ()
  test-menu-box-panes-buttons();
  test-menu-box-panes-button-selections();
end test menu-box-panes-test;
||#

(test menu-box-panes-test
  :description "menu box panes test"
  (test-menu-box-panes-buttons)
  (test-menu-box-panes-button-selections))


#||

/// Define the menus test suite

define suite duim-menus-suite ()
  test frame-menu-bar-test;
  test menu-parents-test;
  test menu-box-panes-test;
end suite duim-menus-suite;
||#

(defun test-menus ()
  (format t "~%test-duim-menus - surely there's more checks than this...")
  (run! 'duim-menus))

