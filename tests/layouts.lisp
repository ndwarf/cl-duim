;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-TESTS -*-
(in-package #:duim-tests)

(def-suite duim-layouts
    :description "DUIM-LAYOUTS test suite"
    :in all-tests)
(in-suite duim-layouts)

#||
define sideways method make-test-instance
    (class :: subclass(<layout>)) => (instance :: <layout>)
  make-test-pane(class)
end method make-test-instance;

define sideways method make-test-instance
    (class :: subclass(<grid-layout>)) => (instance :: <grid-layout>)
  let cell-space-requirement
    = make(<space-requirement>, width: 100, height: 100);
  make-test-pane(<grid-layout>, cell-space-requirement: cell-space-requirement)
end method make-test-instance;

define duim-layouts class-test <layout> ()
  //---*** Fill this in...
end class-test <layout>;

define duim-layouts class-test <basic-composite-pane> ()
  //---*** Fill this in...
end class-test <basic-composite-pane>;

define duim-layouts class-test <drawing-pane> ()
  //---*** Fill this in...
end class-test <drawing-pane>;

define duim-layouts class-test <leaf-pane> ()
  //---*** Fill this in...
end class-test <leaf-pane>;

define duim-layouts class-test <multiple-child-composite-pane> ()
  //---*** Fill this in...
end class-test <multiple-child-composite-pane>;

define duim-layouts class-test <null-pane> ()
  //---*** Fill this in...
end class-test <null-pane>;

define duim-layouts class-test <simple-pane> ()
  //---*** Fill this in...
end class-test <simple-pane>;

define duim-layouts class-test <single-child-composite-pane> ()
  //---*** Fill this in...
end class-test <single-child-composite-pane>;

define duim-layouts class-test <top-level-sheet> ()
  //---*** Fill this in...
end class-test <top-level-sheet>;
||#

;;; Layout tests

;;; Some default values
(defparameter *layout-default-width*  200)
(defparameter *layout-default-height* 300)
(defparameter *child-explicit-width*  200)
(defparameter *child-explicit-height* 300)

#||

/// Layout resizing

define method test-layout-child-resizing 
    (class :: <class>, #rest args) => ()
  let gadget-name = gadget-class-name(class);
  let child-1 = make-test-pane(<push-button>, label: "Child 1");
  let sub-layout = make-test-pane(<column-layout>, children: vector(child-1));
  let layout 
    = apply(make-test-pane, class,
	    child: sub-layout, contents: vector(vector(sub-layout)),
	    args);
  check-layout-pane-layout(layout,
			   concatenate("Before resizing ", gadget-name));
  let child-2 = make-test-pane(<push-button>, label: "Child 2");
  sheet-children(sub-layout) := vector(child-1, child-2);
  relayout-parent(sub-layout);
  check-layout-pane-layout(layout,
			   concatenate("After resizing ", gadget-name));
end method test-layout-child-resizing;
||#
(defmethod test-layout-child-resizing ((class class) &rest args)
  (let* ((gadget-name (gadget-class-name class))
	 (child-1 (make-test-pane (find-class '<push-button>) :label "Child 1"))
	 (sub-layout (make-test-pane (find-class '<column-layout>) :children (vector child-1)))
	 (layout (apply #'make-test-pane class
			:child sub-layout :contents (vector (vector sub-layout))
			args)))
    (check-layout-pane-layout layout
			      (concatenate 'string "Before resizing " gadget-name))
    (let ((child-2 (make-test-pane (find-class '<push-button>) :label "Child 2")))
      (setf (sheet-children sub-layout) (vector child-1 child-2))
      (relayout-parent sub-layout)
      (check-layout-pane-layout layout (concatenate 'string "After resizing " gadget-name)))))


#||
define method test-multiple-child-layout-manipulation
    (class :: <class>, #rest class-args) => ()
  let class-name = gadget-class-name(class);
  let button1 = make-test-pane(<test-push-button-pane>);
  let layout = apply(make-test-pane, class, children: vector(button1), class-args);
  check-equal(concatenate(class-name, " child's initial parent"),
              sheet-parent(button1), layout);
  let button2 = make-test-pane(<test-radio-button-pane>);
  replace-child(layout, button1, button2);
  check-equal(concatenate(class-name, " replace-child old child unparented"),
              sheet-parent(button1), #f);
  check-equal(concatenate(class-name, " replace-child new child's parent"),
              sheet-parent(button2), layout);
  let button3 = make-test-pane(<test-check-button-pane>);
  sheet-children(layout) := vector(button3);
  let button4 = make-test-pane(<test-push-button-pane>);
  add-child(layout, button4);
  check-equal(concatenate(class-name, " add-child old child still parented"),
              sheet-parent(button3), layout);
  check-equal(concatenate(class-name, " add-child new child's parent"),
              sheet-parent(button4), layout);
  check-equal(concatenate(class-name, " add-child updates parent's children"),
              sheet-children(layout), vector(button3, button4));
  remove-child(layout, button3);
  check-equal(concatenate(class-name, " removed child unparented"),
              sheet-parent(button3), #f);
end method test-multiple-child-layout-manipulation;
||#
(defmethod test-multiple-child-layout-manipulation ((class class) &rest class-args)
  (let* ((class-name (gadget-class-name class))
	 (button1 (make-test-pane (find-class '<test-push-button-pane>)))
	 (layout (apply #'make-test-pane class :children (vector button1) class-args)))
    (is (equal? (sheet-parent button1) layout)
	(concatenate 'string class-name " child's initial parent"))
    (let ((button2 (make-test-pane (find-class '<test-radio-button-pane>))))
      (replace-child layout button1 button2)
      (is (equal? (sheet-parent button1) nil)
	  (concatenate 'string class-name " replace-child old child unparented"))
      (is (equal? (sheet-parent button2) layout)
	  (concatenate 'string class-name " replace-child new child's parent"))
      (let ((button3 (make-test-pane (find-class '<test-check-button-pane>))))
	(setf (sheet-children layout) (vector button3))
	(let ((button4 (make-test-pane (find-class '<test-push-button-pane>))))
	  (add-child layout button4)
	  (is (equal? (sheet-parent button3) layout)
	      (concatenate 'string class-name " add-child old child still parented"))
	  (is (equal? (sheet-parent button4) layout)
	      (concatenate 'string class-name " add-child new child's parent"))
	  (is (equal? (sheet-children layout) (vector button3 button4))
	      (concatenate 'string class-name " add-child updates parent's children"))
	  (remove-child layout button3)
	  (is (equal? (sheet-parent button3) nil)
	      (concatenate 'string class-name " removed child unparented")))))))


#||
/// expected-width and expected-height

// I've made these methods horribly complicated so that each test can
// pick and choose as to the simplest place to redefine. It works as
// follows:
//
// 1. expected-named-size 
// This takes the name of the size it is looking for. Redefining on this
// method means you can implement a single method that will do everything.
// 2. expected-named-width/expected-named-height
// This one can also be subclasses, in this case if all of the width
// cases can be handled in one go, for example.
// 3. expected-<xxx>-width/expected-<xxx>-height
// These methods allow redefinition of the methods that produce each
// individual result

define generic expected-default-size
    (name, #key width, height) => (size :: <integer>);
||#

(defgeneric expected-default-size (name &key width height))


#||
define generic expected-named-size
    (pane, name, #key width, height, #all-keys) => (size :: <integer>);
||#

(defgeneric expected-named-size (pane name &key width height &allow-other-keys))


#||
define generic expected-named-width
    (pane, name, #key width, height, #all-keys) => (size :: <integer>);
||#

(defgeneric expected-named-width (pane name &key width height &allow-other-keys))


#||
define generic expected-named-height
    (pane, name, #key width, height, #all-keys) => (size :: <integer>);
||#

(defgeneric expected-named-height (pane name &key width height &allow-other-keys))


#||
define generic expected-width 
    (pane, #key, #all-keys) => (width :: <integer>);
||#

(defgeneric expected-width (pane &key &allow-other-keys))


#||
define generic expected-min-width 
    (pane, #key, #all-keys) => (width :: <integer>);
||#

(defgeneric expected-min-width (pane &key &allow-other-keys))


#||
define generic expected-max-width 
    (pane, #key, #all-keys) => (width :: <integer>);
||#

(defgeneric expected-max-width (pane &key &allow-other-keys))


#||
define generic expected-height
    (pane, #key, #all-keys) => (height :: <integer>);
||#

(defgeneric expected-height (pane &key &allow-other-keys))


#||
define generic expected-min-height
    (pane, #key, #all-keys) => (height :: <integer>);
||#

(defgeneric expected-min-height (pane &key &allow-other-keys))


#||
define generic expected-max-height
    (pane, #key, #all-keys) => (height :: <integer>);
||#

(defgeneric expected-max-height (pane &key &allow-other-keys))


#||
define method expected-default-size 
    (name :: <symbol>, #key width, height) => (size :: <integer>)
  select (name)
    #"width"      => width | 100;
    #"min-width"  => 0;
    #"max-width"  => $fill;
    #"height"     => height | 100;
    #"min-height" => 0;
    #"max-height" => $fill;
  end
end method expected-default-size;
||#

(defmethod expected-default-size ((name symbol) &key width height)
  (ecase name
    (:width (or width 100))
    (:min-width 0)
    (:max-width +fill+)
    (:height (or height 100))
    (:min-height 0)
    (:max-height +fill+)))


#||
define method expected-size-function 
    (name :: <symbol>) => (size-function :: <function>)
  select (name)
    #"width"      => expected-width;
    #"min-width"  => expected-min-width;
    #"max-width"  => expected-max-width;
    #"height"     => expected-height;
    #"min-height" => expected-min-height;
    #"max-height" => expected-max-height;
  end
end method expected-size-function;
||#

(defmethod expected-size-function ((name symbol))
  (ecase name
    (:width      #'expected-width)
    (:min-width  #'expected-min-width)
    (:max-width  #'expected-max-width)
    (:height     #'expected-height)
    (:min-height #'expected-min-height)
    (:max-height #'expected-max-height)))


#||
define method expected-named-size
    (pane, name, #key width, height) => (size :: <integer>)
  select (name)
    #"width"      => space-requirement-width;
    #"min-width"  => space-requirement-min-width;
    #"max-width"  => space-requirement-max-width;
    #"height"     => space-requirement-height;
    #"min-height" => space-requirement-min-height;
    #"max-height" => space-requirement-max-height;
  end(pane, compose-space(pane, width: width, height: height))
end method expected-named-size;
||#

(defmethod expected-named-size (pane name &key width height)
  (let ((function (ecase name
		    (:width #'space-requirement-width)
		    (:min-width #'space-requirement-min-width)
		    (:max-width #'space-requirement-max-width)
		    (:height #'space-requirement-height)
		    (:min-height #'space-requirement-min-height)
		    (:max-height #'space-requirement-max-height))))
    (funcall function pane (compose-space pane :width width :height height))))

    
#||
define method expected-width-given-default
    (pane, #rest args, #key width, #all-keys) => (width :: <integer>)
  apply(expected-constrained-width, pane, width, args)
  /*---*** code was this... is it still correct?
  max(apply(expected-min-width, pane, args),
      min(width, apply(expected-max-width, pane, args)))
  */
end method expected-width-given-default;
||#

(defmethod expected-width-given-default (pane &rest args &key width &allow-other-keys)
    (apply #'expected-constrained-width pane width args))


#||
define method expected-height-given-default 
    (pane, #rest args, #key height, #all-keys) => (width :: <integer>)
  apply(expected-constrained-height, pane, height, args)
  /*---*** code was this... is it still correct?
  max(apply(expected-min-height, pane, args),
      min(height, apply(expected-max-height, pane, args)))
  */
end method expected-height-given-default;
||#

(defmethod expected-height-given-default (pane &rest args &key height &allow-other-keys)
  (apply #'expected-constrained-height pane height args))


#||
define method expected-named-width
    (pane, name, #rest args, #key width, height, #all-keys)
 => (width :: <integer>)
  apply(expected-named-size, pane, name, args)
end method expected-named-width;
||#

(defmethod expected-named-width (pane name &rest args &key width height &allow-other-keys)
  (declare (ignore width height))
  (apply #'expected-named-size pane name args))

#||
define method expected-named-height 
    (pane, name, #rest args, #key width, height, #all-keys)
 => (height :: <integer>)
  apply(expected-named-size, pane, name, args)
end method expected-named-height;
||#

(defmethod expected-named-height (pane name &rest args &key width height &allow-other-keys)
  (declare (ignore width height))
  (apply #'expected-named-size pane name args))


#||
define method expected-width 
    (pane, #rest args, #key width, #all-keys)
 => (width :: <integer>)
  ignore(width);
  apply(expected-named-width, pane, #"width", args)
end method expected-width;
||#

(defmethod expected-width (pane &rest args &key width &allow-other-keys)
  (declare (ignore width))
  (apply #'expected-named-width pane :width args))


#||
define method expected-height 
    (pane, #rest args, #key height, #all-keys)
 => (height :: <integer>)
  ignore(height);
  apply(expected-named-height, pane, #"height", args)
end method expected-height;
||#

(defmethod expected-height (pane &rest args &key height &allow-other-keys)
  (declare (ignore height))
  (apply #'expected-named-height pane :height args))


#||
define method expected-min-width 
    (pane, #rest args, #key, #all-keys)
 => (min-width :: <integer>)
  apply(expected-named-width, pane, #"min-width", args)
end method expected-min-width;
||#

(defmethod expected-min-width (pane &rest args &key &allow-other-keys)
  (apply #'expected-named-width pane :min-width args))


#||
define method expected-min-height 
    (pane, #rest args, #key, #all-keys)
 => (min-height :: <integer>)
  apply(expected-named-height, pane, #"min-height", args)
end method expected-min-height;
||#

(defmethod expected-min-height (pane &rest args &key &allow-other-keys)
  (apply #'expected-named-height pane :min-height args))


#||
define method expected-max-width 
    (pane, #rest args, #key, #all-keys)
 => (max-width :: <integer>)
  apply(expected-named-width, pane, #"max-width", args)
end method expected-max-width;
||#

(defmethod expected-max-width (pane &rest args &key &allow-other-keys)
  (apply #'expected-named-width pane :max-width args))


#||
define method expected-max-height 
    (pane, #rest args, #key, #all-keys)
 => (max-height :: <integer>)
  apply(expected-named-height, pane, #"max-height", args)
end method expected-max-height;
||#

(defmethod expected-max-height (pane &rest args &key &allow-other-keys)
  (apply #'expected-named-height pane :max-height args))


#||
define method expected-fixed-width? 
    (pane, #rest args, #key, #all-keys)
 => (fixed? :: <boolean>)
  apply(expected-width, pane, args) = apply(expected-max-width, pane, args)
end method expected-fixed-width?;
||#

(defmethod expected-fixed-width? (pane &rest args &key &allow-other-keys)
  (equal? (apply #'expected-width pane args)
	  (apply #'expected-max-width pane args)))


#||
define method expected-fixed-height? 
    (pane, #rest args, #key, #all-keys)
 => (fixed? :: <boolean>)
  apply(expected-height, pane, args) = apply(expected-max-height, pane, args)
end method expected-fixed-height?;
||#

(defmethod expected-fixed-height? (pane &rest args &key &allow-other-keys)
  (equal? (apply #'expected-height pane args)
	  (apply #'expected-max-height pane args)))


#||
define method expected-constrained-width
    (pane, width :: <integer>, #rest args, #key, #all-keys)
 => (width :: <integer>)
  min(max(width, apply(expected-min-width, pane, args)),
      apply(expected-max-width, pane, args))
end method expected-constrained-width;
||#

(defmethod expected-constrained-width (pane (width integer) &rest args &key &allow-other-keys)
  (min (max width (apply #'expected-min-width pane args))
       (apply #'expected-max-width pane args)))

#||
define method expected-constrained-height
    (pane, height :: <integer>, #rest args, #key, #all-keys)
 => (height :: <integer>)
  min(max(height, apply(expected-min-height, pane, args)),
      apply(expected-max-height, pane, args))
end method expected-constrained-height;
||#

(defmethod expected-constrained-height (pane (height integer) &rest args &key &allow-other-keys)
  (min (max height (apply #'expected-min-height pane args))
       (apply #'expected-max-height pane args)))


#||
define method normalize-max-size
    (size :: <integer>) => (size :: <integer>)
  min(size, $fill)
end method normalize-max-size;
||#

(defmethod normalize-max-size ((size integer))
  (min size +fill+))


#||
/// check-layout-pane-layout

define method check-default-pane-size
    (pane, name, #rest args, #key, #all-keys) => ()
  invalidate-space-requirements(pane);
  let space = compose-space(pane);
  let (width, min-width, max-width, height, min-height, max-height)
    = space-requirement-components(pane, space);
  check(concatenate(name, " min-width <= default width"), \<=,
        min-width, width);
  check(concatenate(name, " default width <= max-width"), \<=,
        width, max-width);
  check(concatenate(name, " min-height <= default height"), \<=,
        min-height, height);
  check(concatenate(name, " default height <= max-height"), \<=,
        height, max-height);
  check-equal(concatenate(name, " default width"),
              width,
              apply(expected-width, pane, args));
  check-equal(concatenate(name, " default height"),
              height,
              apply(expected-height, pane, args));
  check-equal(concatenate(name, " default min-width"),
              min-width,
              apply(expected-min-width, pane, args));
  check-equal(concatenate(name, " default min-height"),
              min-height,
              apply(expected-min-height, pane, args));
  check-equal(concatenate(name, " default max-width"),
              normalize-max-size(max-width),
              normalize-max-size(apply(expected-max-width, pane, args)));
  check-equal(concatenate(name, " default max-height"),
              normalize-max-size(max-height),
              normalize-max-size(apply(expected-max-height, pane, args)));
end method check-default-pane-size;
||#

(defmethod check-default-pane-size (pane name &rest args &key &allow-other-keys)
  (invalidate-space-requirements pane)
  (let ((space (compose-space pane)))
    (multiple-value-bind (width min-width max-width height min-height max-height)
	(space-requirement-components pane space)
      (is (<= min-width width)
	  "check-default-pane-size|~a min-width [~a] <= default width [~a]"
	  name min-width width)
      (is (<= width max-width)
	  "check-default-pane-size|~a default width [~a] <= max-width [~a]"
	  name width max-width)
      (is (<= min-height height)
	  "check-default-pane-size|~a min-height [~a] <= default height [~a]"
	  name min-height height)
      (is (<= height max-height)
	  "check-default-pane-size|~a default height [~a] <= max-height [~a]"
	  name height max-height)
      (is (= width (apply #'expected-width pane args))
	  "check-default-pane-size|~a default width was ~a expected ~a"
	  name width (apply #'expected-width pane args))
      (is (= height (apply #'expected-height pane args))
	  "check-default-pane-size|~a default height was ~a expected ~a"
	  name height (apply #'expected-height pane args))
      (is (= min-width (apply #'expected-min-width pane args))
	  "check-default-pane-size|~a default min-width was ~a expected ~a"
	  name min-width (apply #'expected-min-width pane args))
      (is (= min-height (apply #'expected-min-height pane args))
	  "check-default-pane-size|~a default min-height was ~a expected ~a"
	  name min-height (apply #'expected-min-height pane args))
      (is (= (normalize-max-size max-width)
	     (normalize-max-size (apply #'expected-max-width pane args)))
	  "check-default-pane-size|~a default max-width got ~a expected ~a"
	  name
	  (normalize-max-size max-width)
	  (normalize-max-size (apply #'expected-max-width pane args)))
      (is (= (normalize-max-size max-height)
	     (normalize-max-size (apply #'expected-max-height pane args)))
	  "check-default-pane-size|~a default max-height got ~a expected ~a"
	  name
	  (normalize-max-size max-height)
	  (normalize-max-size (apply #'expected-max-height pane args))))))


#||
define method check-pane-size 
    (pane, name,
     #rest args, 
     #key width: user-width, height: user-height, #all-keys)
 => ()
  invalidate-space-requirements(pane);
  let space = compose-space(pane,
                            width: user-width,
                            height: user-height);
  let (width, min-width, max-width, height, min-height, max-height)
    = space-requirement-components(pane, space);
  check(concatenate(name, " min-width <= width"), \<=,
        min-width, width);
  check(concatenate(name, " width <= max-width"), \<=,
        width, max-width);
  check-equal(concatenate(name, " accepts explicit width"),
              width,
              max(min-width, min(user-width, max-width)));
  check(concatenate(name, " min-height <= height"), \<=,
        min-height, height);
  check(concatenate(name, " height <= max-height"), \<=,
        height, max-height);
  check-equal(concatenate(name, " accepts explicit height"),
              height,
              max(min-height, min(user-height, max-height)));
  check-equal(concatenate(name, " requested width"),  
              width,
              apply(expected-width, pane, args));
  check-equal(concatenate(name, " requested height"),
              height,
              apply(expected-height, pane, args));
  check-equal(concatenate(name, " requested min-width"),
              min-width,
              apply(expected-min-width, pane, args));
  check-equal(concatenate(name, " requested min-height"),
              min-height,
              apply(expected-min-height, pane, args));
  check-equal(concatenate(name, " requested max-width"),
              normalize-max-size(max-width),
              normalize-max-size(apply(expected-max-width, pane, args)));
  check-equal(concatenate(name, " requested max-height"),
              normalize-max-size(max-height),
              normalize-max-size(apply(expected-max-height, pane, args)));
end method check-pane-size;
||#

(defmethod check-pane-size (pane name
			    &rest args
			    &key ((:width user-width)) ((:height user-height)) &allow-other-keys)
  (invalidate-space-requirements pane)
  (let ((space (compose-space pane :width user-width :height user-height)))
    (multiple-value-bind (width min-width max-width height min-height max-height)
	(space-requirement-components pane space)
      (is (<= min-width width)
	  "check-pane-size|~a min-width [~a] <= width [~a]"
	  name min-width width)
      (is (<= width max-width)
	  "check-pane-size|~a width [~a] <= max-width [~a]"
	  name width max-width)
      (is (= width (max min-width (min user-width max-width)))
	  "check-pane-size|~a accepts explicit width" name)
      (is (<= min-height height)
	  "check-pane-size|~a min-height [~a] <= height [~a]"
	  name min-height height)
      (is (<= height max-height)
	  "check-pane-size|~a height [~a] <= max-height [~a]"
	  name height max-height)
      (is (= height (max min-height (min user-height max-height)))
	  "check-pane-size|~a accepts explicit height" name)
      (is (= width (apply #'expected-width pane args))
	  "check-pane-size|~a requested width got ~a expected ~a (pane=~a, args=~a)"
	  name width (apply #'expected-width pane args) pane args)
      (is (= height (apply #'expected-height pane args))
	  "check-pane-size|~a requested height got ~a expected ~a"
	  name height (apply #'expected-height pane args))
      (is (= min-width (apply #'expected-min-width pane args))
	  "check-pane-size|~a requested min-width got ~a expected ~a"
	  name min-width (apply #'expected-min-width pane args))
      (is (= min-height (apply #'expected-min-height pane args))
	  "check-pane-size|~a requested min-height got ~a expected ~a"
	  name min-height (apply #'expected-min-height pane args))
      (is (= (normalize-max-size max-width)
	     (normalize-max-size (apply #'expected-max-width pane args)))
	  "check-pane-size|~a requested max-width got ~a expected ~a"
	  name max-width (apply #'expected-max-width pane args))
      (is (= (normalize-max-size max-height)
	     (normalize-max-size (apply #'expected-max-height pane args)))
	  "check-pane-size|~a requested max-height got ~a expected ~a"
	  name max-height (apply #'expected-max-height pane args)))))


#||
define generic expected-space-allocation
    (pane, #key, #all-keys)
 => (space-allocation :: false-or(<sequence>));
||#

(defgeneric expected-space-allocation (pane &key &allow-other-keys))


#||
define method sheet-position-in-parent 
    (sheet :: <sheet>) => (x :: <integer>, y :: <integer>)
  let point = transform-region(sheet-transform(sheet),
                               make(<point>, x: 0, y: 0));
  values(floor(point-x(point)), floor(point-y(point)))
end method sheet-position-in-parent;
||#

(defmethod sheet-position-in-parent ((sheet <sheet>))
  (let ((point (transform-region (sheet-transform sheet)
				 (make-point 0 0))))
    (values (floor (point-x point)) (floor (point-y point)))))


#||
define method enforce-fully-within-parent? 
    (sheet :: <sheet>) => (enforce? :: <boolean>)
  #t
end method enforce-fully-within-parent?;
||#

(defmethod enforce-fully-within-parent? ((sheet <sheet>))
  t)


#||
define method check-child-allocations (layout, name, #rest args) => ()
  let child-allocations = apply(expected-space-allocation, layout, args);
  let (layout-width, layout-height) = box-size(sheet-region(layout));
  if (child-allocations)
    for (child in sheet-children(layout),
         allocation in child-allocations,
         count from 1)
      let (x, y) = sheet-position-in-parent(child);
      let (width, height) = box-size(sheet-region(child));
      let child-name = format-to-string("%s child %d", name, count);
      if (enforce-fully-within-parent?(layout))
        check-true(concatenate(child-name, " fully within parent"),
                   x >= 0 & y >= 0
                   & (x + width)  <= layout-width
                   & (y + height) <= layout-height)
      end;
      check-equal(concatenate(child-name, " x"),
                  floor(x), allocation[0]);
      check-equal(concatenate(child-name, " y"),
                  floor(y), allocation[1]);
      check-equal(concatenate(child-name, " width"),
                  width, allocation[2]);
      check-equal(concatenate(child-name, " height"),
                  height, allocation[3]);
      check(concatenate(child-name, " width >= min-width"), \>=,
            width, expected-min-width(child));
      check(concatenate(child-name, " width <= max-width"), \<=,
            width, expected-max-width(child));
      check(concatenate(child-name, " height >= min-height"), \>=,
            height, expected-min-height(child));
      check(concatenate(child-name, " height <= max-height"), \<=,
            height, expected-max-height(child));
    end
  end
end method check-child-allocations;
||#
(defmethod check-child-allocations (layout name &rest args)
  (let ((child-allocations (apply #'expected-space-allocation layout args)))
    (multiple-value-bind (layout-width layout-height)
	(box-size (sheet-region layout))
      (when child-allocations
	(let ((count 0))
	  (map nil #'(lambda (child allocation)
		       (incf count)
		       (multiple-value-bind (x y)
			   (sheet-position-in-parent child)
			 (multiple-value-bind (width height)
			     (box-size (sheet-region child))
			   (let ((child-name (format nil "~a child ~d" name count)))
			     (declare (ignore child-name)) ;SN: it appears this isn't needed in CL-DUIM versions of the test.
			     (when (enforce-fully-within-parent? layout)
			       (is (and (>= x 0) (>= y 0)
					(<= (+ x width) layout-width)
					(<= (+ y height) layout-height))
				   "check-child-allocations|~a fully within parent"
				   name))
			     (is (= (floor x) (aref allocation 0))
				 "check-child-allocations|~a x allocation got ~a, expected ~a"
				 name (floor x) (aref allocation 0))
			     (is (= (floor y) (aref allocation 1))
				 "check-child-allocations|~a y allocation got ~a, expected ~a"
				 name (floor y) (aref allocation 1))
			     (is (= width (aref allocation 2))
				 "check-child-allocations|~a width allocation got ~a, expected ~a"
				 name width (aref allocation 2))
			     (is (= height (aref allocation 3))
				 "check-child-allocations|~a height allocation got ~a, expected ~a"
				 name height (aref allocation 3))
			     (is (>= width (expected-min-width child))
				 "check-child-allocations|~a width [~a] >= min-width [~a]"
				 name width (expected-min-width child))
			     (is (<= width (expected-max-width child))
				 "check-child-allocations|~a width [~a] <= max-width [~a]"
				 name width (expected-max-width child))
			     (is (>= height (expected-min-height child))
				 "check-child-allocations|~a height [~a] >= min-height [~a]"
				 name height (expected-min-height child))
			     (LET ((EMH (EXPECTED-MAX-HEIGHT CHILD)))
			       (is (<= height EMH)
				   "check-child-allocations|~a height [~a] <= max-height [~a]"
				   name height EMH))))))
	       (sheet-children layout) child-allocations))))))


#||
define method check-space-allocation 
    (pane, name, #rest args, 
     #key width = *layout-default-width*, height = *layout-default-height*,
     #all-keys)
 => ()
  let children = sheet-children(pane);
  check-true(concatenate(name, " children's positions = (0,0)"),
             every?(method (child)
                      let (x, y) = box-position(sheet-region(child));
                      x = 0 & y = 0
                    end,
                    children));
  apply(check-child-allocations, pane, name, 
        width: width, height: height,
        args);
end method check-space-allocation;
||#

(defmethod check-space-allocation (pane name &rest args
				   &key (width *layout-default-width*) (height *layout-default-height*)
				     &allow-other-keys)
  (let ((children (sheet-children pane)))
    (is (every #'(lambda (child)
		   (multiple-value-bind (x y)
		       (box-position (sheet-region child))
		     (and (= x 0) (= y 0))))
	       children)
	(concatenate 'string name " children's positions = (0,0)"))
    (apply #'check-child-allocations pane name
	   :width width :height height
	   args)))


#||
define method expected-constrained-size
    (sheet, width :: <integer>, height :: <integer>, 
     #rest args,
     #key, #all-keys)
 => (constrained-width :: <integer>, constrained-height :: <integer>)
  let w = min(max(width, apply(expected-min-width, sheet, args)),
              apply(expected-max-width, sheet, args));
  let h = min(max(height, apply(expected-min-height, sheet, args)),
              apply(expected-max-height, sheet, args));
  values(w, h)
end method expected-constrained-size;
||#

(defmethod expected-constrained-size (sheet (width integer) (height integer)
				      &rest args
				      &key &allow-other-keys)
    (let ((w (min (max width (apply #'expected-min-width sheet args))
		  (apply #'expected-max-width sheet args)))
	  (h (min (max height (apply #'expected-min-height sheet args))
		  (apply #'expected-max-height sheet args))))
      (values w h)))

#||
define method check-layout-pane-layout 
    (pane, name,
     #rest args,
     #key x = 0, y = 0, width, height, allocate-space? = #t,
     #all-keys) => ()
  let (w, h) = apply(expected-constrained-size, pane,
                     width  | *layout-default-width*,
                     height | *layout-default-height*,
                     args);
  apply(check-default-pane-size, pane, name, width: #f, height: #f, args);
  apply(check-pane-size, pane, name, width: w, height: h, args);
  if (allocate-space?)
    if (w = 0 | h = 0)
      allocate-space(pane, w, h)
    else
      set-sheet-edges(pane, x, y, w, h);
      let (new-width, new-height) = box-size(pane);
      check-equal(concatenate(name, " width"), new-width, w);
      check-equal(concatenate(name, " height"), new-height, h);
    end
  end;
  apply(check-space-allocation, pane, name, width: w, height: h, args)
end method check-layout-pane-layout;
||#

(defmethod check-layout-pane-layout (pane name
				     &rest args
				     &key (x 0) (y 0) width height (allocate-space? t)
				       &allow-other-keys)
  (multiple-value-bind (w h)
	(apply #'expected-constrained-size pane
	       (or width *layout-default-width*)
	       (or height *layout-default-height*)
	       args)
    (apply #'check-default-pane-size pane name :width nil :height nil args)
    (apply #'check-pane-size pane name :width w :height h args)
    (when allocate-space?
      (if (or (= w 0) (= h 0))
	  (allocate-space pane w h)
	  ;; else
	  (progn
	    (set-sheet-edges pane x y w h)
	    (multiple-value-bind (new-width new-height)
		(box-size pane)
	      (is (= new-width w) (concatenate 'string name " width"))
	      (is (= new-height h) (concatenate 'string name " height"))))))
    (apply #'check-space-allocation pane name :width w :height h args)))


#||
/// wrapping-layout-pane tests

define method expected-named-size
    (pane :: <wrapping-layout-mixin>, name, #rest args, #key width, height)
 => (size :: <integer>)
  ignore(width, height);
  let child = sheet-child(pane);
  case
    child     => apply(expected-size-function(name), child, args);
    otherwise => apply(expected-default-size, name, args);
  end
end method expected-named-size;
||#

(defmethod expected-named-size ((pane <wrapping-layout-mixin>) name &rest args &key width height)
  (declare (ignore width height))
  (let ((child (sheet-child pane)))
    (cond
      (child (apply (expected-size-function name) child args))
      (t     (apply #'expected-default-size name args)))))

#||
define method expected-space-allocation
    (pane :: <wrapping-layout-mixin>, #key width, height, #all-keys)
 => (space-allocation :: false-or(<vector>))
  let child = sheet-child(pane);
  if (child)
    let space = compose-space(child, width: width, height: height);
    vector(vector(0, 0, 
                  width  | space-requirement-width(child, space),
                  height | space-requirement-height(child, space)))
  end
end method expected-space-allocation;
||#

(defmethod expected-space-allocation ((pane <wrapping-layout-mixin>) &key width height &allow-other-keys)
  (let ((child (sheet-child pane)))
    (when child
      (let* ((space (compose-space child :width width :height height)))
	(vector (vector 0 0
			(or width (space-requirement-width child space))
			(or height (space-requirement-height child space))))))))

#||

/// top-level-sheet tests
define method test-top-level-sheet-layout
    (name, child, #rest args, #key) => ()
  let t-l-s = make-test-pane(<top-level-sheet>, child: child);
  apply(check-layout-pane-layout, t-l-s, 
        concatenate(name, " ", gadget-class-name(<top-level-sheet>)),
        args)
end method test-top-level-sheet-layout;
||#

(defmethod test-top-level-sheet-layout (name child &rest args &key)
  (let ((t-l-s (make-test-pane (find-class '<top-level-sheet>) :child child)))
    (apply #'check-layout-pane-layout t-l-s
	   (concatenate 'string name " " (gadget-class-name (find-class '<top-level-sheet>)))
	   args)))


#||
define test top-level-sheet-layouts-test ()
  test-top-level-sheet-layout("empty", #f);
  test-top-level-sheet-layout("fixed", make-test-pane(<test-push-button-pane>));
  test-top-level-sheet-layout("non-fixed", make-test-pane(<test-list-box>));
end test top-level-sheet-layouts-test;
||#

(test top-level-sheet-layouts-test
      :description "top level sheet layouts test"
      (test-top-level-sheet-layout "empty" nil)
      (test-top-level-sheet-layout "fixed" (make-test-pane (find-class '<test-push-button-pane>)))
      (test-top-level-sheet-layout "non-fixed" (make-test-pane (find-class '<test-list-box>))))


#||

/// Fixed layout tests

define method test-fixed-layout
    (name, children :: <sequence>, #rest args, #key) => ()
  let fixed-layout = make-test-pane(<fixed-layout>, children: children);
  let name = concatenate(name, " ", gadget-class-name(<fixed-layout>));
  apply(check-layout-pane-layout,
        fixed-layout, name,
        args)
end method test-fixed-layout;
||#

(defmethod test-fixed-layout (name (children sequence) &rest args &key)
  (let ((fixed-layout (make-test-pane (find-class '<fixed-layout>)
				      :children children))
	(name (concatenate 'string name " "
			   (gadget-class-name (find-class '<fixed-layout>)))))
    (apply #'check-layout-pane-layout fixed-layout name args)))


#||
// A fixed's children don't have to be within the parent
define method enforce-fully-within-parent?
    (layout :: <fixed-layout>) => (enforce? :: <boolean>)
  #f
end method enforce-fully-within-parent?;
||#

(defmethod enforce-fully-within-parent? ((layout <fixed-layout>))
    nil)


#||
define method expected-space-allocation
    (pane :: <fixed-layout>, #key)
 => (space-allocation :: false-or(<sequence>))
  let children = sheet-children(pane);
  let space-allocations = make(<vector>, size: size(children));
  for (child in children,
       count from 0)
    let (x, y) = sheet-position-in-parent(child);
    let width = expected-width(child);
    let height = expected-height(child);
    space-allocations[count] := vector(x, y, width, height);
  end;
  space-allocations
end method expected-space-allocation;
||#

(defmethod expected-space-allocation ((pane <fixed-layout>) &key)
  (let* ((children (sheet-children pane))
	 (space-allocations (MAKE-SIMPLE-VECTOR :size (length children))))
    (loop for child across children
       for count from 0
       do (multiple-value-bind (x y)
	      (sheet-position-in-parent child)
	    (let ((width (expected-width child))
		  (height (expected-height child)))
	      (setf (aref space-allocations count) (vector x y width height)))))
    space-allocations))


#||
define duim-layouts class-test <fixed-layout> ()
  test-fixed-layout("empty", #());
  test-fixed-layout("one child",
		    vector(make-test-pane(<test-list-box>, 
					  x: 100, y: 200,
					  width: 200, height: 300)));
  test-fixed-layout("two children",
		    vector(make-test-pane(<test-list-box>, 
					  x: 100, y: 200,
					  width: 200, height: 300),
			   make-test-pane(<test-list-box>, 
					  x: 200, y: 300,
					  width: 400, height: 500)));
  test-layout-child-resizing(<fixed-layout>);
  test-multiple-child-layout-manipulation(<fixed-layout>);
end class-test <fixed-layout>;
||#

(test <fixed-layout>-class-test
      :description "<fixed-layout> class test"
      (test-fixed-layout "empty" #())
      (test-fixed-layout "one child"
			 (vector (make-test-pane (find-class '<test-list-box>)
						 :x 100 :y 200
						 :width 200 :height 300)))
      (test-fixed-layout "two children"
			 (vector (make-test-pane (find-class '<test-list-box>)
						 :x 100 :y 200
						 :width 200 :height 300)
				 (make-test-pane (find-class '<test-list-box>)
						 :x 200 :y 300
						 :width 400 :height 500)))
      (test-layout-child-resizing (find-class '<fixed-layout>))
      (test-multiple-child-layout-manipulation (find-class '<fixed-layout>)))


#||

/// Pinboard layout tests

define method test-pinboard-layout
    (name, children :: <sequence>, #rest args, #key) => ()
  let pinboard-layout = make-test-pane(<pinboard-layout>, children: children);
  let name = concatenate(name, " ", gadget-class-name(<pinboard-layout>));
  apply(check-layout-pane-layout,
        pinboard-layout, name,
        args)
end method test-pinboard-layout;
||#

(defmethod test-pinboard-layout (name (children sequence) &rest args &key)
  (let ((pinboard-layout (make-test-pane (find-class '<pinboard-layout>) :children children))
	(name (concatenate 'string name " " (gadget-class-name (find-class '<pinboard-layout>)))))
    (apply #'check-layout-pane-layout
	   pinboard-layout name args)))


#||
// A pinboard's children don't have to be within the parent
define method enforce-fully-within-parent?
    (layout :: <pinboard-layout>) => (enforce? :: <boolean>)
  #f
end method enforce-fully-within-parent?;
||#

(defmethod enforce-fully-within-parent? ((layout <pinboard-layout>))
  nil)


#||
define method expected-space-allocation
    (pane :: <pinboard-layout>, #key)
 => (space-allocation :: false-or(<sequence>))
  let children = sheet-children(pane);
  let space-allocations = make(<vector>, size: size(children));
  for (child in children,
       count from 0)
    let (x, y) = sheet-position-in-parent(child);
    let width = expected-width(child);
    let height = expected-height(child);
    space-allocations[count] := vector(x, y, width, height);
  end;
  space-allocations
end method expected-space-allocation;
||#

(defmethod expected-space-allocation ((pane <pinboard-layout>) &key)
  (let* ((children (sheet-children pane))
	 (space-allocations (MAKE-SIMPLE-VECTOR :size (length children))))
    (loop for child across children
       for count from 0
       do (multiple-value-bind (x y)
	      (sheet-position-in-parent child)
	    (let ((width (expected-width child))
		  (height (expected-height child)))
	      (setf (aref space-allocations count) (vector x y width height)))))
    space-allocations))


#||
define duim-layouts class-test <pinboard-layout> ()
  test-pinboard-layout("empty", #());
  test-pinboard-layout("one child",
                       vector(make-test-pane(<test-list-box>, 
					     x: 100, y: 200,
					     width: 200, height: 300)));
  test-pinboard-layout("two children",
                       vector(make-test-pane(<test-list-box>, 
					     x: 100, y: 200,
					     width: 200, height: 300),
                              make-test-pane(<test-list-box>, 
					     x: 200, y: 300,
					     width: 400, height: 500)));
  test-layout-child-resizing(<pinboard-layout>);
  test-multiple-child-layout-manipulation(<pinboard-layout>);
end class-test <pinboard-layout>;
||#

(test <pinboard-layout>-class-test
      :description "<pinboard-layout> class test"
      (test-pinboard-layout "empty" #())
      (test-pinboard-layout "one child"
			    (vector (make-test-pane (find-class '<test-list-box>)
						    :x 100 :y 200
						    :width 200 :height 300)))
      (test-pinboard-layout "two children"
			    (vector (make-test-pane (find-class '<test-list-box>)
						    :x 100 :y 200
						    :width 200 :height 300)
				    (make-test-pane (find-class '<test-list-box>)
						    :x 200 :y 300
						    :width 400 :height 500)))
      (test-layout-child-resizing (find-class '<pinboard-layout>))
      (test-multiple-child-layout-manipulation (find-class '<pinboard-layout>)))


#||

/// Stack layout tests

define method test-stack-layout
    (name, children :: <sequence>, #rest args, #key) => ()
  let stack-layout = make-test-pane(<stack-layout>, children: children);
  let name = concatenate(name, " ", gadget-class-name(<stack-layout>));
  apply(check-layout-pane-layout,
        stack-layout, name,
        args)
end method test-stack-layout;
||#

(defmethod test-stack-layout (name (children sequence) &rest args &key)
  (let ((stack-layout (make-test-pane (find-class '<stack-layout>) :children children))
	(name (concatenate 'string name " " (gadget-class-name (find-class '<stack-layout>)))))
    (apply #'check-layout-pane-layout stack-layout name args)))


#||
define method expected-named-size
    (pane :: <stack-layout>, name, #key width, height)
 => (size :: <integer>)
  let size-function = expected-size-function(name);
  let size = 0;
  for (child in sheet-children(pane))
    size := max(size, size-function(child))
  end;
  size + (layout-border(pane) * 2)
end method expected-named-size;
||#

(defmethod expected-named-size ((pane <stack-layout>) name &rest args &key width height)
  (cond ((and (eql name :width) width) width)
	((and (eql name :height) height) height)
	(t
	 (let ((size-function (expected-size-function name))
	       (size 0))
	   (if (not (empty? (sheet-children pane)))
	       (loop for child across (sheet-children pane)
		  do (setf size (max size (funcall size-function child))))
	       (setf size (apply #'expected-default-size name args)))
	   (+ size (* (layout-border pane) 2))))))


#||
define method expected-space-allocation
    (pane :: <stack-layout>, #key width, height, #all-keys)
 => (space-allocation :: false-or(<sequence>))
  let children = sheet-children(pane);
  let border = layout-border(pane);
  let border*2 = border * 2;
  let space-allocations = make(<vector>, size: size(children));
  let available-width  = width  & (width  - border*2);
  let available-height = height & (height - border*2);
  for (child in children,
       count from 0)
    let width  = expected-width (child, width:  available-width);
    let height = expected-height(child, height: available-height);
    space-allocations[count] := vector(border, border, width, height);
  end;
  space-allocations
end method expected-space-allocation;
||#

(defmethod expected-space-allocation ((pane <stack-layout>) &key width height &allow-other-keys)
  (let* ((children (sheet-children pane))
	 (border (layout-border pane))
	 (border*2 (* border 2))
	 (space-allocations (make-simple-vector :size (length children)))
	 (available-width (and width (- width border*2)))
	 (available-height (and height (- height border*2))))
    ;; #'expected-width does not know its pane is contained inside a stack
    ;; layout which constrains its bounds; make sure the bounds don't overflow
    ;; the available space if the child's space composition method doesn't
    ;; respect the provided width / height already.
    (loop for child across children
       for count from 0
       do (let ((width (min available-width (expected-width child :width available-width)))
		(height (min available-height (expected-height child :height available-height))))
	    (setf (aref space-allocations count) (vector border border width height))))
    space-allocations))


#||
define duim-layouts class-test <stack-layout> ()
  test-stack-layout("empty", #());
  test-stack-layout("one child",
		    vector(make-test-pane(<test-list-box>, 
					  x: 100, y: 200,
					  width: 200, height: 300)));
  test-stack-layout("two children",
		    vector(make-test-pane(<test-list-box>, 
					  x: 100, y: 200,
					  width: 200, height: 300),
			   make-test-pane(<test-list-box>, 
					  x: 200, y: 300,
					  width: 400, height: 500)));
  test-layout-child-resizing(<stack-layout>);
  test-multiple-child-layout-manipulation(<stack-layout>);
end class-test <stack-layout>;
||#

(test <stack-layout>-class-test
  :description "<stack-layout> class test"
  (test-stack-layout "empty" #())
  (test-stack-layout "one child"
		     (vector (make-test-pane (find-class '<test-list-box>)
					     :x 100 :y 200
					     :width 200 :height 300)))
  (test-stack-layout "two children"
		     (vector (make-test-pane (find-class '<test-list-box>)
					     :x 100 :y 200
					     :width 200 :height 300)
			     (make-test-pane (find-class '<test-list-box>)
					     :x 200 :y 300
					     :width 400 :height 500)))
  (test-layout-child-resizing (find-class '<stack-layout>))
  (test-multiple-child-layout-manipulation (find-class '<stack-layout>)))


#||

/// Table layout tests

// we make this subclass so that we don't lose the contents field
define class <test-table-layout-pane> (<table-layout>)
  constant slot contents :: <sequence> = #(), 
    init-keyword: contents:;
end class <test-table-layout-pane>;
||#

(defclass <test-table-layout-pane> (<table-layout>)
  ((contents :type sequence
	     :initarg :contents
	     :initform '()
	     :reader contents)))


#||
define method class-for-make-pane 
    (framem :: <test-frame-manager>, class == <table-layout>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<test-table-layout-pane>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <test-frame-manager>) (class (eql (find-class '<table-layout>))) &key)
  (values (find-class '<test-table-layout-pane>) nil))


#||
define method table-number-of-rows 
    (table :: <test-table-layout-pane>) => (rows :: <integer>)
  size(contents(table))
end method table-number-of-rows;
||#

(defmethod table-number-of-rows ((table <test-table-layout-pane>))
  (length (contents table)))


#||
define method table-number-of-columns 
    (table :: <test-table-layout-pane>) => (columns :: <integer>)
  let contents = contents(table);
  case
    empty?(contents) => 0;
    otherwise        => size(contents[0]);
  end
end method table-number-of-columns;
||#

(defmethod table-number-of-columns ((table <test-table-layout-pane>))
  (let ((contents (contents table)))
    (cond
      ((empty? contents) 0)
      (t (length (sequence-elt contents 0))))))


#||
define method expected-width
    (table :: <test-table-layout-pane>, #rest args, #key width)
 => (width :: <integer>)
  case
    width     => apply(expected-width-given-default, table, args);
    otherwise => next-method();
  end
end method expected-width;
||#

(defmethod expected-width ((table <test-table-layout-pane>) &rest args &key width)
  (if width
      (apply #'expected-width-given-default table args)
      (call-next-method)))


#||
define method expected-height
    (table :: <test-table-layout-pane>, #rest args, #key height)
 => (height :: <integer>)
  case
    height    => apply(expected-height-given-default, table, args);
    otherwise => next-method();
  end
end method expected-height;
||#

(defmethod expected-height ((table <test-table-layout-pane>) &rest args &key height)
  (if height
      (apply #'expected-height-given-default table args)
      (call-next-method)))


#||
define method expected-named-width 
    (table :: <test-table-layout-pane>, name,
     #rest args, #key width, height, x-spacing = 0)
 => (width :: <integer>)
  let size-function = expected-size-function(name);
  let no-of-columns = table-number-of-columns(table);
  let width = 0;
  if (no-of-columns > 0)
    for (column-index from 0 below no-of-columns)
      let subwidth = 0;
      for (column in contents(table))
	if (column-index < size(column))
	  let child = column[column-index];
	  if (child)
	    subwidth := max(subwidth, size-function(child))
	  end
	end
      end;
      width := width + subwidth
    end;
    width + x-spacing * (no-of-columns - 1)
  else
    apply(expected-default-size, name, args)
  end
end method expected-named-width;
||#

(defmethod expected-named-width ((table <test-table-layout-pane>) name
				 &rest args &key width height (x-spacing 0))
  (declare (ignore width height))
  (let ((size-function (expected-size-function name))
	(no-of-columns (table-number-of-columns table))
	(width 0))
    (if (> no-of-columns 0)
	(progn
	  (loop for column-index from 0 below no-of-columns
	     do (let ((subwidth 0))
		  (loop for column across (contents table)
		     do (when (< column-index (size column))
			  (let ((child (sequence-elt column column-index)))
			    (when child
			      (setf subwidth (max subwidth (funcall size-function child)))))))
		  (setf width (+ width subwidth))))
	  (+ width (* x-spacing (- no-of-columns 1))))
	;; else
	(apply #'expected-default-size name args))))


#||
define method expected-named-height 
    (table :: <test-table-layout-pane>, name, 
     #key y-spacing = 0, width, height)
 => (height :: <integer>)
  let size-function = expected-size-function(name);
  let no-of-rows = table-number-of-rows(table);
  let height = 0;
  let rows = contents(table);
  if (~empty?(rows))
    for (row in rows)
      let subheight = 0;
      for (child in row)
	if (child)
	  subheight := max(subheight, size-function(child))
	end
      end;
      height := height + subheight
    end;
    height + y-spacing * (no-of-rows - 1)
  else
    expected-default-size(name)
  end
end method expected-named-height;
||#

(defmethod expected-named-height ((table <test-table-layout-pane>) name
				  &key (y-spacing 0) width height)
  (declare (ignore width height))
  (let ((size-function (expected-size-function name))
	(no-of-rows (table-number-of-rows table))
	(height 0)
	(rows (contents table)))
    (if (not (empty? rows))
	(progn
	  (loop for row across rows
	     do (let ((subheight 0))
		  (loop for child across row
		     do (when child
			  (setf subheight (max subheight (funcall size-function child)))))
		  (setf height (+ height subheight))))
	  (+ height (* y-spacing (- no-of-rows 1))))
	;; else
	(expected-default-size name))))


#||
define method expected-fixed-column? 
    (table :: <test-table-layout-pane>, column-index)
 => (fixed-column? :: <boolean>)
  every?(method (row)
           let child = column-index >= size(row) & row[column-index];
           if (child)
             expected-fixed-width?(child)
           end
         end,
         contents(table))
end method expected-fixed-column?;
||#

(defmethod expected-fixed-column? ((table <test-table-layout-pane>) column-index)
  (every? #'(lambda (row)
	      (let ((child (and (>= column-index (length row))
				(sequence-elt row column-index))))
		(when child
		  (expected-fixed-width? child))))
	  (contents table)))


#||
define method expected-fixed-row? 
    (table :: <test-table-layout-pane>, row-index)
 => (fixed-row? :: <boolean>)
  every?(method (child)
           if (child)
             expected-fixed-width?(child)
           end
         end,
         contents(table)[row-index])
end method expected-fixed-row?;
||#

(defmethod expected-fixed-row? ((table <test-table-layout-pane>) row-index)
  (every? #'(lambda (child)
	      (when child
		(expected-fixed-width? child)))
	  (sequence-elt (contents table) row-index)))


#||
define method expected-column-named-width 
    (table :: <test-table-layout-pane>, name, column-index)
 => (width :: <integer>)
  let size-function = expected-size-function(name);
  let width = 0;
  do(method (row)
       if (column-index < size(row))
         let child = row[column-index];
         if (child)
           max!(width, size-function(child))
         end
       end
     end,
     contents(table));
  width
end method expected-column-named-width;
||#

(defmethod expected-column-named-width ((table <test-table-layout-pane>) name column-index)
  (let ((size-function (expected-size-function name))
	(width 0))
    (map nil #'(lambda (row)
		 (when (< column-index (length row))
		   (let ((child (aref row column-index)))
		     (when child
		       (setf width
			     (max width (funcall size-function child)))))))
	 (contents table))
    width))


#||
define method expected-row-named-height
    (table :: <test-table-layout-pane>, name, row-index)
 => (height :: <integer>)
  let size-function = expected-size-function(name);
  let height = 0;
  do(method (child)
       if (child)
         max!(height, size-function(child))
       end
     end,
     contents(table)[row-index]);
  height
end method expected-row-named-height;
||#

(defmethod expected-row-named-height ((table <test-table-layout-pane>) name row-index)
  (let ((size-function (expected-size-function name))
	(height 0))
    (map nil #'(lambda (child)
		 (when child
		   (setf height
			 (max height (funcall size-function child)))))
	 (aref (contents table) row-index))
    height))


#||
define method layout-nth-ratio
    (n :: <integer>, ratios :: false-or(<sequence>)) => (ratio :: <integer>)
  (ratios & n < size(ratios) & ratios[n])
    | 1
end method layout-nth-ratio;
||#

(defmethod layout-nth-ratio ((n integer) ratios)
  (or (and ratios (< n (length ratios)) (elt ratios n)) 1))


#||
define method layout-fixed-size
    (box :: <layout-pane>, no-of-items :: <integer>,
     size-function :: <function>, fixed-size?-function :: <function>)
 => (fixed-size :: <integer>)
  let size = 0;
  for (count from 0 below no-of-items)
    if (fixed-size?-function(count))
      inc!(size, size-function(count))
    end
  end;
  size
end method layout-fixed-size;
||#

(defmethod layout-fixed-size ((box <layout-pane>) (no-of-items integer)
			      (size-function function) (fixed-size?-function function))
  (let ((size 0))
    (loop for count from 0 below no-of-items
       do (when (funcall fixed-size?-function count)
	    (incf size (funcall size-function count))))
    size))


#||
define method layout-ratio-unit 
    (box :: <layout-pane>, no-of-items :: <integer>, total-size :: <integer>,
     size-function :: <function>, fixed-size?-function :: <function>,
     ratios :: false-or(<sequence>))
 => (unit :: <integer>)
  let fixed-size
    = layout-fixed-size(box, no-of-items, size-function, fixed-size?-function);
  let denominator = 0;
  for (count from 0 below no-of-items)
    unless (fixed-size?-function(count))
      inc!(denominator, layout-nth-ratio(count, ratios))
    end
  end;
  if (denominator > 0)
    floor/(total-size - fixed-size, denominator)
  else
    0
  end
end method layout-ratio-unit;
||#

(defmethod layout-ratio-unit ((box <layout-pane>) (no-of-items integer) (total-size integer)
			      (size-function function) (fixed-size?-function function)
			      ratios)
  ;; Calculates the proportion of available space taken up by each 'unit'
  ;; of a proportial ratio. Basically, calculates the amount of space needed
  ;; by the fixed size components, and divides the remaining space by the
  ;; number of 'units' in the ratio required.

  ;; example: total size = 100, size for fixed elements = 40, leaving 60
  ;; for proportial items. Assuming 2 proportial items, one of which being
  ;; 2/3 and the other being 1/3, each 'unit' would be 20.
  (let ((fixed-size (layout-fixed-size box no-of-items size-function fixed-size?-function))
	(denominator 0))
    (loop for count from 0 below no-of-items
       do (unless (funcall fixed-size?-function count)
	    (incf denominator (layout-nth-ratio count ratios))))
    (if (> denominator 0)
	(floor (- total-size fixed-size) denominator)
	0)))


#||
define method expected-space-allocation
    (table :: <test-table-layout-pane>, 
     #key width, height,
          x-spacing = 0, y-spacing = 0,
          x-ratios, y-ratios,
          x-alignment = #"left", y-alignment = #"top")
 => (space-allocation :: false-or(<sequence>))
  let allocation = make(<stretchy-vector>);
  let contents = contents(table);
  let total-x-spacing = x-spacing * (table-number-of-columns(table) - 1);
  let total-y-spacing = y-spacing * (table-number-of-rows(table)    - 1);
  let x-ratio-unit
    = layout-ratio-unit(table, table-number-of-columns(table),
                        width - total-x-spacing,
                        curry(expected-column-named-width, table, #"width"),
                        curry(expected-fixed-column?, table),
                        x-ratios);
  let y-ratio-unit
    = layout-ratio-unit(table, table-number-of-rows(table),
                        height - total-y-spacing,
                        curry(expected-row-named-height, table, #"height"),
                        curry(expected-fixed-row?, table),
                        y-ratios);
  let y = 0;
  for (row in contents,
       row-index from 0)
    let x = 0;
    let y-ratio = layout-nth-ratio(row-index, y-ratios);
    let row-height = if (expected-fixed-row?(table, row-index))
                       expected-row-named-height(table, #"height", row-index)
                     else
                       fix-coordinate(y-ratio-unit * y-ratio)
                     end;
    for (child in row,
         column-index from 0)
      let x-ratio = layout-nth-ratio(column-index, x-ratios);
      let column-width = if (expected-fixed-column?(table, column-index))
			   expected-column-named-width(table, #"width",
						       column-index)
			 else
			   fix-coordinate(x-ratio-unit * x-ratio)
			 end;
      when (child)
	let width = case
		      expected-fixed-width?(child) => expected-width(child);
		      otherwise => column-width;
		    end;
	let height = case
		       expected-fixed-height?(child) => expected-height(child);
		       otherwise => row-height;
		     end;
        let this-x-alignment
          = if (instance?(x-alignment, <sequence>))
              x-alignment[column-index]
            else
              x-alignment
            end;
        let this-y-alignment
          = if (instance?(y-alignment, <sequence>))
              y-alignment[row-index]
            else
              y-alignment
            end;
	let x-adjust = select (this-x-alignment)
			 #"left"  => 0;
			 #"right" => column-width - width;
			 #"centre", #"center" => floor/(column-width - width, 2);
		       end;
	let y-adjust = select (this-y-alignment)
			 #"top" => 0;
			 #"bottom" => row-height - height;
			 #"centre", #"center" => floor/(row-height - height, 2);
		       end;
	add!(allocation, vector(x + x-adjust, y + y-adjust, width, height));
      end;
      inc!(x, column-width + x-spacing)
    end;
    inc!(y, row-height + y-spacing)
  end;
  allocation
end method expected-space-allocation;
||#

(defmethod expected-space-allocation ((table <test-table-layout-pane>)
				      &key width height
				      (x-spacing 0) (y-spacing 0)
				      x-ratios y-ratios
				      (x-alignment :left) (y-alignment :top))
  (let* ((allocation (MAKE-STRETCHY-VECTOR))
	 (contents (contents table))
	 (total-x-spacing (* x-spacing (- (table-number-of-columns table) 1)))
	 (total-y-spacing (* y-spacing (- (table-number-of-rows table) 1)))
	 (x-ratio-unit (layout-ratio-unit table (table-number-of-columns table)
					  (- width total-x-spacing)
					  (alexandria:curry #'expected-column-named-width table :width)
					  (alexandria:curry #'expected-fixed-column? table)
					  x-ratios))
	 (y-ratio-unit (layout-ratio-unit table (table-number-of-rows table)
					  (- height total-y-spacing)
					  (alexandria:curry #'expected-row-named-height table :height)
					  (alexandria:curry #'expected-fixed-row? table)
					  y-ratios))
	 (y 0))
    (loop for row across contents
       for row-index from 0
       do (let* ((x 0)
		 (y-ratio (layout-nth-ratio row-index y-ratios))
		 (row-height (if (expected-fixed-row? table row-index)
				 (expected-row-named-height table :height row-index)
				 ;; else
				 (fix-coordinate (* y-ratio-unit y-ratio)))))
	    (loop for child across row
	       for column-index from 0
	       do (let* ((x-ratio (layout-nth-ratio column-index x-ratios))
			 (column-width (if (expected-fixed-column? table column-index)
					   (expected-column-named-width table :width column-index)
					   ;; else
					   (fix-coordinate (* x-ratio-unit x-ratio)))))
		    (when child
		      (let* ((width (if (expected-fixed-width? child)
					(expected-width child)
					column-width))
			     (height (if (expected-fixed-height? child)
					 (expected-height child)
					 row-height))
			     (this-x-alignment (if (INSTANCE? x-alignment 'sequence)
						   (SEQUENCE-ELT x-alignment column-index)
						   ;; else
						   x-alignment))
			     (this-y-alignment (if (INSTANCE? y-alignment 'sequence)
						   (SEQUENCE-ELT y-alignment row-index)
						   ;; else
						   y-alignment))
			     (x-adjust (ecase this-x-alignment
					 (:left 0)
					 (:right (- column-width width))
					 ((:centre :center) (floor (- column-width width) 2))))
			     (y-adjust (ecase this-y-alignment
					 (:top 0)
					 (:bottom (- row-height height))
					 ((:center :centre) (floor (- row-height height) 2)))))
			(add! allocation (vector (+ x x-adjust) (+ y y-adjust) width height))))
		    (incf x (+ column-width x-spacing))))
	    (incf y (+ row-height y-spacing))))
    allocation))


#||
define method make-table-layout-pane 
    (children :: <sequence>, no-of-columns :: <integer>, #rest args, #key, #all-keys)
 => (sheet :: <table-layout>)
  let no-of-children = size(children);
  let no-of-rows = ceiling/(no-of-children, no-of-columns);
  let contents = make(<vector>, size: no-of-rows);
  for (i from 0 below no-of-rows)
    let start = i * no-of-columns;
    let end-index = min(start + no-of-columns, no-of-children);
    contents[i] := copy-sequence(children, start: start, end: end-index)
  end;
  apply(make-test-pane, <test-table-layout-pane>, contents: contents, args)
end method make-table-layout-pane;
||#

(defmethod make-table-layout-pane ((children sequence) (no-of-columns integer) &rest args &key &allow-other-keys)
  (let* ((no-of-children (length children))
	 (no-of-rows (ceiling no-of-children no-of-columns))
	 (contents (MAKE-SIMPLE-VECTOR :size no-of-rows)))
    (loop for i from 0 below no-of-rows
       do (let* ((start (* i no-of-columns))
		 (end-index (min (+ start no-of-columns) no-of-children)))
	    (setf (SEQUENCE-ELT contents i) (SEQUENCE-COPY children :start start :end end-index))))
    (apply #'make-test-pane (find-class '<test-table-layout-pane>) :contents contents args)))


#||
define method make-default-table-layout-pane 
    (#rest args,
     #key gadget-class = <test-push-button-pane>,
          x-spacing = 0,
          y-spacing = 0)
 => (sheet :: <table-layout>)
  apply(make-table-layout-pane,
	vector(make-test-pane(gadget-class),
	       make-test-pane(gadget-class),
	       make-test-pane(gadget-class),
	       make-test-pane(gadget-class),
	       make-test-pane(gadget-class)),
        2,
	x-spacing: x-spacing, y-spacing: y-spacing,
        args)
end method make-default-table-layout-pane;
||#

(defmethod make-default-table-layout-pane (&rest args
					   &key (gadget-class (find-class '<test-push-button-pane>))
					   (x-spacing 0) (y-spacing 0)
			                   &allow-other-keys)
  (apply #'make-table-layout-pane
	 (vector (make-test-pane gadget-class)
		 (make-test-pane gadget-class)
		 (make-test-pane gadget-class)
		 (make-test-pane gadget-class)
		 (make-test-pane gadget-class))
	 2
	 :x-spacing x-spacing :y-spacing y-spacing
	 args))


#||
define method test-default-table-layout-pane-layout
    (gadget-class :: <class>, name :: <string>, #rest args, #key, #all-keys) => ()
  let gadget-name = gadget-class-name(<table-layout>);
  let table
    = apply(make-default-table-layout-pane,
	    gadget-class: gadget-class,
	    args);
  apply(check-layout-pane-layout, table,
        concatenate(name, " ", gadget-name),
        args)
end method test-default-table-layout-pane-layout;
||#

(defmethod test-default-table-layout-pane-layout (gadget-class (name string) &rest args &key &allow-other-keys)
  (let ((gadget-name (gadget-class-name (find-class '<table-layout>)))
	(table (apply #'make-default-table-layout-pane
		      :gadget-class gadget-class
		      args)))
    (apply #'check-layout-pane-layout table
	   (concatenate 'string name " " gadget-name)
	   args)))


#||
define method test-table-layout-pane-layout 
    (children :: <sequence>, name, #rest args, #key, #all-keys) => ()
  let gadget-name = gadget-class-name(<table-layout>);
  let table = apply(make-table-layout-pane, children, 2, args);
  apply(check-layout-pane-layout, table, 
        concatenate(name, " ", gadget-name),
        args)
end method test-table-layout-pane-layout;
||#

(defmethod test-table-layout-pane-layout ((children sequence) name &rest args &key &allow-other-keys)
  (let ((gadget-name (gadget-class-name (find-class '<table-layout>)))
	(table (apply #'make-table-layout-pane children 2 args)))
    (apply #'check-layout-pane-layout table
	   (concatenate 'string name " " gadget-name)
	   args)))


#||
define method test-mixed-table-layout-pane-layout 
    (name, #rest args, #key, #all-keys) => ()
  let children = vector(make-test-pane(<test-push-button-pane>),
                        make-test-pane(<test-list-box>),
                        make-test-pane(<test-list-box>),
                        make-test-pane(<test-push-button-pane>),
                        make-test-pane(<test-list-box>),
                        make-test-pane(<test-push-button-pane>));
  apply(test-table-layout-pane-layout, children, name, args);
end method test-mixed-table-layout-pane-layout;
||#

(defmethod test-mixed-table-layout-pane-layout (name &rest args &key &allow-other-keys)
  (let ((children (vector (make-test-pane (find-class '<test-push-button-pane>))
			  (make-test-pane (find-class '<test-list-box>))
			  (make-test-pane (find-class '<test-list-box>))
			  (make-test-pane (find-class '<test-push-button-pane>))
			  (make-test-pane (find-class '<test-list-box>))
			  (make-test-pane (find-class '<test-push-button-pane>)))))
    (apply #'test-table-layout-pane-layout children name args)))


#||
define duim-layouts class-test <table-layout> ()
  let x-spacing = 10;
  let y-spacing = 20;
  test-table-layout-pane-layout(#(), "empty");
  test-default-table-layout-pane-layout
    (<test-push-button-pane>, "fixed",
     x-spacing: x-spacing, y-spacing: y-spacing);

  test-default-table-layout-pane-layout
    (<test-list-box>, "non-fixed",
     x-spacing: x-spacing, y-spacing: y-spacing);

  test-default-table-layout-pane-layout
    (<test-list-box>, "specified ratios",
     x-spacing: x-spacing, y-spacing: y-spacing,
     x-ratios: #(1, 2), y-ratios: #(1, 3));

  test-mixed-table-layout-pane-layout
    ("fixed and non-fixed",
     x-spacing: x-spacing, y-spacing: y-spacing);

  for (alignment in #(#"left", #"center", #"right", #(#"right", #"left")))
    test-mixed-table-layout-pane-layout
      (format-to-string("x %s aligned", alignment),
       x-alignment: alignment);
  end;

  for (alignment in #(#"top", #"center", #"bottom", #(#"bottom", #"top", #"bottom")))
    test-mixed-table-layout-pane-layout
      (format-to-string("y %s aligned", alignment),
       y-alignment: alignment);
  end;

  let children
    = vector(make-default-table-layout-pane(),
	     make-test-pane(<test-list-box>),
	     make-test-pane(<test-list-box>),
	     make-default-table-layout-pane(),
	     make-test-pane(<test-list-box>),
	     make-default-table-layout-pane());
  test-table-layout-pane-layout
    (children, "non-fixed recursive",
     x-spacing: x-spacing, y-spacing: y-spacing);

  test-table-layout-pane-layout
    (vector(#f, #f, #f), "only gaps",
     x-spacing: x-spacing, y-spacing: y-spacing);

  let children
    = vector(#f,
	     make-test-pane(<test-push-button-pane>),
	     make-test-pane(<test-push-button-pane>),
	     #f);
  test-table-layout-pane-layout
    (children, "fixed with gaps",
     x-spacing: x-spacing, y-spacing: y-spacing);

  let children
    = vector(#f,
	     make-test-pane(<test-list-box>),
	     make-test-pane(<test-list-box>),
	     #f);
  test-table-layout-pane-layout
    (children, "non-fixed with gaps",
     x-spacing: x-spacing, y-spacing: y-spacing);
  test-layout-child-resizing(<table-layout>, rows: 1);
  test-multiple-child-layout-manipulation(<table-layout>, columns: 2);
end class-test <table-layout>;
||#

(test <table-layout>-class-test
      :description "<table-layout> class test"
      (let ((x-spacing 10)
	    (y-spacing 20))
	(test-table-layout-pane-layout #() "empty")
	(test-default-table-layout-pane-layout (find-class '<test-push-button-pane>) "fixed"
					       :x-spacing x-spacing :y-spacing y-spacing)

	(test-default-table-layout-pane-layout (find-class '<test-list-box>) "non-fixed"
					       :x-spacing x-spacing :y-spacing y-spacing)

	(test-default-table-layout-pane-layout (find-class '<test-list-box>) "specified ratios"
					       :x-spacing x-spacing :y-spacing y-spacing
					       :x-ratios #(1 2) :y-ratios #(1 3))

	(test-mixed-table-layout-pane-layout "fixed and non-fixed" :x-spacing x-spacing :y-spacing y-spacing)

	(loop for alignment across #(:left :center :right #(:right :left))
	   do (test-mixed-table-layout-pane-layout (format nil "x ~a aligned" alignment)
						   :x-alignment alignment))

	(loop for alignment across #(:top :center :bottom #(:bottom :top :bottom))
	   do (test-mixed-table-layout-pane-layout (format nil "y ~a aligned" alignment)
						   :y-alignment alignment))

	(let ((children (vector (make-default-table-layout-pane)
				(make-test-pane (find-class '<test-list-box>))
				(make-test-pane (find-class '<test-list-box>))
				(make-default-table-layout-pane)
				(make-test-pane (find-class '<test-list-box>))
				(make-default-table-layout-pane))))
	  (test-table-layout-pane-layout children "non-fixed recursive"
					 :x-spacing x-spacing :y-spacing y-spacing))

	(test-table-layout-pane-layout (vector nil nil nil) "only gaps"
				       :x-spacing x-spacing :y-spacing y-spacing)

	(let ((children (vector nil
				(make-test-pane (find-class '<test-push-button-pane>))
				(make-test-pane (find-class '<test-push-button-pane>))
				nil)))
	  (test-table-layout-pane-layout children "fixed with gaps"
					 :x-spacing x-spacing :y-spacing y-spacing))

	(let ((children (vector nil
				(make-test-pane (find-class '<test-list-box>))
				(make-test-pane (find-class '<test-list-box>))
				nil)))
	  (test-table-layout-pane-layout children "non-fixed with gaps"
					 :x-spacing x-spacing :y-spacing y-spacing))

	(test-layout-child-resizing (find-class '<table-layout>) :rows 1)
	(test-multiple-child-layout-manipulation (find-class '<table-layout>) :columns 2)))


#||

/// Grid layout tests

// we make this subclass so that we don't lose the contents field
define class <test-grid-layout-pane> (<grid-layout>, <test-table-layout-pane>)
end class <test-grid-layout-pane>;
||#

(defclass <test-grid-layout-pane>
    (<grid-layout>
     <test-table-layout-pane>)
  ())


#||
define method class-for-make-pane
    (framem :: <test-frame-manager>, class == <grid-layout>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<test-grid-layout-pane>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <test-frame-manager>) (class (eql (find-class '<grid-layout>))) &key)
  (values (find-class '<test-grid-layout-pane>) nil))


#||
define duim-layouts class-test <grid-layout> ()
  let x-spacing = 10;
  let y-spacing = 20;
  let space-req = make(<space-requirement>, width: 100, height: 100);

/*---*** This needs to make a <grid-layout> somehow!
  test-table-layout-pane-layout(#(), "empty");
  test-default-table-layout-pane-layout
    (<test-push-button-pane>, "fixed",
     cell-space-requirement: space-req,
     x-spacing: x-spacing, y-spacing: y-spacing);

  test-default-table-layout-pane-layout
    (<test-list-box>, "non-fixed",
     cell-space-requirement: space-req,
     x-spacing: x-spacing, y-spacing: y-spacing);

  test-default-table-layout-pane-layout
    (<test-list-box>, "specified ratios",
     cell-space-requirement: space-req,
     x-spacing: x-spacing, y-spacing: y-spacing,
     x-ratios: #(1, 2), y-ratios: #(1, 3));

  test-mixed-table-layout-pane-layout
    ("fixed and non-fixed",
     cell-space-requirement: space-req,
     x-spacing: x-spacing, y-spacing: y-spacing);

  for (alignment in #(#"left", #"center", #"right", #(#"right", #"left")))
    test-mixed-table-layout-pane-layout
      (format-to-string("x %s aligned", alignment),
       cell-space-requirement: space-req,
       x-alignment: alignment);
  end;

  for (alignment in #(#"top", #"center", #"bottom", #(#"bottom", #"top", #"bottom")))
    test-mixed-table-layout-pane-layout
      (format-to-string("y %s aligned", alignment),
       cell-space-requirement: space-req,
       y-alignment: alignment);
  end;

  let children
    = vector(make-default-table-layout-pane(),
	     make-test-pane(<test-list-box>),
	     make-test-pane(<test-list-box>),
	     make-default-table-layout-pane(),
	     make-test-pane(<test-list-box>),
	     make-default-table-layout-pane());
  test-table-layout-pane-layout
    (children, "non-fixed recursive",
     cell-space-requirement: space-req,
     x-spacing: x-spacing, y-spacing: y-spacing);

  test-table-layout-pane-layout
    (vector(#f, #f, #f), "only gaps",
     cell-space-requirement: space-req,
     x-spacing: x-spacing, y-spacing: y-spacing);

  let children
    = vector(#f,
	     make-test-pane(<test-push-button-pane>),
	     make-test-pane(<test-push-button-pane>),
	     #f);
  test-table-layout-pane-layout
    (children, "fixed with gaps",
     cell-space-requirement: space-req,
     x-spacing: x-spacing, y-spacing: y-spacing);

  let children
    = vector(#f,
	     make-test-pane(<test-list-box>),
	     make-test-pane(<test-list-box>),
	     #f);
  test-table-layout-pane-layout
    (children, "non-fixed with gaps",
     cell-space-requirement: space-req,
     x-spacing: x-spacing, y-spacing: y-spacing);
  test-layout-child-resizing
     (<grid-layout>, 
      cell-space-requirement: space-req,
      rows: 1);
  test-multiple-child-layout-manipulation
    (<grid-layout>,
      cell-space-requirement: space-req,
     columns: 2);
*/
end class-test <grid-layout>;
||#

(test <grid-layout>-class-test
  :description "<grid-layout> class test"
  (let ((x-spacing 10)
	(y-spacing 20)
	(space-req (make-space-requirement :width 100 :height 100)))
    (declare (ignore x-spacing y-spacing space-req))

    ;;---*** This needs to make a <grid-layout> somehow!

    ))


#||

/// Box pane tests

define method box-compute-major-size 
    (box :: <box-layout-pane>, space-function :: <function>, 
     #key spacing = 0, #all-keys)
 => (major-size :: <integer>)
  let major-size = 0;
  let children = sheet-children(box);
  for (child in children)
    major-size := major-size + space-function(child)
  end;
  major-size + spacing * (size(children) - 1);
end method box-compute-major-size;
||#

(defmethod box-compute-major-size ((box <box-layout-pane>) (space-function function)
				   &key (spacing 0) &allow-other-keys)
  (let ((major-size 0)
	(children (sheet-children box)))
    (loop for child across children
       do (setf major-size (+ major-size (funcall space-function child))))
    (+ major-size (* spacing (- (length children) 1)))))


#||
define method box-compute-minor-size
    (box :: <box-layout-pane>, space-function :: <function>)
 => (minor-size :: <integer>)
  let size = 0;
  for (child in sheet-children(box))
    size := max(size, space-function(child))
  end;
  size
end method box-compute-minor-size;
||#

(defmethod box-compute-minor-size ((box <box-layout-pane>) (space-function function))
  (let ((size 0))
    (loop for child across (sheet-children box)
       do (setf size (max size (funcall space-function child))))
    size))


#||
define generic box-compute-width 
    (box :: <layout>, space-function :: <function>, #key, #all-keys)
 => (width :: <integer>);
||#

(defgeneric box-compute-width (box space-function &key &allow-other-keys))


#||
define generic box-compute-height
    (box :: <layout>, space-function :: <function>, #key, #all-keys)
 => (height :: <integer>);
||#

(defgeneric box-compute-height (box space-function &key &allow-other-keys))


#||
define method box-compute-width 
    (box :: <row-layout>, space-function :: <function>, #rest args, #key)
 => (width :: <integer>)
  apply(box-compute-major-size, box, space-function, args)
end method box-compute-width;
||#

(defmethod box-compute-width ((box <row-layout>) (space-function function) &rest args &key)
  (apply #'box-compute-major-size box space-function args))


#||
define method box-compute-height
    (box :: <row-layout>, space-function :: <function>, #rest args, #key)
 => (height :: <integer>)
  box-compute-minor-size(box, space-function)
end method box-compute-height;
||#

(defmethod box-compute-height ((box <row-layout>) (space-function function) &rest args &key)
  (declare (ignore args))
  (box-compute-minor-size box space-function))


#||
define method box-compute-width 
    (box :: <column-layout>, space-function :: <function>, #rest args, #key)
 => (width :: <integer>)
  box-compute-minor-size(box, space-function)
end method box-compute-width;
||#

(defmethod box-compute-width ((box <column-layout>) (space-function function) &rest args &key)
  (declare (ignore args))
  (box-compute-minor-size box space-function))


#||
define method box-compute-height
    (box :: <column-layout>, space-function :: <function>, #rest args, #key)
 => (height :: <integer>)
  apply(box-compute-major-size, box, space-function, args)
end method box-compute-height;
||#

(defmethod box-compute-height ((box <column-layout>) (space-function function) &rest args &key)
  (apply #'box-compute-major-size box space-function args))


#||
define method expected-width
    (table :: <box-layout-pane>, #rest args, #key width)
 => (width :: <integer>)
  case
    width     => apply(expected-width-given-default, table, args);
    otherwise => next-method();
  end
end method expected-width;
||#

(defmethod expected-width ((table <box-layout-pane>) &rest args &key width)
  (cond
    (width (apply #'expected-width-given-default table args))
    (t     (call-next-method))))


#||
define method expected-height
    (table :: <box-layout-pane>, #rest args, #key height)
 => (height :: <integer>)
  case
    height    => apply(expected-height-given-default, table, args);
    otherwise => next-method();
  end
end method expected-height;
||#

(defmethod expected-height ((table <box-layout-pane>) &rest args &key height)
  (cond
    (height (apply #'expected-height-given-default table args))
    (t      (call-next-method))))


#||
define method expected-named-width
    (box :: <box-layout-pane>, name, #rest args, #key width, height)
 => (width :: <integer>)
  if (~empty?(sheet-children(box)))
    apply(box-compute-width, box, expected-size-function(name), args)
  else
    apply(expected-default-size, name, args)
  end
end method expected-named-width;
||#

(defmethod expected-named-width ((box <box-layout-pane>) name &rest args &key width height)
  (declare (ignore width height))
  (if (not (empty? (sheet-children box)))
      (apply #'box-compute-width box (expected-size-function name) args)
      (apply #'expected-default-size name args)))


#||
define method expected-named-height 
    (box :: <box-layout-pane>, name, #rest args, #key width, height)
 => (height :: <integer>)
  if (~empty?(sheet-children(box)))
    apply(box-compute-height, box, expected-size-function(name), args)
  else
    apply(expected-default-size, name, args)
  end
end method expected-named-height;
||#

(defmethod expected-named-height ((box <box-layout-pane>) name &rest args &key width height)
  (declare (ignore width height))
  (if (not (empty? (sheet-children box)))
      (apply #'box-compute-height box (expected-size-function name) args)
      (apply #'expected-default-size name args)))


#||
define method expected-space-allocation
    (box :: <row-layout>,
     #key spacing = 0,
          y-alignment = #"top",
          x-ratios,
          ratios = x-ratios,
          width, height)
 => (space-allocation :: false-or(<sequence>))
  let children = sheet-children(box);
  let number-of-children = size(children);
  let total-spacing = spacing * (number-of-children - 1);
  let non-fixed-width-unit
    = layout-ratio-unit(box, number-of-children, width - total-spacing,
                        method (count)
                          expected-width(children[count])
                        end,
                        method (count)
                          expected-fixed-width?(children[count])
                        end,
                        ratios);
  let x = 0;
  let child-allocations = make(<stretchy-vector>);
  for (child in children,
       count from 0)
    let ratio = layout-nth-ratio(count, ratios);
    let child-width 
      = if (expected-fixed-width?(child))
          expected-width(child)
        else
          expected-constrained-width
            (child, fix-coordinate(non-fixed-width-unit * ratio))
        end;
    let child-height = expected-constrained-height(child, height);
    let y = select (y-alignment)
              #"top" => 0;
              #"bottom" => height - child-height;
              #"centre", #"center" => floor/(height - child-height, 2);
            end;
    add!(child-allocations, vector(x, y, child-width, child-height));
    inc!(x, child-width + spacing)
  end;
  child-allocations
end method expected-space-allocation;
||#

(defmethod expected-space-allocation ((box <row-layout>)
				      &key (spacing 0)
				      (y-alignment :top)
				      x-ratios
				      (ratios x-ratios)
				      width height)
  (let* ((children (sheet-children box))
	 (number-of-children (length children))
	 (total-spacing (* spacing (- number-of-children 1)))
	 (non-fixed-width-unit (layout-ratio-unit box number-of-children (- width total-spacing)
						  #'(lambda (count)
						      (expected-width (SEQUENCE-ELT children count)))
						  #'(lambda (count)
						      (expected-fixed-width? (SEQUENCE-ELT children count)))
						  ratios))
	 (x 0)
	 (child-allocations (MAKE-STRETCHY-VECTOR)))
    (loop for child across children
       for count from 0
       do (let* ((ratio (layout-nth-ratio count ratios))
		 (child-width (if (expected-fixed-width? child)
				  (expected-width child)
				  (expected-constrained-width child
							      (fix-coordinate (* non-fixed-width-unit ratio)))))
		 (child-height (expected-constrained-height child height))
		 (y (ecase y-alignment
		      (:top 0)
		      (:bottom (- height child-height))
		      ((:centre :center) (floor (- height child-height) 2)))))
	    (add! child-allocations (vector x y child-width child-height))
	    (incf x (+ child-width spacing))))
    child-allocations))


#||
define method expected-space-allocation
    (box :: <column-layout>, 
     #key spacing = 0,
          x-alignment = #"left",
          y-ratios,
          ratios = y-ratios,
          width, height)
 => (space-allocation :: false-or(<sequence>))
  let children = sheet-children(box);
  let number-of-children = size(children);
  let total-spacing = spacing * (number-of-children - 1);
  let non-fixed-height-unit
    = layout-ratio-unit(box, number-of-children, height - total-spacing,
                        method (count)
                          expected-height(children[count])
                        end,
                        method (count)
                          expected-fixed-height?(children[count])
                        end,
                        ratios);
  let y = 0;
  let child-allocations = make(<stretchy-vector>);
  for (child in children,
       count from 0)
    let ratio = layout-nth-ratio(count, ratios);
    let child-height
      = if (expected-fixed-height?(child))
          expected-height(child)
        else
          expected-constrained-height
            (child, fix-coordinate(non-fixed-height-unit * ratio))
        end;
    let child-width = expected-constrained-width(child, width);
    let x = select (x-alignment)
              #"left"              => 0;
              #"right"             => width - child-width;
              #"center", #"centre" => floor/(width - child-width, 2);
            end;
    add!(child-allocations, vector(x, y, child-width, child-height));
    inc!(y, child-height + spacing)
  end;
  child-allocations
end method expected-space-allocation;
||#

(defmethod expected-space-allocation ((box <column-layout>)
				      &key (spacing 0)
				      (x-alignment :left)
				      y-ratios
				      (ratios y-ratios)
					width height)
  (let* ((children (sheet-children box))
	 (number-of-children (length children))
	 (total-spacing (* spacing (- number-of-children 1)))
	 (non-fixed-height-unit (layout-ratio-unit box number-of-children (- height total-spacing)
						   #'(lambda (count)
						       (expected-height (SEQUENCE-ELT children count)))
						   #'(lambda (count)
						       (expected-fixed-height? (SEQUENCE-ELT children count)))
						   ratios))
	 (y 0)
	 (child-allocations (MAKE-STRETCHY-VECTOR)))
    (loop for child across children
       for count from 0
       do (let* ((ratio (layout-nth-ratio count ratios))
		 (child-height (if (expected-fixed-height? child)
				   (expected-height child)
				   (expected-constrained-height child
								(fix-coordinate (* non-fixed-height-unit ratio)))))
		 (child-width (expected-constrained-width child width))
		 (x (ecase x-alignment
		      (:left 0)
		      (:right (- width child-width))
		      ((:center :centre) (floor (- width child-width) 2)))))
	    (add! child-allocations (vector x y child-width child-height))
	    (incf y (+ child-height spacing))))
    child-allocations))


#||
define method test-three-child-layout 
    (class :: <class>, child-class :: <class>,
     #rest args,
     #key name,
          spacing = 10,
          first-child-class = child-class,
     #all-keys) => ()
  let gadget-name = gadget-class-name(class);
  let test-name = if (name)
                    concatenate(name, " ", gadget-name)
                  else
                    gadget-name
                  end;

  let children = vector(make-test-pane(first-child-class),
                        make-test-pane(child-class),
                        make-test-pane(child-class));
  let layout = apply(make-test-pane, class, children: children, spacing: spacing, args);
  apply(check-layout-pane-layout, layout, test-name, spacing: spacing, args);

  let children = vector(make-test-pane(first-child-class, 
                             width:  *child-explicit-width*,
                             height: *child-explicit-height*),
                        make-test-pane(child-class),
                        make-test-pane(child-class));
  let layout = apply(make-test-pane, class, children: children, spacing: spacing, args);
  apply(check-layout-pane-layout, layout, 
        concatenate("explicit child size ", test-name),
        spacing: spacing,
        args);
end method test-three-child-layout;
||#

(defmethod test-three-child-layout ((class class) (child-class class)
				    &rest args
				    &key name
				      (spacing 10)
				      (first-child-class child-class)
				      &allow-other-keys)
  (let* ((gadget-name (gadget-class-name class))
	 (test-name (if name
			(concatenate 'string name " " gadget-name)
			gadget-name))

	 (children (vector (make-test-pane first-child-class)
			   (make-test-pane child-class)
			   (make-test-pane child-class)))
	 (layout (apply #'make-test-pane class :children children :spacing spacing args)))
    (apply #'check-layout-pane-layout layout test-name :spacing spacing args)

    (let* ((children (vector (make-test-pane first-child-class
					     :width *child-explicit-width*
					     :height *child-explicit-height*)
			     (make-test-pane child-class)
			     (make-test-pane child-class)))
	   (layout (apply #'make-test-pane class :children children :spacing spacing args)))
      (apply #'check-layout-pane-layout layout
	     (concatenate 'string "explicit child size " test-name)
	     :spacing spacing
	     args))))


#||
define method test-box-pane-layout 
    (class :: <class>, #key spacing = 10) => ()
  check-layout-pane-layout(make-test-pane(class),
                           concatenate("empty ", gadget-class-name(class)));
  test-three-child-layout(class, <test-push-button-pane>,
                          spacing: spacing);
  test-three-child-layout(class, <test-list-box>,
                          name: "non-fixed",
                          spacing: spacing);
  test-three-child-layout(class, <test-list-box>,
                          name: "specified ratios",
                          spacing: spacing,
                          ratios: #(1, 2, 3));
  test-three-child-layout(class, <test-list-box>,
                          first-child-class: <test-push-button-pane>,
                          name: "fixed and non-fixed",
                          spacing: spacing);
  test-three-child-layout(class, <test-list-box>,
                          first-child-class: <test-push-button-pane>,
                          name: "centered",
                          x-alignment: #"center",
                          y-alignment: #"center");
  test-three-child-layout(class, <test-list-box>,
                          first-child-class: <test-push-button-pane>,
                          name: "right-aligned",
                          x-alignment: #"right",
                          y-alignment: #"bottom");
end method test-box-pane-layout;
||#

(defmethod test-box-pane-layout ((class class) &key (spacing 10))
  (check-layout-pane-layout (make-test-pane class)
			    (concatenate 'string "empty " (gadget-class-name class)))
  (test-three-child-layout class (find-class '<test-push-button-pane>)
			   :spacing spacing)
  (test-three-child-layout class (find-class '<test-list-box>)
			   :name "non-fixed"
			   :spacing spacing)
  (test-three-child-layout class (find-class '<test-list-box>)
			   :name "specified ratios"
			   :spacing spacing
			   :ratios #(1 2 3))
  (test-three-child-layout class (find-class '<test-list-box>)
			   :first-child-class (find-class '<test-push-button-pane>)
			   :name "fixed and non-fixed"
			   :spacing spacing)
  (test-three-child-layout class (find-class '<test-list-box>)
			   :first-child-class (find-class '<test-push-button-pane>)
			   :name "centered"
			   :x-alignment :center
			   :y-alignment :center)
  (test-three-child-layout class (find-class '<test-list-box>)
			   :first-child-class (find-class '<test-push-button-pane>)
			   :name "right-aligned"
			   :x-alignment :right
			   :y-alignment :bottom))


#||
define method test-nested-layouts
    (outer-class :: <class>, inner-class :: <class>, #key spacing = 10) => ()
  let buttons1 = vector(make-test-pane(<test-push-button-pane>),
                        make-test-pane(<test-push-button-pane>),
                        make-test-pane(<test-push-button-pane>));
  let buttons2 = vector(make-test-pane(<test-push-button-pane>),
                        make-test-pane(<test-push-button-pane>),
                        make-test-pane(<test-push-button-pane>));
  let column1 = make-test-pane(inner-class, children: buttons1);
  let column2 = make-test-pane(inner-class, children: buttons2);
  let pane = make-test-pane(outer-class, children: vector(column1, column2),
                  spacing: spacing);
  let name = gadget-class-name(outer-class);
  check-layout-pane-layout(pane, concatenate("nested ", name),
                           spacing: spacing);
end method test-nested-layouts;
||#

(defmethod test-nested-layouts ((outer-class class) (inner-class class) &key (spacing 10))
  (let* ((buttons1 (vector (make-test-pane (find-class '<test-push-button-pane>))
			   (make-test-pane (find-class '<test-push-button-pane>))
			   (make-test-pane (find-class '<test-push-button-pane>))))
	 (buttons2 (vector (make-test-pane (find-class '<test-push-button-pane>))
			   (make-test-pane (find-class '<test-push-button-pane>))
			   (make-test-pane (find-class '<test-push-button-pane>))))
	 (column1  (make-test-pane inner-class :children buttons1))
	 (column2  (make-test-pane inner-class :children buttons2))
	 (pane     (make-test-pane outer-class :children (vector column1 column2)
				   :spacing spacing))
	 (name     (gadget-class-name outer-class)))
    (check-layout-pane-layout pane (concatenate 'string "nested " name)
			      :spacing spacing)))


#||
define duim-layouts class-test <column-layout> ()
  test-box-pane-layout(<column-layout>);
  test-nested-layouts(<column-layout>, <row-layout>);
  test-multiple-child-layout-manipulation(<column-layout>);
  test-layout-child-resizing(<column-layout>);
end class-test <column-layout>;
||#

(test <column-layout>-class-test
  :description "<column-layout> class test"
  (test-box-pane-layout (find-class '<column-layout>))
  (test-nested-layouts (find-class '<column-layout>) (find-class '<row-layout>))
  (test-multiple-child-layout-manipulation (find-class '<column-layout>))
  (test-layout-child-resizing (find-class '<column-layout>)))


#||
define duim-layouts class-test <row-layout> ()
  test-box-pane-layout(<row-layout>);
  test-nested-layouts(<row-layout>, <column-layout>);
  test-multiple-child-layout-manipulation(<row-layout>);
  test-layout-child-resizing(<row-layout>);
end class-test <row-layout>;
||#

(test <row-layout>-class-test
  :description "<row-layout> class test"
  (test-box-pane-layout (find-class '<row-layout>))
  (test-nested-layouts (find-class '<row-layout>) (find-class '<column-layout>))
  (test-multiple-child-layout-manipulation (find-class '<row-layout>))
  (test-layout-child-resizing (find-class '<row-layout>)))


#||

/// Button box layout tests

define method test-button-box 
    (class :: <class>, #key items = #("red", "green", "blue")) => ()
  let name = gadget-class-name(class);
  let box = make-test-pane(class, items: items, spacing: 0);
  let layout = sheet-child(box);
  check-layout-pane-layout(layout, name)
end test-button-box;
||#

(defmethod test-button-box ((class class) &key (items #(:red :green :blue)))
  (let* ((name   (gadget-class-name class))
	 (box    (make-test-pane class :items items :spacing 0))
	 (layout (sheet-child box)))
    (check-layout-pane-layout layout name)))


#||
define test button-box-layouts-test ()
  test-button-box(<push-box>);
  test-button-box(<radio-box>);
  test-button-box(<check-box>);
end test button-box-layouts-test;
||#

(test button-box-layouts-test
  :description "button box layouts test"
  (test-button-box (find-class '<push-box>))
  (test-button-box (find-class '<radio-box>))
  (test-button-box (find-class '<check-box>)))


#||
/// Layout manipulation tests

define method test-single-child-layout-manipulation (class :: <class>) => ()
  let class-name = gadget-class-name(class);
  let button1 = make-test-pane(<test-push-button-pane>);
  let layout = make-test-pane(class, child: button1);
  check-equal(concatenate(class-name, " child's initial parent"),
              sheet-parent(button1), layout);
  let button2 = make-test-pane(<test-radio-button-pane>);
  replace-child(layout, button1, button2);
  check-equal(concatenate(class-name, " replace-child old child unparented"),
              sheet-parent(button1), #f);
  check-equal(concatenate(class-name, " replace-child new child's parent"),
              sheet-parent(button2), layout);
  let button3 = make-test-pane(<test-check-button-pane>);
  sheet-child(layout) := button3;
  let button4 = make-test-pane(<test-push-button-pane>);
  remove-child(layout, button3);
  add-child(layout, button4);
  check-equal(concatenate(class-name, " removed child unparented"),
              sheet-parent(button3), #f);
  check-equal(concatenate(class-name, " added child parent"),
              sheet-parent(button4), layout);
end method test-single-child-layout-manipulation;
||#

(defmethod test-single-child-layout-manipulation ((class class))
  (let* ((class-name (gadget-class-name class))
	 (button1 (make-test-pane (find-class '<test-push-button-pane>)))
	 (layout  (make-test-pane class :child button1)))
    (is (equal? (sheet-parent button1) layout)
	(concatenate 'string class-name " child's initial parent"))
    (let ((button2 (make-test-pane (find-class '<test-radio-button-pane>))))
      (replace-child layout button1 button2)
      (is (equal? (sheet-parent button1) nil)
	  (concatenate 'string class-name " replace-child old child unparented"))
      (is (equal? (sheet-parent button2) layout)
	  (concatenate 'string class-name " replace-child new child's parent"))
      (let ((button3 (make-test-pane (find-class '<test-check-button-pane>))))
	(setf (sheet-child layout) button3)
	(let ((button4 (make-test-pane (find-class '<test-push-button-pane>))))
	  (remove-child layout button3)
	  (add-child layout button4)
	  (is (equal? (sheet-parent button3) nil)
	      (concatenate 'string class-name " removed child unparented"))
	  (is (equal? (sheet-parent button4) layout)
	      (concatenate 'string class-name " added child parent")))))))


#||
define test layout-manipulation-test ()
  test-single-child-layout-manipulation(<spacing-pane>);
  test-single-child-layout-manipulation(<border-pane>);
  test-single-child-layout-manipulation(<test-viewport>);
end test layout-manipulation-test;
||#

(test layout-manipulation-test
  :description "layout manipulation test"
  (test-single-child-layout-manipulation (find-class '<spacing-pane>))
  (test-single-child-layout-manipulation (find-class '<border-pane>))
  (test-single-child-layout-manipulation (find-class '<test-viewport>)))


#||
define test layout-resizing-test ()
  test-layout-child-resizing(<spacing-pane>);
  test-layout-child-resizing(<border-pane>);
  test-layout-child-resizing(<viewport>);
end test layout-resizing-test;
||#

(test layout-resizing-test
  :description "layout resizing test"
  (test-layout-child-resizing (find-class '<spacing-pane>))
  (test-layout-child-resizing (find-class '<border-pane>))
  (test-layout-child-resizing (find-class '<viewport>)))


#||

/// Define the layout test suite

define suite duim-layouts-suite ()
  test button-box-layouts-test;
  test top-level-sheet-layouts-test;
  test layout-manipulation-test;
  test layout-resizing-test;
end suite duim-layouts-suite;
||#

(defun test-layouts ()
  (run! 'duim-layouts))

