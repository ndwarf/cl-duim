;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

(in-package :duim-tests)

(def-suite all-tests
    :description "Master suite of all duim tests")
