;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-TESTS -*-
(in-package :duim-tests)

(def-suite duim-geometry
    :description "DUIM-GEOMETRY test suite"
    :in all-tests)
(in-suite duim-geometry)


#||
define sideways method make-test-instance
    (class == <point>) => (instance :: <point>)
  make(<point>, x: 10, y: 20)
end method make-test-instance;
||#

(test make-test-<point>-instance
  :description "make-test-instance <point>"
  (is-true (make-point 10 20)))


#||
define duim-geometry constant-test $largest-coordinate ()
  check-true("$smallest-coordinate < $largest-coordinate",
             $smallest-coordinate < $largest-coordinate);
  check-true("$largest-coordinate > 10000",
             $largest-coordinate > 10000)
end constant-test $largest-coordinate;
||#

(test $largest-coordinate-constant-tests
      :description "$largest-coordinate constant test"
  (is (< +smallest-coordinate+ +largest-coordinate+) "$smallest-coordinate < $largest-coordinate")
  (is (> +largest-coordinate+ 10000) "$largest-coordinate > 10000"))


#||
define duim-geometry constant-test $smallest-coordinate ()
  check-true("$smallest-coordinate < -10000",
             $smallest-coordinate < -10000)
end constant-test $smallest-coordinate;
||#

(test $smallest-coordinate-constant-tests
      :description "$smallest-coordinate constant test"
      (is (< +smallest-coordinate+ -10000) "$smallest-coordinate < -10000"))


#||

/// Function tests

define duim-geometry function-test do-coordinates ()
  //---*** Fill this in...
end function-test do-coordinates;

define duim-geometry function-test do-endpoint-coordinates ()
  //---*** Fill this in...
end function-test do-endpoint-coordinates;

define duim-geometry function-test fix-coordinate ()
  //---*** Fill this in...
end function-test fix-coordinate;

define duim-geometry function-test bounding-box ()
  //---*** Fill this in...
end function-test bounding-box;

define duim-geometry function-test bounding-box? ()
  //---*** Fill this in...
end function-test bounding-box?;

define duim-geometry function-test box-edges ()
  //---*** Fill this in...
end function-test box-edges;

define duim-geometry function-test set-box-edges ()
  //---*** Fill this in...
end function-test set-box-edges;

define duim-geometry function-test box-position ()
  //---*** Fill this in...
end function-test box-position;

define duim-geometry function-test set-box-position ()
  //---*** Fill this in...
end function-test set-box-position;

define duim-geometry function-test box-size ()
  //---*** Fill this in...
end function-test box-size;

define duim-geometry function-test set-box-size ()
  //---*** Fill this in...
end function-test set-box-size;

define duim-geometry function-test box-left ()
  //---*** Fill this in...
end function-test box-left;

define duim-geometry function-test box-top ()
  //---*** Fill this in...
end function-test box-top;

define duim-geometry function-test box-right ()
  //---*** Fill this in...
end function-test box-right;

define duim-geometry function-test box-bottom ()
  //---*** Fill this in...
end function-test box-bottom;

define duim-geometry function-test box-height ()
  //---*** Fill this in...
end function-test box-height;

define duim-geometry function-test box-width ()
  //---*** Fill this in...
end function-test box-width;

define duim-geometry function-test make-bounding-box ()
  //---*** Fill this in...
end function-test make-bounding-box;


/// Tests
define test coordinates-test ()
  let x-count = 0;
  let y-count = 0;
  do-coordinates(method (x, y)
                   x-count := x-count + x;
                   y-count := y-count + y;
                 end method,
                 vector(10, 100,
                        90, 200));
  check-true("do-coordinates in x", x-count = 100);
  check-true("do-coordinates in y", y-count = 300);
  let top-count = 0;
  let left-count = 0;
  let bottom-count = 0;
  let right-count = 0;
  do-endpoint-coordinates
    (method (left, top, right, bottom)
       left-count   := left-count   + left;
       top-count    := top-count    + top;
       right-count  := right-count  + right;
       bottom-count := bottom-count + bottom;
     end method,
     vector(10,  100,
            150, 300,
            90,  200,
            250, 400));
  check-true("do-endpoint-coordinates for left",   left-count = 100);
  check-true("do-endpoint-coordinates for top",    top-count = 300);
  check-true("do-endpoint-coordinates for right",  right-count = 400);
  check-true("do-endpoint-coordinates for bottom", bottom-count = 700);
end test coordinates-test;
||#

(test do-coordinates-test
      :description "test do-coordinates"
      (let ((x-count 0)
	    (y-count 0))
	(do-coordinates #'(lambda (x y)
			    (setf x-count (+ x-count x))
			    (setf y-count (+ y-count y)))
	  (vector 10 100 90 200))
	(is (= x-count 100) "do-coordinates in x")
	(is (= y-count 300) "do-coordinates in y")))


(test do-endpoint-coordinates-test
      :description "test do-endpoint-coordinates"
      (let ((top-count 0)
	    (left-count 0)
	    (bottom-count 0)
	    (right-count 0))
	(do-endpoint-coordinates #'(lambda (left top right bottom)
				     (setf left-count (+ left-count left))
				     (setf top-count (+ top-count top))
				     (setf right-count (+ right-count right))
				     (setf bottom-count (+ bottom-count bottom)))
	  (vector 10 100 150 300 90 200 250 400))
	(is (= left-count 100) "do-endpoint-coordinates for left")
	(is (= top-count 300) "do-endpoint-coordinates for top")
	(is (= right-count 400) "do-endpoint-coordinates for right")
	(is (= bottom-count 700) "do-endpoint-coordinates for bottom")))


#||

/// Geometry class tests

define sideways method make-test-instance
    (class == <bounding-box>) => (box :: <bounding-box>)
  make(<bounding-box>, left: 0, top: 0, right: 100, bottom: 100)
end method make-test-instance;
||#

(test make-test-<bounding-box>-instance
  :description "make-test-instance <bounding-box>"
  (is-true (make-bounding-box 0 0 100 100)))


#||
define sideways method make-test-instance
    (class == <standard-point>) => (box :: <standard-point>)
  make(<standard-point>, x: 100, y: 150)
end method make-test-instance;
||#

(test make-test-<standard-point>-instance
  :description "make-test-instance <standard-point>"
  (is-true (make-instance '<standard-point> :x 100 :y 150)))


#||
define sideways method make-test-instance
    (class == <reflection-underspecified>)
 => (instance :: <reflection-underspecified>)
  make(<reflection-underspecified>, points: #())
end method make-test-instance;
||#

(test make-test-<reflection-underspecified>-instance
  :description "make-test-instance <reflection-underspecified>"
  (is-true (make-instance '<reflection-underspecified>
			  :points #())))


#||
define sideways method make-test-instance
    (class == <singular-transform>) => (instance :: <singular-transform>)
  make(<singular-transform>, transform: make-transform(0, 0, 0, 0, 0, 0))
end method make-test-instance;
||#

(test make-test-<singular-transform>-instance
  :description "make-test-instance <singular-transform>"
  (is-true (make-instance '<singular-transform>
			  :transform (make-transform 0 0 0 0 0 0))))


#||
define sideways method make-test-instance
    (class == <transform-underspecified>)
 => (instance :: <transform-underspecified>)
  make(<transform-underspecified>, points: #())
end method make-test-instance;
||#

(test make-test-<transform-underspecified>-instance
  :description "make-test-instance <transform-underspecified>"
  (is-true (make-instance '<transform-underspecified> :points #())))


#||           
define duim-geometry class-test <bounding-box> ()
  let box = make-bounding-box(50, 100, 150, 300);
  check-true("bounding-box?(box)", bounding-box?(box));
  check-false("bounding-box?(100)", bounding-box?(100));
  check-equal("box-left",   box-left(box),   50);
  check-equal("box-top",    box-top(box),    100);
  check-equal("box-right",  box-right(box),  150);
  check-equal("box-bottom", box-bottom(box), 300);
  check-equal("box-width",  box-width(box), 100);
  check-equal("box-height", box-height(box), 200);
  let (x, y) = box-position(box);
  check-equal("box-position x", x, 50);
  check-equal("box-position y", y, 100);
  let (left, top, right, bottom) = box-edges(box);
  check-equal("box-edges left",   left,   50);
  check-equal("box-edges top",    top,    100);
  check-equal("box-edges right",  right,  150);
  check-equal("box-edges bottom", bottom, 300);
  set-box-size(box, 400, 450);
  check-equal("set-box-size keeps old left",   box-left(box),   50);
  check-equal("set-box-size keeps old top",    box-top(box),    100);
  check-equal("set-box-size new width", box-width(box), 400);
  check-equal("set-box-size new height", box-height(box), 450);
  set-box-position(box, 0, 50);
  check-equal("set-box-position new left",         box-left(box),   0);
  check-equal("set-box-position new top",          box-top(box),    50);
  check-equal("set-box-position keeps old width",  box-width(box),  400);
  check-equal("set-box-position keeps old height", box-height(box), 450);
  set-box-edges(box, 100, 150, 200, 400);
  check-equal("set-box-edges box-left",   box-left(box),   100);
  check-equal("set-box-edges box-top",    box-top(box),    150);
  check-equal("set-box-edges box-right",  box-right(box),  200);
  check-equal("set-box-edges box-bottom", box-bottom(box), 400);
  let new-box = bounding-box(box);
  check-false("bounding-box creates a new box", new-box == box);
  check-equal("bounding-box creates identical box", new-box, box);
end class-test <bounding-box>;
||#

(test <bounding-box>-class-test
      :description "tests for <bounding-box> class"
      (let ((box (make-bounding-box 50 100 150 300)))
	(is (bounding-box-p box) "(bounding-box-p box)")
	(is (not (bounding-box-p 100)) "(bounding-box-p 100)")
	(is (= (box-left box) 50) "box-left")
	(is (= (box-top box) 100) "box-top")
	(is (= (box-right box) 150) "box-right")
	(is (= (box-bottom box) 300) "box-bottom")
	(is (= (box-width box) 100) "box-width")
	(is (= (box-height box) 200) "box-height")
	(multiple-value-bind (x y) (box-position box)
	  (is (= x 50) "box-position x")
	  (is (= y 100) "box-position y"))
	(multiple-value-bind (left top right bottom) (box-edges box)
	  (is (= left 50) "box-edges left")
	  (is (= top 100) "box-edges top")
	  (is (= right 150) "box-edges right")
	  (is (= bottom 300) "box-edges bottom"))
	(set-box-size box 400 450)
	(is (= (box-left box) 50) "set-box-size keeps old left")
	(is (= (box-top box) 100) "set-box-size keeps old top")
	(is (= (box-width box) 400) "set-box-size new width")
	(is (= (box-height box) 450) "set-box-size new height")
	(set-box-position box 0 50)
	(is (= (box-left box) 0) "set-box-position new left")
	(is (= (box-top box) 50) "set-box-position new top")
	(is (= (box-width box) 400) "set-box-position keeps old width")
	(is (= (box-height box) 450) "set-box-position keeps old height")
	(set-box-edges box 100 150 200 400)
	(is (= (box-left box) 100) "set-box-edges box-left")
	(is (= (box-top box) 150) "set-box-edges box-top")
	(is (= (box-right box) 200) "set-box-edges box-right")
	(is (= (box-bottom box) 400) "set-box-edges box-bottom")
	(let ((new-box (bounding-box box)))
	  (is (not (eql new-box box)) "bounding-box creates a new box")
	  (is (equal? new-box box) "bounding-box creates identical box"))))


#||
define duim-geometry class-test <area> ()
  //---*** Fill this in...
end class-test <area>;

define duim-geometry class-test <path> ()
  //---*** Fill this in...
end class-test <path>;

define duim-geometry class-test <point> ()
  //---*** Fill this in...
end class-test <point>;

define duim-geometry class-test <region-set> ()
  //---*** Fill this in...
end class-test <region-set>;

define duim-geometry class-test <region> ()
  //---*** Fill this in...
end class-test <region>;

define duim-geometry class-test <standard-point> ()
  //---*** Fill this in...
end class-test <standard-point>;

define duim-geometry class-test <singular-transform> ()
  //---*** Fill this in...
end class-test <singular-transform>;

define duim-geometry class-test <transform-error> ()
  //---*** Fill this in...
end class-test <transform-error>;

define duim-geometry class-test <transform> ()
  //---*** Fill this in...
end class-test <transform>;

define duim-extended-geometry class-test <polygon> ()
  //---*** Fill this in...
end class-test <polygon>;

define duim-extended-geometry class-test <line> ()
  //---*** Fill this in...
end class-test <line>;

define duim-extended-geometry class-test <elliptical-arc> ()
  //---*** Fill this in...
end class-test <elliptical-arc>;

define duim-extended-geometry class-test <rectangle> ()
  //---*** Fill this in...
end class-test <rectangle>;

define duim-extended-geometry class-test <ellipse> ()
  //---*** Fill this in...
end class-test <ellipse>;

define duim-extended-geometry class-test <polyline> ()
  //---*** Fill this in...
end class-test <polyline>;


/// Install the geometry test suite

define suite duim-geometry-suite ()
  test coordinates-test;
end suite duim-geometry-suite;
||#

;;; Could use * "(5am:run! 'duim-tests::duim-geometry)" instead...
(defun test-geometry ()
  (run! 'duim-geometry))

