These tests were cleaned up in April 2020. Compiler warnings were
fixed. Where warnings remain, they are a reminder to fix
something. The only remaining one at this time is in layouts.lisp,
where there is an unused lexial variable CHILD-NAME. This is because
the original DUIM tests used this, but the existing CL-DUIM tests are
abbreviated, and need to be expanded.

I have left the original Dylan-DUIM tests in the source text. At this
stage they are required to complete the CL-DUIM tests. Once CL-DUIM
reaches the same level of test coverage, they can be removed.

The test harness is setup in test-port.lisp and classes.lisp.

# Coupling of the tests
The DUIM-CORE and DUIM-EXTENDED packages are coupled in the REGION
test. This is because all types of REGION are tested here, and that
includes POINT and the other regions, that reside in
EXTENDED. Unfortuntely this dependency means you cannot test DUIM-CORE
without having extensions available. Be careful not to introduce
further coupling.

# Coverage
The Dylan tests look as if they were added late, and quickly. There
are many places marked "Fill this in". The CL tests, for the most
part, implement those tests that were existing in Dylan, not those to
be filled in.

A notable gap in coverage is in dialogs.lisp, where Dylan had only one
implemented tests, which didn't work. Implementing dialog tests is
tracked in [issue 13][ISSUE-13]

## Future work
There are tools to help determine test coverage. SBCL has a built-in
one, [sb-cover][SB-COVER], and there is a general tool [COVER][COVER],
both of which generate reports on the effectivness of the test
suite. COVER is best investigated first, as it appears more mature and
is cross-platform.



[ISSUE-13] https://bitbucket.org/ndwarf/cl-duim/issues/13/implement-dialog-tests
[SB-COVER] http://www.sbcl.org/manual/index.html#sb_002dcover
[COVER] https://github.com/pfdietz/cover
