;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-TESTS -*-
(in-package :duim-tests)

(def-suite duim-gadgets
    :description "DUIM-GADGETS test suite"
    :in all-tests)
(in-suite duim-gadgets)

#||
define sideways method make-test-instance
    (class :: subclass(<abstract-gadget>)) => (instance :: <gadget>)
  make-test-pane(class)
end method make-test-instance;

define sideways method make-test-instance
    (class == <table-control>) => (instance :: <table-control>)
  make-test-pane(<table-control>,
		 headings: #["Heading"],
		 generators: vector(identity))
end method make-test-instance;
||#
(test make-test-<table-control>-instance
  :description "make-test-instance <table-control>"
  (is-true (make-test-pane (find-class '<table-control>)
			   :headings #("Heading")
			   :generators (vector #'identity))))


#||
define sideways method make-test-instance
    (class == <tree-control>) => (instance :: <tree-control>)
  make-test-pane(<tree-control>,
		 children-generator: always(#[]))
end method make-test-instance;
||#

(test make-test-<tree-control>-instance
  :description "make-test-instance <tree-control>"
  (is-true (make-test-pane (find-class '<tree-control>)
			   :children-generator (constantly #()))))




#||

/// Gadget testing

define sideways method class-test-function
    (class :: subclass(<abstract-gadget>)) => (function :: <function>)
  test-gadget-class
end method class-test-function;

define open generic test-gadget-class
    (class :: subclass(<abstract-gadget>), #key, #all-keys) => ();

define method test-gadget-class
    (class :: subclass(<abstract-gadget>), #key name, instantiable?, #all-keys) => ()
  if (instantiable?)
  end
end method test-gadget-class;


/// parent tests

define method subchild? 
    (sheet :: <object>, child :: <sheet>) => (subchild? :: <boolean>)
  let parent = sheet-parent(child);
  if (parent)
    if (parent == sheet)
      #t
    else
      subchild?(sheet, parent)
    end
  end
end method subchild?;
||#

(defmethod subchild? ((sheet t) (child <sheet>))
  (let ((parent (sheet-parent child)))
    (when parent
      (if (eql parent sheet)
	  t
	  (subchild? sheet parent)))))


#||
define test parents-test ()
  let button = make-test-pane(<push-button>);
  let layout = make-test-pane(<column-layout>, children: vector(button));
  check-equal("Initial gadget parent", layout, button.sheet-parent);
  let frame = make-test-frame(<test-frame>, layout: layout);
  check-true("Parent after initializing frame layout",
	      subchild?(top-level-sheet(frame), layout));
  remove-child(layout, button);
  let check-button = make-test-pane(<check-button>);
  sheet-children(layout) := vector(button, check-button);
  check-equal("Parent after setting sheet children 1",
	      layout, button.sheet-parent);
  check-equal("Parent after setting sheet children 2",
	      layout, check-button.sheet-parent);
end test parents-test;
||#

(test parents-test
  :description "parents test"
  (let* ((button (make-test-pane (find-class '<push-button>)))
	 (layout (make-test-pane (find-class '<column-layout>) :children (vector button))))
    (is (equal? layout (sheet-parent button))
	"Initial gadget parent")
    (let ((frame (make-test-frame (find-class '<test-frame>) :layout layout)))
      (is (subchild? (top-level-sheet frame) layout) "Parent after initializing frame layout")
      (remove-child layout button)
      (let ((check-button (make-test-pane (find-class '<check-button>))))
	(setf (sheet-children layout) (vector button check-button))
	(is (equal? layout (sheet-parent button)) "Parent after setting sheet children 1")
	(is (equal? layout (sheet-parent check-button))
	    "Parent after setting sheet children 2")))))


#||

/// Gadget tests

define method verify-gadget-box-pane-button-selection 
    (box :: <gadget>) => ()
  let buttons = box.gadget-box-buttons;
  let items = box.gadget-items;
  let selection = box.gadget-selection;
  let failed = #f;
  if (buttons & items)
    for (index from 0 to size(items),
         button in buttons)
      if (button.gadget-value ~= member?(index, selection))
        failed := #t;
      end
    end;
  else
    failed := items
  end;
  check-equal(concatenate(gadget-class-name(box), " button selection"),
              failed, #f)
end method verify-gadget-box-pane-button-selection;
||#

(defmethod verify-gadget-box-pane-button-selection ((box <gadget>))
  (let ((buttons (gadget-box-buttons box))
	(items   (gadget-items box))
	(selection (gadget-selection box))
	(failed nil))
    (if (and buttons items)
	(loop for index from 0 to (length items)
	   for button across buttons
	   do (when (not (equal? (gadget-value button) (MEMBER? index selection)))
		(setf failed t)))
	  ;; else
	(setf failed items))
    (is-false failed (concatenate 'string (gadget-class-name box) " button selection"))))


#||
define constant $default-collection-items
  = #(#("Red", #"red"), 
      #("Green", #"green"),
      #("Blue", #"blue"));
||#

(defparameter *default-collection-items*
  (list (list "Red" :red)
	(list "Green" :green)
	(list "Blue" :blue)))


#||
define method make-collection-gadget
    (class :: subclass(<collection-gadget>), #rest args,
     #key items = $default-collection-items,
          label-key = first,
          value-key = second,
     #all-keys)
 => (gadget :: <collection-gadget>)
  apply(make-test-pane, class, 
        items: items,
        label-key: label-key,
        value-key: value-key,
        args)
end method make-collection-gadget;
||#
(defmethod make-collection-gadget ((class class) &rest args
				   &key (items *default-collection-items*)
				     (label-key #'first)
				     (value-key #'second)
				     &allow-other-keys)
  (declare (ignore items label-key value-key)) ; SN:20200405 quiet compiler until we can refactor this
  (if (subtypep class (find-class '<collection-gadget>))
      (apply #'make-collection-gadget-1 class args)
      (call-next-method)))

(defun make-collection-gadget-1 (class &rest args
				 &key (items *default-collection-items*)
				 (label-key #'first)
				 (value-key #'second)
				   &allow-other-keys)
  ;; FIXME: NEED A CONFIGURABLE WAY TO BUILD UP THESE FACTORY METHODS,
  ;; MAKING A CHANGE HERE IS NOT ACCESSIBLE TO OTHER TYPES THAT MIGHT
  ;; WANT TO IMPLEMENT THEIR OWN MAKE-COLLECTION-GADGET METHOD.
  (let ((make-fn (cond ((subtypep class (find-class '<table-control>))
			#'make-<table-control>-collection-gadget)
		       ((subtypep class (find-class '<collection-gadget>))
			#'make-test-pane)
		       (t
			(error "DUIM-TESTS;GADGETS:MAKE-COLLECTION-GADGET invoked on non-<COLLECTION-GADGET> type ~a" class)))))
    (apply make-fn class :items items :label-key label-key
	   :value-key value-key args)))



#||
define method make-collection-gadget
    (class :: subclass(<table-control>), #rest args,
     #key items = $default-collection-items,
          label-key = first,
          value-key = second,
     #all-keys)
 => (gadget :: <collection-gadget>)
  apply(make-test-pane, class, 
        items: items,
        label-key: label-key,
        value-key: value-key,
        headings: #("One", "Two"),
        generators: vector(identity, identity),
        args)
end method make-collection-gadget;
||#


(defmethod make-<table-control>-collection-gadget (class
						   &rest args
						   &key (items *default-collection-items*)
						     (label-key #'first)
						     (value-key #'second)
						     &allow-other-keys)
  (assert (INSTANCE? class (find-class '<table-control>)))
  (apply #'make-test-pane
	 class
	 :items items
	 :label-key label-key
	 :value-key value-key
	 :headings #("One" "Two")
	 :generators (vector #'identity #'identity)
	 args))


#||
define method verify-gadget-box-pane-buttons 
    (test :: <string>, box :: <gadget>) => ()
  let buttons = box.gadget-box-buttons;
  let items = box.gadget-items;
  check-equal(test, buttons & size(buttons), items & size(items));
end method verify-gadget-box-pane-buttons;
||#

(defmethod verify-gadget-box-pane-buttons ((test string) (box <gadget>))
  (let ((buttons (gadget-box-buttons box))
	(items   (gadget-items box)))
    (is (eql (and buttons (length buttons)) (and items (length items)))
	test)))


#||
define method test-gadget-box-pane-buttons 
    (class :: subclass(<gadget>)) => ()
  let box-pane = make-collection-gadget(class);
  let name = gadget-class-name(class);
  verify-gadget-box-pane-buttons(concatenate(name, " pane buttons"),
                                 box-pane);
  gadget-items(box-pane)
    := #(#("Four", 4), 
         #("Five", 5),
         #("Six", 6),
         #("Seven", 7));
  verify-gadget-box-pane-buttons(concatenate(name, " pane change buttons"),
                                 box-pane);
end method test-gadget-box-pane-buttons;
||#

(defmethod test-gadget-box-pane-buttons (class) ;<gadget-metaclass>))
  (warn "DUIM-TESTS;GADGETS:TEST-GADGET-BOX-PANE-BUTTONS invoked")
  (assert (INSTANCE? class (find-class '<gadget>)))
  (let ((box-pane (make-collection-gadget class))
	(name (gadget-class-name class)))
    (verify-gadget-box-pane-buttons (concatenate 'string name " pane buttons")
				    box-pane)
    (setf (gadget-items box-pane)
	  (list (list "Four" 4)
		(list "Five" 5)
		(list "Six" 6)
		(list "Seven" 7)))
    (verify-gadget-box-pane-buttons (concatenate 'string name " pane change buttons")
				    box-pane)))


#||
define method check-value 
    (name :: <string>, gadget :: <value-gadget>, expected-value) => ()
  check-equal(concatenate(name, " value"),
              gadget-value(gadget), expected-value)
end method check-value;
||#

(defmethod check-value ((name string) (gadget <value-gadget>) expected-value)
  (is (equal? (gadget-value gadget) expected-value)
      (concatenate 'string name " value")))


#||
define method test-collection-gadget-value 
    (gadget-class :: subclass(<collection-gadget>)) => ()
  let class-name = gadget-class-name(gadget-class);
  let pane = make-collection-gadget(gadget-class, selection-mode: #"none");
  let name = concatenate("single selection ", class-name);
  check-value(name, pane, #f);
  test-single-selection-collection-gadget-values(gadget-class, name: name);
  
  let pane = make-collection-gadget(gadget-class, selection-mode: #"multiple");
  let name = concatenate("multiple selection ", class-name);
  check-value(concatenate(name, " default"), pane, #());
  let pane
    = make-collection-gadget(gadget-class,
			     selection-mode: #"multiple",
			     value: #(#"red", #"green"));
  check-value(concatenate(name, " initial"), pane, #(#"red", #"green"));
  gadget-value(pane) := #(#"red", #"blue");
  check-value(concatenate(name, " new"), pane, #(#"red", #"blue"));
  gadget-selection(pane) := #();
  check-value(concatenate(name, " after selection cleared"), pane, #());
  gadget-selection(pane) := #(0, 1);
  check-value(concatenate(name, " after selection changed"),
	      pane, #(#"red", #"green"));
  gadget-items(pane) := reverse($default-collection-items);
  check-value(concatenate(name, " after items reordered"),
	      pane, #(#"red", #"green"));
  gadget-items(pane) := #(#("One", 1), #("Two", 2), #("Three", 3));
  check-value(concatenate(name, " after items changed"), pane, #())
end method test-collection-gadget-value;
||#

(defmethod test-collection-gadget-value (gadget-class)
  (let* ((class-name (gadget-class-name gadget-class))
	 (pane (make-collection-gadget gadget-class :selection-mode :none))
	 (name (concatenate 'string "single selection " class-name)))
    (is-false (gadget-value pane) name)
    (test-single-selection-collection-gadget-values gadget-class :name name)
    (let ((pane (make-collection-gadget gadget-class :selection-mode :multiple))
	  (name (concatenate 'string "multiple selection " class-name)))
      (is (equal? (gadget-value pane) '())
	  (concatenate 'string name " default"))
      (let ((pane (make-collection-gadget gadget-class
					  :selection-mode :multiple
					  :value '(:red :green))))
	(is (equal? (gadget-value pane) '(:red :green))
	    (concatenate 'string name " initial"))
	(setf (gadget-value pane) '(:red :blue))
	(is (equal? (gadget-value pane) '(:red :blue))
	    (concatenate 'string name " new"))
	(setf (gadget-selection pane) '())
	(is (equal? (gadget-value pane) '())
	    (concatenate 'string name " after selection cleared"))
	(setf (gadget-selection pane) '(0 1))
	(is (equal? (gadget-value pane) '(:red :green))
	    (concatenate 'string name " after selection changed"))
	(setf (gadget-items pane) (reverse *default-collection-items*))
	(is (equal? (gadget-value pane) '(:red :green))
	    (concatenate 'string name " after items reordered"))
	(setf (gadget-items pane) '(("One" 1) ("Two" 2) ("Three" 3)))
	(is (equal? (gadget-value pane) '())
	    (concatenate 'string name " after items changed"))))))


#||
define method test-single-selection-collection-gadget-values
    (gadget-class :: <class>,
     #key name = gadget-class-name(gadget-class),
          value = #"green") => ()
  let pane = make-collection-gadget(gadget-class, selection-mode: #"single");
  check-value(concatenate(name, " default"), pane, #"red");
  let pane 
    = make-collection-gadget(gadget-class,
                             selection-mode: #"single",
                             value: value);
  check-value(concatenate(name, " initial"), pane, #"green");
  gadget-value(pane) := #"blue";
  check-value(concatenate(name, " new"), pane, #"blue");
  gadget-items(pane) := reverse($default-collection-items);
  check-value(concatenate(name, " after items reordered"), pane, #"blue");
  gadget-items(pane) := #(#("One", 1), #("Two", 2), #("Three", 3));
  check-value(concatenate(name, " after items changed"), pane, #f);
end method test-single-selection-collection-gadget-values;
||#

(defmethod test-single-selection-collection-gadget-values ((gadget-class class)
							   &key (name (gadget-class-name gadget-class))
							     (value :green))
  (let ((pane (make-collection-gadget gadget-class :selection-mode :single)))
    (is (equal? (gadget-value pane) :red) (concatenate 'string name " default")))
  (let ((pane (make-collection-gadget gadget-class
				      :selection-mode :single
				      :value value)))
    (is (equal? (gadget-value pane) :green)
	(concatenate 'string name " initial"))
    (setf (gadget-value pane) :blue)
    (is (equal? (gadget-value pane) :blue) (concatenate 'string name " new"))
    (setf (gadget-items pane) (reverse *default-collection-items*))
    (is (equal? (gadget-value pane) :blue) (concatenate 'string name " after items reordered"))
    (setf (gadget-items pane) '(("One" 1) ("Two" 2) ("Three" 3)))
    (is-false (gadget-value pane)
	      (concatenate 'string name " after items changed"))))


#||
define method test-range-pane-values
    (class :: <class>, #key default = 10, make-function = make-test-pane) => ()
  let name = gadget-class-name(class);
  let gadget = make-function(class, value-range: range(from: 10, to: 100));
  check-value(concatenate(name, " default"), gadget, default);
  let gadget 
    = make-function(class, value-range: range(from: 10, to: 100), value: 20);
  check-value(concatenate(name, " initial"), gadget, 20);
  gadget-value(gadget) := 120;
  check-value(concatenate(name, " maximized"), gadget, 100);
  gadget-value(gadget) := 0;
  check-value(concatenate(name, " minimized"), gadget, 10);
  let new-range = range(from: 200, to: 300);
  gadget-value-range(gadget) := new-range;
  check-equal(concatenate(name, " new range"),
	      gadget-value-range(gadget), new-range);
  check-value(concatenate(name, " after range change"),
	      gadget, 200);
end method test-range-pane-values;
||#

(defmethod test-range-pane-values ((class class)
				   &key (default 10)
				     (make-function #'make-test-pane))
  (let ((name (gadget-class-name class))
	(gadget (funcall make-function class
			 :value-range (range :from 10 :to 100))))
    (is (equal? (gadget-value gadget) default)
	"~a default got ~a expected ~a" name (gadget-value gadget) default)
    (let ((gadget (funcall make-function class
			   :value-range (range :from 10 :to 100)
			   :value 20)))
      (is (equal? (gadget-value gadget) 20)
	  "~a initial got ~a expected ~a" name (gadget-value gadget) 20)
      (setf (gadget-value gadget) 120)
      (is (equal? (gadget-value gadget) 100)
	  "~a maximized got ~a expected ~a" name (gadget-value gadget) 100)
      (setf (gadget-value gadget) 0)
      (is (equal? (gadget-value gadget) 10)
	  "~a minimized got ~a expected ~a" name (gadget-value gadget) 10)
      (let ((new-range (range :from 200 :to 300)))
	(setf (gadget-value-range gadget) new-range)
	(is (equal? (gadget-value-range gadget) new-range)
	    (concatenate 'string name " new range"))
	(is (equal? (gadget-value gadget) 200)
	    (concatenate 'string name " after range change"))))))


#||
define method test-slug-gadget-values
    (class :: <class>, 
     #key default = 10, 
          slug-size = 20,
          make-function = make-test-pane) => ()
  let name = gadget-class-name(class);
  let gadget
    = make-function(class, slug-size: slug-size, value-range: range(from: 10, to: 100));
  check-value(concatenate(name, " default"), gadget, default);
  let gadget 
    = make-function(class, 
                    slug-size: slug-size,
                    value-range: range(from: 10, to: 100), value: 20);
  check-value(concatenate(name, " initial"), gadget, 20);
  gadget-value(gadget) := 120;
  check-value(concatenate(name, " maximized"), gadget, 100 - slug-size + 1);
  gadget-value(gadget) := 0;
  check-value(concatenate(name, " minimized"), gadget, 10);
  let new-range = range(from: 200, to: 300);
  gadget-value-range(gadget) := new-range;
  check-equal(concatenate(name, " new range"),
	      gadget-value-range(gadget), new-range);
  check-value(concatenate(name, " after range change"),
	      gadget, 200);
end method test-slug-gadget-values;
||#

(defmethod test-slug-gadget-values ((class class)
				    &key (default 10)
				      (slug-size 20)
				      (make-function #'make-test-pane))
  (let ((name (gadget-class-name class))
	(gadget (funcall make-function class
			 :slug-size slug-size
			 :value-range (range :from 10 :to 100))))
    (is (equal? (gadget-value gadget) default)
	(concatenate 'string name " default"))
    (let ((gadget (funcall make-function class
			   :slug-size slug-size
			   :value-range (range :from 10 :to 100) :value 20)))
      (is (equal? (gadget-value gadget) 20)
	  (concatenate 'string name " initial"))
      (setf (gadget-value gadget) 120)
      (is (equal? (gadget-value gadget) (+ (- 100 slug-size) 1))
	  (concatenate 'string name " maximized"))
      (setf (gadget-value gadget) 0)
      (is (equal? (gadget-value gadget) 10)
	  (concatenate 'string name " minimized"))
      (let ((new-range (range :from 200 :to 300)))
	(setf (gadget-value-range gadget) new-range)
	(is (equal? (gadget-value-range gadget) new-range)
	    (concatenate 'string name " new range"))
	(is (equal? (gadget-value gadget) 200)
	    (concatenate 'string name " after range change"))))))


#||
define method test-button-values
    (class :: <class>) => ()
  let button = make-test-pane(class);
  let name = gadget-class-name(class);
  check-value(concatenate(name, " default"), button, #f);
  gadget-value(button) := #t;
  check-value(concatenate(name, " new"), button, #t);
  check-value(concatenate(name, " initial"), make-test-pane(class, value: #t), #t);
end method test-button-values;
||#

(defmethod test-button-values ((class class))
  (let ((button (make-test-pane class))
	(name (gadget-class-name class)))
    (is-false (gadget-value button) (concatenate 'string name " default"))
    (setf (gadget-value button) t)
    (is-true (gadget-value button) (concatenate 'string name " new"))
    (is-true (gadget-value (make-test-pane class :value t))
	     (concatenate 'string name " initial"))))


#||
define method test-text-field-values 
    (class :: <class>) => ()
  let name = gadget-class-name(class);
  let text-field = make-test-pane(class);
  check-value(concatenate(name, " default"), text-field, "");
  gadget-value(text-field) := "Hello";
  check-value(concatenate(name, " changed"), text-field, "Hello");
  check-value(concatenate(name, " initial"),
	      make-test-pane(class, value: "Initial"), "Initial");
  let text-field = make-test-pane(class, value: 0, value-type: <integer>);
  check-equal(concatenate(name, " integer initial value"),
              gadget-value(text-field), 0);
  check-equal(concatenate(name, " integer initial text"),
              gadget-text(text-field), "0");
  gadget-value(text-field) := 10;
  check-equal(concatenate(name, " changed value value"),
              gadget-value(text-field), 10);
  check-equal(concatenate(name, " changed value text"),
              gadget-text(text-field), "10");
  gadget-text(text-field) := "100";
  check-equal(concatenate(name, " changed text value"),
              gadget-value(text-field), 100);
  check-equal(concatenate(name, " changed text text"),
              gadget-text(text-field), "100");
end method test-text-field-values;
||#

(defmethod test-text-field-values ((class class))
  (let ((name (gadget-class-name class))
	(text-field (make-test-pane class)))
    (is (equal? (gadget-value text-field) "")
	(concatenate 'string name " default"))
    (setf (gadget-value text-field) "Hello")
    (is (equal? (gadget-value text-field) "Hello")
	(concatenate 'string name " changed"))
    (is (equal? (gadget-value (make-test-pane class :value "Initial"))
		"Initial")
	(concatenate 'string name " initial"))
    (let ((text-field (make-test-pane class :value 0 :value-type (find-class 'integer))))
      (is (equal? (gadget-value text-field) 0)
	  (concatenate 'string name " integer initial value"))
      (is (equal? (gadget-text text-field) "0")
	  (concatenate 'string name " integer initial text"))
      (setf (gadget-value text-field) 10)
      (is (equal? (gadget-value text-field) 10)
	  (concatenate 'string name " changed value value"))
      (is (equal? (gadget-text text-field) "10")
	  (concatenate 'string name " changed value text"))
      (setf (gadget-text text-field) "100")
      (is (equal? (gadget-value text-field) 100)
	  (concatenate 'string name " changed text value"))
      (is (equal? (gadget-text text-field) "100")
	  (concatenate 'string name " changed text text")))))


#||
define method test-no-value-gadget-values 
    (class :: <class>) => ()
  let name = gadget-class-name(class);
  check-value(concatenate(name, " default"), make-test-pane(class), #f);
  check-value(concatenate(name, " initial"), make-test-pane(class, value: 10), #f);
end method test-no-value-gadget-values;
||#

(defmethod test-no-value-gadget-values ((class class))
  (let ((name (gadget-class-name class)))
    (is (equal? (gadget-value (make-test-pane class)) nil)
	(concatenate 'string name " default"))
    (is (equal? (gadget-value (make-test-pane class :value 10)) nil)
	(concatenate 'string name " initial"))))


#||
define method test-tree-control-children-generator 
    (x :: <integer>) => (children :: <vector>)
  let children
    = if (x < 8)
        vector(x * 2, (x * 2) + 1)
      else
        #[]
      end;
  children
end method test-tree-control-children-generator;
||#

(defmethod test-tree-control-children-generator ((x integer))
  (let ((children (if (< x 8)
		      (vector (* x 2) (+ (* x 2) 1))
		      #())))
    children))


#||
define method make-test-tree-control 
    (#rest args, #key depth = 2, #all-keys)
 => (tree-control :: <tree-control>)
  apply(make-test-pane, <tree-control>,
        roots: #(1),
        children-generator: test-tree-control-children-generator,
        depth: depth,
        args)
end method make-test-tree-control;
||#

(defun make-test-tree-control (&rest args &key (depth 2) &allow-other-keys)
  (apply #'make-test-pane (find-class '<tree-control>)
	 :roots '(1)
	 :children-generator #'test-tree-control-children-generator
	 :depth depth
	 args))


#||
define method test-tree-control-values () => ()
  let name = gadget-class-name(<tree-control>);

  let pane-name = concatenate("no selection ", name);
  let pane = make-test-tree-control(selection-mode: #"none");
  check-value(pane-name, pane, #f);

  let pane-name = concatenate("multiple selection ", name);
  let pane = make-test-tree-control(selection-mode: #"multiple");
  check-value(concatenate(pane-name, " default"), pane, #());
  let pane
    = make-test-tree-control(selection-mode: #"multiple", value: #(2, 4));
  check-value(concatenate(name, " initial"), pane, #(2, 4));
  gadget-value(pane) := #(3, 5);
  check-value(concatenate(name, " new"), pane, #(3, 5));
  gadget-selection(pane) := #();
  check-value(concatenate(name, " after selection cleared"), pane, #());
  gadget-selection(pane) := #(0, 1);
  check-value(concatenate(name, " after selection changed"),
	      pane, #(1, 2));
end method test-tree-control-values;
||#

(defmethod test-tree-control-values ()
  (let* ((name (gadget-class-name (find-class '<tree-control>)))

	 (pane-name (concatenate 'string "no selection " name))
	 (pane (make-test-tree-control :selection-mode :none)))
    (is (equal? (gadget-value pane) nil) pane-name)

    (let ((pane-name (concatenate 'string "multiple selection " name))
	  (pane (make-test-tree-control :selection-mode :multiple)))
      (is (equal? (gadget-value pane) '())
	  (concatenate 'string pane-name " default"))
      (let ((pane (make-test-tree-control :selection-mode :multiple :value '(2 4))))
	(is (equal? (gadget-value pane) '(2 4))
	    (concatenate 'string name " initial"))
	(setf (gadget-value pane) '(3 5))
	(is (equal? (gadget-value pane) '(3 5))
	    (concatenate 'string name " new"))
	(setf (gadget-selection pane) '())
	(is (equal? (gadget-value pane) ())
	    (concatenate 'string name " after selection cleared"))
	(setf (gadget-selection pane) '(0 1))
	(is (equal? (gadget-value pane) '(1 2))
	    (concatenate 'string name " after selection changed"))))))


#||
define method make-test-table-control 
    (#rest args, #key, #all-keys)
 => (table-control :: <table-control>)
  apply(make-test-pane, <table-control>,
        headings: #("Identity", "+1", "+2"),
        generators: vector(identity, curry(\+, 1), curry(\+, 2)),
        args)
end method make-test-table-control;
||#

(defun make-test-table-control (&rest args &key &allow-other-keys)
  (apply #'make-test-pane (find-class '<table-control>)
	 :headings '("Identity" "+1" "+2")
	 :generators (vector #'identity (alexandria:curry #'+ 1) (alexandria:curry #'+ 2))
	 args))


#||
define method test-table-control-values () => ()
  let name = gadget-class-name(<table-control>);
  let pane = make-test-table-control(items: range(from: 0, to: 10));
  check-value(concatenate(name, " default"), pane, 0);
  gadget-value(pane) := 4;
  check-value(concatenate(name, " new"), pane, 4);
  let pane = make-test-table-control(items: range(from: 0, to: 10), value: 5);
  check-value(concatenate(name, " initial"), pane, 5);
end method test-table-control-values;
||#

(defmethod test-table-control-values ()
  (let ((name (gadget-class-name (find-class '<table-control>)))
	(pane (make-test-table-control :items (range :from 0 :to 10))))
    (is (equal? (gadget-value pane) 0)
	"~a default got ~a expected ~a" name (gadget-value pane) 0)
    (setf (gadget-value pane) 4)
    (is (equal? (gadget-value pane) 4)
	"~a new got ~a expected ~a" name (gadget-value pane) 4)
    (let ((pane (make-test-table-control
		 :items (range :from 0 :to 10)
		 :value 5)))
      (is (equal? (gadget-value pane) 5)
	  "~a initial got ~a expected ~a" name (gadget-value pane) 5))))


#||

/// Gadget labels

define method test-gadget-label (class :: <class>, #rest args) => ()
  let name = gadget-class-name(class);
  let pane = apply(make-test-pane, class, args);
  check-equal(concatenate(name, " default label"),
              gadget-label(pane), "");
  let pane
     = apply(make-test-pane, class, label: "Hello", args);
  check-equal(concatenate(name, " initial label"),
              gadget-label(pane), "Hello");
  gadget-label(pane) := "New label";
  check-equal(concatenate(name, " new label"),
              gadget-label(pane), "New label");
end method test-gadget-label;
||#
(defmethod test-gadget-label ((class class) &rest args)
  (let ((name (gadget-class-name class))
	(pane (apply #'make-test-pane class args)))
    (is (equal? (gadget-label pane) "")
	(concatenate 'string name " default label"))
    (let ((pane (apply #'make-test-pane class :label "Hello" args)))
      (is (equal? (gadget-label pane) "Hello")
	  (concatenate 'string name " initial label"))
      (setf (gadget-label pane) "New label")
      (is (equal? (gadget-label pane) "New label")
	  (concatenate 'string name " new label")))))
#||

/// Gadget text testing

define method test-text-gadget-text
    (class :: subclass(<text-gadget>)) => ()
  let name = gadget-class-name(class);
  let text-field = make-test-pane(class);
  check-equal(concatenate(name, " default text"), gadget-text(text-field), "");
  gadget-text(text-field) := "Hello";
  check-equal(concatenate(name, " changed text"), gadget-text(text-field), "Hello");
  check-equal(concatenate(name, " specified text"),
	      gadget-text(make-test-pane(class, text: "Initial")), "Initial")
end method test-text-gadget-text;
||#
(defmethod test-text-gadget-text (class)
  (assert (INSTANCE? class (find-class '<text-gadget>)))
  (let ((name (gadget-class-name class))
	(text-field (make-test-pane class)))
    (is (equal? (gadget-text text-field) "")
	(concatenate 'string name " default text"))
    (setf (gadget-text text-field) "Hello")
    (is (equal? (gadget-text text-field) "Hello")
	(concatenate 'string name " changed text"))
    (is (equal? (gadget-text (make-test-pane class :text "Initial")) "Initial")
	(concatenate 'string name " specified text"))))


#||

/// border-pane tests

define method expected-named-border-size
    (pane, name, #key width, height, thickness = 1, #all-keys)
 => (size :: <integer>)
  let child = sheet-child(pane);
  let double-thickness = thickness * 2;
  if (child)
    let size-function = expected-size-function(name);
    size-function(child, 
                  width:  width & width - double-thickness,
                  height: height & height - double-thickness)
      + double-thickness
  else
    expected-default-size(name, width: width, height: height)
  end
end method expected-named-border-size;
||#

(defun expected-named-border-size (pane name &key width height thickness &allow-other-keys)
"
Returns a size that includes the border-thickness
"
  (let ((child (sheet-child pane))
	(double-thickness (* (or thickness (border-thickness pane)) 2)))
    (if child
	(let ((size-function (expected-size-function name)))
	  (+ (funcall size-function child
		      :width (and width (- width double-thickness))
		      :height (and height (- height double-thickness)))
	     double-thickness))
	(expected-default-size name :width width :height height))))


#||
define method expected-border-space-allocation
    (pane, #key thickness = 1, width, height, #all-keys)
 => (space-allocation :: false-or(<sequence>))
  if (sheet-child(pane))
    vector(vector(thickness, thickness,
                  width - thickness * 2,
                  height - thickness * 2))
  end
end method expected-border-space-allocation;
||#

(defun expected-border-space-allocation (pane &key thickness width height &allow-other-keys)
"
If :THICKNESS is not supplied, reduces the allocated size for the
child by border-thickness instead.
"
  (when (sheet-child pane)
    (let ((border-thickness (or thickness (border-thickness pane))))
      (vector (vector border-thickness border-thickness
		      (- width (* border-thickness 2))
		      (- height (* border-thickness 2)))))))



#||
define method expected-named-size
    (pane :: <border>, name, #rest args, #key width, height)
 => (size :: <integer>)
  ignore(width, height);
  apply(expected-named-border-size, pane, name, args)
end method expected-named-size;
||#

(defmethod expected-named-size ((pane <border>) name &rest args &key width height)
  (declare (ignore width height))
  (apply #'expected-named-border-size pane name
	 :THICKNESS (BORDER-THICKNESS PANE) args))


#||
define method expected-space-allocation 
    (pane :: <border>, #rest args, #key)
 => (space-allocation :: false-or(<sequence>))
  apply(expected-border-space-allocation, pane, args)
end method expected-space-allocation;
||#

(defmethod expected-space-allocation ((pane <border>) &rest args &key)
  (apply #'expected-border-space-allocation pane args))


#||
define method expected-named-size
    (pane :: <spacing>, name, #rest args, #key width, height)
 => (size :: <integer>)
  ignore(width, height);
  apply(expected-named-border-size, pane, name, args)
end method expected-named-size;
||#

(defmethod expected-named-size ((pane <spacing>) name &rest args &key width height)
  (declare (ignore width height))
  (apply #'expected-named-border-size pane name args))


#||
define method expected-space-allocation 
    (pane :: <spacing>, #rest args, #key)
 => (space-allocation :: false-or(<sequence>))
  apply(expected-border-space-allocation, pane, args)
end method expected-space-allocation;
||#

(defmethod expected-space-allocation ((pane <spacing>) &rest args &key)
  (apply #'expected-border-space-allocation pane args))


#||
define method test-border-pane
    (class :: <class>, name, child, #rest args,
     #key thickness = 1, #all-keys) => ()
  let border-pane = make-test-pane(class, child: child, thickness: thickness);
  let frame = make-test-frame(<test-frame>, layout: border-pane);
  let name = concatenate(name, " ", gadget-class-name(class));
  apply(check-layout-pane-layout,
        border-pane, name,
        thickness: thickness,
        args)
end method test-border-pane;
||#

(defmethod test-border-pane ((class class) name child &rest args
			     &key (thickness 1) &allow-other-keys)
  (let* ((border-pane (make-test-pane class :child child :thickness thickness))
	 (frame (make-test-frame (find-class '<test-frame>) :layout border-pane))
	 (name (concatenate 'string name " " (gadget-class-name class))))
    (declare (ignore frame))
    (apply #'check-layout-pane-layout
	   border-pane name
	   :thickness thickness
	   args)))


#||
define method test-border-pane-layout (class :: <class>) => ()
  test-border-pane(class, "empty", #f, thickness: 0);
  test-border-pane(class, "thickness 0", make-test-pane(<test-push-button-pane>),
		   thickness: 0);
  test-border-pane(class, "thickness 10", make-test-pane(<test-push-button-pane>),
		   thickness: 10);
  test-border-pane(class, "non-fixed thickness 10",
		   make-test-pane(<test-list-box>),
		   thickness: 10);
  test-border-pane(class, "nested",
		   make-test-pane(<border-pane>,
                                  child: make-test-pane(<test-push-button-pane>)));
  test-border-pane(class, "non-fixed nested",
		   make-test-pane(<border-pane>, 
                                  child: make-test-pane(<test-list-box>)));
end method test-border-pane-layout;
||#

(defmethod test-border-pane-layout ((class class))
  (test-border-pane class "empty" nil :thickness 0)
  (test-border-pane class "thickness 0"  (make-test-pane (find-class '<test-push-button-pane>)) :thickness 0)
  (test-border-pane class "thickness 10" (make-test-pane (find-class '<test-push-button-pane>)) :thickness 10)
  (test-border-pane class "non-fixed thickness 10" (make-test-pane (find-class '<test-list-box>)) :thickness 10)
  (test-border-pane class "nested"
		    (make-test-pane (find-class '<border-pane>)
				    :child (make-test-pane (find-class '<test-push-button-pane>))))
  (test-border-pane class "non-fixed nested"
		    (make-test-pane (find-class '<border-pane>)
				    :child (make-test-pane (find-class '<test-list-box>)))))


#||
define duim-gadgets class-test <gadget> ()
  //---*** Fill this in...
end class-test <gadget>;

define duim-gadgets class-test <action-gadget> ()
  //---*** Fill this in...
end class-test <action-gadget>;

define duim-gadgets class-test <value-gadget> ()
  //---*** Fill this in...
end class-test <value-gadget>;

define duim-gadgets class-test <value-range-gadget> ()
  //---*** Fill this in...
end class-test <value-range-gadget>;

define duim-gadgets class-test <collection-gadget> ()
  //---*** Fill this in...
end class-test <collection-gadget>;

define duim-gadgets class-test <text-gadget> ()
  //---*** Fill this in...
end class-test <text-gadget>;

define duim-gadgets class-test <label> ()
  test-gadget-label(<label>);
end class-test <label>;
||#

(test <label>-class-test
  :description "<label> class test"
  (test-gadget-label (find-class '<label>)))


#||
define duim-gadgets class-test <password-field> ()
  test-text-gadget-text(<password-field>);
  test-text-field-values(<password-field>);
end class-test <password-field>;
||#

(test <password-field>-class-test
  :description "<password-field> class test"
  (test-text-gadget-text  (find-class '<password-field>))
  (test-text-field-values (find-class '<password-field>)))


#||
define duim-gadgets class-test <slider> ()
  test-range-pane-values(<slider>);
end class-test <slider>;
||#

(test <slider>-class-test
  :description "<slider> class test"
  (test-range-pane-values (find-class '<slider>)))


#||
define duim-gadgets class-test <text-editor> ()
  test-text-gadget-text(<text-editor>);
  test-text-field-values(<text-editor>);
end class-test <text-editor>;
||#

(test <text-editor>-class-test
  :description "<text-editor> class test"
  (test-text-gadget-text  (find-class '<text-editor>))
  (test-text-field-values (find-class '<text-editor>)))


#||
define duim-gadgets class-test <text-field> ()
  test-text-gadget-text(<text-field>);
  test-text-field-values(<text-field>);
end class-test <text-field>;
||#

(test <text-field>-class-test
  :description "<text-field> class test"
  (test-text-gadget-text  (find-class '<text-field>))
  (test-text-field-values (find-class '<text-field>)))


#||
define duim-gadgets class-test <button-box> ()
  test-collection-gadget-value(<button-box>);
end class-test <button-box>;
||#

(test <button-box>-class-test
  :description "<button-box> class test"
  (test-collection-gadget-value (find-class '<button-box>)))


#||
define duim-gadgets class-test <button> ()
  //---*** Fill this in...
end class-test <button>;

define duim-gadgets class-test <check-box> ()
  test-gadget-box-pane-buttons(<check-box>);
  let cbp = make-collection-gadget(<check-box>, selection: #(0, 2));
  verify-gadget-box-pane-button-selection(cbp);
  let button1 = make-test-pane(<check-button>, id: #"one");
  let button2 = make-test-pane(<check-button>, id: #"two");
  let button3 = make-test-pane(<check-button>, id: #"three");
  let sub-layout
    = make-test-pane(<row-layout>, children: vector(button2, button3));
  let layout
    = make-test-pane(<column-layout>, children: vector(button1, sub-layout));
  let cbp = make-test-pane(<check-box>, child: layout);
  check-equal("arbitrary layout <check-box> items",
	      gadget-items(cbp),
	      #[#"one", #"two", #"three"]);
  check-equal("arbitrary layout <check-box> initial value",
              gadget-value(cbp),
	      #[]);
  check-equal("arbitrary layout <check-box> set value",
	      begin
		gadget-value(cbp) := #[#"two", #"three"];
		gadget-value(cbp)
	      end,
	      #[#"two", #"three"]);
end class-test <check-box>;
||#

(test <check-box>-class-test
  :description "<check-box> class test"
  (test-gadget-box-pane-buttons (find-class '<check-box>))
  (let ((cbp (make-collection-gadget (find-class '<check-box>) :selection #(0 2))))
    (verify-gadget-box-pane-button-selection cbp)
    (let* ((button1 (make-test-pane (find-class '<check-button>) :id :one))
	   (button2 (make-test-pane (find-class '<check-button>) :id :two))
	   (button3 (make-test-pane (find-class '<check-button>) :id :three))
	   (sub-layout (make-test-pane (find-class '<row-layout>) :children (vector button2 button3)))
	   (layout (make-test-pane (find-class '<column-layout>) :children (vector button1 sub-layout)))
	   (cbp (make-test-pane (find-class '<check-box>) :child layout)))
      (is (equal? (gadget-items cbp) #(:one :two :three))
	  "arbitrary layout <check-box> items, ~a should be #(:one :two :three)"
	  (gadget-items cbp))
      (is (equal? (gadget-value cbp) #())
	  "arbitrary layout <check-box> initial value, ~a should be #()"
	  (gadget-value cbp))
      (is (equal? (progn
		    (setf (gadget-value cbp) #(:two :three))
		    (gadget-value cbp))
		  #(:two :three))
	  "arbitrary layout <check-box> set value"))))


#||
define duim-gadgets class-test <check-button> ()
  test-gadget-label(<check-button>);
  test-button-values(<check-button>);
end class-test <check-button>;
||#

(test <check-button>-class-test
  :description "<check-button> class test"
  (test-gadget-label  (find-class '<check-button>))
  (test-button-values (find-class '<check-button>)))


#||
define duim-gadgets class-test <check-menu-button> ()
  test-gadget-label(<check-menu-button>);
  test-button-values(<check-menu-button>);
end class-test <check-menu-button>;
||#

(test <check-menu-button>-class-test
  :description "<check-menu-button> class test"
  (test-gadget-label  (find-class '<check-menu-button>))
  (test-button-values (find-class '<check-menu-button>)))


#||
define duim-gadgets class-test <check-menu-box> ()
  test-gadget-box-pane-buttons(<check-menu-box>);
  let cbp = make-collection-gadget(<check-menu-box>, selection: #(0, 2));
  verify-gadget-box-pane-button-selection(cbp);
end class-test <check-menu-box>;
||#

(test <check-menu-box>-class-test
  :description "<check-menu-box> class test"
  (test-gadget-box-pane-buttons (find-class '<check-menu-box>))
  (let ((cbp (make-collection-gadget (find-class '<check-menu-box>) :selection #(0 2))))
    (verify-gadget-box-pane-button-selection cbp)))


#||
define duim-gadgets class-test <list-box> ()
  test-collection-gadget-value(<list-box>);
end class-test <list-box>;
||#

(test <list-box>-class-test
  :description "<list-box> class test"
  (test-collection-gadget-value (find-class '<list-box>)))


#||
define duim-gadgets class-test <menu-bar> ()
  //---*** Fill this in...
end class-test <menu-bar>;

define duim-gadgets class-test <menu-button> ()
  //---*** Fill this in...
end class-test <menu-button>;

define duim-gadgets class-test <menu-box> ()
  test-collection-gadget-value(<menu-box>);
end class-test <menu-box>;
||#

(test <menu-box>-class-test
  :description "<menu-box> class test"
  (test-collection-gadget-value (find-class '<menu-box>)))


#||
define duim-gadgets class-test <menu> ()
  test-gadget-label(<menu>);
  test-no-value-gadget-values(<menu>);
end class-test <menu>;
||#

(test <menu>-class-test
  :description "<menu> class test"
  (test-gadget-label (find-class '<menu>))
  (test-no-value-gadget-values (find-class '<menu>)))


#||
define duim-gadgets class-test <option-box> ()
  test-single-selection-collection-gadget-values(<option-box>);
end class-test <option-box>;
||#

(test <option-box>-class-test
  :description "<option-box> class test"
  (test-single-selection-collection-gadget-values (find-class '<option-box>)))


#||
define duim-gadgets class-test <combo-box> ()
  test-text-gadget-text(<combo-box>);
  test-text-field-values(<combo-box>);
end class-test <combo-box>;
||#

(test <combo-box>-class-test
  :description "<combo-box> class test"
  (test-text-gadget-text  (find-class '<combo-box>))
  (test-text-field-values (find-class '<combo-box>)))


#||
define duim-gadgets class-test <push-box> ()
  test-gadget-box-pane-buttons(<push-box>);
end class-test <push-box>;
||#

(test <push-box>-class-test
  :description "<push-box> class test"
  (test-gadget-box-pane-buttons (find-class '<push-box>)))


#||
define duim-gadgets class-test <push-button> ()
  test-gadget-label(<push-button>);
  test-button-values(<push-button>);
end class-test <push-button>;
||#

(test <push-button>-class-test
  :description "<push-button> class test"
  (test-gadget-label  (find-class '<push-button>))
  (test-button-values (find-class '<push-button>)))


#||
define duim-gadgets class-test <push-menu-button> ()
  test-gadget-label(<push-menu-button>);
  test-button-values(<push-menu-button>);
end class-test <push-menu-button>;
||#

(test <push-menu-button>-class-test
  :description "<push-menu-button> class test"
  (test-gadget-label  (find-class '<push-menu-button>))
  (test-button-values (find-class '<push-menu-button>)))


#||
define duim-gadgets class-test <push-menu-box> ()
  test-gadget-box-pane-buttons(<push-menu-box>);
end class-test <push-menu-box>;
||#

(test <push-menu-box>-class-test
  :description "<push-menu-box> class test"
  (test-gadget-box-pane-buttons (find-class '<push-menu-box>)))


#||
define duim-gadgets class-test <radio-box> ()
  let rbp = make-collection-gadget(<radio-box>);
  verify-gadget-box-pane-button-selection(rbp);
  test-gadget-box-pane-buttons(<radio-box>);
  let button1 = make-test-pane(<radio-button>, id: #"one");
  let button2 = make-test-pane(<radio-button>, id: #"two");
  let button3 = make-test-pane(<radio-button>, id: #"three");
  let sub-layout
    = make-test-pane(<row-layout>, children: vector(button2, button3));
  let layout
    = make-test-pane(<column-layout>, children: vector(button1, sub-layout));
  let rbp = make-test-pane(<radio-box>, child: layout);
  check-equal("arbitrary layout <radio-box> items",
	      gadget-items(rbp),
	      #[#"one", #"two", #"three"]);
  check-equal("arbitrary layout <radio-box> initial value",
              gadget-value(rbp),
	      #"one");
  check-equal("arbitrary layout <radio-box> set value",
	      begin
		gadget-value(rbp) := #"two";
		gadget-value(rbp)
	      end,
	      #"two");
end class-test <radio-box>;
||#

(test <radio-box>-class-test
  :description "<radio-box> class test"
  (let ((rbp (make-collection-gadget (find-class '<radio-box>))))
    (verify-gadget-box-pane-button-selection rbp)
    (test-gadget-box-pane-buttons (find-class '<radio-box>))
    (let* ((button1 (make-test-pane (find-class '<radio-button>) :id :one))
	   (button2 (make-test-pane (find-class '<radio-button>) :id :two))
	   (button3 (make-test-pane (find-class '<radio-button>) :id :three))
	   (sub-layout (make-test-pane (find-class '<row-layout>) :children (vector button2 button3)))
	   (layout (make-test-pane (find-class '<column-layout>) :children (vector button1 sub-layout)))
	   (rbp (make-test-pane (find-class '<radio-box>) :child layout)))
      (is (equal? (gadget-items rbp) #(:one :two :three))
	  "arbitrary layout <radio-box> items, ~a = #(:one :two :three)"
	  (gadget-items rbp))
      (is (equal? (gadget-value rbp) :one)
	  "arbitrary layout <radio-box> initial value, ~a = :one"
	  (gadget-value rbp))
      (is (equal? (progn
		    (setf (gadget-value rbp) :two)
		    (gadget-value rbp))
		  :two)
	  "arbitrary layout <radio-box> set value"))))


#||
define duim-gadgets class-test <radio-button> ()
  test-gadget-label(<radio-button>);
  test-button-values(<radio-button>);
end class-test <radio-button>;
||#

(test <radio-button>-class-test
  :description "<radio-button> class test"
  (test-gadget-label  (find-class '<radio-button>))
  (test-button-values (find-class '<radio-button>)))


#||
define duim-gadgets class-test <radio-menu-button> ()
  test-gadget-label(<radio-menu-button>);
  test-button-values(<radio-menu-button>);
end class-test <radio-menu-button>;
||#

(test <radio-menu-button>-class-test
  :description "<radio-menu-button> class test"
  (test-gadget-label  (find-class '<radio-menu-button>))
  (test-button-values (find-class '<radio-menu-button>)))


#||
define duim-gadgets class-test <radio-menu-box> ()
  let rbp = make-collection-gadget(<radio-menu-box>);
  verify-gadget-box-pane-button-selection(rbp);
  test-gadget-box-pane-buttons(<radio-menu-box>);
end class-test <radio-menu-box>;
||#

(test <radio-menu-box>-class-test
  :description "<radio-menu-box> class test"
  (let ((rbp (make-collection-gadget (find-class '<radio-menu-box>))))
    (verify-gadget-box-pane-button-selection rbp)
    (test-gadget-box-pane-buttons (find-class '<radio-menu-box>))))


#||
define duim-gadgets class-test <spin-box> ()
  test-single-selection-collection-gadget-values(<spin-box>);
end class-test <spin-box>;
||#

(test <spin-box>-class-test
  :description "<spin-box> class test"
  (test-single-selection-collection-gadget-values (find-class '<spin-box>)))


#||
define duim-gadgets class-test <status-bar> ()
  test-gadget-label(<status-bar>);
  test-range-pane-values(<status-bar>, 
                         make-function: method (class, #rest args)
                                          apply(make-test-pane, class,
                                                progress-bar?: #t,
                                                args)
                                        end,
                         default: #f);
end class-test <status-bar>;
||#

(test <status-bar>-class-test
  :description "<status-bar> class test"
  (test-gadget-label (find-class '<status-bar>))
  (test-range-pane-values (find-class '<status-bar>)
			  :make-function #'(lambda (class &rest args)
					     (apply #'make-test-pane class
						    :progress-bar? t
						    args))
			  :default nil))


#||
define duim-gadgets class-test <tool-bar> ()
  test-no-value-gadget-values(<tool-bar>);
end class-test <tool-bar>;
||#

(test <tool-bar>-class-test
  :description "<tool-bar> class test"
  (test-no-value-gadget-values (find-class '<tool-bar>)))


#||
define duim-gadgets class-test <scroll-bar> ()
  test-slug-gadget-values(<scroll-bar>);
end class-test <scroll-bar>;
||#

(test <scroll-bar>-class-test
  :description "<scroll-bar> class test"
  (test-slug-gadget-values (find-class '<scroll-bar>)))


#||
define duim-gadgets class-test <border> ()
  test-border-pane-layout(<border>);
end class-test <border>;
||#

(test <border>-class-test
  :description "<border> class test"
  (test-border-pane-layout (find-class '<border>)))


#||
define duim-gadgets class-test <group-box> ()
  //---*** We don't really want to test its layout as such, since we
  //---*** don't want to assume a particular look. More, we want to
  //---*** make sure that the child is okay.
  // test-border-pane-layout(<group-box>);
end class-test <group-box>;

define duim-gadgets class-test <separator> ()
  test-no-value-gadget-values(<separator>);
end class-test <separator>;
||#

(test <separator>-class-test
  :description "<separator> class test"
  (test-no-value-gadget-values (find-class '<separator>)))


#||
define duim-gadgets class-test <spacing> ()
  test-border-pane-layout(<spacing>);
end class-test <spacing>;
||#

(test <spacing>-class-test
  :description "<spacing> class test"
  (test-border-pane-layout (find-class '<spacing>)))


#||
define duim-gadgets class-test <splitter> ()
  //---*** Fill this in...
end class-test <splitter>;

define duim-gadgets class-test <tab-control> ()
  let button-1 = make-test-pane(<button>, label: "One");
  let button-2 = make-test-pane(<button>, label: "Two");
  let tab-control
    = make-test-pane(<tab-control>, pages: vector(button-1, button-2));
  let frame = make-test-frame(<test-frame>, layout: tab-control);
  ignore(frame);
  check-equal("Tab control default visible child",
	      tab-control-current-page(tab-control),
	      button-1);
  check-equal("Tab control default value",
	      gadget-value(tab-control), "One");
  tab-control-current-page(tab-control) := button-2;
  check-equal("Tab control new visible child",
	      tab-control-current-page(tab-control),
	      button-2);
  gadget-value(tab-control) := gadget-label(button-1);
  check-equal("Tab control new value",
	      gadget-value(tab-control), gadget-label(button-1));
  let button-3 = make-test-pane(<button>, label: "Three");
  tab-control-pages(tab-control) := vector(button-1, button-2, button-3);
  check-equal("Tab control keeps visible child after contents change",
	      tab-control-current-page(tab-control),
	      button-1);
  tab-control-current-page(tab-control) := button-3;
  check-equal("Tab control additional visible child",
	      tab-control-current-page(tab-control),
	      button-3);
  tab-control-pages(tab-control) := #[];
  check-equal("Empty tab control",
	      tab-control-pages(tab-control), #[]);
  check-equal("Empty tab control has no visible child",
	      tab-control-current-page(tab-control), #f);
end class-test <tab-control>;
||#

(test <tab-control>-class-test
  :description "<tab-control> class test"
  (let* ((button-1 (make-test-pane (find-class '<button>) :label "One"))
	 (button-2 (make-test-pane (find-class '<button>) :label "Two"))
	 (tab-control (make-test-pane (find-class '<tab-control>) :pages (vector button-1 button-2)))
	 (frame (make-test-frame (find-class '<test-frame>) :layout tab-control)))
    (declare (ignore frame))
    (is (equal? (tab-control-current-page tab-control) button-1)
	"Tab control default visible child")
    (is (equal? (gadget-value tab-control) "One")
	"Tab control default value")
    (setf (tab-control-current-page tab-control) button-2)
    (is (equal? (tab-control-current-page tab-control) button-2)
	"Tab control new visible child")
    (setf (gadget-value tab-control) (gadget-label button-1))
    (is (equal? (gadget-value tab-control) (gadget-label button-1))
	"Tab control new value")
    (let ((button-3 (make-test-pane (find-class '<button>) :label "Three")))
      (setf (tab-control-pages tab-control) (vector button-1 button-2 button-3))
      (is (equal? (tab-control-current-page tab-control) button-1)
	  "Tab control keeps visible child after contents change")
      (setf (tab-control-current-page tab-control) button-3)
      (is (equal? (tab-control-current-page tab-control) button-3)
	  "Tab control additional visible child")
      (setf (tab-control-pages tab-control) #())
      (is (equal? (tab-control-pages tab-control) #())
	  "Empty tab control")
      (is (equal? (tab-control-current-page tab-control) nil)
	  "Empty tab control has no visible child"))))


#||
define duim-gadget class-test <page> ()
  //---*** Fill this in...
end class-test <page>;

define duim-gadget class-test <tab-control-page> ()
  //---*** Fill this in...
end class-test <tab-control-page>;

define duim-gadgets class-test <list-control> ()
  test-collection-gadget-value(<list-control>);
end class-test <list-control>;
||#

(test <list-control>-class-test
  :description "<list-control> class test"
  (test-collection-gadget-value (find-class '<list-control>)))


#||
define duim-gadgets class-test <list-item> ()
  //---*** Fill this in...
end class-test <list-item>;

define duim-gadgets class-test <tree-control> ()
  test-tree-control-values();
end class-test <tree-control>;
||#

(test <tree-control>-class-test
  :description "<tree-control> class test"
  (test-tree-control-values))


#||
define duim-gadgets class-test <tree-node> ()
  //---*** Fill this in...
end class-test <tree-node>;

define duim-gadgets class-test <table-control> ()
  test-collection-gadget-value(<table-control>);
  test-table-control-values();
end class-test <table-control>;
||#

(test <table-control>-class-test
  :description "<table-control> class test"
  (test-collection-gadget-value (find-class '<table-control>))
  (test-table-control-values))


#||
define duim-gadgets class-test <table-item> ()
  //---*** Fill this in...
end class-test <table-item>;

define duim-gadgets class-test <progress-bar> ()
  test-range-pane-values(<progress-bar>, default: #f);
end class-test <progress-bar>;
||#

(test <progress-bar>-class-test
  :description "<progress-bar> class test"
  (test-range-pane-values (find-class '<progress-bar>)))

#||

/// Define gadgets test suite

define suite duim-gadgets-suite ()
  test parents-test;
end suite duim-gadgets-suite;
||#

(defun test-gadgets ()
  (run! 'duim-gadgets))

