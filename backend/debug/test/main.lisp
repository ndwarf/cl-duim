
(in-package #:debug-backend-test)

(def-suite all-tests
    :description "All debug-backend tests")

(in-suite all-tests)

(defun test-debug-backend ()
  (run! 'all-tests))
