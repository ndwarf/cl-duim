
(in-package #:debug-backend-test)

(def-suite port-suite
    :description "DUIM port tests"
    :in all-tests)

(in-suite port-suite)

(defparameter *test-port* nil)


(defun ensure-test-port ()
  (setf *test-port* (duim-sheets:find-port :server-path (list :debug)))
  (clear-events *test-port*))


;; create and destroy

(test class-for-make-port
  (is (eql (duim-sheets-internals:class-for-make-port :debug) (find-class '<debug-port>))))


(test can-find-port
  (ensure-test-port)
  (is (not (null *test-port*)))
  (is (empty? (debug-port-events *test-port*))))


(test port-matches-server-path
  (ensure-test-port)
  (is-true (duim-sheets-internals:port-matches-server-path? *test-port* (list :debug))))


(test can-destroy-port
  (ensure-test-port)
  (duim-sheets:destroy-port *test-port*)
  (is (null (duim-sheets-internals:port-displays *test-port*)))
  (is-false (duim-sheets-internals::port-alive? *test-port*))
  (is (empty? (debug-port-events *test-port*))))


(test restart-port
  (ensure-test-port)
  (let ((val (duim-sheets:restart-port *test-port*)))
    (is (null val)
	"-- expected NIL got ~A" val) 
    (is (empty? (debug-port-events *test-port*))
	"-- expected no events, got ~A" (debug-port-events *test-port*))))


;;; <<PORT-PROTOCOL>>

(test process-next-event
  (ensure-test-port)
  (is-false (duim-sheets-internals:process-next-event *test-port*)))


(test port-modifier-state
  (ensure-test-port)
  (let ((state (duim-sheets-internals:port-modifier-state *test-port*)))
    (is-true (zerop state) "-- expected 0 but was ~D" state)))


(test port-pointer
  (ensure-test-port)
  (let ((pointer (duim-sheets-internals:port-pointer *test-port*))
	(standard-pointer (find-class 'duim-sheets-internals:<standard-pointer>)))
    (is (eql (class-of pointer) standard-pointer)
	"-- expected ~A but was ~A" standard-pointer (class-of pointer))))


(test port-pointer-setter
  (ensure-test-port)
  (let ((test-pointer (make-instance 'duim-sheets-internals:<pointer>)))
    (setf (duim-sheets-internals:port-pointer *test-port*) test-pointer)
    (let ((pointer (duim-sheets-internals:port-pointer *test-port*)))
      (is (instance? pointer (find-class 'duim-sheets-internals:<standard-pointer>))
	  "-- expected ~A but was ~A" test-pointer pointer)
      (is (eql pointer test-pointer)
	  "-- expected ~A but was ~A" test-pointer pointer))))


(test port-input-focus
  (ensure-test-port)
  (let ((focus (duim-sheets-internals:port-input-focus *test-port*)))
    (is (null focus))))


;; EVENT GENERATOR
;; todo: should this also cause <frame-input-focus-changed-event> events?
;;       see frames;frames.lisp (setf frame-input-focus).
;; This is called from ports.lisp when (setf port-input-focus) is called, IF
;; the new input focus has an associated frame. So we should see those once
;; everything is set up properly in the debug port.
;; When (setf frame-input-focus) is called directly it updates the port focus
;; too if necessary. So it *should* all hang together...
;; -- ADD TEST TO CHECK CURRENT FRAME HAS ITS FOCUS UPDATED WHEN PORT FOCUS
;; -- IS UPDATED
(test port-input-focus-setter
  (ensure-test-port)
  (let ((test-focus1 (duim-sheets-internals:make-pane 'duim-layouts-internals:<drawing-pane>))
	(test-focus2 (duim-sheets-internals:make-pane 'duim-layouts-internals:<drawing-pane>)))
    (setf (duim-sheets-internals:port-input-focus *test-port*) test-focus1)
    (let ((events (debug-port-events *test-port*)))
      (is (= (length events) 1) "-- expected single event, got ~A" events)
      (is (instance? (first events) (find-class 'duim-sheets:<input-focus-in-event>)))
      (is (eql (duim-sheets:event-sheet (first events)) test-focus1)))
    (let ((focus (duim-sheets-internals:port-input-focus *test-port*)))
      (setf (duim-sheets-internals:port-input-focus *test-port*) test-focus2)
      (is (instance? focus (find-class 'duim-layouts-internals::<concrete-drawing-pane>))
	  "-- expected ~A got ~A" test-focus1 focus)
      (is (eql focus test-focus1)
	  "-- expected ~A got ~A" test-focus1 focus)
      (let ((events (debug-port-events *test-port*)))
	;; in the debug port events are held in last-received-first order
	(is (= (length events) 3)
	    "-- expected focus-in event then focus-out event, got ~A" events)
	(is (instance? (first events) (find-class 'duim-sheets:<input-focus-in-event>)))
	(is (eql (duim-sheets:event-sheet (first events)) test-focus2))
	(is (instance? (second events) (find-class 'duim-sheets:<input-focus-out-event>)))
	(is (eql (duim-sheets:event-sheet (second events)) test-focus1))
	(is (instance? (third events) (find-class 'duim-sheets:<input-focus-in-event>)))
	(is (eql (duim-sheets:event-sheet (third events)) test-focus1)))))
  (setf (duim-sheets-internals:port-input-focus *test-port*) nil))


(test port-focus-policy
  (ensure-test-port)
  (let ((policy (duim-sheets-internals:port-focus-policy *test-port*)))
    (is (eql policy :SHEET-UNDER-POINTER)
 	"-- expected :SHEET-UNDER-POINTER, got ~A" policy)))


(test port-frame-managers
  (ensure-test-port)
  (let ((managers (duim-sheets-internals:port-frame-managers *test-port*)))
    (is (instance? (first managers)
		   (find-class 'duim-sheets-internals:<portable-frame-manager>))
	"-- expected <PORTABLE-FRAME-MANAGER> got ~A" (first managers))))


(test port-default-frame-manager
  (ensure-test-port)
  (let ((manager (duim-sheets-internals:port-default-frame-manager *test-port*)))
    (is (instance? manager
		   (find-class 'duim-sheets-internals:<portable-frame-manager>))
	"-- expected <PORTABLE-FRAME-MANAGER> got ~A" manager)))


(test port-default-palette
  (ensure-test-port)
  (let ((palette (duim-sheets-internals:port-default-palette *test-port*)))
    (is (null palette)
	"-- expected NIL, got ~A" palette)))


(test port-default-palette-setter
  (ensure-test-port)
  (let ((original (duim-sheets-internals:port-default-palette *test-port*))
	(palette (duim-dcs:make-palette *test-port*)))
    (setf (duim-sheets-internals:port-default-palette *test-port*) palette)
    (is (eql palette (duim-sheets-internals:port-default-palette *test-port*))
	"-- expected ~A got ~A" palette
	(duim-sheets-internals:port-default-palette *test-port*))
    (is (empty? (debug-port-events *test-port*))
	"-- expected no events, got ~A" (debug-port-events *test-port*))
    (setf (duim-sheets-internals:port-default-palette *test-port*) original)))


(test port-name
  (ensure-test-port)
  (let ((name (duim-sheets-internals:port-name *test-port*)))
    (is (string= name "DUIM Debug Port")
	"-- expected \"DUIM Debug Port\", got ~A" name)))


(test port-type
  (ensure-test-port)
  (let ((type (duim-sheets-internals:port-type *test-port*)))
    (is (eql type :DEBUG)
	"-- expected :DEBUG, got ~A" type)))


(test port-server-path
  (ensure-test-port)
  (let ((server-path (duim-sheets-internals:port-server-path *test-port*)))
    (is (equalp server-path (list :debug))
	"-- expected (:DEBUG), got ~A" server-path)))


(test port-properties
  (ensure-test-port)
  (let ((properties (duim-sheets-internals:port-properties *test-port*)))
    (is (vectorp properties) "-- expected vector, got ~A" properties)
    (is (empty? properties) "-- expected empty vector, got ~A" properties)))


(test port-properties-setter
  (ensure-test-port)
  ;; FIXME PORT-PROPERTIES should only accepts property lists...
  (signals type-error
    (setf (duim-sheets-internals:port-properties *test-port*) (list :PROPERTY "XXX")))
  (setf (duim-sheets-internals:port-properties *test-port*) (vector :PROPERTY "XXX"))
  (let ((properties (duim-sheets-internals:port-properties *test-port*)))
    (is (equalp properties (vector :PROPERTY "XXX"))
	"-- expected #(:PROPERTY \"XXX\"), got ~A" properties))
  ;; reset to empty properties for tests that follow this one
  (setf (duim-sheets-internals:port-properties *test-port*) (vector)))


(test port-displays
  (ensure-test-port)
  (let ((displays (duim-sheets-internals:port-displays *test-port*)))
    (is (null displays)
	"-- expected NIL, got ~A" displays)))


;; fixme: pretty sure this should only accept lists containing
;;        <display> subtypes...
(test port-displays-setter
  (ensure-test-port)
  (setf (duim-sheets-internals:port-displays *test-port*) (list :DEBUG-DISPLAY))
  (let ((displays (duim-sheets-internals:port-displays *test-port*)))
    (is (equalp displays (list :DEBUG-DISPLAY))
	"-- expected (:DEBUG-DISPLAY), got ~A" displays))
  ;; reset to empty displays for tests that follow this one
  (setf (duim-sheets-internals:port-displays *test-port*) nil)
  (is (null (duim-sheets-internals:port-displays *test-port*))))


(test port-font-mapping-table
  (ensure-test-port)
  (let ((table (duim-sheets-internals:port-font-mapping-table *test-port*)))
    (is-true (hash-table-p table) "-- expected hashtable, got ~A" table)))


(test port-font-mapping-cache
  (ensure-test-port)
  (let ((cache (duim-sheets-internals:port-font-mapping-cache *test-port*)))
    (is (equalp cache (list nil)) "-- expected (NIL), got ~A" cache)))


(test port-undefined-text-style
  (ensure-test-port)
  (let ((comparison duim-dcs-internals:*undefined-text-style*)
	(text-style (duim-sheets-internals:port-undefined-text-style *test-port*)))
    (is (equal? comparison text-style) "-- expected ~A, got ~A" comparison text-style)))


(test port-undefined-text-style-setter
  (ensure-test-port)
  (let ((original (duim-sheets-internals:port-undefined-text-style *test-port*))
	(text-style (duim-dcs:make-text-style :sans-serif "courier" :normal :italic :normal)))
    (setf (duim-sheets-internals:port-undefined-text-style *test-port*) text-style)
    (is (eql text-style (duim-sheets-internals:port-undefined-text-style *test-port*))
	"-- expected ~A got ~A" text-style (duim-sheets-internals:port-undefined-text-style *test-port*))
    (is (empty? (debug-port-events *test-port*))
	"-- expected no events, got ~A" (debug-port-events *test-port*))
    (setf (duim-sheets-internals:port-undefined-text-style *test-port*) original)))


(test port-default-foreground
  (ensure-test-port)
  (let* ((sheet (make-instance 'duim-sheets-internals:<basic-sheet>))
	 (default-foreground (duim-sheets-internals:port-default-foreground *test-port* sheet)))
    (is (null default-foreground)
	"-- expected NIL got ~A" default-foreground)
    (is (empty? (debug-port-events *test-port*))
	"-- expected no events, got ~A" (debug-port-events *test-port*))))


(test port-default-background
  (ensure-test-port)
  (let* ((sheet (make-instance 'duim-sheets-internals:<basic-sheet>))
	 (default-background (duim-sheets-internals:port-default-background *test-port* sheet)))
    (is (null default-background)
	"-- expected NIL got ~A" default-background)
    (is (empty? (debug-port-events *test-port*))
	"-- expected no events, got ~A" (debug-port-events *test-port*))))


(test port-default-text-style
  (ensure-test-port)
  (let* ((sheet (make-instance 'duim-sheets-internals:<basic-sheet>))
	 (default-text-style (duim-sheets-internals:port-default-text-style *test-port* sheet)))
    (is (null default-text-style)
	"-- expected NIL got ~A" default-text-style)
    (is (empty? (debug-port-events *test-port*))
	"-- expected no events, got ~A" (debug-port-events *test-port*))))


;; fixme: expect this to end up with default foreground UNLESS port-default-foreground
;; returns some value.
(test get-default-foreground
  (ensure-test-port)
  (let* ((sheet (make-instance 'duim-sheets-internals:<basic-sheet>))
	 (default-foreground (duim-sheets-internals:get-default-foreground *test-port* sheet)))
    (is (eql duim-dcs-internals:*default-foreground* default-foreground)
	"-- expected *DEFAULT-FOREGROUND* got ~A" default-foreground)
    (is (empty? (debug-port-events *test-port*))
	"-- expected no events, got ~A" (debug-port-events *test-port*))))


;; fixme: expect this to end up with default foreground UNLESS port-default-background
;; returns some value.
(test get-default-background
  (ensure-test-port)
  (let* ((sheet (make-instance 'duim-sheets-internals:<basic-sheet>))
	 (default-background (duim-sheets-internals:get-default-background *test-port* sheet)))
    (is (eql duim-dcs-internals:*default-background* default-background)
	"-- expected *DEFAULT-BACKGROUND* got ~A" default-background)
    (is (empty? (debug-port-events *test-port*))
	"-- expected no events, got ~A" (debug-port-events *test-port*))))


;; fixme: expect this to end up with default foreground UNLESS port-default-text-style
;; returns some value.
(test get-default-text-style
  (ensure-test-port)
  (let* ((sheet (make-instance 'duim-sheets-internals:<basic-sheet>))
	 (default-text-style (duim-sheets-internals:get-default-text-style *test-port* sheet)))
    (is (eql duim-dcs-internals:*default-text-style* default-text-style)
	"-- expected *DEFAULT-TEXT-STYLE* got ~A" default-text-style)
    (is (empty? (debug-port-events *test-port*))
	"-- expected no events, got ~A" (debug-port-events *test-port*))))

