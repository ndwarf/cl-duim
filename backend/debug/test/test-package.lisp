
(defpackage #:debug-backend-test
  (:use #:cl
	#:debug-backend
	#:duim-utilities
	#:fiveam)
  (:export #:test-debug-backend
	   #:run!
	   #:all-tests))
