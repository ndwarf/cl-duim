;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DEBUG-BACKEND -*-
(in-package #:debug-backend)


(defclass <debug-palette> (<basic-palette>)
  ())


(defmethod make-palette ((_port <debug-port>)
			 &key
			   color? dynamic? colormap
			   &allow-other-keys)
  (declare (ignorable color? dynamic? colormap))
  (make-instance '<debug-palette>))
