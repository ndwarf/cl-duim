;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

(defpackage #:debug-backend
  (:use #:common-lisp
	#:duim-utilities
	;; fixme: surely both these are not necessary...
	#:duim-internals
	#:duim-sheets-internals)
  (:export
   #:<debug-port>
   #:debug-port-events
   #:clear-events

   #:<debug-palette>
   ))
