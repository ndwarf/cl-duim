
(in-package :debug-backend)

;;; See SHEETS;PORTS.LISP and the <<PORT-PROTOCOL>>.

(defclass <debug-port> (<basic-port>)
  ((received-events :accessor debug-port-events :initform nil)))


;;; Port must be registered. Not sure this is documented anywhere.

;;; TODO: do something with server-path?
;;; TODO: event-processor-type should be an attribute of the port?
;;;       not sure it should be passed as initargs.

;; Beware: making this :DEFAULT? T and loading it after GTK is
;; loaded means this, instead of GTK, assumes the role of default port.
(register-port-class :debug (find-class '<debug-port>)) ;; :default? t)

(defmethod initialize-instance :after ((_port <debug-port>)
				      &key
					server-path
					&allow-other-keys)
  (declare (ignorable server-path))
  (format t "Constructing ~A" (port-name _port)))


(defmethod class-for-make-port ((type (eql :debug)) &rest initargs &key)
  (values (find-class '<debug-port>)
	  (append initargs (list :event-processor-type :n))))


(defmethod port-type ((_port <debug-port>))
  :debug)


(defmethod port-name ((_port <debug-port>))
  "DUIM Debug Port")


(defmethod destroy-port :after ((_port <debug-port>))
  (format t "Destroying ~A" (port-name _port)))

;;; use FIND-PORT

;;; test the events... make sure they're received somewhere and
;;; look right - document the flow.

;;; handle a single 'raw' event at the backend. Return timed-out?

(defmethod process-next-event ((_port <debug-port>) &key timeout)
  (declare (ignorable timeout))
  nil)


(defmethod distribute-event ((_port <debug-port>) event)
  (push event (debug-port-events _port)))


(defmethod clear-events ((_port <debug-port>))
  (setf (debug-port-events _port) nil))


