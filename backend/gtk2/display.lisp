;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GTK2-DUIM -*-
(in-package #:gtk2-duim)

#||
Module:       gtk-duim
Synopsis:     GTK display implementation
Author:       Andy Armstrong, Scott McKay
Copyright:    Original Code is Copyright (c) 1995-2004 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND

/// GTK displays, i.e., the screen

define constant $points-per-mm = 72.0 / 25.4;
||#

(defconstant +points-per-mm+ (/ 72.0 25.4))


#||
define sealed class <display-mirror> (<gtk-mirror>)
end class <display-mirror>;
||#

;;; FIXME: I'M NOT SURE IF THIS IS A GDKSCREEN, OR A GDKDISPLAY!
(defclass <gtk-display-mirror> (<gtk-mirror>) ())


#||
// Mirror the display, set its region, and set its characteristics
define sealed method initialize-display 
    (_port :: <gtk-port>, _display :: <standard-display>) => ()
  let mirror
    = make(<display-mirror>,
	   sheet:  _display);
  let mm-width     = gdk-screen-width-mm();
  let mm-height    = gdk-screen-height-mm();
  let pixel-width  = gdk-screen-width();
  let pixel-height = gdk-screen-height();
  display-pixel-width(_display)  := pixel-width;
  display-pixel-height(_display) := pixel-height;
  display-mm-width(_display)     := mm-width;
  display-mm-height(_display)    := mm-height;
  display-pixels-per-point(_display)
    := sqrt(  (pixel-width  / (mm-width  * $points-per-mm))
	    * (pixel-height / (mm-height * $points-per-mm)));
  sheet-region(_display)
    := set-box-edges(sheet-region(_display),
		     0, 0, pixel-width, pixel-height);
  sheet-direct-mirror(_display) := mirror;
  /*---*** Not doing palettes yet...
  let palette  = port-default-palette(_port);
  let drawable = xt/XtWindow(top-shell);
  palette.%default-drawable := drawable;
  palette.%gcontext         := x/XCreateGC(_port.%display, drawable)
  */
end method initialize-display;
||#

(defmethod initialize-display ((_port <gtk-port>)
			       (_display <standard-display>))
  (let ((mirror (make-instance '<gtk-display-mirror> :sheet _display))
	(mm-width (cffi/gdk-screen-width-mm))
	(mm-height (cffi/gdk-screen-height-mm))
	(pixel-width (cffi/gdk-screen-width))
	(pixel-height (cffi/gdk-screen-height)))
    (setf (display-pixel-width _display) pixel-width
	  (display-pixel-height _display) pixel-height
	  (display-mm-width _display) mm-width
	  (display-mm-height _display) mm-height)
    (setf (display-pixels-per-point _display)
	  (sqrt (* (/ pixel-width (* mm-width +points-per-mm+))
		   (/ pixel-height (* mm-height +points-per-mm+)))))
    (setf (sheet-region _display)
	  (set-box-edges (sheet-region _display)
			 0 0 pixel-width pixel-height))
    (setf (sheet-direct-mirror _display) mirror)
    #||---*** Not doing palettes yet
    (let ((palette (port-default-palette _port))
    (drawable (xt/XtWindow top-shell)))
    (setf (%default-drawable palette) drawable
    (%gcontext palette) (x/XCreateGC (%display _port) drawable)))
    ||#
    ))

#||
define method set-mirror-parent
    (child :: <top-level-mirror>, parent :: <display-mirror>)
 => ()
  ignoring("set-mirror-parent for <top-level-mirror>")
end method set-mirror-parent;
||#

(defmethod set-mirror-parent ((child <gtk-top-level-mirror>)
			      (parent <gtk-display-mirror>))
  (ignoring "set-mirror-parent for <top-level-mirror>"))


#||    
define method move-mirror
    (parent :: <display-mirror>, child :: <top-level-mirror>,
     x :: <integer>, y :: <integer>)
 => ()
  let widget = GTK-WIDGET(mirror-widget(child));
  //---*** This causes problems!
  // gtk-widget-set-uposition(widget, x, y)
end method move-mirror;
||#

(defmethod move-mirror ((parent <gtk-display-mirror>)
			(child <gtk-top-level-mirror>)
			(x integer)
			(y integer))
  #-(and) (duim-debug-message "gtk-display.MOVE-MIRROR for ~a to ~d,~d" child x y)
  #-(and) (duim-debug-message "    - 'current' region is ~a" (%region child))
  (let* ((widget (CFFI/GTK-WIDGET (mirror-widget child)))
	 (fx     (cffi:foreign-alloc 'gint))
	 (fy     (cffi:foreign-alloc 'gint)))
    ;; Note: we change the gravity to "GDK_GRAVITY_STATIC" so
    ;; we get this stuff returned WITHOUT regard to the window's decoration.
    (cffi/gtk-window-get-position widget fx fy)
    #-(and) (duim-debug-message "    - gtk thinks it's ~d,~d" (cffi:mem-aref fx :int) (cffi:mem-aref fy :int))
    ;;---*** This causes problems!
    ;;(gtk-widget-set-uposition widget x y)
    ;; Novel; the window starts off top-left, moves diagonally down and
    ;; right, until it stops in the bottom-right of the screen.
    #-(and)
    (with-disabled-event-handler (widget "configure-event")
      (gtk-window-move widget x y))
    ;;;---*** This causes problems too! Because gtk doesn't report the 'correct' values for
    ;;;---*** the window position. What can be done about this?
    ;;; The window just wobbles from side to side...
    ;;    (duim-debug-message "gtk-display.MOVE-MIRROR -- ignoring move to ~d, ~d [FIXME!]" x y)
    #-(and)
    (if (and (= (cffi:mem-aref fx :int) x)
	     (= (cffi:mem-aref fy :int) y))
	;; Window is in the position we think it should be already
	(duim-debug-message "gtk-display.MOVE-MIRROR - ignoring move, window is already correctly placed")
	(gtk-window-move widget x y))
    (cffi:foreign-free fx)
    (cffi:foreign-free fy)))


#||
define method size-mirror
    (parent :: <display-mirror>, child :: <top-level-mirror>,
     width :: <integer>, height :: <integer>)
 => ()
  let widget = GTK-WIDGET(mirror-widget(child));
  //--- This may not work after the sheet is mapped...
  //---*** This causes the window to grow and grow...
  // gtk-window-set-default-size(widget, width, height)
end method size-mirror;
||#

(defmethod size-mirror ((parent <gtk-display-mirror>) (child <gtk-top-level-mirror>)
			(width integer) (height integer))
  (let* ((widget (CFFI/GTK-WIDGET (mirror-widget child))))
    (cffi/gtk-window-set-default-size widget width height)))


