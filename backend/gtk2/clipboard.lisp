;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GTK2-DUIM -*-
(in-package #:gtk2-duim)

#||
Module:       gtk-duim
Synopsis:     GTK basic clipboard implementation
Author:       Andy Armstrong, Scott McKay
Copyright:    Original Code is Copyright (c) 1995-2004 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND

/// GTK clipboard handling

/*---*** No clipboard for now...
define class <gtk-clipboard> (<clipboard>)
  sealed slot clipboard-sheet :: <sheet>,
    required-init-keyword: sheet:;
  sealed slot clipboard-cleared? :: <boolean> = #f;
end class <gtk-clipboard>;
||#

(defclass <gtk-clipboard> (<clipboard>)
  ((native-clipboard :initarg :native-clipboard
		     :initform (error ":native-clipboard initarg must be provided")
		     :accessor %native-clipboard)
   (clipboard-sheet :initarg :sheet
		    :initform (error ":sheet initarg must be provided")
		    :accessor clipboard-sheet)
   (clipboard-cleared? :initform nil :accessor clipboard-cleared?)))


#||
define variable *clipboard* :: false-or(<gtk-clipboard>) = #f;
||#

(defparameter *clipboard* nil)
(defparameter *clipboard-target* (cffi/gdk-atom-intern "DUIM-CLIPBOARD" nil))


#||
define sealed method open-clipboard
    (port :: <gtk-port>, sheet :: <sheet>)
 => (clipboard :: false-or(<gtk-clipboard>))
  let top-sheet = top-level-sheet(sheet);
  when (top-sheet)
    //---*** OPEN THE CLIPBOARD, e.g. Win32 OpenClipboard
    if (clipboard)
      clipboard-sheet(clipboard) := top-sheet;
      clipboard
    else
      *clipboard* := make(<gtk-clipboard>, sheet: top-sheet)
    end
  end
end method open-clipboard;
||#

(defmethod open-clipboard ((port <gtk-port>) (sheet <sheet>))
  (let ((top-sheet (top-level-sheet sheet)))
    (if *clipboard*
	(progn
	  (setf (clipboard-sheet *clipboard*) top-sheet)
	  *clipboard*)
	(let ((gtkclipboard (cffi/gtk-clipboard-get *clipboard-target*)))
	  (setf *clipboard* (make-instance '<gtk-clipboard>
					   :native-clipboard gtkclipboard
					   :sheet top-sheet))))))


#||
define sealed method close-clipboard
    (port :: <gtk-port>, clipboard :: <gtk-clipboard>) => ()
  //---*** CLOSE THE CLIPBOARD, e.g. Win32 CloseClipboard
  clipboard-cleared?(clipboard) := #f
end method close-clipboard;
||#

(defmethod close-clipboard ((port <gtk-port>) (clipboard <gtk-clipboard>))
  ;; Native clipboard is owned by gtk and should not be freed
  (setf (clipboard-cleared? clipboard) nil))


#||
define sealed method clipboard-owner
    (clipboard :: <gtk-clipboard>)
 => (owner :: false-or(<sheet>))
  //---*** GET THE OWNER, e.g. GetClipboardOwner
  let mirror = window-mirror(owner);
  mirror & mirror-sheet(mirror)
end method clipboard-owner;

define sealed method add-clipboard-data-as
    (type :: subclass(<string>), clipboard :: <gtk-clipboard>, data :: <string>)
 => (success? :: <boolean>)
  let buffer = string-to-clipboard-buffer(data);
  when (buffer)
    maybe-clear-clipboard(clipboard);
    //---*** SET THE DATA, e.g. SetClipboardData
    #t
  end
end method add-clipboard-data-as;
||#

(defmethod add-clipboard-data-as (type (clipboard <gtk-clipboard>) data)
  ;; This method is here for documentation as much as anything else
  ;; Eventually the clipboard will deal with pretty much everything and
  ;; it can be removed.
  (cond ((INSTANCE? type (find-class 'string))       (add-clipboard-data-as (find-class 'string)       clipboard data))
	((INSTANCE? type (find-class '<gtk-pixmap>)) (add-clipboard-data-as (find-class '<gtk-pixmap>) clipboard data))

	#-(and)
	((INSTANCE? type (find-class '<image>))
	 (multiple-value-bind (pixmap something no-idea-what)
	     (decode-ink params)
	   (add-clipboard-data-as (find-class '<gtk-pixmap>) clipboard pixmap)))

	;; FIXME: DEAL WITH OTHER TYPES OF DATA
	;; FIXME: GTK-CLIPBOARD-SET-WITH-OWNER?
	;; FIXME: GTK-CLIPBOARD-SET-WITH-DATA?
	;;
	;; GTK seems to support the following types of data on clipboards:
	;;    text - utf8 strings
	;;    image - GdkPixbufs
	;;    rich text - guint8*
	;;    uris - gchar**
	;;    data - everything else
	;;
	;; Not a specific gtk type - let DUIM clipboard implementation deal with arbitrary
	;; objects...
	(t (gtk-debug "[GTK-CLIPBOARD ADD-CLIPBOARD-DATA-AS] Calling DUIM built-in to deal with CL object")
	   (maybe-clear-clipboard clipboard)
	   (call-next-method (find-class 'standard-class) clipboard data))))


(defmethod add-clipboard-data-as ((type (eql (find-class 'string)))
				  (clipboard <gtk-clipboard>)
				  (data string))
  (let ((native-clipboard (%native-clipboard clipboard)))
    (cffi/gtk-clipboard-set-text native-clipboard data -1)
    ;; Data that is converted into something of general applicability
    ;; (text, uris, images, but not data, should also invoke
    ;; GTK-CLIPBOARD-STORE to keep it around if the lisp exits.
    (cffi/gtk-clipboard-store native-clipboard)))


(defmethod add-clipboard-data-as ((type (eql (find-class '<gtk-pixmap>)))
				  (clipboard <gtk-clipboard>)
				  (data <gtk-pixmap>))
  (let* ((native-clipboard (%native-clipboard clipboard))
	 ;; FIXME: Should generate a pixbuf directly
	 (gdk-pixmap (pixmap-to-gdk-pixmap data))
	 ;; Convert GdkPixmap (server-side) -> GdkPixbuf (client side)
	 (gdk-pixbuf (cffi/gdk-pixbuf-get-from-drawable nil gdk-pixmap nil ;colormap
							0 0 0 0
							(width data) (height data))))
    (cffi/gtk-clipboard-set-image native-clipboard gdk-pixbuf)
    (cffi/gtk-clipboard-store native-clipboard)))


#||
define sealed method maybe-clear-clipboard
    (clipboard :: <gtk-clipboard>) => ()
  unless (clipboard-cleared?(clipboard))
    clear-clipboard(clipboard)
  end
end method maybe-clear-clipboard;
||#

;; This only clears CL data from the clipboard; the GTK clipboard is left
;; unchanged...
(defmethod maybe-clear-clipboard ((clipboard <gtk-clipboard>))
  (unless (clipboard-cleared? clipboard)
    (clear-clipboard clipboard)))


#||
define sealed method clear-clipboard
    (clipboard :: <gtk-clipboard>) => ()
  next-method();
  //---*** CLEAR THE CLIPBOARD, e.g. EmptyClipboard
  clipboard-cleared?(clipboard) := #t
end method clear-clipboard;
||#

;; This only clears CL data from the clipboard; the GTK clipboard is left
;; unchanged...
(defmethod clear-clipboard ((clipboard <gtk-clipboard>))
  (call-next-method)
  (setf (clipboard-cleared? clipboard) t))


#||
define sealed method clipboard-data-available?
    (class :: subclass(<string>), clipboard :: <gtk-clipboard>)
 => (available? :: <boolean>)
  ignore(class);
  clipboard-format-available?(clipboard, #"text")
end method clipboard-data-available?;
||#

(defmethod clipboard-data-available? (type (clipboard <gtk-clipboard>))
  ;; This method is here for documentation as much as anything else
  ;; Eventually the clipboard will deal with pretty much everything and
  ;; it can be removed.
  (cond ((INSTANCE? type (find-class 'string))       (clipboard-data-available? (find-class 'string)  clipboard))
	((INSTANCE? type (find-class '<gtk-pixmap>)) (clipboard-data-available? (find-class '<image>) clipboard))
	((INSTANCE? type (find-class '<image>))      (clipboard-data-available? (find-class '<image>) clipboard))

	;; FIXME: DEAL WITH OTHER TYPES OF DATA
	;; FIXME: GTK-CLIPBOARD-SET-WITH-OWNER?
	;; FIXME: GTK-CLIPBOARD-SET-WITH-DATA?
	;;
	;; GTK seems to support the following types of data on clipboards:
	;;    text - utf8 strings
	;;    image - GdkPixbufs
	;;    rich text - guint8*
	;;    uris - gchar**
	;;    data - everything else
	(t (gtk-debug "[GTK-CLIPBOARD CLIPBOARD-DATA-AVAILABLE?] Calling DUIM built-in for CL object")
	   (call-next-method (find-class 'standard-class) clipboard))))


(defmethod clipboard-data-available? ((class (eql (find-class 'string))) (clipboard <gtk-clipboard>))
  (let ((native-clipboard (%native-clipboard clipboard)))
    (when (null native-clipboard)
      (error "Clipboard not initialized"))
    (cffi/gtk-clipboard-wait-is-text-available native-clipboard)))


(defmethod clipboard-data-available? ((class (eql (find-class '<image>))) (clipboard <gtk-clipboard>))
  (let ((native-clipboard (%native-clipboard clipboard)))
    (when (null native-clipboard)
      (error "Clipboard not initialized"))
    (cffi/gtk-clipboard-wait-is-image-available native-clipboard)))


;; FIXME
;; gtk-clipboard-wait-is-rich-text-available
;; gtk-clipboard-wait-is-uris-available


#||
define sealed method get-clipboard-data-as
    (class :: subclass(<string>), clipboard :: <gtk-clipboard>)
 => (string :: false-or(<string>))
  ignore(class);
  when (clipboard-format-available?(clipboard, #"text"))
    //---*** GET THE TEXT DATA FROM THE CLIPBOARD, e.g. GetClipboardData
    clipboard-buffer-to-string(buffer)
  end
end method get-clipboard-data-as;
||#

(defmethod get-clipboard-data-as (type (clipboard <gtk-clipboard>))
  ;; This method is here for documentation as much as anything else
  ;; Eventually the clipboard will deal with pretty much everything and
  ;; it can be removed.
  (cond ((INSTANCE? type (find-class 'string))       (get-clipboard-data-as (find-class 'string)       clipboard))
	((INSTANCE? type (find-class '<gtk-pixmap>)) (get-clipboard-data-as (find-class '<gtk-pixmap>) clipboard))
	;; XXX: Whilst it's possible to put arbitrary <image> types onto the clipboard, currently
	;; you can only get them back off as <gtk-pixmap>s. Not sure it matters...

	;; FIXME: DEAL WITH OTHER TYPES OF DATA
	;; FIXME: GTK-CLIPBOARD-SET-WITH-OWNER?
	;; FIXME: GTK-CLIPBOARD-SET-WITH-DATA?
	;;
	;; GTK seems to support the following types of data on clipboards:
	;;    text - utf8 strings
	;;    image - GdkPixbufs
	;;    rich text - guint8*
	;;    uris - gchar**
	;;    data - everything else
	(t (gtk-debug "[GTK-CLIPBOARD GET-CLIPBOARD-DATA-AS] Calling DUIM built-in for CL object")
	   (call-next-method (find-class 'standard-class) clipboard))))


(defmethod get-clipboard-data-as ((class (eql (find-class 'string))) (clipboard <gtk-clipboard>))
  (let ((native-clipboard (%native-clipboard clipboard)))
    (when native-clipboard
      ;; Note:
      ;; 1. this will block
      ;; 2. the return value should be freed when done (how?)
      (cffi/gtk-clipboard-wait-for-text native-clipboard))))


(defmethod get-clipboard-data-as ((class (eql (find-class '<gtk-pixmap>))) (clipboard <gtk-clipboard>))
  (let ((native-clipboard (%native-clipboard clipboard)))
    (when native-clipboard
      ;; Note:
      ;; 1. this will block
      ;; 2. the return value should be freed when done (how?)
      (cffi/gtk-clipboard-wait-for-image native-clipboard))))


#||

/// Raw clipboard handling

// _Not_ sealed, so that users can extend it
define method clipboard-format-available?
    (clipboard :: <gtk-clipboard>, format)
 => (available? :: <boolean>)
  //---*** SEE IF THERE IS DATA OF THE GIVEN FORMAT
end method clipboard-format-available?;

define macro with-clipboard-lock
  { with-clipboard-lock (?buffer:name = ?buffer-handle:expression) ?body:body end }
    => { begin
	   let _locked? = #f;
	   //---*** LOCK THE CLIPBOARD
	   _locked? := #t;
	   block ()
	     let ?buffer :: <C-string> = XXX;	//---*** GET A DATA BUFFER
	     ?body
	   cleanup
	     when (_locked?)
	       //---*** UNLOCK THE CLIPBOARD
	     end
	   end
	 end }
end macro with-clipboard-lock;

define function string-to-clipboard-buffer
    (string :: <string>) => (handle :: <XXX>)
  let string = convert-to-native-newlines(string);
  //---*** COPY THE STRING INTO THE CLIPBOARD BUFFER
end function string-to-clipboard-buffer;

define function clipboard-buffer-to-string
    (handle :: <XXX>) => (string :: <byte-string>)
  with-clipboard-lock (buffer = handle)
    let string-size = size(buffer);
    let string = make(<byte-string>, size: string-size);
    without-bounds-checks
      for (i from 0 below string-size)
        string[i] := buffer[i]
      end
    end;
    convert-from-native-newlines(string)
  end
end function clipboard-buffer-to-string;
*/

//---*** Until the code above is commented back in
ignore(convert-from-native-newlines);
||#
