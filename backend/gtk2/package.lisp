;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

(defpackage #:gtk2-duim
  (:use #:common-lisp
	#:duim-utilities
	#:duim-internals
	#:duim-gadget-panes-internals)  ;;---*** Until all native gadgets are in
  (:export
           ;; Basic classes
           #:<gtk-port>
	   #:<gtk-medium>
	   #:<gtk-frame-manager>

	   ;; Gadget creation protocols
	   #:<gtk-mirror>
	   #:<gtk-widget-mirror>
	   #:<gtk-gadget-mirror>
	   #:make-gtk-mirror
	   #:install-event-handlers
	   #:install-named-handlers
	   #:update-mirror-attributes
	   #:set-mirror-parent
	   #:move-mirror
	   #:size-mirror

	   ;; These can be used by someone who wants to import their own GTK
	   ;; gadget
	   #:event-handler-definer
	   #:<gtk-pane-mixin>
	   #:<gtk-gadget-mixin>
	   #:<gtk-text-gadget-mixin>
	   #:<gtk-top-level-sheet-mixin>
	   #:handle-gtk-destroy-event
	   #:handle-gtk-delete-event
	   #:handle-gtk-motion-event
	   #:handle-gtk-button-press-event
	   #:handle-gtk-button-release-event
	   #:handle-gtk-key-press-event
	   #:handle-gtk-key-release-event
	   #:handle-gtk-configure-event
	   #:handle-gtk-expose-event
	   #:handle-gtk-enter-event
	   #:handle-gtk-leave-event
	   #:handle-gtk-clicked-event
	   #:handle-gtk-select-row-event
	   #:handle-gtk-click-column-event
	   #:handle-gtk-resize-column-event

	   ;; Other possibly useful exports
	   #:shutdown-gtk-duim))
