;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GTK2-DUIM -*-
(in-package #:gtk2-duim)

#||
Module:       gtk-duim
Synopsis:     GTK drawing implementation
Author:       Andy Armstrong, Scott McKay
Copyright:    Original Code is Copyright (c) 1995-2004 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND

/// GTK graphics

define constant $2pi-in-64ths-of-degree :: <integer> = 360 * 64;
define constant $supports-titled-ellipses = #f;
||#

(defconstant +2pi-in-64ths-of-degree+ (* 360 64))
(defconstant +supports-tilted-ellipses+ nil)


(DEFUN SET-CAIRO-SOURCE-FROM-COLOR (CR MEDIUM COLOR)
"
Decode _color_ and set the source rgb(a) for cr to the
rgb components.
"
;; _color_ might be an indirect ink, so decode it
  (let ((duim-color (decode-ink medium cr color)))
    (multiple-value-bind (red green blue alpha)
	(color-rgb duim-color)
      (if (= alpha 1.0)
	  ;; Make an rgb surface...
	  (cffi/cairo_set_source_rgb cr
				     (coerce red   'double-float)
				     (coerce green 'double-float)
				     (coerce blue  'double-float))
	  ;; else - Make an rgba surface...
	  (cffi/cairo_set_source_rgba cr
				      (coerce red   'double-float)
				      (coerce green 'double-float)
				      (coerce blue  'double-float)
				      (coerce alpha 'double-float))))))
 


;; Unfortunately this repeats some of the steps in UPDATE-DRAWING-STATE
;; but we don't know in that method how the colours/operations will be
;; used.
(DEFUN SET-CR-SOURCE-SURFACE (CR MEDIUM &KEY FILLED? BACKGROUND? X Y)
"
Sets the source surface on CR from medium. If BACKGROUND? is
T, the ink will be either the (%BACKGROUND-INK MEDIUM) image
surface (if set) or an RGB(A) surface from (MEDIUM-BACKGROUND
MEDIUM).

If BACKGROUND? is NIL and FILLED? it T, the surface will be
either (%BRUSH-INK MEDIUM) or (BRUSH-FOREGROUND (MEDIUM-BRUSH MEDIUM)).
BRUSH-BACKGROUND seems to be totally ignored!

If BACKGROUND? is NIL and FILLED? is NIL, the surface will be
either (%BRUSH-INK MEDIUM), (BRUSH-FOREGROUND (MEDIUM-BRUSH MEDIUM)),
(%FOREGROUND-INK MEDIUM) or (MEDIUM-FOREGROUND MEDIUM), depending
on whether MEDIUM-BRUSH is non-nil or not.

Sets: source surface, pattern extend (if applicable), operator.
"
  (cond (background?
	 ;; Set up to draw in the background ink
	 (if (%background-ink medium)
	     (progn
	       (cffi/cairo_set_source_surface cr (%background-ink medium) x y)
	       (cffi/cairo_pattern_set_extend (cffi/cairo_get_source cr)
					      (%background-fill-style medium)))
	     ;; else
	     (SET-CAIRO-SOURCE-FROM-COLOR cr medium (medium-background medium)))
	 ;; set operation
	 (cffi/cairo_set_operator cr (%background-operation medium)))
	((and (not background?) filled?)
	 ;; Set up to draw in the brush ink
	 (if (%brush-ink medium)
	     (progn
	       (cffi/cairo_set_source_surface cr (%brush-ink medium) x y)
	       (cffi/cairo_pattern_set_extend (cffi/cairo_get_source cr)
					      (%brush-fill-style medium)))
	     ;; else
	     (SET-CAIRO-SOURCE-FROM-COLOR cr medium (medium-brush medium)))
	 (cffi/cairo_set_operator cr (%brush-operation medium)))
	(t
	 ;; Set up to draw in the brush ink if there is a brush, else in the
	 ;; foreground ink
	 (if (medium-brush medium)
	     ;; Set up to draw in the brush ink
	     (if (%brush-ink medium)
		 (progn
		   (cffi/cairo_set_source_surface cr (%brush-ink medium) x y)
		   (cffi/cairo_pattern_set_extend (cffi/cairo_get_source cr)
						  (%brush-fill-style medium)))
		 ;; else
		 (SET-CAIRO-SOURCE-FROM-COLOR cr medium (medium-brush medium)))
	     ;; else use the medium foreground
	     (if (%foreground-ink medium)
		 (progn
		   (cffi/cairo_set_source_surface cr (%foreground-ink medium) x y)
		   (cffi/cairo_pattern_set_extend (cffi/cairo_get_source cr)
						  (%foreground-fill-style medium)))
		 ;; else
		 (SET-CAIRO-SOURCE-FROM-COLOR cr medium (medium-foreground medium))))
	 (cffi/cairo_set_operator cr (%foreground-operation medium)))))


#||
define sealed method draw-point
    (medium :: <gtk-medium>, x, y) => (record)
  let (drawable :: <GdkDrawable*>, gcontext :: <GdkGC*>)
    = update-drawing-state(medium);
  let transform = medium-device-transform(medium);
  with-device-coordinates (transform, x, y)
    let thickness = pen-width(medium-pen(medium));
    if (thickness < 2)
      gdk-draw-point(drawable, gcontext, x, y)
    else 
      let thickness/2 = truncate/(thickness, 2);
      gdk-draw-arc(drawable, gcontext, $true,
		   x - thickness/2, y - thickness/2, thickness, thickness,
		   0, $2pi-in-64ths-of-degree)
    end
  end;
  #f
end method draw-point;
||#

(defmethod draw-point ((medium <gtk-medium>) x y)
  (let ((cr (update-drawing-state medium))
	(transform (medium-device-transform medium)))
    (cffi/cairo-save cr)
    (with-device-coordinates (transform x y)
      
      (SET-CR-SOURCE-SURFACE CR MEDIUM)

      ;; In Cairo draw a degenerate path with a round line cap...
      (cffi/cairo_set_line_cap cr +CFFI/CAIRO_LINE_CAP_ROUND+)
      (cffi/cairo_move_to cr (coerce x 'double-float) (coerce y 'double-float))
      (cffi/cairo_line_to cr (coerce x 'double-float) (coerce y 'double-float))
      (cffi/cairo_stroke cr))
    (cffi/cairo-restore cr))
  nil)

	      
#||
define sealed method draw-points
    (medium :: <gtk-medium>, coord-seq :: <coordinate-sequence>) => (record)
  let (drawable :: <GdkDrawable*>, gcontext :: <GdkGC*>)
    = update-drawing-state(medium);
  let transform = medium-device-transform(medium);
  let thickness = pen-width(medium-pen(medium));
  if (thickness < 2)
    do-coordinates
      (method (x, y)
	 with-device-coordinates (transform, x, y)
	   //---*** Use gdk-draw-points
	   gdk-draw-point(drawable, gcontext, x, y)
	 end
       end,
       coord-seq)
  else
    let thickness/2 = truncate/(thickness, 2);
    do-coordinates
      (method (x, y)
	 with-device-coordinates (transform, x, y)
	   gdk-draw-arc(drawable, gcontext, $true,
			x - thickness/2, y - thickness/2, thickness, thickness,
			0, $2pi-in-64ths-of-degree)
	 end
       end,
       coord-seq)
  end;
  #f
end method draw-points;
||#

(defmethod draw-points ((medium <gtk-medium>) (coord-seq sequence))
  (let ((cr (update-drawing-state medium)))
    (cffi/cairo-save cr)
    
    (SET-CR-SOURCE-SURFACE CR MEDIUM)

    ;; "Cheat" by setting the cairo cap shape to round
    (cffi/cairo_set_line_cap cr +CFFI/CAIRO_LINE_CAP_ROUND+)
    (let ((transform (medium-device-transform medium)))
      (do-coordinates #'(lambda (x y)
			  (with-device-coordinates (transform x y)
			    (cffi/cairo_move_to cr (coerce x 'double-float) (coerce y 'double-float))
			    (cffi/cairo_line_to cr (coerce x 'double-float) (coerce y 'double-float))))
	coord-seq)
      (cffi/cairo_stroke cr))
    (cffi/cairo-restore cr))
  nil)



#||
/// Pixel graphics

//---*** Do an efficient version of this
define sealed method set-pixel
    (medium :: <gtk-medium>, color :: <rgb-color>, x, y) => (record)
  with-drawing-options (medium, brush: color)
    draw-point(medium, x, y)
  end;
  #f
end method set-pixel;
||#

(defmethod set-pixel ((medium <gtk-medium>)
		      (color <rgb-color>)
		      x y)
  (with-drawing-options (medium :brush color)
    (draw-point medium x y))
  nil)


#||
//---*** Do an efficient version of this
define sealed method set-pixels
    (medium :: <gtk-medium>, color :: <rgb-color>, 
     coord-seq :: <coordinate-sequence>)
 => (record)
  with-drawing-options (medium, brush: color)
    draw-points(medium, coord-seq)
  end;
  #f
end method set-pixels;
||#

(defmethod set-pixels ((medium <gtk-medium>)
		       (color <rgb-color>)
		       (coord-seq sequence))
  (with-drawing-options (medium :brush color)
    (draw-points medium coord-seq))
  nil)



#||
define sealed method draw-line
    (medium :: <gtk-medium>, x1, y1, x2, y2) => (record)
  let (drawable :: <GdkDrawable*>, gcontext :: <GdkGC*>)
    = update-drawing-state(medium, pen: medium-pen(medium));
  let transform = medium-device-transform(medium);
  with-device-coordinates (transform, x1, y1, x2, y2)
    gdk-draw-line(drawable, gcontext, x1, y1, x2, y2)
  end;
  #f
end method draw-line;
||#

(defmethod draw-line ((medium <gtk-medium>) x1 y1 x2 y2)
  (let ((cr (update-drawing-state medium :pen (medium-pen medium)))
	(transform (medium-device-transform medium)))
    (cffi/cairo-save cr)

    (SET-CR-SOURCE-SURFACE CR MEDIUM)

    (with-device-coordinates (transform x1 y1 x2 y2)
      (cffi/cairo_move_to cr (coerce x1 'double-float) (coerce y1 'double-float))
      (cffi/cairo_line_to cr (coerce x2 'double-float) (coerce y2 'double-float))
      (cffi/cairo_stroke cr))
    (cffi/cairo-restore cr))
  nil)


#||
define sealed method draw-lines
    (medium :: <gtk-medium>, coord-seq :: <coordinate-sequence>) => (record)
  let (drawable :: <GdkDrawable*>, gcontext :: <GdkGC*>)
    = update-drawing-state(medium, pen: medium-pen(medium));
  let transform = medium-device-transform(medium);
  //---*** Use gdk-draw-segments
  do-endpoint-coordinates
    (method (x1, y1, x2, y2)
       with-device-coordinates (transform, x1, y1, x2, y2)
	 gdk-draw-line(drawable, gcontext, x1, y1, x2, y2)
       end
     end,
     coord-seq);
  #f
end method draw-lines;
||#

(defmethod draw-lines ((medium <gtk-medium>) (coord-seq sequence))
  (let ((cr (update-drawing-state medium :pen (medium-pen medium)))
	(transform (medium-device-transform medium))
	(first t))
    (cffi/cairo-save cr)

    (SET-CR-SOURCE-SURFACE CR MEDIUM)

    (do-coordinates #'(lambda (x y)
			(if first
			    (with-device-coordinates (transform x y)
			      (cffi/cairo_move_to cr (coerce x 'double-float) (coerce y 'double-float))
			      (setf first nil))
			    (with-device-coordinates (transform x y)
			      (cffi/cairo_line_to cr (coerce x 'double-float) (coerce y 'double-float)))))
      coord-seq)
    (cffi/cairo_stroke cr)
    (cffi/cairo-restore cr))
  nil)


#||
define sealed method draw-rectangle
    (medium :: <gtk-medium>, x1, y1, x2, y2,
     #key filled? = #t) => (record)
  let transform = medium-device-transform(medium);
  if (~rectilinear-transform?(transform))
    with-stack-vector (coords = x1, y1, x2, y1, x2, y2, x1, y2)
      draw-polygon(medium, coords, filled?: filled?, closed?: #t)
    end
  else
    let (drawable :: <GdkDrawable*>, gcontext :: <GdkGC*>)
      = update-drawing-state(medium, pen: ~filled? & medium-pen(medium));
    //---*** Might need to use 'gdk-gc-set-ts-origin' to set tile/stipple origin to x1/y1
    with-device-coordinates (transform, x1, y1, x2, y2)
      gdk-draw-rectangle(drawable, gcontext,
			 if (filled?) $true else $false end,
			 x1, y1, x2 - x1, y2 - y1)
    end
  end;
  #f
end method draw-rectangle;
||#

(defmethod draw-rectangle ((medium <gtk-medium>) x1 y1 x2 y2
			   &key (filled? t))
  (let ((transform (medium-device-transform medium)))
    (if (not (rectilinear-transform? transform))
	(with-stack-vector (coords = x1 y1 x2 y1 x2 y2 x1 y2)
	  (draw-polygon medium coords :filled? filled? :closed? t))
	;; else
	(let ((cr (update-drawing-state medium :pen (and (not filled?) (medium-pen medium)))))
	  (cffi/cairo-save cr)

	  (SET-CR-SOURCE-SURFACE CR MEDIUM :FILLED? filled?)

	  ;;---*** Might need to use 'gdk-gc-set-ts-origin- to set tile/stipple origin to x1/y1
	  (with-device-coordinates (transform x1 y1 x2 y2)
	    ;; FIXME: Filled rectangles are 1 unit smaller in both dimensions than unfilled rectangles...
	    (cffi/cairo_rectangle cr
				  (coerce x1 'double-float)
				  (coerce y1 'double-float)
				  (coerce (- x2 x1) 'double-float)
				  (coerce (- y2 y1) 'double-float))
	    (if filled?
		(cffi/cairo_fill cr)
		(cffi/cairo_stroke cr)))
	  (cffi/cairo-restore cr))))
  nil)


#||
define sealed method draw-rectangles
    (medium :: <gtk-medium>, coord-seq :: <coordinate-sequence>,
     #key filled? = #t) => (record)
  let transform = medium-device-transform(medium);
  if (~rectilinear-transform?(transform))
    draw-transformed-rectangles(medium, coord-seq, filled?: filled?)
  else
    let (drawable :: <GdkDrawable*>, gcontext :: <GdkGC*>)
      = update-drawing-state(medium, pen: ~filled? & medium-pen(medium));
    let transform = medium-device-transform(medium);
    do-endpoint-coordinates
      (method (x1, y1, x2, y2)
	 with-device-coordinates (transform, x1, y1, x2, y2)
	   gdk-draw-rectangle(drawable, gcontext, 
			      if (filled?) $true else $false end,
			      x1, y1, x2 - x1, y2 - y1)
	 end
       end,
       coord-seq);
  end;
  #f
end method draw-rectangles;
||#

(defmethod draw-rectangles ((medium <gtk-medium>)
			    (coord-seq sequence)
			    &key (filled? t))
  (let ((transform (medium-device-transform medium)))
    (if (not (rectilinear-transform? transform))
	(draw-transformed-rectangles medium coord-seq :filled? filled?)
	;; else
	(let ((cr (update-drawing-state medium :pen (and (not filled?) (medium-pen medium))))
	      (transform (medium-device-transform medium)))
	  (cffi/cairo-save cr)

	  (SET-CR-SOURCE-SURFACE CR MEDIUM :FILLED? filled?)

	  (do-endpoint-coordinates #'(lambda (x1 y1 x2 y2)
				       (with-device-coordinates (transform x1 y1 x2 y2)
					 ;; Filled rectangles are 1 unit smaller in both dimensions than unfilled...
					 (cffi/cairo_rectangle cr
							       (coerce x1 'double-float)
							       (coerce y1 'double-float)
							       (coerce (- x2 x1) 'double-float)
							       (coerce (- y2 y1) 'double-float))))
	    coord-seq)
	  (if filled?
	      (cffi/cairo_fill cr)
	      (cffi/cairo_stroke cr))
	  (cffi/cairo-restore cr))))
  nil)


#||
define sealed method draw-transformed-rectangles
    (medium :: <gtk-medium>, coord-seq :: <coordinate-sequence>,
     #rest keys, #key filled? = #t) => (record)
  dynamic-extent(keys);
  ignore(filled?);
  let ncoords :: <integer> = size(coord-seq);
  assert(zero?(modulo(ncoords, 4)),
	 "The coordinate sequence has the wrong number of elements");
  local method draw-one (x1, y1, x2, y2) => ()
	  with-stack-vector (coords = x1, y1, x2, y1, x2, y2, x1, y2)
	    apply(draw-polygon, medium, coords, closed?: #t, keys)
	  end
        end method;
  dynamic-extent(draw-one);
  without-bounds-checks
    for (i :: <integer> = 0 then i + 4, until: i = ncoords)
      draw-one(coord-seq[i + 0], coord-seq[i + 1],
	       coord-seq[i + 2], coord-seq[i + 3])
    end
  end;
  #f
end method draw-transformed-rectangles;
||#

(defgeneric draw-transformed-rectangles (medium coord-seq &rest keys &key filled?))

(defmethod draw-transformed-rectangles ((medium <gtk-medium>) (coord-seq sequence)
					&rest keys &key (filled? t))
  (declare (dynamic-extent keys) (ignore filled?))
  (let ((ncoords (size coord-seq)))
    (unless (zerop (mod ncoords 4))
      (error "The coordinate sequence has the wrong number of elements (~d)"
	     ncoords))
    (labels ((draw-one (x1 y1 x2 y2)
	       (with-stack-vector (coords = x1 y1 x2 y1 x2 y2 x1 y2)
		 (apply #'draw-polygon medium coords :closed? t keys))))
      (without-bounds-checks
        (loop for i = 0 then (+ i 4) until (equal? i ncoords)
	      do (draw-one (aref coord-seq (+ i 0))
			   (aref coord-seq (+ i 1))
			   (aref coord-seq (+ i 2))
			   (aref coord-seq (+ i 3)))))))
  nil)


#||
define sealed method draw-rounded-rectangle
    (medium :: <gtk-medium>, x1, y1, x2, y2,
     #key filled? = #t, radius) => (record)
  let (drawable :: <GdkDrawable*>, gcontext :: <GdkGC*>)
    = update-drawing-state(medium, pen: ~filled? & medium-pen(medium));
  let transform = medium-device-transform(medium);
  with-device-coordinates (transform, x1, y1, x2, y2)
    unless (radius)
      let width  = x2 - x1;
      let height = y2 - y1;
      radius := max(truncate/(min(width, height), 3), 2)
    end;
    //---*** DO THIS FOR REAL
    draw-rectangle(medium, x1, y1, x2, y2, filled?: filled?)
  end;
  #f
end method draw-rounded-rectangle;
||#

(defmethod draw-rounded-rectangle ((medium <gtk-medium>) x1 y1 x2 y2
				   &key (filled? t) radius)
  (let ((cr (update-drawing-state medium :pen (and (not filled?) (medium-pen medium))))
	(transform (medium-device-transform medium)))
    (cffi/cairo-save cr)

    (SET-CR-SOURCE-SURFACE CR MEDIUM :FILLED? filled?)

    (with-device-coordinates (transform x1 y1 x2 y2)
      (unless radius
	(let ((width (- x2 x1))
	      (height (- y2 y1)))
	  (setf radius (max (truncate (min width height) 3) 2))))
      (let ((x1+rad (+ x1 radius))
	    (x2-rad (- x2 radius))
	    (y1+rad (+ y1 radius))
	    (y2-rad (- y2 radius))
	    (rad    radius)
	    (north  (/ pi 2))
	    (east   0)
	    (south  (* (/ pi 2) 3))
	    (west   pi))
	;; FIXME: this stands no chance of actually working... revisit it.
	(unless filled?
	  (draw-line medium x1+rad y1 x2-rad y1)   ;; top
	  (draw-line medium x1+rad y2 x2-rad y2)   ;; bottom
	  (draw-line medium x1 y1+rad x1 y2-rad)   ;; left
	  (draw-line medium x2 y1+rad x2 y2-rad)   ;; right
	  ;; FIXME: What should these radii-dx values be?
	  (draw-ellipse medium x1+rad y1+rad 0 rad rad 0 :start-angle north :end-angle west :filled? nil)  ;; nw
	  (draw-ellipse medium x2-rad y1+rad 0 0 rad rad :start-angle north :end-angle east :filled? nil)  ;; ne
	  (draw-ellipse medium x1+rad y2-rad rad 0 0 rad :start-angle south :end-angle west :filled? nil)  ;; sw
	  (draw-ellipse medium x2-rad y2-rad rad 0 0 rad :start-angle south :end-angle east :filled? nil)) ;; se
	(when filled?
	  (draw-rectangle medium x1+rad y1 x2-rad y2 :filled? t)
	  (draw-rectangle medium x1 y1+rad x2 y2-rad :filled? t)
	  (draw-ellipse medium x1+rad y1+rad rad rad rad rad :start-angle north :end-angle west :filled? t)  ;; nw
	  (draw-ellipse medium x2-rad y1+rad rad rad rad rad :start-angle north :end-angle east :filled? t)  ;; ne
	  (draw-ellipse medium x1+rad y2-rad rad rad rad rad :start-angle south :end-angle west :filled? t)  ;; sw
	  (draw-ellipse medium x2-rad y2-rad rad rad rad rad :start-angle south :end-angle east :filled? t)))) ;; se
    (cffi/cairo-restore cr))
  nil)


#||
define sealed method draw-polygon
    (medium :: <gtk-medium>, coord-seq :: <coordinate-sequence>,
     #key closed? = #t, filled? = #t) => (record)
  let (drawable :: <GdkDrawable*>, gcontext :: <GdkGC*>)
    = update-drawing-state(medium, pen: ~filled? & medium-pen(medium));
  let transform = medium-device-transform(medium);
  let scoords :: <integer> = size(coord-seq);
  let ncoords :: <integer> = size(coord-seq);
  let npoints :: <integer> = floor/(ncoords, 2) + if (closed? & ~filled?) 1 else 0 end;
  with-stack-structure (points :: <GdkPoint*>, element-count: npoints)
    //--- Can't use without-bounds-checks until it works on FFI 'element-setter' calls
    // without-bounds-checks
      for (i :: <integer> from 0 below ncoords by 2,
	   j :: <integer> from 0)
	let x = coord-seq[i + 0];
	let y = coord-seq[i + 1];
	with-device-coordinates (transform, x, y)
	  //---*** This doesn't work in the FFI!
	  // let point = points[j];
	  let point = pointer-value-address(points, index: j);
	  point.x-value := x;
	  point.y-value := y;
	end;
      finally
	when (closed? & ~filled?)
	  //---*** This doesn't work in the FFI!
	  // let point = points[0];
	  let first-point = pointer-value-address(points, index: 0);
	  let last-point  = pointer-value-address(points, index: npoints - 1);
	  last-point.x-value := first-point.x-value;
	  last-point.y-value := first-point.y-value;
	end
      end;
    // end;
    if (filled?)
      gdk-draw-polygon(drawable, gcontext, 
                       $true,
                       points, npoints)
    else
// ---*** gdk-draw-lines doesn't work on Win32 for some reason so use kludge instead.
// ---*** Kludge draws each line in turn after frigging the gcontext so that
// ---*** the line ends don't go over the start of the next line.
// ---*** Unfortunately, drawing to a copied gcontext seemed to cause crashes
// ---*** (I tried both Dylan stack allocated and gdk-gc-new gcontexts)
// ---*** so the code has to frig a potentially shared gcontext (= not good).
//      gdk-draw-lines(drawable, gcontext, points, npoints)
      with-stack-structure (gcontext-values :: <GdkGCValues*>)
        let old-cap-style = #f;
        block ()
          gdk-gc-get-values(gcontext, gcontext-values);
          old-cap-style := gcontext-values.cap-style-value;
          gdk-gc-set-line-attributes(gcontext,
                                     gcontext-values.line-width-value,
                                     gcontext-values.line-style-value,
                                     $gdk-cap-butt, // NB short lines for better joins
                                     gcontext-values.join-style-value);
          let previous-p = pointer-value-address(points, index: 0);
          for (i from 1 below npoints)
            let previous-x :: <integer> = previous-p.x-value;
            let previous-y :: <integer> = previous-p.y-value;
            let p = pointer-value-address(points, index: i);
            let x = p.x-value;
            let y = p.y-value;
            gdk-draw-line(drawable, gcontext, previous-x, previous-y, x, y);
            previous-p := p;
          end;
        cleanup
          if (old-cap-style)
            gdk-gc-set-line-attributes(gcontext,
                                       gcontext-values.line-width-value,
                                       gcontext-values.line-style-value,
                                       old-cap-style,
                                       gcontext-values.join-style-value);
          end;
        end block;
      end with-stack-structure;
    end
  end;
  #f
end method draw-polygon;
||#

(defmethod draw-polygon ((medium <gtk-medium>) (coord-seq sequence)
			 &key (closed? t) (filled? t))
  (let* ((cr (update-drawing-state medium :pen (and (not filled?) (medium-pen medium))))
	 (transform (medium-device-transform medium))
	 (ncoords (size coord-seq)))
    (cffi/cairo-save cr)

    (SET-CR-SOURCE-SURFACE CR MEDIUM :FILLED? filled?)

    (without-bounds-checks
      (loop for i from 0 below ncoords by 2
	 for j from 0
	 for first = t then nil
	 ;; XXX: 'usual' drawing ops pass an array for the coords; the
	 ;; recording code passes a list. Should the recording code
	 ;; be 'fixed'?
	 do (let ((x (elt coord-seq (+ i 0)))
		  (y (elt coord-seq (+ i 1))))
	      (with-device-coordinates (transform x y)
		(if first
		    (cffi/cairo_move_to cr (coerce x 'double-float) (coerce y 'double-float))
		    (cffi/cairo_line_to cr (coerce x 'double-float) (coerce y 'double-float)))))
	 finally (when (and closed? (not filled?))
		   (cffi/cairo_close_path cr))))
    (if filled?
	(cffi/cairo_fill cr)
	(cffi/cairo_stroke cr))
    (cffi/cairo-restore cr))
  nil)


#||
define sealed method draw-ellipse
    (medium :: <gtk-medium>, center-x, center-y,
     radius-1-dx, radius-1-dy, radius-2-dx, radius-2-dy,
     #key start-angle, end-angle, filled? = #t) => (record)
  let (drawable :: <GdkDrawable*>, gcontext :: <GdkGC*>)
    = update-drawing-state(medium, pen: ~filled? & medium-pen(medium));
  let transform = medium-device-transform(medium);
  with-device-coordinates (transform, center-x, center-y)
    with-device-distances (transform, radius-1-dx, radius-1-dy, radius-2-dx, radius-2-dy)
      let (angle-2, x-radius, y-radius, angle-1)
	= singular-value-decomposition-2x2(radius-1-dx, radius-2-dx, radius-1-dy, radius-2-dy);
      if (~$supports-titled-ellipses
	  | x-radius = abs(y-radius)		// a circle - rotations are irrelevant
	  | zero?(angle-1))			// axis-aligned ellipse
	let (angle, delta-angle)
	  = if (start-angle & end-angle)
	      let start-angle = modulo(start-angle, $2pi);
	      let end-angle   = modulo(end-angle, $2pi);
	      when (end-angle < start-angle)
		end-angle := end-angle + $2pi
	      end;
	      values(round($2pi-in-64ths-of-degree * (($2pi - start-angle) / $2pi)),
		     round($2pi-in-64ths-of-degree * ((start-angle - end-angle) / $2pi)))
	    else
	      values(0, $2pi-in-64ths-of-degree)
	    end;
	x-radius := abs(x-radius);
	y-radius := abs(y-radius);
	gdk-draw-arc(drawable, gcontext, 
		     if (filled?) $true else $false end,
		     center-x - x-radius, center-y - y-radius,
		     x-radius * 2, y-radius * 2, angle, delta-angle)
      else
	ignoring("draw-ellipse for tilted ellipses");
	#f
      end;
      // SelectObject(hDC, old-object)
    end
  end;
  #f
end method draw-ellipse;
||#

(defmethod draw-ellipse ((medium <gtk-medium>) center-x center-y
			 radius-1-dx radius-1-dy radius-2-dx radius-2-dy
			 &key start-angle end-angle (filled? t))
  (let ((cr (update-drawing-state medium :pen (and (not filled?) (medium-pen medium))))
	(transform (medium-device-transform medium)))
    (cffi/cairo-save cr)

    (SET-CR-SOURCE-SURFACE CR MEDIUM :FILLED? filled?)

    (with-device-coordinates (transform center-x center-y)
      (with-device-distances (transform radius-1-dx radius-1-dy radius-2-dx radius-2-dy)
	(multiple-value-bind (angle-2 x-radius y-radius angle-1)
	    (singular-value-decomposition-2x2 radius-1-dx radius-2-dx
					      radius-1-dy radius-2-dy)
	  ;; TODO 2020-04-10 DR: should this be ignored? Cairo can do non-axis aligned...
	  (declare (ignore angle-2))
	  (if (or (not +supports-tilted-ellipses+)
		  (equal? x-radius (abs y-radius))      ;; a circle - rotations are irrelevent
		  (zerop angle-1))                       ;; axis-aligned ellipse
	      (multiple-value-bind (angle delta-angle)
		  (if (and start-angle end-angle)
		      (let ((start-angle (mod start-angle +2pi+))
			    (end-angle (mod end-angle +2pi+)))
			(when (< end-angle start-angle)
			  (setf end-angle (+ end-angle +2pi+)))
			(values (round (* +2pi-in-64ths-of-degree+
					  (/ (- +2pi+ start-angle) +2pi+)))
				(round (* +2pi-in-64ths-of-degree+
					  (/ (- start-angle end-angle) +2pi+)))))
		      ;; else
		      (values 0 +2pi-in-64ths-of-degree+))
		;; TODO 2020-04-10 DR: should either of these be used...?
		(declare (ignore angle delta-angle))
		(setf x-radius (abs x-radius)
		      y-radius (abs y-radius))
		;; Draw a unit circle, and scale by x-radius/y-radius. Draw the path
		;; in a different subcontext to the one we stroke in so that the line
		;; width is not also scaled. This works because the current path is NOT
		;; part of the context.
		(cffi/cairo-save cr)
		(cffi/cairo_scale cr (coerce x-radius 'double-float) (coerce y-radius 'double-float))
		;; Move the origin to center-x, center-y remembering that it has been scaled by the
		;; cairo_scale above!
		(cffi/cairo_translate cr
				      (/ (coerce center-x 'double-float) (coerce x-radius 'double-float))
				      (/ (coerce center-y 'double-float) (coerce y-radius 'double-float)))
		(cffi/cairo_arc cr
				0.0d0  ;; center-x
				0.0d0  ;; center-y
				1.0d0  ;; radius
				(coerce (or start-angle 0.0) 'double-float)
				(coerce (or end-angle +2pi+) 'double-float))
		(cffi/cairo-restore cr)
		(if filled?
		    (cffi/cairo_fill cr)
		    (cffi/cairo_stroke cr)))
	      ;; else
	      ;; FIXME: CAN PROBABLY DO THIS BY MORE PATH TRANSFORMS...
	      (progn
		(ignoring "draw-ellipse for tilted ellipses")
		nil)))))
    (cffi/cairo-restore cr))
  nil)


#||
// GTK bitmaps and icons are handled separately
define sealed method draw-image
    (medium :: <gtk-medium>, image :: <image>, x, y) => (record)
  let (drawable :: <GdkDrawable*>, gcontext :: <GdkGC*>)
    = update-drawing-state(medium);
  let transform = medium-device-transform(medium);
  with-device-coordinates (transform, x, y)
    let width  = image-width(image);
    let height = image-height(image);
    ignoring("draw-image");
    //---*** DRAW THE IMAGE, BUT FOR NOW DRAW A RECTANGLE
    // let (pixel, fill-style, operation, pattern)
    //   = convert-ink-to-DC-components(medium, hDC, image);
    // let old-object :: <HANDLE> = SelectObject(hDC, $null-hpen);
    // Rectangle(hDC, x, y, x + width, y + height);
    // SelectObject(hDC, old-object)
  end;
  #f
end method draw-image;
||#

(defmethod draw-image ((medium <gtk-medium>) (image <image>) x y)
  (let ((cr (update-drawing-state medium))
	;; The transforms here just make the image appear in the right place
	;; on the display; the cairo matrix would perform the stretching and
	;; what have you -- test this!
	(transform (medium-device-transform medium)))
    (cffi/cairo-save cr)
    (with-device-coordinates (transform x y)
      (let ((width  (image-width  image))
	    (height (image-height image)))
	(with-device-distances (transform width height)
	  (multiple-value-bind (color fill-type operation pixmap)
	      (decode-ink medium cr image)
	    (declare (ignore color fill-type operation))
	    ;; We want the source surface origin to be at (x, y) on the destination surface
	    (cffi/cairo_set_source_surface cr (%surface pixmap) (coerce x 'double-float) (coerce y 'double-float))
	    (cffi/cairo_rectangle cr
				  (coerce x 'double-float)
				  (coerce y 'double-float)
				  (coerce width 'double-float)
				  (coerce height 'double-float))
	    (cffi/cairo_fill cr)))))
    (cffi/cairo-restore cr))
  nil)


;;; FIXME: DECIDE WHETHER TO USE DO-COPY-AREA OR NOT... I THINK NOT FOR
;;; DRAWING OPS.
#-(and)
(defmethod draw-image ((medium <gtk-medium>) (image <pattern>) x y)
  (multiple-value-bind (drawable cr)
      (update-drawing-state medium)
    (let ((width  (image-width  image))
	  (height (image-height image)))
      (do-copy-area pixmap 0 0 width height medium x y))))


#-(and)
(defmethod draw-image ((medium <gtk-medium>)
		       (image <stencil>) x y)
  ;; FIXME: Should we be transforming ts-x/y? YES WE SHOULD! I'm just not sure that
  ;; this is the right place to do so -- it does nothing for tiles here.
  ;; Perhaps we're better off leaving all this for the user to deal with (maybe add
  ;; a "with-transformed-ts-coordinates" macro or something?)
;;   (let ((transform (medium-device-transform medium))
;; 	(ts-x x)
;; 	(ts-y y))
;;     (with-device-coordinates (transform ts-x ts-y)
  (ignoring "draw-image")
  #-(and)
  (with-brush (medium :stipple image :ts-x x :ts-y y)
    (let ((width  (image-width  image))
	  (height (image-height image)))
;;      (gtk-debug "gtk-draw: draw-image <stencil> drawing ~dx~d image to ~d, ~d"
;;		 width height x y)
      ;; This isn't going to be the correct x2, y2 if the medium has
      ;; anything other than $identity-transform I think... FIXME?
      (draw-rectangle medium x y (+ x width) (+ y height) :filled? t))))


#||
/// Path graphics

define sealed method start-path
    (medium :: <gtk-medium>) => (record)
  ignoring("GTK does not support path-based graphics")
end method start-path;
||#

;;;---*** Hrm. With Cairo, these statements are no longer true I think...

;;; TODO 2020-04-13 DR: add a gui-test to do some path drawing and
;;;                     implement these methods.

(defmethod start-path ((medium <gtk-medium>))
  (ignoring "GTK backend does not currently support path-based graphics"))

#||
define sealed method end-path
    (medium :: <gtk-medium>) => (record)
  ignoring("GTK does not support path-based graphics")
end method end-path;
||#

(defmethod end-path ((medium <gtk-medium>))
  (ignoring "GTK backend does not currently support path-based graphics"))

#||
define sealed method abort-path
    (medium :: <gtk-medium>) => (record)
  ignoring("GTK does not support path-based graphics")
end method abort-path;
||#

(defmethod abort-path ((medium <gtk-medium>))
  (ignoring "GTK backend does not currently support path-based graphics"))

#||
define sealed method close-path
    (medium :: <gtk-medium>) => (record)
  ignoring("GTK does not support path-based graphics")
end method close-path;
||#

(defmethod close-path ((medium <gtk-medium>))
  (ignoring "GTK backend does not currently support path-based graphics"))

#||
define sealed method stroke-path
    (medium :: <gtk-medium>, #key filled?) => (record)
  ignoring("GTK does not support path-based graphics")
end method stroke-path;
||#

(defmethod stroke-path ((medium <gtk-medium>)
			&key filled?)
  (declare (ignore filled?))
  (ignoring "GTK backend does not currently support path-based graphics"))

#||
define sealed method fill-path
    (medium :: <gtk-medium>) => (record)
  ignoring("GTK does not support path-based graphics")
end method fill-path;
||#

(defmethod fill-path ((medium <gtk-medium>))
  (ignoring "GTK backend does not currently support path-based graphics"))

#||
define sealed method clip-from-path
    (medium :: <gtk-medium>, #key function = $boole-and) => (record)
  ignoring("GTK does not support path-based graphics")
end method clip-from-path;
||#

(defmethod clip-from-path ((medium <gtk-medium>)
			   &key (function +boole-and+))
  (declare (ignore function))
  (ignoring "GTK backend does not currently support path-based graphics"))

#||
define sealed method save-clipping-region
    (medium :: <gtk-medium>) => (record)
  ignoring("GTK does not support path-based graphics")
end method save-clipping-region;
||#

(defmethod save-clipping-region ((medium <gtk-medium>))
  (ignoring "GTK backend does not currently support path-based graphics"))

#||
define sealed method restore-clipping-region
    (medium :: <gtk-medium>) => (record)
  ignoring("GTK does not support path-based graphics")
end method restore-clipping-region;
||#

(defmethod restore-clipping-region ((medium <gtk-medium>))
  (ignoring "GTK backend does not currently support path-based graphics"))

#||
define sealed method move-to
    (medium :: <gtk-medium>, x, y) => (record)
  ignoring("GTK does not support path-based graphics")
end method move-to;
||#

(defmethod move-to ((medium <gtk-medium>) x y)
  (declare (ignore x y))
  (ignoring "GTK backend does not currently support path-based graphics"))

#||
define sealed method line-to
    (medium :: <gtk-medium>, x, y) => (record)
  ignoring("GTK does not support path-based graphics")
end method line-to;
||#

(defmethod line-to ((medium <gtk-medium>) x y)
  (declare (ignore x y))
  (ignoring "GTK backend does not currently support path-based graphics"))

#||
define sealed method arc-to
    (medium :: <gtk-medium>, center-x, center-y,
     radius-1-dx, radius-1-dy, radius-2-dx, radius-2-dy,
     #key start-angle, end-angle) => (record)
  ignoring("GTK does not support path-based graphics")
end method arc-to;
||#

(defmethod arc-to ((medium <gtk-medium>) center-x center-y
		   radius-1-dx radius-1-dy radius-2-dx radius-2-dy
		   &key start-angle end-angle)
  (declare (ignore start-angle end-angle center-x center-y radius-1-dx radius-1-dy
		   radius-2-dx radius-2-dy))
  (ignoring "GTK backend does not currently support path-based graphics"))

#||
define sealed method curve-to
    (medium :: <gtk-medium>, x1, y1, x2, y2, x3, y3) => (record)
  ignoring("GTK does not support path-based graphics")
end method curve-to;
||#

(defmethod curve-to ((medium <gtk-medium>) x1 y1 x2 y2 x3 y3)
  (declare (ignore x1 y1 x2 y2 x3 y3))
  (ignoring "GTK backend does not currently support path-based graphics"))


#||
/// 'draw-pixmap', etc

define sealed method draw-pixmap
    (medium :: <gtk-medium>, pixmap :: <pixmap>, x, y,
     #key function = $boole-1) => (record)
  do-copy-area(pixmap, 0, 0, image-width(pixmap), image-height(pixmap),
	       medium, x, y)
end method draw-pixmap;
||#

(defmethod draw-pixmap ((medium <gtk-medium>)
			(pixmap <pixmap>)
			x y
			&key (function +boole-1+))
  (do-copy-area pixmap
                0 0
		(image-width pixmap)
		(image-height pixmap)
		medium x y
		:function function))


#||
define sealed method clear-box
    (medium :: <gtk-medium>, left, top, right, bottom) => ()
  let (drawable :: <GdkDrawable*>, gcontext :: <GdkGC*>)
    = get-gcontext(medium);
  let sheet = medium-sheet(medium);
  let transform = sheet-device-transform(sheet);
  with-device-coordinates (transform, left, top, right, bottom)
    gdk-window-clear-area(drawable, left, top, right - left, bottom - top)
  end
end method clear-box;
||#

(defmethod clear-box ((medium <gtk-medium>) left top right bottom)
  ;; We *want* to use the GTK gc for this I think; although if the 'standard'
  ;; background is different to the one DUIM expects, it might not do what
  ;; we think (might be the case for drawing-pane mirrors). FIXME.
  ;; Because we're using the GTK background for these panes, they must
  ;; be mirrored (and have permanent mediums?) -- all drawing-area panes
  ;; are. That means we can set the actual GTK gc's background to the
  ;; colour we want the background to be. (Also the case when we set
  ;; pixmaps). This should be safe for drawing panes because GTK won't
  ;; draw into them. I *guess* we never put buttons or anything into
  ;; drawing panes... FIXME. Need to provide gf + methods for setting
  ;; the background depending on if the sheet is a drawing area or not.
  ;; WE NEED "BORDER-PANES" TO PICK UP THE DEFAULT BG COLOUR
  ;; FOR GTK WINDOWS (FROM THE TOOLKIT). WE NEED DRAWING AREAS TO PICK
  ;; UP THE BG COLOUR SPECIFIED BY DUIM. HOW DO WE WORK OUT WHEN THE
  ;; BACKGROUND COLOUR CHANGES?
  (let* ((cr (get-context medium))
	 (sheet (medium-sheet medium))
	 (transform (sheet-device-transform sheet)))
    (cffi/cairo-save cr)

    (SET-CR-SOURCE-SURFACE CR MEDIUM :BACKGROUND? t)

    (with-device-coordinates (transform left top right bottom)
      ;; This also appears to be afflicted with the '- 1' size issue.
      ;; FIXME: What is the fix for this issue?
      (cffi/cairo_rectangle cr
			    (coerce left 'double-float)
			    (coerce top 'double-float)
			    (coerce (- right left) 'double-float)
			    (coerce (- bottom top) 'double-float))
      (cffi/cairo_clip cr)
      (cffi/cairo_paint cr))
    (cffi/cairo-restore cr)))



#||
/// Text drawing

define sealed method draw-text
    (medium :: <gtk-medium>, character :: <character>, x, y,
     #rest keys,
     #key start: _start, end: _end,
          align-x = #"left", align-y = #"baseline", do-tabs? = #f,
          towards-x, towards-y, transform-glyphs?) => (record)
  ignore(_start, _end, align-x, align-y, do-tabs?,
          towards-x, towards-y, transform-glyphs?);
  let string = make(<string>, size: 1, fill: character);
  apply(draw-text, medium, string, x, y, keys)
end method draw-text;
||#

(defmethod draw-text ((medium <gtk-medium>)
		      (character character)
		      x y
		      &rest keys
		      &key ((:start _start)) ((:end _end))
		      (align-x :left) (align-y :baseline) (do-tabs? nil)
		      towards-x towards-y transform-glyphs?)
  (declare (ignore _start _end align-x align-y do-tabs?
		   towards-x towards-y transform-glyphs?))
  (let ((string (make-string 1 :initial-element character)))
    (apply #'draw-text medium string x y keys)))


#||
//---*** What do we do about Unicode strings?
define sealed method draw-text
    (medium :: <gtk-medium>, string :: <string>, x, y,
     #key start: _start :: <integer> = 0, end: _end :: <integer> = size(string),
          align-x = #"left", align-y = #"baseline", do-tabs? = #f,
          towards-x, towards-y, transform-glyphs?) => (record)
  let text-style :: <text-style> = medium-merged-text-style(medium);
  let font :: <gtk-font> = text-style-mapping(port(medium), text-style);
  let length :: <integer> = size(string);
  let (drawable :: <GdkDrawable*>, gcontext :: <GdkGC*>)
    = update-drawing-state(medium, font: font);
  let transform = medium-device-transform(medium);
  with-device-coordinates (transform, x, y)
    when (towards-x & towards-y)
      convert-to-device-coordinates!(transform, towards-x, towards-y)
    end;
    //---*** What about x and y alignment?
    if (do-tabs?)
      ignoring("draw-text with do-tabs?: #t");
      /*---*** Not yet implemented!
      let tab-width  = text-size(medium, " ") * 8;
      let tab-origin = if (do-tabs? == #t) x else do-tabs? end;
      let x = 0;
      let s = _start;
      block (break)
	while (#t)
	  let e = position(string, '\t', start: s, end: _end);
	  //---*** It would be great if 'with-c-string' took start & end!
	  let substring = copy-sequence(string, start: s, end: e);
	  with-c-string (c-string = substring)
	    gdk-draw-text(drawable, font, gcontext,
			  tab-origin + x, y, string, e - s)
	  end;
	  if (e = _end)
	    break()
	  else
	    let (x1, y1, x2, y2) = GET-STRING-EXTENT(drawable, string, font, s, e);
	    ignore(x1, y1, y2);
	    x := floor/(x + x2 + tab-width, tab-width) * tab-width;
	    s := min(e + 1, _end)
	  end
	end
      end
      */
    else
      ignoring("draw-text");
      /*---*** Fonts not working yet!
      //---*** It would be great if 'with-c-string' took start & end!
      let substring
	= if (_start = 0 & _end = length) string
	  else copy-sequence(string, start: _start, end: _end) end;
      with-c-string (c-string = substring)
	gdk-draw-string(drawable, font, gcontext,
			x, y, c-string)
      end
      */
    end
  end
end method draw-text;
||#

;;---*** What to do about Unicode strings?
;; FIXME: NEED TO CACHE A BUNCH OF THE INTERMEDIATE (PANGO) OBJECTS WE CREATE
;; IN THIS METHOD!
#||
Notes:

The align-x + align-y tell us where the x and y relate to, relative to the
bounds of the drawn text.

align-x = left: x gives the coordinate of the left side of the bounding box
align-x = right: x gives the coordinate of the right side of the bounding box
align-x = center: x gives the coordinate of the center of the horizontal
                  axis of the bounding box

align-y = top: y gives the coordinate of the top of the bounding box
align-y = bottom: y gives the coordinate of the bottom of the bounding box
align-y = center: y gives the coordinate of the center of the vertical axis
                  of the bounding box
align-y = baseline: y gives the coordinate for the vertical position of the
                    *baseline* of the text.
||#

(defmethod draw-text ((medium <gtk-medium>) (string string) x y
		      &key ((:start _start) 0) ((:end _end) (length string))
		      (align-x :left) (align-y :baseline) (do-tabs? nil)
			towards-x towards-y transform-glyphs?)
  (declare (ignore do-tabs? transform-glyphs?))
  (let* ((text-style (medium-merged-text-style medium))
	 (font (text-style-mapping (port medium) text-style))
	 ;; The following are just so we can get a handle on the widget onto which
	 ;; we're drawing. Might be an easier way to get this, maybe from GTK...
	 ;; FIXME: DO THIS BETTER...?
	 (sheet  (medium-sheet medium))
	 (mirror (sheet-mirror sheet))
	 (widget (mirror-widget mirror)))
    (let* ((cr (update-drawing-state medium :font font))
	   (transform (medium-device-transform medium)))

      (SET-CR-SOURCE-SURFACE CR MEDIUM)

      ;; FIXME: GET THIS CONTEXT FROM THE CAIRO_T?
      ;; Context is owned by the widget; don't free it
      (let* ((context (cffi/gtk-widget-get-pango-context widget))
	     ;; TODO: cache the layout?
	     (layout  (cffi/pango-layout-new context)))
	(when (null layout)
	  (error "Null Pango layout whilst attempting to draw on medium ~a" medium))
	(cffi/pango-layout-set-font-description layout (%pango-font-description font))
	;; This could be a <device-font>!
	(unless (device-font? text-style)
	  (multiple-value-bind (family name weight slant size underline? strikeout?)
	      (text-style-components text-style)
	    (declare (ignore family name weight slant size))
	    (when (or underline? strikeout?)
	      (let ((st-attr  (cffi/pango-attr-strikethrough-new +true+))
		    (ul-attr  (cffi/pango-attr-underline-new +true+))       ; method takes a "PangoUnderline".
		    (list-ptr (cffi/pango-attr-list-new)))
		(cffi/pango-attr-list-insert list-ptr st-attr)
		(cffi/pango-attr-list-insert list-ptr ul-attr)
		(cffi/pango-layout-set-attributes layout list-ptr)
		(cffi/pango-attr-list-unref list-ptr)))))
	(multiple-value-bind (font width height ascent descent)
	    (gtk-font-metrics font :pango-context context)
	  (declare (ignore font width height descent))
	  (with-device-coordinates (transform x y)
	    (when (and towards-x towards-y)
	      (not-yet-implemented "[GTK-DRAW] DRAW-TEXT with non-axis aligned text")
	      ;; TODO: set up a pango matrix to do the rotation
	      ;; See "pango_matrix_rotate" and friends.
	      ;; FIXME -- need to write another test that does rotated, underlined,
	      ;; struckthrough text. Also one which renders the text bounding box +
	      ;; baseline etc. so there's a visual on how that works.
	      (convert-to-device-coordinates! transform towards-x towards-y))
	    ;; FIXME -- do something with this
	    #-(and)
	    (when do-tabs?
	      ;; Set some Tab Stops up, then use "pango_layout_set_tabs". Pango
	      ;; should deal with tabs in the text automatically after that.
	      (ignoring "[GTK-DRAW] - ignoring do-tabs? (~a)" do-tabs?))
	    (let ((substring (subseq string _start _end)))
	      ;; 'baseline' in the following is the baseline of the first line in the
	      ;; case there are multiple lines
	      (multiple-value-bind (x1 y1 rw rh baseline)
		  (%measure-text layout substring)
		(declare (ignore x1 y1 baseline))
		;; Work out where this puppy is going to be drawn... note that x1, y1 could be
		;; negative - FIXME
		;; ecase because I want to know if I haven't covered all the values...
		(let ((pos-x (ecase align-x
			       (:left   x)
			       ((:center :centre) (- x (floor rw 2)))
			       (:right  (- x rw))))
		      (pos-y (ecase align-y
			       ;; FIXME: These are probably not right... especially not for blocks
			       ;; of text.
			       (:top      y)
			       ((:center :centre) (- y (floor rh 2)))
			       (:bottom   (- y rh))
			       (:baseline (- y ascent)))))
		  (cffi/cairo_move_to cr (coerce pos-x 'double-float) (coerce pos-y 'double-float))
		  (cffi/pango_cairo_show_layout cr layout)
		  (cffi/g-object-unref layout))))))))))



