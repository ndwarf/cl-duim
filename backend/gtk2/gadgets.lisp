;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GTK2-DUIM -*-
(in-package #:gtk2-duim)

#||
Module:       gtk-duim
Synopsis:     GTK gadget implementation
Author:       Andy Armstrong, Scott McKay
Copyright:    Original Code is Copyright (c) 1995-2004 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND
||#

;; TODO: <<gtk-widget-protocol>>, <<gtk-gadget-protocol>>,
;;       <<gtk-tree-view-control-protocol>> etc.

(defgeneric gadget-widget (gadget)
  (:documentation
"
 Returns the native widget associated with _gadget_.
"))


;;; This is the widget's *preferred* size. It's NOT the widget's
;;; *allocated* size
(defgeneric widget-size (widget)
  (:documentation
"
 Returns the width and height from _widget_'s size request as two
 values.

 See also: gtk-widget-size-request, gtk-space-requirements
"))

(defgeneric defaulted-gadget-label (gadget))
(defgeneric update-mirror-label (gadget mirror))

;;; Can't a gadget's label have text AND an image? What about menu items in DUIM?
;;; Can't these have icons on them either?
(defgeneric text-or-image-from-gadget-label (gadget))

(defgeneric activate-gtk-gadget (gadget))
(defgeneric widget-range-bounds (widget range))
(defgeneric update-mirror-text (gadget mirror))
(defgeneric %set-text-mirror-text (widget text))
(defgeneric %get-text-mirror-text (widget))
(defgeneric %configure-mirror-columns (gadget mirror))
(defgeneric gadget-range-values (gadget))
(defgeneric scroll-bar-adjusted-contents (gadget))
(defgeneric %list-control-update-gtk-model (gadget widget store))

#||
/// Useful constants

define constant $default-label = "";
define constant $button-box-x-spacing = 3;
define constant $button-box-y-spacing = 3;
||#

(defparameter *default-label* "")
(defconstant +button-box-x-spacing+ 3)
(defconstant +button-box-y-spacing+ 3)


#||
/// GTK gadgets

define class <gadget-mirror> (<widget-mirror>)
end class <gadget-mirror>;
||#

(defclass <gtk-gadget-mirror> (<gtk-widget-mirror>) ())

#||
define open abstract class <gtk-gadget-mixin>
    (<gadget>,
     <gtk-pane-mixin>)
  // sealed constant each-subclass slot %gtk-fixed-width? :: <boolean> = #f,
  //   init-keyword: gtk-fixed-width?:;
  // sealed constant each-subclass slot %gtk-fixed-height? :: <boolean> = #f,
  //   init-keyword: gtk-fixed-height?:;
end class <gtk-gadget-mixin>;
||#

(defclass <gtk-gadget-mixin>
    (<gadget>
     <gtk-pane-mixin>)
  ())

;;; <GTK-GADGET-MIXIN> DEALS WITH THE FOLLOWING:-
;;;    <<GADGET-PROTOCOL>>
;;;        gadget-enabled?        via UPDATE-MIRROR-ATTRIBUTES, NOTE-GADGET-ENABLED/DISABLED
;;;        gadget-documentation   ditto
;;;        gadget-label (general) via UPDATE-MIRROR-LABEL. Overload NOTE-GADGET-LABEL-CHANGED instead?


#||
define open generic %gtk-fixed-width?
    (gadget :: <gtk-gadget-mixin>)
 => (fixed? :: <boolean>);
||#

(defgeneric %gtk-fixed-width? (gadget))


#||
define method %gtk-fixed-width?
    (gadget :: <gtk-gadget-mixin>)
 => (fixed? :: <boolean>);
  #f;
end method;
||#

(defmethod %gtk-fixed-width? ((gadget <gtk-gadget-mixin>))
  nil)


#||
define open generic %gtk-fixed-height?
    (gadget :: <gtk-gadget-mixin>)
 => (fixed? :: <boolean>);
||#

(defgeneric %gtk-fixed-height? (gadget))


#||
define method %gtk-fixed-height?
    (gadget :: <gtk-gadget-mixin>)
 => (fixed? :: <boolean>);
  #f;
end method;
||#

(defmethod %gtk-fixed-height? ((gadget <gtk-gadget-mixin>))
  nil)


#||
define method gadget-widget (sheet :: <gtk-gadget-mixin>)
 => (widget)
  let mirror = sheet-mirror(sheet);
  let widget = mirror & mirror-widget(mirror);
  widget
end;
||#

(defmethod gadget-widget ((sheet <gtk-gadget-mixin>))
  (let* ((mirror (sheet-mirror sheet))
	 (widget (and mirror (mirror-widget mirror))))
    widget))


#||
/*---*** Not used yet!
define method widget-attributes
    (_port :: <gtk-port>, gadget :: <gtk-gadget-mixin>)
 => (foreground, background, font)
  let foreground :: <color> = get-default-foreground(_port, gadget);
  let background :: <color> = get-default-background(_port, gadget);
  let text-style = get-default-text-style(_port, gadget);
  let foreground
    = if (default-foreground(gadget))
	vector(foreground:, allocate-color(foreground, port-default-palette(_port)))
      else
	#[]
      end;
  let background
    = if (default-background(gadget))
	vector(background:, allocate-color(background, port-default-palette(_port)))
      else
	#[]
      end;
  let font
    = if (default-text-style(gadget))
	vector(text-style:, text-style-mapping(_port, text-style).%font-name)
      else
	#[]
      end;
  values(foreground, background, font)
end method widget-attributes;
*/


define method do-compose-space
    (gadget :: <gtk-gadget-mixin>, #key width, height)
 => (space-req :: <space-requirement>)
//  debug-message("do-compose-space(%= , %d, %d)", gadget, width, height);
  let mirror = sheet-direct-mirror(gadget);
  if (mirror)
    let widget = GTK-WIDGET(mirror-widget(mirror));
    gtk-space-requirements(gadget, widget)
  else
    gtk-debug("Composing space on an unmirrored gadget!");
    default-space-requirement(gadget, width: width, height: height)
  end
end method do-compose-space;
||#

(defmethod do-compose-space ((gadget <gtk-gadget-mixin>) &key width height)
  (let ((mirror (sheet-direct-mirror gadget)))
    (cond (mirror
	   ;; Take the gtk space request as the default; if these are not
	   ;; sufficient, add DO-COMPOSE-SPACE methods to specific gadgets
	   ;; that need more twiddling.
	   (let ((widget (mirror-widget mirror)))
	     (multiple-value-bind (width height)
		 (widget-size widget)
	       (let ((max-width  (if (%gtk-fixed-width?  gadget) width  +fill+))
		     (max-height (if (%gtk-fixed-height? gadget) height +fill+)))
		 (make-instance '<space-requirement>
				:min-width  width
				:width      width
				:max-width  max-width
				:min-height height
				:height     height
				:max-height max-height)))))
	  (t
	   (gtk-debug "Composing space on an unmirrored gadget!")
	   (default-space-requirement gadget :width width :height height)))))


#||
// We take the values suggested by GTK as the default sizes
define method gtk-space-requirements
    (gadget :: <gtk-gadget-mixin>, widget :: <GtkWidget*>)
 => (space-req :: <space-requirement>)
  let (width, height) = widget-size(widget);
  let max-width  = if (gadget.%gtk-fixed-width?)  width  else $fill end;
  let max-height = if (gadget.%gtk-fixed-height?) height else $fill end;
  make(<space-requirement>,
       min-width:  width,  width:  width,  max-width:  max-width,
       min-height: height, height: height, max-height: max-height)
end method gtk-space-requirements;


define method widget-size
    (widget :: <GtkWidget*>)
 => (width :: <integer>, height :: <integer>)
  with-stack-structure (request :: <GtkRequisition*>)
    gtk-widget-size-request(widget, request);
    values(request.width-value, request.height-value)
  end
end method widget-size;
||#

(defmethod widget-size (widget)
  (cffi/with-stack-structure (request '(:struct GtkRequisition))
    (cffi/gtk-widget-size-request widget request)
    (let ((w (cffi/gtkrequisition->width request))
	  (h (cffi/gtkrequisition->height request)))
      (values w h))))


#||
define sealed method defaulted-gadget-label
    (gadget :: <gadget>) => (label)
  gadget-label(gadget) | $default-label
end method defaulted-gadget-label;
||#

(defmethod defaulted-gadget-label ((gadget <gadget>))
  (or (gadget-label gadget) *default-label*))


;; This method on <gtk-gadget-mixin> disables the gadget if it is not
;; enabled, and sets up tooltips if the gadget has documentation.
(defmethod update-mirror-attributes ((gadget <gtk-gadget-mixin>) (mirror <gtk-mirror>))
  (call-next-method)
  (let ((documentation (gadget-documentation gadget))
	(widget (mirror-widget mirror)))
    (and documentation (register-tooltip-for-sheet gadget documentation))
    (unless (gadget-enabled? gadget)
      (when widget
	(cffi/gtk-widget-set-sensitive widget +false+)))))


(defmethod (setf gadget-documentation) (documentation (gadget <gtk-gadget-mixin>))
  (unregister-tooltip-for-sheet gadget)
  (call-next-method)
  (and documentation (register-tooltip-for-sheet gadget documentation))
  documentation)


(defun unregister-tooltip-for-sheet (gadget)
  (let* ((widget (gadget-widget gadget)))
    (cffi/gtk-widget-set-tooltip-text widget nil)))


(defun register-tooltip-for-sheet (gadget documentation)
  (let* ((widget (gadget-widget gadget)))
    (cffi/gtk-widget-set-tooltip-text widget documentation)))


#||
define sealed method note-gadget-label-changed
    (gadget :: <gtk-gadget-mixin>) => ()
  next-method();
  let mirror = sheet-direct-mirror(gadget);
  mirror & update-mirror-label(gadget, mirror)
end method note-gadget-label-changed;
||#

(defmethod note-gadget-label-changed ((gadget <gtk-gadget-mixin>))
  (call-next-method)
  (let ((mirror (sheet-direct-mirror gadget)))
    (and mirror (update-mirror-label gadget mirror))))


#||
define sealed method update-mirror-label
    (gadget :: <gtk-gadget-mixin>, mirror :: <gadget-mirror>) => ()
  let widget = mirror-widget(sheet-direct-mirror(gadget));
  let label  = defaulted-gadget-label(gadget);
  let label :: <string> = if (instance?(label, <string>)) label else "" end;
  // xt/XtSetValues(widget, label-string: label)
  ignoring("update-mirror-label")
end method update-mirror-label;
||#

(defmethod update-mirror-label ((gadget <gtk-gadget-mixin>) (mirror <gtk-gadget-mirror>))
  (ignoring "update-mirror-label on ~a (mirror=~a)" gadget mirror))


#||
define sealed method text-or-image-from-gadget-label
    (gadget :: <gadget>)
 => (text :: false-or(<string>), image :: false-or(<image>),
     mnemonic :: false-or(<mnemonic>), index :: false-or(<integer>));
  let label = defaulted-gadget-label(gadget);
  let (label, mnemonic, index)
    = compute-mnemonic-from-label(gadget, label);
  let mnemonic = mnemonic & as-uppercase(gesture-character(mnemonic));
  select (label by instance?)
    <string> =>
      values(add-gadget-label-postfix(gadget, label),
	     #f, mnemonic, index);
/*---*** Not ready yet!
    <gtk-bitmap>, <gtk-icon> =>
      values(if (mnemonic) as(<string>, vector(mnemonic)) else "" end,
	     label, mnemonic, index);
*/
    <image> =>
      //---*** Decode the image and return a pixmap or something
      values("<image>",
	     #f, mnemonic, index);
  end
end method text-or-image-from-gadget-label;
||#

(defmethod text-or-image-from-gadget-label ((gadget <gadget>)) ;; => text, image, mnemonic, index
  (let ((label (defaulted-gadget-label gadget)))
    (multiple-value-bind (label mnemonic index)
	(compute-mnemonic-from-label gadget label)
      (let ((mnemonic (and mnemonic (char-upcase (gesture-character mnemonic)))))
	(etypecase label
	  (string (values label nil mnemonic index))
#||---*** Not ready yet!
          ((<gtk-bitmap> <gtk-icon>)
	   (values (if mnemonic (coerce 'string (vector mnemonic)) "")
	           label mnemonic index))
||#       ;; What about <gtk-bitmap>, <gtk-icon> (which don't yet exist...)?
	  (<gtk-pixmap>
	   #-(and)
	   (gtk-debug "[GTK-GADGETS TEXT-OR-IMAGE-FROM-GADGET-LABEL] got <gtk-pixmap> label")
	   (values (if mnemonic (coerce 'string (vector mnemonic)) "")
		   label mnemonic index))
	  (<image>
	   #-(and)
	   (gtk-debug "[GTK-GADGETS TEXT-OR-IMAGE-FROM-GADGET-LABEL] got <image> label")
;;;	   (multiple-value-bind (colour fillstyle operation gtk-pixmap)
;;;	       (decode-ink medium nil label)
;;;	     (declare (ignore colour fillstyle operation))
	   (values (if mnemonic (coerce 'string (vector mnemonic)) "")
		   #|gtk-pixmap|# nil mnemonic index)))))))


#||
define sealed method note-gadget-enabled
    (client, gadget :: <gtk-gadget-mixin>) => ()
  ignore(client);
  next-method();
  let widget = GTK-WIDGET(gadget-widget(gadget));
  gtk-widget-set-sensitive(widget, $true)
end method note-gadget-enabled;
||#

(defmethod note-gadget-enabled (client (gadget <gtk-gadget-mixin>))
  (declare (ignore client))
  (call-next-method)
  (let ((widget (gadget-widget gadget)))
    (cffi/gtk-widget-set-sensitive widget +true+)))


#||
define sealed method note-gadget-disabled
    (client, gadget :: <gtk-gadget-mixin>) => ()
  ignore(client);
  next-method();
  let widget = GTK-WIDGET(gadget-widget(gadget));
  gtk-widget-set-sensitive(widget, $false)
end method note-gadget-disabled;
||#

(defmethod note-gadget-disabled (client (gadget <gtk-gadget-mixin>))
  (declare (ignore client))
  (call-next-method)
  (let ((widget (gadget-widget gadget)))
    (cffi/gtk-widget-set-sensitive widget +false+)))


#||
//---*** DO WE NEED THIS?
define sealed method activate-gtk-gadget
    (gadget :: <action-gadget>) => (activated? :: <boolean>)
  when (gadget-activate-callback(gadget))
    distribute-activate-callback(gadget);
    #t
  end
end method activate-gtk-gadget;
||#

(defmethod activate-gtk-gadget ((gadget <action-gadget>))
  #-(and)
  (gtk-debug "[GTK-GADGETS ACTIVATE-GTK-GADGET] invoked on <ACTION-GADGET> ~a" gadget)
  (when (gadget-activate-callback gadget)
    (distribute-activate-callback gadget)
    t))


#||
//---*** DO WE NEED THIS?
define sealed method activate-gtk-gadget
    (gadget :: <text-field>) => (activated? :: <boolean>)
  handle-text-gadget-changed(gadget);
  next-method()
end method activate-gtk-gadget;
||#

(defmethod activate-gtk-gadget ((gadget <text-field>))
  #-(and)
  (gtk-debug "[GTK-GADGETS ACTIVATE-GTK-GADGET] invoked on <TEXT-FIELD> ~a" gadget)
  (handle-text-gadget-changed gadget)
  (call-next-method))



#||
/// Exit, cancel, default button, etc.

/*---*** Do we need any of this?
define method handle-command-for-id
    (sheet :: <sheet>, id :: <integer>) => (handled? :: <boolean>)
  let frame = sheet-frame(sheet);
  select (id)
    $IDOK => 
      duim-debug-message("Handling IDOK for %=", sheet);
      activate-default-button(frame);
    $IDCANCEL =>
      duim-debug-message("Handling IDCANCEL for %=", sheet);
      handle-cancel(frame);
    otherwise =>
      let gadget = id->gadget(sheet, id);
      if (gadget)
	handle-gadget-activation(gadget)
      else
	handle-id-activation(frame, id)
      end;
  end
end method handle-command-for-id;


define sealed method handle-gadget-activation
    (gadget :: <gadget>) => (handled? :: <boolean>)
  duim-debug-message("Ignoring activation command for gadget %=", gadget);
  #f
end method handle-gadget-activation;


// This handles IDOK commands for more than just buttons...
define method activate-default-button
    (frame :: <frame>) => (activated? :: <boolean>)
  let gadget = gtk-sheet-with-focus();
  duim-debug-message("  Handling IDOK: focus currently %=", gadget);
  let activated? = instance?(gadget, <action-gadget>)
		   & gadget-enabled?(gadget)
		   & activate-gtk-gadget(gadget);
  // If we didn't activate the gadget, try to activate the default button
  unless (activated?)
    let button = frame-default-button(frame);
    // Don't activate an upmapped or disabled default button...
    when (button & sheet-mapped?(button) & gadget-enabled?(button))
      handle-gadget-activation(button)
    end
  end
end method activate-default-button;


XXX---*** Do we need this too?
define function gtk-sheet-with-focus
    () => (sheet :: false-or(<sheet>))
  let handle = GetFocus();
  let mirror = gadget-mirror(handle);
  when (mirror)
    let sheet = mirror-sheet(mirror);
    if (instance?(sheet, <gtk-subgadget-mixin>))
      subgadget-owner(sheet)
    else
      sheet
    end
  end
end function gtk-sheet-with-focus;


define function handle-cancel
    (frame :: <frame>) => (handled? :: <boolean>)
  let gadget = gtk-sheet-with-focus();
  duim-debug-message("  Handling IDCANCEL: focus currently %=", gadget);
  if (instance?(gadget, <gadget>) & cancel-gadget(gadget))
    #t
  else
    cancel-frame(frame)
  end
end function handle-cancel;


define sealed method cancel-frame
    (frame :: <frame>) => (handled? :: <boolean>)
  //---*** We should handle ESCAPE as canceling popups by default,
  //---*** for example in combo boxes.
  #f
end method cancel-frame;


define sealed method cancel-gadget 
    (gadget :: <gadget>) => (handled? :: <boolean>)
  #f
end method cancel-gadget;
*/

//---*** What do we do about setting the color and font of a gadget?
||#

#||
/// Labels

define sealed class <gtk-label> 
    (<gtk-gadget-mixin>,
     <label>,
     <leaf-pane>,
     <sealed-constructor-mixin>)
  keyword gtk-fixed-width?:  = #t;
  keyword gtk-fixed-height?: = #t;
end class <gtk-label>;
||#

(defclass <gtk-label>
    (<gtk-gadget-mixin>
     <label>
     <leaf-pane>)
  ;; Is the label an image or text? Expected values are nil, :image, or :text.
  ((%label-type :type symbol :accessor %label-type))
  (:default-initargs :gtk-fixed-width? t :gtk-fixed-height? t))


#||
define method %gtk-fixed-width?
    (gadget :: <gtk-label>)
 => (fixed? :: <boolean>)
  #t;
end method;
||#

(defmethod %gtk-fixed-width? ((gadget <gtk-label>))
  t)


#||
define method %gtk-fixed-height?
    (gadget :: <gtk-label>)
 => (fixed? :: <boolean>)
  #t;
end method;
||#

(defmethod %gtk-fixed-height? ((gadget <gtk-label>))
  t)


#||
define sealed method class-for-make-pane 
    (framem :: <gtk-frame-manager>, class == <label>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<gtk-label>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <gtk-frame-manager>)
				(class (eql (find-class '<label>)))
				&key)
  (values (find-class '<gtk-label>) nil))


#||
define sealed method make-gtk-mirror
    (gadget :: <gtk-label>)
 => (mirror :: <gadget-mirror>)
  with-c-string (c-string = defaulted-gadget-label(gadget))
    let widget = GTK-LABEL(gtk-label-new(c-string));
    assert(~null-pointer?(widget), "gtk-label-new failed");
    make(<gadget-mirror>,
	 widget: widget,
	 sheet:  gadget)
  end
end method make-gtk-mirror;
||#

(defmethod make-gtk-mirror ((gadget <gtk-label>))
  (multiple-value-bind (text image mnemonic index)
      (text-or-image-from-gadget-label gadget)
    ;; Mnemonics?
    (declare (ignore mnemonic index))
    (let* ((mask   nil)
	   (widget (if image
		       (cffi/gtk-image-new-from-pixmap (pixmap-to-gdk-pixmap image) mask)
		       (cffi/gtk-label-new text))))
      (when (null widget)
	(error "(MAKE-GTK-MIRROR <GTK-LABEL>) failed for gadget ~a" gadget))
      ;; Store the type of the label in the gadget so we know if it needs
      ;; updating whether it currently holds an image or text.
      (setf (%label-type gadget) (if image :image :text))
      (make-gtk-widget-mirror-instance (find-class '<gtk-gadget-mirror>)
				       :widget widget
				       :sheet  gadget))))


(defmethod port-handles-repaint? ((port <gtk-port>) (gadget <gtk-label>))
  t)


#||
define sealed method update-mirror-label
    (gadget :: <gtk-label>, mirror :: <gadget-mirror>) => ()
  with-c-string (c-string = defaulted-gadget-label(gadget))
    let widget = GTK-LABEL(mirror-widget(mirror));
    gtk-label-set-text(widget, c-string)
  end
end method update-mirror-label;
||#

(defmethod update-mirror-label ((gadget <gtk-label>) (mirror <gtk-gadget-mirror>))
  (let ((label-type-changed? nil))
    (multiple-value-bind (text image mnemonic index)
	(text-or-image-from-gadget-label gadget)
      (declare (ignore mnemonic index))
      (let ((type   (%label-type gadget))
	    (widget (mirror-widget mirror))
	    (mask   nil))
	(labels ((swap-label (mirror new-label)
		   ;; TODO: Write a test for swapping between text + image labels!
		   ;; XXX: Does the old mirror need to be hidden?
		   ;; XXX: Is this memory management sufficient? Do we also need to
		   ;; free the gdk-pixmap?
		   (cffi/g-object-unref widget)
		   (setf widget new-label)
		   (setf (mirror-widget mirror) widget)
		   ;; XXX: Does the new mirror need to be shown?
		   (setf label-type-changed? t)))
	  ;; If we have an image and the current label holds an image, update it
	  ;; If we have an image and the current label holds text, swap the text label
	  ;; for an image label
	  (cond (image (if (eql type :image)
			   (cffi/gtk-image-set-from-pixmap widget image mask)
			   (progn
			     (swap-label mirror (cffi/gtk-label-new text))
			     (setf (%label-type gadget) :text))))
		;; If we have text and the current label holds text, update it
		;; If we have text and the current label holds an image, swap the image label
		;; for a text label.
		(text  (if (eql type :text)
			   (cffi/gtk-label-set-text widget text)
			   (progn
			     (swap-label mirror (cffi/gtk-image-new-from-pixmap (pixmap-to-gdk-pixmap image) mask))
			     (setf (%label-type gadget) :image))))
		(t     (error "UPDATE-MIRROR-LABEL called to update gadget mirror with no label!"))))))))



#||
/// Separators
/*---*** Use the default separators
XXX: write a separator test
define sealed class <gtk-separator>
    (<gtk-gadget-mixin>,
     <separator>,
     <leaf-pane>,
     <sealed-constructor-mixin>)
end class <gtk-separator>;


define sealed method class-for-make-pane
    (framem :: <gtk-frame-manager>, class == <separator>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<gtk-separator>, #f)
end method class-for-make-pane;


define sealed method make-gtk-mirror
    (gadget :: <gtk-separator>)
 => (mirror :: <gadget-mirror>)
  let parent = sheet-device-parent(gadget);
  let parent-widget = gadget-widget(parent);
  let (foreground, background, font) = widget-attributes(_port, gadget);
  ignore(font);
  let resources
    = vector(mapped-when-managed:, #f);
  let widget
    = xt/XtCreateManagedWidget("DUIMSeparator", xm/<XmSeparatorGadget>, parent-widget,
			       resources:
				 concatenate(resources, foreground, background));
  values(widget, #f)
end method make-gtk-mirror;


define sealed method do-compose-space
    (pane :: <gtk-separator>, #key width, height)
 => (space-requirement :: <space-requirement>)
  select (gadget-orientation(pane))
    #"horizontal" =>
      make(<space-requirement>,
	   min-width: 1, width: width | 1, max-width: $fill,
	   height: 2);
    #"vertical" =>
      make(<space-requirement>,
	   width: 2,
	   min-height: 1, height: height | 1, max-height: $fill);
  end
end method do-compose-space;
*/



/// Text gadgets

define gtk-type-cast-function GTK-EDITABLE => <GtkEditable*>;

// --- TODO: should this be unicode?
define gtk-type-cast-function GTK-STRING => <c-string>;
define method gtk-copy-text (text :: <c-string>) => (str :: <string>)
  // Convert to gc'able string.
  as(<byte-string>, text)
end;


// Mixin class for text fields, password fields, and text editors, i.e.
// all GtkEditable objects.
define open abstract class <gtk-text-gadget-mixin>
    (<gtk-gadget-mixin>,
     <text-field>)
  sealed slot %changed? :: <boolean> = #f;
  sealed constant slot %current-selection :: <simple-text-range>
    = make(<text-range>, start: -1, end: -1);
end class <gtk-text-gadget-mixin>;
||#

#||
   CPL for <GTK-TEXT-GADGET-MIXIN> - text-field, action-gadget, changing-value-gadget, value-gadget, text-gadget

  (#<STANDARD-CLASS GTK-DUIM:<GTK-TEXT-GADGET-MIXIN>>
   #<STANDARD-CLASS GTK-DUIM:<GTK-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<TEXT-FIELD>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<BORDERED-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<ACTION-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<ACTION-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<CHANGING-VALUE-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<VALUE-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<TEXT-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<VALUE-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<BASIC-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<ABSTRACT-GADGET>>
   #<STANDARD-CLASS GTK-DUIM:<GTK-PANE-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<STANDARD-INPUT-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<SHEET-WITH-EVENT-QUEUE-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<MIRRORED-SHEET-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<SHEET-WITH-RESOURCE-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS:<ABSTRACT-SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<EVENT-HANDLER>>
   #<STANDARD-CLASS STANDARD-OBJECT>
   #<BUILT-IN-CLASS T>)
||#

;;; <GTK-TEXT-GADGET-MIXIN> deals with:-
;;;
;;;     <<action-gadget-protocol>>
;;;         handle-gtk-activate-signal
;;;         handle-gtk-changed-signal
;;;     <<changing-value-gadget-protocol>>
;;;         handle-text-gadget-changed
;;;         handle-gtk-text-gadget-changing
;;;     <<text-field-protocol>>
;;;         note-gadget-text-changed
;;;         text-selection
;;;         (setf text-selection)
;;;         selected-text
;;;         text-caret-position
;;;         (setf text-caret-position)

(defclass <gtk-text-gadget-mixin>    ;; FIXME:ABSTRACT
    (<gtk-gadget-mixin>
     <text-field>)
  ((%changed? :type boolean
	      :initform nil
	      :accessor %changed?)
   (%current-selection :type <simple-text-range>
		       :initform (make-text-range :start -1 :end -1)
		       :reader %current-selection)))

#||
define method install-event-handlers
    (sheet :: <gtk-text-gadget-mixin>, mirror :: <gadget-mirror>) => ()
  next-method();
  install-named-handlers(mirror, #[#"changed", #"activate"]);
end method install-event-handlers;


// #"activate" signal
define method gtk-activate-signal-handler (gadget :: <gtk-text-gadget-mixin>,
					   user-data :: <gpointer>)
  ignore(user-data);
  activate-gtk-gadget(gadget);
end;
||#

(defmethod handle-gtk-activate-signal ((gadget <gtk-text-gadget-mixin>)
				       widget
				       user-data)
  (declare (ignore widget user-data))
  (activate-gtk-gadget gadget))


#||
// #"changed" signal
define method gtk-changed-signal-handler (gadget :: <gtk-text-gadget-mixin>,
					  user-data :: <gpointer>)
  ignore(user-data);
  handle-text-gadget-changing(gadget);
end;
||#

(defmethod handle-gtk-changed-signal ((gadget <gtk-text-gadget-mixin>)
				      widget
				      user-data)
  (declare (ignore widget user-data))
  (handle-text-gadget-changing gadget))


#||
define sealed method update-mirror-attributes
    (gadget :: <gtk-text-gadget-mixin>, mirror :: <gadget-mirror>) => ()
  next-method();
  // Set the initial text selection
  text-selection(gadget) := gadget.%current-selection
end method update-mirror-attributes;
||#

(defmethod update-mirror-attributes ((gadget <gtk-text-gadget-mixin>)
				     (mirror <gtk-gadget-mirror>))
  (call-next-method)
  ;; Set the initial text selection
  (setf (text-selection gadget) (%current-selection gadget)))


#||
// This is called right after gadget buffer text changes in DUIM
define sealed method note-gadget-text-changed 
    (gadget :: <gtk-text-gadget-mixin>) => ()
  gtk-debug("note-gadget-text-changed");
  next-method();
  let mirror = sheet-direct-mirror(gadget);
  mirror & update-gadget-text(gadget, mirror)
end method note-gadget-text-changed;
||#

(defmethod note-gadget-text-changed ((gadget <gtk-text-gadget-mixin>))
  (call-next-method)
  (let ((mirror (sheet-direct-mirror gadget)))
    (and mirror (update-mirror-text gadget mirror))))


#||
define sealed method handle-text-gadget-changing
    (gadget :: <gtk-text-gadget-mixin>) => ()
  gtk-debug("handle-text-gadget-changing");
  let old-text = gadget.gadget-text-buffer;
  let widget = GTK-EDITABLE(gadget-widget(gadget));
  // --- TODO: use a stretchy buffer to avoid copying on each character?
  let chars = GTK-STRING(gtk-editable-get-chars(widget, 0, -1));
  let new-text = unless (old-text = chars)
		   gadget.gadget-text-buffer := gtk-copy-text(chars);
		 end;
  g-free(chars);
  when (new-text)
    gadget.%changed? := #t;
    distribute-text-changing-callback(gadget, new-text)
  end;
end method handle-text-gadget-changing;
||#


(defmethod handle-text-gadget-changing ((gadget <gtk-text-gadget-mixin>))
  (let ((old-text (gadget-text-buffer gadget))
	(widget   (gadget-widget gadget)))
    ;; --- TODO: use a stretchy buffer to avoid copying on each character?
    (let* ((chars    (cffi/gtk-editable-get-chars widget 0 -1))
	   (new-text (unless (equal? old-text chars)
		       (setf (gadget-text-buffer gadget) chars))))
      (when new-text
	(setf (%changed? gadget) t)
	(distribute-text-changing-callback gadget new-text)))))


#||
define sealed method handle-text-gadget-changed
    (gadget :: <gtk-text-gadget-mixin>) => ()
  gtk-debug("handle-text-gadget-changed");
  when (gadget.%changed?)
    let text = gadget-text-buffer(gadget);
    distribute-text-changed-callback(gadget, text);
    gadget.%changed? := #f
  end
end method handle-text-gadget-changed;
||#

;;; This is invoked from the "activate-gtk-gadget" method (i.e. it's what happens
;;; when the text gadget is activated).

(defmethod handle-text-gadget-changed ((gadget <gtk-text-gadget-mixin>))
  (when (%changed? gadget)
    (let ((text (gadget-text-buffer gadget)))
      (distribute-text-changed-callback gadget text)
      (setf (%changed? gadget) nil))))


#||
define sealed method text-selection
    (gadget :: <gtk-text-gadget-mixin>) => (range :: type-union(<text-range>, one-of(#f)))
  let widget = GTK-EDITABLE(gadget-widget(gadget));
  let start-pos = widget.selection-start-pos-value;
  let end-pos = widget.selection-end-pos-value;
  when (start-pos < end-pos)
    make(<text-range>, start: start-pos, end: end-pos)
  end;
end method text-selection;
||#

(defun %text-selection-range (gadget)
"
Provide start and end indexes for any selected range of _gadget_.

Returns (VALUES START END). If there is no selection, the _start_ value
will be equal to the _end_ value.
"
  (let* ((widget (gadget-widget gadget)))
    (cffi/with-stack-structure (start-ptr :int)
      (cffi/with-stack-structure (end-ptr :int)
	(let ((selection? (cffi/gtk-editable-get-selection-bounds widget start-ptr end-ptr)))
	  (if selection?
	      (let ((start-pos (cffi:mem-ref start-ptr :int))
		    (end-pos   (cffi:mem-ref end-ptr   :int)))
		(values start-pos end-pos))
	      (values 0 0)))))))


(defmethod text-selection ((gadget <gtk-text-gadget-mixin>))
  ;; Returns the <text-range> representing the start and end positions of
  ;; any selected text
  (multiple-value-bind (start-pos end-pos)
      (%text-selection-range gadget)
    (when (< start-pos end-pos)
      (make-instance '<text-range> :start start-pos :end end-pos))))

#||
define sealed method selected-text
    (gadget :: <gtk-text-gadget-mixin>) => (string :: false-or(<string>))
  let widget = GTK-EDITABLE(gadget-widget(gadget));
  let start-pos = widget.selection-start-pos-value;
  let end-pos = widget.selection-end-pos-value;
  if (start-pos >= end-pos)
    #f
  elseif (start-pos = 0 & end-pos = gadget.gadget-text-buffer.size)
    gadget.gadget-text-buffer
  else
    let chars = GTK-STRING(gtk-editable-get-chars(widget, start-pos, end-pos));
    let string = gtk-copy-text(chars);
    g-free(chars);
    string
  end;
end method selected-text;
||#

;; Called when using cut/copy from cut and paste
(defmethod selected-text ((gadget <gtk-text-gadget-mixin>))
  (let ((widget (gadget-widget gadget)))
    (when widget
      (multiple-value-bind (start-pos end-pos)
	  (%text-selection-range gadget)
	(cond ((>= start-pos end-pos) nil)
	      ((and (= start-pos 0) (= end-pos (size (gadget-text-buffer gadget))))
	       (gadget-text-buffer gadget))
	      (t (let ((string (cffi/gtk-editable-get-chars widget start-pos end-pos)))
		   string)))))))


#||
define method widget-range-bounds (widget, range == #t)
 => (start-pos :: <integer>, end-pos :: <integer>)
  values(0, -1)
end method widget-range-bounds;
||#

(defmethod widget-range-bounds (widget (range (eql t)))
  (declare (ignore widget))
  ;; 0 = start, -1 = end (all)
  (values 0 -1))

#||
define method widget-range-bounds (widget, range == #f)
 => (start-pos :: <integer>, end-pos :: <integer>)
  let pos = gtk-editable-get-position(widget);
  values(pos, pos)
end method widget-range-bounds;
||#

(defmethod widget-range-bounds (widget (range null))
  (let ((pos (cffi/gtk-editable-get-position widget)))
    (values pos pos)))

#||
define method widget-range-bounds (widget, range :: <text-range>)
 => (start-pos :: <integer>, end-pos :: <integer>)  
  let start-pos = range.text-range-start;
  let end-pos = range.text-range-end;
  if (start-pos < end-pos)
    values(start-pos, end-pos)
  else
    widget-range-bounds(widget, #f)
  end;
end method widget-range-bounds;
||#

(defmethod widget-range-bounds (widget (range <text-range>))
  (let ((start-pos (text-range-start range))
	(end-pos   (text-range-end   range)))
    (if (< start-pos end-pos)
	(values start-pos end-pos)
	(widget-range-bounds widget nil))))

#||
define sealed method text-selection-setter
    (range :: type-union(<text-range>, one-of(#t, #f)),
     gadget :: <gtk-text-gadget-mixin>)
 => (range :: type-union(<text-range>, one-of(#t, #f)))
  let widget = GTK-EDITABLE(gadget-widget(gadget));
  let (start-pos, end-pos) = widget-range-bounds(widget, range);
  gtk-editable-select-region(widget, start-pos, end-pos);
  range
end method text-selection-setter;
||#

(defmethod (setf text-selection) ((range <text-range>) (gadget <gtk-text-gadget-mixin>))
  (let ((widget (gadget-widget gadget)))
    (multiple-value-bind (start-pos end-pos)
	(widget-range-bounds widget range)
      (cffi/gtk-editable-select-region widget start-pos end-pos)
      range)))


(defmethod (setf text-selection) ((range null)
				  (gadget <gtk-text-gadget-mixin>))
  (let ((widget (gadget-widget gadget)))
    (multiple-value-bind (start-pos end-pos)
	(widget-range-bounds widget range)
      (cffi/gtk-editable-select-region widget start-pos end-pos)
      range)))


(defmethod (setf text-selection) ((range (eql t))
				  (gadget <gtk-text-gadget-mixin>))
  (let ((widget (gadget-widget gadget)))
    (multiple-value-bind (start-pos end-pos)
	(widget-range-bounds widget range)
      (cffi/gtk-editable-select-region widget start-pos end-pos)
      range)))


#||
define sealed method text-caret-position
    (gadget :: <gtk-text-gadget-mixin>)
 => (position :: <integer>)
  let widget = GTK-EDITABLE(gadget-widget(gadget));
  gtk-editable-get-position(widget);
end method text-caret-position;
||#

(defmethod text-caret-position ((gadget <gtk-text-gadget-mixin>))
  (let ((widget (gadget-widget gadget)))
    (cffi/gtk-editable-get-position widget)))


#||
define sealed method text-caret-position-setter
    (position :: false-or(<integer>), gadget :: <gtk-text-gadget-mixin>)
 => (position :: false-or(<integer>))
  if (position)
    let widget = GTK-EDITABLE(gadget-widget(gadget));
    gtk-editable-set-position(widget, position);
    position
  end;
end method text-caret-position-setter;
||#

(defmethod (setf text-caret-position) (position (gadget <gtk-text-gadget-mixin>))
  (when position
    (let ((widget (gadget-widget gadget)))
      (cffi/gtk-editable-set-position widget position)
      position)))



#||
/// Text and password fields

/// Text fields

define abstract class <gtk-text-field-mixin>
    (<gtk-text-gadget-mixin>,
     <text-field>)
  // sealed constant each-subclass slot %gtk-text-visibility,
  //  required-init-keyword: gtk-text-visibility:;
  keyword gtk-fixed-height?: = #t;
end class <gtk-text-field-mixin>;
||#

#||
  CPL for <GTK-TEXT-FIELD-MIXIN>

  (#<STANDARD-CLASS GTK-DUIM::<GTK-TEXT-FIELD-MIXIN>>
   #<STANDARD-CLASS GTK-DUIM:<GTK-TEXT-GADGET-MIXIN>>
   #<STANDARD-CLASS GTK-DUIM:<GTK-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<TEXT-FIELD>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<BORDERED-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<ACTION-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<ACTION-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<CHANGING-VALUE-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<VALUE-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<TEXT-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<VALUE-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<BASIC-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<ABSTRACT-GADGET>>
   #<STANDARD-CLASS GTK-DUIM:<GTK-PANE-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<STANDARD-INPUT-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<SHEET-WITH-EVENT-QUEUE-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<MIRRORED-SHEET-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<SHEET-WITH-RESOURCE-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS:<ABSTRACT-SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<EVENT-HANDLER>>
   #<STANDARD-CLASS STANDARD-OBJECT>
   #<BUILT-IN-CLASS T>)

||#

;;; <gtk-text-field-mixin> provides:
;;;
;;;    <<updatable-gadget-protocol>>
;;;        update-gadget (update-mirror-text)

(defclass <gtk-text-field-mixin>    ;; FIXME:ABSTRACT
    (<gtk-text-gadget-mixin>
     <text-field>)
  ()
  (:default-initargs :gtk-fixed-height? t))


(defmethod install-event-handlers ((sheet <gtk-text-field-mixin>)
				   (mirror <gtk-gadget-mirror>))
    (install-named-handlers mirror #("changed" "activate")))

#||
define open generic %gtk-text-visibility
    (gadget :: <gtk-text-field-mixin>)
 => (fixed? :: <boolean>);
||#

(defgeneric %gtk-text-visibility (gadget)
  (:documentation
"
Indicates whether text entered into a text field should be visible
or hidden (i.e. the text field is a password field).
"))


#||
define method %gtk-fixed-height?
    (gadget :: <gtk-text-field-mixin>)
 => (fixed? :: <boolean>)
  #t;
end method;
||#

(defmethod %gtk-fixed-height? ((gadget <gtk-text-field-mixin>))
  t)


#||
define sealed method make-gtk-mirror
    (gadget :: <gtk-text-field-mixin>)
 => (mirror :: <gadget-mirror>)
  let max = text-field-maximum-size(gadget);
  let text = gadget-text-buffer(gadget);
  let visibility = %gtk-text-visibility(gadget);
  let widget = if (max)
                 GTK-ENTRY(gtk-entry-new-with-max-length(max))
	       else
                 GTK-ENTRY(gtk-entry-new());
	       end;
  assert(~null-pointer?(widget), "gtk-entry-new failed");
  // Note that this is happening before install-event-handlers, so don't
  // need to disable events.
  gtk-entry-set-visibility(widget, if (visibility) 1 else 0 end);
  unless (empty?(text))
    with-c-string (c-text = text)
      gtk-entry-set-text(widget, c-text);
    end;
  end;
  make(<gadget-mirror>,
       widget: widget,
       sheet:  gadget)
end method make-gtk-mirror;
||#

(defmethod make-gtk-mirror ((gadget <gtk-text-field-mixin>))
  (let* ((max (text-field-maximum-size gadget))
	 (text (gadget-text-buffer gadget))
	 (visibility (%gtk-text-visibility gadget))
	 (widget (if max
		     (cffi/gtk-entry-new-with-max-length max)
		     (cffi/gtk-entry-new))))
    (when (null widget)
      (error "gtk-entry-new failed"))
    ;; Note that this is happening before install-event-handlers, so don't
    ;; need to disable events.
    (cffi/gtk-entry-set-visibility widget (if visibility 1 0))
    ;; There doesn't seem to be a way to set this for DUIM...
    #-(and)
    (cffi/gtk-entry-set-width-chars widget columns)
    ;; GADGET-X-ALIGNMENT is only defined for <labelled-gadget-mixin>
    #-(and)
    (let ((gtk-align (ecase (gadget-x-alignment gadget)
		       (:left   0)
		       (:right  1)
		       (:center 0.5))))
      (cffi/gtk-entry-set-alignment widget gtk-align))
    (unless (empty? text)
      (cffi/gtk-entry-set-text widget text))
    (make-gtk-widget-mirror-instance (find-class '<gtk-gadget-mirror>)
				     :widget widget
				     :sheet  gadget)))


(defmethod port-handles-repaint? ((port <gtk-port>)
				  (gadget <gtk-text-field-mixin>))
  t)


#||
// Updates the GTK text field from the DUIM gadget
define sealed method update-gadget-text
    (gadget :: <gtk-text-field-mixin>, mirror :: <gadget-mirror>) => ()
  ignore(mirror);
  let widget = GTK-ENTRY(gadget-widget(gadget));
  let new-text = gadget-text-buffer(gadget);
  with-disabled-event-handler (widget, #"changed")
    with-c-string (c-text = new-text)
      gtk-entry-set-text(widget, c-text);
    end;
  end;
end method update-gadget-text;
||#

(defmethod update-mirror-text ((gadget <gtk-text-field-mixin>) (mirror <gtk-gadget-mirror>))
  (declare (ignore mirror))
  (let ((widget (gadget-widget gadget))
	(new-text (gadget-text-buffer gadget)))
    (with-disabled-event-handler (widget "changed")
      (cffi/gtk-entry-set-text widget new-text))))


#||
/// Text fields

define sealed class <gtk-text-field>
    (<gtk-text-field-mixin>,
     <text-field>,
     <leaf-pane>,
     <sealed-constructor-mixin>)
  keyword gtk-text-visibility: = $true;
end class <gtk-text-field>;
||#

#||
  CPL for <GTK-TEXT-FIELD>

  (#<STANDARD-CLASS GTK-DUIM::<GTK-TEXT-FIELD>>
   #<STANDARD-CLASS GTK-DUIM::<GTK-TEXT-FIELD-MIXIN>>
   #<STANDARD-CLASS GTK-DUIM:<GTK-TEXT-GADGET-MIXIN>>
   #<STANDARD-CLASS GTK-DUIM:<GTK-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<TEXT-FIELD>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<BORDERED-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<ACTION-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<ACTION-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<CHANGING-VALUE-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<VALUE-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<TEXT-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<VALUE-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<BASIC-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<ABSTRACT-GADGET>>
   #<STANDARD-CLASS GTK-DUIM:<GTK-PANE-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<STANDARD-INPUT-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<SHEET-WITH-EVENT-QUEUE-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<MIRRORED-SHEET-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<SHEET-WITH-RESOURCE-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS:<LEAF-PANE>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<CACHED-SPACE-REQUIREMENT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<CLIENT-OVERRIDABILITY-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<SPACE-REQUIREMENT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<LEAF-LAYOUT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<LAYOUT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS:<LAYOUT>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<BASIC-SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<ABSTRACT-SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<EVENT-HANDLER>>
   #<STANDARD-CLASS STANDARD-OBJECT>
   #<BUILT-IN-CLASS T>)

||#

;;; <gtk-text-field> provides:
;;;
;;; nothing

(defclass <gtk-text-field>
    (<gtk-text-field-mixin>
     <text-field>
     <leaf-pane>)
  ()
  (:default-initargs :gtk-text-visibility +true+))


#||
define method %gtk-text-visibility
    (gadget :: <gtk-text-field>)
 => (fixed? :: <boolean>);
  #t
end method %gtk-text-visibility;
||#

(defmethod %gtk-text-visibility ((gadget <gtk-text-field>))
  t)


#||
define sealed method class-for-make-pane 
    (framem :: <gtk-frame-manager>, class == <text-field>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<gtk-text-field>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <gtk-frame-manager>)
				(class (eql (find-class '<text-field>)))
				&key)
  (values (find-class '<gtk-text-field> nil)))


#||
/// Password fields

define sealed class <gtk-password-field>
    (<gtk-text-field-mixin>,
     <password-field>,
     <leaf-pane>,
     <sealed-constructor-mixin>)
  keyword gtk-text-visibility: = $false;
end class <gtk-password-field>;
||#

;;; <gtk-password-field> provides:
;;;
;;; nothing

(defclass <gtk-password-field>
    (<gtk-text-field-mixin>
     <password-field>
     <leaf-pane>)
  ()
  (:default-initargs :gtk-text-visibility +false+))


#||
define method %gtk-text-visibility
    (gadget :: <gtk-password-field>)
 => (fixed? :: <boolean>)
  #f
end method %gtk-text-visibility;
||#

(defmethod %gtk-text-visibility ((gadget <gtk-password-field>))
  nil)


#||
define sealed method class-for-make-pane 
    (framem :: <gtk-frame-manager>, class == <password-field>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<gtk-password-field>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <gtk-frame-manager>)
				(class (eql (find-class '<password-field>)))
				&key)
  (values (find-class '<gtk-password-field>) nil))



#||
/// Text editors

define sealed class <gtk-text-editor>
    (<gtk-text-gadget-mixin>,
     <text-editor>,
     <leaf-pane>,
     <sealed-constructor-mixin>)
end class <gtk-text-editor>;
||#

#||
  CPL for <GTK-TEXT-EDITOR>

  (#<STANDARD-CLASS GTK-DUIM::<GTK-TEXT-EDITOR>>
   #<STANDARD-CLASS GTK-DUIM:<GTK-TEXT-GADGET-MIXIN>>
   #<STANDARD-CLASS GTK-DUIM:<GTK-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<TEXT-EDITOR>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<SCROLLING-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<TEXT-FIELD>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<BORDERED-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<ACTION-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<ACTION-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<CHANGING-VALUE-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<VALUE-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<TEXT-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<VALUE-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<BASIC-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<ABSTRACT-GADGET>>
   #<STANDARD-CLASS GTK-DUIM:<GTK-PANE-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<STANDARD-INPUT-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<SHEET-WITH-EVENT-QUEUE-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<MIRRORED-SHEET-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<SHEET-WITH-RESOURCE-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS:<LEAF-PANE>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<CACHED-SPACE-REQUIREMENT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<CLIENT-OVERRIDABILITY-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<SPACE-REQUIREMENT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<LEAF-LAYOUT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<LAYOUT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS:<LAYOUT>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<BASIC-SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<ABSTRACT-SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<EVENT-HANDLER>>
   #<STANDARD-CLASS STANDARD-OBJECT>
   #<BUILT-IN-CLASS T>)

||#

;;; <gtk-text-editor> provides:
;;;
;;;    <<updatable-gadget-protocol>>
;;;        update-mirror-text
;;;        %set-text-mirror-text
;;;    <<changing-value-gadget-protocol>>
;;;        handle-text-gadget-changing

(defclass <gtk-text-editor>
    (<gtk-text-gadget-mixin>
     <text-editor>
     <leaf-pane>)
  ())


(defmethod install-event-handlers ((sheet <gtk-text-editor>)
				   (mirror <gtk-gadget-mirror>))
  (let* ((widget (and mirror (mirror-widget mirror)))
	 (buffer (and widget (cffi/gtk-text-view-get-buffer widget))))
    ;; It's the GtkTextBuffer that emits "changed", but we handle it
    ;; on the text-editor.
    (install-named-handlers mirror #("changed") :swap buffer)
    (install-named-handlers mirror #("activate") :swap buffer)))


#||
define sealed method class-for-make-pane 
    (framem :: <gtk-frame-manager>, class == <text-editor>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<gtk-text-editor>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <gtk-frame-manager>)
				(class (eql (find-class '<text-editor>)))
				&key)
  (values (find-class '<gtk-text-editor>) nil))


#||
define sealed method make-gtk-mirror
    (gadget :: <gtk-text-editor>)
 => (mirror :: <gadget-mirror>)
  let lines = gadget-lines(gadget);
  let columns = gadget-columns(gadget);
  let word-wrap? = text-field-word-wrap?(gadget);
  let text = gadget-text-buffer(gadget);
  let widget = GTK-TEXT(gtk-text-new(null-pointer(<GtkAdjustment*>),
                                     null-pointer(<GtkAdjustment*>)));
  assert(~null-pointer?(widget), "gtk-text-new failed");
  // Note that this is happening before install-event-handlers, so don't
  // need to disable events.
  when (lines | columns)
    ignoring("lines:/columns:")
  end;
  gtk-text-set-word-wrap(widget, if (word-wrap?) $true else $false end);
  set-text-widget-text(widget, text);
  make(<gadget-mirror>,
       widget: widget,
       sheet:  gadget)
end method make-gtk-mirror;
||#

(defmethod make-gtk-mirror ((gadget <gtk-text-editor>))
  (let ((lines      (gadget-lines gadget))
	(columns    (gadget-columns gadget))
	(word-wrap? (text-field-word-wrap? gadget))
	(text       (gadget-text-buffer gadget))
	(widget     (cffi/gtk-text-view-new)))
    ;; FIXME: How to put this into a scroller? Or is that something that DUIM should
    ;; do automatically on our behalf? Ditto list controls...
    (when (null widget)
      (error "gtk-text-new failed"))
    ;; Note that this is happening before install-event-handlers, so don't
    ;; need to disable events
    (when (or lines columns)
      (ignoring ":lines/:columns [GTK-GADGETS:MAKE-GTK-MIRROR <GTK-TEXT-EDITOR>]"))
    (cffi/gtk-text-view-set-wrap-mode widget (if word-wrap? +CFFI/GTK-WRAP-WORD+ +CFFI/GTK-WRAP-NONE+))
    (%set-text-mirror-text widget text)
    (make-gtk-widget-mirror-instance (find-class '<gtk-gadget-mirror>)
				     :widget widget
				     :sheet  gadget)))


(defmethod port-handles-repaint? ((port <gtk-port>)
				  (gadget <gtk-text-editor>))
  t)


;;; NOTE: in general we use the size request of the gtk widget for DUIM space composition
;;;       purposes, but GTK has a very bad idea of what that should be for empty text
;;; views so we'll use the space DUIM thinks we should have as the default.

(defmethod do-compose-space ((gadget <gtk-text-editor>) &key width height)
  ;; FIXME: Use :lines and :columns here to work out the appropriate size, or use
  ;; the default if none are specified.
  (let ((lines   (gadget-lines gadget))
	(columns (gadget-columns gadget)))
    (when (or lines columns)
      (ignoring ":lines/:columns [GTK-GADGETS:MAKE-GTK-MIRROR <GTK-TEXT-EDITOR>]"))
    (default-space-requirement gadget :width width :height height)))


#||
define sealed method update-gadget-text
    (gadget :: <gtk-text-editor>, mirror :: <gadget-mirror>) => ()
  ignore(mirror);
  let widget = GTK-TEXT(gadget-widget(gadget));
  when (widget)
    let new-text = gadget-text-buffer(gadget);
    let old-text = GTK-STRING(gtk-editable-get-chars(widget, 0, -1));
    let update? = old-text ~= new-text;
    g-free(old-text);
    when (update?)
      block ()
	gtk-text-freeze(widget);
	with-disabled-event-handler (widget, #"changed")
	  set-text-widget-text(widget, new-text);
	end;
      cleanup
	gtk-text-thaw(widget);
      end
    end;
  end;
end method update-gadget-text;
||#

(defmethod update-mirror-text ((gadget <gtk-text-editor>) (mirror <gtk-gadget-mirror>))
  (declare (ignore mirror))
  (let ((widget (gadget-widget gadget)))
    (when widget
      (let* ((new-text (gadget-text-buffer gadget))
	     (old-text (%get-text-mirror-text widget))
	     (update?  (not (string= new-text old-text))))
	(when update?
	  ;; XXX: Note: the "changed" signal for <gtk-text-editor> is actually emitted
	  ;; by the *GtkTextBuffer* associated with the GtkTextView. Why isn't this
	  ;; blocking the signal?
	  ;; ^ I believe blocking the signal delays it rather than cancels it.
	  ;; XXX: Keep track of the handler ids when handlers are registered, then
	  ;; disable / disconnect by id.
	  (with-disabled-event-handler (widget "changed" :swap (cffi/gtk-text-view-get-buffer widget))
	    (%set-text-mirror-text widget new-text)))))))


(defmethod %get-text-mirror-text (widget)
  (let* ((tk-buffer (and widget (cffi/gtk-text-view-get-buffer widget))))
    (cffi/with-stack-structure (tk-start '(:struct GtkTextIter))
      (cffi/with-stack-structure (tk-end '(:struct GtkTextIter))
	(cffi/gtk-text-buffer-get-bounds tk-buffer tk-start tk-end)
	;; The +false+ on the next line says not to copy "invisible" chars. Is
	;; that what we want?
	(cffi/gtk-text-buffer-get-text tk-buffer tk-start tk-end +false+)))))


#||
define method set-text-widget-text (widget, text :: <string>)
  with-c-string (c-text = text)
    with-stack-structure (position :: <c-int*>)
      gtk-editable-delete-text(widget, 0, -1);
      pointer-value(position) := 0;
      gtk-editable-insert-text(widget, c-text, text.size,
                               pointer-cast(<gint*>, position));
    end;
  end;
end set-text-widget-text;
||#

(defmethod %set-text-mirror-text (widget (text string))
  (let ((tk-buffer (cffi/gtk-text-view-get-buffer widget)))
    (cffi/gtk-text-buffer-set-text tk-buffer text -1)))


;;; Update DUIM's idea of the text associated with the gadget to reflect
;;; the (modified) native widget contents.
(defmethod handle-text-gadget-changing ((gadget <gtk-text-editor>))
  (let* ((old-text (gadget-text-buffer gadget))
	 (widget   (gadget-widget gadget))
	 (new-text (%get-text-mirror-text widget))
	 (update?  (not (string= old-text new-text))))
    (when update?
      (setf (gadget-text-buffer gadget) new-text)
      (setf (%changed? gadget) t)
      (distribute-text-changing-callback gadget new-text))))


#||
/// Buttons

define class <gtk-button-mixin> (<gtk-gadget-mixin>, <button>)
end class <gtk-button-mixin>;
||#

#||
  CPL for <GTK-BUTTON-MIXIN>

  (#<STANDARD-CLASS GTK-DUIM::<GTK-BUTTON-MIXIN>>
   #<STANDARD-CLASS GTK-DUIM:<GTK-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<BUTTON>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<ACCELERATOR-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<MNEMONIC-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<LABELLED-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<LABELLED-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<GADGET-COMMAND-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<VALUE-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<ABSTRACT-GADGET>>
   #<STANDARD-CLASS GTK-DUIM:<GTK-PANE-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<STANDARD-INPUT-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<SHEET-WITH-EVENT-QUEUE-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<MIRRORED-SHEET-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<SHEET-WITH-RESOURCE-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS:<ABSTRACT-SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<EVENT-HANDLER>>
   #<STANDARD-CLASS STANDARD-OBJECT>
   #<BUILT-IN-CLASS T>)

||#

;;; <gtk-button-mixin> provides:
;;;
;;;    <<action-gadget-protocol>>
;;;        handle-gtk-clicked-event
;;;    <<gadget-protocol>>
;;;        update-mirror-label

(defclass <gtk-button-mixin>
    (<gtk-gadget-mixin> <button>)
  ())

;;; FIXME: ACCELERATORS AND MNEMONICS - and how do they tie in to command tables?

#||
define method install-event-handlers
    (sheet :: <gtk-button-mixin>, mirror :: <gadget-mirror>) => ()
  next-method();
  install-named-handlers(mirror, #[#"clicked"])
end method install-event-handlers;
||#

(defmethod install-event-handlers ((sheet <gtk-button-mixin>)
				   (mirror <gtk-gadget-mirror>))
  (install-named-handlers mirror #("clicked")))


#||
define sealed method handle-gtk-clicked-event
    (gadget :: <gtk-button-mixin>, widget :: <GtkWidget*>,
     event :: <GdkEventAny*>)
 => (handled? :: <boolean>)
  gtk-debug("Clicked on button %=", gadget-label(gadget));
  handle-button-gadget-click(gadget)
end method handle-gtk-clicked-event;
||#

(defmethod handle-gtk-clicked-signal ((gadget <gtk-button-mixin>) widget user-data)
  (declare (ignore widget user-data))
  (handle-button-gadget-click gadget))


#||
define sealed method button-box-spacing
    (framem :: <gtk-frame-manager>, box :: <button-box>)
 => (spacing :: <integer>)
  select (gadget-orientation(box))
    #"horizontal" => $button-box-x-spacing;
    #"vertical"   => $button-box-y-spacing;
  end
end method button-box-spacing;
||#

(defmethod button-box-spacing ((framem <gtk-frame-manager>)
			       (box    <button-box>))
  (ecase (gadget-orientation box)
    (:horizontal +button-box-x-spacing+)
    (:vertical   +button-box-y-spacing+)))


(defmethod update-mirror-label ((gadget <gtk-button-mixin>) (mirror <gtk-gadget-mirror>))
  (let* ((widget (mirror-widget (sheet-direct-mirror gadget)))
	 (label  (defaulted-gadget-label gadget))
	 ;; FIXME: image + text/image buttons
	 ;; FIXME: mnemonics?
	 ;; FIXME: accelerators?
	 (label  (if (stringp label) label "")))
    (cffi/gtk-button-set-label widget label)))



#||
/// Push buttons

define sealed class <gtk-push-button>
    (<gtk-button-mixin>,
     <push-button>,
     <leaf-pane>,
     <sealed-constructor-mixin>)
  keyword gtk-fixed-width?:  = #t;
  keyword gtk-fixed-height?: = #t;
end class <gtk-push-button>;
||#

#||
  CPL for <GTK-PUSH-BUTTON>

  (#<STANDARD-CLASS GTK-DUIM::<GTK-PUSH-BUTTON>>
   #<STANDARD-CLASS GTK-DUIM::<GTK-BUTTON-MIXIN>>
   #<STANDARD-CLASS GTK-DUIM:<GTK-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<PUSH-BUTTON>>
   #<STANDARD-CLASS DUIM-GADGETS:<BUTTON>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<ACCELERATOR-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<MNEMONIC-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<LABELLED-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<LABELLED-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<GADGET-COMMAND-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<DEFAULT-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<ACTION-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<ACTION-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<BASIC-VALUE-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<VALUE-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<VALUE-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<BASIC-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<ABSTRACT-GADGET>>
   #<STANDARD-CLASS GTK-DUIM:<GTK-PANE-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<STANDARD-INPUT-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<SHEET-WITH-EVENT-QUEUE-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<MIRRORED-SHEET-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<SHEET-WITH-RESOURCE-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS:<LEAF-PANE>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<CACHED-SPACE-REQUIREMENT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<CLIENT-OVERRIDABILITY-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<SPACE-REQUIREMENT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<LEAF-LAYOUT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<LAYOUT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS:<LAYOUT>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<BASIC-SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<ABSTRACT-SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<EVENT-HANDLER>>
   #<STANDARD-CLASS STANDARD-OBJECT>
   #<BUILT-IN-CLASS T>)

||#

;;; <gtk-push-button> provides:
;;;
;;;    <<gadget-protocol>>
;;;        gadget-default?

(defclass <gtk-push-button>
    (<gtk-button-mixin>
     <push-button>
     <leaf-pane>)
  ()
  (:default-initargs :gtk-fixed-width? t :gtk-fixed-height? t))

;;; FIXME: DEAL WITH ACCELERATORS ON PUSH BUTTONS (AND TEST THEM!)

#||
define method %gtk-fixed-width?
    (gadget :: <gtk-push-button>)
 => (fixed? :: <boolean>)
  #t;
end method;
||#

(defmethod %gtk-fixed-width? ((gadget <gtk-push-button>))
  t)


#||
define method %gtk-fixed-height?
    (gadget :: <gtk-push-button>)
 => (fixed? :: <boolean>)
  #t;
end method;
||#

(defmethod %gtk-fixed-height? ((gadget <gtk-push-button>))
  t)


#||
define sealed method class-for-make-pane 
    (framem :: <gtk-frame-manager>, class == <push-button>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<gtk-push-button>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <gtk-frame-manager>)
				(class (eql (find-class '<push-button>)))
				&key)
  (values (find-class '<gtk-push-button>) nil))


#||
define sealed method make-gtk-mirror
    (gadget :: <gtk-push-button>)
 => (mirror :: <gadget-mirror>)
  let (text, image, mnemonic, index) 
    = text-or-image-from-gadget-label(gadget);
  if (image)
    ignoring("image label")
  end;
  with-c-string (c-string = text)
    let widget = GTK-BUTTON(gtk-button-new-with-label(c-string));
    assert(~null-pointer?(widget), "gtk-button-new-with-label failed");
    make(<gadget-mirror>,
	 widget: widget,
	 sheet:  gadget)
  end
end method make-gtk-mirror;
||#

;; FIXME: Some of these calls use "defaulted-gadget-label" and some use
;; "text-or-image-from-gadget-label" to get the label. Decide which
;; to use... -- USE TEXT-OR-IMAGE-FROM-GADGET-LABEL.
(defmethod make-gtk-mirror ((gadget <gtk-push-button>))
  (multiple-value-bind (text image mnemonic index)
      (text-or-image-from-gadget-label gadget)
    ;; XXX: Buttons are a gtk-bin, so we can put labels or gtkimage's
    ;; onto them (or both, I guess. Work out what DUIM thinks is permitted).
    (when image
      ;; FIXME: Don't ignore - see how we deal with <label> instances for some
      ;; pointers
      (ignoring "image label"))
    (when mnemonic
      ;; Furtle with text and stick a "_" in front of the mnemonic char.
      (setf text (%insert-gtk-mnemonic-indicator! text index mnemonic)))
    ;; FIXME: Whether these things should be looked up from stock items
    ;; or not should be configurable...
    ;; Look stuff up from stock; get the image, accelerator, mnemonic.
    ;; User-supplied image/accelerator/mnemonic trumps the stock ones.
    ;; Then use gtk_button_set_label, gtk_button_set_image to set them up.
    (let* ((stock-id (%lookup-stock-id gadget))
	   ;; XXX: This won't honour any specified mnemonic. Should construct
	   ;; this stuff differently I think (manually look up the accelerator,
	   ;; image, mnemonic from the stock data, then use those to set
	   ;; what we want - we might want the image but not the accelerator or
	   ;; mnemonic, or maybe we want the image and mnemonic but not the
	   ;; accelerator, or perhaps we want everything from the stock item.
	   (widget (or (and stock-id (cffi/gtk-button-new-from-stock stock-id))
		       (if mnemonic
			   (cffi/gtk-button-new-with-mnemonic text)
			   (cffi/gtk-button-new-with-label text)))))
      (when (null widget)
	(error "gtk-button-new-with-label failed"))
      #-(and)
      (let* ((frame   (frame gadget))
	     (default (and frame (frame-default-button frame))))
	;; We can't grab default when we're being made. Only after we're realized!
	(when (eql gadget default)
	  ;; Only <push-button>s and <push-menu-button>s attempt to grab default
	  (cffi/gtk-widget-set-can-default widget +true+)
	  (cffi/gtk-widget-grab-default widget)))
      (make-gtk-widget-mirror-instance (find-class '<gtk-gadget-mirror>)
				       :widget widget
				       :sheet  gadget))))


(defmethod update-mirror-attributes ((gadget <gtk-push-button>) (mirror <gtk-widget-mirror>))
  (when (gadget-default? gadget)
    (let ((widget (gadget-widget gadget)))
      (cffi/gtk-widget-grab-default widget))))


(defmethod port-handles-repaint? ((port <gtk-port>)
				  (gadget <gtk-push-button>))
  t)


#||
define sealed method gadget-default?-setter
    (default? :: <boolean>, gadget :: <gtk-push-button>)
 => (default? :: <boolean>)
  next-method();
  ignoring("gadget-default?-setter");
  default?
end method gadget-default?-setter;
||#

(defmethod (setf gadget-default?) ((default? (eql t))
				   (gadget <gtk-push-button>))
  (call-next-method)
  (let ((widget (gadget-widget gadget)))
    ;; will be called before widgets are constructed
    (when widget
      (cffi/gtk-widget-grab-default widget)))
  default?)



#||
/// Radio buttons

define sealed class <gtk-radio-button>
    (<gtk-button-mixin>,
     <radio-button>,
     <leaf-pane>,
     <sealed-constructor-mixin>)
  keyword gtk-fixed-width?:  = #t;
  keyword gtk-fixed-height?: = #t;
end class <gtk-radio-button>;
||#

#||

  CPL for <GTK-RADIO-BUTTON>

  (#<STANDARD-CLASS GTK-DUIM::<GTK-RADIO-BUTTON>>
   #<STANDARD-CLASS GTK-DUIM::<GTK-BUTTON-MIXIN>>
   #<STANDARD-CLASS GTK-DUIM:<GTK-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<RADIO-BUTTON>>
   #<STANDARD-CLASS DUIM-GADGETS:<BUTTON>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<ACCELERATOR-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<MNEMONIC-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<LABELLED-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<LABELLED-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<GADGET-COMMAND-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<ACTION-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<ACTION-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<BASIC-VALUE-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<VALUE-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<VALUE-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<BASIC-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<ABSTRACT-GADGET>>
   #<STANDARD-CLASS GTK-DUIM:<GTK-PANE-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<STANDARD-INPUT-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<SHEET-WITH-EVENT-QUEUE-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<MIRRORED-SHEET-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<SHEET-WITH-RESOURCE-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS:<LEAF-PANE>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<CACHED-SPACE-REQUIREMENT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<CLIENT-OVERRIDABILITY-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<SPACE-REQUIREMENT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<LEAF-LAYOUT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<LAYOUT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS:<LAYOUT>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<BASIC-SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<ABSTRACT-SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<EVENT-HANDLER>>
   #<STANDARD-CLASS STANDARD-OBJECT>
   #<BUILT-IN-CLASS T>)
||#

;;; <gtk-radio-button> provides:
;;;
;;;    <<value-gadget-protocol>>
;;;        note-gadget-value-changed  -- note: this is also defined for <gtk-check-button>. Why
;;;                                      do some define for all and others share the same one?

(defclass <gtk-radio-button>
    (<gtk-button-mixin>
     <radio-button>
     <leaf-pane>)
  ;;; FIXHERE: we could hold the currently active radio button in the
  ;;; box that this instance belongs to here.
  ()
  (:default-initargs :gtk-fixed-width? t :gtk-fixed-height? t))


#||
define method %gtk-fixed-width?
    (gadget :: <gtk-radio-button>)
 => (fixed? :: <boolean>)
  #t;
end method;
||#

(defmethod %gtk-fixed-width? ((gadget <gtk-radio-button>))
  t)


#||
define method %gtk-fixed-height?
    (gadget :: <gtk-radio-button>)
 => (fixed? :: <boolean>)
  #t;
end method;
||#

(defmethod %gtk-fixed-height? ((gadget <gtk-radio-button>))
  t)


#||
define sealed method class-for-make-pane 
    (framem :: <gtk-frame-manager>, class == <radio-button>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<gtk-radio-button>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <gtk-frame-manager>)
				(class (eql (find-class '<radio-button>)))
				&key)
  (values (find-class '<gtk-radio-button>) nil))


#||
define sealed method make-gtk-mirror
    (gadget :: <gtk-radio-button>)
 => (mirror :: <gadget-mirror>)
  let (text, image, mnemonic, index) 
    = text-or-image-from-gadget-label(gadget);
  if (image)
    ignoring("image label")
  end;
  with-c-string (c-string = text)
    let widget
      = if (push-button-like?(gadget))
	  GTK-TOGGLE-BUTTON(gtk-toggle-button-new-with-label(c-string))
	else
          GTK-RADIO-BUTTON(gtk-radio-button-new-with-label
                             (null-pointer(<GSList*>), c-string))
	end;
    assert(~null-pointer?(widget), "gtk-toggle/radio-button-new-with-label failed");
    make(<gadget-mirror>,
	 widget: widget,
	 sheet:  gadget)
  end
end method make-gtk-mirror;
||#

(defun %gtk-button-group (gadget)
"
If the DUIM button is in a gadget-box they should work as
a unit; in this case all the gtk buttons should be placed
in the same 'gtk button group'.

Radio buttons must always be in a 'gtk button group' even
if that group contains only a single button. The first button
constructed in a duim gadget-box defines the gtk group for
all members of the gadget box.

This method returns the gtk group (which is a GSList*) for
the button, or nil if the button is not in a gadget-box.
"
  ;; This should not be an expensive operation if the buttons
  ;; are mapped in the same order that 'gadget-box-buttons'
  ;; returns them.
  ;; XXX: Perhaps we should subclass gadget-box and add a slot
  ;; for the GTK group?
  (let ((box (button-gadget-box gadget)))
    (when box
      (map nil #'(lambda (sheet)
		   (let ((mirror (sheet-direct-mirror sheet)))
		     (when mirror
		       (let* ((widget    (mirror-widget mirror))
			      (gtk-group (cffi/gtk-radio-button-get-group widget)))
			 (return-from %gtk-button-group gtk-group)))))
	   (gadget-box-buttons box)))))


(defmethod make-gtk-mirror ((gadget <gtk-radio-button>))
  (multiple-value-bind (text image mnemonic index)
      (text-or-image-from-gadget-label gadget)
    (when image
      ;; FIXME: Don't ignore
      (ignoring "image label"))
    (when mnemonic
      ;; Furtle with text and stick a "_" in front of the mnemonic char.
      (setf text (%insert-gtk-mnemonic-indicator! text index mnemonic)))
    (let* ((group  (%gtk-button-group gadget))
	   (widget (if (push-button-like? gadget)
		       ;; Toggle buttons can't be added to a group.
		       ;; DUIM is responsible for setting their state.
		       (if mnemonic
			   (cffi/gtk-toggle-button-new-with-mnemonic text)
			   (if (and text (string/= text ""))
			       (cffi/gtk-toggle-button-new-with-label text)
			       (cffi/gtk-toggle-button-new)))
		       ;; else... 'radio-button-like' :)
		       (if mnemonic
			   (cffi/gtk-radio-button-new-with-mnemonic group text)
			   (if (and text (string/= text ""))
			       (cffi/gtk-radio-button-new-with-label group text)
			       (cffi/gtk-radio-button-new group))))))
      (when (null widget)
	(error "gtk-toggle/radio-button-new-with-label failed"))
      (make-gtk-widget-mirror-instance (find-class '<gtk-gadget-mirror>)
				       :widget widget
				       :sheet  gadget))))


(defmethod port-handles-repaint? ((port <gtk-port>)
				  (gadget <gtk-radio-button>))
  t)


#||
define method update-mirror-attributes
    (gadget :: <gtk-radio-button>, mirror :: <gadget-mirror>) => ()
  next-method();
  let widget = mirror.mirror-widget;
  let selected? = gadget-value(gadget);
  with-disabled-event-handler (widget, #"clicked")
    gtk-toggle-button-set-active
      (widget, if (selected?) $true else $false end)
  end
end method update-mirror-attributes;
||#

(defun %gtk-button-group-selected-button (group)
"
Identify the currently selected toolkit widget in the radio button
group 'group' (a GSList*).

Returns the currently selected radio button (GtkRadioButton*), or NIL
if no currently selected button can be found.
"
  (unless (or (null group) (and (cffi:pointerp group) (cffi:null-pointer-p group)))
    (labels ((process-list (list)
	       (cond ((null list) nil)
		     (t (let ((item (cffi/gslist->data list)))
			  (when (= (cffi/gtk-toggle-button-get-active item) +true+)
			    (return-from %gtk-button-group-selected-button item))
			  (process-list (cffi/g_slist_next list)))))))
      (process-list group))))


;;; XXX: The problems in here are to do with programatically setting radio
;;; button states; I think because the 'toggled' signal is emitted which
;;; makes the currently-selected button toggle also, which in turn makes
;;; the new selected button de-selected again. Since each group must always
;;; have a selected button, the first button in the group is selected by
;;; default.
(defmethod update-mirror-attributes ((gadget <gtk-radio-button>) (mirror <gtk-gadget-mirror>))
  (call-next-method)
  (let* ((widget       (mirror-widget mirror))
	 (selected?    (gadget-value gadget)))
    (if (push-button-like? gadget)
	;; "push-button-like" radio buttons are not in a (gtk) group, so everything about them
	;; is handled by duim and we don't get into a mess with other buttons signalling when
	;; we don't expect them to.
	(with-disabled-event-handler (widget "clicked")
	  (cffi/gtk-toggle-button-set-active widget (if selected? +true+ +false+)))
	;; else
	;; Unfortunately, "standard" radio buttons ARE in a (gtk) group, so when we select one
	;; button, either nothing happens (button is already "on"), or we get "clicked" + "toggled"
	;; emissions from the button that we're changing, AS WELL AS from the button gtk helpfully
	;; toggles off on our behalf so that the group remains in a consistent state (i.e. the
	;; previously active member of the button group). Signal emission needs to be disabled for
	;; BOTH of these buttons.
	;; We can only change a radio button from 'unselected' to 'selected' ('selected' ->
	;; 'selected' is a no-op, and it's not possible to de-select radio buttons).
	;; Disable "clicked" signals on the currently selected button in the group.
	;; Also note that a "clicked" signal can only ever turn the recipient *on*. If recipient is already
	;; on, it does nothing. If recipient is currently off, it turns recipient to on. It looks like
	;; when radio buttons are in a group (which they always seem to be, even if the group only contains
	;; a single member) then there is *always* at least 1 member that is "on" on the gtk side of things.
	(when selected?
	  (let* ((group (cffi/gtk-radio-button-get-group widget))
		 (selected-button (%gtk-button-group-selected-button group)))
	    ;; By definition if there's a group, there's an selected button in it
	    (when (not (cffi:pointer-eq selected-button widget))
	      ;; There's an active radio button and it is not the one we want to be active.
	      ;; Stop it, and the one we want to change here, from emitting signals.
	      (with-disabled-event-handler (selected-button "clicked")
		(with-disabled-event-handler (widget "clicked")
		  (cffi/gtk-toggle-button-set-active widget +true+)))))))))


#||
define sealed method note-gadget-value-changed
    (gadget :: <gtk-radio-button>) => ()
  next-method();
  let mirror = sheet-direct-mirror(gadget);
  mirror & update-mirror-attributes(gadget, mirror)
end method note-gadget-value-changed;
||#

(defmethod note-gadget-value-changed ((gadget <gtk-radio-button>))
  (call-next-method)
  (let ((mirror (sheet-direct-mirror gadget)))
    (and mirror (update-mirror-attributes gadget mirror))))



#||
/// Check buttons

define sealed class <gtk-check-button>
    (<gtk-button-mixin>,
     <check-button>,
     <leaf-pane>,
     <sealed-constructor-mixin>)
  keyword gtk-fixed-width?:  = #t;
  keyword gtk-fixed-height?: = #t;
end class <gtk-check-button>;
||#

#||
  CPL for <GTK-CHECK-BUTTON>

  (#<STANDARD-CLASS GTK-DUIM::<GTK-CHECK-BUTTON>>
   #<STANDARD-CLASS GTK-DUIM::<GTK-BUTTON-MIXIN>>
   #<STANDARD-CLASS GTK-DUIM:<GTK-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<CHECK-BUTTON>>
   #<STANDARD-CLASS DUIM-GADGETS:<BUTTON>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<ACCELERATOR-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<MNEMONIC-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<LABELLED-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<LABELLED-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<GADGET-COMMAND-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<ACTION-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<ACTION-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<BASIC-VALUE-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<VALUE-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<VALUE-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<BASIC-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<ABSTRACT-GADGET>>
   #<STANDARD-CLASS GTK-DUIM:<GTK-PANE-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<STANDARD-INPUT-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<SHEET-WITH-EVENT-QUEUE-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<MIRRORED-SHEET-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<SHEET-WITH-RESOURCE-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS:<LEAF-PANE>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<CACHED-SPACE-REQUIREMENT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<CLIENT-OVERRIDABILITY-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<SPACE-REQUIREMENT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<LEAF-LAYOUT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<LAYOUT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS:<LAYOUT>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<BASIC-SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<ABSTRACT-SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<EVENT-HANDLER>>
   #<STANDARD-CLASS STANDARD-OBJECT>
   #<BUILT-IN-CLASS T>)

||#

;;; <gtk-check-button> provides:
;;;
;;;    <<value-gadget-protocol>>
;;;        note-gadget-value-changed

(defclass <gtk-check-button>
    (<gtk-button-mixin>
     <check-button>
     <leaf-pane>)
  ()
  (:default-initargs :gtk-fixed-width? t :gtk-fixed-height? t))


#||
define method %gtk-fixed-width?
    (gadget :: <gtk-check-button>)
 => (fixed? :: <boolean>)
  #t;
end method;
||#

(defmethod %gtk-fixed-width? ((gadget <gtk-check-button>))
  t)

#||
define method %gtk-fixed-height?
    (gadget :: <gtk-check-button>)
 => (fixed? :: <boolean>)
  #t;
end method;
||#

(defmethod %gtk-fixed-height? ((gadget <gtk-check-button>))
  t)


#||
define sealed method class-for-make-pane
    (framem :: <gtk-frame-manager>, class == <check-button>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<gtk-check-button>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <gtk-frame-manager>)
				(class (eql (find-class '<check-button>)))
				&key)
  (values (find-class '<gtk-check-button>) nil))


#||
define sealed method make-gtk-mirror
    (gadget :: <gtk-check-button>)
 => (mirror :: <gadget-mirror>)
  let (text, image, mnemonic, index) 
    = text-or-image-from-gadget-label(gadget);
  if (image)
    ignoring("image label")
  end;
  with-c-string (c-string = text)
    let widget
      = if (push-button-like?(gadget))
          GTK-TOGGLE-BUTTON(gtk-toggle-button-new-with-label(c-string))
	else
          GTK-CHECK-BUTTON(gtk-check-button-new-with-label(c-string))
	end;
    assert(~null-pointer?(widget), "gtk-toggle/radio-button-new-with-label failed");
    make(<gadget-mirror>,
	 widget: widget,
	 sheet:  gadget)
  end
end method make-gtk-mirror;
||#

;;; We see these with no label in the gui tests.

(defmethod make-gtk-mirror ((gadget <gtk-check-button>))
  (multiple-value-bind (text image mnemonic index)
      (text-or-image-from-gadget-label gadget)
    (when image
      (ignoring "image label"))
    (when mnemonic
      ;; Furtle with text and stick a "_" in front of the mnemonic char.
      (setf text (%insert-gtk-mnemonic-indicator! text index mnemonic)))
    ;; FIXME: Use button from stock on this one?
    (let ((widget (if (push-button-like? gadget)
		      (if mnemonic
			  (cffi/gtk-toggle-button-new-with-mnemonic text)
			  (if (and text (string/= text ""))
			      (cffi/gtk-toggle-button-new-with-label text)
			      (cffi/gtk-toggle-button-new)))
		      (if mnemonic
			  (cffi/gtk-check-button-new-with-mnemonic text)
			  (if (and text (string/= text ""))
			      (cffi/gtk-check-button-new-with-label text)
			      (cffi/gtk-check-button-new))))))
      (when (null widget)
	(error "gtk-toggle/check-button-new-with-label failed"))
      (make-gtk-widget-mirror-instance (find-class '<gtk-gadget-mirror>)
				       :widget widget
				       :sheet  gadget))))


(defmethod port-handles-repaint? ((port <gtk-port>)
				  (gadget <gtk-check-button>))
  t)


#||
define method update-mirror-attributes
    (gadget :: <gtk-check-button>, mirror :: <gadget-mirror>) => ()
  next-method();
  let widget = mirror.mirror-widget;
  let selected? = gadget-value(gadget);
  with-disabled-event-handler (widget, #"clicked")
    gtk-toggle-button-set-active
      (widget, if (selected?) $true else $false end)
  end
end method update-mirror-attributes;
||#

(defmethod update-mirror-attributes ((gadget <gtk-check-button>)
				     (mirror <gtk-gadget-mirror>))
  (call-next-method)
  (let ((widget (mirror-widget mirror))
	(selected? (if (gadget-value gadget) +true+ +false+)))
    (with-disabled-event-handler (widget "clicked")
      (cffi/gtk-toggle-button-set-active widget selected?))))


#||
define sealed method note-gadget-value-changed
    (gadget :: <gtk-check-button>) => ()
  next-method();
  let mirror = sheet-direct-mirror(gadget);
  mirror & update-mirror-attributes(gadget, mirror)
end method note-gadget-value-changed;
||#

(defmethod note-gadget-value-changed ((gadget <gtk-check-button>))
  (call-next-method)
  (let ((mirror (sheet-direct-mirror gadget)))
    (and mirror (update-mirror-attributes gadget mirror))))



#||
/// Group box ---*** Use the fake one for now...



/// Scroll bars

define sealed class <gtk-scroll-bar>
    (<gtk-gadget-mixin>,
     <scroll-bar>,
     <leaf-pane>,
     <sealed-constructor-mixin>)
end class <gtk-scroll-bar>;
||#

#||
  CPL for <GTK-SCROLL-BAR>

  (#<STANDARD-CLASS GTK-DUIM::<GTK-SCROLL-BAR>>
   #<STANDARD-CLASS GTK-DUIM:<GTK-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<SCROLL-BAR>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<ORIENTED-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<SLUG-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<RANGE-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<VALUE-RANGE-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<CHANGING-VALUE-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<BASIC-VALUE-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<VALUE-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<VALUE-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<BASIC-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<ABSTRACT-GADGET>>
   #<STANDARD-CLASS GTK-DUIM:<GTK-PANE-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<STANDARD-INPUT-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<SHEET-WITH-EVENT-QUEUE-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<MIRRORED-SHEET-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<SHEET-WITH-RESOURCE-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS:<LEAF-PANE>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<CACHED-SPACE-REQUIREMENT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<CLIENT-OVERRIDABILITY-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<SPACE-REQUIREMENT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<LEAF-LAYOUT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<LAYOUT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS:<LAYOUT>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<BASIC-SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<ABSTRACT-SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<EVENT-HANDLER>>
   #<STANDARD-CLASS STANDARD-OBJECT>
   #<BUILT-IN-CLASS T>)

||#

;;; <gtk-scroll-bar> provides:
;;;
;;;    gadget-range-values
;;;    scroll-bar-adjusted-contents
;;;    <<changing-value-gadget-protocol>>
;;;        handle-gtk-value-changed-signal
;;;    <<scrolling-protocol>>
;;;        note-scroll-bar-changed
;;;    <<gadget-protocol>>
;;;        note-gadget-slug-size-changed
;;;    <<value-gadget-protocol>>
;;;        note-gadget-value-changed
;;;        note-gadget-value-range-changed

(defclass <gtk-scroll-bar>
    (<gtk-gadget-mixin>
     <scroll-bar>
     <leaf-pane>)
  ())


#||
define sealed class <gtk-horizontal-scroll-bar> (<gtk-scroll-bar>)
  keyword gtk-fixed-height?: = #t;
end class <gtk-horizontal-scroll-bar>;
||#

(defclass <gtk-horizontal-scroll-bar>
    (<gtk-scroll-bar>)
  ()
  (:default-initargs :gtk-fixed-height? t))


#||
define method %gtk-fixed-height?
    (gadget :: <gtk-horizontal-scroll-bar>)
 => (fixed? :: <boolean>)
  #t;
end method;
||#

(defmethod %gtk-fixed-height? ((gadget <gtk-horizontal-scroll-bar>))
  t)


#||
define sealed class <gtk-vertical-scroll-bar> (<gtk-scroll-bar>)
  keyword gtk-fixed-width?: = #t;
end class <gtk-vertical-scroll-bar>;
||#

(defclass <gtk-vertical-scroll-bar>
    (<gtk-scroll-bar>)
  ()
  (:default-initargs :gtk-fixed-width? t))


#||
define method %gtk-fixed-width?
    (gadget :: <gtk-vertical-scroll-bar>)
 => (fixed? :: <boolean>)
  #t;
end method;
||#

(defmethod %gtk-fixed-width? ((gadget <gtk-vertical-scroll-bar>))
  t)


#||
define sealed method class-for-make-pane 
    (framem :: <gtk-frame-manager>, class == <scroll-bar>, 
     #key orientation = #"horizontal")
 => (class :: <class>, options :: false-or(<sequence>))
  values(select (orientation)
	   #"horizontal" => <gtk-horizontal-scroll-bar>;
	   #"vertical"   => <gtk-vertical-scroll-bar>;
	 end,
	 #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <gtk-frame-manager>)
				(class (eql (find-class '<scroll-bar>)))
				&key (orientation :horizontal))
  (values (ecase orientation
	    (:horizontal (find-class '<gtk-horizontal-scroll-bar>))
	    (:vertical   (find-class '<gtk-vertical-scroll-bar>)))
	  nil))


#||
define function gadget-range-values
    (gadget :: <range-gadget-mixin>)
 => (start-value :: <real>, end-value :: <real>, increment :: <real>)
  let range = gadget-value-range(gadget);
  let n = range.size;
  select (n)
    0 => 
      values(0, 0, 0);
    1 => 
      let start = range[0];
      values(start, start, 0);
    otherwise =>
      let start = range[0];
      values(start, range[n - 1], range[1] - start)
  end;
end gadget-range-values;
||#

(defmethod gadget-range-values ((gadget <range-gadget-mixin>))
  (let* ((range (gadget-value-range gadget))
	 (n     (size range)))
    (case n
      (0 (values 0 0 0))
      (1 (let ((start (elt range 0)))
	   (values start start 0)))
      (otherwise
       (let ((start (elt range 0)))
	 (values start
		 (elt range (- n 1))
		 (- (elt range 1) start)))))))


#||
define method scroll-bar-adjusted-contents
    (gadget :: <gtk-scroll-bar>)
 => (value :: <single-float>,
     lower :: <single-float>, upper :: <single-float>,
     step-increment :: <single-float>, page-increment :: <single-float>,
     page-size :: <single-float>)
  let range-value = gadget-value(gadget);
  let (range-start, range-end, range-step) = gadget-range-values(gadget);
  let slug-size = gadget-slug-size(gadget);

  let lower = as(<single-float>, range-start);
  let page-size = as(<single-float>, slug-size);
  let step-increment = as(<single-float>, range-step);
  let page-increment = max(page-size, step-increment);
  let upper = as(<single-float>, range-end); // this inclues page size.
  let value = as(<single-float>, range-value);

  values(value, lower, upper, step-increment, page-increment, page-size)
end scroll-bar-adjusted-contents;
||#

(defmethod scroll-bar-adjusted-contents ((gadget <gtk-scroll-bar>))
  (let ((range-value (gadget-value gadget)))
    (multiple-value-bind (range-start range-end range-step)
	(gadget-range-values gadget)
      (let* ((slug-size (gadget-slug-size gadget))
	     (lower (float range-start 1.0d0))
	     (page-size (float slug-size 1.0d0))
	     (step-increment (float range-step 1.0d0))
	     (page-increment (max page-size step-increment))
	     (upper (float range-end 1.0d0))   ;; this includes page size
	     (value (float range-value 1.0d0)))
	(values value lower upper step-increment page-increment page-size)))))


#||
define sealed method make-gtk-mirror
    (gadget :: <gtk-scroll-bar>)
 => (mirror :: <gadget-mirror>)
  let (value, lower, upper, step-inc, page-inc, page-size)
    = scroll-bar-adjusted-contents(gadget);
  let adj = GTK-ADJUSTMENT(gtk-adjustment-new(value,
                                              lower,
                                              upper,
                                              step-inc,
                                              page-inc,
                                              page-size));
  let widget = select(gadget-orientation(gadget))
		 #"horizontal" => GTK-HSCROLLBAR(gtk-hscrollbar-new(adj));
		 #"vertical"   => GTK-VSCROLLBAR(gtk-vscrollbar-new(adj));
	       end;
  assert(~null-pointer?(widget), "gtk-h/vscrollbar-new failed");
  // --- Does DUIM have anything to select/deselect smooth scrolling?
  // gtk-range-set-update-policy(widget, $gtk-update-discontinuous);
  make(<gadget-mirror>,
       widget: widget,
       sheet:  gadget)
end method make-gtk-mirror;
||#

(defmethod make-gtk-mirror ((gadget <gtk-scroll-bar>))
  (multiple-value-bind (value lower upper step-inc page-inc page-size)
      (scroll-bar-adjusted-contents gadget)
    (let* ((adj (cffi/gtk-adjustment-new value lower upper step-inc page-inc page-size))
	   (widget (ecase (gadget-orientation gadget)
		     (:horizontal (cffi/gtk-hscrollbar-new adj))
		     (:vertical   (cffi/gtk-vscrollbar-new adj)))))
      (when (null widget)
	(error "gtk-h/vscrollbar-new failed"))
      ;; --- Does DUIM have anything to select/deselect smooth scrolling?
      ;;(gtk-range-set-update-policy widget $gtk-update-discontinuous)
      (make-gtk-widget-mirror-instance (find-class '<gtk-gadget-mirror>)
				       :widget widget
				       :sheet  gadget))))


(defmethod port-handles-repaint? ((port <gtk-port>)
				  (gadget <gtk-scroll-bar>))
  t)


#||
define method install-event-handlers
    (sheet :: <gtk-scroll-bar>, mirror :: <gadget-mirror>) => ()
  next-method();
  let adj = gtk-range-get-adjustment(mirror-widget(mirror));
  install-named-handlers(mirror, #[#"adjustment/value_changed"],
			 adjustment: adj);
end method install-event-handlers;
||#

(defmethod install-event-handlers ((sheet <gtk-scroll-bar>)
				   (mirror <gtk-gadget-mirror>))
  (let ((adj (cffi/gtk-range-get-adjustment (mirror-widget mirror))))
    (install-named-handlers mirror #("value-changed") :swap adj)))


#||
define method gtk-adjustment-value-changed-signal-handler
    (gadget :: <gtk-scroll-bar>, adjustment :: <GtkAdjustment*>) => ()
  let value = adjustment.value-value;
  scroll-to-position(gadget, value);
end;
||#

(defmethod handle-gtk-value-changed-signal ((gadget <gtk-scroll-bar>)
					    widget
					    adjustment)
  (declare (ignore widget))
  ;; NOTE: because this signal is connected using g-signal-connect-SWAPPED, the
  ;; adjustment that emitted the signal is actually passed in the user-data
  ;; parameter, so I've renamed the parameters for readability.
  (let ((value (floor (cffi/gtk_adjustment_get_value adjustment))))
    (scroll-to-position gadget value)))


#||
define sealed method update-mirror-attributes
    (gadget :: <gtk-scroll-bar>, mirror :: <gadget-mirror>) => ()
  next-method();
//  ignoring("update-mirror-attributes on <scroll-bar>")
end method update-mirror-attributes;
||#

(defmethod update-mirror-attributes ((gadget <gtk-scroll-bar>)
				     (mirror <gtk-gadget-mirror>))
  (call-next-method)
  ;; Make the scroll-bar reflect the offsets as set up in DUIM
  (note-scroll-bar-changed gadget))


#||
define sealed method note-gadget-slug-size-changed
    (gadget :: <gtk-scroll-bar>) => ()
  next-method();
  note-scroll-bar-changed(gadget);
end method note-gadget-slug-size-changed;
||#

(defmethod note-gadget-slug-size-changed ((gadget <gtk-scroll-bar>))
  (call-next-method)
  (note-scroll-bar-changed gadget))


#||
define sealed method note-gadget-value-changed
    (gadget :: <gtk-scroll-bar>) => ()
  next-method();
  note-scroll-bar-changed(gadget);
end method note-gadget-value-changed;
||#

(defmethod note-gadget-value-changed ((gadget <gtk-scroll-bar>))
  (call-next-method)
  (note-scroll-bar-changed gadget))


#||
define sealed method note-gadget-value-range-changed
    (gadget :: <gtk-scroll-bar>) => ()
  next-method();
  note-scroll-bar-changed(gadget);
end method note-gadget-value-range-changed;
||#

(defmethod note-gadget-value-range-changed ((gadget <gtk-scroll-bar>))
  (call-next-method)
  (note-scroll-bar-changed gadget))


#||
define sealed method note-scroll-bar-changed
    (gadget :: <gtk-scroll-bar>) => ()
  let widget = gadget-widget(gadget);
  when (widget)
    let (value, lower, upper, step-inc, page-inc, page-size)
      = scroll-bar-adjusted-contents(gadget);
    let adjustment :: <GtkAdjustment*> = gtk-range-get-adjustment(widget);
    adjustment.lower-value := lower;
    adjustment.upper-value := upper;
    adjustment.value-value := value;
    adjustment.step-increment-value := step-inc;
    adjustment.page-increment-value := page-inc;
    adjustment.page-size-value := page-size;
    // --- TODO: cache gtk-signal-lookup
    with-c-string (name = "changed")
      gtk-signal-emitv-by-name(adjustment, name, null-pointer(<GtkArg*>));
    end;
  end;
end method note-scroll-bar-changed;
||#

(defmethod note-scroll-bar-changed ((gadget <gtk-scroll-bar>))
  (let ((widget (gadget-widget gadget)))
    (when widget
      (multiple-value-bind (value lower upper step-inc page-inc page-size)
	  (scroll-bar-adjusted-contents gadget)
	(let ((adjustment (cffi/gtk-range-get-adjustment widget)))
	  (cffi/gtk_adjustment_configure adjustment
					 value lower upper
					 step-inc page-inc page-size)
	  (cffi/gtk-signal-emitv-by-name adjustment "changed" nil))))))



#||

/// List gadgets

define sealed class <gtk-list-control-mixin> 
    (<gtk-gadget-mixin>,
     <collection-gadget>,
     <sealed-constructor-mixin>)
end class <gtk-list-control-mixin>;
||#

#||
  CPL for <GTK-LIST-CONTROL-MIXIN>

  (#<STANDARD-CLASS GTK-DUIM::<GTK-LIST-CONTROL-MIXIN>>
   #<STANDARD-CLASS GTK-DUIM:<GTK-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<COLLECTION-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<VALUE-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<ABSTRACT-GADGET>>
   #<STANDARD-CLASS GTK-DUIM:<GTK-PANE-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<STANDARD-INPUT-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<SHEET-WITH-EVENT-QUEUE-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<MIRRORED-SHEET-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<SHEET-WITH-RESOURCE-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS:<ABSTRACT-SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<EVENT-HANDLER>>
   #<STANDARD-CLASS STANDARD-OBJECT>
   #<BUILT-IN-CLASS T>)

||#

;;; <gtk-list-control-mixin> provides:
;;;
;;;    <<value-gadget-protocol>>
;;;        handle-gtk-changed-signal
;;;        note-gadget-value-changed
;;;    <<action-gadget-protocol>>
;;;        handle-gtk-row-activated-signal
;;;    <<collection-gadget-protocol>>
;;;        list-selection
;;;        note-gadget-items-changed
;;;        update-mirror-selection
;;; update-gadget
;;; update-list-control-items

;; FIXME: How to do borders/scrolling? -- combo + combo box entry do scrolling automatically
;; A mixin for things built on GtkTreeView (list, table, and tree views)
(defclass <gtk-tree-view-control-mixin>
    (<gtk-gadget-mixin>
     <collection-gadget>)
  ;; XXX: We could cache some of the gtk tree view artifacts here...
  (#-(and)(%list-store :initform nil :accessor %list-store)
   ;; Number of columns in the list-store for this list object. A list box
   ;; has a single text column, a list control can additionally have an image
   #-(and)(%ncolumns   :initform 1 :accessor %ncolumns)))


#||
define method update-mirror-attributes
    (gadget :: <gtk-list-control-mixin>, mirror :: <gadget-mirror>) => ()
  next-method();
  let widget = GTK-CLIST(mirror.mirror-widget);
  gtk-clist-set-selection-mode
    (widget,
     select (gadget-selection-mode(gadget))
       #"none"     => $GTK-SELECTION-BROWSE;
       #"single"   => $GTK-SELECTION-SINGLE;
       #"multiple" => $GTK-SELECTION-EXTENDED;
     end);
  gtk-clist-set-shadow-type(widget, $GTK-SHADOW-IN);
  if (instance?(gadget, <table-control>))
    gtk-clist-column-titles-show(widget)
  else
    gtk-clist-column-titles-hide(widget);
    //---*** How should we decide this?
    gtk-clist-set-column-width(widget, 0, 500)
  end;
  update-list-control-items(gadget, mirror)
end method update-mirror-attributes;
||#

(defmethod update-mirror-attributes ((gadget <gtk-tree-view-control-mixin>)
				     (mirror <gtk-gadget-mirror>))
  (call-next-method)
  (let* ((widget         (mirror-widget mirror))
	 (tree-selection (cffi/gtk-tree-view-get-selection widget)))
    (cffi/gtk-tree-selection-set-mode tree-selection
				      (ecase (gadget-selection-mode gadget)
					(:none     +CFFI/GTK-SELECTION-NONE+)
					(:single   +CFFI/GTK-SELECTION-BROWSE+)
					(:multiple +CFFI/GTK-SELECTION-MULTIPLE+)))
    (if (INSTANCE? gadget '<table-control>)
	(cffi/gtk-tree-view-set-headers-visible widget +true+)
	(cffi/gtk-tree-view-set-headers-visible widget +false+))
    (update-gtk-tree-view-control-items gadget mirror)
    (update-mirror-selection gadget)))


#||
define method install-event-handlers
    (sheet :: <gtk-list-control-mixin>, mirror :: <gadget-mirror>) => ()
  next-method();
  install-named-handlers(mirror,
			 #[#"select_row", #"button_press_event"])
end method install-event-handlers;
||#

(defmethod install-event-handlers ((sheet <gtk-tree-view-control-mixin>)
				   (mirror <gtk-gadget-mirror>))
  (let ((tree-selection (cffi/gtk-tree-view-get-selection (mirror-widget mirror))))
    ;; This signal is emitted by the selection object, but we want to pretend
    ;; it came from the list-control, and deal with it as if it came from the
    ;; list-control (so it's connected using gtk-signal-connect-swapped).
    (install-named-handlers mirror #("changed") :swap tree-selection)
    (install-named-handlers mirror #("row-activated"))))


#||
define sealed method handle-gtk-select-row-event
    (gadget :: <gtk-list-control-mixin>, widget :: <GtkWidget*>,
     event :: <GdkEventAny*>)
 => (handled? :: <boolean>)
  gtk-debug("Clicked on list control!");
  let selection = list-selection(gadget, sheet-direct-mirror(gadget));
  gtk-debug("  Selection now %=", selection);
  distribute-selection-changed-callback(gadget, selection);
  #t
end method handle-gtk-select-row-event;

define sealed method handle-gtk-button-press-event
    (gadget :: <gtk-list-control-mixin>, widget :: <GtkWidget*>,
     event :: <GdkEventButton*>)
 => (handled? :: <boolean>)
  gtk-debug("Pressed button %=, type %=",
		event.button-value,
		select (event.type-value)
		  $GDK-BUTTON-PRESS  => "button press";
		  $GDK-2BUTTON-PRESS => "double click";
		  $GDK-3BUTTON-PRESS => "treble click";
		  otherwise => event.type-value;
		end);
  if (event.type-value == $GDK-2BUTTON-PRESS)
    gtk-debug("Double clicked on list control!");
    when (gadget-activate-callback(gadget))
      distribute-activate-callback(gadget);
    end;
    #t
  end
end method handle-gtk-button-press-event;
||#

(defmethod handle-gtk-changed-signal ((gadget <gtk-tree-view-control-mixin>)
				      widget
				      selection)
  (declare (ignore widget selection))
  ;; This signal is connected using g-signal-connect-SWAPPED since it's the
  ;; "selection" object that emits the signal but we want to handle it on
  ;; the list-control.
  (let ((selection (mirror-selection gadget (sheet-direct-mirror gadget))))
    (distribute-selection-changed-callback gadget selection))
  nil)


(defmethod handle-gtk-row-activated-signal ((gadget <gtk-tree-view-control-mixin>)
					    widget
					    path
					    column
					    user-data)
  (declare (ignore widget path column user-data))
  (when (gadget-activate-callback gadget)
    (distribute-activate-callback gadget))
  nil)


#||
define method list-selection
    (gadget :: <gtk-list-control-mixin>, mirror :: <gadget-mirror>)
 => (vector :: <vector>)
  let widget = GTK-CLIST(mirror.mirror-widget);
  let selection = widget.selection-value;
  glist-to-vector(selection, <integer>)
end method list-selection;
||#

;; Returns the selection from the native widget as a vector of "tree path"s.
(defmethod mirror-selection ((gadget <gtk-tree-view-control-mixin>)
			     (mirror <gtk-gadget-mirror>))
  (let* ((widget (mirror-widget mirror))
	 (selection (cffi/gtk-tree-view-get-selection widget))
	 (model nil)
	 (selected-rows (cffi/gtk-tree-selection-get-selected-rows selection model)))
    ;; selected-rows is a (pointer to a) GList containing a GtkTreePath for each
    ;; selected row.
    (let ((vector (make-stretchy-vector)))
      (%walk-glist #'(lambda (data)
		       (let ((path (cffi:mem-ref (cffi/gtk-tree-path-get-indices data) 'gint)))
			 (add! vector path)
			 (cffi/gtk-tree-path-free data)))
		   selected-rows)
      (unless (or (null selected-rows) (and (cffi:pointerp selected-rows) (cffi:null-pointer-p selected-rows)))
	;; XXX: Pressing keys when the list-box has focus can lead here with no selected rows,
	;; generally when the user types a letter that GTK can't find in the list box - gtk deselects
	;; the selection then.
	(cffi/g_list_free selected-rows))
      ;; Freeing the selection causes bad things to happen..
      #-(and)
      (cffi/g-free selection)
      vector)))


(defun %walk-glist (fn glist)
  (labels ((process-list (list)
	     (cond ((or (null list) (cffi:null-pointer-p list)) nil)
		   (t (let ((data (cffi/glist->data list)))
			(funcall fn data)
			(process-list (cffi/g_list_next list)))))))
    (process-list glist)))


#||
define method glist-to-vector
    (GList :: <GList*>, type :: <type>)
 => (vector :: <stretchy-object-vector>)
  let vector = make(<stretchy-object-vector>);
  local method process-list
	    (GList :: <GList*>)
	  case
	    null-pointer?(GList) =>
	      #f;
	    otherwise =>
	      add!(vector, c-type-cast(type, glist.data-value));
	      process-list(glist.next-value);
	  end
	end;
  process-list(GList);
  vector
end method glist-to-vector;


define sealed method note-gadget-items-changed
    (gadget :: <gtk-list-control-mixin>) => ()
  next-method();
  let mirror = sheet-direct-mirror(gadget);
  mirror & update-list-control-items(gadget, mirror)
end method note-gadget-items-changed;
||#

(defmethod note-gadget-items-changed ((gadget <gtk-tree-view-control-mixin>))
  ;; called when duim modifies the list contents
  (call-next-method)
  (let ((mirror (sheet-direct-mirror gadget)))
    (and mirror (update-gtk-tree-view-control-items gadget mirror))))


#||
define method update-gadget
    (gadget :: <gtk-list-control-mixin>) => ()
  // No, we don't call 'next-method' here!
  let mirror = sheet-direct-mirror(gadget);
  mirror & update-list-control-items(gadget, mirror)
end method update-gadget;


define sealed method update-list-control-items
    (gadget :: <gtk-list-control-mixin>, mirror :: <gadget-mirror>)
 => ()
  let widget = GTK-CLIST(mirror.mirror-widget);
  let items = gadget-items(gadget);
  let label-function = gadget-label-key(gadget);
  gtk-clist-clear(widget);
  with-stack-structure(string* :: <C-string*>)
    for (item in items)
      let label = label-function(item);
      with-c-string (string = label)
	string*[0] := string;
	gtk-clist-append(widget, pointer-cast(<gchar**>, string*))
      end;
    end;
  end
end method update-list-control-items;
||#

;; _store-kind_ is one of :list or :tree
(defmethod make-gtk-tree-view-store ((gadget <gtk-tree-view-control-mixin>) store-kind &rest types)
  (let ((ncols (length types)))
    (cffi:with-foreign-object (gtypes :ulong ncols)
      (loop for i from 0 to (- ncols 1)
	 for type in types
         do (setf (cffi:mem-aref gtypes :ulong i) type))
      (let* ((store (ecase store-kind
		      (:list (cffi/gtk-list-store-newv ncols gtypes))
		      (:tree (cffi/gtk-tree-store-newv ncols gtypes)))))
	;; _store_ is returned from constructor with a ref count of 1
	(when (null store)
	  (error "Failed to make GtkTreeView store"))
	store))))


(defmethod update-gtk-tree-view-model ((gadget <gtk-tree-view-control-mixin>) widget store)
  ;; FIXME: Perhaps this should mutate any existing model instead of
  ;; just recreating it...?
  (let ((selection (cffi/gtk-tree-view-get-selection widget)))
    ;; XXX: Does the selection need to be freed?
    (with-disabled-event-handler (widget "changed" :swap selection)
      ;; XXX: If there's an existing store associated with widget
      ;; should it be freed?
      (cffi/gtk-tree-view-set-model widget store))))


;; See DUIM-EXTENDED;PANES;LIST-CONTROL-PANES for some ideas!
;; XXX: Perhaps the new and old items should be merged but for now
;; just cons up a new list store
(defmethod update-gtk-tree-view-control-items ((gadget <gtk-tree-view-control-mixin>)
					       (mirror <gtk-gadget-mirror>)
					       &key (store-kind :list))
  ;; XXX: When we get around to writing native tree controls they need to specialize this
  ;; and arrange to pass along the correct :store-kind before calling this method.
  ;; XXX: "New" Dylan version caches the list store.
  (let* ((widget         (mirror-widget mirror))
	 (gadget-items   (gadget-items gadget))
	 (items          (make-stretchy-vector :contents gadget-items))   ; can be a sequence or a <range>.
	 (label-function (gadget-label-key gadget))
	 (icon-function  (list-control-icon-function gadget))
	 ;; Store column always contains a label, even if it is the empty string.
	 (store          (if icon-function
			     (make-gtk-tree-view-store gadget store-kind
						       (cffi/gdk-pixbuf-get-type)
						       +CFFI/G-TYPE-STRING+)
			     (make-gtk-tree-view-store gadget store-kind
						       +CFFI/G-TYPE-STRING+))))
    (cffi/with-stack-structure (iter '(:struct GtkTreeIter))
      ;; fixme: not sure about the way (:struct GValue) is being handled here vvv
      (let* ((cinit   (if icon-function '(0 1) '(0)))
	     (columns (cffi:foreign-alloc :int :initial-contents cinit))   ; ARRAY of column numbers
	     (ncols   (if icon-function 2 1))
	     (gvalue-size (cffi:foreign-type-size '(:struct GValue)))
	     ;; ARRAY of GValues. Memory for GValues must be zeroed out...
	     (values      (cffi/g-slice-alloc0 (* ncols gvalue-size)))
	     ;; pointer to text column GValue in 'values'
	     (textv-ptr (if icon-function
			    (cffi:mem-aptr values '(:struct GValue) 1)
			    (cffi:mem-aptr values '(:struct GValue) 0)))
	     ;; pointer to pixbuf column GValue in 'values'
	     (iconv-ptr (if icon-function (cffi:mem-aref values '(:struct GValue) 0) nil)))
	(cffi/g-value-init textv-ptr +CFFI/G-TYPE-STRING+)
	(when iconv-ptr
	  (cffi/g-value-init iconv-ptr (cffi/gdk-pixbuf-get-type)))
	(map nil #'(lambda (item)
		     (if (eql store-kind :list)
			 (cffi/gtk-list-store-append store iter)
			 (cffi/gtk-tree-store-append store iter))
		     (let* ((label  (or (and label-function (funcall label-function item)) ""))
			    (icon   (and icon-function (funcall icon-function item)))
			    (pixbuf (and icon (pixmap-to-gdk-pixbuf icon))))
		       (cffi/g-value-reset textv-ptr)
		       (cffi/g-value-set-string textv-ptr label)
		       (when icon
			 (cffi/g-value-reset iconv-ptr)
			 (cffi/g-value-set-object iconv-ptr pixbuf))
		       (if (eql store-kind :list)
			   (cffi/gtk-list-store-set-valuesv store iter columns values ncols)
			   (cffi/gtk-tree-store-set-valuesv store iter columns values ncols))))
	     items)
	(cffi:foreign-free columns)
	(cffi/g-slice-free1 (* ncols gvalue-size) values)))
    (update-gtk-tree-view-model gadget widget store)
    (cffi/g-object-unref store))
  gadget)


#||
define sealed method update-gadget-selection
    (gadget :: <gtk-list-control-mixin>) => ()
  select (gadget-selection-mode(gadget))
    #"none" =>
      #f;
    #"single" =>
      ignoring("update-gadget-selection");
    #"multiple" =>
      ignoring("update-gadget-selection");
  end
end method update-gadget-selection;
||#

(defmethod update-mirror-selection ((gadget <gtk-tree-view-control-mixin>))
  (let ((widget (gadget-widget gadget)))
    (when widget
      (let ((selection (cffi/gtk-tree-view-get-selection widget)))
	;; XXX: Is this the right event handler to be disabling?
	(with-disabled-event-handler (widget "changed" :swap selection)
	  (cffi/gtk-tree-selection-unselect-all selection)
	  (unless (eql (gadget-selection-mode gadget) :none)
	    (when (> (length (gadget-selection gadget)) 0)
	      (map nil #'(lambda (path)
			   (let ((tree-path (cffi/gtk-tree-path-new-from-string (format nil "~a" path))))
			     (cffi/gtk-tree-selection-select-path selection tree-path)))
		   (gadget-selection gadget)))))))))


#||
define sealed method note-gadget-value-changed
    (gadget :: <gtk-list-control-mixin>) => ()
  next-method();
  update-gadget-selection(gadget)
end method note-gadget-value-changed;
||#

(defmethod note-gadget-value-changed ((gadget <gtk-tree-view-control-mixin>))
  (call-next-method)
  (update-mirror-selection gadget))


#||
// List boxes

define sealed class <gtk-list-box> 
    (<gtk-list-control-mixin>,
     <list-box>,
     <leaf-pane>)
end class <gtk-list-box>;
||#

#||
  CPL for <GTK-LIST-BOX>

  (#<STANDARD-CLASS GTK-DUIM::<GTK-LIST-BOX>>
   #<STANDARD-CLASS GTK-DUIM::<GTK-LIST-CONTROL-MIXIN>>
   #<STANDARD-CLASS GTK-DUIM:<GTK-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<LIST-BOX>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<BORDERED-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<SCROLLING-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<ACTION-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<ACTION-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<BASIC-CHOICE-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<GADGET-SELECTION-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<COLLECTION-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<VALUE-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<COLLECTION-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<VALUE-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<BASIC-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<ABSTRACT-GADGET>>
   #<STANDARD-CLASS GTK-DUIM:<GTK-PANE-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<STANDARD-INPUT-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<SHEET-WITH-EVENT-QUEUE-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<MIRRORED-SHEET-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<SHEET-WITH-RESOURCE-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS:<LEAF-PANE>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<CACHED-SPACE-REQUIREMENT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<CLIENT-OVERRIDABILITY-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<SPACE-REQUIREMENT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<LEAF-LAYOUT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<LAYOUT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS:<LAYOUT>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<BASIC-SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<ABSTRACT-SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<EVENT-HANDLER>>
   #<STANDARD-CLASS STANDARD-OBJECT>
   #<BUILT-IN-CLASS T>)

||#

;;; <gtk-list-box> provides:
;;;
;;; nothing

(defclass <gtk-list-box>
    (<gtk-tree-view-control-mixin>
     <list-box>
     <leaf-pane>)
  ())


#||
define sealed method class-for-make-pane 
    (framem :: <gtk-frame-manager>, class == <list-box>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<gtk-list-box>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <gtk-frame-manager>)
				(class (eql (find-class '<list-box>)))
				&key)
  (values (find-class '<gtk-list-box> nil)))


#||
define sealed method make-gtk-mirror
    (gadget :: <gtk-list-box>)
 => (mirror :: <gadget-mirror>)
  let widget = GTK-CLIST(gtk-clist-new(1));
  assert(~null-pointer?(widget), "gtk-clist-new failed");
  make(<gadget-mirror>,
       widget: widget,
       sheet:  gadget)
end method make-gtk-mirror;
||#


(defmethod %configure-mirror-columns ((gadget <gtk-list-box>) widget)
  ;; This is a little complicated because we're configuring the
  ;; columns before the model is associated with the tree...
  (let* ((renderer (cffi/gtk-cell-renderer-text-new))
	 (column (cffi/gtk-tree-view-column-new)))
    (cffi/gtk-tree-view-column-pack-start column renderer +FALSE+)
    ;; Get text from item 0 in the list model
    (cffi/gtk-tree-view-column-add-attribute column renderer "text" 0)
    (cffi/gtk-tree-view-append-column widget column)))


;; Maybe we should just have a specific 'update-gtk-tree-view-control-items'
;; method for list boxes, but it seems a shame to not share the existing one
;; with the list-control and tree-control just for the sake of not defining
;; the following method...
(defmethod list-control-icon-function ((gadget <gtk-tree-view-control-mixin>))
  nil)


;; XXX: A list box can only display a single text column. If you want to
;; display an icon alongside the text, you need to use the list CONTROL.
(defmethod make-gtk-mirror ((gadget <gtk-list-box>))
  ;; XXX: This is specific to the <list-box> - single column containing a string.
  ;; XXX: We don't have the model yet; that comes in update-mirror-attributes
  ;; XXX: Maybe all this should be done there? It would be easier...
  (let ((widget (cffi/gtk-tree-view-new)))
    (when (null widget)
      (error "gtk-tree-view-new failed"))
    (%configure-mirror-columns gadget widget)
    (make-gtk-widget-mirror-instance (find-class '<gtk-gadget-mirror>)
				     :widget widget
				     :sheet  gadget)))


(defmethod port-handles-repaint? ((port <gtk-port>)
				  (gadget <gtk-list-box>))
  t)



#||
/// Option boxes

define sealed class <gtk-option-box> 
    (<gtk-list-control-mixin>,
     <option-box>,
     <leaf-pane>,
     <sealed-constructor-mixin>)
end class <gtk-option-box>;
||#

#||
  CPL for <GTK-OPTION-BOX>

  (#<STANDARD-CLASS GTK-DUIM::<GTK-OPTION-BOX>>
   #<STANDARD-CLASS GTK-DUIM::<GTK-LIST-CONTROL-MIXIN>>
   #<STANDARD-CLASS GTK-DUIM:<GTK-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<OPTION-BOX>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<BORDERED-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<SCROLLING-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<BASIC-CHOICE-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<GADGET-SELECTION-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<COLLECTION-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<VALUE-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<COLLECTION-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<VALUE-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<BASIC-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<ABSTRACT-GADGET>>
   #<STANDARD-CLASS GTK-DUIM:<GTK-PANE-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<STANDARD-INPUT-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<SHEET-WITH-EVENT-QUEUE-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<MIRRORED-SHEET-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<SHEET-WITH-RESOURCE-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS:<LEAF-PANE>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<CACHED-SPACE-REQUIREMENT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<CLIENT-OVERRIDABILITY-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<SPACE-REQUIREMENT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<LEAF-LAYOUT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<LAYOUT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS:<LAYOUT>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<BASIC-SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<ABSTRACT-SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<EVENT-HANDLER>>
   #<STANDARD-CLASS STANDARD-OBJECT>
   #<BUILT-IN-CLASS T>)

||#

;;; <gtk-option-box> provides:
;;;
;;; list-selection
;;; note-gadget-items-changed
;;; update-gadget-selection
;;; note-gadget-value-changed


;;; FIXME: WHAT PROVIDES THE BORDERS FOR THESE? WHAT PROVIDES THE SCROLLER? DITTO FOR LIST BOXES ETC.
;;; Scrolling is automatic.

(defclass <gtk-option-box>
    (<gtk-tree-view-control-mixin>
     <option-box>
     <leaf-pane>)
  ())

#||
define sealed method class-for-make-pane 
    (framem :: <gtk-frame-manager>, class == <option-box>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<gtk-option-box>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <gtk-frame-manager>)
				(class (eql (find-class '<option-box>)))
				&key)
  (values (find-class '<gtk-option-box>) nil))


#||
define sealed method make-gtk-mirror
    (gadget :: <gtk-option-box>)
 => (mirror :: <gadget-mirror>)
  let widget = GTK-CLIST(gtk-clist-new(1));
  assert(~null-pointer?(widget), "gtk-clist-new failed");
  make(<gadget-mirror>,
       widget: widget,
       sheet:  gadget)
end method make-gtk-mirror;
||#

(defmethod %configure-mirror-columns ((gadget <gtk-option-box>) widget)
  (let ((renderer (cffi/gtk-cell-renderer-text-new)))
    (cffi/gtk-cell-layout-pack-start widget renderer +FALSE+)
    ;; Tell the renderer to get the text from item 0 in the list model
    ;; XXX: How to generalise this for multiple columns?
    (cffi/gtk-cell-layout-add-attribute widget renderer "text" 0)))


(defmethod make-gtk-mirror ((gadget <gtk-option-box>))
  ;; XXX: Is this the same deal as <list-box>? Or is it more like a <list-control>?
  ;; Perhaps it's a <table-control>...
  (let ((widget (cffi/gtk-combo-box-new)))
    (when (null widget)
      (error "gtk-combo-box-new failed"))
    ;; FIXME: get-renderer-for-column-type ?
    (%configure-mirror-columns gadget widget)
    (make-gtk-widget-mirror-instance (find-class '<gtk-gadget-mirror>)
				     :widget widget
				     :sheet  gadget)))


(defmethod install-event-handlers ((sheet <gtk-option-box>)
				   (mirror <gtk-gadget-mirror>))
  (install-named-handlers mirror #("changed")))


(defmethod port-handles-repaint? ((port <gtk-port>)
				  (gadget <gtk-option-box>))
  t)


(defmethod %gtk-fixed-height? ((gadget <gtk-option-box>))
  t)


(defmethod update-gtk-tree-view-model ((gadget <gtk-option-box>) widget store)
  (with-disabled-event-handler (widget "changed")
    (cffi/gtk-combo-box-set-model widget store)))


(defmethod update-mirror-attributes ((gadget <gtk-option-box>)
				     (mirror <gtk-gadget-mirror>))
  ;; No CALL-NEXT-METHOD (off to <gtk-list-control-mixin>); the "attributes"
  ;; for <gtk-option-box> instances are significantly different to those of
  ;; list controls.
;;   (call-next-method)
  (update-gtk-tree-view-control-items gadget mirror)
  ;; It would be nice to use "UPDATE-GADGET-SELECTION" here but the gadget
  ;; hasn't been associated with the mirror yet (and no mirror parameter is
  ;; accepted by that method).
  ;;(update-gadget-selection gadget))
  (let* ((selection (gadget-selection gadget))
	 (widget    (mirror-widget mirror))
	 ;; there should only ever be 1 (or 0) - ignore extras if more.
	 (selected  (if (> (length selection) 0)
			(elt selection 0)
			-1)))
    (with-disabled-event-handler (widget "changed")
      (cffi/gtk-combo-box-set-active widget selected))))


(defmethod mirror-selection ((gadget <gtk-option-box>)
			     (mirror <gtk-gadget-mirror>))
  ;; We're a <gtk-tree-view-control-mixin>, so use the method on that class to
  ;; handle the event. All the work is done in this method anyway so just
  ;; implement this to deal with the "changed" signal.
  (let* ((widget    (mirror-widget mirror))
	 (selection (cffi/gtk-combo-box-get-active widget)))
    (if (= -1 selection)   ; -1 = no selection
	#()
	(vector selection))))


#||
define sealed method note-gadget-items-changed
    (gadget :: <gtk-option-box>) => ()
  next-method();
  ignoring("note-gadget-items-changed on <option-box>")
end method note-gadget-items-changed;
||#

(defmethod note-gadget-items-changed ((gadget <gtk-option-box>))
  ;; off to <gtk-tree-view-control-mixin> to repopulate the box
  (call-next-method))


#||
define sealed method update-gadget-selection
    (gadget :: <gtk-option-box>) => ()
  ignoring("update-gadget-selection on <option-box>")
end method update-gadget-selection;
||#

(defmethod update-mirror-selection ((gadget <gtk-option-box>))
  (let ((selection (gadget-selection gadget))
	(widget    (gadget-widget gadget)))
    (when widget
      ;; FIXME? Could do some more sanity checking... throw out some debug if
      ;; the selection has > 1 entry, etc.?
      (let ((selected (if (> (length selection) 0)
			  (elt selection 0)
			  -1)))
	(with-disabled-event-handler (widget "changed")
	  (cffi/gtk-combo-box-set-active widget selected))))))


#||
define sealed method note-gadget-value-changed
    (gadget :: <gtk-option-box>) => ()
  next-method();
  update-gadget-selection(gadget)
end method note-gadget-value-changed;
||#

(defmethod note-gadget-value-changed ((gadget <gtk-option-box>))
  (call-next-method)
  (update-mirror-selection gadget))



#||
/// Combo boxes

define sealed class <gtk-combo-box> 
    (<gtk-list-control-mixin>,
     <combo-box>,
     <leaf-pane>,
     <sealed-constructor-mixin>)
end class <gtk-combo-box>;
||#


#||
  CPL for <GTK-COMBO-BOX>

  (#<STANDARD-CLASS GTK-DUIM::<GTK-COMBO-BOX>>
   #<STANDARD-CLASS GTK-DUIM::<GTK-LIST-CONTROL-MIXIN>>
   #<STANDARD-CLASS GTK-DUIM:<GTK-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<COMBO-BOX>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<BORDERED-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<SCROLLING-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<ACTION-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<ACTION-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<CHANGING-VALUE-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<TEXT-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<BASIC-CHOICE-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<GADGET-SELECTION-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<COLLECTION-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<VALUE-GADGET-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS:<COLLECTION-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<VALUE-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<BASIC-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<ABSTRACT-GADGET>>
   #<STANDARD-CLASS GTK-DUIM:<GTK-PANE-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<STANDARD-INPUT-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<SHEET-WITH-EVENT-QUEUE-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<MIRRORED-SHEET-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<SHEET-WITH-RESOURCE-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS:<LEAF-PANE>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<CACHED-SPACE-REQUIREMENT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<CLIENT-OVERRIDABILITY-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<SPACE-REQUIREMENT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<LEAF-LAYOUT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<LAYOUT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS:<LAYOUT>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<BASIC-SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<ABSTRACT-SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<EVENT-HANDLER>>
   #<STANDARD-CLASS STANDARD-OBJECT>
   #<BUILT-IN-CLASS T>)

||#

;;; <gtk-combo-box> provides:
;;;
;;;    <<collection-gadget-protocol>>
;;;        note-gadget-items-changed
;;;        update-gadget-selection
;;;    <<value-gadget-protocol>>
;;;        note-gadget-value-changed
;;;        handle-gtk-changed-signal
;;;    <<action-gadget-protocol>>
;;;        handle-gtk-activate-signal
;;;
;;; FIXME: What about all the text protocols?

;;; FIXME: should the text-gadget-mixin protocols be replicated for
;;; combo boxes? (%changed? etc.) -- YES THEY SHOULD

;;; FIXME: Who is responsible for the borders/scrolling on this?
;;; Scrolling is automatic.

(defclass <gtk-combo-box>
    (<gtk-tree-view-control-mixin>
     <combo-box>
     <leaf-pane>)
  ())


#||
define sealed method class-for-make-pane 
    (framem :: <gtk-frame-manager>, class == <combo-box>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<gtk-combo-box>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <gtk-frame-manager>)
				(class (eql (find-class '<combo-box>)))
				&key)
  (values (find-class '<gtk-combo-box> nil)))


#||
define sealed method make-gtk-mirror
    (gadget :: <gtk-combo-box>)
 => (mirror :: <gadget-mirror>)
  let widget = GTK-CLIST(gtk-clist-new(1));
  assert(~null-pointer?(widget), "gtk-clist-new failed");
  make(<gadget-mirror>,
       widget: widget,
       sheet:  gadget)
end method make-gtk-mirror;
||#

;; For documentation and symmetry
(defmethod %configure-mirror-columns ((gadget <gtk-combo-box>) widget)
  (declare (ignore widget))
  nil)


(defmethod make-gtk-mirror ((gadget <gtk-combo-box>))
  ;; A copy of the one for <gtk-option-box> since they "inherit" from
  ;; each other.
  (let ((widget (cffi/gtk-combo-box-entry-new)))
    (when (null widget)
      (error "gtk-combo-box-entry-new failed"))
    (%configure-mirror-columns gadget widget)
    (make-gtk-widget-mirror-instance (find-class '<gtk-gadget-mirror>)
				     :widget widget
				     :sheet  gadget)))


(defmethod install-event-handlers ((sheet <gtk-combo-box>)
				   (mirror <gtk-gadget-mirror>))
  (let ((entry (cffi/gtk-bin-get-child (CFFI/GTK-BIN (mirror-widget mirror)))))
    ;; Note: this *should* catch both changes in the combo box's drop-down
    ;; and text-entry gadgets.
    (install-named-handlers mirror #("activate") :swap entry)
    (install-named-handlers mirror #("changed")  :swap entry)))


(defmethod port-handles-repaint? ((port <gtk-port>)
				  (gadget <gtk-combo-box>))
  t)


(defmethod %gtk-fixed-height? ((gadget <gtk-combo-box>))
  t)


(defmethod update-gtk-tree-view-model ((gadget <gtk-combo-box>) widget store)
  (let ((entry (cffi/gtk-bin-get-child widget)))
    (with-disabled-event-handler (widget "changed" :swap entry)
      (cffi/gtk-combo-box-set-model widget store)
      (cffi/gtk-combo-box-entry-set-text-column widget 0))))


(defmethod update-mirror-attributes ((gadget <gtk-combo-box>)
				     (mirror <gtk-gadget-mirror>))
  (update-gtk-tree-view-control-items gadget mirror)
  (let* ((text   (gadget-text gadget))
	 (widget (mirror-widget mirror))
	 (entry  (cffi/gtk-bin-get-child widget)))
    (with-disabled-event-handler (widget "changed" :swap entry)
      (cffi/gtk-entry-set-text entry text))))


;;; This isn't used by combo boxes, even though they are a
;;; <gtk-tree-view-control-mixin> type...

#-(and)
(defmethod list-selection ((gadget <gtk-option-box>)
			   (mirror <gtk-gadget-mirror>))
  (let* ((widget (GTK-COMBO-BOX-ENTRY (mirror-widget mirror)))
	 (selection (gtk-combo-box-get-active widget)))
    (if (= -1 selection)
	#()
	(vector selection))))


(defmethod note-gadget-items-changed ((gadget <gtk-combo-box>))
  (call-next-method))


(defmethod update-mirror-selection ((gadget <gtk-combo-box>))
  (let ((widget (gadget-widget gadget)))
    (when widget
      (let ((entry (cffi/gtk-bin-get-child widget)))
	(with-disabled-event-handler (widget "changed" :swap entry)
	  (let ((text (gadget-text gadget)))
	    (cffi/gtk-entry-set-text entry text)))))))


(defmethod note-gadget-value-changed ((gadget <gtk-combo-box>))
  (call-next-method)
  (update-mirror-selection gadget))


(defmethod handle-gtk-changed-signal ((gadget <gtk-combo-box>)
				      widget
				      combo-entry)
  (declare (ignore widget))
  ;; Grab the text from the widget's child (= combo-entry) and use that to update
  ;; duim's internal state.
  ;; This is a copy of "handle-text-gadget-changing" on <gtk-text-gadget-mixin>.
  (let* ((old-text (gadget-text gadget))
	 (chars    (cffi/gtk-editable-get-chars combo-entry 0 -1))
	 (new-text (unless (equal? old-text chars)
		     (setf (gadget-text gadget) chars))))
    (when new-text
      (distribute-text-changing-callback gadget new-text))))


(defmethod handle-gtk-activate-signal ((gadget <gtk-combo-box>)
				       widget
				       combo-entry)
  (declare (ignore widget combo-entry))
  ;; This is a copy of "handle-text-gadget-changed" on <gtk-text-gadget-mixin>
  (let ((text (gadget-text gadget)))
    (distribute-text-changed-callback gadget text)))


#||
/*---*** No combo boxes for now...
define sealed class <gtk-combo-box> 
    (<gtk-gadget-mixin>,
     <combo-box>,
     <leaf-pane>,
     <sealed-constructor-mixin>)
  sealed slot %changed? :: <boolean> = #f;
end class <gtk-combo-box>;

//--- If <gtk-combo-box> was a <text-field>, we would not need this
define sealed method activate-gtk-gadget
    (gadget :: <combo-box>) => (activated? :: <boolean>)
  handle-text-gadget-changed(gadget);
  next-method()
end method activate-gtk-gadget;

define sealed class <gtk-combo-box-text-field>
    (<gtk-subgadget-mixin>,
     <gtk-text-field>)
end class <gtk-combo-box-text-field>;

define sealed method class-for-make-pane 
    (framem :: <gtk-frame-manager>, class == <combo-box>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<gtk-combo-box>, #f)
end method class-for-make-pane;

define sealed method make-gadget-control
    (gadget :: <gtk-combo-box>, parent :: <HWND>, options :: <options-type>,
     #key x, y, width, height)
 => (handle :: <HWND>)
  let ext-style = if (border-type(gadget) == #"none") 0 else $WS-EX-CLIENTEDGE end;
  let handle :: <HWND>
    = CreateWindowEx(ext-style,
		     "COMBOBOX",
		     "",
		     %logior(options, 
			     $WS-GROUP, $WS-TABSTOP,
			     $CBS-AUTOHSCROLL, $CBS-HASSTRINGS,
			     $CBS-DROPDOWN),
		     x, y, width, height,
		     parent,
		     $null-hMenu,
		     application-instance-handle(),
		     $NULL-VOID);
  check-result("CreateWindowEx (COMBOBOX)", handle);
  subclass-combo-box-text-field(gadget, handle);
  handle
end method make-gadget-control;

define sealed method update-mirror-attributes
    (gadget :: <gtk-combo-box>, mirror :: <gadget-mirror>) => ()
  next-method();
  note-gadget-items-changed(gadget)
end method update-mirror-attributes;

// This is a bizarre hack to subclass the text field which is
// a child of the combo box.
define function subclass-combo-box-text-field
    (gadget :: <gtk-combo-box>, handle :: <HWND>) => ()
  let edit-control = GetWindow(handle, $GW-CHILD);
  check-result("Finding the combo box's edit control", edit-control);
  // This is odd, but making this gadget actually does all the work
  // to mirror and attach everything correctly.
  make(<gtk-combo-box-text-field>,
       owner: gadget, handle: edit-control);
end function subclass-combo-box-text-field;

define sealed method do-compose-space 
    (gadget :: <gtk-combo-box>, #key width, height)
 => (space-req :: <space-requirement>)
  ignore(height);
  let _port = port(gadget);
  let text-style = get-default-text-style(_port, gadget);
  let min-width = $minimum-visible-characters * font-width(text-style, _port);
  let width = constrain-size(width | min-width, min-width, $fill);
  //---*** How should we really calculate the constant below?
  let height = font-height(text-style, _port) + $option-box-extra-height;
  make(<space-requirement>,
       width:  max(width, min-width), min-width: min-width, max-width: $fill,
       height: height)
end method do-compose-space;

define sealed method gtk-combo-box-height
    (gadget :: <gtk-combo-box>) => (height :: <integer>)
  let _port = port(gadget);
  let text-style = get-default-text-style(_port, gadget);
  let n-items :: <integer> = size(gadget-items(gadget));
  let line-height = font-height(text-style, _port);
  let vsp         = $default-vertical-spacing;
  let nlines      = max(n-items, 1);
  //---*** How can we compute this for real?
  line-height + $option-box-extra-height + 4
    + min($option-box-maximum-popup-height,
	  nlines * line-height + (nlines - 1) * vsp)
end method gtk-combo-box-height;

define sealed method note-gadget-items-changed
    (gadget :: <gtk-combo-box>) => ()
  next-method();
  let mirror = sheet-direct-mirror(gadget);
  when (mirror)
    update-gadget-items(gadget, $CB-RESETCONTENT, $CB-ADDSTRING);
    update-gadget-text(gadget, mirror);
    // Call 'set-mirror-edges' to make sure that the drop-down menu
    // is the correct size.
    let _port = port(gadget);
    let (left, top, right, bottom) = mirror-edges(_port, gadget, mirror);
    set-mirror-edges(_port, gadget, mirror, left, top, right, bottom)
  end
end method note-gadget-items-changed;

define sealed method note-gadget-text-changed 
    (gadget :: <gtk-combo-box>) => ()
  next-method();
  let mirror = sheet-direct-mirror(gadget);
  mirror & update-gadget-text(gadget, mirror)
end method note-gadget-text-changed;

define sealed method note-gadget-value-changed
    (gadget :: <gtk-combo-box>) => ()
  next-method();
  let mirror = sheet-direct-mirror(gadget);
  mirror & update-gadget-text(gadget, mirror)
end method note-gadget-value-changed;

define sealed method handle-selection-changed
    (gadget :: <gtk-combo-box>) => (handled? :: <boolean>)
  let handle = window-handle(gadget);
  let selection = SendMessage(handle, $CB-GETCURSEL, 0, 0);
  unless (selection = $CB-ERR)
    let item = gadget-items(gadget)[selection];
    let text = collection-gadget-item-label(gadget, item);
    distribute-text-changed-callback(gadget, text);
    #t
  end
end method handle-selection-changed;

define sealed method handle-command
    (gadget :: <gtk-combo-box>, mirror :: <gadget-mirror>,
     id :: <integer>, event :: <integer>)
 => (handled? :: <boolean>)
  ignore(mirror, id);
  select (event)
    $CBN-EDITCHANGE => handle-text-gadget-changing(gadget);
    $CBN-SELENDOK   => handle-selection-changed(gadget);
//---*** This doesn't seem to work, and also messes up
//---*** the SELENDOK so I've taken it out for now.
//  $EN-KILLFOCUS   => handle-text-gadget-changed(gadget);
    otherwise       => next-method();
  end
end method handle-command;

//--- This is a hack to wrestle the magic keys from the combo-box so
//--- that we can correctly handle hitting return, escape or tab.
define sealed method handle-control-message
    (text-field :: <gtk-combo-box-text-field>, message :: <message-type>,
     wParam :: <wparam-type>, lParam :: <lparam-type>)
 => (handled? :: <boolean>)
  let gadget = subgadget-owner(text-field);
  duim-debug-message("Handling message #x%x for subclassed %=",
		     message, gadget);
  when (message = $WM-KEYUP | message = $WM-CHAR | message = $WM-KEYDOWN)
    let key-name = virtual-key->keysym(wParam);
    duim-debug-message("Handling key-name %= for subclassed %=",
		       key-name, gadget);
    select (key-name)
      #"return", #"escape" =>
	message = $WM-KEYDOWN & handle-command-for-id(gadget, $IDOK);
	#t;
      #"tab" =>
	//---*** We need to handle Tab and shift-Tab somehow, since
	//---*** combo boxes won't do it for us.
	duim-debug-message("Dropping Tab on the floor for %=!", gadget);
	#t;
      otherwise =>
	#f;
    end
  end
end method handle-control-message;

define sealed method cancel-gadget 
    (gadget :: <gtk-combo-box>) => (handled? :: <boolean>)
  let handle = window-handle(gadget);
  when (handle & (SendMessage(handle, $CB-GETDROPPEDSTATE, 0, 0) = $true))
    SendMessage(handle, $CB-SHOWDROPDOWN, $false, 0);
    #t
  end
end method cancel-gadget;
*/



/// Viewports

define sealed class <gtk-viewport>
    (<viewport>,
     <gtk-pane-mixin>,
     <permanent-medium-mixin>,
     <single-child-composite-pane>,
     <sealed-constructor-mixin>)
end class <gtk-viewport>;
||#

#||
  CPL for <GTK-VIEWPORT>

  (#<STANDARD-CLASS GTK-DUIM::<GTK-VIEWPORT>>
   #<STANDARD-CLASS DUIM-GADGETS:<VIEWPORT>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<BASIC-GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<GADGET>>
   #<STANDARD-CLASS DUIM-GADGETS:<ABSTRACT-GADGET>>
   #<STANDARD-CLASS GTK-DUIM:<GTK-PANE-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<STANDARD-INPUT-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<SHEET-WITH-EVENT-QUEUE-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<MIRRORED-SHEET-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<SHEET-WITH-RESOURCE-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<CLIENT-OVERRIDABILITY-MIXIN>>
   #<STANDARD-CLASS DUIM-GADGETS-INTERNALS:<SCROLLING-SHEET-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<PERMANENT-MEDIUM-MIXIN>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<SHEET-WITH-MEDIUM-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS:<SINGLE-CHILD-COMPOSITE-PANE>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<SINGLE-CHILD-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS:<BASIC-COMPOSITE-PANE>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<CACHED-SPACE-REQUIREMENT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<COMPOSITE-LAYOUT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS-INTERNALS:<LAYOUT-MIXIN>>
   #<STANDARD-CLASS DUIM-LAYOUTS:<LAYOUT>>
   #<STANDARD-CLASS DUIM-SHEETS-INTERNALS:<BASIC-SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<ABSTRACT-SHEET>>
   #<STANDARD-CLASS DUIM-SHEETS:<EVENT-HANDLER>>
   #<STANDARD-CLASS STANDARD-OBJECT>
   #<BUILT-IN-CLASS T>)

||#

;;; <gtk-viewport> provides:
;;;
;;; nothing

;; XXX: These are mirrored in the win32 b/end, don't think they need
;; to be for GTK though.
(defclass <gtk-viewport>
    (<viewport>
     <gtk-pane-mixin>
     <permanent-medium-mixin>
     <single-child-composite-pane>)
  ())

#||
define sealed method class-for-make-pane 
    (framem :: <gtk-frame-manager>, class == <viewport>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<gtk-viewport>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <gtk-frame-manager>)
				(class (eql (find-class '<viewport>)))
				&key)
  (values (find-class '<gtk-viewport>) nil))


#||
// ---*** make viewports drawing areas for now so that we can see some content
define method make-gtk-mirror
    (sheet :: <gtk-viewport>)
 => (mirror :: <widget-mirror>)
 let widget = GTK-DRAWING-AREA(gtk-drawing-area-new());
 gtk-drawing-area-size(widget, 200, 200);
 make(<drawing-area-mirror>,
      widget: widget,
      sheet:  sheet);
end method;
||#

;;; FIXME: Maybe this should all be on DO-make-gtk-mirror in mirrors.lisp?

(defmethod make-gtk-mirror ((sheet <gtk-viewport>))
  ;; GtkLayout is like a drawing pane, but you can add widgets to it as
  ;; well as draw.
  (let ((widget (cffi/gtk-layout-new nil nil)))
    ;; FIXME: THIS SETS THE SIZE OF THE *SCROLLABLE AREA*. IS THAT IS WANTED?
    ;; Does DUIM have a way of saying "size the scrollable area to XXX"?
    (cffi/gtk-layout-set-size widget 200 200)
    (let ((mirror (make-gtk-widget-mirror-instance (find-class '<gtk-viewport-mirror>)
						   :widget widget
						   :sheet  sheet)))
      mirror)))


(defmethod port-handles-repaint? ((port <gtk-port>)
				  (sheet <gtk-viewport>))
  nil)


#||

/// Borders

||#

