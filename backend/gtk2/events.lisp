;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GTK2-DUIM -*-
(in-package #:gtk2-duim)

#||
Module:       gtk-duim
Synopsis:     GTK event processing implementation
Author:       Andy Armstrong, Scott McKay
Copyright:    Original Code is Copyright (c) 1995-2004 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND

/// GTK signals

define constant $signal-handlers = make(<object-table>);
||#

(defparameter *signal-handlers* (make-hash-table))

#||
define class <signal-handler> (<sealed-constructor-mixin>)
  constant sealed slot handler-name :: <C-string>,
    required-init-keyword: name:;
  constant sealed slot handler-function :: <GtkSignalFunc>,
    required-init-keyword: function:;
end class <signal-handler>;
||#

;;; TODO: also capture the gulong handler-id so can block/unblock
;;; more easily

(defclass <signal-handler>
    ()
  ;; HANDLER-NAME is a CL string
  ((handler-name :initarg :name
		 :initform (required-slot ":name" "<signal-handler>")
		 :reader handler-name)
   ;; HANDLER-FUNCTION is a foreign pointer to a function
   (handler-function :initarg :function
		     :initform (required-slot ":function" "<signal-handler>")
		     :reader handler-function)))

#||
define function register-signal-handler
    (name :: <string>, function :: <GtkSignalFunc>,
     #key key = as(<symbol>, name)) => ()
  $signal-handlers[key]
    := make(<signal-handler>,
	    name:     as(<C-string>, name),
	    function: function)
end function register-signal-handler;
||#

(defun register-signal-handler (name function) ;;<GtkSignalFunc>)
  (check-type name string)
  (let* ((name (string-downcase name))
	 (key (intern name (find-package "KEYWORD"))))
    (setf (gethash key *signal-handlers*)
	  (make-instance '<signal-handler>
			 ;; GTK is case sensitive!
			 :name     name ;(cffi/string->foreign-string name)
			 :function function))))

#||
define function fetch-signal-handler
    (name :: <symbol>) => (_handler :: <signal-handler>)
  element($signal-handlers, name, default: #f)
    | error("No GTK handler registered for '%s'", name)
end function fetch-signal-handler;
||#

(defun fetch-signal-handler (name)
  (check-type name string)
  (let ((key (intern (string-downcase name) (find-package "KEYWORD"))))
    (or (gethash key *signal-handlers*)
	(error "No GTK handler registered for '~s'" name))))

#||
define function do-with-disabled-event-handler
    (function :: <function>, widget :: <GtkWidget*>, name :: <symbol>)
 => (#rest values)
  let _handler = handler-function(fetch-signal-handler(name));
  let object = GTK-OBJECT(widget);
  block ()
    gtk-signal-handler-block-by-func(object, _handler, $null-gpointer);
    function()
  cleanup
    gtk-signal-handler-unblock-by-func(object, _handler, $null-gpointer);
  end
end function do-with-disabled-event-handler;
||#

(defun do-with-disabled-event-handler (function widget name &key swap)
  (check-type function function)
  (check-type name string)
  (let* ((_handler (handler-function (fetch-signal-handler name))))
    ;; This needs to deal with signals that have been connected via
    ;; g-signal-connect-swap.  If the target object AND the data
    ;; passed at connect time don't match the values passed here, the
    ;; signal isn't blocked.
    (unwind-protect
	 (let ((num-blocked (cffi/g-signal-handlers-block-by-func
			     (if swap swap widget)
			     _handler
			     (if swap widget nil))))
	   (declare (ignorable num-blocked))
	   (funcall function))
      (let ((num-unblocked (cffi/g-signal-handlers-unblock-by-func
			    (if swap swap widget)
			    _handler
			    (if swap widget nil))))
	(declare (ignorable num-unblocked))
	#-(and)
	(gtk-debug "WITH-DISABLED-EVENT-HANDLER: ~d SIGNALS UNBLOCKED FOR ~a" num-unblocked name)))))

#||
define macro with-disabled-event-handler
  { with-disabled-event-handler (?widget:expression, ?name:expression)
      ?body:body
    end }
 => { do-with-disabled-event-handler(method () ?body end, GTK-WIDGET(?widget), ?name) }
end macro with-disabled-event-handler;
||#

(defmacro with-disabled-event-handler ((widget name &key swap) &body body)
  `(do-with-disabled-event-handler
       #'(lambda () ,@body)
     (CFFI/GTK-WIDGET ,widget)
     ,name
     :swap ,swap))


#||
define macro event-handler-definer
  { define event-handler (?name:name, ?event-type:name)
      ?eq:token ?handler:name }
 => { define gtk-callback ("_gtk-" ## ?name ## "-callback", ?event-type)
        ?eq "handle_" ## ?name;
      register-signal-handler(as-lowercase(?"name"), "_gtk-" ## ?name ## "-callback");
      define open generic ?handler
	  (sheet :: <abstract-sheet>, widget :: <GtkWidget*>, 
	   event :: ?event-type)
       => (handled? :: <boolean>);
      define function "handle_" ## ?name
	  (widget :: <GtkWidget*>, event :: ?event-type)
       => (code :: <integer>)
	do-handle-gtk-signal
	  (?handler, widget, ?"name", widget, event)
      end }
end macro event-handler-definer;


define event-handler (destroy,              <GdkEventAny*>)       = handle-gtk-destroy-event;
define event-handler (delete_event,         <GdkEventAny*>)       = handle-gtk-delete-event;
define event-handler (motion_event,         <GdkEventMotion*>)    = handle-gtk-motion-event;
define event-handler (button_press_event,   <GdkEventButton*>)    = handle-gtk-button-press-event;
define event-handler (button_release_event, <GdkEventButton*>)    = handle-gtk-button-release-event;
define event-handler (key_press_event,      <GdkEventKey*>)       = handle-gtk-key-press-event;
define event-handler (key_release_event,    <GdkEventKey*>)       = handle-gtk-key-release-event;
define event-handler (configure_event,      <GdkEventConfigure*>) = handle-gtk-configure-event;
define event-handler (expose_event,         <GdkEventExpose*>)    = handle-gtk-expose-event;
define event-handler (enter_event,          <GdkEventCrossing*>)  = handle-gtk-enter-event;
define event-handler (leave_event,          <GdkEventCrossing*>)  = handle-gtk-leave-event;

define event-handler (clicked,              <GdkEventAny*>)       = handle-gtk-clicked-event;
define event-handler (select_row,           <GdkEventAny*>)       = handle-gtk-select-row-event;
define event-handler (click_column,         <GdkEventAny*>)       = handle-gtk-click-column-event;
define event-handler (resize_column,        <GdkEventAny*>)       = handle-gtk-resize-column-event;

define inline function do-handle-gtk-signal
    (handler_ :: <function>, widget :: <GtkWidget*>, name :: <string>,
     #rest args)
 => (code :: <integer>)
  let mirror = widget-mirror(widget);
  debug-assert(mirror, "Unknown widget");
  let gadget = mirror-sheet(mirror);
  debug-message("Handling %s for %=", name, gadget);
  let value = apply(handler_, gadget, args);
  debug-message("  handled?: %=", value);
  if (instance?(value, <integer>))
    value
  elseif (value)
    $true
  else
    $false
  end
end function do-handle-gtk-signal;
||#

(defun do-handle-gtk-signal (handler_ widget name &rest args)
  (check-type handler_ function)
  (check-type name string)
  ;; Find the DUIM gadget that's supposed to be handling the signal,
  ;; and invoke the event handler on it.
  ;; Note: whilst not all widgets have an 'x-window mirror', they *do*
  ;; all have a duim back-end mirror.
  (let ((mirror (widget-mirror widget)))
    (unless mirror
      (error "Unknown widget ~a" mirror))
    (let ((gadget (mirror-sheet mirror)))
      ;; The handler should be called with the same args as the original
      ;; GTK callback, except that we prepend the corresponding DUIM
      ;; gadget.
      (if (apply handler_ gadget args)
	  +true+
	  +false+))))

#||
/// Non-event signals

define macro signal-handler-definer
  { define signal-handler ?:name (?args:*) ?eq:token ?handler:name }
    => { signal-handler-aux ?"name",
         ?handler (?args),
	"%gtk-" ## ?name ## "-signal-handler" (?args),
	"_gtk-" ## ?name ## "-signal-handler" (?args)
       end }
end macro;
||#

(defmacro define-signal-handler (name return-type (&rest args) = handler)
  (declare (ignore =))
"
Generates all the atomic parts necessary to create a GTK signal handler.
NAME is a symbol denoting the signal (for example, BUTTON-PRESSED-EVENT,
CLICKED), ARGS are the arguments the signal handler accepts with the
initial GtkObject* omitted (this parameter will be added by this macro).
HANDLER is the name of the Lisp procedure ends up being invoked.

Example:-

    (DEFINE-SIGNAL-HANDLER \"click-column\" :void
        (column :gint user-data :gpointer)
      = handle-gtk-click-column-signal)

The above form defines a c-callable procedure named
``_GTK-CLICK-COLUMN-SIGNAL-HANDLER'' which takes three parameters
consisting of the GtkObject* on which the signal was emitted, an
integer column id and a pointer to some user-data (provided to GTK
when the signal handler is bound to the signal with g_signal_connect).
Note that the callbacks for different signals can take different
parameters, the parameters to the callback must be enumerated in the
defining form. These parameter definitions use CFFI syntax.

It also defines a lisp method named ``%GTK-CLICK-COLUMN-SIGNAL-HANDLER''
which invokes DO-HANDLE-GTK-SIGNAL passing it the widget, name of the
signal, and any arguments supplied from the c-callable procedure when
it is invoked by the c-callable procedure.

DO-HANDLE-GTK-SIGNAL in turn resolves the GTK widget on which the callback
was originally invoked into a DUIM <GADGET> type before applying HANDLER
to the gadget, signal name and callback arguments.
"
  (let ((%handler-name (intern (concatenate 'string
					    "%GTK-"
					    (string-upcase name)
					    "-SIGNAL-HANDLER")))
	(_handler-name (intern (concatenate 'string
					    "_GTK-"
					    (string-upcase name)
					    "-SIGNAL-HANDLER"))))
    `(signal-handler-aux ,(string-upcase name)
			 ;; This one is the (Lisp) handler we want to
			 ;; ultimately have invoked.
			 (,handler ,@args)
			 ;; %GTK-...-HANDLER defines a Lisp method that
			 ;; invokes the handler provided via DO-HANDLE-
			 ;; GTK-SIGNAL.
			 (,%handler-name ,@args)
			 ;; _GTK-...-HANDLER defines a native method that
			 ;; gets registered as the signal handler in GTK.
			 (,_handler-name ,return-type ,@args))))

#||
define macro signal-handler-aux
  { signal-handler-aux ?signal:expression,
      ?handler:name (?args),
      ?%handler:name (?params:*),
      ?_handler:name (?c-params)
    end }
    => { define function ?%handler (widget :: <GtkWidget*>, ?params)
	   do-handle-gtk-signal(?handler, widget, ?signal, ?args)
	 end;
         define C-callable-wrapper ?_handler of ?%handler
            parameter widget :: <GtkWidget*>;
           ?c-params
         end;
         register-signal-handler(?signal, ?_handler)
       }
c-params:
    { } => { }
    { ?:variable, ... } => { parameter ?variable; ... }
args:
    { } => { }
    { ?arg:name :: ?:expression, ... } => { ?arg, ... }
end macro;
||#

;;; This *probably* needs to be a bit cleverer... what if there's already a signal
;;; handler? At the very least we should be defining fns rather than methods. FIXME

(defmacro signal-handler-aux (signal-expr
			      (handler  &rest args)
			      (%handler &rest params)
			      (_handler return-type &rest c-params))
  `(progn
     (defun ,%handler (widget ,@(loop for p in params
				          collect (first p)))
       (do-handle-gtk-signal #',handler
		             widget
			     ,signal-expr
			     ;; Remove type 'specifiers' from 'args'.
			     widget
			     ,@(loop for arg in args
				     collect (first arg))))
     (cffi:defcallback ,_handler
	          ,return-type
	          ((widget gpointer) ,@params)        ; parameters
	          (,%handler widget ,@(loop for arg in c-params
				            collect (first arg))))
     (register-signal-handler ,signal-expr (cffi:callback ,_handler))))



;;; GENERIC SIGNALS WE WANT TO HANDLE (ALL WIDGETS CAN DEAL WITH THESE):

;;; TODO: document event handling strategy very carefully; expain what each
;;; event is and its intention; also document how the events are handled,
;;; in particular some are passed on to duim and then left, expecting duim
;;; to handle them - and some are not!

;;; FRAME SHUTDOWN REQUEST -- is "destroy-event" needed also? Depends if
;;; DUIM synthesises its own, or waits for the back-end to generate one.

#| GtkWidget::button-press-event ---------------------------------------

The ::button-press-event signal will be emitted when a
button (typically from a mouse) is pressed.

To receive this signal, the GdkWindow associated to the widget needs
to enable the GDK_BUTTON_PRESS_MASK mask.

This signal will be sent to the grab widget if there is one.
|#
(define-signal-handler "button-press-event" gboolean ((event (:pointer (:struct GdkEventButton))) (user-data gpointer))
		       = handle-gtk-button-press-event-signal)

#| GtkWidget::button-release-event -------------------------------------

The ::button-release-event signal will be emitted when a
button (typically from a mouse) is released.

To receive this signal, the GdkWindow associated to the widget needs
to enable the GDK_BUTTON_RELEASE_MASK mask.

This signal will be sent to the grab widget if there is one.
|#
(define-signal-handler "button-release-event" gboolean ((event (:pointer (:struct GdkEventButton))) (user-data gpointer))
		       = handle-gtk-button-release-event-signal)

#| GtkWidget::configure-event ------------------------------------------

The ::configure-event signal will be emitted when the size, position
or stacking of the widget's window has changed.

To receive this signal, the GdkWindow associated to the widget needs
to enable the GDK_STRUCTURE_MASK mask. GDK will enable this mask
automatically for all new windows.
|#
(define-signal-handler "configure-event" gboolean ((event (:pointer (:struct GdkEventConfigure))) (user-data gpointer))
		       = handle-gtk-configure-event-signal)  ; X window changed size

#| GtkWidget::delete-event ---------------------------------------------

The ::delete-event signal is emitted if a user requests that a
toplevel window is closed. The default handler for this signal
destroys the window. Connecting gtk_widget_hide_on_delete() to this
signal will cause the window to be hidden instead, so that it can
later be shown again without reconstructing it.
|#
(define-signal-handler "delete-event" gboolean ((event :pointer) (user-data gpointer))
		       = handle-gtk-delete-event-signal)

#| GtkWidget::enter-notify-event ---------------------------------------

The ::enter-notify-event will be emitted when the pointer enters the
widget's window.

To receive this signal, the GdkWindow associated to the widget needs
to enable the GDK_ENTER_NOTIFY_MASK mask.

This signal will be sent to the grab widget if there is one.
|#
(define-signal-handler "enter-notify-event" gboolean ((event (:pointer (:struct GdkEventCrossing))) (user-data gpointer))
		       = handle-gtk-enter-notify-event-signal)

#| GtkWidget::expose-event ---------------------------------------------

Deprecated in gtk3; use damage-event instead there I think
|#
(define-signal-handler "expose-event" gboolean ((event (:pointer (:struct GdkEventExpose))) (user-data gpointer))
		       = handle-gtk-expose-event-signal)     ; X window exposed

#| GtkWidget::key-press-event ------------------------------------------

The ::key-press-event signal is emitted when a key is pressed. The
signal emission will reoccur at the key-repeat rate when the key is
kept pressed.

To receive this signal, the GdkWindow associated to the widget needs
to enable the GDK_KEY_PRESS_MASK mask.

This signal will be sent to the grab widget if there is one.
|#
(define-signal-handler "key-press-event" gboolean ((event (:pointer (:struct GdkEventKey))) (user-data gpointer))
		       = handle-gtk-key-press-event-signal)

#| GtkWidget::key-release-event ----------------------------------------

The ::key-release-event signal is emitted when a key is released.

To receive this signal, the GdkWindow associated to the widget needs
to enable the GDK_KEY_RELEASE_MASK mask.

This signal will be sent to the grab widget if there is one.
|#
(define-signal-handler "key-release-event" gboolean ((event (:pointer (:struct GdkEventKey))) (user-data gpointer))
 		       = handle-gtk-key-release-event-signal)

#| GtkWidget::leave-notify-event ---------------------------------------

The ::leave-notify-event will be emitted when the pointer leaves the
widget 's window.

To receive this signal, the GdkWindow associated to the widget needs
to enable the GDK_LEAVE_NOTIFY_MASK mask.

This signal will be sent to the grab widget if there is one.
|#
(define-signal-handler "leave-notify-event" gboolean ((event (:pointer (:struct GdkEventCrossing))) (user-data gpointer))
		       = handle-gtk-leave-notify-event-signal)

;;; The handlers for the next two are implemented in GTK-KEYBOARD.LISP

#| GtkWidget::motion-notify-event --------------------------------------

The ::motion-notify-event signal is emitted when the pointer moves
over the widget's GdkWindow.

To receive this signal, the GdkWindow associated to the widget needs
to enable the GDK_POINTER_MOTION_MASK mask.

This signal will be sent to the grab widget if there is one.
|#
(define-signal-handler "motion-notify-event" gboolean ((event (:pointer (:struct GdkEventMotion))) (user-data gpointer))
		       = handle-gtk-motion-notify-event-signal)

#-(and)
(define-signal-handler "drag-motion"  gboolean ((drag-context GdkDragContext) (x gint) (y gint) (time guint) (user-data gpointer))
		       = handle-gtk-drag-motion-signal)

#| GtkWidget - others, maybe? ------------------------------------------

damage-event
drag-*
focus
focus-in-event
focus-out-event
grab-notify
map
map-event
mnemonic-activate
popup-menu - NOT 3 ARG - might be handled by DUIM maybe?
query-tooltip - NOT 3 ARG - might be handled by DUIM maybe?
realize - NOT 3 ARG
screen-changed
scroll-event    <-- deals with scroll wheels
selection-*
show - NOT 3 ARG
show-help
size-allocate
state-flags-changed
style-updated
unmap
unmap-event
unrealize
window-state-event
|#

;;; WIDGET-SPECIFIC SIGNALS:

#| GtkButton::clicked --------------------------------------------------

Emitted when the button has been activated (pressed and released).

;;; SIGNAL:     "clicked"
;;; EMITTED BY: GtkButton, GtkToolButton, GtkTreeViewColumn
;;; ARGUMENTS:  GtkButton* widget,
;;;             gpointer data
;;; RUN ORDER:  run first

;; * no handler
|#
(define-signal-handler "clicked" :void ((user-data gpointer))
		       = handle-gtk-clicked-signal)

#| GtkMenuShell::deactivate --------------------------------------------

void
user_function (GtkMenuShell *menushell,
               gpointer      user_data)
Flags: Run First
|#
(define-signal-handler "deactivate" :void ((user-data gpointer))
		       = handle-gtk-deactivate-signal)


#| GtkTreeView::row-activated ------------------------------------------

XXX: Use GtkTreeView for lists and for trees

The "row-activated" signal is emitted when the method
gtk_tree_view_row_activated() is called, when the user double clicks a
treeview row with the "activate-on-single-click" property set to
FALSE, or when the user single clicks a row when
the "activate-on-single-click" property set to TRUE. It is also
emitted when a non-editable row is selected and one of the keys:
Space, Shift+Space, Return or Enter is pressed.

For selection handling refer to the tree widget conceptual overview as
well as GtkTreeSelection.

;;; SIGNAL:     "row-activated"
;;; EMITTED BY: GtkTreeView
;;; ARGUMENTS:  GtkTreeView* tree-view
;;;             GtkTreePath* path
;;;             GtkTreeViewColumn* column
;;;             gpointer user-data
;;; RUN ORDER:  run last / action

;; * no handler
|#
(define-signal-handler "row-activated" :void ((path :pointer) (column :pointer) (user-data gpointer))
		       = handle-gtk-row-activated-signal)

;;; also consider:
;;;
;;; expand-collapse-cursor-row
;;; row-collapsed
;;; row-expanded
;;; select-all
;;; select-cursor-parent
;;; select-cursor-row
;;; start-interactive-search
;;; test-collapse-row
;;; test-expand-row
;;; toggle-cursor-row
;;; unselect-all

#||
define signal-handler changed (user-data :: <gpointer>)
  = gtk-changed-signal-handler;
define signal-handler activate (user-data :: <gpointer>)
  = gtk-activate-signal-handler;
||#

;;; SIGNAL:     "changed"
;;; EMITTED BY: GtkAccelMap, GtkTreeSelection, GtkTextBuffer,
;;;             GtkRadioAction (group), GtkOptionMenu, GtkEditable,
;;;             GtkComboBox, GtkAdjustment
;;; ARGUMENTS:  GtkObject* widget,
;;;             [GtkRadioAction* action]
;;;             gpointer user-data
;;; RUN ORDER:  run first / run last (depending on emitter)
;;;
;;; Note: args are (GtkObject* widget, gpointer user_data) on all of these
;;; EXCEPT radio actions which take an additional GtkRadioAction* as a
;;; second parameter. We don't use GtkRadioAction in duim atm.

(define-signal-handler "changed" :void ((user-data gpointer))
		       = handle-gtk-changed-signal)

;;; SIGNAL:     "activate"
;;; EMITTED BY: GtkAction, GtkButton, GtkEntry, GtkMenuItem
;;; ARGUMENTS:  GtkObject* widget
;;;             gpointer user-data
;;; RUN ORDER:  FIXME

(define-signal-handler "activate" :void ((user-data gpointer))
		       = handle-gtk-activate-signal)

;;; SIGNAL:     "select"
;;; EMITTED BY: GtkItem
;;; ARGUMENTS:  GtkItem* item
;;;             gpointer user-data
;;; RUN ORDER:  Run first

(defgeneric handle-gtk-select-signal (gadget widget user-data))

(define-signal-handler "select" :void ((user-data gpointer))
		       = handle-gtk-select-signal)

#||
/// Adjustments
define function %gtk-adjustment-value-changed-signal-handler
    (adj :: <GtkAdjustment*>, widget :: <GtkWidget*>)
  do-handle-gtk-signal(gtk-adjustment-value-changed-signal-handler,
		       widget, "adjustment/value_changed", adj)
end;

define C-callable-wrapper _gtk-adjustment-value-changed-signal-handler
  of %gtk-adjustment-value-changed-signal-handler
  parameter adj :: <GtkAdjustment*>;
  parameter user-data :: <GtkWidget*>;
end;
register-signal-handler("value_changed",
			_gtk-adjustment-value-changed-signal-handler,
			key: #"adjustment/value_changed");
||#

;;; SIGNAL:     "value-changed"
;;; EMITTED BY: GtkAdjustment, GtkRange, GtkScaleButton, GtkSpinButton
;;; ARGUMENTS:  GtkAdjustment* widget
;;;             gpointer user-data
;;; RUN ORDER:  
;;;
;;; Note: the args on GtkScaleButton are completely different, but we
;;; don't use those (yet).

(define-signal-handler "value-changed" :void ((user-data gpointer))
		       = handle-gtk-value-changed-signal)


;;; SIGNAL:     "set-focus"
;;; EMITTED BY: GtkWindow
;;; ARGUMENTS:  GtkWindow*, GtkWidget*, gpointer user-data
;;; RUN ORDER:  Run Last
;;;
;;; void user_function (GtkWindow *window,
;;;                     GtkWidget *widget,
;;;                     gpointer   user_data)      : Run Last
;;;
;;; This signal is emitted whenever the currently focused widget in this window changes.
;;;
;;; Parameters
;;;   - window :: the window which received the signal
;;;   - widget :: the newly focused widget (or NULL for no focus).
;;;   - user_data :: user data set when the signal handler was connected.

(define-signal-handler "set-focus" :void ((widget-arg :pointer) (user-data gpointer))
		       = handle-gtk-set-focus-signal)


(define-signal-handler "focus-in-event" gboolean ((event :pointer) (user-data gpointer))
		       = handle-gtk-focus-in-signal)

(defgeneric handle-gtk-focus-in-signal (gadget widget event user-data))


;;; WIDGET-SPECIFIC SIGNALS:

;;; SIGNAL:     "activate-item"
;;; EMITTED BY: GtkMenuItem
;;; ARGUMENTS:  GtkMenuItem* widget,
;;;             gpointer data
;;; RUN ORDER:  run first

(defgeneric handle-gtk-activate-item-signal (gadget widget user-data))

;; * no handler
(define-signal-handler "activate-item" :void ((user-data gpointer))
		       = handle-gtk-activate-item-signal)


;;; SIGNAL:     "activate-current"
;;; EMITTED BY: GtkMenuShell
;;; ARGUMENTS:  GtkMenuShell* menushell
;;;             gboolean force_hide
;;;             gpointer user_data
;;; RUN ORDER:  run last

(defgeneric handle-gtk-activate-current-signal (gadget widget force-hide user-data))

(define-signal-handler "activate-current" :void ((force-hide gboolean)
						 (user-data gpointer))
		       = handle-gtk-activate-current-signal)

#||
define function install-named-handlers
    (mirror :: <gtk-mirror>, handlers :: <sequence>, #key adjustment) => ()
  let widget = mirror-widget(mirror);
  let object = GTK-OBJECT(widget);
  debug-message("Installing handlers for %=: %=",
		mirror-sheet(mirror), handlers);
  for (key :: <symbol> in handlers)
    let handler_ = fetch-signal-handler(key);
    if (handler_)
      let name     = handler_.handler-name;
      let function = handler_.handler-function;
      let value = if (adjustment)
		    gtk-signal-connect(adjustment, name, function, widget)
		  else
		   //--- Should we pass an object to help map back to a mirror?
		    gtk-signal-connect(object, name, function, $null-gpointer)
		  end;
      if (zero?(value))
	debug-message("Unable to connect signal '%s'", name)
      end
    end
  end;
  gtk-widget-add-events(widget,
			logior($GDK-EXPOSURE-MASK,
			       $GDK-LEAVE-NOTIFY-MASK,
			       if (member?(#"motion", handlers))
				 logior($GDK-POINTER-MOTION-MASK,
					$GDK-POINTER-MOTION-HINT-MASK)
			       else
				 0
			       end))
end function install-named-handlers;
||#

(defun install-named-handlers (mirror handlers &key (swap nil))
  (check-type mirror <gtk-mirror>)
  (check-type handlers sequence)
  (let* ((widget (mirror-widget mirror)))
    (map nil #'(lambda (key)
		 (gtk-debug "ADDING HANDLER FOR SIGNAL ~A TO MIRROR ~A" KEY MIRROR)
		 (let ((handler_ (fetch-signal-handler key)))
		   (if handler_
		       (let* ((name (handler-name handler_))
			      (function (handler-function handler_))
			      (value (if swap
					 (cffi/g-signal-connect-swapped swap
									name
									function
									widget)
					 ;;--- Should we pass an object to help map back to a mirror?
					 ;; FIXME DR 2020-04-25: should this a neater way...
					 #-(and)
					 (cffi/g-signal-connect widget
								name
								function
								nil)
					 (if (string= key "expose-event")
					     (cffi/g-signal-connect-after widget
									  name
									  function
									  nil)
					     (cffi/g-signal-connect widget
								    name
								    function
								    nil)))))
			 (when (zerop value)
			   (gtk-debug "Unable to connect signal '~s'" name))))))
	 handlers)
    ;; Do we need to be sensitive to any of the other event types?:-
    ;;
    ;;  + GDK_ENTER_NOTIFY_MASK
    ;;  + GDK_STRUCTURE_MASK
    ;;  + GDK_PROPERTY_CHANGE_MASK
    ;;  + GDK_VISIBILITY_NOTIFY_MASK
    ;;  + GDK_PROXIMITY_IN_MASK
    ;;  + GDK_PROXIMITY_OUT_MASK
    ;;  + GDK_SUBSTRUCTURE_MASK
    ;;  + GDK_SCROLL_MASK
    ;;  + GDK_ALL_EVENTS_MASK
    ;; FIXME: SHOULD WE MAYBE ALLOW SYMBOLIC NAMES FOR KEY/BUTTON/MOTION EVENTS?
    (cffi/gtk-widget-add-events widget
				(logior +CFFI/GDK-EXPOSURE-MASK+
					+CFFI/GDK-LEAVE-NOTIFY-MASK+
					(if (find "key-press-event" handlers :test #'string=)
					    +CFFI/GDK-KEY-PRESS-MASK+
					    0)
					(if (find "key-release-event" handlers :test #'string=)
					    +CFFI/GDK-KEY-RELEASE-MASK+
					    0)
					(if (find "button-press-event" handlers :test #'string=)
					    +CFFI/GDK-BUTTON-PRESS-MASK+
					    0)
					(if (find "button-release-event" handlers :test #'string=)
					    +CFFI/GDK-BUTTON-RELEASE-MASK+
					    0)
					(if (find "focus-in-event" handlers :test #'string=)
					    +CFFI/GDK-FOCUS-CHANGE-MASK+
					    0)
					(if (find "motion-notify-event" handlers :test #'string=)
					    ;; FIXME: Will this stuff up drag 'n drop? Nope, 'cause it doesn't
					    ;; work (i.e. no motion-notify events generated when a button is
					    ;; held down. *sigh* - WHY NOT?
					    (logior +CFFI/GDK-POINTER-MOTION-MASK+
						    +CFFI/GDK-BUTTON-MOTION-MASK+
						    ;; I don't think these are necessary when the above is present,
						    ;; but just in case (for experimentation)...
						    +CFFI/GDK-BUTTON1-MOTION-MASK+
						    +CFFI/GDK-BUTTON2-MOTION-MASK+
						    +CFFI/GDK-BUTTON3-MOTION-MASK+)
					    0)
					))))


#||
/// Install event handlers

define sealed method generate-trigger-event
    (port :: <gtk-port>, sheet :: <sheet>) => ()
  let mirror = sheet-mirror(sheet);
  when (mirror)
    let widget = mirror-widget(mirror);
    ignoring("generate-trigger-event")
  end
end method generate-trigger-event;
||#

(defmethod generate-trigger-event ((port <gtk-port>)
				   (sheet <sheet>))
  (cffi/g-main-context-wakeup nil))

#||
define sealed method process-next-event
    (_port :: <gtk-port>, #key timeout)
 => (timed-out? :: <boolean>)
  //--- We should do something with the timeout
  ignore(timeout);
  gtk-main-iteration();
  #f;
end method process-next-event;
||#

(defmethod process-next-event ((_port <gtk-port>)
			       &key timeout)
  (declare (ignore timeout))
  (cffi/gtk-main-iteration)
  nil)


#||
/// Event handlers

define method handle-gtk-destroy-event
    (sheet :: <sheet>, widget :: <GtkWidget*>, event :: <GdkEventAny*>)
 => (handled? :: <boolean>)
  ignoring("handle-gtk-destroy-event");
  // frame-can-quit?...
  #t
end method handle-gtk-destroy-event;
||#

(defmethod handle-gtk-delete-event-signal ((sheet <sheet>)
					   widget ;;<GtkWidget*>)
					   event  ;;<GdkEventAny*>))
					   user-data)
  (declare (ignore widget event user-data))
  (ignoring "***** HANDLE-GTK-DELETE-EVENT-SIGNAL - FOR <SHEET>")
  nil)


#||
define method handle-gtk-motion-event
    (sheet :: <sheet>, widget :: <GtkWidget*>, event :: <GdkEventMotion*>)
 => (handled? :: <boolean>)
  let mirror = widget-mirror(widget);
  let sheet = mirror-sheet(mirror);
  let _port = port(sheet);
  when (_port)
    let native-x  = event.x-value;
    let native-y  = event.y-value;
    let state     = event.state-value;
    ignoring("motion modifiers");
    let modifiers = 0;
    let (x, y)
      = untransform-position(sheet-native-transform(sheet), native-x, native-y);
    distribute-event(_port,
		     make(<pointer-motion-event>,
			  sheet: sheet,
			  pointer: port-pointer(_port),
			  modifier-state: modifiers,
			  x: x, y: y))
  end;
  #t
end method handle-gtk-motion-event;
||#


(defun gtk-state->duim-buttons (state)
  (logior (if (not (zerop (logand state +CFFI/GDK-BUTTON1-MASK+)))
	      +left-button+
	      0)
	  (if (not (zerop (logand state +CFFI/GDK-BUTTON2-MASK+)))
	      +middle-button+
	      0)
	  (if (not (zerop (logand state +CFFI/GDK-BUTTON3-MASK+)))
	      +right-button+
	      0)))


(defmethod handle-gtk-motion-notify-event-signal ((sheet <sheet>)
						  widget ;;<GtkWidget*>)
						  event  ;;<GdkEventMotion*>))
						  user-data)
  (declare (ignore user-data))
  (let* ((mirror (widget-mirror widget))
	 (sheet (mirror-sheet mirror))
	 (_port (port sheet)))
    (when _port
      ;; x + y are returned as doubles from gtk, but the rest of duim appears to expect
      ;; them to be integer values. FIXME?
      (let* ((native-x  (floor (cffi/gdkeventmotion->x event)))
	     (native-y  (floor (cffi/gdkeventmotion->y event)))
	     (event-state (cffi/gdkeventmotion->state event))
	     (modifiers (gtk-state->duim-modifiers event-state))
	     (button (gtk-state->duim-buttons event-state)))
	(multiple-value-bind (x y)
	    (untransform-position (sheet-native-transform sheet) native-x native-y)
	  (if (zerop button)
	      ;; no button, vanilla motion event
	      (distribute-event _port
				(make-instance '<pointer-motion-event>
					       :sheet sheet
					       :pointer (port-pointer _port)
					       :modifier-state modifiers
					       :x x :y y))
	      ;; else - drag event
	      (distribute-event _port
				(make-instance '<pointer-drag-event>
					       :sheet sheet
					       :pointer (port-pointer _port)
					       :button button
					       :modifier-state modifiers
					       :x x :y y)))))))
  nil)


#||
define method handle-gtk-enter-event
    (sheet :: <sheet>, widget :: <GtkWidget*>, event :: <GdkEventCrossing*>)
 => (handled? :: <boolean>)
  ignore(widget);
  handle-gtk-crossing-event(sheet, event, <pointer-enter-event>)
end method handle-gtk-enter-event;
||#

(defmethod handle-gtk-enter-notify-event-signal ((sheet <sheet>)
						 widget ;;<GtkWidget*>)
						 event  ;;<gdkEventCrossing*>))
						 user-data)
  (declare (ignore widget user-data))
  (handle-gtk-crossing-event sheet event (find-class '<pointer-enter-event>))
  nil)

#||
define method handle-gtk-leave-event
    (sheet :: <sheet>, widget :: <GtkWidget*>, event :: <GdkEventCrossing*>)
 => (handled? :: <boolean>)
  ignore(widget);
  handle-gtk-crossing-event(sheet, event, <pointer-exit-event>)
end method handle-gtk-leave-event;
||#

(defmethod handle-gtk-leave-notify-event-signal ((sheet <sheet>)
						 widget ;;<GtkWidget*>)
						 event  ;;<GdkEventCrossing*>))
						 user-data)
  (declare (ignore widget user-data))
  (handle-gtk-crossing-event sheet event (find-class '<pointer-exit-event>))
  nil)

#||
// Watch out, because leave events show up after window have been killed!
define sealed method handle-gtk-crossing-event
    (sheet :: <sheet>, event :: <GdkEventCrossing*>,
     event-class :: subclass(<pointer-motion-event>))
 => (handled? :: <boolean>)
  let _port = port(sheet);
  when (_port)
    let native-x  = event.x-value;
    let native-y  = event.y-value;
    let state     = event.state-value;
    let modifiers = 0;	//--- Do this properly!
    let detail    = event.detail-value;
    let (x, y)
      = untransform-position(sheet-native-transform(sheet), native-x, native-y);
    distribute-event(_port,
		     make(event-class,
			  sheet: sheet,
			  pointer: port-pointer(_port),
			  kind: gtk-detail->duim-crossing-kind(detail),
			  modifier-state: modifiers,
			  x: x, y: y));
    #t
  end
end method handle-gtk-crossing-event;
||#

(defmethod handle-gtk-crossing-event ((sheet <sheet>)
				      event ;;<GdkEventCrossing*>)
				      event-class)
  (check-type event-class <pointer-motion-event>)
  (let ((_port (port sheet)))
    (when _port
      (let ((native-x  (cffi/gdkeventcrossing->x event))
	    (native-y  (cffi/gdkeventcrossing->y event))
	    (modifiers (gtk-state->duim-modifiers (cffi/gdkeventcrossing->state event)))
	    (detail    (cffi/gdkeventcrossing->detail event)))
	(multiple-value-bind (x y)
	    (untransform-position (sheet-native-transform sheet) native-x native-y)
	  (distribute-event _port
			    (make-instance event-class
					   :sheet sheet
					   :pointer (port-pointer _port)
					   :kind (gtk-detail->duim-crossing-kind detail)
					   :modifier-state modifiers
					   :x x :y y))))
      nil)))

#||
define function gtk-detail->duim-crossing-kind
    (detail :: <integer>) => (kind :: <symbol>)
  select (detail)
    $GDK-NOTIFY-ANCESTOR          => #"ancestor";
    $GDK-NOTIFY-VIRTUAL           => #"virtual";
    $GDK-NOTIFY-INFERIOR          => #"inferior";
    $GDK-NOTIFY-NONLINEAR         => #"nonlinear";
    $GDK-NOTIFY-NONLINEAR-VIRTUAL => #"nonlinear-virtual";
  end
end function gtk-detail->duim-crossing-kind;
||#

(defun gtk-detail->duim-crossing-kind (detail)
  (ecase detail
    (+GDK-NOTIFY-ANCESTOR+          :ancestor)
    (+GDK-NOTIFY-VIRTUAL+           :virtual)
    (+GDK-NOTIFY-INFERIOR+          :inferior)
    (+GDK-NOTIFY-NONLINEAR+         :nonlinear)
    (+GDK-NOTIFY-NONLINEAR-VIRTUAL+ :nonlinear-virtual)))

#||
define method handle-gtk-button-press-event
    (sheet :: <sheet>, widget :: <GtkWidget*>, event :: <GdkEventButton*>)
 => (handled? :: <boolean>)
  handle-gtk-button-event(sheet, widget, event)
end method handle-gtk-button-press-event;
||#

(defmethod handle-gtk-button-press-event-signal ((sheet <sheet>)
						 widget ;;<GtkWidget*>)
						 event  ;;<GdkEventButton*>))
						 user-data)
  (declare (ignore user-data))
  (handle-gtk-button-event sheet widget event))

#||
define method handle-gtk-button-release-event
    (sheet :: <sheet>, widget :: <GtkWidget*>, event :: <GdkEventButton*>)
 => (handled? :: <boolean>)
  handle-gtk-button-press-event(sheet, widget, event)
end method handle-gtk-button-release-event;
||#

(defmethod handle-gtk-button-release-event-signal ((sheet <sheet>)
						   widget ;;<GtkWidget*>)
						   event  ;;<GdkEventButton*>))
						   user-data)
  (declare (ignore user-data))
  (handle-gtk-button-event sheet widget event))

#||
define method handle-gtk-button-event
    (sheet :: <sheet>, widget :: <GtkWidget*>, event :: <GdkEventButton*>)
 => (handled? :: <boolean>)
  let _port = port(sheet);
  when (_port)
    let native-x  = event.x-value;
    let native-y  = event.y-value;
    let button    = gtk-button->duim-button(event.button-value);
    let state     = event.state-value;
    let type      = event.type-value;
    let modifiers = 0;	//--- Do this!
    let event-class
      = select (type)
	  $GDK-2BUTTON-PRESS  => <double-click-event>;
	  $GDK-BUTTON-PRESS   => <button-press-event>;
	  $GDK-BUTTON-RELEASE => <button-release-event>;
	  otherwise           => #f;
	end;
    if (event-class)
      let (x, y)
	= untransform-position(sheet-native-transform(sheet), native-x, native-y);
      port-modifier-state(_port)    := modifiers;
      let pointer = port-pointer(_port);
      pointer-button-state(pointer) := button;
      distribute-event(_port,
		       make(event-class,
			    sheet: sheet,
			    pointer: pointer,
			    button: button,
			    modifier-state: modifiers,
			    x: x, y: y));
      #t
    end
  end
end method handle-gtk-button-event;
||#

(defmethod handle-gtk-button-event ((sheet <sheet>)
				    widget ;;<GtkWidget*>)
				    event) ;;<GdkEventButton*>))
  (declare (ignore widget))
  (gtk-debug "[GTK-EVENTS HANDLE-GTK-BUTTON-EVENT] entered for sheet ~a" sheet)
  (let ((_port (port sheet)))
    (when _port
      ;; x + y are returned as doubles from gtk, but the rest of duim appears to expect
      ;; them to be integer values. FIXME?
      (let* ((native-x  (round (cffi/gdkeventbutton->x event)))
	     (native-y  (round (cffi/gdkeventbutton->y event)))
	     (button    (gtk-button->duim-button (cffi/gdkeventbutton->button event)))
	     (state     (cffi/gdkeventbutton->state event))
	     (type      (cffi/gdkeventbutton->type event))
	     (modifiers (gtk-state->duim-modifiers state))
	     (event-class (cond ((equal? type +CFFI/GDK-2BUTTON-PRESS+)  (find-class '<double-click-event>))
				((equal? type +CFFI/GDK-BUTTON-PRESS+)   (find-class '<button-press-event>))
				((equal? type +CFFI/GDK-BUTTON-RELEASE+) (find-class '<button-release-event>))
				(t nil))))
	(when event-class
	  (multiple-value-bind (x y)
	      (untransform-position (sheet-native-transform sheet) native-x native-y)
	    (setf (port-modifier-state _port) modifiers)
	    (let ((pointer (port-pointer _port)))
	      (setf (pointer-button-state pointer) button)
	      (distribute-event _port
				(make-instance event-class
					       :sheet sheet
					       :pointer pointer
					       :button button
					       :modifier-state modifiers
					       :x x :y y))
	      nil)))))))

#||
define function gtk-button->duim-button
    (gtk-button :: <integer>) => (duim-button :: <integer>)
  select (gtk-button)
    1 => $left-button;
    2 => $middle-button;
    3 => $right-button;
  end
end function gtk-button->duim-button;
||#

(defun gtk-button->duim-button (gtk-button)
  (ecase gtk-button
    (1 +left-button+)
    (2 +middle-button+)
    (3 +right-button+)))

#||
define method handle-gtk-expose-event
    (sheet :: <sheet>, widget :: <GtkWidget*>, event :: <GdkEventExpose*>)
 => (handled? :: <boolean>)
  let _port = port(sheet);
  when (_port)
    let area = event.area-value;
    let native-x = area.x-value;
    let native-y = area.y-value;
    let native-width = area.width-value;
    let native-height = area.height-value;
    let native-transform = sheet-native-transform(sheet);
    let (x, y)
      = untransform-position(native-transform, native-x, native-y);
    let (width, height)
      = untransform-distance(native-transform, native-width, native-height);
    let region = make-bounding-box(x, y, x + width, y + height);
    distribute-event(_port,
		     make(<window-repaint-event>,
			  sheet:  sheet,
			  region: region))
  end;
  #t
end method handle-gtk-expose-event;


/*---*** Not handling state changes yet
define xt/xt-event-handler state-change-callback
    (widget, mirror, event)
  ignore(widget);
  handle-gtk-state-change-event(mirror, event);
  #t
end xt/xt-event-handler state-change-callback;

define sealed method handle-gtk-state-change-event
    (mirror :: <gtk-mirror>, event :: x/<XEvent>) => ()
  let sheet = mirror-sheet(mirror);
  let _port = port(sheet);
  when (_port)
    let type = event.x/type-value;
    select (type)
      #"configure-notify" =>
	handle-gtk-configuration-change-event(_port, sheet, event);
      #"map-notify"       =>
	note-mirror-enabled/disabled(_port, sheet, #t);
      #"unmap-notify"     =>
	note-mirror-enabled/disabled(_port, sheet, #f);
      #"circulate-notify" => #f;
      #"destroy-notify"   => #f;
      #"gravity-notify"   => #f;
      #"reparent-notify"  => #f;
    end
  end
end method handle-gtk-state-change-event;


define xt/xt-event-handler state-change-config-callback
    (widget, mirror, event)
  ignore(widget);
  handle-gtk-state-change-config-event(mirror, event);
  #t
end xt/xt-event-handler state-change-config-callback;

define sealed method handle-gtk-state-change-config-event
    (mirror :: <gtk-mirror>, event :: x/<XEvent>) => ()
  let sheet = mirror-sheet(mirror);
  let _port = port(sheet);
  when (_port)
    let type = event.x/type-value;
    select (type)
      #"configure-notify" =>
	handle-gtk-configuration-change-event(_port, sheet, event);
      #"map-notify"       => #f;
      #"unmap-notify"     => #f;
      #"circulate-notify" => #f;
      #"destroy-notify"   => #f;
      #"gravity-notify"   => #f;
      #"reparent-notify"  => #f;
    end
  end
end method handle-gtk-state-change-config-event;


define xt/xt-event-handler state-change-no-config-callback
    (widget, mirror, event)
  ignore(widget);
  handle-gtk-state-change-no-config-event(mirror, event);
  #t
end xt/xt-event-handler state-change-no-config-callback;

define sealed method handle-gtk-state-change-no-config-event
    (mirror :: <gtk-mirror>, event :: x/<XEvent>) => ()
  let sheet = mirror-sheet(mirror);
  let _port = port(sheet);
  when (_port)
    let type = event.x/type-value;
    select (type)
      #"map-notify"       =>
	note-mirror-enabled/disabled(_port, sheet, #t);
      #"unmap-notify"     =>
	note-mirror-enabled/disabled(_port, sheet, #f);
      #"configure-notify" => #f;
      #"circulate-notify" => #f;
      #"destroy-notify"   => #f;
      #"gravity-notify"   => #f;
      #"reparent-notify"  => #f;
    end
  end
end method handle-gtk-state-change-no-config-event;
*/

define method handle-gtk-configure-event
    (sheet :: <sheet>, widget :: <GtkWidget*>, event :: <GdkEventConfigure*>)
 => (handled? :: <boolean>)
  let allocation = widget.allocation-value;
  let native-x  = event.x-value;
  let native-y  = event.y-value;
  let native-width  = allocation.width-value;
  let native-height = allocation.height-value;
  let native-transform = sheet-native-transform(sheet);
  let (x, y)
    = untransform-position(native-transform, native-x, native-y);
  let (width, height)
    = untransform-distance(native-transform, native-width, native-height);
  let region = make-bounding-box(x, y, x + width, y + height);
  distribute-event(port(sheet),
		   make(<window-configuration-event>,
			sheet:  sheet,
			region: region));
  #t
end method handle-gtk-configure-event;
||#

(defmethod handle-gtk-configure-event-signal ((sheet <sheet>)
					      widget ;;<GtkWidget*>)
					      event  ;;<GdkEventConfigure*>))
					      user-data)
  (declare (ignorable user-data))
  (cffi:with-foreign-object (allocn '(:struct GtkAllocation))
    (let* ((allocation    (cffi/gtk_widget_get_allocation widget allocn))
	   (native-x      (cffi/gdkeventconfigure->x event))
	   (native-y      (cffi/gdkeventconfigure->y event))
	   (native-width  (cffi/gdkeventconfigure->width allocation))
	   (native-height (cffi/gdkeventconfigure->height allocation))
	   (native-transform (sheet-native-transform sheet)))
      (multiple-value-bind (x y)
	  (untransform-position native-transform native-x native-y)
	(multiple-value-bind (width height)
	    (untransform-distance native-transform native-width native-height)
	  (let ((region (make-bounding-box x y (+ x width) (+ y height))))
	    (distribute-event (port sheet)
			      (make-instance '<window-configuration-event>
					     :sheet sheet
					     :region region)))))))
  nil)

#||
define sealed method note-mirror-enabled/disabled
    (_port :: <gtk-port>, sheet :: <sheet>, enabled? :: <boolean>) => ()
  ignoring("note-mirror-enabled/disabled")
end method note-mirror-enabled/disabled;
||#

(defgeneric note-mirror-enabled/disabled (_port sheet enabled?))

(defmethod note-mirror-enabled/disabled ((_port <gtk-port>)
					 (sheet <sheet>)
					 enabled?)
  (declare (ignore enabled?))
  (ignoring "note-mirror-enabled/disabled (<sheet>)"))

#||
define sealed method note-mirror-enabled/disabled
    (_port :: <gtk-port>, sheet :: <top-level-sheet>, enabled? :: <boolean>) => ()
  ignoring("note-mirror-enabled/disabled")
end method note-mirror-enabled/disabled;
||#

(defmethod note-mirror-enabled/disabled ((_port <gtk-port>)
					 (sheet <top-level-sheet>)
					 enabled?)
  (declare (ignore enabled?))
  (ignoring "note-mirror-enabled/disabled (<top-level-sheet>)"))
