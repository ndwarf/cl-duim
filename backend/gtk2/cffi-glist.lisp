;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GTK2-DUIM -*-
(in-package #:gtk2-duim)

;;; ------ GList - Doubly linked list

;;; typedef struct {
;;;   gpointer data;
;;;   GList *next;
;;;   GList *prev;
;;; } GList;

;; .H CHECK A-OK

(cffi:defcstruct GList
  (data gpointer)
  (next GList*)
  (prev GList*))


(defun cffi/g_list_alloc ()
"
GList *
g_list_alloc (void);

Allocates space for one GList element. It is called by
g_list_append(), g_list_prepend(), g_list_insert() and
g_list_insert_sorted() and so is rarely used on its own.

Returns
    a pointer to the newly-allocated GList element
"
  (cffi:foreign-funcall "g_list_alloc"
			(:pointer (:struct GList))))


(defun cffi/g_list_free (glst)
"
void g_list_free (GList *list);

Frees all of the memory used by a GList. The freed elements are
returned to the slice allocator.

Note: If list elements contain dynamically-allocated memory, you
should either use g_list_free_full() or free them manually first.
"
  (cffi:foreign-funcall "g_list_free"
			(:pointer (:struct GList)) glst
			:void))


(defun cffi/g_list_previous (glst)
  (cffi:foreign-slot-value glst '(:struct GList) 'prev))


(defun cffi/g_list_next (glst)
  (cffi:foreign-slot-value glst '(:struct GList) 'next))


(defun cffi/glist->data (glst)
  (cffi:foreign-slot-value glst '(:struct GList) 'data))


