;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GTK2-DUIM -*-
(in-package #:gtk2-duim)

;;; ========================================================================
;;;
;;;    Support for events
;;;

;;; This blocks until an event is received; see also 'gtk-do-main-iteration'
;;; (or whatever it's called) for one that accepts a timeout value.
;;; Actually there's aren't any that block with a timeout. We'll need to
;;; roll our own to do that.

(defun cffi/gtk-main ()
  "void gtk_main (void);

Runs the main loop until gtk_main_quit() is called. You can nest
calls to gtk_main(). In that case gtk_main_quit() will make the
innermost invocation of the main loop return."
  (cffi:foreign-funcall "gtk_main" :void))


(defun cffi/gtk-main-iteration ()
  ;;; gboolean gtk_main_iteration (void)
  (let ((gtk-result (cffi:foreign-funcall "gtk_main_iteration" gboolean)))
    gtk-result))


(defun cffi/gtk-events-pending ()
  "gboolean gtk_events_pending (void);

Checks if any events are pending.

This can be used to update the UI and invoke timeouts etc. while doing
some time intensive computation.

Updating the UI during a long computation

    // computation going on...

    while (gtk_events_pending ())
        gtk_main_iteration ();

    // ...computation continued

Returns

    TRUE if any events are pending, FALSE otherwise"
  (cffi:foreign-funcall "gtk_events_pending"
			gboolean))


(defun cffi/gtk-main-quit ()
  "void gtk_main_quit (void);

Makes the innermost invocation of the main loop return when it regains
control."
  (cffi:foreign-funcall "gtk_main_quit" :void)
  nil)


(defun cffi/g-main-context-new ()
  "GMainContext* g_main_context_new (void);

Creates a new GMainContext structure.

Returns :

    the new GMainContext"
  (let ((context (cffi:foreign-funcall "g_main_context_new"
				       :pointer)))
    (pointer-or-nil context)))



(defun cffi/g-main-context-iteration (context blocking?)
  "gboolean g_main_context_iteration (GMainContext *context,
                                      gboolean may_block);

Runs a single iteration for the given main loop. This involves
checking to see if any event sources are ready to be processed, then
if no events sources are ready and may_block is TRUE, waiting for a
source to become ready, then dispatching the highest priority events
sources that are ready. Otherwise, if may_block is FALSE sources are
not waited to become ready, only those highest priority events sources
will be dispatched (if any), that are ready at this given moment
without further waiting.

Note that even when may_block is TRUE, it is still possible for
g_main_context_iteration() to return FALSE, since the the wait may be
interrupted for other reasons than an event source becoming ready.

    - context :: a GMainContext (if NULL, the default context will be used)
    - may_block :: whether the call may block.

Returns :

    TRUE if events were dispatched.
"
  (cffi:foreign-funcall "g_main_context_iteration"
			:pointer (or context (cffi:null-pointer))
			gboolean blocking?
			gboolean))


(defun cffi/g-main-context-unref (context)
  "void g_main_context_unref (GMainContext *context);

Decreases the reference count on a GMainContext object by one. If the
result is zero, free the context and free all associated memory.

    - context :: a GMainContext (assume: if NULL, the default context
                 will be used)"
  (cffi:foreign-funcall "g_main_context_unref"
			:pointer (or context (cffi:null-pointer))
			:void))


(defun cffi/g-main-context-wakeup (context)
  "void g_main_context_wakeup (GMainContext *context);

If context is currently blocking in g_main_context_iteration() waiting
for a source to become ready, cause it to stop blocking and
return. Otherwise, cause the next invocation of
g_main_context_iteration() to return without blocking.

This API is useful for low-level control over GMainContext; for
example, integrating it with main loop implementations such as
GMainLoop.

Another related use for this function is when implementing a main loop
with a termination condition, computed from multiple threads:

#define NUM_TASKS 10
static volatile gint tasks_remaining = NUM_TASKS;
...
 
while (g_atomic_int_get (&tasks_remaining) != 0)
  g_main_context_iteration (NULL, TRUE);

Then in a thread:

perform_work();

if (g_atomic_int_dec_and_test (&tasks_remaining))
  g_main_context_wakeup (NULL);

Parameters
    - context :: a GMainContext (assume: if NULL, the default context
                 will be used)
"
  (cffi:foreign-funcall "g_main_context_wakeup"
			:pointer (or context (cffi:null-pointer))
			:void))


(defun cffi/gtk-accelerator-get-default-mod-mask ()
"
 Gets the value set by gtk_accelerator_set_default_mod_mask().

    Returns - the default accelerator modifier mask
"
  ;; guint gtk_accelerator_get_default_mod_mask (void);
  (cffi:foreign-funcall "gtk_accelerator_get_default_mod_mask" guint))


(defun cffi/gtk-accel-group-new ()
"
 Creates a new GtkAccelGroup.

    Returns : a new GtkAccelGroup object

No floating ref, does not inherit from GInitiallyUnowned
"
  ;; GtkAccelGroup* gtk_accel_group_new (void);
  (let ((group (cffi:foreign-funcall "gtk_accel_group_new" :pointer)))
    (pointer-or-nil group)))


(defun cffi/gdk-keyval-name (keyval)
"
 Converts a key value into a symbolic name. The names are the same as those in
 the <gdk/gdkkeysyms.h> header file but without the leading 'GDK_'.

    keyval  - a key value.
    Returns - a string containing the name of the key, or NULL if keyval is not
              a valid key. The string should not be modified.
"
  ;; gchar* gdk_keyval_name (guint keyval);
  (let ((name (cffi:foreign-funcall "gdk_keyval_name"
				    :uint keyval
				    :string)))
    name))


(defun cffi/gdk-keyval-from-name (keyval-name)
"
 guint gdk_keyval_from_name (const gchar *keyval_name);

 Converts a key name to a key value.

    keyval_name - a key name.
    Returns     - the corresponding key value, or GDK_VoidSymbol if the key name is not a valid key.
"
  (cffi:foreign-funcall "gdk_keyval_from_name"
			:string keyval-name
			:uint))


(defun cffi/gdk-keyval-to-unicode (keyval)
"
 Convert from a GDK key symbol to the corresponding ISO10646 (Unicode) character.

    keyval  - a GDK key symbol
    Returns - the corresponding unicode character, or 0 if there is no corresponding character.
"
  ;; guint32 gdk_keyval_to_unicode (guint keyval);
  (cffi:foreign-funcall "gdk_keyval_to_unicode" guint keyval guint32))


;;; ------ G_OBJECT stuff


(defun cffi/g_object_ref (object)
"
Increases the reference count of object.

Since GLib 2.56, if GLIB_VERSION_MAX_ALLOWED is 2.56 or greater, the
type of object will be propagated to the return type (using the GCC
typeof() extension), so any casting the caller needs to do on the
return type must be explicit.

Parameters
    object - a GObject. [type GObject.Object]

Returns
    the same object.    [type GObject.Object][transfer none]
"
  ;; gpointer g_object_ref (gpointer object)
  (cffi:foreign-funcall "g_object_ref"
			:pointer object
			:pointer))


(defun cffi/g-object-unref (object)
"

Decreases the reference count of object . When its reference count
drops to 0, the object is finalized (i.e. its memory is freed).

If the pointer to the GObject may be reused in future (for example, if
it is an instance variable of another object), it is recommended to
clear the pointer to NULL rather than retain a dangling pointer to a
potentially invalid GObject instance. Use g_clear_object() for this.

Parameters
    object - a GObject.    [type GObject.Object]
"
  ;; void g_object_unref (gpointer object);
  (cffi:foreign-funcall "g_object_unref"
		      :pointer object
		      :void))



(defun cffi/g-object-ref-sink (object)
"
gpointer g_object_ref_sink (gpointer object);

Increase the reference count of object, and possibly remove
the floating reference, if object has a floating reference.

In other words, if the object is floating, then this call
'assumes ownership' of the floating reference, converting it
to a normal reference by clearing the floating flag while
leaving the reference count unchanged. If the object is not
floating, then this call adds a new normal reference increasing
the reference count by one.

    object : a GObject. [type GObject.Object]
    Returns : object. [type GObject.Object][transfer none]
"
  (if (or (null object) (cffi:null-pointer-p object))
      (error "CFFI/G-OBJECT-REF-SINK invoked with null OBJECT")
      (cffi:foreign-funcall "g_object_ref_sink"
			    :pointer object
			    :void))
  object)


(defun cffi/g-object-is-floating (object)
"
 Checks wether object has a floating reference.

    object  - a GObject
    Returns - TRUE if object has a floating reference
"
  ;; gboolean g_object_is_floating (gpointer object);
  (if (or (null object) (cffi:null-pointer-p object))
      (error "CFFI/G-OBJECT-IS-FLOATING invoked with null OBJECT")
      (cffi:foreign-funcall "g_object_is_floating"
			    :pointer object
			    gboolean)))



#||
;;;void g_object_set (gpointer object,
;;;                   const gchar *first_property_name,
;;;                   ...);
;;;
(defcfun ("g_object_set" g-object-set) :void
"
 Sets properties on an object.

    object              - a GObject
    first_property_name - name of the first property to set
    ...                 - value for the first property, followed optionally by
                          more name/value pairs, followed by NULL
"
   (object :pointer)
   (first-prop :pointer)
   &rest)
||#

;;; ------ G_OBJECT signals

(defun cffi/g-signal-connect-data (instance detailed_signal c_handler data destroy_data connect_flags)
"
 Connects a GCallback function to a signal for a particular object.
 Similar to g_signal_connect(), but allows to provide a GClosureNotify
 for the data which will be called when the signal handler is
 disconnected and no longer used. Specify connect_flags if you need
 ..._after() or ..._swapped() variants of this function.

    INSTANCE        - the instance to connect to.
    DETAILED_SIGNAL - a string of the form ``signal-name::detail''.
    C_HANDLER       - the GCallback to connect.
    DATA            - data to pass to c_handler calls.
    DESTROY_DATA    - a GClosureNotify for data.
    CONNECT_FLAGS   - a combination of GConnectFlags.

    Returns : the handler id
"
  ;; gulong g_signal_connect_data (gpointer instance
  ;;                               const gchar* detailed_signal,
  ;;                               GCallback c_handler,
  ;;                               gpointer data,
  ;;                               GClosureNotify destroy_data,
  ;;                               GConnectFlags connect_flags);
  (cond ((or (null instance) (cffi:null-pointer-p instance))
	 (error "CFFI/G-SIGNAL-CONNECT-DATA invoked with null INSTANCE"))
	((or (null detailed_signal) (and (cffi:pointerp detailed_signal) (cffi:null-pointer-p detailed_signal)))
	 (error "CFFI/G-SIGNAL-CONNECT-DATA invoked with null DETAILED_SIGNAL"))
	((or (null c_handler) (cffi:null-pointer-p c_handler))
	 (error "CFFI/G-SIGNAL-CONNECT-DATA invoked with null C_HANDLER"))
	(t (let* ((data (or data (cffi:null-pointer)))
		  (destroy_data (or destroy_data (cffi:null-pointer)))
		  (id (cffi:foreign-funcall "g_signal_connect_data"
					    gpointer       instance
					    :string        detailed_signal
					    GCallback      c_handler
					    gpointer       data
					    GClosureNotify destroy_data
					    GConnectFlags  connect_flags
					    gulong)))
	     id))))


(defun cffi/g-signal-connect (instance detailed_signal c_handler data)
"
 Connects the 'GCallback' function C_HANDLER to a signal for a
 particular object.
 The handler will be called before the default handler of the signal.

    INSTANCE        - the instance to connect to
    DETAILED_SIGNAL - a string of the form ``signal-name::detail''
    C_HANDLER       - the GCallback to connect
    DATA            - data to pass to C_HANDLER calls

    Returns         - the handler id
"
  (cffi/g-signal-connect-data instance
			 detailed_signal
			 c_handler
			 data
			 (cffi:null-pointer)
			 0))


(defun cffi/g-signal-connect-after (instance detailed_signal c_handler data)
"
 Connects a GCallback function to a signal for a particular object.

 The handler will be called after the default handler of the signal.

    INSTANCE        - the instance to connect to.
    DETAILED_SIGNAL - a string of the form ``signal-name::detail''.
    C_HANDLER       - the GCallback to connect.
    DATA            - data to pass to c_handler calls.

    Returns - the handler id
"
  (cffi/g-signal-connect-data instance
			 detailed_signal
			 c_handler
			 data
			 (cffi:null-pointer)
			 +CFFI/G-CONNECT-AFTER+))


(defun cffi/g-signal-connect-swapped (instance detailed_signal c_handler data)
"
 Connects a GCallback function to a signal for a particular object.
 The instance on which the signal is emitted and data will be swapped
 when calling the handler.

    INSTANCE        - the instance to connect to.
    DETAILED_SIGNAL - a string of the form ``signal-name::detail''.
    C_HANDLER       - the GCallback to connect.
    DATA            - data to pass to c_handler calls.

    Returns : the handler id 
"
    (cffi/g-signal-connect-data instance
			   detailed_signal
			   c_handler
			   data
			   (cffi:null-pointer)
			   +CFFI/G-CONNECT-SWAPPED+))


(defun cffi/g-main-loop-new (context running?)
"
Creates a new GMainLoop structure.

    context    - a GMainContext (if NULL, the default context will be used).
    is_running - set to TRUE to indicate that the loop is running.
                 This is not very important since calling g_main_loop_run()
                 will set this to TRUE anyway.
    Returns    - a new GMainLoop.
"
  ;; GMainLoop* g_main_loop_new (GMainContext *context, gboolean is_running);
  (let* ((context (or context (cffi:null-pointer)))
	 (main-loop (cffi:foreign-funcall "g_main_loop_new"
					  :pointer context
					  gboolean running?
					  :pointer)))
    (pointer-or-nil main-loop)))


(defun cffi/g-main-loop-run (mloop)
"
Runs a main loop until g_main_loop_quit() is called on the loop. If
this is called for the thread of the loop's GMainContext, it will
process events from the loop, otherwise it will simply wait.

    loop : a GMainLoop
"
  ;; void g_main_loop_run (GMainLoop *loop);
  (if (or (null mloop) (cffi:null-pointer-p mloop))
      (error "CFFI/G-MAIN-LOOP-RUN invoked with null MLOOP")
      (cffi:foreign-funcall "g_main_loop_run"
			    :pointer mloop
			    :void))
  mloop)


(defun cffi/g-main-loop-get-context (mloop)
  "GMainContext* g_main_loop_get_context (GMainLoop *loop);

Returns the GMainContext of loop.

    - loop :: a GMainLoop.

Returns :

    the GMainContext of loop"
  (let ((context (cffi:foreign-funcall "g_main_loop_get_context"
				       :pointer (or mloop (cffi:null-pointer))
				       :pointer)))
    (pointer-or-nil context)))


(defun cffi/g-main-loop-unref (mloop)
"
 Decreases the reference count on a GMainLoop object by one. If the
 result is zero, free the loop and free all associated memory.

    loop - a GMainLoop
"
  ;; void g_main_loop_unref (GMainLoop *loop);
  (if (or (null mloop) (cffi:null-pointer-p mloop))
      (error "CFFI/G-MAIN-LOOP-UNREF invoked with null MLOOP")
      (cffi:foreign-funcall "g_main_loop_unref"
			    :pointer mloop
			    :void))
  mloop)


(defun cffi/g-signal-emit-by-name (instance signal-name)
  "void g_signal_emit_by_name (gpointer instance,
                               const gchar *detailed_signal,
                               ...);

Emits a signal.

Note that g_signal_emit_by_name() resets the return value to the
default if no handlers are connected, in contrast to g_signal_emitv().

Parameters

    - instance :: the instance the signal is being emitted on.

    - detailed_signal :: a string of the form ``signal-name::detail''.

    - ... :: parameters to be passed to the signal, followed by a
      location for the return value. If the return type of the signal
      is G_TYPE_NONE, the return value location can be omitted.

"
  (cffi:foreign-funcall-varargs "g_signal_emit_by_name"
				(:pointer instance
				 :string signal-name)
				:void))


(defun cffi/g-signal-handler-block (instance handler_id)
"
 Blocks a handler of an instance so it will not be called during any
 signal emissions unless it is unblocked again. Thus ``blocking'' a signal
 handler means to temporarily deactive it, a signal handler has to be
 unblocked exactly the same amount of times it has been blocked before to
 become active again.

 The HANDLER_ID has to be a valid signal handler id, connected to a signal
 of INSTANCE.

    INSTANCE   - The instance to block the signal handler of.
    HANDLER_ID - Handler id of the handler to be blocked. 
"
  ;; void g_signal_handler_block (gpointer instance, gulong handler_id);
  (if (or (null instance) (cffi:null-pointer-p instance))
      (error "CFFI/G-SIGNAL-HANDLER-BLOCK invoked with null INSTANCE")
      (cffi:foreign-funcall "g_signal_handler_block"
			    gpointer instance
			    gulong   handler_id
			    :void))
  instance)


(defun cffi/g-signal-handler-unblock (instance handler_id)
"
 Undoes the effect of a previous g_signal_handler_block() call.
 A blocked handler is skipped during signal emissions and will not be
 invoked, unblocking it (for exactly the amount of times it has been
 blocked before) reverts its ``blocked'' state, so the handler will be
 recognized by the signal system and is called upon future or currently
 ongoing signal emissions (since the order in which handlers are called
 during signal emissions is deterministic, whether the unblocked handler
 in question is called as part of a currently ongoing emission depends
 on how far that emission has proceeded yet).

 The HANDLER_ID has to be a valid id of a signal handler that is
 connected to a signal of INSTANCE and is currently blocked.

    INSTANCE   - The instance to unblock the signal handler of.
    HANDLER_ID - Handler id of the handler to be unblocked. 
"
  ;; void g_signal_handler_unblock (gpointer instance, gulong handler_id);
  (if (or (null instance) (cffi:null-pointer-p instance))
      (error "CFFI/G-SIGNAL-HANDLER-UNBLOCK invoked with null INSTANCE")
      (cffi:foreign-funcall "g_signal_handler_unblock"
			    gpointer instance
			    gulong   handler_id
			    :void))
  instance)


(defun cffi/g-signal-handlers-block-matched (instance mask i1 i2 ptr1 func data)
"
 Blocks all handlers on an instance that match a certain selection criteria.
 The criteria mask is passed as an OR-ed combination of GSignalMatchType
 flags, and the criteria values are passed as arguments. Passing at least
 one of the G_SIGNAL_MATCH_CLOSURE, G_SIGNAL_MATCH_FUNC or G_SIGNAL_MATCH_DATA
 match flags is required for successful matches. If no handlers were found, 0
 is returned, the number of blocked handlers otherwise.

    INSTANCE  - The instance to block handlers from.
    MASK      - Mask indicating which of signal_id, detail, closure,
                func and/or data the handlers have to match.
    SIGNAL_ID -	Signal the handlers have to be connected to.
    DETAIL    - Signal detail the handlers have to be connected to.
    CLOSURE   - The closure the handlers will invoke.
    FUNC      - The C closure callback of the handlers (useless for non-C
                closures).
    DATA      - The closure data of the handlers' closures.

    Returns   - The number of handlers that matched.
"
  (let ((count (cffi:foreign-funcall "g_signal_handlers_block_matched"
				     gpointer         instance
				     GSignalMatchType mask
				     guint     i1     ; signal_id
				     GQuark    i2     ; detail
				     GClosure* ptr1   ; closure
				     gpointer         func
				     gpointer         data
				     guint)))
    count))


(defun cffi/g-signal-handlers-unblock-matched (instance mask i1 i2 ptr1 func data)
"
 Unblocks all handlers on an instance that match a certain selection criteria.
 The criteria mask is passed as an OR-ed combination of GSignalMatchType
 flags, and the criteria values are passed as arguments. Passing at least one
 of the G_SIGNAL_MATCH_CLOSURE, G_SIGNAL_MATCH_FUNC or G_SIGNAL_MATCH_DATA
 match flags is required for successful matches. If no handlers were found, 0
 is returned, the number of unblocked handlers otherwise. The match criteria
 should not apply to any handlers that are not currently blocked.

    INSTANCE  - The instance to unblock handlers from.
    MASK      - Mask indicating which of signal_id, detail, closure,
                func and/or data the handlers have to match.
    SIGNAL_ID - Signal the handlers have to be connected to.
    DETAIL    - Signal detail the handlers have to be connected to.
    CLOSURE   - The closure the handlers will invoke.
    FUNC      - The C closure callback of the handlers (useless for non-C
                closures).
    DATA      - The closure data of the handlers' closures.

    Returns   - The number of handlers that matched.
"
  (if (or (null instance) (cffi:null-pointer-p instance))
      (error "CFFI/G-SIGNAL-HANDLERS-UNBLOCK-MATCHED invoked with null INSTANCE")
      (if (or (null func) (cffi:null-pointer-p func))
	  (error "CFFI/G-SIGNAL-HANDLERS-UNBLOCK-MATCHED invoked with null FUNC")
	  (let* ((data (or data (cffi:null-pointer)))
		 (count (cffi:foreign-funcall "g_signal_handlers_unblock_matched"
					      gpointer         instance
					      GSignalMatchType mask
					      guint     i1     ; signal_id
					      GQuark    i2     ; detail
					      GClosure* ptr1   ; closure
					      gpointer         func
					      gpointer         data
					      guint)))
	    count))))


(defun cffi/g-signal-handlers-block-by-func (instance func data)
"
 Blocks all handlers on an instance that match func and data.

    INSTANCE - The instance to block handlers from.
    FUNC     - The C closure callback of the handlers (useless for
               non-C closures).
    DATA     - The closure data of the handlers' closures.

    Returns  - The number of handlers that got blocked.
"
  (if (or (null instance) (cffi:null-pointer-p instance))
      (error "CFFI/G-SIGNAL-HANDLERS-BLOCK-BY-FUNC invoked with null INSTANCE")
      (if (or (null func) (cffi:null-pointer-p func))
	  (error "CFFI/G-SIGNAL-HANDLERS-BLOCK-BY-FUNC invoked with null FUNC")
	  (let* ((data (or data (cffi:null-pointer)))
		 (count (cffi/g-signal-handlers-block-matched instance
							      (logior +CFFI/G-SIGNAL-MATCH-FUNC+
								      +CFFI/G-SIGNAL-MATCH-DATA+)
							      0
							      0
							      (cffi:null-pointer)
							      func
							      data)))
	    count))))


(defun cffi/g-signal-handlers-unblock-by-func (instance func data)
"
 Unblocks all handlers on an instance that match func and data.

    INSTANCE - The instance to unblock handlers from.
    FUNC     - The C closure callback of the handlers (useless for
               non-C closures).
    DATA     - The closure data of the handlers' closures.

    Returns  - The number of handlers that got unblocked.
"
  (if (or (null instance) (cffi:null-pointer-p instance))
      (error "CFFI/G-SIGNAL-HANDLERS-UNBLOCK-BY-FUNC invoked with null INSTANCE")
      (if (or (null func) (cffi:null-pointer-p func))
	  (error "CFFI/G-SIGNAL-HANDLERS-UNBLOCK-BY-FUNC invoked with null FUNC")
	  (let* ((data (or data (cffi:null-pointer)))
		 (count (cffi/g-signal-handlers-unblock-matched instance
								(logior +CFFI/G-SIGNAL-MATCH-FUNC+
									+CFFI/G-SIGNAL-MATCH-DATA+)
								0
								0
								(cffi:null-pointer)
								func
								data)))
	    count))))


;;; FIXME: g_signal_handler_disconnect?

(defun cffi/g-object-freeze-notify (object)
"
 Stops emission of ``notify'' signals on object. The signals are queued
 until g_object_thaw_notify() is called on object.

 This is necessary for accessors that modify multiple properties to prevent
 premature notification while the object is still being modified.

    OBJECT : a GObject
"
  ;; void g_object_freeze_notify (GObject* object);
  (if (or (null object) (cffi:null-pointer-p object))
      (error "CFFI/G-OBJECT-FREEZE-NOTIFY invoked with null OBJECT")
      (cffi:foreign-funcall "g_object_freeze_notify"
			    GObject* object
			    :void))
  object)


(defun cffi/g-object-thaw-notify (object)
"
 Reverts the effect of a previous call to g_object_freeze_notify().
 This causes all queued ``notify'' signals on object to be emitted.

    OBJECT - a GObject
"
  ;; void g_object_thaw_notify (GObject* object);
  (if (or (null object) (cffi:null-pointer-p object))
      (error "CFFI/G-OBJECT-THAW-NOTIFY invoked with null OBJECT")
      (cffi:foreign-funcall "g_object_thaw_notify"
			    GObject* object
			    :void))
  object)


;;; ------ GTK signals

(defun cffi/gtk-signal-emitv-by-name (object name arg-ptr)
  ;; XXX: Work out what to do here that isn't deprecated :)
  #-(and)
  (gtk-debug "DEPRECATED: gtk-signal-emitv-by-name")
  ;; void gtk_signal_emitv_by_name (GtkObject* object, const gchar* name,
  ;;                               GtkArg* args)
  (if (or (null object) (cffi:null-pointer-p object))
      (error "CFFI/GTK-SIGNAL-EMITV-BY-NAME invoked with null OBJECT")
      (if (or (null name) (and (cffi:pointerp name) (cffi:null-pointer-p name)))
	  (error "CFFI/GTK-SIGNAL-EMITV-BY-NAME invoked with null NAME")
	  (let ((arg-ptr (or arg-ptr (cffi:null-pointer))))
	    (cffi:foreign-funcall "gtk_signal_emitv_by_name"
				  GtkObject* object
				  :string    name
				  GtkArg*    arg-ptr
				  :void))))
  object)


;;; ------

(defun cffi/gtk-get-current-event-time ()
"
If there is a current event and it has a timestamp, return that timestamp,
otherwise return GDK_CURRENT_TIME.

    Returns - the timestamp from the current event, or GDK_CURRENT_TIME.
"
  ;; guint32 gtk_get_current_event_time (void);
  (cffi:foreign-funcall "gtk_get_current_event_time" guint32))


