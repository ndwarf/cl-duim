;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GTK2-DUIM -*-
(in-package #:gtk2-duim)

#||
Module:       gtk-duim
Synopsis:     GTK top level window handling
Author:       Andy Armstrong, Scott McKay
Copyright:    Original Code is Copyright (c) 1995-2004 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND

/// Useful constants 

//---*** This should really be computed
define constant $top-level-border     = 0;
define constant $top-level-y-spacing  = 3;		// in pixels
define constant $default-window-title = "DUIM Window";
||#

(defconstant +top-level-border+ 0)
(defconstant +top-level-y-spacing+ 3)
(defparameter *default-window-title* "DUIM Window")


(defgeneric (setf %accelerator-table) (table mirror))
(defgeneric top-level-mirror (sheet &key error?))
(defgeneric accelerator-table (sheet &key create?))
(defgeneric make-accelerator-table (sheet))
(defgeneric destroy-accelerator-table (sheet))
(defgeneric mirror-registered-dialogs (mirror))
(defgeneric register-dialog-mirror (frame dialog-mirror))
(defgeneric unregister-dialog-mirror (frame dialog-mirror))
(defgeneric make-top-level-mirror (sheet frame))
(defgeneric top-level-layout-child (frame-manager frame layout))



#||
/// Top level mirrors

define sealed class <top-level-mirror> (<widget-mirror>)
  sealed slot %dialog-mirrors :: <stretchy-object-vector>
    = make(<stretchy-vector>);
end class <top-level-mirror>;
||#

;;; moved to classes.lisp
;; (defclass <gtk-top-level-mirror> (<gtk-widget-mirror>)
;;   ((%dialog-mirrors :type array :initform (make-array 0 :adjustable t :fill-pointer t) :accessor %dialog-mirrors)
;;    (%accelerator-table :initform nil :reader %accelerator-table :writer %accelerator-table-setter)))


;; Note: this is called *from* DESTROY-ACCELERATOR-TABLE which seems to be a bit
;; of a no-op.
;; FIXME: Remember to set this to nil when the frame is shutting down...
(defmethod (setf %accelerator-table) (table (mirror <gtk-top-level-mirror>))
  (declare (ignore table))
  #-(and)
  (let ((previous-table (%accelerator-table mirror))
	;; FIXME: We could check for the TOP_LEVEL flag, but what to do if it isn't the window?
	(gtk-window (cffi/gtk-widget-get-toplevel (mirror-widget mirror))))
    (when previous-table
      (cffi/gtk-window-remove-accel-group gtk-window previous-table)
      (cffi/g-object-unref previous-table))
    (when table
      (cffi/gtk-window-add-accel-group gtk-window table))
    (%accelerator-table-setter table mirror)
    table))


#||
define sealed method top-level-mirror
    (sheet :: <sheet>, #key error? = #f)
 => (mirror :: false-or(<top-level-mirror>))
  let sheet  = top-level-sheet(sheet);
  let mirror = sheet & sheet-direct-mirror(sheet);
  mirror
    | (error? & error("Failed to find top-level mirror for %=", sheet))
end method top-level-mirror;
||#

(defmethod top-level-mirror ((sheet <sheet>) &key (error? nil))
  (let* ((sheet  (top-level-sheet sheet))
	 (mirror (and sheet (sheet-direct-mirror sheet))))
    (or mirror
	(and error? (error "Failed to find top-level mirror for ~a" sheet)))))

#||
define sealed method top-level-mirror
    (frame :: <frame>, #key error? = #f)
 => (mirror :: false-or(<top-level-mirror>))
  let sheet  = top-level-sheet(frame);
  let mirror = sheet & sheet-direct-mirror(sheet);
  mirror
    | (error? & error("Failed to find top-level mirror for %=", sheet))
end method top-level-mirror;
||#

(defmethod top-level-mirror ((frame <frame>) &key (error? nil))
  (let* ((sheet (top-level-sheet frame))
	 (mirror (and sheet (sheet-direct-mirror sheet))))
    (or mirror
	(and error? (error "Failed to find top-level mirror for ~a" sheet)))))

#||
define method set-mirror-parent
    (child :: <widget-mirror>, parent :: <top-level-mirror>)
 => ()
  let (x, y) = sheet-native-edges(mirror-sheet(child));
  gtk-container-add(GTK-CONTAINER(mirror-widget(parent)),
		    mirror-widget(child))
end method set-mirror-parent;
||#

(defmethod set-mirror-parent ((child <gtk-widget-mirror>)
			      (parent <gtk-top-level-mirror>))
  (cffi/gtk-container-add (mirror-widget parent) (mirror-widget child)))


#||    
define method move-mirror
    (parent :: <top-level-mirror>, child :: <widget-mirror>,
     x :: <integer>, y :: <integer>)
 => ()
  unless (x == 0 & y == 0)
    ignoring("move-mirror for <top-level-mirror>")
  end
end method move-mirror;
||#

(defmethod move-mirror ((parent <gtk-top-level-mirror>)
			(child <gtk-widget-mirror>)
			(x integer)
			(y integer))
  #-(and)
  (cffi/gtk-window-move window x y)
  (unless (= x y 0)
    (ignoring "move-mirror for <gtk-top-level-mirror>")))


#||
define method size-mirror
    (parent :: <top-level-mirror>, child :: <widget-mirror>,
     width :: <integer>, height :: <integer>)
 => ()
  ignore(parent);
  set-mirror-size(child, width, height)
end method size-mirror;
||#

(defmethod size-mirror ((parent <gtk-top-level-mirror>)
			(child <gtk-widget-mirror>)
			(width integer)
			(height integer))
  (declare (ignore parent))
  (set-mirror-size child width height))



#||
/// Accelerator handling

define function make-keyboard-gesture
    (keysym :: <symbol>, #rest modifiers)
 => (gesture :: <keyboard-gesture>)
  make(<keyboard-gesture>, keysym: keysym, modifiers: modifiers)
end function make-keyboard-gesture;


define function gesture-modifiers
    (gesture :: <keyboard-gesture>)
 => (shift? :: <boolean>, control? :: <boolean>, alt? :: <boolean>)
  let modifier-state = gesture-modifier-state(gesture);
  values(~zero?(logand(modifier-state, $shift-key)),
	 ~zero?(logand(modifier-state, $control-key)),
	 ~zero?(logand(modifier-state, $alt-key)))
end function gesture-modifiers;
||#

(defun gesture-modifiers (gesture)
  ;; => shift?, control?, alt?, (altgr?, meta?), super?, hyper?
  (check-type gesture <keyboard-gesture>)
  (let ((modifier-state (gesture-modifier-state gesture)))
    (values (not (zerop (logand modifier-state +CFFI/GDK-SHIFT-MASK+)))
	    (not (zerop (logand modifier-state +CFFI/GDK-CONTROL-MASK+)))
	    (not (zerop (logand modifier-state +CFFI/GDK-MOD1-MASK+)))    ;; alt
	    ;; FIXME: altgr, meta?
;;	    (not (zerop (logand modifier-state +GDK-ALTGR-MASK+)))
;;	    (not (zerop (logand modifier-state (if alt-is-meta? +CFFI/GDK-MOD1-MASK+ +CFFI/GDK-META-MASK+))))
	    (not (zerop (logand modifier-state +CFFI/GDK-SUPER-MASK+)))
	    (not (zerop (logand modifier-state +CFFI/GDK-HYPER-MASK+))))))



#||
//---*** WHAT ABOUT ALL THIS ACCELERATOR STUFF?
define table $accelerator-table :: <object-table>
  = { // This is the set defined by WIG, Appendix B, Table B.2, page 438
      #"Copy"        => make-keyboard-gesture(#"c", #"control"),
      #"Cut"         => make-keyboard-gesture(#"x", #"control"),
      #"Help"        => make-keyboard-gesture(#"f1"),
      #"Open"        => make-keyboard-gesture(#"o", #"control"),
      #"Open..."     => make-keyboard-gesture(#"o", #"control"),
      #"Paste"       => make-keyboard-gesture(#"v", #"control"),
      #"Print"       => make-keyboard-gesture(#"p", #"control"),
      #"Print..."    => make-keyboard-gesture(#"p", #"control"),
      #"Save"        => make-keyboard-gesture(#"s", #"control"),
      #"Undo"        => make-keyboard-gesture(#"z", #"control"),

      // The same set with the mnemonics already in (a bit of a hack!)
      #"&Copy"       => make-keyboard-gesture(#"c", #"control"),
      #"Cu&t"        => make-keyboard-gesture(#"x", #"control"),
      #"&Help"       => make-keyboard-gesture(#"f1"),
      #"&Open"       => make-keyboard-gesture(#"o", #"control"),
      #"&Open..."    => make-keyboard-gesture(#"o", #"control"),
      #"&Paste"      => make-keyboard-gesture(#"v", #"control"),
      #"&Print"      => make-keyboard-gesture(#"p", #"control"),
      #"&Print..."   => make-keyboard-gesture(#"p", #"control"),
      #"&Save"       => make-keyboard-gesture(#"s", #"control"),
      #"&Undo"       => make-keyboard-gesture(#"z", #"control"),

      // Some extras that seemed to be missing
      #"Delete"      => make-keyboard-gesture(#"delete"),
      #"Find"        => make-keyboard-gesture(#"f", #"control"),
      #"Find..."     => make-keyboard-gesture(#"f", #"control"),
      #"New"         => make-keyboard-gesture(#"n", #"control"),
      #"New..."      => make-keyboard-gesture(#"n", #"control"),
      #"Redo"        => make-keyboard-gesture(#"y", #"control"),
      #"Select All"  => make-keyboard-gesture(#"a", #"control"),

      // The same set with the mnemonics already in (a bit of a hack!)
      #"&Delete"     => make-keyboard-gesture(#"delete"),
      #"&Find"       => make-keyboard-gesture(#"f", #"control"),
      #"&Find..."    => make-keyboard-gesture(#"f", #"control"),
      #"&New"        => make-keyboard-gesture(#"n", #"control"),
      #"&New..."     => make-keyboard-gesture(#"n", #"control"),
      #"&Redo"       => make-keyboard-gesture(#"y", #"control"),
      #"&Select All" => make-keyboard-gesture(#"a", #"control")
      };
||#

;;; The user has two choices; either create a frame that uses GTK 'stock items'
;;; in which case any menu items that accelerators are NOT defined for will pick up
;;; any stock accelerator, or explicitly supply all the accelerators.
;;;
;;; Since DUIM wants to know what the accelerator is we enumerate the items
;;; that should "find" stock items in gtk and the accelerators the should
;;; end up with.
;;;
;;; These are listed twice because we want to match the menu label
;;; irrespective of if there's a mnemonic embedded in it too...

(defparameter +accelerator-table+ (make-hash-table :test #'equal))
#-(and)
(macrolet ((frob (key gesture)
	     `(setf (gethash ,key +accelerator-table+) ,gesture)))

  ;; FILE MENU
  (frob "New"                (make-keyboard-gesture :n :control))
  (frob "Open"               (make-keyboard-gesture :o :control))
  (frob "Save"               (make-keyboard-gesture :s :control))
  (frob "Print"              (make-keyboard-gesture :p :control))
  (frob "Close"              (make-keyboard-gesture :w :control))
  (frob "Quit"               (make-keyboard-gesture :q :control))

  (frob "&New"               (make-keyboard-gesture :n :control))
  (frob "&Open"              (make-keyboard-gesture :o :control))
  (frob "&Save"              (make-keyboard-gesture :s :control))
  (frob "&Print"             (make-keyboard-gesture :p :control))
  (frob "&Close"             (make-keyboard-gesture :w :control))
  (frob "&Quit"              (make-keyboard-gesture :q :control))

  ;; EDIT MENU
  (frob "Undo"               (make-keyboard-gesture :z :control))
  (frob "Redo"               (make-keyboard-gesture :Z :shift :control))
  (frob "Cut"                (make-keyboard-gesture :x :control))
  (frob "Copy"               (make-keyboard-gesture :c :control))
  (frob "Paste"              (make-keyboard-gesture :v :control))
  (frob "Duplicate"          (make-keyboard-gesture :u :control))
  (frob "Select All"         (make-keyboard-gesture :a :control))
  (frob "Invert Selection"   (make-keyboard-gesture :i :control))
  (frob "Delete"             (make-keyboard-gesture :delete))
  (frob "Find..."            (make-keyboard-gesture :f :control))
  (frob "Search..."          (make-keyboard-gesture :f :shift :control))
  (frob "Find Next"          (make-keyboard-gesture :g :control))
  (frob "Replace..."         (make-keyboard-gesture :h :control))
  (frob "Rename"             (make-keyboard-gesture :f2))

  (frob "&Undo"              (make-keyboard-gesture :z :control))
  (frob "&Redo"              (make-keyboard-gesture :z :shift :control))
  (frob "Cu&t"               (make-keyboard-gesture :x :control))
  (frob "&Copy"              (make-keyboard-gesture :c :control))
  (frob "&Paste"             (make-keyboard-gesture :v :control))
  (frob "&Duplicate"         (make-keyboard-gesture :u :control))
  (frob "Select &All"        (make-keyboard-gesture :a :control))
  (frob "In&vert Selection"  (make-keyboard-gesture :i :control))
  (frob "&Delete"            (make-keyboard-gesture :delete))
  (frob "&Find..."           (make-keyboard-gesture :f :control))
  (frob "&Search..."         (make-keyboard-gesture :f :shift :control))
  (frob "Find Ne&xt"         (make-keyboard-gesture :g :control))
  (frob "&Replace..."        (make-keyboard-gesture :h :control))
  (frob "R&ename"            (make-keyboard-gesture :f2))

  ;; VIEW MENU
  (frob "Zoom In"            (make-keyboard-gesture :+ :control))
  (frob "Zoom Out"           (make-keyboard-gesture :- :control))
  (frob "Normal Size"        (make-keyboard-gesture :0 :control))
  (frob "Refresh"            (make-keyboard-gesture :r :control))
  (frob "Reload"             (make-keyboard-gesture :r :shift :control))
  (frob "Properties"         (make-keyboard-gesture :enter :alt))

  (frob "Zoom &In"           (make-keyboard-gesture :+ :control))
  (frob "Zoom &Out"          (make-keyboard-gesture :- :control))
  (frob "&Normal Size"       (make-keyboard-gesture :0 :control))
  (frob "&Refresh"           (make-keyboard-gesture :r :control))
  (frob "&Reload"            (make-keyboard-gesture :r :shift :control))
  (frob "Pr&operties"        (make-keyboard-gesture :enter :alt))

  ;; BOOKMARKS MENU
  (frob "Add Bookmark"       (make-keyboard-gesture :d :control))
  (frob "Edit Bookmarks..."  (make-keyboard-gesture :d :shift :control))

  (frob "Add &Bookmark"      (make-keyboard-gesture :d :control))
  (frob "&Edit Bookmarks..." (make-keyboard-gesture :d :shift :control))

  ;; GO MENU
  (frob "Back"               (make-keyboard-gesture :left :alt))
  (frob "Next"               (make-keyboard-gesture :right :alt))
  (frob "Up"                 (make-keyboard-gesture :up :alt))
  (frob "Home"               (make-keyboard-gesture :home :alt))
  (frob "Location..."        (make-keyboard-gesture :l :shift :control))

  (frob "&Back"              (make-keyboard-gesture :left :alt))
  (frob "Ne&xt"              (make-keyboard-gesture :right :alt))
  (frob "&Up"                (make-keyboard-gesture :up :alt))
  (frob "&Home"              (make-keyboard-gesture :home :alt))
  (frob "&Location..."       (make-keyboard-gesture :l :shift :control))

  ;; FORMAT MENU
  (frob "Bold"               (make-keyboard-gesture :b :control))
  (frob "Underline"          (make-keyboard-gesture :u :control))
  (frob "Italic"             (make-keyboard-gesture :i :control))

  (frob "&Bold"              (make-keyboard-gesture :b :control))
  (frob "&Underline"         (make-keyboard-gesture :u :control))
  (frob "&Italic"            (make-keyboard-gesture :i :control))

  ;; HELP MENU
  (frob "Contents"           (make-keyboard-gesture :f1))

  (frob "&Contents"          (make-keyboard-gesture :f1)))


#||
define sealed method defaulted-gadget-accelerator
    (framem :: <gtk-frame-manager>, gadget :: <accelerator-mixin>)
 => (accelerator :: false-or(<accelerator>))
  let accelerator = gadget-accelerator(gadget);
  if (unsupplied?(accelerator))
    let label = gadget-label(gadget);
    let key   = instance?(label, <string>) & as(<symbol>, label);
    element($accelerator-table, key, default: #f)
  else
    accelerator
  end
end method defaulted-gadget-accelerator;
||#

#-(and)
(defmethod defaulted-gadget-accelerator ((framem <gtk-frame-manager>) (gadget <accelerator-mixin>))
  ;; => accelerator (if supplied or there will be a stock accelerator),
  ;;    nil otherwise
  (let ((accelerator (gadget-accelerator gadget)))
    (if (unsupplied? accelerator)
	;; Check to see if there is a stock item for the label and use that if so.
	;; FIXME: Use CFFI/GTK-STOCK-LOOKUP instead of rolling our own? Would
	;; reduce LOC and be better future-proofed for when GTK change their
	;; preferred accelerators...
	(let* ((label (gadget-label gadget))
	       (key   (or (and (typep label 'symbol) (coerce label 'string))
			  label))
	       (entry (gethash key +accelerator-table+ nil)))
	  (or entry :unsupplied))
	;; else
	accelerator)))


#||
define sealed method add-gadget-label-postfix
    (gadget :: <gtk-gadget-mixin>, label :: <string>) => (label :: <string>)
  label
end method add-gadget-label-postfix;


define sealed method add-gadget-label-postfix
    (gadget :: <accelerator-mixin>, label :: <string>) => (label :: <string>)
  let framem  = frame-manager(gadget);
  let gesture = defaulted-gadget-accelerator(framem, gadget);
  if (gesture)
    let keysym = gesture-keysym(gesture);
    let (shift?, control?, alt?) = gesture-modifiers(gesture);
    concatenate-as(<string>, 
		   label,
		   "\t",
		   if (shift?)   "Shift+" else "" end,
		   if (control?) "Ctrl+"  else "" end,
		   if (alt?)     "Alt+"   else "" end,
		   keysym->key-name(keysym))
  else
    label
  end
end method add-gadget-label-postfix;


// Map keysyms to their labels on a typical keyboard
define table $keysym->key-name :: <object-table>
  = { #"return"     => "Enter",
      #"newline"    => "Shift+Enter",
      #"linefeed"   => "Line Feed",
      #"up"	    => "Up Arrow",
      #"down"	    => "Down Arrow",
      #"left"	    => "Left Arrow",
      #"right"	    => "Right Arrow",
      #"prior"	    => "Page Up",
      #"next"	    => "Page Down",
      #"lwin"	    => "Left Windows",
      #"rwin"	    => "Right Windows",
      #"numpad0"    => "Num 0",
      #"numpad1"    => "Num 1",
      #"numpad2"    => "Num 2",
      #"numpad3"    => "Num 3",
      #"numpad4"    => "Num 4",
      #"numpad5"    => "Num 5",
      #"numpad6"    => "Num 6",
      #"numpad7"    => "Num 7",
      #"numpad8"    => "Num 8",
      #"numpad9"    => "Num 9",
      #"num-lock"   => "Num Lock",
      #"caps-lock"  => "Caps Lock" };

define function keysym->key-name
    (keysym) => (name :: <string>)
  element($keysym->key-name, keysym, default: #f)
  | string-capitalize(as(<string>, keysym))
end function keysym->key-name;

/*---*** What should we do here?
define sealed method accelerator-table
    (sheet :: <top-level-sheet>) => (accelerators :: false-or(<HACCEL>))
  let mirror = sheet-direct-mirror(sheet);
  // Ensure that we don't build the accelerator table too early (i.e.,
  // before all of the resource ids have been created).  This isn't as bad
  // as it seems, since users won't have been able to use an accelerator
  // before the top-level sheet is mapped anyway...
  when (sheet-mapped?(sheet))
    mirror.%accelerator-table
    | (mirror.%accelerator-table := make-accelerator-table(sheet))
  end
end method accelerator-table;
||#

(defmethod accelerator-table ((sheet <top-level-sheet>) &key (create? t))
  (declare (ignore create?))
  (let ((mirror (sheet-direct-mirror sheet)))
    (or (%accelerator-table mirror)
	#-(and)
	(when create?
	  (setf (%accelerator-table mirror)
		(make-accelerator-table sheet))))))


#||
define sealed method accelerator-table
    (sheet :: <sheet>) => (accelerators :: false-or(<HACCEL>))
  let top-sheet = top-level-sheet(sheet);
  top-sheet & accelerator-table(top-sheet);
||#

(defmethod accelerator-table ((sheet <sheet>) &key (create? t))
  (let ((top-sheet (top-level-sheet sheet)))
    (and top-sheet (accelerator-table top-sheet :create? create?))))


#||
define method make-accelerator-table
    (sheet :: <top-level-sheet>) => (accelerators :: <HACCEL>)
  local method fill-accelerator-entry
	    (gadget :: <accelerator-mixin>, accelerator :: <accelerator>,
	     entry :: <LPACCEL>) => ()
	  let keysym    = gesture-keysym(accelerator);
	  let modifiers = gesture-modifier-state(accelerator);
	  let char      = gesture-character(accelerator);
	  let (vkey :: <integer>, fVirt :: <integer>)
	    = case
		char
		& zero?(logand(modifiers, logior($control-key, $meta-key)))
		& character->virtual-key(char) =>
		  values(character->virtual-key(char), 0);
		keysym->virtual-key(keysym) =>
		  values(keysym->virtual-key(keysym),
			 logior($ACCEL-FVIRTKEY,
				if (zero?(logand(modifiers, $shift-key)))   0 else $ACCEL-FSHIFT end,
				if (zero?(logand(modifiers, $control-key))) 0 else $ACCEL-FCONTROL end,
				if (zero?(logand(modifiers, $alt-key)))     0 else $ACCEL-FALT end));
		otherwise =>
		  error("Can't decode the gesture with keysym %=, modifiers #o%o",
			keysym, modifiers);
	      end;
	  let cmd :: <integer>
	    = sheet-resource-id(gadget) | gadget->id(gadget);
	  entry.fVirt-value := fVirt;
	  entry.key-value   := vkey;
	  entry.cmd-value   := cmd;
	end method;
  let accelerators   = frame-accelerators(sheet-frame(sheet));
  let n :: <integer> = size(accelerators);
  if (n > 0)
    with-stack-structure (entries :: <LPACCEL>, element-count: n)
      for (i :: <integer> from 0 below n)
	let entry  = accelerators[i];
	let gadget = entry[0];
	let accel  = entry[1];
	let entry  = pointer-value-address(entries, index: i);
	fill-accelerator-entry(gadget, accel, entry)
      end;
      check-result("CreateAcceleratorTable", CreateAcceleratorTable(entries, n))
    end
  else
    $null-HACCEL
  end
end method make-accelerator-table;
||#

;;; FIXME: Is this really doing the right thing? It looks *almost* right, to me.
;;; TODO: Work out what is going on with alt/altgr, and alt-is-meta? etc.

(defmethod make-accelerator-table ((sheet <top-level-sheet>))
  #-(and)
  (let ((accel-group (cffi/gtk-accel-group-new)))
    (labels ((fill-accelerator-entry (gadget accelerator)
	       (let* ((keysym    (duim-keysym->gdk-keyval (gesture-keysym accelerator)))
		      (modifiers (gesture-modifier-state accelerator))
;;;		      (char      (gesture-character accelerator))
		      (widget    (gadget-widget gadget))
		      (mask      (logior (if (zerop (logand modifiers +shift-key+))   0 +CFFI/GDK-SHIFT-MASK+)
					 (if (zerop (logand modifiers +control-key+)) 0 +CFFI/GDK-CONTROL-MASK+)
					 (if (zerop (logand modifiers +alt-key+))     0 +CFFI/GDK-MOD1-MASK+)
;; XXX: Altgr = left-ctrl + right-alt.
;; 					   (if (zerop (logand modifiers $altgr-key))   0 +GDK-ALTGR-MASK+)
					 (if (zerop (logand modifiers +meta-key+))    0 +CFFI/GDK-META-MASK+)
					 (if (zerop (logand modifiers +super-key+))   0 +CFFI/GDK-SUPER-MASK+)
					 (if (zerop (logand modifiers +hyper-key+))   0 +CFFI/GDK-HYPER-MASK+)))
		      (visible?  +CFFI/GTK-ACCEL-VISIBLE+))
		 ;; FIXME: We should use gtk_accel_map_add_entry, gtk_widget_set_accel_path,
		 ;; gtk_menu_item_set_accel_path instead so that accelerators are dynamic.
		 (cffi/gtk-widget-add-accelerator widget "activate" accel-group keysym mask visible?))))
      (let* ((accelerators (frame-accelerators (sheet-frame sheet)))
	     (n (size accelerators)))
	(when (> n 0)
	  (loop for i from 0 below n
	     do (let* ((entry       (aref accelerators i))
		       (gadget      (aref entry 0))
		       (accelerator (aref entry 1)))
		  (fill-accelerator-entry gadget accelerator))))
	accel-group))))
	    

#||
define sealed method destroy-accelerator-table
    (sheet :: <top-level-sheet>) => ()
  let accelerator-table = accelerator-table(sheet);
  when (accelerator-table & ~null-handle?(accelerator-table))
    DestroyAcceleratorTable(accelerator-table)
  end;
  let mirror = sheet-direct-mirror(sheet);
  mirror.%accelerator-table := #f
end method destroy-accelerator-table;
*/
||#

;;; FIXME: THIS NEEDS TO BE INVOKED! It should be called either when accelerators
;;; are rebuilt to destroy the old table, or when the window is shutting down to
;;; release resources.

(defmethod destroy-accelerator-table ((sheet <top-level-sheet>))
  ;; Grab the accel-table, but don't create one if it's nil.
  #-(and)
  (let ((accelerator-table (accelerator-table sheet :create? nil)))
    (when accelerator-table
      ;; FIXME: do we need to remove the accelerators from the widgets?
      (cffi/g-object-unref accelerator-table))
    (let ((mirror (sheet-direct-mirror sheet)))
      (setf (%accelerator-table mirror) nil))))


#||
define method note-accelerators-changed
    (framem :: <gtk-frame-manager>, frame :: <basic-frame>) => ()
  // Force the accelerators to be recomputed
  let top-sheet = top-level-sheet(frame);
  when (top-sheet)
    ignoring("note-accelerators-changed")
  end
end method note-accelerators-changed;
||#

;; Unfortunately this method is invoked *before* the new accelerators are
;; associated with the frame, and they aren't passed anywhere.
(defmethod note-accelerators-changed ((framem <gtk-frame-manager>) (frame <basic-frame>))
  (let ((top-sheet (top-level-sheet frame)))
    (when top-sheet
      (let ((new-table (make-accelerator-table top-sheet)))
	(setf (%accelerator-table (sheet-mirror top-sheet)) new-table)))))



#||
/// Dialog handling

define method mirror-registered-dialogs
    (mirror :: <top-level-mirror>) => (dialogs :: <sequence>)
  mirror.%dialog-mirrors
end method mirror-registered-dialogs;
||#

(defmethod mirror-registered-dialogs ((mirror <gtk-top-level-mirror>))
  (%dialog-mirrors mirror))

#||
define method register-dialog-mirror
    (frame :: <simple-frame>, dialog-mirror :: <dialog-mirror>) => ()
  let top-sheet = top-level-sheet(frame);
  when (top-sheet)
    let top-mirror = sheet-direct-mirror(top-sheet);
    add!(top-mirror.%dialog-mirrors, dialog-mirror)
  end
end method register-dialog-mirror;
||#

(defmethod register-dialog-mirror ((frame <simple-frame>)
				   (dialog-mirror <gtk-dialog-mirror>))
  (let ((top-sheet (top-level-sheet frame)))
    (when top-sheet
      (let ((top-mirror (sheet-direct-mirror top-sheet)))
	(add! (%dialog-mirrors top-mirror) dialog-mirror)))))

#||
define method unregister-dialog-mirror
    (frame :: <simple-frame>, dialog-mirror :: <dialog-mirror>) => ()
  let top-sheet = top-level-sheet(frame);
  when (top-sheet)
    let top-mirror = sheet-direct-mirror(top-sheet);
    remove!(top-mirror.%dialog-mirrors, dialog-mirror)
  end
end method unregister-dialog-mirror;
||#

(defmethod unregister-dialog-mirror ((frame <simple-frame>)
				     (dialog-mirror <gtk-dialog-mirror>))
  (let ((top-sheet (top-level-sheet frame)))
    (when top-sheet
      (let ((top-mirror (sheet-direct-mirror top-sheet)))
	(setf (%dialog-mirrors top-mirror)
	      (delete dialog-mirror (%dialog-mirrors top-mirror)))))))


#||
/// Top level sheets

define open abstract class <gtk-top-level-sheet-mixin>
    (<standard-repainting-mixin>,
     <permanent-medium-mixin>,
     <gtk-pane-mixin>)
end class <gtk-top-level-sheet-mixin>;
||#

(defclass <gtk-top-level-sheet-mixin>    ;; FIXME:ABSTRACT
    (<standard-repainting-mixin>
     <permanent-medium-mixin>
     <gtk-pane-mixin>)
  ())

#||
define sealed class <gtk-top-level-sheet>
    (<gtk-top-level-sheet-mixin>,
     <top-level-sheet>)
end class <gtk-top-level-sheet>;
||#

(defclass <gtk-top-level-sheet>
    (<gtk-top-level-sheet-mixin> <top-level-sheet>)
  ())

#||
define sealed method class-for-make-pane
    (framem :: <gtk-frame-manager>, class == <top-level-sheet>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<gtk-top-level-sheet>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <gtk-frame-manager>)
				(class (eql (find-class '<top-level-sheet>)))
				&key)
  (values (find-class '<gtk-top-level-sheet>) nil))

#||
// Like a top-level sheet, but for embedded apps such as OLE parts
define sealed class <gtk-embedded-top-level-sheet>
    (<gtk-top-level-sheet-mixin>,
     <embedded-top-level-sheet>)
end class <gtk-embedded-top-level-sheet>;
||#

;; Like a top-level sheet, but for embedded apps such as OLE parts
(defclass <gtk-embedded-top-level-sheet>
    (<gtk-top-level-sheet-mixin>
     <embedded-top-level-sheet>)
  ())

#||
define sealed method class-for-make-pane
    (framem :: <gtk-frame-manager>, class == <embedded-top-level-sheet>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<gtk-embedded-top-level-sheet>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <gtk-frame-manager>)
				(class (eql (find-class '<embedded-top-level-sheet>)))
				&key)
  (values (find-class '<gtk-emedded-top-level-sheet>) nil))

#||
define sealed method make-gtk-mirror
    (sheet :: <gtk-top-level-sheet-mixin>)
 => (mirror :: <top-level-mirror>)
  let frame = sheet-frame(sheet);
  make-top-level-mirror(sheet, frame)
end method make-gtk-mirror;
||#

(defmethod make-gtk-mirror ((sheet <gtk-top-level-sheet-mixin>))
  (let ((frame (sheet-frame sheet)))
    (make-top-level-mirror sheet frame)))

#||
define sealed method make-top-level-mirror
    (sheet :: <top-level-sheet>, frame :: <basic-frame>)
 => (mirror :: <top-level-mirror>)
  let widget = GTK-WINDOW(gtk-window-new($GTK-WINDOW-TOPLEVEL));
  make(<top-level-mirror>,
       widget: widget,
       sheet:  sheet)
end method make-top-level-mirror;
||#

(defmethod make-top-level-mirror ((sheet <top-level-sheet>)
				  (frame <basic-frame>))
  (let ((widget (cffi/gtk-window-new (cffi:foreign-enum-value 'GtkWindowType
							      :+CFFI/GTK-WINDOW-TOPLEVEL+))))
    (GTK-DEBUG "------------------------> MAKE-TOP-LEVEL-MIRROR: FRAME=~A, WIDGET=~A"
	       frame widget)
      
    (make-gtk-widget-mirror-instance (find-class '<gtk-top-level-mirror>)
				     :widget widget
				     :sheet  sheet)))

#||
define method update-mirror-attributes
    (sheet :: <top-level-sheet>, mirror :: <top-level-mirror>)
 => ()
  next-method();
  let frame = sheet-frame(sheet);
  let widget = mirror-widget(mirror);
  let modal? = frame-mode(frame) == #"modal";
  let title = frame-title(frame) | $default-window-title;
  with-c-string (c-string = title)
    gtk-window-set-title(widget, c-string)
  end;
  gtk-window-set-modal(widget, if (modal?) $true else $false end);
  gtk-container-set-border-width(GTK-CONTAINER(widget), $top-level-border);
end method update-mirror-attributes;
||#

(defmethod update-mirror-attributes ((sheet <gtk-top-level-sheet>)
				     (mirror <gtk-top-level-mirror>))
  ;; called from GTK-MIRROR.DO-MAKE-MIRROR
  (call-next-method)
  (duim-debug-message "gtk-top.UPDATE-MIRROR-ATTRIBUTES entered for <gtk-top-level-sheet> ~a with mirror ~a" sheet mirror)
  (let* ((frame (sheet-frame sheet))
	 (widget (mirror-widget mirror))
	 (modal? (eql (frame-mode frame) :modal))
	 (title (or (frame-title frame) *default-window-title*)))
    (cffi/gtk-window-set-title widget title)
    ;; According to the GTK+ FAQ, the correct way to do this is to
    ;; call "gtk_grab_add(window)", and then "gtk_grab_remove(window)"
    ;; when the window is destroyed. Certainly what we have below
    ;; appears not to work. FIXME
    (cffi/gtk-window-set-modal widget (if modal? +true+ +false+))
    ;; FIXME: only do this for dialogs?
    (when (and modal? (frame-owner frame))
      (FORMAT T "~%UPDATE-MIRROR-ATTRIBUTES got FRAME-OWNER for ~a of ~a" frame (frame-owner frame))
      ;; fixme: if dialogs have no top level sheet, trying to set this might explain
      ;;        some of the gtk messages we get out of ccl.
      (cffi/gtk-window-set-transient-for widget (mirror-widget (sheet-mirror (top-level-sheet (frame-owner frame)))))
      ;; FIXME: gtk-window-destroy-with-parent ?
      )
    (multiple-value-bind (width height)
	(sheet-size sheet)
      (cffi/gtk-window-set-default-size widget width height))
    ;; FIXME: Now that layout etc. is working, does this make any difference?
    (cffi/gtk-container-set-border-width widget +top-level-border+)))

#||
define method install-event-handlers
    (sheet :: <gtk-top-level-sheet-mixin>, mirror :: <top-level-mirror>) => ()
  next-method();
  install-named-handlers(mirror,
			 #[#"delete_event", #"configure_event"])
end method install-event-handlers;
||#

(defmethod install-event-handlers ((sheet <gtk-top-level-sheet-mixin>)
				   (mirror <gtk-top-level-mirror>))
  (install-named-handlers mirror #("delete-event"         ; 'X' clicked 
				   "configure-event"      ; frame resized
				   "set-focus"            ; focus change within frame
				   "focus-in-event"       ; frame gained focus
				   "button-press-event"   ; mouse button pressed
				   "button-release-event" ; mouse button released
				   "motion-notify-event"  ; mouse moved
				   )))



;;;
;;; Deal with focus changes within a frame
;;;
(defmethod handle-gtk-set-focus-signal ((sheet <sheet>)   ; sheet that's handling the event
					widget-receiver   ; widget that emitted the signal
					widget-target     ; widget that is the new focus
					user-data)
  ;; RUN-LAST
  (declare (ignore widget-receiver user-data))
  (let* ((target-mirror (and widget-target (widget-mirror widget-target)))
	 (target-sheet  (and target-mirror (mirror-sheet target-mirror)))
	 (frame         (and sheet (sheet-frame sheet)))
	 (port          (and frame (port frame)))
	 (old-focus     (and port  (port-input-focus port))))
    (if target-sheet
	(unless (eql target-sheet old-focus)
	  (let ((new-focus (if (sheet-accepts-focus? target-sheet)
			       target-sheet
			       (find-child-for-focus target-sheet))))
	    (setf (port-input-focus port) new-focus)))
	(setf (port-input-focus port) nil)))
  nil)


(defun find-child-for-focus (sheet)
  (declare (ignore sheet))
  ;; TODO: Find a sheet with a direct mirror that accepts focus
  (gtk-debug "ignoring \"FIND-CHILD-FOR-FOCUS\""))


(defmethod handle-gtk-focus-in-signal ((top-sheet <gtk-top-level-sheet-mixin>) widget event user-data)
  (declare (ignore widget event user-data))
  ;; RUN-LAST
  ;; Focus is already set up on the sheet, but the port focus will be
  ;; nil.
  (let* ((frame       (sheet-frame top-sheet))
	 (frame-focus (frame-input-focus frame))
	 (port        (port top-sheet)))
    (setf (port-input-focus port) frame-focus))
  nil)


#||
define sealed method map-mirror
    (_port :: <gtk-port>, sheet :: <gtk-top-level-sheet-mixin>,
     mirror :: <top-level-mirror>)
 => ()
  let widget = mirror-widget(mirror);
  gtk-widget-show(widget)
end method map-mirror;
||#

(defmethod map-mirror ((_port <gtk-port>)
		       (sheet <gtk-top-level-sheet-mixin>)
		       (mirror <gtk-top-level-mirror>))
  (let ((widget (mirror-widget mirror)))
    ;; XXX: This appears to work, but seems hacky (wrong place to set size)
    (multiple-value-bind (width height)
	(sheet-size sheet)
      (cffi/gtk-window-resize widget width height))
    ;; If DUIM has an idea of what the focus should be in this frame, make the
    ;; corresponding widget grab the focus - otherwise GTK will decide and provide
    ;; a 'focus-in' signal.
    ;; XXX: This appears to work, but seems hacky (wrong place to set focus)
    ;; It's a bit different to the win32 b/end which only fiddles with focus
    ;; here if there is no frame-input-focus.
    (let ((frame (sheet-frame sheet)))
      (when (frame-input-focus frame)
	(let* ((focus-sheet  (frame-input-focus frame))
	       (focus-mirror (sheet-mirror focus-sheet))
	       (focus-widget (and focus-mirror (mirror-widget focus-mirror))))
	  (and focus-widget (cffi/gtk-widget-grab-focus focus-widget))))
      ;; XXX: Is this the right thing to do about the default button?
      (let ((default (frame-default-button frame)))
	(gtk-debug "[GTK-TOP MAP-MIRROR] got default = ~a and frame-input-focus = ~a"
		   default (frame-input-focus frame))
	(when default
	  (let* ((default-widget (mirror-widget (sheet-mirror default))))
	    ;; Only <push-button>s and <push-menu-button>s attempt to grab default
	    (cffi/gtk-widget-set-can-default default-widget +true+)
	    (cffi/gtk-widget-grab-default default-widget)))))
    (cffi/gtk-window-present widget)))


#||
define sealed method unmap-mirror
    (_port :: <gtk-port>,
     sheet :: <gtk-top-level-sheet-mixin>, mirror :: <top-level-mirror>)
 => ()
  let widget = mirror-widget(mirror);
  gtk-widget-hide(widget)
end method unmap-mirror;
||#

(defmethod unmap-mirror ((_port <gtk-port>)
			 (sheet <gtk-top-level-sheet-mixin>)
			 (mirror <gtk-top-level-mirror>))
  (let ((widget (mirror-widget mirror)))
    (cffi/gtk-widget-hide widget)))


#||
define sealed method raise-mirror 
    (_port :: <gtk-port>, sheet :: <gtk-top-level-sheet-mixin>,
     mirror :: <top-level-mirror>,
     #key activate? :: <boolean> = #f)
 => ()
  ignoring("raise-mirror")
end method raise-mirror;
||#

(defmethod raise-mirror ((_port <gtk-port>)
			 (sheet <gtk-top-level-sheet-mixin>)
			 (mirror <gtk-top-level-mirror>)
			 &key (activate? nil))
  (declare (ignore activate?))
  (let ((window (mirror-widget mirror)))
    (when window
      (cffi/gtk-window-present window))))


#||
define sealed method lower-mirror
    (_port :: <gtk-port>, sheet :: <gtk-top-level-sheet-mixin>, 
     mirror :: <top-level-mirror>)
 => ()
  ignoring("lower-mirror")
end method lower-mirror;
||#

(defmethod lower-mirror ((_port <gtk-port>)
			 (sheet <gtk-top-level-sheet-mixin>)
			 (mirror <gtk-top-level-mirror>))
  (ignoring "lower-mirror"))

#||
define sealed method handle-gtk-delete-event
    (sheet :: <top-level-sheet>, widget :: <GtkWidget*>,
     event :: <GdkEventAny*>)
 => (handled? :: <boolean>)
  let frame  = sheet-frame(sheet);
  let controller = frame & frame-controlling-frame(frame);
  when (controller)
    debug-message("Exiting frame");
    exit-frame(frame, destroy?: #t)
  end;
  debug-message("Handled delete event");
  #t
end method handle-gtk-delete-event;
||#

(defmethod handle-gtk-delete-event-signal ((sheet <top-level-sheet>)
					   widget ;;<GtkWidget*>)
					   event  ;;<GdkEventAny*>))
					   user-data)
  (declare (ignore widget event user-data))
  (let ((frame (sheet-frame sheet)))
    (if (instance? frame (find-class '<dialog-frame>))
	(progn
	  ;; This is invoked if the dialog is canceled with the escape
	  ;; key
	  (gtk-debug "Canceling frame")
	  (cancel-frame frame))
	(let ((controller (and frame (frame-controlling-frame frame))))
	  (when controller
	    (gtk-debug "Exiting frame")
	    (exit-frame frame :destroy? t)))))
  (gtk-debug "Handled delete event")
  ;; The mirrors are destroyed when DUIM core invokes DESTROY-MIRROR;
  ;; no need for gtk to destroy them so stop propagation.
  t)


#||
define sealed method destroy-mirror 
    (_port :: <gtk-port>,
     sheet :: <gtk-top-level-sheet-mixin>, mirror :: <top-level-mirror>)
 => ()
  debug-message("destroy-mirror of %=", mirror);
  let widget = mirror-widget(mirror);
  gtk-widget-destroy(widget);
  next-method();
end method destroy-mirror;
||#

(defmethod destroy-mirror ((_port <gtk-port>)
			   (sheet <gtk-top-level-sheet>)
			   (mirror <gtk-top-level-mirror>))
  (destroy-accelerator-table sheet)
  (call-next-method)
  (let* ((frame (sheet-frame sheet))
	 (owned-frame? (frame-owner frame))
	 (processor-type (port-event-processor-type _port)))
    (when (and (not owned-frame?) (eql :N processor-type))
      ;; in an :N event processing port, once the owning frame is
      ;; gone there is nothing to process events; however GTK still
      ;; posts some to its main loop. These must be consumed so the
      ;; frame exits.
      (gtk-debug "Flushing GTK MainLoop")
      (loop while (eql (cffi/gtk-events-pending) +true+)
	    do (cffi/gtk-main-iteration)))))



#||
/// Top level layout

define class <top-level-layout> 
    (<mirrored-sheet-mixin>, 
     <single-child-wrapping-pane>)
end class <top-level-layout>;
||#

(defclass <gtk-top-level-layout>
    (<mirrored-sheet-mixin> <single-child-wrapping-pane>)
  ()
  (:documentation "Acts as the frame-wrapper for a GTK window"))


#||
define method frame-wrapper
    (framem :: <gtk-frame-manager>, 
     frame :: <simple-frame>,
     layout :: false-or(<sheet>))
 => (wrapper :: false-or(<top-level-layout>))
  with-frame-manager (framem)
    make(<top-level-layout>,
	 child: top-level-layout-child(framem, frame, layout))
  end
end method frame-wrapper;
||#

(defmethod frame-wrapper ((framem <gtk-frame-manager>)
			  (frame <simple-frame>)
			  (layout null))
  (with-frame-manager (framem)
    (let ((child (top-level-layout-child framem frame layout)))
      (make-pane '<gtk-top-level-layout> :child child))))


(defmethod frame-wrapper ((framem <gtk-frame-manager>)
			  (frame <simple-frame>)
			  (layout <sheet>))
  (with-frame-manager (framem)
    (let ((child (top-level-layout-child framem frame layout)))
      (make-pane '<gtk-top-level-layout> :child child))))

#||
define method top-level-layout-child
    (framem :: <gtk-frame-manager>, 
     frame :: <simple-frame>,
     layout :: false-or(<sheet>))
 => (layout :: false-or(<column-layout>))
  let menu-bar      = frame-menu-bar(frame);
  let tool-bar   = frame-tool-bar(frame);
  let status-bar = frame-status-bar(frame);
  with-frame-manager (framem)
    let indented-children
      = make-children(tool-bar & tool-bar-decoration(tool-bar), layout);
    let indented-children-layout
      = unless (empty?(indented-children))
	  with-spacing (spacing: 2)
	    make(<column-layout>,
		 children: indented-children,
		 y-spacing: $top-level-y-spacing)
          end
        end;
    make(<column-layout>,
	 children: make-children(menu-bar, indented-children-layout, status-bar),
	 y-spacing: $top-level-y-spacing)
  end
end method top-level-layout-child;
||#

(defun %reparent-layout-children (layout)
"
Recursively verifies that all children of layout (and the children's
children etc.)
have the correct parentage.
"
  ;; FIXME: Use "do-sheet-tree" for this?
;;  (duim-debug-message "gtk-top.%REPARENT-LAYOUT-CHILDREN -- FIXME [look for a better fix]")
  (labels ((check-kids (parent)
	     (map nil #'(lambda (child)
			  (unless (eql (sheet-parent child) parent)
;;			    (duim-debug-message "    __________ child ~a has wrong parent! Expected ~a, got ~a"
;;						child parent (sheet-parent child))
			    ;; THIS DOES INDEED FIX THE ISSUE. BUT IS IT A GOOD FIX? MAYBE
			    ;; THE FIX IS AS BAD AS THE PROBLEM WAS IN THE FIRST PLACE...
			    ;; SEE IF FIXING THIS THE "EXTERIOR" WAY WORKS.
			    ;; NOPE, GET "The single-child sheet #<<BORDER-PANE> {BDD2859}> already has a child"
			    ;; ERROR REPORTED. THAT COULD BE HACKED SINCE THAT BORDER PANE'S CHILD IS IN FACT
			    ;; THE CHILD WE WANT TO REPARENT, SO AN EQUALITY TEST IN (SETF (SHEET-CHILDREN ...))
			    ;; WOULD SORT THAT OUT. BUT IS THAT REALLY WHAT WE WANT TO DO? BAH, I'M NOT SURE.
			    ;; PERHAPS I'M MISSING A MORE FUNDAMENTAL ISSUE IN THE TRANSLITERATED CODE THAT
			    ;; ALLOWS THIS, BUT IF SO I (OBVIOUSLY) CAN'T SEE IT. PERHAPS THERE IS AN ERROR IN
			    ;; THE "DEFINE-FRAME" MACRO OR SOMEPLACE THAT I'VE MISSED THAT WOULD ENABLE ALL
			    ;; THIS TO "JUST WORK". FIX, OR NOT FIX? THAT IS THE QUESTION.
			    #-(and)
			    (setf (sheet-parent child) parent)
			    (DUIM-SHEETS-INTERNALS::%PARENT-SETTER PARENT CHILD))
			  (check-kids child))
		  (sheet-children parent))))
    (check-kids layout)))


(defmethod top-level-layout-child ((framem <gtk-frame-manager>)
                                   (frame <simple-frame>)
                                   layout)   ;; false-or(<sheet>))
  ;; "layout" is the layout of the frame (from (frame-layout frame)).
  (let ((menu-bar (frame-menu-bar frame))
        (tool-bar (frame-tool-bar frame))
        (status-bar (frame-status-bar frame)))
    (with-frame-manager (framem)
      ;; FIXME: NOT ALL CHILDREN ARE VALID. I WONDER IF THIS IS WHY THE WIN32 EQUIVALENT OF THIS DOES AN
      ;; "UPDATE-SHEET-CHILDREN" CALL -- IT *PROBABLY* IS, SO WE WANT TO DO SOMETHING SIMILAR. THE ROOT
      ;; CAUSE OF ALL THIS, I THINK, IS THAT THE SAME PANES ARE USED IN DIFFERENT LAYOUTS IN THE FRAME
      ;; DEFINITION, BUT PANES CAN'T HAVE MULTIPLE PARENTS -- SO ONCE THE LAYOUT IS CHANGED THE SHEETS
      ;; LOWER DOWN IN THE HIERARCHY ARE POINTING UP TO PARENTS THAT ARE IN A DIFFERENT LAYOUT! WHAT A
      ;; PAIN.
      ;; AT LEAST *I THINK* THIS CAN BE FIXED BY SUCKING OUT MORE OF THE CODE FROM WTOP.DYLAN ;) THE OTHER
      ;; ALTERNATIVE IS TO JUST USE THIS "validate-layout-children" CALL TO FIX THE PARENTAGE OF THE
      ;; PANES, BUT THAT MIGHT NOT WORK EITHER (OR MIGHT WORK JUST THE FIRST TIME, OR SOMETHING).
      ;; CAN ALWAYS DO THE REPARENTING THE "INTERNAL" WAY SO AS NOT TO KICK OFF ANY OF THE NOTE-SHEET-ADDED
      ;; OR NOTE-SHEET-REMOVED CALLS. I'M WORRIED THAT MIGHT LEAD TO SUBTLE BUGS IN THE FUTURE. HAVE A
      ;; CAREFUL THINK ABOUT THIS (WHILST LOOKING AT THE MACROEXPANSION OF THE <MULTIPLE-LAYOUT-FRAME>
      ;; DEFINITION!)
      (WHEN LAYOUT
	(%REPARENT-LAYOUT-CHILDREN LAYOUT))
      (let* ((indented-children (make-children (and tool-bar (tool-bar-decoration tool-bar)) layout))
             (indented-children-layout (unless (empty? indented-children)
                                         (with-spacing (:spacing 2)
                                           (make-pane '<column-layout>
                                                      :children indented-children
                                                      :y-spacing +top-level-y-spacing+)))))
          (make-pane '<column-layout>
                     :children (make-children menu-bar indented-children-layout status-bar)
                     :y-spacing +top-level-y-spacing+)))))


#||
define function make-children
    (#rest maybe-children)
 => (children :: <sequence>)
  let children :: <stretchy-object-vector> = make(<stretchy-vector>);
  for (child in maybe-children)
    when (child)
      add!(children, child)
    end
  end;
  children
end function make-children;
||#

(defun make-children (&rest maybe-children)
  (let ((children (make-array 0 :adjustable t :fill-pointer t)))
    (loop for child in maybe-children
	  do (when child
	       (setf children (add! children child))))
    children))

#||
define method update-frame-layout
    (framem :: <gtk-frame-manager>, frame :: <simple-frame>) => ()
  let top-sheet = top-level-sheet(frame);
  let wrapper = sheet-child(top-sheet);
  let layout = frame-layout(frame);
  let new-child = top-level-layout-child(framem, frame, layout);
  sheet-child(wrapper) := new-child;
  relayout-parent(new-child)
end method update-frame-layout;
||#

;;; This is the 'update-frame-layout' method from win32...

(defmethod update-frame-layout ((framem <gtk-frame-manager>) (frame <simple-frame>))
  (update-frame-wrapper framem frame))


#||
define sealed method update-frame-wrapper
    (framem :: <gtk-frame-manager>, frame :: <simple-frame>) => ()
  let top-sheet = top-level-sheet(frame);
  if (top-sheet)
    let wrapper = sheet-child(top-sheet);
    let layout = frame-layout(frame);
    let new-child = top-level-layout-child(framem, frame, layout);
    sheet-child(wrapper) := new-child;
    relayout-parent(new-child)
  end
end method update-frame-wrapper;
||#

(defmethod update-frame-wrapper ((framem <gtk-frame-manager>) (frame <simple-frame>))
  (let ((top-sheet (top-level-sheet frame)))
    (when top-sheet
      (let* ((wrapper   (sheet-child top-sheet))
	     (layout    (frame-layout frame))
	     (new-child (top-level-layout-child framem frame layout)))
	;; FIXME: Does the 'toolbar' also need fixing up, or does that bit 'just work'?
	(setf (sheet-child wrapper) new-child)
	(if (sheet-mapped? wrapper)
	    (setf (sheet-mapped? new-child :clear? t :do-repaint? t) t)
	    #-(and)
	    (duim-debug-message "gtk-top.UPDATE-FRAME-WRAPPER updating unmapped top-sheet (shows later)"))
	;; TODO: I'm not sure if the menus need to be mapped or just created in order to get a sensible
	;; size out of them; pretty sure we just need to have the mirrors created which doesn't happen
	;; until we try to map them (I think!). ACTUALLY, MIRRORS ARE MADE WHEN SHEETS ARE ATTACHED TO A
        ;; MIRRORED SHEET (= grafted?). CHECK IF THE SIZE STUFF WORKS AT THIS POINT.
	;; If the layout is done before the sheets are mapped, they're not laid out properly. If done
	;; after, they are.
	;; We need a better fix for this though. If the sheet is already laid out to the correct size we
	;; never try to set the size of the mirror, even though the mirror isn't laid out to the same size.
	;; We might want to add a "previously-mirrored?" flag on the menu-bar sheet type and force the
	;; right width onto the menu-bar mirror if its set irrespective of the size of the sheet or
	;; something.
        ;; FIXME: DO WE CARE THAT THE NEW-CHILD HAS ALREADY BEEN MAPPED? IT SEEMS WASTEFUL TO DO THIS HERE
        ;; (AND IT MIGHT CAUSE FLICKERING). TRY RESIZING THE MULTIPLE-LAYOUT-FRAME BETWEEN SWITCHING LAYOUTS
        ;; AND SEE WHAT HAPPENS... IT SEEMS TO MAKE NO DIFFERENCE WHATSOEVER. DOES THIS CALL EVEN DO ANYTHING?
	(relayout-parent new-child)))))


#||
/// Geometry updating

define sealed method handle-move
    (sheet :: <top-level-sheet>, mirror :: <top-level-mirror>,
     x :: <integer>, y :: <integer>)
 => (handled? :: <boolean>)
  let (old-x, old-y) = sheet-position(sheet);
  unless (x = old-x & y = old-y)
    let frame = sheet-frame(sheet);
    duim-debug-message("Sheet %= moved to %=, %= (from %=, %=)",
		       sheet, x, y, old-x, old-y);
    set-sheet-position(sheet, x, y)
  end;
  #t
end method handle-move;
||#

;;; FIXME: who calls this? Nobody, apparently.

(defmethod handle-move ((sheet <gtk-top-level-sheet>)
			(mirror <gtk-top-level-mirror>)
			(x integer)
			(y integer))
  (gtk-debug "gtk-top.HANDLE-MOVE entered for sheet ~a" sheet)
  (multiple-value-bind (old-x old-y)
      (sheet-position sheet)
    (unless (and (= x old-x) (= y old-y))
      (let ((frame (sheet-frame sheet)))
	(declare (ignore frame))
;;	(gtk-debug "Sheet ~a moved to ~d, ~d (from ~d, ~d)"
;;			    sheet x y old-x old-y)
	(set-sheet-position sheet x y))))
  t)

#||
define sealed method handle-gtk-configure-event
    (sheet :: <top-level-sheet>, widget :: <GtkWidget*>,
     event :: <GdkEventConfigure*>)
 => (handled? :: <boolean>)
  let frame  = sheet-frame(sheet);
  let left   = event.x-value;
  let top    = event.y-value;
  let width  = event.width-value;
  let height = event.height-value;
  let region = make-bounding-box(left, top, left + width, top + height);
  let (old-width, old-height) = box-size(sheet-region(sheet));
  //---*** Switch back to duim-debug-message
  debug-message("Resizing %= to %dx%d -- was %dx%d",
		sheet, width, height, old-width, old-height);
  distribute-event(port(sheet),
		   make(<window-configuration-event>,
			sheet: sheet,
			region: region));
  #t
end method handle-gtk-configure-event;
||#

(defmethod handle-gtk-configure-event-signal ((sheet <gtk-top-level-sheet>)
					      widget ;;<GtkWidget*>)
					      event  ;;<GdkEventConfigure*>))
					      user-data)
  (declare (ignore widget user-data))
  (let* ((frame (sheet-frame sheet))
	 (left  (cffi/gdkeventconfigure->x event))
	 (top   (cffi/gdkeventconfigure->y event))
	 (width (cffi/gdkeventconfigure->width event))
	 (height (cffi/gdkeventconfigure->height event))
	 (region (make-bounding-box left top (+ left width) (+ top height))))
    (declare (ignore frame))
    (multiple-value-bind (old-width old-height)
	(box-size (sheet-region sheet))
      (declare (ignore old-width old-height))
;;      (gtk-debug "  - Resizing ~a to ~dx~d -- was ~dx~d"
;;			  sheet width height old-width old-height)
      (distribute-event (port sheet)
			(make-instance '<window-configuration-event>
				       :sheet sheet
				       :region region))))
  nil)

