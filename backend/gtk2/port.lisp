;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GTK2-DUIM -*-
(in-package #:gtk2-duim)

#||
Module:       gtk-duim
Synopsis:     GTK port implementation
Author:       Andy Armstrong, Scott McKay
Copyright:    Original Code is Copyright (c) 1995-2004 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND

/// Some magic GTK constants

define constant $caret-width :: <integer> = 2;
||#

(defconstant +caret-width+ 2)


#||
/// GTK ports

define sealed class <gtk-port> (<basic-port>)
  sealed slot %app-context = #f;
  sealed slot %app-shell   = #f;
  sealed slot %modifier-map :: <simple-object-vector> = #[];
  // Cache for image cursors
  sealed slot %cursor-cache :: <object-table> = make(<object-table>);
  keyword focus-policy: = #"sheet-under-pointer";
end class <gtk-port>;
||#

(defclass <gtk-port> (<basic-port>)
  ((%modifier-map :type array
		  :initform (make-array 0 :adjustable t :fill-pointer t)
		  :accessor %modifier-map)
   ;; Default pango references
   (%default-pango-text-description :initform nil :accessor %default-pango-text-description)
   (%default-pango-context :initform nil :accessor %default-pango-context)
   (%default-pango-layout  :initform nil :accessor %default-pango-layout)
   ;; Cache for image cursors
   (%cursor-cache :type hash-table
		  :initform (make-hash-table :test #'eql)
		  :accessor %cursor-cache))
  (:default-initargs :focus-policy :click-to-select))


#||
define sealed method initialize
    (_port :: <gtk-port>, #key server-path) => ()
  next-method();
  initialize-gtk();
  /*---*** What to do here?
  let type    = head(server-path);
  let display = get-property(tail(server-path), #"display",
			     default: environment-variable("DISPLAY"));
  ignore(type);
  let (shell, context, unused-args)
    = construct-application("DUIM port",	// class name -- defines resources
			    display-name: display,
			    app-context-name: format-to-string("DUIM port on %s", display),
			    fallback-resources: $primitive-resources);
  ignore(unused-args);
  _port.%display      := xt/XtDisplay(shell);
  _port.%app-shell    := shell;
  _port.%app-context  := context;
  _port.%modifier-map := initialize-modifier-map(_port.%display);
  install-default-palette(_port);
  install-default-text-style-mappings(_port);
  */
end method initialize;
||#

(defmethod initialize-instance :after ((_port <gtk-port>)
				       &key
				       server-path
				       &allow-other-keys)
  (declare (ignore server-path))
  ;; Safe without gdk lock
  (cffi/initialize-gtk)
  (install-default-palette _port)
  #-(and)
  (install-default-text-style-mappings _port))


#||
register-port-class(#"gtk", <gtk-port>, default?: #t);
||#

(register-port-class :gtk (find-class '<gtk-port>) :default? t)


#||
define sideways method class-for-make-port
    (type == #"gtk", #rest initargs, #key)
 => (class :: <class>, initargs :: false-or(<sequence>))
  values(<gtk-port>, concatenate(initargs, #(event-processor-type:, #"n")))
end method class-for-make-port;
||#

(defmethod class-for-make-port ((type (eql :gtk)) &rest initargs &key)
  (values (find-class '<gtk-port>)
	  #-(and)
	  (append initargs (list :event-processor-type :n+1))
	  (append initargs (list :event-processor-type :n))))


#||
define sealed method port-type
    (_port :: <gtk-port>) => (type :: <symbol>)
  #"gtk"
end method port-type;
||#

(defmethod port-type ((_port <gtk-port>))
  :gtk)


#||
define sealed method port-name
    (_port :: <gtk-port>) => (name :: false-or(<string>))
  "No Port Name"
end method port-name;
||#

(defmethod port-name ((_port <gtk-port>))
  "GTK DUIM Port")


#||
define sealed method destroy-port
    (_port :: <gtk-port>) => ()
  next-method();
  // release-default-text-style-mappings(_port);
  ignoring("destroy-port")
end method destroy-port;
||#

(defmethod destroy-port :after ((_port <gtk-port>))

  (GTK-DEBUG "~%DESTROY-PORT :AFTER ~A" _PORT)
  
  (release-default-text-style-mappings _port))


(defun release-default-text-style-mappings (_port)
  (declare (ignore _port))
  ;; TODO 2017-11-12: free these things
  #-(and) (%default-pango-text-description :initform nil :accessor %default-pango-text-description)
  #-(and) (%default-pango-context :initform nil :accessor %default-pango-context)
  #-(and) (%default-pango-layout  :initform nil :accessor %default-pango-layout)
  ;; Cache for image cursors
  #-(and) (%cursor-cache :type hash-table
		 :initform (make-hash-table :test #'eql)
		 :accessor %cursor-cache))


#||
define function shutdown-gtk-duim ()
  let ports :: <stretchy-object-vector> = make(<stretchy-vector>);
  do-ports(method (_port)
	     when (instance?(_port, <gtk-port>))
	       add!(ports, _port)
	     end
	   end method);
  do(destroy-port, ports)
end function shutdown-gtk-duim;
||#

(defun shutdown-gtk-duim ()
  ;; XXX: 2017-11-12 - Nothing calls this - maybe the port should be
  ;; shut down when the last frame is exited? Maybe the port should
  ;; never be shut down?
  (gtk-debug "~%**** [GTK-PORT SHUTDOWN-GTK-DUIM] shutting down")
  (let ((ports (make-array 0 :adjustable t :fill-pointer t)))
    (do-ports #'(lambda (_port)
		  (when (instance? _port '<gtk-port>)
		    (add! ports _port))))
    (gtk-debug "~%    got gtk ports to destroy: ~a" ports)
    (map nil #'destroy-port ports)))



#||
/// Beeping, etc

define sealed method beep
    (_port :: <gtk-port>) => ()
  gdk-beep()
end method beep;
||#

(defmethod beep ((_port <gtk-port>))
  (cffi/gdk-beep))



#||
/// Pointer position hacking

define sealed method do-pointer-position
    (_port :: <gtk-port>, pointer :: <pointer>, sheet :: <sheet>)
 => (x :: <integer>, y :: <integer>)
  ignoring("do-pointer-position");
  values(0, 0)
end method do-pointer-position;
||#

(defmethod do-pointer-position ((_port <gtk-port>)
				(pointer <pointer>)
				(sheet <sheet>))
  (let ((widget (mirror-widget (sheet-mirror sheet))))
    (if widget
	(progn
	  ;; TODO: 2017-11-12 - finish this
	  #-(and)
	  (cffi/gtk-widget-get-pointer widget x-ptr y-ptr)
	  (ignoring "do-pointer-position"))
	(values 0 0))))


#||
define sealed method do-pointer-position
    (_port :: <gtk-port>, pointer :: <pointer>, sheet :: <display>)
 => (x :: <integer>, y :: <integer>)
  ignoring("do-pointer-position");
  values(0, 0)
end method do-pointer-position;
||#

(defmethod do-pointer-position ((_port <gtk-port>)
				(pointer <pointer>)
				(sheet <display>))
  #-(and)
  (cffi/gdk-display-get-pointer display screen x y mask)
  (ignoring "do-pointer-position")
  (values 0 0))


#||
define sealed method do-set-pointer-position
    (_port :: <gtk-port>, pointer :: <pointer>, sheet :: <sheet>, 
     x :: <integer>, y :: <integer>)
 => ()
  ignoring("do-set-pointer-position")
end method do-set-pointer-position;
||#

(defmethod do-set-pointer-position ((_port <gtk-port>)
				    (pointer <pointer>)
				    (sheet <sheet>)
				    (x integer)
				    (y integer))
  (ignoring "do-set-pointer-position"))


#||
define sealed method do-set-pointer-position
    (_port :: <gtk-port>, pointer :: <pointer>, sheet :: <display>, 
     x :: <integer>, y :: <integer>) => ()
  ignoring("do-set-pointer-position")
end method do-set-pointer-position;
||#

(defmethod do-set-pointer-position ((_port <gtk-port>)
				    (pointer <pointer>)
				    (sheet <display>)
				    (x integer)
				    (y integer))
  #-(and)
  (cffi/gdk-display-warp-pointer display screen x y)
  (ignoring "do-set-pointer-position"))


#||
/// Pointer cursor hacking

/*---*** Need a GTK version of this...
define table $cursor-table :: <table>
  = { #"default"           => x/$XC-TOP-LEFT-ARROW,
      #"busy"              => x/$XC-WATCH,
      #"vertical-scroll"   => x/$XC-SB-V-DOUBLE-ARROW,
      #"horizontal-scroll" => x/$XC-SB-H-DOUBLE-ARROW,
      #"scroll-up"         => x/$XC-SB-UP-ARROW,
      #"scroll-down"       => x/$XC-SB-DOWN-ARROW,
      #"scroll-left"       => x/$XC-SB-LEFT-ARROW,
      #"scroll-right"      => x/$XC-SB-RIGHT-ARROW,
      #"upper-left"        => x/$XC-TOP-LEFT-CORNER,
      #"upper-right"       => x/$XC-TOP-RIGHT-CORNER,
      #"lower-left"        => x/$XC-BOTTOM-LEFT-CORNER,
      #"lower-right"       => x/$XC-BOTTOM-RIGHT-CORNER,
      #"vertical-thumb"    => x/$XC-SB-RIGHT-ARROW,
      #"horizontal-thumb"  => x/$XC-SB-UP-ARROW,
      #"button"            => x/$XC-TOP-LEFT-ARROW,
      #"prompt"            => x/$XC-QUESTION-ARROW,
      #"move"              => x/$XC-FLEUR,
      #"position"          => x/$XC-CROSSHAIR,
      #"i-beam"            => x/$XC-SB-UP-ARROW,
      #"cross"             => x/$XC-CROSSHAIR,
      #"starting"          => x/$XC-CLOCK,
      #"hand"              => x/$XC-I-BEAM };
*/
||#

(defparameter *cursor-table* (make-hash-table :test #'eql))

;;; Table contains both the GTK (symbolic) names from GTK docs, and
;;; the ones from the table above (guess: those are defined for DUIM)

(setf (gethash :none *cursor-table*) "none")
(setf (gethash :default *cursor-table*) "default")
(setf (gethash :help *cursor-table*) "help")
(setf (gethash :pointer *cursor-table*) "pointer")
(setf (gethash :context-menu *cursor-table*) "context-menu")
(setf (gethash :progress *cursor-table*) "progress")
(setf (gethash :wait *cursor-table*) "wait")
(setf (gethash :cell *cursor-table*) "cell")
(setf (gethash :crosshair *cursor-table*) "crosshair")
(setf (gethash :text *cursor-table*) "text")
(setf (gethash :vertical-text *cursor-table*) "vertical-text")
(setf (gethash :alias *cursor-table*) "alias")
(setf (gethash :copy *cursor-table*) "copy")
(setf (gethash :no-drop *cursor-table*) "no-drop")
(setf (gethash :move *cursor-table*) "move")
(setf (gethash :not-allowed *cursor-table*) "not-allowed")
(setf (gethash :grab *cursor-table*) "grab")
(setf (gethash :grabbing *cursor-table*) "grabbing")
(setf (gethash :all-scroll *cursor-table*) "all-scroll")
(setf (gethash :col-resize *cursor-table*) "col-resize")
(setf (gethash :row-resize *cursor-table*) "row-resize")
(setf (gethash :n-resize *cursor-table*) "n-resize")
(setf (gethash :e-resize *cursor-table*) "e-resize")
(setf (gethash :s-resize *cursor-table*) "s-resize")
(setf (gethash :w-resize *cursor-table*) "w-resize")
(setf (gethash :ne-resize *cursor-table*) "ne-resize")
(setf (gethash :nw-resize *cursor-table*) "nw-resize")
(setf (gethash :sw-resize *cursor-table*) "sw-resize")
(setf (gethash :se-resize *cursor-table*) "se-resize")
(setf (gethash :ew-resize *cursor-table*) "ew-resize")
(setf (gethash :ns-resize *cursor-table*) "ns-resize")
(setf (gethash :nesw-resize *cursor-table*) "nesw-resize")
(setf (gethash :nwse-resize *cursor-table*) "nwse-resize")
(setf (gethash :zoom-in *cursor-table*) "zoom-in")
(setf (gethash :zoom-out *cursor-table*) "zoom-out")

;;; DUIM names to GTK cursor names
(setf (gethash :busy *cursor-table*) "wait") ; d
(setf (gethash :vertical-scroll *cursor-table*) "ns-resize") ; d
(setf (gethash :horizontal-scroll *cursor-table*) "ew-resize") ; d
;;; #"scroll-up"         => x/$XC-SB-UP-ARROW,
;;; #"scroll-down"       => x/$XC-SB-DOWN-ARROW,
;;; #"scroll-left"       => x/$XC-SB-LEFT-ARROW,
;;; #"scroll-right"      => x/$XC-SB-RIGHT-ARROW,
(setf (gethash :upper-left *cursor-table*) "nw-resize") ; d
(setf (gethash :upper-right *cursor-table*) "ne-resize") ; d
(setf (gethash :lower-left *cursor-table*) "sw-resize") ; d
(setf (gethash :lower-right *cursor-table*) "se-resize") ; d
;;; #"vertical-thumb"    => x/$XC-SB-RIGHT-ARROW,
;;; #"horizontal-thumb"  => x/$XC-SB-UP-ARROW,
(setf (gethash :button *cursor-table*) "default") ; d
(setf (gethash :prompt *cursor-table*) "help") ; d
(setf (gethash :move *cursor-table*) "move") ; d
(setf (gethash :position *cursor-table*) "crosshair") ; d
(setf (gethash :i-beam *cursor-table*) "text") ; d
(setf (gethash :cross *cursor-table*) "crosshair") ; d
(setf (gethash :starting *cursor-table*) "wait") ; d
(setf (gethash :hand *cursor-table*) "grab") ; d

#||
define sealed method do-set-pointer-cursor
    (_port :: <gtk-port>, pointer :: <pointer>, cursor :: <cursor>) => ()
  ignoring("do-set-pointer-cursor")
end method do-set-pointer-cursor;
||#

(defmethod do-set-pointer-cursor ((_port <gtk-port>) (pointer <pointer>) (cursor symbol))
  #-(and)
  (let* ((c-name (or (gethash *cursor-table* cursor) "default"))
	 (GdkCursor* (cffi/gdk-cursor-new-from-name display c-name))))
  (ignoring "do-set-pointer-cursor"))


(defmethod do-set-pointer-cursor ((_port <gtk-port>) (pointer <pointer>) (cursor <image>))
  #-(and)
  (let* ((pixbuf (decode-image cursor))
	 (GdkCursor* (cffi/gdk-cursor-new-from-pixbuf display pixbuf x y))))
  (ignoring "do-set-pointer-cursor"))


#||
define sealed method do-set-sheet-cursor
    (_port :: <gtk-port>, sheet :: <sheet>, cursor :: <cursor>) => ()
  ignoring("do-set-sheet-cursor")
end method do-set-sheet-cursor;
||#

(defmethod do-set-sheet-cursor ((_port <gtk-port>) (sheet <sheet>) (cursor symbol))
  (ignoring "do-set-sheet-cursor"))


(defmethod do-set-sheet-cursor ((_port <gtk-port>) (sheet <sheet>) (cursor <image>))
  (ignoring "do-set-sheet-cursor"))


#||
define method grab-pointer
    (_port :: <gtk-port>, pointer :: <pointer>, sheet :: <sheet>)
 => (success? :: <boolean>)
  let mirror = sheet-mirror(sheet);
  let widget = mirror & mirror-widget(mirror);
  let result :: <integer> = 0;
  when (widget)
    //---*** Get real current time...
    let current-time = 0;
    result
      := gdk-pointer-grab(widget,
			  0,		// owner events
			  logior($GDK-POINTER-MOTION-MASK,
				 $GDK-BUTTON-PRESS-MASK,
				 $GDK-BUTTON-RELEASE-MASK),
			  null-pointer(<GdkWindow*>),		// confine to
			  null-pointer(<GdkCursor*>),		// cursor
			  current-time);
  end;
  result ~= 0
end method grab-pointer;
||#

(defmethod grab-pointer ((_port <gtk-port>)
			 (pointer <pointer>)
			 (sheet <sheet>))
  (let* ((mirror (sheet-mirror sheet))
	 (widget (and mirror (mirror-widget mirror)))
	 (result 0))
    (when widget
      (let ((current-time (cffi/gtk-get-current-event-time)))
	(setf result (cffi/gdk-pointer-grab widget
					    0          ; owner events
					    (LOGIOR +CFFI/GDK-POINTER-MOTION-MASK+
						    +CFFI/GDK-BUTTON-PRESS-MASK+
						    +CFFI/GDK-BUTTON-RELEASE-MASK+)
					    nil ; confine to cursor
					    nil
					    current-time))))
    (/= result 0)))


#||
define method ungrab-pointer
    (_port :: <gtk-port>, pointer :: <pointer>)
 => (success? :: <boolean>)
  let sheet  = pointer-grabbed?(pointer);
  let mirror = sheet-mirror(sheet);
  let widget = mirror & mirror-widget(mirror);
  let result = #f;
  if (widget)
    //---*** How do we get the current time?
    let current-time = 0;
    gdk-pointer-ungrab(current-time);
    #t
  end
end method ungrab-pointer;
||#

(defmethod ungrab-pointer ((_port <gtk-port>)
			   (pointer <pointer>))
  (let* ((sheet (pointer-grabbed? pointer))
	 (mirror (sheet-mirror sheet))
	 (widget (and mirror (mirror-widget mirror))))
    (if widget
	(let ((current-time (cffi/gtk-get-current-event-time)))
	  (cffi/gdk-pointer-ungrab current-time)
	  t)
	nil)))


#||
define sealed method realize-cursor
    (_port :: <gtk-port>, cursor :: <symbol>) => (gtk-cursor)
  ignoring("realize-cursor")
end method realize-cursor;


define sealed method realize-cursor
    (_port :: <gtk-port>, cursor :: <integer>) => (gtk-cursor)
  gethash(_port.%cursor-cache, cursor)
  | begin
      ignoring("realize-cursor")
    end
end method realize-cursor;
||#


#||
/// Focus and carets

define sealed class <gtk-caret> (<basic-caret>)
end class <gtk-caret>;
||#

(defclass <gtk-caret> (<basic-caret>) ())


#||
define sealed method make-caret
    (_port :: <gtk-port>, sheet :: <sheet>, #key x, y, width, height)
 => (caret :: <gtk-caret>)
  make(<gtk-caret>,
       port: _port, sheet: sheet,
       x: x | 0, y: y | 0,
       width:  width  | $caret-width,
       height: height | (sheet-line-height(sheet) + sheet-line-spacing(sheet)))
end method make-caret;
||#

(defmethod make-caret ((_port <gtk-port>)
		       (sheet <sheet>)
		       &key x y width height)
  (make-instance '<gtk-caret>
		 :port _port
		 :sheet sheet
		 :x (or x 0)
		 :y (or y 0)
		 :width (or width +caret-width+)
		 :height (or height (+ (sheet-line-height sheet)
				       (sheet-line-spacing sheet)))))


#||
define sealed method do-set-caret-position
    (caret :: <gtk-caret>, x :: <integer>, y :: <integer>) => ()
  let transform = sheet-device-transform(caret-sheet(caret));
  with-device-coordinates (transform, x, y)
    ignoring("do-set-caret-position")
  end
end method do-set-caret-position;
||#

(defmethod do-set-caret-position ((caret <gtk-caret>) (x integer) (y integer))
  (let ((transform (sheet-device-transform (caret-sheet caret))))
    (with-device-coordinates (transform x y)
      (ignoring "do-set-caret-position"))))


#||
define sealed method do-set-caret-size
    (caret :: <gtk-caret>, width :: <integer>, height :: <integer>) => ()
  ignoring("do-set-caret-size")
end method do-set-caret-size;
||#

(defmethod do-set-caret-size ((caret <gtk-caret>) (width integer) (height integer))
  (ignoring "do-set-caret-size"))


#||
define sealed method do-show-caret
    (caret :: <gtk-caret>, #key tooltip?) => ()
  ignore(tooltip?);
  let sheet  = caret-sheet(caret);
  let widget = sheet & mirror-widget(sheet-mirror(sheet));
  when (widget)
    ignoring("do-show-caret")
  end
end method do-show-caret;
||#

(defmethod do-show-caret ((caret <gtk-caret>) &key tooltip?)
  (declare (ignore tooltip?))
  (let* ((sheet (caret-sheet caret))
	 (widget (and sheet (mirror-widget (sheet-mirror sheet)))))
    (when widget
      (ignoring "do-show-caret"))))


#||
define sealed method do-hide-caret
    (caret :: <gtk-caret>, #key tooltip?) => ()
  ignore(tooltip?);
  let sheet  = caret-sheet(caret);
  let widget = sheet & mirror-widget(sheet-mirror(sheet));
  when (widget)
    ignoring("do-hide-caret")
  end
end method do-hide-caret;
||#

(defmethod do-hide-caret ((caret <gtk-caret>) &key tooltip?)
  (declare (ignore tooltip?))
  (let* ((sheet (caret-sheet caret))
	 (widget (and sheet (mirror-widget (sheet-mirror sheet)))))
    (when widget
      (ignoring "do-hide-caret"))))



#||
/// Input focus handling

define sealed method note-focus-in
    (_port :: <gtk-port>, sheet :: <sheet>) => ()
  next-method();
  ignoring("note-focus-in")
end method note-focus-in;
||#

;;; XXX: Called by DUIM front-end to tell the back-end of a focus change

(defmethod note-focus-in ((_port <gtk-port>) (sheet <sheet>))
  (call-next-method)
  #-(and)
  (gtk-debug "[GTK-PORT NOTE-FOCUS-IN] sheet ~a gained focus" sheet)
  ;; Called when DUIM has decided to give a sheet focus
  (let* ((mirror (sheet-mirror sheet))
	 (widget (and mirror (mirror-widget mirror))))
    ;; GTK port does not track focus independently of basic port
    ;; If widget is already focussed, no need to do anything.
    ;; If widget CANNOT focus, we've screwed something up so error.
    ;; Get the widget to grab the focus.
    (if widget
	(if (cffi/gtk-widget-is-focus widget)
	    ;; attempting to give focus to widget that is already focused; ignoring
	    nil
	    ;; win32 b/end does "frame & call-in-frame(frame, fun ()
	    ;; set-focus(sheet) end)" here... that would be needed for
	    ;; n+1 I think.
	    (cffi/gtk-widget-grab-focus widget))
	;; TODO 2020-04-13 DR: look for a suitable focus target
	(gtk-debug "Attempting to give focus to sheet with no widget; ignoring"))))


#||
define sealed method note-focus-out
    (_port :: <gtk-port>, sheet :: <sheet>) => ()
  next-method();
  ignoring("note-focus-out")
end method note-focus-out;
||#

(defmethod note-focus-out ((_port <gtk-port>) (sheet <sheet>))
  (call-next-method)
  #-(and)
  (gtk-debug "[GTK-PORT NOTE-FOCUS-OUT] sheet ~a lost focus" sheet)
  #-(and)
  (gtk-debug "    - current focus for frame ~a is ~a"
	     (sheet-frame sheet) (frame-input-focus (sheet-frame sheet)))
  ;; FIXME: Work out how to do this and do it!
  #-(and)
  (let ((window ???))
    (gtk-debug "    - gtk current focus for window is ~a" (cffi/gtk-window-get-focus window)))
  #-(and)
  (gtk-debug "    - current focus for port is ~a" (port-input-focus _port))
  ;; FIXME: Does anything need to be done for this? Should GTK be told
  ;; that the sheet wants to abdicate the focus?
  ;; win32 b/end does "call-in-frame(frame, method () remove-focus() end)" here...
  (ignoring "note-focus-out"))


#||

define method set-focus
    (sheet :: <sheet>) => (set? :: <boolean>)
  let parent = sheet-device-parent(sheet, error?: #f);
  parent & set-focus(parent)
end method set-focus;

define method set-focus
    (sheet :: <mirrored-sheet-mixin>) => (set? :: <boolean>)
  let sheet :: false-or(<mirrored-sheet-mixin>)
    = if (sheet-accepts-focus?(sheet))
	sheet
      else
	find-child-for-focus(sheet)
      end;
  let handle = sheet & window-handle(sheet);
  case
    ~handle =>
      warn("Ignored attempt to set focus to unattached sheet %=", sheet);
      #f;
    GetFocus() = handle =>		// avoid recursion
      duim-debug-message("'set-focus' avoiding recursion");
      #f;
    otherwise =>
      duim-debug-message("'set-focus' setting focus to %=", sheet);
      // Don't check the result, because SetFocus doesn't properly clear
      // the error code.
      // check-result("SetFocus", SetFocus(handle));
      SetFocus(handle);
      #t;
  end
end method set-focus;

define method remove-focus
    () => ()
  duim-debug-message("'remove-focus' removing the focus");
  // We don't check the return result, because if we remove the focus
  // when there is no current input focus, it will look like an error
  SetFocus($NULL-HWND)
end method remove-focus;

// Finds the first mirrored child that can accept the input focus
//---*** The whole way we do this is really horrid
define method find-child-for-focus
    (sheet :: <sheet>)
 => (child :: false-or(<mirrored-sheet-mixin>))
  let child
    = block (return)
	local method find-child (sheet :: <sheet>)
		for (child :: <sheet> in sheet-children(sheet))
		  unless (instance?(child, <menu>))
		    find-child(child);
		    let mirror = sheet-direct-mirror(child);
		    when (instance?(mirror, <window-mirror>)
			  & sheet-mapped?(child)
			  & sheet-accepts-focus?(child))
		      return(child)
		    end
		  end
		end
	      end method;
	find-child(sheet);
	#f
      end;
  // If the child is a radio button within a radio box, put the focus
  // on the selected radio button
  when (instance?(child, <radio-button>))
    let box = button-gadget-box(child);
    when (instance?(box, <radio-box>))
      let selection = gadget-selection(box);
      let index     = ~empty?(selection) & selection[0];
      let button    = index & gadget-box-buttons(box)[index];
      button & (child := button)
    end
  end;
  child
end method find-child-for-focus;

// If the focus is set to the top level sheet, try to revert the focus
// to the frame's input focus. If there isn't one, then just use the
// ordinary guessing algorithm.
define method find-child-for-focus
    (sheet :: <top-level-sheet>)
 => (child :: false-or(<mirrored-sheet-mixin>))
  let frame = sheet-frame(sheet);
  let focus = frame & frame-input-focus(frame);
  if (instance?(focus, <mirrored-sheet-mixin>))
    focus
  else
    next-method()
  end
end method find-child-for-focus;

define sealed method maybe-update-focus
    (_port :: <win32-port>) => ()
  let focus = GetFocus();
  let old-focus = _port.%focus;
  let sheet = handle-sheet(focus);
  if (sheet)
    if (sheet-ignore-focus-change?(sheet))
      duim-debug-message("Ignoring focus change for %=", sheet)
    end;
    unless (sheet-ignore-focus-change?(sheet)
	      | focus = old-focus)
      let new
	= if (sheet-accepts-focus?(sheet))
	    sheet
	  else
	    duim-debug-message("Focus set to non-accepting sheet: %=", sheet);
	    find-child-for-focus(sheet)
	  end;
      case
	new =>
	  duim-debug-message("Focus now set to %=", new);
	  _port.%focus := focus;
	  unless (new == sheet)
	    let handle = window-handle(new);
	    handle & SetFocus(handle)
	  end;
	  port-input-focus(_port) := new;
	old-focus =>
	  duim-debug-message("Reverting focus from %= back to %=",
			     sheet,
			     handle-sheet(old-focus) | old-focus);
	  SetFocus(old-focus);
	otherwise =>
	  #f;
      end
    end
  else
    _port.%focus := focus;
    port-input-focus(_port) := #f
  end
end method maybe-update-focus;

define method sheet-ignore-focus-change?
    (sheet :: <sheet>) => (ignore? :: <boolean>)
  #f
end method sheet-ignore-focus-change?;

||#



#||
/// Port defaults

define method port-default-foreground
    (_port :: <gtk-port>, sheet :: <sheet>)
 => (foreground :: false-or(<ink>))
  query-widget-for-color(sheet, #"foreground")
end method port-default-foreground;
||#

(defmethod port-default-foreground ((_port <gtk-port>) (sheet <sheet>))
  (query-widget-for-color sheet :foreground))


#||
// Most sheets should show up with the standard 3d gray background...
define method port-default-background
    (_port :: <gtk-port>, sheet :: <sheet>)
 => (background :: false-or(<ink>));
  query-widget-for-color(sheet, #"background")
end method port-default-background;
||#

(defmethod port-default-background ((_port <gtk-port>) (sheet <sheet>))
  (query-widget-for-color sheet :background))


#||
// ...but drawing panes should defaultly have a white background
define method port-default-background
    (_port :: <gtk-port>, sheet :: <drawing-pane>)
 => (background :: false-or(<ink>));
  $white
end method port-default-background;
||#

(defmethod port-default-background ((_port <gtk-port>) (sheet <drawing-pane>))
  *white*)


#||
define method query-widget-for-color
    (sheet :: <sheet>, key :: one-of(#"foreground", #"background"))
 => (color :: false-or(<ink>))
  ignoring("query-widget-for-color");
  let mirror = sheet-mirror(sheet);
  let widget = mirror & mirror-widget(mirror);
  when (widget)
    #f
    // query-pixel-for-color(xt/XtGetValues(widget, key), port-default-palette(_port))
  end
end method query-widget-for-color;
||#

(defgeneric query-widget-for-color (sheet which)
  (:documentation
"
Returns the foreground or background color of _sheet_ in its 'normal'
state ('normal' being GTK parlance).
_which_ can take one of the values :FOREGROUND or :BACKGROUND.

Source file: gtk-port.lisp"))


(defmethod query-widget-for-color ((sheet <sheet>) which)
  (let* ((port   (port sheet))
	 (mirror (sheet-mirror sheet))
	 (widget (and mirror (mirror-widget mirror))))
    (when widget
      (let ((gtkstyle* (cffi/gtk-widget-get-style widget)))
	(cond
	  #||
	  ((eql which :foreground)
	  ;; fixme: this is where we were at the other day, where cffi
	  ;;     converts the structure into a Lisp plist but we want the
	  ;; pointer because it's actually an array. How to get the
	  ;; actual array element we want out of it?
	  ;; Put the hack back in for now...
	  (native-color->color (mem-aref (cffi/gtkstyle->fg gtkstyle*)
	  '(:struct GdkColor)
	  +CFFI/GTK-STATE-NORMAL+)
	  (port-default-palette port)))
	  ((eql which :background)
	  (native-color->color (mem-aref (cffi/gtkstyle->bg gtkstyle*)
	  '(:struct GdkColor)
	  +CFFI/GTK-STATE-NORMAL+)
	  (port-default-palette port)))
	  ||#
	  ((eql which :foreground)
	   (native-color->color (cffi/gtkstyle->fg gtkstyle*)
				(port-default-palette port)))
	  ((eql which :background)
	   (native-color->color (cffi/gtkstyle->bg gtkstyle*)
				(port-default-palette port)))
	  (t
	   (error "Can only query widget :FOREGROUND or :BACKGROUND for colour")))))))


#||
//---*** WHAT TO DO ABOUT THIS?

// FYI, the normal size on GTK is 8-points
// We arrange to map this to something close to ANSI_VAR_FONT
define constant $gtk-default-text-style
    = make(<text-style>,
	   family: #"sans-serif", weight: #"normal",
	   slant: #"roman", size: #"normal");
||#

(defparameter *gtk-default-text-style*
  (make-text-style :sans-serif
		   nil
		   :normal
		   :roman
		   :normal))


#||
// Note that this "default default" text style is _not_ the one that we use
// for gadgets.  There's another method for that on <gtk-gadget-mixin>.
define method port-default-text-style
    (_port :: <gtk-port>, sheet :: <sheet>)
 => (text-style :: false-or(<text-style>))
  $gtk-default-text-style
end method port-default-text-style;
||#

(defmethod port-default-text-style ((_port <gtk-port>) (sheet <sheet>))
  *gtk-default-text-style*)


