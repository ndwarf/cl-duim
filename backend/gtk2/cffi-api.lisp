;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GTK2-DUIM -*-
(in-package #:gtk2-duim)

;;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;    PREAMBLE
;;;
;;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; The contents of this file depend on CFFI-TYPES+CONSTANTS.LISP
;;; The contents of this file depend on CFFI-STRUCTURES.LISP

;;; FIXME: this needs a serious tidying up...

;;; The file is organised in the following way:-
;;;
;;; Major sections are separated by ';;;; ... ;;;;', subsections
;;; by '==== ... ====', and minor sections by '------'.
;;;
;;; PREAMBLE (you're reading it)
;;; NOTES
;;; FOREIGN FUNCTION INVOCATION & WRAPPING
;;;     IMPORT LIBRARIES
;;;     GTK INITIALIZATION
;;;     SCREEN (DUIM = 'display') MANIPULATION
;;;     WINDOW (DUIM = 'frame') MANIPULATION
;;;     CONTAINERS
;;;         Container
;;;         Fixed
;;;     SUPPORT FOR EVENTS
;;;     MISCELLANEOUS
;;;         Beep
;;;         Flushing
;;;         Pointer grabbing
;;;         Selections
;;;         GTK-String
;;;         GTK-Object
;;; DYLAN -> CFFI UTILITIES


;;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;    NOTES
;;;
;;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Need to be consistent in what these FFI calls return, in the case
;;; of void C return types.

;;; NOTE: most of these gdk/gtk-* procedures WILL NOT WORK until
;;; after INITIALIZE-GTK is called...

;;; FIXME: How can we get some semblance of type-safety (in the
;;; 'main' backend code) without making lisp types? Maybe cffi
;;; can do something, somewhere...
;;; Look @ translate-to-foreign + translate-from-foreign stuff
;;; in CFFI for this, maybe...

;;; All method parameters that are expected to be pointers are
;;; checked; Lisp nils are converted to cffi:null-pointer.

;;; All methods that return pointers have the pointer value
;;; checked; any that return cffi:null-pointer return a Lisp
;;; nil instead.

;;; All methods that cause a GLib allocation call g_obj_ref_sink
;;; and either convert any floating reference into a hard reference,
;;; or just increase the reference count. THESE OBJECTS MUST BE
;;; FREED!

(defun pointer-or-nil (pointer)
"
If _pointer_ is the null pointer, returns nil; else returns
_pointer_.
"
  (if (cffi:null-pointer-p pointer) nil pointer))





;;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;    FOREIGN FUNCTION INVOCATION & WRAPPING
;;;
;;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; ========================================================================
;;;
;;;    Import libraries
;;;

;;; As long as the library is in the PATH environment variable, CFFI
;;; should find them and their dependencies.

;;; First, define them all in CFFI
(cffi:define-foreign-library libgthread
  (:unix (:or "libgthread-2.0.so.0" "libgthread-2.0.so"))
  (t (:default "libgthread-2.0-0")))

#-(and)
(cffi:define-foreign-library libglib
  (:unix (:or "/usr/lib/libglib-2.0.so.0" "libglib-2.0.so"))
  (t (:default "libglib")))

(cffi:define-foreign-library libgdk
  (:unix (:or "libgdk-x11-2.0.so.0" "libgdk-x11-2.0.so"))
  (t (:default "libgdk-win32-2.0-0")))

(cffi:define-foreign-library libgtk
  (:unix (:or "libgtk-x11-2.0.so.0" "libgtk-x11-2.0.so"))
  (t (:default "libgtk-win32-2.0-0")))

;;; TODO:SN:20200331 Determine the library names for UNIX
(cffi:define-foreign-library libgobject
  (:unix (:or "libgtk-x11-2.0.so.0" "libgtk-x11-2.0.so"))
  (t (:default "libgobject-2.0-0")))

;;; TODO:SN:20200331 Determine the library names for UNIX
(cffi:define-foreign-library libgobject
  (:unix (:or "libgtk-x11-2.0.so.0" "libgtk-x11-2.0.so"))
  (t (:default "libgmodule-2.0-0")))

#-(and)
(cffi:define-foreign-library libpango
  (:unix (:or "libpango-1.0.so.0" "libpango-1.0.so"))
  (t (:default "libpango-1.0")))

;;; Now load them all into the lisp image
(cffi:use-foreign-library libgthread)
(cffi:use-foreign-library libgdk)
(cffi:use-foreign-library libgtk)
(cffi:use-foreign-library libgobject)
;(cffi:use-foreign-library libgmodule)
#-(and) (cffi:use-foreign-library libpango)
#-(and) (cffi:use-foreign-library libglib)


;;; ========================================================================
;;;
;;;    GTK initialization
;;;

(defun cffi/initialize-gtk ()
  (cffi/gtk-init (cffi:null-pointer) (cffi:null-pointer))
  t)


;;; ========================================================================
;;;
;;;    GObject + GDK thread support
;;;

(defun cffi/g-slice-alloc0 (size)
"
 gpointer g_slice_alloc0 (gsize block_size);

 Allocates a block of memory via g_slice_alloc() and initialize the returned
 memory to 0. Note that the underlying slice allocation mechanism can be
 changed with the G_SLICE=always-malloc environment variable.

 Allocated memory should be freed with 'g_slice_free1()'.

    block_size : the number of bytes to allocate
    Returns : a pointer to the allocated block
"
  (let ((pointer (cffi:foreign-funcall "g_slice_alloc0"
				       gsize size
				       :pointer)))
    (pointer-or-nil pointer)))


(defun cffi/g-slice-free1 (size pointer)
"
 void g_slice_free1 (gsize block_size, gpointer mem_block);

 Frees a block of memory. The memory must have been allocated via
 g_slice_alloc() or g_slice_alloc0() and the block_size has to
 match the size specified upon allocation. Note that the exact
 release behaviour can be changed with the G_DEBUG=gc-friendly
 environment variable, also see G_SLICE for related debugging options.

    block_size : the size of the block
    mem_block : a pointer to the block to free
"
  (if (or (null pointer) (cffi:null-pointer-p pointer))
      (error "CFFI/G-SLICE-FREE1 invoked with null POINTER")
      (cffi:foreign-funcall "g_slice_free1"
			    gsize size
			    :pointer pointer
			    :void))
  nil)


(defun cffi/g-type-make-fundamental (type)
  (ash type +CFFI/G-TYPE-FUNDAMENTAL-SHIFT+))


(defun cffi/gtk-tree-path-free (path)
"
 void gtk_tree_path_free (GtkTreePath *path);

 Frees path. If path is NULL, it simply returns.

    path : a GtkTreePath. [allow-none]
"
  (cffi:foreign-funcall "gtk_tree_path_free"
			:pointer path
			:void)
  nil)


(defun cffi/g-value-init (value type)
"
 GValue * g_value_init (GValue *value,GType g_type);

 Initializes value with the default value of type.

 value : A zero-filled (uninitialized) GValue structure.
 g_type : Type the GValue should hold values of.

 Returns : the GValue structure that has been passed in. [transfer none]
"
  (if (or (null value) (cffi:null-pointer-p value))
      (error "CFFI/G-VALUE-INIT invoked with null VALUE")
      (let ((value (cffi:foreign-funcall "g_value_init"
					 :pointer value
					 GType type
					 :pointer)))
	(pointer-or-nil value))))


(defun cffi/g-value-set-string (value string)
"
 FIXME: LOOKUP THE DOCS FOR THIS!
"
  (if (or (null value) (cffi:null-pointer-p value))
      (error "CFFI/G-VALUE-SET-STRING invoked with null VALUE")
      (if (or (null string) (and (cffi:pointerp string) (cffi:null-pointer-p string)))
	  (error "CFFI/G-VALUE-SET-STRING invoked with null STRING")
	  (cffi:foreign-funcall "g_value_set_string"
				:pointer value
				:string string
				:void)))
  value)


(defun cffi/g-value-reset (value)
"
 GValue * g_value_reset (GValue *value);

 Clears the current value in value and resets it to the
 default value (as if the value had just been initialized).

    value : An initialized GValue structure.
    Returns : the GValue structure that has been passed in
"
  (if (or (null value) (cffi:null-pointer-p value))
      (error "CFFI/G-VALUE-RESET invoked with null VALUE")
      (let ((rval (cffi:foreign-funcall "g_value_reset"
					:pointer value
					:pointer)))
	(pointer-or-nil rval))))


#|| UPGRADE NOTE:

g_thread_init()

Not needed since GLib 2.32 (released: 2012) See:

https://developer.gnome.org/glib/2.64/glib-Threads.html
||#

(defun cffi/gdk-threads-init ()
  "void gdk_threads_init (void);

Initializes GDK so that it can be used from multiple threads in
conjunction with gdk_threads_enter() and
gdk_threads_leave(). g_thread_init() must be called previous to this
function.

This call must be made before any use of the main loop from GTK+; to
be safe, call it before gtk_init()."
  (cffi:foreign-funcall "gdk_threads_init"
			:void))


(defun cffi/gtk-init (argc argv)
  ;; void gtk_init (int *argc, char ***argv)
  (cffi:foreign-funcall "gtk_init"
			(:pointer :int) argc
			:pointer argv
			:void))


(defun cffi/gdk-threads-enter ()
"
This macro marks the beginning of a critical section in which GDK and GTK+
functions can be called safely and without causing race conditions. Only one
thread at a time can be in such a critial section.
"
  ;; void gdk_threads_enter (void);
  (cffi:foreign-funcall "gdk_threads_enter"
			:void)
  t)


(defun cffi/gdk-threads-leave ()
"
Leaves a critical region begun with gdk_threads_enter(). 
"
  ;; void gdk_threads_leave (void);
  (cffi:foreign-funcall "gdk_threads_leave"
			:void)
  t)


;;; ========================================================================
;;;
;;;    CLIPBOARD SUPPORT
;;;

(defun cffi/gdk-atom-intern (atom-name only-if-exists)
"
 GdkAtom gdk_atom_intern (const gchar *atom_name, gboolean only_if_exists);

 Finds or creates an atom corresponding to a given string.

    atom_name : a string.
    only_if_exists : if TRUE, GDK is allowed to not create a new atom, but just
                     return GDK_NONE if the requested atom doesn't already exist.
    Currently, the flag is ignored, since checking the existance of an atom is as
    expensive as creating it.

    Returns : the atom corresponding to atom_name.
"
  (let ((atom (cffi:foreign-funcall "gdk_atom_intern"
				    :string atom-name
				    :boolean only-if-exists
				    :pointer)))
    (pointer-or-nil atom)))


(defun cffi/gtk-clipboard-get (selection)
"
 GtkClipboard * gtk_clipboard_get (GdkAtom selection);

 Returns the clipboard object for the given selection. See gtk_clipboard_get_for_display()
 for complete details.

    selection : a GdkAtom which identifies the clipboard to use

    Returns : the appropriate clipboard object. If no clipboard already exists, a new
              one will be created. Once a clipboard object has been created, it is persistent
    and, since it is owned by GTK+, must not be freed or unreffed. [transfer none]
"
  (let ((clipboard (cffi:foreign-funcall "gtk_clipboard_get"
					 :pointer selection
					 :pointer)))
    (pointer-or-nil clipboard)))


(defun cffi/gtk-clipboard-set-text (clipboard text len)
"
 void gtk_clipboard_set_text (GtkClipboard *clipboard, const gchar *text, gint len);

 Sets the contents of the clipboard to the given UTF-8 string. GTK+ will make a copy
 of the text and take responsibility for responding for requests for the text, and for
 converting the text into the requested format.

    clipboard : a GtkClipboard object
    text : a UTF-8 string.
    len : length of text, in bytes, or -1, in which case the length will be determined
          with strlen().
"
  (if (or (null clipboard) (cffi:null-pointer-p clipboard))
      (error "CFFI/GTK-CLIPBOARD-SET-TEXT invoked with null CLIPBOARD")
      (cffi:foreign-funcall "gtk_clipboard_set_text"
			    :pointer clipboard
			    :string text
			    :int len
			    :void))
  clipboard)


(defun cffi/gtk-clipboard-set-image (clipboard pixbuf)
"
 void gtk_clipboard_set_image (GtkClipboard *clipboard, GdkPixbuf *pixbuf);

 Sets the contents of the clipboard to the given GdkPixbuf. GTK+ will take
 responsibility for responding for requests for the image, and for converting
 the image into the requested format.

    clipboard : a GtkClipboard object
    pixbuf : a GdkPixbuf

 Since 2.6
"
  (if (or (null clipboard) (cffi:null-pointer-p clipboard))
      (error "CFFI/GTK-CLIPBOARD-SET-IMAGE invoked with null CLIPBOARD")
      (if (or (null pixbuf) (cffi:null-pointer-p pixbuf))
	  (error "CFFI/GTK-CLIPBOARD-SET-IMAGE invoked with null PIXBUF")
	  (cffi:foreign-funcall "gtk_clipboard_set_image"
				:pointer clipboard
				:pointer pixbuf
				:void)))
  clipboard)


(defun cffi/gtk-clipboard-store (clipboard)
"
 void gtk_clipboard_store (GtkClipboard *clipboard);

 Stores the current clipboard data somewhere so that it will stay around after the
 application has quit.

    clipboard : a GtkClipboard

 Since 2.6
"
  (if (or (null clipboard) (cffi:null-pointer-p clipboard))
      (error "CFFI/GTK-CLIPBOARD-STORE invoked with null CLIPBOARD")
      (cffi:foreign-funcall "gtk_clipboard_store"
			    :pointer clipboard
			    :void))
  clipboard)


(defun cffi/gtk-clipboard-wait-is-text-available (clipboard)
"
 gboolean gtk_clipboard_wait_is_text_available (GtkClipboard *clipboard);

 Test to see if there is text available to be pasted This is done by requesting the
 TARGETS atom and checking if it contains any of the supported text targets. This
 function waits for the data to be received using the main loop, so events, timeouts,
 etc, may be dispatched during the wait.

 This function is a little faster than calling gtk_clipboard_wait_for_text() since
 it doesn't need to retrieve the actual text.

    clipboard : a GtkClipboard

    Returns : TRUE is there is text available, FALSE otherwise.
"
  (if (or (null clipboard) (cffi:null-pointer-p clipboard))
      (error "CFFI/GTK-CLIPBOARD-WAIT-IS-TEXT-AVAILABLE invoked with null CLIPBOARD")
      (let ((available? (cffi:foreign-funcall "gtk_clipboard_wait_is_text_available"
					      :pointer clipboard
					      :boolean)))
	available?)))


(defun cffi/gtk-clipboard-wait-is-image-available (clipboard)
"
 gboolean gtk_clipboard_wait_is_image_available (GtkClipboard *clipboard);

 Test to see if there is an image available to be pasted This is done by requesting
 the TARGETS atom and checking if it contains any of the supported image targets. This
 function waits for the data to be received using the main loop, so events, timeouts,
 etc, may be dispatched during the wait.

 This function is a little faster than calling gtk_clipboard_wait_for_image() since it
 doesn't need to retrieve the actual image data.

    clipboard : a GtkClipboard

    Returns : TRUE is there is an image available, FALSE otherwise.

 Since 2.6
"
  (if (or (null clipboard) (cffi:null-pointer-p clipboard))
      (error "CFFI/GTK-CLIPBOARD-WAIT-IS-IMAGE-AVAILABLE invoked with null CLIPBOARD")
      (let ((available? (cffi:foreign-funcall "gtk_clipboard_wait_is_image_available"
					      :pointer clipboard
					      :boolean)))
	available?)))


(defun cffi/gtk-clipboard-wait-for-text (clipboard)
"
 gchar * gtk_clipboard_wait_for_text (GtkClipboard *clipboard);

 Requests the contents of the clipboard as text and converts the result to UTF-8 if
 necessary. This function waits for the data to be received using the main loop, so
 events, timeouts, etc, may be dispatched during the wait.

    clipboard : a GtkClipboard

    Returns : a newly-allocated UTF-8 string which must be freed with g_free(), or NULL
              if retrieving the selection data failed. (This could happen for various
    reasons, in particular if the clipboard was empty or if the contents of the clipboard
    could not be converted into text form.)
"
  (if (or (null clipboard) (cffi:null-pointer-p clipboard))
      (error "CFFI/GTK-CLIPBOARD-WAIT-FOR-TEXT invoked with null CLIPBOARD")
      (let ((text (cffi:foreign-funcall "gtk_clipboard_wait_for_text"
					:pointer clipboard
					:string)))
	text)))


(defun cffi/gtk-clipboard-wait-for-image (clipboard)
"
 GdkPixbuf * gtk_clipboard_wait_for_image (GtkClipboard *clipboard);

 Requests the contents of the clipboard as image and converts the result to a GdkPixbuf.
 This function waits for the data to be received using the main loop, so events, timeouts,
 etc, may be dispatched during the wait.

    clipboard : a GtkClipboard

    Returns : a newly-allocated GdkPixbuf object which must be disposed with g_object_unref(),
              or NULL if retrieving the selection data failed. (This could happen for various
    reasons, in particular if the clipboard was empty or if the contents of the clipboard
    could not be converted into an image.). [transfer full]

 Since 2.6
"
  (if (or (null clipboard) (cffi:null-pointer-p clipboard))
      (error "CFFI/GTK-CLIPBOARD-WAIT-FOR-IMAGE invoked with null CLIPBOARD")
      (let ((pixbuf (cffi:foreign-funcall "gtk_clipboard_wait_for_image"
					  :pointer clipboard
					  :pointer)))
	(pointer-or-nil pixbuf))))


;;; ========================================================================
;;;
;;;    Pango
;;;

(defun cffi/pango-font-description-new ()
"
Creates a new font description structure with all fields unset.

    Returns - the newly allocated PangoFontDescription, which should be freed
              using pango_font_description_free(). 
"
  ;; PangoFontDescription * pango_font_description_new (void);
  (let ((ptr (cffi:foreign-funcall "pango_font_description_new" :pointer)))
    (duim-mem-debug "GTK-CFFI-API:PANGO-FONT-DESCRIPTION-NEW" ptr)
    (pointer-or-nil ptr)))


(defun cffi/pango-font-description-set-family (desc family)
"
Sets the family name field of a font description. The family name represents
a family of related font styles, and will resolve to a particular PangoFontFamily.
In some uses of PangoFontDescription, it is also possible to use a comma separated
list of family names for this field.

    desc   - a PangoFontDescription.
    family - a string representing the family name. 
"
  ;; void pango_font_description_set_family (PangoFontDescription *desc,
  ;;                                         const char *family);
  (if (or (not desc) (cffi:null-pointer-p desc))
      (error "CFFI/PANGO-FONT-DESCRIPTION-SET-FAMILY called with null FONT-DESCRIPTION")
      (cffi:foreign-funcall "pango_font_description_set_family"
			    :pointer desc
			    :string family
			    :void)))


;;; FIXME: should hide things like strings away inside here instead of polluting
;;; the "higher level" backend functionality.

(defun cffi/pango-font-description-set-size (desc size)
"
Sets the size field of a font description in fractional points. This is mutually exclusive
with pango_font_description_set_absolute_size().

    desc - a PangoFontDescription
    size - the size of the font in points, scaled by PANGO_SCALE. (That is, a size value
           of 10 * PANGO_SCALE is a 10 point font. The conversion factor between points and
           device units depends on system configuration and the output device. For screen
           display, a logical DPI of 96 is common, in which case a 10 point font corresponds
           to a 10 * (96 / 72) = 13.3 pixel font. Use
           pango_font_description_set_absolute_size() if you need a particular size in
           device units. 
"
  ;; void pango_font_description_set_size (PangoFontDescription *desc,
  ;;                                       gint size);
  (if (or (not desc) (cffi:null-pointer-p desc))
      (error "CFFI/PANGO-FONT-DESCRIPTION-SET-SIZE called with null FONT-DESCRIPTION")
      (cffi:foreign-funcall "pango_font_description_set_size"
			    :pointer desc
			    gint size
			    :void)))


(defun cffi/pango-font-description-set-stretch (desc stretch)
"
Sets the stretch field of a font description. The stretch field specifies how narrow or wide
the font should be.

    desc    - a PangoFontDescription
    stretch - the stretch for the font description 
"
  ;; void pango_font_description_set_stretch (PangoFontDescription *desc,
  ;;                                          PangoStretch stretch);
  (if (or (not desc) (cffi:null-pointer-p desc))
      (error "CFFI/PANGO-FONT-DESCRIPTION-SET-STRETCH called with null FONT-DESCRIPTION")
      (cffi:foreign-funcall "pango_font_description_set_stretch"
			    :pointer desc
			    :int stretch
			    :void)))


(defun cffi/pango-font-description-set-style (desc style)
"
Sets the style field of a PangoFontDescription. The PangoStyle enumeration
describes whether the font is slanted and the manner in which it is slanted;
it can be either PANGO_STYLE_NORMAL, PANGO_STYLE_ITALIC, or
PANGO_STYLE_OBLIQUE. Most fonts will either have a italic style or an oblique
style, but not both, and font matching in Pango will match italic
specifications with oblique fonts and vice-versa if an exact match is not found.

    desc  - a PangoFontDescription
    style - the style for the font description 
"
  ;; void pango_font_description_set_style (PangoFontDescription *desc,
  ;;                                        PangoStyle style);
  (if (or (not desc) (cffi:null-pointer-p desc))
      (error "CFFI/PANGO-FONT-DESCRIPTION-SET-STYLE called with null FONT-DESCRIPTION")
      (cffi:foreign-funcall "pango_font_description_set_style"
			    :pointer desc
			    :int style
			    :void)))


(defun cffi/pango-font-description-set-variant (desc variant)
"
Sets the variant field of a font description. The PangoVariant can either
be PANGO_VARIANT_NORMAL or PANGO_VARIANT_SMALL_CAPS.

    desc    - a PangoFontDescription
    variant - the variant type for the font description. 
"
  ;; void pango_font_description_set_variant (PangoFontDescription *desc,
  ;;                                          PangoVariant variant);
  (if (or (not desc) (cffi:null-pointer-p desc))
      (error "CFFI/PANGO-FONT-DESCRIPTION-SET-VARIANT called with null FONT-DESCRIPTION")
      (cffi:foreign-funcall "pango_font_description_set_variant"
			    :pointer desc
			    :int variant
			    :void)))


(defun cffi/pango-font-description-set-weight (desc weight)
"
Sets the weight field of a font description. The weight field specifies how bold
or light the font should be. In addition to the values of the PangoWeight
enumeration, other intermediate numeric values are possible.

    desc   - a PangoFontDescription
    weight - the weight for the font description. 
"
  ;; void pango_font_description_set_weight (PangoFontDescription *desc,
  ;;                                         PangoWeight weight);
  (if (or (not desc) (cffi:null-pointer-p desc))
      (error "CFFI/PANGO-FONT-DESCRIPTION-SET-WEIGHT called with null FONT-DESCRIPTION")
      (cffi:foreign-funcall "pango_font_description_set_weight"
			    :pointer desc
			    :int weight
			    :void)))


(defun cffi/pango-font-metrics-get-approximate-char-width (metrics)
"
Gets the approximate character width for a font metrics structure. This is
merely a representative value useful, for example, for determining the initial
size for a window. Actual characters in text will be wider and narrower than this.

    metrics - a pointer to a PangoFontMetrics structure
    Returns - the character width, in Pango units. 
"
  ;; int pango_font_metrics_get_approximate_char_width (PangoFontMetrics *metrics);
  (if (or (not metrics) (cffi:null-pointer-p metrics))
      (error "CFFI/PANGO-FONT-METRICS-GET-APPROXIMATE-CHAR-WIDTH called with null METRICS")
      (cffi:foreign-funcall "pango_font_metrics_get_approximate_char_width"
			    :pointer metrics
			    :int)))


(defun cffi/pango-font-metrics-get-ascent (metrics)
"
Gets the ascent from a font metrics structure. The ascent is the distance from the
baseline to the logical top of a line of text. (The logical top may be above or
below the top of the actual drawn ink. It is necessary to lay out the text to
figure where the ink will be.)

    metrics - a PangoFontMetrics structure
    Returns - the ascent, in Pango units. 
"
  ;; int pango_font_metrics_get_ascent (PangoFontMetrics *metrics);
  (if (or (not metrics) (cffi:null-pointer-p metrics))
      (error "CFFI/PANGO-FONT-METRICS-GET-ASCENT called with null METRICS")
      (cffi:foreign-funcall "pango_font_metrics_get_ascent"
			    :pointer metrics
			    :int)))


(defun cffi/pango-font-metrics-get-descent (metrics)
"
Gets the descent from a font metrics structure. The descent is the distance from the
baseline to the logical bottom of a line of text. (The logical bottom may be above or
below the bottom of the actual drawn ink. It is necessary to lay out the text to figure
where the ink will be.)

    metrics - a PangoFontMetrics structure
    Returns - the descent, in Pango units. 
"
  ;; int pango_font_metrics_get_descent (PangoFontMetrics *metrics);
  (if (or (not metrics) (cffi:null-pointer-p metrics))
      (error "CFFI/PANGO-FONT-METRICS-GET-DESCENT called with null METRICS")
      (cffi:foreign-funcall "pango_font_metrics_get_descent"
			    :pointer metrics
			    :int)))


(defun cffi/pango-font-metrics-unref (metrics)
"
Decrease the reference count of a font metrics structure by one. If the result is zero,
frees the structure and any associated memory.

    metrics - a PangoFontMetrics structure, may be NULL
"
  ;; void pango_font_metrics_unref (PangoFontMetrics *metrics);
  (let ((metrics (or metrics (cffi:null-pointer))))
    (cffi:foreign-funcall "pango_font_metrics_unref"
			  :pointer metrics
			  :void)))


(defun cffi/pango-context-get-metrics (context desc language)
"
Get overall metric information for a particular font description. Since the
metrics may be substantially different for different scripts, a language tag
can be provided to indicate that the metrics should be retrieved that
correspond to the script(s) used by that language.

The PangoFontDescription is interpreted in the same way as by pango_itemize(),
and the family name may be a comma separated list of figures. If characters
from multiple of these families would be used to render the string, then the
returned fonts would be a composite of the metrics for the fonts loaded for
the individual families.

    context  - a PangoContext
    desc     - a PangoFontDescription structure. NULL means that the font
               description from the context will be used.
    language - language tag used to determine which script to get the metrics
               for. NULL means that the language tag from the context will be
               used. If no language tag is set on the context, metrics for the
               default language (as determined by pango_language_get_default())
               will be returned. 
    Returns  - a PangoFontMetrics object. The caller must call
               pango_font_metrics_unref() when finished using the object.
"
  ;; PangoFontMetrics * pango_context_get_metrics (PangoContext *context,
  ;;                                               const PangoFontDescription *desc,
  ;;                                               PangoLanguage *language);
  (if (or (null context) (cffi:null-pointer-p context))
      (error "CFFI/PANGO-CONTEXT-GET-METRICS called with null CONTEXT")
      (let* ((desc (or desc (cffi:null-pointer)))
	     (language (or language (cffi:null-pointer)))
	     (metrics (cffi:foreign-funcall "pango_context_get_metrics"
					    :pointer context
					    :pointer desc
					    :pointer language
					    :pointer)))
	(pointer-or-nil metrics))))


(defun cffi/gdk-pango-context-get ()
"
Creates a PangoContext for the default GDK screen.

The context must be freed when you're finished with it.

When using GTK+, normally you should use gtk_widget_get_pango_context() instead
of this function, to get the appropriate context for the widget you intend to
render text onto.

The newly created context will have the default font options (see
cairo_font_options_t) for the default screen; if these options change it will
not be updated. Using gtk_widget_get_pango_context() is more convenient if you
want to keep a context around and track changes to the screen's font rendering
settings.

    Returns - a new PangoContext for the default display 
"
  ;; PangoContext * gdk_pango_context_get (void);
  (let ((context (cffi:foreign-funcall "gdk_pango_context_get" :pointer)))
    (pointer-or-nil context)))


(defun cffi/gtk-widget-get-pango-context (widget)
"
Gets a PangoContext with the appropriate font map, font description, and base
direction for this widget. Unlike the context returned by
gtk_widget_create_pango_context(), this context is owned by the widget (it can
be used until the screen for the widget changes or the widget is removed from
its toplevel), and will be updated to match any changes to the widget's
attributes.

If you create and keep a PangoLayout using this context, you must deal with
changes to the context by calling pango_layout_context_changed() on the layout
in response to the \"style-set\" and \"direction-changed\" signals for the
widget.

    widget  - a GtkWidget
    Returns - the PangoContext for the widget.. transfer none. 
"
  ;; PangoContext * gtk_widget_get_pango_context (GtkWidget *widget);
  (if (or (null widget) (cffi:null-pointer-p widget))
      (error "CFFI/GTK-WIDGET-GET-PANGO-CONTEXT called with null WIDGET")
      (let ((context (cffi:foreign-funcall "gtk_widget_get_pango_context"
					   :pointer widget
					   :pointer)))
	(pointer-or-nil context))))


(defun cffi/pango-layout-new (context)
"
Create a new PangoLayout object with attributes initialized to default values
for a particular PangoContext.

    context : a PangoContext
    Returns : the newly allocated PangoLayout, with a reference count of one,
              which should be freed with g_object_unref(). 
"
  ;; PangoLayout * pango_layout_new (PangoContext *context);
  (if (or (null context) (cffi:null-pointer-p context))
      (error "CFFI/PANGO-LAYOUT-NEW called with null CONTEXT")
      (let ((layout (cffi:foreign-funcall "pango_layout_new"
					  :pointer context
					  :pointer)))
	(pointer-or-nil layout))))


(defun cffi/pango-layout-set-font-description (layout desc)
"
Sets the default font description for the layout. If no font description is set
on the layout, the font description from the layout's context is used.

    layout - a PangoLayout
    desc   - the new PangoFontDescription, or NULL to unset the current font
             description 
"
  ;; void pango_layout_set_font_description (PangoLayout *layout,
  ;;                                         const PangoFontDescription *desc);
  (if (or (null layout) (cffi:null-pointer-p layout))
      (error "CFFI/PANGO-LAYOUT-SET-FONT-DESCRIPTION called with null LAYOUT")
      (let ((desc (or desc (cffi:null-pointer))))
	(cffi:foreign-funcall "pango_layout_set_font_description"
			      :pointer layout
			      :pointer desc
			      :void))))


(defun cffi/pango-layout-set-text (layout text length)
"
Sets the text of the layout.
Note that if you have used pango_layout_set_markup() or
pango_layout_set_markup_with_accel() on layout before, you may want to call
pango_layout_set_attributes() to clear the attributes set on the layout from
the markup as this function does not clear attributes.

    layout - a PangoLayout
    text   - a valid UTF-8 string
    length - maximum length of text, in bytes. -1 indicates that the string is
             nul-terminated and the length should be calculated. The text will
             also be truncated on encountering a nul-termination even when
             length is positive.
"
  ;; void pango_layout_set_text (PangoLayout *layout, const char *text, int length);
  (if (or (null layout) (cffi:null-pointer-p layout))
      (error "CFFI/PANGO-LAYOUT-SET-TEXT called with null LAYOUT")
      (if (or (null text) (and (cffi:pointerp text) (cffi:null-pointer-p text)))
	  (error "CFFI/PANGO-LAYOUT-SET-TEXT called with null TEXT")
	  (cffi:foreign-funcall "pango_layout_set_text"
				:pointer layout
				:string text
				:int length
				:void))))


(defun cffi/pango-attr-strikethrough-new (strikethrough?)
"
 PangoAttribute * pango_attr_strikethrough_new (gboolean strikethrough);

 Create a new strike-through attribute.

    strikethrough : TRUE if the text should be struck-through.

    Returns : the newly allocated PangoAttribute, which should be freed with
              pango_attribute_destroy(). [transfer full]
"
  (let ((attr (cffi:foreign-funcall "pango_attr_strikethrough_new"
				    :boolean strikethrough?
				    :pointer)))
    (pointer-or-nil attr)))

#||
typedef enum {
  PANGO_UNDERLINE_NONE,
  PANGO_UNDERLINE_SINGLE,
  PANGO_UNDERLINE_DOUBLE,
  PANGO_UNDERLINE_LOW,
  PANGO_UNDERLINE_ERROR
} PangoUnderline;
||#

(defconstant +CFFI/PANGO-UNDERLINE-NONE+ 0)
(defconstant +CFFI/PANGO-UNDERLINE-SINGLE+ 1)
(defconstant +CFFI/PANGO-UNDERLINE-DOUBLE+ 2)
(defconstant +CFFI/PANGO-UNDERLINE-LOW+ 3)


(defun cffi/pango-attr-underline-new (underline?)
"
 PangoAttribute * pango_attr_underline_new (PangoUnderline underline);

 Create a new underline-style attribute.

    underline : the underline style.

    Returns : the newly allocated PangoAttribute, which should be freed with
              pango_attribute_destroy(). [transfer full]
"
  (let* ((underline (if underline? +CFFI/PANGO-UNDERLINE-SINGLE+ +CFFI/PANGO-UNDERLINE-NONE+))
	 (attr (cffi:foreign-funcall "pango_attr_underline_new"
				     :int underline
				     :pointer)))
    (pointer-or-nil attr)))


(defun cffi/pango-attr-list-new ()
"
 PangoAttrList * pango_attr_list_new (void);

 Create a new empty attribute list with a reference count of one.

    Returns : the newly allocated PangoAttrList, which should be freed with
              pango_attr_list_unref(). [transfer full]
"
  (let ((list (cffi:foreign-funcall "pango_attr_list_new"
				    :pointer)))
    (pointer-or-nil list)))


(defun cffi/pango-attr-list-insert (list attribute)
"
 void pango_attr_list_insert (PangoAttrList *list, PangoAttribute *attr);

 Insert the given attribute into the PangoAttrList. It will be inserted after all other
 attributes with a matching start_index.

    list : a PangoAttrList
    attr : the attribute to insert. Ownership of this value is assumed by the list.
           [transfer full]
"
  (if (or (null list) (and (cffi:pointerp list) (cffi:null-pointer-p list)))
      (error "CFFI/PANGO-ATTR-LIST-INSERT invoked with null LIST")
      (if (or (null attribute) (and (cffi:pointerp attribute) (cffi:null-pointer-p attribute)))
	  (error "CFFI/PANGO-ATTR-LIST-INSERT invoked with null ATTRIBUTE")
	  (cffi:foreign-funcall "pango_attr_list_insert"
				:pointer list
				:pointer attribute
				:void)))
  list)


(defun cffi/pango-layout-set-attributes (layout attributes)
"
void                pango_layout_set_attributes         (PangoLayout *layout,
                                                         PangoAttrList *attrs);
Sets the text attributes for a layout object. References attrs, so the caller can unref its reference.

layout :
a PangoLayout
attrs :
a PangoAttrList, can be NULL. [allow-none][transfer full]
"
  (if (or (null layout) (and (cffi:pointerp layout) (cffi:null-pointer-p layout)))
      (error "CFFI/PANGO-LAYOUT-SET-ATTRIBUTES invoked with null LAYOUT")
      (let ((attributes (or attributes (cffi:null-pointer))))
	(cffi:foreign-funcall "pango_layout_set_attributes"
			      :pointer layout
			      :pointer attributes
			      :void)))
  layout)


(defun cffi/pango-attr-list-unref (list)
"
 void pango_attr_list_unref (PangoAttrList *list);

 Decrease the reference count of the given attribute list by one. If the result is
 zero, free the attribute list and the attributes it contains.

    list : a PangoAttrList, may be NULL
"
  (let ((list (or list (cffi:null-pointer))))
    (cffi:foreign-funcall "pango_attr_list_unref"
			  :pointer list
			  :void))
  nil)


(defun cffi/gdk-draw-layout (drawable gcontext x y layout)
"
Render a PangoLayout onto a GDK drawable

If the layout's PangoContext has a transformation matrix set, then x and y
specify the position of the top left corner of the bounding box (in device
space) of the transformed layout.

If you're using GTK+, the usual way to obtain a PangoLayout is
gtk_widget_create_pango_layout().

    drawable - the drawable on which to draw string
    gc       - base graphics context to use
    x        - the X position of the left of the layout (in pixels)
    y        - the Y position of the top of the layout (in pixels)
    layout   - a PangoLayout
"
  ;; void gdk_draw_layout (GdkDrawable *drawable, GdkGC *gc,
  ;;                       gint x, gint y, PangoLayout *layout);
  (warn "CFFI/GDK-DRAW-LAYOUT ENTERED WITH X=~a Y=~a" x y)
  (cond ((or (null drawable) (cffi:null-pointer-p drawable))
	 (error "CFFI/GDK-DRAW-LAYOUT called with null DRAWABLE"))
	((or (null gcontext) (cffi:null-pointer-p gcontext))
	 (error "CFFI/GDK-DRAW-LAYOUT called with null GCONTEXT"))
	((or (null layout) (cffi:null-pointer-p layout))
	 (error "CFFI/GDK-DRAW-LAYOUT called with null LAYOUT"))
	(t (cffi:foreign-funcall "gdk_draw_layout"
				 :pointer drawable
				 :pointer gcontext
				 gint x
				 gint y
				 :pointer layout
				 :void))))


;; FIXME: should we use pango-layout-get-spacing for inter-line spacing?
;; FIXME: these extents *could have* -ve x + y...
(defun cffi/pango-layout-get-pixel-extents (layout ink-rect logical-rect)
"
Computes the logical and ink extents of layout in device units. This function
just calls pango_layout_get_extents() followed by two pango_extents_to_pixels()
calls, rounding ink_rect and logical_rect such that the rounded rectangles
fully contain the unrounded one (that is, passes them as first argument to
pango_extents_to_pixels()).

    layout       - a PangoLayout
    ink_rect     - rectangle used to store the extents of the layout as drawn
                   or NULL to indicate that the result is not needed.
    logical_rect - rectangle used to store the logical extents of the layout
                   or NULL to indicate that the result is not needed. 
"
  ;; void pango_layout_get_pixel_extents (PangoLayout *layout,
  ;;                                      PangoRectangle *ink_rect,
  ;;                                      PangoRectangle *logical_rect);
  (if (or (null layout) (cffi:null-pointer-p layout))
      (error "CFFI/PANGO-LAYOUT-GET-PIXEL-EXTENTS called with null LAYOUT")
      (let ((ink-rect (or ink-rect (cffi:null-pointer)))
	    (logical-rect (or logical-rect (cffi:null-pointer))))
	(cffi:foreign-funcall "pango_layout_get_pixel_extents"
			      :pointer layout
			      :pointer ink-rect
			      :pointer logical-rect
			      :void))))


(defun cffi/pango-font-get-metrics (font language)
"
Gets overall metric information for a font. Since the metrics may be substantially
different for different scripts, a language tag can be provided to indicate that
the metrics should be retrieved that correspond to the script(s) used by that language.

If font is NULL, this function gracefully sets some sane values in the output
variables and returns.

    font     - a PangoFont
    language - language tag used to determine which script to get the metrics for, or
               NULL to indicate to get the metrics for the entire font.
    Returns  - a PangoFontMetrics object. The caller must call pango_font_metrics_unref()
               when finished using the object. 
"
  ;; PangoFontMetrics * pango_font_get_metrics (PangoFont *font,
  ;;                                            PangoLanguage *language);
  (let* ((font (or font (cffi:null-pointer)))
	 (language (or language (cffi:null-pointer)))
	 (metrics (cffi:foreign-funcall "pango_font_get_metrics"
					:pointer font
					:pointer language
					:pointer)))
    (pointer-or-nil metrics)))


(defun cffi/pango-language-get-default ()
"
Returns the PangoLanguage for the current locale of the process. Note that this can
change over the life of an application.

On Unix systems, this is the return value is derived from setlocale(LC_CTYPE, NULL),
and the user can affect this through the environment variables LC_ALL, LC_CTYPE or
LANG (checked in that order). The locale string typically is in the form lang_COUNTRY,
where lang is an ISO-639 language code, and COUNTRY is an ISO-3166 country code. For
instance, sv_FI for Swedish as written in Finland or pt_BR for Portuguese as written
in Brazil.

On Windows, the C library does not use any such environment variables, and setting
them won't affect the behavior of functions like ctime(). The user sets the locale
through the Regional Options in the Control Panel. The C library (in the setlocale()
function) does not use country and language codes, but country and language names
spelled out in English. However, this function does check the above environment
variables, and does return a Unix-style locale string based on either said environment
variables or the thread's current locale.

Your application should call setlocale(LC_ALL, \"\"); for the user settings to take effect.
Gtk+ does this in its initialization functions automatically (by calling gtk_set_locale()).
See man setlocale for more details.

    Returns - the default language as a PangoLanguage, must not be freed. 
"
  ;; PangoLanguage * pango_language_get_default (void);
  (let ((language (cffi:foreign-funcall "pango_language_get_default" :pointer)))
    (pointer-or-nil language)))


(defun cffi/pango-font-description-to-string (desc)
"
Creates a string representation of a font description. See
pango_font_description_from_string() for a description of the format of the string
representation. The family list in the string description will only have a
terminating comma if the last word of the list is a valid style option.

    desc    - a PangoFontDescription
    Returns - a new string that must be freed with g_free(). 
"
  ;; char * pango_font_description_to_string (const PangoFontDescription *desc);
  (if (or (null desc) (cffi:null-pointer-p desc))
      (error "CFFI/PANGO-FONT-DESCRIPTION-TO-STRING called with null DESCRIPTION")
      (let ((string (cffi:foreign-funcall "pango_font_description_to_string"
					  :pointer desc
					  :string)))
	string)))


(defun cffi/pango-font-description-from-string (string)
"
Creates a new font description from a string representation in the form
`[FAMILY-LIST] [STYLE-OPTIONS] [SIZE]', where FAMILY-LIST is a comma separated
list of families optionally terminated by a comma, STYLE_OPTIONS is a
whitespace separated list of words where each WORD describes one of style,
variant, weight, stretch, or gravity, and SIZE is a decimal number (size in
points) or optionally followed by the unit modifier `px' for absolute size. Any
one of the options may be absent. If FAMILY-LIST is absent, then the
family_name field of the resulting font description will be initialized to NULL.
If STYLE-OPTIONS is missing, then all style options will be set to the default
values. If SIZE is missing, the size in the resulting font description will be
set to 0.

    str     - string representation of a font description.
    Returns - a new PangoFontDescription.
"
  ;; PangoFontDescription* pango_font_description_from_string (const char *str);
  (let ((description (cffi:foreign-funcall "pango_font_description_from_string"
					   :string string
					   :pointer)))
    (pointer-or-nil description)))


(defun cffi/pango_layout_get_baseline (layout)
"
 int pango_layout_get_baseline (PangoLayout *layout);

 Gets the Y position of baseline of the first line in layout.

    layout : a PangoLayout

    Returns : baseline of first line, from top of layout.
"
  (if (or (null layout) (cffi:null-pointer-p layout))
      (error "CFFI/PANGO_LAYOUT_GET_BASELINE called with null LAYOUT")
      (cffi:foreign-funcall "pango_layout_get_baseline"
			    :pointer layout
			    :int)))


;;; ========================================================================
;;;
;;;    Screen (DUIM = 'display') manipulation
;;;

(defun cffi/gdk-screen-height (&optional screen)
  (let ((screen (or screen (cffi:foreign-funcall "gdk_screen_get_default" GdkScreen*))))
    (cffi:foreign-funcall "gdk_screen_get_height" GdkScreen* screen gint)))


(defun cffi/gdk-screen-height-mm (&optional screen)
  (let ((screen (or screen (cffi:foreign-funcall "gdk_screen_get_default" GdkScreen*))))
    (cffi:foreign-funcall "gdk_screen_get_height_mm" GdkScreen* screen gint)))


(defun cffi/gdk-screen-width (&optional screen)
  (let ((screen (or screen (cffi:foreign-funcall "gdk_screen_get_default" GdkScreen*))))
    (cffi:foreign-funcall "gdk_screen_get_width" GdkScreen* screen gint)))


(defun cffi/gdk-screen-width-mm (&optional screen)
  (let ((screen (or screen (cffi:foreign-funcall "gdk_screen_get_default" GdkScreen*))))
    (cffi:foreign-funcall "gdk_screen_get_width_mm" GdkScreen* screen gint)))

;;;
;;; Support for 'extended' types
;;;

(defun cffi/gdk-pixbuf-get-type ()
  (cffi:foreign-funcall "gdk_pixbuf_get_type"
			:uchar))


(defun cffi/gtk-vbox-new ()
"
    homogeneous - all children given equal space
    spacing     - additional vertical space between children

Sinks initial floating reference
"
  ;; GtkWidget* gtk_vbox_new (gboolean homogeneous, gint spacing)

  (FORMAT T "~%CFFI/GTK-VBOX-NEW ENTERED")

  (let ((gtk-widget (cffi:foreign-funcall "gtk_vbox_new"
					  gboolean 0
					  gint 0
					  GtkWidget*)))

    (FORMAT T "~%    GOT WIDGET ~A" GTK-WIDGET)

    (sink-floating-ref gtk-widget)
    (pointer-or-nil gtk-widget)))

;;; ========================================================================
;;;
;;;    Containers
;;;

;;; ------ Container

(defun cffi/gtk-container (arg)
  arg)


(defun cffi/gtk-container-set-border-width (container border-width)
;;  (gtk-debug "gtk-container-set-border-width ~a ~a"
;;		      container border-width)
  ;; void gtk_container_set_border_width (GtkContainer* container,
  ;;                                      guint border_width)
  (if (or (null container) (cffi:null-pointer-p container))
      (error "CFFI/GTK-CONTAINER-SET-BORDER-WIDTH invoked with null CONTAINER")
      (cffi:foreign-funcall "gtk_container_set_border_width"
			    GtkContainer* container
			    guint border-width
			    :void))
  container)


(defun cffi/gtk-container-add (container widget)
;;  (gtk-debug "gtk-container-add ~a ~a" container widget)
  ;; void gtk_container_add (GtkContainer* container, GtkWidget* widget)
  (if (or (null container) (cffi:null-pointer-p container))
      (error "CFFI/GTK-CONTAINER-ADD invoked with null CONTAINER")
      (if (or (null widget) (cffi:null-pointer-p widget))
	  (error "CFFI/GTK-CONTAINER-ADD invoked with null WIDGET")
	  (cffi:foreign-funcall "gtk_container_add"
				GtkContainer* container
				GtkWidget* widget
				:void)))
  container)


(defun cffi/gtk-container-remove (container widget)
"
Removes widget from container. widget must be inside container. Note that
container will own a reference to widget, and that this may be the last
reference held; so removing a widget from its container can destroy that
widget. If you want to use widget again, you need to add a reference to it
while it's not inside a container, using g_object_ref(). If you don't want
to use widget again it's usually more efficient to simply destroy it directly
using gtk_widget_destroy() since this will remove it from the container and
help break any circular reference count cycles.

    container - a GtkContainer
    widget    - a current child of container
"
  ;; void gtk_container_remove (GtkContainer *container, GtkWidget *widget);
  (if (or (null container) (cffi:null-pointer-p container))
      (error "CFFI/GTK-CONTAINER-REMOVE invoked with null CONTAINER")
      (if (or (null widget) (cffi:null-pointer-p widget))
	  (error "CFFI/GTK-CONTAINER-REMOVE invoked with null WIDGET")
	  (cffi:foreign-funcall "gtk_container_remove"
				:pointer container
				:pointer widget
				:void)))
  container)


(defun cffi/gtk-container-get-children (container)
  ;; GList* gtk_container_get_children (GtkContainer* container);
  (if (or (null container) (cffi:null-pointer-p container))
      (error "CFFI/GTK-CONTAINER-GET-CHILDREN invoked with null CONTAINER")
      (let ((children (cffi:foreign-funcall "gtk_container_get_children"
					    GtkContainer* container
					    GList*)))
	(pointer-or-nil children))))


(defun cffi/gtk-container-set-resize-mode (container resize-mode)
  ;; void gtk_container_set_resize_mode (GtkContainer* container,
  ;;                                     GtkResizeMode resize_mode)
  (if (or (null container) (cffi:null-pointer-p container))
      (error "CFFI/GTK-CONTAINER-SET-RESIZE-MODE invoked with null CONTAINER")
      (cffi:foreign-funcall "gtk_container_set_resize_mode"
			    GtkContainer* container
			    GtkResizeMode resize-mode
			    :void))
  container)


;;; ------ Fixed

;;; A container that allows you to position widgets at fixed
;;; coordinates...

(defun cffi/gtk-fixed (arg)
  arg)


(defun cffi/gtk-fixed-set-has-window (fixed window?)
;;  (gtk-debug "gtk-fixed-set-has-window fixed=~a, window?=~a"
;;		      fixed window?)
  ;; void gtk_fixed_set_has_window (GtkFixed *fixed, gboolean has_window);
  (if (or (null fixed) (cffi:null-pointer-p fixed))
      (error "CFFI/GTK-FIXED-SET-HAS-WINDOW invoked with null FIXED")
      (cffi:foreign-funcall "gtk_fixed_set_has_window"
			    GtkFixed* fixed
			    gboolean  window?
			    :void))
  fixed)


(defun cffi/gtk-fixed-get-has-window (fixed)
;;  (gtk-debug "gtk-fixed-get-has-window fixed=~a" fixed)
  ;; gboolean gtk_fixed_get_has_window (GtkFixed* fixed);
  (if (or (null fixed) (cffi:null-pointer-p fixed))
      (error "CFFI/GTK-FIXED-GET-HAS-WINDOW invoked with null FIXED")
      (let ((window? (cffi:foreign-funcall "gtk_fixed_get_has_window"
					   GtkFixed* fixed
					   gboolean)))
	window?)))


(defun cffi/gtk-fixed-new ()
"
Sinks initial floating reference
"
;;  (gtk-debug "GTK-CFFI:GTK-FIXED-NEW")
  ;; GtkWidget* gtk_fixed_new (void)
  (let ((gtk-fixed (cffi:foreign-funcall "gtk_fixed_new" GtkWidget*)))
;;    ;; FIXME: we should do this from the b/end code rather than here.
;;    ;; By default, GtkFixed do not have their own X-window. This is
;;    ;; a change in behaviour. (seems not to affect our problem :( )
;;    (unless (null-ptr-p gtk-fixed)
;;      (let ((has-window? (gtk-fixed-get-has-window gtk-fixed)))
;;	(gtk-debug " + fixed has-window?=~a" has-window?)
;;	(unless (eql has-window? +true+)
;;	  (gtk-fixed-set-has-window gtk-fixed +true+))))
    (sink-floating-ref gtk-fixed)
    (pointer-or-nil gtk-fixed)))


(defun cffi/gtk-fixed-put (fixed widget x y)
  ;; void gtk_fixed_put (GtkFixed* fixed, GtkWidget* widget
  ;;                     gint x, gint y)
  (if (or (null fixed) (cffi:null-pointer-p fixed))
      (error "CFFI/GTK-FIXED-PUT invoked with null FIXED")
      (if (or (null widget) (cffi:null-pointer-p widget))
	  (error "CFFI/GTK-FIXED-PUT invoked with null WIDGET")
	  (cffi:foreign-funcall "gtk_fixed_put"
				GtkFixed* fixed
				GtkWidget* widget
				gint x gint y
				:void)))
  fixed)


(defun cffi/gtk-fixed-move (fixed widget x y)
  ;; void gtk_fixed_move (GtkFixed* fixed, GtkWidget* widget,
  ;;                      gint x, gint y)
  (if (or (null fixed) (cffi:null-pointer-p fixed))
      (error "CFFI/GTK-FIXED-MOVE invoked with null FIXED")
      (if (or (null widget) (cffi:null-pointer-p widget))
	  (error "CFFI/GTK-FIXED-MOVE invoked with null WIDGET")
	  (cffi:foreign-funcall "gtk_fixed_move"
				GtkFixed* fixed
				GtkWidget* widget
				gint x gint y
				:void)))
  fixed)


;;; ------ Colour support

(defun cffi/gdk-colormap-get-system ()
  ;; GdkColormap* gdk_colormap_get_system (void);
  (let ((cmap (cffi:foreign-funcall "gdk_colormap_get_system" GdkColormap*)))
    (pointer-or-nil cmap)))


(defun cffi/gdk-colormap-get-visual (colormap)
  ;; GdkVisual* gdk_colormap_get_visual (GdkColormap* colormap);
  (if (or (null colormap) (cffi:null-pointer-p colormap))
      (error "CFFI/GDK-COLORMAP-GET-VISUAL invoked with null COLORMAP")
      (let ((visual (cffi:foreign-funcall "gdk_colormap_get_visual"
					  GdkColormap* colormap
					  GdkVisual*)))
	(pointer-or-nil visual))))


(defun cffi/gdk-colormap-free-colors (colormap colors count)
  ;; void gdk_colormap_free_colors (GdkColormap* colormap,
  ;;                                GdkColor*    colors,
  ;;                                gint         n_colors);
  (if (or (null colormap) (cffi:null-pointer-p colormap))
      (error "CFFI/GDK-COLORMAP-FREE-COLORS invoked with null COLORMAP")
      (if (or (null colors) (cffi:null-pointer-p colors))
	  (error "CFFI/GDK-COLORMAP-FREE-COLORS invoked with null COLORS")
	  (cffi:foreign-funcall "gdk_colormap_free_colors"
				GdkColormap* colormap
				GdkColor*    colors
				gint         count
				:void)))
  colormap)


(defun cffi/gdk-colormap-alloc-color (colormap color writeable? best-match?)
  ;; gboolean gdk_colormap_alloc_color (GdkColormap* colormap,
  ;;                                    GdkColor*    color,
  ;;                                    gboolean     writeable,
  ;;                                    gboolean     best_match);
  (if (or (null colormap) (cffi:null-pointer-p colormap))
      (error "CFFI/GDK-COLORMAP-ALLOC-COLOR invoked with null COLORMAP")
      (if (or (null color) (cffi:null-pointer-p color))
	  (error "CFFI/GDK-COLORMAP-ALLOC-COLOR invoked with null COLOR")
	  (let ((result (cffi:foreign-funcall "gdk_colormap_alloc_color"
					      GdkColormap* colormap
					      GdkColor*    color
					      gboolean     writeable?
					      gboolean     best-match?
					      gboolean)))
	    result))))


(defun cffi/gdk-color-parse (name colour)
  ;; gboolean gdk_color_parse (const gchar* spec, GdkColor* color);
  (if (or (null name) (and (cffi:pointerp name) (cffi:null-pointer-p name)))
      (error "CFFI/GDK-COLOR-PARSE invoked with null NAME")
      (if (or (null colour) (cffi:null-pointer-p colour))
	  (error "CFFI/GDK-COLOR-PARSE invoked with null COLOUR")
	  (let ((result (cffi:foreign-funcall "gdk_color_parse"
					      :string   name
					      GdkColor* colour
					      gboolean)))
	    result))))


(defun cffi/gdk-colormap-query-color (colormap pixel result)
  ;; void gdk_colormap_query_color (GdkColormap* colormap,
  ;;                                gulong       pixel,
  ;;                                GdkColor*    result);
  (if (or (null colormap) (cffi:null-pointer-p colormap))
      (error "CFFI/GDK-COLORMAP-QUERY-COLOR invoked with null COLORMAP")
      (if (or (null result) (cffi:null-pointer-p result))
	  (error "CFFI/GDK-COLORMAP-QUERY-COLOR invoked with null RESULT")
	  (cffi:foreign-funcall "gdk_colormap_query_color"
				GdkColormap* colormap
				gulong       pixel
				GdkColor*    result
				:void)))
  result)


(defun cffi/gdk-color-change (colormap color)
  ;; gint gdk_color_change (GdkColormap* colormap, GdkColor* color);
  (if (or (null colormap) (cffi:null-pointer-p colormap))
      (error "CFFI/GDK-COLOR-CHANGE invoked with null COLORMAP")
      (if (or (null color) (cffi:null-pointer-p color))
	  (error "CFFI/GDK-COLOR-CHANGE invoked with null COLOR")
	  (let ((result (cffi:foreign-funcall "gdk_color_change"
					      GdkColormap* colormap
					      GdkColor*    color
					      gint)))
	    result))))


(defun cffi/gtk-widget-modify-bg (widget state color)
"
Sets the background color for a widget in a particular state. All other
style values are left untouched. See also gtk_widget_modify_style().

Note that 'no window' widgets (which have the GTK_NO_WINDOW flag set)
draw on their parent container's window and thus may not draw any
background themselves. This is the case for e.g. GtkLabel. To modify
the background of such widgets, you have to set the background color
on their parent; if you want to set the background of a rectangular
area around a label, try placing the label in a GtkEventBox widget and
setting the background color on that.

    widget - a GtkWidget
    state  - the state for which to set the background color
    color  - the color to assign (does not need to be allocated), or
             NULL to undo the effect of previous calls to of
             gtk_widget_modify_bg().
"
  ;; void gtk_widget_modify_bg (GtkWidget *widget,
  ;;                            GtkStateType state,
  ;;                            const GdkColor *color);
  (if (or (null widget) (cffi:null-pointer-p widget))
      (error "CFFI/GTK-WIDGET-MODIFY-BG invoked with null WIDGET")
      (let ((color (or color (cffi:null-pointer))))
	(cffi:foreign-funcall "gtk_widget_modify_bg"
			      :pointer widget
			      gint    state
			      :pointer color
			      :void)))
  widget)


#-(and)
(defun gdk-visual-get-best-type
"
Return the best available visual type for the default GDK screen.

    Returns - best visual type
"
  ;; GdkVisualType gdk_visual_get_best_type (void);
  (cffi:foreign-funcall "gdk_visual_get_best_type"
			:int))


;;; ------ Queueing drawing

(defun cffi/gtk-widget-queue-draw (widget)
"
 void gtk_widget_queue_draw (GtkWidget *widget);

 Equivalent to calling gtk_widget_queue_draw_area()
 for the entire area of a widget.

    widget : a GtkWidget
"
  ;; void gtk_widget_queue_draw (GtkWidget* widget);
  (if (or (null widget) (cffi:null-pointer-p widget))
      (error "CFFI/GTK-WIDGET-QUEUE-DRAW invoked with null WIDGET")
      (cffi:foreign-funcall "gtk_widget_queue_draw" GtkWidget* widget :void))
  widget)


;;; ========================================================================
;;;
;;;    Graphics contexts
;;;

(defun cffi/gdk-gc-get-values (gcontext gc-values)
  ;; void gdk_gc_get_values (GdkGC* gc, GdkGCValues* values)
  (if (or (null gcontext) (cffi:null-pointer-p gcontext))
      (error "CFFI/GDK-GC-GET-VALUES invoked with null GCONTEXT")
      (if (or (null gc-values) (cffi:null-pointer-p gc-values))
	  (error "CFFI/GDK-GC-GET-VALUES invoked with null GC-VALUES")
	  (cffi:foreign-funcall "gdk_gc_get_values"
				GdkGC* gcontext
				GdkGCValues* gc-values
				:void)))
  gc-values)


(defun cffi/gdk-gc-new (drawable)
"
Does not inherit from GInitiallyUnowned so no floating reference.

Return has ref count = 1
"
  ;; GdkGC* gdk_gc_new (GdkDrawable* drawable);
  (warn "CFFI/GDK-GC-NEW deprecated; use Cairo for rendering")
  (if (or (null drawable) (cffi:null-pointer-p drawable))
      (error "CFFI/GDK-GC-NEW invoked with null DRAWABLE")
      (let ((gc (cffi:foreign-funcall "gdk_gc_new"
				      GdkDrawable* drawable
				      GdkGC*)))
	(pointer-or-nil gc))))


(defun cffi/gdk-gc-copy (dest-gcontext src-gcontext)
  ;; void gdk_gc_copy (GdkGC* dst_gc, GdkGC* src_gc);
  (if (or (null dest-gcontext) (cffi:null-pointer-p dest-gcontext))
      (error "CFFI/GDK-GC-COPY invoked with null DEST-GCONTEXT")
      (if (or (null src-gcontext) (cffi:null-pointer-p src-gcontext))
	  (error "CFFI/GDK-GC-COPY invoked with null SRC-GCONTEXT")
	  (cffi:foreign-funcall "gdk_gc_copy"
				GdkGC* dest-gcontext
				GdkGC* src-gcontext
				:void)))
  dest-gcontext)


(defun cffi/gdk-gc-set-clip-rectangle (gcontext rect)
"
 Sets the clip mask for a graphics context from a rectangle. The clip mask is
 interpreted relative to the clip origin. (See gdk_gc_set_clip_origin()).

    gc        - a GdkGC.
    rectangle - the rectangle to clip to.
"
  ;; void gdk_gc_set_clip_rectangle (GdkGC *gc, GdkRectangle *rectangle);
  (deprecated "CFFI/GDK-GC-SET-CLIP-RECTANGLE - USE CAIRO-RECTANGLE() AND CAIRO-CLIP()")
  (if (or (null gcontext) (cffi:null-pointer-p gcontext))
      (error "CFFI/GDK-GC-SET-CLIP-RECTANGLE invoked with null GCONTEXT")
      (let ((rect (or rect (cffi:null-pointer))))
	(cffi:foreign-funcall "gdk_gc_set_clip_rectangle"
			      :pointer gcontext
			      :pointer rect
			      :void)))
  gcontext)


(defun cffi/gdk-gc-set-line-attributes (gcontext
				   line-width line-style
				   cap-style join-style)
  ;; void gdk_gc_set_line_attributes (GdkGC* gc, gint line_width,
  ;;                                 GdkLineStyle line_style,
  ;;                                 GdkCapStyle  cap_style,
  ;;                                 GdkJoinStyle join_style)
  (if (or (null gcontext) (cffi:null-pointer-p gcontext))
      (error "CFFI/GDK-GC-SET-LINE-ATTRIBUTES invoked with null GCONTEXT")
      (cffi:foreign-funcall "gdk_gc_set_line_attributes"
			    GdkGC* gcontext
			    gint line-width
			    GdkLineStyle line-style
			    GdkCapStyle cap-style
			    GdkJoinStyle join-style
			    :void))
  gcontext)


(defun cffi/gdk-gc-set-fill (gc fill)
  ;; void gdk_gc_set_fill (GdkGC* gc, GdkFill fill);
  (if (or (null gc) (cffi:null-pointer-p gc))
      (error "CFFI/GDK-GC-SET-FILL invoked with null GC")
      (cffi:foreign-funcall "gdk_gc_set_fill"
			    GdkGC*  gc
			    GdkFill fill
			    :void))
  gc)


(defun cffi/gdk-gc-set-foreground (gc colour)
  ;; void gdk_gc_set_foreground (GdkGC* gc, const GdkColor* color);
  (if (or (null gc) (cffi:null-pointer-p gc))
      (error "CFFI/GDK-GC-SET-FOREGROUND invoked with null GC")
      (if (or (null colour) (cffi:null-pointer-p colour))
	  (error "CFFI/GDK-GC-SET-FOREGROUND invoked with null COLOUR")
	  (cffi:foreign-funcall "gdk_gc_set_foreground"
				GdkGC*    gc
				GdkColor* colour
				:void)))
  gc)


(defun cffi/gdk-gc-set-background (gc colour)
  ;; void gdk_gc_set_background (GdkGC* gc, const GdkColor* color);
  (if (or (null gc) (cffi:null-pointer-p gc))
      (error "CFFI/GDK-GC-SET-BACKGROUND invoked with null GC")
      (if (or (null colour) (cffi:null-pointer-p colour))
	  (error "CFFI/GDK-GC-SET-BACKGROUND invoked with null COLOUR")
	  (cffi:foreign-funcall "gdk_gc_set_background"
				GdkGC*    gc
				GdkColor* colour
				:void)))
  gc)


(defun cffi/gdk-gc-set-function (gc function)
  ;; void gdk_gc_set_function (GdkGC* gc, GdkFunction function);
  (if (or (null gc) (cffi:null-pointer-p gc))
      (error "CFFI/GDK-GC-SET-FUNCTION invoked with null GC")
      (cffi:foreign-funcall "gdk_gc_set_function"
			    GdkGC*      gc
			    GdkFunction function
			    :void))
  gc)


(defun cffi/gdk-gc-set-stipple (gc stipple)
"
Set the stipple bitmap for a graphics context. The stipple will only be used if
the fill mode is GDK_STIPPLED or GDK_OPAQUE_STIPPLED.

    gc      : a GdkGC.
    stipple : the new stipple bitmap.
"
  ;; void gdk_gc_set_stipple (GdkGC *gc, GdkPixmap *stipple);
  (if (or (null gc) (cffi:null-pointer-p gc))
      (error "CFFI/GDK-GC-SET-STIPPLE invoked with null GC")
      (if (or (null stipple) (cffi:null-pointer-p stipple))
	  (error "CFFI/GDK-GC-SET-STIPPLE invoked with null STIPPLE")
	  (cffi:foreign-funcall "gdk_gc_set_stipple"
				:pointer gc
				:pointer stipple
				:void)))
  gc)


(defun cffi/gdk-gc-set-tile (gc tile)
"
 Set a tile pixmap for a graphics context. This will only be used if the fill mode is GDK_TILED.

    gc   : a GdkGC.
    tile : the new tile pixmap.
"
  ;; void gdk_gc_set_tile (GdkGC *gc, GdkPixmap *tile);
  (if (or (null gc) (cffi:null-pointer-p gc))
      (error "CFFI/GDK-GC-SET-TILE invoked with null GC")
      (if (or (null tile) (cffi:null-pointer-p tile))
	  (error "CFFI/GDK-GC-SET-TILE invoked with null TILE")
	  (cffi:foreign-funcall "gdk_gc_set_tile"
				:pointer gc
				:pointer tile
				:void)))
  gc)


(defun cffi/gdk-gc-set-ts-origin (gc x y)
"
 Set the origin when using tiles or stipples with the GC. The tile or stipple
 will be aligned such that the upper left corner of the tile or stipple will
 coincide with this point.

    gc : a GdkGC.
    x  : the x-coordinate of the origin.
    y  : the y-coordinate of the origin.
"
  ;; void gdk_gc_set_ts_origin (GdkGC *gc, gint x, gint y);
  (if (or (null gc) (cffi:null-pointer-p gc))
      (error "CFFI/GDK-GC-SET-TS-ORIGIN invoked with null GC")
      (cffi:foreign-funcall "gdk_gc_set_ts_origin"
			    :pointer gc
			    gint x
			    gint y
			    :void))
  gc)


;;; ========================================================================
;;;
;;;    MISCELLANEOUS
;;;

;;; ------ Beep

(defun cffi/gdk-beep ()
  ;; void gdk_beep (void);
  (cffi:foreign-funcall "gdk_beep" :void)
  t)


;;; ------ Flushing

(defun cffi/gdk-flush ()
  ;; void gdk_flush (void);
  (cffi:foreign-funcall "gdk_flush" :void)
  t)


;;; ------ Pointer grabbing

(defun cffi/gdk-pointer-grab (widget owner-events mask confine-to-cursor unknown time)
  (declare (ignore widget owner-events mask confine-to-cursor unknown time))  ;; until method is implemented!
  (not-yet-implemented "gdk-pointer-grab"))


(defun cffi/gdk-pointer-ungrab (time)
"
 Ungrabs the pointer, if it is grabbed by this application.

    time_: a timestamp from a GdkEvent, or GDK_CURRENT_TIME if no
           timestamp is available.
"
  (declare (ignore time))
  (not-yet-implemented "gdk-pointer-ungrab"))


;;; ------ Selections

(defun cffi/gtk-editable-get-selection-bounds (editable start-pos end-pos)
"
 gboolean gtk_editable_get_selection_bounds (GtkEditable *editable,
                                             gint *start_pos, gint *end_pos);

 Retrieves the selection bound of the editable. start_pos will be filled with the start
 of the selection and end_pos with end. If no text was selected both will be identical
 and FALSE will be returned.

 Note that positions are specified in characters, not bytes.

    editable : a GtkEditable
    start_pos : location to store the starting position, or NULL. [out][allow-none]
    end_pos : location to store the end position, or NULL. [out][allow-none]

    Returns : TRUE if an area is selected, FALSE otherwise
"
  (if (or (null editable) (cffi:null-pointer-p editable))
      (error "CFFI/GTK-EDITABLE-GET-SELECTION-BOUNDS invoked with null EDITABLE")
      (if (or (null start-pos) (cffi:null-pointer-p start-pos))
	  (error "CFFI/GTK-EDITABLE-GET-SELECTION-BOUNDS invoked with null START-POS")
	  (if (or (null end-pos) (cffi:null-pointer-p end-pos))
	      (error "CFFI/GTK-EDITABLE-GET-SELECTION-BOUNDS invoked with null END-POS")
	      (let ((selection? (cffi:foreign-funcall "gtk_editable_get_selection_bounds"
						      :pointer editable
						      :pointer start-pos
						      :pointer end-pos
						      :boolean)))
		selection?)))))


;;; ------ GTK-String

(defun cffi/gtk-string (arg)
  (not-yet-implemented "gtk-string ~a" arg)
  arg)


;;; ------ GTK-Object

(defun cffi/gtk-object (arg)
  arg)


;;; ------ GtkDialog


(defun cffi/gtk-dialog (dialog)
  dialog)


(defun cffi/gtk-dialog-new ()
  "GtkWidget * gtk_dialog_new (void);"
  (let ((dialog (cffi:foreign-funcall "gtk_dialog_new"
				      :pointer)))
    (pointer-or-nil dialog)))


(defun cffi/gtk-dialog-run (dialog)
  ;; gint gtk_dialog_run (GtkDialog* dialog);
  (if (or (null dialog) (cffi:null-pointer-p dialog))
      (error "CFFI/GTK-DIALOG-RUN invoked with null DIALOG")
      (let ((response-id (cffi:foreign-funcall "gtk_dialog_run"
					       GtkDialog* dialog
					       gint)))
	response-id)))


(defun cffi/gtk-dialog-get-content-area (dialog)
  "GtkWidget * gtk_dialog_get_content_area (GtkDialog *dialog);

Returns the content area of dialog .

Parameters

    - dialog :: a GtkDialog
 
Returns

    the content area GtkVBox. [transfer none]

Since: 2.14"
  (let ((vbox (cffi:foreign-funcall "gtk_dialog_get_content_area"
				    :pointer dialog
				    :pointer)))
    (pointer-or-nil vbox)))


;;; ------ GtkFileChooserDialog

;;; FIXME: ANOTHER VARIADIC METHOD... WHAT TO DO ABOUT THIS?
(defun cffi/gtk-file-chooser-dialog-new (title parent action ;;&rest buttons)
				    ;; this should be varargs, but for now...
				    cancel-btn cancel-resp
				    action-btn action-resp
								 terminator)
"
Creates a new GtkFileChooserDialog. This function is analogous to
gtk_dialog_new_with_buttons().

Parameters

title - Title of the dialog, or NULL.  [allow-none]
parent - Transient parent of the dialog, or NULL.  [allow-none]
action - Open or save mode for the dialog
first_button_text - stock ID or text to go in the first button, or
                    NULL.  [allow-none]

Varargs

response ID for the first button, then additional (button, id) pairs,
ending with NULL

This should be a top-level window, and as such should not have
a floating reference that needs sinking.
"
  ;; GtkWidget* gtk_file_chooser_dialog_new (const gchar* title,
  ;;                                         GtkWindow* parent,
  ;;                                         GtkFileChooserAction action,
  ;;                                         const gchar* first_button_text,
  ;;                                         ...);

  (let* ((dialog (cffi:foreign-funcall "gtk_file_chooser_dialog_new"
				       :string    title
				       GtkWindow* parent
				       gint       action ; enum GtkFileChooserAction
				       :string    cancel-btn
				       gint       cancel-resp ; enum GtkResponseType
				       :string    action-btn
				       gint       action-resp ; enum GtkResponseType
				       :string    (or terminator (cffi:null-pointer))
				       GtkWidget*)))
    dialog))


(defun cffi/gtk-file-chooser-get-filename (chooser)
  ;; gchar* gtk_file_chooser_get_filename (GtkFileChooser* chooser)
  (if (or (null chooser) (cffi:null-pointer-p chooser))
      (error "CFFI/GTK-FILE-CHOOSER-GET-FILENAME invoked with null CHOOSER")
      (let ((fname (cffi:foreign-funcall "gtk_file_chooser_get_filename"
					 GtkFileChooser* chooser
					 :string)))
	fname)))


;;; ------ GtkColorSelectionDialog

(defun cffi/gtk-color-selection-dialog-new (title)
"
This should be a top-level window, and as such should not have
a floating reference that needs sinking.
"
  ;; GtkWidget* gtk_color_selection_dialog_new (const gchar* title);
  (if (or (null title) (and (cffi:pointerp title) (cffi:null-pointer-p title)))
      (error "CFFI/GTK-COLOR-SELECTION-DIALOG-NEW invoked with null TITLE")
      (let ((dialog (cffi:foreign-funcall "gtk_color_selection_dialog_new"
					  :string title
					  GtkWidget*)))
	(pointer-or-nil dialog))))


(defun cffi/gtk-color-selection-get-current-color (colour-sel colour)
  ;; void gtk_color_selection_get_current_color (GtkColorSelection* colorsel,
  ;;                                             const GdkColor* color);
  (if (or (null colour-sel) (cffi:null-pointer-p colour-sel))
      (error "CFFI/GTK-COLOR-SELECTION-GET-CURRENT-COLOR invoked with null COLOUR-SEL")
      (if (or (null colour) (cffi:null-pointer-p colour))
	  (error "CFFI/GTK-COLOR-SELECTION-GET-CURRENT-COLOR invoked with null COLOUR")
	  (cffi:foreign-funcall "gtk_color_selection_get_current_color"
				GtkColorSelection* colour-sel
				GdkColor*          colour
				:void)))
  colour)


;;; ------ GtkFontSelectionDialog

(defun cffi/gtk-font-selection-dialog-new (title)
"
This should be a top-level window, and as such should not have
a floating reference that needs sinking.
"
  ;; GtkWidget* gtk_font_selection_dialog_new (const gchar* title);
  (if (or (null title) (and (cffi:pointerp title) (cffi:null-pointer-p title)))
      (error "CFFI/GTK-FONT-SELECTION-DIALOG-NEW invoked with null TITLE")
      (let ((widget (cffi:foreign-funcall "gtk_font_selection_dialog_new"
					  :string title
					  GtkWidget*)))
	(pointer-or-nil widget))))


;;; fixme: does the returned value need ref'ing? unref'ing?
(defun cffi/gtk-font-selection-dialog-get-font (dialog)
  ;; GdkFont* gtk_font_selection_dialog_get_font
  ;;                      (GtkFontSelectionDialog* dialog);
  (if (or (null dialog) (cffi:null-pointer-p dialog))
      (error "CFFI/GTK-FONT-SELECTION-DIALOG-GET-FONT invoked with null DIALOG")
      (let ((font (cffi:foreign-funcall "gtk_font_selection_dialog_get_font"
					GtkFontSelectionDialog* dialog
					GdkFont*)))
	(pointer-or-nil font))))


(defun cffi/gtk-font-selection-dialog-get-font-name (dialog)
"
 Gets the currently-selected font name. Note that this can be a different string
 than what you set with gtk_font_selection_set_font_name(), as the font
 selection widget may normalize font names and thus return a string with a
 different structure. For example, `Helvetica Italic Bold 12' could be
 normalized to `Helvetica Bold Italic 12'. Use pango_font_description_equal() if
 you want to compare two font descriptions.

    fontsel - a GtkFontSelection
    Returns - A string with the name of the current font, or NULL if no font is selected. You must free this string with g_free().
"
  ;; gchar* gtk_font_selection_get_font_name (GtkFontSelection *fontsel);
  (if (or (null dialog) (cffi:null-pointer-p dialog))
      (error "CFFI/GTK-FONT-SELECTION-DIALOG-GET-FONT-NAME invoked with null DIALOG")
      (let ((font (cffi:foreign-funcall "gtk_font_selection_dialog_get_font_name"
					GtkFontSelectionDialog* dialog
					:pointer)))
	(pointer-or-nil font))))


;;; ------ GtkStockItem stuff

(defun cffi/gtk-stock-lookup (id item)
  ;; gboolean gtk_stock_lookup (const gchar* stock_id, GtkStockItem* item);
  (if (or (null id) (and (cffi:pointerp id) (cffi:null-pointer-p id)))
      (error "CFFI/GTK-STOCK-LOOKUP invoked with null ID")
      (if (or (null item) (cffi:null-pointer-p item))
	  (error "CFFI/GTK-STOCK-LOOKUP invoked with null ITEM")
	  (let ((result (cffi:foreign-funcall "gtk_stock_lookup"
					      :string       id
					      GtkStockItem* item
					      gboolean)))
	    result))))

;;; ------ GtkImage

(defun cffi/gtk-image-new-from-pixmap (pixmap mask)
"
 GtkWidget * gtk_image_new_from_pixmap (GdkPixmap *pixmap, GdkBitmap *mask);

 Creates a GtkImage widget displaying pixmap with a mask. A GdkPixmap is a server-side
 image buffer in the pixel format of the current display. The GtkImage does not assume
 a reference to the pixmap or mask; you still need to unref them if you own references.
 GtkImage will add its own reference rather than adopting yours.

    pixmap : a GdkPixmap, or NULL. [allow-none]
    mask : a GdkBitmap, or NULL. [allow-none]

    Returns : a new GtkImage

Sinks initial floating reference
"
  (let* ((pixmap (or pixmap (cffi:null-pointer)))
	 (mask   (or mask   (cffi:null-pointer)))
	 (widget (cffi:foreign-funcall "gtk_image_new_from_pixmap"
				       :pointer pixmap
				       :pointer mask
				       :pointer)))
    (sink-floating-ref widget)
    (pointer-or-nil widget)))


(defun cffi/gtk-image-set-from-pixmap (image pixmap mask)
"
 void gtk_image_set_from_pixmap (GtkImage *image, GdkPixmap *pixmap, GdkBitmap *mask);

 See gtk_image_new_from_pixmap() for details.

    image : a GtkImage
    pixmap : a GdkPixmap or NULL. [allow-none]
    mask : a GdkBitmap or NULL. [allow-none]
"
  (if (or (null pixmap) (and (cffi:pointerp pixmap) (cffi:null-pointer-p pixmap)))
      (error "CFFI/GTK-IMAGE-SET-FROM-PIXMAP invoked with null PIXMAP")
      (let* ((pixmap (or pixmap (cffi:null-pointer)))
	     (mask   (or mask   (cffi:null-pointer))))
	(cffi:foreign-funcall "gtk_image_set_from_pixmap"
			      :pointer image
			      :pointer pixmap
			      :pointer mask
			      :void)))
  image)




;;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;    DYLAN -> CFFI UTILITIES
;;;
;;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defmacro cffi/with-stack-structure ((val type &rest opts) &body body)
  `(cffi:with-foreign-object (,val ,type ,@opts)
     ,@body))


(defun cffi/g-free (address)
  (if (or (null address) (cffi:null-pointer-p address))
      (error "CFFI/G-FREE invoked with null ADDRESS")
      (cffi:foreign-funcall "g_free" gpointer address :void))
  nil)




;;; ------ Gtk "stock" items

;;; #define GTK_STOCK_DIALOG_AUTHENTICATION \
;;;                                    "gtk-dialog-authentication"
;;; #define GTK_STOCK_DIALOG_INFO      "gtk-dialog-info"
;;; #define GTK_STOCK_DIALOG_WARNING   "gtk-dialog-warning"
;;; #define GTK_STOCK_DIALOG_ERROR     "gtk-dialog-error"
;;; #define GTK_STOCK_DIALOG_QUESTION  "gtk-dialog-question"

;;; #define GTK_STOCK_DND              "gtk-dnd"
;;; #define GTK_STOCK_DND_MULTIPLE     "gtk-dnd-multiple"

;;; #define GTK_STOCK_ABOUT            "gtk-about"
;;; #define GTK_STOCK_ADD              "gtk-add"
;;; #define GTK_STOCK_APPLY            "gtk-apply"
;;; #define GTK_STOCK_BOLD             "gtk-bold"
;;; #define GTK_STOCK_CANCEL           "gtk-cancel"
;;; #define GTK_STOCK_CDROM            "gtk-cdrom"
;;; #define GTK_STOCK_CLEAR            "gtk-clear"
;;; #define GTK_STOCK_CLOSE            "gtk-close"
;;; #define GTK_STOCK_COLOR_PICKER     "gtk-color-picker"
;;; #define GTK_STOCK_CONVERT          "gtk-convert"
;;; #define GTK_STOCK_CONNECT          "gtk-connect"
;;; #define GTK_STOCK_COPY             "gtk-copy"
;;; #define GTK_STOCK_CUT              "gtk-cut"
;;; #define GTK_STOCK_DELETE           "gtk-delete"
;;; #define GTK_STOCK_DIRECTORY        "gtk-directory"
;;; #define GTK_STOCK_DISCARD          "gtk-discard"
;;; #define GTK_STOCK_DISCONNECT       "gtk-disconnect"
;;; #define GTK_STOCK_EDIT             "gtk-edit"
;;; #define GTK_STOCK_EXECUTE          "gtk-execute"
;;; #define GTK_STOCK_FILE             "gtk-file"
;;; #define GTK_STOCK_FIND             "gtk-find"
;;; #define GTK_STOCK_FIND_AND_REPLACE "gtk-find-and-replace"
;;; #define GTK_STOCK_FLOPPY           "gtk-floppy"
;;; #define GTK_STOCK_FULLSCREEN       "gtk-fullscreen"
;;; #define GTK_STOCK_GOTO_BOTTOM      "gtk-goto-bottom"
;;; #define GTK_STOCK_GOTO_FIRST       "gtk-goto-first"
;;; #define GTK_STOCK_GOTO_LAST        "gtk-goto-last"
;;; #define GTK_STOCK_GOTO_TOP         "gtk-goto-top"
;;; #define GTK_STOCK_GO_BACK          "gtk-go-back"
;;; #define GTK_STOCK_GO_DOWN          "gtk-go-down"
;;; #define GTK_STOCK_GO_FORWARD       "gtk-go-forward"
;;; #define GTK_STOCK_GO_UP            "gtk-go-up"
;;; #define GTK_STOCK_HARDDISK         "gtk-harddisk"
;;; #define GTK_STOCK_HELP             "gtk-help"
;;; #define GTK_STOCK_HOME             "gtk-home"
;;; #define GTK_STOCK_INDEX            "gtk-index"
;;; #define GTK_STOCK_INDENT           "gtk-indent"
;;; #define GTK_STOCK_INFO             "gtk-info"
;;; #define GTK_STOCK_UNINDENT         "gtk-unindent"
;;; #define GTK_STOCK_ITALIC           "gtk-italic"
;;; #define GTK_STOCK_JUMP_TO          "gtk-jump-to"
;;; #define GTK_STOCK_JUSTIFY_CENTER   "gtk-justify-center"
;;; #define GTK_STOCK_JUSTIFY_FILL     "gtk-justify-fill"
;;; #define GTK_STOCK_JUSTIFY_LEFT     "gtk-justify-left"
;;; #define GTK_STOCK_JUSTIFY_RIGHT    "gtk-justify-right"
;;; #define GTK_STOCK_LEAVE_FULLSCREEN "gtk-leave-fullscreen"
;;; #define GTK_STOCK_MISSING_IMAGE    "gtk-missing-image"
;;; #define GTK_STOCK_MEDIA_FORWARD    "gtk-media-forward"
;;; #define GTK_STOCK_MEDIA_NEXT       "gtk-media-next"
;;; #define GTK_STOCK_MEDIA_PAUSE      "gtk-media-pause"
;;; #define GTK_STOCK_MEDIA_PLAY       "gtk-media-play"
;;; #define GTK_STOCK_MEDIA_PREVIOUS   "gtk-media-previous"
;;; #define GTK_STOCK_MEDIA_RECORD     "gtk-media-record"
;;; #define GTK_STOCK_MEDIA_REWIND     "gtk-media-rewind"
;;; #define GTK_STOCK_MEDIA_STOP       "gtk-media-stop"
;;; #define GTK_STOCK_NETWORK          "gtk-network"
;;; #define GTK_STOCK_NEW              "gtk-new"
;;; #define GTK_STOCK_NO               "gtk-no"
;;; #define GTK_STOCK_OK               "gtk-ok"
;;; #define GTK_STOCK_OPEN             "gtk-open"
;;; #define GTK_STOCK_ORIENTATION_PORTRAIT "gtk-orientation-portrait"
;;; #define GTK_STOCK_ORIENTATION_LANDSCAPE "gtk-orientation-landscape"
;;; #define GTK_STOCK_ORIENTATION_REVERSE_LANDSCAPE "gtk-orientation-reverse-landscape"
;;; #define GTK_STOCK_ORIENTATION_REVERSE_PORTRAIT "gtk-orientation-reverse-portrait"
;;; #define GTK_STOCK_PASTE            "gtk-paste"
;;; #define GTK_STOCK_PREFERENCES      "gtk-preferences"
;;; #define GTK_STOCK_PRINT            "gtk-print"
;;; #define GTK_STOCK_PRINT_PREVIEW    "gtk-print-preview"
;;; #define GTK_STOCK_PROPERTIES       "gtk-properties"
;;; #define GTK_STOCK_QUIT             "gtk-quit"
;;; #define GTK_STOCK_REDO             "gtk-redo"
;;; #define GTK_STOCK_REFRESH          "gtk-refresh"
;;; #define GTK_STOCK_REMOVE           "gtk-remove"
;;; #define GTK_STOCK_REVERT_TO_SAVED  "gtk-revert-to-saved"
;;; #define GTK_STOCK_SAVE             "gtk-save"
;;; #define GTK_STOCK_SAVE_AS          "gtk-save-as"
;;; #define GTK_STOCK_SELECT_ALL       "gtk-select-all"
;;; #define GTK_STOCK_SELECT_COLOR     "gtk-select-color"
;;; #define GTK_STOCK_SELECT_FONT      "gtk-select-font"
;;; #define GTK_STOCK_SORT_ASCENDING   "gtk-sort-ascending"
;;; #define GTK_STOCK_SORT_DESCENDING  "gtk-sort-descending"
;;; #define GTK_STOCK_SPELL_CHECK      "gtk-spell-check"
;;; #define GTK_STOCK_STOP             "gtk-stop"
;;; #define GTK_STOCK_STRIKETHROUGH    "gtk-strikethrough"
;;; #define GTK_STOCK_UNDELETE         "gtk-undelete"
;;; #define GTK_STOCK_UNDERLINE        "gtk-underline"
;;; #define GTK_STOCK_UNDO             "gtk-undo"
;;; #define GTK_STOCK_YES              "gtk-yes"
;;; #define GTK_STOCK_ZOOM_100         "gtk-zoom-100"
;;; #define GTK_STOCK_ZOOM_FIT         "gtk-zoom-fit"
;;; #define GTK_STOCK_ZOOM_IN          "gtk-zoom-in"
;;; #define GTK_STOCK_ZOOM_OUT         "gtk-zoom-out"

;;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;    TEMPORARY GARBAGE
;;;
;;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; For now, all missing types and methods are listed in this file; over
;;; time they'll be filled in properly and some moved.

#||
(defmethod realize-cursor (&rest args)
  (declare (ignore args))
  (not-yet-implemented "realize-cursor"))

(defmethod query-widget-for-color (&rest args)
  (declare (ignore args))
  (not-yet-implemented "query-widget-for-color"))
||#

