;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GTK2-DUIM -*-
(in-package #:gtk2-duim)

#||

See: https://developer.gnome.org/pango/unstable/pango-Cairo-Rendering.html

PangoCairoFont
typedef struct _PangoCairoFont PangoCairoFont;
PangoCairoFont is an interface exported by fonts for use with Cairo. The actual type of the font will depend on the particular font technology Cairo was compiled to use.

Since 1.18

PangoCairoFontMap
typedef struct _PangoCairoFontMap PangoCairoFontMap;
PangoCairoFontMap is an interface exported by font maps for use with Cairo. The actual type of the font map will depend on the particular font technology Cairo was compiled to use.

Since 1.10
||#

#||
(defun cffi/pango_cairo_font_map_get_default ()
"
PangoFontMap *      pango_cairo_font_map_get_default    (void);
Gets a default PangoCairoFontMap to use with Cairo.

Note that the type of the returned object will depend on the particular font backend Cairo was compiled to use; You generally should only use the PangoFontMap and PangoCairoFontMap interfaces on the returned object.

The default Cairo fontmap can be changed by using pango_cairo_font_map_set_default(). This can be used to change the Cairo font backend that the default fontmap uses for example.

Note that since Pango 1.32.6, the default fontmap is per-thread. Each thread gets its own default fontmap. In this way, PangoCairo can be used safely from multiple threads.

Returns :
the default PangoCairo fontmap for the current thread. This object is owned by Pango and must not be freed. [transfer none]
Since 1.10
"


(defun cffi/pango_cairo_font_map_set_default ()
"
void                pango_cairo_font_map_set_default    (PangoCairoFontMap *fontmap);
Sets a default PangoCairoFontMap to use with Cairo.

This can be used to change the Cairo font backend that the default fontmap uses for example. The old default font map is unreffed and the new font map referenced.

Note that since Pango 1.32.6, the default fontmap is per-thread. This function only changes the default fontmap for the current thread. Default fontmaps of exisiting threads are not changed. Default fontmaps of any new threads will still be created using pango_cairo_font_map_new().

A value of NULL for fontmap will cause the current default font map to be released and a new default font map to be created on demand, using pango_cairo_font_map_new().

fontmap :
The new default font map, or NULL
Since 1.22
"


(defun cffi/pango_cairo_font_map_new ()
"
PangoFontMap *      pango_cairo_font_map_new            (void);
Creates a new PangoCairoFontMap object; a fontmap is used to cache information about available fonts, and holds certain global parameters such as the resolution. In most cases, you can use pango_cairo_font_map_get_default() instead.

Note that the type of the returned object will depend on the particular font backend Cairo was compiled to use; You generally should only use the PangoFontMap and PangoCairoFontMap interfaces on the returned object.

Returns :
the newly allocated PangoFontMap, which should be freed with g_object_unref(). [transfer full]
Since 1.10
"


(defun cffi/pango_cairo_font_map_new_for_font_type ()
"
PangoFontMap *      pango_cairo_font_map_new_for_font_type
                                                        (cairo_font_type_t fonttype);
Creates a new PangoCairoFontMap object of the type suitable to be used with cairo font backend of type fonttype.

In most cases one should simply use pango_cairo_font_map_new(), or in fact in most of those cases, just use pango_cairo_font_map_get_default().

fonttype :
desired cairo_font_type_t
Returns :
(transfer full) : the newly allocated PangoFontMap of suitable type which should be freed with g_object_unref(), or NULL if the requested cairo font backend is not supported / compiled in.
Since 1.18
"


(defun cffi/pango_cairo_font_map_get_font_type ()
"
cairo_font_type_t   pango_cairo_font_map_get_font_type  (PangoCairoFontMap *fontmap);
Gets the type of Cairo font backend that fontmap uses.

fontmap :
a PangoCairoFontMap
Returns :
the cairo_font_type_t cairo font backend type
Since 1.18
"


(defun cffi/pango_cairo_font_map_set_resolution ()
"
void                pango_cairo_font_map_set_resolution (PangoCairoFontMap *fontmap,
                                                         double dpi);
Sets the resolution for the fontmap. This is a scale factor between points specified in a PangoFontDescription and Cairo units. The default value is 96, meaning that a 10 point font will be 13 units high. (10 * 96. / 72. = 13.3).

fontmap :
a PangoCairoFontMap
dpi :
the resolution in "dots per inch". (Physical inches aren't actually involved; the terminology is conventional.)
Since 1.10
"


(defun cffi/pango_cairo_font_map_get_resolution ()
"
double              pango_cairo_font_map_get_resolution (PangoCairoFontMap *fontmap);
Gets the resolution for the fontmap. See pango_cairo_font_map_set_resolution()

fontmap :
a PangoCairoFontMap
Returns :
the resolution in "dots per inch"
Since 1.10

pango_cairo_font_map_create_context ()
PangoContext *      pango_cairo_font_map_create_context (PangoCairoFontMap *fontmap);
Warning
pango_cairo_font_map_create_context has been deprecated since version 1.22 and should not be used in newly-written code. Use pango_font_map_create_context() instead.

Create a PangoContext for the given fontmap.

fontmap :
a PangoCairoFontMap
Returns :
the newly created context; free with g_object_unref().
Since 1.10
"


(defun cffi/pango_cairo_font_get_scaled_font ()
"
cairo_scaled_font_t * pango_cairo_font_get_scaled_font  (PangoCairoFont *font);
Gets the cairo_scaled_font_t used by font. The scaled font can be referenced and kept using cairo_scaled_font_reference().

font :
a PangoFont from a PangoCairoFontMap
Returns :
the cairo_scaled_font_t used by font, or NULL if font is NULL.
Since 1.18
"


(defun cffi/pango_cairo_context_set_resolution ()
"
void                pango_cairo_context_set_resolution  (PangoContext *context,
                                                         double dpi);
Sets the resolution for the context. This is a scale factor between points specified in a PangoFontDescription and Cairo units. The default value is 96, meaning that a 10 point font will be 13 units high. (10 * 96. / 72. = 13.3).

context :
a PangoContext, from a pangocairo font map
dpi :
the resolution in "dots per inch". (Physical inches aren't actually involved; the terminology is conventional.) A 0 or negative value means to use the resolution from the font map.
Since 1.10
"


(defun cffi/pango_cairo_context_get_resolution ()
"
double              pango_cairo_context_get_resolution  (PangoContext *context);
Gets the resolution for the context. See pango_cairo_context_set_resolution()

context :
a PangoContext, from a pangocairo font map
Returns :
the resolution in "dots per inch". A negative value will be returned if no resolution has previously been set.
Since 1.10
"


(defun cffi/pango_cairo_context_set_font_options ()
"
void                pango_cairo_context_set_font_options
                                                        (PangoContext *context,
                                                         const cairo_font_options_t *options);
Sets the font options used when rendering text with this context. These options override any options that pango_cairo_update_context() derives from the target surface.

context :
a PangoContext, from a pangocairo font map
options :
a cairo_font_options_t, or NULL to unset any previously set options. A copy is made.
Since 1.10
"


(defun cffi/pango_cairo_context_get_font_options ()
"
const cairo_font_options_t * pango_cairo_context_get_font_options
                                                        (PangoContext *context);
Retrieves any font rendering options previously set with pango_cairo_font_map_set_font_options(). This function does not report options that are derived from the target surface by pango_cairo_update_context()

context :
a PangoContext, from a pangocairo font map
Returns :
the font options previously set on the context, or NULL if no options have been set. This value is owned by the context and must not be modified or freed.
Since 1.10
"

#||
PangoCairoShapeRendererFunc ()
void                (*PangoCairoShapeRendererFunc)      (cairo_t *cr,
                                                         PangoAttrShape *attr,
                                                         gboolean do_path,
                                                         gpointer data);
Function type for rendering attributes of type PANGO_ATTR_SHAPE with Pango's Cairo renderer.

cr :
a Cairo context with current point set to where the shape should be rendered
attr :
the PANGO_ATTR_SHAPE to render
do_path :
whether only the shape path should be appended to current path of cr and no filling/stroking done. This will be set to TRUE when called from pango_cairo_layout_path() and pango_cairo_layout_line_path() rendering functions.
data :
user data passed to pango_cairo_context_set_shape_renderer()
pango_cairo_context_set_shape_renderer ()
void                pango_cairo_context_set_shape_renderer
                                                        (PangoContext *context,
                                                         PangoCairoShapeRendererFunc func,
                                                         gpointer data,
                                                         GDestroyNotify dnotify);
Sets callback function for context to use for rendering attributes of type PANGO_ATTR_SHAPE. See PangoCairoShapeRendererFunc for details.

context :
a PangoContext, from a pangocairo font map
func :
Callback function for rendering attributes of type PANGO_ATTR_SHAPE, or NULL to disable shape rendering.
data :
User data that will be passed to func.
dnotify :
Callback that will be called when the context is freed to release data, or NULL.
Since 1.18

pango_cairo_context_get_shape_renderer ()
PangoCairoShapeRendererFunc pango_cairo_context_get_shape_renderer
                                                        (PangoContext *context,
                                                         gpointer *data);
Sets callback function for context to use for rendering attributes of type PANGO_ATTR_SHAPE. See PangoCairoShapeRendererFunc for details.

Retrieves callback function and associated user data for rendering attributes of type PANGO_ATTR_SHAPE as set by pango_cairo_context_set_shape_renderer(), if any.

context :
a PangoContext, from a pangocairo font map
data :
Pointer to gpointer to return user data
Returns :
the shape rendering callback previously set on the context, or NULL if no shape rendering callback have been set.
Since 1.18
||#


(defun cffi/pango_cairo_create_context ()
"
PangoContext *      pango_cairo_create_context          (cairo_t *cr);
Creates a context object set up to match the current transformation and target surface of the Cairo context. This context can then be used to create a layout using pango_layout_new().

This function is a convenience function that creates a context using the default font map, then updates it to cr. If you just need to create a layout for use with cr and do not need to access PangoContext directly, you can use pango_cairo_create_layout() instead.

cr :
a Cairo context
Returns :
the newly created PangoContext. Free with g_object_unref(). [transfer full]
Since 1.22
"


(defun cffi/pango_cairo_update_context ()
"
void                pango_cairo_update_context          (cairo_t *cr,
                                                         PangoContext *context);
Updates a PangoContext previously created for use with Cairo to match the current transformation and target surface of a Cairo context. If any layouts have been created for the context, it's necessary to call pango_layout_context_changed() on those layouts.

cr :
a Cairo context
context :
a PangoContext, from a pangocairo font map
Since 1.10
"


(defun cffi/pango_cairo_create_layout ()
"
 PangoLayout * pango_cairo_create_layout (cairo_t *cr);

 Creates a layout object set up to match the current transformation and target surface of
 the Cairo context. This layout can then be used for text measurement with functions like
 pango_layout_get_size() or drawing with functions like pango_cairo_show_layout(). If you
 change the transformation or target surface for cr, you need to call
 pango_cairo_update_layout()

This function is the most convenient way to use Cairo with Pango, however it is slightly inefficient since it creates a separate PangoContext object for each layout. This might matter in an application that was laying out large amounts of text.

cr :
a Cairo context
Returns :
the newly created PangoLayout. Free with g_object_unref(). [transfer full]
Since 1.10
"


(defun cffi/pango_cairo_update_layout ()
"
void                pango_cairo_update_layout           (cairo_t *cr,
                                                         PangoLayout *layout);
Updates the private PangoContext of a PangoLayout created with pango_cairo_create_layout() to match the current transformation and target surface of a Cairo context.

cr :
a Cairo context
layout :
a PangoLayout, from pango_cairo_create_layout()
Since 1.10
"


(defun cffi/pango_cairo_show_glyph_string ()
"
void                pango_cairo_show_glyph_string       (cairo_t *cr,
                                                         PangoFont *font,
                                                         PangoGlyphString *glyphs);
Draws the glyphs in glyphs in the specified cairo context. The origin of the glyphs (the left edge of the baseline) will be drawn at the current point of the cairo context.

cr :
a Cairo context
font :
a PangoFont from a PangoCairoFontMap
glyphs :
a PangoGlyphString
Since 1.10
"


(defun cffi/pango_cairo_show_glyph_item ()
"
void                pango_cairo_show_glyph_item         (cairo_t *cr,
                                                         const char *text,
                                                         PangoGlyphItem *glyph_item);
Draws the glyphs in glyph_item in the specified cairo context, embedding the text associated with the glyphs in the output if the output format supports it (PDF for example), otherwise it acts similar to pango_cairo_show_glyph_string().

The origin of the glyphs (the left edge of the baseline) will be drawn at the current point of the cairo context.

Note that text is the start of the text for layout, which is then indexed by glyph_item->item->offset.

cr :
a Cairo context
text :
the UTF-8 text that glyph_item refers to
glyph_item :
a PangoGlyphItem
Since 1.22
"


(defun cffi/pango_cairo_show_layout_line ()
"
void                pango_cairo_show_layout_line        (cairo_t *cr,
                                                         PangoLayoutLine *line);
Draws a PangoLayoutLine in the specified cairo context. The origin of the glyphs (the left edge of the line) will be drawn at the current point of the cairo context.

cr :
a Cairo context
line :
a PangoLayoutLine
Since 1.10
"
||#

(defun cffi/pango_cairo_show_layout (cr layout)
"
 void pango_cairo_show_layout (cairo_t *cr, PangoLayout *layout);

 Draws a PangoLayout in the specified cairo context. The top-left corner of the PangoLayout will
 be drawn at the current point of the cairo context.

    cr : a Cairo context
    layout : a Pango layout

 Since 1.10
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/PANGO_CAIRO_SHOW_LAYOUT invoked with null CR")
      (if (or (null layout) (cffi:null-pointer-p layout))
	  (error "CFFI/PANGO_CAIRO_SHOW_LAYOUT invoked with null LAYOUT")
	  (cffi:foreign-funcall "pango_cairo_show_layout"
				:pointer cr
				:pointer layout
				:void)))
  cr)


#||
(defun cffi/pango_cairo_show_error_underline ()
"
void                pango_cairo_show_error_underline    (cairo_t *cr,
                                                         double x,
                                                         double y,
                                                         double width,
                                                         double height);
Draw a squiggly line in the specified cairo context that approximately covers the given rectangle in the style of an underline used to indicate a spelling error. (The width of the underline is rounded to an integer number of up/down segments and the resulting rectangle is centered in the original rectangle)

cr :
a Cairo context
x :
The X coordinate of one corner of the rectangle
y :
The Y coordinate of one corner of the rectangle
width :
Non-negative width of the rectangle
height :
Non-negative height of the rectangle
Since 1.14
"


(defun cffi/pango_cairo_glyph_string_path ()
"
void                pango_cairo_glyph_string_path       (cairo_t *cr,
                                                         PangoFont *font,
                                                         PangoGlyphString *glyphs);
Adds the glyphs in glyphs to the current path in the specified cairo context. The origin of the glyphs (the left edge of the baseline) will be at the current point of the cairo context.

cr :
a Cairo context
font :
a PangoFont from a PangoCairoFontMap
glyphs :
a PangoGlyphString
Since 1.10
"


(defun cffi/pango_cairo_layout_line_path ()
"
void                pango_cairo_layout_line_path        (cairo_t *cr,
                                                         PangoLayoutLine *line);
Adds the text in PangoLayoutLine to the current path in the specified cairo context. The origin of the glyphs (the left edge of the line) will be at the current point of the cairo context.

cr :
a Cairo context
line :
a PangoLayoutLine
Since 1.10
"


(defun cffi/pango_cairo_layout_path ()
"
void                pango_cairo_layout_path             (cairo_t *cr,
                                                         PangoLayout *layout);
Adds the text in a PangoLayout to the current path in the specified cairo context. The top-left corner of the PangoLayout will be at the current point of the cairo context.

cr :
a Cairo context
layout :
a Pango layout
Since 1.10
"


(defun cffi/pango_cairo_error_underline_path ()
"
void                pango_cairo_error_underline_path    (cairo_t *cr,
                                                         double x,
                                                         double y,
                                                         double width,
                                                         double height);
Add a squiggly line to the current path in the specified cairo context that approximately covers the given rectangle in the style of an underline used to indicate a spelling error. (The width of the underline is rounded to an integer number of up/down segments and the resulting rectangle is centered in the original rectangle)

cr :
a Cairo context
x :
The X coordinate of one corner of the rectangle
y :
The Y coordinate of one corner of the rectangle
width :
Non-negative width of the rectangle
height :
Non-negative height of the rectangle
Since 1.14
"

||#
