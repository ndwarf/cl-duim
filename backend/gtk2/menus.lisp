;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GTK2-DUIM -*-
(in-package #:gtk2-duim)

#||
Module:       gtk-duim
Synopsis:     GTK menus implementation
Author:       Andy Armstrong, Scott McKay
Copyright:    Original Code is Copyright (c) 1995-2004 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND

/// GTK menu mirrors

define sealed class <menu-button-mirror> (<gadget-mirror>)
end class <menu-button-mirror>;
||#

(defclass <gtk-menu-button-mirror> (<gtk-gadget-mirror>)
  ())


(defclass <gtk-menu-bar-mirror> (<gtk-gadget-mirror>)
  ((%used-mnemonics :type sequence
		    :initform (make-array 0 :adjustable t :fill-pointer t)
		    :accessor %used-mnemonics)))

#||
define sealed class <menu-mirror> (<gadget-mirror>)
  sealed slot %created? :: <boolean> = #f;
  sealed slot mirror-selection-owner :: false-or(<menu>) = #f,
    init-keyword: selection-owner:;
  sealed slot %used-mnemonics :: <stretchy-object-vector> = make(<stretchy-vector>);
end class <menu-mirror>;
||#

;;; XXX: should never invoke GTK-SHOW on these...

(defclass <gtk-menu-mirror> (<gtk-gadget-mirror>)
  ((%created? :type boolean :initform nil :accessor %created?)
   (mirror-selection-owner :type (or null <menu>)
			   :initarg :selection-owner
			   :initform nil
			   :accessor mirror-selection-owner)
   (%used-mnemonics :type sequence
		    :initform (make-array 0 :adjustable t :fill-pointer t)
		    :accessor %used-mnemonics)))

#||
define method set-mirror-parent
    (child :: <menu-button-mirror>, parent :: <menu-mirror>)
 => ()
  debug-message("Adding %= to menu %=",
		gadget-label(mirror-sheet(child)),
		gadget-label(mirror-sheet(parent)));
  gtk-menu-append(GTK-MENU(mirror-widget(parent).submenu-value),
		  mirror-widget(child))
end method set-mirror-parent;
||#

(defmethod set-mirror-parent ((child  <gtk-menu-button-mirror>)
			      (parent <gtk-menu-mirror>))
  (let ((parent-widget (mirror-widget parent))
	(child-widget  (mirror-widget child)))
    (cffi/gtk-menu-shell-append (cffi/gtk-menu-item-get-submenu parent-widget)
			   child-widget)))


;;; Popup menus are constructed differently to menus that go into the menu-bar, and
;;; are not given a (gtk) submenu. This function ensures that every menu we try to
;;; map has a submenu (the mapping process goes through and attempts to add children
;;; of the menu to the (duim) menu's (gtk) submenu, so the submenu must exist by then).

(DEFUN %ENSURE-GTK-SUBMENU (WIDGET)
  (WHEN (NULL (CFFI/GTK-MENU-ITEM-GET-SUBMENU WIDGET))
    (LET ((NEW-MENU (CFFI/GTK-MENU-NEW)))
      (DUIM-MEM-DEBUG "gtk-menus.%ENSURE-GTK-SUBMENU new-menu" NEW-MENU)
      (CFFI/GTK-MENU-ITEM-SET-SUBMENU WIDGET NEW-MENU))))


#||    
define method set-mirror-parent
    (child :: <menu-mirror>, parent :: <menu-mirror>)
 => ()
  let widget = mirror-widget(child);
  let menu = GTK-MENU(gtk-menu-new());
  debug-message("Creating submenu for %s",
		gadget-label(mirror-sheet(child)));
  gtk-menu-item-set-submenu(widget, menu);
  gtk-menu-append(GTK-MENU(mirror-widget(parent).submenu-value),
		  widget)
end method set-mirror-parent;
||#

(defmethod set-mirror-parent ((child  <gtk-menu-mirror>)
			      (parent <gtk-menu-mirror>))
  (let ((child-widget (mirror-widget child))
	(parent-widget (mirror-widget parent)))
    (cffi/gtk-menu-shell-append (cffi/gtk-menu-item-get-submenu parent-widget)
				child-widget)))


#||    
define method set-mirror-parent
    (child :: <menu-mirror>, parent :: <gadget-mirror>)
 => ()
  if (instance?(parent.mirror-sheet, <menu-bar>))
    let widget = mirror-widget(child);
    if (child.mirror-sheet.gadget-label = "Help")
      gtk-menu-item-right-justify(widget)
    end;
    let menu = GTK-MENU(gtk-menu-new());
    debug-message("Creating submenu for menu bar");
    gtk-menu-item-set-submenu(widget, menu);
    gtk-menu-bar-append(mirror-widget(parent),
			widget)
  else
    next-method()
  end
end method set-mirror-parent;
||#

(defmethod set-mirror-parent ((child  <gtk-menu-mirror>)
			      (parent <gtk-menu-bar-mirror>))
  (let ((widget (mirror-widget child)))
    (cffi/gtk-menu-shell-append (mirror-widget parent) widget)))


#||    
define sealed method destroy-mirror 
    (_port :: <gtk-port>, menu :: <menu>, mirror :: <menu-mirror>) => ()
  ignoring("destroy-mirror for <menu>")
end method destroy-mirror;
||#

(defmethod destroy-mirror ((_port <gtk-port>)
			   (menu  <menu>)
			   (mirror <gtk-menu-mirror>))
  (ignoring "destroy-mirror for <menu>")
  ;; Need to clear up any direct-mirror etc. associated with the menu (which is what
  ;; next-method does).
  (call-next-method))


#||
define sealed method note-mirror-destroyed
    (menu :: <menu>, mirror :: <menu-mirror>) => ()
  ignoring("note-mirror-destroyed for menu")
end method note-mirror-destroyed;
||#

(defmethod note-mirror-destroyed ((menu <menu>) (mirror <gtk-menu-mirror>))
  (ignoring "note-mirror-destroyed for menu"))


#||
define sealed class <popup-menu-mirror> (<menu-mirror>)
  sealed slot mirror-selected-gadget :: false-or(<gadget>) = #f;
end class <popup-menu-mirror>;
||#

(defclass <gtk-popup-menu-mirror> (<gtk-menu-mirror>)
  ((mirror-selected-gadget :type (or null <gadget>)
			   :initform nil
			   :accessor mirror-selected-gadget)))


(defmethod install-event-handlers ((sheet <menu>)
				   (mirror <gtk-popup-menu-mirror>))
  ;; popup is associated with the menu item; but need to handle
  ;; the 'deactivate' signal from the submenu.
  (let* ((widget (mirror-widget mirror))
	 (target (cffi/gtk-menu-item-get-submenu widget)))
    (install-named-handlers mirror #("deactivate") :swap target)))



#||
/// Mnemonic handling
///---*** Is this the right thing for GTK?

define table $mnemonic-table :: <object-table>
  = { // This is the set defined by WIG, Appendix B, Table B.5, page 441
      #"About"                => "&About",
      #"Always on Top"        => "Always on &Top",
      #"Apply"                => "&Apply",
      #"Back"                 => "&Back",
      #"< Back"               => "< &Back",
      #"Browse"               => "&Browse",
      #"Close"                => "&Close",
      #"Copy"                 => "&Copy",
      #"Copy Here"            => "&Copy Here",
      #"Create Shortcut"      => "Create &Shortcut",
      #"Create Shortcut Here" => "Create &Shortcut Here",
      #"Cut"                  => "Cu&t",
      #"Delete"               => "&Delete",
      #"Edit"                 => "&Edit",
      #"Exit"                 => "E&xit",
      #"Explore"              => "&Explore",
      #"File"                 => "&File",
      #"Find"                 => "&Find",
      #"Help"                 => "&Help",
      #"Help Topics"          => "Help &Topics",
      #"Hide"                 => "&Hide",
      #"Insert"               => "&Insert",
      #"Insert Object"        => "Insert &Object",
      #"Link Here"            => "&Link Here",
      #"Maximize"             => "Ma&ximize",
      #"Minimize"             => "Mi&nimize",
      #"Move"                 => "&Move",
      #"Move Here"            => "&Move Here",
      #"New"                  => "&New",
      #"Next"                 => "&Next",
      #"Next >"               => "&Next >",
      #"No"                   => "&No",
      #"Open"                 => "&Open",
      #"Open With"            => "Open &With",
      #"Paste"                => "&Paste",
      #"Paste Link"           => "Paste &Link",
      #"Paste Shortcut"       => "Paste &Shortcut",
      #"Page Setup"           => "Page Set&up",
      #"Paste Special"        => "Paste &Special",
      #"Pause"                => "&Pause",
      #"Play"                 => "&Play",
      #"Print"                => "&Print",
      #"Print Here"           => "&Print Here",
      #"Properties"           => "P&roperties",
      #"Quick View"           => "&Quick View",
      #"Redo"                 => "&Redo",
      #"Repeat"               => "&Repeat",
      #"Restore"              => "&Restore",
      #"Resume"               => "&Resume",
      #"Retry"                => "&Retry",
      #"Run"                  => "&Run",
      #"Save"                 => "&Save",
      #"Save As"              => "Save &As",
      #"Select All"           => "Select &All",
      #"Send To"              => "Se&nd To",
      #"Show"                 => "&Show",
      #"Size"                 => "&Size",
      #"Split"                => "S&plit",
      #"Stop"                 => "&Stop",
      #"Undo"                 => "&Undo",
      #"View"                 => "&View",
      #"What's This?"         => "&What's This?",
      #"Window"               => "&Window",
      #"Yes"                  => "&Yes",

      // Some extras that seemed to be missing
      #"Find Next"            => "Find &Next",
      #"Find Previous"        => "Find &Previous",
      #"Forward"              => "&Forward",
      #"Previous"             => "&Previous",
      #"Replace"              => "R&eplace",
      #"Replace All"          => "Replace &All",
      #"Save All"             => "Save Al&l",
      #"Status Bar"           => "Status &Bar" };
||#

(defparameter *mnemonic-table* (make-hash-table :test #'equal))
(macrolet ((frob (key value)
	     `(setf (gethash ,key *mnemonic-table*) ,value)))
  ;; (frob <dotless label> (<label+mnemonic>    <stock-id>))
  ;; This is the set defined by Gnome HIG, 'User Input / Keyboard Interaction', table 10.2.4.
  ;; FILE MENU
  (frob "New"              "&New")              ;; ctrl+n
  (frob "Open"             "&Open")             ;; ctrl+o
  (frob "Save"             "&Save")             ;; ctrl+s
  (frob "Save As"          "Save &As")
  (frob "Print"            "&Print")            ;; ctrl+p
  (frob "Print Preview"    "Print Pre&view")
  (frob "Close"            "&Close")            ;; ctrl+w
  (frob "Quit"             "&Quit")             ;; ctrl+q
  (frob "Exit"             "&Quit")
  ;; EDIT MENU
  (frob "Undo"             "&Undo")             ;; ctrl+z
  (frob "Redo"             "&Redo")             ;; shift+ctrl+z
  (frob "Cut"              "Cu&t")              ;; ctrl+x
  (frob "Copy"             "&Copy")             ;; ctrl+c
  (frob "Paste"            "&Paste")            ;; ctrl+v
  (frob "Duplicate"        "&Duplicate")        ;; ctrl+u
  (frob "Select All"       "Select &All")       ;; ctrl+a
  (frob "Invert Selection" "In&vert Selection") ;; ctrl+i
  (frob "Delete"           "&Delete")           ;; del
  (frob "Find"             "&Find")             ;; ctrl+f
;  [GTK_STOCK_FIND_AND_REPLACE]
  (frob "Search"           "&Search")           ;; ctrl+f / shift+ctrl+f (see note in HIG)
  (frob "Find Next"        "Find Ne&xt")        ;; ctrl+g
  (frob "Replace"          "&Replace")          ;; ctrl+h
  (frob "Rename"           "R&ename")           ;; f2
  ;; VIEW MENU
  (frob "Zoom In"          "Zoom &In")          ;; ctrl+Plus
  (frob "Zoom Out"         "Zoom &Out")         ;; ctrl+minus
  (frob "Normal Size"      "&Normal Size")      ;; ctrl+0
  (frob "Refresh"          "&Refresh")          ;; ctrl+r
  (frob "Reload"           "&Reload")           ;; ctrl+r / shift+ctrl+r (see note in HIG)
  (frob "Properties"       "Pr&operties")       ;; alt+enter
  ;; BOOKMARKS MENU
  (frob "Add Bookmark"     "Add &Bookmark")     ;; ctrl+d
  (frob "Edit Bookmarks"   "&Edit Bookmarks")   ;; ctrl+b / shift+ctrl+d (see note in HIG)
  ;; GO MENU
  (frob "Back"             "&Back")             ;; alt+left
  (frob "Next"             "Ne&xt")             ;; alt+right
  (frob "Up"               "&Up")               ;; alt+up
  (frob "Home"             "&Home")             ;; alt+home
  (frob "Location"         "&Location")         ;; ctrl+L
  ;; FORMAT MENU
  (frob "Bold"             "&Bold")             ;; ctrl+b
  (frob "Underline"        "&Underline")        ;; ctrl+u
  (frob "Italic"           "&Italic")           ;; ctrl+i
  ;; HELP MENU
  (frob "Help"             "&Help Contents")
  (frob "Help Contents"    "&Help Contents")
  (frob "Contents"         "&Help Contents"))   ;; f1


#||
define sealed method compute-mnemonic-from-label
    (gadget :: <gtk-gadget-mixin>, label :: <string>, 
     #key remove-ampersand? = #f)
 => (label, mnemonic :: false-or(<mnemonic>), index :: false-or(<integer>))
  let (label, mnemonic, index) = next-method();
  if (mnemonic)
    values(label, mnemonic, index)
  else
    compute-standard-gtk-mnemonic
      (gadget, label, remove-ampersand?: remove-ampersand?)
  end
end method compute-mnemonic-from-label;
||#

(defmethod compute-mnemonic-from-label ((gadget <gtk-gadget-mixin>)
					(label string)
					&key (remove-ampersand? nil)) ; => label, mnemonic, index
  ;; Try to get a mnemonic from the label itself first (this allows the
  ;; user to override the "standard" gtk mnemonics and supply their own).
  (multiple-value-bind (label mnemonic index)
      (call-next-method)
    (if mnemonic
	(values label mnemonic index)
	(compute-standard-gtk-mnemonic gadget
				       label
				       :remove-ampersand? remove-ampersand?))))


#||
define sealed method compute-standard-gtk-mnemonic
    (gadget :: <gadget>, label :: <string>, #key remove-ampersand? = #f)
 => (label, mnemonic :: false-or(<mnemonic>), index :: false-or(<integer>))
  let length :: <integer> = size(label);
  let dots :: <byte-string> = "...";
  let dots?
    = length > 3 & string-equal?(label, dots, start1: length - 3);
  let dotless-label
    = if (dots?) copy-sequence(label, end: length - 3)
      else label end;
  let new-label
    = element($mnemonic-table, as(<symbol>, dotless-label), default: #f);
  if (new-label)
    when (dots?)
      new-label := concatenate-as(<string>, new-label, dots)
    end;
    compute-mnemonic-from-label
      (gadget, new-label, remove-ampersand?: remove-ampersand?)
  else
    values(label, #f, #f)
  end
end method compute-standard-gtk-mnemonic;
||#

(defmethod compute-standard-gtk-mnemonic ((gadget <gadget>)
					  (label string)
					  &key
					  (remove-ampersand? nil)) ; => label, mnemonic, index
  (let* ((length        (size label))
	 (dots          "...")
	 (dots?         (and (> length 3) (string= label dots :start1 (- length 3))))
	 (dotless-label (if dots? (subseq label 0 (- length 3)) label))
	 (new-label     (gethash dotless-label *mnemonic-table* nil)))
    (cond (new-label
	   (when dots?
	     (setf new-label (concatenate 'string new-label dots)))
	   (compute-mnemonic-from-label gadget
					new-label
					:remove-ampersand? remove-ampersand?))
	  (t
	   (values label nil nil)))))


#||
define inline function vowel? 
    (char :: <character>) => (vowel? :: <boolean>)
  member?(as-uppercase(char), "AEIOU")
end function vowel?;
||#

(defun vowel? (char)
  (find (char-upcase char) "AEIOU"))


#||
define inline function consonant? 
    (char :: <character>) => (consonant? :: <boolean>)
  member?(as-uppercase(char), "BCDFGHJKLMNPQRSTVWXYZ")
end function consonant?;
||#

(defun consonant? (char)
  (find (char-upcase char) "BCDFGHJKLMNPQRSTVWXYZ"))


#||
define sealed method compute-used-mnemonics
    (gadget :: <gtk-gadget-mixin>) => ()
  let mirror = sheet-mirror(gadget);
  let used-mnemonics = mirror.%used-mnemonics;
  used-mnemonics.size := 0;
  local method maybe-add-mnemonic
	    (mnemonic :: false-or(<character>))
	  if (mnemonic)
	    add!(used-mnemonics,
		 as-uppercase(gesture-character(mnemonic)))
	  end
	end method maybe-add-mnemonic;
  for (child in sheet-children(gadget))
    select (child by instance?)
      <menu>, <menu-button> =>
	let (label, mnemonic, index)
	  = compute-mnemonic-from-label(child, defaulted-gadget-label(child));
	ignore(label, index);
	maybe-add-mnemonic(mnemonic);
      <menu-box> =>
	for (sub-child in sheet-children(child))
	  select (sub-child by instance?)
	    <menu>, <menu-button> =>
	      let (label, mnemonic, index)
		= compute-mnemonic-from-label
		    (sub-child, defaulted-gadget-label(sub-child));
	      ignore(label, index);
	      maybe-add-mnemonic(mnemonic);
	    <menu-box> =>
	      error("Found menu-box %= as child of menu-box %=",
		    sub-child, child);
	  end;
	end;
    end
  end;
end method compute-used-mnemonics;
||#

(defmethod compute-used-mnemonics ((gadget <gtk-gadget-mixin>))
  (let* ((mirror (sheet-mirror gadget))
	 (used-mnemonics (%used-mnemonics mirror)))
    (setf (fill-pointer used-mnemonics) 0)
    (labels ((maybe-add-mnemonic (mnemonic)
	       (when mnemonic
		 (add! used-mnemonics
		       (char-upcase (gesture-character mnemonic))))))
      (map nil #'(lambda (child)
		   (etypecase child
		     (<menu>
		      (multiple-value-bind (label mnemonic index)
			  (compute-mnemonic-from-label child
						       (defaulted-gadget-label child))
			(declare (ignore label index))
			(maybe-add-mnemonic mnemonic)))
		     (<menu-button>
		      (multiple-value-bind (label mnemonic index)
			  (compute-mnemonic-from-label child
						       (defaulted-gadget-label child))
			(declare (ignore label index))
			(maybe-add-mnemonic mnemonic)))
		     (<menu-box>
		      (map nil #'(lambda (sub-child)
				   (etypecase sub-child
				     (<menu>
				      (multiple-value-bind (label mnemonic index)
					  (compute-mnemonic-from-label sub-child
								       (defaulted-gadget-label sub-child))
					(declare (ignore label index))
					(maybe-add-mnemonic mnemonic)))
				     (<menu-button>
				      (multiple-value-bind (label mnemonic index)
					  (compute-mnemonic-from-label sub-child
								       (defaulted-gadget-label sub-child))
					(declare (ignore label index))
					(maybe-add-mnemonic mnemonic)))
				     (<menu-box>
				      (error "Found menu-box ~a as child of menu-box ~a"
					     sub-child child))))
			   (sheet-children child)))))
	   ;;; FIXME: What about other gadgets, such as buttons? Do we want mnemonics for those
	   ;;; too? How about linking a mnemonic in a label gadget to a text field, say?
	   (sheet-children gadget)))))


#||
// Mnemonics are allocated on GTK in the following order:
//   - first letter (or digit -- our own rule)
//   - any consonant
//   - any vowel
//   - any digit (our own rule)
// Note that all of the standard mnemonics and any user chosen ones
// have already been removed from consideration by 'compute-used-mnemonics'.
define sealed method allocate-unique-mnemonic 
    (gadget :: <gtk-gadget-mixin>, string :: <string>)
 => (char :: false-or(<character>))
  assert(~empty?(string),
	 "Menu label for %= must have contents", gadget);
  let mirror = sheet-mirror(gadget);
  let used-mnemonics = mirror.%used-mnemonics;
  block (return)
    local method maybe-return-char (char)
	    let uppercase-char = as-uppercase(char);
	    unless (member?(uppercase-char, used-mnemonics))
	      add!(used-mnemonics, uppercase-char);
	      return(uppercase-char)
	    end
	  end;
    if (size(string) > 0)
      let first-char = string[0];
      when (consonant?(first-char)
	    | vowel?(first-char)
	    | digit-char?(first-char))
	maybe-return-char(string[0])
      end;
      for (char in string)
	when (consonant?(char))
	  maybe-return-char(char)
	end
      end;
      for (char in string)
	when (vowel?(char))
	  maybe-return-char(char)
	end
      end;
      for (char in string)
	when (digit-char?(char))
	  maybe-return-char(char)
	end
      end
    end
  end
end method allocate-unique-mnemonic;
||#

(defmethod allocate-unique-mnemonic ((gadget <gtk-gadget-mixin>)
				     (string string))
  (when (empty? string)
    (error "Menu label for ~a must have contents" gadget))
  (let* ((mirror         (sheet-mirror gadget))
	 (used-mnemonics (%used-mnemonics mirror)))
    (block return
      (labels ((maybe-return-char (char)
		 (let ((uppercase-char (char-upcase char)))
		   (unless (find uppercase-char used-mnemonics)
		     (add! used-mnemonics uppercase-char)
		     (return-from return uppercase-char)))))
	(if (> (length string) 0)
	    (let ((first-char (char string 0)))
	      (when (or (consonant? first-char)
			(vowel? first-char)
			(digit-char-p first-char))
		(maybe-return-char (char string 0)))
	      (loop for char across string
		    do (when (consonant? char)
			 (maybe-return-char char)))
	      (loop for char across string
		    do (when (vowel? char)
			 (maybe-return-char char)))
	      (loop for char across string
		    do (when (digit-char-p char)
			 (maybe-return-char char)))))))))


(defparameter *stock-item-table* (make-hash-table :test #'equal))
(macrolet ((frob (key value)
	     `(setf (gethash ,key *stock-item-table*) ',value)))
  ;; (frob <dotless label> (<stock-id>))
  (frob "About"                         ("gtk-about"))
  (frob "Add"                           ("gtk-add"))
  (frob "Apply"                         ("gtk-apply"))
  (frob "Back"                          ("gtk-go-back"))         ;; alt+left
  (frob "Bold"                          ("gtk-bold"))            ;; ctrl+b
  (frob "Cancel"                        ("gtk-cancel"))
  (frob "CD ROM"                        ("gtk-cdrom"))
  (frob "Clear"                         ("gtk-clear"))
  (frob "Close"                         ("gtk-close"))           ;; ctrl+w
  (frob "Color Picker"                  ("gtk-color-picker"))
  (frob "Colour Picker"                 ("gtk-color-picker"))
  (frob "Connect"                       ("gtk-connect"))
  (frob "Contents"                      ("gtk-index"))           ;; f1
  (frob "Convert"                       ("gtk-convert"))
  (frob "Copy"                          ("gtk-copy"))            ;; ctrl+c
  (frob "Cut"                           ("gtk-cut"))             ;; ctrl+x
  (frob "Delete"                        ("gtk-delete"))          ;; del
  ;; Dialog Authentication?
  (frob "Dialog Error"                  ("gtk-dialog-error"))
  (frob "Dialog Info"                   ("gtk-dialog-info"))
  (frob "Dialog Question"               ("gtk-dialog-question"))
  (frob "Dialog Warning"                ("gtk-dialog-warning"))
  (frob "Directory"                     ("gtk-directory"))
  (frob "Discard"                       ("gtk-discard"))
  (frob "Disconnect"                    ("gtk-disconnect"))
  (frob "DND"                           ("gtk-dnd"))
  (frob "DND Multiple"                  ("gtk-dnd-multiple"))
  (frob "Edit"                          ("gtk-edit"))
  (frob "Execute"                       ("gtk-execute"))
  (frob "Exit"                          ("gtk-quit"))
  (frob "File"                          ("gtk-file"))
  (frob "Find"                          ("gtk-find"))            ;; ctrl+f
  (frob "Find And Replace"              ("gtk-find-and-replace"))
  (frob "Floppy"                        ("gtk-floppy"))
  (frob "Fullscreen"                    ("gtk-fullscreen"))
  (frob "Go Back"                       ("gtk-go-back"))
  (frob "Go Down"                       ("gtk-go-down"))
  (frob "Go Forward"                    ("gtk-go-forward"))
  (frob "Go Up"                         ("gtk-go-up"))
  (frob "Goto Bottom"                   ("gtk-goto-bottom"))
  (frob "Goto First"                    ("gtk-goto-first"))
  (frob "Goto Last"                     ("gtk-goto-last"))
  (frob "Goto Top"                      ("gtk-goto-top"))
  (frob "Harddisk"                      ("gtk-harddisk"))
  (frob "Help"                          ("gtk-help"))
  (frob "Help Contents"                 ("gtk-help"))
  (frob "Home"                          ("gtk-home"))            ;; alt+home
  (frob "Indent"                        ("gtk-indent"))
  (frob "Index"                         ("gtk-index"))
  (frob "Info"                          ("gtk-info"))
  (frob "Italic"                        ("gtk-italic"))          ;; ctrl+i
  (frob "Jump To"                       ("gtk-jump-to"))
  (frob "Justify Center"                ("gtk-justify-center"))
  (frob "Justify Fill"                  ("gtk-justify-fill"))
  (frob "Justify Left"                  ("gtk-justify-left"))
  (frob "Justify Right"                 ("gtk-justify-right"))
  (frob "Leave Fullscreen"              ("gtk-leave-fullscreen"))
  (frob "Media Forward"                 ("gtk-media-forward"))
  (frob "Media Next"                    ("gtk-media-next"))
  (frob "Media Pause"                   ("gtk-media-pause"))
  (frob "Media Play"                    ("gtk-media-play"))
  (frob "Media Previous"                ("gtk-media-previous"))
  (frob "Media Record"                  ("gtk-media-record"))
  (frob "Media Rewind"                  ("gtk-media-rewind"))
  (frob "Media Stop"                    ("gtk-media-stop"))
  (frob "Missing Image"                 ("gtk-missing-image"))
  (frob "Network"                       ("gtk-network"))
  (frob "New"                           ("gtk-new"))             ;; ctrl+n
  (frob "Next"                          ("gtk-go-forward"))      ;; alt+right
  (frob "No"                            ("gtk-no"))
  (frob "Normal Size"                   ("gtk-zoom-100"))        ;; ctrl+0
  (frob "OK"                            ("gtk-ok"))
  (frob "Open"                          ("gtk-open"))            ;; ctrl+o
  (frob "Orientation Landscape"         ("gtk-orientation-landscape"))
  (frob "Orientation Portrait"          ("gtk-orientation-portrait"))
  (frob "Orientation Reverse Landscape" ("gtk-orientation-reverse-landscape"))
  (frob "Orientation Reverse Portrait"  ("gtk-orientation-reverse-portrait"))
  (frob "Paste"                         ("gtk-paste"))           ;; ctrl+v
  (frob "Preferences"                   ("gtk-preferences"))
  (frob "Print Preview"                 ("gtk-print-preview"))
  (frob "Print"                         ("gtk-print"))           ;; ctrl+p
  (frob "Properties"                    ("gtk-properties"))      ;; alt+enter
  (frob "Quit"                          ("gtk-quit"))            ;; ctrl+q
  (frob "Redo"                          ("gtk-redo"))            ;; shift+ctrl+z
  (frob "Refresh"                       ("gtk-refresh"))         ;; ctrl+r
  (frob "Reload"                        ("gtk-revert-to-saved")) ;; ctrl+r / shift+ctrl+r (see note in HIG)
  (frob "Remove"                        ("gtk-remove"))
  (frob "Revert To Saved"               ("gtk-revert-to-saved"))
  (frob "Save As"                       ("gtk-save-as"))
  (frob "Save"                          ("gtk-save"))            ;; ctrl+s
  (frob "Select All"                    ("gtk-select-all"))      ;; ctrl+a
  (frob "Select Color"                  ("gtk-select-color"))
  (frob "Select Colour"                 ("gtk-select-color"))
  (frob "Select Font"                   ("gtk-select-font"))
  (frob "Sort Ascending"                ("gtk-sort-ascending"))
  (frob "Sort Descending"               ("gtk-sort-descending"))
  (frob "Spell Check"                   ("gtk-spell-check"))
  (frob "Stop"                          ("gtk-stop"))
  (frob "Strikethrough"                 ("gtk-strikethrough"))
  (frob "Undelete"                      ("gtk-undelete"))
  (frob "Underline"                     ("gtk-underline"))       ;; ctrl+u
  (frob "Undo"                          ("gtk-undo"))            ;; ctrl+z
  (frob "Unindent"                      ("gtk-unindent"))
  (frob "Yes"                           ("gtk-yes"))
  (frob "Zoom 100"                      ("gtk-zoom-100"))
  (frob "Zoom Fit"                      ("gtk-zoom-fit"))
  (frob "Zoom In"                       ("gtk-zoom-in"))         ;; ctrl+Plus
  (frob "Zoom Out"                      ("gtk-zoom-out"))        ;; ctrl+minus
  )


#||
/// Menu bars

define sealed class <gtk-menu-bar>
    (<gtk-gadget-mixin>,
     <menu-bar>,
     <multiple-child-composite-pane>)
  keyword gtk-fixed-height?: = #t;
end class <gtk-menu-bar>;
||#

(defclass <gtk-menu-bar>
    (<gtk-gadget-mixin>
     <menu-bar>
     <multiple-child-composite-pane>)
  ()
  (:default-initargs :gtk-fixed-height? t))


(defmethod %gtk-fixed-height? ((gadget <gtk-menu-bar>))
  t)


#||
define sealed method class-for-make-pane
    (framem :: <gtk-frame-manager>, class == <menu-bar>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<gtk-menu-bar>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <gtk-frame-manager>)
				(class (eql (find-class '<menu-bar>)))
				&key)
  (values (find-class '<gtk-menu-bar>) nil))

#||
define sealed method make-gtk-mirror
    (gadget :: <gtk-menu-bar>)
 => (mirror :: <gadget-mirror>)
  let widget = GTK-MENU-BAR(gtk-menu-bar-new());
  make(<gadget-mirror>,
       widget: widget,
       sheet:  gadget)
end method make-gtk-mirror;
||#

(defmethod make-gtk-mirror ((gadget <gtk-menu-bar>))
  (let ((widget (cffi/gtk-menu-bar-new)))
    (let ((mirror (make-gtk-widget-mirror-instance (find-class '<gtk-menu-bar-mirror>)
						   :widget widget
						   :sheet  gadget)))
      ;; FIXME: Should we make the menu-bar's contents here too?
      (make-gtk-menu-bar-contents gadget mirror)
      ;; It seems we need to do this *before* the menu-bar is actually mapped
      ;; to force any kind of space composition (other than 0x0) by GTK.
      ;; FIXME: verify this is necessary.
      ;; FIXME: can we tell if the sheet is laid out to a sensible size so
      ;; that we can set the mirror to the correct size? At the moment in the
      ;; "multiple layout frame" test, the menu bar is initially laid out to
      ;; the correct size, but after changing layouts it isn't (the sheet size
      ;; doesn't change, so even though the mirror isn't sized correctly it is
      ;; never updated).
      (cffi/gtk-widget-show widget)
      mirror)))


#||
define sealed method make-gtk-menu-bar-contents
    (menu-bar :: <gtk-menu-bar>, mirror :: <gadget-mirror>) => ()
  let _port = port(menu-bar);
  compute-used-mnemonics(menu-bar);
  do(method (menu)
       menu.%port := _port;		//--- normally done in 'graft-sheet'
       make-mirror(_port, menu)
     end,
     sheet-children(menu-bar));
  mirror.%created? := #t
end method make-gtk-menu-bar-contents;
||#

;;; FIXME: we need to invoke "compute-used-mnemonics" somewhere!

(defmethod make-gtk-menu-bar-contents ((menu-bar <gtk-menu-bar>)
				       (mirror <gtk-gadget-mirror>))
  (let ((_port (port menu-bar)))
    (compute-used-mnemonics menu-bar)
    (map nil #'(lambda (menu)
		 (%port-setter _port menu)  ;;--- normally done in 'graft-sheet'
		 (make-mirror _port menu))
	 (sheet-children menu-bar))
    ;; this is not applicable for menu bars!
    #-(and)
    (setf (%created? mirror) t)))

#||
define sealed method refresh-menu-bar
    (menu-bar :: <gtk-menu-bar>) => ()
  let mirror = sheet-direct-mirror(menu-bar);
  if (mirror)
    ignoring("refresh-menu-bar")
  end
end method refresh-menu-bar;
||#

(defmethod refresh-menu-bar ((menu-bar <gtk-menu-bar>))
  (let ((mirror (sheet-direct-mirror menu-bar)))
    (when mirror
      ;; TODO: REMOVE ALL MENU-BAR CHILDREN + SET %CREATED? TO NIL.
      ;; TODO: INVOKE "MAKE-GTK-MENU-BAR-CONTENTS"
      (ignoring "refresh-menu-bar"))))


#||
/// Menu buttons

define open abstract class <gtk-menu-button-mixin>
    (<gtk-gadget-mixin>,
     <value-gadget>)
end class <gtk-menu-button-mixin>;
||#

(defclass <gtk-menu-button-mixin>
    (<gtk-gadget-mixin>
     <value-gadget>)
  ())

#||
define sealed method make-gtk-mirror
    (gadget :: <gtk-menu-button-mixin>)
 => (mirror :: <menu-button-mirror>)
  let selection-mode = gadget-selection-mode(gadget);
  let radio-button?  = selection-mode == #"single";
  let (text, image, mnemonic, index)
    = text-or-image-from-gadget-label(gadget);
  if (image)
    ignoring("menu button with image")
  end;
  unless (mnemonic)
    mnemonic := allocate-unique-mnemonic(gadget, text)
  end;
  with-c-string (c-string = text)
    let widget = GTK-MENU-ITEM(gtk-menu-item-new-with-label(c-string));
    make(<menu-button-mirror>,
	 widget: widget,
	 sheet:  gadget)
  end
end method make-gtk-mirror;
||#


(defun %insert-gtk-mnemonic-indicator! (text index mnemonic)
"
Inserts an underscore into 'text' to mark which character in the label
GTK should use for the mnemonic.  If 'text' contains an '&' leading
the mnemonic character, then this ampersand will be replaced by an
underscore.

Note that this function is destructive to 'text'.
"
  ;; If we have an index it means "text-or-image-from-gadget-label" managed to find a
  ;; mnemonic. Otherwise, the mnemonic was generated from "allocate-unique-mnemonic".
  (if index
      (let* ((leading-ampersand? (and (> index 0) (char= (char text (1- index)) #\&))))
	;;; TODO: Use 'replace'?
	(concatenate 'string
		     (subseq text 0 (if leading-ampersand? (1- index) index))
		     "_"
		     (subseq text index)))
      ;; else
      (if mnemonic
	  (let* ((index (position mnemonic text :test #'char-equal)))
	    (concatenate 'string
			 (subseq text 0 index)
			 "_"
			 (subseq text index)))
	  ;; else - no index and no mnemonic!
	  text)))


(defun %lookup-stock-id (gadget)
  ;; Use the gadget's label; this function is called after DUIM has pushed
  ;; a mnemonic indicator into the label which messes things up somewhat.
  (let* ((text          (defaulted-gadget-label gadget))
	 (length        (size text))
	 (dots          "...")
	 (dots?         (and (> length 3) (string= text dots :start1 (- length 3))))
	 (dotless-label (if dots? (subseq text 0 (- length 3)) text))
	 (stock-id      (first (gethash dotless-label *stock-item-table* nil))))
    stock-id))


(defmethod make-gtk-mirror ((gadget <gtk-menu-button-mixin>))
  ;; FIXME: Do we need to invoke "COMPUTE-USED-MNEMONICS"?
  (multiple-value-bind (text image mnemonic index)
      (text-or-image-from-gadget-label gadget)
    (when image
      ;; FIXME: Make use of any icon a user supplies, assuming that's valid? Check the docs.
      (ignoring "menu button with image"))
    (unless mnemonic
      (setf mnemonic (allocate-unique-mnemonic gadget text)))
    ;; If we found a mnemonic, replace any '&' with '_' or insert a '_' at the appropriate
    ;; place in the text (but don't lose the original 'text').
    (let* ((label (if mnemonic (%insert-gtk-mnemonic-indicator! text index mnemonic) text))
	   (accel-path nil)
	   (widget (ecase (gadget-selection-mode gadget)
		     (:none     (let ((stock-id (%lookup-stock-id gadget)))
				  (if stock-id
				      ;; The accelerator is not set here (accel-group = nil). It gets
				      ;; set later in 'install-accelerator' (called from
				      ;; 'update-mirror-attributes') so that user-supplied accelerators
				      ;; can override the gtk stock accelerators.
				      (cffi/gtk-image-menu-item-new-from-stock stock-id nil)
				      ;; else
				      (if mnemonic
					  (cffi/gtk-menu-item-new-with-mnemonic label)
					  (cffi/gtk-menu-item-new-with-label label)))))
		     ;; XXX: radio-menu-items are handled in their own
		     ;; make-gtk-mirror implementation (for now! might
		     ;; move them back in here once the code for them is
		     ;; stable).
		     (:multiple (if mnemonic
				    (cffi/gtk-check-menu-item-new-with-mnemonic label)
				    (cffi/gtk-check-menu-item-new-with-label label))))))
      (declare (ignore accel-path))
      ;; In order for a menu-bar to have any idea of a size-request it seem
      ;; to be necessary to invoke GTK-WIDGET-SHOW on a child menu item, so we
      ;; do that whenever we construct a GtkMenuItem.
      #-(and)
      (let* ((frame   (frame gadget))
	     (default (and frame (frame-default-button frame))))
	(when (eql gadget default)
	  ;; Only <push-button> and <push-menu-button>s grab default
	  (cffi/gtk-widget-set-can-default widget +true+)
	  (cffi/gtk-widget-grab-default widget)))
      (cffi/gtk-widget-show widget)
      (make-gtk-widget-mirror-instance (find-class '<gtk-menu-button-mirror>)
				       :widget widget
				       :sheet  gadget
				       #-(and):accel-path #-(and)accel-path))))


#||
define method install-event-handlers
    (sheet :: <gtk-menu-button-mixin>, mirror :: <gadget-mirror>) => ()
  next-method();
  install-named-handlers(mirror, #[#"activate"])
end method install-event-handlers;
||#

(defmethod install-event-handlers ((sheet <gtk-menu-button-mixin>)
				   (mirror <gtk-gadget-mirror>))
  (install-named-handlers mirror #("activate")))


#||
// #"activate" signal
define method gtk-activate-signal-handler (gadget :: <gtk-menu-button-mixin>,
					   user-data :: <gpointer>)
  ignore(user-data);
  activate-gtk-gadget(gadget);
end;
||#

;;; NOTE: The CLIENT of a menu button (from (gadget-client button)) should
;;; be any menu-box in which the button can be found.

(defmethod handle-gtk-activate-signal ((gadget <gtk-menu-button-mixin>)
				       widget
				       user-data)
  (declare (ignore widget user-data))
  (activate-gtk-gadget gadget))


#||
define method update-mirror-attributes
    (gadget :: <gtk-menu-button-mixin>, mirror :: <menu-button-mirror>) => ()
  next-method();
  let widget = mirror.mirror-widget;
end method update-mirror-attributes;
||#

(defmethod update-mirror-attributes ((gadget <gtk-menu-button-mixin>) (mirror <gtk-menu-button-mirror>))
  (call-next-method)
  #-(and)
  (let ((gadget-accelerator (defaulted-gadget-accelerator (frame-manager gadget) gadget)))
    (when (and gadget-accelerator (supplied? gadget-accelerator))
      (install-accelerator mirror gadget-accelerator)))
  (let ((widget (mirror-widget mirror)))
    (unless (gadget-enabled? gadget)
      (when widget
	(cffi/gtk-widget-set-sensitive widget +false+)))))

#-(and)
(defun install-accelerator (mirror duim-accelerator)
  ;; Get the accelerator table from the frame (do we have a frame atm?)
  ;; Convert duim accelerator to gtk accelerator and add it to accelerator table.
  (let ((widget       (mirror-widget mirror))
	(accel-group  (accelerator-table (mirror-sheet mirror)))
	(accel-key    (duim-keysym->gdk-keyval (gesture-keysym duim-accelerator)))
	(accel-mods   (duim-modifiers->gtk-state (gesture-modifier-state duim-accelerator)))
	;; FIXME: DO THESE FLAGS NEED TO BE SHIFTED BY THE +GDK-ACCEL-MASK+?
	(accel-flags  +CFFI/GTK-ACCEL-VISIBLE+))
    ;; XXX: Do we also need GTK-WIDGET-REMOVE-ACCELERATOR? How can we notice that no accelerators are set up
    ;; when GTK deals with firing off the accelerator action? Maybe the keyboard handling should be changed
    ;; so that *DUIM* deals with the accelerators + mnemonics, passing all other key events back off to
    ;; GTK unless we are in a sheet that handles the keyboard...? Bah, dunno. It seems that we'd lose a
    ;; lot that GTK provides for free if we do this.
    (cffi/gtk-widget-add-accelerator widget "activate" accel-group accel-key accel-mods accel-flags)))


#||
define sealed method note-gadget-label-changed
    (gadget :: <gtk-menu-button-mixin>) => ()
  next-method();
  ignoring("note-gadget-label-changed for menu button")
end method note-gadget-label-changed;
||#

(defmethod update-mirror-label ((gadget <gtk-menu-button-mixin>)
				(mirror <gtk-widget-mirror>))
  (let* ((widget (mirror-widget mirror))
	 (label  (cffi/gtk-bin-get-child widget)))
    ;; TODO 2020-04-13 DR:
    ;;   - do we want to get the 'defaulted-gadget-label' instead?
    ;;   - what about images, text/images, mnemonics?
    ;; Need more tests to check the behaviour of all the different DUIM
    ;; options (buttons with images on them etc.)
    (cffi/gtk-label-set-text label (gadget-label gadget))))



#||
/// Menu handling

define sealed class <gtk-menu>
    (<gtk-gadget-mixin>,
     <menu>,
     <multiple-child-composite-pane>)
  sealed slot menu-record-selection? = #f;
end class <gtk-menu>;
||#

(defclass <gtk-menu>
    (<gtk-gadget-mixin>
     <menu>
     <multiple-child-composite-pane>)
  ((menu-record-selection? :initform nil :accessor menu-record-selection?)))

#||
define sealed method class-for-make-pane 
    (framem :: <gtk-frame-manager>, class == <menu>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<gtk-menu>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <gtk-frame-manager>)
				(class (eql (find-class '<menu>)))
				&key)
  (values (find-class '<gtk-menu> nil)))


#||
define sealed method make-gtk-mirror
    (gadget :: <gtk-menu>)
 => (mirror :: <menu-mirror>)
  let (text, image, mnemonic, index)
    = text-or-image-from-gadget-label(gadget);
  if (image)
    ignoring("menu with image")
  end;
  with-c-string (c-string = text)
    let widget = GTK-MENU-ITEM(gtk-menu-item-new-with-label(c-string));
    let owner = menu-owner(gadget);
    let owner = if (frame?(owner)) top-level-sheet(owner) else owner end;
    make-menu-mirror-for-owner(owner, gadget, widget)
  end
end method make-gtk-mirror;
||#

(defmethod make-gtk-mirror ((gadget <gtk-menu>))
  ;; FIXME: Invoke "COMPUTE-USED-MNEMONICS"?
  (multiple-value-bind (text image mnemonic index)
      (text-or-image-from-gadget-label gadget)
    (when image
      ;; make a GtkImageMenuItem?
      (ignoring "menu with image"))
    (unless (or mnemonic (empty? text))
      (setf mnemonic (allocate-unique-mnemonic gadget text)))
    (when mnemonic
      ;; Furtle with text and stick a "_" in front of the mnemonic char.
      ;; No need to check for stock items here.
      (setf text (%insert-gtk-mnemonic-indicator! text index mnemonic)))
    (let* ((widget (if mnemonic
		       (cffi/gtk-menu-item-new-with-mnemonic text)
		       (cffi/gtk-menu-item-new-with-label text)))
	   (owner  (menu-owner gadget))
	   (owner  (if (framep owner)
		       (top-level-sheet owner)
		       owner)))
      (%ENSURE-GTK-SUBMENU WIDGET)
      ;; TODO 2020-04-13 DR: verify this "show widget" behaviour is still required
      ;; In order for a menu-bar to have any idea of a size-request it seems
      ;; to be necessary to invoke GTK-WIDGET-SHOW on a child menu item, so we
      ;; do that whenever we construct a GtkMenuItem.
      (cffi/gtk-widget-show widget)
      (let ((mirror (make-menu-mirror-for-owner owner gadget widget)))
	(refresh-menu gadget)
	mirror))))


#||
define sealed method make-menu-mirror-for-owner
    (owner :: <sheet>, gadget :: <gtk-menu>, widget :: <GtkMenuItem*>)
 => (mirror :: <popup-menu-mirror>)
  let selection-owner = menu-record-selection?(gadget) & gadget;
  make(<popup-menu-mirror>, 
       widget: widget,
       sheet:  gadget, 
       selection-owner: selection-owner)
end method make-menu-mirror-for-owner;
||#

(defmethod make-menu-mirror-for-owner ((owner <sheet>)
				       (gadget <gtk-menu>)
				       widget) ; <GtkMenuItem*>))
  (let ((selection-owner (and (menu-record-selection? gadget) gadget)))
    (make-gtk-widget-mirror-instance '<gtk-popup-menu-mirror>
				     :widget widget
				     :sheet  gadget
				     :selection-owner selection-owner)))

#||
define sealed method make-menu-mirror-for-owner
    (owner == #f, gadget :: <gtk-menu>, widget :: <GtkMenuItem*>)
 => (mirror :: <menu-mirror>)
  make(<menu-mirror>,
       widget: widget,
       sheet:  gadget)
end method make-menu-mirror-for-owner;
||#

(defmethod make-menu-mirror-for-owner ((owner null)
				       (gadget <gtk-menu>)
				       widget) ; <GtkMenuItem*>))
  (make-gtk-widget-mirror-instance '<gtk-menu-mirror>
				   :widget widget
				   :sheet  gadget))

#||
define sealed method map-mirror
    (_port :: <gtk-port>, menu :: <gtk-menu>, 
     mirror :: <popup-menu-mirror>) => ()
  ignoring("map-mirror on a popup menu")
end method map-mirror;
||#

(defmethod map-mirror ((_port <gtk-port>) (menu <gtk-menu>) (mirror <gtk-menu>))
  ;; GTK docs say not to invoke gtk_widget_show on menus (but ok on menu-bar and menu-item)
  ;; Can't find this explicitly in the documentation, but there are comments
  ;; in the gtk2 tutorial manual menu source example:
  ;; https://developer.gnome.org/gtk-tutorial/stable/x1577.html  
  (GTK-DEBUG "~%IGNORING MAP-MIRROR for <GTK-MENU> ~A" mirror)
  
  nil)

(defmethod unmap-mirror ((_port <gtk-port>) (menu <gtk-menu>) (mirror <gtk-menu>))
  ;; gtk_widget_show not invoked for menus (but ok on menu-bar and menu-item)
  ;; 2017-11-12
  (GTK-DEBUG "~%IGNORING UNMAP-MIRROR for <GTK-MENU> ~A" mirror)
  
  nil)

(defmethod map-mirror ((_port <gtk-port>)
		       (menu  <gtk-menu>)
		       (mirror <gtk-popup-menu-mirror>))
  ;; Invoked when we want to pop up a menu
  #-(and)
  (handle-menu-update menu)

  (let* ((widget  (gadget-widget menu))
	 (gtkmenu (cffi/gtk-menu-item-get-submenu widget)))
      
    (cffi/gtk-menu-popup gtkmenu
			 nil   ; parent-menu-shell
			 nil   ; parent-menu-item
			 nil   ; menu-position-func
			 nil   ; data
			 0     ; button
			 (cffi/gtk-get-current-event-time))
    ;; The deactivate" signal handler exits
    ;; this recursive main loop
    (cffi/gtk-main)))


(defmethod handle-gtk-deactivate-signal ((gadget <gtk-menu>)
					 widget
					 user-data)
  (declare (ignore widget user-data))
  (cffi/gtk-main-quit))


(defmethod set-sheet-position ((menu <gtk-menu>)
			       x y)
  (declare (ignore x y))
  nil)


;;; FIXME: Is this no-op method really necessary (other than duim chokes
;;; without it whilst trying to add the popup to the display)? Perhaps
;;; this should call "note-sheet-grafted" or something...
(defmethod set-mirror-parent ((child  <gtk-popup-menu-mirror>)
			      (parent <gtk-display-mirror>))
  (ignoring "SET-MIRROR-PARENT <GTK-POPUP-MENU-MIRROR> <GTK-DISPLAY-MIRROR>"))


#||
/*---*** Should be called just before a menu pops up
define method handle-menu-update
    (menu :: <gtk-menu>) => ()
  local method update-child-menu-boxes
	    (gadget :: <gadget>) => ()
	  execute-update-callback
	    (gadget, gadget-client(gadget), gadget-id(gadget));
	  for (child in sheet-children(gadget))
	    if (instance?(child, <menu-box>))
	      update-child-menu-boxes(child)
	    end
	  end
	end method update-child-menu-boxes;
  update-child-menu-boxes(menu);
  ensure-menus-mirrored(menu)
end method handle-menu-update;
||#

#-(and)
(defmethod handle-menu-update ((menu <gtk-menu>))
  (gtk-debug "[GTK-MENUS] HANDLE-MENU-UPDATE entered on menu ~a" menu)
;;   (labels ((update-child-menu-boxes (gadget)
;; 	     (execute-update-callback gadget
;; 				      (gadget-client gadget)
;; 				      (gadget-id gadget))
;; 	     (map nil #'(lambda (child)
;; 			  (when (typep child '<menu-box>)
;; 			    (update-child-menu-boxes child)))
;; 		  (sheet-children gadget))))
;;     (update-child-menu-boxes menu)
    (ensure-menus-mirrored menu))


#||
define sealed method ensure-menus-mirrored
    (gadget :: <gadget>) => ()
  let mirrored?
    = begin
	if (instance?(gadget, <gtk-menu>))
	  let mirror = sheet-direct-mirror(gadget);
	  if (mirror & ~mirror.%created?)
	    make-gtk-menu-contents(gadget, mirror);
	    #t
	  end
	end
      end;
  unless (mirrored?)
    do(ensure-menus-mirrored, sheet-children(gadget))
  end
end method ensure-menus-mirrored;
*/
||#

#-(and)
(defmethod ensure-menus-mirrored ((gadget <gadget>))
  (let ((mirrored? (when (typep gadget '<gtk-menu>)
		     (let ((mirror (sheet-direct-mirror gadget)))
		       (when (and mirror (not (%created? mirror)))
			 ;; FIXME: even with this commented out the menu
			 ;; is populated properly via the back-end's
			 ;; usual mirroring machinery.
			 #-(and)
			 (make-gtk-menu-contents gadget mirror)
			 t)))))
    (unless mirrored?
      (map nil #'ensure-menus-mirrored (sheet-children gadget)))))


#||
/// Menu handling

define sealed method make-gtk-menu-contents
    (menu :: <menu>, mirror :: <menu-mirror>) => ()
  let _port = port(menu);
  let widget = mirror.mirror-widget;
  let need-separator? = #f;
  let seen-item? = #f;
  local
    method add-separator () => ()
      ignoring("add-separator");
      need-separator? := #f;
      seen-item? := #f
    end method add-separator,

    method add-menu-children
	(gadget :: <gadget>) => ()
      for (child in sheet-children(gadget))
	select (child by instance?)
	  <menu> =>
	    child.%port := _port;	//--- normally done in 'graft-sheet'
	    make-mirror(_port, child);
	    seen-item? := #t;
	  <menu-box> =>
	    if (seen-item?) need-separator? := #t end;
	    add-menu-children(child);
	    need-separator? := #t;
	  <menu-button> =>
	    when (need-separator?) add-separator() end;
	    make-mirror(_port, child);
	    seen-item? := #t;
	end
      end;
      mirror.%created? := #t
    end method add-menu-children;
  add-menu-children(menu)
end method make-gtk-menu-contents;
||#

(defmethod make-gtk-menu-contents ((menu <menu>)
				   (mirror <gtk-menu-mirror>))
  (let* ((_port  (port menu))
	 (widget (mirror-widget mirror))
	 (shell  (cffi/gtk-menu-item-get-submenu widget))
	 (need-separator? nil)
	 (seen-item? nil))
    (labels ((add-separator ()
	       ;; Add a separation line by adding an empty menu item.
	       (let ((separator (cffi/gtk-menu-item-new)))
		 (cffi/gtk-menu-shell-append shell separator)
		 (cffi/gtk-widget-show separator))
	       (setf need-separator? nil)
	       (setf seen-item? nil))

	     (add-menu-children (gadget)
	       (map nil #'(lambda (child)
			    (etypecase child
			      (<menu>
			       (%port-setter _port child) ;;--- normally done in 'graft-sheet'
			       (make-mirror _port child)
			       (setf seen-item? t))
			      (<menu-box>
			       (when seen-item?
				 (setf need-separator? t))
			       ;; The children of the <menu-box> get added to the
			       ;; menu we're concerned with; the <menu-box> just
			       ;; serves to group related items in the menu.
			       (add-menu-children child)
			       (setf need-separator? t))
			      (<menu-button>
			       (when need-separator?
				 (add-separator))
			       (make-mirror _port child)
			       (setf seen-item? t))))
		    (sheet-children gadget))
	       (setf (%created? mirror) t)))
      (add-menu-children menu))))



#||
define sealed method remove-gtk-menu-contents
    (gadget :: <gtk-gadget-mixin>, mirror :: <menu-mirror>) => ()
  ignoring("remove-gtk-menu-contents")
end method remove-gtk-menu-contents;
||#

(defmethod remove-gtk-menu-contents ((gadget <gtk-gadget-mixin>)
				     (mirror <gtk-menu-mirror>))
  ;; FIXME: this appears to successfully remove the menu items, but they are never then added
  ;; back! Something is *presumably* supposed to kick off "refresh-menu" or similar I
  ;; guess -- why isn't it? [we don't see the "menu activated" signals from GTK, so that's
  ;; one problem at least. It may well also be the case that the logic in here is just wrong.]
  ;; Note that this doesn't remove all the items from the gadget. Weird.
  (do-sheet-tree #'(lambda (cgadget)
		     (when (and (not (eql cgadget gadget))
				(sheet-direct-mirror cgadget))
		       (let ((widget (gadget-widget cgadget)))
			 (cffi/gtk-widget-destroy widget))
		       ;; FIXME: is setting the direct mirror to nil sufficient? Are we going
		       ;; to try to deallocate the widget twice here (once in line above, once
		       ;; in response to it having its mirror set to nil)? It looks like
		       ;; *nothing* happens... so the independent destroy is necessary.
		       (setf (sheet-direct-mirror cgadget) nil)))
    gadget)
  (LET ((MENU-WIDGET (MIRROR-WIDGET MIRROR)))
    (CFFI/GTK-MENU-ITEM-SET-SUBMENU MENU-WIDGET NIL)
    (%ENSURE-GTK-SUBMENU MENU-WIDGET))
  ;; FIXME: SINCE WE DON'T SEEM TO GET ANY SIGNALS FROM GTK WHEN IT WANTS TO DISPLAY THE MENU
  ;; WE'LL REBUILD THE MENU HERE. THIS IS SUB-OPTIMAL BECAUSE THERE MAY BE SEVERAL MENU CHANGES
  ;; AND WE'LL END UP REBUILDING THE MENU SEVERAL TIMES IN THAT CASE WHEN IT *COULD* BE BUILT
  ;; JUST THE ONCE.
  (MAKE-GTK-MENU-CONTENTS GADGET MIRROR))


#||
define sealed method refresh-menu (menu :: <gtk-menu>) => ()
  let mirror = sheet-direct-mirror(menu);
  if (mirror)
    remove-gtk-menu-contents(menu, mirror);
    mirror.%created? := #f
  end
end method refresh-menu;
||#

(defmethod refresh-menu ((menu <gtk-menu>))
  (let ((mirror (sheet-direct-mirror menu)))
    (when mirror
      ;; This removes + destroys the menu's contents; what is it that is
      ;; supposed to rebuild them?
      (remove-gtk-menu-contents menu mirror)
      (setf (%created? mirror) nil))))

#||
define sealed method note-child-added
    (menu-bar :: <gtk-menu-bar>, menu :: <gtk-menu>) => ()
  ignore(menu);
  next-method();
  refresh-menu-bar(menu-bar)
end method note-child-added;
||#

(defmethod note-child-added ((menu-bar <gtk-menu-bar>)
			     (menu     <gtk-menu>))
  (declare (ignore menu))
  (call-next-method)
  (refresh-menu-bar menu-bar))

#||
define sealed method note-child-added
    (menu :: <gtk-menu>, child :: <gadget>) => ()
  ignore(child);
  next-method();
  refresh-menu(menu)
end method note-child-added;
||#

(defmethod note-child-added ((menu <gtk-menu>)
			     (child <gadget>))
  (declare (ignore child))
  (call-next-method)
  (refresh-menu menu))

#||
define sealed method note-child-added
    (gadget :: <menu-box>, child :: <gtk-menu-button-mixin>) => ()
  ignore(child);
  next-method();
  let menu = find-ancestor-of-type(gadget, <menu>);
  menu & refresh-menu(menu)
end method note-child-added;
||#

(defmethod note-child-added ((gadget <menu-box>)
			     (child  <gtk-menu-button-mixin>))
  (declare (ignore child))
  (call-next-method)
  (let ((menu (find-ancestor-of-type gadget (find-class '<menu>))))
    (and menu (refresh-menu menu))))

#||
define sealed method note-child-removed
    (menu-bar :: <gtk-menu-bar>, menu :: <gtk-menu>) => ()
  ignore(menu);
  next-method();
  refresh-menu-bar(menu-bar)
end method note-child-removed;
||#

(defmethod note-child-removed ((menu-bar <gtk-menu-bar>)
			       (menu     <gtk-menu>))
  (declare (ignore menu))
  (call-next-method)
  (refresh-menu-bar menu-bar))

#||
define sealed method note-child-removed
    (menu :: <gtk-menu>, child :: <gadget>) => ()
  ignore(child);
  next-method();
  refresh-menu(menu)
end method note-child-removed;
||#

(defmethod note-child-removed ((menu <gtk-menu>)
			       (child <gadget>))
  (declare (ignore child))
  (call-next-method)
  (refresh-menu menu))

#||
define sealed method note-child-removed
    (gadget :: <menu-box>, child :: <gtk-menu-button-mixin>) => ()
  ignore(child);
  next-method();
  let menu = find-ancestor-of-type(gadget, <menu>);
  menu & refresh-menu(menu)
end method note-child-removed;
||#

(defmethod note-child-removed ((gadget <menu-box>)
			       (child  <gtk-menu-button-mixin>))
  (declare (ignore child))
  (call-next-method)
  (let ((menu (find-ancestor-of-type gadget (find-class '<menu>))))
    (and menu (refresh-menu menu))))

#||
// Record the activation if necessary for popup menus, rather than
// doing it.  This is to handle 'choose-from-menu'.
define sealed method handle-gadget-activation
    (gadget :: <menu-button>) => (handled? :: <boolean>)
  let mirror = sheet-mirror(gadget);
  let selection-owner = mirror & mirror-selection-owner(mirror);
  if (selection-owner)
    let selection-mirror = sheet-direct-mirror(selection-owner);
    mirror-selected-gadget(selection-mirror) := gadget;
    #t
  else
    handle-button-gadget-click(gadget)
  end
end method handle-gadget-activation;
||#

(defmethod handle-gadget-activation ((gadget <menu-button>))
  (let* ((mirror (sheet-mirror gadget))
	 (selection-owner (and mirror (mirror-selection-owner mirror))))
    (if selection-owner
	(let ((selection-mirror (sheet-direct-mirror selection-owner)))
	  (setf (mirror-selected-gadget selection-mirror) gadget)
	  t)
	;; else
	(handle-button-gadget-click gadget))))

#||
define sealed method note-gadget-value-changed
    (gadget :: <gtk-menu-button-mixin>) => ()
  ignoring("note-gadget-value-changed for <menu-button>")
end method note-gadget-value-changed;
||#

(defmethod note-gadget-value-changed ((gadget <gtk-menu-button-mixin>))
  (ignoring "note-gadget-value-changed for <menu-button>")
  #-(and)
  (let ((mirror (sheet-mirror  gadget)))
    (when mirror
      (let ((widget (GTK-CHECK-MENU-ITEM (mirror-widget mirror)))
	    (value  (if (gadget-value gadget) +true+ +false+)))
	(case (gadget-selection-mode gadget)
	  ((:single :multiple)    ; update radio/check-box state
	   ;; FIXME:- this seems to result in an ACTIVATE signal being
	   ;; emitted. That's not good (throws the code into a loop).
	   ;; It doesn't play well with 'activate-gtk-gadget' on
	   ;; radio menu items :( *suspect* we don't need to set this
	   ;; explicitly. How about programatic value modification?
	   (gtk-check-menu-item-set-active widget value))
	  (otherwise nil))))))    ; do nothing

#||
define sealed method note-gadget-enabled 
    (client, gadget :: <gtk-menu-button-mixin>) => ()
  ignoring("note-gadget-enabled for <menu-button>")
end method note-gadget-enabled;
||#

(defmethod note-gadget-enabled (client (gadget <gtk-menu-button-mixin>))
  (declare (ignore client))
  (let* ((mirror (sheet-mirror gadget))
	 (widget (CFFI/GTK-WIDGET (mirror-widget mirror))))
    (cffi/gtk-widget-set-sensitive widget +true+)))

#||
define sealed method note-gadget-disabled
    (client, gadget :: <gtk-menu-button-mixin>) => ()
  ignoring("note-gadget-disabled for <menu-button>")
end method note-gadget-disabled;
||#

(defmethod note-gadget-disabled (client (gadget <gtk-menu-button-mixin>))
  (declare (ignore client))
  (let* ((mirror (sheet-mirror gadget))
	 (widget (CFFI/GTK-WIDGET (mirror-widget mirror))))
    (cffi/gtk-widget-set-sensitive widget +false+)))

#||
define sealed method note-gadget-enabled 
    (client, gadget :: <gtk-menu>) => ()
  ignoring("note-gadget-enabled for <menu>")
end method note-gadget-enabled;
||#

(defmethod note-gadget-enabled (client (gadget <gtk-menu>))
  (declare (ignore client))
  (let* ((mirror (sheet-mirror gadget))
	 (widget (CFFI/GTK-WIDGET (mirror-widget mirror))))
    (cffi/gtk-widget-set-sensitive widget +true+)))

#||
define sealed method note-gadget-disabled
    (client, gadget :: <gtk-menu>) => ()
  ignoring("note-gadget-disabled for <menu>")
end method note-gadget-disabled;
||#

(defmethod note-gadget-disabled (client (gadget <gtk-menu>))
  (declare (ignore client))
  (let* ((mirror (sheet-mirror gadget))
	 (widget (CFFI/GTK-WIDGET (mirror-widget mirror))))
    (cffi/gtk-widget-set-sensitive widget +false+)))


#||
/// The concrete menu button classes

define sealed class <gtk-push-menu-button>
    (<gtk-menu-button-mixin>,
     <push-menu-button>,
     <leaf-pane>)
end class <gtk-push-menu-button>;
||#

(defclass <gtk-push-menu-button>
    (<gtk-menu-button-mixin>
     <push-menu-button>
     <leaf-pane>)
  ())

#||
define sealed method class-for-make-pane
    (framem :: <gtk-frame-manager>, class == <push-menu-button>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<gtk-push-menu-button>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <gtk-frame-manager>)
				(class (eql (find-class '<push-menu-button>)))
				&key)
  (values (find-class '<gtk-push-menu-button>) nil))

#||
define sealed method gadget-default?-setter
    (default? :: <boolean>, gadget :: <gtk-push-menu-button>)
 => (default? :: <boolean>)
  next-method();
  ignoring("gadget-default?-setter for a menu button");
  default?
end method gadget-default?-setter;
||#

(defmethod (setf gadget-default?) (default? ;;boolean)
				   (gadget <gtk-push-menu-button>))
  (call-next-method)
  (ignoring "gadget-default?-setter for a menu button")
  default?)


#||
define sealed class <gtk-radio-menu-button>
    (<gtk-menu-button-mixin>,
     <radio-menu-button>,
     <leaf-pane>)
end class <gtk-radio-menu-button>;
||#

(defclass <gtk-radio-menu-button>
    (<gtk-menu-button-mixin>
     <radio-menu-button>
     <leaf-pane>)
  ())

#||
define sealed method class-for-make-pane 
    (framem :: <gtk-frame-manager>, class == <radio-menu-button>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<gtk-radio-menu-button>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <gtk-frame-manager>)
				(class (eql (find-class '<radio-menu-button>)))
				&key)
  (values (find-class '<gtk-radio-menu-button>) nil))


;;; FIXME: do we need to do this for check-buttons? No, because in GTK
;;; they cannot be in a group (they are/can be in DUIM).
;;; FIXME: we ignore any values set for the gadget. How do we know which
;;; radio-button to activate by default? This information must be in
;;; DUIM someplace...
(defmethod make-gtk-mirror ((gadget <gtk-radio-menu-button>))
;;  (duim-debug-message "gtk-menus.MAKE-GTK-MIRROR <radio-menu-button> entered with gadget label ~a"
;;		      (defaulted-gadget-label gadget))
  (multiple-value-bind (text image mnemonic index)
      (text-or-image-from-gadget-label gadget)
    (when image
      ;; FIXME: Make a GtkImageMenuItem [is this valid for radio buttons?]
      (ignoring "menu button with image"))
    (unless mnemonic
      (setf mnemonic (allocate-unique-mnemonic gadget text)))
    (when mnemonic
      ;; Furtle with text and stick a "_" in front of the mnemonic char.
      ;; No need to check for stock items here.
      (setf text (%insert-gtk-mnemonic-indicator! text index mnemonic)))
    ;; FIXME: we need to do this for 'normal' radio and check button groups
    ;; too, so this should *probably* be moved out into a utility method
    ;; someplace accessible from both gtk-menus and gtk-gadgets.
    ;; Unfortunately we invoke different gtk native methods to work out
    ;; the group id / GSList* for different button types. Bah.
    (flet ((identify-gtk-button-group (sheet)
	     ;; FIXME: do we need to use GADGET-CLIENT instead of
	     ;; BUTTON-GADGET-BOX? Even if we don't, is it clearer and
	     ;; robust?
	     (let ((box (button-gadget-box sheet)))
	       (when box
		 ;; Find the first child that is mirrored and ask for its
		 ;; group. If no children are mirrored, don't set a group;
		 ;; subsequent buttons in the box will inherit the group
		 ;; from this radio button.
		 (map nil #'(lambda (child)
			      (let ((mirror (sheet-direct-mirror child)))
				(when mirror
				  (let ((widget (mirror-widget mirror)))
				    (return-from identify-gtk-button-group
				      (cffi/gtk-radio-menu-item-get-group widget))))))
		      (sheet-children box))))
	     nil))
      (let* ((group  (identify-gtk-button-group gadget))
	     (widget (if mnemonic
			 (cffi/gtk-radio-menu-item-new-with-mnemonic group text)
			 (cffi/gtk-radio-menu-item-new-with-label group text))))
	;;	  (duim-mem-debug "gtk-menus.MAKE-GTK-MIRROR radio-menu-button widget" widget)
	(make-gtk-widget-mirror-instance (find-class '<gtk-menu-button-mirror>)
					 :widget widget
					 :sheet  gadget)))))


;;; No <action-gadget-mixin> in radio menu buttons...
(defmethod handle-gtk-activate-signal ((gadget <gtk-radio-menu-button>)
				       widget
				       user-data)
  (declare (ignore widget user-data))
  (handle-button-gadget-click gadget))

#||
define sealed class <gtk-check-menu-button>
    (<gtk-menu-button-mixin>,
     <check-menu-button>,
     <leaf-pane>)
end class <gtk-check-menu-button>;
||#

(defclass <gtk-check-menu-button>
    (<gtk-menu-button-mixin>
     <check-menu-button>
     <leaf-pane>)
  ())

#||
define sealed method class-for-make-pane 
    (framem :: <gtk-frame-manager>, class == <check-menu-button>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<gtk-check-menu-button>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <gtk-frame-manager>)
				(class (eql (find-class '<check-menu-button>)))
				&key)
  (values (find-class '<gtk-check-menu-button>) nil))


;;; No <action-gadget-mixin> in check menu buttons...
(defmethod handle-gtk-activate-signal ((gadget <gtk-check-menu-button>)
				       widget
				       user-data)
  (declare (ignore widget user-data))
  (handle-button-gadget-click gadget))


#||
/// Choose from menu

define sealed method do-choose-from-menu
    (framem :: <gtk-frame-manager>, owner :: <sheet>, menu :: <menu>,
     #key title, value, label-key, value-key,
          width, height, foreground, background, text-style,
          multiple-sets?,
     #all-keys)
 => (value, success? :: <boolean>)
  ignore(title, value, label-key, value-key, 
	 width, height, foreground, background, text-style, multiple-sets?);
  // record-selection? determines whether the events are distributed or
  // just recorded so that we can pick them up afterwards.
  menu-record-selection?(menu) := #t;
  menu-owner(menu) := owner;
  sheet-mapped?(menu) := #t;
  let mirror = sheet-mirror(menu);
  let selected-button = mirror-selected-gadget(mirror);
  values(selected-button & button-gadget-value(selected-button),
         selected-button & #t)
end method do-choose-from-menu;
||#

(defmethod do-choose-from-menu ((framem <gtk-frame-manager>)
				(owner  <sheet>)
				(menu   <menu>)
				&key title value label-key value-key
				width height foreground background text-style
				multiple-sets?
				&allow-other-keys)
  (declare (ignore title value label-key value-key
		   width height foreground background text-style multiple-sets?))
  (duim-debug-message "gtk-menus.DO-CHOOSE-FROM-MENU entered")
  ;; record-selection? determines whether the events are distributed or
  ;; just recorded so that we can pick them up afterwards
  (setf (menu-record-selection? menu) t
	(menu-owner menu) owner
	(sheet-mapped? menu) t)
  ;;; FIXME: THIS WILL SUFFER FROM THE SAME FATE AS GENERAL POPUP MENUS CURRENTLY
  ;;; WHICH IS THAT THE POPUP IS SHOWN, THEN HIDDEN. WHILST THAT PROBLEM IS
  ;;; MITIGATED BY REFUSING TO UNMAP-MIRROR ON THE POPUP, UNFORTUNATELY THIS
  ;;; METHOD IS EXPECTING EVERYTHING TO BE BLOCKED WHILST THE POPUP IS ON THE
  ;;; SCREEN (WHICH CURRENTLY IT WON'T / DOESN'T) SO WE'RE ALWAYS GOING TO
  ;;; GET (nil nil) BACK FROM THIS METHOD :(
  ;;; --- SHOULD WE MAKE POPUPS BLOCK? IT'LL BE MORE COMPLICATED (NESTED EVENT
  ;;; --- LOOPS!) BUT IS IT "THE RIGHT THING"? I don't want to be managing event
  ;;; --- loops in GTK if it's not absolutely positively completely necessary.
  ;;; (If we do go that route, use "GTK_MAIN()" + "GTK_MAIN_QUIT()" to manage the
  ;;; secondary event loop)
  (let* ((mirror (sheet-mirror menu))
	 (selected-button (mirror-selected-gadget mirror)))
    (values (and selected-button (button-gadget-value selected-button))
	    (and selected-button t))))



