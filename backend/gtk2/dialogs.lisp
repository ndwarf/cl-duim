;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GTK2-DUIM -*-
(in-package #:gtk2-duim)

#||
Module:       gtk-duim
Synopsis:     GTK dialog implementation
Author:       Andy Armstrong, Scott McKay
Copyright:    Original Code is Copyright (c) 1995-2004 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND
||#

(defgeneric cancel-frame (dialog))
(defgeneric ensure-dialog-position (frame mirror))
(defgeneric compute-dialog-position (frame))

#||
/// Useful constants

define constant $dialog-border         = 10;
define constant $dialog-spacing        = 10;
define constant $exit-button-min-width = 100;
||#

(defconstant +dialog-border+ 10)
(defconstant +dialog-spacing+ 10)
(defconstant +exit-button-min-width+ 100)



#||
/// DUIM dialogs

define sealed class <dialog-mirror> (<top-level-mirror>)
  sealed constant slot %owner :: false-or(<frame>),
    required-init-keyword: owner:;
end class <dialog-mirror>;

define sealed domain make (singleton(<dialog-mirror>));
define sealed domain initialize (<dialog-mirror>);


ignore(mirror-registered-dialogs);

define sealed method make-top-level-mirror
    (sheet :: <top-level-sheet>, frame :: <dialog-frame>)
 => (mirror :: <top-level-mirror>)
  let widget = GTK-WINDOW(gtk-window-new($GTK-WINDOW-DIALOG));
  let owner = frame-owner(frame);
  make(<dialog-mirror>,
       widget: widget,
       sheet:  sheet,
       owner: owner)
end method make-top-level-mirror;
||#

(defmethod make-top-level-mirror ((sheet <top-level-sheet>)
				  (frame <dialog-frame>))
  (let ((widget (cffi/gtk-dialog-new))
	(owner  (frame-owner frame)))
    (make-gtk-widget-mirror-instance (find-class '<gtk-dialog-mirror>)
				     :widget widget
				     :sheet  sheet
				     :owner  owner)))


(defmethod set-mirror-parent ((child <gtk-widget-mirror>)
			      (parent <gtk-dialog-mirror>))
  (let* ((widget (mirror-widget parent))
	 (dialog-vbox (cffi/gtk-dialog-get-content-area widget)))
    (cffi/gtk-container-add dialog-vbox (mirror-widget child))))


(defmethod install-event-handlers ((sheet <gtk-top-level-sheet-mixin>)
				   (mirror <gtk-dialog-mirror>))
  (install-named-handlers mirror #("delete-event"         ; 'ESC' hit
				   "set-focus"            ; focus change within frame
				   "focus-in-event"       ; frame gained focus
				   "button-press-event"   ; mouse button pressed
				   "button-release-event" ; mouse button released
				   ;; + motion-notify-event?
				   )))


#||
define sealed method cancel-frame
    (dialog :: <dialog-frame>) => (handled? :: <boolean>)
  let button = dialog-cancel-button(dialog);
  when (button & gadget-enabled?(button))
    handle-gadget-activation(button)
  end
end method cancel-frame;
||#

(defmethod cancel-frame ((dialog <dialog-frame>))
  ;; Arrange to call the button activation callback if there is an
  ;; enabled cancel button
  (let ((button (dialog-cancel-button dialog)))
    (if (and button (gadget-enabled? button))
	(handle-button-gadget-click button)
	(cancel-dialog dialog :destroy? t))))


#||
define sealed method map-mirror
    (_port :: <gtk-port>,
     sheet :: <gtk-top-level-sheet-mixin>, mirror :: <dialog-mirror>) => ()
  ensure-dialog-position(sheet-frame(sheet), mirror);
  next-method();
  let dialog = sheet-frame(sheet);
  let owner  = frame-owner(dialog);
  owner & register-dialog-mirror(owner, mirror)
end method map-mirror;
||#

(defmethod map-mirror ((_port <gtk-port>)
		       (sheet <gtk-top-level-sheet-mixin>)
		       (mirror <gtk-dialog-mirror>))
  (let* ((dialog (sheet-frame sheet))
	 (owner (frame-owner dialog)))
    (call-next-method)
    (and owner (register-dialog-mirror owner mirror))))


#||
define sealed method unmap-mirror
    (_port :: <gtk-port>,
     sheet :: <gtk-top-level-sheet-mixin>, mirror :: <dialog-mirror>) => ()
  next-method();
  let dialog = sheet-frame(sheet);
  let owner  = frame-owner(dialog);
  owner & unregister-dialog-mirror(owner, mirror)
end method unmap-mirror;
||#

(defmethod unmap-mirror ((_port <gtk-port>)
			 (sheet <gtk-top-level-sheet-mixin>)
			 (mirror <gtk-dialog-mirror>))
  (call-next-method)
  (let* ((dialog (sheet-frame sheet))
	 (owner  (frame-owner dialog)))
    (and owner (unregister-dialog-mirror owner mirror))))

#||
define sealed method ensure-dialog-position
    (frame :: <dialog-frame>, mirror :: <dialog-mirror>) => ()
  ignoring("ensure-dialog-position")
end method ensure-dialog-position;


define sealed method compute-dialog-position
    (frame :: <dialog-frame>)
 => (x :: <integer>, y :: <integer>)
  //--- Is there a better way to get this?  'frame-position' always
  //--- gets the info from the top level sheet which isn't what
  //--- we want here.
  let sheet = top-level-sheet(frame);
  let (width, height) = sheet-size(sheet);
  let geometry = frame-geometry(frame);
  let frame-x = geometry[0];
  let frame-y = geometry[1];
  let frame-width = geometry[2];
  let frame-height = geometry[3];
  let width = frame-width | width;
  let height = frame-height | height;
  if (frame-x & frame-y)
    values(frame-x, frame-y)
  else
    let _display = display(sheet);
    let owner = frame-owner(frame);
    let owner-top-sheet = owner & top-level-sheet(owner);
    let owner-mirror = owner-top-sheet & sheet-direct-mirror(owner-top-sheet);
    let (screen-width, screen-height) = sheet-size(_display);
    if (owner-mirror)
      // Center the dialog over the client area
      let (owner-x, owner-y) = client-to-screen-position(owner-mirror, 0, 0);
      // let owner-handle = window-handle(owner-mirror);
      // let (owner-width, owner-height) = get-client-size(owner-handle);
      let (owner-width, owner-height) = sheet-size(owner);
      duim-debug-message("  Owner currently %d x %d, at %d, %d",
			 owner-width, owner-height, owner-x, owner-y);
      duim-debug-message("  Dialog currently %d x %d",
			 width, height);
      let x
	= max(min(screen-width - width,
		  owner-x + floor/(owner-width  - width, 2)),
	      0);
      let y
	= max(min(screen-height - height,
		  owner-y + floor/(owner-height - height, 2)),
	      0);
      values(x, y)
    else
      // Center the dialog on the screen
      values(max(floor/(screen-width  - width, 2),  0),
	     max(floor/(screen-height - height, 2), 0))
    end
  end
end method compute-dialog-position;



/// Piggy-back on the default dialogs from gadget-panes for now

define method top-level-layout-child
    (framem :: <gtk-frame-manager>, 
     dialog :: <dialog-frame>,
     layout :: false-or(<sheet>))
 => (layout :: false-or(<sheet>))
  default-dialog-frame-wrapper(framem, dialog, layout);
end method top-level-layout-child;
||#

(defmethod top-level-layout-child ((framem <gtk-frame-manager>)
				   (dialog <dialog-frame>)
				   layout)  ;; false-or(<sheet>)
  (default-dialog-frame-wrapper framem dialog layout))

#||
define sealed method update-frame-layout
    (framem :: <gtk-frame-manager>, frame :: <dialog-frame>) => ()
  update-default-dialog-layout(framem, frame)
end method update-frame-layout;
||#

(defmethod update-frame-layout ((framem <gtk-frame-manager>)
				(frame  <dialog-frame>))
  (update-default-dialog-layout framem frame))

#||
define method make-exit-button
    (framem :: <gtk-frame-manager>, dialog :: <dialog-frame>,
     callback :: false-or(<callback-type>), label :: <string>,
     #rest initargs, 
     #key enabled? = (callback ~= #f), #all-keys)
 => (button :: false-or(<push-button>))
  when (callback)
    with-frame-manager (framem)
      apply(make, <push-button>,
	    activate-callback: method (button)
				 let dialog = sheet-frame(button);
				 execute-callback(dialog, callback, dialog)
			       end,
	    label: label,
	    enabled?: enabled?,
	    min-width: $exit-button-min-width,
	    initargs)
    end
  end
end method make-exit-button;
||#

(defmethod make-exit-button ((framem <gtk-frame-manager>)
			     (dialog <dialog-frame>)
			     activate-callback
			     (label string)
			     &rest initargs
			     &key (enabled? (and activate-callback t))
			     &allow-other-keys)
  (when activate-callback
    (with-frame-manager (framem)
      (apply #'make-pane '<push-button>
	     :activate-callback #'(lambda (button)
				    (let ((dialog (sheet-frame button)))
				      (execute-callback dialog
							activate-callback
							dialog)))
	     :label label
	     :enabled? enabled?
	     :min-width +exit-button-min-width+
	     initargs))))

#||
define sealed method default-dialog-border
    (framem :: <gtk-frame-manager>, dialog :: <dialog-frame>)
 => (border :: <integer>)
  $dialog-border
end method default-dialog-border;
||#

(defmethod default-dialog-border ((framem <gtk-frame-manager>)
				  (dialog <dialog-frame>))
  +dialog-border+)

#||
define sealed method default-dialog-spacing
    (framem :: <gtk-frame-manager>, dialog :: <dialog-frame>)
 => (border :: <integer>)
  $dialog-spacing
end method default-dialog-spacing;
||#

(defmethod default-dialog-spacing ((framem <gtk-frame-manager>)
				   (dialog <dialog-frame>))
  +dialog-spacing+)

#||
define sealed method default-dialog-button-spacing
    (framem :: <gtk-frame-manager>, dialog :: <dialog-frame>)
 => (border :: <integer>)
  select (dialog-exit-buttons-position(dialog))
    #"left", #"right" => 8;
    #"top", #"bottom" => 8;
    otherwise         => 8;
  end
end method default-dialog-button-spacing;
||#

(defmethod default-dialog-button-spacing ((framem <gtk-frame-manager>)
					  (dialog <dialog-frame>))
  ;; There's a "button-spacing" style property in GTK for this
  ;; we could use.
  (case (dialog-exit-buttons-position dialog)
    ((:left :right) 8)
    ((:top :bottom) 8)
    (t              8)))

#||
define sealed method default-dialog-extra-size
    (framem :: <gtk-frame-manager>, dialog :: <dialog-frame>)
 => (width :: <integer>, height :: <integer>)
  ignore(framem);
  ignoring("default-dialog-extra-size");
  values(0, 0)
end method default-dialog-extra-size;
||#

(defmethod default-dialog-extra-size ((framem <gtk-frame-manager>)
				      (dialog <dialog-frame>))
  (declare (ignore framem))
  (ignoring "default-dialog-extra-size")
  (values 0 0))


#||
/// Dialog flow control

// Generate an ordinary exit event
define sealed method do-exit-dialog
    (framem :: <gtk-frame-manager>, dialog :: <dialog-frame>, #key destroy? = #t) => ()
  // Under GTK, we need to re-enable the owner before we dismiss
  // the dialog so that the focus gets returned to the right place
  let owner  = frame-owner(dialog);
  let modal? = (frame-mode(dialog) == #"modal");
  when (owner & modal?)
    frame-enabled?(owner) := #t
  end;
  frame-mapped?(dialog) := #f;
  distribute-event(port(dialog),
		   make(<dialog-exit-event>,
			frame: dialog,
			destroy-frame?: destroy?))
end method do-exit-dialog;
||#

(defmethod do-exit-dialog ((framem <gtk-frame-manager>)
			   (dialog <dialog-frame>)
			   &key (destroy? t))
  ;; Under GTK we need to re-enable the owner before we dismiss
  ;; the dialog so that the focus gets returned to the right place
  (let ((owner (frame-owner dialog))
	(modal? (eql (frame-mode dialog) :modal)))
    (when (and owner modal?)
      (setf (frame-enabled? owner) t))
    (setf (frame-mapped? dialog) nil)
    (distribute-event (port dialog)
		      (make-instance '<dialog-exit-event>
				     :frame dialog
				     :destroy-frame? destroy?))))

#||
// Generate an "error" exit event
define sealed method do-cancel-dialog 
    (framem :: <gtk-frame-manager>, dialog :: <dialog-frame>, #key destroy? = #t) => ()
  let owner  = frame-owner(dialog);
  let modal? = (frame-mode(dialog) == #"modal");
  when (owner & modal?)
    frame-enabled?(owner) := #t
  end;
  frame-mapped?(dialog) := #f;
  distribute-event(port(dialog),
		   make(<dialog-cancel-event>,
			frame: dialog,
			destroy-frame?: destroy?))
end method do-cancel-dialog;
||#

(defmethod do-cancel-dialog ((framem <gtk-frame-manager>)
			     (dialog <dialog-frame>)
			     &key (destroy? t))
  (let ((owner (frame-owner dialog))
	(modal? (eq (frame-mode dialog) :modal)))
    (when (and owner modal?)
      (setf (frame-enabled? owner) t))
    (setf (frame-mapped? dialog) nil)
    (distribute-event (port dialog)
		      (make-instance '<dialog-cancel-event>
				     :frame dialog
				     :destroy-frame? destroy?))))


#||
/// Notify user

define class <notification-dialog> (<dialog-frame>)
  slot notification-dialog-result :: one-of(#"yes", #"no", #"ok", #"cancel"),
    init-value: #"cancel";
  constant slot notification-dialog-exit-style :: <notification-exit-style>,
    required-init-keyword: exit-style:;
  constant slot notification-dialog-yes-callback :: false-or(<function>) = #f,
    init-keyword: yes-callback:;
  constant slot notification-dialog-no-callback :: false-or(<function>) = #f,
    init-keyword: no-callback:;
end class;
||#

(defclass <gtk-notification-dialog> (<dialog-frame>)
  ((notification-dialog-result :type (member :yes :no :ok :cancel)
			       :initform :cancel
			       :accessor notification-dialog-result)
   (notification-dialog-exit-style :type <notification-exit-style>
				   :initarg :exit-style
				   :initform (required-slot ":exit-style" "<gtk-notification-dialog>")
				   :reader notification-dialog-exit-style)
   (notification-dialog-yes-callback :type (or null function)
				     :initarg :yes-callback
				     :initform nil
				     :accessor notification-dialog-yes-callback)
   (notification-dialog-no-callback :type (or null function)
				    :initarg :no-callback
				    :initform nil
				    :reader notification-dialog-no-callback)))

#||
define sealed method do-notify-user
    (framem :: <gtk-frame-manager>, owner :: <sheet>,
     message :: <string>, style :: <notification-style>,
     #key title, documentation, exit-boxes, name,
          exit-style :: false-or(<notification-exit-style>) = #f,
          foreground, background, text-style)
 => (ok? :: <boolean>, exit-type)
  ignore(exit-boxes);
  let (x, y)
    = begin
	let (x, y) = sheet-size(owner);
	let (x, y) = values(floor/(x, 2), floor/(y, 2));
	with-device-coordinates (sheet-device-transform(owner), x, y)
	  values(x, y)
	end
      end;
  let title
    = title | select (style)
		#"information"   => "Note";
		#"question"      => "Note";
		#"warning"       => "Warning";
		#"error"         => "Error";
		#"serious-error" => "Error";
		#"fatal-error"   => "Error";
	      end;
  let exit-style
    = exit-style | if (style == #"question") #"yes-no" else #"ok" end;
  local
    method notify-callback(dialog :: <notification-dialog>,
                           result :: one-of(#"yes", #"no", #"ok"))
     => ();
      dialog.notification-dialog-result := result;
      exit-dialog(dialog);
    end method;
  let exit-options
    = select(exit-style)
        #"ok" =>
          vector(exit-callback: rcurry(notify-callback, #"ok"),
                 cancel-callback: #f);
        #"ok-cancel" =>
          vector(exit-callback: rcurry(notify-callback, #"ok"));
        #"yes-no" =>
          vector(yes-callback: rcurry(notify-callback, #"yes"),
                 no-callback: rcurry(notify-callback, #"no"),
                 exit-callback: #f,
                 cancel-callback: #f);
        #"yes-no-cancel" =>
          vector(yes-callback: rcurry(notify-callback, #"yes"),
                 no-callback: rcurry(notify-callback, #"no"),
                 exit-callback: #f);
      end;
        
  let dialog = apply(make,
                     <notification-dialog>,
                     x: x, y: y,
                     title: title,
                     foreground: foreground,
                     background: background,
                     exit-style: exit-style,
                     layout: make(<label>,
                                 label: message,
                                 text-style: text-style),
                     exit-options);
  if (start-dialog(dialog))
    select (dialog.notification-dialog-result)
      #"yes"    => values(#t, #"yes");
      #"no"     => values(#f, #"no");
      #"ok"     => values(#t, #"ok");
      #"cancel" => values(#f, #"cancel");
    end;
  else
    values(#f, #"cancel");
  end if;
end method do-notify-user;
||#

;;; Just use a GtkMessageDialog for this?

(defmethod do-notify-user ((framem <gtk-frame-manager>)
			   (owner  <sheet>)
			   (message string)
			   style
			   &key title documentation exit-boxes name
			   exit-style foreground background text-style)
  (declare (ignore exit-boxes documentation name))
  (check-type style <notification-style>)
  (multiple-value-bind (x y)
      (multiple-value-bind (x y)
	  (sheet-size owner)
	(multiple-value-bind (x y)
	    (values (floor x 2) (floor y 2))
	  (with-device-coordinates ((sheet-device-transform owner) x y)
	    (values x y))))
    (let* ((title (or title (case style
			      (:information   "Note")     ; => +GTK-MESSAGE-INFO+     -- see GtkMessageType
			      (:question      "Note")     ; => +GTK-MESSAGE-QUESTION+
			      (:warning       "Warning")  ; => +GTK-MESSAGE-WARNING+
			      (:error         "Error")    ; => +GTK-MESSAGE-ERROR+
			      (:serious-error "Error")    ; => +GTK-MESSAGE-ERROR+
			      (:fatal-error   "Error")))) ; => +GTK-MESSAGE-ERROR+
	   (exit-style (or exit-style (if (eql style :question) :yes-no :ok))))
      (labels ((notify-callback (dialog result)
		 (setf (notification-dialog-result dialog) result)
		 (exit-dialog dialog)))
	(let* ((exit-options (ecase exit-style
			       (:ok
				(list :exit-callback
				      (alexandria:rcurry #'notify-callback :ok)
				      :cancel-callback nil))
			       (:ok-cancel
				(list :exit-callback
				      (alexandria:rcurry #'notify-callback :ok)))
			       (:yes-no
				(list :yes-callback
				      (alexandria:rcurry #'notify-callback :yes)
				      :no-callback
				      (alexandria:rcurry #'notify-callback :no)
				      :exit-callback nil
				      :cancel-callback nil))
			       (:yes-no-cancel
				(list :yes-callback
				      (alexandria:rcurry #'notify-callback :yes)
				      :no-callback
				      (alexandria:rcurry #'notify-callback :no)
				      :exit-callback nil))))
	       ;; Use MAKE-INSTANCE, not MAKE-PANE, for frames!
	       (dialog (apply #'make-instance '<gtk-notification-dialog>
			     :x x :y y :title title
			     :foreground foreground
			     :background background
			     :exit-style exit-style
			     :layout (make-pane '<label>
						:label message
						:text-style text-style)
			     exit-options)))
	  (if (start-dialog dialog)
	      (progn
		(GTK-DEBUG "START-DIALOG RETURNED T")
		(ecase (notification-dialog-result dialog)
		  (:yes    (values t   :yes))
		  (:no     (values nil :no))
		  (:ok     (values t   :ok))
		  (:cancel (values nil :cancel))))
	      (progn
		(GTK-DEBUG "START-DIALOG RETURNED NIL")
		(values nil :cancel))))))))


#||
define sealed method make-exit-buttons
    (framem :: <gtk-frame-manager>, dialog :: <notification-dialog>)
 => (buttons :: <sequence>)
  select(dialog.notification-dialog-exit-style)
    #"ok", #"ok-cancel" =>
      next-method();
    #"yes-no", #"yes-no-cancel" =>
      let yes-button
        = make-exit-button(framem, dialog,
                           dialog.notification-dialog-yes-callback, "Yes");
      let no-button
        = make-exit-button(framem, dialog,
                           dialog.notification-dialog-no-callback, "No");
      unless (frame-default-button(dialog))
        frame-default-button(dialog) := yes-button;
      end;
      concatenate(vector(yes-button, no-button), next-method());
  end select;
end method make-exit-buttons;
||#

(defmethod make-exit-buttons ((framem <gtk-frame-manager>)
			      (dialog <gtk-notification-dialog>))
  (ecase (notification-dialog-exit-style dialog)
    ((:ok :ok-cancel) (call-next-method))
    ((:yes-no :yes-no-cancel)
     (let ((yes-button (make-exit-button framem dialog
					 (notification-dialog-yes-callback dialog)
					 "Yes"))
	   (no-button (make-exit-button framem dialog
					(notification-dialog-no-callback dialog)
					"No")))
       (unless (frame-default-button dialog)
	 (setf (frame-default-button dialog) yes-button))
       (concatenate 'vector (vector yes-button no-button) (call-next-method))))))


#||
/// Choose file
/*---
define sealed method do-choose-file
    (framem :: <gtk-frame-manager>, owner :: <sheet>, 
     direction :: one-of(#"input", #"output"),
     #key title :: false-or(<string>), documentation :: false-or(<string>), exit-boxes,
	  if-exists, if-does-not-exist = #"ask",
	  default :: false-or(<string>), default-type = $unsupplied,
	  filters, default-filter,
     #all-keys)
 => (locator :: false-or(<string>), filter :: false-or(<integer>))
  ignore(if-exists);
  let _port     = port(owner);
  let x-display = _port.%display;
  let parent-widget = mirror-widget(sheet-mirror(owner));
  let (visual, colormap, depth) = xt/widget-visual-specs(parent-widget);
  let (directory, pattern) = gtk-directory-and-pattern(default, default-type);
  let shell-resources
    = vector(visual:, visual,
	     colormap:, colormap,
	     depth:, depth);
  let resources
    = vector(directory:, directory,
	     pattern:, pattern,
	     dialog-title:, title, 
	     dialog-style:, xm/$XmDIALOG-FULL-APPLICATION-MODAL,
	     default-position:, #f);

  when (file-label)
    resources
      := concatenate!(resources, vector(file-list-label-string:, file-label))
  end;
  when (directory-label)
    resources
      := concatenate!(resources, vector(dir-list-label-string:, directory-label))
  end;
  let (x, y)
    = begin
	let (x, y) = sheet-size(owner);
	let (x, y) = values(floor/(x, 2), floor/(y, 2));
	with-device-coordinates (sheet-device-transform(owner), x, y)
	  values(x, y)
	end
      end;
  let shell  = #f;
  let dialog = #f;
  let result = #f;
  let client-data  = #f;
  block ()
    local method waiter () result end method,
	  method setter (value) result := value end method;
    shell  := xt/XtCreatePopupShell("ChooseFileShell", xm/<dialog-shell>, parent-widget,
				   resources: shell-resources);
    dialog := xm/XmCreateFileSelectionBox(shell, "ChooseFile",
					  resources: resources);
    client-data := make(<callback-client-data>,
			owner-widget: parent-widget,
			x-display: x-display,
			pointer-x: x,
			pointer-y: y,
			setter: setter);
    xt/XtAddCallback(dialog, "okCallback",     choose-file-button-press-callback, client-data);
    xt/XtAddCallback(dialog, "cancelCallback", choose-file-button-press-callback, client-data)
    xt/XtUnmanageChild(xm/XmFileSelectionBoxGetChild(dialog, xm/$XmDIALOG-HELP-BUTTON))
    xt/XtAddCallback(dialog, "mapCallback", notifier-map-callback, client-data);
    xm/XmAddWmProtocolCallback(xt/XtParent(dialog), "wmDeleteWindow", notifier-delete-window-callback, setter);
    xt/XtManageChild(dialog);
    //---*** CLIM does this: '(mp:process-wait "Waiting for CLIM:SELECT-FILE" #'waiter)'
    if (result == #"cancel")
      values(#f, #f)
    else
      values(result, #f)
    end
  cleanup
    when (dialog)
      x/XSync(x-display, #f);
      xt/XtUnmanageChild(dialog);
      xt/XtDestroyWidget(shell)
    end;
  end
end method do-choose-file;

define xm/xm-callback-function choose-file-button-press-callback
    (widget, client-data :: <callback-client-data>, call-data :: xm/<XmFileSelectionBoxCallbackStruct>)
  ignore(widget);
  select (call-data.xm/reason-value)
    $XmCR-OK  =>
      let filename = xm/XmStringGetLToR(call-data.xm/value-value);
      client-data.%setter(filename);
    otherwise =>
      client-data.%setter(#"cancel");
  end
end xm/xm-callback-function choose-file-button-press-callback;

define function gtk-directory-and-pattern
    (default :: false-or(<string>), default-type)
 => (directory :: false-or(<string>), pattern :: false-or(<string>))
  if (~default)
    values(#f, #f)
  else
    let type
      = select (type by instance?)
	  <string> =>
	    type;
	  <symbol> =>
	    gethash($file-type-table, type) | as-lowercase(as(<string>, type));
	  otherwise =>
	    "";
	end;
    //---*** USE PROPER LOCATORS FUNCTIONS
    let default    = as(<locator>, default);
    let directory  = DIRECTORY-NAMESTRING(DEFAULT);
    let wild-name? = (LOCATOR-NAME(DEFAULT) = "*");
    let wild-type? = (LOCATOR-TYPE(DEFAULT) = "*");
    let pattern    = when (wild-name? ~== wild-type?)
		       as(<string>, make-locator(name: LOCATOR-NAME(DEFAULT),
						 type: LOCATOR-type(DEFAULT)))
		     end;
    values(directory, pattern)
  end
end function gtk-directory-and-pattern;

//---*** This should be in Locators or File-System
define table $file-type-table :: <table>
  = { #"dylan"       => "dylan",
      #"c"           => "c",
      #"c-include"   => "h",
      #"cpp"         => "cpp",
      #"cpp-include" => "hpp",
      #"text"        => "text",
      #"project"     => "hdp",
      #"lid"         => "lid",
      #"executable"  => "exe",
      #"resource"    => "res",
      #"library"     => "lib" };
||#

;;; do-choose-file => GtkFileChooserDialog
(defmethod do-choose-file ((framem <gtk-frame-manager>)
			   (owner  <sheet>)
			   direction
			   &key
			   title
			   documentation
			   exit-boxes
			   if-exists
			   (if-does-not-exist :ask)
			   default
			   (default-type :unsupplied)
			   filters
			   default-filter
			   &allow-other-keys)
  (declare (ignore exit-boxes if-exists if-does-not-exist default default-type
		   filters default-filter documentation))
  (unless (or (eql direction :input)
	      (eql direction :output))
    (error "'direction' must be one of :INPUT or :OUTPUT (was ~a)" direction))
  ;; Do stuff with documentation, exit-boxes + default ??
  (let ((dialog-title (or title "Choose File"))
	(chooser-action (if (eql direction :input)
			    +CFFI/GTK-FILE-CHOOSER-ACTION-OPEN+
			    +CFFI/GTK-FILE-CHOOSER-ACTION-SAVE+))
	(parent (cffi/gtk_widget_get_window (mirror-widget (sheet-direct-mirror owner))))
	(dialog nil))
    (let ((stock-action (if (eql direction :input) "gtk-open" "gtk-save")))
      (setf dialog (cffi/gtk-file-chooser-dialog-new dialog-title
						     parent    ; GtkWindow*
						     chooser-action ; GtkFileChooserAction
						     ;; buttons + responses
						     "gtk-cancel"
						     +CFFI/GTK-RESPONSE-CANCEL+
						     stock-action
						     +CFFI/GTK-RESPONSE-ACCEPT+
						     nil)))
    (when (null dialog)
      (error "Failed to make Choose File chooser dialog"))
    ;; In DUIM, choosers are modal; GTK doesn't seem to have a
    ;; preference, so go with DUIM's approach.
    ;; Since the event handlers still get run even when the dialog
    ;; is running 'modally', theoretically we could hook up a bunch
    ;; of extra stuff to these. I'm not convinced it's necessary
    ;; though...
    ;; FIXME: set up filters (see GtkFileFilter). Do stuff with
    ;; if-exists, if-does-not-exist, default, default-type,
    ;; default-filter.
    (let* ((result (cffi/gtk-dialog-run dialog))
	   ;; XXX: Does this need to be freed? Do we have a memory leak here?
	   (filename (if (eql result +CFFI/GTK-RESPONSE-ACCEPT+)
			 ;; get the filename from the dialog; see
			 ;; note in GtkFileChooser documentation
			 ;; about charset issues. FIXME
			 (cffi/gtk-file-chooser-get-filename dialog)
			 ;; else
			 nil)))
      ;; FIXME: is "widget-destroy" the right thing?
      (cffi/gtk-widget-destroy dialog)
      ;; FIXME: need to read the DUIM docs for this. Does it expect multiple
      ;; return values? Or just a single value? Can this be specified? And
      ;; what to do about it?
      ;; Turn this into a CL pathname? FIXME
      ;; Is 'g_filename_to_utf8' needed?
      filename)))


#||
 
/// Choose directory

define variable $Shell-IMalloc :: false-or(<C-Interface>) = #f;

define function get-shell-IMalloc () => (IMalloc :: false-or(<C-Interface>))
  unless ($Shell-IMalloc)
    let (result, IMalloc) = SHGetMalloc();
    when (result = $NOERROR)
      $Shell-IMalloc := IMalloc
    end
  end;
  $Shell-IMalloc
end function get-shell-IMalloc;

define sealed method do-choose-directory
    (framem :: <gtk-frame-manager>, owner :: <sheet>,
     #key title :: false-or(<string>), documentation :: false-or(<string>), exit-boxes,
	  default :: false-or(<string>),
     #all-keys)
 => (locator :: false-or(<string>))
  let locator = #f;
  let shell-IMalloc = get-shell-IMalloc();
  when (shell-IMalloc)
    let handle = dialog-owner-handle(owner);
    // "C:\" and "C:\WINDOWS" are valid paths, but "C:\WINDOWS\"
    // is not.  If we pass in an invalid path, the dialog silently
    // ignores us.  For simplicity, just strip off any trailing '\\'.
    when (default)
      let _size = size(default);
      let _end  = if (element(default, _size - 1, default: #f) = '\\') _size - 1
		  else _size end;
      default := copy-sequence(default, end: _end)
    end;
    with-stack-structure (bi :: <LPBROWSEINFO>)
      with-stack-structure (buffer :: <C-string>, size: $MAX-PATH)
	title   := if (title) as(<C-string>, copy-sequence(title))
		   else $NULL-string end;
        default := if (default) as(<C-string>, default)
		   else $NULL-string end;
	bi.hwndOwner-value := handle;
	bi.pidlRoot-value  := null-pointer(<LPCITEMIDLIST>);
	bi.pszDisplayName-value := buffer;
	bi.lpszTitle-value := title;
	bi.ulFlags-value   := $BIF-RETURNONLYFSDIRS;
	bi.lpfn-value      := browse-for-folder;	// see below
	bi.lParam-value    := pointer-address(default);
	bi.iImage2-value   := 0;
	let pidlBrowse = SHBrowseForFolder(bi);
	when (SHGetPathFromIDList(pidlBrowse, buffer))
	  locator := as(<byte-string>, buffer)
	end;
	IMalloc/Free(shell-IMalloc, pidlBrowse); 
        unless (default = $NULL-string) destroy(default) end;
        unless (title   = $NULL-string) destroy(title)   end;
      end
    end
  end;
  locator
end method do-choose-directory;

// This callback allows the dialog to open with its selection set to
// the 'default:' passed in to 'do-choose-directory' 
define sealed method browse-for-folder-function
    (handle :: <HWND>,		// window handle
     message :: <message-type>,	// type of message
     lParam  :: <wparam-type>,	// additional information
     lpData  :: <lparam-type>)	// additional information
 => (result :: <lresult-type>)
  ignore(lParam);
  when (message = $BFFM-INITIALIZED)
    PostMessage(handle, $BFFM-SETSELECTION, 1, lpData)
  end;
  0
end method browse-for-folder-function;

define callback browse-for-folder :: <LPBFFCALLBACK> = browse-for-folder-function;
||#

;;; do-choose-directory => the same as do-choose-file, but we pass different
;;; flags into the GtkFileSelectionDialog.
(defmethod do-choose-directory ((framem <gtk-frame-manager>)
				(owner  <sheet>)
				&key
				title
				documentation
				exit-boxes
				default
				&allow-other-keys)
  ;; Do stuff with documentation, exit-boxes + default ??
  (declare (ignore exit-boxes default documentation))
  (let ((dialog-title (or title "Choose Directory"))
	(parent (cffi/gtk_widget_get_window (mirror-widget (sheet-direct-mirror owner))))
	(dialog nil))
    (setf dialog (cffi/gtk-file-chooser-dialog-new dialog-title
						   parent    ; GtkWindow*
						   +CFFI/GTK-FILE-CHOOSER-ACTION-SELECT-FOLDER+ ; GtkFileChooserAction
						   ;; buttons + responses
						   "gtk-cancel"
						   +CFFI/GTK-RESPONSE-CANCEL+
						   "gtk-ok"
						   +CFFI/GTK-RESPONSE-ACCEPT+
						   nil))
    (when (null dialog)
      (error "Failed to make Choose Directory chooser dialog"))
    ;; In DUIM, choosers are modal; GTK doesn't seem to have a
    ;; preference, so go with DUIM's approach.
    ;; Since the event handlers still get run even when the dialog
    ;; is running 'modally', theoretically we could hook up a bunch
    ;; of extra stuff to these. I'm not convinced it's necessary
    ;; though...
    ;; FIXME: set up filters (see GtkFileFilter). Do stuff with
    ;; if-exists, if-does-not-exist, default, default-type,
    ;; default-filter.
    (let* ((result (cffi/gtk-dialog-run dialog))
	   ;; XXX: Is this a memory leak? Does CFFI properly free the
	   ;; returned filename?
	   (filename (if (eql result +CFFI/GTK-RESPONSE-ACCEPT+)
			 ;; get the filename from the dialog; see
			 ;; note in GtkFileChooser documentation
			 ;; about charset issues. FIXME
			 (cffi/gtk-file-chooser-get-filename dialog)
			 ;; else
			 nil)))
      (cffi/gtk-widget-destroy dialog)
      ;; FIXME: need to read the DUIM docs for this. Does it expect multiple
      ;; return values? Or just a single value? Can this be specified? And
      ;; what to do about it?
;;      (when filename
;;	(cffi/g-free filename))
      ;; Turn this into a CL pathname? FIXME
      ;; Is 'g_filename_to_utf8' needed?
      ;; See comments in 'do-choose-file'
      filename)))

#||
 
/// Color chooser

define variable *custom-colors* :: <LPCOLORREF>
    = make(<LPCOLORREF>, element-count: 16);

define sealed method do-choose-color
    (framem :: <gtk-frame-manager>, owner :: <sheet>,
     #key title :: false-or(<string>), documentation :: false-or(<string>), exit-boxes,
	  default :: false-or(<color>),
     #all-keys)
 => (color :: false-or(<color>));
  let handle = dialog-owner-handle(owner);
  with-stack-structure (color :: <LPCHOOSECOLOR>)
    color.lStructSize-value  := size-of(<CHOOSECOLOR>);
    color.hwndOwner-value    := handle;
    color.hInstance-value    := application-instance-handle();
    color.rgbResult-value    := if (default) %color->native-color(default)
				else $native-black end;
    color.Flags-value        := %logior($CC-ANYCOLOR,
					if (default) $CC-RGBINIT else 0 end,
					$CC-SHOWHELP);
    color.lpCustColors-value := *custom-colors*;
    if (ChooseColor(color))
      let colorref = color.rgbResult-value;
      %native-color->color(colorref)
    else
      ensure-no-dialog-error("ChooseColor")
    end
  end
end method do-choose-color;
||#

(defmethod do-choose-color ((framem <gtk-frame-manager>)
			    (owner  <sheet>)
			    &key
			    title
			    documentation
			    exit-boxes
			    default
			    &allow-other-keys)
  ;; FIXME: deal with exit-boxes, default, documentation...
  (declare (ignore exit-boxes default documentation))
  (let ((dialog-title (or title "Choose Colour")))
    (let ((dialog (cffi/gtk-color-selection-dialog-new dialog-title)))
      (when (null dialog)
	(error "Failed to make Colour chooser dialog"))
      ;; In DUIM, choosers are modal; GTK doesn't seem to have a
      ;; preference, so go with DUIM's approach.
      ;; Since the event handlers still get run even when the dialog
      ;; is running 'modally', theoretically we could hook up a bunch
      ;; of extra stuff to these. I'm not convinced it's necessary
      ;; though...
      ;;	(duim-mem-debug "gtk-dialogs.DO-CHOOSE-COLOR color-selection dialog" dialog)
      ;; FIXME: Should probably sink the reference
      (let ((result (cffi/gtk-dialog-run dialog)))
	;;	  (gtk-debug "gtk-dialog-run for colour chooser returned ~a" (dialog-gtk-result-to-text result))
	(let ((colour (if (eql result +CFFI/GTK-RESPONSE-OK+)
			  ;; get the colour from the dialog
			  (let* ((colorsel (cffi/gtk_color_selection_dialog_get_color_selection dialog)))
			    ;; fixme: cffi already has methods to stack allocate native objects. Use them.
			    (cffi/with-stack-structure (ncol '(:struct GdkColor))
			      (cffi/gtk-color-selection-get-current-color colorsel ncol)
			      ;; FIXME: is it reasonable to get the palette
			      ;; from the port?
			      ;; Maybe we should get it from some frame or
			      ;; medium instead? If so, which? :)
			      (native-color->color (cffi:convert-from-foreign ncol '(:struct GdkColor)) (port-default-palette (port framem)))))
			  ;; else
			  nil)))
	  (cffi/gtk-widget-destroy dialog)
	  colour)))))

#||

/// Text style chooser

define sealed method do-choose-text-style
    (framem :: <gtk-frame-manager>, owner :: <sheet>,
     #key title :: false-or(<string>), documentation :: false-or(<string>), exit-boxes,
	  default :: false-or(<text-style>),
     #all-keys)
 => (text-style :: false-or(<text-style>));
  let _port = port(framem);
  let hDC :: <hDC> = _port.%memory-hDC;
  let handle = dialog-owner-handle(owner);
  with-stack-structure (logfont :: <LPLOGFONT>)
    with-stack-structure (cf :: <LPCHOOSEFONT>)
      cf.lStructSize-value := size-of(<CHOOSEFONT>);
      cf.hwndOwner-value   := handle;
      cf.hInstance-value   := application-instance-handle();
      cf.hDC-value         := hDC;
      cf.lpLogFont-value   := logfont;
      cf.Flags-value       := %logior($CF-SHOWHELP,
				      $CF-FORCEFONTEXIST,
				      $CF-SCREENFONTS,
				      $CF-EFFECTS);
      cf.lpszStyle-value   := $NULL-string;
      if (ChooseFont(cf))
	make-text-style-from-font(_port, logfont)
      else
	ensure-no-dialog-error("ChooseFont")
      end
    end
  end
end method do-choose-text-style;
*/
||#

(defmethod do-choose-text-style ((framem <gtk-frame-manager>)
				 (owner  <sheet>)
				 &key
				 title
				 documentation
				 exit-boxes
				 default
				   &allow-other-keys)
  (declare (ignore documentation exit-boxes default))
  ;; Do stuff with documentation, exit-boxes + default ??
  (let* ((dialog-title (or title "Choose Text Style"))
	 (dialog (cffi/gtk-font-selection-dialog-new dialog-title)))
    (when (null dialog)
      (error "Failed to make Text Style chooser dialog"))
    ;; In DUIM, choosers are modal; GTK doesn't seem to have a
    ;; preference, so go with DUIM's approach.
    ;; Since the event handlers still get run even when the dialog
    ;; is running 'modally', theoretically we could hook up a bunch
    ;; of extra stuff to these. I'm not convinced it's necessary
    ;; though...
    (let* ((result (cffi/gtk-dialog-run dialog))
	   (native-font-name (if (eql result +CFFI/GTK-RESPONSE-OK+)
				 ;; get the font from the dialog
				 (cffi/gtk-font-selection-dialog-get-font-name dialog)
				 ;; else
				 nil)))
      (cffi/gtk-widget-destroy dialog)
      ;; Turn the native font name into a DUIM device-font... the win32 back end
      ;; does quite a lot of work here, but I'm not sure why.
      (let* ((pango-font-description (cffi/pango-font-description-from-string native-font-name))
	     ;; FIXME: Work through all this font stuff again. Is this really doing "the right
	     ;; thing"?
	     (backend-font (make-instance '<gtk-font> :name "device" :font-description pango-font-description))
	     (device-font (make-device-font (port framem) backend-font)))
	(when native-font-name
	  (cffi/g-free native-font-name))
	device-font))))


;;; about-dialog? (GtkAboutDialog) [this is actually a kind of 'help']
;;; use GtkMessageDialog for notify-user?
