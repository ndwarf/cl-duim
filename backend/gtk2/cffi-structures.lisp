;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GTK2-DUIM -*-
(in-package #:gtk2-duim)

;;; ========================================================================
;;;
;;;    Foreign structure definitions
;;;

;;; ------ GtkStockItem

;;; struct _GtkStockItem {
;;;   gchar *stock_id;
;;;   gchar *label;
;;;   GdkModifierType modifier;
;;;   guint keyval;
;;;   gchar *translation_domain;
;;; };

(cffi:defcstruct GtkStockItem
  (stock_id           gchar*)
  (label              gchar*)
  (modifier           gint)   ;; enum GdkModifierType
  (keyval             guint)
  (translation_domain gchar*))


;;; ------ GSList - Singly linked list

;;; typedef struct {
;;;   gpointer data;
;;;   GList *next;
;;; } GSList;

(cffi:defcstruct GSList
  (data gpointer)
  (next GSList*))

(defun cffi/g_slist_alloc ()
  (cffi:foreign-funcall "g_slist_alloc"
			(:pointer (:struct GSList))))

(defun cffi/g_slist_free (glst)
  (cffi:foreign-funcall "g_slist_free"
			(:pointer (:struct GSList)) glst
			:void))

(defun cffi/g_slist_next (glst)
  (cffi:foreign-slot-value glst '(:struct GSList) 'next))

(defun cffi/gslist->data (glst)
  (if (or (null glst) (cffi:null-pointer-p glst))
      (error "CFFI/GSLIST->DATA invoked with null GLST")
      (let ((data-value (cffi:foreign-slot-value glst '(:struct GSList) 'data)))
	(pointer-or-nil data-value))))


;;; ------ GtkRequisition

;; .H CHECK A-OK

(cffi:defcstruct GtkRequisition
  (width gint) (height gint))

(defun cffi/gtkrequisition->width (requisition)
  (if (or (null requisition) (cffi:null-pointer-p requisition))
      (error "CFFI/GTKREQUISITION->WIDTH invoked with null REQUISITION")
      (cffi:foreign-slot-value requisition '(:struct GtkRequisition) 'width)))

(defun cffi/gtkrequisition->height (requisition)
  (if (or (null requisition) (cffi:null-pointer-p requisition))
      (error "CFFI/GTK-REQUISITION-HEIGHT-VALUE invoked with null REQUISITION")
      (cffi:foreign-slot-value requisition '(:struct GtkRequisition) 'height)))


;;; ------ GdkPoint

;; .H CHECK A-OK

(cffi:defcstruct GdkPoint
  (x gint) (y gint))

(defun cffi/gdkpoint->x (point)
  (if (or (null point) (cffi:null-pointer-p point))
      (error "CFFI/GDKPOINT->X invoked with null POINTER")
      (cffi:foreign-slot-value point '(:struct GdkPoint) 'x)))

(defun cffi/gdkpoint->y (point)
  (if (or (null point) (cffi:null-pointer-p point))
      (error "CFFI/GDKPOINT->Y invoked with null POINTER")
      (cffi:foreign-slot-value point '(:struct GdkPoint) 'y)))

(defun cffi/set-gdkpoint->x (point value)
  (if (or (null point) (cffi:null-pointer-p point))
      (error "CFFI/SET-GDKPOINT->X invoked with null POINTER")
      (setf (cffi:foreign-slot-value point '(:struct GdkPoint) 'x) value)))

(defun cffi/set-gdkpoint->y (point value)
  (if (or (null point) (cffi:null-pointer-p point))
      (error "CFFI/SET-GDKPOINT->Y invoked with null POINTER")
      (setf (cffi:foreign-slot-value point '(:struct GdkPoint) 'y) value)))


;;; ------ GdkRectangle

;; .H CHECK A-OK

(cffi:defcstruct GdkRectangle
  (x gint) (y gint)
  (width gint) (height gint))

(defun cffi/gdkrectangle->x (rect)
  (getf rect 'x))

(defun cffi/set-gdkrectangle->x (rect value)
  (setf (getf rect 'x) value))

(defun cffi/gdkrectangle->y (rect)
  (getf rect 'y))

(defun cffi/set-gdkrectangle->y (rect value)
  (setf (getf rect 'y) value))

(defun cffi/gdkrectangle->width (rect)
  (getf rect 'width))

(defun cffi/set-gdkrectangle->width (rect value)
  (setf (getf rect 'width) value))

(defun cffi/gdkrectangle->height (rect)
  (getf rect 'height))

(defun cffi/set-gdkrectangle->height (rect value)
  (setf (getf rect 'height) value))


;;; ------ GtkAllocation

;; .H CHECK A-OK [GtkAllocation=GtkRectangle]

(cffi:defcstruct GtkAllocation
  (x gint) (y gint)
  (width gint) (height gint))

(defun cffi/gtkallocation->x (alloc)
  (if (or (null alloc) (cffi:null-pointer-p alloc))
      (error "CFFI/GTKALLOCATION->X invoked with null ALLOC")
      (cffi:foreign-slot-value alloc '(:struct GtkAllocation) 'x)))

(defun cffi/set-gtkallocation->x (alloc value)
  (if (or (null alloc) (cffi:null-pointer-p alloc))
      (error "CFFI/SET-GTKALLOCATION->X invoked with null ALLOC")
      (setf (cffi:foreign-slot-value alloc '(:struct GtkAllocation) 'x) value)))

(defun cffi/gtkallocation->y (alloc)
  (if (or (null alloc) (cffi:null-pointer-p alloc))
      (error "CFFI/GTKALLOCATION->Y invoked with null ALLOC")
      (cffi:foreign-slot-value alloc '(:struct GtkAllocation) 'y)))

(defun cffi/set-gtkallocation->y (alloc value)
  (if (or (null alloc) (cffi:null-pointer-p alloc))
      (error "CFFI/SET-GTKALLOCATION->Y invoked with null ALLOC")
      (setf (cffi:foreign-slot-value alloc '(:struct GtkAllocation) 'y) value)))

(defun cffi/gtkallocation->width (alloc)
  (if (or (null alloc) (cffi:null-pointer-p alloc))
      (error "CFFI/GTKALLOCATION->WIDTH invoked with null ALLOC")
      (cffi:foreign-slot-value alloc '(:struct GtkAllocation) 'width)))

(defun cffi/set-gtkallocation->width (alloc value)
  (if (or (null alloc) (cffi:null-pointer-p alloc))
      (error "CFFI/SET-GTKALLOCATION->WIDTH invoked with null ALLOC")
      (setf (cffi:foreign-slot-value alloc '(:struct GtkAllocation) 'width) value)))

(defun cffi/gtkallocation->height (alloc)
  (if (or (null alloc) (cffi:null-pointer-p alloc))
      (error "CFFI/GTKALLOCATION->HEIGHT invoked with null ALLOC")
      (cffi:foreign-slot-value alloc '(:struct GtkAllocation) 'height)))

(defun cffi/set-gtkallocation->height (alloc value)
  (if (or (null alloc) (cffi:null-pointer-p alloc))
      (error "CFFI/SET-GTKALLOCATION->HEIGHT invoked with null ALLOC")
      (setf (cffi:foreign-slot-value alloc '(:struct GtkAllocation) 'height) value)))


;;; ------ GdkColor

;;; typedef struct {
;;;   guint32 pixel;
;;;   guint16 red;
;;;   guint16 green;
;;;   guint16 blue;
;;; } GdkColor;

;; .H CHECK A-OK

(cffi:defcstruct GdkColor
  (pixel guint32) (red guint16)
  (green guint16) (blue guint16))

(defun cffi/gdkcolor->pixel (colour)
  (if (or (null colour) (cffi:null-pointer-p colour))
      (error "CFFI/GDKCOLOR->PIXEL invoked with null COLOUR")
      (cffi:foreign-slot-value colour '(:struct GdkColor) 'pixel)))

(defun cffi/set-gdkcolor->pixel (colour value)
  (if (or (null colour) (cffi:null-pointer-p colour))
      (error "CFFI/SET-GDKCOLOR->PIXEL invoked with null COLOUR")
      (setf (cffi:foreign-slot-value colour '(:struct GdkColor) 'pixel) value)))

(defun cffi/gdkcolor->red (colour)
  (if (or (null colour) (cffi:null-pointer-p colour))
      (error "CFFI/GDKCOLOR->RED invoked with null COLOUR")
      (cffi:foreign-slot-value colour '(:struct GdkColor) 'red)))

(defun cffi/set-gdkcolor->red (colour value)
  (if (or (null colour) (cffi:null-pointer-p colour))
      (error "CFFI/SET-GDKCOLOR->RED invoked with null COLOUR")
      (setf (cffi:foreign-slot-value colour '(:struct GdkColor) 'red) value)))

(defun cffi/gdkcolor->green (colour)
  (if (or (null colour) (cffi:null-pointer-p colour))
      (error "CFFI/GDKCOLOR->GREEN invoked with null COLOUR") 
      (cffi:foreign-slot-value colour '(:struct GdkColor) 'green)))

(defun cffi/set-gdkcolor->green (colour value)
  (if (or (null colour) (cffi:null-pointer-p colour))
      (error "CFFI/SET-GDKCOLOR->GREEN invoked with null COLOUR")
      (setf (cffi:foreign-slot-value colour '(:struct GdkColor) 'green) value)))

(defun cffi/gdkcolor->blue (colour)
  (if (or (null colour) (cffi:null-pointer-p colour))
      (error "CFFI/GDKCOLOR->BLUE invoked with null COLOUR")
      (cffi:foreign-slot-value colour '(:struct GdkColor) 'blue)))

(defun cffi/set-gdkcolor->blue (colour value)
  (if (or (null colour) (cffi:null-pointer-p colour))
      (error "CFFI/SET-GDKCOLOR->BLUE invoked with null COLOUR")
      (setf (cffi:foreign-slot-value colour '(:struct GdkColor) 'blue) value)))


;;; ------ GdkGCValues

;; .H CHECK A-OK

;;; typedef struct {
;;;   GdkColor	        foreground;
;;;   GdkColor	        background;
;;;   GdkFont	        *font;
;;;   GdkFunction	function;
;;;   GdkFill	        fill;
;;;   GdkPixmap	        *tile;
;;;   GdkPixmap	        *stipple;
;;;   GdkPixmap	        *clip_mask;
;;;   GdkSubwindowMode  subwindow_mode;
;;;   gint		ts_x_origin;
;;;   gint		ts_y_origin;
;;;   gint		clip_x_origin;
;;;   gint		clip_y_origin;
;;;   gint		graphics_exposures;
;;;   gint		line_width;
;;;   GdkLineStyle	line_style;
;;;   GdkCapStyle	cap_style;
;;;   GdkJoinStyle	join_style;
;;; } GdkGCValues;

(cffi:defcstruct GdkGCValues
  (foreground         (:struct GdkColor))
  (background         (:struct GdkColor))
  (font               GdkFont*)
  (function           GdkFunction)
  (fill               GdkFill)
  (tile               GdkPixmap*)
  (stipple            GdkPixmap*)
  (clip-mask          GdkPixmap*)
  (subwindow-mode     GdkSubwindowMode)
  (ts-x-origin        gint)
  (ts-y-origin        gint)
  (clip-x-origin      gint)
  (clip-y-origin      gint)
  (graphics-exposures gint)
  (line-width         gint)
  (line-style         GdkLineStyle)
  (cap-style          GdkCapStyle)
  (join-style         GdkJoinStyle))

(defun cffi/gdkgcvalues->cap-style (gc-values)
  (if (or (null gc-values) (cffi:null-pointer-p gc-values))
      (error "CFFI/GDKGCVALUES->CAP-STYLE invoked with null GC-VALUES")
      (cffi:foreign-slot-value gc-values '(:struct GdkGCValues) 'cap-style)))

(defun cffi/gdkgcvalues->line-width (gc-values)
  (if (or (null gc-values) (cffi:null-pointer-p gc-values))
      (error "CFFI/GDKGCVALUES->LINE-WIDTH invoked with null GC-VALUES")
      (cffi:foreign-slot-value gc-values '(:struct GdkGCValues) 'line-width)))

(defun cffi/gdkgcvalues->line-style (gc-values)
  (if (or (null gc-values) (cffi:null-pointer-p gc-values))
      (error "CFFI/GDKGCVALUES->LINE-STYLE invoked with null GC-VALUES")
      (cffi:foreign-slot-value gc-values '(:struct GdkGCValues) 'line-style)))

(defun cffi/gdkgcvalues->join-style (gc-values)
  (if (or (null gc-values) (cffi:null-pointer-p gc-values))
      (error "CFFI/GDKGCVALUES->JOIN-STYLE invoked with null GC-VALUES")
      (cffi:foreign-slot-value gc-values '(:struct GdkGCValues) 'join-style)))


;;; ------ GObject and dependencies

;; .H CHECK A-OK

(cffi:defcstruct GTypeInstance
  (g_class GTypeClass*))

;; .H CHECK A-OK

;;; This one is actually a _GObject
(cffi:defcstruct GInitiallyUnowned
  (type_inst (:struct GTypeInstance))
  (ref_count guint)
  (qdata     GData*))

(cffi:defctype GObject (:struct GInitiallyUnowned))

(defun cffi/gobject->ref-count (gobject)
 (if (or (null gobject) (cffi:null-pointer-p gobject))
     (error "CFFI/GOBJECT->REF-COUNT invoked with null GOBJECT")
     (cffi:foreign-slot-value gobject '(:struct GInitiallyUnowned) 'ref_count)))


;; .H CHECK A-OK

(cffi:defcstruct GtkObject
  (parent (:struct GInitiallyUnowned))
  (flags  guint32))


(defun cffi/gtkobject->flags (gtk-object)
  (if (or (null gtk-object) (cffi:null-pointer-p gtk-object))
      (error "CFFI/GTKOBJECT->FLAGS invoked with null GTK-OBJECT")
      (cffi:foreign-slot-value gtk-object '(:struct GtkObject) 'flags)))


(defun cffi/set-gtkobject->flags (gtk-object value)
  (if (or (null gtk-object) (cffi:null-pointer-p gtk-object))
      (error "CFFI/SET-GTKOBJECT->FLAGS invoked with null GTK-OBJECT")
      (setf (cffi:foreign-slot-value gtk-object '(:struct GtkObject) 'flags) value)))


;;; ------ GValue

#||
struct _GValue
{
  /*< private >*/
  GType		g_type;

  /* public for GTypeValueTable methods */
  union {
    gint	v_int;
    guint	v_uint;
    glong	v_long;
    gulong	v_ulong;
    gint64      v_int64;
    guint64     v_uint64;
    gfloat	v_float;
    gdouble	v_double;
    gpointer	v_pointer;
  } data[2];
};
||#

(cffi:defcunion GValueDataUnion
  (v_int     gint)
;;  (v_uint    guint)    -- I'm going to guess the 64bits for the pointer will be enough... fingers crossed
;;  (v_long    glong)
;;  (v_ulong   gulong)
;;  (v_int64   gint64)
;;  (v_uint64  guint64)
;;  (v_float   gfloat)
;;  (v_double  v_double)
  (v_pointer gpointer))

(cffi:defcstruct GValue
  (g_type GType)
  (data   (:union GValueDataUnion) :count 2))


;;; ------ GtkStyle

;; .H CHECK A-OK

;;; typedef struct {
;;;   GdkColor fg[5];
;;;   GdkColor bg[5];
;;;   GdkColor light[5];
;;;   GdkColor dark[5];
;;;   GdkColor mid[5];
;;;   GdkColor text[5];
;;;   GdkColor base[5];
;;;   GdkColor text_aa[5];		/* Halfway between text/base */
;;;  
;;;   GdkColor black;
;;;   GdkColor white;
;;;   PangoFontDescription *font_desc;
;;;  
;;;   gint xthickness;
;;;   gint ythickness;
;;;  
;;;   GdkGC *fg_gc[5];
;;;   GdkGC *bg_gc[5];
;;;   GdkGC *light_gc[5];
;;;   GdkGC *dark_gc[5];
;;;   GdkGC *mid_gc[5];
;;;   GdkGC *text_gc[5];
;;;   GdkGC *base_gc[5];
;;;   GdkGC *text_aa_gc[5];
;;;   GdkGC *black_gc;
;;;   GdkGC *white_gc;
;;;  
;;;   GdkPixmap *bg_pixmap[5];
;;; } GtkStyle;

(cffi:defcstruct GtkStyle

  ;; 'private'

  (parent_instance GObject)

  ;; 'public'

  (fg       (:struct GdkColor) :count 5) ;  GdkColor fg[5];
  (bg       (:struct GdkColor) :count 5) ;  GdkColor bg[5];
  (light    (:struct GdkColor) :count 5) ;  GdkColor light[5];
  (darg     (:struct GdkColor) :count 5) ;  GdkColor dark[5];
  (mid      (:struct GdkColor) :count 5) ;  GdkColor mid[5];
  (text     (:struct GdkColor) :count 5) ;  GdkColor text[5];
  (base     (:struct GdkColor) :count 5) ;  GdkColor base[5];
  (text_aa  (:struct GdkColor) :count 5) ;  GdkColor text_aa[5]; // Halfway between text/base

  (black     (:struct GdkColor))   ;  GdkColor black;
  (white     (:struct GdkColor))   ;  GdkColor white;
  (font_desc PangoFontDescription*)  ;  PangoFontDescription *font_desc;

  (xthickness gint)     ;  gint xthickness;
  (ythickness gint)     ;  gint ythickness;

  (fg_gc      GdkGC* :count 5) ;  GdkGC *fg_gc[5];
  (bg_gc      GdkGC* :count 5) ;  GdkGC *bg_gc[5];
  (light_gc   GdkGC* :count 5) ;  GdkGC *light_gc[5];
  (dark_gc    GdkGC* :count 5) ;  GdkGC *dark_gc[5];
  (mid_gc     GdkGC* :count 5) ;  GdkGC *mid_gc[5];
  (text_gc    GdkGC* :count 5) ;  GdkGC *text_gc[5];
  (base_gc    GdkGC* :count 5) ;  GdkGC *base_gc[5];
  (text_aa_gc GdkGC* :count 5) ;  GdkGC *text_aa_gc[5];
  (black_gc   GdkGC*)    ;  GdkGC *black_gc;
  (white_gc   GdkGC*)    ;  GdkGC *white_gc;

  (bg_pixmap GdkPixmap* :count 5))         ;  GdkPixmap *bg_pixmap[5];

;;; Also ('private'), just in case we need them:-
;;;
;;; gint attach_count;
;;;
;;; gint depth;
;;; GdkColormap* colormap;
;;; GdkFont* private_font;
;;; PangoFontDescription* private_font_desc;
;;; GtkRcStyle* rc_style;
;;; GSList* styles;
;;; GArray* property_cache;
;;; GSList* icon_factories;

(defun cffi/gtkstyle->fg (gtk-style)
  (if (or (null gtk-style) (cffi:null-pointer-p gtk-style))
      (error "CFFI/GTKSTYLE->FG invoked with null GTK-STYLE")
      (cffi:foreign-slot-value gtk-style '(:struct GtkStyle) 'fg)))

(defun cffi/gtkstyle->bg (gtk-style)
  (if (or (null gtk-style) (cffi:null-pointer-p gtk-style))
      (error "CFFI/GTKSTYLE->BG invoked with null GTK-STYLE")
      (cffi:foreign-slot-value gtk-style '(:struct GtkStyle) 'bg)))

(defun cffi/gtkstyle->fg-gc (gtk-style)
  (if (or (null gtk-style) (cffi:null-pointer-p gtk-style))
      (error "CFFI/GTKSTYLE->FG-GC invoked with null GTK-STYLE")
      (cffi:foreign-slot-value gtk-style '(:struct GtkStyle) 'fg_gc)))

(defun cffi/gtkstyle->bg-gc (gtk-style)
  (if (or (null gtk-style) (cffi:null-pointer-p gtk-style))
      (error "CFFI/GTKSTYLE->BG-GC invoked with null GTK-STYLE")
      (cffi:foreign-slot-value gtk-style '(:struct GtkStyle) 'bg_gc)))


;;; ------ GtkAdjustment

;;; struct _GtkAdjustment {
;;;   GtkObject parent_instance;
;;;  
;;;   gdouble lower;
;;;   gdouble upper;
;;;   gdouble value;
;;;   gdouble step_increment;
;;;   gdouble page_increment;
;;;   gdouble page_size;
;;; };

(defun cffi/gtk_adjustment_get_lower (adjustment)
  (cffi:foreign-funcall "gtk_adjustment_get_lower"
			:pointer adjustment
			gdouble))

(defun cffi/gtk_adjustment_set_lower (adjustment lower)
  (cffi:foreign-funcall "gtk_adjustment_set_lower"
			:pointer adjustment
			gdouble  lower
			:void))

(defun cffi/gtk_adjustment_get_upper (adjustment)
  (cffi:foreign-funcall "gtk_adjustment_get_upper"
			:pointer adjustment
			gdouble))

(defun cffi/gtk_adjustment_set_upper (adjustment upper)
  (cffi:foreign-funcall "gtk_adjustment_set_upper"
			:pointer adjustment
			gdouble upper
			:void))

(defun cffi/gtk_adjustment_get_value (adjustment)
  (cffi:foreign-funcall "gtk_adjustment_get_value"
			:pointer adjustment
			gdouble))

(defun cffi/gtk_adjustment_set_value (adjustment value)
  (cffi:foreign-funcall "gtk_adjustment_set_value"
			:pointer adjustment
			gdouble value
			:void))

(defun cffi/gtk_adjustment_get_step_increment (adjustment)
  (cffi:foreign-funcall "gtk_adjustment_get_step_increment"
			:pointer adjustment
			gdouble))

(defun cffi/gtk_adjustment_set_step_increment (adjustment increment)
  (cffi:foreign-funcall "gtk_adjustment_set_step_increment"
			:pointer adjustment
			gdouble increment
			:void))

(defun cffi/gtk_adjustment_get_page_increment (adjustment)
  (cffi:foreign-funcall "gtk_adjustment_get_page_increment"
			:pointer adjustment
			gdouble))

(defun cffi/gtk_adjustment_set_page_increment (adjustment increment)
  (cffi:foreign-funcall "gtk_adjustment_set_page_increment"
			:pointer adjustment
			gdouble increment
			:void))

(defun cffi/gtk_adjustment_get_page_size (adjustment)
  (cffi:foreign-funcall "gtk_adjustment_get_page_size"
			:pointer adjustment
			gdouble))

(defun cffi/gtk_adjustment_set_page_size (adjustment size)
  (cffi:foreign-funcall "gtk_adjustment_set_page_size"
			:pointer adjustment
			gdouble size
			:void))

;; Use this one to change multiple adjustment values at once and only
;; receive the one event. Otherwise see the docs for gtk_adjustment_set_lower.
(defun cffi/gtk_adjustment_configure (adjustment value lower upper step_increment page_increment page_size)
  (cffi:foreign-funcall "gtk_adjustment_configure"
			:pointer adjustment
			gdouble value
			gdouble lower
			gdouble upper
			gdouble step_increment
			gdouble page_increment
			gdouble page_size
			:void))

;;; ------ GdkWindow

;;; Opaque struct, we shouldn't need anything out of here.

;;; ------ GtkWidget

;; .H CHECK A-OK

;;; typedef struct {
;;;   /* The style for the widget. The style contains the
;;;    *  colors the widget should be drawn in for each state
;;;    *  along with graphics contexts used to draw with and
;;;    *  the font to use for text.
;;;    */
;;;   GtkStyle *style;
;;;  
;;;   /* The widget's desired size.
;;;    */
;;;   GtkRequisition requisition;
;;;  
;;;   /* The widget's allocated size.
;;;    */
;;;   GtkAllocation allocation;
;;;  
;;;   /* The widget's window or its parent window if it does
;;;    *  not have a window. (Which will be indicated by the
;;;    *  GTK_NO_WINDOW flag being set).
;;;    */
;;;   GdkWindow *window;
;;;  
;;;   /* The widget's parent.
;;;    */
;;;   GtkWidget *parent;
;;; } GtkWidget;


(cffi:defcstruct GtkWidget

  ;; 'private'

  (object      (:struct GtkObject))
  (priv_flags  guint16)
  (state       guint8)
  (saved_state guint8)
  (name        gchar*)

  ;; 'public'

  (style       GtkStyle*)
  (requisition (:struct GtkRequisition))
  (allocation  (:struct GtkAllocation))
  (window      GdkWindow*)        ; Opaque pointer...
  (parent      GtkWidget*))

;; note: this allocation is reduced by any border etc. requirements,
;; so it might not be what DUIM needs. See docs.
;; use 'with-foreign-object' to allocate allocation on the stack
;; prior to calling...
(defun cffi/gtk_widget_get_allocation (widget allocation)
  (cffi:foreign-funcall "gtk_widget_get_allocation"
			:pointer widget
			:pointer allocation
			:void)
  allocation)


(defun cffi/gtk_widget_get_style (widget)
  (cffi:foreign-funcall "gtk_widget_get_style"
			:pointer widget
			:pointer))  ;; GtkStyle

(defun cffi/gtk_widget_get_window (widget)
  (cffi:foreign-funcall "gtk_widget_get_window"
			:pointer widget
			:pointer))  ;; GtkWindow

(defun cffi/gtk_widget_get_state (widget)
  (cffi:foreign-funcall "gtk_widget_get_state"
			:pointer widget
			GtkStateType))



;;; ------ GtkList/TreeStore + support

;; struct _GtkTreeIter
;; {
;;   gint stamp;
;;   gpointer user_data;
;;   gpointer user_data2;
;;   gpointer user_data3;
;; };

(cffi:defcstruct GtkTreeIter
  (stamp      gint)
  (user_data  gpointer)
  (user_data2 gpointer)
  (user_data3 gpointer))


;;; ------ GtkCList

;;; struct _GtkCList
;;; {
;;;   GtkContainer container;
;;;  
;;;   guint16 flags;
;;;  
;;;   gpointer reserved1;
;;;   gpointer reserved2;
;;;
;;;   guint freeze_count;
;;;  
;;;   /* allocation rectangle after the conatiner_border_width
;;;    * and the width of the shadow border */
;;;   GdkRectangle internal_allocation;
;;;  
;;;   /* rows */
;;;   gint rows;
;;;   gint row_height;
;;;   GList *row_list;
;;;   GList *row_list_end;
;;;  
;;;   /* columns */
;;;   gint columns;
;;;   GdkRectangle column_title_area;
;;;   GdkWindow *title_window;
;;;  
;;;   /* dynamicly allocated array of column structures */
;;;   GtkCListColumn *column;
;;;  
;;;   /* the scrolling window and its height and width to
;;;    * make things a little speedier */
;;;   GdkWindow *clist_window;
;;;   gint clist_window_width;
;;;   gint clist_window_height;
;;;  
;;;   /* offsets for scrolling */
;;;   gint hoffset;
;;;   gint voffset;
;;;  
;;;   /* border shadow style */
;;;   GtkShadowType shadow_type;
;;;  
;;;   /* the list's selection mode (gtkenums.h) */
;;;   GtkSelectionMode selection_mode;
;;;  
;;;   /* list of selected rows */
;;;   GList *selection;
;;;   GList *selection_end;
;;;  
;;;   GList *undo_selection;
;;;   GList *undo_unselection;
;;;   gint undo_anchor;
;;;  
;;;   /* mouse buttons */
;;;   guint8 button_actions[5];
;;;
;;;   guint8 drag_button;
;;;
;;;   /* dnd */
;;;   GtkCListCellInfo click_cell;
;;;
;;;   /* scroll adjustments */
;;;   GtkAdjustment *hadjustment;
;;;   GtkAdjustment *vadjustment;
;;;  
;;;   /* xor GC for the vertical drag line */
;;;   GdkGC *xor_gc;
;;;  
;;;   /* gc for drawing unselected cells */
;;;   GdkGC *fg_gc;
;;;   GdkGC *bg_gc;
;;;  
;;;   /* cursor used to indicate dragging */
;;;   GdkCursor *cursor_drag;
;;;  
;;;   /* the current x-pixel location of the xor-drag line */
;;;   gint x_drag;
;;;  
;;;   /* focus handling */
;;;   gint focus_row;
;;;
;;;   gint focus_header_column;
;;;  
;;;   /* dragging the selection */
;;;   gint anchor;
;;;   GtkStateType anchor_state;
;;;   gint drag_pos;
;;;   gint htimer;
;;;   gint vtimer;
;;;  
;;;   GtkSortType sort_type;
;;;   GtkCListCompareFunc compare;
;;;   gint sort_column;
;;;
;;;   gint drag_highlight_row;
;;;   GtkCListDragPos drag_highlight_pos;
;;; };

;; .H CHECK A-OK

(cffi:defcstruct GtkContainer
  (widget             (:struct GtkWidget))
  (focus_child        GtkWidget*)

  (bitfield           guint))
;;;  =>
;;;  (border_width       :guint : 16)   ;; how to deal with bit-fields?
;;;  
;;;  ;; private
;;;
;;;  (need_resize        :guint : 1)
;;;  (resize_mode        :guint : 2)
;;;  (reallocate_redraws :guint : 1)
;;;  (has_focus_chain    :guint : 1))


;; FIXME 2020-04-11 Replace this with GtkTreeView

(cffi:defcstruct GtkCListCellInfo
  (row gint) (column gint))

(cffi:defcstruct GtkCList
  (container           (:struct GtkContainer))
  (flags               guint16)
  (reserved1           gpointer)
  (reserved2           gpointer)
  (freeze_count        guint)
  (internal_allocation (:struct GdkRectangle))
  (rows                gint)
  (row_height          gint)
  (row_list            GList*)
  (row_list_end        GList*)
  (columns             gint)
  (column_title_area   (:struct GdkRectangle))
  (title_window        GdkWindow*)
  (column              GtkCListColumn*)
  (clist_window        GdkWindow*)
  (clist_window_width  gint)
  (clist_window_height gint)
  (hoffset             gint)
  (voffset             gint)
  (shadow_type         GtkShadowType)
  (selection_mode      GtkSelectionMode)
  (selection           GList*)
  (selection_end       GList*)
  (undo_selection      GList*)
  (undo_unselection    GList*)
  (undo_anchor         gint)
  (button_actions      guint8 :count 5)
  (drag_button         guint8)
  (click_cell          (:struct GtkCListCellInfo))
  (hadjustment         GtkAdjustment*)
  (vadjustment         GtkAdjustment*)
  (xor_gc              GdkGC*)
  (fg_gc               GdkGC*)
  (bg_gc               GdkGC*)
  (cursor_drag         GdkCursor*)
  (x_drag              gint)
  (focus_row           gint)
  (focus_header_column gint)
  (anchor              gint)
  (anchor_state        GtkStateType)
  (drag_pos            gint)
  (htimer              gint)
  (vtimer              gint)
  (sort_type           GtkSortType)
  (compare             GtkCListCompareFunc)
  (sort_column         gint)
  (drag_highlight_row  gint)
  (drag_highlight_pos  GtkCListDragPos))

(defun cffi/gtkclist->selection (clist)
  (warn "CFFI/GTKCLIST->SELECTION deprecated; use new tree stuff")
  (if (or (null clist) (cffi:null-pointer-p clist))
      (error "CFFI/GTKCLIST->SELECTION invoked with null CLIST")
      (let ((selection (cffi:foreign-slot-value clist '(:struct GtkCList) 'selection)))
	(pointer-or-nil selection))))


;;; ------ GtkLayout

;; struct _GtkLayout
;; {
;;   GtkContainer container;
;;
;;   GList *children;
;;
;;   guint width;
;;   guint height;
;;
;;   GtkAdjustment *hadjustment;
;;   GtkAdjustment *vadjustment;
;;
;;   /*< public >*/
;;   GdkWindow *bin_window;
;;
;;   /*< private >*/
;;   GdkVisibilityState visibility;
;;   gint scroll_x;
;;   gint scroll_y;

;;   guint freeze_count;
;; };

(defun cffi/gtk_layout_get_bin_window (layout)
  (cffi:foreign-funcall "gtk_layout_get_bin_window"
			:pointer layout
			:pointer))  ;; GtkWindow


;;; ------ Event structures

;;; typedef struct {
;;;   GdkEventType type;
;;;   GdkWindow *window;
;;;   gint8 send_event;
;;;   gint x, y;
;;;   gint width;
;;;   gint height;
;;; } GdkEventConfigure;

;; .H CHECK A-OK

(cffi:defcstruct GdkEventConfigure
  (type       GdkEventType)
  (window     GdkWindow*)
  (send_event gint8)
  (x          gint)
  (y          gint)
  (width      gint)
  (height     gint))

(defun cffi/gdkeventconfigure->x (event)
  (if (or (null event) (cffi:null-pointer-p event))
      (error "CFFI/GDKEVENTCONFIGURE->X invoked with null EVENT")
      (cffi:foreign-slot-value event '(:struct GdkEventConfigure) 'x)))

(defun cffi/gdkeventconfigure->y (event)
  (if (or (null event) (cffi:null-pointer-p event))
      (error "CFFI/GDKEVENTCONFIGURE->Y invoked with null EVENT")
      (cffi:foreign-slot-value event '(:struct GdkEventConfigure) 'y)))

(defun cffi/gdkeventconfigure->width (event)
  (if (or (null event) (cffi:null-pointer-p event))
      (error "CFFI/GDKEVENTCONFIGURE->WIDTH invoked with null EVENT")
      (cffi:foreign-slot-value event '(:struct GdkEventConfigure) 'width)))

(defun cffi/gdkeventconfigure->height (event)
  (if (or (null event) (cffi:null-pointer-p event))
      (error "CFFI/GDKEVENTCONFIGURE->HEIGHT invoked with null EVENT")
      (cffi:foreign-slot-value event '(:struct GdkEventConfigure) 'height)))


;;; typedef struct {
;;;   GdkEventType type;
;;;   GdkWindow *window;
;;;   gint8 send_event;
;;;   GdkRectangle area;
;;;   GdkRegion *region;
;;;   gint count; /* If non-zero, how many more events follow. */
;;; } GdkEventExpose;

;; .H CHECK A-OK

(cffi:defcstruct GdkEventExpose
  (type       GdkEventType)
  (window     GdkWindow*)
  (send_event gint8)
  (area       (:struct GdkRectangle))
  (region     GdkRegion*)
  (count      gint))

(defun cffi/gdkeventexpose->area (event)
  (if (or (null event) (cffi:null-pointer-p event))
      (error "CFFI/GDKEVENTEXPOSE->AREA invoked with null EVENT")
      (cffi:foreign-slot-value event '(:struct GdkEventExpose) 'area)))


;; .H CHECK A-OK

;;; typedef struct {
;;;   GdkEventType type;
;;;   GdkWindow *window;
;;;   gint8 send_event;
;;;   guint32 time;
;;;   gdouble x;
;;;   gdouble y;
;;;   gdouble *axes;
;;;   guint state;
;;;   gint16 is_hint;
;;;   GdkDevice *device;
;;;   gdouble x_root, y_root;
;;; } GdkEventMotion;

(cffi:defcstruct GdkEventMotion
  (type       GdkEventType)
  (window     GdkWindow*)
  (send_event gint8)
  (time       guint32)
  (x          gdouble)
  (y          gdouble)
  (axes       gdouble*)
  (state      guint)
  (is_hint    gint16)
  (device     GdkDevice*)
  (x_root     gdouble)
  (y_root     gdouble))

(defun cffi/gdkeventmotion->x (event)
  (if (or (null event) (cffi:null-pointer-p event))
      (error "CFFI/GDKEVENTMOTION->X invoked with null EVENT")
      (cffi:foreign-slot-value event '(:struct GdkEventMotion) 'x)))

(defun cffi/gdkeventmotion->y (event)
  (if (or (null event) (cffi:null-pointer-p event))
      (error "CFFI/GDKEVENTMOTION->Y invoked with null EVENT")
      (cffi:foreign-slot-value event '(:struct GdkEventMotion) 'y)))

(defun cffi/gdkeventmotion->state (event)
  (if (or (null event) (cffi:null-pointer-p event))
      (error "CFFI/GDKEVENTMOTION->STATE invoked with null EVENT")
      (cffi:foreign-slot-value event '(:struct GdkEventMotion) 'state)))


;; .H CHECK A-OK

;;; typedef struct {
;;;   GdkEventType type;
;;;   GdkWindow *window;
;;;   gint8 send_event;
;;;   GdkWindow *subwindow;
;;;   guint32 time;
;;;   gdouble x;
;;;   gdouble y;
;;;   gdouble x_root;
;;;   gdouble y_root;
;;;   GdkCrossingMode mode;
;;;   GdkNotifyType detail;

;;;   gboolean focus;
;;;   guint state;
;;; } GdkEventCrossing;


(cffi:defcstruct GdkEventCrossing
  (type       GdkEventType)
  (window     GdkWindow*)
  (send_event gint8)
  (subwindow  GdkWindow*)
  (time       guint32)
  (x          gdouble)
  (y          gdouble)
  (x_root     gdouble)
  (y_root     gdouble)
  (mode       :int)     ;; GdkCrossingMode)
  (detail     :int)     ;; GdkNotifyType)
  (focus      gboolean)
  (state      guint))

(defun cffi/gdkeventcrossing->x (event)
  (if (or (null event) (cffi:null-pointer-p event))
      (error "CFFI/GDKEVENTCROSSING->X invoked with null EVENT")
      (cffi:foreign-slot-value event '(:struct GdkEventCrossing) 'x)))

(defun cffi/gdkeventcrossing->y (event)
  (if (or (null event) (cffi:null-pointer-p event))
      (error "CFFI/GDKEVENTCROSSING->Y invoked with null EVENT")
      (cffi:foreign-slot-value event '(:struct GdkEventCrossing) 'y)))

(defun cffi/gdkeventcrossing->state (event)
  (if (or (null event) (cffi:null-pointer-p event))
      (error "CFFI/GDKEVENTCROSSING->STATE invoked with null EVENT")
      (cffi:foreign-slot-value event '(:struct GdkEventCrossing) 'state)))

(defun cffi/gdkeventcrossing->detail (event)
  (if (or (null event) (cffi:null-pointer-p event))
      (error "CFFI/GDKEVENTCROSSING->DETAIL invoked with null EVENT")
      (cffi:foreign-slot-value event '(:struct GdkEventCrossing) 'detail)))


;;; typedef struct {
;;;   GdkEventType type;
;;;   GdkWindow *window;
;;;   gint8 send_event;
;;;   guint32 time;
;;;   gdouble x;
;;;   gdouble y;
;;;   gdouble *axes;
;;;   guint state;
;;;   guint button;
;;;   GdkDevice *device;
;;;   gdouble x_root, y_root;
;;; } GdkEventButton;

;; .H CHECK A-OK

(cffi:defcstruct GdkEventButton
  (type       GdkEventType)
  (window     GdkWindow*)
  (send_event gint8)
  (time       guint32)
  (x          gdouble)
  (y          gdouble)
  (axes       gdouble*)
  (state      guint)
  (button     guint)
  (device     GdkDevice*)
  (x_root     gdouble)
  (y_root     gdouble))  ;; Wouldn't these last couple be useful?

(defun cffi/gdkeventbutton->x (event)
  (if (or (null event) (cffi:null-pointer-p event))
      (error "CFFI/GDKEVENTBUTTON->X invoked with null EVENT")
      (cffi:foreign-slot-value event '(:struct GdkEventButton) 'x)))

(defun cffi/gdkeventbutton->y (event)
  (if (or (null event) (cffi:null-pointer-p event))
      (error "CFFI/GDKEVENTBUTTON->Y invoked with null EVENT")
      (cffi:foreign-slot-value event '(:struct GdkEventButton) 'y)))

(defun cffi/gdkeventbutton->time (event)
  (if (or (null event) (cffi:null-pointer-p event))
      (error "CFFI/GDKEVENTBUTTON->TIME invoked with null EVENT")
      (cffi:foreign-slot-value event '(:struct GdkEventButton) 'time)))

(defun cffi/gdkeventbutton->state (event)
  (if (or (null event) (cffi:null-pointer-p event))
      (error "CFFI/GDKEVENTBUTTON->STATE invoked with null EVENT")
      (cffi:foreign-slot-value event '(:struct GdkEventButton) 'state)))

(defun cffi/gdkeventbutton->button (event)
  (if (or (null event) (cffi:null-pointer-p event))
      (error "CFFI/GDKEVENTBUTTON->BUTTON invoked with null EVENT")
      (cffi:foreign-slot-value event '(:struct GdkEventButton) 'button)))

(defun cffi/gdkeventbutton->type (event)
  (if (or (null event) (cffi:null-pointer-p event))
      (error "CFFI/GDKEVENTBUTTON->TYPE invoked with null EVENT")
      (cffi:foreign-slot-value event '(:struct GdkEventButton) 'type)))


;; typedef struct {
;;   GdkEventType type;
;;   GdkWindow *window;
;;   gint8 send_event;
;;   guint32 time;
;;   guint state;
;;   guint keyval;
;;   gint length;
;;   gchar *string;
;;   guint16 hardware_keycode;
;;   guint8 group;
;;   guint is_modifier : 1;
;; } GdkEventKey;

(cffi:defcstruct GdkEventKey
  (type       GdkEventType)
  (window     GdkWindow*)
  (send_event gint8)
  (time       guint32)
  (state      guint)
  (keyval     guint)
  (length     gint)
  (string     gchar*)
  (hardware_keycode guint16)
  (group      guint8)
  (is_modifier guint))

(defun cffi/gdkeventkey->type (event)
  (if (or (null event) (cffi:null-pointer-p event))
      (error "CFFI/GDKEVENTKEY->TYPE invoked with null EVENT")
      (cffi:foreign-slot-value event '(:struct GdkEventKey) 'type)))

(defun cffi/gdkeventkey->keyval (event)
  (if (or (null event) (cffi:null-pointer-p event))
      (error "CFFI/GDKEVENTKEY->KEYVAL invoked with null EVENT")
      (cffi:foreign-slot-value event '(:struct GdkEventKey) 'keyval)))

(defun cffi/gdkeventkey->state (event)
  (if (or (null event) (cffi:null-pointer-p event))
      (error "CFFI/GDKEVENTKEY->STATE invoked with null EVENT")
      (cffi:foreign-slot-value event '(:struct GdkEventKey) 'state)))

(defun cffi/gdkeventkey->length (event)
  (if (or (null event) (cffi:null-pointer-p event))
      (error "CFFI/GDKEVENTKEY->LENGTH invoked with null EVENT")
      (cffi:foreign-slot-value event '(:struct GdkEventKey) 'length)))

(defun cffi/gdkeventkey->string (event)
  (if (or (null event) (cffi:null-pointer-p event))
      (error "CFFI/GDKEVENTKEY->STRING invoked with null EVENT")
      ;; CFFI already puts this into a Lisp string, so no 'pointer-or-nil'
      (cffi:foreign-slot-value event '(:struct GdkEventKey) 'string)))


;;; ------ GdkVisual

;;; typedef enum {
;;;   GDK_LSB_FIRST,
;;;   GDK_MSB_FIRST
;;; } GdkByteOrder;


;;; struct _GdkVisual {
;;;
;;;   GObject parent_instance
;;;
;;;   GdkVisualType type;
;;;   gint depth;
;;;   GdkByteOrder byte_order;
;;;   gint colormap_size;
;;;   gint bits_per_rgb;
;;;
;;;   guint32 red_mask;
;;;   gint red_shift;
;;;   gint red_prec;
;;;
;;;   guint32 green_mask;
;;;   gint green_shift;
;;;   gint green_prec;
;;;
;;;   guint32 blue_mask;
;;;   gint blue_shift;
;;;   gint blue_prec;
;;; };

(defun cffi/gdk_visual_get_visual_type (visual)
  (cffi:foreign-funcall "gdk_visual_get_visual_type"
			:pointer visual
			GdkVisualType))

;;; ------ GtkBin

;; struct _GtkBin {
;;   GtkContainer container;

;;   GtkWidget *child;
;; };

(cffi:defcstruct GtkBin
  (container (:struct GtkContainer))
  (child     GtkWidget*))


;;; ------ GtkWindow

;; struct _GtkWindow {
;;   GtkBin bin;

;;   gchar *title;
;;   gchar *wmclass_name;
;;   gchar *wmclass_class;
;;   gchar *wm_role;

;;   GtkWidget *focus_widget;
;;   GtkWidget *default_widget;
;;   GtkWindow *transient_parent;
;;   GtkWindowGeometryInfo *geometry_info;
;;   GdkWindow *frame;
;;   GtkWindowGroup *group;

;;   guint16 configure_request_count;
;;   guint allow_shrink : 1;
;;   guint allow_grow : 1;
;;   guint configure_notify_received : 1;
;;   /* The following flags are initially TRUE (before a window is mapped).
;;    * They cause us to compute a configure request that involves
;;    * default-only parameters. Once mapped, we set them to FALSE.
;;    * Then we set them to TRUE again on unmap (for position)
;;    * and on unrealize (for size).
;;    */
;;   guint need_default_position : 1;
;;   guint need_default_size : 1;
;;   guint position : 3;
;;   guint type : 4; /* GtkWindowType */ 
;;   guint has_user_ref_count : 1;
;;   guint has_focus : 1;

;;   guint modal : 1;
;;   guint destroy_with_parent : 1;
  
;;   guint has_frame : 1;

;;   /* gtk_window_iconify() called before realization */
;;   guint iconify_initially : 1;
;;   guint stick_initially : 1;
;;   guint maximize_initially : 1;
;;   guint decorated : 1;
  
;;   guint type_hint : 3; /* GdkWindowTypeHint if the hint is one of the original eight. If not, then
;; 			* it contains GDK_WINDOW_TYPE_HINT_NORMAL
;; 			*/
;;   guint gravity : 5; /* GdkGravity */

;;   guint is_active : 1;
;;   guint has_toplevel_focus : 1;
  
;;   guint frame_left;
;;   guint frame_top;
;;   guint frame_right;
;;   guint frame_bottom;

;;   guint keys_changed_handler;
  
;;   GdkModifierType mnemonic_modifier;
;;   GdkScreen      *screen;
;; };


;;; ------ GtkDialog

;; struct _GtkDialog {
;;   GtkWindow window;

;;   /*< public >*/
;;   GtkWidget *vbox;
;;   GtkWidget *action_area;

;;   /*< private >*/
;;   GtkWidget *separator;
;; };


;;; ------ GtkTextIter

;; struct _GtkTextIter {
;;   /* GtkTextIter is an opaque datatype; ignore all these fields.
;;    * Initialize the iter with gtk_text_buffer_get_iter_*
;;    * functions
;;    */
;;   /*< private >*/
;;   gpointer dummy1;
;;   gpointer dummy2;
;;   gint dummy3;
;;   gint dummy4;
;;   gint dummy5;
;;   gint dummy6;
;;   gint dummy7;
;;   gint dummy8;
;;   gpointer dummy9;
;;   gpointer dummy10;
;;   gint dummy11;
;;   gint dummy12;
;;   /* padding */
;;   gint dummy13;
;;   gpointer dummy14;
;; };

(cffi:defcstruct GtkTextIter
  (dummy1  gpointer)
  (dummy2  gpointer)
  (dummy3  gint)
  (dummy4  gint)
  (dummy5  gint)
  (dummy6  gint)
  (dummy7  gint)
  (dummy8  gint)
  (dummy9  gpointer)
  (dummy10 gpointer)
  (dummy11 gint)
  (dummy12 gint)
  (dummy13 gint)
  (dummy14 gpointer))


;;; ------ GtkColorSelectionDialog

;; struct _GtkColorSelectionDialog {
;;   GtkDialog parent_instance;

;;   GtkWidget *colorsel;
;;   GtkWidget *ok_button;
;;   GtkWidget *cancel_button;
;;   GtkWidget *help_button;
;; };

(defun cffi/gtk_color_selection_dialog_get_color_selection (dialog)
"
Returns the GtkWidget* for the COLORSEL foreign structure slot.
"
  (cffi:foreign-funcall "gtk_color_selection_dialog_get_color_selection"
		        :pointer dialog
			:pointer))  ;; GtkWidget


;; (defun cffi/gtkcolorselectiondialog->ok-button (dialog)
;; "
;; Returns the GtkWidget* for the OK_BUTTON of the colour selection
;; dialog. DUIM needs to create a 'clicked' event handler to detect
;; when this button is activated.

;; Returned pointer must be freed after use.
;; "
;;   (let ((buttonptr (cffi:foreign-alloc :pointer)))
;;     (cffi:foreign-funcall "g_object_get"
;; 			  :pointer dialog
;; 			  :string "ok-button"
;; 			  :pointer buttonptr
;; 			  :pointer nil
;; 			  :void)
;;     buttonptr))


;; (defun cffi/gtkcolorselectiondialog->cancel-button (dialog)
;; "
;; Returns the GtkWidget* for the CANCEL_BUTTON of the colour selection
;; dialog. DUIM needs to create a 'clicked' event handler to detect
;; when this button is activated.

;; Returned pointer must be freed after use.
;; "
;;   (let ((buttonptr (cffi:foreign-alloc :pointer)))
;;     (cffi:foreign-funcall "g_object_get"
;; 			  :pointer dialog
;; 			  :string "cancel-button"
;; 			  :pointer buttonptr
;; 			  :pointer nil
;; 			  :void)
;;     buttonptr))


;; (defun cffi/gtkcolorselectiondialog->help-button (dialog)
;; "
;; Returns the GtkWidget* for the HELP_BUTTON of the color selection
;; dialog. DUIM needs to create a 'clicked' event handler to detect
;; when this button is activated.

;; Returned pointer must be freed after use.
;; "
;;   (let ((buttonptr (cffi:foreign-alloc :pointer)))
;;     (cffi:foreign-funcall "g_object_get"
;; 			  :pointer dialog
;; 			  :string "help-button"
;; 			  :pointer buttonptr
;; 			  :pointer nil
;; 			  :void)
;;     buttonptr))


;;; ------ PangoRectangle

(cffi:defcstruct PangoRectangle
  (x :int) (y :int)
  (width :int) (height :int))

(defun cffi/pangorectangle->x (rect)
  (if (or (null rect) (cffi:null-pointer-p rect))
      (error "CFFI/PANGORECTANGLE->X invoked with null RECT")
      (cffi:foreign-slot-value rect '(:struct PangoRectangle) 'x)))

(defun cffi/set-pangorectangle->x (rect value)
  (if (or (null rect) (cffi:null-pointer-p rect))
      (error "CFFI/SET-PANGORECTANGLE->X invoked with null RECT")
  (setf (cffi:foreign-slot-value rect '(:struct PangoRectangle) 'x) value)))

(defun cffi/pangorectangle->y (rect)
  (if (or (null rect) (cffi:null-pointer-p rect))
      (error "CFFI/PANGORECTANGLE->Y invoked with null RECT")
      (cffi:foreign-slot-value rect '(:struct PangoRectangle) 'y)))

(defun cffi/set-pangorectangle->y (rect value)
  (if (or (null rect) (cffi:null-pointer-p rect))
      (error "CFFI/SET-PANGORECTANGLE->Y invoked with null RECT")
      (setf (cffi:foreign-slot-value rect '(:struct PangoRectangle) 'y) value)))

(defun cffi/pangorectangle->width (rect)
  (if (or (null rect) (cffi:null-pointer-p rect))
      (error "CFFI/PANGORECTANGLE->WIDTH invoked with null RECT")
      (cffi:foreign-slot-value rect '(:struct PangoRectangle) 'width)))

(defun cffi/set-pangorectangle->width (rect value)
  (if (or (null rect) (cffi:null-pointer-p rect))
      (error "CFFI/SET-PANGORECTANGLE->WIDTH invoked with null RECT")
      (setf (cffi:foreign-slot-value rect '(:struct PangoRectangle) 'width) value)))

(defun cffi/pangorectangle->height (rect)
  (if (or (null rect) (cffi:null-pointer-p rect))
      (error "CFFI/PANGORECTANGLE->HEIGHT invoked with null RECT")
      (cffi:foreign-slot-value rect '(:struct PangoRectangle) 'height)))

(defun cffi/set-pangorectangle->height (rect value)
  (if (or (null rect) (cffi:null-pointer-p rect))
      (error "CFFI/SET-PANGORECTANGLE->HEIGHT invoked with null RECT")
      (setf (cffi:foreign-slot-value rect '(:struct PangoRectangle) 'height) value)))
