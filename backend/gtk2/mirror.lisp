;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GTK2-DUIM -*-
(in-package #:gtk2-duim)

#||
Module:       gtk-duim
Synopsis:     GTK mirror implementation
Author:       Andy Armstrong, Scott McKay
Copyright:    Original Code is Copyright (c) 1995-2004 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND

/// GTK panes
||#

(defgeneric widget-mirror (widget)
  (:documentation
"
Returns the mirror associated with _widget_, or NIL if _widget_ is
not directly mirrored.
"))


(defgeneric (setf widget-mirror) (mirror widget)
  (:documentation
"
Creates a direct mirror association between _widget_ and mirror _mirror_.
"))


(defgeneric make-gtk-widget-mirror-instance (mirror-class &rest args &key sheet &allow-other-keys)
  (:documentation
"
Makes a gtk mirror with class _mirror-class_ using the native edges of :SHEET
for the mirror's size. Any other provided arguments are passed through to the
MAKE-INSTANCE call that constructs the instance.
"))


(defgeneric note-mirror-destroyed (sheet mirror))
(defgeneric sheet-screen-position (port sheet)
  (:documentation
"
Returns the position of _sheet_ in 'absolute' (screen) coordinates.
"))


(defgeneric client-to-screen-position (mirror x y)
  (:documentation
"
Given a position (_x_, _y_) within a mirror, convert it to a position on the screen.
"))


(defgeneric do-make-gtk-mirror (sheet)
  (:documentation
"
Construct an appropriate mirror object for the sheet _sheet_ by
calling MAKE-GTK-WIDGET-MIRROR-INSTANCE.
"))


(defgeneric handle-gtk-expose-event-signal (sheet widget event user-data)
  (:documentation
"
Source file: gtk-mirror.lisp
"))


(defgeneric set-mirror-size (mirror width height)
  (:documentation
"
Sets the 'gtk-widget-size-request' of _mirror_ to _width_ x _height_.
_width_ and _height_ are in the coordinate system of the mirror's
parent mirror.
"))


#||
define open abstract class <gtk-pane-mixin>
    (<standard-input-mixin>,
     <mirrored-sheet-mixin>)
end class <gtk-pane-mixin>;
||#

;;; This allows native sheets to be wrapped as "something to do with gtk".
;;; When these things are mirrored, they get associated with an instance of
;;; <gtk-mirror>.
(defclass <gtk-pane-mixin> (<standard-input-mixin> <mirrored-sheet-mixin>) ())


#||
// Returns #t, meaning that the port will take care of repainting
define method port-handles-repaint?
    (_port :: <gtk-port>, sheet :: <mirrored-sheet-mixin>)
 => (true? :: <boolean>)
  #t
end method port-handles-repaint?;
||#

(defmethod port-handles-repaint? ((_port <gtk-port>)
				  (sheet <mirrored-sheet-mixin>))
  ;; Once all the mirrored sheets are actual gtk toolkit widgets, we can probably turn this
  ;; into T. Currently we do want DUIM to repaint things like borders etc. because they're
  ;; drawn onto the GtkWindow's drawable, and gtk will *not* repaint stuff *we* draw on that
  ;; (although it will repaint stuff that it draws).
  nil)



#||
/// GTK mirrors

define open abstract class <gtk-mirror> (<mirror>)
  sealed slot mirror-sheet :: <sheet>,
    required-init-keyword: sheet:;
end class <gtk-mirror>;
||#

(defclass <gtk-mirror> (<mirror>)
  ((mirror-sheet :type <sheet>
		 :initarg :sheet
		 :initform (required-slot ":sheet" "<gtk-mirror>")
		 :accessor mirror-sheet)))


#||
define method initialize
    (mirror :: <gtk-mirror>, #key) => ()
  next-method();
  sheet-direct-mirror(mirror-sheet(mirror)) := mirror;
end method initialize;
||#

(defmethod initialize-instance ((mirror <gtk-mirror>)
				&key
				&allow-other-keys)
  (call-next-method)
  (setf (sheet-direct-mirror (mirror-sheet mirror)) mirror))


#||
define protocol <<gtk-mirror-protocol>> ()
  function make-gtk-mirror
    (sheet :: <abstract-sheet>) => (mirror :: <gtk-mirror>);
  function install-event-handlers
    (sheet :: <abstract-sheet>, mirror :: <gtk-mirror>) => ();
  function update-mirror-attributes
    (sheet :: <abstract-sheet>, mirror :: <gtk-mirror>) => ();
  function set-mirror-parent
    (mirror :: <gtk-mirror>, parent :: <gtk-mirror>) => ();
  function move-mirror
    (parent :: <gtk-mirror>, mirror :: <gtk-mirror>, 
     x :: <integer>, y :: <integer>)
 => ();
  function size-mirror
    (parent :: <gtk-mirror>, mirror :: <gtk-mirror>, 
     width :: <integer>, height :: <integer>)
 => ();
end protocol <<gtk-mirror-protocol>>;
||#

(define-protocol <<gtk-mirror-protocol>> ()
  (:function make-gtk-mirror (sheet))
  (:function install-event-handlers (sheet mirror)
    (:documentation
"
Add event handlers so that GTK events on mirror invoke
appropriate callbacks.
"))
  (:function update-mirror-attributes (sheet mirror)
    (:documentation
"
Updates the state of the mirror to reflect that held internally by DUIM.
For example when a scroll bar is constructed an instance of a native
scroll bar is instantiated, then the scroll offsets and range are set
from those held by DUIM by invoking this method.

DUIM state is updated from the front end via events.
GTK state is updated from DUIM via NOTE-XXX methods.
DUIM sheet state can be reflected in a GTK mirror in bulk
via UPDATE-MIRROR-ATTRIBUTES.
"))

  (:function set-mirror-parent (mirror parent))
  (:function move-mirror (parent mirror x y))
  ;; This one is called from 'set-mirror-edges'
  (:function size-mirror (parent mirror width height)
    (:documentation
"
Sets the dimensions of _mirror_ within _parent_ to be
_width_ x _height_.
")))


#||
define constant $mirror-widget-table :: <object-table> = make(<table>);
||#

(defvar *mirror-widget-table* (make-hash-table)
"
Map of native widget pointer address to Lisp mirror.

Lookup MIRROR from table using (WIDGET-MIRROR WIDGET)
Add MIRROR for WIDGET to table using (SETF (WIDGET-MIRROR WIDGET) MIRROR)
Remove MIRROR from table using (SETF (WIDGET-MIRROR WIDGET) NIL)
")


#||
define sealed method do-make-mirror
    (_port :: <gtk-port>, sheet :: <sheet>)
 => (mirror :: <gtk-mirror>)
  let parent = sheet-device-parent(sheet);
  let mirror = make-gtk-mirror(sheet);
  install-event-handlers(sheet, mirror);
  update-mirror-attributes(sheet, mirror);
  set-mirror-parent(mirror, sheet-direct-mirror(parent));
  mirror
end method do-make-mirror;
||#

(defmethod do-make-mirror ((_port <gtk-port>) (sheet <sheet>))
"
Make a mirror (Lisp object maps onto native widget) for the sheet.
Install event handlers for the mirror passing sheet reference to
map back to DUIM.
Invoke UPDATE-MIRROR-ATTRIBUTES to reflect DUIM sheet's attributes
onto the native widget.
Set mirror parent.
"
  (let* ((parent (sheet-device-parent sheet))
	 (mirror (make-gtk-mirror sheet)))
    (install-event-handlers sheet mirror)
    (update-mirror-attributes sheet mirror)
    (set-mirror-parent mirror (sheet-direct-mirror parent))
    mirror))


#||
define sealed method widget-mirror
    (widget :: <C-pointer>) => (mirror :: false-or(<gtk-mirror>))
  element($mirror-widget-table, pointer-address(widget), default: #f)
end method widget-mirror;
||#

(defmethod widget-mirror (widget)
  (gethash (cffi-sys:pointer-address widget) *mirror-widget-table* nil))


#||
define sealed method widget-mirror-setter
    (mirror :: <gtk-mirror>, widget :: <C-pointer>)
 => (mirror :: <gtk-mirror>)
  element($mirror-widget-table, pointer-address(widget)) := mirror
end method widget-mirror-setter;
||#

(defmethod (setf widget-mirror) ((mirror <gtk-mirror>) widget)
  (setf (gethash (cffi-sys:pointer-address widget) *mirror-widget-table*) mirror))


#||
define sealed method widget-mirror-setter
    (mirror :: singleton(#f), widget :: <C-pointer>)
 => (mirror :: singleton(#f))
  remove-key!($mirror-widget-table, pointer-address(widget));
  #f
end method widget-mirror-setter;
||#

(defmethod (setf widget-mirror) ((mirror null) widget)
  (remhash (cffi-sys:pointer-address widget) *mirror-widget-table*)
  nil)



#||
/// Empty methods on non-window mirrors

define sealed method mirror-edges
    (_port :: <gtk-port>, sheet :: <sheet>, mirror :: <gtk-mirror>)
 => (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
  values(0, 0, 100, 100)	//--- kludge city
end method mirror-edges;
||#

(defmethod mirror-edges ((_port <gtk-port>) (sheet <sheet>) (mirror <gtk-mirror>))
  ;;; FIXME -- should we maybe return an invalid region here?
  (values 0 0 100 100))


#||
// The real methods are on more specific classes, such as <widget-mirror>
define sealed method set-mirror-edges
    (_port :: <gtk-port>, sheet :: <sheet>, mirror :: <gtk-mirror>,
     left  :: <integer>, top    :: <integer>,
     right :: <integer>, bottom :: <integer>) => ()
  #f
end method set-mirror-edges;
||#

(defmethod set-mirror-edges ((_port <gtk-port>) (sheet <sheet>) (mirror <gtk-mirror>)
			     (left integer) (top integer)
			     (right integer) (bottom integer))
  nil)


#||
// Ditto...
define sealed method map-mirror
    (_port :: <gtk-port>, sheet :: <sheet>, mirror :: <gtk-mirror>) => ()
  #f
end method map-mirror;
||#

(defmethod map-mirror ((_port <gtk-port>) (sheet <sheet>) (mirror <gtk-mirror>))
  nil)


#||
// Ditto...
define sealed method unmap-mirror
    (_port :: <gtk-port>, sheet :: <sheet>, mirror :: <gtk-mirror>) => ()
  #f
end method unmap-mirror;
||#

(defmethod unmap-mirror ((_port <gtk-port>) (sheet <sheet>) (mirror <gtk-mirror>))
  nil)


#||
// Ditto...
define sealed method destroy-mirror 
    (_port :: <gtk-port>, sheet :: <sheet>, mirror :: <gtk-mirror>) => ()
  sheet-direct-mirror(sheet) := #f
end method destroy-mirror;
||#

(defmethod destroy-mirror ((_port <gtk-port>) (sheet <sheet>) (mirror <gtk-mirror>))
  (setf (sheet-direct-mirror sheet) nil))


#||
// Ditto...
define method install-event-handlers
    (sheet :: <sheet>, mirror :: <gtk-mirror>) => ()
  #f
end method install-event-handlers;
||#

(defmethod install-event-handlers ((sheet <sheet>) (mirror <gtk-mirror>))
  nil)


#||
// Ditto...
define method update-mirror-attributes
    (sheet :: <sheet>, mirror :: <gtk-mirror>) => ()
  #f
end method update-mirror-attributes;
||#

(defmethod update-mirror-attributes ((sheet <sheet>) (mirror <gtk-mirror>))
  nil)



#||
/// Mirror creation and destruction

define abstract class <widget-mirror> (<gtk-mirror>)
  sealed slot mirror-widget = #f,
    init-keyword: widget:;
  sealed slot %region :: <bounding-box>,
    required-init-keyword: region:;
end class <widget-mirror>;

define sealed domain make (singleton(<widget-mirror>));
define sealed domain initialize (<widget-mirror>);
||#

(defclass <gtk-widget-mirror> (<gtk-mirror>)    ;; FIXME:ABSTRACT
  ((mirror-widget :initarg :widget
		  :initform nil
		  :accessor mirror-widget)
   (%region :type <bounding-box>
	    :initarg :region
	    :initform (required-slot ":region" "<gtk-widget-mirror>")
	    :accessor %region)))


#||
define sealed inline method make
    (mirror :: subclass(<widget-mirror>), #rest args, #key sheet)
 => (mirror :: <widget-mirror>)
  let (left, top, right, bottom) = sheet-native-edges(sheet);
  apply(next-method, mirror,
	region: make-bounding-box(left, top, right, bottom),
	args)
end method make;
||#

;;; MAKE-MIRROR -> DO-MAKE-MIRROR -> MAKE-GTK-MIRROR -> DO-MAKE-GTK-MIRROR
;;; -> MAKE-GTK-WIDGET-MIRROR-INSTANCE. Phew... FIXME?

;;; FIXME: this might well be wrong. Also, does this need to be generic? Is it needed at all?
;;; This could be done as part of the DO-MAKE-GTK-MIRROR method...

(defmethod make-gtk-widget-mirror-instance (mirror-class
					    &rest args
					    &key sheet widget
					    &allow-other-keys)
  (assert (subtypep mirror-class (find-class '<gtk-widget-mirror>)))
  ;; TODO: work out what the "real" region is. Note that the widget is always passed
  ;; in to this method as far as I can tell, so we can pick up the size it wants to be
  ;; from it.
  (multiple-value-bind (width height)
      (widget-size widget)
    (multiple-value-bind (left top right bottom)
	(sheet-native-edges sheet)
      (declare (ignore right bottom))
      (apply #'make-instance
	     mirror-class
	     ;; Make the recorded region be the same as the actual widget extents.
	     :region (make-bounding-box left top (+ left width) (+ top height))
	     args))))


#||
define method initialize
    (mirror :: <widget-mirror>, #key) => ()
  next-method();
  let widget = mirror-widget(mirror);
  when (widget)
    widget-mirror(widget) := mirror
  end
end method initialize;
||#

(defmethod initialize-instance :after ((mirror <gtk-widget-mirror>)
				       &key
				       &allow-other-keys)
  (let ((widget (mirror-widget mirror)))
    (when widget
      (setf (widget-mirror widget) mirror))))


(defmethod update-mirror-attributes ((sheet <sheet>) (mirror <gtk-widget-mirror>))
  (call-next-method)
  (let ((widget (mirror-widget mirror))
	(frame (sheet-frame sheet)))
    (when (and (sheet-handles-keyboard? sheet)
	       (sheet-accepts-focus? sheet))
      (cffi/gtk-widget-set-can-focus widget +true+))
    (when (and frame (eql (frame-input-focus frame) sheet))
      (cffi/gtk-widget-grab-focus widget))))


#||
define sealed method destroy-mirror 
    (_port :: <gtk-port>, sheet :: <sheet>, mirror :: <widget-mirror>) => ()
  let widget = mirror-widget(mirror);
  mirror-widget(mirror) := #f;
  ignoring("destroy-mirror")
end method destroy-mirror;
||#

(defmethod destroy-mirror ((_port <gtk-port>)
			   (sheet <sheet>)
			   (mirror <gtk-widget-mirror>))
  (call-next-method)
  (let ((widget (mirror-widget mirror)))
    (cffi/gtk-widget-destroy widget)
    (setf (mirror-widget mirror) nil)))


#||
//---*** WHAT ABOUT THIS?  WHO IS SUPPOSED TO CALL IT?
// Called by main WM_DESTROY handler
define sealed method note-mirror-destroyed
    (sheet :: <sheet>, mirror :: <widget-mirror>) => ()
  ignoring("note-mirror-destroyed")
  // let handle :: <HWND> = window-handle(mirror);
  // window-mirror(handle) := #f;
  // window-handle(mirror) := $NULL-HWND
end method note-mirror-destroyed;
||#

(defmethod note-mirror-destroyed ((sheet <sheet>) (mirror <gtk-widget-mirror>))
  (ignoring "~%_______________________ NOTE-MIRROR-DESTROYED"))



#||
/// Mirror manipulation

// For non-top-level sheets, we just show the window
define sealed method map-mirror
    (_port :: <gtk-port>, sheet :: <sheet>, mirror :: <widget-mirror>) => ()
  let widget = mirror-widget(mirror);
  debug-message("Showing %=", sheet);
  gtk-widget-show(widget)
end method map-mirror;
||#

(defmethod map-mirror ((_port <gtk-port>) (sheet <sheet>) (mirror <gtk-widget-mirror>))
  (let ((widget (mirror-widget mirror)))
    (cffi/gtk-widget-show widget)))


#||
define sealed method unmap-mirror
    (_port :: <gtk-port>, sheet :: <sheet>, mirror :: <widget-mirror>) => ()
  let widget = mirror-widget(mirror);
  gtk-widget-hide(widget)
end method unmap-mirror;
||#

(defmethod unmap-mirror ((_port <gtk-port>) (sheet <sheet>) (mirror <gtk-widget-mirror>))
  (let ((widget (mirror-widget mirror)))
    (cffi/gtk-widget-hide widget)))


#||
define sealed method raise-mirror 
    (_port :: <gtk-port>, sheet :: <sheet>, mirror :: <widget-mirror>,
     #key activate? = #t)
 => ()
  if (activate?)
    ignoring("activate? keyword to raise-mirror")
  end;
  let widget = mirror-widget(mirror);
  gdk-window-raise(widget.window-value)
end method raise-mirror;
||#

(defmethod raise-mirror ((_port <gtk-port>) (sheet <sheet>) (mirror <gtk-widget-mirror>)
     &key (activate? t))
  (when activate?
    (ignoring "activate? keyword to raise-mirror"))
  (let ((widget (mirror-widget mirror)))
    (cffi/gdk-window-raise (cffi/gtk_widget_get_window widget))))


#||
define sealed method lower-mirror
    (_port :: <gtk-port>, sheet :: <sheet>, mirror :: <widget-mirror>) => ()
  let widget = mirror-widget(mirror);
  gdk-window-lower(widget.window-value)
end method lower-mirror;
||#

(defmethod lower-mirror ((_port <gtk-port>) (sheet <sheet>) (mirror <gtk-widget-mirror>))
  (let ((widget (mirror-widget mirror)))
    (cffi/gdk-window-lower (cffi/gtk_widget_get_window widget))))


#||
define sealed method mirror-visible? 
    (_port :: <gtk-port>, sheet :: <sheet>, mirror :: <widget-mirror>)
 => (visible? :: <boolean>)
  let widget = mirror-widget(mirror);
  gdk-window-is-visible(widget.window-value) == $false
end method mirror-visible?;
||#

(defmethod mirror-visible? ((_port <gtk-port>) (sheet <sheet>) (mirror <gtk-widget-mirror>))
  (let ((widget (mirror-widget mirror)))
    (eql +FALSE+ (cffi/gdk-window-is-visible (cffi/gtk_widget_get_window widget)))))



#||
/// Window mirrors

define sealed method mirror-edges
    (_port :: <gtk-port>, sheet :: <sheet>, mirror :: <widget-mirror>)
 => (left :: <integer>, top :: <integer>, right :: <integer>, bottom :: <integer>)
  box-edges(mirror.%region)
end method mirror-edges;
||#

(defmethod mirror-edges ((_port <gtk-port>) (sheet <sheet>) (mirror <gtk-widget-mirror>))
  (box-edges (%region mirror)))


#||
define sealed method set-mirror-edges
    (_port :: <gtk-port>, sheet :: <sheet>, mirror :: <widget-mirror>,
     left  :: <integer>, top    :: <integer>,
     right :: <integer>, bottom :: <integer>)
 => ()
  let parent = sheet-device-parent(sheet);
  let parent-mirror = sheet-direct-mirror(parent);
  let width  = right - left;
  let height = bottom - top;
  let old-region = mirror.%region;
  let (old-left, old-top)     = box-position(old-region);
  let (old-width, old-height) = box-size(old-region);
  mirror.%region := set-box-edges(mirror.%region, left, top, right, bottom);
  if (left ~== old-left | top ~== old-top)
    move-mirror(parent-mirror, mirror, left, top)
  end;
  if (width ~== old-width | height ~== old-height)
    size-mirror(parent-mirror, mirror, width, height)
  end
end method set-mirror-edges;
||#

(defmethod set-mirror-edges
    ((_port <gtk-port>) (sheet <sheet>) (mirror <gtk-widget-mirror>)
     (left integer) (top integer) (right integer) (bottom integer))
;;; => ()
  (let* ((parent (sheet-device-parent sheet))
	 (parent-mirror (sheet-direct-mirror parent))
	 (width (- right left))
	 (height (- bottom top))
	 (old-region (%region mirror)))
    (multiple-value-bind (old-left old-top)
	(box-position old-region)
      (multiple-value-bind (old-width old-height)
	  (box-size old-region)
	(setf (%region mirror)
	      (set-box-edges (%region mirror) left top right bottom))
	(when (or (/= left old-left)
		  (/= top old-top))
	  (move-mirror parent-mirror mirror left top))
	(when (or (/= width old-width)
		  (/= height old-height))
	  (size-mirror parent-mirror mirror width height))))))


#||
// Returns the position of the sheet in "absolute" (screen) coordinates
define sealed method sheet-screen-position
    (_port :: <gtk-port>, sheet :: <sheet>)
 => (x :: <integer>, y :: <integer>)
  let ancestor  = sheet-device-parent(sheet);
  let transform = sheet-delta-transform(sheet, ancestor);
  // Get the position of the sheet in its mirrored parent's coordinates
  let (x, y) = transform-position(transform, 0, 0);
  let mirror = sheet-direct-mirror(ancestor);
  client-to-screen-position(mirror, x, y)
end method sheet-screen-position;
||#

(defmethod sheet-screen-position ((_port <gtk-port>)
				  (sheet <sheet>))
  (let* ((ancestor (sheet-device-parent sheet))
	 (transform (sheet-delta-transform sheet ancestor)))
    ;; Get the position of the sheet in its mirrored parent's coordinates.
    (multiple-value-bind (x y)
	(transform-position transform 0 0)
      (let ((mirror (sheet-direct-mirror ancestor)))
	(client-to-screen-position mirror x y)))))

#||
// Given a position (x, y) within a mirror, convert it to a position on the screen
define sealed method client-to-screen-position
    (mirror :: <widget-mirror>, x :: <integer>, y :: <integer>)
 => (screen-x :: <integer>, screen-y :: <integer>)
  ignoring("client-to-screen-position");
  values(x, y)
end method client-to-screen-position;
||#

(defmethod client-to-screen-position ((mirror <gtk-widget-mirror>)
				      (x integer)
				      (y integer))
  ;; FIXME: there's a GTK method that will do this...
  (ignoring "client-to-screen-position")
  (values x y))

#||

/// Fixed container mirrors
///
/// The class of mirror that can contain other mirrors

define class <fixed-container-mirror> (<widget-mirror>)
end class <fixed-container-mirror>;
||#

(defclass <gtk-fixed-container-mirror> (<gtk-widget-mirror>) ())

#||
define class <drawing-area-mirror> (<widget-mirror>)
end class <drawing-area-mirror>;
||#

(defclass <gtk-drawing-area-mirror> (<gtk-widget-mirror>) ())

#||
define method make-gtk-mirror 
    (sheet :: <mirrored-sheet-mixin>)
=> (mirror :: <widget-mirror>)
  do-make-gtk-mirror(sheet)
end method make-gtk-mirror;
||#

;;; NOTE: only invoked for mirrored sheets that no more specific
;;; method is defined for; each gadget has a more specific
;;; method defined. What about <border-panes> which are standard
;;; repainting but NOT mirrored...?

(defmethod make-gtk-mirror ((sheet <mirrored-sheet-mixin>))
  (do-make-gtk-mirror sheet))

#||
define method do-make-gtk-mirror 
    (sheet :: <mirrored-sheet-mixin>)
=> (mirror :: <widget-mirror>)
  let widget = GTK-FIXED(gtk-fixed-new());
  //---*** We really want to switch this off entirely...
  gtk-container-set-resize-mode(widget, $GTK-RESIZE-QUEUE);
  make(<fixed-container-mirror>,
       widget: widget,
       sheet:  sheet)
end method do-make-gtk-mirror;
||#

(defmethod do-make-gtk-mirror ((sheet <mirrored-sheet-mixin>))
  (let ((widget (CFFI/GTK-FIXED (cffi/gtk-fixed-new))))
    (make-gtk-widget-mirror-instance (find-class '<gtk-fixed-container-mirror>)
				     :widget widget
				     :sheet  sheet)))

#||
define method do-make-gtk-mirror
    (sheet :: <standard-repainting-mixin>)
  => (mirror :: <widget-mirror>)
  let widget = GTK-DRAWING-AREA(gtk-drawing-area-new());
  gtk-drawing-area-size(widget, 200, 200);
  make(<drawing-area-mirror>,
       widget: widget,
       sheet:  sheet);
end method do-make-gtk-mirror;
||#

(defmethod do-make-gtk-mirror ((sheet <standard-repainting-mixin>))
  (let ((widget (cffi/gtk-drawing-area-new)))
    ;; MIRRORS are constructed before a medium is associated with the sheet so colours
    ;; etc. can't be set here. Really we want to set the widget's bg colour when we know
    ;; the drawable (see gtk-mediums -- get-gcontext).
    (make-gtk-widget-mirror-instance (find-class '<gtk-drawing-area-mirror>)
				     :widget widget
				     :sheet sheet)))


(defmethod port-handles-repaint? ((_port <gtk-port>)
				  (sheet <standard-repainting-mixin>))
  nil)


#||
define method install-event-handlers
    (sheet :: <mirrored-sheet-mixin>, mirror :: <fixed-container-mirror>) => ()
  next-method();
  install-named-handlers(mirror, #[#"expose_event"])
end method install-event-handlers;
||#

(defmethod install-event-handlers ((sheet <mirrored-sheet-mixin>)
				   (mirror <gtk-fixed-container-mirror>))
  (install-named-handlers mirror #("expose-event")))


#||
define method install-event-handlers
    (sheet :: <mirrored-sheet-mixin>, mirror :: <drawing-area-mirror>) => ()
  next-method();
  install-named-handlers(mirror, #[#"expose_event"])
end method install-event-handlers;
||#

(defmethod install-event-handlers ((sheet <mirrored-sheet-mixin>)
				   (mirror <gtk-drawing-area-mirror>))
  (install-named-handlers mirror #("expose-event"
				   "key-press-event"
				   "key-release-event")))


#||
define sealed method handle-gtk-expose-event
    (sheet :: <mirrored-sheet-mixin>, widget :: <GtkWidget*>,
     event :: <GdkEventExpose*>)
 => (handled? :: <boolean>)
  let area   = event.area-value;
  let x      = area.x-value;
  let y      = area.y-value;
  let width  = area.width-value;
  let height = area.height-value;
  let region = make-bounding-box(x, y, x + width, y + height);
  duim-debug-message("Repainting %=: %d, %d %d x %d",
		     sheet, x, y, width, height);
  // We call 'handle-event' instead of 'distribute-event' because we
  // want the repainting to happen between BeginPaint and EndPaint
  handle-event(sheet,
	       make(<window-repaint-event>,
		    sheet: sheet,
		    region: region))
end method handle-gtk-expose-event;
||#

(defmethod handle-gtk-expose-event-signal ((sheet <mirrored-sheet-mixin>)
					   widget ;;<GtkWidget*>)
					   event  ;;<GdkEventExpose*>))
					   user-data)
  (declare (ignore widget user-data))
  (let* ((area (cffi/gdkeventexpose->area event))
	 (native-x (cffi/gdkrectangle->x area))
	 (native-y (cffi/gdkrectangle->y area))
	 (native-width (cffi/gdkrectangle->width area))
	 (native-height (cffi/gdkrectangle->height area))
	 (native-transform (sheet-native-transform sheet)))
    (multiple-value-bind (x y)
	(untransform-position native-transform native-x native-y)
      (multiple-value-bind (width height)
	  (untransform-distance native-transform native-width native-height)
	(let ((region (make-bounding-box x y (+ x width) (+ y height))))
	  ;; we call 'handle-event' instead of 'distribute-event' because we
	  ;; want the repainting to happen between beginpaint and endpaint
	  ;; (i.e. handle the event synchronously right now rather than
	  ;; queuing it up for handling later)
	  (handle-event sheet (make-instance '<window-repaint-event>
					     :sheet sheet
					     :region region))))))
  ;; If we redraw it it's been handled...
  t)


#||
define method set-mirror-parent
    (child :: <widget-mirror>, parent :: <fixed-container-mirror>)
 => ()
  let (x, y) = sheet-native-edges(mirror-sheet(child));
  gtk-fixed-put(mirror-widget(parent),
		mirror-widget(child),
		x, y)
end method set-mirror-parent;
||#

(defmethod set-mirror-parent ((child <gtk-widget-mirror>)
			      (parent <gtk-fixed-container-mirror>))
  (multiple-value-bind (x y width height)
      (sheet-native-edges (mirror-sheet child))
    (declare (ignore width height))
    (cffi/gtk-fixed-put (mirror-widget parent)
			(mirror-widget child)
			x y)))

#||    
define method move-mirror
    (parent :: <fixed-container-mirror>, child :: <widget-mirror>,
     x :: <integer>, y :: <integer>)
 => ()
  gtk-fixed-move(mirror-widget(parent),
		 mirror-widget(child),
		 x, y)
end method move-mirror;
||#

(defmethod move-mirror ((parent <gtk-fixed-container-mirror>)
			(child <gtk-widget-mirror>)
			(x integer)
			(y integer))
  (cffi/gtk-fixed-move (mirror-widget parent)
		       (mirror-widget child)
		       x y))

#||
define method size-mirror
    (parent :: <fixed-container-mirror>, child :: <widget-mirror>,
     width :: <integer>, height :: <integer>)
 => ()
  ignore(parent);
  set-mirror-size(child, width, height)
end method size-mirror;
||#

(defmethod size-mirror ((parent <gtk-fixed-container-mirror>)
			(child <gtk-widget-mirror>)
			(width integer)
			(height integer))
  (declare (ignore parent))
  (set-mirror-size child width height))


#||
define method set-mirror-size
    (mirror :: <widget-mirror>, width :: <integer>, height :: <integer>)
 => ()
  let widget = mirror.mirror-widget;
  let (left, top) = box-position(mirror.%region);
  with-stack-structure (allocation :: <GtkAllocation*>)
    allocation.x-value      := left;
    allocation.y-value      := top;
    allocation.width-value  := width;
    allocation.height-value := height;
    gtk-widget-size-allocate(widget, allocation)
  end
  // ---*** debugging code
//  let (new-width, new-height) = widget-size(widget);
//  if (new-width ~== width | new-height ~== height)
//    debug-message("mirror not resized!: %= wanted: %d x %d, but still: %d x %d", mirror.mirror-sheet, width, height, new-width, new-height);
//  end;
end method set-mirror-size;
||#


(defmethod set-mirror-size ((mirror <gtk-widget-mirror>) (width integer) (height integer))
  (let ((widget (mirror-widget mirror)))
    (cffi/gtk-widget-set-size-request widget width height)))

#-(and)
(defmethod set-mirror-size ((mirror <gtk-widget-mirror>) (width integer) (height integer))
  (let ((widget (mirror-widget mirror)))

    ;; FIXME: THIS IS A HACK. SCROLL BARS DON'T PICK UP THE RIGHT SIZE VIA ALLOCATION,
    ;; ONLY VIA THE SIZE REQUEST. WEIRD + ANNOYING (COULD BE A SIMPLE MATTER OF LEAVING
    ;; THE DEFAULT SIZE DURING COMPOSE SPACE -- TREAT LIKE WE TREAT EDITORS)
    ;;
    ;; MAYBE that's because the allocated size ends up being smaller than the
    ;; size request? Supposedly widgets should cope with that, but maybe they
    ;; don't.
    ;;
    ;; OUTLYERS:
    ;;    + <GTK-SCROLL-BAR> GADGETs
    ;;    + <GTK-SLIDER> GADGETs
    ;;    + <GTK-TEXT-EDITOR> GADGETs
    ;;    + <GTK-DRAWING-AREA-MIRROR> MIRROR [different method]
    ;;    + <GTK-VIEWPORT-MIRROR> MIRROR     [different method]
    ;;
    
    ;; XXX: DEBUG CODE
    #-(and)
    (multiple-value-bind (rwidth rheight)
	(widget-size widget)
      (gtk-debug "[GTK-MIRROR SET-MIRROR-SIZE] setting allocation ~a x ~a on widget which has request ~a x ~a"
		 width height rwidth rheight)
      (when (or (< rwidth 2) (< rheight 2))
	(gtk-debug "    + mirror is for ~a" (mirror-sheet mirror))))

    (LET ((SHEET (MIRROR-SHEET MIRROR)))
      (IF (OR (INSTANCE? SHEET '<GTK-SCROLL-BAR>)
	      (INSTANCE? SHEET '<GTK-SLIDER>)
	      (INSTANCE? SHEET '<GTK-TEXT-EDITOR>))
	  ;; Why doesn't setting the allocation on these work?
    	  (CFFI/GTK-WIDGET-SET-SIZE-REQUEST WIDGET WIDTH HEIGHT)

	  (multiple-value-bind (left top)
	      (box-position (%region mirror))
	    (cffi/with-stack-structure (allocation '(:struct GtkAllocation))
	      (cffi/set-gtkallocation->x allocation left)
	      (cffi/set-gtkallocation->y allocation top)
	      (cffi/set-gtkallocation->width allocation width)
	      (cffi/set-gtkallocation->height allocation height)
	      (cffi/gtk-widget-size-allocate widget allocation)))))

    ;;---*** debugging code
    #-(and)
    (multiple-value-bind (new-width new-height)
	(widget-size widget)
      (when (or (/= new-width width) (/= new-height height))
	(gtk-debug "    + mirror not resized! ~a wanted ~d x ~d but is still ~d x ~d"
		   (mirror-sheet mirror) width height new-width new-height)))

	))


#||
define method set-mirror-size
    (mirror :: <drawing-area-mirror>, width :: <integer>, height :: <integer>)
 => ()
  gtk-drawing-area-size(mirror-widget(mirror), width, height);
end method set-mirror-size;
||#

;;; FIXME: WITH THIS METHOD PRESENT, THE TOP-LEVEL WINDOW CAN'T BE SHRUNK
;;; (WINDOW MUST BE HONOURING THE SIZE-REQUEST OF THE DRAWING-AREA MIRROR).
;;; WITH IT MISSING THE DRAWING AREA APPEARS AT 0,0 WITH EXTENTS 1x1.
;;;
;;; THIS IN CONJUNCTION WITH THE CHANGE ABOVE MEANS THAT MENUS ARE NOT DRAWN
;;; AT THE PROPER SIZE UNTIL AFTER THE WINDOW IS RESIZED AT LEAST A LITTLE.
;;; LOOK AT ALL THE SPACE-REQUIREMENT/ALLOCATION STUFF AGAIN, BUT FOR NOW
;;; LEAVE IT AT THIS.

;;; THERE'S ALSO A METHOD ON THIS FOR <GTK-VIEWPORT-MIRROR>, BELOW. IT LOOKS
;;; LIKE WE HAVE TO DO THIS FOR ALL "CONTAINER" PANES.

#-(and)
(defmethod set-mirror-size ((mirror <gtk-drawing-area-mirror>) (width integer) (height integer))
  #-(and)
  (gtk-debug "[GTK-MIRROR SET-MIRROR-SIZE ~dx~d] on drawing area ~a" width height mirror)
  (let ((widget (mirror-widget mirror)))

    ;; FIXME: When the top-level window is expanded, nothing handles the configuration
    ;; event; DUIM still thinks the window is its original size.

    ;; THIS ONE DOESN'T WORK AT ALL (NOT SURPRISING, IT'S A WIDGET NOT A WINDOW)
    #-(and)
    (cffi/gtk-window-set-default-size widget width height)

    ;; THIS ONE SIZES THE DRAWING AREA BUT WON'T ALLOW THE TOP-LEVEL WINDOW TO
    ;; SHRINK - but it is the one we're "supposed" to use for GtkDrawingArea types.
    ;; Note: this should only be for the default size (i.e. when mirror is first
    ;; mapped / constructed). Should poke the allocation the rest of the time.
    (cffi/gtk-drawing-area-size widget width height)

    ;; THIS ONE SIZES THE DRAWING AREA BUT WON'T ALLOW THE TOP-LEVEL WINDOW TO
    ;; SHRINK
    #-(and)
    (cffi/gtk-widget-set-size-request widget width height)

    ;; THIS ONE DOESN'T SIZE THE DRAWING AREA AT ALL
    #-(and)
    (multiple-value-bind (left top)
	(box-position (%region mirror))
      (cffi/with-stack-structure (allocation '(:struct GtkAllocation))
	(cffi/set-gtkallocation->x allocation left)
	(cffi/set-gtkallocation->y allocation top)
	(cffi/set-gtkallocation->width allocation width)
	(cffi/set-gtkallocation->height allocation height)
	(cffi/gtk-widget-size-allocate widget allocation)))

    ;;---*** debugging code
    #-(and)
    (multiple-value-bind (new-width new-height)
	(widget-size widget)
      (if (or (/= new-width width) (/= new-height height))
	  (duim-debug-message "MIRROR NOT RESIZED!: ~a wanted: ~d x ~d, but still: ~d x ~d"
			      (mirror-sheet mirror) width height new-width new-height)))

      ))




;;;; Viewport mirrors
;;;;
;;;; Provides a scrollable area.

;;;; Is any of this right?

(defclass <gtk-viewport-mirror> (<gtk-widget-mirror>) ())

#||
define method do-make-gtk-mirror 
    (sheet :: <mirrored-sheet-mixin>)
=> (mirror :: <widget-mirror>)
  let widget = GTK-FIXED(gtk-fixed-new());
  //---*** We really want to switch this off entirely...
  gtk-container-set-resize-mode(widget, $GTK-RESIZE-QUEUE);
  make(<fixed-container-mirror>,
       widget: widget,
       sheet:  sheet)
end method do-make-gtk-mirror;
||#

#||
(defmethod do-make-gtk-mirror ((sheet <viewport>))
  (let ((widget (GTK-FIXED (gtk-fixed-new))))
    ;;---*** We really want to switch this off entirely...
    (gtk-container-set-resize-mode widget +GTK-RESIZE-QUEUE+)
    (make-gtk-widget-mirror-instance #m<gtk-fixed-container-mirror>
				     :widget widget
				     :sheet  sheet)))
||#

(defmethod port-handles-repaint? ((_port <gtk-port>)
				  (sheet <viewport>))
  ;; GTK does not automatically repaint GTK-DRAWING-AREA or GTK-LAYOUT
  ;; windows. We need to handle the expose events and have DUIM
  ;; repaint them...
  nil)


#||
define method install-event-handlers
    (sheet :: <mirrored-sheet-mixin>, mirror :: <fixed-container-mirror>) => ()
  next-method();
  install-named-handlers(mirror, #[#"expose_event"])
end method install-event-handlers;


define sealed method handle-gtk-expose-event
    (sheet :: <mirrored-sheet-mixin>, widget :: <GtkWidget*>,
     event :: <GdkEventExpose*>)
 => (handled? :: <boolean>)
  let area   = event.area-value;
  let x      = area.x-value;
  let y      = area.y-value;
  let width  = area.width-value;
  let height = area.height-value;
  let region = make-bounding-box(x, y, x + width, y + height);
  duim-debug-message("Repainting %=: %d, %d %d x %d",
		     sheet, x, y, width, height);
  // We call 'handle-event' instead of 'distribute-event' because we
  // want the repainting to happen between BeginPaint and EndPaint
  handle-event(sheet,
	       make(<window-repaint-event>,
		    sheet: sheet,
		    region: region))
end method handle-gtk-expose-event;
||#

#-(and)
(defmethod handle-gtk-expose-event-signal ((sheet <viewport>)
					   widget ;;<GtkWidget*>)
					   event  ;;<GdkEventExpose*>))
					   user-data)
  (declare (ignore widget user-data))
  #-(and)
  (gtk-debug "[GTK-MIRROR HANDLE-GTK-EXPOSE-EVENT-SIGNAL] entered for viewport ~a" sheet)
  (let* ((area   (cffi/gdkeventexpose->area event))
	 (x      (cffi/gdkrectangle->x area))
	 (y      (cffi/gdkrectangle->y area))
	 (width  (cffi/gdkrectangle->width area))
	 (height (cffi/gdkrectangle->height area))
	 (region (make-bounding-box x y (+ x width) (+ y height))))
    ;; we call 'handle-event' instead of 'distribute-event' because we
    ;; want the repainting to happen between beginpaint and endpaint
    ;; (i.e. handle the event synchronously right now rather than
    ;; queuing it up for handling later)
;;    (warn "INVOKING HANDLE-EVENT FOR REPAINT ON SHEET ~A IN REGION ~A" SHEET REGION)

    ;; XXX: WE DO GET EXPOSE EVENTS! "same window bitblt" appears to not actually redraw
    ;; the window. And we don't get expose events on the viewport whilst scrolling.
    (handle-event sheet (make-instance '<window-repaint-event>
				       :sheet sheet
				       :region region)))
  t)

#||
define method set-mirror-parent
    (child :: <widget-mirror>, parent :: <fixed-container-mirror>)
 => ()
  let (x, y) = sheet-native-edges(mirror-sheet(child));
  gtk-fixed-put(mirror-widget(parent),
		mirror-widget(child),
		x, y)
end method set-mirror-parent;
||#

(defmethod set-mirror-parent ((child <gtk-widget-mirror>)
			      (parent <gtk-viewport-mirror>))
  (multiple-value-bind (x y width height)
      (sheet-native-edges (mirror-sheet child))
    (declare (ignore width height))
    (cffi/gtk-layout-put (mirror-widget parent)
			 (mirror-widget child)
			 x y)))




(defmethod move-mirror ((parent <gtk-viewport-mirror>)
			(child <gtk-widget-mirror>)
			(x integer)
			(y integer))
  (cffi/gtk-layout-move (mirror-widget parent)
			(mirror-widget child)
			x y))


;;; FIXME: WHAT TO DO HERE? :)

(defmethod size-mirror ((parent <gtk-viewport-mirror>)
			(child <gtk-widget-mirror>)
			(width integer)
			(height integer))
  (declare (ignore parent))
  #-(and)
  (gtk-debug "[GTK-MIRROR SIZE-MIRROR ~dx~d] sizing child ~a in parent ~a" width height child parent)
  ;; XXX: Should this ever be called? There doesn't seem to be a lot of
  ;; point to it.
  (set-mirror-size child width height))


;; XXX: Without this, the viewports appear but they have no contents.
#-(and)
(defmethod set-mirror-size ((mirror <gtk-viewport-mirror>) (width integer) (height integer))
  #-(and)
  (gtk-debug "[GTK-MIRROR SET-MIRROR-SIZE] sizing viewport mirror ~a to ~dx~d" mirror width height)
  (let ((widget (mirror-widget mirror)))
    (cffi/gtk-widget-set-size-request widget width height)
    ;; XXX: Should be this, but then the children don't show up
;;    (cffi/gtk-layout-set-size widget width height)
    ;;---*** debugging code
    (multiple-value-bind (new-width new-height)
	(widget-size widget)
      ;; NOTE: WIDGET-SIZE RETURNS THE *SIZE REQUEST*, NOT THE ALLOCATED SIZE!
      (when (or (/= new-width width) (/= new-height height))
	  (duim-debug-message "MIRROR NOT RESIZED!: ~a wanted: ~d x ~d, but still: ~d x ~d"
			      (mirror-sheet mirror) width height new-width new-height)))

      ))
