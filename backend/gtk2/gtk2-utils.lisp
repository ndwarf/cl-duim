;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GTK2-DUIM -*-
(in-package #:gtk2-duim)

#||
Module:       gtk-duim
Synopsis:     GTK back-end utilities
Author:       Andy Armstrong, Scott McKay
Copyright:    Original Code is Copyright (c) 1995-2004 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND

/// Useful constants

define constant $TRUE  :: <integer> = 1;
define constant $FALSE :: <integer> = 0;

define constant $null-gpointer = null-pointer(<gpointer>);
||#

;; FIXME: Should be in CFFI?
(defconstant +TRUE+ 1)
(defconstant +FALSE+ 0)


#||
/// Sealing

define sealed class <sealed-constructor-mixin> (<object>) end;

define sealed domain make (subclass(<sealed-constructor-mixin>));
define sealed domain initialize (<sealed-constructor-mixin>);
||#



#||
/// Error handling

define function not-yet-implemented
    (format-message :: <string>, #rest format-args)
  apply(error, 
	concatenate(format-message, " not yet implemented!"),
	format-args)
end function not-yet-implemented;
||#

(defun not-yet-implemented (format-message &rest format-args)
  (apply #'warn (concatenate 'string
			     format-message
			     " not yet implemented!")
	 format-args))

#||
define function ignoring
    (format-message :: <string>, #rest format-args)
  apply(debug-message, 
	concatenate("Ignoring ", format-message),
	format-args)
end function ignoring;
||#

(defun ignoring (format-message &rest format-args)
  (declare (ignore format-message format-args))
  ;; TODO 2020-04-10 DR: this is too noisy for now, replace it
  ;; when fewer things are ignored.
  #-(and)
  (apply #'duim-debug-message
	 (concatenate 'string "Ignoring " format-message)
	 format-args))

#||
// define constant *gtk-debug* = #f;
define variable *gtk-debug* = #t;
||#

(defparameter *gtk-debug* t)

#||
define inline-only function gtk-debug (#rest args)
  *gtk-debug* & apply(debug-message, args)
end;
||#

(defun gtk-debug (&rest args)
  (and *gtk-debug*
       (apply #'duim-debug-message args)))


#||
/// String conversion utilities

// We're running on Unix where the newline convention is '\n'
define sealed inline method convert-to-native-newlines
    (string :: <byte-string>)
 => (new-string :: <byte-string>)
  string
end method convert-to-native-newlines;
||#

(defun convert-to-native-newlines (string)
  string)

#||
define method convert-from-native-newlines
    (string :: <byte-string>)
 => (new-string :: <byte-string>)
  string
end method convert-from-native-newlines;
||#

(defun convert-from-native-newlines (string)
  string)


    
      
