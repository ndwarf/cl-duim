;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GTK2-DUIM -*-
(in-package #:gtk2-duim)

#||
Module:       gtk-duim
Synopsis:     GTK frame manager implementation
Author:       Andy Armstrong, Scott McKay
Copyright:    Original Code is Copyright (c) 1995-2004 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND

/// GTK frame management

define sealed class <gtk-frame-manager> (<basic-frame-manager>)
end class <gtk-frame-manager>;

define sealed domain make (singleton(<gtk-frame-manager>));
define sealed domain initialize (<gtk-frame-manager>);
||#

(defclass <gtk-frame-manager> (<basic-frame-manager>) ())


#||
define sealed method make-frame-manager
    (_port :: <gtk-port>,
     #key palette, class = <gtk-frame-manager>, #all-keys)
 => (framem :: <frame-manager>)
  make(class, port: _port, palette: palette)
end method make-frame-manager;
||#

(defmethod make-frame-manager ((_port <gtk-port>)
			       &key palette (class (find-class '<gtk-frame-manager>))
			       &allow-other-keys)
  (make-instance class :port _port :palette palette))


#||
define method note-frame-title-changed
    (framem :: <gtk-frame-manager>, frame :: <frame>) => ()
  // Update the title in the top-level window
  let sheet  = top-level-sheet(frame);
  let mirror = sheet & sheet-direct-mirror(sheet);
  when (mirror)
    let widget = GTK-WINDOW(mirror-widget(mirror));
    let title   = frame-title(frame) | "";
    with-c-string (c-string = title)
      gtk-window-set-title(widget, c-string)
    end
  end
end method note-frame-title-changed;
||#

(defmethod note-frame-title-changed ((framem <gtk-frame-manager>) (frame <frame>))
  ;; Update the title in the top-level window
  (let* ((sheet  (top-level-sheet frame))
	 (mirror (and sheet (sheet-direct-mirror sheet))))
    (when mirror
      (let ((widget (CFFI/GTK-WINDOW (mirror-widget mirror)))
	    (title  (or (frame-title frame) "")))
	(cffi/gtk-window-set-title widget title)))))


#||
define method note-frame-icon-changed
    (framem :: <gtk-frame-manager>, frame :: <frame>) => ()
  // Update the icon in the top-level window
  let sheet  = top-level-sheet(frame);
  let mirror = sheet & sheet-direct-mirror(sheet);
  when (mirror)
    update-mirror-icon(mirror, frame-icon(frame))
  end
end method note-frame-icon-changed;
||#

(defmethod note-frame-icon-changed ((framem <gtk-frame-manager>) (frame <frame>))
  ;; Update the icon in the top-level window
  (let* ((sheet (top-level-sheet frame))
	 (mirror (and sheet (sheet-direct-mirror sheet))))
    (when mirror
      (update-mirror-icon mirror (frame-icon frame)))))


#||
define method update-mirror-icon
    (mirror :: <top-level-mirror>, icon :: false-or(<image>)) => ()
  ignoring("update-mirror-icon")
end method update-mirror-icon;
||#

(defmethod update-mirror-icon ((mirror <gtk-top-level-mirror>) (icon <image>))
  ;; FIXME - note that the 'decode-icon' method will be needed at
  ;; some point for the clipboard functionality...
  (let ((gtk-icon nil) ;(decode-icon icon))
	(window   (mirror-widget mirror)))
    (cffi/gtk-window-set-icon window gtk-icon)))


(defmethod update-mirror-icon ((mirror <gtk-top-level-mirror>) (icon null))
  (let ((window (mirror-widget mirror)))
    (cffi/gtk-window-set-icon window nil)))


#||
define method do-frame-occluded?
    (framem :: <gtk-frame-manager>, frame :: <basic-frame>)
 => (occluded? :: <boolean>)
  ignoring("do-frame-occluded?");
  #f
end method do-frame-occluded?;
||#

(defmethod do-frame-occluded? ((framem <gtk-frame-manager>) (frame <basic-frame>))
  (ignoring "do-frame-occluded")
  nil)


#||
define method note-frame-enabled
    (framem :: <gtk-frame-manager>, frame :: <basic-frame>) => ()
  ignoring("note-frame-enabled")
end method note-frame-enabled;
||#

(defmethod note-frame-enabled ((framem <gtk-frame-manager>) (frame <basic-frame>))
  (ignoring "note-frame-enabled"))


#||
define method note-frame-disabled
    (framem :: <gtk-frame-manager>, frame :: <basic-frame>) => ()
  ignoring("note-frame-disabled")
end method note-frame-disabled;
||#

(defmethod note-frame-disabled ((framem <gtk-frame-manager>) (frame <basic-frame>))
  (ignoring "note-frame-disabled"))


#||
define sealed method note-frame-iconified
    (framem :: <gtk-frame-manager>, frame :: <simple-frame>) => ()
  next-method();				// update the frame's state
  let sheet  = top-level-sheet(frame);
  let mirror = sheet & sheet-direct-mirror(sheet);
  when (mirror)
    ignoring("note-frame-iconified")
  end
end method note-frame-iconified;
||#

(defmethod note-frame-iconified ((framem <gtk-frame-manager>) (frame <simple-frame>))
  (call-next-method)  ;; Update the frame's state
  (let* ((sheet  (top-level-sheet frame))
	 (window (and sheet (sheet-direct-mirror sheet))))
    (when window
      (cffi/gtk-window-iconify window))))


#||
define sealed method note-frame-deiconified
    (framem :: <gtk-frame-manager>, frame :: <simple-frame>) => ()
  next-method();				// update the frame's state
  let sheet  = top-level-sheet(frame);
  let mirror = sheet & sheet-direct-mirror(sheet);
  when (mirror)
    ignoring("note-frame-deiconified")
  end
end method note-frame-deiconified;
||#

(defmethod note-frame-deiconified ((framem <gtk-frame-manager>) (frame <simple-frame>))
  (call-next-method)  ;; Update the frame's state
  (let* ((sheet  (top-level-sheet frame))
	 (window (and sheet (sheet-direct-mirror sheet))))
    (when window
      (cffi/gtk-window-deiconify window))))
