;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GTK2-DUIM -*-
(in-package #:gtk2-duim)

;;; ========================================================================
;;;
;;;    Window (DUIM = 'frame') manipulation
;;;

(defun cffi/gtk-window (arg)
  arg)


(defun cffi/gtk-window-new (window-type)
"
GtkWindows do not have a floating reference when they are created.
All other widget types do.
"
  ;; GtkWidget* gtk_window_new (GtkWindowType window_type)
  (cffi:foreign-funcall "gtk_window_new"
			GtkWindowType window-type
			GtkWidget*))


(defun cffi/gtk-window-present (window)
"
void gtk_window_present (GtkWindow *window);

Presents a window to the user. This may mean raising the window in the
stacking order, deiconifying it, moving it to the current desktop,
and/or giving it the keyboard focus, possibly dependent on the user's
platform, window manager, and preferences.

If window is hidden, this function calls gtk_widget_show() as well.

This function should be used when the user tries to open a window
that's already open.
Say for example the preferences dialog is currently open, and the user
chooses Preferences from the menu a second time; use
gtk_window_present() to move the already-open dialog where the user
can see it.

If you are calling this function in response to a user interaction, it
is preferable to use gtk_window_present_with_time().

    window : a GtkWindow
"
  (if (or (null window) (and (cffi:pointerp window) (cffi:null-pointer-p window)))
      (error "CFFI/GTK-WINDOW-PRESENT invoked with null WINDOW")
      (cffi:foreign-funcall "gtk_window_present"
			    :pointer window
			    :void))
  window)


(defun cffi/gtk-window-get-gravity (window)
"
Gets the value set by gtk_window_set_gravity().

    window  - a GtkWindow
    Returns - window gravity 
"
  ;; GdkGravity gtk_window_get_gravity (GtkWindow *window);
  (if (or (null window) (cffi:null-pointer-p window))
      (error "CFFI/GTK-WINDOW-GET-GRAVITY invoked with null WINDOW")
      (let ((gravity (cffi:foreign-funcall "gtk_window_get_gravity" :pointer window :int)))
	gravity)))


(defun cffi/gtk-window-set-decorated (window decorated?)
  "void gtk_window_set_decorated (GtkWindow *window,
                                  gboolean setting);

By default, windows are decorated with a title bar, resize controls,
etc. Some window managers allow GTK+ to disable these decorations,
creating a borderless window. If you set the decorated property to
FALSE using this function, GTK+ will do its best to convince the
window manager not to decorate the window. Depending on the system,
this function may not have any effect when called on a window that is
already visible, so you should call it before calling
gtk_window_show().

On Windows, this function always works, since there's no window
manager policy involved.

Parameters

    - window :: a GtkWindow
    - setting :: TRUE to decorate the window"
  (cffi:foreign-funcall "gtk_window_set_decorated"
			:pointer window
			gboolean decorated?
			:void))

(defun cffi/gtk-window-set-deletable (window deletable?)
  "void gtk_window_set_deletable (GtkWindow *window,
                                  gboolean setting);

By default, windows have a close button in the window frame. Some
window managers allow GTK+ to disable this button. If you set the
deletable property to FALSE using this function, GTK+ will do its best
to convince the window manager not to show a close button. Depending
on the system, this function may not have any effect when called on a
window that is already visible, so you should call it before calling
gtk_window_show().

On Windows, this function always works, since there's no window
manager policy involved.

Parameters

    - window :: a GtkWindow
    - setting :: TRUE to decorate the window as deletable
 
Since: 2.10"
  (cffi:foreign-funcall "gtk_window_set_deletable"
			:pointer window
			gboolean deletable?
			:void))

(defun cffi/gtk-window-set-resizable (window resizable?)
  "void gtk_window_set_resizable (GtkWindow *window,
                                  gboolean resizable);

Sets whether the user can resize a window. Windows are user resizable
by default.

Parameters

    - window :: a GtkWindow
    - resizable :: TRUE if the user can resize this window"
  (cffi:foreign-funcall "gtk_window_set_resizable"
			:pointer window
			gboolean resizable?
			:void))

(defun cffi/gtk-window-set-gravity (window gravity)
"
Window gravity defines the meaning of coordinates passed to
gtk_window_move(). See gtk_window_move() and GdkGravity for more
details.

The default window gravity is GDK_GRAVITY_NORTH_WEST which will
typically \"do what you mean.\"

    window  - a GtkWindow
    gravity - window gravity
"
  ;; void gtk_window_set_gravity (GtkWindow *window, GdkGravity gravity);
  (if (or (null window) (cffi:null-pointer-p window))
      (error "CFFI/GTK-WINDOW-SET-GRAVITY invoked with null WINDOW")
      (cffi:foreign-funcall "gtk_window_set_gravity" :pointer window :int gravity :void)))


(defun cffi/gtk-window-move (window x y)
"
void
gtk_window_get_position (GtkWindow *window,
                         gint *root_x,
                         gint *root_y);

This function returns the position you need to pass to
gtk_window_move() to keep window in its current position. This means
that the meaning of the returned value varies with window gravity. See
gtk_window_move() for more details.

If you haven't changed the window gravity, its gravity will be
GDK_GRAVITY_NORTH_WEST. This means that gtk_window_get_position() gets
the position of the top-left corner of the window manager frame for
the window. gtk_window_move() sets the position of this same top-left
corner.

gtk_window_get_position() is not 100% reliable because the X Window
System does not specify a way to obtain the geometry of the
decorations placed on a window by the window manager. Thus GTK+ is
using a 'best guess' that works with most window managers.

Moreover, nearly all window managers are historically broken with
respect to their handling of window gravity. So moving a window to its
current position as returned by gtk_window_get_position() tends to
result in moving the window slightly. Window managers are slowly
getting better over time.

If a window has gravity GDK_GRAVITY_STATIC the window manager frame is
not relevant, and thus gtk_window_get_position() will always produce
accurate results. However you can't use static gravity to do things
like place a window in a corner of the screen, because static gravity
ignores the window manager decorations.

If you are saving and restoring your application's window positions,
you should know that it's impossible for applications to do this
without getting it somewhat wrong because applications do not have
sufficient knowledge of window manager state. The Correct Mechanism is
to support the session management protocol (see the 'GnomeClient'
object in the GNOME libraries for example) and allow the window
manager to save your window sizes and positions.  Parameters

window - a GtkWindow
root_x - return location for X coordinate of gravity-determined
	 reference point.  [out][allow-none]
root_y - return location for Y coordinate of gravity-determined
	 reference point.  [out][allow-none]
"
  ;; void gtk_window_move (GtkWindow* window, gint x, gint y);
  (if (or (null window) (cffi:null-pointer-p window))
      (error "CFFI/GTK-WINDOW-MOVE invoked with null WINDOW")
      (cffi:foreign-funcall "gtk_window_move"
			    GtkWindow* window
			    gint x
			    gint y
			    :void))
  window)


(defun cffi/gtk-window-set-default-size (window width height)
"
void
gtk_window_set_default_size (GtkWindow *window,
                             gint width,
                             gint height);

Sets the default size of a window. If the window’s “natural” size (its
size request) is larger than the default, the default will be
ignored. More generally, if the default size does not obey the
geometry hints for the window (gtk_window_set_geometry_hints() can be
used to set these explicitly), the default size will be clamped to the
nearest permitted size.

Unlike gtk_widget_set_size_request(), which sets a size request for a
widget and thus would keep users from shrinking the window, this
function only sets the initial size, just as if the user had resized
the window themselves. Users can still shrink the window again as they
normally would. Setting a default size of -1 means to use the
“natural” default size (the size request of the window).

For more control over a window’s initial size and how resizing works,
investigate gtk_window_set_geometry_hints().

For some uses, gtk_window_resize() is a more appropriate
function. gtk_window_resize() changes the current size of the window,
rather than the size to be used on initial
display. gtk_window_resize() always affects the window itself, not the
geometry widget.

The default size of a window only affects the first time a window is
shown; if a window is hidden and re-shown, it will remember the size
it had prior to hiding, rather than using the default size.

Windows can’t actually be 0x0 in size, they must be at least 1x1, but
passing 0 for width and height is OK, resulting in a 1x1 default size.

If you use this function to reestablish a previously saved window
size, note that the appropriate size to save is the one returned by
gtk_window_get_size(). Using the window allocation directly will not
work in all circumstances and can lead to growing or shrinking
windows.  Parameters

window - a GtkWindow
width - width in pixels, or -1 to unset the default width
height - height in pixels, or -1 to unset the default height
"
  ;; void gtk_window_set_default_size (GtkWindow* window,
  ;;                                   gint width, gint height);
  (if (or (null window) (cffi:null-pointer-p window))
      (error "CFFI/GTK-WINDOW-SET-DEFAULT-SIZE invoked with null WINDOW")
      (cffi:foreign-funcall "gtk_window_set_default_size"
			    GtkWindow* window
			    :int width
			    :int height
			    :void))
  window)


(defun cffi/gtk-window-get-size (window width height)
"
 Obtains the current size of window. If window is not onscreen, it
 returns the size GTK+ will suggest to the window manager for the
 initial window size (but this is not reliably the same as the size the
 window manager will actually select). The size obtained by
 gtk_window_get_size() is the last size received in a
 GdkEventConfigure, that is, GTK+ uses its locally-stored size, rather
 than querying the X server for the size. As a result, if you call
 gtk_window_resize() then immediately call gtk_window_get_size(), the
 size won't have taken effect yet. After the window manager processes
 the resize request, GTK+ receives notification that the size has
 changed via a configure event, and the size of the window gets
 updated.

 Note 1: Nearly any use of this function creates a race condition,
 because the size of the window may change between the time that you
 get the size and the time that you perform some action assuming that
 size is the current size. To avoid race conditions, connect to
 \"configure_event\" on the window and adjust your size-dependent state
 to match the size delivered in the GdkEventConfigure.

 Note 2: The returned size does not include the size of the window
 manager decorations (aka the window frame or border). Those are not
 drawn by GTK+ and GTK+ has no reliable method of determining their
 size.

 Note 3: If you are getting a window size in order to position the
 window onscreen, there may be a better way. The preferred way is to
 simply set the window's semantic type with gtk_window_set_type_hint(),
 which allows the window manager to e.g. center dialogs. Also, if you
 set the transient parent of dialogs with
 gtk_window_set_transient_for() window managers will often center the
 dialog over its parent window. It's much preferred to let the window
 manager handle these things rather than doing it yourself, because all
 apps will behave consistently and according to user prefs if the
 window manager handles it. Also, the window manager can take the size
 of the window decorations/border into account, while your application
 cannot.

 In any case, if you insist on application-specified window
 positioning, there's still a better way than doing it yourself -
 gtk_window_set_position() will frequently handle the details for you.

    window - a GtkWindow 
    width  - return location for width, or NULL 
    height - return location for height, or NULL
"
  ;; void gtk_window_get_size (GtkWindow *window, gint *width, gint *height);
  (if (or (null window) (cffi:null-pointer-p window))
      (error "CFFI/GTK-WINDOW-GET-SIZE invoked with null WINDOW")
      (cffi:foreign-funcall "gtk_window_get_size"
			    :pointer window
			    :pointer width
			    :pointer height
			    :void)))


;;; FIXME: Should maybe use gtk-window-set-position too...

(defun cffi/gtk-window-get-position (window wx wy)
"
This function returns the position you need to pass to
gtk_window_move() to keep window in its current position. This means
that the meaning of the returned value varies with window gravity. See
gtk_window_move() for more details.

If you haven't changed the window gravity, its gravity will be
GDK_GRAVITY_NORTH_WEST. This means that gtk_window_get_position() gets
the position of the top-left corner of the window manager frame for
the window. gtk_window_move() sets the position of this same top-left
corner.

gtk_window_get_position() is not 100% reliable because the X Window
System does not specify a way to obtain the geometry of the
decorations placed on a window by the window manager. Thus GTK+ is
using a \"best guess\" that works with most window managers.

Moreover, nearly all window managers are historically broken with
respect to their handling of window gravity. So moving a window to its
current position as returned by gtk_window_get_position() tends to
result in moving the window slightly. Window managers are slowly
getting better over time.

If a window has gravity GDK_GRAVITY_STATIC the window manager frame is
not relevant, and thus gtk_window_get_position() will always produce
accurate results. However you can't use static gravity to do things
like place a window in a corner of the screen, because static gravity
ignores the window manager decorations.

If you are saving and restoring your application's window positions,
you should know that it's impossible for applications to do this
without getting it somewhat wrong because applications do not have
sufficient knowledge of window manager state. The Correct Mechanism is
to support the session management protocol (see the \"GnomeClient\"
object in the GNOME libraries for example) and allow the window
manager to save your window sizes and positions.

    window - a GtkWindow 
    root_x - return location for X coordinate of gravity-determined reference point 
    root_y - return location for Y coordinate of gravity-determined reference point
"
  ;; void gtk_window_get_position (GtkWindow *window, gint *root_x, gint *root_y);
  (if (or (null window) (cffi:null-pointer-p window))
      (error "CFFI/GTK-WINDOW-GET-POSITION invoked with null WINDOW")
      (cffi:foreign-funcall "gtk_window_get_position"
			    :pointer window
			    :pointer wx
			    :pointer wy
			    :void)))


(defun cffi/gtk-window-resize (window width height)
"
Resizes the window as if the user had done so, obeying geometry constraints.
The default geometry constraint is that windows may not be smaller than their
size request; to override this constraint, call gtk_widget_set_size_request()
to set the window's request to a smaller value.

If gtk_window_resize() is called before showing a window for the first time,
it overrides any default size set with gtk_window_set_default_size().

Windows may not be resized smaller than 1 by 1 pixels.

    window - a GtkWindow
    width  - width in pixels to resize the window to
    height - height in pixels to resize the window to
"
  ;; void gtk_window_resize (GtkWindow *window, gint width, gint height);
  (if (or (null window) (cffi:null-pointer-p window))
      (error "CFFI/GTK-WINDOW-RESIZE invoked with null WINDOW")
      (cffi:foreign-funcall "gtk_window_resize"
			    :pointer window
			    gint width
			    gint height
			    :void)))


(defun cffi/gtk-window-set-title (window title)
;;  (gtk-debug "gtk-window-set-title ~a ~a" window title)
  ;; void gtk_window_set_title (GtkWindow* window, const gchar* title)
  (if (or (null window) (cffi:null-pointer-p window))
      (error "CFFI/GTK-WINDOW-SET-TITLE invoked with null WINDOW")
      (if (or (null title) (and (cffi:pointerp title) (cffi:null-pointer-p title)))
	  (error "CFFI/GTK-WINDOW-SET-TITLE invoked with null TITLE")
	  (cffi:foreign-funcall "gtk_window_set_title"
				GtkWindow* window
				:string title
				:void)))
  window)


(defun cffi/gtk-window-set-modal (window modal?)
"
void
gtk_window_set_modal (GtkWindow *window,
                      gboolean modal);

Sets a window modal or non-modal. Modal windows prevent interaction
with other windows in the same application. To keep modal dialogs on
top of main application windows, use gtk_window_set_transient_for() to
make the dialog transient for the parent; most window managers will
then disallow lowering the dialog below the parent.  Parameters

    window - a GtkWindow
    modal  - whether the window is modal
"
;;  (gtk-debug "gtk-window-set-modal ~a ~a" window modal?)
  ;; void gtk_window_set_modal (GtkWindow* window, gboolean modal)
  (if (or (null window) (cffi:null-pointer-p window))
      (error "CFFI/GTK-WINDOW-SET-MODAL invoked with null WINDOW")
      (cffi:foreign-funcall "gtk_window_set_modal"
			    GtkWindow* window
			    gboolean modal?
			    :void))
  window)


(defun cffi/gdk-window-clear-area (window x y width height)
  ;; void gdk_window_clear_area (GdkWindow* window, gint x, gint y,
  ;;                             gint width, gint height
  (if (or (null window) (cffi:null-pointer-p window))
      (error "CFFI/GDK-WINDOW-CLEAR-AREA invoked with null WINDOW")
      (cffi:foreign-funcall "gdk_window_clear_area"
			    GdkWindow* window
			    gint x
			    gint y
			    gint width
			    gint height
			    :void))
  window)


(defun cffi/gdk-window-is-visible (window)
  ;; gboolean gdk_window_is_visible (GdkWindow* window)
  (if (or (null window) (cffi:null-pointer-p window))
      (error "CFFI/GTK-WINDOW-IS-VISIBLE invoked with null WINDOW")
      (cffi:foreign-funcall "gdk_window_is_visible"
			    GdkWindow* window
			    gboolean)))


(defun cffi/gdk-window-lower (window)
  ;; void gdk_window_lower (GdkWindow* window)
  (if (or (null window) (cffi:null-pointer-p window))
      (error "CFFI/GDK-WINDOW-LOWER invoked with null WINDOW")
      (cffi:foreign-funcall "gdk_window_lower"
			    GdkWindow* window
			    :void))
  window)


(defun cffi/gdk-window-raise (window)
  ;; void gdk_window_raise (GdkWindow* window)
  (if (or (null window) (cffi:null-pointer-p window))
      (error "CFFI/GDK-WINDOW-STATE invoked with null WINDOW")
      (cffi:foreign-funcall "gdk_window_raise"
			    GdkWindow* window
			    :void))
  window)


(defun cffi/gdk-window-get-position (window x y)
"
Obtains the position of the window as reported in the
most-recently-processed GdkEventConfigure. Contrast with
gdk_window_get_geometry() which queries the X server for the current
window position, regardless of which events have been received or
processed.

The position coordinates are relative to the window's parent window.

    window - a GdkWindow
    x      - X coordinate of window
    y      - Y coordinate of window
"
  ;; void gdk_window_get_position (GdkWindow *window, gint *x, gint *y);
  (cond ((or (null window) (cffi:null-pointer-p window))
	 (error "CFFI/GDK-WINDOW-GET-POSITION invoked with null WINDOW"))
	((or (null x) (cffi:null-pointer-p x))
	 (error "CFFI/GDK-WINDOW-GET-POSITION invoked with null X"))
	((or (null y) (cffi:null-pointer-p y))
	 (error "CFFI/GDK-WINDOW-GET-POSITION invoked with null Y"))
	(t (cffi:foreign-funcall "gdk_window_get_position"
				 :pointer window
				 :pointer x
				 :pointer y
				 :void)))
  window)


(defun cffi/gdk-window-get-geometry (window x y width height depth)
"
Any of the return location arguments to this function may be NULL, if
you aren't interested in getting the value of that field.

The X and Y coordinates returned are relative to the parent window of
window, which for toplevels usually means relative to the window
decorations (titlebar, etc.) rather than relative to the root
window (screen-size background window).

On the X11 platform, the geometry is obtained from the X server, so
reflects the latest position of window; this may be out-of-sync with
the position of window delivered in the most-recently-processed
GdkEventConfigure. gdk_window_get_position() in contrast gets the
position from the most recent configure event.

Note

If window is not a toplevel, it is much better to call
gdk_window_get_position() and gdk_drawable_get_size() instead, because
it avoids the roundtrip to the X server and because
gdk_drawable_get_size() supports the full 32-bit coordinate space,
whereas gdk_window_get_geometry() is restricted to the 16-bit
coordinates of X11.

    window - a GdkWindow
    x      - return location for X coordinate of window (relative to its parent)
    y      - return location for Y coordinate of window (relative to its parent)
    width  - return location for width of window
    height - return location for height of window
    depth  - return location for bit depth of window
"
  ;; void gdk_window_get_geometry (GdkWindow *window, gint *x, gint *y, gint *width, gint *height, gint *depth);
  (if (or (null window) (cffi:null-pointer-p window))
      (error "CFFI/GDK-WINDOW-GET-GEOMETRY invoked with null WINDOW")
      (cffi:foreign-funcall "gdk_window_get_geometry"
			    :pointer window
			    :pointer x
			    :pointer y
			    :pointer width
			    :pointer height
			    :pointer depth
			    :void))
  window)


(defun cffi/gtk-window-add-accel-group (window accel-group)
"
Associate accel_group with window, such that calling
gtk_accel_groups_activate() on window will activate accelerators in
accel_group.

    window      : window to attach accelerator group to
    accel_group : a GtkAccelGroup
"
  ;; void gtk_window_add_accel_group (GtkWindow *window,
  ;;                                  GtkAccelGroup *accel_group);
  (if (or (null window) (cffi:null-pointer-p window))
      (error "CFFI/GTK-WINDOW-ADD-ACCEL-GROUP called with null WINDOW")
      (if (or (null accel-group) (cffi:null-pointer-p accel-group))
	  (error "CFFI/GTK-WINDOW-ADD-ACCEL-GROUP called with null ACCEL-GROUP")
	  (cffi:foreign-funcall "gtk_window_add_accel_group"
				:pointer window
				:pointer accel-group
				:void)))
  window)


(defun cffi/gtk-window-remove-accel-group (window accel-group)
"
Reverses the effects of gtk_window_add_accel_group().

    window      : a GtkWindow
    accel_group : a GtkAccelGroup
"
  ;; void gtk_window_remove_accel_group (GtkWindow *window,
  ;;                                     GtkAccelGroup *accel_group);
  ;; FIXME: If the "close" button is clicked, the window is cleared up
  ;; and will be nil when we get here. This functionality should be done
  ;; before the window is disposed of!
  (if (or (null window) (cffi:null-pointer-p window))
      (warn "CFFI/GTK-WINDOW-REMOVE-ACCEL-GROUP called with null WINDOW")
      (if (or (null accel-group) (cffi:null-pointer-p accel-group))
	  (error "CFFI/GTK-WINDOW-REMOVE-ACCEL-GROUP called with null ACCEL-GROUP")
	  (cffi:foreign-funcall "gtk_window_remove_accel_group"
				:pointer window
				:pointer accel-group
				:void)))
  window)


(defun cffi/gtk-window-set-transient-for (window parent)
"
Dialog windows should be set transient for the main application window
they were spawned from. This allows window managers to e.g. keep the
dialog on top of the main window, or center the dialog over the main
window. gtk_dialog_new_with_buttons() and other convenience functions
in GTK+ will sometimes call gtk_window_set_transient_for() on your
behalf.

On Windows, this function puts the child window on top of the parent,
much as the window manager would have done on X.

    window - a GtkWindow
    parent - parent window
"
  ;; void gtk_window_set_transient_for (GtkWindow *window, GtkWindow *parent);
  ;;
  ;; I think both of these can be null...
;;  (if (or (null window) (cffi:null-pointer-p window))
;;      (error "CFFI/GTK-WINDOW-SET-TRANSIENT-FOR invoked with null WINDOW")
;;      (if (or (null parent) (cffi:null-pointer-p parent))
;;	  (error "CFFI/GTK-WINDOW-SET-TRANSIENT-FOR invoked with null PARENT")
  (let* ((window (or window (cffi:null-pointer)))
	 (parent (or parent (cffi:null-pointer))))
    (cffi:foreign-funcall "gtk_window_set_transient_for"
			  :pointer window
			  :pointer parent
			  :void))
  window)


(defun cffi/gtk-window-iconify (window)
"
 Asks to iconify (i.e. minimize) the specified window. Note that you
 shouldn't assume the window is definitely iconified afterward, because
 other entities (e.g. the user or window manager) could deiconify it
 again, or there may not be a window manager in which case
 iconification isn't possible, etc. But normally the window will end up
 iconified. Just don't write code that crashes if not.

 It's permitted to call this function before showing a window, in which
 case the window will be iconified before it ever appears onscreen.

 You can track iconification via the 'window_state_event' signal on
 GtkWidget.

     window : a GtkWindow
"
  (if (or (null window) (cffi:null-pointer-p window))
      (error "CFFI/GTK-WINDOW-ICONIFY invoked with null WINDOW")
      (ignoring "cffi:gtk-window-iconify"))
  window)


(defun cffi/gtk-window-deiconify (window)
"
 Asks to deiconify (i.e. unminimize) the specified window. Note that you
 shouldn't assume the window is definitely deiconified afterward, because
 other entities (e.g. the user or window manager) could iconify it again
 before your code which assumes deiconification gets to run.

 You can track iconification via the 'window_state_event' signal on
 GtkWidget.

     window : a GtkWindow
"
  (if (or (null window) (cffi:null-pointer-p window))
      (error "CFFI/GTK-WINDOW-DEICONIFY invoked with null WINDOW")
      (ignoring "cffi/gtk-window-deiconify"))
  window)


(defun cffi/gtk-window-set-icon (window pixbuf)
"
 void        gtk_window_set_icon             (GtkWindow *window,
                                              GdkPixbuf *icon);

 Sets up the icon representing a GtkWindow. This icon is used when
 the window is minimized (also known as iconified). Some window
 managers or desktop environments may also place it in the window
 frame, or display it in other contexts.

 The icon should be provided in whatever size it was naturally
 drawn; that is, don't scale the image before passing it to GTK+.
 Scaling is postponed until the last minute, when the desired
 final size is known, to allow best quality.

 If you have your icon hand-drawn in multiple sizes, use
 gtk_window_set_icon_list(). Then the best size will be used.

 This function is equivalent to calling gtk_window_set_icon_list()
 with a 1-element list.

 See also gtk_window_set_default_icon_list() to set the icon for all
 windows in your application in one go.

    window : a GtkWindow
    icon   : icon image, or NULL
"
  (if (or (null window) (cffi:null-pointer-p window))
      (error "CFFI/GTK-WINDOW-SET-ICON invoked with null WINDOW")
      (let ((pixbuf (or pixbuf (cffi:null-pointer))))
	(declare (ignore pixbuf))
	(ignoring"cffi/gtk-window-set-icon")))
  window)


