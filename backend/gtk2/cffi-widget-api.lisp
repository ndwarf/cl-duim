;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GTK2-DUIM -*-
(in-package #:gtk2-duim)


;;; ========================================================================
;;;
;;;    Floating reference utils
;;;

;;; fixme: deal with this inline just call g_object_ref_sink in all the
;;;        'new' methods

(defun cffi/g_initially_unowned_get_type ()
  (cffi:foreign-funcall "g_initially_unowned_get_type"
			:pointer))

(defun cffi/g_type_is_a (object type)
  (cffi:foreign-funcall "g_type_is_a"
			:pointer object
			:pointer type
			gboolean))

(defun sink-floating-ref (widget)
  (when widget
    (cffi/g-object-ref-sink widget)))


;;; ========================================================================
;;;
;;;    Widgets
;;;

;;; ------ GTK-Widget

(defun cffi/gtk-widget (object)
  object)


(defun cffi/gtk-widget-is-focus (widget)
"
 gboolean gtk_widget_is_focus (GtkWidget *widget);

 Determines if the widget is the focus widget within its toplevel. (This does
 not mean that the HAS_FOCUS flag is necessarily set; HAS_FOCUS will only be
 set if the toplevel widget additionally has the global input focus.)

    widget : a GtkWidget

    Returns : TRUE if the widget is the focus widget.
"
  (if (or (null widget) (and (cffi:pointerp widget) (cffi:null-pointer-p widget)))
      (error "CFFI/GTK-WIDGET-IS-FOCUS invoked with null WIDGET")
      (let ((focused? (cffi:foreign-funcall "gtk_widget_is_focus"
					    :pointer widget
					    :boolean)))
	focused?)))

(defun cffi/gtk-widget-get-can-focus (widget)
"
 gboolean gtk_widget_get_can_focus (GtkWidget *widget);

 Determines whether widget can own the input focus. See gtk_widget_set_can_focus().

    widget : a GtkWidget

    Returns : TRUE if widget can own the input focus, FALSE otherwise

 Since 2.18
"
  (if (or (null widget) (and (cffi:pointerp widget) (cffi:null-pointer-p widget)))
      (error "CFFI/GTK-WIDGET-GET-CAN-FOCUS invoked with null WIDGET")
      (let ((can-focus? (cffi:foreign-funcall "gtk_widget_get_can_focus"
					      :pointer widget
					      :boolean)))
	can-focus?)))


(defun cffi/gtk-widget-set-can-focus (widget can-focus?)
"
 void gtk_widget_set_can_focus (GtkWidget *widget, gboolean can_focus);

 Specifies whether widget can own the input focus. See gtk_widget_grab_focus() for
 actually setting the input focus on a widget.

    widget : a GtkWidget
    can_focus : whether or not widget can own the input focus.

 Since 2.18
"
  (if (or (null widget) (and (cffi:pointerp widget) (cffi:null-pointer-p widget)))
      (error "CFFI/GTK-WIDGET-SET-CAN-FOCUS invoked with null WIDGET")
      (cffi:foreign-funcall "gtk_widget_set_can_focus"
			    :pointer widget
			    :boolean can-focus?
			    :void))
  can-focus?)


(defun cffi/gtk-widget-add-events (widget events)
  ;; void gtk_widget_add_events (GtkWidget* widget, gint events);
  (if (or (null widget) (cffi:null-pointer-p widget))
      (error "CFFI/GTK-WIDGET-ADD-EVENTS invoked with null WIDGET")
      (cffi:foreign-funcall "gtk_widget_add_events"
			    GtkWidget* widget
			    gint events
			    :void))
  events)


(defun cffi/gtk-widget-destroy (widget)
"
void
gtk_widget_destroy (GtkWidget *widget);

Destroys a widget.

When a widget is destroyed all references it holds on other objects
will be released:

    if the widget is inside a container, it will be removed from its
    parent

    if the widget is a container, all its children will be destroyed,
    recursively

    if the widget is a top level, it will be removed from the list of
    top level widgets that GTK+ maintains internally

It's expected that all references held on the widget will also be
released; you should connect to the “destroy” signal if you hold a
reference to widget and you wish to remove it when this function is
called. It is not necessary to do so if you are implementing a
GtkContainer, as you'll be able to use the GtkContainerClass.remove()
virtual function for that.

It's important to notice that gtk_widget_destroy() will only cause the
widget to be finalized if no additional references, acquired using
g_object_ref(), are held on it. In case additional references are in
place, the widget will be in an 'inert' state after calling this
function; widget will still point to valid memory, allowing you to
release the references you hold, but you may not query the widget's
own state.

You should typically call this function on top level widgets, and
rarely on child widgets.

See also: gtk_container_remove()
Parameters

widget - a GtkWidget
"
  ;; void gtk_widget_destroy (GtkWidget* widget)
  (if (or (null widget) (cffi:null-pointer-p widget))
      (error "CFFI/GTK-WIDGET-DESTROY invoked with null WIDGET")
      (cffi:foreign-funcall "gtk_widget_destroy" GtkWidget* widget :void))
  t)


(defun cffi/gtk-widget-hide (widget)
  ;; void gtk_widget_hide (GtkWidget* widget)
  (if (or (null widget) (cffi:null-pointer-p widget))
      (error "CFFI/GTK-WIDGET-HIDE invoked with null WIDGET")
      (cffi:foreign-funcall "gtk_widget_hide" GtkWidget* widget :void))
  widget)


(defun cffi/gtk-widget-set-sensitive (widget sensitive?)
  ;; void gtk_widget_set_sensitive (GtkWidget*, gboolean)
  (if (or (null widget) (cffi:null-pointer-p widget))
      (error "CFFI/GTK-WIDGET-SET-SENSITIVE invoked with null WIDGET")
      (cffi:foreign-funcall "gtk_widget_set_sensitive"
			    GtkWidget* widget
			    gboolean sensitive?
			    :void))
  widget)


(defun cffi/gtk-widget-size-allocate (widget allocation)
  ;; void gtk_widget_size_allocate (GtkWidget* widget, GtkAllocation* alloc)
  (if (or (null widget) (cffi:null-pointer-p widget))
      (error "CFFI/GTK-WIDGET-SIZE-ALLOCATE invoked with null WIDGET")
      (if (or (null allocation) (cffi:null-pointer-p allocation))
	  (error "CFFI/GTK-WIDGET-SIZE-ALLOCATE invoked with null ALLOCATION")
	  (cffi:foreign-funcall "gtk_widget_size_allocate"
				GtkWidget* widget
				GtkAllocation* allocation
				:void)))
  widget)


(defun cffi/gtk-widget-set-size-request (widget width height)
"
Sets the minimum size of a widget; that is, the widget's size request
will be width by height. You can use this function to force a widget
to be either larger or smaller than it normally would be.

In most cases, gtk_window_set_default_size() is a better choice for
toplevel windows than this function; setting the default size will
still allow users to shrink the window. Setting the size request will
force them to leave the window at least as large as the size
request. When dealing with window sizes,
gtk_window_set_geometry_hints() can be a useful function as well.

Note the inherent danger of setting any fixed size - themes,
translations into other languages, different fonts, and user action
can all change the appropriate size for a given widget. So, it's
basically impossible to hardcode a size that will always be correct.

The size request of a widget is the smallest size a widget can accept
while still functioning well and drawing itself correctly. However in
some strange cases a widget may be allocated less than its requested
size, and in many cases a widget may be allocated more space than it
requested.

If the size request in a given direction is -1 (unset), then the
'natural' size request of the widget will be used instead.

Widgets can't actually be allocated a size less than 1 by 1, but you
can pass 0,0 to this function to mean 'as small as possible'.

    widget - a GtkWidget
    width  - width widget should request, or -1 to unset
    height - height widget should request, or -1 to unset
"
  ;; void gtk_widget_set_size_request (GtkWidget *widget, gint width, gint height);
  (if (or (null widget) (cffi:null-pointer-p widget))
      (error "CFFI/GTK-WIDGET-SET-SIZE-REQUEST invoked with null WIDGET")
      (cffi:foreign-funcall "gtk_widget_set_size_request"
			    :pointer widget
			    gint width
			    gint height
			    :void))
  widget)


(defun cffi/gtk-widget-set-tooltip-text (widget text)
"
Sets text as the contents of the tooltip. This function will take care
of setting GtkWidget:has-tooltip to TRUE and of the default handler
for the GtkWidget::query-tooltip signal.

See also the GtkWidget:tooltip-text property and
gtk_tooltip_set_text().

    widget - a GtkWidget
    text   - the contents of the tooltip for widget
"
  ;; void gtk_widget_set_tooltip_text (GtkWidget *widget, const gchar *text);
  (if (or (null widget) (cffi:null-pointer-p widget))
      (error "CFFI/GTK-WIDGET-SET-TOOLTIP-TEXT invoked with null WIDGET")
      ;; FIXME -- SHOULD ALLOW A LISP STRING FOR THE TEXT...
      (if (or (null text) (and (cffi:pointerp text) (cffi:null-pointer-p text)))
	  (error "CFFI/GTK-WIDGET-SET-TOOLTIP-TEXT invoked with null TEXT")
	  (cffi:foreign-funcall "gtk_widget_set_tooltip_text"
				:pointer widget
				:string text
				:void)))
  widget)


(defun cffi/gtk-widget-get-parent-window (widget)
"
 GdkWindow* gtk_widget_get_parent_window (GtkWidget *widget);

 Gets widget's parent window.

    widget  : a GtkWidget.
    Returns : the parent window of widget.
"
  (if (or (null widget) (cffi:null-pointer-p widget))
      (error "CFFI/GTK-WIDGET-GET-PARENT-WINDOW invoked with null WIDGET")
      (let ((window (cffi:foreign-funcall "gtk_widget_get_parent_window"
					  :pointer widget
					  :pointer)))
	(warn "PARENT WINDOW=~a [WINDOW SLOT=~a]" window (cffi/gtk_widget_get_window widget))
	(pointer-or-nil window))))


(defun cffi/gtk-widget-get-style (widget)
  ;; GtkStyle* gtk_widget_get_style (GtkWidget* widget);
  (if (or (null widget) (cffi:null-pointer-p widget))
      (error "CFFI/GTK-WIDGET-GET-STYLE invoked with null WIDGET")
      (let ((style (cffi:foreign-funcall "gtk_widget_get_style"
					 GtkWidget* widget
					 GtkStyle*)))
	(pointer-or-nil style))))


(defun cffi/gtk-widget-size-request (widget requisition)
  ;; void gtk_widget_size_request (GtkWidget* widget, GtkRequisition* req)
  (if (or (null widget) (cffi:null-pointer-p widget))
      (error "CFFI/GTK-WIDGET-SIZE-REQUEST invoked with null WIDGET")
      (if (or (null requisition) (cffi:null-pointer-p requisition))
	  (error "CFFI:GTK-WIDGET-SIZE-REQUEST invoked with null REQUISITION")
	  (cffi:foreign-funcall "gtk_widget_size_request"
				GtkWidget* widget
				GtkRequisition* requisition
				:void)))
  widget)


(defun cffi/gtk-widget-get-allocation (widget alloc)
  ;; void gtk_widget_get_allocation (GtkWidget* widget, GtkAllocation* alloc)
  (if (or (null widget) (cffi:null-pointer-p widget))
      (error "CFFI/GTK-WIDGET-GET-ALLOCATION invoked with null WIDGET")
      (if (or (null alloc) (cffi:null-pointer-p alloc))
	  (error "CFFI/GTK-WIDGET-GET-ALLOCATION invoked with null ALLOCATION")
	  (cffi:foreign-funcall "gtk_widget_get_allocation"
				GtkWidget* widget
				GtkAllocation* alloc
				:void)))
  widget)


(defun cffi/gtk-widget-show (widget)
  ;;; void gtk_widget_show (GtkWidget* widget)
  (if (or (null widget) (cffi:null-pointer-p widget))
      (error "CFFI/GTK-WIDGET-SHOW invoked with null WIDGET")
      (cffi:foreign-funcall "gtk_widget_show" GtkWidget* widget :void))
  widget)


(defun cffi/gtk-widget-draw (widget rectangle)
  ;; void gtk_widget_draw (GtkWidget *widget, GdkRectangle *area);
  (if (or (null widget) (cffi:null-pointer-p widget))
      (error "CFFI/GTK-WIDGET-DRAW invoked with null WIDGET")
      (if (or (null rectangle) (cffi:null-pointer-p rectangle))
	  (error "CFFI/GTK-WIDGET-DRAW invoked with null RECTANGLE")
	  (cffi:foreign-funcall "gtk_widget_draw"
				GtkWidget* widget
				GdkRectangle* rectangle
				:void)))
  widget)


(defun cffi/gtk-widget-grab-focus (widget)
"
Causes widget to have the keyboard focus for the GtkWindow it's inside. widget
must be a focusable widget, such as a GtkEntry; something like GtkFrame won't
work. (More precisely, it must have the GTK_CAN_FOCUS flag set.)

    widget - a GtkWidget
"
  ;; void gtk_widget_grab_focus (GtkWidget *widget);
  (if (or (null widget) (cffi:null-pointer-p widget))
      (error "CFFI/GTK-WIDGET-GRAB-FOCUS invoked with null WIDGET")
      (cffi:foreign-funcall "gtk_widget_grab_focus" :pointer widget :void))
  widget)


(defun cffi/gtk-widget-grab-default (widget)
"
 void gtk_widget_grab_default (GtkWidget *widget);

 Causes widget to become the default widget. widget must have the GTK_CAN_DEFAULT
 flag set; typically you have to set this flag yourself by calling
 gtk_widget_set_can_default (widget, TRUE). The default widget is activated when
 the user presses Enter in a window. Default widgets must be activatable, that is,
 gtk_widget_activate() should affect them.

    widget : a GtkWidget
"
  (if (or (null widget) (cffi:null-pointer-p widget))
      (error "CFFI/GTK-WIDGET-GRAB-DEFAULT invoked with null WIDGET")
      (cffi:foreign-funcall "gtk_widget_grab_default" :pointer widget :void))
  widget)


(defun cffi/gtk-widget-set-can-default (widget can-default?)
"
 void gtk_widget_set_can_default (GtkWidget *widget, gboolean can_default);

 Specifies whether widget can be a default widget. See gtk_widget_grab_default() for
 details about the meaning of 'default'.

    widget : a GtkWidget
    can_default : whether or not widget can be a default widget.

 Since 2.18
"
  (if (or (null widget) (and (cffi:pointerp widget) (cffi:null-pointer-p widget)))
      (error "CFFI/GTK-WIDGET-SET-CAN-DEFAULT invoked with null WIDGET")
      (cffi:foreign-funcall "gtk_widget_set_can_default"
			    :pointer widget
			    :boolean can-default?
			    :void))
  widget)

(defun cffi/gtk-widget-add-accelerator (widget accel-signal accel-group accel-key accel-mods accel-flags)
"
Installs an accelerator for this widget in accel_group that causes accel_signal
to be emitted if the accelerator is activated. The accel_group needs to be
added to the widget's toplevel via gtk_window_add_accel_group(), and the signal
must be of type G_RUN_ACTION. Accelerators added through this function are not
user changeable during runtime. If you want to support accelerators that can be
changed by the user, use gtk_accel_map_add_entry() and
gtk_widget_set_accel_path() or gtk_menu_item_set_accel_path() instead.

    widget       - widget to install an accelerator on
    accel_signal - widget signal to emit on accelerator activation
    accel_group  - accel group for this widget, added to its toplevel
    accel_key    - GDK keyval of the accelerator
    accel_mods   - modifier key combination of the accelerator
    accel_flags  - flag accelerators, e.g. GTK_ACCEL_VISIBLE
"
  ;; void gtk_widget_add_accelerator (GtkWidget *widget,
  ;;                                  const gchar *accel_signal,
  ;;                                  GtkAccelGroup *accel_group,
  ;;                                  guint accel_key,
  ;;                                  GdkModifierType accel_mods,
  ;;                                  GtkAccelFlags accel_flags);
  (cond ((or (null widget) (cffi:null-pointer-p widget))
	 (error "CFFI/GTK-WIDGET-ADD-ACCELERATOR invoked with null WIDGET"))
	((or (null accel-signal) (and (cffi:pointerp accel-signal) (cffi:null-pointer-p accel-signal)))
	 (error "CFFI/GTK-WIDGET-ADD-ACCELERATOR invoked with null ACCEL-SIGNAL"))
	((or (null accel-group) (cffi:null-pointer-p accel-group))
	 (error "CFFI/GTK-WIDGET-ADD-ACCELERATOR invoked with null ACCEL-GROUP"))
	(t (cffi:foreign-funcall "gtk_widget_add_accelerator"
				 :pointer widget
				 :string  accel-signal
				 :pointer accel-group
				 guint    accel-key
				 guint    accel-mods
				 guint    accel-flags
				 :void)))
  widget)


(defun cffi/gtk-widget-get-toplevel (widget)
"
This function returns the topmost widget in the container hierarchy widget is a
part of. If widget has no parent widgets, it will be returned as the topmost
widget. No reference will be added to the returned widget; it should not be
unreferenced.

Note the difference in behavior vs. gtk_widget_get_ancestor();
gtk_widget_get_ancestor (widget, GTK_TYPE_WINDOW) would return NULL if widget
wasn't inside a toplevel window, and if the window was inside a
GtkWindow-derived widget which was in turn inside the toplevel GtkWindow. While
the second case may seem unlikely, it actually happens when a GtkPlug is
embedded inside a GtkSocket within the same application.

To reliably find the toplevel GtkWindow, use gtk_widget_get_toplevel() and
check if the TOPLEVEL flags is set on the result.

 GtkWidget *toplevel = gtk_widget_get_toplevel (widget);
 if (GTK_WIDGET_TOPLEVEL (toplevel))
   {
     [ Perform action on toplevel. ]
   }

    widget  - a GtkWidget
    Returns - the topmost ancestor of widget, or widget itself if there's no
              ancestor.
"
  ;; GtkWidget* gtk_widget_get_toplevel (GtkWidget *widget);
  (if (or (null widget) (cffi:null-pointer-p widget))
      (error "CFFI/GTK-WIDGET-GET-TOPLEVEL invoked with null WIDGET")
      (let ((toplevel (cffi:foreign-funcall "gtk_widget_get_toplevel"
					    :pointer widget
					    :pointer)))
	(pointer-or-nil toplevel))))


;;; ------ GtkBin

(defun cffi/gtk-bin (arg)
  arg)


(defun cffi/gtk-bin-get-child (bin)
  ;; GtkWidget* gtk_bin_get_child (GtkBin* bin);
  (if (or (null bin) (cffi:null-pointer-p bin))
      (error "CFFI/GTK-BIN-GET-CHILD invoked with null BIN")
      (let ((widget (cffi:foreign-funcall "gtk_bin_get_child"
					  GtkBin* bin
					  GtkWidget*)))
	(pointer-or-nil widget))))


;;; ------ GtkLayout


(defun cffi/gtk-layout (object)
  object)


(defun cffi/gtk-layout-new (hadjustment vadjustment)
"
Creates a new GtkLayout. Unless you have a specific adjustment you'd like the
layout to use for scrolling, pass NULL for hadjustment and vadjustment.

    hadjustment - horizontal scroll adjustment, or NULL
    vadjustment - vertical scroll adjustment, or NULL
    Returns     - a new GtkLayout

Sinks initial floating reference
"
  ;; GtkWidget* gtk_layout_new (GtkAdjustment *hadjustment,
  ;;                            GtkAdjustment *vadjustment);
  (let* ((hadjustment (or hadjustment (cffi:null-pointer)))
	 (vadjustment (or vadjustment (cffi:null-pointer)))
	 (layout (cffi:foreign-funcall "gtk_layout_new"
				       :pointer hadjustment
				       :pointer vadjustment
				       :pointer)))
    (sink-floating-ref layout)
    (pointer-or-nil layout)))


(defun cffi/gtk-layout-set-size (layout width height)
"
Sets the size of the scrollable area of the layout.

    layout - a GtkLayout
    width  - width of entire scrollable area
    height - height of entire scrollable area
"
  ;; void gtk_layout_set_size (GtkLayout *layout, guint width, guint height);
  (if (or (null layout) (cffi:null-pointer-p layout))
      (error "CFFI/GTK-LAYOUT-SET-SIZE invoked with null LAYOUT")
      (cffi:foreign-funcall "gtk_layout_set_size"
			    :pointer layout
			    guint    width
			    guint    height
			    :void))
  layout)


(defun cffi/gtk-layout-put (layout child-widget x y)
"
Adds child_widget to layout, at position (x,y). layout becomes the new
parent container of child_widget.

    layout       - a GtkLayout
    child_widget - child widget
    x            - X position of child widget
    y            - Y position of child widget
"
  ;; void gtk_layout_put (GtkLayout *layout, GtkWidget *child_widget, gint x, gint y);
  (if (or (null layout) (cffi:null-pointer-p layout))
      (error "CFFI/GTK-LAYOUT-PUT invoked with null LAYOUT")
      (if (or (null child-widget) (cffi:null-pointer-p child-widget))
	  (error "CFFI/GTK-LAYOUT-PUT invoked with null CHILD-WIDGET")
	  (cffi:foreign-funcall "gtk_layout_put"
				:pointer layout
				:pointer child-widget
				gint x
				gint y
				:void)))
  layout)


(defun cffi/gtk-layout-move (layout child-widget x y)
"
Moves a current child of layout to a new position.

    layout       - a GtkLayout
    child_widget - a current child of layout
    x            - X position to move to
    y            - Y position to move to
"
  ;; void gtk_layout_move (GtkLayout *layout, GtkWidget *child_widget, gint x, gint y);
  (if (or (null layout) (cffi:null-pointer-p layout))
      (error "CFFI/GTK-LAYOUT-MOVE invoked with null LAYOUT")
      (if (or (null child-widget) (cffi:null-pointer-p child-widget))
	  (error "CFFI/GTK-LAYOUT-MOVE invoked with null CHILD-WIDGET")
	  (cffi:foreign-funcall "gtk_layout_move"
				:pointer layout
				:pointer child-widget
				gint x
				gint y
				:void)))
  layout)


(defun cffi/gtk-box-pack-start (box child expand fill padding)
"
Adds child to box , packed with reference to the start of box . The
child is packed after any other child packed with reference to the
start of box.

    box     - a GtkBox
    child   - the GtkWidget to be added to box
    expand  - TRUE if the new child is to be given extra space allocated
              to box. The extra space will be divided evenly between all
              children of box that use this option
    fill    - TRUE if space given to child by the expand option is
              actually allocated to child , rather than just padding it.
              This parameter has no effect if expand is set to FALSE. A
              child is always allocated the full height of a GtkHBox and
              the full width of a GtkVBox. This option affects the other
              dimension
    padding - extra space in pixels to put between this child and its
              neighbors, over and above the global amount specified by
              “spacing” property. If child is a widget at one of the
              reference ends of box , then padding pixels are also put
              between child and the reference edge of box
"
  ;; void gtk_box_pack_start (GtkBox* box, GtkWidget* child, gboolean expand,
  ;;                          gboolean fill, guint padding)
  (if (or (null box) (cffi:null-pointer-p box))
      (error "CFFI/GTK-BOX-PACK-START invoked with null BOX")
      (if (or (null child) (cffi:null-pointer-p child))
	  (error "CFFI/GTK-BOX-PACK-START invoked with null CHILD")
	  (cffi:foreign-funcall "gtk_box_pack_start"
				:pointer box
				:pointer child
				gboolean (if expand 1 0)
				gboolean (if fill 1 0)
				guint padding
				:void)))
  box)
  
(defun cffi/gtk-box-pack-end (box child expand fill padding)
" Adds child to box , packed with reference to the end of box . The
child is packed after (away from end of) any other child packed with
reference to the end of box.

box     - a GtkBox
child   - the GtkWidget to be added to box
expand  - TRUE if the new child is to be given extra space allocated to
          box. The extra space will be divided evenly between all children
          of box that use this option
fill    - TRUE if space given to child by the expand option is actually
          allocated to child , rather than just padding it. This parameter
          has no effect if expand is set to FALSE. A child is always
          allocated the full height of a GtkHBox and the full width of a
          GtkVBox. This option affects the other dimension
padding - extra space in pixels to put between this child and its neighbors,
          over and above the global amount specified by “spacing” property.
          If child is a widget at one of the reference ends of box , then
          padding pixels are also put between child and the reference edge
          of box
"
  ;; void gtk_box_pack_ebd (GtkBox* box, GtkWidget* child, gboolean expand,
  ;;                        gboolean fill, guint padding)
  (if (or (null box) (cffi:null-pointer-p box))
      (error "CFFI/GTK-BOX-PACK-END invoked with null BOX")
      (if (or (null child) (cffi:null-pointer-p child))
	  (error "CFFI/GTK-BOX-PACK-END invoked with null CHILD")
	  (cffi:foreign-funcall "gtk_box_pack_end"
				:pointer box
				:pointer child
				gboolean (if expand 1 0)
				gboolean (if fill 1 0)
				guint padding
				:void)))
  box)
  
;;; ------ Gtk-Drawing-area

(defun cffi/gtk-drawing-area (arg)
  arg)

(defun cffi/gtk-drawing-area-new ()
"
Sinks floating reference
"
  ;; GtkWidget* gtk_drawing_area_new (void)
  (let ((gtk-area (cffi:foreign-funcall "gtk_drawing_area_new" GtkWidget*)))
    (sink-floating-ref gtk-area)
    (pointer-or-nil gtk-area)))


(defun cffi/gtk-drawing-area-size (drawing-area width height)
  ;; void gtk_drawing_area_size (GtkDrawingArea* darea, gint wdth, gint hght)
  (if (or (null drawing-area) (cffi:null-pointer-p drawing-area))
      (error "CFFI/GTK-DRAWING-AREA-SIZE invoked with null DRAWING-AREA")
      (cffi:foreign-funcall "gtk_drawing_area_size"
			    GtkDrawingArea* drawing-area
			    gint width gint height
			    :void))
  drawing-area)


;;; ------ Adjustments

(defun cffi/gtk-adjustment (arg)
  arg)


(defun cffi/gtk-adjustment-new (value lower upper step-increment
				page-increment page-size)
"
Sinks floating reference
"
  ;; GtkObject* gtk_adjustment_new (gdouble value, gdouble lower, gdouble upper
  ;;                                gdouble step_increment,
  ;;                                gdouble page_increment,
  ;;                                gdouble page_size)
  (let ((gtk-obj (cffi:foreign-funcall "gtk_adjustment_new"
				       gdouble value
				       gdouble lower
				       gdouble upper
				       gdouble step-increment
				       gdouble page-increment
				       gdouble page-size
				       GtkObject*)))
    (sink-floating-ref gtk-obj)
    (pointer-or-nil gtk-obj)))

#-(and)
(defun cffi/gtk-adjustment-get-value (adjustment)
  (if (or (null adjustment) (cffi:null-pointer-p adjustment))
      (error "CFFI/GTK-ADJUSTMENT-GET-VALUE invoked with null ADJUSTMENT")
      (cffi/gtk-adjustment-value-value adjustment)))

#-(and)
(defun cffi/gtk-adjustment-get-lower (adjustment)
  (if (or (null adjustment) (cffi:null-pointer-p adjustment))
      (error "CFFI/GTK-ADJUSTMENT-GET-LOWER invoked with null ADJUSTMENT")
      (cffi/gtk-adjustment-lower-value adjustment)))

#-(and)
(defun cffi/gtk-adjustment-get-upper (adjustment)
  (if (or (null adjustment) (cffi:null-pointer-p adjustment))
      (error "CFFI/GTK-ADJUSTMENT-GET-UPPER invoked with null ADJUSTMENT")
      (cffi/gtk-adjustment-upper-value adjustment)))

#-(and)
(defun cffi/gtk-adjustment-get-step-increment (adjustment)
  (if (or (null adjustment) (cffi:null-pointer-p adjustment))
      (error "CFFI/GTK-ADJUSTMENT-GET-STEP-INCREMENT invoked with null ADJUSTMENT")
      (cffi/gtk-adjustment-step-increment-value adjustment)))

#-(and)
(defun cffi/gtk-adjustment-get-page-increment (adjustment)
  (if (or (null adjustment) (cffi:null-pointer-p adjustment))
      (error "CFFI/GTK-ADJUSTMENT-GET-PAGE-INCREMENT invoked with null ADJUSTMENT")
      (cffi/gtk-adjustment-page-increment-value adjustment)))

#-(and)
(defun cffi/gtk-adjustment-get-page-size (adjustment)
  (if (or (null adjustment) (cffi:null-pointer-p adjustment))
      (error "CFFI/GTK-ADJUSTMENT-GET-PAGE-SIZE invoked with null ADJUSTMENT")
      (cffi/gtk-adjustment-page-size-value adjustment)))

#-(and)
(defun cffi/gtk-adjustment-set-value (adjustment value)
  (if (or (null adjustment) (cffi:null-pointer-p adjustment))
      (error "CFFI/GTK-ADJUSTMENT-SET-VALUE")
      (cffi/set-gtk-adjustment-value-value adjustment value)))

#-(and)
(defun cffi/gtk-adjustment-set-lower (adjustment lower)
  (if (or (null adjustment) (cffi:null-pointer-p adjustment))
      (error "CFFI/GTK-ADJUSTMENT-SET-LOWER invoked with null ADJUSTMENT")
      (cffi/set-gtk-adjustment-lower-value adjustment lower)))

#-(and)
(defun cffi/gtk-adjustment-set-upper (adjustment upper)
  (if (or (null adjustment) (cffi:null-pointer-p adjustment))
      (error "CFFI/GTK-ADJUSTMENT-SET-UPPER invoked with null ADJUSTMENT")
      (cffi/set-gtk-adjustment-upper-value adjustment upper)))

#-(and)
(defun cffi/gtk-adjustment-set-step-increment (adjustment step-incr)
  (if (or (null adjustment) (cffi:null-pointer-p adjustment))
      (error "CFFI/GTK-ADJUSTMENT-SET-STEP-INCREMENT invoked with null ADJUSTMENT")
      (cffi/set-gtk-adjustment-step-increment-value adjustment step-incr)))

#-(and)
(defun cffi/gtk-adjustment-set-page-increment (adjustment page-incr)
  (if (or (null adjustment) (cffi:null-pointer-p adjustment))
      (error "CFFI/GTK-ADJUSTMENT-SET-PAGE-INCREMENT invoked with null ADJUSTMENT")
      (cffi/set-gtk-adjustment-page-increment-value adjustment page-incr)))

#-(and)
(defun cffi/gtk-adjustment-set-page-size (adjustment page-size)
  (if (or (null adjustment) (cffi:null-pointer-p adjustment))
      (error "CFFI/GTK-ADJUSTMENT-SET-PAGE-SIZE invoked with null ADJUSTMENT")
      (cffi/set-gtk-adjustment-page-size-value adjustment page-size)))


;;; ------ Buttons

(defun cffi/gtk-button (arg)
  arg)


(defun cffi/gtk-button-new-with-label (label)
"
 GtkWidget * gtk_button_new_with_label (const gchar *label);

 Creates a GtkButton widget with a GtkLabel child containing the given text.

    label : The text you want the GtkLabel to hold.
    Returns : The newly created GtkButton widget.

Sinks initial floating reference
"
  ;;; GtkWidget* gtk_button_new_with_label (const gchar* label);
  (if (or (null label) (and (cffi:pointerp label) (cffi:null-pointer-p label)))
      (error "CFFI/GTK-BUTTON-NEW-WITH-LABEL invoked with null LABEL")
      (let* ((gtk-widget (cffi:foreign-funcall "gtk_button_new_with_label"
					       :string label
					       GtkWidget*)))
	(sink-floating-ref gtk-widget)
	(pointer-or-nil gtk-widget))))


(defun cffi/gtk-button-new-from-stock (stock-id)
"
Creates a new GtkButton containing the image and text from a stock
item. Some stock ids have preprocessor macros like GTK_STOCK_OK and
GTK_STOCK_APPLY.

If stock_id is unknown, then it will be treated as a mnemonic
label (as for gtk_button_new_with_mnemonic()).

    stock_id - the name of the stock item
    Returns  - a new GtkButton

Sinks initial floating reference
"
  ;; GtkWidget* gtk_button_new_from_stock (const gchar *stock_id);
  (if (or (null stock-id) (and (cffi:pointerp stock-id) (cffi:null-pointer-p stock-id)))
      (error "CFFI/GTK-BUTTON-NEW-FROM-STOCK invoked with null STOCK-ID")
      (let ((button (cffi:foreign-funcall "gtk_button_new_from_stock"
					  :string stock-id
					  :pointer)))
	(sink-floating-ref button)
	(pointer-or-nil button))))


(defun cffi/gtk-button-new-with-mnemonic (label)
"
 Creates a new GtkButton containing a label. If characters in label are preceded
 by an underscore, they are underlined. If you need a literal underscore
 character in a label, use '__' (two underscores). The first underlined
 character represents a keyboard accelerator called a mnemonic. Pressing Alt and
 that key activates the button.

    label   - The text of the button, with an underscore in front of the
              mnemonic character
    Returns - a new GtkButton

Sinks initial floating reference
"
  ;; GtkWidget* gtk_button_new_with_mnemonic (const gchar *label);
  (if (or (null label) (and (cffi:pointerp label) (cffi:null-pointer-p label)))
      (error "CFFI/GTK-BUTTON-NEW-WITH-MNEMONIC invoked with null LABEL")
      (let ((button (cffi:foreign-funcall "gtk_button_new_with_mnemonic"
					  :string label
					  :pointer)))
	(sink-floating-ref button)
	(pointer-or-nil button))))


(defun cffi/gtk-button-set-label (button label)
  ;; void gtk_button_set_label (GtkButton* button, const gchar* label);
  (if (or (null button) (cffi:null-pointer-p button))
      (error "CFFI/GTK-BUTTON-SET-LABEL invoked with null BUTTON")
      (if (or (null label) (and (cffi:pointerp label) (cffi:null-pointer-p label)))
	  (error "CFFI/GTK-BUTTON-SET-LABEL invoked with null LABEL")
	  (cffi:foreign-funcall "gtk_button_set_label"
				GtkButton* button
				:string label
				:void)))
  button)


(defun cffi/gtk-check-button (arg)
  arg)


(defun cffi/gtk-check-button-new ()
"
Creates a new GtkCheckButton.

    Returns - a GtkWidget.

Sinks initial floating reference
"
  ;; GtkWidget* gtk_check_button_new (void);
  (let ((button (cffi:foreign-funcall "gtk_check_button_new"
				      :pointer)))
    (sink-floating-ref button)
    button))



(defun cffi/gtk-check-button-new-with-label (label)
"
Sinks initial floating reference
"
  ;;; GtkWidget* gtk_check_button_new_with_label (const gchar* label);
  (if (or (null label) (and (cffi:pointerp label) (cffi:null-pointer-p label)))
      (error "CFFI/GTK-CHECK-BUTTON-NEW-WITH-LABEL invoked with null LABEL")
      (let ((gtk-widget (cffi:foreign-funcall "gtk_check_button_new_with_label"
					      :string label
					      GtkWidget*)))
	(sink-floating-ref gtk-widget)
	(pointer-or-nil gtk-widget))))


(defun cffi/gtk-check-button-new-with-mnemonic (label)
"
Creates a new GtkCheckButton containing a label. The label will be created
using gtk_label_new_with_mnemonic(), so underscores in label indicate the
mnemonic for the check button.

    label   - The text of the button, with an underscore in front of the mnemonic character
    Returns - a new GtkCheckButton

Sinks initial floating reference
"
  ;; GtkWidget* gtk_check_button_new_with_mnemonic (const gchar *label);
  (if (or (null label) (cffi:null-pointer-p label))
      (error "CFFI/GTK-CHECK-BUTTON-NEW-WITH-MNEMONIC invoked with null LABEL")
      (let ((button (cffi:foreign-funcall "gtk_check_button_new_with_mnemonic"
					  :string label
					  :pointer)))
	(sink-floating-ref button)
	(pointer-or-nil button))))


(defun cffi/gtk-radio-button (arg)
  arg)


(defun cffi/gtk-radio-button-new (group)
"
Creates a new GtkRadioButton. To be of any practical value, a widget should
then be packed into the radio button.

    group   - an existing radio button group, or NULL if you are creating a new
              group.
    Returns - a new radio button.

Sinks initial floating reference
"
  ;; GtkWidget* gtk_radio_button_new (GSList *group);
  (let* ((group (or group (cffi:null-pointer)))
	 (button (cffi:foreign-funcall "gtk_radio_button_new" :pointer group :pointer)))
    (sink-floating-ref button)
    (pointer-or-nil button)))


(defun cffi/gtk-radio-button-new-with-label (group label)
"
Sinks initial floating reference
"
  ;;; GtkWidget* gtk_radio_button_new_with_label (GSList* group,
  ;;;                                             const gchar* label);
  (let* ((group (or group (cffi:null-pointer)))
	 (gtk-widget (cffi:foreign-funcall "gtk_radio_button_new_with_label"
					   GSList* group
					   :string label
					   GtkWidget*)))
    (sink-floating-ref gtk-widget)
    (pointer-or-nil gtk-widget)))


(defun cffi/gtk-radio-button-new-with-mnemonic (group label)
"
Creates a new GtkRadioButton containing a label, adding it to the same group as
group. The label will be created using gtk_label_new_with_mnemonic(), so
underscores in label indicate the mnemonic for the button.

    group   - the radio button group
    label   - the text of the button, with an underscore in front of the mnemonic character
    Returns - a new GtkRadioButton

Sinks initial floating reference
"
  ;; GtkWidget* gtk_radio_button_new_with_mnemonic (GSList *group, const gchar *label);
  (if (or (null label) (and (cffi:pointerp label) (cffi:null-pointer-p label)))
      (error "CFFI/GTK-RADIO-BUTTON-NEW-WITH-MNEMONIC invoked with null LABEL")
      (let* ((group (or group (cffi:null-pointer)))
	     (button (cffi:foreign-funcall "gtk_radio_button_new_with_mnemonic"
					   :pointer group
					   :string label
					   :pointer)))
	(sink-floating-ref button)
	(pointer-or-nil button))))


(defun cffi/gtk-radio-button-new-with-label-from-widget (widget label)
"
Sinks initial floating reference
"
  ;;; GtkWidget* gtk_radio_button_new_with_label_from_widget
  ;;;                                (GtkRadioButton* group_member,
  ;;;                                 const gchar* label);

  #|| FIXME:
  (with-null-disallowed "CFFI/GTK-RADIO-BUTTON-NEW-WITH-LABEL-FROM-WIDGET" (widget label)...
  ||#
  (if (or (null widget) (cffi:null-pointer-p widget))
      (error "CFFI/GTK-RADIO-BUTTON-NEW-WITH-LABEL-FROM-WIDGET invoked with null WIDGET")
      (if (or (null label) (and (cffi:pointerp label) (cffi:null-pointer-p label)))
	  (error "CFFI/GTK-RADIO-BUTTON-NEW-WITH-LABEL-FROM-WIDGET invoked with null LABEL")
	  (let ((gtk-widget (cffi:foreign-funcall "gtk_radio_button_new_with_label_from_widget"
						  GtkRadioButton* widget
						  :string label
						  GtkWidget*)))
	    (sink-floating-ref gtk-widget)
	    (pointer-or-nil gtk-widget)))))


(defun cffi/gtk-radio-button-get-group (button)
  ;; GSList* gtk_radio_button_get_group (GtkRadioButton* radio_button);
  (if (or (null button) (cffi:null-pointer-p button))
      (error "CFFI/GTK-RADIO-BUTTON-GET-GROUP invoked with null BUTTON")
      (let ((button-group (cffi:foreign-funcall "gtk_radio_button_get_group"
						GtkRadioButton* button
						GSList*)))
	(pointer-or-nil button-group))))


(defun cffi/gtk-radio-button-set-group (button group)
  ;; void gtk_radio_button_set_group (GtkRadioButton* button, GSList* group);
  (if (or (null button) (cffi:null-pointer-p button))
      (error "CFFI/GTK-RADIO-BUTTON-SET-GROUP invoked with null BUTTON")
      (if (or (null group) (cffi:null-pointer-p group))
	  (error "CFFI/GTK-RADIO-BUTTON-SET-GROUP invoked with null GROUP")
	  (cffi:foreign-funcall "gtk_radio_button_set_group"
				GtkRadioButton* button
				GSList* group
				:void)))
  button)


(defun cffi/gtk-toggle-button (arg)
  arg)


(defun cffi/gtk-toggle-button-new ()
"
 Creates a new toggle button. A widget should be packed into the button, as in
 gtk_button_new().

    Returns - a new toggle button.

Sinks initial floating reference
"
  ;; GtkWidget* gtk_toggle_button_new (void);
  (let ((button (cffi:foreign-funcall "gtk_toggle_button_new" :pointer)))
    (sink-floating-ref button)
    (pointer-or-nil button)))


(defun cffi/gtk-toggle-button-new-with-label (label)
"
Sinks initial floating reference
"
  ;; GtkWidget* gtk_toggle_button_new_with_label (const gchar* label);
  (if (or (null label) (and (cffi:pointerp label) (cffi:null-pointer-p label)))
      (error "CFFI/GTK-TOGGLE-BUTTON-NEW-WITH-LABEL invoked with null LABEL")
      (let ((gtk-toggle (cffi:foreign-funcall "gtk_toggle_button_new_with_label"
					      :string label
					      GtkWidget*)))
	(sink-floating-ref gtk-toggle)
	(pointer-or-nil gtk-toggle))))


(defun cffi/gtk-toggle-button-new-with-mnemonic (label)
"
 Creates a new GtkToggleButton containing a label. The label will be created
 using gtk_label_new_with_mnemonic(), so underscores in label indicate the
 mnemonic for the button.

    label   - the text of the button, with an underscore in front of the
              mnemonic character
    Returns - a new GtkToggleButton

Sinks initial floating reference
"
  ;; GtkWidget* gtk_toggle_button_new_with_mnemonic (const gchar *label);
  (if (or (null label) (cffi:null-pointer-p label))
      (error "CFFI/GTK-TOGGLE-BUTTON-NEW-WITH-MNEMONIC invoked with null LABEL")
      (let ((button (cffi:foreign-funcall "gtk_toggle_button_new_with_mnemonic"
					  :string label
					  :pointer)))
	(sink-floating-ref button)
	(pointer-or-nil button))))


(defun cffi/gtk-toggle-button-set-active (button active?)
  ;; void gtk_toggle_button_set_active (GtkToggleToolButton* button,
  ;;                                    gboolean is_active);
  (if (or (null button) (cffi:null-pointer-p button))
      (error "CFFI/GTK-TOGGLE-BUTTON-SET-ACTIVE invoked with null BUTTON")
      (cffi:foreign-funcall "gtk_toggle_button_set_active"
			    GtkToggleButton* button
			    gboolean active?
			    :void))
  button)


(defun cffi/gtk-toggle-button-get-active (button)
  ;; gboolean gtk_toggle_button_get_active (GtkToggleToolButton* button);
  (if (or (null button) (cffi:null-pointer-p button))
      (error "CFFI/GTK-TOGGLE-BUTTON-GET-ACTIVE invoked with null BUTTON")
      (let ((active? (cffi:foreign-funcall "gtk_toggle_button_get_active"
					   GtkToggleButton* button
					   gboolean)))
	active?)))


;;; ------ Labels

(defun cffi/gtk-label (arg)
  arg)


(defun cffi/gtk-label-new (text)
"
Sinks initial floating reference
"
  ;; GtkWidget* gtk_label_new (const gchar* str)
  (if (or (null text) (and (cffi:pointerp text) (cffi:null-pointer-p text)))
      (error "CFFI/GTK-LABEL-NEW invoked with null TEXT")
      (let ((gtk-label (cffi:foreign-funcall "gtk_label_new"
					     :string text
					     GtkWidget*)))
	(sink-floating-ref gtk-label)
	(pointer-or-nil gtk-label))))


(defun cffi/gtk-label-set-text (widget text)
;;  (gtk-debug "gtk-label-set-text ~a ~a" widget text)
  ;; void gtk_label_set_text (GtkWidget* label, const gchar* str)
  (if (or (null widget) (cffi:null-pointer-p widget))
      (error "CFFI/GTK-LABEL-SET-TEXT invoked with null WIDGET")
      (if (or (null text) (and (cffi:pointerp text) (cffi:null-pointer-p text)))
	  (error "CFFI/GTK-LABEL-SET-TEXT invoked with null TEXT")
	  (cffi:foreign-funcall "gtk_label_set_text"
				GtkWidget* widget
				:string text
				:void)))
  widget)


;;; ------ GtkListStore

(defun cffi/gtk-list-store-newv (ncols types)
"
 Creates a new list store as with n_columns columns each of the types
 passed in. Note that only types derived from standard GObject fundamental
 types are supported.

 As an example,

    gtk_tree_store_new (3, G_TYPE_INT, G_TYPE_STRING, GDK_TYPE_PIXBUF);

 will create a new GtkListStore with three columns, of type int, string
 and GdkPixbuf respectively.

    n_columns - number of columns in the list store
    types     - an array of GType types for the columns, from first to last

    Returns   - a new GtkListStore

Sinks initial floating reference
"
  ;; A GType is just a gint.
  ;; GtkListStore* gtk_list_store_newv (gint n_columns, GType* types);
  (if (or (null types) (cffi:null-pointer-p types))
      (error "CFFI/GTK-LIST-STORE-NEWV invoked with null TYPES")
      (let ((store (cffi:foreign-funcall "gtk_list_store_newv"
					 gint   ncols
					 GType* types
					 GtkListStore*)))
	(sink-floating-ref store)
	(pointer-or-nil store))))


(defun cffi/gtk-list-store-append (list-store iter)
"
Appends a new row to list_store. iter will be changed to point to this
new row. The row will be empty after this function is called. To fill
in values, you need to call gtk_list_store_set(), or
gtk_list_store_set_value() [-> gtk-list-store-set-valist].

    list_store - A GtkListStore 
    iter       - An unset GtkTreeIter to set to the appended row 
"
  ;; void gtk_list_store_append (GtkListStore* list_store, GtkTreeIter* iter);
  (if (or (null list-store) (cffi:null-pointer-p list-store))
      (error "CFFI/GTK-LIST-STORE-APPEND invoked with null LIST-STORE")
      (if (or (null iter) (cffi:null-pointer-p iter))
	  (error "CFFI/GTK-LIST-STORE-APPEND invoked with null ITER")
	  (cffi:foreign-funcall "gtk_list_store_append"
				GtkListStore* list-store
				GtkTreeIter*  iter
				:void)))
  list-store)


(defun cffi/gtk-list-store-set-valuesv (list-store iter columns values nvalues)
"
 void gtk_list_store_set_valuesv (GtkListStore *list_store,
                                  GtkTreeIter *iter,
                                  gint *columns,
                                  GValue *values,
                                  gint n_values);

 A variant of gtk_list_store_set_valist() which takes the columns and values as two
 arrays, instead of varargs. This function is mainly intended for language-bindings
 and in case the number of columns to change is not known until run-time.

    list_store : A GtkListStore
    iter : A valid GtkTreeIter for the row being modified
    columns : an array of column numbers. [array length=n_values]
    values : an array of GValues. [array length=n_values]
    n_values : the length of the columns and values arrays

 Documentation for gtk_list_store_set_valist:-

 void gtk_list_store_set_valist (GtkListStore *list_store,
                                 GtkTreeIter *iter,
                                 va_list var_args);

 See gtk_list_store_set(); this version takes a va_list for use by language bindings.

    list_store : A GtkListStore
    iter : A valid GtkTreeIter for the row being modified
    var_args : va_list of column/value pairs

 Documentation for gtk_list_store_set:-

 Sets the value of one or more cells in the row referenced by iter. The
 variable argument list should contain integer column numbers, each column
 number followed by the value to be set. The list is terminated by a -1.
 For example, to set column 0 with type G_TYPE_STRING to ``Foo'', you would
 write gtk_list_store_set (store, iter, 0, ``Foo'', -1).

    list_store - a GtkListStore
    iter       - row iterator
    ...        - pairs of column number and value, terminated with -1
"
  ;; void gtk_list_store_set_valuesv (GtkListStore *list_store,
  ;;                                  GtkTreeIter *iter,
  ;;                                  gint *columns,
  ;;                                  GValue *values,
  ;;                                  gint n_values);
  (cond ((or (null list-store) (cffi:null-pointer-p list-store))
	 (error "CFFI/GTK-LIST-STORE-SET-VALUESV invoked with null LIST-STORE"))
	((or (null iter) (cffi:null-pointer-p iter))
	 (error "CFFI/GTK-LIST-STORE-SET-VALUESV invoked with null ITER"))
	((or (null columns) (cffi:null-pointer-p columns))
	 (error "CFFI/GTK-LIST-STORE-SET-VALUESV invoked with null COLUMNS"))
	((or (null values) (cffi:null-pointer-p values))
	 (error "CFFI/GTK-LIST-STORE-SET-VALUESV invoked with null VALUES"))
	(t (cffi:foreign-funcall "gtk_list_store_set_valuesv"
				 :pointer list-store
				 :pointer iter
				 :pointer columns
				 :pointer values
				 gint nvalues
				 :void)))
  list-store)


;;; ------ GtkTreeView

(defun cffi/gtk-tree-view (tree)
  tree)


(defun cffi/gtk-tree-view-new ()
"
Creates a new GtkTreeView widget.

    Returns - A newly created GtkTreeView widget.

Sinks initial floating reference
"
  ;; GtkWidget* gtk_tree_view_new (void);
  (let ((tree (cffi:foreign-funcall "gtk_tree_view_new" GtkWidget*)))
    (sink-floating-ref tree)
    (pointer-or-nil tree)))


(defun cffi/gtk-tree-view-set-model (tree-view model)
"
 Sets the model for a GtkTreeView. If the tree_view already has a model
 set, it will remove it before setting the new model. If model is NULL,
 then it will unset the old model.

    tree_view - A GtkTreeNode.
    model - The model.
"
  ;; void gtk_tree_view_set_model (GtkTreeView* tree_view,
  ;;                               GtkTreeModel* model);
  (if (or (null tree-view) (cffi:null-pointer-p tree-view))
      (error "CFFI/GTK-TREE-VIEW-SET-MODEL invoked with null TREE-VIEW")
      (let* ((model (or model (cffi:null-pointer))))
	(cffi:foreign-funcall "gtk_tree_view_set_model"
			      GtkTreeView* tree-view
			      GtkTreeModel* model
			      :void)))
  tree-view)


(defun cffi/gtk-tree-store-newv (ncols types)
"
 GtkTreeStore * gtk_tree_store_newv (gint n_columns, GType *types);

 Non vararg creation function. Used primarily by language bindings.

    n_columns : number of columns in the tree store
    types : an array of GType types for the columns, from first to last. [array length=n_columns]

    Returns : a new GtkTreeStore. [transfer full]

Sinks initial floating reference
"
  (if (or (null types) (cffi:null-pointer-p types))
      (error "CFFI/GTK-LIST-STORE-NEWV invoked with null TYPES")
      (let ((store (cffi:foreign-funcall "gtk_list_store_newv"
					 :int ncols
					 :pointer types
					 :pointer)))
	(sink-floating-ref store)
	(pointer-or-nil store))))

(defun cffi/gtk-tree-store-append (store iter)
  (declare (ignore store iter))
  (not-yet-implemented "cffi/gtk-tree-store-append")
  nil)

(defun cffi/gtk-tree-store-set-valuesv (store iter columns values ncols)
  (declare (ignore store iter columns values ncols))
  (not-yet-implemented "cffi/gtk-tree-store-set-valuesv")
  nil)

(defun cffi/gtk-tree-selection-set-mode (tree-selection type)
"
 Sets the selection mode of the selection. If the previous type was
 GTK_SELECTION_MULTIPLE, then the anchor is kept selected, if it was
 previously selected.

    selection - A GtkTreeSelection.
    type      - The selection mode 
"
  ;; void gtk_tree_selection_set_mode (GtkTreeSelection* selection,
  ;;                                   GtkSelectionMode type);
  (if (or (null tree-selection) (cffi:null-pointer-p tree-selection))
      (error "CFFI/GTK-TREE-SELECTION-SET-MODE invoked with null TREE-SELECTION")
      (cffi:foreign-funcall "gtk_tree_selection_set_mode"
			    GtkTreeSelection* tree-selection
			    gint              type  ; enum
			    :void))
  tree-selection)


(defun cffi/gtk-tree-view-get-selection (tree-view)
"
 Gets the GtkTreeSelection associated with tree_view.

    tree_view - A GtkTreeView.

    Returns - A GtkTreeSelection object. 
"
  ;; GtkTreeSelection* gtk_tree_view_get_selection (GtkTreeView* tree_view);
  (if (or (null tree-view) (cffi:null-pointer-p tree-view))
      (error "CFFI/GTK-TREE-VIEW-GET-SELECTION invoked with null TREE-VIEW")
      (let ((selection (cffi:foreign-funcall "gtk_tree_view_get_selection"
					     GtkTreeView* tree-view
					     GtkTreeSelection*)))
	(pointer-or-nil selection))))


(defun cffi/gtk-tree-view-set-headers-visible (tree-view visible?)
"
 Sets the visibility state of the headers.

 tree_view       - A GtkTreeView.
 headers_visible - TRUE if the headers are visible 
"
  ;; void gtk_tree_view_set_headers_visible (GtkTreeView* tree_view,
  ;;                                         gboolean headers_visible);
  (if (or (null tree-view) (cffi:null-pointer-p tree-view))
      (error "CFFI/GTK-TREE-VIEW-SET-HEADERS-VISIBLE invoked with null TREE-VIEW")
      (cffi:foreign-funcall "gtk_tree_view_set_headers_visible"
			    GtkTreeView* tree-view
			    gboolean     visible?
			    :void))
  tree-view)


(defun cffi/gtk-cell-renderer-text-new ()
"
 Creates a new GtkCellRendererText. Adjust how text is drawn using object
 properties. Object properties can be set globally (with g_object_set()).
 Also, with GtkTreeViewColumn, you can bind a property to a value in a
 GtkTreeModel. For example, you can bind the 'text' property on the cell
 renderer to a string value in the model, thus rendering a different
 string in each row of the GtkTreeView

    Returns - the new cell renderer 

Sinks initial floating reference
"
  ;; GtkCellRenderer* gtk_cell_renderer_text_new(void);
  (let ((renderer (cffi:foreign-funcall "gtk_cell_renderer_text_new" gpointer)))
    (sink-floating-ref renderer)
    (pointer-or-nil renderer)))


(defun cffi/gtk-tree-view-column-new ()
"
Sinks initial floating reference
"
  ;; GtkTreeViewColumn*  gtk_tree_view_column_new (void);
  (let((column (cffi:foreign-funcall "gtk_tree_view_column_new" gpointer)))
    (sink-floating-ref column)
    (pointer-or-nil column)))


(defun cffi/gtk-tree-view-column-pack-start (column renderer expand?)
  ;; void gtk_tree_view_column_pack_start (GtkTreeViewColumn *tree_column,
  ;;                                       GtkCellRenderer *cell,
  ;;                                       gboolean expand);
  (if (or (null column) (cffi:null-pointer-p column))
      (error "CFFI/GTK-TREE-VIEW-COLUMN-PACK-START invoked with null COLUMN")
      (if (or (null renderer) (cffi:null-pointer-p renderer))
	  (error "CFFI/GTK-TREE-VIEW-COLUMN-PACK-START invoked with null RENDERER")
	  (cffi:foreign-funcall "gtk_tree_view_column_pack_start"
				gpointer column
				gpointer renderer
				gboolean expand?
				:void)))
  column)


(defun cffi/gtk-tree-view-column-add-attribute (tree-column renderer attribute column)
  ;; void gtk_tree_view_column_add_attribute (GtkTreeViewColumn *tree_column,
  ;;                                          GtkCellRenderer *cell_renderer,
  ;;                                          const gchar *attribute,
  ;;                                          gint column);
  (cond ((or (null tree-column) (cffi:null-pointer-p tree-column))
	 (error "CFFI/GTK-TREE-VIEW-COLUMN-ADD-ATTRIBUTE invoked with null TREE-COLUMN"))
	((or (null renderer) (cffi:null-pointer-p renderer))
	 (error "CFFI/GTK-TREE-VIEW-COLUMN-ADD-ATTRIBUTE invoked with null RENDERER"))
	((or (null attribute) (and (cffi:pointerp attribute) (cffi:null-pointer-p attribute)))
	 (error "CFFI/GTK-TREE-VIEW-COLUMN-ADD-ATTRIBUTE invoked with null ATTRIBUTE"))
	(t (cffi:foreign-funcall "gtk_tree_view_column_add_attribute"
				 gpointer tree-column
				 gpointer renderer
				 :string attribute
				 gint column
				 :void)))
  column)


(defun cffi/gtk-tree-view-column-set-title (column title)
"
void gtk_tree_view_column_set_title (GtkTreeViewColumn *self,
                                     gchar* title);
"
  (if (or (null column) (cffi:null-pointer-p column))
      (error "CFFI/GTK-TREE-VIEW-COLUMN-SET-TITLE invoked with null COLUMN")
      (if (or (null title) (and (cffi:pointerp title) (cffi:null-pointer-p title)))
	  (error "CFFI/GTK-TREE-VIEW-COLUMN-SET-TITLE invoked with null TITLE")
	  (cffi:foreign-funcall "gtk_tree_view_column_set_title"
				:pointer column
				:string title
				:void)))
  column)


(defun cffi/gtk-tree-view-column-new-with-attributes (title renderer &rest attributes)
"
 GtkTreeViewColumn* gtk_tree_view_column_new_with_attributes (const gchar *title,
                                                              GtkCellRenderer *cell,
                                                              ...);

 Creates a new GtkTreeViewColumn with a number of default values. This is equivalent to
 calling gtk_tree_view_column_set_title(), gtk_tree_view_column_pack_start(), and
 gtk_tree_view_column_set_attributes() on the newly created GtkTreeViewColumn.

 NOTE: VARARGS METHODS ARE A PITA IN CFFI SO THIS IS DONE THE MANUAL WAY, CALLING
 EACH OF THE METHODS LISTED ABOVE IN TURN.

Sinks initial floating reference
"
  (if (or (null title) (cffi:null-pointer-p title))
      (error "CFFI/GTK-TREE-VIEW-COLUMN-NEW-WITH-ATTRIBUTES invoked with null TITLE")
      (if (or (null renderer) (cffi:null-pointer-p renderer))
	  (error "CFFI/GTK-TREE-VIEW-COLUMN-NEW-WITH-ATTRIBUTES invoked with null RENDERER")
	  (let* ((column (cffi/gtk-tree-view-column-new)))
	    (cffi/gtk-tree-view-column-set-title column title)
	    (cffi/gtk-tree-view-column-pack-start column renderer +false+)
	    (loop for llist = attributes then (cddr llist)
	       for column-attribute = (car llist)
	       for column-index  = (cadr llist)
	       do (cffi/gtk-tree-view-column-add-attribute column renderer column-attribute column-index))
	    (sink-floating-ref column)
	    (pointer-or-nil column)))))


(defun cffi/gtk-widget-set-app-paintable (widget paintable?)
"
 void gtk_widget_set_app_paintable (GtkWidget *widget, gboolean app_paintable);

    widget:	
    app_paintable:
"
  (if (or (null widget) (cffi:null-pointer-p widget))
      (error "CFFI/GTK-WIDGET-SET-APP-PAINTABLE invoked with null WIDGET")
      (cffi:foreign-funcall "gtk_widget_set_app_paintable"
			    :pointer widget
			    :boolean paintable?
			    :void))
  widget)


(defun cffi/gtk-tree-view-append-column (widget column)
"
 Appends column to the list of columns. If tree_view has 'fixed_height' mode
 enabled, then column must have its 'sizing' property set to be
 GTK_TREE_VIEW_COLUMN_FIXED.

    tree_view - A GtkTreeView.
    column    - The GtkTreeViewColumn to add.

    Returns   - The number of columns in tree_view after appending. 
"
  ;; gint gtk_tree_view_append_column (GtkTreeView *tree_view, GtkTreeViewColumn *column);
  (if (or (null widget) (cffi:null-pointer-p widget))
      (error "CFFI/GTK-TREE-VIEW-APPEND-COLUMN invoked with null WIDGET")
      (if (or (null column) (cffi:null-pointer-p column))
	  (error "CFFI/GTK-TREE-VIEW-APPEND-COLUMN invoked with null COLUMN")
	  (let ((column-count (cffi:foreign-funcall "gtk_tree_view_append_column"
						    gpointer widget
						    gpointer column
						    gint)))
	    column-count))))


(defun cffi/gtk-tree-path-get-indices (path)
"
 Returns the current indices of path. This is an array of integers, each
 representing a node in a tree. This value should not be freed.

    path    - A GtkTreePath.
    Returns - The current indices, or NULL. 
"
  ;; gint* gtk_tree_path_get_indices (GtkTreePath *path);
  (if (or (null path) (cffi:null-pointer-p path))
      (error "CFFI/GTK-TREE-PATH-GET-INDICES invoked with null PATH")
      (let ((indices (cffi:foreign-funcall "gtk_tree_path_get_indices"
					   :pointer path
					   :pointer)))
	(pointer-or-nil indices))))


(defun cffi/gtk-tree-selection-get-selected-rows (selection model)
"
 Creates a list of path of all selected rows. Additionally, if you are
 planning on modifying the model after calling this function, you may want
 to convert the returned list into a list of GtkTreeRowReferences. To do
 this, you can use gtk_tree_row_reference_new().

 To free the return value, use:

 g_list_foreach (list, gtk_tree_path_free, NULL);
 g_list_free (list);

    selection - A GtkTreeSelection.
    model     - A pointer to set to the GtkTreeModel, or NULL.

    Returns - A GList containing a GtkTreePath for each selected row. 
"
  ;; GList* gtk_tree_selection_get_selected_rows (GtkTreeSelection *selection,
  ;;                                              GtkTreeModel **model);
  (if (or (null selection) (cffi:null-pointer-p selection))
      (error "CFFI/GTK-TREE-SELECTION-GET-SELECTED-ROWS invoked with null SELECTION")
      (let* ((model (or model (cffi:null-pointer)))
	     (glist (cffi:foreign-funcall "gtk_tree_selection_get_selected_rows"
					  :pointer selection
					  :pointer model
					  :pointer)))
	(pointer-or-nil glist))))


(defun cffi/gtk-tree-selection-unselect-all (tree-selection)
"
 Unselects all the nodes.

    selection - A GtkTreeSelection.
"
  ;; void gtk_tree_selection_unselect_all (GtkTreeSelection *selection);
  (if (or (null tree-selection) (cffi:null-pointer-p tree-selection))
      (error "CFFI/GTK-TREE-SELECTION-UNSELECT-ALL invoked with null TREE-SELECTION")
      (cffi:foreign-funcall "gtk_tree_selection_unselect_all"
			    :pointer tree-selection
			    :void))
  tree-selection)


(defun cffi/gtk-tree-selection-select-path (tree-selection tree-path)
"
 Select the row at path.

    selection - A GtkTreeSelection.
    path      - The GtkTreePath to be selected.
"
  ;; void gtk_tree_selection_select_path (GtkTreeSelection *selection, GtkTreePath *path);
  (if (or (null tree-selection) (cffi:null-pointer-p tree-selection))
      (error "CFFI/GTK-TREE-SELECTION-SELECT-PATH invoked with null TREE-SELECTION")
      (if (or (null tree-path) (cffi:null-pointer-p tree-path))
	  (error "CFFI/GTK-TREE-SELECTION-SELECT-PATH invoked with null TREE-PATH")
	  (cffi:foreign-funcall "gtk_tree_selection_select_path"
				:pointer tree-selection
				:pointer tree-path
				:void)))
  tree-selection)


(defun cffi/gtk-tree-path-new-from-string (path)
"
 GtkTreePath* gtk_tree_path_new_from_string (const gchar *path);

 Creates a new GtkTreePath initialized to path. path is expected to
 be a colon separated list of numbers. For example, the string '10:4:0'
 would create a path of depth 3 pointing to the 11th child of the root
 node, the 5th child of that 11th child, and the 1st child of that
 5th child. If an invalid path string is passed in, NULL is returned.

    path : The string representation of a path.
    Returns : A newly-created GtkTreePath, or NULL

fixme: should this have a sunk reference? It's not a widget...
"
  (if (or (null path) (and (cffi:pointerp path) (cffi:null-pointer-p path)))
      (error "CFFI/GTK-TREE-PATH-NEW-FROM-STRING invoked with null PATH")
      (let ((tree-path (cffi:foreign-funcall "gtk_tree_path_new_from_string"
					     :string path
					     :pointer)))
	(pointer-or-nil tree-path))))


;;; ------ GtkComboBox

(defun cffi/gtk-combo-box (arg)
  arg)


(defun cffi/gtk-combo-box-new ()
"
 Creates a new empty GtkComboBox.

    Returns - A new GtkComboBox.

Sinks initial floating reference
"
  ;; GtkWidget * gtk_combo_box_new (void);
  (let ((box (cffi:foreign-funcall "gtk_combo_box_new" :pointer)))
    (sink-floating-ref box)
    (pointer-or-nil box)))


;;; FIXME: This method seems not to exist in GTK!
#-(and)
(defun gtk-combo-box-set-button-sensitivity (widget sensitivity)
"
Sets whether the dropdown button of the combo box should be always sensitive
 (GTK_SENSITIVITY_ON), never sensitive (GTK_SENSITIVITY_OFF) or only if there
is at least one item to display (GTK_SENSITIVITY_AUTO).

    combo_box   - a GtkComboBox
    sensitivity - specify the sensitivity of the dropdown button
"
  ;; void gtk_combo_box_set_button_sensitivity (GtkComboBox *combo_box,
  ;;                                         GtkSensitivityType sensitivity);
  (cffi:foreign-funcall "gtk_combo_box_set_button_sensitivity"
			:pointer widget
			:int     sensitivity
			:void))


(defun cffi/gtk-combo-box-set-model (widget model)
"
 Sets the model used by combo_box to be model. Will unset a previously set model
 (if applicable). If model is NULL, then it will unset the model.

 Note that this function does not clear the cell renderers, you have to call
 gtk_cell_layout_clear() yourself if you need to set up different cell
 renderers for the new model.

    combo_box - A GtkComboBox
    model     - A GtkTreeModel. allow-none.
"
  ;; void gtk_combo_box_set_model (GtkComboBox *combo_box,
  ;;                               GtkTreeModel *model);
  (if (or (null widget) (cffi:null-pointer-p widget))
      (error "CFFI/GTK-COMBO-BOX-SET-MODEL invoked with null WIDGET")
      (let* ((model (or model (cffi:null-pointer))))
	(cffi:foreign-funcall "gtk_combo_box_set_model"
			      :pointer widget
			      :pointer model
			      :void)))
  widget)


(defun cffi/gtk-combo-box-set-active (widget index)
"
 Sets the active item of combo_box to be the item at index.

    combo_box - A GtkComboBox
    index     - An index in the model passed during construction, or -1 to have no active item
"
  ;; void gtk_combo_box_set_active (GtkComboBox *combo_box, gint index_);
  (if (or (null widget) (cffi:null-pointer-p widget))
      (error "CFFI/GTK-COMBO-BOX-SET-ACTIVE invoked with null WIDGET")
      (cffi:foreign-funcall "gtk_combo_box_set_active"
			    :pointer widget
			    gint index
			    :void))
  widget)


(defun cffi/gtk-combo-box-get-active (widget)
"
Returns the index of the currently active item, or -1 if there's no active item.
If the model is a non-flat treemodel, and the active item is not an immediate
child of the root of the tree, this function returns
gtk_tree_path_get_indices (path)[0], where path is the GtkTreePath of the
active item.

    combo_box - A GtkComboBox
    Returns   - An integer which is the index of the currently active item,
                or -1 if there's no active item.
"
  ;; gint gtk_combo_box_get_active (GtkComboBox *combo_box);
  (if (or (null widget) (cffi:null-pointer-p widget))
      (error "CFFI/GTK-COMBO-BOX-GET-ACTIVE invoked with null WIDGET")
      (cffi:foreign-funcall "gtk_combo_box_get_active"
			    :pointer widget
			    gint)))


;;; ------ GtkComboBoxEntry

(defun cffi/gtk-combo-box-entry (object)
  object)


(defun cffi/gtk-combo-box-entry-new ()
"
Creates a new GtkComboBoxEntry which has a GtkEntry as child. After
construction, you should set a model using gtk_combo_box_set_model() and a text
column using gtk_combo_box_entry_set_text_column().

    Returns - A new GtkComboBoxEntry.

Sinks initial floating reference
"
  ;; GtkWidget * gtk_combo_box_entry_new (void);
  (let ((box (cffi:foreign-funcall "gtk_combo_box_entry_new" :pointer)))
    (sink-floating-ref box)
    (pointer-or-nil box)))


(defun cffi/gtk-combo-box-entry-set-text-column (widget column)
"
Sets the model column which entry_box should use to get strings from to be text_column.

    entry_box   - A GtkComboBoxEntry.
    text_column - A column in model to get the strings from.
"
  ;; void gtk_combo_box_entry_set_text_column (GtkComboBoxEntry *entry_box,
  ;;                                           gint text_column);
  (if (or (null widget) (cffi:null-pointer-p widget))
      (error "CFFI/GTK-COMBO-BOX-ENTRY-SET-TEXT-COLUMN invoked with null WIDGET")
      (cffi:foreign-funcall "gtk_combo_box_entry_set_text_column"
			    :pointer widget
			    gint column
			    :void))
  widget)


;;; ------ GtkCellLayout

(defun cffi/gtk-cell-layout-pack-start (layout renderer expand?)
"
 Packs the cell into the beginning of cell_layout. If expand is FALSE, then the
 cell is allocated no more space than it needs. Any unused space is divided
 evenly between cells for which expand is TRUE.

 Note that reusing the same cell renderer is not supported.

    cell_layout : A GtkCellLayout.
    cell : A GtkCellRenderer.
    expand : TRUE if cell is to be given extra space allocated to cell_layout.
"
  ;; void gtk_cell_layout_pack_start (GtkCellLayout *cell_layout,
  ;;                                  GtkCellRenderer *cell,
  ;;                                  gboolean expand);
  (if (or (null layout) (cffi:null-pointer-p layout))
      (error "CFFI/GTK-CELL-LAYOUT-PACK-START invoked with null LAYOUT")
      (if (or (null renderer) (cffi:null-pointer-p renderer))
	  (error "CFFI/GTK-CELL-LAYOUT-PACK-START invoked with null RENDERER")
	  (cffi:foreign-funcall "gtk_cell_layout_pack_start"
				:pointer layout
				:pointer renderer
				gboolean expand?
				:void)))
  layout)


(defun cffi/gtk-cell-layout-add-attribute (layout renderer attribute column)
"
 Adds an attribute mapping to the list in cell_layout. The column is the column
 of the model to get a value from, and the attribute is the parameter on cell to
 be set from the value. So for example if column 2 of the model contains
 strings, you could have the \"text\" attribute of a GtkCellRendererText get its
 values from column 2.

    cell_layout - A GtkCellLayout.
    cell        - A GtkCellRenderer.
    attribute   - An attribute on the renderer.
    column      - The column position on the model to get the attribute from.
"
  ;; void gtk_cell_layout_add_attribute (GtkCellLayout *cell_layout,
  ;;                                     GtkCellRenderer *cell,
  ;;                                     const gchar *attribute,
  ;;                                     gint column);
  (cond ((or (null layout) (cffi:null-pointer-p layout))
	 (error "CFFI/GTK-CELL-LAYOUT-ADD-ATTRIBUTE invoked with null LAYOUT"))
	((or (null renderer) (cffi:null-pointer-p renderer))
	 (error "CFFI/GTK-CELL-LAYOUT-ADD-ATTRIBUTE invoked with null RENDERER"))
	((or (null attribute) (and (cffi:pointerp attribute) (cffi:null-pointer-p attribute)))
	 (error "CFFI/GTK-CELL-LAYOUT-ADD-ATTRIBUTE invoked with null ATTRIBUTE"))
	(t (cffi:foreign-funcall "gtk_cell_layout_add_attribute"
				 :pointer layout
				 :pointer renderer
				 :string attribute
				 gint column
				 :void))))


;;; ------ HScale / VScale

(defun cffi/gtk-hscale (arg)
  arg)


(defun cffi/gtk-hscale-new (adjustment)
"
Sinks initial floating reference
"

;;  (gtk-debug "gtk-hscale-new ~a" adjustment)
  ;; GtkWidget* gtk_hscale_new (GtkAdjustment* adjustment);
  (if (or (null adjustment) (cffi:null-pointer-p adjustment))
      (error "CFFI/GTK-HSCALE-NEW invoked with null ADJUSTMENT")
      (let ((hscale (cffi:foreign-funcall "gtk_hscale_new"
					  GtkAdjustment* adjustment
					  GtkWidget*)))
	(sink-floating-ref hscale)
	(pointer-or-nil hscale))))


(defun cffi/gtk-hscale-new-with-range (min max step)
"
 Creates a new horizontal scale widget that lets the user input a number between
 min and max (including min and max) with the increment step. step must be
 nonzero; it's the distance the slider moves when using the arrow keys to adjust
 the scale value.

 Note that the way in which the precision is derived works best if step is a
 power of ten. If the resulting precision is not suitable for your needs, use
 gtk_scale_set_digits() to correct it.

    min     - minimum value
    max     - maximum value
    step    - step increment (tick size) used with keyboard shortcuts
    Returns - a new GtkHScale

Sinks initial floating reference
"
  ;; GtkWidget* gtk_hscale_new_with_range (gdouble min, gdouble max, gdouble step);
  (let ((hscale (cffi:foreign-funcall "gtk_hscale_new_with_range"
				      gdouble min
				      gdouble max
				      gdouble step
				      :pointer)))
    (sink-floating-ref hscale)
    (pointer-or-nil hscale)))


(defun cffi/gtk-vscale (arg)
  arg)


(defun cffi/gtk-vscale-new (adjustment)
"
Sinks initial floating reference
"
  ;; GtkWidget* gtk_vscale_new (GtkAdjustment* adjustment);
  (if (or (null adjustment) (cffi:null-pointer-p adjustment))
      (error "CFFI/GTK-VSCALE-NEW invoked with null ADJUSTMENT")
      (let ((vscale (cffi:foreign-funcall "gtk_vscale_new"
					  GtkAdjustment* adjustment
					  GtkWidget*)))
	(sink-floating-ref vscale)
	(pointer-or-nil vscale))))


(defun cffi/gtk-vscale-new-with-range (min max step)
"
 Creates a new vertical scale widget that lets the user input a number between
 min and max (including min and max) with the increment step. step must be
 nonzero; it's the distance the slider moves when using the arrow keys to adjust
 the scale value.

 Note that the way in which the precision is derived works best if step is a
 power of ten. If the resulting precision is not suitable for your needs, use
 gtk_scale_set_digits() to correct it.

    min     - minimum value
    max     - maximum value
    step    - step increment (tick size) used with keyboard shortcuts
    Returns - a new GtkHScale

Sinks initial floating reference
"
  ;; GtkWidget* gtk_vscale_new_with_range (gdouble min, gdouble max, gdouble step);
  (let ((vscale (cffi:foreign-funcall "gtk_vscale_new_with_range"
				      gdouble min
				      gdouble max
				      gdouble step
				      :pointer)))
    (sink-floating-ref vscale)
    (pointer-or-nil vscale)))


;;; ------ Scrollbars

(defun cffi/gtk-vscrollbar (arg)
  arg)


(defun cffi/gtk-vscrollbar-new (adjustment)
"
Sinks initial floating reference
"
  ;; GtkWidget* gtk_vscrollbar_new (GtkAdjustment* adjustment);
  (if (or (null adjustment) (cffi:null-pointer-p adjustment))
      (error "CFFI/GTK-VSCROLLBAR-NEW invoked with null ADJUSTMENT")
      (let ((vscroll (cffi:foreign-funcall "gtk_vscrollbar_new"
					   GtkAdjustment* adjustment
					   GtkWidget*)))
	(sink-floating-ref vscroll)
	(pointer-or-nil vscroll))))


(defun cffi/gtk-hscrollbar (arg)
  arg)


(defun cffi/gtk-hscrollbar-new (adjustment)
"
Sinks initial floating reference
"
  ;; GtkWidget* gtk_hscrollbar_new (GtkAdjustment* adjustment);
  (if (or (null adjustment) (cffi:null-pointer-p adjustment))
      (error "CFFI/GTK-HSCROLLBAR-NEW invoked with null ADJUSTMENT")
      (let ((hscroll (cffi:foreign-funcall "gtk_hscrollbar_new"
					   GtkAdjustment* adjustment
					   GtkWidget*)))
	(sink-floating-ref hscroll)
	(pointer-or-nil hscroll))))


;;; ------ GtkRange

(defun cffi/gtk-range-get-adjustment (range-ptr)
  ;; GtkAdjustment* gtk_range_get_adjustment (GtkRange* range);
  (if (or (null range-ptr) (cffi:null-pointer-p range-ptr))
      (error "CFFI/GTK-RANGE-GET-ADJUSTMENT invoked with null RANGE-PTR")
      (let ((adjustment (cffi:foreign-funcall "gtk_range_get_adjustment"
					      GtkRange* range-ptr
					      GtkAdjustment*)))
	(pointer-or-nil adjustment))))


(defun cffi/gtk-range-set-range (widget min max)
"
Sets the allowable values in the GtkRange, and clamps the range value to be
between min and max. (If the range has a non-zero page size, it is clamped
between min and max - page-size.)

    range - a GtkRange
    min   - minimum range value
    max   - maximum range value
"
  ;; void gtk_range_set_range (GtkRange *range, gdouble min, gdouble max);
  (if (or (null widget) (cffi:null-pointer-p widget))
      (error "CFFI/GTK-RANGE-SET-RANGE invoked with null WIDGET")
      (cffi:foreign-funcall "gtk_range_set_range"
			    :pointer widget
			    gdouble min
			    gdouble max
			    :void))
  widget)


(defun cffi/gtk-range-set-increments (widget step page)
"
Sets the step and page sizes for the range. The step size is used when the user
clicks the GtkScrollbar arrows or moves GtkScale via arrow keys. The page size
is used for example when moving via Page Up or Page Down keys.

    range - a GtkRange
    step  - step size
    page  - page size
"
  ;; void gtk_range_set_increments (GtkRange *range, gdouble step, gdouble page); 
  (if (or (null widget) (cffi:null-pointer-p widget))
      (error "CFFI/GTK-RANGE-SET-INCREMENTS invoked with null WIDGET")
      (cffi:foreign-funcall "gtk_range_set_increments"
			    :pointer widget
			    gdouble step
			    gdouble page
			    :void))
  widget)


(defun cffi/gtk-range-get-value (widget)
"
 Gets the current value of the range.

    range   - a GtkRange
    Returns - current value of the range.
"
  ;; gdouble gtk_range_get_value (GtkRange *range);
  (if (or (null widget) (cffi:null-pointer-p widget))
      (error "CFFI/GTK-RANGE-GET-VALUE invoked with null WIDGET")
      (cffi:foreign-funcall "gtk_range_get_value"
			    :pointer widget
			    gdouble)))


(defun cffi/gtk-range-set-value (widget value)
"
Sets the current value of the range; if the value is outside the minimum or
maximum range values, it will be clamped to fit inside them. The range emits
the \"value-changed\" signal if the value changes.

    range - a GtkRange
    value - new value of the range
"
  ;; void gtk_range_set_value (GtkRange *range, gdouble value);
  (if (or (null widget) (cffi:null-pointer-p widget))
      (error "CFFI/GTK-RANGE-SET-VALUE invoked with null WIDGET")
      (cffi:foreign-funcall "gtk_range_set_value"
			    :pointer widget
			    gdouble value
			    :void))
  widget)


;;; ------ Text widgets

;;; Multi-line text editing.

(defun cffi/gtk-text-view (arg)
  arg)


(defun cffi/gtk-text-view-new ()
"
Sinks initial floating reference
"
  ;; GtkWidget* gtk_text_view_new (void);
  (let ((text-view (cffi:foreign-funcall "gtk_text_view_new" GtkWidget*)))
    (sink-floating-ref text-view)
    (pointer-or-nil text-view)))


(defun cffi/gtk-text-view-set-wrap-mode (view wrap-mode)
  ;; void gtk_text_view_set_wrap_mode (GtkTextView* view,
  ;;                                   GtkWrapMode wrap_mode);
  (if (or (null view) (cffi:null-pointer-p view))
      (error "CFFI/GTK-TEXT-VIEW-SET-WRAP-MODE invoked with null VIEW")
      (cffi:foreign-funcall "gtk_text_view_set_wrap_mode"
			    GtkTextView* view
			    :int wrap-mode
			    :void))
  view)


(defun cffi/gtk-text-view-get-buffer (widget)
"
Returns the GtkTextBuffer being displayed by this text view. The reference
count on the buffer is not incremented; the caller of this function won't own
a new reference.

    text_view - a GtkTextView
    Returns   - a GtkTextBuffer
"
  ;; GtkTextBuffer* gtk_text_view_get_buffer (GtkTextView *text_view);
  (if (or (null widget) (cffi:null-pointer-p widget))
      (error "CFFI/GTK-TEXT-VIEW-GET-BUFFER invoked with null WIDGET")
      (let ((buffer (cffi:foreign-funcall "gtk_text_view_get_buffer"
					  GtkTextView* widget
					  :pointer)))
	(pointer-or-nil buffer))))


;;; GtkTextBuffer

(defun cffi/gtk-text-buffer-get-bounds (buffer start-iter end-iter)
"
 Retrieves the first and last iterators in the buffer, i.e. the entire buffer
 lies within the range [start,end].

    buffer - a GtkTextBuffer
    start  - iterator to initialize with first position in the buffer
    end    - iterator to initialize with the end iterator
"
  ;; void gtk_text_buffer_get_bounds (GtkTextBuffer *buffer,
  ;;                                  GtkTextIter *start,
  ;;                                  GtkTextIter *end);
  (cond ((or (null buffer) (cffi:null-pointer-p buffer))
	 (error "CFFI/GTK-TEXT-BUFFER-GET-BOUNDS invoked with null BUFFER"))
	((or (null start-iter) (cffi:null-pointer-p start-iter))
	 (error "CFFI/GTK-TEXT-BUFFER-GET-BOUNDS invoked with null START-ITER"))
	((or (null end-iter) (cffi:null-pointer-p end-iter))
	 (error "CFFI/GTK-TEXT-BUFFER-GET-BOUNDS invoked with null END-ITER"))
	(t (cffi:foreign-funcall "gtk_text_buffer_get_bounds"
				 :pointer buffer
				 :pointer start-iter
				 :pointer end-iter
				 :void)))
  buffer)


(defun cffi/gtk-text-buffer-get-text (buffer start-iter end-iter include-hidden-chars?)
"
 Returns the text in the range [start,end]. Excludes undisplayed text (text
 marked with tags that set the invisibility attribute) if include_hidden_chars
 is FALSE. Does not include characters representing embedded images, so byte
 and character indexes into the returned string do not correspond to byte and
 character indexes into the buffer. Contrast with gtk_text_buffer_get_slice().

    buffer               - a GtkTextBuffer
    start                - start of a range
    end                  - end of a range
    include_hidden_chars - whether to include invisible text
    Returns              - an allocated UTF-8 string
"
  ;; gchar* gtk_text_buffer_get_text (GtkTextBuffer *buffer,
  ;;                                  const GtkTextIter *start,
  ;;                                  const GtkTextIter *end,
  ;;                                  gboolean include_hidden_chars);
  (cond ((or (null buffer) (cffi:null-pointer-p buffer))
	 (error "CFFI/GTK-TEXT-BUFFER-GET-TEXT invoked with null BUFFER"))
	((or (null start-iter) (cffi:null-pointer-p start-iter))
	 (error "CFFI/GTK-TEXT-BUFFER-GET-TEXT invoked with null START-ITER"))
	((or (null end-iter) (cffi:null-pointer-p end-iter))
	 (error "CFFI/GTK-TEXT-BUFFER-GET-TEXT invoked with null END-ITER"))
	(t (let ((gchar (cffi:foreign-funcall "gtk_text_buffer_get_text"
					      :pointer buffer
					      :pointer start-iter
					      :pointer end-iter
					      gboolean include-hidden-chars?
					      :string)))
	     gchar))))


(defun cffi/gtk-text-buffer-set-text (buffer text len)
"
Deletes current contents of buffer, and inserts text instead. If len is -1,
text must be nul-terminated. text must be valid UTF-8.

    buffer - a GtkTextBuffer
    text   - UTF-8 text to insert
    len    - length of text in bytes
"
  ;; void gtk_text_buffer_set_text (GtkTextBuffer *buffer,
  ;;                                const gchar *text,
  ;;                                gint len);
  (if (or (null buffer) (cffi:null-pointer-p buffer))
      (error "CFFI/GTK-TEXT-BUFFER-SET-TEXT invoked with null BUFFER")
      (if (or (null text) (and (cffi:pointerp text) (cffi:null-pointer-p text)))
	  (error "CFFI/GTK-TEXT-BUFFER-SET-TEXT invoked with null TEXT")
	  (cffi:foreign-funcall "gtk_text_buffer_set_text"
				:pointer buffer
				:string text
				gint len
				:void)))
  buffer)


;;; GtkEditable

(defun cffi/gtk-editable (arg)
  arg)


(defun cffi/gtk-editable-delete-text (editable start-pos end-pos)
  ;; void gtk_editable_delete_text (GtkEditable* editable,
  ;;                                gint start_pos, gint end_pos);
  (if (or (null editable) (cffi:null-pointer-p editable))
      (error "CFFI/GTK-EDITABLE-DELETE-TEXT invoked with null EDITABLE")
      (cffi:foreign-funcall "gtk_editable_delete_text"
			    GtkEditable* editable
			    gint start-pos
			    gint end-pos
			    :void))
  editable)


(defun cffi/gtk-editable-get-chars (editable start-pos end-pos)
  ;; gchar* gtk_editable_get_chars (GtkEditable* editable,
  ;;                                gint start_pos, gint end_pos);
  (if (or (null editable) (cffi:null-pointer-p editable))
      (error "CFFI/GTK-EDITABLE-GET-CHARS invoked with null EDITABLE")
      (cffi:foreign-funcall "gtk_editable_get_chars"
			    GtkEditable* editable
			    gint start-pos
			    gint end-pos
			    :string)))


(defun cffi/gtk-editable-get-position (editable)
  ;; gint gtk_editable_get_position (GtkEditable* editable);
  (if (or (null editable) (cffi:null-pointer-p editable))
      (error "CFFI/GTK-EDITABLE-GET-POSITION invoked with null EDITABLE")
      (let ((posn (cffi:foreign-funcall "gtk_editable_get_position"
					GtkEditable* editable
					gint)))
	posn)))


(defun cffi/gtk-editable-set-position (editable posn)
  ;; void gtk_editable_set_position (GtkEditable* editable, gint position);
  (if (or (null editable) (cffi:null-pointer-p editable))
      (error "CFFI/GTK-EDITABLE-SET-POSITION invoked with null EDITABLE")
      (cffi:foreign-funcall "gtk_editable_set_position"
			    GtkEditable* editable
			    gint posn
			    :void))
  editable)


(defun cffi/gtk-editable-insert-text (editable new-text new-text-length position)
  ;; void gtk_editable_insert_text (GtkEditable* editable,
  ;;                                const gchar* new-text,
  ;;                                gint new-text-length,
  ;;                                gint* position);
  (cond ((or (null editable) (cffi:null-pointer-p editable))
	 (error "CFFI/GTK-EDITABLE-INSERT-TEXT invoked with null EDITABLE"))
	((or (null new-text) (and (cffi:pointerp new-text) (cffi:null-pointer-p new-text)))
	 (error "CFFI/GTK-EDITABLE-INSERT-TEXT invoked with null NEW-TEXT"))
	((or (null position) (cffi:null-pointer-p position))
	 (error "CFFI/GTK-EDITABLE-INSERT-TEXT invoked with null POSITION"))
	(t (cffi:foreign-funcall "gtk_editable_insert_text"
				 GtkEditable* editable
				 :string new-text
				 gint new-text-length
				 (:pointer gint) position
				 :void)))
  position)


(defun cffi/gtk-editable-select-region (editable start-pos end-pos)
  ;; void gtk_editable_select_region (GtkEditable* editable,
  ;;                                  gint start_pos, gint end_pos);
  (if (or (null editable) (cffi:null-pointer-p editable))
      (error "CFFI/GTK-EDITABLE-SELECT-REGION invoked with null EDITABLE")
      (cffi:foreign-funcall "gtk_editable_select_region"
			    GtkEditable* editable
			    gint start-pos gint end-pos
			    :void))
  editable)


;;; GtkEntry

(defun cffi/gtk-entry (arg)
  arg)


(defun cffi/gtk-entry-new ()
"
Sinks initial floating reference
"
  ;; GtkWidget* gtk_entry_new (void);
  (let ((entry (cffi:foreign-funcall "gtk_entry_new" GtkWidget*)))
    (sink-floating-ref entry)
    (pointer-or-nil entry)))


(defun cffi/gtk-entry-new-with-max-length (max)
"
Sinks initial floating reference
"
  ;; GtkWidget* gtk_entry_new_with_max_length (gint max);
  (let ((entry (cffi:foreign-funcall "gtk_entry_new_with_max_length" gint max GtkWidget*)))
    (sink-floating-ref entry)
    (pointer-or-nil entry)))


(defun cffi/gtk-entry-set-text (entry text)
  ;; void gtk_entry_set_text (GtkEntry* entry, const gchar* text);
  (if (or (null entry) (cffi:null-pointer-p entry))
      (error "CFFI/GTK-ENTRY-SET-TEXT invoked with null ENTRY")
      (if (or (null text) (and (cffi:pointerp text) (cffi:null-pointer-p text)))
	  (error "CFFI/GTK-ENTRY-SET-TEXT invoked with null TEXT")
	  (cffi:foreign-funcall "gtk_entry_set_text"
				GtkEntry* entry
				:string text
				:void)))
  entry)


(defun cffi/gtk-entry-set-visibility (entry visible?)
  ;; void gtk_entry_set_visibility (GtkEntry* entry, gboolean visible);
  (if (or (null entry) (cffi:null-pointer-p entry))
      (error "CFFI/GTK-ENTRY-SET-VISIBILITY invoked with null ENTRY")
      (cffi:foreign-funcall "gtk_entry_set_visibility"
			    GtkEntry* entry
			    gboolean visible?
			    :void))
  entry)


