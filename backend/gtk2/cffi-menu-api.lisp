;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GTK2-DUIM -*-
(in-package #:gtk2-duim)

;;; Not just menus. Also accelerators!

;;; ------ Menus

;;; Menu bars

(defun cffi/gtk-menu-bar (arg)
  arg)


(defun cffi/gtk-menu-bar-new ()
"
Sinks initial floating reference
"
  ;; GtkWidget* gtk_menu_bar_new (void);
  (let ((gtk-menubar (cffi:foreign-funcall "gtk_menu_bar_new" GtkWidget*)))
    (sink-floating-ref gtk-menubar)
    (pointer-or-nil gtk-menubar)))


;;; fixme: use gtk_menu_shell_append
#-(and)
(defun cffi/gtk-menu-bar-append (menu-bar child)
  ;; void gtk_menu_shell_append (GtkMenuShell* menu_shell, GtkWidget* child);
  (if (or (null menu-bar) (cffi:null-pointer-p menu-bar))
      (error "CFFI/GTK-MENU-BAR-APPEND invoked with null MENU-BAR")
      (if (or (null child) (cffi:null-pointer-p child))
	  (error "CFFI/GTK-MENU-BAR-APPEND invoked with null CHILD")
	  (cffi:foreign-funcall "gtk_menu_shell_append"
				GtkMenuShell* menu-bar
				GtkWidget* child
				:void)))
  menu-bar)


;;; menus

(defun cffi/gtk-menu (arg)
  arg)

(defun cffi/gtk-menu-new ()
"
Sinks initial floating reference
"
  ;; GtkWidget* gtk_menu_new (void);
  (let ((gtk-menu (cffi:foreign-funcall "gtk_menu_new" GtkWidget*)))
    (sink-floating-ref gtk-menu)
    (pointer-or-nil gtk-menu)))


(defun cffi/gtk-menu-shell-append (menu child)
  ;; void gtk_menu_shell_append (GtkMenuShell* menu_shell, GtkWidget* child);
  (if (or (null menu) (cffi:null-pointer-p menu))
      (error "CFFI/GTK-MENU-SHELL-APPEND invoked with null MENU")
      (if (or (null child) (cffi:null-pointer-p child))
	  (error "CFFI/GTK-MENU-SHELL-APPEND invoked with null CHILD")
	  (cffi:foreign-funcall "gtk_menu_shell_append"
				GtkMenuShell* menu
				GtkWidget* child
				:void)))
  menu)


;;; popup menu support

(defun cffi/gtk-menu-popup (menu parent-menu-shell parent-menu-item func data button activate-time)
"
 Displays a menu and makes it available for selection. Applications can use this
 function to display context-sensitive menus, and will typically supply NULL for
 the parent_menu_shell, parent_menu_item, func and data parameters. The default
 menu positioning function will position the menu at the current mouse cursor
 position.

 The button parameter should be the mouse button pressed to initiate the menu
 popup. If the menu popup was initiated by something other than a mouse button
 press, such as a mouse button release or a keypress, button should be 0.

 The activate_time parameter is used to conflict-resolve initiation of
 concurrent requests for mouse/keyboard grab requests. To function properly,
 this needs to be the time stamp of the user event (such as a mouse click or
 key press) that caused the initiation of the popup. Only if no such event is
 available, gtk_get_current_event_time() can be used instead.

    menu              - a GtkMenu.
    parent_menu_shell - the menu shell containing the triggering menu item,
                        or NULL
    parent_menu_item  - the menu item whose activation triggered the popup,
                        or NULL
    func              - a user supplied function used to position the menu,
                        or NULL
    data              - user supplied data to be passed to func.
    button            - the mouse button which was pressed to initiate the
                        event.
    activate_time     - the time at which the activation event occurred.
"
  ;; void gtk_menu_popup (GtkMenu *menu, GtkWidget *parent_menu_shell,
  ;;                      GtkWidget *parent_menu_item,
  ;;                      GtkMenuPositionFunc func,
  ;;                      gpointer data,
  ;;                      guint button,
  ;;                      guint32 activate_time);
  (if (or (null menu) (cffi:null-pointer-p menu))
      (error "CFFI/GTK-MENU-POPUP invoked with null MENU")
      (let* ((parent-menu-shell (or parent-menu-shell (cffi:null-pointer)))
	     (parent-menu-item  (or parent-menu-item  (cffi:null-pointer)))
	     (func              (or func              (cffi:null-pointer)))
	     (data              (or data              (cffi:null-pointer))))
	(cffi:foreign-funcall "gtk_menu_popup"
			      :pointer menu
			      :pointer parent-menu-shell
			      :pointer parent-menu-item
			      :pointer func
			      :pointer data
			      guint button
			      guint32 activate-time
			      :void)))
  menu)


;;; menu items

(defun cffi/gtk-menu-item (arg)
  arg)


(defun cffi/gtk-radio-menu-item (arg)
  arg)


(defun cffi/gtk-check-menu-item (arg)
  arg)


(defun cffi/gtk-menu-item-new ()
"
 Creates a new GtkMenuItem.

    Returns - the newly created GtkMenuItem

Sinks initial floating reference
"
  ;; GtkWidget* gtk_menu_item_new (void);
  (let ((menu-item (cffi:foreign-funcall "gtk_menu_item_new" :pointer)))
    (sink-floating-ref menu-item)
    (pointer-or-nil menu-item)))


(defun cffi/gtk-menu-attach-to-widget (menu attach-widget detacher)
"
 Attaches the menu to the widget and provides a callback function that will be
 invoked when the menu calls gtk_menu_detach() during its destruction.

    menu          - a GtkMenu.
    attach_widget - the GtkWidget that the menu will be attached to.
    detacher      - the user supplied callback function that will be called
                    when the menu calls gtk_menu_detach().
"
  ;; void gtk_menu_attach_to_widget (GtkMenu *menu,
  ;;                                 GtkWidget *attach_widget,
  ;;                                 GtkMenuDetachFunc detacher);
  (cond ((or (null menu) (cffi:null-pointer-p menu))
	 (error "CFFI/GTK-MENU-ATTACH-TO-WIDGET invoked with null MENU"))
	((or (null attach-widget) (cffi:null-pointer-p attach-widget))
	 (error "CFFI/GTK-MENU-ATTACH-TO-WIDGET invoked with null ATTACH-WIDGET"))
	((or (null detacher) (cffi:null-pointer-p detacher))
	 (error "CFFI/GTK-MENU-ATTACH-TO-WIDGET invoked with null DETACHER"))
	(t (cffi:foreign-funcall "gtk_menu_attach_to_widget"
				 :pointer menu
				 :pointer attach-widget
				 :pointer detacher
				 :void)))
  menu)


(defun cffi/gtk-menu-item-new-with-label (label)
"
Sinks initial floating reference
"
  ;; GtkWidget* gtk_menu_item_new_with_label (const gchar* label);
  (if (or (null label) (and (cffi:pointerp label) (cffi:null-pointer-p label)))
      (error "CFFI/GTK-MENU-ITEM-NEW-WITH-LABEL invoked with null LABEL")
      (let ((menu-item (cffi:foreign-funcall "gtk_menu_item_new_with_label"
					     :string label
					     GtkWidget*)))
	(sink-floating-ref menu-item)
	(pointer-or-nil menu-item))))


(defun cffi/gtk-menu-item-new-with-mnemonic (label)
"
Creates a new GtkMenuItem containing a label. The label will be created using
gtk_label_new_with_mnemonic(), so underscores in label indicate the mnemonic
for the menu item.

    label   - The text of the button, with an underscore in front of the
              mnemonic character
    Returns - a new GtkMenuItem

Sinks initial floating reference
"
  ;; GtkWidget* gtk_menu_item_new_with_mnemonic (const gchar *label);
  (if (or (null label) (and (cffi:pointerp label) (cffi:null-pointer-p label)))
      (error "CFFI/GTK-MENU-ITEM-NEW-WITH-MNEMONIC invoked with null LABEL")
      (let ((menu-item (cffi:foreign-funcall "gtk_menu_item_new_with_mnemonic"
					     :string label
					     :pointer)))
	(sink-floating-ref menu-item)
	(pointer-or-nil menu-item))))


(defun cffi/gtk-image-menu-item-new-from-stock (stock-id accel-group)
"
Creates a new GtkImageMenuItem containing the image and text from a
stock item. Some stock ids have preprocessor macros like GTK_STOCK_OK
and GTK_STOCK_APPLY.

If you want this menu item to have changeable accelerators, then pass
in NULL for accel_group. Next call gtk_menu_item_set_accel_path() with
an appropriate path for the menu item, use gtk_stock_lookup() to look
up the standard accelerator for the stock item, and if one is found,
call gtk_accel_map_add_entry() to register it.

    stock_id    - the name of the stock item.
    accel_group - the GtkAccelGroup to add the menu items accelerator
                  to, or NULL.
    Returns     - a new GtkImageMenuItem.

Sinks initial floating reference
"
  ;; GtkWidget* gtk_image_menu_item_new_from_stock  (const gchar *stock_id, GtkAccelGroup *accel_group);
  (if (or (null stock-id) (and (cffi:pointerp stock-id) (cffi:null-pointer-p stock-id)))
      (error "CFFI/GTK-IMAGE-MENU-ITEM-NEW-FROM-STOCK invoked with null STOCK-ID")
      (let* ((accel-group (or accel-group (cffi:null-pointer)))
	     (menu-item (cffi:foreign-funcall "gtk_image_menu_item_new_from_stock"
					      :string stock-id
					      :pointer accel-group
					      :pointer)))
	(sink-floating-ref menu-item)
	(pointer-or-nil menu-item))))


(defun cffi/gtk-radio-menu-item-new-with-label (group label)
"
Sinks initial floating reference
"
  ;; GtkWidget* gtk_radio_menu_item_new_with_label (GSList* group,
  ;;                                                const gchar* label);
  (if (or (null group) (and (cffi:pointerp label) (cffi:null-pointer-p group)))
      (error "CFFI/GTK-RADIO-MENU-ITEM-NEW-WITH-LABEL invoked with null GROUP")
      (if (or (null label) (and (cffi:pointerp label) (cffi:null-pointer-p label)))
	  (error "CFFI/GTK-RADIO-MENU-ITEM-NEW-WITH-LABEL invoked with null LABEL")
	  (let ((menu-item (cffi:foreign-funcall "gtk_radio_menu_item_new_with_label"
						 GSList* group
						 :string label
						 GtkWidget*)))
	    (sink-floating-ref menu-item)
	    (pointer-or-nil menu-item)))))


(defun cffi/gtk-radio-menu-item-new-with-mnemonic (group label)
"
Creates a new GtkRadioMenuItem containing a label. The label will be created
using gtk_label_new_with_mnemonic(), so underscores in label indicate the
mnemonic for the menu item.

    group   - group the radio menu item is inside
    label   - the text of the button, with an underscore in front of the
              mnemonic character
    Returns - a new GtkRadioMenuItem

Sinks initial floating reference
"
  ;; GtkWidget* gtk_radio_menu_item_new_with_mnemonic (GSList *group, const gchar *label);
  (if (or (null label) (and (cffi:pointerp label) (cffi:null-pointer-p label)))
      (error "CFFI/GTK-RADIO-MENU-ITEM-NEW-WITH-MNEMONIC invoked with null LABEL")
      (let* ((group (or group (cffi:null-pointer)))
	     (menu-item (cffi:foreign-funcall "gtk_radio_menu_item_new_with_mnemonic"
					      :pointer group
					      :string label
					      :pointer)))
	(sink-floating-ref menu-item)
	(pointer-or-nil menu-item))))


(defun cffi/gtk-radio-menu-item-get-group (item)
  ;; GSList* gtk_radio_menu_item_get_group (GtkRadioMenuItem* item);
  (if (or (null item) (cffi:null-pointer-p item))
      (error "CFFI/GTK-RADIO-MENU-ITEM-GET-GROUP invoked with null ITEM")
      (let ((group (cffi:foreign-funcall "gtk_radio_menu_item_get_group"
					 GtkRadioMenuItem* item
					 GSList*)))
	(pointer-or-nil group))))


(defun cffi/gtk-check-menu-item-new-with-label (label)
"
Sinks initial floating reference
"
  ;; GtkWidget* gtk_check_menu_item_new_with_label (const gchar* label);
  (if (or (null label) (and (cffi:pointerp label) (cffi:null-pointer-p label)))
      (error "CFFI/GTK-CHECK-MENU-ITEM-NEW-WITH-LABEL invoked with null LABEL")
      (let ((menu-item (cffi:foreign-funcall "gtk_check_menu_item_new_with_label"
					     :string label
					     GtkWidget*)))
	(sink-floating-ref menu-item)
	(pointer-or-nil menu-item))))


(defun cffi/gtk-check-menu-item-new-with-mnemonic (label)
"
 Creates a new GtkCheckMenuItem containing a label. The label will be created
 using gtk_label_new_with_mnemonic(), so underscores in label indicate the
 mnemonic for the menu item.

    label   - The text of the button, with an underscore in front of the
              mnemonic character
    Returns - a new GtkCheckMenuItem

Sinks initial floating reference
"
  ;; GtkWidget* gtk_check_menu_item_new_with_mnemonic (const gchar *label);
  (if (or (null label) (and (cffi:pointerp label) (cffi:null-pointer-p label)))
      (error "CFFI/GTK-CHECK-MENU-ITEM-NEW-WITH-MNEMONIC invoked with null LABEL")
      (let ((menu-item (cffi:foreign-funcall "gtk_check_menu_item_new_with_mnemonic"
					     :string label
					     :pointer)))
	(sink-floating-ref menu-item)
	(pointer-or-nil menu-item))))


(defun cffi/gtk-check-menu-item-set-active (item active?)
  ;; void gtk_check_menu_item_set_active (GtkCheckMenuItem* item,
  ;;                                      gboolean active);
  (if (or (null item) (cffi:null-pointer-p item))
      (error "CFFI/GTK-CHECK-MENU-ITEM-SET-ACTIVE invoked with null ITEM")
      (cffi:foreign-funcall "gtk_check_menu_item_set_active"
			    GtkCheckMenuItem* item
			    gboolean active?
			    :void))
  item)


(defun cffi/gtk-menu-item-right-justify (menu-item)
  ;; void gtk_menu_item_set_right_justified (GtkMenuItem* menu_item,
  ;;                                        gboolean right_justified);
  (if (or (null menu-item) (cffi:null-pointer-p menu-item))
      (error "CFFI/GTK-MENU-ITEM-RIGHT-JUSTIFY invoked with null MENU-ITEM")
      (cffi:foreign-funcall "gtk_menu_item_set_right_justified"
			    GtkMenuItem* menu-item
			    gboolean +true+
			    :void))
  menu-item)


(defun cffi/gtk-menu-item-set-submenu (menu-item submenu)
  ;; void gtk_menu_item_set_submenu (GtkMenuItem* menu_item,
  ;;                                 GtkWidget* submenu);
  (if (or (null menu-item) (cffi:null-pointer-p menu-item))
      (error "CFFI/GTK-MENU-ITEM-SET-SUBMENU invoked with null MENU-ITEM")
      (let ((submenu (or submenu (cffi:null-pointer))))
	(cffi:foreign-funcall "gtk_menu_item_set_submenu"
			      GtkMenuItem* menu-item
			      GtkWidget* submenu
			      :void)))
  menu-item)


(defun cffi/gtk-menu-item-get-submenu (menu-item)
  ;; GtkWidget* gtk_menu_item_get_submenu (GtkMenuItem* menu_item);
  (if (or (null menu-item) (cffi:null-pointer-p menu-item))
      (error "CFFI/GTK-MENU-ITEM-GET-SUBMENU invoked with null MENU-ITEM")
      (let ((submenu (cffi:foreign-funcall "gtk_menu_item_get_submenu"
					   GtkMenuItem* menu-item
					   GtkWidget*)))
	(pointer-or-nil submenu))))


(defun cffi/gtk-recent-chooser-menu-new ()
"
Sinks initial floating reference
"
  ;; GtkWidget* gtk_recent_chooser_menu_new (void);
  (let ((menu (cffi:foreign-funcall "gtk_recent_chooser_menu_new" :pointer)))
    (sink-floating-ref menu)
    (pointer-or-nil menu)))


(defun cffi/gtk-menu-item-set-accel-path (menu-item accel-path)
"
 void gtk_menu_item_set_accel_path (GtkMenuItem *menu_item, const gchar *accel_path);

 Set the accelerator path on menu_item, through which runtime changes of the menu item's
 accelerator caused by the user can be identified and saved to persistant storage (see
 gtk_accel_map_save() on this). To setup a default accelerator for this menu item, call
 gtk_accel_map_add_entry() with the same accel_path. See also gtk_accel_map_add_entry()
 on the specifics of accelerator paths, and gtk_menu_set_accel_path() for a more convenient
 variant of this function.

 This function is basically a convenience wrapper that handles calling
 gtk_widget_set_accel_path() with the appropriate accelerator group for the menu item.

 Note that you do need to set an accelerator on the parent menu with
 gtk_menu_set_accel_group() for this to work.

 Note that accel_path string will be stored in a GQuark. Therefore, if you pass a static
 string, you can save some memory by interning it first with g_intern_static_string().

    menu_item : a valid GtkMenuItem
    accel_path : accelerator path, corresponding to this menu item's functionality, or NULL
                 to unset the current path. [allow-none]
"
  (if (or (null menu-item) (and (cffi:pointerp menu-item) (cffi:null-pointer-p menu-item)))
      (error "CFFI/GTK-MENU-ITEM-SET-ACCEL-PATH invoked with null MENU-ITEM")
      (cffi:foreign-funcall "gtk_menu_item_set_accel_path"
			    :pointer menu-item
			    :string  accel-path
			    :void))
  menu-item)

