;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GTK2-DUIM -*-
(in-package #:gtk2-duim)

;;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;    FOREIGN TYPE DEFINITIONS
;;;
;;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; ========================================================================
;;;
;;;    'Primitive' & pointer types
;;;
;;; See https://developer.gnome.org/glib/stable/glib-Basic-Types.html

(cffi:defctype gint     :int)
(cffi:defctype gint16   :short)
(cffi:defctype guint    :uint)
(cffi:defctype gboolean gint)
(cffi:defctype gdouble  :double)
(cffi:defctype gulong   :ulong)
(cffi:defctype guchar   :uchar)

(cffi:defctype GType    :uchar)

(cffi:defctype gpointer :pointer)
(cffi:defctype gchar*   :pointer)
(cffi:defctype gchar**  :pointer)
(cffi:defctype guint*   :pointer)

(cffi:defctype GObject*         :pointer)
(cffi:defctype GtkObject*       :pointer)
(cffi:defctype GdkEvent*        :pointer)
(cffi:defctype GdkScreen*       :pointer)
(cffi:defctype GdkDisplay*      :pointer)
(cffi:defctype GtkWidget*       :pointer)
(cffi:defctype GtkDialog*       :pointer)
(cffi:defctype GtkContainer*    :pointer)
(cffi:defctype GtkButton*       :pointer)
(cffi:defctype GtkBin*          :pointer)
(cffi:defctype GdkWindow*       :pointer)
(cffi:defctype GtkWindow*       :pointer)
(cffi:defctype GtkWindowGeometryInfo* :pointer)
(cffi:defctype GtkWindowGroup*  :pointer GtkWindowGroup)
(cffi:defctype GtkFixed*        :pointer GtkFixed)
(cffi:defctype GtkResizeMode*   :pointer GtkResizeMode)
(cffi:defctype GdkDrawable*     :pointer GdkDrawable)
(cffi:defctype GdkGC*           :pointer GdkGC)
(cffi:defctype GtkRequisition*  :pointer GtkRequisition)
(cffi:defctype GtkAllocation*   :pointer GtkAllocation)
(cffi:defctype GdkGCValues*     :pointer GdkGCValues)
(cffi:defctype GtkToggleButton* :pointer GtkToggleButton)
(cffi:defctype GtkToolButton*   :pointer GtkToolButton)
(cffi:defctype GtkDrawingArea*  :pointer GtkDrawingArea)
(cffi:defctype GdkPoint*        :pointer GdkPoint)
(cffi:defctype GtkEntry*        :pointer GtkEntry)
(cffi:defctype GtkEditable*     :pointer GtkEditable)
(cffi:defctype GtkTextView*     :pointer GtkTextView)
(cffi:defctype GtkAdjustment*   :pointer GtkAdjustment)
(cffi:defctype GtkMenuItem*     :pointer GtkMenuItem)
(cffi:defctype GtkMenuShell*    :pointer GtkMenuShell)
(cffi:defctype GtkArg*          :pointer GtkArg)
(cffi:defctype GtkCList*        :pointer GtkCList)
(cffi:defctype GtkStockItem*    :pointer GtkStockItem)
(cffi:defctype GtkFileChooser*  :pointer GtkFileChooser)
(cffi:defctype PangoFontDescription* :pointer PangoFontDescription)
(cffi:defctype GtkListStore*    :pointer GtkListStore)
(cffi:defctype GtkTreeView*     :pointer GtkTreeView)
(cffi:defctype GtkTreeIter*     :pointer GtkTreeIter)
(cffi:defctype GtkTreePath*     :pointer GtkTreePath)
(cffi:defctype GtkTreeModel*    :pointer GtkTreeModel)
(cffi:defctype gint*            :pointer)
(cffi:defctype GValue*          :pointer)
(cffi:defctype GType*           :pointer)
(cffi:defctype va_list          :pointer)
(cffi:defctype GtkTreeSelection* :pointer GtkTreeSelection)   ; opaque pointer-to-struct

(cffi:defctype GClosure*           :pointer)  ; function pointer?

(cffi:defctype GtkSignalFunc       :pointer)  ; function pointer
(cffi:defctype GtkDestroyNotify    :pointer)  ; function pointer
(cffi:defctype GtkCallbackMarshall :pointer)  ; function pointer
(cffi:defctype GCallback           :pointer)  ; function pointer
(cffi:defctype GClosureNotify      :pointer)  ; function pointer
(cffi:defctype GtkCListCompareFunc :pointer)  ; ptr to fn

(cffi:defctype GdkDevice*       :pointer)
(cffi:defctype gdouble*         :pointer)
(cffi:defctype GdkRegion*       :pointer)
(cffi:defctype GtkRange*        :pointer)
(cffi:defctype GList*           :pointer)
(cffi:defctype GData*           :pointer)
(cffi:defctype GTypeClass*      :pointer)
(cffi:defctype GtkStyle*        :pointer)
(cffi:defctype GtkCListColumn*  :pointer)
(cffi:defctype GdkCursor*       :pointer)
(cffi:defctype GSList*          :pointer)
(cffi:defctype GdkRectangle*    :pointer)
(cffi:defctype GtkCheckMenuItem* :pointer)
(cffi:defctype GtkRadioMenuItem* :pointer)
(cffi:defctype GtkRadioButton*  :pointer)
(cffi:defctype GdkColormap*     :pointer)
(cffi:defctype GtkColorSelection* :pointer)
(cffi:defctype GdkColor*        :pointer)
(cffi:defctype GdkVisual*       :pointer)
(cffi:defctype GtkFontSelectionDialog* :pointer)

(cffi:defctype gsize            :ulong)
(cffi:defctype GConnectFlags    gint)
(cffi:defctype GdkFill          gint)
(cffi:defctype GdkFunction      gint)
(cffi:defctype GdkSubwindowMode gint)
(cffi:defctype GdkLineStyle     gint)
(cffi:defctype GdkCapStyle      gint)
(cffi:defctype GdkJoinStyle     gint)
(cffi:defctype GtkResizeMode    gint)
(cffi:defctype GdkVisualType    gint)
(cffi:defctype GdkByteOrder     gint)
(cffi:defctype GdkFont*         :pointer)
(cffi:defctype GdkPixmap*       :pointer)
(cffi:defctype guint32          :uint32)
(cffi:defctype guint16          :uint16)
(cffi:defctype guint8           :uint8)
(cffi:defctype gint8            :int8)    ; char?
(cffi:defctype GdkEventType     gint)    ; enum
(cffi:defctype GtkShadowType    gint)    ; enum
(cffi:defctype GtkSelectionMode gint)    ; enum
(cffi:defctype GtkStateType     gint)    ; enum
(cffi:defctype GtkSortType      gint)    ; enum
(cffi:defctype GtkCListDragPos  gint)    ; enum
(cffi:defctype GSignalMatchType gint)    ; enum
(cffi:defctype GdkModifierType  gint)    ; enum
;;; GQuark types map between strings and integers.
(cffi:defctype GQuark           guint32)

;;; ========================================================================
;;;
;;;    Constants derived from enums
;;;

;;; --- GtkFileChooserAction

;;; typedef enum
;;; {
;;;   GTK_FILE_CHOOSER_ACTION_OPEN,
;;;   GTK_FILE_CHOOSER_ACTION_SAVE,
;;;   GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
;;;   GTK_FILE_CHOOSER_ACTION_CREATE_FOLDER
;;; } GtkFileChooserAction;

(defconstant +CFFI/GTK-FILE-CHOOSER-ACTION-OPEN+          0)
(defconstant +CFFI/GTK-FILE-CHOOSER-ACTION-SAVE+          1)
(defconstant +CFFI/GTK-FILE-CHOOSER-ACTION-SELECT-FOLDER+ 2)
(defconstant +CFFI/GTK-FILE-CHOOSER-ACTION-CREATE-FOLDER+ 3)

;;; --- GtkResponseType

;;; typedef enum
;;; {
;;;   /* GTK returns this if a response widget has no response_id,
;;;    * or if the dialog gets programmatically hidden or destroyed.
;;;    */
;;;   GTK_RESPONSE_NONE = -1,
;;;
;;;   /* GTK won't return these unless you pass them in
;;;    * as the response for an action widget. They are
;;;    * for your convenience.
;;;    */
;;;   GTK_RESPONSE_REJECT = -2,
;;;   GTK_RESPONSE_ACCEPT = -3,
;;;
;;;   /* If the dialog is deleted. */
;;;   GTK_RESPONSE_DELETE_EVENT = -4,
;;;
;;;   /* These are returned from GTK dialogs, and you can also use them
;;;    * yourself if you like.
;;;    */
;;;   GTK_RESPONSE_OK     = -5,
;;;   GTK_RESPONSE_CANCEL = -6,
;;;   GTK_RESPONSE_CLOSE  = -7,
;;;   GTK_RESPONSE_YES    = -8,
;;;   GTK_RESPONSE_NO     = -9,
;;;   GTK_RESPONSE_APPLY  = -10,
;;;   GTK_RESPONSE_HELP   = -11
;;; } GtkResponseType;


(defconstant +CFFI/GTK-RESPONSE-NONE+         -1)
(defconstant +CFFI/GTK-RESPONSE-REJECT+       -2)
(defconstant +CFFI/GTK-RESPONSE-ACCEPT+       -3)
(defconstant +CFFI/GTK-RESPONSE-DELETE-EVENT+ -4)
(defconstant +CFFI/GTK-RESPONSE-OK+           -5)
(defconstant +CFFI/GTK-RESPONSE-CANCEL+       -6)
(defconstant +CFFI/GTK-RESPONSE-CLOSE+        -7)
(defconstant +CFFI/GTK-RESPONSE-YES+          -8)
(defconstant +CFFI/GTK-RESPONSE-NO+           -9)
(defconstant +CFFI/GTK-RESPONSE-APPLY+        -10)
(defconstant +CFFI/GTK-RESPONSE-HELP+         -11)

;;; --- GtkStateType

;;; typedef enum
;;; {
;;;   GTK_STATE_NORMAL,
;;;   GTK_STATE_ACTIVE,
;;;   GTK_STATE_PRELIGHT,
;;;   GTK_STATE_SELECTED,
;;;   GTK_STATE_INSENSITIVE
;;; } GtkStateType;

(defconstant +CFFI/GTK-STATE-NORMAL+      0)
(defconstant +CFFI/GTK-STATE-ACTIVE+      1)
(defconstant +CFFI/GTK-STATE-PRELIGHT+    2)
(defconstant +CFFI/GTK-STATE-SELECTED+    3)
(defconstant +CFFI/GTK-STATE-INSENSITIVE+ 4)

;;; --- GConnectFlags

;;; typedef enum
;;; {
;;;   G_CONNECT_AFTER	= 1 << 0,
;;;   G_CONNECT_SWAPPED	= 1 << 1
;;; } GConnectFlags;

(defconstant +CFFI/G-CONNECT-AFTER+   1)
(defconstant +CFFI/G-CONNECT-SWAPPED+ 2)

;;; --- GtkButtonAction

;;; typedef enum
;;; {
;;;   GTK_BUTTON_IGNORED = 0,
;;;   GTK_BUTTON_SELECTS = 1 << 0,
;;;   GTK_BUTTON_DRAGS   = 1 << 1,
;;;   GTK_BUTTON_EXPANDS = 1 << 2
;;; } GtkButtonAction;

(defconstant +CFFI/GTK-BUTTON-IGNORED+ 0)
(defconstant +CFFI/GTK-BUTTON-SELECTS+ (ash 1 0))
(defconstant +CFFI/GTK-BUTTON-DRAGS+   (ash 1 1))
(defconstant +CFFI/GTK-BUTTON-EXPANDS+ (ash 1 2))

;;; --- GSignalMatchType

;;; typedef enum
;;; {
;;;   G_SIGNAL_MATCH_ID	   = 1 << 0,
;;;   G_SIGNAL_MATCH_DETAIL	   = 1 << 1,
;;;   G_SIGNAL_MATCH_CLOSURE   = 1 << 2,
;;;   G_SIGNAL_MATCH_FUNC	   = 1 << 3,
;;;   G_SIGNAL_MATCH_DATA	   = 1 << 4,
;;;   G_SIGNAL_MATCH_UNBLOCKED = 1 << 5
;;; } GSignalMatchType;

(defconstant +CFFI/G-SIGNAL-MATCH-ID+        (ash 1 0))
(defconstant +CFFI/G-SIGNAL-MATCH-DETAIL+    (ash 1 1))
(defconstant +CFFI/G-SIGNAL-MATCH-CLOSURE+   (ash 1 2))
(defconstant +CFFI/G-SIGNAL-MATCH-FUNC+      (ash 1 3))
(defconstant +CFFI/G-SIGNAL-MATCH-DATA+      (ash 1 4))
(defconstant +CFFI/G-SIGNAL-MATCH-UNBLOCKED+ (ash 1 5))


;;; --- GdkWindowType

;;; typedef enum {
;;;     GDK_WINDOW_ROOT,     -- root window; no parent, entire screen
;;;     GDK_WINDOW_TOPLEVEL, -- top-level window (GtkWindow)
;;;     GDK_WINDOW_CHILD,    -- child window (GtkEntry)
;;;     GDK_WINDOW_DIALOG,   -- useless / deprecated
;;;     GDK_WINDOW_TEMP,     -- override redirect temporary window (GtkMenu)
;;;     GDK_WINDOW_FOREIGN   -- foreign window (e.g. XLib)
;;; } GdkWindowType;

(defconstant +CFFI/GDK-WINDOW-ROOT+     0)
(defconstant +CFFI/GDK-WINDOW-TOPLEVEL+ 1)
(defconstant +CFFI/GDK-WINDOW-CHILD+    2)
(defconstant +CFFI/GDK-WINDOW-DIALOG+   3)
(defconstant +CFFI/GDK-WINDOW-TEMP+     4)
(defconstant +CFFI/GDK-WINDOW-FOREIGN+  5)

;;; ------ GtkWindowType

;;; typedef enum {
;;;    GTK_WINDOW_TOPLEVEL, -- regular window, such as a dialog
;;;    GTK_WINDOW_POPUP     -- special window, such as a tooltip
;;; } GtkWindowType;

(cffi:defcenum GtkWindowType 
  :+CFFI/GTK-WINDOW-TOPLEVEL+
  :+CFFI/GTK-WINDOW-POPUP+)

;;; ------ GdkFunction

;;; This is defined in the 'graphics contexts' section of the
;;; gdk documentation.

;;; typedef enum {
;;;     GDK_COPY,         -- dst = src
;;;     GDK_INVERT,       -- dst = NOT dst
;;;     GDK_XOR,          -- dst = src XOR dst
;;;     GDK_CLEAR,        -- dst = 0
;;;     GDK_AND,          -- dst = dst AND src
;;;     GDK_AND_REVERSE,  -- dst = src AND (NOT dst)
;;;     GDK_AND_INVERT,   -- dst = (NOT src) AND dst
;;;     GDK_NOOP,         -- dst = dst
;;;     GDK_OR,           -- dst = src OR dst
;;;     GDK_EQUIV,        -- dst = (NOT src) XOR dst
;;;     GDK_OR_REVERSE,   -- dst = src OR (NOT dst)
;;;     GDK_COPY_INVERT,  -- dst = NOT src
;;;     GDK_OR_INVERT,    -- dst = (NOT src) OR dst
;;;     GDK_NAND,         -- dst = (NOT src) OR (NOT dst)
;;;     GDK_NOR,          -- dst = (NOT src) AND (NOT dst)
;;;     GDK_SET           -- dst = 1
;;; } GdkFunction;

(defconstant +CFFI/GDK-COPY+        0)
(defconstant +CFFI/GDK-INVERT+      1)
(defconstant +CFFI/GDK-XOR+         2)
(defconstant +CFFI/GDK-CLEAR+       3)
(defconstant +CFFI/GDK-AND+         4)
(defconstant +CFFI/GDK-AND-REVERSE+ 5)
(defconstant +CFFI/GDK-AND-INVERT+  6)
(defconstant +CFFI/GDK-NOOP+        7)
(defconstant +CFFI/GDK-OR+          8)
(defconstant +CFFI/GDK-EQUIV+       9)
(defconstant +CFFI/GDK-OR-REVERSE+  10)
(defconstant +CFFI/GDK-COPY-INVERT+ 11)
(defconstant +CFFI/GDK-OR-INVERT+   12)
(defconstant +CFFI/GDK-NAND+        13)
(defconstant +CFFI/GDK-NOR+         14)
(defconstant +CFFI/GDK-SET+         15)

;;; ------ GdkCapStyle

;;; typedef enum {
;;;     GDK_CAP_NOT_LAST,
;;;     GDK_CAP_BUTT,
;;;     GDK_CAP_ROUND,
;;;     GDK_CAP_PROJECTING
;;; } GdkCapStyle;

(defconstant +CFFI/GDK-CAP-NOT-LAST+ 0)
(defconstant +CFFI/GDK-CAP-BUTT+ 1)
(defconstant +CFFI/GDK-CAP-ROUND+ 2)
(defconstant +CFFI/GDK-CAP-PROJECTING+ 3)

;;; ------ GdkJoinStyle

;;; typedef enum {
;;;     GDK_JOIN_MITER,
;;;     GDK_JOIN_ROUND,
;;;     GDK_JOIN_BEVEL
;;; } GdkJoinStyle;

(defconstant +CFFI/GDK-JOIN-MITER+ 0)
(defconstant +CFFI/GDK-JOIN-ROUND+ 1)
(defconstant +CFFI/GDK-JOIN-BEVEL+ 2)

;;; ------ GdkLineStyle

;;; typedef enum {
;;;     GDK_LINE_SOLID
;;;     GDK_LINE_ON_OFF_DASH
;;;     GDK_LINE_DOUBLE_DASH
;;; } GdkLineStyle;

(defconstant +CFFI/GDK-LINE-SOLID+ 0)
(defconstant +CFFI/GDK-LINE-ON-OFF-DASH+ 1)
(defconstant +CFFI/GDK-LINE-DOUBLE-DASH+ 2)

;;; ------ GdkFill

;;; typedef enum {
;;;     GDK_SOLID,          -- draw with the foreground colour
;;;     GDK_TILED,          -- draw with a tiled pixmap
;;;     GDK_STIPPLED,       -- draw using the stipple bitmap. Pixels
;;;                            corresponding to bits in the stipple bitmap
;;;                            that are set will be drawn in foreground
;;;                            colour, others left untouched
;;;     GDK_OPAQUE_STIPPLED -- draw using stipple bitmap. Like GDK_STIPPLED
;;;                            except that pixels that are unset in the
;;;                            stipple bitmap are drawn in the background
;;;                            colour
;;; } GdkFill;

(defconstant +CFFI/GDK-SOLID+           0)
(defconstant +CFFI/GDK-TILED+           1)
(defconstant +CFFI/GDK-STIPPLED+        2)
(defconstant +CFFI/GDK-OPAQUE-STIPPLED+ 3)

;;; ------ GtkShadowType

;;; typedef enum {
;;;     GTK_SHADOW_NONE,       -- no outline
;;;     GTK_SHADOW_IN,         -- bevelled inwards
;;;     GTK_SHADOW_OUT,        -- bevelled outwards like a button
;;;     GTK_SHADOW_ETCHED_IN,  -- sunken 3d appearance
;;;     GTK_SHADOW_ETCHED_OUT  -- raised 3d appearance
;;; } GtkShadowType;

(defconstant +CFFI/GTK-SHADOW-NONE+       0)
(defconstant +CFFI/GTK-SHADOW-IN+         1)
(defconstant +CFFI/GTK-SHADOW-OUT+        2)
(defconstant +CFFI/GTK-SHADOW-ETCHED-IN+  3)
(defconstant +CFFI/GTK-SHADOW-ETCHED-OUT+ 4)

;;; ------ GtkSelectionMode

;;; typedef enum {
;;;     GTK_SELECTION_NONE,      -- no selection is possible
;;;     GTK_SELECTION_SINGLE,    -- zero or one
;;;     GTK_SELECTION_BROWSE,    -- exactly one element selected
;;;     GTK_SELECTION_MULTIPLE,  -- any number of elements may be selected
;;;     GTK_SELECTION_EXTENDED = GTK_SELECTION_MULTIPLE -- deprecated
;;; } GtkSelectionMode;

(defconstant +CFFI/GTK-SELECTION-NONE+     0)
(defconstant +CFFI/GTK-SELECTION-SINGLE+   1)
(defconstant +CFFI/GTK-SELECTION-BROWSE+   2)
(defconstant +CFFI/GTK-SELECTION-MULTIPLE+ 3)
(defconstant +CFFI/GTK-SELECTION-EXTENDED+ 3)

;;; ------ GdkEventType

;;; typedef enum
;;; {
;;;   GDK_NOTHING	    = -1,
;;;   GDK_DELETE	    = 0,
;;;   GDK_DESTROY	    = 1,
;;;   GDK_EXPOSE	    = 2,
;;;   GDK_MOTION_NOTIFY	    = 3,
;;;   GDK_BUTTON_PRESS	    = 4,
;;;   GDK_2BUTTON_PRESS	    = 5,
;;;   GDK_3BUTTON_PRESS	    = 6,
;;;   GDK_BUTTON_RELEASE    = 7,
;;;   GDK_KEY_PRESS	    = 8,
;;;   GDK_KEY_RELEASE	    = 9,
;;;   GDK_ENTER_NOTIFY	    = 10,
;;;   GDK_LEAVE_NOTIFY	    = 11,
;;;   GDK_FOCUS_CHANGE	    = 12,
;;;   GDK_CONFIGURE	    = 13,
;;;   GDK_MAP		    = 14,
;;;   GDK_UNMAP		    = 15,
;;;   GDK_PROPERTY_NOTIFY   = 16,
;;;   GDK_SELECTION_CLEAR   = 17,
;;;   GDK_SELECTION_REQUEST = 18,
;;;   GDK_SELECTION_NOTIFY  = 19,
;;;   GDK_PROXIMITY_IN	    = 20,
;;;   GDK_PROXIMITY_OUT	    = 21,
;;;   GDK_DRAG_ENTER        = 22,
;;;   GDK_DRAG_LEAVE        = 23,
;;;   GDK_DRAG_MOTION       = 24,
;;;   GDK_DRAG_STATUS       = 25,
;;;   GDK_DROP_START        = 26,
;;;   GDK_DROP_FINISHED     = 27,
;;;   GDK_CLIENT_EVENT	    = 28,
;;;   GDK_VISIBILITY_NOTIFY = 29,
;;;   GDK_NO_EXPOSE	    = 30,
;;;   GDK_SCROLL            = 31,
;;;   GDK_WINDOW_STATE      = 32,
;;;   GDK_SETTING           = 33,
;;;   GDK_OWNER_CHANGE      = 34,
;;;   GDK_GRAB_BROKEN       = 35,
;;;   GDK_DAMAGE            = 36
;;; } GdkEventType;

(defconstant +CFFI/GDK-BUTTON-PRESS+   4)
(defconstant +CFFI/GDK-2BUTTON-PRESS+  5)
(defconstant +CFFI/GDK-3BUTTON-PRESS+  6)
(defconstant +CFFI/GDK-BUTTON-RELEASE+ 7)
(defconstant +CFFI/GDK-KEY-PRESS+      8)
(defconstant +CFFI/GDK-KEY-RELEASE+    9)

;;; typedef enum
;;; {
;;;   GTK_RESIZE_PARENT,		/* Pass resize request to the parent */
;;;   GTK_RESIZE_QUEUE,		/* Queue resizes on this widget */
;;;   GTK_RESIZE_IMMEDIATE		/* Perform the resizes now */
;;; } GtkResizeMode;

(defconstant +CFFI/GTK-RESIZE-PARENT+    0)
(defconstant +CFFI/GTK-RESIZE-QUEUE+     1)
(defconstant +CFFI/GTK-RESIZE-IMMEDIATE+ 2)

;;; ------------------------------------------------------------------------

;;; ------ GdkModifierType

;;; typedef enum {
;;;     GDK_SHIFT_MASK   = 1 << 0,
;;;     GDK_LOCK_MASK    = 1 << 1,
;;;     GDK_CONTROL_MASK = 1 << 2,
;;;     GDK_MOD1_MASK    = 1 << 3,
;;;     GDK_MOD2_MASK    = 1 << 4,
;;;     GDK_MOD3_MASK    = 1 << 5,
;;;     GDK_MOD4_MASK    = 1 << 6,
;;;     GDK_MOD5_MASK    = 1 << 7,
;;;     GDK_BUTTON1_MASK = 1 << 8,
;;;     GDK_BUTTON2_MASK = 1 << 9,
;;;     GDK_BUTTON3_MASK = 1 << 10,
;;;     GDK_BUTTON4_MASK = 1 << 11,
;;;     GDK_BUTTON5_MASK = 1 << 12,
;;;
;;;     GDK_SUPER_MASK   = 1 << 26,
;;;     GDK_HYPER_MASK   = 1 << 27,
;;;     GDK_META_MASK    = 1 << 28,
;;;
;;;     GDK_RELEASE_MASK = 1 << 30,      -- not used by GDK, but by GTK.
;;;     GDK_MODIFIER_MASK = 0x5c001fff   -- all modifiers
;;; } GdkModifierType;

(defconstant +CFFI/GDK-SHIFT-MASK+ (ash 1 0))
(defconstant +CFFI/GDK-LOCK-MASK+  (ash 1 1))
(defconstant +CFFI/GDK-CONTROL-MASK+ (ash 1 2))
(defconstant +CFFI/GDK-MOD1-MASK+ (ash 1 3))
(defconstant +CFFI/GDK-MOD2-MASK+ (ash 1 4))
(defconstant +CFFI/GDK-MOD3-MASK+ (ash 1 5))
(defconstant +CFFI/GDK-MOD4-MASK+ (ash 1 6))
(defconstant +CFFI/GDK-MOD5-MASK+ (ash 1 7))
(defconstant +CFFI/GDK-BUTTON1-MASK+ (ash 1 8))
(defconstant +CFFI/GDK-BUTTON2-MASK+ (ash 1 9))
(defconstant +CFFI/GDK-BUTTON3-MASK+ (ash 1 10))
(defconstant +CFFI/GDK-BUTTON4-MASK+ (ash 1 11))
(defconstant +CFFI/GDK-BUTTON5-MASK+ (ash 1 12))

(defconstant +CFFI/GDK-SUPER-MASK+ (ash 1 26))
(defconstant +CFFI/GDK-HYPER-MASK+ (ash 1 27))

(defconstant +CFFI/GDK-META-MASK+ (ash 1 28))

;;; ------ GdkEventMask

;;; typedef enum {
;;;     GDK_EXPOSURE_MASK = 1 << 1,
;;;     GDK_POINTER_MOTION_MASK = 1 << 2,
;;;     GDK_POINTER_MOTION_HINT_MASK = 1 << 3,
;;;     GDK_BUTTON_MOTION_MASK = 1 << 4,
;;;     GDK_BUTTON1_MOTION_MASK = 1 << 5,
;;;     GDK_BUTTON2_MOTION_MASK = 1 << 6,
;;;     GDK_BUTTON3_MOTION_MASK = 1 << 7,
;;;     GDK_BUTTON_PRESS_MASK = 1 << 8,
;;;     GDK_BUTTON_RELEASE_MASK = 1 << 9,
;;;     GDK_KEY_PRESS_MASK = 1 << 10,
;;;     GDK_KEY_RELEASE_MASK = 1 << 11,
;;;     GDK_ENTER_NOTIFY_MASK = 1 << 12,
;;;     GDK_LEAVE_NOTIFY_MASK = 1 << 13,
;;;     GDK_FOCUS_CHANGE_MASK = 1 << 14,
;;;     GDK_STRUCTURE_MASK = 1 << 15,
;;;     GDK_PROPERTY_CHANGE_MASK = 1 << 16,
;;;     GDK_VISIBILITY_NOTIFY_MASK = 1 << 17,
;;;     GDK_PROXIMITY_IN_MASK = 1 << 18,
;;;     GDK_PROXIMITY_OUT_MASK = 1 << 19,
;;;     GDK_SUBSTRUCTURE_MASK = 1 << 20,
;;;     GDK_SCROLL_MASK = 1 << 21,
;;;     GDK_ALL_EVENTS_MASK = 0x3ffffe
;;; } GdkEventMask;

(defconstant +CFFI/GDK-EXPOSURE-MASK+            (ash 1 1))
(defconstant +CFFI/GDK-POINTER-MOTION-MASK+      (ash 1 2))
(defconstant +CFFI/GDK-POINTER-MOTION-HINT-MASK+ (ash 1 3))

;;; These aren't used by DUIM currently, but are included for
;;; completeness
(defconstant +CFFI/GDK-BUTTON-MOTION-MASK+       (ash 1 4))
(defconstant +CFFI/GDK-BUTTON1-MOTION-MASK+      (ash 1 5))
(defconstant +CFFI/GDK-BUTTON2-MOTION-MASK+      (ash 1 6))
(defconstant +CFFI/GDK-BUTTON3-MOTION-MASK+      (ash 1 7))

(defconstant +CFFI/GDK-BUTTON-PRESS-MASK+        (ash 1 8))
(defconstant +CFFI/GDK-BUTTON-RELEASE-MASK+      (ash 1 9))

;;; These aren't used by DUIM currently, but are included for
;;; completeness
(defconstant +CFFI/GDK-KEY-PRESS-MASK+           (ash 1 10))
(defconstant +CFFI/GDK-KEY-RELEASE-MASK+         (ash 1 11))
(defconstant +CFFI/GDK-ENTER-NOTIFY-MASK+        (ash 1 12))

(defconstant +CFFI/GDK-LEAVE-NOTIFY-MASK+        (ash 1 13))

;;; These aren't used by DUIM currently, but are included for
;;; completeness
(defconstant +CFFI/GDK-FOCUS-CHANGE-MASK+        (ash 1 14))
(defconstant +CFFI/GDK-STRUCTURE-MASK+           (ash 1 15))
(defconstant +CFFI/GDK-PROPERTY-CHANGE-MASK+     (ash 1 16))
(defconstant +CFFI/GDK-VISIBILITY-NOTIFY-MASK+   (ash 1 17))
(defconstant +CFFI/GDK-PROXIMITY-IN-MASK+        (ash 1 18))
(defconstant +CFFI/GDK-PROXIMITY-OUT-MASK+       (ash 1 19))
(defconstant +CFFI/GDK-SUBSTRUCTURE-MASK+        (ash 1 20))
(defconstant +CFFI/GDK-SCROLL-MASK+              (ash 1 21))
(defconstant +CFFI/GDK-ALL-EVENTS-MASK+          #x3ffffe)

;;; ------ GtkWrapMode

;;; typedef enum
;;; {
;;;   GTK_WRAP_NONE,     -- don't wrap, just make text area larger
;;;   GTK_WRAP_CHAR,     -- wrap text, breaking anywhere
;;;   GTK_WRAP_WORD,     -- wrap text, breaking in between words
;;;   GTK_WRAP_WORD_CHAR -- wrap text, breaking in between words, or between
;;;                         graphemes if that is insufficient
;;; } GtkWrapMode;

(defconstant +CFFI/GTK-WRAP-NONE+      0)
(defconstant +CFFI/GTK-WRAP-CHAR+      1)
(defconstant +CFFI/GTK-WRAP-WORD+      2)
(defconstant +CFFI/GTK-WRAP-WORD-CHAR+ 3)

;;; ------ GdkVisualType

;;; typedef enum {
;;;   GDK_VISUAL_STATIC_GRAY,    - each pixel value indexes a grayscale value
;;;                                directly
;;;   GDK_VISUAL_GRAYSCALE,      - each pixel is an index into a colormap
;;;                                that maps pixel values into grayscale
;;;                                values. The colormap can be changed by an
;;;                                application.
;;;   GDK_VISUAL_STATIC_COLOR,   - each pixel value is an index into a
;;;                                predefined, unmodifiable colormap that maps
;;;                                pixel values into rgb values
;;;   GDK_VISUAL_PSEUDO_COLOR,   - each pixel is an index into a colormap that
;;;                                maps pixel values into rgb values. The
;;;                                colormap can be changed by an application.
;;;   GDK_VISUAL_TRUE_COLOR,     - Each pixel value directly contains red,
;;;                                green and blue components. The red_mask,
;;;                                green_mask and blue_mask fields in the
;;;                                GdkVisual structure describe how the
;;;                                components are assembled into a pixel value.
;;;   GDK_VISUAL_DIRECT_COLOR    - Each pixel value contains red, green and
;;;                                blue components as for GDK_VISUAL_TRUE_COLOR,
;;;                                but the components are mapped via a color
;;;                                table into the final output table instead
;;;                                of being converted directly.
;;; } GdkVisualType;

(defconstant +CFFI/GDK-VISUAL-STATIC-GRAY+  0)
(defconstant +CFFI/GDK-VISUAL-GRAYSCALE+    1)
(defconstant +CFFI/GDK-VISUAL-STATIC-COLOR+ 2)
(defconstant +CFFI/GDK-VISUAL-PSEUDO-COLOR+ 3)
(defconstant +CFFI/GDK-VISUAL-TRUE-COLOR+   4)
(defconstant +CFFI/GDK-VISUAL-DIRECT-COLOR+ 5)


;;; typedef enum {
;;;   GDK_NOTIFY_ANCESTOR		= 0,
;;;   GDK_NOTIFY_VIRTUAL		= 1,
;;;   GDK_NOTIFY_INFERIOR		= 2,
;;;   GDK_NOTIFY_NONLINEAR		= 3,
;;;   GDK_NOTIFY_NONLINEAR_VIRTUAL	= 4,
;;;   GDK_NOTIFY_UNKNOWN		= 5
;;; } GdkNotifyType;

(defconstant +CFFI/GDK-NOTIFY-ANCESTOR+          0)
(defconstant +CFFI/GDK-NOTIFY-VIRTUAL+           1)
(defconstant +CFFI/GDK-NOTIFY-INFERIOR+          2)
(defconstant +CFFI/GDK-NOTIFY-NONLINEAR+         3)
(defconstant +CFFI/GDK-NOTIFY-NONLINEAR-VIRTUAL+ 4)
(defconstant +CFFI/GDK-NOTIFY-UNKNOWN+           5)


;; typedef enum {
;;   GDK_CROSSING_NORMAL,
;;   GDK_CROSSING_GRAB,
;;   GDK_CROSSING_UNGRAB
;; } GdkCrossingMode;

(defconstant +CFFI/GDK-CROSSING-NORMAL+ 0)
(defconstant +CFFI/GDK-CROSSING-GRAB+   1)
(defconstant +CFFI/GDK-CROSSING-UNGRAB+ 2)


;; PANGO

(defconstant +PANGO-SCALE+ 1024)

;; typedef enum {
;;   PANGO_VARIANT_NORMAL,
;;   PANGO_VARIANT_SMALL_CAPS
;; } PangoVariant;

(defconstant +PANGO-VARIANT-NORMAL+ 0)
(defconstant +PANGO-VARIANT-SMALL-CAPS+ 1)

;; typedef enum {
;;   PANGO_WEIGHT_ULTRALIGHT = 200,
;;   PANGO_WEIGHT_LIGHT = 300,
;;   PANGO_WEIGHT_NORMAL = 400,
;;   PANGO_WEIGHT_SEMIBOLD = 600,
;;   PANGO_WEIGHT_BOLD = 700,
;;   PANGO_WEIGHT_ULTRABOLD = 800,
;;   PANGO_WEIGHT_HEAVY = 900
;; } PangoWeight;

(defconstant +PANGO-WEIGHT-ULTRALIGHT+ 200)
(defconstant +PANGO-WEIGHT-LIGHT+ 300)
(defconstant +PANGO-WEIGHT-NORMAL+ 400)
(defconstant +PANGO-WEIGHT-SEMIBOLD+ 600)
(defconstant +PANGO-WEIGHT-BOLD+ 700)
(defconstant +PANGO-WEIGHT-ULTRABOLD+ 800)
(defconstant +PANGO-WEIGHT-HEAVY+ 900)

;; typedef enum {
;;   PANGO_STYLE_NORMAL,
;;   PANGO_STYLE_OBLIQUE,
;;   PANGO_STYLE_ITALIC
;; } PangoStyle;

(defconstant +PANGO-STYLE-NORMAL+ 0)
(defconstant +PANGO-STYLE-OBLIQUE+ 1)
(defconstant +PANGO-STYLE-ITALIC+ 2)

;; typedef enum {
;;   PANGO_STRETCH_ULTRA_CONDENSED,
;;   PANGO_STRETCH_EXTRA_CONDENSED,
;;   PANGO_STRETCH_CONDENSED,
;;   PANGO_STRETCH_SEMI_CONDENSED,
;;   PANGO_STRETCH_NORMAL,
;;   PANGO_STRETCH_SEMI_EXPANDED,
;;   PANGO_STRETCH_EXPANDED,
;;   PANGO_STRETCH_EXTRA_EXPANDED,
;;   PANGO_STRETCH_ULTRA_EXPANDED
;; } PangoStretch;

(defconstant +PANGO-STRETCH-ULTRA-CONDENSED+ 0)
(defconstant +PANGO-STRETCH-EXTRA-CONDENSED+ 1)
(defconstant +PANGO-STRETCH-CONDENSED+ 2)
(defconstant +PANGO-STRETCH-SEMI-CONDENSED+ 3)
(defconstant +PANGO-STRETCH-NORMAL+ 4)
(defconstant +PANGO-STRETCH-SEMI-EXPANDED+ 5)
(defconstant +PANGO-STRETCH-EXPANDED+ 6)
(defconstant +PANGO-STRETCH-EXTRA-EXPANDED+ 7)
(defconstant +PANGO-STRETCH-ULTRA-EXPANDED+ 8)


;;; ------ GObject type constants; only fundamental types

(defconstant +CFFI/G-TYPE-FUNDAMENTAL-SHIFT+ 2)

(defconstant +CFFI/G-TYPE-INVALID+ (ash 0 2))
(defconstant +CFFI/G-TYPE-NONE+ (ash 1 2))
(defconstant +CFFI/G-TYPE-INTERFACE+ (ash 2 2))
(defconstant +CFFI/G-TYPE-CHAR+ (ash 3 2))
(defconstant +CFFI/G-TYPE-UCHAR+ (ash 4 2))
(defconstant +CFFI/G-TYPE-BOOLEAN+ (ash 5 2))
(defconstant +CFFI/G-TYPE-INT+ (ash 6 2))
(defconstant +CFFI/G-TYPE-UINT+ (ash 7 2))
(defconstant +CFFI/G-TYPE-LONG+ (ash 8 2))
(defconstant +CFFI/G-TYPE-ULONG+ (ash 9 2))
(defconstant +CFFI/G-TYPE-INT64+ (ash 10 2))
(defconstant +CFFI/G-TYPE-UINT64+ (ash 11 2))
(defconstant +CFFI/G-TYPE-ENUM+ (ash 12 2))
(defconstant +CFFI/G-TYPE-FLAGS+ (ash 13 2))
(defconstant +CFFI/G-TYPE-FLOAT+ (ash 14 2))
(defconstant +CFFI/G-TYPE-DOUBLE+ (ash 15 2))
(defconstant +CFFI/G-TYPE-STRING+ (ash 16 2))
(defconstant +CFFI/G-TYPE-POINTER+ (ash 17 2))
(defconstant +CFFI/G-TYPE-BOXED+ (ash 18 2))
(defconstant +CFFI/G-TYPE-PARAM+ (ash 19 2))
(defconstant +CFFI/G-TYPE-OBJECT+ (ash 20 2))
(defconstant +CFFI/G-TYPE-VARIANT+ (ash 21 2))


;;; ------ Button sensitivity for GtkComboBox

(defconstant +CFFI/GTK-SENSITIVITY-AUTO+ 0)
(defconstant +CFFI/GTK-SENSITIVITY-ON+   1)
(defconstant +CFFI/GTK-SENSITIVITY-OFF+  2)


;;; ------ Wrap modes for GtkTextView

(defconstant +CFFI/GTK-WRAP-NONE+      0)
(defconstant +CFFI/GTK-WRAP-CHAR+      1)
(defconstant +CFFI/GTK-WRAP-WORD+      2)
(defconstant +CFFI/GTK-WRAP-WORD-CHAR+ 3)

;;; ------ GdkRgbDither

(defconstant +CFFI/GDK-RGB-DITHER-NONE+   0)
(defconstant +CFFI/GDK-RGB-DITHER-NORMAL+ 1)
(defconstant +CFFI/GDK-RGB-DITHER-MAX+    2)

;;; ------ GtkWidgetFlags

;; typedef enum
;; {
;;   GTK_TOPLEVEL         = 1 << 4,
;;   GTK_NO_WINDOW        = 1 << 5,
;;   GTK_REALIZED         = 1 << 6,
;;   GTK_MAPPED           = 1 << 7,
;;   GTK_VISIBLE          = 1 << 8,
;;   GTK_SENSITIVE        = 1 << 9,
;;   GTK_PARENT_SENSITIVE = 1 << 10,
;;   GTK_CAN_FOCUS        = 1 << 11,
;;   GTK_HAS_FOCUS        = 1 << 12,

;;   /* widget is allowed to receive the default via gtk_widget_grab_default
;;    * and will reserve space to draw the default if possible
;;    */
;;   GTK_CAN_DEFAULT      = 1 << 13,

;;   /* the widget currently is receiving the default action and should be drawn
;;    * appropriately if possible
;;    */
;;   GTK_HAS_DEFAULT      = 1 << 14,

;;   GTK_HAS_GRAB	       = 1 << 15,
;;   GTK_RC_STYLE	       = 1 << 16,
;;   GTK_COMPOSITE_CHILD  = 1 << 17,
;;   GTK_NO_REPARENT      = 1 << 18,
;;   GTK_APP_PAINTABLE    = 1 << 19,

;;   /* the widget when focused will receive the default action and have
;;    * HAS_DEFAULT set even if there is a different widget set as default
;;    */
;;   GTK_RECEIVES_DEFAULT = 1 << 20,

;;   GTK_DOUBLE_BUFFERED  = 1 << 21,
;;   GTK_NO_SHOW_ALL      = 1 << 22
;; } GtkWidgetFlags;

(defconstant +CFFI/GTK-TOPLEVEL+         (ash 1 4))
(defconstant +CFFI/GTK-NO-WINDOW+        (ash 1 5))
(defconstant +CFFI/GTK-REALIZED+         (ash 1 6))
(defconstant +CFFI/GTK-MAPPED+           (ash 1 7))
(defconstant +CFFI/GTK-VISIBLE+          (ash 1 8))
(defconstant +CFFI/GTK-SENSITIVE+        (ash 1 9))
(defconstant +CFFI/GTK-PARENT-SENSITIVE+ (ash 1 10))
(defconstant +CFFI/GTK-CAN-FOCUS+        (ash 1 11))
(defconstant +CFFI/GTK-HAS-FOCUS+        (ash 1 12))
(defconstant +CFFI/GTK-CAN-DEFAULT+      (ash 1 13))
(defconstant +CFFI/GTK-HAS-DEFAULT+      (ash 1 14))
(defconstant +CFFI/GTK-HAS-GRAB+         (ash 1 15))
(defconstant +CFFI/GTK-RC-STYLE+         (ash 1 16))
(defconstant +CFFI/GTK-COMPOSITE-CHILD+  (ash 1 17))
(defconstant +CFFI/GTK-NO-REPARENT+      (ash 1 18))
(defconstant +CFFI/GTK-APP-PAINTABLE+    (ash 1 19))
(defconstant +CFFI/GTK-RECEIVES-DEFAULT+ (ash 1 20))
(defconstant +CFFI/GTK-DOUBLE-BUFFERED+  (ash 1 21))
(defconstant +CFFI/GTK-NO-SHOW-ALL+      (ash 1 22))


;; typedef enum
;; {
;;   GTK_ACCEL_VISIBLE        = 1 << 0,	/* display in GtkAccelLabel? */
;;   GTK_ACCEL_LOCKED         = 1 << 1,	/* is it removable? */
;;   GTK_ACCEL_MASK           = 0x07
;; } GtkAccelFlags;

(defconstant +CFFI/GTK-ACCEL-VISIBLE+ (ash 1 0))
(defconstant +CFFI/GTK-ACCEL-LOCKED+  (ash 1 1))
(defconstant +CFFI/GTK-ACCEL-MASK+    #x07)


;; typedef enum
;; {
;;   GDK_GRAVITY_NORTH_WEST = 1,
;;   GDK_GRAVITY_NORTH,
;;   GDK_GRAVITY_NORTH_EAST,
;;   GDK_GRAVITY_WEST,
;;   GDK_GRAVITY_CENTER,
;;   GDK_GRAVITY_EAST,
;;   GDK_GRAVITY_SOUTH_WEST,
;;   GDK_GRAVITY_SOUTH,
;;   GDK_GRAVITY_SOUTH_EAST,
;;   GDK_GRAVITY_STATIC
;; } GdkGravity;
;;
;; Defines the reference point of a window and the meaning of coordinates passed to gtk_window_move().
;; See gtk_window_move() and the "implementation notes" section of the Extended Window Manager Hints specification for more details.
;;
;; GDK_GRAVITY_NORTH_WEST
;; 	the reference point is at the top left corner.
;;
;; GDK_GRAVITY_NORTH
;; 	the reference point is in the middle of the top edge.
;;
;; GDK_GRAVITY_NORTH_EAST
;; 	the reference point is at the top right corner.
;;
;; GDK_GRAVITY_WEST
;; 	the reference point is at the middle of the left edge.
;;
;; GDK_GRAVITY_CENTER
;; 	the reference point is at the center of the window.
;;
;; GDK_GRAVITY_EAST
;; 	the reference point is at the middle of the right edge.
;;
;; GDK_GRAVITY_SOUTH_WEST
;; 	the reference point is at the lower left corner.
;;
;; GDK_GRAVITY_SOUTH
;; 	the reference point is at the middle of the lower edge.
;;
;; GDK_GRAVITY_SOUTH_EAST
;; 	the reference point is at the lower right corner.
;;
;; GDK_GRAVITY_STATIC
;; 	the reference point is at the top left corner of the window itself, ignoring window manager decorations. 

(defconstant +CFFI/GDK-GRAVITY-NORTH-WEST+ 1)
(defconstant +CFFI/GDK-GRAVITY-NORTH+      2)
(defconstant +CFFI/GDK-GRAVITY-NORTH-EAST+ 3)
(defconstant +CFFI/GDK-GRAVITY-WEST+       4)
(defconstant +CFFI/GDK-GRAVITY-CENTER+     5)
(defconstant +CFFI/GDK-GRAVITY-EAST+       6)
(defconstant +CFFI/GDK-GRAVITY-SOUTH-WEST+ 7)
(defconstant +CFFI/GDK-GRAVITY-SOUTH+      8)
(defconstant +CFFI/GDK-GRAVITY-SOUTH-EAST+ 9)
(defconstant +CFFI/GDK-GRAVITY-STATIC+     10)
