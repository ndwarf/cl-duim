;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GTK2-DUIM -*-
(in-package #:gtk2-duim)

;;; Some error reporting for cairo fns

(defun cairo-status-report (context)
"
 Executes CAIRO_STATUS on CONTEXT and reports on the result
 (if not CAIRO_STATUS_SUCCESS). This is used to report on
 fns that manipulate cairo_t instances.
"
  (let ((status (cffi/cairo-status context)))
    (unless (zerop status)
      (warn "cffi/cairo-status returned error: ~s" (cffi/cairo_status_to_string status)))))


(defun cairo-surface-status-report (surface)
"
 Executes CAIRO_SURFACE_STATUS on SURFACE and reports on the result
 (if not CAIRO_STATUS_SUCCESS). This is used to report on
 fns that manipulate cairo_surface_t instances.
"
  (let ((status (cffi/cairo_surface_status surface)))
    (unless (zerop status)
      (warn "cffi/cairo-surface-status returned error: ~s" (cffi/cairo_status_to_string status)))))


(defun cairo-pattern-status-report (pattern)
"
 Executes CAIRO_PATTERN_STATUS on PATTERN and reports on the result
 (if not CAIRO_STATUS_SUCCESS). This is used to report on
 fns that manipulate cairo_pattern_t instances.
"
  (let ((status (cffi/cairo_pattern_status pattern)))
    (unless (zerop status)
      (warn "cffi/cairo-pattern-status returned error: ~s" (cffi/cairo_status_to_string status)))))

;; Not done: cairo-font-face-status-report, cairo-scaled-font-status-report, cairo-region-status-report...

#-(and)
(defun cairo-region-status-report (region)
"
 Executes CAIRO_REGION_STATUS on REGION and reports on the result
 (if not CAIRO_STATUS_SUCCESS). This is used to report on
 fns that manipulate cairo_region_t instances.
"
  (let ((status (cffi/cairo_region_status surface)))
    (unless (zerop status)
      (warn "cffi/cairo-region-status returned error: %s" (cffi/cairo_status_to_string status)))))

;;;;;;;;;;;;;;;;;;;;;; GDK hooks (mainly unused, here for completeness)

(defun cffi/gdk-window-create-similar-surface (window content width height)
"
 cairo_surface_t * gdk_window_create_similar_surface (GdkWindow *window,
                                                      cairo_content_t content,
                                                      int width, int height);

 Create a new surface that is as compatible as possible with the given window. For example the
 new surface will have the same fallback resolution and font options as window. Generally, the
 new surface will also use the same backend as window, unless that is not possible for some
 reason. The type of the returned surface may be examined with cairo_surface_get_type().

 Initially the surface contents are all 0 (transparent if contents have transparency, black
 otherwise.)

    window : window to make new surface similar to
    content : the content for the new surface
    width : width of the new surface
    height : height of the new surface

    Returns : a pointer to the newly allocated surface. The caller owns the surface and should call
              cairo_surface_destroy() when done with it. This function always returns a valid pointer,
              but it will return a pointer to a "nil" surface if other is already in an error state
     or any other error occurs.

 Since 2.22
"
  (if (or (null window) (cffi:null-pointer-p window))
      (error "CFFI/GDK-WINDOW-CREATE-SIMILAR-SURFACE invoked with null WINDOW")
      (let ((surface (cffi:foreign-funcall "gdk_window_create_similar_surface"
					   :pointer window
					   :int content
					   :int width
					   :int height
					   :pointer)))
	(cairo-surface-status-report surface)
	surface)))
      

(defun cffi/gdk-cairo-create (drawable)
"
 cairo_t * gdk_cairo_create (GdkDrawable *drawable);

 Creates a Cairo context for drawing to drawable.

 Note

 Note that due to double-buffering, Cairo contexts created in a GTK+ expose event handler cannot be
 cached and reused between different expose events.

    drawable : a GdkDrawable

    Returns : A newly created Cairo context. Free with cairo_destroy() when you are done drawing.

 Since 2.8
"
  (if (or (null drawable) (cffi:null-pointer-p drawable))
      (error "CFFI/GDK-CAIRO-CREATE invoked with null DRAWABLE")
      (let ((context (cffi:foreign-funcall "gdk_cairo_create"
					   :pointer drawable
					   :pointer)))
	(cairo-status-report context)
	context)))


(defun cffi/gdk-cairo-set-source-color (cr color)
"
 void gdk_cairo_set_source_color (cairo_t *cr, const GdkColor *color);

 Sets the specified GdkColor as the source color of cr.

    cr : a cairo_t
    color : a GdkColor

 Since 2.8
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/GDK-CAIRO-SET-SOURCE-COLOR invoked with null CR")
      (if (or (null color) (cffi:null-pointer-p color))
	  (error "CFFI/GDK-CAIRO-SET-SOURCE-COLOR invoked with null COLOR")
	  (cffi:foreign-funcall "gdk_cairo_set_source_color"
				:pointer cr
				:pointer color
				:void)))
  (cairo-status-report cr)
  cr)


(defun cffi/gdk_cairo_set_source_pixbuf (cr pixbuf pixbuf-x pixbuf-y)
"
 void gdk_cairo_set_source_pixbuf (cairo_t *cr, const GdkPixbuf *pixbuf,
                                   double pixbuf_x, double pixbuf_y);

 Sets the given pixbuf as the source pattern for the Cairo context. The pattern has an
 extend mode of CAIRO_EXTEND_NONE and is aligned so that the origin of pixbuf is
 pixbuf_x, pixbuf_y

    cr : a Cairo context
    pixbuf : a GdkPixbuf
    pixbuf_x : X coordinate of location to place upper left corner of pixbuf
    pixbuf_y : Y coordinate of location to place upper left corner of pixbuf

 Since 2.8
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/GDK_CAIRO_SET_SOURCE_PIXBUF invoked with null CR")
      (if (or (null pixbuf) (cffi:null-pointer-p pixbuf))
	  (error "CFFI/GDK_CAIRO_SET_SOURCE_PIXBUF invoked with null PIXBUF")
	  (cffi:foreign-funcall "gdk_cairo_set_source_pixbuf"
				:pointer cr
				:pointer pixbuf
				:double pixbuf-x :double pixbuf-y
				:void)))
  (cairo-status-report cr)
  cr)


(defun cffi/gdk-cairo-set-source-pixmap (cr pixmap pixmap-x pixmap-y)
"
 void gdk_cairo_set_source_pixmap (cairo_t *cr, GdkPixmap *pixmap,
                                   double pixmap_x, double pixmap_y);

 Warning

 gdk_cairo_set_source_pixmap has been deprecated since version 2.24 and should not be used
 in newly-written code. This function is being removed in GTK+ 3 (together with GdkPixmap).
 Instead, use gdk_cairo_set_source_window() where appropriate.

 Sets the given pixmap as the source pattern for the Cairo context. The pattern has an
 extend mode of CAIRO_EXTEND_NONE and is aligned so that the origin of pixmap is
 pixmap_x, pixmap_y

    cr : a Cairo context
    pixmap : a GdkPixmap
    pixmap_x : X coordinate of location to place upper left corner of pixmap
    pixmap_y : Y coordinate of location to place upper left corner of pixmap

 Since 2.10
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/GDK-CAIRO-SET-SOURCE-PIXMAP invoked with null CR")
      (if (or (null pixmap) (cffi:null-pointer-p pixmap))
	  (error "CFFI/GDK-CAIRO-SET-SOURCE-PIXMAP invoked with null PIXMAP")
	  (cffi:foreign-funcall "gdk_cairo_set_source_pixmap"
				:pointer cr
				:pointer pixmap
				:double pixmap-x :double pixmap-y
				:void)))
  (cairo-status-report cr)
  cr)


(defun cffi/gdk-cairo-set-source-window (cr window x y)
"
 void gdk_cairo_set_source_window (cairo_t *cr, GdkWindow *window,
                                   double x, double y);

 Sets the given window as the source pattern for the Cairo context. The pattern has an
 extend mode of CAIRO_EXTEND_NONE and is aligned so that the origin of window is x, y.
 The window contains all its subwindows when rendering.

 Note that the contents of window are undefined outside of the visible part of window,
 so use this function with care.

    cr : a Cairo context
    window : a GdkWindow
    x : X coordinate of location to place upper left corner of window
    y : Y coordinate of location to place upper left corner of window

 Since 2.24
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/GDK-CAIRO-SET-SOURCE-WINDOW invoked with null CR")
      (if (or (null window) (cffi:null-pointer-p window))
	  (error "CFFI/GDK-CAIRO-SET-SOURCE-WINDOW invoked with null WINDOW")
	  (cffi:foreign-funcall "gdk_cairo_set_source_window"
				:pointer cr
				:pointer window
				:double x :double y
				:void)))
  (cairo-status-report cr)
  cr)


(defun cffi/gdk-cairo-rectangle (cr rectangle)
"
 void gdk_cairo_rectangle (cairo_t *cr, const GdkRectangle *rectangle);

 Adds the given rectangle to the current path of cr.

    cr : a cairo_t
    rectangle : a GdkRectangle

 Since 2.8
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/GDK-CAIRO-RECTANGLE invoked with null CR")
      (if (or (null rectangle) (cffi:null-pointer-p rectangle))
	  (error "CFFI/GDK-CAIRO-RECTANGLE invoked with null RECTANGLE")
	  (cffi:foreign-funcall "gdk_cairo_rectangle"
				:pointer cr
				:pointer rectangle
				:void)))
  (cairo-status-report cr)
  cr)


(defun cffi/gdk-cairo-region (cr region)
"
 void gdk_cairo_region (cairo_t *cr, const GdkRegion *region);

 Adds the given region to the current path of cr.

    cr : a cairo_t
    region : a GdkRegion

 Since 2.8
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/GDK-CAIRO-REGION invoked with null CR")
      (if (or (null region) (cffi:null-pointer-p region))
	  (error "CFFI/GDK-CAIRO-REGION invoked with null REGION")
	  (cffi:foreign-funcall "gdk_cairo_region"
				:pointer cr
				:pointer region
				:void)))
  (cairo-status-report cr)
  cr)


(defun cffi/gdk-cairo-reset-clip (cr drawable)
"
 void gdk_cairo_reset_clip (cairo_t *cr, GdkDrawable *drawable);

 Resets the clip region for a Cairo context created by gdk_cairo_create().

 This resets the clip region to the 'empty' state for the given drawable.
 This is required for non-native windows since a direct call to
 cairo_reset_clip() would unset the clip region inherited from the drawable
 (i.e. the window clip region), and thus let you e.g. draw outside your
 window.

 This is rarely needed though, since most code just create a new cairo_t using
 gdk_cairo_create() each time they want to draw something.

    cr : a cairo_t
    drawable : a GdkDrawable

 Since 2.18
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/GDK-CAIRO-RESET-CLIP invoked with null CR")
      (if (or (null drawable) (cffi:null-pointer-p drawable))
	  (error "CFFI/GDK-CAIRO-RESET-CLIP invoked with null DRAWABLE")
	  (cffi:foreign-funcall "gdk_cairo_reset_clip"
				:pointer cr
				:pointer drawable
				:void)))
  (cairo-status-report cr)
  cr)


;;;;;;;;;;;;;;;;;;;;;; CAIRO_STATUS_T

(cffi:defcenum cairo_status_t
"
 cairo_status_t is used to indicate errors that can occur when using Cairo.
 In some cases it is returned directly by functions. but when using cairo_t,
 the last error, if any, is stored in the context and can be retrieved with
 cairo_status().

 New entries may be added in future versions. Use cairo_status_to_string()
 to get a human-readable representation of an error message.

 CAIRO_STATUS_SUCCESS
     no error has occurred (Since 1.0)
 CAIRO_STATUS_NO_MEMORY
     out of memory (Since 1.0)
 CAIRO_STATUS_INVALID_RESTORE
     cairo_restore() called without matching cairo_save() (Since 1.0)
 CAIRO_STATUS_INVALID_POP_GROUP
     no saved group to pop, i.e. cairo_pop_group() without matching
     cairo_push_group() (Since 1.0)
 CAIRO_STATUS_NO_CURRENT_POINT
     no current point defined (Since 1.0)
 CAIRO_STATUS_INVALID_MATRIX
     invalid matrix (not invertible) (Since 1.0)
 CAIRO_STATUS_INVALID_STATUS
     invalid value for an input cairo_status_t (Since 1.0)
 CAIRO_STATUS_NULL_POINTER
     NULL pointer (Since 1.0)
 CAIRO_STATUS_INVALID_STRING
     input string not valid UTF-8 (Since 1.0)
 CAIRO_STATUS_INVALID_PATH_DATA
     input path data not valid (Since 1.0)
 CAIRO_STATUS_READ_ERROR
     error while reading from input stream (Since 1.0)
 CAIRO_STATUS_WRITE_ERROR
     error while writing to output stream (Since 1.0)
 CAIRO_STATUS_SURFACE_FINISHED
     target surface has been finished (Since 1.0)
 CAIRO_STATUS_SURFACE_TYPE_MISMATCH
     the surface type is not appropriate for the operation (Since 1.0)
 CAIRO_STATUS_PATTERN_TYPE_MISMATCH
     the pattern type is not appropriate for the operation (Since 1.0)
 CAIRO_STATUS_INVALID_CONTENT
     invalid value for an input cairo_content_t (Since 1.0)
 CAIRO_STATUS_INVALID_FORMAT
     invalid value for an input cairo_format_t (Since 1.0)
 CAIRO_STATUS_INVALID_VISUAL
     invalid value for an input Visual* (Since 1.0)
 CAIRO_STATUS_FILE_NOT_FOUND
     file not found (Since 1.0)
 CAIRO_STATUS_INVALID_DASH
     invalid value for a dash setting (Since 1.0)
 CAIRO_STATUS_INVALID_DSC_COMMENT
     invalid value for a DSC comment (Since 1.2)
 CAIRO_STATUS_INVALID_INDEX
     invalid index passed to getter (Since 1.4)
 CAIRO_STATUS_CLIP_NOT_REPRESENTABLE
     clip region not representable in desired format (Since 1.4)
 CAIRO_STATUS_TEMP_FILE_ERROR
     error creating or writing to a temporary file (Since 1.6)
 CAIRO_STATUS_INVALID_STRIDE
     invalid value for stride (Since 1.6)
 CAIRO_STATUS_FONT_TYPE_MISMATCH
     the font type is not appropriate for the operation (Since 1.8)
 CAIRO_STATUS_USER_FONT_IMMUTABLE
     the user-font is immutable (Since 1.8)
 CAIRO_STATUS_USER_FONT_ERROR
     error occurred in a user-font callback function (Since 1.8)
 CAIRO_STATUS_NEGATIVE_COUNT
     negative number used where it is not allowed (Since 1.8)
 CAIRO_STATUS_INVALID_CLUSTERS
     input clusters do not represent the accompanying text and glyph
     array (Since 1.8)
 CAIRO_STATUS_INVALID_SLANT
     invalid value for an input cairo_font_slant_t (Since 1.8)
 CAIRO_STATUS_INVALID_WEIGHT
     invalid value for an input cairo_font_weight_t (Since 1.8)
 CAIRO_STATUS_INVALID_SIZE
     invalid value (typically too big) for the size of the input (surface,
     pattern, etc.) (Since 1.10)
 CAIRO_STATUS_USER_FONT_NOT_IMPLEMENTED
     user-font method not implemented (Since 1.10)
 CAIRO_STATUS_DEVICE_TYPE_MISMATCH
     the device type is not appropriate for the operation (Since 1.10)
 CAIRO_STATUS_DEVICE_ERROR
     an operation to the device caused an unspecified error (Since 1.10)
 CAIRO_STATUS_INVALID_MESH_CONSTRUCTION
     a mesh pattern construction operation was used outside of a
     cairo_mesh_pattern_begin_patch()/cairo_mesh_pattern_end_patch()
     pair (Since 1.12)
 CAIRO_STATUS_DEVICE_FINISHED
     target device has been finished (Since 1.12)
 CAIRO_STATUS_LAST_STATUS
     this is a special value indicating the number of status values defined
     in this enumeration. When using this value, note that the version of cairo
     at run-time may have additional status values defined than the value of this
     symbol at compile-time. (Since 1.10)

 Since 1.0
"
    (:CFFI/CAIRO_STATUS_SUCCESS 0)

    :CFFI/CAIRO_STATUS_NO_MEMORY
    :CFFI/CAIRO_STATUS_INVALID_RESTORE
    :CFFI/CAIRO_STATUS_INVALID_POP_GROUP
    :CFFI/CAIRO_STATUS_NO_CURRENT_POINT
    :CFFI/CAIRO_STATUS_INVALID_MATRIX
    :CFFI/CAIRO_STATUS_INVALID_STATUS
    :CFFI/CAIRO_STATUS_NULL_POINTER
    :CFFI/CAIRO_STATUS_INVALID_STRING
    :CFFI/CAIRO_STATUS_INVALID_PATH_DATA
    :CFFI/CAIRO_STATUS_READ_ERROR
    :CFFI/CAIRO_STATUS_WRITE_ERROR
    :CFFI/CAIRO_STATUS_SURFACE_FINISHED
    :CFFI/CAIRO_STATUS_SURFACE_TYPE_MISMATCH
    :CFFI/CAIRO_STATUS_PATTERN_TYPE_MISMATCH
    :CFFI/CAIRO_STATUS_INVALID_CONTENT
    :CFFI/CAIRO_STATUS_INVALID_FORMAT
    :CFFI/CAIRO_STATUS_INVALID_VISUAL
    :CFFI/CAIRO_STATUS_FILE_NOT_FOUND
    :CFFI/CAIRO_STATUS_INVALID_DASH
    :CFFI/CAIRO_STATUS_INVALID_DSC_COMMENT
    :CFFI/CAIRO_STATUS_INVALID_INDEX
    :CFFI/CAIRO_STATUS_CLIP_NOT_REPRESENTABLE
    :CFFI/CAIRO_STATUS_TEMP_FILE_ERROR
    :CFFI/CAIRO_STATUS_INVALID_STRIDE
    :CFFI/CAIRO_STATUS_FONT_TYPE_MISMATCH
    :CFFI/CAIRO_STATUS_USER_FONT_IMMUTABLE
    :CFFI/CAIRO_STATUS_USER_FONT_ERROR
    :CFFI/CAIRO_STATUS_NEGATIVE_COUNT
    :CFFI/CAIRO_STATUS_INVALID_CLUSTERS
    :CFFI/CAIRO_STATUS_INVALID_SLANT
    :CFFI/CAIRO_STATUS_INVALID_WEIGHT
    :CFFI/CAIRO_STATUS_INVALID_SIZE
    :CFFI/CAIRO_STATUS_USER_FONT_NOT_IMPLEMENTED
    :CFFI/CAIRO_STATUS_DEVICE_TYPE_MISMATCH
    :CFFI/CAIRO_STATUS_DEVICE_ERROR
    :CFFI/CAIRO_STATUS_INVALID_MESH_CONSTRUCTION
    :CFFI/CAIRO_STATUS_DEVICE_FINISHED

    :CFFI/CAIRO_STATUS_LAST_STATUS)


(defconstant +CFFI/CAIRO_STATUS_SUCCESS+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_SUCCESS))
(defconstant +CFFI/CAIRO_STATUS_NO_MEMORY+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_NO_MEMORY))
(defconstant +CFFI/CAIRO_STATUS_INVALID_RESTORE+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_INVALID_RESTORE))
(defconstant +CFFI/CAIRO_STATUS_INVALID_POP_GROUP+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_INVALID_POP_GROUP))
(defconstant +CFFI/CAIRO_STATUS_NO_CURRENT_POINT+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_NO_CURRENT_POINT))
(defconstant +CFFI/CAIRO_STATUS_INVALID_MATRIX+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_INVALID_MATRIX))
(defconstant +CFFI/CAIRO_STATUS_INVALID_STATUS+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_INVALID_STATUS))
(defconstant +CFFI/CAIRO_STATUS_NULL_POINTER+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_NULL_POINTER))
(defconstant +CFFI/CAIRO_STATUS_INVALID_STRING+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_INVALID_STRING))
(defconstant +CFFI/CAIRO_STATUS_INVALID_PATH_DATA+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_INVALID_PATH_DATA))
(defconstant +CFFI/CAIRO_STATUS_READ_ERROR+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_READ_ERROR))
(defconstant +CFFI/CAIRO_STATUS_WRITE_ERROR+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_WRITE_ERROR))
(defconstant +CFFI/CAIRO_STATUS_SURFACE_FINISHED+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_SURFACE_FINISHED))
(defconstant +CFFI/CAIRO_STATUS_SURFACE_TYPE_MISMATCH+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_SURFACE_TYPE_MISMATCH))
(defconstant +CFFI/CAIRO_STATUS_PATTERN_TYPE_MISMATCH+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_PATTERN_TYPE_MISMATCH))
(defconstant +CFFI/CAIRO_STATUS_INVALID_CONTENT+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_INVALID_CONTENT))
(defconstant +CFFI/CAIRO_STATUS_INVALID_FORMAT+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_INVALID_FORMAT))
(defconstant +CFFI/CAIRO_STATUS_INVALID_VISUAL+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_INVALID_VISUAL))
(defconstant +CFFI/CAIRO_STATUS_FILE_NOT_FOUND+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_FILE_NOT_FOUND))
(defconstant +CFFI/CAIRO_STATUS_INVALID_DASH+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_INVALID_DASH))
(defconstant +CFFI/CAIRO_STATUS_INVALID_DSC_COMMENT+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_INVALID_DSC_COMMENT))
(defconstant +CFFI/CAIRO_STATUS_INVALID_INDEX+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_INVALID_INDEX))
(defconstant +CFFI/CAIRO_STATUS_CLIP_NOT_REPRESENTABLE+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_CLIP_NOT_REPRESENTABLE))
(defconstant +CFFI/CAIRO_STATUS_TEMP_FILE_ERROR+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_TEMP_FILE_ERROR))
(defconstant +CFFI/CAIRO_STATUS_INVALID_STRIDE+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_INVALID_STRIDE))
(defconstant +CFFI/CAIRO_STATUS_FONT_TYPE_MISMATCH+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_FONT_TYPE_MISMATCH))
(defconstant +CFFI/CAIRO_STATUS_USER_FONT_IMMUTABLE+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_USER_FONT_IMMUTABLE))
(defconstant +CFFI/CAIRO_STATUS_USER_FONT_ERROR+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_USER_FONT_ERROR))
(defconstant +CFFI/CAIRO_STATUS_NEGATIVE_COUNT+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_NEGATIVE_COUNT))
(defconstant +CFFI/CAIRO_STATUS_INVALID_CLUSTERS+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_INVALID_CLUSTERS))
(defconstant +CFFI/CAIRO_STATUS_INVALID_SLANT+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_INVALID_SLANT))
(defconstant +CFFI/CAIRO_STATUS_INVALID_WEIGHT+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_INVALID_WEIGHT))
(defconstant +CFFI/CAIRO_STATUS_INVALID_SIZE+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_INVALID_SIZE))
(defconstant +CFFI/CAIRO_STATUS_USER_FONT_NOT_IMPLEMENTED+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_USER_FONT_NOT_IMPLEMENTED))
(defconstant +CFFI/CAIRO_STATUS_DEVICE_TYPE_MISMATCH+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_DEVICE_TYPE_MISMATCH))
(defconstant +CFFI/CAIRO_STATUS_DEVICE_ERROR+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_DEVICE_ERROR))
(defconstant +CFFI/CAIRO_STATUS_INVALID_MESH_CONSTRUCTION+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_INVALID_MESH_CONSTRUCTION))
(defconstant +CFFI/CAIRO_STATUS_DEVICE_FINISHED+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_DEVICE_FINISHED))
(defconstant +CFFI/CAIRO_STATUS_LAST_STATUS+
  (cffi:foreign-enum-value 'cairo_status_t :CFFI/CAIRO_STATUS_LAST_STATUS))


(defun cffi/cairo_status_to_string (status)
"
 const char * cairo_status_to_string (cairo_status_t status);

 Provides a human-readable description of a cairo_status_t.

    status : a cairo status

    Returns : a string representation of the status

 Since 1.0
"
  (cffi:foreign-funcall "cairo_status_to_string"
			:int status
			:string))


(defun cffi/cairo_debug_reset_static_data ()
"
 void cairo_debug_reset_static_data (void);

 Resets all static data within cairo to its original state, (ie. identical
 to the state at the time of program invocation). For example, all caches
 within cairo will be flushed empty.

 This function is intended to be useful when using memory-checking tools such
 as valgrind. When valgrind's memcheck analyzes a cairo-using program without
 a call to cairo_debug_reset_static_data(), it will report all data reachable
 via cairo's static objects as 'still reachable'. Calling
 cairo_debug_reset_static_data() just prior to program termination will make
 it easier to get squeaky clean reports from valgrind.

 WARNING: It is only safe to call this function when there are no active cairo
 objects remaining, (ie. the appropriate destroy functions have been called as
 necessary). If there are active cairo objects, this call is likely to cause a
 crash, (eg. an assertion failure due to a hash table being destroyed when
 non-empty).

 Since 1.0
"
  (cffi:foreign-funcall "cairo_debug_reset_static_data"
			:void))

;; See Also

;; cairo_status(), cairo_surface_status(), cairo_pattern_status(),
;; cairo_font_face_status(), cairo_scaled_font_status(), cairo_region_status()

;;;;;;;;;;;;;;;;;;;;;; CAIRO_CONTENT_T

(cffi:defcenum cairo_content_t
"
 cairo_content_t is used to describe the content that a surface will contain,
 whether color information, alpha information (translucence vs. opacity), or
 both.

 Note: The large values here are designed to keep cairo_content_t values
 distinct from cairo_format_t values so that the implementation can detect the
 error if users confuse the two types.

 CAIRO_CONTENT_COLOR
     The surface will hold color content only. (Since 1.0)
 CAIRO_CONTENT_ALPHA
     The surface will hold alpha content only. (Since 1.0)
 CAIRO_CONTENT_COLOR_ALPHA
     The surface will hold color and alpha content. (Since 1.0)

 Since 1.0
"
    (:CFFI/CAIRO_CONTENT_COLOR	#x1000)
    (:CFFI/CAIRO_CONTENT_ALPHA	#x2000)
    (:CFFI/CAIRO_CONTENT_COLOR_ALPHA #x3000))


(defconstant +CFFI/CAIRO_CONTENT_COLOR+ (cffi:foreign-enum-value 'cairo_content_t :CFFI/CAIRO_CONTENT_COLOR))
(defconstant +CFFI/CAIRO_CONTENT_ALPHA+ (cffi:foreign-enum-value 'cairo_content_t :CFFI/CAIRO_CONTENT_ALPHA))
(defconstant +CFFI/CAIRO_CONTENT_COLOR_ALPHA+ (cffi:foreign-enum-value 'cairo_content_t :CFFI/CAIRO_CONTENT_COLOR_ALPHA))


;;;;;;;;;;;;;;;;;;;;;; CAIRO_T

(defun cffi/cairo-create (target)
"
 cairo_t * cairo_create (cairo_surface_t *target);

 Creates a new cairo_t with all graphics state parameters set to
 default values and with target as a target surface. The target
 surface should be constructed with a backend-specific function
 such as cairo_image_surface_create() (or any other
 cairo_backend_surface_create() variant).

 This function references target, so you can immediately call
 cairo_surface_destroy() on it if you don't need to maintain a
 separate reference to it.

    target : target surface for the context

    Returns : a newly allocated cairo_t with a reference count of 1.
              The initial reference count should be released with
    cairo_destroy() when you are done using the cairo_t. This function
    never returns NULL. If memory cannot be allocated, a special cairo_t
    object will be returned on which cairo_status() returns
    CAIRO_STATUS_NO_MEMORY. If you attempt to target a surface which does
    not support writing (such as cairo_mime_surface_t) then a
    CAIRO_STATUS_WRITE_ERROR will be raised. You can use this object
    normally, but no drawing will be done.
"
  (if (or (null target) (cffi:null-pointer-p target))
      (error "CFFI/CAIRO-CREATE invoked with null TARGET")
      (let ((cairo-t (cffi:foreign-funcall "cairo_create"
					   :pointer target
					   :pointer)))
	(cairo-status-report cairo-t)
	cairo-t)))


(defun cffi/cairo-reference (cr)
"
 cairo_t * cairo_reference (cairo_t *cr);

 Increases the reference count on cr by one. This prevents cr from being
 destroyed until a matching call to cairo_destroy() is made.

 The number of references to a cairo_t can be get using
 cairo_get_reference_count().

    cr : a cairo_t

    Returns : the referenced cairo_t.
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO-REFERENCE invoked with null CR")
      (cffi:foreign-funcall "cairo_reference"
			    :pointer cr
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo-destroy (cr)
"
 void cairo_destroy (cairo_t *cr);

 Decreases the reference count on cr by one. If the result is zero, then
 cr and all associated resources are freed. See cairo_reference().

    cr : a cairo_t
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO-DESTROY invoked with null CR")
      (cffi:foreign-funcall "cairo_destroy"
			    :pointer cr
			    :void))
  nil)


(defun cffi/cairo-status (cr)
"
 cairo_status_t cairo_status (cairo_t *cr);

 Checks whether an error has previously occurred for this context.

    cr : a cairo context

    Returns : the current status of this context, see cairo_status_t
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO-STATUS invoked with null CR")
      (cffi:foreign-funcall "cairo_status"
			    :pointer cr
			    :int)))


(defun cffi/cairo-save (cr)
"
 void cairo_save (cairo_t *cr);

 Makes a copy of the current state of cr and saves it on an internal
 stack of saved states for cr. When cairo_restore() is called, cr
 will be restored to the saved state. Multiple calls to cairo_save()
 and cairo_restore() can be nested; each call to cairo_restore()
 restores the state from the matching paired cairo_save().

 It isn't necessary to clear all saved states before a cairo_t is
 freed. If the reference count of a cairo_t drops to zero in response
 to a call to cairo_destroy(), any saved states will be freed along
 with the cairo_t.

    cr : a cairo_t
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO-SAVE invoked with null CR")
      (cffi:foreign-funcall "cairo_save"
			    :pointer cr
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo-restore (cr)
"
 void cairo_restore (cairo_t *cr);

 Restores cr to the state saved by a preceding call to cairo_save()
 and removes that state from the stack of saved states.

    cr : a cairo_t
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO-RESTORE invoked with null CR")
      (cffi:foreign-funcall "cairo_restore"
			    :pointer cr
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo-get-target (cr)
"
 cairo_surface_t * cairo_get_target (cairo_t *cr);

 Gets the target surface for the cairo context as passed to
 cairo_create().

 This function will always return a valid pointer, but the result
 can be a 'nil' surface if cr is already in an error state,
 (ie. cairo_status() != CAIRO_STATUS_SUCCESS). A nil surface is
 indicated by cairo_surface_status() != CAIRO_STATUS_SUCCESS.

    cr : a cairo context

    Returns : the target surface. This object is owned by cairo.
              To keep a reference to it, you must call
    cairo_surface_reference().
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO-GET-TARGET invoked with null CR")
      (let ((surface (cffi:foreign-funcall "cairo_get_target"
					   :pointer cr
					   :pointer)))
	(cairo-surface-status-report surface)
	surface)))



(defun cffi/cairo-push-group (cr)
"
 void cairo_push_group (cairo_t *cr);

 Temporarily redirects drawing to an intermediate surface known as
 a group. The redirection lasts until the group is completed by a
 call to cairo_pop_group() or cairo_pop_group_to_source(). These
 calls provide the result of any drawing to the group as a pattern,
 (either as an explicit object, or set as the source pattern).

 This group functionality can be convenient for performing
 intermediate compositing. One common use of a group is to render
 objects as opaque within the group, (so that they occlude each
 other), and then blend the result with translucence onto the
 destination.

 Groups can be nested arbitrarily deep by making balanced calls to
 cairo_push_group()/cairo_pop_group(). Each call pushes/pops the
 new target group onto/from a stack.

 The cairo_push_group() function calls cairo_save() so that any
 changes to the graphics state will not be visible outside the
 group, (the pop_group functions call cairo_restore()).

 By default the intermediate group will have a content type of
 CAIRO_CONTENT_COLOR_ALPHA. Other content types can be chosen for
 the group by using cairo_push_group_with_content() instead.

 As an example, here is how one might fill and stroke a path with
 translucence, but without any portion of the fill being visible
 under the stroke:

    1   cairo_push_group (cr);
    2   cairo_set_source (cr, fill_pattern);
    3   cairo_fill_preserve (cr);
    4   cairo_set_source (cr, stroke_pattern);
    5   cairo_stroke (cr);
    6   cairo_pop_group_to_source (cr);
    7   cairo_paint_with_alpha (cr, alpha);

    cr : a cairo context
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO-PUSH-GROUP invoked with null CR")
      (cffi:foreign-funcall "cairo_push_group"
			    :pointer cr
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo-push-group-with-content (cr content)
"
 void cairo_push_group_with_content (cairo_t *cr,
                                     cairo_content_t content);

 Temporarily redirects drawing to an intermediate surface known as a
 group. The redirection lasts until the group is completed by a call
 to cairo_pop_group() or cairo_pop_group_to_source(). These calls
 provide the result of any drawing to the group as a pattern, (either
 as an explicit object, or set as the source pattern).

 The group will have a content type of content. The ability to control
 this content type is the only distinction between this function and
 cairo_push_group() which you should see for a more detailed
 description of group rendering.

    cr : a cairo context
    content : a cairo_content_t indicating the type of group that will be created
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO-PUSH-GROUP-WITH-CONTENT invoked with null cr")
      (cffi:foreign-funcall "cairo_push_group_with_content"
			    :pointer cr
                            :int content
			    :void))
  (cairo-status-report cr)
  cr)



(defun cffi/cairo-pop-group (cr)
"
 cairo_pattern_t * cairo_pop_group (cairo_t *cr);

 Terminates the redirection begun by a call to cairo_push_group() or
 cairo_push_group_with_content() and returns a new pattern containing
 the results of all drawing operations performed to the group.

 The cairo_pop_group() function calls cairo_restore(), (balancing a
 call to cairo_save() by the push_group function), so that any changes
 to the graphics state will not be visible outside the group.

    cr : a cairo context

    Returns : a newly created (surface) pattern containing the results
              of all drawing operations performed to the group. The
    caller owns the returned object and should call cairo_pattern_destroy()
    when finished with it.
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO-POP-GROUP invoked with null CR")
      (let ((pattern (cffi:foreign-funcall "cairo_pop_group"
					   :pointer cr
					   :pointer)))
	(cairo-pattern-status-report pattern)
	pattern)))



(defun cffi/cairo-pop-group-to-source (cr)
"
 void cairo_pop_group_to_source (cairo_t *cr);

 Terminates the redirection begun by a call to cairo_push_group() or
 cairo_push_group_with_content() and installs the resulting pattern as
 the source pattern in the given cairo context.

 The behavior of this function is equivalent to the sequence of
 operations:

    1 cairo_pattern_t *group = cairo_pop_group (cr);
    2 cairo_set_source (cr, group);
    3 cairo_pattern_destroy (group);

 but is more convenient as their is no need for a variable to store the
 short-lived pointer to the pattern.

 The cairo_pop_group() function calls cairo_restore(), (balancing a call
 to cairo_save() by the push_group function), so that any changes to the
 graphics state will not be visible outside the group.

    cr : a cairo context
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO-POP-GROUP-TO-SOURCE invoked with null CR")
      (cffi:foreign-funcall "cairo_pop_group_to_source"
			    :pointer cr
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_get_group_target (cr)
"
 cairo_surface_t * cairo_get_group_target (cairo_t *cr);

 Gets the current destination surface for the context. This is either the
 original target surface as passed to cairo_create() or the target surface
 for the current group as started by the most recent call to
 cairo_push_group() or cairo_push_group_with_content().

 This function will always return a valid pointer, but the result can be a
 'nil' surface if cr is already in an error state, (ie. cairo_status() !=
 CAIRO_STATUS_SUCCESS). A nil surface is indicated by cairo_surface_status()
 != CAIRO_STATUS_SUCCESS.

    cr : a cairo context

    Returns : the target surface. This object is owned by cairo. To keep a
              reference to it, you must call cairo_surface_reference().

 Since 1.2
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_GET_GROUP_TARGET invoked with null cr")
      (let ((surface (cffi:foreign-funcall "cairo_get_group_target"
					   :pointer cr
					   :pointer)))
	(cairo-surface-status-report surface)
	surface)))



(defun cffi/cairo_set_source_rgb (cr red green blue)
"
 void cairo_set_source_rgb (cairo_t *cr,
                            double red,
                            double green,
                            double blue);

 Sets the source pattern within cr to an opaque color. This opaque color
 will then be used for any subsequent drawing operation until a new source
 pattern is set.

 The color components are floating point numbers in the range 0 to 1. If
 the values passed in are outside that range, they will be clamped.

 The default source pattern is opaque black, (that is, it is equivalent to
 cairo_set_source_rgb(cr, 0.0, 0.0, 0.0)).

    cr : a cairo context
    red : red component of color
    green : green component of color
    blue : blue component of color

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_SET_SOURCE_RGB invoked with null CAIRO-T")
      (cffi:foreign-funcall "cairo_set_source_rgb"
			    :pointer cr
			    :double red
			    :double green
			    :double blue
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_set_source_rgba (cr red green blue alpha)
"
 void cairo_set_source_rgba (cairo_t *cr,
                             double red,  double green,
                             double blue, double alpha);

 Sets the source pattern within cr to a translucent color. This color will
 then be used for any subsequent drawing operation until a new source pattern
 is set.

 The color and alpha components are floating point numbers in the range 0 to 1.
 If the values passed in are outside that range, they will be clamped.

 The default source pattern is opaque black, (that is, it is equivalent to
 cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0)).

    cr : a cairo context
    red : red component of color
    green : green component of color
    blue : blue component of color
    alpha : alpha component of color

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_SET_SOURCE_RGBA invoked with null cr")
      (cffi:foreign-funcall "cairo_set_source_rgba"
			    :pointer cr
			    :double red
			    :double green
			    :double blue
			    :double alpha
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_set_source (cr source)
"
 void cairo_set_source (cairo_t *cr, cairo_pattern_t *source);

 Sets the source pattern within cr to source. This pattern will then be
 used for any subsequent drawing operation until a new source pattern is set.

 Note: The pattern's transformation matrix will be locked to the user space in
 effect at the time of cairo_set_source(). This means that further
 modifications of the current transformation matrix will not affect the source
 pattern. See cairo_pattern_set_matrix().

 The default source pattern is a solid pattern that is opaque black, (that is,
 it is equivalent to cairo_set_source_rgb(cr, 0.0, 0.0, 0.0)).

    cr : a cairo context
    source : a cairo_pattern_t to be used as the source for subsequent drawing
             operations.

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_SET_SOURCE invoked with null CR")
      (if (or (null source) (cffi:null-pointer-p source))
	  (error "CFFI/CAIRO_SET_SOURCE invoked with null SOURCE")
	  (cffi:foreign-funcall "cairo_set_source"
				:pointer cr
				:pointer source
				:void)))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_set_source_surface (cr surface x y)
"
 void cairo_set_source_surface (cairo_t *cr,
                                cairo_surface_t *surface,
                                double x,
                                double y);

 This is a convenience function for creating a pattern from surface and
 setting it as the source in cr with cairo_set_source().

 The x and y parameters give the user-space coordinate at which the surface
 origin should appear. (The surface origin is its upper-left corner before
 any transformation has been applied.) The x and y parameters are negated
 and then set as translation values in the pattern matrix.

 Other than the initial translation pattern matrix, as described above, all
 other pattern attributes, (such as its extend mode), are set to the default
 values as in cairo_pattern_create_for_surface(). The resulting pattern can
 be queried with cairo_get_source() so that these attributes can be modified
 if desired, (eg. to create a repeating pattern with
 cairo_pattern_set_extend()).

    cr : a cairo context
    surface : a surface to be used to set the source pattern
    x : User-space X coordinate for surface origin
    y : User-space Y coordinate for surface origin

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_SET_SOURCE_SURFACE invoked with null CR")
      (if (or (null surface) (cffi:null-pointer-p surface))
	  (error "CFFI/CAIRO_SET_SOURCE_SURFACE invoked with null SURFACE")
	  (cffi:foreign-funcall "cairo_set_source_surface"
				:pointer cr
				:pointer surface
				:double x
				:double y)))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_get_source (cr)
"
 cairo_pattern_t * cairo_get_source (cairo_t *cr);

 Gets the current source pattern for cr.

    cr : a cairo context

    Returns : the current source pattern. This object is owned by cairo.
              To keep a reference to it, you must call
    cairo_pattern_reference().

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_GET_SOURCE invoked with null CR")
      (let ((pattern (cffi:foreign-funcall "cairo_get_source"
					   :pointer cr
					   :pointer)))
	(cairo-pattern-status-report pattern)
	pattern)))



(cffi:defcenum cairo_antialias_t
"
 Specifies the type of antialiasing to do when rendering text or shapes.

 As it is not necessarily clear from the above what advantages a particular
 antialias method provides, since 1.12, there is also a set of hints:
 CAIRO_ANTIALIAS_FAST: Allow the backend to degrade raster quality for speed
 CAIRO_ANTIALIAS_GOOD: A balance between speed and quality
 CAIRO_ANTIALIAS_BEST: A high-fidelity, but potentially slow, raster mode

 These make no guarantee on how the backend will perform its rasterisation
 (if it even rasterises!), nor that they have any differing effect other than
 to enable some form of antialiasing. In the case of glyph rendering,
 CAIRO_ANTIALIAS_FAST and CAIRO_ANTIALIAS_GOOD will be mapped to
 CAIRO_ANTIALIAS_GRAY, with CAIRO_ANTALIAS_BEST being equivalent to
 CAIRO_ANTIALIAS_SUBPIXEL.

 The interpretation of CAIRO_ANTIALIAS_DEFAULT is left entirely up to the
 backend, typically this will be similar to CAIRO_ANTIALIAS_GOOD.

 CAIRO_ANTIALIAS_DEFAULT
	Use the default antialiasing for the subsystem and target device,
        since 1.0

 CAIRO_ANTIALIAS_NONE
	Use a bilevel alpha mask, since 1.0

 CAIRO_ANTIALIAS_GRAY
	Perform single-color antialiasing (using shades of gray for black
        text on a white background, for example), since 1.0

 CAIRO_ANTIALIAS_SUBPIXEL
	Perform antialiasing by taking advantage of the order of subpixel
        elements on devices such as LCD panels, since 1.0

 CAIRO_ANTIALIAS_FAST
	Hint that the backend should perform some antialiasing but prefer
        speed over quality, since 1.12

 CAIRO_ANTIALIAS_GOOD
	The backend should balance quality against performance, since 1.12

 CAIRO_ANTIALIAS_BEST
	Hint that the backend should render at the highest quality,
        sacrificing speed if necessary, since 1.12

 Since 1.0
"
    :CFFI/CAIRO_ANTIALIAS_DEFAULT

    ;; method
    :CFFI/CAIRO_ANTIALIAS_NONE
    :CFFI/CAIRO_ANTIALIAS_GRAY
    :CFFI/CAIRO_ANTIALIAS_SUBPIXEL

    ;; hints
    :CFFI/CAIRO_ANTIALIAS_FAST
    :CFFI/CAIRO_ANTIALIAS_GOOD
    :CFFI/CAIRO_ANTIALIAS_BEST)


(defconstant +CFFI/CAIRO_ANTIALIAS_DEFAULT+  (cffi:foreign-enum-value 'cairo_antialias_t :CFFI/CAIRO_ANTIALIAS_DEFAULT))
(defconstant +CFFI/CAIRO_ANTIALIAS_NONE+     (cffi:foreign-enum-value 'cairo_antialias_t :CFFI/CAIRO_ANTIALIAS_NONE))
(defconstant +CFFI/CAIRO_ANTIALIAS_GRAY+     (cffi:foreign-enum-value 'cairo_antialias_t :CFFI/CAIRO_ANTIALIAS_GRAY))
(defconstant +CFFI/CAIRO_ANTIALIAS_SUBPIXEL+ (cffi:foreign-enum-value 'cairo_antialias_t :CFFI/CAIRO_ANTIALIAS_SUBPIXEL))
(defconstant +CFFI/CAIRO_ANTIALIAS_FAST+     (cffi:foreign-enum-value 'cairo_antialias_t :CFFI/CAIRO_ANTIALIAS_FAST))
(defconstant +CFFI/CAIRO_ANTIALIAS_GOOD+     (cffi:foreign-enum-value 'cairo_antialias_t :CFFI/CAIRO_ANTIALIAS_GOOD))
(defconstant +CFFI/CAIRO_ANTIALIAS_BEST+     (cffi:foreign-enum-value 'cairo_antialias_t :CFFI/CAIRO_ANTIALIAS_BEST))


(defun cffi/cairo_set_antialias (cr antialias)
"
 void cairo_set_antialias (cairo_t *cr, cairo_antialias_t antialias);

 Set the antialiasing mode of the rasterizer used for drawing shapes. This
 value is a hint, and a particular backend may or may not support a particular
 value. At the current time, no backend supports CAIRO_ANTIALIAS_SUBPIXEL
 when drawing shapes.

 Note that this option does not affect text rendering, instead see
 cairo_font_options_set_antialias().

    cr : a cairo_t
    antialias : the new antialiasing mode

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_SET_ANTIALIAS invoked with null CR")
      (cffi:foreign-funcall "cairo_set_antialias"
			    :pointer cr
			    cairo_antialias_t antialias
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_get_antialias (cr)
"
 cairo_antialias_t cairo_get_antialias (cairo_t *cr);

 Gets the current shape antialiasing mode, as set by
 cairo_set_antialias().

    cr : a cairo context

    Returns : the current shape antialiasing mode.

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_GET_ANTIALIAS invoked with null CR")
      (let ((antialias (cffi:foreign-funcall "cairo_get_antialias"
					     :pointer cr
					     cairo_antialias_t)))
	antialias)))


(defun cffi/cairo_set_dash (cr dashes num-dashes offset)
"
 void cairo_set_dash (cairo_t *cr,
                      const double *dashes,
                      int num_dashes,
                      double offset);

 Sets the dash pattern to be used by cairo_stroke(). A dash pattern is
 specified by dashes, an array of positive values. Each value provides the
 length of alternate 'on' and 'off' portions of the stroke. The offset
 specifies an offset into the pattern at which the stroke begins.

 Each 'on' segment will have caps applied as if the segment were a separate
 sub-path. In particular, it is valid to use an 'on' length of 0.0 with
 CAIRO_LINE_CAP_ROUND or CAIRO_LINE_CAP_SQUARE in order to distributed dots
 or squares along a path.

 Note: The length values are in user-space units as evaluated at the time of
 stroking. This is not necessarily the same as the user space at the time of
 cairo_set_dash().

 If num_dashes is 0 dashing is disabled.

 If num_dashes is 1 a symmetric pattern is assumed with alternating on and off
 portions of the size specified by the single value in dashes.

 If any value in dashes is negative, or if all values are 0, then cr will be
 put into an error state with a status of CAIRO_STATUS_INVALID_DASH.

    cr : a cairo context
    dashes : an array specifying alternate lengths of on and off stroke
             portions
    num_dashes : the length of the dashes array
    offset : an offset into the dash pattern at which the stroke should
             start

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_SET_DASH invoked with null CR")
      (let* ((dashes (or dashes (cffi:null-pointer))))
	(cffi:foreign-funcall "cairo_set_dash"
			      :pointer cr
			      :pointer dashes
			      :int num-dashes
			      :double offset
			      :void)))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_get_dash_count (cr)
"
 int cairo_get_dash_count (cairo_t *cr);

 This function returns the length of the dash array in cr (0 if dashing is
 not currently in effect).

 See also cairo_set_dash() and cairo_get_dash().

    cr : a cairo_t

    Returns : the length of the dash array, or 0 if no dash array set.

 Since 1.4
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_GET_DASH_COUNT invoked with null CR")
      (cffi:foreign-funcall "cairo_get_dash_count"
			    :pointer cr
			    :int)))


(defun cffi/cairo_get_dash (cr dashes offset)
"
 void cairo_get_dash (cairo_t *cr, double *dashes, double *offset);

 Gets the current dash array. If not NULL, dashes should be big enough to hold
 at least the number of values returned by cairo_get_dash_count().

    cr : a cairo_t
    dashes : return value for the dash array, or NULL
    offset : return value for the current dash offset, or NULL

 Since 1.4
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_GET_DASH invoked with null CR")
      (let* ((dashes (or dashes (cffi:null-pointer)))
	     (offset (or offset (cffi:null-pointer))))
	(cffi:foreign-funcall "cairo_get_dash"
			      :pointer cr
			      :pointer dashes
			      :pointer offset
			      :void)))
  (cairo-status-report cr)
  cr)


(cffi:defcenum cairo_fill_rule_t
"
 cairo_fill_rule_t is used to select how paths are filled. For both fill
 rules, whether or not a point is included in the fill is determined by
 taking a ray from that point to infinity and looking at intersections with
 the path. The ray can be in any direction, as long as it doesn't pass
 through the end point of a segment or have a tricky intersection such as
 intersecting tangent to the path. (Note that filling is not actually
 implemented in this way. This is just a description of the rule that is
 applied.)

 The default fill rule is CAIRO_FILL_RULE_WINDING.

 New entries may be added in future versions.

 CAIRO_FILL_RULE_WINDING
	If the path crosses the ray from left-to-right, counts +1. If the
        path crosses the ray from right to left, counts -1. (Left and right
 are determined from the perspective of looking along the ray from the
 starting point.) If the total count is non-zero, the point will be filled.
 (Since 1.0)

 CAIRO_FILL_RULE_EVEN_ODD
	Counts the total number of intersections, without regard to the
        orientation of the contour. If the total number of intersections is
 odd, the point will be filled. (Since 1.0)

 Since 1.0
"
  :CFFI/CAIRO_FILL_RULE_WINDING
  :CFFI/CAIRO_FILL_RULE_EVEN_ODD)


(defconstant +CFFI/CAIRO_FILL_RULE_WINDING+
  (cffi:foreign-enum-value 'cairo_fill_rule_t :CFFI/CAIRO_FILL_RULE_WINDING))
(defconstant +CFFI/CAIRO_FILL_RULE_EVEN_ODD+
  (cffi:foreign-enum-value 'cairo_fill_rule_t :CFFI/CAIRO_FILL_RULE_EVEN_ODD))


(defun cffi/cairo_set_fill_rule (cr fill-rule)
"
 void cairo_set_fill_rule (cairo_t *cr, cairo_fill_rule_t fill_rule);

 Set the current fill rule within the cairo context. The fill rule is
 used to determine which regions are inside or outside a complex
 (potentially self-intersecting) path. The current fill rule affects
 both cairo_fill() and cairo_clip(). See cairo_fill_rule_t for details
 on the semantics of each available fill rule.

 The default fill rule is CAIRO_FILL_RULE_WINDING.

    cr : a cairo_t
    fill_rule : a fill rule, specified as a cairo_fill_rule_t

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_SET_FILL_RULE invoked with null CR")
      (cffi:foreign-funcall "cairo_set_fill_rule"
			    :pointer cr
			    cairo_fill_rule_t fill-rule
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_get_fill_rule (cr)
"
 cairo_fill_rule_t cairo_get_fill_rule (cairo_t *cr);

 Gets the current fill rule, as set by cairo_set_fill_rule().

    cr : a cairo context

    Returns : the current fill rule.

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_GET_FILL_RULE invoked with null CR")
      (cffi:foreign-funcall "cairo_get_fill_rule"
			    :pointer cr
			    cairo_fill_rule_t)))


;;; FIXME: SHOULD THESE ENUMS BE CALLED "cffi/...."?

(cffi:defcenum cairo_line_cap_t
"
 Specifies how to render the endpoints of the path when stroking.

 The default line cap style is CAIRO_LINE_CAP_BUTT.

 CAIRO_LINE_CAP_BUTT
	start(stop) the line exactly at the start(end) point (Since 1.0)

 CAIRO_LINE_CAP_ROUND
	use a round ending, the center of the circle is the end point (Since 1.0)

 CAIRO_LINE_CAP_SQUARE
	use squared ending, the center of the square is the end point (Since 1.0)

 Since 1.0
"
  :CFFI/CAIRO_LINE_CAP_BUTT
  :CFFI/CAIRO_LINE_CAP_ROUND
  :CFFI/CAIRO_LINE_CAP_SQUARE)


(defconstant +CFFI/CAIRO_LINE_CAP_BUTT+   (cffi:foreign-enum-value 'cairo_line_cap_t :CFFI/CAIRO_LINE_CAP_BUTT))
(defconstant +CFFI/CAIRO_LINE_CAP_ROUND+  (cffi:foreign-enum-value 'cairo_line_cap_t :CFFI/CAIRO_LINE_CAP_ROUND))
(defconstant +CFFI/CAIRO_LINE_CAP_SQUARE+ (cffi:foreign-enum-value 'cairo_line_cap_t :CFFI/CAIRO_LINE_CAP_SQUARE))


(defun cffi/cairo_set_line_cap (cr line-cap)
"
 void cairo_set_line_cap (cairo_t *cr, cairo_line_cap_t line_cap);

 Sets the current line cap style within the cairo context. See cairo_line_cap_t
 for details about how the available line cap styles are drawn.

 As with the other stroke parameters, the current line cap style is examined by
 cairo_stroke(), cairo_stroke_extents(), and cairo_stroke_to_path(), but does
 not have any effect during path construction.

 The default line cap style is CAIRO_LINE_CAP_BUTT.

    cr : a cairo context
    line_cap : a line cap style

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_SET_LINE_CAP invoked with null CR")
      (cffi:foreign-funcall "cairo_set_line_cap"
			    :pointer cr
			    cairo_line_cap_t line-cap
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_get_line_cap (cr)
"
 cairo_line_cap_t cairo_get_line_cap (cairo_t *cr);

 Gets the current line cap style, as set by cairo_set_line_cap().

    cr : a cairo context

    Returns : the current line cap style.

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_GET_LINE_CAP invoked with null CR")
      (cffi:foreign-funcall "cairo_get_line_cap"
			    :pointer cr
			    cairo_line_cap_t)))


(cffi:defcenum cairo_line_join_t
"
 Specifies how to render the junction of two lines when stroking.

 The default line join style is CAIRO_LINE_JOIN_MITER.

 CAIRO_LINE_JOIN_MITER
	use a sharp (angled) corner, see cairo_set_miter_limit() (Since 1.0)

 CAIRO_LINE_JOIN_ROUND
	use a rounded join, the center of the circle is the joint point (Since 1.0)

 CAIRO_LINE_JOIN_BEVEL
	use a cut-off join, the join is cut off at half the line width from
        the joint point (Since 1.0)

 Since 1.0
"
  :CFFI/CAIRO_LINE_JOIN_MITER
  :CFFI/CAIRO_LINE_JOIN_ROUND
  :CFFI/CAIRO_LINE_JOIN_BEVEL)


(defconstant +CFFI/CAIRO_LINE_JOIN_MITER+ (cffi:foreign-enum-value 'cairo_line_join_t :CFFI/CAIRO_LINE_JOIN_MITER))
(defconstant +CFFI/CAIRO_LINE_JOIN_ROUND+ (cffi:foreign-enum-value 'cairo_line_join_t :CFFI/CAIRO_LINE_JOIN_ROUND))
(defconstant +CFFI/CAIRO_LINE_JOIN_BEVEL+ (cffi:foreign-enum-value 'cairo_line_join_t :CFFI/CAIRO_LINE_JOIN_BEVEL))


(defun cffi/cairo_set_line_join (cr line-join)
"
 void cairo_set_line_join (cairo_t *cr, cairo_line_join_t line_join);

 Sets the current line join style within the cairo context. See
 cairo_line_join_t for details about how the available line join styles
 are drawn.

 As with the other stroke parameters, the current line join style is
 examined by cairo_stroke(), cairo_stroke_extents(), and
 cairo_stroke_to_path(), but does not have any effect during path
 construction.

 The default line join style is CAIRO_LINE_JOIN_MITER.

    cr : a cairo context
    line_join : a line join style

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_SET_LINE_JOIN invoked with null CR")
      (cffi:foreign-funcall "cairo_set_line_join"
			    :pointer cr
			    cairo_line_join_t line-join
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_get_line_join (cr)
"
 cairo_line_join_t cairo_get_line_join (cairo_t *cr);

 Gets the current line join style, as set by cairo_set_line_join().

    cr : a cairo context

    Returns : the current line join style.

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_GET_LINE_JOIN invoked with null CR")
      (cffi:foreign-funcall "cairo_get_line_join"
			    :pointer cr
			    cairo_line_join_t)))


(defun cffi/cairo_set_line_width (cr width)
"
 void cairo_set_line_width (cairo_t *cr, double width);

 Sets the current line width within the cairo context. The line width value
 specifies the diameter of a pen that is circular in user space, (though
 device-space pen may be an ellipse in general due to scaling/shear/rotation
 of the CTM).

 Note: When the description above refers to user space and CTM it refers to
 the user space and CTM in effect at the time of the stroking operation, not
 the user space and CTM in effect at the time of the call to cairo_set_line_width().
 The simplest usage makes both of these spaces identical. That is, if there is
 no change to the CTM between a call to cairo_set_line_width() and the stroking
 operation, then one can just pass user-space values to cairo_set_line_width()
 and ignore this note.

 As with the other stroke parameters, the current line width is examined by
 cairo_stroke(), cairo_stroke_extents(), and cairo_stroke_to_path(), but does
 not have any effect during path construction.

 The default line width value is 2.0.

    cr : a cairo_t
    width : a line width

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_SET_LINE_WIDTH invoked with null CR")
      (cffi:foreign-funcall "cairo_set_line_width"
			    :pointer cr
			    :double width
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_get_line_width (cr)
"
 double cairo_get_line_width (cairo_t *cr);

 This function returns the current line width value exactly as set
 by cairo_set_line_width(). Note that the value is unchanged even
 if the CTM has changed between the calls to cairo_set_line_width()
 and cairo_get_line_width().

    cr : a cairo context

    Returns : the current line width.

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_GET_LINE_WIDTH invoked with null CR")
      (cffi:foreign-funcall "cairo_get_line_width"
			    :pointer cr
			    :double)))


(defun cffi/cairo_set_miter_limit (cr limit)
"
 void cairo_set_miter_limit (cairo_t *cr, double limit);

 Sets the current miter limit within the cairo context.

 If the current line join style is set to CAIRO_LINE_JOIN_MITER
 (see cairo_set_line_join()), the miter limit is used to determine
 whether the lines should be joined with a bevel instead of a miter.
 Cairo divides the length of the miter by the line width. If the
 result is greater than the miter limit, the style is converted to a
 bevel.

 As with the other stroke parameters, the current line miter limit
 is examined by cairo_stroke(), cairo_stroke_extents(), and
 cairo_stroke_to_path(), but does not have any effect during path
 construction.

 The default miter limit value is 10.0, which will convert joins
 with interior angles less than 11 degrees to bevels instead of
 miters. For reference, a miter limit of 2.0 makes the miter cutoff
 at 60 degrees, and a miter limit of 1.414 makes the cutoff at 90
 degrees.

 A miter limit for a desired angle can be computed as:
 miter limit = 1/sin(angle/2)

    cr : a cairo context
    limit : miter limit to set

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_SET_MITER_LIMIT invoked with null CR")
      (cffi:foreign-funcall "cairo_set_miter_limit"
			    :pointer cr
			    :double limit
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_get_miter_limit (cr)
"
 double cairo_get_miter_limit (cairo_t *cr);

 Gets the current miter limit, as set by cairo_set_miter_limit().

    cr : a cairo context

    Returns : the current miter limit.

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_GET_MITER_LIMIT invoked with null CR")
      (cffi:foreign-funcall "cairo_get_miter_limit"
			    :pointer cr
			    :double)))


(cffi:defcenum cairo_operator_t
"
 cairo_operator_t is used to set the compositing operator for all cairo drawing
 operations.

 The default operator is CAIRO_OPERATOR_OVER.

 The operators marked as unbounded modify their destination even outside of the
 mask layer (that is, their effect is not bound by the mask layer). However,
 their effect can still be limited by way of clipping.

 To keep things simple, the operator descriptions here document the behavior for
 when both source and destination are either fully transparent or fully opaque.
 The actual implementation works for translucent layers too. For a more detailed
 explanation of the effects of each operator, including the mathematical
 definitions, see http://cairographics.org/operators/.

 CAIRO_OPERATOR_CLEAR
	clear destination layer (bounded) (Since 1.0)

 CAIRO_OPERATOR_SOURCE
	replace destination layer (bounded) (Since 1.0)

 CAIRO_OPERATOR_OVER
	draw source layer on top of destination layer (bounded) (Since 1.0)

 CAIRO_OPERATOR_IN
	draw source where there was destination content (unbounded) (Since 1.0)

 CAIRO_OPERATOR_OUT
	draw source where there was no destination content (unbounded) (Since 1.0)

 CAIRO_OPERATOR_ATOP
	draw source on top of destination content and only there (Since 1.0)

 CAIRO_OPERATOR_DEST
	ignore the source (Since 1.0)

 CAIRO_OPERATOR_DEST_OVER
	draw destination on top of source (Since 1.0)

 CAIRO_OPERATOR_DEST_IN
	leave destination only where there was source content (unbounded) (Since 1.0)

 CAIRO_OPERATOR_DEST_OUT
	leave destination only where there was no source content (Since 1.0)

 CAIRO_OPERATOR_DEST_ATOP
	leave destination on top of source content and only there (unbounded)
        (Since 1.0)

 CAIRO_OPERATOR_XOR
	source and destination are shown where there is only one of them (Since 1.0)

 CAIRO_OPERATOR_ADD
	source and destination layers are accumulated (Since 1.0)

 CAIRO_OPERATOR_SATURATE
	like over, but assuming source and dest are disjoint geometries
        (Since 1.0)

 CAIRO_OPERATOR_MULTIPLY
	source and destination layers are multiplied. This causes the result
        to be at least as dark as the darker inputs. (Since 1.10)

 CAIRO_OPERATOR_SCREEN
	source and destination are complemented and multiplied. This causes
        the result to be at least as light as the lighter inputs. (Since 1.10)

 CAIRO_OPERATOR_OVERLAY
	multiplies or screens, depending on the lightness of the destination
        color. (Since 1.10)

 CAIRO_OPERATOR_DARKEN
	replaces the destination with the source if it is darker, otherwise
        keeps the source. (Since 1.10)

 CAIRO_OPERATOR_LIGHTEN
	replaces the destination with the source if it is lighter, otherwise
        keeps the source. (Since 1.10)

 CAIRO_OPERATOR_COLOR_DODGE
	brightens the destination color to reflect the source color. (Since 1.10)

 CAIRO_OPERATOR_COLOR_BURN
	darkens the destination color to reflect the source color. (Since 1.10)

 CAIRO_OPERATOR_HARD_LIGHT
	Multiplies or screens, dependent on source color. (Since 1.10)

 CAIRO_OPERATOR_SOFT_LIGHT
	Darkens or lightens, dependent on source color. (Since 1.10)

 CAIRO_OPERATOR_DIFFERENCE
	Takes the difference of the source and destination color. (Since 1.10)

 CAIRO_OPERATOR_EXCLUSION
	Produces an effect similar to difference, but with lower contrast. (Since 1.10)

 CAIRO_OPERATOR_HSL_HUE
	Creates a color with the hue of the source and the saturation and luminosity
        of the target. (Since 1.10)

 CAIRO_OPERATOR_HSL_SATURATION
	Creates a color with the saturation of the source and the hue and luminosity
        of the target. Painting with this mode onto a gray area produces no change.
        (Since 1.10)

 CAIRO_OPERATOR_HSL_COLOR
	Creates a color with the hue and saturation of the source and the luminosity
        of the target. This preserves the gray levels of the target and is useful
        for coloring monochrome images or tinting color images. (Since 1.10)

 CAIRO_OPERATOR_HSL_LUMINOSITY
	Creates a color with the luminosity of the source and the hue and saturation
        of the target. This produces an inverse effect to CAIRO_OPERATOR_HSL_COLOR.
        (Since 1.10)

 Since 1.0
"
  :CFFI/CAIRO_OPERATOR_CLEAR
  :CFFI/CAIRO_OPERATOR_SOURCE
  :CFFI/CAIRO_OPERATOR_OVER
  :CFFI/CAIRO_OPERATOR_IN
  :CFFI/CAIRO_OPERATOR_OUT
  :CFFI/CAIRO_OPERATOR_ATOP

  :CFFI/CAIRO_OPERATOR_DEST
  :CFFI/CAIRO_OPERATOR_DEST_OVER
  :CFFI/CAIRO_OPERATOR_DEST_IN
  :CFFI/CAIRO_OPERATOR_DEST_OUT
  :CFFI/CAIRO_OPERATOR_DEST_ATOP

  :CFFI/CAIRO_OPERATOR_XOR
  :CFFI/CAIRO_OPERATOR_ADD
  :CFFI/CAIRO_OPERATOR_SATURATE

  :CFFI/CAIRO_OPERATOR_MULTIPLY
  :CFFI/CAIRO_OPERATOR_SCREEN
  :CFFI/CAIRO_OPERATOR_OVERLAY
  :CFFI/CAIRO_OPERATOR_DARKEN
  :CFFI/CAIRO_OPERATOR_LIGHTEN
  :CFFI/CAIRO_OPERATOR_COLOR_DODGE
  :CFFI/CAIRO_OPERATOR_COLOR_BURN
  :CFFI/CAIRO_OPERATOR_HARD_LIGHT
  :CFFI/CAIRO_OPERATOR_SOFT_LIGHT
  :CFFI/CAIRO_OPERATOR_DIFFERENCE
  :CFFI/CAIRO_OPERATOR_EXCLUSION
  :CFFI/CAIRO_OPERATOR_HSL_HUE
  :CFFI/CAIRO_OPERATOR_HSL_SATURATION
  :CFFI/CAIRO_OPERATOR_HSL_COLOR
  :CFFI/CAIRO_OPERATOR_HSL_LUMINOSITY)


(defconstant +CFFI/CAIRO_OPERATOR_CLEAR+ (cffi:foreign-enum-value 'cairo_operator_t :CFFI/CAIRO_OPERATOR_CLEAR))
(defconstant +CFFI/CAIRO_OPERATOR_SOURCE+ (cffi:foreign-enum-value 'cairo_operator_t :CFFI/CAIRO_OPERATOR_SOURCE))
(defconstant +CFFI/CAIRO_OPERATOR_OVER+ (cffi:foreign-enum-value 'cairo_operator_t :CFFI/CAIRO_OPERATOR_OVER))
(defconstant +CFFI/CAIRO_OPERATOR_IN+ (cffi:foreign-enum-value 'cairo_operator_t :CFFI/CAIRO_OPERATOR_IN))
(defconstant +CFFI/CAIRO_OPERATOR_OUT+ (cffi:foreign-enum-value 'cairo_operator_t :CFFI/CAIRO_OPERATOR_OUT))
(defconstant +CFFI/CAIRO_OPERATOR_ATOP+ (cffi:foreign-enum-value 'cairo_operator_t :CFFI/CAIRO_OPERATOR_ATOP))

(defconstant +CFFI/CAIRO_OPERATOR_DEST+ (cffi:foreign-enum-value 'cairo_operator_t :CFFI/CAIRO_OPERATOR_DEST))
(defconstant +CFFI/CAIRO_OPERATOR_DEST_OVER+ (cffi:foreign-enum-value 'cairo_operator_t :CFFI/CAIRO_OPERATOR_DEST_OVER))
(defconstant +CFFI/CAIRO_OPERATOR_DEST_IN+ (cffi:foreign-enum-value 'cairo_operator_t :CFFI/CAIRO_OPERATOR_DEST_IN))
(defconstant +CFFI/CAIRO_OPERATOR_DEST_OUT+ (cffi:foreign-enum-value 'cairo_operator_t :CFFI/CAIRO_OPERATOR_DEST_OUT))
(defconstant +CFFI/CAIRO_OPERATOR_DEST_ATOP+ (cffi:foreign-enum-value 'cairo_operator_t :CFFI/CAIRO_OPERATOR_DEST_ATOP))

(defconstant +CFFI/CAIRO_OPERATOR_XOR+ (cffi:foreign-enum-value 'cairo_operator_t :CFFI/CAIRO_OPERATOR_XOR))
(defconstant +CFFI/CAIRO_OPERATOR_ADD+ (cffi:foreign-enum-value 'cairo_operator_t :CFFI/CAIRO_OPERATOR_ADD))
(defconstant +CFFI/CAIRO_OPERATOR_SATURATE+ (cffi:foreign-enum-value 'cairo_operator_t :CFFI/CAIRO_OPERATOR_SATURATE))

(defconstant +CFFI/CAIRO_OPERATOR_MULTIPLY+ (cffi:foreign-enum-value 'cairo_operator_t :CFFI/CAIRO_OPERATOR_MULTIPLY))
(defconstant +CFFI/CAIRO_OPERATOR_SCREEN+ (cffi:foreign-enum-value 'cairo_operator_t :CFFI/CAIRO_OPERATOR_SCREEN))
(defconstant +CFFI/CAIRO_OPERATOR_OVERLAY+ (cffi:foreign-enum-value 'cairo_operator_t :CFFI/CAIRO_OPERATOR_OVERLAY))
(defconstant +CFFI/CAIRO_OPERATOR_DARKEN+ (cffi:foreign-enum-value 'cairo_operator_t :CFFI/CAIRO_OPERATOR_DARKEN))
(defconstant +CFFI/CAIRO_OPERATOR_LIGHTEN+ (cffi:foreign-enum-value 'cairo_operator_t :CFFI/CAIRO_OPERATOR_LIGHTEN))
(defconstant +CFFI/CAIRO_OPERATOR_COLOR_DODGE+ (cffi:foreign-enum-value 'cairo_operator_t :CFFI/CAIRO_OPERATOR_COLOR_DODGE))
(defconstant +CFFI/CAIRO_OPERATOR_COLOR_BURN+ (cffi:foreign-enum-value 'cairo_operator_t :CFFI/CAIRO_OPERATOR_COLOR_BURN))
(defconstant +CFFI/CAIRO_OPERATOR_HARD_LIGHT+ (cffi:foreign-enum-value 'cairo_operator_t :CFFI/CAIRO_OPERATOR_HARD_LIGHT))
(defconstant +CFFI/CAIRO_OPERATOR_SOFT_LIGHT+ (cffi:foreign-enum-value 'cairo_operator_t :CFFI/CAIRO_OPERATOR_SOFT_LIGHT))
(defconstant +CFFI/CAIRO_OPERATOR_DIFFERENCE+ (cffi:foreign-enum-value 'cairo_operator_t :CFFI/CAIRO_OPERATOR_DIFFERENCE))
(defconstant +CFFI/CAIRO_OPERATOR_EXCLUSION+ (cffi:foreign-enum-value 'cairo_operator_t :CFFI/CAIRO_OPERATOR_EXCLUSION))
(defconstant +CFFI/CAIRO_OPERATOR_HSL_HUE+ (cffi:foreign-enum-value 'cairo_operator_t :CFFI/CAIRO_OPERATOR_HSL_HUE))
(defconstant +CFFI/CAIRO_OPERATOR_HSL_SATURATION+ (cffi:foreign-enum-value 'cairo_operator_t :CFFI/CAIRO_OPERATOR_HSL_SATURATION))
(defconstant +CFFI/CAIRO_OPERATOR_HSL_COLOR+ (cffi:foreign-enum-value 'cairo_operator_t :CFFI/CAIRO_OPERATOR_HSL_COLOR))
(defconstant +CFFI/CAIRO_OPERATOR_HSL_LUMINOSITY+ (cffi:foreign-enum-value 'cairo_operator_t :CFFI/CAIRO_OPERATOR_HSL_LUMINOSITY))


(defun cffi/cairo_set_operator (cr op)
"
 void cairo_set_operator (cairo_t *cr, cairo_operator_t op);

 Sets the compositing operator to be used for all drawing operations.
 See cairo_operator_t for details on the semantics of each available
 compositing operator.

 The default operator is CAIRO_OPERATOR_OVER.

    cr : a cairo_t
    op : a compositing operator, specified as a cairo_operator_t

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_SET_OPERATOR invoked with null CR")
      (cffi:foreign-funcall "cairo_set_operator"
			    :pointer cr
			    cairo_operator_t op
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_get_operator (cr)
"
 cairo_operator_t cairo_get_operator (cairo_t *cr);

 Gets the current compositing operator for a cairo context.

    cr : a cairo context

    Returns : the current compositing operator.

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_GET_OPERATOR invoked with null CR")
      (cffi:foreign-funcall "cairo_get_operator"
			    :pointer cr
			    cairo_operator_t))
  cr)


(defun cffi/cairo_set_tolerance (cr tolerance)
"
 void cairo_set_tolerance (cairo_t *cr, double tolerance);

 Sets the tolerance used when converting paths into trapezoids. Curved
 segments of the path will be subdivided until the maximum deviation
 between the original path and the polygonal approximation is less than
 tolerance. The default value is 0.1. A larger value will give better
 performance, a smaller value, better appearance. (Reducing the value
 from the default value of 0.1 is unlikely to improve appearance
 significantly.) The accuracy of paths within Cairo is limited by the
 precision of its internal arithmetic, and the prescribed tolerance is
 restricted to the smallest representable internal value.

    cr : a cairo_t
    tolerance : the tolerance, in device units (typically pixels)

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_SET_TOLERANCE invoked with null CR")
      (cffi:foreign-funcall "cairo_set_tolerance"
			    :pointer cr
			    :double tolerance
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_get_tolerance (cr)
"
 double cairo_get_tolerance (cairo_t *cr);

 Gets the current tolerance value, as set by cairo_set_tolerance().

    cr : a cairo context

    Returns : the current tolerance value.

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_GET_TOLERANCE invoked with null CR")
      (cffi:foreign-funcall "cairo_get_tolerance"
			    :pointer cr
			    :double)))


(defun cffi/cairo_clip (cr)
"
 void cairo_clip (cairo_t *cr);

 Establishes a new clip region by intersecting the current clip region
 with the current path as it would be filled by cairo_fill() and
 according to the current fill rule (see cairo_set_fill_rule()).

 After cairo_clip(), the current path will be cleared from the cairo
 context.

 The current clip region affects all drawing operations by effectively
 masking out any changes to the surface that are outside the current
 clip region.

 Calling cairo_clip() can only make the clip region smaller, never
 larger. But the current clip is part of the graphics state, so a
 temporary restriction of the clip region can be achieved by calling
 cairo_clip() within a cairo_save()/cairo_restore() pair. The only
 other means of increasing the size of the clip region is
 cairo_reset_clip().

    cr : a cairo context

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_CLIP invoked with null CR")
      (cffi:foreign-funcall "cairo_clip"
			    :pointer cr
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_clip_preserve (cr)
"
 void cairo_clip_preserve (cairo_t *cr);

 Establishes a new clip region by intersecting the current clip region
 with the current path as it would be filled by cairo_fill() and
 according to the current fill rule (see cairo_set_fill_rule()).

 Unlike cairo_clip(), cairo_clip_preserve() preserves the path within
 the cairo context.

 The current clip region affects all drawing operations by effectively
 masking out any changes to the surface that are outside the current
 clip region.

 Calling cairo_clip_preserve() can only make the clip region smaller,
 never larger. But the current clip is part of the graphics state, so
 a temporary restriction of the clip region can be achieved by calling
 cairo_clip_preserve() within a cairo_save()/cairo_restore() pair. The
 only other means of increasing the size of the clip region is
 cairo_reset_clip().

    cr : a cairo context

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_CLIP_PRESERVE invoked with null CR")
      (cffi:foreign-funcall "cairo_clip_preserve"
			    :pointer cr
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_clip_extents (cr x1 y1 x2 y2)
"
 void cairo_clip_extents (cairo_t *cr,
                          double *x1,double *y1,
                          double *x2,double *y2);

 Computes a bounding box in user coordinates covering the area inside the
 current clip.

    cr : a cairo context
    x1 : left of the resulting extents
    y1 : top of the resulting extents
    x2 : right of the resulting extents
    y2 : bottom of the resulting extents

 Since 1.4
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_CLIP_EXTENTS invoked with null CR")
      (if (or (null x1) (cffi:null-pointer-p x1))
	  (error "CFFI/CAIRO_CLIP_EXTENTS invoked with null X1")
	  (if (or (null y1) (cffi:null-pointer-p y1))
	      (error "CFFI/CAIRO_CLIP_EXTENTS invoked with null Y1")
	      (if (or (null x2) (cffi:null-pointer-p x2))
		  (error "CFFI/CAIRO_CLIP_EXTENTS invoked with null X2")
		  (if (or (null y2) (cffi:null-pointer-p y2))
		      (error "CFFI/CAIRO_CLIP_EXTENTS invoked with null Y2")
		      (cffi:foreign-funcall "cairo_clip_extents"
					    :pointer cr
					    :pointer x1
					    :pointer y1
					    :pointer x2
					    :pointer y2
					    :void))))))
  (cairo-status-report cr)
  cr)


(cffi:defctype cairo_bool_t :int
"
 cairo_bool_t is used for boolean values. Returns of type cairo_bool_t
 will always be either 0 or 1, but testing against these values explicitly
 is not encouraged; just use the value as a boolean condition.
")


(defun cffi/cairo_in_clip (cr x y)
"
 cairo_bool_t cairo_in_clip (cairo_t *cr, double x, double y);

 Tests whether the given point is inside the area that would be visible
 through the current clip, i.e. the area that would be filled by a
 cairo_paint() operation.

 See cairo_clip(), and cairo_clip_preserve().

    cr : a cairo context
    x : X coordinate of the point to test
    y : Y coordinate of the point to test

    Returns : A non-zero value if the point is inside, or zero if
              outside.

 Since 1.10
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_IN_CLIP invoked with null CR")
      (cffi:foreign-funcall "cairo_in_clip"
			    :pointer cr
			    :double x
			    :double y
			    cairo_bool_t)))


(defun cffi/cairo_reset_clip (cr)
"
 void cairo_reset_clip (cairo_t *cr);

 Reset the current clip region to its original, unrestricted state.
 That is, set the clip region to an infinitely large shape containing
 the target surface. Equivalently, if infinity is too hard to grasp,
 one can imagine the clip region being reset to the exact bounds of
 the target surface.

 Note that code meant to be reusable should not call
 cairo_reset_clip() as it will cause results unexpected by higher-level
 code which calls cairo_clip(). Consider using cairo_save() and
 cairo_restore() around cairo_clip() as a more robust means of
 temporarily restricting the clip region.

    cr : a cairo context

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_RESET_CLIP invoked with null CR")
      (cffi:foreign-funcall "cairo_reset_clip"
			    :pointer cr
			    :void))
  (cairo-status-report cr)
  cr)


(cffi:defcstruct cairo_rectangle_t
"
 A data structure for holding a rectangle.

    double x;      X coordinate of the left side of the rectangle
    double y;      Y coordinate of the the top side of the rectangle
    double width;  width of the rectangle
    double height; height of the rectangle

 Since 1.4
"
  (x :double) (y :double)
  (width :double) (height :double))

(defun cffi/cairo_rectangle_t->x (crt)
  (if (or (null crt) (cffi:null-pointer-p crt))
      (error "CFFI/CAIRO_RECTANGLE_T->X invoked with null CRT")
      (cffi:foreign-slot-value crt '(:struct cairo_rectangle_t) 'x)))

(defun cffi/cairo_rectangle_t->y (crt)
  (if (or (null crt) (cffi:null-pointer-p crt))
      (error "CFFI/CAIRO_RECTANGLE_T->Y invoked with null CRT")
      (cffi:foreign-slot-value crt '(:struct cairo_rectangle_t) 'x)))

(defun cffi/cairo_rectangle_t->width (crt)
  (if (or (null crt) (cffi:null-pointer-p crt))
      (error "CFFI/CAIRO_RECTANGLE_T->WIDTH invoked with null CRT")
      (cffi:foreign-slot-value crt '(:struct cairo_rectangle_t) 'x)))

(defun cffi/cairo_rectangle_t->height (crt)
  (if (or (null crt) (cffi:null-pointer-p crt))
      (error "CFFI/CAIRO_RECTANGLE_T->HEIGHT invoked with null CRT")
      (cffi:foreign-slot-value crt '(:struct cairo_rectangle_t) 'x)))


(cffi:defcstruct cairo_rectangle_list_t
"
 A data structure for holding a dynamically allocated array of rectangles.

    cairo_status_t status;         Error status of the rectangle list
    cairo_rectangle_t *rectangles; Array containing the rectangles
    int num_rectangles;            Number of rectangles in this list

 Since 1.4
"
  (status         :int)
  (rectangles     (:pointer (:struct cairo_rectangle_t)))
  (num_rectangles :int))

(defun cffi/cairo_rectangle_list_t->status (crlt)
  (if (or (null crlt) (cffi:null-pointer-p crlt))
      (error "CFFI/CAIRO_RECTANGLE_LIST_T->STATUS invoked with null CRLT")
      (cffi:foreign-slot-value crlt '(:struct cairo_rectangle_list_t) 'status)))

(defun cffi/cairo_rectangle_list_t->rectangles (crlt)
  (if (or (null crlt) (cffi:null-pointer-p crlt))
      (error "CFFI/CAIRO_RECTANGLE_LIST_T->RECTANGLES invoked with null CRLT")
      (let ((rectangles (cffi:foreign-slot-value crlt
						 '(:struct cairo_rectangle_list_t)
						 'rectangles)))
	(pointer-or-nil rectangles))))

(defun cffi/cairo_rectangle_list_t->num_rectangles (crlt)
  (if (or (null crlt) (cffi:null-pointer-p crlt))
      (error "CFFI/CAIRO_RECTANGLE_LIST_T->NUM_RECTANGLES invoked with null CRLT")
      (cffi:foreign-slot-value crlt '(:struct cairo_rectangle_list_t) 'num_rectangles)))


(cffi:defctype cairo_rectangle_list_t* :pointer)


(defun cffi/cairo_rectangle_list_destroy (rectangle-list)
"
 void cairo_rectangle_list_destroy (cairo_rectangle_list_t *rectangle_list);

 Unconditionally frees rectangle_list and all associated references.
 After this call, the rectangle_list pointer must not be dereferenced.

    rectangle_list : a rectangle list, as obtained from
                     cairo_copy_clip_rectangle_list()

 Since 1.4
"
  (if (or (null rectangle-list) (cffi:null-pointer-p rectangle-list))
      (error "CFFI/CAIRO_RECTANGLE_LIST_DESTROY invoked with null RECTANGLE-LIST")
      (cffi:foreign-funcall "cairo_rectangle_list_destroy"
			    :pointer rectangle-list
			    :void))
  nil)


(defun cffi/cairo_copy_clip_rectangle_list (cr)
"
 cairo_rectangle_list_t * cairo_copy_clip_rectangle_list (cairo_t *cr);

 Gets the current clip region as a list of rectangles in user
 coordinates. Never returns NULL.

 The status in the list may be CAIRO_STATUS_CLIP_NOT_REPRESENTABLE to
 indicate that the clip region cannot be represented as a list of
 user-space rectangles. The status may have other values to indicate
 other errors.

    cr : a cairo context

    Returns : the current clip region as a list of rectangles in user
              coordinates, which should be destroyed using
    cairo_rectangle_list_destroy().

 Since 1.4
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_COPY_CLIP_RECTANGLE_LIST invoked with null CR")
      (let ((rectangles (cffi:foreign-funcall "cairo_copy_clip_rectangle_list"
					      :pointer cr
					      cairo_rectangle_list_t*)))
	(cairo-status-report cr)
	rectangles)))



(defun cffi/cairo_fill (cr)
"
 void cairo_fill (cairo_t *cr);

 A drawing operator that fills the current path according to the
 current fill rule, (each sub-path is implicitly closed before
 being filled). After cairo_fill(), the current path will be
 cleared from the cairo context. See cairo_set_fill_rule() and
 cairo_fill_preserve().

    cr : a cairo context

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_FILL invoked with null CR")
      (cffi:foreign-funcall "cairo_fill"
			    :pointer cr
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_fill_preserve (cr)
"
 void cairo_fill_preserve (cairo_t *cr);

 A drawing operator that fills the current path according to the
 current fill rule, (each sub-path is implicitly closed before
 being filled). Unlike cairo_fill(), cairo_fill_preserve()
 preserves the path within the cairo context.

 See cairo_set_fill_rule() and cairo_fill().

    cr : a cairo context

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_FILL_PRESERVE invoked with null CR")
      (cffi:foreign-funcall "cairo_fill_preserve"
			    :pointer cr
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_fill_extents (cr x1 y1 x2 y2)
"
 void cairo_fill_extents (cairo_t *cr,
                          double *x1,
                          double *y1,
                          double *x2,
                          double *y2);

 Computes a bounding box in user coordinates covering the area
 that would be affected, (the 'inked' area), by a cairo_fill()
 operation given the current path and fill parameters. If the
 current path is empty, returns an empty rectangle ((0,0), (0,0)).
 Surface dimensions and clipping are not taken into account.

 Contrast with cairo_path_extents(), which is similar, but
 returns non-zero extents for some paths with no inked area,
 (such as a simple line segment).

 Note that cairo_fill_extents() must necessarily do more work to
 compute the precise inked areas in light of the fill rule, so
 cairo_path_extents() may be more desirable for sake of
 performance if the non-inked path extents are desired.

 See cairo_fill(), cairo_set_fill_rule() and cairo_fill_preserve().

    cr : a cairo context
    x1 : left of the resulting extents
    y1 : top of the resulting extents
    x2 : right of the resulting extents
    y2 : bottom of the resulting extents

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_FILL_EXTENTS invoked with null CR")
      (if (or (null x1) (cffi:null-pointer-p x1))
	  (error "CFFI/CAIRO_FILL_EXTENTS invoked with null X1")
	  (if (or (null y1) (cffi:null-pointer-p y1))
	      (error "CFFI/CAIRO_FILL_EXTENTS invoked with null Y1")
	      (if (or (null x2) (cffi:null-pointer-p x2))
		  (error "CFFI/CAIRO_FILL_EXTENTS invoked with null X2")
		  (if (or (null y2) (cffi:null-pointer-p y2))
		      (error "CFFI/CAIRO_FILL_EXTENTS invoked with null Y2")
		      (cffi:foreign-funcall "cairo_fill_extents"
					    :pointer cr
					    :pointer x1
					    :pointer y1
					    :pointer x2
					    :pointer y2
					    :void))))))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_in_fill (cr x y)
"
 cairo_bool_t cairo_in_fill (cairo_t *cr,
                             double x,
                             double y);

 Tests whether the given point is inside the area that would be affected
 by a cairo_fill() operation given the current path and filling parameters.
 Surface dimensions and clipping are not taken into account.

 See cairo_fill(), cairo_set_fill_rule() and cairo_fill_preserve().

    cr : a cairo context
    x : X coordinate of the point to test
    y : Y coordinate of the point to test

    Returns : A non-zero value if the point is inside, or zero if outside.

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_IN_FILL invoked with null CR")
      (cffi:foreign-funcall "cairo_in_fill"
			    :pointer cr
			    :double x
			    :double y
			    cairo_bool_t)))


(defun cffi/cairo_mask (cr pattern)
"
 void cairo_mask (cairo_t *cr, cairo_pattern_t *pattern);

 A drawing operator that paints the current source using the alpha
 channel of pattern as a mask. (Opaque areas of pattern are painted
 with the source, transparent areas are not painted.)

    cr : a cairo context
    pattern : a cairo_pattern_t

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_MASK invoked with null CR")
      (if (or (null pattern) (cffi:null-pointer-p pattern))
	  (error "CFFI/CAIRO_MASK invoked with null PATTERN")
	  (cffi:foreign-funcall "cairo_mask"
				:pointer cr
				:pointer pattern
				:void)))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_mask_surface (cr surface surface-x surface-y)
"
 void cairo_mask_surface (cairo_t *cr,
                          cairo_surface_t *surface,
                          double surface_x,
                          double surface_y);

 A drawing operator that paints the current source using the alpha channel
 of surface as a mask. (Opaque areas of surface are painted with the source,
 transparent areas are not painted.)

    cr : a cairo context
    surface : a cairo_surface_t
    surface_x : X coordinate at which to place the origin of surface
    surface_y : Y coordinate at which to place the origin of surface

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_MASK_SURFACE invoked with null CR")
      (if (or (null surface) (cffi:null-pointer-p surface))
	  (error "CFFI/CAIRO_MASK_SURFACE invoked with null SURFACE")
	  (cffi:foreign-funcall "cairo_mask_surface"
				:pointer cr
				:pointer surface
				:double surface-x
				:double surface-y
				:void)))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_paint (cr)
"
 void cairo_paint (cairo_t *cr);

 A drawing operator that paints the current source everywhere within the
 current clip region.

    cr : a cairo context

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_PAINT invoked with null CR")
      (cffi:foreign-funcall "cairo_paint"
			    :pointer cr
			    :void))
  (cairo-status-report cr)
  cr)


;; FIXME: USE THIS TO DISABLE HOMEGROWN STUFF?
(defun cffi/cairo_paint_with_alpha (cr alpha)
"
 void cairo_paint_with_alpha (cairo_t *cr, double alpha);

 A drawing operator that paints the current source everywhere within
 the current clip region using a mask of constant alpha value alpha.
 The effect is similar to cairo_paint(), but the drawing is faded out
 using the alpha value.

    cr : a cairo context
    alpha : alpha value, between 0 (transparent) and 1 (opaque)

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_PAINT_WITH_ALPHA invoked with null CR")
      (cffi:foreign-funcall "cairo_paint_with_alpha"
			    :pointer cr
			    :double alpha
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_stroke (cr)
"
 void cairo_stroke (cairo_t *cr);

 A drawing operator that strokes the current path according to the
 current line width, line join, line cap, and dash settings. After
 cairo_stroke(), the current path will be cleared from the cairo
 context. See cairo_set_line_width(), cairo_set_line_join(),
 cairo_set_line_cap(), cairo_set_dash(), and cairo_stroke_preserve().

 Note: Degenerate segments and sub-paths are treated specially and
 provide a useful result. These can result in two different situations:

 1. Zero-length 'on' segments set in cairo_set_dash(). If the cap
    style is CAIRO_LINE_CAP_ROUND or CAIRO_LINE_CAP_SQUARE then these
 segments will be drawn as circular dots or squares respectively. In
 the case of CAIRO_LINE_CAP_SQUARE, the orientation of the squares is
 determined by the direction of the underlying path.

 2. A sub-path created by cairo_move_to() followed by either a
    cairo_close_path() or one or more calls to cairo_line_to() to the
 same coordinate as the cairo_move_to(). If the cap style is
 CAIRO_LINE_CAP_ROUND then these sub-paths will be drawn as circular
 dots. Note that in the case of CAIRO_LINE_CAP_SQUARE a degenerate
 sub-path will not be drawn at all, (since the correct orientation is
 indeterminate).

 In no case will a cap style of CAIRO_LINE_CAP_BUTT cause anything to
 be drawn in the case of either degenerate segments or sub-paths.

    cr : a cairo context

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_STROKE invoked with null CR")
      (cffi:foreign-funcall "cairo_stroke"
			    :pointer cr
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_stroke_preserve (cr)
"
 void cairo_stroke_preserve (cairo_t *cr);

 A drawing operator that strokes the current path according to the
 current line width, line join, line cap, and dash settings. Unlike
 cairo_stroke(), cairo_stroke_preserve() preserves the path within
 the cairo context.

 See cairo_set_line_width(), cairo_set_line_join(),
 cairo_set_line_cap(), cairo_set_dash(), and cairo_stroke_preserve().

    cr : a cairo context

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_STROKE_PRESERVE invoked with null CR")
      (cffi:foreign-funcall "cairo_stroke_preserve"
			    :pointer cr
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_stroke_extents (cr x1 y1 x2 y2)
"
 void cairo_stroke_extents (cairo_t *cr,
                            double *x1,
                            double *y1,
                            double *x2,
                            double *y2);

 Computes a bounding box in user coordinates covering the area that
 would be affected, (the 'inked' area), by a cairo_stroke() operation
 given the current path and stroke parameters. If the current path is
 empty, returns an empty rectangle ((0,0), (0,0)). Surface dimensions
 and clipping are not taken into account.

 Note that if the line width is set to exactly zero, then
 cairo_stroke_extents() will return an empty rectangle. Contrast with
 cairo_path_extents() which can be used to compute the non-empty bounds
 as the line width approaches zero.

 Note that cairo_stroke_extents() must necessarily do more work to
 compute the precise inked areas in light of the stroke parameters, so
 cairo_path_extents() may be more desirable for sake of performance
 if non-inked path extents are desired.

 See cairo_stroke(), cairo_set_line_width(), cairo_set_line_join(),
 cairo_set_line_cap(), cairo_set_dash(), and cairo_stroke_preserve().

    cr : a cairo context
    x1 : left of the resulting extents
    y1 : top of the resulting extents
    x2 : right of the resulting extents
    y2 : bottom of the resulting extents

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_STROKE_EXTENTS invoked with null CR")
      (if (or (null x1) (cffi:null-pointer-p x1))
	  (error "CFFI/CAIRO_STROKE_EXTENTS invoked with null X1")
	  (if (or (null y1) (cffi:null-pointer-p y1))
	      (error "CFFI/CAIRO_STROKE_EXTENTS invoked with null Y1")
	      (if (or (null x2) (cffi:null-pointer-p x2))
		  (error "CFFI/CAIRO_STROKE_EXTENTS invoked with null X2")
		  (if (or (null y2) (cffi:null-pointer-p y2))
		      (error "CFFI/CAIRO_STROKE_EXTENTS invoked with null Y2")
		      (cffi:foreign-funcall "cairo_stroke_extents"
					    :pointer cr
					    :pointer x1
					    :pointer y1
					    :pointer x2
					    :pointer y2
					    :void))))))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_in_stroke (cr x y)
"
 cairo_bool_t cairo_in_stroke (cairo_t *cr,
                               double x,
                               double y);

 Tests whether the given point is inside the area that would be
 affected by a cairo_stroke() operation given the current path and
 stroking parameters. Surface dimensions and clipping are not
 taken into account.

 See cairo_stroke(), cairo_set_line_width(), cairo_set_line_join(),
 cairo_set_line_cap(), cairo_set_dash(), and cairo_stroke_preserve().

    cr : a cairo context
    x : X coordinate of the point to test
    y : Y coordinate of the point to test

    Returns : A non-zero value if the point is inside, or zero if outside.

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_IN_STROKE invoked with null CR")
      (cffi:foreign-funcall "cairo_in_stroke"
			    :pointer cr
			    :double x
			    :double y
			    cairo_bool_t)))


(defun cffi/cairo_copy_page (cr)
"
 void cairo_copy_page (cairo_t *cr);

 Emits the current page for backends that support multiple pages, but
 doesn't clear it, so, the contents of the current page will be
 retained for the next page too. Use cairo_show_page() if you want to
 get an empty page after the emission.

 This is a convenience function that simply calls
 cairo_surface_copy_page() on cr's target.

    cr : a cairo context

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_COPY_PAGE invoked with null CR")
      (cffi:foreign-funcall "cairo_copy_page"
			    :pointer cr
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_show_page (cr)
"
 void cairo_show_page (cairo_t *cr);

 Emits and clears the current page for backends that support multiple
 pages. Use cairo_copy_page() if you don't want to clear the page.

 This is a convenience function that simply calls
 cairo_surface_show_page() on cr's target.

    cr : a cairo context

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_SHOW_PAGE invoked with null CR")
      (cffi:foreign-funcall "cairo_show_page"
			    :pointer cr
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_get_reference_count (cr)
"
 unsigned int cairo_get_reference_count (cairo_t *cr);

 Returns the current reference count of cr.

    cr : a cairo_t

    Returns : the current reference count of cr. If the object is a nil
              object, 0 will be returned.

 Since 1.4
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_GET_REFERENCE_COUNT invoked with null CR")
      (cffi:foreign-funcall "cairo_get_reference_count"
			    :pointer cr
			    :uint)))

(cffi:defctype cairo_destroy_func_t :pointer
"
 void (*cairo_destroy_func_t) (void *data);

 cairo_destroy_func_t the type of function which is called when a data
 element is destroyed. It is passed the pointer to the data element and
 should free any memory and resources allocated for it.

    data : The data element being destroyed.
")

(defun cffi/cairo_set_user_data (cr key user-data destroy)
"
 cairo_status_t cairo_set_user_data (cairo_t *cr,
                                     const cairo_user_data_key_t *key,
                                     void *user_data,
                                     cairo_destroy_func_t destroy);

 Attach user data to cr. To remove user data from a surface, call this
 function with the key that was used to set it and NULL for data.

    cr : a cairo_t
    key : the address of a cairo_user_data_key_t to attach the user
          data to
    user_data : the user data to attach to the cairo_t
    destroy : a cairo_destroy_func_t which will be called when the
              cairo_t is destroyed or when new user data is attached
              using the same key.

    Returns : CAIRO_STATUS_SUCCESS or CAIRO_STATUS_NO_MEMORY if a slot
              could not be allocated for the user data.

 Since 1.4
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_SET_USER_DATA invoked with null CR")
      (if (or (null key) (cffi:null-pointer-p key))
	  (error "CFFI/CAIRO_SET_USER_DATA invoked with null KEY")
	  (let ((user-data (or user-data (cffi:null-pointer))))
	    (cffi:foreign-funcall "cairo_set_user_data"
				  :pointer cr
				  :pointer key
				  :pointer user-data
				  cairo_destroy_func_t destroy
				  :int)))))


(defun cffi/cairo_get_user_data (cr key)
"
 void * cairo_get_user_data (cairo_t *cr,
                             const cairo_user_data_key_t *key);

 Return user data previously attached to cr using the specified key. If
 no user data has been attached with the given key this function
 returns NULL.

    cr : a cairo_t
    key : the address of the cairo_user_data_key_t the user data was attached to

    Returns : the user data previously attached or NULL.

 Since 1.4
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_GET_USER_DATA invoked with null CR")
      (if (or (null key) (cffi:null-pointer-p key))
	  (error "CFFI/CAIRO_GET_USER_DATA invoked with null KEY")
	  (let ((data (cffi:foreign-funcall "cairo_get_user_data"
					    :pointer cr
					    :pointer key
					    :pointer)))
	    (pointer-or-nil data)))))

;;See Also
;;cairo_surface_t 

;;;;;;;;;; CAIRO_PATH_T

#||
cairo_path_t

typedef struct {
    cairo_status_t status;
    cairo_path_data_t *data;
    int num_data;
} cairo_path_t;
A data structure for holding a path. This data structure serves as the return value for cairo_copy_path() and cairo_copy_path_flat() as well the input value for cairo_append_path().

See cairo_path_data_t for hints on how to iterate over the actual data within the path.

The num_data member gives the number of elements in the data array. This number is larger than the number of independent path portions (defined in cairo_path_data_type_t), since the data includes both headers and coordinates for each portion.

cairo_status_t status;

the current error status
cairo_path_data_t *data;

the elements in the path
int num_data;

the number of elements in the data array
Since 1.0
||#

#||
union cairo_path_data_t

union _cairo_path_data_t {
    struct {
	cairo_path_data_type_t type;
	int length;
    } header;
    struct {
	double x, y;
    } point;
};
cairo_path_data_t is used to represent the path data inside a cairo_path_t.

The data structure is designed to try to balance the demands of efficiency and ease-of-use. A path is represented as an array of cairo_path_data_t, which is a union of headers and points.

Each portion of the path is represented by one or more elements in the array, (one header followed by 0 or more points). The length value of the header is the number of array elements for the current portion including the header, (ie. length == 1 + # of points), and where the number of points for each element type is as follows:

    %CAIRO_PATH_MOVE_TO:     1 point
    %CAIRO_PATH_LINE_TO:     1 point
    %CAIRO_PATH_CURVE_TO:    3 points
    %CAIRO_PATH_CLOSE_PATH:  0 points
The semantics and ordering of the coordinate values are consistent with cairo_move_to(), cairo_line_to(), cairo_curve_to(), and cairo_close_path().

Here is sample code for iterating through a cairo_path_t:

1  int i;
2  cairo_path_t *path;
3  cairo_path_data_t *data;
4
5  path = cairo_copy_path (cr);
6
7  for (i=0; i < path->num_data; i += path->data[i].header.length) {
8      data = &path->data[i];
9      switch (data->header.type) {
10     case CAIRO_PATH_MOVE_TO:
11         do_move_to_things (data[1].point.x, data[1].point.y);
12         break;
13     case CAIRO_PATH_LINE_TO:
14         do_line_to_things (data[1].point.x, data[1].point.y);
15         break;
16     case CAIRO_PATH_CURVE_TO:
17         do_curve_to_things (data[1].point.x, data[1].point.y,
18                             data[2].point.x, data[2].point.y,
19                             data[3].point.x, data[3].point.y);
20         break;
21     case CAIRO_PATH_CLOSE_PATH:
22         do_close_path_things ();
23         break;
24     }
25 }
26 cairo_path_destroy (path);

As of cairo 1.4, cairo does not mind if there are more elements in a portion of the path than needed. Such elements can be used by users of the cairo API to hold extra values in the path data structure. For this reason, it is recommended that applications always use data->header.length to iterate over the path data, instead of hardcoding the number of elements for each element type.

Since 1.0
||#

#||
enum cairo_path_data_type_t

typedef enum {
    CAIRO_PATH_MOVE_TO,
    CAIRO_PATH_LINE_TO,
    CAIRO_PATH_CURVE_TO,
    CAIRO_PATH_CLOSE_PATH
} cairo_path_data_type_t;
cairo_path_data_t is used to describe the type of one portion of a path when represented as a cairo_path_t. See cairo_path_data_t for details.

CAIRO_PATH_MOVE_TO

A move-to operation, since 1.0
CAIRO_PATH_LINE_TO

A line-to operation, since 1.0
CAIRO_PATH_CURVE_TO

A curve-to operation, since 1.0
CAIRO_PATH_CLOSE_PATH

A close-path operation, since 1.0
Since 1.0
||#

(defun cffi/cairo_copy_path (cr)
"
 cairo_path_t * cairo_copy_path (cairo_t *cr);

 Creates a copy of the current path and returns it to the user as a cairo_path_t. See
 cairo_path_data_t for hints on how to iterate over the returned data structure.

 This function will always return a valid pointer, but the result will have no data
 (data==NULL and num_data==0), if either of the following conditions hold:

 If there is insufficient memory to copy the path. In this case path->status will be
 set to CAIRO_STATUS_NO_MEMORY.
 If cr is already in an error state. In this case path->status will contain the same
 status that would be returned by cairo_status().

    cr : a cairo context

    Returns : the copy of the current path. The caller owns the returned object and
              should call cairo_path_destroy() when finished with it.
 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_COPY_PATH invoked with null CR")
      (let ((path (cffi:foreign-funcall "cairo_copy_path"
					:pointer cr
					:pointer)))
	;; FIXME: This will never be nil. See docs.
	(pointer-or-nil path))))


(defun cffi/cairo_copy_path_flat (cr)
"
 cairo_path_t * cairo_copy_path_flat (cairo_t *cr);

 Gets a flattened copy of the current path and returns it to the user as a cairo_path_t.
 See cairo_path_data_t for hints on how to iterate over the returned data structure.

 This function is like cairo_copy_path() except that any curves in the path will be
 approximated with piecewise-linear approximations, (accurate to within the current
 tolerance value). That is, the result is guaranteed to not have any elements of type
 CAIRO_PATH_CURVE_TO which will instead be replaced by a series of CAIRO_PATH_LINE_TO
 elements.

 This function will always return a valid pointer, but the result will have no data
 (data==NULL and num_data==0), if either of the following conditions hold:

 If there is insufficient memory to copy the path. In this case path->status will be
 set to CAIRO_STATUS_NO_MEMORY.
 If cr is already in an error state. In this case path->status will contain the same
 status that would be returned by cairo_status().

    cr : a cairo context

    Returns : the copy of the current path. The caller owns the returned object and
              should call cairo_path_destroy() when finished with it.

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_COPY_PATH_FLAT invoked with null CR")
      (let ((path (cffi:foreign-funcall "cairo_copy_path_flat"
					:pointer cr
					:pointer)))
	;; FIXME: This will never be nil. See docs.
	(pointer-or-nil path))))


(defun cffi/cairo_path_destroy (path)
"
 void cairo_path_destroy (cairo_path_t *path);

 Immediately releases all memory associated with path. After a call to
 cairo_path_destroy() the path pointer is no longer valid and should not be used
 further.

 Note: cairo_path_destroy() should only be called with a pointer to a cairo_path_t
 returned by a cairo function. Any path that is created manually (ie. outside of
 cairo) should be destroyed manually as well.

    path : a path previously returned by either cairo_copy_path() or
           cairo_copy_path_flat().

 Since 1.0
"
  (if (or (null path) (cffi:null-pointer-p path))
      (error "CFFI/CAIRO_PATH_DESTROY invoked with null PATH")
      (cffi:foreign-funcall "cairo_path_destroy"
			    :pointer path
			    :void))
  nil)


(defun cffi/cairo_append_path (cr path)
"
 void cairo_append_path (cairo_t *cr, const cairo_path_t *path);

 Append the path onto the current path. The path may be either the return value
 from one of cairo_copy_path() or cairo_copy_path_flat() or it may be constructed
 manually. See cairo_path_t for details on how the path data structure should be
 initialized, and note that path->status must be initialized to
 CAIRO_STATUS_SUCCESS.

    cr : a cairo context
    path : path to be appended

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_APPEND_PATH invoked with null CR")
      (if (or (null path) (cffi:null-pointer-p path))
	  (error "CFFI/CAIRO_APPEND_PATH invoked with null PATH")
	  (cffi:foreign-funcall "cairo_append_path"
				:pointer cr
				:pointer path
				:void)))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_has_current_point (cr)
"
 cairo_bool_t cairo_has_current_point (cairo_t *cr);

 Returns whether a current point is defined on the current path. See
 cairo_get_current_point() for details on the current point.

    cr : a cairo context

    Returns : whether a current point is defined.

 Since 1.6
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_HAS_CURRENT_POINT invoked with null CR")
      (cffi:foreign-funcall "cairo_has_current_point"
			    :pointer cr
			    cairo_bool_t)))


(defun cffi/cairo_get_current_point (cr x y)
"
 void cairo_get_current_point (cairo_t *cr, double *x, double *y);

 Gets the current point of the current path, which is conceptually the final
 point reached by the path so far.

 The current point is returned in the user-space coordinate system. If there
 is no defined current point or if cr is in an error status, x and y will
 both be set to 0.0. It is possible to check this in advance with
 cairo_has_current_point().

 Most path construction functions alter the current point. See the following
 for details on how they affect the current point: cairo_new_path(),
 cairo_new_sub_path(), cairo_append_path(), cairo_close_path(),
 cairo_move_to(), cairo_line_to(), cairo_curve_to(), cairo_rel_move_to(),
 cairo_rel_line_to(), cairo_rel_curve_to(), cairo_arc(), cairo_arc_negative(),
 cairo_rectangle(), cairo_text_path(), cairo_glyph_path(),
 cairo_stroke_to_path().

 Some functions use and alter the current point but do not otherwise change
 current path: cairo_show_text().

 Some functions unset the current path and as a result, current point:
 cairo_fill(), cairo_stroke().

    cr : a cairo context
    x : return value for X coordinate of the current point
    y : return value for Y coordinate of the current point

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_GET_CURRENT_POINT invoked with null CR")
      (if (or (null x) (cffi:null-pointer-p x))
	  (error "CFFI/CAIRO_GET_CURRENT_POINT invoked with null X")
	  (if (or (null y) (cffi:null-pointer-p y))
	      (error "CFFI/CAIRO_GET_CURRENT_POINT invoked with null Y")
	      (cffi:foreign-funcall "cairo_get_current_point"
				    :pointer cr
				    :pointer x
				    :pointer y
				    :void))))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_new_path (cr)
"
 void cairo_new_path (cairo_t *cr);

 Clears the current path. After this call there will be no path and no
 current point.

    cr : a cairo context

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_NEW_PATH invoked with null CR")
      (cffi:foreign-funcall "cairo_new_path"
			    :pointer cr
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_new_sub_path (cr)
"
 void cairo_new_sub_path (cairo_t *cr);

 Begin a new sub-path. Note that the existing path is not affected. After this call
 there will be no current point.

 In many cases, this call is not needed since new sub-paths are frequently started
 with cairo_move_to().

 A call to cairo_new_sub_path() is particularly useful when beginning a new sub-path
 with one of the cairo_arc() calls. This makes things easier as it is no longer
 necessary to manually compute the arc's initial coordinates for a call to
 cairo_move_to().

    cr : a cairo context

 Since 1.2
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_NEW_SUB_PATH invoked with null CR")
      (cffi:foreign-funcall "cairo_new_subpath"
			    :pointer cr
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_close_path (cr)
"
 void cairo_close_path (cairo_t *cr);

 Adds a line segment to the path from the current point to the beginning of the
 current sub-path, (the most recent point passed to cairo_move_to()), and closes
 this sub-path. After this call the current point will be at the joined endpoint
 of the sub-path.

 The behavior of cairo_close_path() is distinct from simply calling
 cairo_line_to() with the equivalent coordinate in the case of stroking. When a
 closed sub-path is stroked, there are no caps on the ends of the sub-path.
 Instead, there is a line join connecting the final and initial segments of the
 sub-path.

 If there is no current point before the call to cairo_close_path(), this function
 will have no effect.

 Note: As of cairo version 1.2.4 any call to cairo_close_path() will place an
 explicit MOVE_TO element into the path immediately after the CLOSE_PATH element,
 (which can be seen in cairo_copy_path() for example). This can simplify path
 processing in some cases as it may not be necessary to save the 'last move_to point'
 during processing as the MOVE_TO immediately after the CLOSE_PATH will provide that
 point.

    cr : a cairo context

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_CLOSE_PATH invoked with null CR")
      (cffi:foreign-funcall "cairo_close_path"
			    :pointer cr
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_arc (cr xc yc radius angle1 angle2)
"
 void cairo_arc (cairo_t *cr, double xc, double yc,
                 double radius, double angle1, double angle2);

 Adds a circular arc of the given radius to the current path. The arc is centered
 at (xc, yc), begins at angle1 and proceeds in the direction of increasing angles
 to end at angle2. If angle2 is less than angle1 it will be progressively
 increased by 2*M_PI until it is greater than angle1.

 If there is a current point, an initial line segment will be added to the path to
 connect the current point to the beginning of the arc. If this initial line is
 undesired, it can be avoided by calling cairo_new_sub_path() before calling
 cairo_arc().

 Angles are measured in radians. An angle of 0.0 is in the direction of the
 positive X axis (in user space). An angle of M_PI/2.0 radians (90 degrees) is
 in the direction of the positive Y axis (in user space). Angles increase in the
 direction from the positive X axis toward the positive Y axis. So with the default
 transformation matrix, angles increase in a clockwise direction.

 (To convert from degrees to radians, use degrees * (M_PI / 180.).)

 This function gives the arc in the direction of increasing angles; see
 cairo_arc_negative() to get the arc in the direction of decreasing angles.

 The arc is circular in user space. To achieve an elliptical arc, you can scale the
 current transformation matrix by different amounts in the X and Y directions.
 For example, to draw an ellipse in the box given by x, y, width, height:

    1 cairo_save (cr);
    2 cairo_translate (cr, x + width / 2., y + height / 2.);
    3 cairo_scale (cr, width / 2., height / 2.);
    4 cairo_arc (cr, 0., 0., 1., 0., 2 * M_PI);
    5 cairo_restore (cr);

    cr : a cairo context
    xc : X position of the center of the arc
    yc : Y position of the center of the arc
    radius : the radius of the arc
    angle1 : the start angle, in radians
    angle2 : the end angle, in radians

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_ARC invoked with null CR")
      (cffi:foreign-funcall "cairo_arc"
			    :pointer cr
			    :double xc
			    :double yc
			    :double radius
			    :double angle1
			    :double angle2
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_arc_negative (cr xc yc radius angle1 angle2)
"
 void cairo_arc_negative (cairo_t *cr, double xc, double yc,
                          double radius, double angle1, double angle2);

 Adds a circular arc of the given radius to the current path. The arc is centered
 at (xc, yc), begins at angle1 and proceeds in the direction of decreasing angles
 to end at angle2. If angle2 is greater than angle1 it will be progressively
 decreased by 2*M_PI until it is less than angle1.

 See cairo_arc() for more details. This function differs only in the direction of
 the arc between the two angles.

    cr : a cairo context
    xc : X position of the center of the arc
    yc : Y position of the center of the arc
    radius : the radius of the arc
    angle1 : the start angle, in radians
    angle2 : the end angle, in radians

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_ARC_NEGATIVE invoked with null CR")
      (cffi:foreign-funcall "cairo_arc_negative"
			    :pointer cr
			    :double xc
			    :double yc
			    :double radius
			    :double angle1
			    :double angle2
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_curve_to (cr x1 y1 x2 y2 x3 y3)
"
 void cairo_curve_to (cairo_t *cr, double x1, double y1,
                                   double x2, double y2,
                                   double x3, double y3);

 Adds a cubic Bézier spline to the path from the current point to position
 (x3, y3) in user-space coordinates, using (x1, y1) and (x2, y2) as the
 control points. After this call the current point will be (x3, y3).

 If there is no current point before the call to cairo_curve_to() this
 function will behave as if preceded by a call to cairo_move_to(cr, x1, y1).

    cr : a cairo context
    x1 : the X coordinate of the first control point
    y1 : the Y coordinate of the first control point
    x2 : the X coordinate of the second control point
    y2 : the Y coordinate of the second control point
    x3 : the X coordinate of the end of the curve
    y3 : the Y coordinate of the end of the curve

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_CURVE_TO invoked with null CR")
      (cffi:foreign-funcall "cairo_curve_to"
			    :pointer cr
			    :double x1 :double y1
			    :double x2 :double y2
			    :double x3 :double y3
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_line_to (cr x y)
"
 void cairo_line_to (cairo_t *cr, double x, double y);

 Adds a line to the path from the current point to position (x, y) in
 user-space coordinates. After this call the current point will be (x, y).

 If there is no current point before the call to cairo_line_to() this
 function will behave as cairo_move_to(cr, x, y).

    cr : a cairo context
    x : the X coordinate of the end of the new line
    y : the Y coordinate of the end of the new line

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_LINE_TO invoked with null CR")
      (cffi:foreign-funcall "cairo_line_to"
			    :pointer cr
			    :double x
			    :double y
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_move_to (cr x y)
"
 void cairo_move_to (cairo_t *cr, double x, double y);

 Begin a new sub-path. After this call the current point will be (x, y).

    cr : a cairo context
    x : the X coordinate of the new position
    y : the Y coordinate of the new position

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_MOVE_TO invoked with null CR")
      (cffi:foreign-funcall "cairo_move_to"
			    :pointer cr
			    :double x
			    :double y
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_rectangle (cr x y width height)
"
 void cairo_rectangle (cairo_t *cr, double x, double y,
                       double width, double height);

 Adds a closed sub-path rectangle of the given size to the current path at
 position (x, y) in user-space coordinates.

 This function is logically equivalent to:

    1 cairo_move_to (cr, x, y);
    2 cairo_rel_line_to (cr, width, 0);
    3 cairo_rel_line_to (cr, 0, height);
    4 cairo_rel_line_to (cr, -width, 0);
    5 cairo_close_path (cr);

    cr : a cairo context
    x : the X coordinate of the top left corner of the rectangle
    y : the Y coordinate to the top left corner of the rectangle
    width : the width of the rectangle
    height : the height of the rectangle

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_RECTANGLE invoked with null CR")
      (cffi:foreign-funcall "cairo_rectangle"
			    :pointer cr
			    :double x
			    :double y
			    :double width
			    :double height
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_glyph_path (cr glyphs num-glyphs)
"
 void cairo_glyph_path (cairo_t *cr, const cairo_glyph_t *glyphs,int num_glyphs);

 Adds closed paths for the glyphs to the current path. The generated path if filled,
 achieves an effect similar to that of cairo_show_glyphs().

    cr : a cairo context
    glyphs : array of glyphs to show
    num_glyphs : number of glyphs to show

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_GLYPH_PATH invoked with null CR")
      (if (or (null glyphs) (cffi:null-pointer-p glyphs))
	  (error "CFFI/CAIRO_GLYPH_PATH invoked with null GLYPHS")
	  (cffi:foreign-funcall "cairo_glyph_path"
				:pointer cr
				:pointer glyphs
				:int num-glyphs
				:void)))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_text_path (cr utf8)
"
 void cairo_text_path (cairo_t *cr, const char *utf8);

 Adds closed paths for text to the current path. The generated path if filled,
 achieves an effect similar to that of cairo_show_text().

 Text conversion and positioning is done similar to cairo_show_text().

 Like cairo_show_text(), After this call the current point is moved to the
 origin of where the next glyph would be placed in this same progression. That
 is, the current point will be at the origin of the final glyph offset by its
 advance values. This allows for chaining multiple calls to to cairo_text_path()
 without having to set current point in between.

 Note: The cairo_text_path() function call is part of what the cairo designers
 call the 'toy' text API. It is convenient for short demos and simple programs,
 but it is not expected to be adequate for serious text-using applications. See
 cairo_glyph_path() for the 'real' text path API in cairo.

    cr : a cairo context
    utf8 : a NUL-terminated string of text encoded in UTF-8, or NULL

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_TEXT_PATH invoked with null CR")
      (let ((utf8 (or utf8 (cffi:null-pointer))))
	(cffi:foreign-funcall "cairo_text_path"
			      :pointer cr
			      :string utf8
			      :void)))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_rel_curve_to (cr dx1 dy1 dx2 dy2 dx3 dy3)
"
 void cairo_rel_curve_to (cairo_t *cr, double dx1, double dy1,
                                       double dx2, double dy2,
                                       double dx3, double dy3);

 Relative-coordinate version of cairo_curve_to(). All offsets are relative to
 the current point. Adds a cubic Bézier spline to the path from the current
 point to a point offset from the current point by (dx3, dy3), using points
 offset by (dx1, dy1) and (dx2, dy2) as the control points. After this call
 the current point will be offset by (dx3, dy3).

 Given a current point of (x, y),
 cairo_rel_curve_to(cr, dx1, dy1, dx2, dy2, dx3, dy3) is logically equivalent
 to cairo_curve_to(cr, x+dx1, y+dy1, x+dx2, y+dy2, x+dx3, y+dy3).

 It is an error to call this function with no current point. Doing so will
 cause cr to shutdown with a status of CAIRO_STATUS_NO_CURRENT_POINT.

    cr : a cairo context
    dx1 : the X offset to the first control point
    dy1 : the Y offset to the first control point
    dx2 : the X offset to the second control point
    dy2 : the Y offset to the second control point
    dx3 : the X offset to the end of the curve
    dy3 : the Y offset to the end of the curve

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_REL_CURVE_TO invoked with null CR")
      (cffi:foreign-funcall "cairo_rel_curve_to"
			    :pointer cr
			    :double dx1 :double dy1
			    :double dx2 :double dy2
			    :double dx3 :double dy3
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_rel_line_to (cr dx dy)
"
 void cairo_rel_line_to (cairo_t *cr, double dx, double dy);

 Relative-coordinate version of cairo_line_to(). Adds a line to the path from
 the current point to a point that is offset from the current point by (dx, dy)
 in user space. After this call the current point will be offset by (dx, dy).

 Given a current point of (x, y), cairo_rel_line_to(cr, dx, dy) is logically
 equivalent to cairo_line_to(cr, x + dx, y + dy).

 It is an error to call this function with no current point. Doing so will
 cause cr to shutdown with a status of CAIRO_STATUS_NO_CURRENT_POINT.

    cr : a cairo context
    dx : the X offset to the end of the new line
    dy : the Y offset to the end of the new line

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_REL_LINE_TO invoked with null CR")
      (cffi:foreign-funcall "cairo_rel_line_to"
			    :pointer cr
			    :double dx
			    :double dy
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_rel_move_to (cr dx dy)
"
 void cairo_rel_move_to (cairo_t *cr, double dx, double dy);

 Begin a new sub-path. After this call the current point will offset by (x, y).

 Given a current point of (x, y), cairo_rel_move_to(cr, dx, dy) is logically
 equivalent to cairo_move_to(cr, x + dx, y + dy).

 It is an error to call this function with no current point. Doing so will
 cause cr to shutdown with a status of CAIRO_STATUS_NO_CURRENT_POINT.

    cr : a cairo context
    dx : the X offset
    dy : the Y offset

 Since 1.0
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_REL_MOVE_TO invoked with null CR")
      (cffi:foreign-funcall "cairo_rel_move_to"
			    :pointer cr
			    :double dx
			    :double dy
			    :void))
  (cairo-status-report cr)
  cr)


(defun cffi/cairo_path_extents (cr x1 y1 x2 y2)
"
 void cairo_path_extents (cairo_t *cr, double *x1, double *y1,
                                       double *x2, double *y2);

 Computes a bounding box in user-space coordinates covering the points on the
 current path. If the current path is empty, returns an empty rectangle
 ((0,0), (0,0)). Stroke parameters, fill rule, surface dimensions and clipping
 are not taken into account.

 Contrast with cairo_fill_extents() and cairo_stroke_extents() which return
 the extents of only the area that would be 'inked' by the corresponding drawing
 operations.

 The result of cairo_path_extents() is defined as equivalent to the limit of
 cairo_stroke_extents() with CAIRO_LINE_CAP_ROUND as the line width approaches
 0.0, (but never reaching the empty-rectangle returned by cairo_stroke_extents()
 for a line width of 0.0).

 Specifically, this means that zero-area sub-paths such as
 cairo_move_to();cairo_line_to() segments, (even degenerate cases where the
 coordinates to both calls are identical), will be considered as contributing
 to the extents. However, a lone cairo_move_to() will not contribute to the
 results of cairo_path_extents().

    cr : a cairo context
    x1 : left of the resulting extents
    y1 : top of the resulting extents
    x2 : right of the resulting extents
    y2 : bottom of the resulting extents

 Since 1.6
"
  (if (or (null cr) (cffi:null-pointer-p cr))
      (error "CFFI/CAIRO_PATH_EXTENTS invoked with null CR")
      (if (or (null x1) (cffi:null-pointer-p x1))
	  (error "CFFI/CAIRO_PATH_EXTENTS invoked with null X1")
	  (if (or (null y1) (cffi:null-pointer-p y1))
	      (error "CFFI/CAIRO_PATH_EXTENTS invoked with null Y1")
	      (if (or (null x2) (cffi:null-pointer-p x2))
		  (error "CFFI/CAIRO_PATH_EXTENTS invoked with null X2")
		  (if (or (null y2) (cffi:null-pointer-p y2))
		      (error "CFFI/CAIRO_PATH_EXTENTS invoked with null Y2")
		      (cffi:foreign-funcall "cairo_path_extents"
					    :pointer cr
					    :pointer x1 :pointer y1
					    :pointer x2 :pointer y2
					    :void))))))
  (cairo-status-report cr)
  cr)


;;;;;;;;;; CAIRO_PATTERN_T

#||
cairo_pattern_t

typedef struct _cairo_pattern cairo_pattern_t;
A cairo_pattern_t represents a source when drawing onto a surface. There are different subtypes
of cairo_pattern_t, for different types of sources; for example, cairo_pattern_create_rgb() creates
a pattern for a solid opaque color.

Other than various cairo_pattern_create_type() functions, some of the pattern types can be
implicitly created using various cairo_set_source_type() functions; for example cairo_set_source_rgb().

The type of a pattern can be queried with cairo_pattern_get_type().

Memory management of cairo_pattern_t is done with cairo_pattern_reference() and cairo_pattern_destroy().

Since 1.0
||#


(defun cffi/cairo_pattern_add_color_stop_rgb (pattern offset red green blue)
"
 void cairo_pattern_add_color_stop_rgb (cairo_pattern_t *pattern, double offset,
                                        double red, double green, double blue);

 Adds an opaque color stop to a gradient pattern. The offset specifies the location along the
 gradient's control vector. For example, a linear gradient's control vector is from (x0,y0)
 to (x1,y1) while a radial gradient's control vector is from any point on the start circle to
 the corresponding point on the end circle.

 The color is specified in the same way as in cairo_set_source_rgb().

 If two (or more) stops are specified with identical offset values, they will be sorted
 according to the order in which the stops are added, (stops added earlier will compare less
 than stops added later). This can be useful for reliably making sharp color transitions
 instead of the typical blend.

 Note: If the pattern is not a gradient pattern, (eg. a linear or radial pattern), then the
 pattern will be put into an error status with a status of CAIRO_STATUS_PATTERN_TYPE_MISMATCH.

    pattern : a cairo_pattern_t
    offset : an offset in the range [0.0 .. 1.0]
    red : red component of color
    green : green component of color
    blue : blue component of color

 Since 1.0
"
  (if (or (null pattern) (cffi:null-pointer-p pattern))
      (error "CFFI/CAIRO_PATTERN_ADD_COLOR_STOP_RGB invoked with null PATTERN")
      (cffi:foreign-funcall "cairo_pattern_add_color_stop_rgb"
			    :pointer pattern
			    :double offset :double red :double green :double blue
			    :void))
  (cairo-pattern-status-report pattern)
  pattern)


(defun cffi/cairo_pattern_add_color_stop_rgba (pattern offset red green blue alpha)
"
 void cairo_pattern_add_color_stop_rgba (cairo_pattern_t *pattern,
                                         double offset,
                                         double red, double green, double blue,
                                         double alpha);

 Adds a translucent color stop to a gradient pattern. The offset specifies the location along
 the gradient's control vector. For example, a linear gradient's control vector is from (x0,y0)
 to (x1,y1) while a radial gradient's control vector is from any point on the start circle to
 the corresponding point on the end circle.

 The color is specified in the same way as in cairo_set_source_rgba().

 If two (or more) stops are specified with identical offset values, they will be sorted according
 to the order in which the stops are added, (stops added earlier will compare less than stops
 added later). This can be useful for reliably making sharp color transitions instead of the
 typical blend.

 Note: If the pattern is not a gradient pattern, (eg. a linear or radial pattern), then the
 pattern will be put into an error status with a status of CAIRO_STATUS_PATTERN_TYPE_MISMATCH.

    pattern : a cairo_pattern_t
    offset : an offset in the range [0.0 .. 1.0]
    red : red component of color
    green : green component of color
    blue : blue component of color
    alpha : alpha component of color

 Since 1.0
"
  (if (or (null pattern) (cffi:null-pointer-p pattern))
      (error "CFFI/CAIRO_PATTERN_ADD_COLOR_STOP_RGBA invoked with null PATTERN")
      (cffi:foreign-funcall "cairo_pattern_add_color_stop_rgba"
			    :pointer pattern
			    :double offset :double red :double green :double blue :double alpha
			    :void))
  (cairo-pattern-status-report pattern)
  pattern)


(defun cffi/cairo_pattern_get_color_stop_count (pattern count)
"
 cairo_status_t cairo_pattern_get_color_stop_count (cairo_pattern_t *pattern,
                                                    int *count);

 Gets the number of color stops specified in the given gradient pattern.

    pattern : a cairo_pattern_t
    count : return value for the number of color stops, or NULL

    Returns : CAIRO_STATUS_SUCCESS, or CAIRO_STATUS_PATTERN_TYPE_MISMATCH if pattern
              is not a gradient pattern.

 Since 1.4
"
  (if (or (null pattern) (cffi:null-pointer-p pattern))
      (error "CFFI/CAIRO_PATTERN_GET_COLOR_STOP_COUNT invoked with null PATTERN")
      (let* ((count (or count (cffi:null-pointer))))
	(cffi:foreign-funcall "cairo_pattern_get_color_stop_count"
			      :pointer pattern
			      :pointer count
			      :int))))


(defun cffi/cairo_pattern_get_color_stop_rgba (pattern index offset red green blue alpha)
"
 cairo_status_t cairo_pattern_get_color_stop_rgba (cairo_pattern_t *pattern,
                                                   int index, double *offset,
                                                   double *red, double *green, double *blue,
                                                   double *alpha);

 Gets the color and offset information at the given index for a gradient pattern. Values of index
 are 0 to 1 less than the number returned by cairo_pattern_get_color_stop_count().

    pattern : a cairo_pattern_t
    index : index of the stop to return data for
    offset : return value for the offset of the stop, or NULL
    red : return value for red component of color, or NULL
    green : return value for green component of color, or NULL
    blue : return value for blue component of color, or NULL
    alpha : return value for alpha component of color, or NULL

    Returns : CAIRO_STATUS_SUCCESS, or CAIRO_STATUS_INVALID_INDEX if index is not valid for the
              given pattern. If the pattern is not a gradient pattern,
    CAIRO_STATUS_PATTERN_TYPE_MISMATCH is returned.

 Since 1.4
"
  (if (or (null pattern) (cffi:null-pointer-p pattern))
      (error "CFFI/CAIRO_PATTERN_GET_COLOR_STOP_RGBA invoked with null PATTERN")
      (let ((offset (or offset (cffi:null-pointer)))
	    (red    (or red    (cffi:null-pointer)))
	    (green  (or green  (cffi:null-pointer)))
	    (blue   (or blue   (cffi:null-pointer)))
	    (alpha  (or alpha  (cffi:null-pointer))))
	(cffi:foreign-funcall "cairo_pattern_get_color_stop_rgba"
			      :pointer pattern
			      :int index
			      :pointer offset
			      :pointer red :pointer green :pointer blue :pointer alpha
			      :int))))


(defun cffi/cairo_pattern_create_rgb (red green blue)
"
 cairo_pattern_t * cairo_pattern_create_rgb (double red, double green, double blue);

 Creates a new cairo_pattern_t corresponding to an opaque color. The color components are
 floating point numbers in the range 0 to 1. If the values passed in are outside that
 range, they will be clamped.

    red : red component of the color
    green : green component of the color
    blue : blue component of the color

    Returns : the newly created cairo_pattern_t if successful, or an error pattern in case
              of no memory. The caller owns the returned object and should call
    cairo_pattern_destroy() when finished with it. This function will always return a valid
    pointer, but if an error occurred the pattern status will be set to an error. To inspect
    the status of a pattern use cairo_pattern_status().

 Since 1.0
"
  (let ((pattern (cffi:foreign-funcall "cairo_pattern_create_rgb"
				       :double red :double green :double blue
				       :pointer)))
    (cairo-pattern-status-report pattern)
    pattern))



(defun cffi/cairo_pattern_create_rgba (red green blue alpha)
"
 cairo_pattern_t * cairo_pattern_create_rgba (double red,  double green,
                                              double blue, double alpha);

 Creates a new cairo_pattern_t corresponding to a translucent color. The color components
 are floating point numbers in the range 0 to 1. If the values passed in are outside that
 range, they will be clamped.

    red : red component of the color
    green : green component of the color
    blue : blue component of the color
    alpha : alpha component of the color

    Returns : the newly created cairo_pattern_t if successful, or an error pattern in case
              of no memory. The caller owns the returned object and should call
    cairo_pattern_destroy() when finished with it. This function will always return a valid
    pointer, but if an error occurred the pattern status will be set to an error. To inspect
    the status of a pattern use cairo_pattern_status().

 Since 1.0
"
  (let ((pattern (cffi:foreign-funcall "cairo_pattern_create_rgba"
				       :double red :double green :double blue :double alpha
				       :pointer)))
    (cairo-pattern-status-report pattern)
    pattern))



(defun cffi/cairo_pattern_get_rgba (pattern red green blue alpha)
"
 cairo_status_t cairo_pattern_get_rgba (cairo_pattern_t *pattern,
                                        double *red, double *green, double *blue,
                                        double *alpha);

 Gets the solid color for a solid color pattern.

    pattern : a cairo_pattern_t
    red : return value for red component of color, or NULL
    green : return value for green component of color, or NULL
    blue : return value for blue component of color, or NULL
    alpha : return value for alpha component of color, or NULL

    Returns : CAIRO_STATUS_SUCCESS, or CAIRO_STATUS_PATTERN_TYPE_MISMATCH if the
              pattern is not a solid color pattern.

 Since 1.4
"
  (if (or (null pattern) (cffi:null-pointer-p pattern))
      (error "CFFI/CAIRO_PATTERN_GET_RGBA invoked with null PATTERN")
      (let ((red   (or red (cffi:null-pointer)))
	    (green (or green (cffi:null-pointer)))
	    (blue  (or blue  (cffi:null-pointer)))
	    (alpha (or alpha (cffi:null-pointer))))
	(cffi:foreign-funcall "cairo_pattern_get_rgba"
			      :pointer pattern
			      :pointer red :pointer green :pointer blue
			      :pointer alpha
			      :int))))


(defun cffi/cairo_pattern_create_for_surface (surface)
"
 cairo_pattern_t * cairo_pattern_create_for_surface (cairo_surface_t *surface);

 Create a new cairo_pattern_t for the given surface.

    surface : the surface

    Returns : the newly created cairo_pattern_t if successful, or an error pattern in case
              of no memory. The caller owns the returned object and should call
    cairo_pattern_destroy() when finished with it. This function will always return a valid
    pointer, but if an error occurred the pattern status will be set to an error. To inspect
    the status of a pattern use cairo_pattern_status().

 Since 1.0
"
  (if (or (null surface) (cffi:null-pointer-p surface))
      (error "CFFI/CAIRO_PATTERN_CREATE_FOR_SURFACE invoked with null SURFACE")
      (let ((pattern (cffi:foreign-funcall "cairo_pattern_create_for_surface"
					   :pointer surface
					   :pointer)))
	(cairo-pattern-status-report pattern)
	pattern)))



(defun cffi/cairo_pattern_get_surface (pattern surface)
"
 cairo_status_t cairo_pattern_get_surface (cairo_pattern_t *pattern,
                                           cairo_surface_t **surface);

 Gets the surface of a surface pattern. The reference returned in surface is owned by the
 pattern; the caller should call cairo_surface_reference() if the surface is to be
 retained.

    pattern : a cairo_pattern_t
    surface : return value for surface of pattern, or NULL

    Returns : CAIRO_STATUS_SUCCESS, or CAIRO_STATUS_PATTERN_TYPE_MISMATCH if the pattern
              is not a surface pattern.

 Since 1.4
"
  (if (or (null pattern) (cffi:null-pointer-p pattern))
      (error "CFFI/CAIRO_PATTERN_GET_SURFACE invoked with null PATTERN")
      (let ((surface (or surface (cffi:null-pointer))))
	(cffi:foreign-funcall "cairo_pattern_get_surface"
			      :pointer pattern
			      :pointer surface
			      :int))))


(defun cffi/cairo_pattern_create_linear (x0 y0 x1 y1)
"
 cairo_pattern_t * cairo_pattern_create_linear (double x0, double y0,
                                                double x1, double y1);

 Create a new linear gradient cairo_pattern_t along the line defined by (x0, y0) and
 (x1, y1). Before using the gradient pattern, a number of color stops should be defined
 using cairo_pattern_add_color_stop_rgb() or cairo_pattern_add_color_stop_rgba().

 Note: The coordinates here are in pattern space. For a new pattern, pattern space is
 identical to user space, but the relationship between the spaces can be changed with
 cairo_pattern_set_matrix().

    x0 : x coordinate of the start point
    y0 : y coordinate of the start point
    x1 : x coordinate of the end point
    y1 : y coordinate of the end point

    Returns : the newly created cairo_pattern_t if successful, or an error pattern in case
              of no memory. The caller owns the returned object and should call
    cairo_pattern_destroy() when finished with it. This function will always return a valid
    pointer, but if an error occurred the pattern status will be set to an error. To inspect
    the status of a pattern use cairo_pattern_status().

 Since 1.0
"
  (let ((pattern (cffi:foreign-funcall "cairo_pattern_create_linear"
				       :double x0 :double y0
				       :double x1 :double y1
				       :pointer)))
    (cairo-pattern-status-report pattern)
    pattern))



(defun cffi/cairo_pattern_get_linear_points (pattern x0 y0 x1 y1)
"
 cairo_status_t cairo_pattern_get_linear_points (cairo_pattern_t *pattern,
                                                 double *x0, double *y0,
                                                 double *x1, double *y1);

 Gets the gradient endpoints for a linear gradient.

    pattern : a cairo_pattern_t
    x0 : return value for the x coordinate of the first point, or NULL
    y0 : return value for the y coordinate of the first point, or NULL
    x1 : return value for the x coordinate of the second point, or NULL
    y1 : return value for the y coordinate of the second point, or NULL

    Returns : CAIRO_STATUS_SUCCESS, or CAIRO_STATUS_PATTERN_TYPE_MISMATCH
              if pattern is not a linear gradient pattern.

 Since 1.4
"
  (if (or (null pattern) (cffi:null-pointer-p pattern))
      (error "CFFI/CAIRO_PATTERN_GET_LINEAR_POINTS invoked with null PATTERN")
      (let ((x0 (or x0 (cffi:null-pointer)))
	    (y0 (or y0 (cffi:null-pointer)))
	    (x1 (or x1 (cffi:null-pointer)))
	    (y1 (or y1 (cffi:null-pointer))))
	(cffi:foreign-funcall "cairo_pattern_get_linear_points"
			      :pointer pattern
			      :pointer x0 :pointer y0 :pointer x1 :pointer y1
			      :int))))


(defun cffi/cairo_pattern_create_radial (cx0 cy0 radius0 cx1 cy1 radius1)
"
 cairo_pattern_t * cairo_pattern_create_radial (double cx0, double cy0,
                                                double radius0,
                                                double cx1, double cy1,
                                                double radius1);

 Creates a new radial gradient cairo_pattern_t between the two circles defined by
 (cx0, cy0, radius0) and (cx1, cy1, radius1). Before using the gradient pattern, a
 number of color stops should be defined using cairo_pattern_add_color_stop_rgb()
 or cairo_pattern_add_color_stop_rgba().

 Note: The coordinates here are in pattern space. For a new pattern, pattern space
 is identical to user space, but the relationship between the spaces can be changed
 with cairo_pattern_set_matrix().

    cx0 : x coordinate for the center of the start circle
    cy0 : y coordinate for the center of the start circle
    radius0 : radius of the start circle
    cx1 : x coordinate for the center of the end circle
    cy1 : y coordinate for the center of the end circle
    radius1 : radius of the end circle

    Returns : the newly created cairo_pattern_t if successful, or an error pattern in
              case of no memory. The caller owns the returned object and should call
    cairo_pattern_destroy() when finished with it. This function will always return a
    valid pointer, but if an error occurred the pattern status will be set to an error.
    To inspect the status of a pattern use cairo_pattern_status().

 Since 1.0
"
  (let ((pattern (cffi:foreign-funcall "cairo_pattern_create_radial"
				       :double cx0 :double cy0 :double radius0
				       :double cx1 :double cy1 :double radius1
				       :pointer)))
    (cairo-pattern-status-report pattern)
    pattern))



(defun cffi/cairo_pattern_get_radial_circles (pattern x0 y0 r0 x1 y1 r1)
"
 cairo_status_t cairo_pattern_get_radial_circles (cairo_pattern_t *pattern,
                                                  double *x0, double *y0, double *r0,
                                                  double *x1, double *y1, double *r1);

 Gets the gradient endpoint circles for a radial gradient, each specified as a center
 coordinate and a radius.

    pattern : a cairo_pattern_t
    x0 : return value for the x coordinate of the center of the first circle, or NULL
    y0 : return value for the y coordinate of the center of the first circle, or NULL
    r0 : return value for the radius of the first circle, or NULL
    x1 : return value for the x coordinate of the center of the second circle, or NULL
    y1 : return value for the y coordinate of the center of the second circle, or NULL
    r1 : return value for the radius of the second circle, or NULL

    Returns : CAIRO_STATUS_SUCCESS, or CAIRO_STATUS_PATTERN_TYPE_MISMATCH if pattern
              is not a radial gradient pattern.

 Since 1.4
"
  (if (or (null pattern) (cffi:null-pointer-p pattern))
      (error "CFFI/CAIRO_PATTERN_GET_RADIAL_CIRCLES invoked with null PATTERN")
      (let ((x0 (or x0 (cffi:null-pointer)))
	    (y0 (or y0 (cffi:null-pointer)))
	    (r0 (or r0 (cffi:null-pointer)))
	    (x1 (or x1 (cffi:null-pointer)))
	    (y1 (or y1 (cffi:null-pointer)))
	    (r1 (or r1 (cffi:null-pointer))))
	(cffi:foreign-funcall "cairo_pattern_get_radial_circles"
			      :pointer pattern
			      :pointer x0 :pointer y0 :pointer r0
			      :pointer x1 :pointer y1 :pointer r1
			      :int))))


(defun cffi/cairo_pattern_create_mesh ()
"
 cairo_pattern_t * cairo_pattern_create_mesh (void);

 Create a new mesh pattern.

 Mesh patterns are tensor-product patch meshes (type 7 shadings in PDF). Mesh patterns may
 also be used to create other types of shadings that are special cases of tensor-product
 patch meshes such as Coons patch meshes (type 6 shading in PDF) and Gouraud-shaded
 triangle meshes (type 4 and 5 shadings in PDF).

 Mesh patterns consist of one or more tensor-product patches, which should be defined before
 using the mesh pattern. Using a mesh pattern with a partially defined patch as source or
 mask will put the context in an error status with a status of
 CAIRO_STATUS_INVALID_MESH_CONSTRUCTION.

 A tensor-product patch is defined by 4 Bézier curves (side 0, 1, 2, 3) and by 4 additional
 control points (P0, P1, P2, P3) that provide further control over the patch and complete
 the definition of the tensor-product patch. The corner C0 is the first point of the patch.

 Degenerate sides are permitted so straight lines may be used. A zero length line on one
 side may be used to create 3 sided patches.

       C1     Side 1       C2
        +---------------+
        |               |
        |  P1       P2  |
        |               |
 Side 0 |               | Side 2
        |               |
        |               |
        |  P0       P3  |
        |               |
        +---------------+
      C0     Side 3        C3

 Each patch is constructed by first calling cairo_mesh_pattern_begin_patch(), then
 cairo_mesh_pattern_move_to() to specify the first point in the patch (C0). Then the sides
 are specified with calls to cairo_mesh_pattern_curve_to() and cairo_mesh_pattern_line_to().

 The four additional control points (P0, P1, P2, P3) in a patch can be specified with
 cairo_mesh_pattern_set_control_point().

 At each corner of the patch (C0, C1, C2, C3) a color may be specified with
 cairo_mesh_pattern_set_corner_color_rgb() or cairo_mesh_pattern_set_corner_color_rgba().
 Any corner whose color is not explicitly specified defaults to transparent black.

 A Coons patch is a special case of the tensor-product patch where the control points are
 implicitly defined by the sides of the patch. The default value for any control point not
 specified is the implicit value for a Coons patch, i.e. if no control points are specified
 the patch is a Coons patch.

 A triangle is a special case of the tensor-product patch where the control points are
 implicitly defined by the sides of the patch, all the sides are lines and one of them has
 length 0, i.e. if the patch is specified using just 3 lines, it is a triangle. If the
 corners connected by the 0-length side have the same color, the patch is a Gouraud-shaded
 triangle.

 Patches may be oriented differently to the above diagram. For example the first point could
 be at the top left. The diagram only shows the relationship between the sides, corners
 and control points. Regardless of where the first point is located, when specifying colors,
 corner 0 will always be the first point, corner 1 the point between side 0 and side 1 etc.

 Calling cairo_mesh_pattern_end_patch() completes the current patch. If less than 4 sides
 have been defined, the first missing side is defined as a line from the current point to
 the first point of the patch (C0) and the other sides are degenerate lines from C0 to C0.
 The corners between the added sides will all be coincident with C0 of the patch and their
 color will be set to be the same as the color of C0.

 Additional patches may be added with additional calls to cairo_mesh_pattern_begin_patch()/
 cairo_mesh_pattern_end_patch().

    1  cairo_pattern_t *pattern = cairo_pattern_create_mesh ();
    2  
    3  /* Add a Coons patch */
    4  cairo_mesh_pattern_begin_patch (pattern);
    5  cairo_mesh_pattern_move_to (pattern, 0, 0);
    6  cairo_mesh_pattern_curve_to (pattern, 30, -30,  60,  30, 100, 0);
    7  cairo_mesh_pattern_curve_to (pattern, 60,  30, 130,  60, 100, 100);
    8  cairo_mesh_pattern_curve_to (pattern, 60,  70,  30, 130,   0, 100);
    9  cairo_mesh_pattern_curve_to (pattern, 30,  70, -30,  30,   0, 0);
    10 cairo_mesh_pattern_set_corner_color_rgb (pattern, 0, 1, 0, 0);
    11 cairo_mesh_pattern_set_corner_color_rgb (pattern, 1, 0, 1, 0);
    12 cairo_mesh_pattern_set_corner_color_rgb (pattern, 2, 0, 0, 1);
    13 cairo_mesh_pattern_set_corner_color_rgb (pattern, 3, 1, 1, 0);
    14 cairo_mesh_pattern_end_patch (pattern);
    15 
    16 /* Add a Gouraud-shaded triangle */
    17 cairo_mesh_pattern_begin_patch (pattern)
    18 cairo_mesh_pattern_move_to (pattern, 100, 100);
    19 cairo_mesh_pattern_line_to (pattern, 130, 130);
    20 cairo_mesh_pattern_line_to (pattern, 130,  70);
    21 cairo_mesh_pattern_set_corner_color_rgb (pattern, 0, 1, 0, 0);
    22 cairo_mesh_pattern_set_corner_color_rgb (pattern, 1, 0, 1, 0);
    23 cairo_mesh_pattern_set_corner_color_rgb (pattern, 2, 0, 0, 1);
    24 cairo_mesh_pattern_end_patch (pattern)

 When two patches overlap, the last one that has been added is drawn over the first one.

 When a patch folds over itself, points are sorted depending on their parameter coordinates
 inside the patch. The v coordinate ranges from 0 to 1 when moving from side 3 to side 1;
 the u coordinate ranges from 0 to 1 when going from side 0 to side 2. Points with higher v
 coordinate hide points with lower v coordinate. When two points have the same v coordinate,
 the one with higher u coordinate is above. This means that points nearer to side 1 are above
 points nearer to side 3; when this is not sufficient to decide which point is above (for
 example when both points belong to side 1 or side 3) points nearer to side 2 are above
 points nearer to side 0.

 For a complete definition of tensor-product patches, see the PDF specification (ISO32000),
 which describes the parametrization in detail.

 Note: The coordinates are always in pattern space. For a new pattern, pattern space is
 identical to user space, but the relationship between the spaces can be changed with
 cairo_pattern_set_matrix().

    Returns : the newly created cairo_pattern_t if successful, or an error pattern in case of no
              memory. The caller owns the returned object and should call cairo_pattern_destroy()
    when finished with it. This function will always return a valid pointer, but if an error
    occurred the pattern status will be set to an error. To inspect the status of a pattern use
    cairo_pattern_status().

 Since 1.12
"
  (let ((mesh (cffi:foreign-funcall "cairo_pattern_create_mesh"
				    :pointer)))
    (cairo-pattern-status-report mesh)
    mesh))



(defun cffi/cairo_mesh_pattern_begin_patch (pattern)
"
 void cairo_mesh_pattern_begin_patch (cairo_pattern_t *pattern);

 Begin a patch in a mesh pattern.

 After calling this function, the patch shape should be defined with cairo_mesh_pattern_move_to(),
 cairo_mesh_pattern_line_to() and cairo_mesh_pattern_curve_to().

 After defining the patch, cairo_mesh_pattern_end_patch() must be called before using pattern as
 a source or mask.

 Note: If pattern is not a mesh pattern then pattern will be put into an error status with a
 status of CAIRO_STATUS_PATTERN_TYPE_MISMATCH. If pattern already has a current patch, it will be
 put into an error status with a status of CAIRO_STATUS_INVALID_MESH_CONSTRUCTION.

    pattern : a cairo_pattern_t

 Since 1.12
"
  (if (or (null pattern) (cffi:null-pointer-p pattern))
      (error "CFFI/CAIRO_MESH_PATTERN_BEGIN_PATCH invoked with null PATTERN")
      (cffi:foreign-funcall "cairo_mesh_pattern_begin_patch"
			    :pointer pattern
			    :void))
  (cairo-pattern-status-report pattern)
  pattern)


(defun cffi/cairo_mesh_pattern_end_patch (pattern)
"
 void cairo_mesh_pattern_end_patch (cairo_pattern_t *pattern);

 Indicates the end of the current patch in a mesh pattern.

 If the current patch has less than 4 sides, it is closed with a straight line from the current
 point to the first point of the patch as if cairo_mesh_pattern_line_to() was used.

 Note: If pattern is not a mesh pattern then pattern will be put into an error status with a
 status of CAIRO_STATUS_PATTERN_TYPE_MISMATCH. If pattern has no current patch or the current
 patch has no current point, pattern will be put into an error status with a status of
 CAIRO_STATUS_INVALID_MESH_CONSTRUCTION.

    pattern : a cairo_pattern_t

 Since 1.12
"
  (if (or (null pattern) (cffi:null-pointer-p pattern))
      (error "CFFI/CAIRO_MESH_PATTERN_END_PATCH invoked with null PATTERN")
      (cffi:foreign-funcall "cairo_mesh_pattern_end_patch"
			    :pointer pattern
			    :void))
  (cairo-pattern-status-report pattern)
  pattern)


(defun cffi/cairo_mesh_pattern_move_to (pattern x y)
"
 void cairo_mesh_pattern_move_to (cairo_pattern_t *pattern,
                                  double x, double y);

 Define the first point of the current patch in a mesh pattern.

 After this call the current point will be (x, y).

 Note: If pattern is not a mesh pattern then pattern will be put into an error status with a
 status of CAIRO_STATUS_PATTERN_TYPE_MISMATCH. If pattern has no current patch or the current
 patch already has at least one side, pattern will be put into an error status with a status
 of CAIRO_STATUS_INVALID_MESH_CONSTRUCTION.

    pattern : a cairo_pattern_t
    x : the X coordinate of the new position
    y : the Y coordinate of the new position

 Since 1.12
"
  (if (or (null pattern) (cffi:null-pointer-p pattern))
      (error "CFFI/CAIRO_MESH_PATTERN_MOVE_TO invoked with null PATTERN")
      (cffi:foreign-funcall "cairo_mesh_pattern_move_to"
			    :pointer pattern
			    :double x :double y
			    :void))
  (cairo-pattern-status-report pattern)
  pattern)


(defun cffi/cairo_mesh_pattern_line_to (pattern x y)
"
 void cairo_mesh_pattern_line_to (cairo_pattern_t *pattern,
                                  double x, double y);

 Adds a line to the current patch from the current point to position (x, y) in pattern-space
 coordinates.

 If there is no current point before the call to cairo_mesh_pattern_line_to() this function
 will behave as cairo_mesh_pattern_move_to(pattern, x, y).

 After this call the current point will be (x, y).

 Note: If pattern is not a mesh pattern then pattern will be put into an error status with a
 status of CAIRO_STATUS_PATTERN_TYPE_MISMATCH. If pattern has no current patch or the current
 patch already has 4 sides, pattern will be put into an error status with a status of
 CAIRO_STATUS_INVALID_MESH_CONSTRUCTION.

    pattern : a cairo_pattern_t
    x : the X coordinate of the end of the new line
    y : the Y coordinate of the end of the new line

 Since 1.12
"
  (if (or (null pattern) (cffi:null-pointer-p pattern))
      (error "CFFI/CAIRO_MESH_PATTERN_LINE_TO invoked with null PATTERN")
      (cffi:foreign-funcall "cairo_mesh_pattern_line_to"
			    :pointer pattern
			    :double x :double y
			    :void))
  (cairo-pattern-status-report pattern)
  pattern)


(defun cffi/cairo_mesh_pattern_curve_to (pattern x1 y1 x2 y2 x3 y3)
"
 void cairo_mesh_pattern_curve_to (cairo_pattern_t *pattern,
                                   double x1, double y1,
                                   double x2, double y2,
                                   double x3, double y3);

 Adds a cubic Bézier spline to the current patch from the current point to position (x3, y3)
 in pattern-space coordinates, using (x1, y1) and (x2, y2) as the control points.

 If the current patch has no current point before the call to cairo_mesh_pattern_curve_to(),
 this function will behave as if preceded by a call to
 cairo_mesh_pattern_move_to(pattern, x1, y1).

 After this call the current point will be (x3, y3).

 Note: If pattern is not a mesh pattern then pattern will be put into an error status with a
 status of CAIRO_STATUS_PATTERN_TYPE_MISMATCH. If pattern has no current patch or the current
 patch already has 4 sides, pattern will be put into an error status with a status of
 CAIRO_STATUS_INVALID_MESH_CONSTRUCTION.

    pattern : a cairo_pattern_t
    x1 : the X coordinate of the first control point
    y1 : the Y coordinate of the first control point
    x2 : the X coordinate of the second control point
    y2 : the Y coordinate of the second control point
    x3 : the X coordinate of the end of the curve
    y3 : the Y coordinate of the end of the curve

 Since 1.12
"
  (if (or (null pattern) (cffi:null-pointer-p pattern))
      (error "CFFI/CAIRO_MESH_PATTERN_CURVE_TO invoked with null PATTERN")
      (cffi:foreign-funcall "cairo_mesh_pattern_curve_to"
			    :pointer pattern
			    :double x1 :double y1
			    :double x2 :double y2
			    :double x3 :double y3
			    :void))
  (cairo-pattern-status-report pattern)
  pattern)


(defun cffi/cairo_mesh_pattern_set_control_point (pattern point-num x y)
"
 void cairo_mesh_pattern_set_control_point (cairo_pattern_t *pattern,
                                            unsigned int point_num,
                                            double x, double y);

 Set an internal control point of the current patch.

 Valid values for point_num are from 0 to 3 and identify the control points as explained
 in cairo_pattern_create_mesh().

 Note: If pattern is not a mesh pattern then pattern will be put into an error status with
 a status of CAIRO_STATUS_PATTERN_TYPE_MISMATCH. If point_num is not valid, pattern will
 be put into an error status with a status of CAIRO_STATUS_INVALID_INDEX. If pattern has
 no current patch, pattern will be put into an error status with a status of
 CAIRO_STATUS_INVALID_MESH_CONSTRUCTION.

    pattern : a cairo_pattern_t
    point_num : the control point to set the position for
    x : the X coordinate of the control point
    y : the Y coordinate of the control point

 Since 1.12
"
  (if (or (null pattern) (cffi:null-pointer-p pattern))
      (error "CFFI/CAIRO_MESH_PATTERN_SET_CONTROL_POINT invoked with null PATTERN")
      (cffi:foreign-funcall "cairo_mesh_pattern_set_control_point"
			    :pointer pattern
			    :uint point-num
			    :double x :double y
			    :void))
  (cairo-pattern-status-report pattern)
  pattern)


(defun cffi/cairo_mesh_pattern_set_corner_color_rgb (pattern corner-num red green blue)
"
 void cairo_mesh_pattern_set_corner_color_rgb (cairo_pattern_t *pattern,
                                               unsigned int corner_num,
                                               double red, double green, double blue);

 Sets the color of a corner of the current patch in a mesh pattern.

 The color is specified in the same way as in cairo_set_source_rgb().

 Valid values for corner_num are from 0 to 3 and identify the corners as explained in
 cairo_pattern_create_mesh().

 Note: If pattern is not a mesh pattern then pattern will be put into an error status
 with a status of CAIRO_STATUS_PATTERN_TYPE_MISMATCH. If corner_num is not valid, pattern
 will be put into an error status with a status of CAIRO_STATUS_INVALID_INDEX. If pattern
 has no current patch, pattern will be put into an error status with a status of
 CAIRO_STATUS_INVALID_MESH_CONSTRUCTION.

    pattern : a cairo_pattern_t
    corner_num : the corner to set the color for
    red : red component of color
    green : green component of color
    blue : blue component of color

 Since 1.12
"
  (if (or (null pattern) (cffi:null-pointer-p pattern))
      (error "CFFI/CAIRO_MESH_PATTERN_SET_CORNER_COLOR_RGB invoked with null PATTERN")
      (cffi:foreign-funcall "cairo_mesh_pattern_set_corner_color_rgb"
			    :pointer pattern
			    :uint corner-num
			    :double red :double green :double blue
			    :void))
  (cairo-pattern-status-report pattern)
  pattern)


(defun cffi/cairo_mesh_pattern_set_corner_color_rgba (pattern corner-num red green blue alpha)
"
 void cairo_mesh_pattern_set_corner_color_rgba (cairo_pattern_t *pattern,
                                                unsigned int corner_num,
                                                double red, double green, double blue,
                                                double alpha);

 Sets the color of a corner of the current patch in a mesh pattern.

 The color is specified in the same way as in cairo_set_source_rgba().

 Valid values for corner_num are from 0 to 3 and identify the corners as explained in
 cairo_pattern_create_mesh().

 Note: If pattern is not a mesh pattern then pattern will be put into an error status with a
 status of CAIRO_STATUS_PATTERN_TYPE_MISMATCH. If corner_num is not valid, pattern will be put
 into an error status with a status of CAIRO_STATUS_INVALID_INDEX. If pattern has no current
 patch, pattern will be put into an error status with a status of
 CAIRO_STATUS_INVALID_MESH_CONSTRUCTION.

    pattern : a cairo_pattern_t
    corner_num : the corner to set the color for
    red : red component of color
    green : green component of color
    blue : blue component of color
    alpha : alpha component of color

 Since 1.12
"
  (if (or (null pattern) (cffi:null-pointer-p pattern))
      (error "CFFI/CAIRO_MESH_PATTERN_SET_CORNER_COLOR_RGBA invoked with null PATTERN")
      (cffi:foreign-funcall "cairo_mesh_pattern_set_corner_color_rgba"
			    :pointer pattern
			    :uint corner-num
			    :double red :double green :double blue
			    :double alpha
			    :void))
  (cairo-pattern-status-report pattern)
  pattern)


(defun cffi/cairo_mesh_pattern_get_patch_count (pattern count)
"
 cairo_status_t cairo_mesh_pattern_get_patch_count (cairo_pattern_t *pattern,
                                                    unsigned int *count);

 Gets the number of patches specified in the given mesh pattern.

 The number only includes patches which have been finished by calling
 cairo_mesh_pattern_end_patch(). For example it will be 0 during the definition of the
 first patch.

    pattern : a cairo_pattern_t
    count : return value for the number patches, or NULL

    Returns : CAIRO_STATUS_SUCCESS, or CAIRO_STATUS_PATTERN_TYPE_MISMATCH if pattern
    is not a mesh pattern.

 Since 1.12
"
  (if (or (null pattern) (cffi:null-pointer-p pattern))
      (error "CFFI/CAIRO_MESH_PATTERN_GET_PATCH_COUNT invoked with null PATTERN")
      (let ((count (or count (cffi:null-pointer))))
	(cffi:foreign-funcall "cairo_mesh_pattern_get_patch_count"
			      :pointer pattern
			      :pointer count
			      :int))))


(defun cffi/cairo_mesh_pattern_get_path (pattern patch-num)
"
 cairo_path_t * cairo_mesh_pattern_get_path (cairo_pattern_t *pattern,
                                             unsigned int patch_num);

 Gets path defining the patch patch_num for a mesh pattern.

 patch_num can range 0 to 1 less than the number returned by
 cairo_mesh_pattern_get_patch_count().

    pattern : a cairo_pattern_t
    patch_num : the patch number to return data for

    Returns : the path defining the patch, or a path with status
              CAIRO_STATUS_INVALID_INDEX if patch_num or point_num is not valid
    for pattern. If pattern is not a mesh pattern, a path with status
    CAIRO_STATUS_PATTERN_TYPE_MISMATCH is returned.

 Since 1.12
"
  (if (or (null pattern) (cffi:null-pointer-p pattern))
      (error "CFFI/CAIRO_MESH_PATTERN_GET_PATH invoked with null PATTERN")
      (let ((path (cffi:foreign-funcall "cairo_mesh_pattern_get_path"
					:pointer pattern
					:uint patch-num
					:pointer)))
	;; FIXME: NEVER NIL
	(pointer-or-nil path))))


(defun cffi/cairo_mesh_pattern_get_control_point (pattern patch-num point-num x y)
"
 cairo_status_t cairo_mesh_pattern_get_control_point (cairo_pattern_t *pattern,
                                                      unsigned int patch_num,
                                                      unsigned int point_num,
                                                      double *x, double *y);

 Gets the control point point_num of patch patch_num for a mesh pattern.

 patch_num can range 0 to 1 less than the number returned by cairo_mesh_pattern_get_patch_count().

 Valid values for point_num are from 0 to 3 and identify the control points as explained in
 cairo_pattern_create_mesh().

    pattern : a cairo_pattern_t
    patch_num : the patch number to return data for
    point_num : the control point number to return data for
    x : return value for the x coordinate of the control point, or NULL
    y : return value for the y coordinate of the control point, or NULL

    Returns : CAIRO_STATUS_SUCCESS, or CAIRO_STATUS_INVALID_INDEX if patch_num or point_num is not
              valid for pattern. If pattern is not a mesh pattern, CAIRO_STATUS_PATTERN_TYPE_MISMATCH
              is returned.

 Since 1.12
"
  (if (or (null pattern) (cffi:null-pointer-p pattern))
      (error "CFFI/CAIRO_MESH_PATTERN_GET_CONTROL_POINT invoked with null PATTERN")
      (let ((x (or x (cffi:null-pointer)))
	    (y (or y (cffi:null-pointer))))
	(cffi:foreign-funcall "cairo_mesh_pattern_get_control_point"
			      :pointer pattern
			      :uint patch-num
			      :uint point-num
			      :pointer x :pointer y
			      :int))))


(defun cffi/cairo_mesh_pattern_get_corner_color_rgba (pattern patch-num corner-num red green blue alpha)
"
 cairo_status_t cairo_mesh_pattern_get_corner_color_rgba (cairo_pattern_t *pattern,
                                                          unsigned int patch_num,
                                                          unsigned int corner_num,
                                                          double *red, double *green, double *blue,
                                                          double *alpha);

 Gets the color information in corner corner_num of patch patch_num for a mesh pattern.

 patch_num can range 0 to 1 less than the number returned by cairo_mesh_pattern_get_patch_count().

 Valid values for corner_num are from 0 to 3 and identify the corners as explained in
 cairo_pattern_create_mesh().

    pattern : a cairo_pattern_t
    patch_num : the patch number to return data for
    corner_num : the corner number to return data for
    red : return value for red component of color, or NULL
    green : return value for green component of color, or NULL
    blue : return value for blue component of color, or NULL
    alpha : return value for alpha component of color, or NULL
    
    Returns : CAIRO_STATUS_SUCCESS, or CAIRO_STATUS_INVALID_INDEX if patch_num or corner_num is not
              valid for pattern. If pattern is not a mesh pattern, CAIRO_STATUS_PATTERN_TYPE_MISMATCH
              is returned.

 Since 1.12
"
  (if (or (null pattern) (cffi:null-pointer-p pattern))
      (error "CFFI/CAIRO_MESH_PATTERN_GET_CORNER_COLOR_RGBA invoked with null PATTERN")
      (let ((red   (or red   (cffi:null-pointer)))
	    (green (or green (cffi:null-pointer)))
	    (blue  (or blue  (cffi:null-pointer)))
	    (alpha (or alpha (cffi:null-pointer))))
	(cffi:foreign-funcall "cairo_mesh_pattern_get_corner_color_rgba"
			      :pointer pattern
			      :uint patch-num
			      :uint corner-num
			      :pointer red :pointer green :pointer blue
			      :pointer alpha
			      :int))))


(defun cffi/cairo_pattern_reference (pattern)
"
 cairo_pattern_t * cairo_pattern_reference (cairo_pattern_t *pattern);

 Increases the reference count on pattern by one. This prevents pattern from being destroyed
 until a matching call to cairo_pattern_destroy() is made.

 The number of references to a cairo_pattern_t can be get using
 cairo_pattern_get_reference_count().

    pattern : a cairo_pattern_t

    Returns : the referenced cairo_pattern_t.

 Since 1.0
"
  (if (or (null pattern) (cffi:null-pointer-p pattern))
      (error "CFFI/CAIRO_PATTERN_REFERENCE invoked with null PATTERN")
      (let ((referenced (cffi:foreign-funcall "cairo_pattern_reference"
					      :pointer pattern
					      :pointer)))
	(cairo-pattern-status-report referenced)
	referenced)))



(defun cffi/cairo_pattern_destroy (pattern)
"
 void cairo_pattern_destroy (cairo_pattern_t *pattern);

 Decreases the reference count on pattern by one. If the result is zero, then pattern and
 all associated resources are freed. See cairo_pattern_reference().

    pattern : a cairo_pattern_t

 Since 1.0
"
  (if (or (null pattern) (cffi:null-pointer-p pattern))
      (error "CFFI/CAIRO_PATTERN_DESTROY invoked with null PATTERN")
      (cffi:foreign-funcall "cairo_pattern_destroy"
			    :pointer pattern
			    :void))
  nil)


(defun cffi/cairo_pattern_status (pattern)
"
 cairo_status_t cairo_pattern_status (cairo_pattern_t *pattern);

 Checks whether an error has previously occurred for this pattern.

    pattern : a cairo_pattern_t

    Returns : CAIRO_STATUS_SUCCESS, CAIRO_STATUS_NO_MEMORY, CAIRO_STATUS_INVALID_MATRIX,
              CAIRO_STATUS_PATTERN_TYPE_MISMATCH, or CAIRO_STATUS_INVALID_MESH_CONSTRUCTION.

 Since 1.0
"
  (if (or (null pattern) (cffi:null-pointer-p pattern))
      (error "CFFI/CAIRO_PATTERN_STATUS invoked with null PATTERN")
      (cffi:foreign-funcall "cairo_pattern_status"
			    :pointer pattern
			    :int)))


(cffi:defcenum cairo_extend_t
"
 cairo_extend_t is used to describe how pattern color/alpha will be determined for
 areas 'outside' the pattern's natural area, (for example, outside the surface bounds
 or outside the gradient geometry).

 Mesh patterns are not affected by the extend mode.

 The default extend mode is CAIRO_EXTEND_NONE for surface patterns and CAIRO_EXTEND_PAD
 for gradient patterns.

 New entries may be added in future versions.

 CAIRO_EXTEND_NONE
    pixels outside of the source pattern are fully transparent (Since 1.0)
 CAIRO_EXTEND_REPEAT
    the pattern is tiled by repeating (Since 1.0)
 CAIRO_EXTEND_REFLECT
    the pattern is tiled by reflecting at the edges (Since 1.0; but only implemented for
    surface patterns since 1.6)
 CAIRO_EXTEND_PAD
    pixels outside of the pattern copy the closest pixel from the source (Since 1.2; but
    only implemented for surface patterns since 1.6)

 Since 1.0
"
  :CAIRO_EXTEND_NONE
  :CAIRO_EXTEND_REPEAT
  :CAIRO_EXTEND_REFLECT
  :CAIRO_EXTEND_PAD)


(defconstant +CFFI/CAIRO_EXTEND_NONE+    (cffi:foreign-enum-value 'cairo_extend_t :CAIRO_EXTEND_NONE))
(defconstant +CFFI/CAIRO_EXTEND_REPEAT+  (cffi:foreign-enum-value 'cairo_extend_t :CAIRO_EXTEND_REPEAT))
(defconstant +CFFI/CAIRO_EXTEND_REFLECT+ (cffi:foreign-enum-value 'cairo_extend_t :CAIRO_EXTEND_REFLECT))
(defconstant +CFFI/CAIRO_EXTEND_PAD+     (cffi:foreign-enum-value 'cairo_extend_t :CAIRO_EXTEND_PAD))


(defun cffi/cairo_pattern_set_extend (pattern extend)
"
 void cairo_pattern_set_extend (cairo_pattern_t *pattern, cairo_extend_t extend);

 Sets the mode to be used for drawing outside the area of a pattern. See cairo_extend_t for
 details on the semantics of each extend strategy.

 The default extend mode is CAIRO_EXTEND_NONE for surface patterns and CAIRO_EXTEND_PAD for
 gradient patterns.

    pattern : a cairo_pattern_t
    extend : a cairo_extend_t describing how the area outside of the pattern will be drawn

 Since 1.0
"
  (if (or (null pattern) (cffi:null-pointer-p pattern))
      (error "CFFI/CAIRO_PATTERN_SET_EXTEND invoked with null PATTERN")
      (cffi:foreign-funcall "cairo_pattern_set_extend"
			    :pointer pattern
			    :int extend
			    :void))
  (cairo-pattern-status-report pattern)
  pattern)


(defun cffi/cairo_pattern_get_extend (pattern)
"
 cairo_extend_t cairo_pattern_get_extend (cairo_pattern_t *pattern);

 Gets the current extend mode for a pattern. See cairo_extend_t for details on the
 semantics of each extend strategy.

    pattern : a cairo_pattern_t

    Returns : the current extend strategy used for drawing the pattern.

 Since 1.0
"
  (if (or (null pattern) (cffi:null-pointer-p pattern))
      (error "CFFI/CAIRO_PATTERN_GET_EXTEND invoked with null PATTERN")
      (cffi:foreign-funcall "cairo_pattern_get_extend"
			    :pointer pattern
			    :int)))


(cffi:defcenum cairo_filter_t
"
 cairo_filter_t is used to indicate what filtering should be applied when reading pixel
 values from patterns. See cairo_pattern_set_filter() for indicating the desired filter
 to be used with a particular pattern.

 CAIRO_FILTER_FAST
    A high-performance filter, with quality similar to CAIRO_FILTER_NEAREST (Since 1.0)
 CAIRO_FILTER_GOOD
    A reasonable-performance filter, with quality similar to CAIRO_FILTER_BILINEAR (Since 1.0)
 CAIRO_FILTER_BEST
    The highest-quality available, performance may not be suitable for interactive use. (Since 1.0)
 CAIRO_FILTER_NEAREST
    Nearest-neighbor filtering (Since 1.0)
 CAIRO_FILTER_BILINEAR
    Linear interpolation in two dimensions (Since 1.0)
 CAIRO_FILTER_GAUSSIAN
    This filter value is currently unimplemented, and should not be used in current code. (Since 1.0)

 Since 1.0
"
  :CAIRO_FILTER_FAST
  :CAIRO_FILTER_GOOD
  :CAIRO_FILTER_BEST
  :CAIRO_FILTER_NEAREST
  :CAIRO_FILTER_BILINEAR
  :CAIRO_FILTER_GAUSSIAN)


(defconstant +CFFI/CAIRO_FILTER_FAST+     (cffi:foreign-enum-value 'cairo_filter_t :CAIRO_FILTER_FAST))
(defconstant +CFFI/CAIRO_FILTER_GOOD+     (cffi:foreign-enum-value 'cairo_filter_t :CAIRO_FILTER_GOOD))
(defconstant +CFFI/CAIRO_FILTER_BEST+     (cffi:foreign-enum-value 'cairo_filter_t :CAIRO_FILTER_BEST))
(defconstant +CFFI/CAIRO_FILTER_NEAREST+  (cffi:foreign-enum-value 'cairo_filter_t :CAIRO_FILTER_NEAREST))
(defconstant +CFFI/CAIRO_FILTER_BILINEAR+ (cffi:foreign-enum-value 'cairo_filter_t :CAIRO_FILTER_BILINEAR))
(defconstant +CFFI/CAIRO_FILTER_GAUSSIAN+ (cffi:foreign-enum-value 'cairo_filter_t :CAIRO_FILTER_GAUSSIAN))


(defun cffi/cairo_pattern_set_filter (pattern filter)
"
 void cairo_pattern_set_filter (cairo_pattern_t *pattern, cairo_filter_t filter);

 Sets the filter to be used for resizing when using this pattern. See cairo_filter_t for details
 on each filter.

 * Note that you might want to control filtering even when you do not have an explicit
   cairo_pattern_t object, (for example when using cairo_set_source_surface()). In these cases,
   it is convenient to use cairo_get_source() to get access to the pattern that cairo creates
   implicitly. For example:

    1 cairo_set_source_surface (cr, image, x, y);
    2 cairo_pattern_set_filter (cairo_get_source (cr), CAIRO_FILTER_NEAREST);

    pattern : a cairo_pattern_t
    filter : a cairo_filter_t describing the filter to use for resizing the pattern

Since 1.0
"
  (if (or (null pattern) (cffi:null-pointer-p pattern))
      (error "CFFI/CAIRO_PATTERN_SET_FILTER invoked with null PATTERN")
      (cffi:foreign-funcall "cairo_pattern_set_filter"
			    :pointer pattern
			    :int filter
			    :void))
  (cairo-pattern-status-report pattern)
  pattern)


(defun cffi/cairo_pattern_get_filter (pattern)
"
 cairo_filter_t cairo_pattern_get_filter (cairo_pattern_t *pattern);

 Gets the current filter for a pattern. See cairo_filter_t for details on each filter.

    pattern : a cairo_pattern_t

    Returns : the current filter used for resizing the pattern.

 Since 1.0
"
  (if (or (null pattern) (cffi:null-pointer-p pattern))
      (error "CFFI/CAIRO_PATTERN_GET_FILTER invoked with null PATTERN")
      (cffi:foreign-funcall "cairo_pattern_get_filter"
			    :pointer pattern
			    :int)))


(defun cffi/cairo_pattern_set_matrix (pattern matrix)
"
 void cairo_pattern_set_matrix (cairo_pattern_t *pattern, const cairo_matrix_t *matrix);

 Sets the pattern's transformation matrix to matrix. This matrix is a transformation from
 user space to pattern space.

 When a pattern is first created it always has the identity matrix for its transformation
 matrix, which means that pattern space is initially identical to user space.

 Important: Please note that the direction of this transformation matrix is from user space
 to pattern space. This means that if you imagine the flow from a pattern to user space
 (and on to device space), then coordinates in that flow will be transformed by the inverse
 of the pattern matrix.

 For example, if you want to make a pattern appear twice as large as it does by default the
 correct code to use is:

    1 cairo_matrix_init_scale (&matrix, 0.5, 0.5);
    2 cairo_pattern_set_matrix (pattern, &matrix);

 Meanwhile, using values of 2.0 rather than 0.5 in the code above would cause the pattern to
 appear at half of its default size.

 Also, please note the discussion of the user-space locking semantics of cairo_set_source().

    pattern : a cairo_pattern_t
    matrix : a cairo_matrix_t

 Since 1.0
"
  (if (or (null pattern) (cffi:null-pointer-p pattern))
      (error "CFFI/CAIRO_PATTERN_SET_MATRIX invoked with null PATTERN")
      (if (or (null matrix) (cffi:null-pointer-p matrix))
	  (error "CFFI/CAIRO_PATTERN_SET_MATRIX invoked with null MATRIX")
	  (cffi:foreign-funcall "cairo_pattern_set_matrix"
				:pointer pattern
				:pointer matrix
				:void)))
  (cairo-pattern-status-report pattern)
  pattern)


(defun cffi/cairo_pattern_get_matrix (pattern matrix)
"
 void cairo_pattern_get_matrix (cairo_pattern_t *pattern, cairo_matrix_t *matrix);

 Stores the pattern's transformation matrix into matrix.

    pattern : a cairo_pattern_t
    matrix : return value for the matrix

 Since 1.0
"
  (if (or (null pattern) (cffi:null-pointer-p pattern))
      (error "CFFI/CAIRO_PATTERN_GET_MATRIX invoked with null PATTERN")
      (if (or (null matrix) (cffi:null-pointer-p matrix))
	  (error "CFFI/CAIRO_PATTERN_GET_MATRIX invoked with null MATRIX")
	  (cffi:foreign-funcall "cairo_pattern_get_matrix"
				:pointer pattern
				:pointer matrix
				:void)))
  ;; It seems to me this will be more useful than "pattern"...
  (cairo-pattern-status-report pattern)
  matrix)


(cffi:defcenum cairo_pattern_type_t
"
 cairo_pattern_type_t is used to describe the type of a given pattern.

 The type of a pattern is determined by the function used to create it. The
 cairo_pattern_create_rgb() and cairo_pattern_create_rgba() functions create SOLID
 patterns. The remaining cairo_pattern_create functions map to pattern types in
 obvious ways.

 The pattern type can be queried with cairo_pattern_get_type()

 Most cairo_pattern_t functions can be called with a pattern of any type, (though
 trying to change the extend or filter for a solid pattern will have no effect). A
 notable exception is cairo_pattern_add_color_stop_rgb() and
 cairo_pattern_add_color_stop_rgba() which must only be called with gradient patterns
 (either LINEAR or RADIAL). Otherwise the pattern will be shutdown and put into an
 error state.

 New entries may be added in future versions.

 CAIRO_PATTERN_TYPE_SOLID
    The pattern is a solid (uniform) color. It may be opaque or translucent, since 1.2.
 CAIRO_PATTERN_TYPE_SURFACE
    The pattern is a based on a surface (an image), since 1.2.
 CAIRO_PATTERN_TYPE_LINEAR
    The pattern is a linear gradient, since 1.2.
 CAIRO_PATTERN_TYPE_RADIAL
    The pattern is a radial gradient, since 1.2.
 CAIRO_PATTERN_TYPE_MESH
    The pattern is a mesh, since 1.12.
 CAIRO_PATTERN_TYPE_RASTER_SOURCE
    The pattern is a user pattern providing raster data, since 1.12.

 Since 1.2
"
    :CAIRO_PATTERN_TYPE_SOLID
    :CAIRO_PATTERN_TYPE_SURFACE
    :CAIRO_PATTERN_TYPE_LINEAR
    :CAIRO_PATTERN_TYPE_RADIAL
    :CAIRO_PATTERN_TYPE_MESH
    :CAIRO_PATTERN_TYPE_RASTER_SOURCE)


(defconstant +CFFI/CAIRO_PATTERN_TYPE_SOLID+   (cffi:foreign-enum-value 'cairo_pattern_type_t :CAIRO_PATTERN_TYPE_SOLID))
(defconstant +CFFI/CAIRO_PATTERN_TYPE_SURFACE+ (cffi:foreign-enum-value 'cairo_pattern_type_t :CAIRO_PATTERN_TYPE_SURFACE))
(defconstant +CFFI/CAIRO_PATTERN_TYPE_LINEAR+  (cffi:foreign-enum-value 'cairo_pattern_type_t :CAIRO_PATTERN_TYPE_LINEAR))
(defconstant +CFFI/CAIRO_PATTERN_TYPE_RADIAL+  (cffi:foreign-enum-value 'cairo_pattern_type_t :CAIRO_PATTERN_TYPE_RADIAL))
(defconstant +CFFI/CAIRO_PATTERN_TYPE_MESH+    (cffi:foreign-enum-value 'cairo_pattern_type_t :CAIRO_PATTERN_TYPE_MESH))
(defconstant +CFFI/CAIRO_PATTERN_TYPE_RASTER_SOURCE+
  (cffi:foreign-enum-value 'cairo_pattern_type_t :CAIRO_PATTERN_TYPE_RASTER_SOURCE))


(defun cffi/cairo_pattern_get_type (pattern)
"
 cairo_pattern_type_t cairo_pattern_get_type (cairo_pattern_t *pattern);

 This function returns the type a pattern. See cairo_pattern_type_t for available types.

    pattern : a cairo_pattern_t

    Returns : The type of pattern.

 Since 1.2
"
  (if (or (null pattern) (cffi:null-pointer-p pattern))
      (error "CFFI/CAIRO_PATTERN_GET_TYPE invoked with null PATTERN")
      (cffi:foreign-funcall "cairo_pattern_get_type"
			    :pointer pattern
			    :int)))


(defun cffi/cairo_pattern_get_reference_count (pattern)
"
 unsigned int cairo_pattern_get_reference_count (cairo_pattern_t *pattern);

 Returns the current reference count of pattern.

    pattern : a cairo_pattern_t

    Returns : the current reference count of pattern. If the object is a nil
              object, 0 will be returned.

 Since 1.4
"
  (if (or (null pattern) (cffi:null-pointer-p pattern))
      (error "CFFI/CAIRO_PATTERN_GET_REFERENCE_COUNT invoked with null PATTERN")
      (cffi:foreign-funcall "cairo_pattern_get_reference_count"
			    :pointer pattern
			    :uint)))


#||
(defun cffi/cairo_pattern_set_user_data ()
"
cairo_status_t      cairo_pattern_set_user_data         (cairo_pattern_t *pattern,
                                                         const cairo_user_data_key_t *key,
                                                         void *user_data,
                                                         cairo_destroy_func_t destroy);
Attach user data to pattern. To remove user data from a surface, call this function with the key that was used to set it and NULL for data.

pattern :

a cairo_pattern_t
key :

the address of a cairo_user_data_key_t to attach the user data to
user_data :

the user data to attach to the cairo_pattern_t
destroy :

a cairo_destroy_func_t which will be called when the cairo_t is destroyed or when new user data is attached using the same key.
Returns :

CAIRO_STATUS_SUCCESS or CAIRO_STATUS_NO_MEMORY if a slot could not be allocated for the user data.
Since 1.4
"
(defun cffi/cairo_pattern_get_user_data ()
"
void *              cairo_pattern_get_user_data         (cairo_pattern_t *pattern,
                                                         const cairo_user_data_key_t *key);
Return user data previously attached to pattern using the specified key. If no user data has been attached with the given key this function returns NULL.

pattern :

a cairo_pattern_t
key :

the address of the cairo_user_data_key_t the user data was attached to
Returns :

the user data previously attached or NULL.
Since 1.4
"
||#

#||
See Also

cairo_t, cairo_surface_t
||#

;;;;;;;;;; REGIONS?
;;;;;;;;;; TRANSFORMATIONS?

;;;;;;;;;; CAIRO_SURFACE_T

#||
Description

 cairo_surface_t is the abstract type representing all different drawing targets that
 cairo can render to. The actual drawings are performed using a cairo context.

 A cairo surface is created by using backend-specific constructors, typically of the
 form cairo_backend_surface_create().

 Most surface types allow accessing the surface without using Cairo functions. If you
 do this, keep in mind that it is mandatory that you call cairo_surface_flush() before
 reading from or writing to the surface and that you must use cairo_surface_mark_dirty()
 after modifying it.

 Example 1. Directly modifying an image surface

    1  void
    2  modify_image_surface (cairo_surface_t *surface)
    3  {
    4    unsigned char *data;
    5    int width, height, stride;
    6
    7    // flush to ensure all writing to the image was done
    8    cairo_surface_flush (surface);
    9
    10   // modify the image
    11   data = cairo_image_surface_get_data (surface);
    12   width = cairo_image_surface_get_width (surface);
    13   height = cairo_image_surface_get_height (surface);
    14   stride = cairo_image_surface_get_stride (surface);
    15   modify_image_data (data, width, height, stride);
    16
    17   // mark the image dirty so Cairo clears its caches.
    18   cairo_surface_mark_dirty (surface);
    19 }

 Note that for other surface types it might be necessary to acquire the surface's device first.
 See cairo_device_acquire() for a discussion of devices.

 Details

 CAIRO_HAS_MIME_SURFACE
 #define CAIRO_HAS_MIME_SURFACE 1

 CAIRO_MIME_TYPE_JP2
 #define CAIRO_MIME_TYPE_JP2 "image/jp2"
 The Joint Photographic Experts Group (JPEG) 2000 image coding standard (ISO/IEC 15444-1).
 Since 1.10

 CAIRO_MIME_TYPE_JPEG
 #define CAIRO_MIME_TYPE_JPEG "image/jpeg"
 The Joint Photographic Experts Group (JPEG) image coding standard (ISO/IEC 10918-1).
 Since 1.10

 CAIRO_MIME_TYPE_PNG
 #define CAIRO_MIME_TYPE_PNG "image/png"
 The Portable Network Graphics image file format (ISO/IEC 15948).
 Since 1.10

 CAIRO_MIME_TYPE_URI
 #define CAIRO_MIME_TYPE_URI "text/x-uri"
 URI for an image file (unofficial MIME type).
 Since 1.10

 CAIRO_MIME_TYPE_UNIQUE_ID
 #define CAIRO_MIME_TYPE_UNIQUE_ID "application/x-cairo.uuid"
 Unique identifier for a surface (cairo specific MIME type).
 Since 1.12

 cairo_surface_t
 typedef struct _cairo_surface cairo_surface_t;
 A cairo_surface_t represents an image, either as the destination of a drawing operation or
 as source when drawing onto another surface. To draw to a cairo_surface_t, create a cairo
 context with the surface as the target, using cairo_create().

 There are different subtypes of cairo_surface_t for different drawing backends; for example,
 cairo_image_surface_create() creates a bitmap image in memory. The type of a surface can be
 queried with cairo_surface_get_type().

 The initial contents of a surface after creation depend upon the manner of its creation. If
 cairo creates the surface and backing storage for the user, it will be initially cleared;
 for example, cairo_image_surface_create() and cairo_surface_create_similar(). Alternatively,
 if the user passes in a reference to some backing storage and asks cairo to wrap that in a
 cairo_surface_t, then the contents are not modified; for example,
 cairo_image_surface_create_for_data() and cairo_xlib_surface_create().

 Memory management of cairo_surface_t is done with cairo_surface_reference() and
 cairo_surface_destroy().

 Since 1.0
||#


(defun cffi/cairo_surface_create_similar (other content width height)
"
 cairo_surface_t * cairo_surface_create_similar (cairo_surface_t *other,
                                                 cairo_content_t content,
                                                 int width, int height);

 Create a new surface that is as compatible as possible with an existing surface. For example
 the new surface will have the same fallback resolution and font options as other. Generally,
 the new surface will also use the same backend as other, unless that is not possible for some
 reason. The type of the returned surface may be examined with cairo_surface_get_type().

 Initially the surface contents are all 0 (transparent if contents have transparency, black
 otherwise.)

 Use cairo_surface_create_similar_image() if you need an image surface which can be painted
 quickly to the target surface.

    other : an existing surface used to select the backend of the new surface
    content : the content for the new surface
    width : width of the new surface, (in device-space units)
    height : height of the new surface (in device-space units)

    Returns : a pointer to the newly allocated surface. The caller owns the surface and should
              call cairo_surface_destroy() when done with it. This function always returns a
    valid pointer, but it will return a pointer to a 'nil' surface if other is already in an
    error state or any other error occurs.

 Since 1.0
"
  (if (or (null other) (cffi:null-pointer-p other))
      (error "CFFI/CAIRO_SURFACE_CREATE_SIMILAR invoked with null OTHER")
      (let ((surface (cffi:foreign-funcall "cairo_surface_create_similar"
					   :pointer other
					   :int content
					   :int width :int height
					   :pointer)))
	(cairo-surface-status-report surface)
	surface)))



(defun cffi/cairo_surface_create_similar_image (other format width height)
"
 cairo_surface_t * cairo_surface_create_similar_image (cairo_surface_t *other,
                                                       cairo_format_t format,
                                                       int width, int height);

 Create a new image surface that is as compatible as possible for uploading to and the use in
 conjunction with an existing surface. However, this surface can still be used like any normal
 image surface.

 Initially the surface contents are all 0 (transparent if contents have transparency, black
 otherwise.)

 Use cairo_surface_create_similar() if you don't need an image surface.

    other : an existing surface used to select the preference of the new surface
    format : the format for the new surface
    width : width of the new surface, (in device-space units)
    height : height of the new surface (in device-space units)

    Returns : a pointer to the newly allocated image surface. The caller owns the surface and
              should call cairo_surface_destroy() when done with it. This function always returns
    a valid pointer, but it will return a pointer to a 'nil' surface if other is already in an
    error state or any other error occurs.

 Since 1.12
"
  (if (or (null other) (cffi:null-pointer-p other))
      (error "CFFI/CAIRO_SURFACE_CREATE_SIMILAR_IMAGE invoked with null OTHER")
      (let ((surface (cffi:foreign-funcall "cairo_surface_create_similar_image"
					   :pointer other
					   :int format
					   :int width :int height
					   :pointer)))
	(cairo-surface-status-report surface)
	surface)))



(defun cffi/cairo_surface_create_for_rectangle (target x y width height)
"
 cairo_surface_t * cairo_surface_create_for_rectangle (cairo_surface_t *target,
                                                       double x, double y,
                                                       double width, double height);

 Create a new surface that is a rectangle within the target surface. All operations drawn to
 this surface are then clipped and translated onto the target surface. Nothing drawn via this
 sub-surface outside of its bounds is drawn onto the target surface, making this a useful
 method for passing constrained child surfaces to library routines that draw directly onto
 the parent surface, i.e. with no further backend allocations, double buffering or copies.

 Note

 The semantics of subsurfaces have not been finalized yet unless the rectangle is in full
 device units, is contained within the extents of the target surface, and the target or
 subsurface's device transforms are not changed.

    target : an existing surface for which the sub-surface will point to
    x : the x-origin of the sub-surface from the top-left of the target surface (in
        device-space units)
    y : the y-origin of the sub-surface from the top-left of the target surface (in
        device-space units)
    width : width of the sub-surface (in device-space units)
    height : height of the sub-surface (in device-space units)

    Returns : a pointer to the newly allocated surface. The caller owns the surface and should
              call cairo_surface_destroy() when done with it. This function always returns a
    valid pointer, but it will return a pointer to a 'nil' surface if other is already in an
    error state or any other error occurs.

 Since 1.10
"
  (if (or (null target) (cffi:null-pointer-p target))
      (error "CFFI/CAIRO_SURFACE_CREATE_FOR_RECTANGLE invoked with null TARGET")
      (let ((surface (cffi:foreign-funcall "cairo_surface_create_for_rectangle"
					   :pointer target
					   :double x :double y
					   :double width :double height
					   :pointer)))
	(cairo-surface-status-report surface))))



(defun cffi/cairo_surface_reference (surface)
"
 cairo_surface_t * cairo_surface_reference (cairo_surface_t *surface);

 Increases the reference count on surface by one. This prevents surface from being destroyed
 until a matching call to cairo_surface_destroy() is made.

 The number of references to a cairo_surface_t can be get using
 cairo_surface_get_reference_count().

    surface : a cairo_surface_t

    Returns : the referenced cairo_surface_t.

 Since 1.0
"
  (if (or (null surface) (cffi:null-pointer-p surface))
      (error "CFFI/CAIRO_SURFACE_REFERENCE invoked with null SURFACE")
      (let ((reference (cffi:foreign-funcall "cairo_surface_reference"
					     :pointer surface
					     :pointer)))
	(cairo-surface-status-report reference)
	reference)))



(defun cffi/cairo_surface_destroy (surface)
"
 void cairo_surface_destroy (cairo_surface_t *surface);

 Decreases the reference count on surface by one. If the result is zero, then surface and all
 associated resources are freed. See cairo_surface_reference().

    surface : a cairo_surface_t

 Since 1.0
"
  (if (or (null surface) (cffi:null-pointer-p surface))
      (error "CFFI/CAIRO_SURFACE_DESTROY invoked with null SURFACE")
      (cffi:foreign-funcall "cairo_surface_destroy"
			    :pointer surface
			    :void))
  nil)


(defun cffi/cairo_surface_status (surface)
"
 cairo_status_t cairo_surface_status (cairo_surface_t *surface);

 Checks whether an error has previously occurred for this surface.

    surface : a cairo_surface_t

    Returns : CAIRO_STATUS_SUCCESS, CAIRO_STATUS_NULL_POINTER, CAIRO_STATUS_NO_MEMORY,
              CAIRO_STATUS_READ_ERROR, CAIRO_STATUS_INVALID_CONTENT, CAIRO_STATUS_INVALID_FORMAT,
    or CAIRO_STATUS_INVALID_VISUAL.

 Since 1.0
"
  (if (or (null surface) (cffi:null-pointer-p surface))
      (error "CFFI/CAIRO_SURFACE_STATUS invoked with null SURFACE")
      (cffi:foreign-funcall "cairo_surface_status"
			    :pointer surface
			    :int)))


(defun cffi/cairo_surface_finish (surface)
"
 void cairo_surface_finish (cairo_surface_t *surface);

 This function finishes the surface and drops all references to external resources. For example,
 for the Xlib backend it means that cairo will no longer access the drawable, which can be
 freed. After calling cairo_surface_finish() the only valid operations on a surface are getting
 and setting user, referencing and destroying, and flushing and finishing it. Further drawing to
 the surface will not affect the surface but will instead trigger a CAIRO_STATUS_SURFACE_FINISHED
 error.

 When the last call to cairo_surface_destroy() decreases the reference count to zero, cairo will
 call cairo_surface_finish() if it hasn't been called already, before freeing the resources
 associated with the surface.

    surface : the cairo_surface_t to finish

 Since 1.0
"
  (if (or (null surface) (cffi:null-pointer-p surface))
      (error "CFFI/CAIRO_SURFACE_FINISH invoked with null SURFACE")
      (cffi:foreign-funcall "cairo_surface_finish"
			    :pointer surface
			    :void))
  surface)


(defun cffi/cairo_surface_flush (surface)
"
 void cairo_surface_flush (cairo_surface_t *surface);

 Do any pending drawing for the surface and also restore any temporary modifications cairo has
 made to the surface's state. This function must be called before switching from drawing on
 the surface with cairo to drawing on it directly with native APIs. If the surface doesn't
 support direct access, then this function does nothing.

    surface : a cairo_surface_t

 Since 1.0
"
  (if (or (null surface) (cffi:null-pointer-p surface))
      (error "CFFI/CAIRO_SURFACE_FLUSH invoked with null SURFACE")
      (cffi:foreign-funcall "cairo_surface_flush"
			    :pointer surface
			    :void))
  (cairo-surface-status-report surface)
  surface)


(defun cffi/cairo_surface_get_device (surface)
"
 cairo_device_t * cairo_surface_get_device (cairo_surface_t *surface);

 This function returns the device for a surface. See cairo_device_t.

    surface : a cairo_surface_t

    Returns : The device for surface or NULL if the surface does not have an
              associated device.

 Since 1.10
"
  (if (or (null surface) (cffi:null-pointer-p surface))
      (error "CFFI/CAIRO_SURFACE_GET_DEVICE invoked with null SURFACE")
      (let ((device (cffi:foreign-funcall "cairo_surface_get_device"
					  :pointer surface
					  :pointer)))
	(pointer-or-nil device))))


 (defun cffi/cairo_surface_get_font_options (surface options)
"
 void cairo_surface_get_font_options (cairo_surface_t *surface,
                                      cairo_font_options_t *options);

 Retrieves the default font rendering options for the surface. This allows display
 surfaces to report the correct subpixel order for rendering on them, print surfaces
 to disable hinting of metrics and so forth. The result can then be used with
 cairo_scaled_font_create().

    surface : a cairo_surface_t
    options : a cairo_font_options_t object into which to store the retrieved
              options. All existing values are overwritten

 Since 1.0
"
  (if (or (null surface) (cffi:null-pointer-p surface))
      (error "CFFI/CAIRO_SURFACE_GET_FONT_OPTIONS invoked with null SURFACE")
      (if (or (null options) (cffi:null-pointer-p options))
	  (error "CFFI/CAIRO_SURFACE_GET_FONT_OPTIONS invoked with null OPTIONS")
	  (cffi:foreign-funcall "cairo_surface_get_font_options"
				:pointer surface
				:pointer options
				:void)))
  ;; This seems more useful than "surface"...
  (cairo-surface-status-report surface)
  options)


(defun cffi/cairo_surface_get_content (surface)
"
 cairo_content_t cairo_surface_get_content (cairo_surface_t *surface);

 This function returns the content type of surface which indicates whether the surface
 contains color and/or alpha information. See cairo_content_t.

    surface : a cairo_surface_t

    Returns : The content type of surface.

 Since 1.2
"
  (if (or (null surface) (cffi:null-pointer-p surface))
      (error "CFFI/CAIRO_SURFACE_GET_CONTENT invoked with null SURFACE")
      (cffi:foreign-funcall "cairo_surface_get_content"
			    :pointer surface
			    :int)))


(defun cffi/cairo_surface_mark_dirty (surface)
"
 void cairo_surface_mark_dirty (cairo_surface_t *surface);

 Tells cairo that drawing has been done to surface using means other than cairo, and
 that cairo should reread any cached areas. Note that you must call
 cairo_surface_flush() before doing such drawing.

    surface : a cairo_surface_t

 Since 1.0
"
  (if (or (null surface) (cffi:null-pointer-p surface))
      (error "CFFI/CAIRO_SURFACE_MARK_DIRTY invoked with null SURFACE")
      (cffi:foreign-funcall "cairo_surface_mark_dirty"
			    :pointer surface
			    :void))
  (cairo-surface-status-report surface)
  surface)


(defun cffi/cairo_surface_mark_dirty_rectangle (surface x y width height)
"
 void cairo_surface_mark_dirty_rectangle (cairo_surface_t *surface,
                                          int x, int y, int width, int height);

 Like cairo_surface_mark_dirty(), but drawing has been done only to the specified rectangle,
 so that cairo can retain cached contents for other parts of the surface.

 Any cached clip set on the surface will be reset by this function, to make sure that future
 cairo calls have the clip set that they expect.

    surface : a cairo_surface_t
    x : X coordinate of dirty rectangle
    y : Y coordinate of dirty rectangle
    width : width of dirty rectangle
    height : height of dirty rectangle

 Since 1.0
"
  (if (or (null surface) (cffi:null-pointer-p surface))
      (error "CFFI/CAIRO_SURFACE_MARK_DIRTY_RECTANGLE invoked with null SURFACE")
      (cffi:foreign-funcall "cairo_surface_mark_dirty_rectangle"
			    :pointer surface
			    :int x :int y :int width :int height
			    :void))
  (cairo-surface-status-report surface)
  surface)


(defun cffi/cairo_surface_set_device_offset (surface x-offset y-offset)
"
 void cairo_surface_set_device_offset (cairo_surface_t *surface,
                                       double x_offset, double y_offset);

 Sets an offset that is added to the device coordinates determined by the CTM when drawing
 to surface. One use case for this function is when we want to create a cairo_surface_t
 that redirects drawing for a portion of an onscreen surface to an offscreen surface in a
 way that is completely invisible to the user of the cairo API. Setting a transformation
 via cairo_translate() isn't sufficient to do this, since functions like
 cairo_device_to_user() will expose the hidden offset.

 Note that the offset affects drawing to the surface as well as using the surface in a
 source pattern.

    surface : a cairo_surface_t
    x_offset : the offset in the X direction, in device units
    y_offset : the offset in the Y direction, in device units

 Since 1.0
"
  (if (or (null surface) (cffi:null-pointer-p surface))
      (error "CFFI/CAIRO_SURFACE_SET_DEVICE_OFFSET invoked with null SURFACE")
      (cffi:foreign-funcall "cairo_surface_set_device_offset"
			    :pointer surface
			    :double x-offset :double y-offset
			    :void))
  (cairo-surface-status-report surface)
  surface)


(defun cffi/cairo_surface_get_device_offset (surface x-offset y-offset)
"
 void cairo_surface_get_device_offset (cairo_surface_t *surface,
                                       double *x_offset, double *y_offset);

 This function returns the previous device offset set by cairo_surface_set_device_offset().

    surface : a cairo_surface_t
    x_offset : the offset in the X direction, in device units
    y_offset : the offset in the Y direction, in device units

 Since 1.2
"
  (if (or (null surface) (cffi:null-pointer-p surface))
      (error "CFFI/CAIRO_SURFACE_GET_DEVICE_OFFSET invoked with null SURFACE")
      (if (or (null x-offset) (cffi:null-pointer-p x-offset))
	  (error "CFFI/CAIRO_SURFACE_GET_DEVICE_OFFSET invoked with null X-OFFSET")
	  (if (or (null y-offset) (cffi:null-pointer-p y-offset))
	      (error "CFFI/CAIRO_SURFACE_GET_DEVICE_OFFSET invoked with null Y-OFFSET")
	      (cffi:foreign-funcall "cairo_surface_get_device_offset"
				    :pointer surface
				    :pointer x-offset
				    :pointer y-offset
				    :void))))
  (cairo-surface-status-report surface)
  surface)


(defun cffi/cairo_surface_set_fallback_resolution (surface x-pixels-per-inch y-pixels-per-inch)
"
 void cairo_surface_set_fallback_resolution (cairo_surface_t *surface,
                                             double x_pixels_per_inch,
                                             double y_pixels_per_inch);

 Set the horizontal and vertical resolution for image fallbacks.

 When certain operations aren't supported natively by a backend, cairo will fallback
 by rendering operations to an image and then overlaying that image onto the output.
 For backends that are natively vector-oriented, this function can be used to set the
 resolution used for these image fallbacks, (larger values will result in more detailed
 images, but also larger file sizes).

 Some examples of natively vector-oriented backends are the ps, pdf, and svg backends.

 For backends that are natively raster-oriented, image fallbacks are still possible,
 but they are always performed at the native device resolution. So this function has
 no effect on those backends.

 Note: The fallback resolution only takes effect at the time of completing a page
 (with cairo_show_page() or cairo_copy_page()) so there is currently no way to have
 more than one fallback resolution in effect on a single page.

 The default fallback resoultion is 300 pixels per inch in both dimensions.

    surface : a cairo_surface_t
    x_pixels_per_inch : horizontal setting for pixels per inch
    y_pixels_per_inch : vertical setting for pixels per inch

 Since 1.2
"
  (if (or (null surface) (cffi:null-pointer-p surface))
      (error "CFFI/CAIRO_SURFACE_SET_FALLBACK_RESOLUTION invoked with null SURFACE")
      (cffi:foreign-funcall "cairo_surface_set_fallback_resolution"
			    :pointer surface
			    :double x-pixels-per-inch
			    :double y-pixels-per-inch
			    :void))
  (cairo-surface-status-report surface)
  surface)


(defun cffi/cairo_surface_get_fallback_resolution (surface x-pixels-per-inch y-pixels-per-inch)
"
 void cairo_surface_get_fallback_resolution (cairo_surface_t *surface,
                                             double *x_pixels_per_inch,
                                             double *y_pixels_per_inch);

 This function returns the previous fallback resolution set by
 cairo_surface_set_fallback_resolution(), or default fallback resolution if
 never set.

    surface : a cairo_surface_t
    x_pixels_per_inch : horizontal pixels per inch
    y_pixels_per_inch : vertical pixels per inch

 Since 1.8
"
  (if (or (null surface) (cffi:null-pointer-p surface))
      (error "CFFI/CAIRO_SURFACE_GET_FALLBACK_RESOLUTION invoked with null SURFACE")
      ;; FIXME: SHOULD MAKE CFFI OBJECTS FOR THESE, THEN RETURN THEM AS MULTIPLE VALUES...
      (if (or (null x-pixels-per-inch) (cffi:null-pointer-p x-pixels-per-inch))
	  (error "CFFI/CAIRO_SURFACE_GET_FALLBACK_RESOLUTION invoked with null X-PIXELS-PER-INCH")
	  (if (or (null y-pixels-per-inch) (cffi:null-pointer-p y-pixels-per-inch))
	      (error "CFFI/CAIRO_SURFACE_GET_FALLBACK_RESOLUTION invoked with null Y-PIXELS-PER-INCH")
	      (cffi:foreign-funcall "cairo_surface_get_fallback_resolution"
				    :pointer surface
				    :pointer x-pixels-per-inch
				    :pointer y-pixels-per-inch
				    :void))))
  (cairo-surface-status-report surface)
  surface)


(cffi:defcenum cairo_surface_type_t
"
 cairo_surface_type_t is used to describe the type of a given surface. The surface types are
 also known as 'backends' or 'surface backends' within cairo.

 The type of a surface is determined by the function used to create it, which will generally
 be of the form cairo_type_surface_create(), (though see cairo_surface_create_similar() as
 well).

 The surface type can be queried with cairo_surface_get_type()

 The various cairo_surface_t functions can be used with surfaces of any type, but some backends
 also provide type-specific functions that must only be called with a surface of the appropriate
 type. These functions have names that begin with cairo_type_surface such as
 cairo_image_surface_get_width().

 The behavior of calling a type-specific function with a surface of the wrong type is undefined.

 New entries may be added in future versions.

 CAIRO_SURFACE_TYPE_IMAGE
    The surface is of type image, since 1.2
 CAIRO_SURFACE_TYPE_PDF
    The surface is of type pdf, since 1.2
 CAIRO_SURFACE_TYPE_PS
    The surface is of type ps, since 1.2
 CAIRO_SURFACE_TYPE_XLIB
    The surface is of type xlib, since 1.2
 CAIRO_SURFACE_TYPE_XCB
    The surface is of type xcb, since 1.2
 CAIRO_SURFACE_TYPE_GLITZ
    The surface is of type glitz, since 1.2
 CAIRO_SURFACE_TYPE_QUARTZ
    The surface is of type quartz, since 1.2
 CAIRO_SURFACE_TYPE_WIN32
    The surface is of type win32, since 1.2
 CAIRO_SURFACE_TYPE_BEOS
    The surface is of type beos, since 1.2
 CAIRO_SURFACE_TYPE_DIRECTFB
    The surface is of type directfb, since 1.2
 CAIRO_SURFACE_TYPE_SVG
    The surface is of type svg, since 1.2
 CAIRO_SURFACE_TYPE_OS2
    The surface is of type os2, since 1.4
 CAIRO_SURFACE_TYPE_WIN32_PRINTING
    The surface is a win32 printing surface, since 1.6
 CAIRO_SURFACE_TYPE_QUARTZ_IMAGE
    The surface is of type quartz_image, since 1.6
 CAIRO_SURFACE_TYPE_SCRIPT
    The surface is of type script, since 1.10
 CAIRO_SURFACE_TYPE_QT
    The surface is of type Qt, since 1.10
 CAIRO_SURFACE_TYPE_RECORDING
    The surface is of type recording, since 1.10
 CAIRO_SURFACE_TYPE_VG
    The surface is a OpenVG surface, since 1.10
 CAIRO_SURFACE_TYPE_GL
    The surface is of type OpenGL, since 1.10
 CAIRO_SURFACE_TYPE_DRM
    The surface is of type Direct Render Manager, since 1.10
 CAIRO_SURFACE_TYPE_TEE
    The surface is of type 'tee' (a multiplexing surface), since 1.10
 CAIRO_SURFACE_TYPE_XML
    The surface is of type XML (for debugging), since 1.10
 CAIRO_SURFACE_TYPE_SKIA
    The surface is of type Skia, since 1.10
 CAIRO_SURFACE_TYPE_SUBSURFACE
    The surface is a subsurface created with cairo_surface_create_for_rectangle(), since
    1.10
 CAIRO_SURFACE_TYPE_COGL
    This surface is of type Cogl, since 1.12

 Since 1.2
"
    :CAIRO_SURFACE_TYPE_IMAGE
    :CAIRO_SURFACE_TYPE_PDF
    :CAIRO_SURFACE_TYPE_PS
    :CAIRO_SURFACE_TYPE_XLIB
    :CAIRO_SURFACE_TYPE_XCB
    :CAIRO_SURFACE_TYPE_GLITZ
    :CAIRO_SURFACE_TYPE_QUARTZ
    :CAIRO_SURFACE_TYPE_WIN32
    :CAIRO_SURFACE_TYPE_BEOS
    :CAIRO_SURFACE_TYPE_DIRECTFB
    :CAIRO_SURFACE_TYPE_SVG
    :CAIRO_SURFACE_TYPE_OS2
    :CAIRO_SURFACE_TYPE_WIN32_PRINTING
    :CAIRO_SURFACE_TYPE_QUARTZ_IMAGE
    :CAIRO_SURFACE_TYPE_SCRIPT
    :CAIRO_SURFACE_TYPE_QT
    :CAIRO_SURFACE_TYPE_RECORDING
    :CAIRO_SURFACE_TYPE_VG
    :CAIRO_SURFACE_TYPE_GL
    :CAIRO_SURFACE_TYPE_DRM
    :CAIRO_SURFACE_TYPE_TEE
    :CAIRO_SURFACE_TYPE_XML
    :CAIRO_SURFACE_TYPE_SKIA
    :CAIRO_SURFACE_TYPE_SUBSURFACE
    :CAIRO_SURFACE_TYPE_COGL)


(defconstant +CFFI/CAIRO_SURFACE_TYPE_IMAGE+ (cffi:foreign-enum-value 'cairo_surface_type_t :CAIRO_SURFACE_TYPE_IMAGE))
(defconstant +CFFI/CAIRO_SURFACE_TYPE_PDF+ (cffi:foreign-enum-value 'cairo_surface_type_t :CAIRO_SURFACE_TYPE_PDF))
(defconstant +CFFI/CAIRO_SURFACE_TYPE_PS+ (cffi:foreign-enum-value 'cairo_surface_type_t :CAIRO_SURFACE_TYPE_PS))
(defconstant +CFFI/CAIRO_SURFACE_TYPE_XLIB+ (cffi:foreign-enum-value 'cairo_surface_type_t :CAIRO_SURFACE_TYPE_XLIB))
(defconstant +CFFI/CAIRO_SURFACE_TYPE_XCB+ (cffi:foreign-enum-value 'cairo_surface_type_t :CAIRO_SURFACE_TYPE_XCB))
(defconstant +CFFI/CAIRO_SURFACE_TYPE_GLITZ+ (cffi:foreign-enum-value 'cairo_surface_type_t :CAIRO_SURFACE_TYPE_GLITZ))
(defconstant +CFFI/CAIRO_SURFACE_TYPE_QUARTZ+ (cffi:foreign-enum-value 'cairo_surface_type_t :CAIRO_SURFACE_TYPE_QUARTZ))
(defconstant +CFFI/CAIRO_SURFACE_TYPE_WIN32+ (cffi:foreign-enum-value 'cairo_surface_type_t :CAIRO_SURFACE_TYPE_WIN32))
(defconstant +CFFI/CAIRO_SURFACE_TYPE_BEOS+ (cffi:foreign-enum-value 'cairo_surface_type_t :CAIRO_SURFACE_TYPE_BEOS))
(defconstant +CFFI/CAIRO_SURFACE_TYPE_DIRECTFB+ (cffi:foreign-enum-value 'cairo_surface_type_t :CAIRO_SURFACE_TYPE_DIRECTFB))
(defconstant +CFFI/CAIRO_SURFACE_TYPE_SVG+ (cffi:foreign-enum-value 'cairo_surface_type_t :CAIRO_SURFACE_TYPE_SVG))
(defconstant +CFFI/CAIRO_SURFACE_TYPE_OS2+ (cffi:foreign-enum-value 'cairo_surface_type_t :CAIRO_SURFACE_TYPE_OS2))
(defconstant +CFFI/CAIRO_SURFACE_TYPE_WIN32_PRINTING+
  (cffi:foreign-enum-value 'cairo_surface_type_t :CAIRO_SURFACE_TYPE_WIN32_PRINTING))
(defconstant +CFFI/CAIRO_SURFACE_TYPE_QUARTZ_IMAGE+
  (cffi:foreign-enum-value 'cairo_surface_type_t :CAIRO_SURFACE_TYPE_QUARTZ_IMAGE))
(defconstant +CFFI/CAIRO_SURFACE_TYPE_SCRIPT+
  (cffi:foreign-enum-value 'cairo_surface_type_t :CAIRO_SURFACE_TYPE_SCRIPT))
(defconstant +CFFI/CAIRO_SURFACE_TYPE_QT+ (cffi:foreign-enum-value 'cairo_surface_type_t :CAIRO_SURFACE_TYPE_QT))
(defconstant +CFFI/CAIRO_SURFACE_TYPE_RECORDING+
  (cffi:foreign-enum-value 'cairo_surface_type_t :CAIRO_SURFACE_TYPE_RECORDING))
(defconstant +CFFI/CAIRO_SURFACE_TYPE_VG+ (cffi:foreign-enum-value 'cairo_surface_type_t :CAIRO_SURFACE_TYPE_VG))
(defconstant +CFFI/CAIRO_SURFACE_TYPE_GL+ (cffi:foreign-enum-value 'cairo_surface_type_t :CAIRO_SURFACE_TYPE_GL))
(defconstant +CFFI/CAIRO_SURFACE_TYPE_DRM+ (cffi:foreign-enum-value 'cairo_surface_type_t :CAIRO_SURFACE_TYPE_DRM))
(defconstant +CFFI/CAIRO_SURFACE_TYPE_TEE+ (cffi:foreign-enum-value 'cairo_surface_type_t :CAIRO_SURFACE_TYPE_TEE))
(defconstant +CFFI/CAIRO_SURFACE_TYPE_XML+ (cffi:foreign-enum-value 'cairo_surface_type_t :CAIRO_SURFACE_TYPE_XML))
(defconstant +CFFI/CAIRO_SURFACE_TYPE_SKIA+ (cffi:foreign-enum-value 'cairo_surface_type_t :CAIRO_SURFACE_TYPE_SKIA))
(defconstant +CFFI/CAIRO_SURFACE_TYPE_SUBSURFACE+
  (cffi:foreign-enum-value 'cairo_surface_type_t :CAIRO_SURFACE_TYPE_SUBSURFACE))
(defconstant +CFFI/CAIRO_SURFACE_TYPE_COGL+ (cffi:foreign-enum-value 'cairo_surface_type_t :CAIRO_SURFACE_TYPE_COGL))


(defun cffi/cairo_surface_get_type (surface)
"
 cairo_surface_type_t cairo_surface_get_type (cairo_surface_t *surface);

 This function returns the type of the backend used to create a surface. See
 cairo_surface_type_t for available types.

    surface : a cairo_surface_t

    Returns : The type of surface.

 Since 1.2
"
  (if (or (null surface) (cffi:null-pointer-p surface))
      (error "CFFI/CAIRO_SURFACE_GET_TYPE invoked with null SURFACE")
      (cffi:foreign-funcall "cairo_surface_get_type"
			    :pointer surface
			    :int)))


(defun cffi/cairo_surface_get_reference_count (surface)
"
 unsigned int cairo_surface_get_reference_count (cairo_surface_t *surface);

 Returns the current reference count of surface.

    surface : a cairo_surface_t

    Returns : the current reference count of surface. If the object is a nil object,
    0 will be returned.

 Since 1.4
"
  (if (or (null surface) (cffi:null-pointer-p surface))
      (error "CFFI/CAIRO_SURFACE_GET_REFERENCE_COUNT invoked with null SURFACE")
      (cffi:foreign-funcall "cairo_surface_get_reference_count"
			    :pointer surface
			    :uint)))

#||
(defun cffi/cairo_surface_set_user_data ()
"
cairo_status_t      cairo_surface_set_user_data         (cairo_surface_t *surface,
                                                         const cairo_user_data_key_t *key,
                                                         void *user_data,
                                                         cairo_destroy_func_t destroy);
Attach user data to surface. To remove user data from a surface, call this function with the key that was used to set it and NULL for data.

surface :

a cairo_surface_t
key :

the address of a cairo_user_data_key_t to attach the user data to
user_data :

the user data to attach to the surface
destroy :

a cairo_destroy_func_t which will be called when the surface is destroyed or when new user data is attached using the same key.
Returns :

CAIRO_STATUS_SUCCESS or CAIRO_STATUS_NO_MEMORY if a slot could not be allocated for the user data.
Since 1.0
"
(defun cffi/cairo_surface_get_user_data ()
"
void *              cairo_surface_get_user_data         (cairo_surface_t *surface,
                                                         const cairo_user_data_key_t *key);
Return user data previously attached to surface using the specified key. If no user data has been attached with the given key this function returns NULL.

surface :

a cairo_surface_t
key :

the address of the cairo_user_data_key_t the user data was attached to
Returns :

the user data previously attached or NULL.
Since 1.0
"
||#


(defun cffi/cairo_surface_copy_page (surface)
"
 void cairo_surface_copy_page (cairo_surface_t *surface);

 Emits the current page for backends that support multiple pages, but doesn't clear it, so
 that the contents of the current page will be retained for the next page. Use
 cairo_surface_show_page() if you want to get an empty page after the emission.

 There is a convenience function for this that takes a cairo_t, namely cairo_copy_page().

    surface : a cairo_surface_t

 Since 1.6
"
  (if (or (null surface) (cffi:null-pointer-p surface))
      (error "CFFI/CAIRO_SURFACE_COPY_PAGE invoked with null SURFACE")
      (cffi:foreign-funcall "cairo_surface_copy_page"
			    :pointer surface
			    :void))
  (cairo-surface-status-report surface)
  surface)


(defun cffi/cairo_surface_show_page (surface)
"
 void cairo_surface_show_page (cairo_surface_t *surface);

 Emits and clears the current page for backends that support multiple pages. Use
 cairo_surface_copy_page() if you don't want to clear the page.

 There is a convenience function for this that takes a cairo_t, namely cairo_show_page().

    surface : a cairo_Surface_t

 Since 1.6
"
  (if (or (null surface) (cffi:null-pointer-p surface))
      (error "CFFI/CAIRO_SURFACE_SHOW_PAGE invoked with null SURFACE")
      (cffi:foreign-funcall "cairo_surface_show_page"
			    :pointer surface
			    :void))
  (cairo-surface-status-report surface)
  surface)


(defun cffi/cairo_surface_has_show_text_glyphs (surface)
"
 cairo_bool_t cairo_surface_has_show_text_glyphs (cairo_surface_t *surface);

 Returns whether the surface supports sophisticated cairo_show_text_glyphs() operations.
 That is, whether it actually uses the provided text and cluster data to a
 cairo_show_text_glyphs() call.

 Note: Even if this function returns FALSE, a cairo_show_text_glyphs() operation
 targeted at surface will still succeed. It just will act like a cairo_show_glyphs()
 operation. Users can use this function to avoid computing UTF-8 text and cluster
 mapping if the target surface does not use it.

    surface : a cairo_surface_t

    Returns : TRUE if surface supports cairo_show_text_glyphs(), FALSE otherwise

 Since 1.8
"
  (if (or (null surface) (cffi:null-pointer-p surface))
      (error "CFFI/CAIRO_SURFACE_HAS_SHOW_TEXT_GLYPHS invoked with null SURFACE")
      (cffi:foreign-funcall "cairo_surface_has_show_text_glyphs"
			    :pointer surface
			    cairo_bool_t)))


;; FIXME: This one is actually some kind of pointer to function taking... whatever.
(cffi:defctype cairo_destroy_func_t :pointer)


(defun cffi/cairo_surface_set_mime_data (surface mime-type data length destroy closure)
"
 cairo_status_t cairo_surface_set_mime_data (cairo_surface_t *surface,
                                             const char *mime_type,
                                             const unsigned char *data,
                                             unsigned long  length,
                                             cairo_destroy_func_t destroy,
                                             void *closure);

 Attach an image in the format mime_type to surface. To remove the data from a surface, call this
 function with same mime type and NULL for data.

 The attached image (or filename) data can later be used by backends which support it (currently:
 PDF, PS, SVG and Win32 Printing surfaces) to emit this data instead of making a snapshot of the
 surface. This approach tends to be faster and requires less memory and disk space.

 The recognized MIME types are the following: CAIRO_MIME_TYPE_JPEG, CAIRO_MIME_TYPE_PNG,
 CAIRO_MIME_TYPE_JP2, CAIRO_MIME_TYPE_URI, CAIRO_MIME_TYPE_UNIQUE_ID.

 See corresponding backend surface docs for details about which MIME types it can handle. Caution:
 the associated MIME data will be discarded if you draw on the surface afterwards. Use this
 function with care.

    surface : a cairo_surface_t
    mime_type : the MIME type of the image data
    data : the image data to attach to the surface
    length : the length of the image data
    destroy : a cairo_destroy_func_t which will be called when the surface is destroyed or when
              new image data is attached using the same mime type.
    closure : the data to be passed to the destroy notifier

    Returns : CAIRO_STATUS_SUCCESS or CAIRO_STATUS_NO_MEMORY if a slot could not be allocated
              for the user data.

 Since 1.10
"
  (if (or (null surface) (cffi:null-pointer-p surface))
      (error "CFFI/CAIRO_SURFACE_SET_MIME_DATA invoked with null SURFACE")
      (if (or (null mime-type) (cffi:null-pointer-p mime-type))
	  (error "CFFI/CAIRO_SURFACE_SET_MIME_DATA invoked with null MIME-TYPE")
	  (if (or (null destroy) (cffi:null-pointer-p destroy))
	      (error "CFFI/CAIRO_SURFACE_SET_MIME_DATA invoked with null DESTROY")
	      (if (or (null closure) (cffi:null-pointer-p closure))
		  (error "CFFI/CAIRO_SURFACE_SET_MIME_DATA invoked with null CLOSURE")
		  (let ((data (or data (cffi:null-pointer))))
		    (cffi:foreign-funcall "cairo_surface_set_mime_data"
					  :pointer surface
					  :pointer mime-type
					  :pointer data
					  :ulong length
					  cairo_destroy_func_t destroy
					  :pointer closure
					  :int)))))))


(defun cffi/cairo_surface_get_mime_data (surface mime-type data length)
"
 void cairo_surface_get_mime_data (cairo_surface_t *surface, const char *mime_type,
                                   const unsigned char **data, unsigned long *length);

 Return mime data previously attached to surface using the specified mime type. If no
 data has been attached with the given mime type, data is set NULL.

    surface : a cairo_surface_t
    mime_type : the mime type of the image data
    data : the image data to attached to the surface
    length : the length of the image data

 Since 1.10
"
  (if (or (null surface) (cffi:null-pointer-p surface))
      (error "CFFI/CAIRO_SURFACE_GET_MIME_DATA invoked with null SURFACE")
      (if (or (null mime-type) (cffi:null-pointer-p mime-type))
	  (error "CFFI/CAIRO_SURFACE_GET_MIME_DATA invoked with null MIME-TYPE")
	  (if (or (null data) (cffi:null-pointer-p data))
	      (error "CFFI/CAIRO_SURFACE_GET_MIME_DATA invoked with null DATA")
	      (if (or (null length) (cffi:null-pointer-p length))
		  (error "CFFI/CAIRO_SURFACE_GET_MIME_DATA invoked with null LENGTH")
		  (cffi:foreign-funcall "cairo_surface_get_mime_data"
					:pointer surface
					:pointer mime-type
					:pointer data
					:pointer length
					:void)))))
  ;; This is probably more use than "surface"
  (cairo-surface-status-report surface)
  data)


(defun cffi/cairo_surface_supports_mime_type (surface mime-type)
"
 cairo_bool_t cairo_surface_supports_mime_type (cairo_surface_t *surface,
                                                const char *mime_type);

 Return whether surface supports mime_type.

    surface : a cairo_surface_t
    mime_type : the mime type

    Returns : TRUE if surface supports mime_type, FALSE otherwise

 Since 1.12
"
  (if (or (null surface) (cffi:null-pointer-p surface))
      (error "CFFI/CAIRO_SURFACE_SUPPORTS_MIME_TYPE invoked with null SURFACE")
      (if (or (null mime-type) (cffi:null-pointer-p mime-type))
	  (error "CFFI/CAIRO_SURFACE_SUPPORTS_MIME_TYPE invoked with null MIME-TYPE")
	  (cffi:foreign-funcall "cairo_surface_supports_mime_type"
				:pointer surface
				:pointer mime-type
				cairo_bool_t))))


(defun cffi/cairo_surface_map_to_image (surface extents)
"
 cairo_surface_t * cairo_surface_map_to_image (cairo_surface_t *surface,
                                               const cairo_rectangle_int_t *extents);

 Returns an image surface that is the most efficient mechanism for modifying the backing
 store of the target surface. The region retrieved may be limited to the extents or NULL
 for the whole surface

 Note, the use of the original surface as a target or source whilst it is mapped is
 undefined. The result of mapping the surface multiple times is undefined. Calling
 cairo_surface_destroy() or cairo_surface_finish() on the resulting image surface results
 in undefined behavior. Changing the device transform of the image surface or of surface
 before the image surface is unmapped results in undefined behavior.

    surface : an existing surface used to extract the image from
    extents : limit the extraction to an rectangular region

    Returns : a pointer to the newly allocated image surface. The caller must use
              cairo_surface_unmap_image() to destroy this image surface. This function
    always returns a valid pointer, but it will return a pointer to a 'nil' surface if
    other is already in an error state or any other error occurs. If the returned
    pointer does not have an error status, it is guaranteed to be an image surface
    whose format is not CAIRO_FORMAT_INVALID.

 Since 1.12
"
  (if (or (null surface) (cffi:null-pointer-p surface))
      (error "CFFI/CAIRO_SURFACE_MAP_TO_IMAGE invoked with null SURFACE")
      (let* ((extents (or extents (cffi:null-pointer)))
	     (image   (cffi:foreign-funcall "cairo_surface_map_to_image"
					    :pointer surface
					    :pointer extents
					    :pointer)))
	(cairo-surface-status-report image)
	image)))



(defun cffi/cairo_surface_unmap_image (surface image)
"
 void cairo_surface_unmap_image (cairo_surface_t *surface,
                                 cairo_surface_t *image);

 Unmaps the image surface as returned from #cairo_surface_map_to_image().

 The content of the image will be uploaded to the target surface. Afterwards,
 the image is destroyed.

 Using an image surface which wasn't returned by cairo_surface_map_to_image()
 results in undefined behavior.

    surface : the surface passed to cairo_surface_map_to_image().
    image : the currently mapped image

 Since 1.12
"
  (if (or (null surface) (cffi:null-pointer-p surface))
      (error "CFFI/CAIRO_SURFACE_UNMAP_IMAGE invoked with null SURFACE")
      (if (or (null image) (cffi:null-pointer-p image))
	  (error "CFFI/CAIRO_SURFACE_UNMAP_IMAGE invoked with null IMAGE")
	  (cffi:foreign-funcall "cairo_surface_unmap_image"
				:pointer surface
				:pointer image
				:void)))
  (cairo-surface-status-report surface)
  surface)


#||
See Also

cairo_t, cairo_pattern_t
||#


;;;;;;;;;; IMAGE SURFACES

#||
Description

Image surfaces provide the ability to render to memory buffers either allocated by cairo or by the calling code. The supported image formats are those defined in cairo_format_t.

Details

CAIRO_HAS_IMAGE_SURFACE

#define CAIRO_HAS_IMAGE_SURFACE 1
Defined if the image surface backend is available. The image surface backend is always built in. This macro was added for completeness in cairo 1.8.

Since 1.8
||#

(cffi:defcenum cairo_format_t
"
 cairo_format_t is used to identify the memory format of image data.

 New entries may be added in future versions.

 CAIRO_FORMAT_INVALID
    no such format exists or is supported.

 CAIRO_FORMAT_ARGB32
    each pixel is a 32-bit quantity, with alpha in the upper 8 bits, then red, then green, then blue.
    The 32-bit quantities are stored native-endian. Pre-multiplied alpha is used. (That is, 50%
    transparent red is 0x80800000, not 0x80ff0000.) (Since 1.0)

    From Wikipedia (assuming an alpha of 0.5):

        However, if this pixel uses premultiplied alpha, all of the RGB values (0, 1, 0) are multiplied
        by 0.5 and then the alpha is appended to the end to yield (0, 0.5, 0, 0.5). In this case, the
        0.5 value for the G channel actually indicates 100% green intensity (with 50% opacity). For this
        reason, knowing whether a file uses premultiplied or straight alpha is essential to correctly
        process or composite it.

 CAIRO_FORMAT_RGB24
    each pixel is a 32-bit quantity, with the upper 8 bits unused. Red, Green, and Blue are stored
    in the remaining 24 bits in that order. (Since 1.0)

 CAIRO_FORMAT_A8
    each pixel is a 8-bit quantity holding an alpha value. (Since 1.0)

 CAIRO_FORMAT_A1
    each pixel is a 1-bit quantity holding an alpha value. Pixels are packed together into 32-bit
    quantities. The ordering of the bits matches the endianess of the platform. On a big-endian
    machine, the first pixel is in the uppermost bit, on a little-endian machine the first pixel is
    in the least-significant bit. (Since 1.0)

 CAIRO_FORMAT_RGB16_565
    each pixel is a 16-bit quantity with red in the upper 5 bits, then green in the middle 6 bits,
    and blue in the lower 5 bits. (Since 1.2)

 CAIRO_FORMAT_RGB30
    like RGB24 but with 10bpc. (Since 1.12)

 Since 1.0
"
    (:CAIRO_FORMAT_INVALID  -1)
    (:CAIRO_FORMAT_ARGB32    0)
    (:CAIRO_FORMAT_RGB24     1)
    (:CAIRO_FORMAT_A8        2)
    (:CAIRO_FORMAT_A1        3)
    (:CAIRO_FORMAT_RGB16_565 4)
    (:CAIRO_FORMAT_RGB30     5))


(defconstant +CFFI/CAIRO_FORMAT_INVALID+ (cffi:foreign-enum-value 'cairo_format_t :CAIRO_FORMAT_INVALID))
(defconstant +CFFI/CAIRO_FORMAT_ARGB32+ (cffi:foreign-enum-value 'cairo_format_t :CAIRO_FORMAT_ARGB32))
(defconstant +CFFI/CAIRO_FORMAT_RGB24+ (cffi:foreign-enum-value 'cairo_format_t :CAIRO_FORMAT_RGB24))
(defconstant +CFFI/CAIRO_FORMAT_A8+ (cffi:foreign-enum-value 'cairo_format_t :CAIRO_FORMAT_A8))
(defconstant +CFFI/CAIRO_FORMAT_A1+ (cffi:foreign-enum-value 'cairo_format_t :CAIRO_FORMAT_A1))
(defconstant +CFFI/CAIRO_FORMAT_RGB16_565+ (cffi:foreign-enum-value 'cairo_format_t :CAIRO_FORMAT_RGB16_565))
(defconstant +CFFI/CAIRO_FORMAT_RGB30+ (cffi:foreign-enum-value 'cairo_format_t :CAIRO_FORMAT_RGB30))


(defun cffi/cairo_format_stride_for_width (format width)
"
 int cairo_format_stride_for_width (cairo_format_t format, int width);

 This function provides a stride value that will respect all alignment requirements of the accelerated
 image-rendering code within cairo. Typical usage will be of the form:

    1 int stride;
    2 unsigned char *data;
    3 cairo_surface_t *surface;
    4
    5 stride = cairo_format_stride_for_width (format, width);
    6 data = malloc (stride * height);
    7 surface = cairo_image_surface_create_for_data (data, format,
    8  					             width, height,
    9 					             stride);

    format : A cairo_format_t value
    width : The desired width of an image surface to be created.

    Returns : the appropriate stride to use given the desired format and width, or -1 if either the
              format is invalid or the width too large.

 Since 1.6
"
  (cffi:foreign-funcall "cairo_format_stride_for_width"
			:int format
			:int width
			:int))


(defun cffi/cairo_image_surface_create (format width height)
"
 cairo_surface_t * cairo_image_surface_create (cairo_format_t format,
                                               int width, int height);

 Creates an image surface of the specified format and dimensions. Initially the surface contents
 are all 0. (Specifically, within each pixel, each color or alpha channel belonging to format will
 be 0. The contents of bits within a pixel, but not belonging to the given format are undefined).

    format : format of pixels in the surface to create
    width : width of the surface, in pixels
    height : height of the surface, in pixels

    Returns : a pointer to the newly created surface. The caller owns the surface and should call
              cairo_surface_destroy() when done with it. This function always returns a valid
    pointer, but it will return a pointer to a 'nil' surface if an error such as out of memory
    occurs. You can use cairo_surface_status() to check for this.

 Since 1.0
"
  (let ((surface (cffi:foreign-funcall "cairo_image_surface_create"
				       :int format
				       :int width :int height
				       :pointer)))
    (cairo-surface-status-report surface)
    surface))



(defun cffi/cairo_image_surface_create_for_data (data format width height stride)
"
 cairo_surface_t * cairo_image_surface_create_for_data (unsigned char *data,
                                                        cairo_format_t format,
                                                        int width, int height, 
                                                        int stride);

 Creates an image surface for the provided pixel data. The output buffer must be kept
 around until the cairo_surface_t is destroyed or cairo_surface_finish() is called on
 the surface. The initial contents of data will be used as the initial image contents;
 you must explicitly clear the buffer, using, for example, cairo_rectangle() and
 cairo_fill() if you want it cleared.

 Note that the stride may be larger than width*bytes_per_pixel to provide proper
 alignment for each pixel and row. This alignment is required to allow high-performance
 rendering within cairo. The correct way to obtain a legal stride value is to call
 cairo_format_stride_for_width() with the desired format and maximum image width value,
 and then use the resulting stride value to allocate the data and to create the image
 surface. See cairo_format_stride_for_width() for example code.

    data : a pointer to a buffer supplied by the application in which to write contents.
           This pointer must be suitably aligned for any kind of variable, (for example,
           a pointer returned by malloc).
    format : the format of pixels in the buffer
    width : the width of the image to be stored in the buffer
    height : the height of the image to be stored in the buffer
    stride : the number of bytes between the start of rows in the buffer as allocated.
             This value should always be computed by cairo_format_stride_for_width()
             before allocating the data buffer.

    Returns : a pointer to the newly created surface. The caller owns the surface and
              should call cairo_surface_destroy() when done with it. This function always
    returns a valid pointer, but it will return a pointer to a 'nil' surface in the case
    of an error such as out of memory or an invalid stride value. In case of invalid stride
    value the error status of the returned surface will be CAIRO_STATUS_INVALID_STRIDE. You
    can use cairo_surface_status() to check for this. See cairo_surface_set_user_data() for
    a means of attaching a destroy-notification fallback to the surface if necessary.

 Since 1.0
"
  (if (or (null data) (cffi:null-pointer-p data))
      (error "CFFI/CAIRO_IMAGE_SURFACE_CREATE_FOR_DATA invoked with null DATA")
      (let ((surface (cffi:foreign-funcall "cairo_image_surface_create_for_data"
					   :pointer data
					   :int format
					   :int width :int height
					   :int stride
					   :pointer)))
	(cairo-surface-status-report surface)
	surface)))



(defun cffi/cairo_image_surface_get_data (surface)
"
 unsigned char * cairo_image_surface_get_data (cairo_surface_t *surface);

 Get a pointer to the data of the image surface, for direct inspection or modification.

 A call to cairo_surface_flush() is required before accessing the pixel data to ensure
 that all pending drawing operations are finished. A call to cairo_surface_mark_dirty()
 is required after the data is modified.

    surface : a cairo_image_surface_t

    Returns : a pointer to the image data of this surface or NULL if surface is not an
              image surface, or if cairo_surface_finish() has been called.
 Since 1.2
"
  (if (or (null surface) (cffi:null-pointer-p surface))
      (error "CFFI/CAIRO_IMAGE_SURFACE_GET_DATA invoked with null SURFACE")
      (let ((data (cffi:foreign-funcall "cairo_image_surface_get_data"
					:pointer surface
					:pointer)))    ;; unsigned char*
	(cairo-surface-status-report surface)
	(pointer-or-nil data))))


(defun cffi/cairo_image_surface_get_format (surface)
"
 cairo_format_t cairo_image_surface_get_format (cairo_surface_t *surface);

 Get the format of the surface.

    surface : a cairo_image_surface_t
    Returns : the format of the surface

 Since 1.2
"
  (if (or (null surface) (cffi:null-pointer-p surface))
      (error "CFFI/CAIRO_IMAGE_SURFACE_GET_FORMAT invoked with null SURFACE")
      (cffi:foreign-funcall "cairo_image_surface_get_format"
			    :pointer surface
			    :int)))


(defun cffi/cairo_image_surface_get_width (surface)
"
 int cairo_image_surface_get_width (cairo_surface_t *surface);

 Get the width of the image surface in pixels.

    surface : a cairo_image_surface_t

    Returns : the width of the surface in pixels.

 Since 1.0
"
  (if (or (null surface) (cffi:null-pointer-p surface))
      (error "CFFI/CAIRO_IMAGE_SURFACE_GET_WIDTH invoked with null SURFACE")
      (cffi:foreign-funcall "cairo_image_surface_get_width"
			    :pointer surface
			    :int)))


(defun cffi/cairo_image_surface_get_height (surface)
"
 int cairo_image_surface_get_height (cairo_surface_t *surface);

 Get the height of the image surface in pixels.

    surface : a cairo_image_surface_t

    Returns : the height of the surface in pixels.

 Since 1.0
"
  (if (or (null surface) (cffi:null-pointer-p surface))
      (error "CFFI/CAIRO_IMAGE_SURFACE_GET_HEIGHT invoked with null SURFACE")
      (cffi:foreign-funcall "cairo_image_surface_get_height"
			    :pointer surface
			    :int)))


(defun cffi/cairo_image_surface_get_stride (surface)
"
 int cairo_image_surface_get_stride (cairo_surface_t *surface);

 Get the stride of the image surface in bytes

    surface : a cairo_image_surface_t

    Returns : the stride of the image surface in bytes (or 0 if surface is not an
              image surface). The stride is the distance in bytes from the
    beginning of one row of the image data to the beginning of the next row.

 Since 1.2
"
  (if (or (null surface) (cffi:null-pointer-p surface))
      (error "CFFI/CAIRO_IMAGE_SURFACE_GET_STRIDE invoked with null SURFACE")
      (cffi:foreign-funcall "cairo_image_surface_get_stride"
			    :pointer surface
			    :int)))

#||
See Also

cairo_surface_t
||#


;;;;;;;;;; cairo transforms


(defun cffi/cairo_translate (context tx ty)
"
 void cairo_translate (cairo_t *cr, double tx, double ty);

 Modifies the current transformation matrix (CTM) by translating the user-space
 origin by (tx, ty). This offset is interpreted as a user-space coordinate
 according to the CTM in place before the new call to cairo_translate(). In other
 words, the translation of the user-space origin takes place after any existing
 transformation.

    cr : a cairo context
    tx : amount to translate in the X direction
    ty : amount to translate in the Y direction

 Since 1.0
"
  (cffi:foreign-funcall "cairo_translate"
			:pointer context
			:double tx :double ty
			:void)
  (cairo-status-report context)
  context)


(defun cffi/cairo_scale (context sx sy)
"
 void cairo_scale (cairo_t *cr, double sx, double sy);

 Modifies the current transformation matrix (CTM) by scaling the X and Y
 user-space axes by sx and sy respectively. The scaling of the axes takes
 place after any existing transformation of user space.

    cr : a cairo context
    sx : scale factor for the X dimension
    sy : scale factor for the Y dimension

 Since 1.0
"
  (cffi:foreign-funcall "cairo_scale"
			:pointer context
			:double sx :double sy
			:void)
  (cairo-status-report context)
  context)


(defun cffi/cairo_rotate (context angle)
"
 void cairo_rotate (cairo_t *cr, double angle);

 Modifies the current transformation matrix (CTM) by rotating the user-space
 axes by angle radians. The rotation of the axes takes places after any
 existing transformation of user space. The rotation direction for positive
 angles is from the positive X axis toward the positive Y axis.

    cr : a cairo context
    angle : angle (in radians) by which the user-space axes will be rotated

 Since 1.0
"
  (cffi:foreign-funcall "cairo_rotate"
			:pointer context
			:double angle
			:void)
  (cairo-status-report context)
  context)


(defun cffi/cairo_transform (context matrix)
"
 void cairo_transform (cairo_t *cr, const cairo_matrix_t *matrix);

 Modifies the current transformation matrix (CTM) by applying matrix as an
 additional transformation. The new transformation of user space takes place
 after any existing transformation.

    cr : a cairo context
    matrix : a transformation to be applied to the user-space axes

 Since 1.0
"
  (cffi:foreign-funcall "cairo_transform"
			:pointer context
			:pointer matrix
			:void)
  (cairo-status-report context)
  context)


(defun cffi/cairo_set_matrix (context matrix)
"
 void cairo_set_matrix (cairo_t *cr, const cairo_matrix_t *matrix);

 Modifies the current transformation matrix (CTM) by setting it equal to matrix.

    cr : a cairo context
    matrix : a transformation matrix from user space to device space

 Since 1.0
"
  (cffi:foreign-funcall "cairo_set_matrix"
			:pointer context
			:pointer matrix
			:void)
  (cairo-status-report context)
  context)


(defun cffi/cairo_get_matrix (context matrix)
"
 void cairo_get_matrix (cairo_t *cr, cairo_matrix_t *matrix);

 Stores the current transformation matrix (CTM) into matrix.

    cr : a cairo context
    matrix : return value for the matrix

 Since 1.0
"
  (cffi:foreign-funcall "cairo_get_matrix"
			:pointer context
			:pointer matrix
			:void)
  (cairo-status-report context)
  context)


(defun cffi/cairo_identity_matrix (context)
"
 void cairo_identity_matrix (cairo_t *cr);

 Resets the current transformation matrix (CTM) by setting it equal to the
 identity matrix. That is, the user-space and device-space axes will be
 aligned and one user-space unit will transform to one device-space unit.

    cr : a cairo context

 Since 1.0
"
  (cffi:foreign-funcall "cairo_identity_matrix"
			:pointer context
			:void)
  (cairo-status-report context)
  context)


(defun cffi/cairo_user_to_device (context x y)
"
 void cairo_user_to_device (cairo_t *cr, double *x, double *y);

 Transform a coordinate from user space to device space by multiplying the
 given point by the current transformation matrix (CTM).

    cr : a cairo context
    x : X value of coordinate (in/out parameter)
    y : Y value of coordinate (in/out parameter)

 Since 1.0
"
  (cffi:foreign-funcall "cairo_user_to_device"
			:pointer context
			:pointer x
			:pointer y
			:void)
  (cairo-status-report context)
  context)


(defun cffi/cairo_user_to_device_distance (context dx dy)
"
 void cairo_user_to_device_distance (cairo_t *cr, double *dx, double *dy);

 Transform a distance vector from user space to device space. This function is
 similar to cairo_user_to_device() except that the translation components of the
 CTM will be ignored when transforming (dx,dy).

    cr : a cairo context
    dx : X component of a distance vector (in/out parameter)
    dy : Y component of a distance vector (in/out parameter)

 Since 1.0
"
  (cffi:foreign-funcall "cairo_user_to_device_distance"
			:pointer context
			:pointer dx
			:pointer dy
			:void)
  (cairo-status-report context)
  context)


(defun cffi/cairo_device_to_user (context x y)
"
 void cairo_device_to_user (cairo_t *cr, double *x, double *y);

 Transform a coordinate from device space to user space by multiplying the
 given point by the inverse of the current transformation matrix (CTM).

    cr : a cairo
    x : X value of coordinate (in/out parameter)
    y : Y value of coordinate (in/out parameter)

 Since 1.0
"
  (cffi:foreign-funcall "cairo_device_to_user"
			:pointer context
			:pointer x
			:pointer y
			:void)
  (cairo-status-report context)
  context)


(defun cffi/cairo_device_to_user_distance (context dx dy)
"
 void cairo_device_to_user_distance (cairo_t *cr, double *dx, double *dy);

 Transform a distance vector from device space to user space. This function is similar
 to cairo_device_to_user() except that the translation components of the inverse CTM
 will be ignored when transforming (dx,dy).

    cr : a cairo context
    dx : X component of a distance vector (in/out parameter)
    dy : Y component of a distance vector (in/out parameter)

 Since 1.0
"
  (cffi:foreign-funcall "cairo_device_to_user_distance"
			:pointer context
			:pointer dx
			:pointer dy
			:void)
  (cairo-status-report context)
  context)


#||
See Also

cairo_matrix_t
||#

;;;;;;;;;; Lots of other stuff... just do what is needed from now on.
;; Maybe look at "User Fonts" too... could be interesting to replicate
;; to old TI Explorer (Lisp Machine) font...
