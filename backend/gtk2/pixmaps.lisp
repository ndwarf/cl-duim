;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GTK2-DUIM -*-
(in-package #:gtk2-duim)

#||
Module:       gtk-duim
Synopsis:     GTK pixmap implementation
Author:       Andy Armstrong, Scott McKay
Copyright:    Original Code is Copyright (c) 1995-2004 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND

/// GTK pixmaps
||#
#||
/*---*** Not doing pixmaps yet!

define sealed class <gtk-pixmap> (<pixmap>)
  sealed slot %pixmap :: false-or(<GdkPixmap*>) = #f,
    init-keyword: pixmap:;
  sealed slot %medium :: <gtk-medium>,
    required-init-keyword: medium:;
end class <gtk-pixmap>;

define sealed domain make (singleton(<gtk-pixmap>));
define sealed domain initialize (<gtk-pixmap>);
||#

(defclass <gtk-pixmap> (<pixmap>)
  ;; was: %pixmap
  ((%surface :initform nil :initarg :surface :accessor %surface)   ;; CAIRO_SURFACE_T reference
   (%context :initform nil :accessor %context)                    ;; CAIRO_T context for drawing on to %SURFACE
   (width :initarg :width :accessor width)
   (height :initarg :height :accessor height)
   ;; FIXME: Is this needed? Not sure it's much use.
   (%medium :initform (required-slot ":medium" "<gtk-pixmap>") :initarg :medium :accessor %medium)))


#||
define sealed method do-make-pixmap
    (_port :: <gtk-port>, medium :: <gtk-medium>, 
     width :: <integer>, height :: <integer>)
 => (pixmap :: <gtk-pixmap>)
  make(<gtk-pixmap>, medium: medium, width: width, height: height)
end method do-make-pixmap;
||#

(defmethod do-make-pixmap ((_port <gtk-port>)
			   (medium <gtk-medium>)
			   (width integer)
			   (height integer))
  (make-instance '<gtk-pixmap>
		 :medium medium
		 :width  width
		 :height height))

#||
define sealed method initialize
    (pixmap :: <gtk-pixmap>, #key width, height, medium) => ()
  next-method();
  let x-display = port(pixmap).%display;
  let x-screen  = x/ScreenOfDisplay(sheet-direct-mirror(display(medium)));
  let drawable  = medium-drawable(medium);
  let depth     = x/DefaultDepthOfScreen(x-screen);
  let x-pixmap  = gdk-pixmap-new(window, width, height, depth);
  let pixmap    = make(<xmeduim-pixmap>, port: port, x-pixmap: x-pixmap);
  //---*** Do we have to call 'gdk-pixmap-ref'???
  pixmap.%pixmap := x-pixmap
end method initialize;
||#

(defmethod initialize-instance :after ((pixmap <gtk-pixmap>) &key surface width height medium &allow-other-keys)
  (declare (ignore medium))
  ;; If no pixmap is supplied (:surface initarg) but width + height are specified, create a cairo
  ;; surface of the right size and store it as the pixmap surface - it will be drawn on at some
  ;; point.
  (if (and (not surface) width height)
      (let* ((surface (cffi/cairo_image_surface_create +CFFI/CAIRO_FORMAT_ARGB32+ width height))
	     (context (cffi/cairo-create surface)))
	;; No need to retain these, but be sure to call cairo_(surface_)destroy() when finished.
	;; That gets done in DESTROY-PIXMAP.
	(setf (%surface pixmap) surface)
	(setf (%context pixmap) context))
      ;; else
      (if surface
	  ;; Create the context from the surface provided!
	  (setf (%context pixmap) (cffi/cairo-create surface))
	  (error "Making a <gtk-pixmap> with no width + height, and no surface, specified"))))

#||
define sealed method destroy-pixmap
    (pixmap :: <gtk-pixmap>) => ()
  let x-pixmap  = pixmap.%pixmap;
  pixmap.%pixmap := #f;
  gdk-pixmap-unref(x-pixmap)
end method destroy-pixmap;
||#

(defmethod destroy-pixmap ((pixmap <gtk-pixmap>))
  (let ((cairo-source  (%surface pixmap))
	(cairo-context (%context pixmap)))
    (setf (%surface pixmap)  nil)
    (setf (%context pixmap) nil)
    (cffi/cairo-destroy cairo-context)
    (cffi/cairo_surface_destroy cairo-source)))


#||
define sealed method port 
    (pixmap :: <gtk-pixmap>) => (port :: <gtk-port>)
  port(pixmap.%medium)
end method port;
||#

(defmethod port ((pixmap <gtk-pixmap>))
  (port (%medium pixmap)))


#||
define sealed method pixmap-drawable
    (pixmap :: <gtk-pixmap>) => (drawable)
  pixmap
end method pixmap-drawable;
||#

;;; This returns a Cairo CONTEXT. Drawing ops on that context
;;; are drawn onto the %SURFACE associated with PIXMAP.
(defmethod pixmap-drawable ((pixmap <gtk-pixmap>))
  (%context pixmap))


#||
define sealed method image-width 
    (pixmap :: <gtk-pixmap>) => (width :: <integer>)
  let x-display = port(pixmap).%display;
  let x-pixmap  = pixmap.%pixmap;
  let (success?, root, x, y, width, height, border, depth)
    = x/XGetGeometry(x-display, x-pixmap);
  ignore(success?, root, x, y, height, border, depth);
  width
end method image-width;
||#

(defmethod image-width ((pixmap <gtk-pixmap>))
  (width pixmap))


#||
define sealed method image-height 
    (pixmap :: <gtk-pixmap>) => (height :: <integer>)
  let x-display = port(pixmap).%display;
  let x-pixmap  = pixmap.%pixmap;
  let (success?, root, x, y, width, height, border, depth)
    = x/XGetGeometry(x-display, x-pixmap);
  ignore(success?, root, x, y, width, border, depth);
  height
end method image-height;
||#

(defmethod image-height ((pixmap <gtk-pixmap>))
  (height pixmap))


#||
define sealed method image-depth 
    (pixmap :: <gtk-pixmap>) => (depth :: <integer>)
  let x-display = port(pixmap).%display;
  let x-pixmap  = pixmap.%pixmap;
  let (success?, root, x, y, width, height, border, depth)
    = x/XGetGeometry(x-display, x-pixmap);
  ignore(success?, root, x, y, width, height, border);
  depth
end method image-depth;
||#

(defmethod image-depth ((pixmap <gtk-pixmap>))
  ;; Pixmap could contain argb32 data, or a8 data
  (let ((content_t (cffi/cairo_surface_get_content (%surface pixmap))))
    (if (eql content_t +CFFI/CAIRO_CONTENT_ALPHA+)
	8      ; alpha only = a8
	32)))  ; Must be argb32



#||
/// GTK pixmap mediums

define sealed class <gtk-pixmap-medium>
    (<gtk-medium>, 
     <basic-pixmap-medium>)
end class <gtk-pixmap-medium>;

define sealed domain make (singleton(<gtk-pixmap-medium>));
define sealed domain initialize (<gtk-pixmap-medium>);
||#

(defclass <gtk-pixmap-medium> (<gtk-medium> <basic-pixmap-medium>) ())


(defmethod get-context ((medium <gtk-pixmap-medium>))
  (medium-drawable medium))


#||
define sealed method make-pixmap-medium
    (_port :: <gtk-port>, sheet :: <sheet>, #key width, height)
 => (medium :: <gtk-pixmap-medium>)
  with-sheet-medium (medium = sheet)
    let pixmap = do-make-pixmap(_port, medium, width, height);
    let medium = make(<gtk-pixmap-medium>,
		      port: _port,
		      sheet: sheet,
		      pixmap: pixmap);
    medium-drawable(medium) := pixmap;
    medium
  end
end method make-pixmap-medium;
||#

(defmethod make-pixmap-medium ((_port <gtk-port>)
			       (sheet <sheet>)
			       &key width height)
  (with-sheet-medium (medium = sheet)
    (let* ((pixmap (do-make-pixmap _port medium width height))
	   (medium (make-instance '<gtk-pixmap-medium>
				  :port   _port
				  :sheet  sheet
				  :pixmap pixmap)))
      ;; The "DRAWABLE" is a CAIRO_T
      (setf (medium-drawable medium) (pixmap-drawable pixmap))
      medium)))



#||
/// BitBlt

//---*** THESE ALL NEED TO GET A GC TO DO THE COPYING ON AND ESTABLISH THE COPYING GCONTEXT

define sealed method do-copy-area
    (from-medium :: <gtk-medium>, from-x :: <integer>, from-y :: <integer>,
     width :: <integer>, height :: <integer>,
     to-medium :: <gtk-medium>, to-x :: <integer>, to-y :: <integer>,
     #key function = $boole-1) => ()
  if (from-medium == to-medium)
    let sheet     = medium-sheet(from-medium);
    let transform = sheet-device-transform(sheet);
    let drawable  = medium-drawable(from-medium);
    with-device-coordinates (transform, from-x, from-y, to-x, to-y)
      with-device-distances (transform, width, height)
	gdk-window-copy-area(drawable, gcontext, to-x, to-y,
			     drawable, from-x, from-y, width, height)
      end
    end
  else
    let from-sheet     = medium-sheet(from-medium);
    let from-transform = sheet-device-transform(from-sheet);
    let from-drawable  = medium-drawable(from-medium);
    let to-sheet       = medium-sheet(to-medium);
    let to-transform   = sheet-device-transform(to-sheet);
    let to-drawable    = medium-drawable(to-medium);
    with-device-coordinates (from-transform, from-x, from-y)
      with-device-coordinates (to-transform, to-x, to-y)
	with-device-distances (from-transform, width, height)
	  gdk-window-copy-area(to-drawable, gcontext, to-x, to-y,
			       from-drawable, from-x, from-y, width, height)
	end
      end
    end
  end
end method do-copy-area;
||#

(DEFUN DO-CAIRO-COPY-AREA (FROM-CR FROM-X FROM-Y WIDTH HEIGHT TO-CR TO-X TO-Y &KEY (FUNCTION +BOOLE-1+))
"
Copy the region WIDTH x HEIGHT @ FROM-X, FROM-Y from FROM-CR onto TO-CR at
TO-X, TO-Y. If FUNCTION is provided, the content will be composited onto
any existing pattern present on the target surface of TO-CR.

All coordinates provided to this method should be in device coordinates.
"
  (CFFI/CAIRO_SET_OPERATOR TO-CR (AREF *FUNCTION-MAP* FUNCTION))
  ;; We want the source surface origin to be at (from-x, from-y).
  (CFFI/CAIRO_SET_SOURCE_SURFACE TO-CR
				 (CFFI/CAIRO-GET-TARGET FROM-CR)
				 (coerce (- TO-X FROM-X) 'double-float)
				 (coerce (- TO-Y FROM-Y) 'double-float))
  (CFFI/CAIRO_RECTANGLE TO-CR
			(coerce TO-X   'double-float)
			(coerce TO-Y   'double-float)
			(coerce WIDTH  'double-float)
			(coerce HEIGHT 'double-float))
  (CFFI/CAIRO_FILL TO-CR))


(defmethod do-copy-area ((from-medium <gtk-medium>) (from-x integer) (from-y integer)
			 (width integer) (height integer)
			 (to-medium <gtk-medium>) (to-x   integer) (to-y   integer)
			 &key (function +boole-1+))
  (if (eql from-medium to-medium)
      ;; same-window bitblt
      (let* ((sheet     (medium-sheet from-medium))
	     (transform (sheet-device-transform sheet))
	     (cr (update-drawing-state from-medium)))
	;; Easiest way is to copy to an intermediate pixmap, then copy back in the right place.
	;; FIXME: Find a more efficient way to do this...
	(with-device-coordinates (transform from-x from-y to-x to-y)
	  (with-device-distances (transform width height)
	    (let* ((pixmap (make-pixmap from-medium width height))
		   (pix-cr (%context pixmap)))
	      ;; FIXME: FUNCTION IN THIS FIRST ONE SHOULD BE "SURFACE" TO COMPLETELY OVERWRITE ANY TARGET.
	      (DO-CAIRO-COPY-AREA CR FROM-X FROM-Y WIDTH HEIGHT PIX-CR 0 0 :FUNCTION +BOOLE-1+)
	      (DO-CAIRO-COPY-AREA PIX-CR 0 0 WIDTH HEIGHT CR TO-X TO-Y :FUNCTION FUNCTION)))))
      ;; else (different drawables bitblt)
      ;; almost the same code, but more transformations
      (let* ((from-sheet     (medium-sheet from-medium))
	     (from-transform (sheet-device-transform from-sheet))
	     (from-cr        (medium-drawable from-medium))
	     (to-sheet       (medium-sheet to-medium))
	     (to-transform   (sheet-device-transform to-sheet)))
	(let ((to-cr (update-drawing-state to-medium)))
	  (with-device-coordinates (from-transform from-x from-y)
	    (with-device-coordinates (to-transform to-x to-y)
	      (with-device-distances (from-transform width height)
		(DO-CAIRO-COPY-AREA FROM-CR FROM-X FROM-Y WIDTH HEIGHT TO-CR TO-X TO-Y :FUNCTION FUNCTION))))))))


#||
define sealed method do-copy-area
    (from-medium :: <gtk-medium>, from-x :: <integer>, from-y :: <integer>,
     width :: <integer>, height :: <integer>,
     to-medium :: <gtk-pixmap-medium>, to-x :: <integer>, to-y :: <integer>,
     #key function = $boole-1) => ()
  let from-transform = sheet-device-transform(medium-sheet(from-medium));
  let from-drawable  = medium-drawable(from-medium);
  let to-drawable    = medium-drawable(to-medium);
  with-device-coordinates (from-transform, from-x, from-y)
    with-device-distances (from-transform, width, height)
      gdk-window-copy-area(to-drawable, gcontext, to-x, to-y,
			   from-drawable, from-x, from-y, width, height)
    end
  end
end method do-copy-area;
||#

(defmethod do-copy-area ((from-medium <gtk-medium>) (from-x integer) (from-y integer)
			 (width integer) (height integer)
			 (to-medium <gtk-pixmap-medium>) (to-x integer) (to-y integer)
			 &key (function +boole-1+))
  (let ((from-transform (sheet-device-transform (medium-sheet from-medium)))
	;; FIXME: What to do about this? "MEDIUM-DRAWABLE" is often not right.
	;; Perhaps that method should trampoline onto GET-CONTEXT?
	#-(and)
	(from-cr (medium-drawable from-medium))
	(from-cr (get-context from-medium)))
    ;; FIXME: WHAT DOES "UPDATE-DRAWING-STATE" DO WHEN PASSED A <GTK-PIXMAP-MEDIUM>?
    ;; WE UPDATE THE DRAWING STATE FOR THE TO-MEDIUM, BUT *NOT* FOR THE FROM-MEDIUM!
    (let ((to-cr (update-drawing-state to-medium)))
      (with-device-coordinates (from-transform from-x from-y)
	(with-device-distances (from-transform width height)
	  ;; XXX: It looks like we assume pixmap mediums always use *identity-transform*
	  (DO-CAIRO-COPY-AREA FROM-CR FROM-X FROM-Y WIDTH HEIGHT TO-CR TO-X TO-Y :FUNCTION FUNCTION))))))


#||
define sealed method do-copy-area
    (from-medium :: <gtk-medium>, from-x :: <integer>, from-y :: <integer>,
     width :: <integer>, height :: <integer>,
     pixmap :: <gtk-pixmap>, to-x :: <integer>, to-y :: <integer>,
     #key function = $boole-1) => ()
  let from-transform = sheet-device-transform(medium-sheet(from-medium));
  let from-drawable  = medium-drawable(from-medium);
  let to-drawable    = pixmap.%pixmap;
  with-device-coordinates (from-transform, from-x, from-y)
    with-device-distances (from-transform, width, height)
      gdk-window-copy-area(to-drawable, gcontext, to-x, to-y,
			   from-drawable, from-x, from-y, width, height)
    end
  end
end method do-copy-area;
||#

(defmethod do-copy-area ((from-medium <gtk-medium>) (from-x integer) (from-y integer)
			 (width integer) (height integer)
			 (pixmap <gtk-pixmap>)      (to-x integer)   (to-y integer)
			 &key (function +boole-1+))
  (let ((from-transform (sheet-device-transform (medium-sheet from-medium)))
	;; FIXME: MEDIUM-DRAWABLE IS NOT CONSTANT! WHAT TO DO ABOUT THAT?
	#-(and)
	(from-cr (medium-drawable from-medium))
	(from-cr (get-context from-medium))
	(to-cr (%context pixmap)))
    (unless to-cr
      (warn "TO-CR = NIL! AIEEE!"))
    (with-device-coordinates (from-transform from-x from-y)
      (with-device-distances (from-transform width height)
	(DO-CAIRO-COPY-AREA FROM-CR FROM-X FROM-Y WIDTH HEIGHT TO-CR TO-X TO-Y :FUNCTION FUNCTION)))))


#||
define sealed method do-copy-area
    (from-medium :: <gtk-pixmap-medium>, from-x :: <integer>, from-y :: <integer>,
     width :: <integer>, height :: <integer>,
     to-medium :: <gtk-medium>, to-x :: <integer>, to-y :: <integer>,
     #key function = $boole-1) => ()
  let to-transform  = sheet-device-transform(medium-sheet(to-medium));
  let from-drawable = medium-drawable(from-medium);
  let to-drawable   = medium-drawable(to-medium);
  with-device-coordinates (to-transform, to-x, to-y)
    gdk-window-copy-area(to-drawable, gcontext, to-x, to-y,
			 from-drawable, from-x, from-y, width, height)
  end
end method do-copy-area;
||#

(defmethod do-copy-area ((from-medium <gtk-pixmap-medium>) (from-x integer) (from-y integer)
			 (width integer) (height integer)
			 (to-medium <gtk-medium>)          (to-x integer)   (to-y integer)
			 &key (function +boole-1+))
  (let ((to-transform (sheet-device-transform (medium-sheet to-medium)))
	(from-cr (medium-drawable from-medium)))
    (let ((to-cr (update-drawing-state to-medium)))
      (with-device-coordinates (to-transform to-x to-y)
	(DO-CAIRO-COPY-AREA FROM-CR FROM-X FROM-Y WIDTH HEIGHT TO-CR TO-X TO-Y :FUNCTION FUNCTION)))))


#||
define sealed method do-copy-area
    (pixmap :: <gtk-pixmap>, from-x :: <integer>, from-y :: <integer>,
     width :: <integer>, height :: <integer>,
     to-medium :: <gtk-medium>, to-x :: <integer>, to-y :: <integer>,
     #key function = $boole-1) => ()
  let to-transform  = sheet-device-transform(medium-sheet(to-medium));
  let from-drawable = pixmap.%pixmap;
  let to-drawable   = medium-drawable(to-medium);
  with-device-coordinates (to-transform, to-x, to-y)
    gdk-window-copy-area(to-drawable, gcontext, to-x, to-y,
			 from-drawable, from-x, from-y, width, height)
  end
end method do-copy-area;
||#

(defmethod do-copy-area ((pixmap <gtk-pixmap>)    (from-x integer) (from-y integer)
			 (width integer) (height integer)
			 (to-medium <gtk-medium>) (to-x integer)   (to-y integer)
			 &key (function +boole-1+))
  (let ((to-transform (sheet-device-transform (medium-sheet to-medium)))
	(from-cr (%context pixmap)))
    (let ((to-cr (update-drawing-state to-medium)))
      (with-device-coordinates (to-transform to-x to-y)
	(DO-CAIRO-COPY-AREA FROM-CR FROM-X FROM-Y WIDTH HEIGHT TO-CR TO-X TO-Y :FUNCTION FUNCTION)))))


#||
define sealed method do-copy-area
    (from-medium :: <gtk-pixmap-medium>, from-x :: <integer>, from-y :: <integer>,
     width :: <integer>, height :: <integer>,
     to-medium :: <gtk-pixmap-medium>, to-x :: <integer>, to-y :: <integer>,
     #key function = $boole-1) => ()
  let from-drawable = medium-drawable(from-medium);
  let to-drawable   = medium-drawable(to-medium);
  gdk-window-copy-area(to-drawable, gcontext, to-x, to-y,
		       from-drawable, from-x, from-y, width, height)
end method do-copy-area;
||#

(defmethod do-copy-area ((from-medium <gtk-pixmap-medium>) (from-x integer) (from-y integer)
			 (width integer) (height integer)
			 (to-medium <gtk-pixmap-medium>)   (to-x integer)   (to-y integer)
			 &key (function +boole-1+))
  (let* ((from-cr (medium-drawable from-medium))
	 (to-cr (update-drawing-state to-medium)))
    (DO-CAIRO-COPY-AREA FROM-CR FROM-X FROM-Y WIDTH HEIGHT TO-CR TO-X TO-Y :FUNCTION FUNCTION)))


#||
define sealed method do-copy-area
    (from-pixmap :: <gtk-pixmap>, from-x :: <integer>, from-y :: <integer>,
     width :: <integer>, height :: <integer>,
     to-pixmap :: <gtk-pixmap>, to-x :: <integer>, to-y :: <integer>,
     #key function = $boole-1) => ()
  let from-drawable = from-pixmap.%pixmap;
  let to-drawable   = to-pixmap.%pixmap;
  gdk-window-copy-area(to-drawable, gcontext, to-x, to-y,
		       from-drawable, from-x, from-y, width, height)
end method do-copy-area;
*/
||#

(defmethod do-copy-area ((from-pixmap <gtk-pixmap>) (from-x integer) (from-y integer)
			 (width integer) (height integer)
			 (to-pixmap <gtk-pixmap>)   (to-x integer)   (to-y integer)
			 &key (function +boole-1+))
  (let ((from-cr (%context from-pixmap))
	(to-cr   (%context to-pixmap)))
    ;; See duim-code;graphics;pixmaps.lisp:do-with-double-buffering
    ;; for some code that might be enlightening here.
    (DO-CAIRO-COPY-AREA FROM-CR FROM-X FROM-Y WIDTH HEIGHT TO-CR TO-X TO-Y :FUNCTION FUNCTION)))


;;; XXX: Something like this...
#-(and)
(defun pixmap-from-gdk-pixmap (gdk-pixmap)
  (cffi/with-stack-structure (width :int)
    (cffi/with-stack-structure (height :int)
      (cffi/gdk-pixmap-get-size gdk-pixmap width height)
      (let ((source-context (cffi/gdk-cairo-create gdk-pixmap))
	    (target-context (cffi/cairo-create-image-surface ARGB32)))
	(DO-CAIRO-COPY-AREA source-context 0 0 width height
			    target-context 0 0)
	(make-a-pixmap-from target-context)))))


;;; XXX: Something like this
(defun pixmap-to-gdk-pixmap (pixmap)
  (let* ((depth -1)
	 (gdk-pixmap     (cffi/gdk-pixmap-new nil (width pixmap) (height pixmap) depth))
	 (source-context (%context pixmap))
	 (target-context (cffi/gdk-cairo-create gdk-pixmap)))
    (DO-CAIRO-COPY-AREA source-context 0 0 (width pixmap) (height pixmap)
			target-context 0 0)
    gdk-pixmap))

;;; XXX 2017-11-12: should the back-end care about pixbuf? Just use whatever gtk provides...

(defun pixmap-to-gdk-pixbuf (pixmap)
  (declare (ignore pixmap))
  (gtk-debug "Implement me: pixmap-to-gdk-pixbuf"))
