;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GTK2-DUIM -*-
(in-package #:gtk2-duim)

#||
Module:       gtk-duim
Synopsis:     GTK font mapping implementation
Author:       Andy Armstrong, Scott McKay
Copyright:    Original Code is Copyright (c) 1995-2004 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND

/// GTK font management

define sealed class <gtk-font> (<object>)
  sealed slot %font-name :: <string>,
    required-init-keyword: name:;
  sealed slot %font-id :: false-or(<GdkFont*>) = #f;
  sealed slot %font-struct = #f;
end class <gtk-font>;

define sealed domain make (singleton(<gtk-font>));
define sealed domain initialize (<gtk-font>);
||#

(defclass <gtk-font> ()
  ((%font-name :type string :initarg :name :initform (required-slot ":name" "<gtk-font>") :accessor %font-name)
   (%pango-font-description :initarg :font-description :initform (required-slot ":font-description" "<gtk-font>")
			    :accessor %pango-font-description)
   ;;--- Maybe we should keep a reference to the DUIM text-style?
   (underline? :initarg :underline? :initform nil :accessor underline?)
   (strikeout? :initarg :strikeout? :initform nil :accessor strikeout?)))


#||
define abstract class <font-error> (<error>)
end class <font-error>;
||#

(define-condition <font-error> (error) ())


#||
define abstract class <font-name-parse-error> (<font-error>)
  sealed constant slot %font-name, required-init-keyword: name:;
end class <font-name-parse-error>;


define sealed class <font-name-private-font-registry> (<font-name-parse-error>)
end class <font-name-private-font-registry>;


define method condition-to-string
    (condition :: <font-name-private-font-registry>) => (string :: <string>)
  format-to-string("Font name %s is from a private registry",
		   condition.%font-name)
end method condition-to-string;


define sealed class <font-name-numeric-field-non-numeric> (<font-name-parse-error>)
  sealed constant slot %start, required-init-keyword: start:;
  sealed constant slot %end,   required-init-keyword: end:;
  sealed constant slot %token, required-init-keyword: token:;
end class <font-name-numeric-field-non-numeric>;


define method condition-to-string
    (condition :: <font-name-numeric-field-non-numeric>) => (string :: <string>)
  format-to-string("Font name %s should have had an integer in %d..%d while looking for %s)",
		   condition.%font-name, condition.%start, condition.%end, condition.%token)
end method condition-to-string;
||#

(define-condition <font-mapping-error> (<font-error>)
  ((text-style :initarg :text-style :accessor text-style)
   (component  :initarg :component  :accessor component))
  (:report (lambda (condition stream)
	     (format stream "Failed to map text-style ~a into Pango font description; failed on component: ~a"
		     (text-style condition)
		     (component condition))))
  (:documentation
"
Reported when the GTK back-end is unable to map a DUIM text style
into a Pango font description.
"))



#||
/// Font mapping

/*---*** Not used yet!
define constant $gtk-font-families :: <list>
  = #(#(#"fix",        "courier"),
      #(#"sans-serif", "helvetica"),
      #(#"serif",      "times", "charter", "new century schoolbook"),
      #(#"symbol",     "symbol"));
*/
||#

(defparameter *gtk-font-families*
  (list (cons :fix        "courier") ;;"Monospace")
	(cons :sans-serif "Sans")
	(cons :serif      "Serif")
	(cons :symbol     "Symbol")))


(defun %family-to-pango-family (family text-style)
"
Convert a DUIM family into a Pango family. _family_ should be one
of the recognised DUIM family names (:fix, :sans-serif, :serif, or
:symbol) or a font family name that Pango can deal with.
"
  (let ((mapped-family (or (cdr (assoc family *gtk-font-families*)) family)))
    (or mapped-family (error '<font-mapping-error> :text-style text-style :component "family"))))


(defparameter *gtk-text-style-weights*
  (list (cons :normal      +pango-weight-normal+)
	(cons :condensed   +pango-weight-ultralight+)
	(cons :thin        +pango-weight-light+)
	(cons :extra-light +pango-weight-ultralight+)
	(cons :light       +pango-weight-light+)
	(cons :medium      +pango-weight-normal+)
	(cons :demibold    +pango-weight-semibold+)
	(cons :bold        +pango-weight-bold+)
	(cons :extra-bold  +pango-weight-ultrabold+)
	(cons :black       +pango-weight-heavy+)))


(defun %weight-to-pango-weight (weight text-style)
  (let ((mapped-weight (or (cdr (assoc weight *gtk-text-style-weights*)) weight)))
    (or mapped-weight (error '<font-mapping-error> :text-style text-style :component "weight"))))


(defparameter *gtk-text-style-slants*
  (list (cons :roman   +pango-style-normal+)
	(cons :italic  +pango-style-italic+)
	(cons :oblique +pango-style-oblique+)))


(defun %slant-to-pango-slant (slant text-style)
  (let ((mapped-slant (or (cdr (assoc slant *gtk-text-style-slants*)) slant)))
    (or mapped-slant (error '<font-mapping-error> :text-style text-style :component "slant"))))


#||
//--- We should compute the numbers based on either device characteristics
//--- or some user option
define constant $gtk-logical-sizes :: <simple-object-vector>
    = #[#[#"normal",     10],	// put most common one first for efficiency
	#[#"small",       8],
	#[#"large",      12],
	#[#"very-small",  6],
	#[#"very-large", 14],
	#[#"tiny",        5],
	#[#"huge",       18]];
||#

(defparameter *gtk-logical-sizes*
  (list(cons :normal     10)
       (cons :small       8)
       (cons :large      12)
       (cons :very-small  6)
       (cons :very-large 14)
       (cons :tiny        5)
       (cons :huge       18)))


#||
/*---*** Not used yet!
define method install-default-text-style-mappings
    (_port :: <gtk-port>) => ()
  ignoring("install-default-text-style-mappings");
end method install-default-text-style-mappings;
      
define method can-install-entire-family?
    (_port :: <gtk-port>, duim-family, x-family :: <byte-string>)
 => (can-install? :: <boolean>)
  ignoring("can-install-entire-family?");
  #f
end method can-install-entire-family?;

define method scaleable-font-name-at-size
    (font-name :: <byte-string>, point-size :: <integer>,
     horiz-dpi :: <integer>, vertical-dpi :: <integer>)
 => (font-name :: <integer>)
  not-yet-implemented("scaleable-font-name-at-size")
end method scaleable-font-name-at-size;
*/

define sealed method do-text-style-mapping
    (_port :: <gtk-port>, text-style :: <standard-text-style>, character-set)
 => (font :: <gtk-font>)
  ignore(character-set);
  let text-style
    = standardize-text-style(_port, text-style,
			     character-set: character-set);
  let table :: <object-table> = port-font-mapping-table(_port);
  let font = gethash(table, text-style);
  font
    | begin
	ignoring("do-text-style-mapping");
	//---*** This is not right!
	make(<gtk-font>, name: "fake")
      end
end method do-text-style-mapping;
||#

;; FIXME: Need to add debug to this method, I don't think the caching of fonts is
;; working...
(defmethod do-text-style-mapping ((_port <gtk-port>) (text-style <standard-text-style>) character-set)
  (let* ((text-style (standardize-text-style _port text-style :character-set character-set))
	 (table (port-font-mapping-table _port))
	 (font  (gethash text-style table)))
    ;; If we got a <gtk-font> back, then good. Otherwise, make a new one
    ;; by parsing the text-style and asking Pango to make a description...
    (or font
	(setf (gethash text-style table)
	      (multiple-value-bind (family name weight slant size underline? strikeout?)
		  (text-style-components text-style)
		(declare (ignore name))
		(let ((font-description (cffi/pango-font-description-new)))
		  (cffi/pango-font-description-set-family  font-description (%family-to-pango-family family text-style))
		  (cffi/pango-font-description-set-weight  font-description (%weight-to-pango-weight weight text-style))
		  (cffi/pango-font-description-set-style   font-description (%slant-to-pango-slant slant text-style))
		  (cffi/pango-font-description-set-size    font-description (* size +pango-scale+))
		  (cffi/pango-font-description-set-stretch font-description +pango-stretch-normal+)
		  (cffi/pango-font-description-set-variant font-description +pango-variant-normal+)
		  (let ((name (cffi/pango-font-description-to-string font-description)))
		    (make-instance '<gtk-font>
				   :name name
				   :font-description font-description
				   :underline? underline?
				   :strikeout? strikeout?))))))))


#||
//--- This approach seems unnecessarily clumsy; we might as well just have 
//--- 'do-text-style-mapping' do the table lookup directly itself.  We shouldn't
//--- need to cons up a whole new text-style object just to map the size.
define sealed method standardize-text-style
    (_port :: <gtk-port>, text-style :: <standard-text-style>,
     #rest keys, #key character-set)
 => (text-style :: <text-style>)
  apply(standardize-text-style-size,
	_port, text-style, $gtk-logical-sizes, keys)
end method standardize-text-style;
||#

(defmethod standardize-text-style ((_port <gtk-port>)
				   (text-style <standard-text-style>)
				   &rest keys
				   &key character-set)
  (declare (ignore character-set))
  (apply #'standardize-text-style-size
	 _port
	 text-style
	 *gtk-logical-sizes*
	 keys))



#||
/// Font metrics

define sealed inline method font-width
    (text-style :: <text-style>, _port :: <gtk-port>,
     #rest keys, #key character-set)
 => (width :: <integer>)
  let (font, width, height, ascent, descent)
    = apply(font-metrics, text-style, _port, keys);
  ignore(font, height, ascent, descent);
  width
end method font-width;
||#

(defmethod font-width ((text-style <text-style>) (_port <gtk-port>) &rest keys &key character-set)
  (declare (ignore character-set))
  (multiple-value-bind (font width height ascent descent)
      (apply #'font-metrics text-style _port keys)
    (declare (ignore font height ascent descent))
    width))


#||
define sealed inline method font-height
    (text-style :: <text-style>, _port :: <gtk-port>,
     #rest keys, #key character-set)
 => (height :: <integer>)
  let (font, width, height, ascent, descent)
    = apply(font-metrics, text-style, _port, keys);
  ignore(font, width, ascent, descent);
  height
end method font-height;
||#

(defmethod font-height ((text-style <text-style>) (_port <gtk-port>) &rest keys &key character-set)
  (declare (ignore character-set))
  (multiple-value-bind (font width height ascent descent)
      (apply #'font-metrics text-style _port keys)
    (declare (ignore font width ascent descent))
    height))


#||
define sealed inline method font-ascent
    (text-style :: <text-style>, _port :: <gtk-port>,
     #rest keys, #key character-set)
 => (ascent :: <integer>)
  let (font, width, height, ascent, descent)
    = apply(font-metrics, text-style, _port, keys);
  ignore(font, width, height, descent);
  ascent
end method font-ascent;
||#

(defmethod font-ascent ((text-style <text-style>) (_port <gtk-port>) &rest keys &key character-set)
  (declare (ignore character-set))
  (multiple-value-bind (font width height ascent descent)
      (apply #'font-metrics text-style _port keys)
    (declare (ignore font width height descent))
    ascent))


#||
define sealed inline method font-descent
    (text-style :: <text-style>, _port :: <gtk-port>,
     #rest keys, #key character-set)
 => (descent :: <integer>)
  let (font, width, height, ascent, descent)
    = apply(font-metrics, text-style, _port, keys);
  ignore(font, width, height, ascent);
  descent
end method font-descent;
||#

(defmethod font-descent ((text-style <text-style>) (_port <gtk-port>) &rest keys &key character-set)
  (declare (ignore character-set))
  (multiple-value-bind (font width height ascent descent)
      (apply #'font-metrics text-style _port keys)
    (declare (ignore font width height ascent))
    descent))


#||
define sealed inline method fixed-width-font?
    (text-style :: <text-style>, _port :: <gtk-port>, #key character-set)
 => (fixed? :: <boolean>)
  ignoring("fixed-width-font?");
  #f
end method fixed-width-font?;
||#

(defmethod fixed-width-font? ((text-style <text-style>) (_port <gtk-port>) &key character-set)
  (declare (ignore character-set))
  (ignoring "fixed-width-font?")
  ;; use pango_font_family_is_monospace?
  #-(and)
  (if (= (pango-font-family-is-monospace (%family-to-pango-family (text-style-family text-style) text-style)) 0)
      nil
      t)
  nil)


#||
define sealed method font-metrics
    (text-style :: <text-style>, _port :: <gtk-port>,
     #rest keys, #key character-set)
 => (font,
     width :: <integer>, height :: <integer>, ascent :: <integer>, descent :: <integer>)
  let font :: <gtk-font>
    = apply(text-style-mapping, _port, text-style, keys);
  gtk-font-metrics(font, _port)
end method font-metrics;
||#

(defmethod font-metrics ((text-style <text-style>) (_port <gtk-port>) &rest keys &key character-set)
  (declare (ignore character-set))
  (let ((font (apply #'text-style-mapping _port text-style keys)))
    (gtk-font-metrics font)))


#||
define sealed method gtk-font-metrics
    (font :: <gtk-font>, _port :: <gtk-port>)
 => (font :: <gtk-font>,
     width :: <integer>, height :: <integer>, ascent :: <integer>, descent :: <integer>)
  ignoring("gtk-font-metrics");
  values(font, 100, 10, 8, 2)
end method gtk-font-metrics;
||#

(defgeneric gtk-font-metrics (font &key pango-context))


(defmethod gtk-font-metrics ((font <gtk-font>)
			     &key (pango-context nil))
  (let* ((context (or pango-context (cffi/gdk-pango-context-get)))  ;; use context for default screen if no other provided.
	 (pango-metrics (cffi/pango-context-get-metrics context
							(%pango-font-description font)
							nil)))
    (let* ((width   (round (cffi/pango-font-metrics-get-approximate-char-width pango-metrics) +pango-scale+))
	   (ascent  (round (cffi/pango-font-metrics-get-ascent pango-metrics) +pango-scale+))
	   (descent (round (cffi/pango-font-metrics-get-descent pango-metrics) +pango-scale+))
	   (height  (+ ascent descent)))
      (cffi/pango-font-metrics-unref pango-metrics)
      (values font width height ascent descent))))



#||
/// Text measurement

define sealed method text-size
    (_port :: <gtk-port>, char :: <character>,
     #key text-style :: <text-style> = $default-text-style,
          start: _start, end: _end, do-newlines? = #f, do-tabs? = #f)
 => (largest-x :: <integer>, largest-y :: <integer>,
     cursor-x :: <integer>, cursor-y :: <integer>, baseline :: <integer>)
  ignore(_start, _end, do-newlines?, do-tabs?);
  let string = make(<string>, size: 1, fill: char);
  text-size(_port, string, text-style: text-style)
end method text-size;
||#

(defmethod text-size ((_port <gtk-port>) (char character)
		      &key (text-style *default-text-style*)
		      ((:start _start)) ((:end _end)) (do-newlines? nil) (do-tabs? nil))
  (declare (ignore _start _end do-newlines? do-tabs?))
  (let ((string (make-string 1 :initial-element char)))
    (text-size _port string :text-style text-style)))


#||
//---*** What do we do about Unicode strings?
define sealed method text-size
    (_port :: <gtk-port>, string :: <string>,
     #key text-style :: <text-style> = $default-text-style,
          start: _start, end: _end, do-newlines? = #f, do-tabs? = #f)
 => (largest-x :: <integer>, largest-y :: <integer>,
     cursor-x :: <integer>, cursor-y :: <integer>, baseline :: <integer>)
  let length :: <integer> = size(string);
  let _start :: <integer> = _start | 0;
  let _end   :: <integer> = _end   | length;
  let (font :: <gtk-font>, width, height, ascent, descent)
    = font-metrics(text-style, _port);
  ignore(width, height);
  local method measure-string
	    (font :: <gtk-font>, string :: <string>,
	     _start :: <integer>, _end :: <integer>)
	 => (x1 :: <integer>, y1 :: <integer>, 
	     x2 :: <integer>, y2 :: <integer>)
	  ignoring("measure-string");
	  values(0, 0, 100, 10)
	end method measure-string;
  case
    do-tabs? & do-newlines? =>
      next-method();		// the slow case...
    do-tabs? =>
      let tab-width :: <integer> = width * 8;
      let last-x    :: <integer> = 0;
      let last-y    :: <integer> = 0;
      let s         :: <integer> = _start;
      block (return)
	while (#t)
	  let e = position(string, '\t', start: s, end: _end) | _end;
	  let (x1, y1, x2, y2) = measure-string(font, string, s, e);
	  ignore(x1);
	  if (e = _end)
	    last-x := last-x + x2
	  else
	    last-x := floor/(last-x + x2 + tab-width, tab-width) * tab-width;
	  end;
	  max!(last-y, y2 - y1);
	  s := min(e + 1, _end);
	  when (e = _end)
	    return(last-x, last-y, last-x, last-y, ascent)
	  end
	end
      end;
    do-newlines? =>
      let largest-x :: <integer> = 0;
      let largest-y :: <integer> = 0;
      let last-x    :: <integer> = 0;
      let last-y    :: <integer> = 0;
      let s         :: <integer> = _start;
      block (return)
	while (#t)
	  let e = position(string, '\n', start: s, end: _end) | _end;
	  let (x1, y1, x2, y2) = measure-string(font, string, s, e);
	  ignore(x1);
	  max!(largest-x, x2);
	  last-x := x2;
	  inc!(largest-y, y2 - y1);
	  last-y := y2;
	  s := min(e + 1, _end);
	  when (e = _end)
	    return(largest-x, largest-y, last-x, last-y, ascent)
	  end
	end
      end;
    otherwise =>
      let (x1, y1, x2, y2) = measure-string(font, string, _start, _end);
      ignore(x1);
      values(x2, y2 - y1, x2, y2 - y1, ascent);
  end
end method text-size;
||#

;; Common method for measuring text

(defun %measure-text (pango-layout text)
"
 Returns (values x y width height baseline) of _text_ when rendered using
 _pango-layout_.
"
  ;; => x y width height baseline
  (cffi/pango-layout-set-text pango-layout text -1)
  (cffi/with-stack-structure (layout-rect '(:struct PangoRectangle))
    ;; Returns extents in device units
    ;; XXX: I think this description (from the man page) is not right. Origin
    ;; seems to always be 0,0...
    ;; ORIGIN of rect = baseline
    ;; ascent = (- rect->y)
    ;; descent = rect->y + rect->height
    ;; 'lbearing' = rect->x
    ;; 'rbearing' = rect->x + rect->width [largest-x]
    (cffi/pango-layout-get-pixel-extents pango-layout nil layout-rect)
    (let* ((x        (cffi/pangorectangle->x layout-rect))
	   (y        (cffi/pangorectangle->y layout-rect))
	   (width    (cffi/pangorectangle->width layout-rect))
	   (height   (cffi/pangorectangle->height layout-rect))
	   (baseline (round (cffi/pango_layout_get_baseline pango-layout) +PANGO-SCALE+)))
      ;;---*** do-newlines? + do-tabs? are ignored completely. Does that matter?
      (values x y width height baseline))))


;; Gives the size of axis-aligned text. The bounding box for rotated text can be
;; calculated by transforming these extents, if needed.

(defmethod text-size ((_port <gtk-port>) (string string)
		      &key (text-style *default-text-style*)
		      ((:start _start)) ((:end _end))
			(do-newlines? nil) (do-tabs? nil))
  (declare (ignore do-newlines? do-tabs?))
  (let* ((length (size string))
	 (_start (or _start 0))
	 (_end (or _end length))
	 (pango-descr (if (eql text-style *default-text-style*)
			  ;; FIXME: MAYBE THIS SHOULD BE A <GTK-FONT> RATHER THAN A PANGO-FONT-DESC?
			  (or (%default-pango-text-description _port)
			      (setf (%default-pango-text-description _port)
				    (%pango-font-description (do-text-style-mapping _port text-style nil))))
			  (%pango-font-description (do-text-style-mapping _port text-style nil))))
	 (pango-context (or (%default-pango-context _port)
			    (setf (%default-pango-context _port)
				  (cffi/gdk-pango-context-get))))           ; Gets default context since we have nothing else
	 (pango-layout  (or (%default-pango-layout _port)
			    (setf (%default-pango-layout _port)
				  (cffi/pango-layout-new pango-context))))) ; Can these be cached? They could go in the port...
    ;; We might be measuring text in a font that is provided, rather than being the
    ;; port's default pango-description - so set this each time through.
    (cffi/pango-layout-set-font-description pango-layout pango-descr)
    (multiple-value-bind (x y width height baseline)
	(%measure-text pango-layout (subseq string _start _end))
      (declare (ignore x y))
      ;;---*** do-newlines? + do-tabs? are ignored completely. Does that matter?
      (values width height width height baseline))))

