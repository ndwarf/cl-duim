;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GTK2-DUIM -*-
(in-package #:gtk2-duim)

#||
Module:       gtk-duim
Synopsis:     GTK keyboard mapping implementation
Author:       Andy Armstrong, Scott McKay
Copyright:    Original Code is Copyright (c) 1995-2004 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND

/// GTK keyboard handling
||#

(defgeneric handle-gtk-key-press-event-signal (sheet widget event user-data))
(defgeneric handle-gtk-key-release-event-signal (sheet widget event user-data))
(defgeneric handle-gtk-set-focus-signal (sheet widget-receiver widget-target user-data))
(defgeneric handle-gtk-key-event (sheet widget event))

#||
define method handle-gtk-key-press-event
    (sheet :: <sheet>, widget :: <GtkWidget*>, event :: <GdkEventKey*>)
 => (handled? :: <boolean>)
  handle-gtk-key-event(sheet, widget, event)
end method handle-gtk-key-press-event;
||#


(defmethod handle-gtk-key-press-event-signal ((sheet <sheet>)
					      widget ;<GtkWidget*>)
					      event ;<GdkEventKey*>))
					      user-data)
  (declare (ignore user-data))
  (handle-gtk-key-event sheet widget event))

#||
define method handle-gtk-key-release-event
    (sheet :: <sheet>, widget :: <GtkWidget*>, event :: <GdkEventKey*>)
 => (handled? :: <boolean>)
  handle-gtk-key-event(sheet, widget, event)
end method handle-gtk-key-release-event;
||#


(defmethod handle-gtk-key-release-event-signal ((sheet <sheet>)
						widget ;<GtkWidget*>)
						event ;<GdkEventKey*>))
						user-data)
  (declare (ignore user-data))
  (handle-gtk-key-event sheet widget event))


#||
define method handle-gtk-key-event
    (sheet :: <sheet>, widget :: <GtkWidget*>, event :: <GdkEventKey*>)
 => (handled? :: <boolean>)
  ignore(widget);
  let _port = port(sheet);
  when (_port)
    ignoring("handle-key-event");
    #f
    /*
    let keycode   = event.x/keycode-value;
    let state     = event.x/state-value;
    let modifiers = gtk-state->duim-state(_port, logand(state, $key-event-modifier-mask));
    let (keysym-modifiers, keysym)
      = xt/XtTranslateKeycode(_port.%display, keycode, state);
    let char   = x-keysym->character(keysym, logand(state, $key-event-modifier-mask));
    let keysym = x-keysym->keysym(keysym);
    port-modifier-state(_port) := modifiers;
    distribute-event(_port,
		     make(event-class,
			  sheet:     sheet,
			  key-name:  keysym,
			  character: char,
			  modifier-state: modifiers));
    #t
    */
  end
end method handle-gtk-key-event;
||#


(defmethod handle-gtk-key-event ((sheet <sheet>)
				 widget ;<GtkWidget*>)
				 event) ;<GdkEventKey*>))
  ;; RUN_LAST
  (declare (ignore widget))
  "GTK generates a key event; this method handles it.

Generates the key-name, character, and modifier-state for a DUIM event
from a GTK event."
  (let ((_port (port sheet)))
    (when _port
      (let* ((%keyval   (cffi/gdkeventkey->keyval event))
	     (%state    (cffi/gdkeventkey->state event))
	     (%event-type  (cffi/gdkeventkey->type event))
	     (modifiers (gtk-state->duim-modifiers %state))
	     (event-class (class-for-gdk-event-key-type %event-type)))
	(setf (port-modifier-state _port) modifiers)
	(distribute-event _port
			  (make-instance event-class
					 :sheet          sheet
					 :key-name       (gdk-keyval->duim-keysym %keyval)
					 :character      (gtk-keyval->character %keyval)
					 :modifier-state modifiers))
	t))))


(defun class-for-gdk-event-key-type (type)
  (cond ((equal? type +CFFI/GDK-KEY-PRESS+)
	 (find-class '<key-press-event>))
	((equal? type +CFFI/GDK-KEY-RELEASE+)
	 (find-class '<key-release-event>))
	(t
	 (error "Unrecognised GDKEVENTKEY->TYPE ~a" type))))

#||
/*---*** No keyboard handling yet!
define constant $x-keysym->character-table :: <object-table> = make(<table>);

define constant $x-keysym->keysym-table    :: <object-table> = make(<table>);
||#

(defparameter *keyval->keysym-lookup-table* (make-hash-table :test #'eql))

#||
define function initialize-character-table () => ()
  //--- Fill some stuff in here
end function initialize-character-table;

define function initialize-keysym-table () => ()
  let table :: <object-table> = $x-keysym->keysym-table;
  // Fill in the alphabetic characters 
 for (code :: <integer> from as(<integer>, 'A') to as(<integer>, 'Z'))
    let keysym = make(<byte-string>, size: 1, fill: as(<character>, code));
    table[code]  := as(<symbol>, keysym)
  end;
  for (code :: <integer> from as(<integer>, 'a') to as(<integer>, 'z'))
    let keysym = make(<byte-string>, size: 1, fill: as(<character>, code));
    table[code]  := as(<symbol>, keysym)
  end;
  // Fill in the digit characters
  for (code :: <integer> from as(<integer>, '0') to as(<integer>, '9'))
    let keysym = make(<byte-string>, size: 1, fill: as(<character>, code));
    table[code]  := as(<symbol>, keysym)
  end;
  // Fill in the symbols
  table[x/$XK-parenleft]    := #"(";
  table[x/$XK-slash]        := #"/";
  table[x/$XK-ampersand]    := #"&";
  table[x/$XK-bracketleft]  := #"[";
  table[x/$XK-numbersign]   := #"#";
  table[x/$XK-percent]      := #"%";
  table[x/$XK-braceleft]    := #"{";
  table[x/$XK-bar]          := #"|";
  table[x/$XK-at]           := #"@";
  table[x/$XK-exclam]       := #"!";
  table[x/$XK-parenright]   := #")";
  table[x/$XK-minus]        := #"-";
  table[x/$XK-underscore]   := #"_";
  table[x/$XK-asciicircum]  := #"^";
  table[x/$XK-quotedbl]     := #"\"";
  table[x/$XK-apostrophe]   := #"'";
  table[x/$XK-grave]        := #"`";
  table[x/$XK-backslash]    := #"\\";
  table[x/$XK-plus]         := #"+";
  table[x/$XK-less]         := #"<";
  table[x/$XK-colon]        := #":";
  table[x/$XK-asterisk]     := #"*";
  table[x/$XK-bracketright] := #"]";
  table[x/$XK-space]        := #"space";
  table[x/$XK-semicolon]    := #";";
  table[x/$XK-greater]      := #">";
  table[x/$XK-question]     := #"?";
  table[x/$XK-asciitilde]   := #"~";
  table[x/$XK-comma]        := #",";
  table[x/$XK-period]       := #".";
  table[x/$XK-equal]        := #"=";
  table[x/$XK-dollar]       := #"$";
  table[x/$XK-return]       := #"return";
  table[x/$XK-delete]       := #"rubout";
  // table[x/$XK-osfDelete] := #"rubout";
  table[x/$XK-backspace]    := #"backspace";
  table[x/$XK-tab]          := #"tab";
  table[x/$XK-linefeed]     := #"linefeed";
  table[x/$XK-escape]       := #"escape";
end function initialize-keysym-table;
||#


(defun initialize-keysym-table ()
  "Populate keysym table with standard DUIM keysyms."
  (let ((table *keyval->keysym-lookup-table*))
    (flet ((char-to-symbol (char)
	     (intern (make-string 1 :initial-element char) :keyword)))
      ;; Fill in the alphabetic characters
      (loop for codepoint from (char-code #\A) to (char-code #\Z)
	 do (let ((keysym (char-to-symbol (code-char codepoint))))
	      (setf (gethash codepoint table) keysym)))
      (loop for codepoint from (char-code #\a) to (char-code #\z)
	 do (let ((keysym (char-to-symbol (code-char codepoint))))
	      (setf (gethash codepoint table) keysym)))
      ;; Fill in the digit characters
      (loop for codepoint from (char-code #\0) to (char-code #\9)
	 do (let ((keysym (char-to-symbol (code-char codepoint))))
	      (setf (gethash codepoint table) keysym)))
      ;; Fill in the symbols; see "gdkkeysyms.h"
      (setf (gethash #x0020 table) :space
	    (gethash #x0021 table) :!
	    (gethash #x0022 table) (char-to-symbol #\" )
	    (gethash #x0023 table) (char-to-symbol #\# )
	    (gethash #x0024 table) :$
	    (gethash #x0025 table) :%
	    (gethash #x0026 table) :&
	    (gethash #x0027 table) (char-to-symbol #\' )
	    (gethash #x0028 table) (char-to-symbol #\( )
	    (gethash #x0029 table) (char-to-symbol #\) )
	    (gethash #x002a table) :*
	    (gethash #x002b table) :+
	    (gethash #x002c table) (char-to-symbol #\, )
	    (gethash #x002d table) :-
	    (gethash #x002e table) (char-to-symbol #\. )
	    (gethash #x002f table) :/
	    (gethash #x003a table) (char-to-symbol #\: )
	    (gethash #x003b table) (char-to-symbol #\; )
	    (gethash #x003c table) :<
	    (gethash #x003d table) :=
	    (gethash #x003e table) :>
	    (gethash #x003f table) :?
	    (gethash #x0040 table) :@
	    (gethash #x005b table) :[
	    (gethash #x005c table) (char-to-symbol #\\ )
	    (gethash #x005d table) :]
	    (gethash #x005e table) :^
	    (gethash #x005f table) :_
	    (gethash #x0060 table) (char-to-symbol #\` )
	    (gethash #x007b table) :{
	    (gethash #x007c table) (char-to-symbol #\| )
	    (gethash #x007d table) :}
	    (gethash #x007e table) :~

	    (gethash #xff08 table) :backspace
	    (gethash #xff09 table) :tab
	    (gethash #xff0a table) :linefeed
	    (gethash #xff0b table) :clear
	    (gethash #xff0d table) :return
	    (gethash #xff1b table) :escape
	    (gethash #xff50 table) :home
	    (gethash #xff51 table) :left
	    (gethash #xff52 table) :up
	    (gethash #xff53 table) :right
	    (gethash #xff54 table) :down
	    (gethash #xff55 table) :prior    ; page-up
	    (gethash #xff56 table) :next     ; page-down
	    (gethash #xff57 table) :end
	    (gethash #xff58 table) :begin
	    (gethash #xff60 table) :select
	    (gethash #xff61 table) :print
	    (gethash #xff62 table) :execute
	    (gethash #xff63 table) :insert
	    (gethash #xff65 table) :undo
	    (gethash #xff66 table) :redo
	    (gethash #xff67 table) :menu
	    (gethash #xff68 table) :find
	    (gethash #xff69 table) :cancel
	    (gethash #xff6a table) :help
	    (gethash #xff6b table) :break
	    (gethash #xff7f table) :numlock
	    (gethash #xffaa table) :multiply   ; numpad *
	    (gethash #xffab table) :add        ; numpad +
	    (gethash #xffac table) :separator
	    (gethash #xffad table) :subtract   ; numpad -
	    (gethash #xffae table) :decimal    ; numpad .
	    (gethash #xffaf table) :divide     ; numpad /
	    (gethash #xffb0 table) :numpad0
	    (gethash #xffb1 table) :numpad1
	    (gethash #xffb2 table) :numpad2
	    (gethash #xffb3 table) :numpad3
	    (gethash #xffb4 table) :numpad4
	    (gethash #xffb5 table) :numpad5
	    (gethash #xffb6 table) :numpad6
	    (gethash #xffb7 table) :numpad7
	    (gethash #xffb8 table) :numpad8
	    (gethash #xffb9 table) :numpad9
	    (gethash #xffbe table) :f1
	    (gethash #xffbf table) :f2
	    (gethash #xffc0 table) :f3
	    (gethash #xffc1 table) :f4
	    (gethash #xffc2 table) :f5
	    (gethash #xffc3 table) :f6
	    (gethash #xffc4 table) :f7
	    (gethash #xffc5 table) :f8
	    (gethash #xffc6 table) :f9
	    (gethash #xffc7 table) :f10
	    (gethash #xffc8 table) :f11
	    (gethash #xffc9 table) :f12
	    (gethash #xffca table) :f13
	    (gethash #xffcb table) :f14
	    (gethash #xffcc table) :f15
	    (gethash #xffcd table) :f16
	    (gethash #xffce table) :f17
	    (gethash #xffcf table) :f18
	    (gethash #xffd0 table) :f19
	    (gethash #xffd1 table) :f20
	    (gethash #xffd2 table) :f21
	    (gethash #xffd3 table) :f22
	    (gethash #xffd4 table) :f23
	    (gethash #xffd5 table) :f24
	    (gethash #xffff table) :rubout))))


#||
begin
  initialize-character-table();
  initialize-keysym-table();
end;
||#

(initialize-keysym-table)

#||
define constant $keysym-will-not-produce-character-mask :: <integer>
  = x/translate-to-modifiers-mask(#"mod1", #"mod2", #"mod3", #"mod4", #"mod5", #"control");

define function x-keysym->character
    (keysym, modifiers :: <integer>) => (char :: false-or(<character>))
  when (zero?(logand(modifiers, $keysym-will-not-produce-character-mask)))
    let value = x/KeysymValue(keysym);
    let char  = if (value >= 32 & value <= 126)
		  as(<byte-character>, value)
		else
		  gethash($x-keysym->character-table, value)
		end;
    char
  end
end function x-keysym->character;
||#

(defun gtk-keyval->character (keyval)
  (let ((codepoint (cffi/gdk-keyval-to-unicode keyval)))   ; returns 0 when no corresponding character
    (cond ((= 0 codepoint) nil)
	  ((< codepoint char-code-limit) (code-char codepoint))
	  (t nil))))

#||
define function x-keysym->keysym
    (keysym) => (keysym)
  let value = x/KeysymValue(keysym);
  gethash($x-keysym->keysym-table, value)
end function x-keysym->keysym;
||#

(defun gdk-keyval->duim-keysym (keyval)
  (let ((keysym (gethash keyval *keyval->keysym-lookup-table* nil)))
    (unless keysym
      (let ((gdk-name (cffi/gdk-keyval-name keyval)))
	;; Not canonicalising the keyval names returned by GTK means
	;; some of the keysyms will be rather funky
	;; (i.e. ":|GDK_ISO_Lock|"), but at least it will be
	;; reversible.
	(setf keysym (intern (string-upcase gdk-name) :keyword))))
    keysym))


(defun duim-keysym->gdk-keyval (keysym)
  (loop for value being the hash-values in *keyval->keysym-lookup-table*
     using (hash-key hashkey)
     do (when (eql value keysym)
	  (return-from duim-keysym->gdk-keyval hashkey)))
  (let ((name (symbol-name keysym)))
    (let ((keyval (cffi/gdk-keyval-from-name name)))
      (if (= keyval #xffffff)   ; "GDK_VoidSymbol"
	  (error "failed to map keysym ~s into system keyval" keysym)
	  keyval))))

#||

/// Modifer maps

define function initialize-modifier-map
    (x-display :: x/<Display>) => (modifier-map :: <simple-object-vector>)
  let map :: <simple-object-vector> = make(<vector>, size: 256, fill: 0);
  let shifts   = vector(x/$XK-Shift-R,   x/$XK-Shift-L);
  let controls = vector(x/$XK-Control-R, x/$XK-Control-L);
  let metas    = vector(x/$XK-Meta-R,    x/$XK-Meta-L, x/$XK-Alt-R, x/$XK-Alt-L);
  let supers   = vector(x/$XK-Super-R,   x/$XK-Super-L);
  let hypers   = vector(x/$XK-Hyper-R,   x/$XK-Hyper-L);
  //---*** FINISH THIS
  map
end function initialize-modifier-map;

define function x-state->duim-state
    (_port :: <gtk-port>, state :: <integer>) => (duim-state :: <integer>)
  _port.%modifier-map[logand(state, #o377)]
end function x-state->duim-state;
*/
||#

(defun duim-modifiers->gtk-state (duim-modifiers)
  (let ((shift?   (= (logand duim-modifiers +shift-key+)   +shift-key+))
	(control? (= (logand duim-modifiers +control-key+) +control-key+))
	(alt?     (= (logand duim-modifiers +alt-key+)     +alt-key+))
	(meta?    (= (logand duim-modifiers +meta-key+)    +meta-key+))
	(super?   (= (logand duim-modifiers +super-key+)   +super-key+))
	(hyper?   (= (logand duim-modifiers +hyper-key+)   +hyper-key+)))
    (logior (if shift?   +CFFI/GDK-SHIFT-MASK+   0)
	    (if control? +CFFI/GDK-CONTROL-MASK+ 0)
	    (if alt?     +CFFI/GDK-MOD1-MASK+    0)
	    (if meta?    +CFFI/GDK-META-MASK+    0)
	    (if super?   +CFFI/GDK-SUPER-MASK+   0)
	    (if hyper?   +CFFI/GDK-HYPER-MASK+   0))))


(defun gtk-state->duim-modifiers (gtk-modifier-state)
  (let ((shift?   (= (logand gtk-modifier-state +CFFI/GDK-SHIFT-MASK+)   +CFFI/GDK-SHIFT-MASK+))
        (control? (= (logand gtk-modifier-state +CFFI/GDK-CONTROL-MASK+) +CFFI/GDK-CONTROL-MASK+))
        (alt?     (= (logand gtk-modifier-state +CFFI/GDK-MOD1-MASK+)    +CFFI/GDK-MOD1-MASK+))
        (meta?    (= (logand gtk-modifier-state +CFFI/GDK-META-MASK+)    +CFFI/GDK-META-MASK+))
        (super?   (= (logand gtk-modifier-state +CFFI/GDK-SUPER-MASK+)   +CFFI/GDK-SUPER-MASK+))
        (hyper?   (= (logand gtk-modifier-state +CFFI/GDK-HYPER-MASK+)   +CFFI/GDK-HYPER-MASK+)))
    (logior (if shift?   +shift-key+   0)
	    (if control? +control-key+ 0)
	    (if alt?     +alt-key+     0)
            (if meta?    +meta-key+    0)
	    (if super?   +super-key+   0)
	    (if hyper?   +hyper-key+   0))))

