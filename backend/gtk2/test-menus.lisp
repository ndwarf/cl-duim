;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GTK2-DUIM -*-
(in-package #:gtk2-duim)

;;; CL version of the tutorial found at:
;;;
;;; https://developer.gnome.org/gtk-tutorial/stable/x1577.html


(cffi:defcallback menuitem_response :void ((user-data :pointer)
					   (widget :pointer))
  ;; because this is hooked up using g_signal_connect_swapped,
  ;; the GADGET and USER-DATA arguments are out of order.
  ;; Gadget is the MENU_ITEMS that was clicked on.
  (format t "~%USER-DATA ~a / WIDGET ~a"
	  (cffi:foreign-string-to-lisp user-data)
	  widget))


;;; GtkWidget::event [gboolean f(GtkWidget*, GdkEvent*, gpointer)]
(cffi:defcallback button_press :boolean ((user-data :pointer)
					 (event :pointer)
					 (widget :pointer))
  ;; another swapped handler. user-data is the menu
  (if (eql (cffi/gdkeventbutton->type event) +CFFI/GDK-BUTTON-PRESS+)
      (progn
	;; event is a GdkEventButton
	(cffi/gtk-menu-popup user-data nil nil nil nil
			     (cffi/gdkeventbutton->button event)
			     (cffi/gdkeventbutton->time event))

	(FORMAT T "~%DONE GONE POPPED UP A POPUP")
	;; This ^^^ is seen whilst the menu is up, so menu-popup
	;; does not block
	
	;; Tell calling code the event has been dealt with
	t)
      ;; Tell calling code to pass the event on, it's not dealt with
      nil))


(defun main ()

  (cffi/initialize-gtk)

  ;; Create the window
  (let ((window (cffi/gtk-window-new (cffi:foreign-enum-value 'GtkWindowType :+CFFI/GTK-WINDOW-TOPLEVEL+))))

    (FORMAT T "~%WINDOW = ~A" WINDOW)
    
    (cffi/gtk-widget-set-size-request window 200 100)
    (cffi/gtk-window-set-title window "GTK Menu Test")
    ;; Call gtk_main_quit when the window receives the "delete-event"
    ;; GtkWidget::delete-event [gboolean f(GtkWidget*, GdkEvent*, gpointer)]
    (cffi/g-signal-connect window "delete-event"
			   (cffi:foreign-symbol-pointer "gtk_main_quit")
			   (cffi:null-pointer))

    ;; Init the menu widget and remember -- never gtk_show_widget the
    ;; menu widget!
    ;; This is the menu that holds the menu items, the one that will
    ;; pop up when "Root Menu" in the app is clicked
    (let ((menu (cffi/gtk-menu-new)))

      (FORMAT T "~%MENU = ~A" MENU)

      ;; Make a few menu entries for "menu". Note the call to
      ;; gtk_menu_shell_append. Here items are being added to
      ;; the menu. Normally the "clicked" signal on each of the menu
      ;; items would have a callback set up for it, but it's omitted
      ;; here to save space.
      
      (loop for i from 0 to 3

	 ;; create a menu item with a specific name
	   
	 do (let* ((name (format nil "Test-undermenu - ~d" i))
		   (menu_items (cffi/gtk-menu-item-new-with-label name)))

	      (FORMAT T "~%MENU_ITEMS = ~A" MENU_ITEMS)
	      
	      ;; add it to the menu
	      (cffi/gtk-menu-shell-append menu menu_items)

	      ;; do something interesting when the menuitem is selected
	      ;; GtkMenuItem::activate [void f(GtkMenuItem*, gpointer)]
	      (cffi/g-signal-connect-swapped menu_items "activate"
					     (cffi:callback menuitem_response)
					     (cffi:foreign-string-alloc name))

	      ;; show the widget
	      (cffi/gtk-widget-show menu_items)))

      ;; FIXME: the above leaks memory; the string is never freed.
      ;;        check the rest of the back-end for such things also.

      ;; this is the root menu and will be the label displayed on
      ;; the menu bar. There won't be a signal handler attached, as
      ;; it only pops up the rest of the menu when pressed

      (let ((root_menu (cffi/gtk-menu-item-new-with-label "Root Menu")))

	(FORMAT T "~%ROOT_MENU = ~A" ROOT_MENU)
	
	(cffi/gtk-widget-show root_menu)

	;; specify that "menu" is the menu for "root_menu"

	(cffi/gtk-menu-item-set-submenu root_menu menu)

	;; a vbox to put a menu and a button in

	(let ((vbox (cffi/gtk-vbox-new)))

	  (FORMAT T "~%VBOX = ~A" VBOX)
	  
	  (cffi/gtk-container-add window vbox)
	  (cffi/gtk-widget-show vbox)

	  ;; create a menu bar to hold the menus and add it to the
	  ;; main window

	  (let ((menu_bar (cffi/gtk-menu-bar-new)))

	    (FORMAT T "~%MENU_BAR = ~A" MENU_BAR)
	    
	    (cffi/gtk-box-pack-start vbox menu_bar nil nil 2)
	    (cffi/gtk-widget-show menu_bar)

	    ;; create a button to which to attach menu as a popup
	    (let ((button (cffi/gtk-button-new-with-label "press me")))
	      ;; GtkWidget::event [gboolean f(GtkWidget*, GdkEvent*, gpointer)]
	      (cffi/g-signal-connect-swapped button "event"
					     (cffi:callback button_press)
					     menu)
	      (cffi/gtk-box-pack-end vbox button t t 2)
	      (cffi/gtk-widget-show button)

	      ;; finally append the menu item to the menu bar -- this is the
	      ;; "root" menu item
	      (cffi/gtk-menu-shell-append menu_bar root_menu)

	      ;; always display the window as the last step
	      (cffi/gtk-widget-show window)

	      (cffi/gtk-main)

	      (FORMAT T "~%EEE XXX III TTT !!!~&"))))))))
	      
(main)
