;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GTK2-DUIM -*-
(in-package #:gtk2-duim)


;;; from top.lisp
(defclass <gtk-top-level-mirror> (<gtk-widget-mirror>)
  ((%dialog-mirrors :type array :initform (make-array 0 :adjustable t :fill-pointer t) :accessor %dialog-mirrors)
   (%accelerator-table :initform nil :reader %accelerator-table :writer %accelerator-table-setter)))


;;; from dialogs.lisp
(defclass <gtk-dialog-mirror> (<gtk-top-level-mirror>)
  ((%owner :type (or null <frame>)
	   :initarg :owner
	   :initform (required-slot ":owner" "<gtk-dialog-mirror>")
	   :reader %owner)))


;;; from gtk-pixmaps.lisp
(defclass <gtk-image> ()
  ((%data      :initarg :data      :accessor %data)    ;; Image data in some cairo_format_t format
   (%format    :initarg :format    :accessor %format)  ;; cairo_format_t indicating %data format
   (%width     :initarg :width     :accessor %width  :type int)
   (%height    :initarg :height    :accessor %height :type int)
   (%stride    :initarg :stride    :accessor %stride :type int)  ;; %data row-stride
   (%transform :initarg :transform :accessor %transform)))       ;; transform from duim <image>

