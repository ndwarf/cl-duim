;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GTK2-DUIM -*-
(in-package #:gtk2-duim)

#||
Module:       gtk-duim
Synopsis:     GTK debugging functionality
Author:       Andy Armstrong, Scott McKay
Copyright:    Original Code is Copyright (c) 1995-2004 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND

*debug-duim-function* := debug-message;
||#

(setf *debug-duim-function* #'format)

(defun dump-sheet-hierarchy (sheet &key (indent ""))
  ;; This method writes the sheet hierarchy of SHEET to stdout.
  ;; Can we get the extents of the mirror too? That seems to be the bit that's going
  ;; wrong... unfortunately MIRROR-EDGES just returns some random values :(
  (let* ((mirror (sheet-direct-mirror sheet))
	 (widget (and mirror (mirror-widget mirror)))
#-(and)	 (allocation (and widget (cffi/gtk_widget_get_allocation widget)))
#-(and)	 (alloc-x (and allocation (cffi/gtkallocation->x allocation)))
#-(and)	 (alloc-y (and allocation (cffi/gtkallocation->y allocation)))
#-(and)	 (alloc-w (and allocation (cffi/gtkallocation->width allocation)))
#-(and)	 (alloc-h (and allocation (cffi/gtkallocation->height allocation))))
    ;; How big does the widget think it is?
    (multiple-value-bind (ww wh)
	(if widget
	    (widget-size widget)
	    (values -1 -1))
      ;; Where does duim think the sheet is positioned?
      (multiple-value-bind (x y)
	  (sheet-position sheet)
	;; How big does duim think the sheet is?
	(multiple-value-bind (width height)
	    (sheet-size sheet)
	  ;; This only really makes sense when the mirror-transform is +identity-transform+.
	  (let ((msg (format nil "[HIER] ~a ~a (~dx~d @ ~d, ~d with mirror ~a [widget size=~dx~d]) ~a"
			     indent sheet width height x y (or mirror "UNMIRRORED")
			     ww wh (and mirror (%native-tk-window mirror)))))
	    (gtk-debug msg))))))
  (when (> (length (sheet-children sheet)) 0)
    (map nil #'(lambda (child)
		 (dump-sheet-hierarchy child :indent (format nil "~a   " indent)))
	 (sheet-children sheet))))


(defun duim-mem-debug (msg object)
  (let ((ref-count (cffi/gobject->ref-count object))
#-(and)	(floating-ref? (cffi/g-object-is-floating object)))
    (duim-debug-message "~a with ref-count ~d"  ;;" (floating? = ~a)"
			msg ref-count
#-(and)			(if (eql floating-ref? +true+) "true" "false"))))


(defun deprecated (msg)
  (duim-debug-message "DEPRECATED: ~a" msg))


(defun dialog-gtk-result-to-text (result)
"
Converts the response codes from the ``standard gtk dialogs'' to something
understandable by the average human.
"
  (ecase result
    (-1  "+GTK-RESPONSE-NONE+")
    (-2  "+GTK-RESPONSE-REJECT+")
    (-3  "+GTK-RESPONSE-ACCEPT+")
    (-4  "+GTK-RESPONSE-DELETE-EVENT+")
    (-5  "+GTK-RESPONSE-OK+")
    (-6  "+GTK-RESPONSE-CANCEL+")
    (-7  "+GTK-RESPONSE-CLOSE+")
    (-8  "+GTK-RESPONSE-YES+")
    (-9  "+GTK-RESPONSE-NO+")
    (-10 "+GTK-RESPONSE-APPLY+")
    (-11 "+GTK-RESPONSE-HELP+")))


;; These are *all* the "note-xxx" methods I found in the codebase. I put before methods on them
;; so we can see when they are invoked. In particular we want to see the order of the "note-attached"
;; and "note-mapped" notifications.

;; [there's some interesting comments on parallelograms/ellipses in extended-geometry;classes.lisp]


#||

;;;-- the following is interesting, but very spammy. Really want to be able to trickle
;;;-- these outputs to different streams or something.

(defmethod NOTE-COMMAND-ENABLED :before (FRAME-MANAGER FRAME COMMAND)
  (gtk-debug "NOTE-COMMAND-ENABLED ~a ~a ~a" frame-manager frame command))

(defmethod NOTE-COMMAND-DISABLED :before (FRAME-MANAGER FRAME COMMAND)
  (gtk-debug "NOTE-COMMAND-DISABLED ~a ~a ~a" frame-manager frame command))

(defmethod NOTE-PROPERTY-FRAME-CURRENT-PAGE-CHANGED :before (GADGET)
  (gtk-debug "NOTE-PROPERTY-FRAME-CURRENT-PAGE-CHANGED ~a" gadget))

(defmethod NOTE-DIALOG-PAGE-CHANGED :before (DIALOG)
  (gtk-debug "NOTE-DIALOG-PAGE-CHANGED ~a" dialog))

(defmethod NOTE-COMMAND-TABLE-CHANGED :before (FRAME-MANAGER FRAME)
  (gtk-debug "NOTE-COMMAND-TABLE-CHANGED ~a ~a" frame-manager frame))

(defmethod NOTE-FRAME-ENABLED :before (FRAME-MANAGER FRAME)
  (gtk-debug "NOTE-FRAME-ENABLED ~a ~a" frame-manager frame))

(defmethod NOTE-FRAME-DISABLED :before (FRAME-MANAGER FRAME)
  (gtk-debug "NOTE-FRAME-DISABLED ~a ~a" frame-manager frame))

(defmethod NOTE-ACCELERATORS-CHANGED :before (FRAME-MANAGER FRAME)
  (gtk-debug "NOTE-ACCELERATORS-CHANGED ~a ~a" frame-manager frame))

(defmethod NOTE-FRAME-STATE-CHANGED :before (FRAME OLD-STATE NEW-STATE)
  (gtk-debug "NOTE-FRAME-STATE-CHANGED ~a ~a -> ~a" frame old-state new-state))

(defmethod NOTE-FRAME-MAPPED :before (FRAME-MANAGER FRAME)
  (gtk-debug "NOTE-FRAME-MAPPED ~a ~a" frame-manager frame))

(defmethod NOTE-FRAME-UNMAPPED :before (FRAME-MANAGER FRAME)
  (gtk-debug "NOTE-FRAME-UNMAPPED ~a ~a" frame-manager frame))

(defmethod NOTE-FRAME-ICONIFIED :before (FRAME-MANAGER FRAME)
  (gtk-debug "NOTE-FRAME-ICONIFIED ~a ~a" frame-manager frame))

(defmethod NOTE-FRAME-DEICONIFIED :before (FRAME-MANAGER FRAME)
  (gtk-debug "NOTE-FRAME-DEICONIFIED ~a ~a" frame-manager frame))

(defmethod NOTE-FRAME-MAXIMIZED :before (FRAME-MANAGER FRAME)
  (gtk-debug "NOTE-FRAME-MAXIMIZED ~a ~a" frame-manager frame))

(defmethod NOTE-FRAME-UNMAXIMIZED :before (FRAME-MANAGER FRAME)
  (gtk-debug "NOTE-FRAME-UNMAXIMIZED ~a ~a" frame-manager frame))

;;UPDATE-FRAME-LAYOUT ?
;;UPDATE-FRAME-WRAPPER ?

(defmethod NOTE-FRAME-TITLE-CHANGED :before (FRAME-MANAGER FRAME)
  (gtk-debug "NOTE-FRAME-TITLE-CHANGED ~a ~a" frame-manager frame))

(defmethod NOTE-FRAME-ICON-CHANGED :before (FRAME-MANAGER FRAME)
  (gtk-debug "NOTE-FRAME-ICON-CHANGED ~a ~a" frame-manager frame))

(defmethod NOTE-BACKGROUND-OPERATION-STARTED :before (FRAME &KEY CURSOR)
  (gtk-debug "NOTE-BACKGROUND-OPERATION-STARTED ~a ~a" frame cursor))

(defmethod NOTE-BACKGROUND-OPERATION-FINISHED :before (FRAME)
  (gtk-debug "NOTE-BACKGROUND-OPERATION-FINISHED ~a" frame))

(defmethod NOTE-FRAME-TOP-LEVEL-FINISHED :before (FRAME)
  (gtk-debug "NOTE-FRAME-TOP-LEVEL-FINISHED ~a" frame))

;; NOTE-FRAME-ACCELERATORS-CHANGED IS INVOKED *BEFORE* THE ACCELERATORS ARE ACTUALLY CHANGED!
;; MAYBE WE NEED TO DO A "DO-LATER #'REBUILD-FRAME-ACCELERATORS FRAME" OR SOMETHING... CHECK
;; IF THERE ARE ANY METHODS AT ALL DEFINED ON THIS METHOD, AND IF NOT MOVE THE NOTE CALL TO
;; AFTER THE ACCELERATORS ARE CHANGED... (CALL-IN-FRAME)

(defmethod NOTE-MENU-ATTACHED :before (FRAME MENU)
  (gtk-debug "NOTE-MENU-ATTACHED ~a ~a" frame menu))

(defmethod NOTE-MENU-DETACHED :before (FRAME MENU)
  (gtk-debug "NOTE-MENU-DETACHED ~a ~a" frame menu))

(defmethod NOTE-DOCUMENT-CHANGED :before (FRAME DOCUMENT)
  (gtk-debug "NOTE-DOCUMENT-CHANGED ~a ~a" frame document))

(defmethod NOTE-GADGET-ITEMS-CHANGED :before (BOX)
  (gtk-debug "NOTE-GADGET-ITEMS-CHANGED ~a" box))

(defmethod NOTE-GADGET-SELECTION-CHANGED :before (BOX)
  (gtk-debug "NOTE-GADGET-SELECTION-CHANGED ~a" box))

(defmethod NOTE-GADGET-ENABLED :before (CLIENT BOX)
  (gtk-debug "NOTE-GADGET-ENABLED ~a ~a" client box))

(defmethod NOTE-GADGET-DISABLED :before (CLIENT BOX)
  (gtk-debug "NOTE-GADGET-DISABLED ~a ~a" client box))

(defmethod NOTE-GADGET-SLUG-SIZE-CHANGED :before (GADGET)
  (gtk-debug "NOTE-GADGET-SLUG-SIZE-CHANGED ~a" gadget))

(defmethod NOTE-GADGET-LABEL-CHANGED :before (GADGET)
  (gtk-debug "NOTE-GADGET-LABEL-CHANGED ~a" gadget))

(defmethod NOTE-GADGET-VALUE-CHANGED :before (GADGET)
  (gtk-debug "NOTE-GADGET-VALUE-CHANGED ~a" gadget))

(defmethod NOTE-GADGET-VALUE-RANGE-CHANGED :before (GADGET)
  (gtk-debug "NOTE-GADGET-VALUE-RANGE-CHANGED ~a" gadget))

(defmethod NOTE-TREE-CONTROL-ROOTS-CHANGED :before (TREE &KEY VALUE)
  (gtk-debug "NOTE-TREE-CONTROL-ROOTS-CHANGED ~a ~a" tree value))

(defmethod NOTE-SCROLL-BAR-CHANGED :before (SCROLL-BAR)
  (gtk-debug "NOTE-SCROLL-BAR-CHANGED ~a" scroll-bar))

(defmethod NOTE-GADGET-TEXT-CHANGED :before (GADGET)
  (gtk-debug "NOTE-GADGET-TEXT-CHANGED ~a" gadget))

(defmethod NOTE-VIEWPORT-POSITION-CHANGED :before (FRAME SHEET X Y)
  (gtk-debug "NOTE-VIEWPORT-POSITION-CHANGED ~a ~a ~a ~a" frame sheet x y))

(defmethod NOTE-VIEWPORT-REGION-CHANGED :before (SHEET VIEWPORT)
  (gtk-debug "NOTE-VIEWPORT-REGION-CHANGED ~a ~a" sheet viewport))

(defmethod NOTE-PAGES-CHANGED :before (PANE)
  (gtk-debug "NOTE-PAGES-CHANGED ~a" pane))

(defmethod NOTE-CHILD-ADDED :before (SHEET CHILD)
  (gtk-debug "NOTE-CHILD-ADDED ~a ~a" sheet child))

(defmethod NOTE-TRANSFORM-CHANGED :before (RECORD)
  (gtk-debug "NOTE-TRANSFORM-CHANGED ~a" record))

(defmethod NOTE-OUTPUT-RECORD-ATTACHED :before (RECORD SHEET)
  (gtk-debug "NOTE-OUTPUT-RECORD-ATTACHED ~a ~a" record sheet))

(defmethod NOTE-OUTPUT-RECORD-DETACHED :before (RECORD)
  (gtk-debug "NOTE-OUTPUT-RECORD-DETACHED ~a" record))

(defmethod NOTE-CHILD-ADDED-1 :before (RECORD CHILD)
  (gtk-debug "NOTE-CHILD-ADDED-1 ~a ~a" record child))

(defmethod NOTE-CHILD-REMOVED-1 :before (RECORD CHILD)
  (gtk-debug "NOTE-CHILD-REMOVED-1 ~a ~a" record child))

(defmethod NOTE-SHEET-ATTACHED :before (SHEET)
  (gtk-debug "NOTE-SHEET-ATTACHED ~a" sheet))

(defmethod NOTE-SHEET-DETACHED :before (SHEET)
  (gtk-debug "NOTE-SHEET-DETACHED ~a" sheet))

(defmethod NOTE-MIRROR-GEOMETRY-CHANGED :before (PORT SHEET REGION)
  (gtk-debug "NOTE-MIRROR-GEOMETRY-CHANGED ~a ~a ~a" port sheet region))

(defmethod NOTE-SHEET-MAPPED :before (SHEET)
  (gtk-debug "NOTE-SHEET-MAPPED ~a" sheet))

(defmethod NOTE-SHEET-UNMAPPED :before (SHEET)
  (gtk-debug "NOTE-SHEET-UNMAPPED ~a" sheet))

(defmethod NOTE-REGION-CHANGED :before (SHEET)
  (gtk-debug "NOTE-REGION-CHANGED ~a" sheet))

||#

(defmethod NOTE-FOCUS-IN :before (PORT SHEET)
  (gtk-debug "NOTE-FOCUS-IN ~a ~a" port sheet))

(defmethod NOTE-FOCUS-OUT :before (PORT SHEET)
  (gtk-debug "NOTE-FOCUS-OUT ~a ~a" port sheet))

;;; We seem to never terminate the port... when is that done?
(defmethod NOTE-PORT-TERMINATED :before (PORT CONDITION)
  (gtk-debug "NOTE-PORT-TERMINATED ~a ~a" port condition))

#||

(defmethod NOTE-CHILD-REMOVED :before (SHEET CHILD)
  (gtk-debug "NOTE-CHILD-REMOVED ~a ~a" sheet child))

(defmethod NOTE-SHEET-MAPPED :before (SHEET)
  (gtk-debug "NOTE-SHEET-MAPPED ~a" sheet))

(defmethod NOTE-SHEET-UNMAPPED :before (SHEET)
  (gtk-debug "NOTE-SHEET-UNMAPPED ~a" sheet))

||#
