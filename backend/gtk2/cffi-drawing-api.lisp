;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GTK2-DUIM -*-
(in-package #:gtk2-duim)

;;; ========================================================================
;;;
;;;    Drawing
;;;

;;; ------ Arcs

(defun cffi/gdk-draw-arc (drawable gcontext filled? x y width height angle1 angle2)
  ;; void gdk_draw_arc (GdkDrawable* drawable, GdkGC* gc, gboolean filled,
  ;;                    gint x, gint y, gint width, gint height,
  ;;                    gint angle1, gint angle2)
  ;; NOTE: angles are 'relative to the 3 o'clock position, counter-clockwise,
  ;; in 1/64ths of a degree'.
  (if (or (null drawable) (cffi:null-pointer-p drawable))
      (error "CFFI/GDK-DRAW-ARC invoked with null DRAWABLE")
      (if (or (null gcontext) (cffi:null-pointer-p gcontext))
	  (error "CFFI/GDK-DRAW-ARC invoked with null GCONTEXT")
	  (cffi:foreign-funcall "gdk_draw_arc"
				GdkDrawable* drawable GdkGC* gcontext
				gboolean filled?
				gint x gint y
				gint width gint height
				gint angle1 gint angle2
				:void)))
  drawable)


;;; ------ Lines

(defun cffi/gdk-draw-line (drawable gcontext x1 y1 x2 y2)
  ;; void gdk_draw_line (GdkDrawable*, GdkGC*, gint, gint, gint, gint)
  (if (or (null drawable) (cffi:null-pointer-p drawable))
      (error "CFFI/GDK-DRAW-LINE invoked with null DRAWABLE")
      (if (or (null gcontext) (cffi:null-pointer-p gcontext))
	  (error "CFFI/GDK-DRAW-LINE invoked with null GCONTEXT")
	  (cffi:foreign-funcall "gdk_draw_line"
				GdkDrawable* drawable GdkGC* gcontext
				gint x1 gint y1
				gint x2 gint y2
				:void)))
  drawable)


;;; ------ Points

(defun cffi/gdk-draw-point (drawable gcontext x y)
  ;; void gdk_draw_point (GdkDrawable*, GdkGC*, gint, gint)
  (if (or (null drawable) (cffi:null-pointer-p drawable))
      (error "CFFI/GDK-DRAW-POINT invoked with null DRAWABLE")
      (if (or (null gcontext) (cffi:null-pointer-p gcontext))
	  (error "CFFI/GDK-DRAW-POINT invoked with null GCONTEXT")
	  (cffi:foreign-funcall "gdk_draw_point"
				GdkDrawable* drawable GdkGC* gcontext
				gint x gint y
				:void)))
  drawable)


;;; ------ Polygons

(defun cffi/gdk-draw-polygon (drawable gcontext filled? points npoints)
  ;; void gdk_draw_polygon (GdkDrawable* drawable, GdkGC* gc,
  ;;                        gboolean filled, const GdkPoint* points,
  ;;                        gint n_points)
  (if (or (null drawable) (cffi:null-pointer-p drawable))
      (error "CFFI/GDK-DRAW-POLYGON invoked with null DRAWABLE")
      (if (or (null gcontext) (cffi:null-pointer-p gcontext))
	  (error "CFFI/GDK-DRAW-POLYGON invoked with null GCONTEXT")
	  (cffi:foreign-funcall "gdk_draw_polygon"
				GdkDrawable* drawable
				GdkGC* gcontext
				gboolean filled?
				GdkPoint* points
				gint npoints
				:void)))
  drawable)


(defun cffi/gdk-draw-lines (drawable gcontext points npoints)
  ;; void gdk_draw_lines (GdkDrawable* drawable,
  ;;                      GdkGC* gc,
  ;;                      const GdkPoint* points,
  ;;                      gint n_points)
  (if (or (null drawable) (cffi:null-pointer-p drawable))
      (error "CFFI/GDK-DRAW-LINES invoked with null DRAWABLE")
      (if (or (null gcontext) (cffi:null-pointer-p gcontext))
	  (error "CFFI/GDK-DRAW-LINES invoked with null GCONTEXT")
	  (cffi:foreign-funcall "gdk_draw_lines"
				GdkDrawable* drawable
				GdkGC* gcontext
				GdkPoint* points
				gint npoints
				:void)))
  drawable)


;;; ------ Rectangles

(defun cffi/gdk-draw-rectangle (drawable gcontext filled? x y width height)
  ;; void gdk_draw_rectangle (GdkDrawable*, GdkGC*, gboolean,
  ;;                          gint, gint, gint, gint)
  (if (or (null drawable) (cffi:null-pointer-p drawable))
      (error "CFFI/GDK-DRAW-RECTANGLE invoked with null DRAWABLE")
      (if (or (null gcontext) (cffi:null-pointer-p gcontext))
	  (error "CFFI/GDK-DRAW-RECTANGLE invoked with null GCONTEXT")
	  (cffi:foreign-funcall "gdk_draw_rectangle"
				GdkDrawable* drawable GdkGC* gcontext
				gboolean filled?
				gint x gint y
				gint width gint height
				:void)))
  drawable)


;;; ========================================================================
;;;
;;;    Pixmap support
;;;

(defun cffi/gdk-draw-drawable (drawable gc src xsrc ysrc xdest ydest width height)
"
Copies the width x height region of src at coordinates (xsrc, ysrc) to
coordinates (xdest, ydest) in drawable. width and/or height may be given as -1,
in which case the entire src drawable will be copied.

Most fields in gc are not used for this operation, but notably the clip mask or
clip region will be honored.

The source and destination drawables must have the same visual and colormap, or
errors will result. (On X11, failure to match visual/colormap results in a
BadMatch error from the X server.) A common cause of this problem is an attempt
to draw a bitmap to a color drawable. The way to draw a bitmap is to set the
bitmap as the stipple on the GdkGC, set the fill mode to GDK_STIPPLED, and then
draw the rectangle.

    drawable : a GdkDrawable
    gc       : a GdkGC sharing the drawable's visual and colormap
    src      : the source GdkDrawable, which may be the same as drawable
    xsrc     : X position in src of rectangle to draw
    ysrc     : Y position in src of rectangle to draw
    xdest    : X position in drawable where the rectangle should be drawn
    ydest    : Y position in drawable where the rectangle should be drawn
    width    : width of rectangle to draw, or -1 for entire src width
    height   : height of rectangle to draw, or -1 for entire src height
"
  ;; void gdk_draw_drawable (GdkDrawable *drawable, GdkGC *gc, GdkDrawable *src,
  ;;                         gint xsrc, gint ysrc, gint xdest, gint ydest,
  ;;                         gint width, gint height);
  (if (or (null drawable) (cffi:null-pointer-p drawable))
      (error "CFFI/GDK-DRAW-DRAWABLE invoked with null DRAWABLE")
      (if (or (null gc) (cffi:null-pointer-p gc))
	  (error "CFFI/GDK-DRAW-DRAWABLE invoked with null GC")
	  (if (or (null src) (cffi:null-pointer-p src))
	      (error "CFFI/GDK-DRAW-DRAWABLE invoked with null SRC")
	      (cffi:foreign-funcall "gdk_draw_drawable"
				    :pointer drawable
				    :pointer gc
				    :pointer src
				    gint     xsrc
				    gint     ysrc
				    gint     xdest
				    gint     ydest
				    gint     width
				    gint     height
				    :void))))
  drawable)


(defun cffi/gdk-drawable-get-depth (drawable)
"
Obtains the bit depth of the drawable, that is, the number of bits that make up
a pixel in the drawable's visual. Examples are 8 bits per pixel, 24 bits per
pixel, etc.

    drawable : a GdkDrawable
    Returns  : number of bits per pixel
"
  ;; gint gdk_drawable_get_depth (GdkDrawable *drawable);
  (if (or (null drawable) (cffi:null-pointer-p drawable))
      (error "CFFI/GDK-DRAWABLE-GET-DEPTH invoked with null DRAWABLE")
      (cffi:foreign-funcall "gdk_drawable_get_depth"
			    :pointer drawable
			    gint)))


(defun cffi/gdk-drawable-get-size (drawable width height)
"
Fills *width and *height with the size of drawable. width or height can be NULL
if you only want the other one.

On the X11 platform, if drawable is a GdkWindow, the returned size is the size
reported in the most-recently-processed configure event, rather than the
current size on the X server.

drawable : a GdkDrawable
width    : location to store drawable's width, or NULL. allow-none.
height   : location to store drawable's height, or NULL. allow-none.
"
  ;; void gdk_drawable_get_size (GdkDrawable *drawable, gint *width, gint *height);
  (if (or (null drawable) (cffi:null-pointer-p drawable))
      (error "CFFI/GDK-DRAWABLE-GET-SIZE invoked with null DRAWABLE")
      (cffi:foreign-funcall "gdk_drawable_get_size"
			    :pointer drawable
			    :pointer width
			    :pointer height
			    :void))
  drawable)


(defun cffi/gdk-pixbuf-get-from-drawable (dest src cmap src-x src-y dest-x dest-y width height)
"
GdkPixbuf * gdk_pixbuf_get_from_drawable (GdkPixbuf *dest,
                                          GdkDrawable *src,
                                          GdkColormap *cmap,
                                          int src_x, int src_y,
                                          int dest_x, int dest_y,
                                          int width, int height);

Transfers image data from a GdkDrawable and converts it to an RGB(A)
representation inside a GdkPixbuf. In other words, copies image data
from a server-side drawable to a client-side RGB(A) buffer. This
allows you to efficiently read individual pixels on the client side.

If the drawable src has no colormap (gdk_drawable_get_colormap()
returns NULL), then a suitable colormap must be specified. Typically a
GdkWindow or a pixmap created by passing a GdkWindow to
gdk_pixmap_new() will already have a colormap associated with it. If
the drawable has a colormap, the cmap argument will be ignored. If the
drawable is a bitmap (1 bit per pixel pixmap), then a colormap is not
required; pixels with a value of 1 are assumed to be white, and pixels
with a value of 0 are assumed to be black. For taking screenshots,
gdk_colormap_get_system() returns the correct colormap to use.

If the specified destination pixbuf dest is NULL, then this function
will create an RGB pixbuf with 8 bits per channel and no alpha, with
the same size specified by the width and height arguments.  In this
case, the dest_x and dest_y arguments must be specified as 0. If the
specified destination pixbuf is not NULL and it contains alpha
information, then the filled pixels will be set to full opacity (alpha
= 255).

If the specified drawable is a pixmap, then the requested source
rectangle must be completely contained within the pixmap, otherwise
the function will return NULL. For pixmaps only (not for windows)
passing -1 for width or height is allowed to mean the full width or
height of the pixmap.

If the specified drawable is a window, and the window is off the
screen, then there is no image data in the obscured/offscreen regions
to be placed in the pixbuf. The contents of portions of the pixbuf
corresponding to the offscreen region are undefined.

If the window you're obtaining data from is partially obscured by
other windows, then the contents of the pixbuf areas corresponding to
the obscured regions are undefined.

If the target drawable is not mapped (typically because it's
iconified/minimized or not on the current workspace), then NULL will
be returned.

If memory can't be allocated for the return value, NULL will be
returned instead.

(In short, there are several ways this function can fail, and if it
fails it returns NULL; so check the return value.)

This function calls gdk_drawable_get_image() internally and converts
the resulting image to a GdkPixbuf, so the documentation for
gdk_drawable_get_image() may also be relevant.

    dest : Destination pixbuf, or NULL if a new pixbuf should be created. [allow-none]
    src : Source drawable.
    cmap : A colormap if src doesn't have one set.
    src_x : Source X coordinate within drawable.
    src_y : Source Y coordinate within drawable.
    dest_x : Destination X coordinate in pixbuf, or 0 if dest is NULL.
    dest_y : Destination Y coordinate in pixbuf, or 0 if dest is NULL.
    width : Width in pixels of region to get.
    height : Height in pixels of region to get.

Returns : The same pixbuf as dest if it was non-NULL, or a
   newly-created pixbuf with a reference count of 1 if no destination
   pixbuf was specified, or NULL on error
"
  ;; GdkPixbuf * gdk_pixbuf_get_from_drawable (GdkPixbuf *dest,
  ;;                                           GdkDrawable *src,
  ;;                                           GdkColormap *cmap,
  ;;                                           int src_x, int src_y,
  ;;                                           int dest_x, int dest_y,
  ;;                                           int width, int height);
  (let ((dest (or dest (cffi:null-pointer)))
	(cmap (or cmap (cffi:null-pointer)))
	(dest-x (if dest dest-x 0))
	(dest-y (if dest dest-y 0)))
    (if (or (null src) (and (cffi:pointerp src) (cffi:null-pointer-p src)))
	(error "CFFI/GDK-PIXBUF-GET-FROM-DRAWABLE invoked with null SRC")
	(let ((gdkpixbuf (cffi:foreign-funcall "gdk_pixbuf_get_from_drawable"
					       :pointer dest
					       :pointer src
					       :pointer cmap
					       :int src-x :int src-y
					       :int dest-x :int dest-y
					       :int width  :int height
					       :pointer)))
	  (pointer-or-nil gdkpixbuf)))))


(defun cffi/gdk-pixmap-new (drawable width height depth)
"
Create a new pixmap with a given size and depth.

    drawable : A GdkDrawable, used to determine default values for the new pixmap.
               Can be NULL if depth is specified,
    width   : The width of the new pixmap in pixels.
    height  : The height of the new pixmap in pixels.
    depth   : The depth (number of bits per pixel) of the new pixmap. If -1,
              and drawable is not NULL, the depth of the new pixmap will be
              equal to that of drawable.

    Returns : the GdkPixmap
"
  ;; GdkPixmap* gdk_pixmap_new (GdkDrawable *drawable, gint width, gint height, gint depth);
  (if (and (or (null drawable) (cffi:null-pointer-p drawable))
	   (= depth -1))
      (error "CFFI/GDK-PIXMAP-NEW invoked with null DRAWABLE and no DEPTH")
      (let ((pixmap (cffi:foreign-funcall "gdk_pixmap_new"
					  :pointer drawable
					  gint     width
					  gint     height
					  gint     depth
					  :pointer)))
	(pointer-or-nil pixmap))))


(defun cffi/gdk-draw-rgb-image (drawable gc x y width height dither rgb-buf rowstride)
"
Draws an RGB image in the drawable. This is the core GdkRGB function, and likely
the only one you will need to use.

The rowstride parameter allows for lines to be aligned more flexibly. For
example, lines may be allocated to begin on 32-bit boundaries, even if the
width of the rectangle is odd. Rowstride is also useful when drawing a
subrectangle of a larger image in memory. Finally, to replicate the same line
a number of times, the trick of setting rowstride to 0 is allowed.

In general, for 0 <= i < width and 0 <= j < height, the pixel (x + i, y + j) is
colored with:

    red value   rgb_buf[j * rowstride + i * 3],
    green value rgb_buf[j * rowstride + i * 3 + 1],
and blue value  rgb_buf[j * rowstride + i * 3 + 2].

    drawable  : The GdkDrawable to draw in (usually a GdkWindow).
    gc        : The graphics context (all GDK drawing operations require one;
                its contents are ignored).
    x         : The x coordinate of the top-left corner in the drawable.
    y         : The y coordinate of the top-left corner in the drawable.
    width     : The width of the rectangle to be drawn.
    height    : The height of the rectangle to be drawn.
    dith      : A GdkRgbDither value, selecting the desired dither mode.
    rgb_buf   : The pixel data, represented as packed 24-bit data.
    rowstride : The number of bytes from the start of one row in rgb_buf to the
                start of the next.
"
  ;; void gdk_draw_rgb_image (GdkDrawable *drawable, GdkGC *gc, gint x, gint y,
  ;;                          gint width, gint height, GdkRgbDither dith,
  ;;                          guchar *rgb_buf, gint rowstride);
  (if (or (null drawable) (cffi:null-pointer-p drawable))
      (error "CFFI/GDK-DRAW-RGB-IMAGE invoked with null DRAWABLE")
      (if (or (null gc) (cffi:null-pointer-p gc))
	  (error "CFFI/GDK-DRAW-RGB-IMAGE invoked with null GC")
	  (if (or (null rgb-buf) (cffi:null-pointer-p rgb-buf))
	      (error "CFFI/GDK-DRAW-RGB-IMAGE invoked with null RGB-BUF")
	      (cffi:foreign-funcall "gdk_draw_rgb_image"
				    :pointer drawable
				    :pointer gc
				    gint x     gint y
				    gint width gint height
				    :int dither
				    :pointer rgb-buf
				    gint rowstride
				    :void))))
  drawable)


;;; ------ Support for GdkBitmap

(defun cffi/gdk-bitmap-create-from-data (drawable data width height)
"
Creates a new bitmap from data in XBM format.

    drawable : a GdkDrawable, used to determine default values for the
        new pixmap. Can be NULL, in which case the root window is used.
    data : a pointer to the XBM data.  width : the width of the new
    pixmap in pixels.  height : the height of the new pixmap in
    pixels.  Returns : the GdkBitmap
"
  ;; GdkBitmap* gdk_bitmap_create_from_data (GdkDrawable *drawable, const gchar *data,
  ;;                                         gint width, gint height);
  (if (or (null data) (cffi:null-pointer-p data))
      (error "CFFI/GDK-BITMAP-CREATE-FROM-DATE invoked with null DATA")
      (let* ((drawable (or drawable (cffi:null-pointer)))
	     (bitmap   (cffi:foreign-funcall "gdk_bitmap_create_from_data"
					     :pointer drawable
					     :pointer data
					     gint width
					     gint height
					     :pointer)))
	(pointer-or-nil bitmap))))


(defun cffi/gdk-pixmap-create-from-data (drawable data width height depth fg bg)
"
Create a two-color pixmap from data in XBM data.

      drawable : a GdkDrawable, used to determine default values for
          the new pixmap. Can be NULL, if the depth is given.
      data     : a pointer to the data.
      width    : the width of the new pixmap in pixels.
      height   : the height of the new pixmap in pixels.
      depth    : the depth (number of bits per pixel) of the new pixmap.
      fg       : the foreground color.
      bg       : the background color.
      Returns  : the GdkPixmap
"
  (declare (ignore drawable data width height depth fg bg))  ;; until this method is implemented!
  ;; GdkPixmap* gdk_pixmap_create_from_data (GdkDrawable *drawable,
  ;;                                         const gchar *data,
  ;;                                         gint width, gint height,
  ;;                                         gint depth,
  ;;                                         const GdkColor *fg,
  ;;                                         const GdkColor *bg);
  (not-yet-implemented "gdk-pixmap-create-from-data"))


