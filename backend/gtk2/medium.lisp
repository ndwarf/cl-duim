;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: GTK2-DUIM -*-
(in-package #:gtk2-duim)

#||
Module:       gtk-duim
Synopsis:     GTK medium implementation
Author:       Andy Armstrong, Scott McKay
Copyright:    Original Code is Copyright (c) 1995-2004 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND

/// GTK mediums

//---*** What should this be?
define constant <native-color> = <integer>;

define sealed class <gtk-medium> (<basic-medium>)
  sealed slot %ink-cache :: <object-table> = make(<table>);
  sealed slot %palette = #f;		//---*** MAYBE JUST USE THE FRAME'S PALETTE?
  sealed slot %foreground-color :: false-or(<GdkColor*>) = #f;
  sealed slot %background-color :: false-or(<GdkColor*>) = #f;
  // Cached clipping region
  sealed slot %clip-mask = #f;		// #f, #"none", or an X region
end class <gtk-medium>;

define sealed domain make (singleton(<gtk-medium>));
define sealed domain initialize (<gtk-medium>);
||#

;;; Only for drawing in "drawing areas"! We don't draw over GTK's widgets.
;;; Note: a CAIRO_T generated in an expose event handler can't be cached outside
;;; that handler's dynamic extent due to the double-buffering GTK puts in during
;;; said handler.

;; medium-drawable = surface

(defclass <gtk-medium> (<basic-medium>)
  ;;; %INK-CACHE is used to cache inks that decode into pixmaps.
  ((%ink-cache :type hash-table
	       :initform (make-hash-table :test #'eql)
	       :accessor %ink-cache)
   ;; Holds the cairo surface used for the foreground; this can
   ;; be an image surface. It is used for stroking paths.
   ;; If the medium-foreground is an RGB(A) color, this slot will
   ;; be NIL.
   (%foreground-ink :initform nil :accessor %foreground-ink)
   (%foreground-fill-style :accessor %foreground-fill-style)
   (%foreground-operation :accessor %foreground-operation)
   ;; Holds the cairo surface used for the background; this can
   ;; be an image surface. It is used for painting the background.
   ;; If the medium-background is an RGB(A) color, this slot will
   ;; be NIL.
   (%background-ink :initform nil :accessor %background-ink)
   (%background-fill-style :accessor %background-fill-style)
   (%background-operation :accessor %background-operation)
   ;; Holds the cairo surface used when filling a path; this can
   ;; be an image surface. If the brush is an RGB(A) color, this
   ;; slot will be NIL.
   (%brush-ink :initform nil :accessor %brush-ink)
   (%brush-fill-style :accessor %brush-fill-style)
   (%brush-operation :accessor %brush-operation)
   ;; Cached cairo context. Note that this is often invalid, especially if
   ;; created within the scope of an expose event handler (in which case we
   ;; get the context of the double-buffering offscreen buffer, not the
   ;; context for the X window)
   ;; XXX: THIS IS CACHED AS THE "MEDIUM-DRAWABLE", NO NEED TO CACHE IT HERE
   ;; AS WELL...
   #-(and) (%context :initform nil :accessor %context)
   ;; Cached clipping region
   (%clip-mask :initform nil   ;; nil, :none, or an X region
	       :accessor %clip-mask)))


#||
// This is intentionally _not_ sealed
define method make-medium
    (_port :: <gtk-port>, sheet :: <sheet>)
 => (medium :: <gtk-medium>)
  make(<gtk-medium>, port: _port, sheet: sheet)
end method make-medium;
||#

(defmethod make-medium ((_port <gtk-port>)
			(sheet <sheet>))
  (make-instance '<gtk-medium>
		 :port  _port
		 :sheet sheet))

#||
define method clear-ink-cache (medium :: <gtk-medium>)
  //--- Should we clear the ink cache from the palette?
end method clear-ink-cache;
||#

(defgeneric clear-ink-cache (medium)
  (:documentation
"
Source file: gtk-medium.lisp
"))

(defmethod clear-ink-cache ((medium <gtk-medium>))
  ;;--- Should we clear the ink cache from the palette?
  ;; Hrm. This isn't necessarily true for image inks...
  ;; We don't clear the %foreground/%background-color cached objects
  ;; because they are related to the mirror to which the medium is
  ;; attached and that's not going to change anytime soon.
  (ignoring "[gtk-medium] clear-ink-cache")
  ;; FIXME: free up any allocated FFI memory when the pixmaps are
  ;; freed!
;;  (clrhash (%ink-cache medium))
  )


#||
define sealed method destroy-medium
    (medium :: <gtk-medium>) => ()
  clear-ink-cache(medium);
  next-method();
  medium-drawable(medium) := #f
end method destroy-medium;
||#

(defmethod destroy-medium ((medium <gtk-medium>))
  (clear-ink-cache medium)
  ;; FIXME - WHERE IS THE CAIRO CONTEXT FREED?
  (call-next-method)
  ;; FIXME: THIS IS DONE IN "DETACH-MEDIUM", NOT HERE
  #-(and)
  (unless (null (medium-drawable medium))
    (cffi/g-object-unref (medium-drawable medium)))
  (setf (medium-drawable medium) nil))


#||
define sealed method do-attach-medium
    (sheet :: <sheet>, medium :: <gtk-medium>) => ()
  let _port  = port(sheet);
  let mirror = sheet-mirror(sheet);
  assert(mirror,
	 "Unexpected failure: no mirror when attaching medium for %=",
	 sheet);
  debug-message("Attaching medium to %= (medium-sheet %=)",
		sheet, medium-sheet(medium));
  let widget = GTK-WIDGET(mirror.mirror-widget);
  let drawable = pointer-cast(<GdkDrawable*>, widget.window-value);
  clear-ink-cache(medium);
  medium-drawable(medium) := drawable;
  // Set up the palette and fg/bg pixels
  let widget  = mirror-widget(mirror);
  let palette = port-default-palette(_port);
  let fg = medium-foreground(medium);
  let bg = medium-background(medium);
  /*---*** Colors not implemented yet!
  medium.%palette := palette;
  medium.%foreground-color
    := case
	 fg => allocate-color(fg, palette);
	   //---*** Fill this in!
	 otherwise => error("Can't get default foreground pixel!");
       end;
  medium.%background-color
    := case
	 fg => allocate-color(fg, palette);
	   //---*** Fill this in!
	 otherwise => error("Can't get default background pixel!");
       end;
  */
end method do-attach-medium;
||#

;; FIXME: I'm pretty sure this %NATIVE-DRAWABLE stuff is not needed... check and
;; remove once the Cairo changes have stabilised

(defgeneric %native-tk-window (mirror))

(defmethod %native-tk-window ((mirror <mirror>))
  (let* ((widget (CFFI/GTK-WIDGET (mirror-widget mirror))))
    (unless widget
      (error "Unable to obtain native-drawable for mirror ~a; mirror-widget is invalid (~a)" mirror widget))
    (cffi/gtk_widget_get_window widget)))


(defmethod %native-tk-window ((mirror <gtk-viewport-mirror>))
  #-(and)(warn "%NATIVE-DRAWABLE <GTK-VIEWPORT-MIRROR> INVOKED")
  ;; GTK_LAYOUT(layout)->bin_window
  (let* ((widget (CFFI/GTK-WIDGET (mirror-widget mirror))))
    (unless widget
      (error "Unable to obtain native-drawable for viewport-mirror ~a; mirror-widget is invalid (~a)" mirror widget))
    (cffi/gtk_layout_get_bin_window widget)))


;; This is invoked when sheets are attached to the display as part of the grafting
;; process, but they are not mapped yet (although the mirrors are created) - so attempting
;; to grab any information from the underlying windowing system is not reliable here.
(defmethod do-attach-medium ((sheet <sheet>) (medium <gtk-medium>))
  (let ((_port  (port sheet))
	;; NOTE: Not sheet-direct-mirror, mediums can be attached to sheets
	;; that use a parent sheet's mirror as the drawable...
	(mirror (sheet-mirror sheet)))
    (declare (ignore _port))
    (unless mirror
      (error "Unexpected failure: no mirror when attaching medium for ~a" sheet))
    (clear-ink-cache medium)
    ;; The native drawable is not available until after CFFI/GTK-WIDGET-SHOW is called
    ;; (i.e. the gtk mirror is realized). Generally that hasn't happened yet so we
    ;; also check and try to set up the native drawable / gcontext in the GET-GCONTEXT
    ;; call. In addition, if the medium is attached within an expose event, the
    ;; context will not be valid for the next expose event due to GTK double-buffering.
    ;; When would be a better time to do this work?
    #-(and)
    (let* ((gdk-drawable (%native-tk-window mirror)))
      (if gdk-drawable
	  (progn
	    ;; As far as I can tell, the only time the code gets in this branch is when
	    ;; the 'multiple layout' frame test is run and a new medium is attached to
	    ;; a pre-existing (and mapped) drawable.
	    (gtk-debug "GTK-MEDIUM - DO-ATTACH-MEDIUM : setting CAIRO_T as MEDIUM-DRAWABLE")
	    ;; This drawable ref is freed in "destroy-medium"
	    (let* ((context (cffi/gdk-cairo-create gdk-drawable)))
	      (setf (medium-drawable medium) context)))
	  (gtk-debug "GTK-MEDIUM - DO-ATTACH-MEDIUM : No drawable!")))))



(DEFUN GTK-IMAGE-TO-SURFACE (IMAGE)
"
Returns a pointer to a cairo image surface for _image_.
"
  ;; FIXME: We lose the %transform from _image_ here.
  (let ((surface (cffi/cairo_image_surface_create_for_data (%data image)
							   (%format image)
							   (%width image)
							   (%height image)
							   (%stride image))))
    surface))



(defmethod attach-medium :after (sheet (medium <gtk-medium>))
;;  (WARN "ATTACH-MEDIUM :AFTER -- ENTERED")
  ;; Set up the palette and fg/bg colors; also set the widget bg initial color
  ;; now it's known
  (let ((_port (port sheet))
	(mirror (sheet-mirror sheet)))
    (unless mirror
      (error "Unexpected failure: no mirror when setting cached colours in medium ~a for sheet ~a" medium sheet))
    (let* ((widget (mirror-widget mirror))
	   (palette (port-default-palette _port))
	   (fg (medium-foreground medium))
	   (bg (medium-background medium)))
      ;; Set up a cairo_surface_t for the foreground <ink>
      (multiple-value-bind (fg-color fg-fill-style fg-operation fg-image)
	  (decode-ink medium nil fg)
	(declare (ignore fg-color))
	(setf (%foreground-ink medium) (if fg-image (GTK-IMAGE-TO-SURFACE fg-image) nil))
	(setf (%foreground-fill-style medium) fg-fill-style)
	(setf (%foreground-operation medium)  fg-operation))
      ;; Set up a cairo_surface_t for the background <ink>
      (multiple-value-bind (bg-color bg-fill-style bg-operation bg-image)
	  (decode-ink medium nil bg)
	(setf (%background-ink medium) (if bg-image (GTK-IMAGE-TO-SURFACE bg-image) nil))
	(setf (%background-fill-style medium) bg-fill-style)
	(setf (%background-operation medium)  bg-operation)
	;; XXX: Maybe this should be done in "update-attributes"?
	;; XXX: This code doesn't deal with setting opacities
	;; XXX: This doesn't deal with image backgrounds!
	(when bg-color
	  (let ((%bg (color->native-color bg palette)))
	    (cffi/gtk-widget-modify-bg widget +CFFI/GTK-STATE-NORMAL+ %bg)))))))


#||
define sealed method do-detach-medium
    (sheet :: <sheet>, medium :: <gtk-medium>) => ()
  //---*** So which is it, 'clear-ink-cache' or 'clear-colors'?
  clear-ink-cache(medium);
  // clear-colors(medium.%palette);
  medium-drawable(medium) := #f
end method do-detach-medium;
||#

(defmethod do-detach-medium ((sheet <sheet>) (medium <gtk-medium>))
  ;;---*** So which is it, 'clear-ink-cache' or 'clear-colors'?
  ;; FIXME: maybe neither since we clear the cache anyway when the
  ;; medium is attached.
  ;; However, since for GTK mediums the palette is shared (we use the
  ;; port-default-palette always, currently) we probably don't want to
  ;; decache all the colors it references already.
  (clear-ink-cache medium)
  ;; (clear-colors (%palette medium))
  (unless (null (medium-drawable medium))
    ;; MEDIUM-DRAWABLE is a CAIRO_T
    (cffi/cairo-destroy (medium-drawable medium))
    (setf (medium-drawable medium) nil))
  (unless (null (%foreground-ink medium))
    (cffi/cairo_surface_destroy (%foreground-ink medium))
    (setf (%foreground-ink medium) nil))
  (unless (null (%background-ink medium))
    (cffi/cairo_surface_destroy (%background-ink medium))
    (setf (%background-ink medium) nil))
  (unless (null (%brush-ink medium))
    (cffi/cairo_surface_destroy (%brush-ink medium))
    (setf (%brush-ink medium) nil)))


#||
define sealed method deallocate-medium
    (_port :: <gtk-port>, medium :: <gtk-medium>) => ()
  next-method();
  medium.%palette := #f;
  medium-drawable(medium) := #f
end method deallocate-medium;
||#

(defmethod deallocate-medium ((_port <gtk-port>)
			      (medium <gtk-medium>))
  (call-next-method)
  ;; No need to clear the ink cache since that's done when the medium
  ;; is detached, which it certainly will be by this point.
  (setf	(medium-drawable medium) nil))


#||
define sealed method medium-foreground-setter
    (foreground :: <color>, medium :: <gtk-medium>)
 => (foreground :: <color>)
  next-method();	// also sets 'medium-drawing-state-cache' to 0
  not-yet-implemented("medium-foreground-setter");
  /*---*** Colors not implemented yet!
  clear-ink-cache(medium);
  let color = allocate-color(foreground, medium.%palette);
  medium.%foreground-color := color;
  queue-repaint(medium-sheet(medium), $everywhere);
  foreground
  */
end method medium-foreground-setter;
||#

(defmethod (setf medium-foreground) ((foreground <color>) (medium <gtk-medium>))
  (call-next-method) ;; also sets 'medium-drawing-state-cache' to 0
  ;; Pixmaps in the cache might reference *foreground*/*background* so
  ;; will need to be regenerated.
  (clear-ink-cache medium)
  (when (%foreground-ink medium)
    (cffi/cairo_surface_destroy (%foreground-ink medium)))
  (multiple-value-bind (fg-color fg-fill-style fg-operation fg-image)
      (decode-ink medium nil foreground)
    (declare (ignore fg-color))
    (setf (%foreground-ink medium) (if fg-image (GTK-IMAGE-TO-SURFACE fg-image) nil))
    (setf (%foreground-fill-style medium) fg-fill-style)
    (setf (%foreground-operation medium)  fg-operation))
  (queue-repaint (medium-sheet medium) *everywhere*)
  foreground)



#||
define sealed method medium-background-setter
    (background :: <color>, medium :: <gtk-medium>)
 => (background :: <color>)
  next-method();	// also sets 'medium-drawing-state-cache' to 0
  not-yet-implemented("medium-foreground-setter");
  /*---*** Colors not implemented yet!
  clear-ink-cache(medium);
  let color = allocate-color(background, medium.%palette);
  medium.%background-color := color;
  queue-repaint(medium-sheet(medium), $everywhere);
  background
  */
end method medium-background-setter;
||#

(defmethod (setf medium-background) ((background <color>) (medium <gtk-medium>))
  (call-next-method)  ;; also sets 'medium-drawing-state-cache' to 0
  ;; Pixmaps in the cache might reference *foreground*/*background* so
  ;; will need to be regenerated.
  (clear-ink-cache medium)
  (when (%background-ink medium)
    (cffi/cairo_surface_destroy (%background-ink medium)))
  (let* ((sheet (medium-sheet medium))
	 (mirror  (sheet-mirror sheet))
	 (widget  (CFFI/GTK-WIDGET (mirror-widget mirror)))
	 (port (port sheet))
	 (palette (port-default-palette port)))
    (multiple-value-bind (bg-color bg-fill-style bg-operation bg-image)
	(decode-ink medium nil background)
      (setf (%background-ink medium) (if bg-image (GTK-IMAGE-TO-SURFACE bg-image) nil))
      (setf (%background-fill-style medium) bg-fill-style)
      (setf (%background-operation medium)  bg-operation)
      ;; XXX: Maybe this should be done in "update-attributes"?
      ;; XXX: This code doesn't deal with setting opacities
      ;; XXX: This doesn't deal with image backgrounds!
      (when bg-color
	(let ((%bg (color->native-color bg-color palette)))
	  (cffi/gtk-widget-modify-bg widget +CFFI/GTK-STATE-NORMAL+ %bg))))
    (queue-repaint (medium-sheet medium) *everywhere*)
    background))


#||
define sealed method medium-clipping-region-setter
    (region :: <region>, medium :: <gtk-medium>) => (region :: <region>)
  next-method();
  // Don't flush the cache if the region isn't really changing.
  // This situation comes up all the time during repainting, when we set
  // the clipping region for every output record, but we almost always
  // just set it to $everywhere.
  unless (region == medium-clipping-region(medium))
    medium.%clip-mask := #f
  end;
  region
end method medium-clipping-region-setter;
||#

(defmethod (setf medium-clipping-region) ((region <region>)
					  (medium <gtk-medium>))
  (call-next-method)
  ;; Don't flush the cache if the region isn't really changing.
  ;; This situation comes up all the time during repainting, when we set
  ;; the clipping region for every output record, but we almost always
  ;; just set it to $everywhere.
  (unless (equal? region (medium-clipping-region medium))
    (setf (%clip-mask medium) nil))
  region)


#||
define sealed method invalidate-cached-region
    (medium :: <gtk-medium>) => ()
  medium.%clip-mask := #f;
  next-method()
end method invalidate-cached-region;
||#

(defmethod invalidate-cached-region ((medium <gtk-medium>))
  (setf (%clip-mask medium) nil)
  (call-next-method))


#||
define sealed method invalidate-cached-transform
    (medium :: <gtk-medium>) => ()
  medium.%clip-mask := #f;
  next-method()
end method invalidate-cached-transform;
||#

(defmethod invalidate-cached-transform ((medium <gtk-medium>))
  (setf (%clip-mask medium) nil)
  (call-next-method))


#||
define sealed method invalidate-cached-drawing-state
    (medium :: <gtk-medium>, cached-state :: <integer>) => ()
  ignore(cached-state);
  #f
end method invalidate-cached-drawing-state;
||#

(defmethod invalidate-cached-drawing-state ((medium <gtk-medium>)
					    (cached-state integer))
  (declare (ignore cached-state))
  ;; FIXME: Is there anything to be done here? Do we need to call-next-
  ;; method? (Not that it does any more than this method does anyhow)
  nil)



#||
/// Display forcing

define sealed method force-display
    (medium :: <gtk-medium>) => ()
  gdk-flush()
end method force-display;
||#

(defmethod force-display ((medium <gtk-medium>))
  ;; FIXME: this should be 'XFlush', probably.
  ;; See the gtk-faq, "why don't my changes show up?".
  ;; (XFlush (GDK-DISPLAY-XDISPLAY display))
  ;; Flush and continue without waiting
  (cffi/gdk-flush))


#||
define sealed method synchronize-display
    (medium :: <gtk-medium>) => ()
  gdk-flush()
end method synchronize-display;
||#

(defmethod synchronize-display ((medium <gtk-medium>))
  ;; Flush and wait for X to synchronise
  (cffi/gdk-flush))



#||
/// Drawing state updating

define inline method get-gcontext
    (medium :: <gtk-medium>)
 => (drawable :: <GdkDrawable*>, gcontext :: <GdkGC*>)
  let sheet = medium.medium-sheet;
  let mirror = sheet.sheet-mirror;
  let widget = GTK-WIDGET(mirror.mirror-widget);
  let drawable = medium-drawable(medium);
  // let gcontext = widget.style-value.black-gc-value;
  let gcontext = widget.style-value.fg-gc-value[GTK-WIDGET(widget).state-value];
  if (null-pointer?(drawable))
    debug-message("Null pointer drawable!");
    let drawable = pointer-cast(<GdkDrawable*>, widget.window-value);
    medium-drawable(medium) := drawable;
    values(drawable, gcontext)
  else
    values(drawable, gcontext)
  end
end method get-gcontext;
||#

(defgeneric get-context (medium)
  (:documentation
"
Returns the toolkit (Cairo) graphics context (cairo_t) associated with
_medium_.
"))

;;; DRAWABLE => CAIRO_T. No need to return anything else.

(defmethod get-context ((medium <gtk-medium>))
  (let* ((sheet   (medium-sheet medium))
	 (mirror  (sheet-mirror sheet))
	 (widget  (CFFI/GTK-WIDGET (mirror-widget mirror)))
	 (context (medium-drawable medium)))
    (declare (ignore widget))
    ;; Just get a new cairo_t each time. This will be a performance hit
    ;; for now. FIXME!
;;    (let ((surface (and context (cffi/cairo-get-target context))))
;;      (when surface
	;; This call should set the surface status to "FINISHED" if the surface is
	;; finished. Unless this is done, the surface status is "SUCCESS", then we get a
	;; "the surface is finished" assert failure when checking the status of
	;; the context. Very strange.
;;	(cffi/cairo_surface_set_device_offset surface 0.0d0 0.0d0))
;;      (when (or (null context)
;;		(and surface (/= +CFFI/CAIRO_STATUS_SUCCESS+ (cffi/cairo_surface_status surface)))
;;		;; if the surface is finished and the previous check is left out, cairo blows up
    ;;		(/= +CFFI/CAIRO_STATUS_SUCCESS+ (cffi/cairo-status context)))
    (when context
      ;; We have a cairo_t but it's no good... create a new one.
      (cffi/cairo-destroy context)
      (setf (medium-drawable medium) nil))
    ;; Sheets are attached (grafted) before they are mapped, so there's no
    ;; native drawable at that time -- which is when the code tries attach
    ;; a medium and set the medium's drawable. If we get here and we don't
    ;; know the drawable, try to set it here too.
    ;;
    ;; Note: this is *most* likely to be called from an expose-event handler. In
    ;; that case, the CAIRO_T we get here will be for the double-buffering offscreen
    ;; window rather than for the widget window and will ONLY be valid for the
    ;; duration of that expose event - hence the status check above (an invalid
    ;; cairo_t will not return status success)
    ;;
    ;; Generate a new cairo_t for the widget
    (let* ((drawable (%native-tk-window mirror)))
      (setf context (cffi/gdk-cairo-create drawable))
      (setf (medium-drawable medium) context))))
;;    ;; FIXME: I *THINK* WE'RE KEEPING THE TOP-LEVEL DRAWABLE EVEN WHEN DRAWING
;;    ;; ONTO CHILDREN OF THAT WINDOW - THAT'S WHY THE DRAWING IS NOT DONE IN
;;    ;; THE RIGHT PLACE. CHECK.
;;    ;; CHECK THAT THE CONTEXT WE WOULD HAVE GOT IF WE WERE CREATING A NEW ONE TO THE
;;    ;; (VALID) CONTEXT WE ACTUALLY HAVE. CREATE A NEW ONE IF THEY ARE DIFFERENT.
;;    ;;---*** WE DEFINITELY TRY TO DRAW TO THE WRONG PLACE, SOMETHING IS NOT RIGHT HERE
;;    ;; THIS WOULD BE MUCH EASIER IF WE WERE CATCHING THE 'DRAW' SIGNAL AND BEING
;;    ;; GIVEN THE CORRECT CONTEXT_T! (GTK 3 FEATURE)
;;    ;;---*** WHEN (IF?) THIS IS FIXED, ALSO PUT THE CACHING BACK IN TO 'UPDATE-
;;    ;; -DRAWING-STATE'
;;    (let* ((drawable (%native-tk-window mirror))
;;	   (dcont    (cffi/gdk-cairo-create drawable)))
;;      (if (cffi:pointer-eq context dcont)
;;	  (duim-debug-message "context is as expected")
;;	  (progn
;;	    (duim-debug-message "using wrong context! swap")
;;	    (setf context dcont)
;;	    (setf (medium-drawable medium) context))))
;;    context))


;;; THERE'S ALSO A METHOD ON THE ABOVE ON <GTK-PIXMAP-MEDIUM>, BUT I'M NOT SURE
;;; IT'S DOING ANYTHING...

#||
// Note that the brush defaults to 'medium-brush(medium)',
// but the pen and font default to #f in order to avoid unnecessary work
define sealed method update-drawing-state
    (medium :: <gtk-medium>, #key brush, pen, font)
 => (drawable :: <GdkDrawable*>, gcontext :: <GdkGC*>)
  let (drawable :: <GdkDrawable*>, gcontext :: <GdkGC*>)
    = get-gcontext(medium);
  ignoring("update-drawing-state");
  /*
  let old-cache :: <integer> = medium-drawing-state-cache(medium);
  let new-cache :: <integer> = 0;
  when (old-cache ~= $medium-fully-cached)
    // Establish a brush, unless it's already cached
    when (zero?(logand(old-cache, $medium-brush-cached)))  
      let brush = brush | medium-brush(medium);
      establish-brush(medium, brush, gcontext);
      new-cache := logior(new-cache, $medium-brush-cached)
    end;
    // Establish a pen, unless it's already cached
    when (zero?(logand(old-cache, $medium-pen-cached)))
      let pen = pen | medium-pen(medium);
      establish-pen(medium, pen, gcontext);
      new-cache := logior(new-cache, $medium-pen-cached)
    end;
    // Establish a font only if requested, unless it's already cached
    when (font & zero?(logand(old-cache, $medium-font-cached)))
      establish-font(medium, font, gcontext);
      new-cache := logior(new-cache, $medium-font-cached)
    end;
    unless (medium.%clip-mask)
      //---*** Fill this in!
      ignoring("clip-mask in update-drawing-state");
      let mask = compute-clip-mask(medium);
      if (mask == #"none")
	//---*** Clear the mask!
      else
	let (x, y, width, height) = values(mask[0], mask[1], mask[2], mask[3]);
	//---*** Set the mask!
      end;
      new-cache := logior(new-cache, $medium-region-cached)
    end;
    medium-drawing-state-cache(medium) := logior(old-cache, new-cache)
  end;
  */
  values(drawable, gcontext)
end method update-drawing-state;
||#

(defgeneric update-drawing-state (medium &key brush pen font)
  (:documentation
"
Note that the brush defaults to 'medium-brush(medium)',
but the pen and font default to #f in order to avoid unnecessary work
"))

(defmethod update-drawing-state ((medium <gtk-medium>)
				 &key brush pen font)
  (let* ((cr (get-context medium))
	 (old-cache (medium-drawing-state-cache medium))
	 (new-cache 0))

    (unless (and medium cr)
      (gtk-debug "Got medium=~a, cr=~a" medium cr))
    ;;---*** TEMPORARY HACK. REMOVE ONCE THE CORRECT CONTEXT_T
    ;;---*** CACHING IS IMPLEMENTED
    (SETF OLD-CACHE 0)

    (when (/= old-cache +medium-fully-cached+)
      ;; Establish a brush, unless it's already cached
      (when (zerop (logand old-cache +medium-brush-cached+))
	(let ((brush (or brush (medium-brush medium))))
	  (establish-brush medium brush cr)
	  (setf new-cache (logior new-cache +medium-brush-cached+))))
      ;; Establish a pen, unless it's already cached
      (when (zerop (logand old-cache +medium-pen-cached+))
	(let ((pen (or pen (medium-pen medium))))
	  (establish-pen medium pen cr)
	  (setf new-cache (logior new-cache +medium-pen-cached+))))
      ;; Establish a font only if requested, unless it's already cached
      (when (and font (zerop (logand old-cache +medium-font-cached+)))
	(establish-font medium font cr)
	;; NOTE: Unlike the other features, establishing a font
	;; creates a PANGO context and caches it in the medium
	;; object (NOTE: NOT THE CONTEXT!) -- FIXME: NOT YET IT DOESN'T...
	(setf new-cache (logior new-cache +medium-font-cached+)))
      (unless (%clip-mask medium)
	(let ((mask (compute-clip-mask medium)))
	  (unless (eql mask :none)
	    (multiple-value-bind (x y width height)
		(values (aref mask 0) (aref mask 1)
			(aref mask 2) (aref mask 3))
	      ;; FIXME: What else (other than a rectangle) can a clip region
	      ;; be? An arbitrary path? An image?
	      (cffi/cairo_rectangle cr
				    (coerce x 'double-float)
				    (coerce y 'double-float)
				    (coerce width 'double-float)
				    (coerce height 'double-float))
	      (cffi/cairo_clip cr))))
	(setf new-cache (logior new-cache +medium-region-cached+)))
      (setf (medium-drawing-state-cache medium) (logior old-cache new-cache)))
    cr))


#||
define constant $function-map :: <simple-object-vector>
    = make(<simple-vector>, size: 16);
||#

(defparameter *function-map* (make-array 16))

#||
begin
  $function-map[$boole-clr]   := $GDK-CLEAR;
  $function-map[$boole-set]   := $GDK-SET;
  $function-map[$boole-1]     := $GDK-COPY;
  $function-map[$boole-2]     := $GDK-NOOP; 
  $function-map[$boole-c1]    := $GDK-COPY-INVERT;
  $function-map[$boole-c2]    := $GDK-INVERT;
  $function-map[$boole-and]   := $GDK-AND;
  $function-map[$boole-ior]   := $GDK-OR;
  $function-map[$boole-xor]   := $GDK-XOR;
  $function-map[$boole-eqv]   := $GDK-EQUIV;
  $function-map[$boole-nand]  := $GDK-NAND;
  $function-map[$boole-nor]   := $GDK-OR-INVERT;  //---*** What should this be?
  $function-map[$boole-andc1] := $GDK-AND-INVERT;
  $function-map[$boole-andc2] := $GDK-AND-REVERSE;
  $function-map[$boole-orc1]  := $GDK-OR-INVERT;
  $function-map[$boole-orc2]  := $GDK-OR-REVERSE
end;
||#

;; FIXME: LOTS TO DO HERE!
(setf (aref *function-map* +boole-clr+)   +CFFI/CAIRO_OPERATOR_CLEAR+
      (aref *function-map* +boole-set+)   +CFFI/CAIRO_OPERATOR_SOURCE+
      (aref *function-map* +boole-1+)     +CFFI/CAIRO_OPERATOR_OVER+
      (aref *function-map* +boole-2+)     +CFFI/CAIRO_OPERATOR_DEST+

      (aref *function-map* +boole-c1+)    +CFFI/CAIRO_OPERATOR_OVER+  ;; FIXME
      (aref *function-map* +boole-c2+)    +CFFI/CAIRO_OPERATOR_OVER+  ;; FIXME

      (aref *function-map* +boole-and+)   +CFFI/CAIRO_OPERATOR_ADD+

      (aref *function-map* +boole-ior+)   +CFFI/CAIRO_OPERATOR_OVER+  ;; FIXME

      (aref *function-map* +boole-xor+)   +CFFI/CAIRO_OPERATOR_XOR+
      (aref *function-map* +boole-eqv+)   +CFFI/CAIRO_OPERATOR_ATOP+

      (aref *function-map* +boole-nand+)  +CFFI/CAIRO_OPERATOR_OVER+  ;; FIXME
      (aref *function-map* +boole-nor+)   +CFFI/CAIRO_OPERATOR_OVER+  ;; FIXME
      (aref *function-map* +boole-andc1+) +CFFI/CAIRO_OPERATOR_OVER+  ;; FIXME
      (aref *function-map* +boole-andc2+) +CFFI/CAIRO_OPERATOR_OVER+  ;; FIXME
      (aref *function-map* +boole-orc1+)  +CFFI/CAIRO_OPERATOR_OVER+  ;; FIXME
      (aref *function-map* +boole-orc2+)  +CFFI/CAIRO_OPERATOR_OVER+) ;; FIXME

#||
define sealed method establish-brush
    (medium :: <gtk-medium>, brush :: <standard-brush>, gcontext :: <GdkGC*>)
 => ()
  ignoring("establish-brush for <standard-brush>")
end method establish-brush;
||#

(defgeneric establish-brush (medium brush context)
  (:documentation
"
Brushes are used to fill in 2 dimensional areas with a specific color
or pattern.
"))


(defmethod establish-brush ((medium <gtk-medium>)
			    (brush  <standard-brush>)
			    cr)  ;; cairo_t
  (when (%brush-ink medium)
    (cffi/cairo_surface_destroy (%brush-ink medium)))
  (multiple-value-bind (color fill-type operation image)
      (decode-ink medium cr brush)
    (declare (ignore color))
    ;; FILL = solid, tiled, stippled...
    ;; If we have an image, set that up; otherwise use the color.
    (if image   ; "color" is never nil so check for image...
	;; We got an image back. It could have come from passing an <image>
	;; for the "brush", or from a <standard-brush> that contains an <image>
	;; in the tile, stipple, or foreground slot.
	(let* ((surface (GTK-IMAGE-TO-SURFACE image)))
	  ;; FIXME: BRUSH-TS-X/TS-Y? - MUST BE DONE IN THE CAIRO MATRIX?
	  (setf (%brush-ink medium) surface)
	  (setf (%brush-operation medium) operation)
	  (setf (%brush-fill-style medium) fill-type))
	;; else - use solid color
	(progn
	  (setf (%brush-ink medium) nil)
	  (setf (%brush-fill-style medium) fill-type)
	  (setf (%brush-operation medium)  operation)))))



#||
define sealed method establish-brush
    (medium :: <gtk-medium>, color :: <rgb-color>, gcontext :: <GdkGC*>) => ()
  ignoring("establish-brush");
  /*---*** Colors not implemented yet!
  gdk-gc-set-fill(gcontext, $GDK-SOLID);
  gdk-gc-set-function(gcontext, $function-map[$boole-set]);
  gdk-gc-set-foreground(gcontext, allocate-color(color, medium.%palette))
  */
end method establish-brush;
||#

(defmethod establish-brush ((medium <gtk-medium>) (color  <rgb-color>) cr)  ; cairo_t
  (declare (ignore cr))
  (when (%brush-ink medium)
    (cffi/cairo_surface_destroy (%brush-ink medium)))
  (setf (%brush-ink medium) nil)
  (setf (%brush-fill-style medium) +CFFI/GDK-SOLID+)
  (setf (%brush-operation medium)  (aref *function-map* +boole-1+)))


#||
define sealed method establish-brush
    (medium :: <gtk-medium>, color :: <contrasting-color>, gcontext :: <GdkGC*>) => ()
  ignoring("establish-brush for <contrasting-color>")
end method establish-brush;
||#

(defmethod establish-brush ((medium <gtk-medium>) (color <contrasting-color>) cr)  ; cairo_t
  (establish-brush medium (contrasting-color->color color) cr))


#||
define sealed method establish-brush
    (medium :: <gtk-medium>, brush :: <foreground>, gcontext :: <GdkGC*>) => ()
  ignoring("establish-brush");
  /*---*** Colors not implemented yet!
  gdk-gc-set-fill(gcontext, $GDK-SOLID);
  gdk-gc-set-function(gcontext, $GDK-COPY);
  gdk-gc-set-foreground(gcontext, medium.%foreground-color)
  */
end method establish-brush;
||#

(defmethod establish-brush ((medium <gtk-medium>) (brush <foreground>) cr)  ; cairo_t
  (when (%brush-ink medium)
    (cffi/cairo_surface_destroy (%brush-ink medium)))
  ;; FIXME: Can short-circuit this by checking for %foreground-ink
  (multiple-value-bind (fg-color fg-fill-style fg-operation fg-image)
      (decode-ink medium cr (medium-foreground medium))
    (declare (ignore fg-color))
    (setf (%brush-ink medium) (if fg-image (GTK-IMAGE-TO-SURFACE fg-image) nil))
    (setf (%brush-fill-style medium) fg-fill-style)
    (setf (%brush-operation medium)  fg-operation)))



#||
define sealed method establish-brush
    (medium :: <gtk-medium>, brush :: <background>, gcontext :: <GdkGC*>) => ()
  ignoring("establish-brush");
  /*---*** Colors not implemented yet!
  gdk-gc-set-fill(gcontext, $GDK-SOLID);
  gdk-gc-set-function(gcontext, $GDK-COPY);
  gdk-gc-set-foreground(gcontext, medium.%background-color)
  */
end method establish-brush;
||#

(defmethod establish-brush ((medium <gtk-medium>) (brush <background>) cr)  ; cairo_t
  (when (%brush-ink medium)
    (cffi/cairo_surface_destroy (%brush-ink medium)))
  ;; FIXME: Can short-circuit this by checking for %background-ink
  (multiple-value-bind (bg-color bg-fill-style bg-operation bg-image)
      (decode-ink medium cr (medium-background medium))
    (declare (ignore bg-color))
    (setf (%brush-ink medium) (if bg-image (GTK-IMAGE-TO-SURFACE bg-image) nil))
    (setf (%brush-fill-style medium) bg-fill-style)
    (setf (%brush-operation medium)  bg-operation)))



#||
define sealed method establish-pen 
    (medium :: <gtk-medium>, pen :: <standard-pen>, gcontext :: <GdkGC*>) => ()
  let width 
    = begin
	let width = pen-width(pen);
	when (pen-units(pen) == #"point")
	  width := width * display-pixels-per-point(display(medium))
	end;
	if (width < 2) 0 else truncate(width) end
      end;
  let dashes = pen-dashes(pen);
  let (dashed?, dash)
    = select (dashes by instance?)
	singleton(#f) => values(#f, #f);
	singleton(#t) => values(#t, #[4,4]);
	<vector>      => values(#t, dashes);
	<list>        => values(#t, as(<vector>, dashes));
      end;
  let cap-shape
    = select (pen-cap-shape(pen))
	#"butt"         => $GDK-CAP-BUTT;
	#"square"       => $GDK-CAP-PROJECTING;
	#"round"        => $GDK-CAP-ROUND;
	#"no-end-point" => $GDK-CAP-NOT-LAST;
      end;
  let joint-shape
    = select (pen-joint-shape(pen))
	#"miter" => $GDK-JOIN-MITER;
	#"none"  => $GDK-JOIN-MITER;
	#"bevel" => $GDK-JOIN-BEVEL;
	#"round" => $GDK-JOIN-ROUND;
      end;
  gdk-gc-set-line-attributes(gcontext, 
			     width,
			     if (dashed?) $GDK-LINE-ON-OFF-DASH else $GDK-LINE-SOLID end,
			     cap-shape, joint-shape);
  when (dashed?)
    ignoring("pen dashes option");
    // gdk-gc-set-dashes(gcontext, 0, dash)
  end
end method establish-pen;
||#

(defgeneric establish-pen (medium pen cr))


(defmethod establish-pen ((medium <gtk-medium>) (pen <standard-pen>) cr) ; cairo_t
  (let ((width (let ((width (pen-width pen)))
		 (when (eql (pen-units pen) :point)
		   (setf width
			 (* width
			    (display-pixels-per-point (display medium)))))
		 (if (< width 2) 1 (truncate width))))
	(dashes (pen-dashes pen)))
    (multiple-value-bind (dashed? dash)
	(typecase dashes
	  (null   (values nil nil))
	  (vector (values t dashes))
	  (list   (values t (apply #'vector dashes)))
	  ;; Just passing a single element would do here, but that's a Cairo
	  ;; peculiarity...
	  (t      (values t #(4 4))))
      (let ((cap-shape   (ecase (pen-cap-shape pen)
			   (:butt         +CFFI/CAIRO_LINE_CAP_BUTT+)
			   (:square       +CFFI/CAIRO_LINE_CAP_SQUARE+)
			   (:round        +CFFI/CAIRO_LINE_CAP_ROUND+)
			   (:no-end-point +CFFI/CAIRO_LINE_CAP_BUTT+)))
	    (joint-shape (ecase (pen-joint-shape pen)
			   (:miter +CFFI/CAIRO_LINE_JOIN_MITER+)
			   (:none  +CFFI/CAIRO_LINE_JOIN_MITER+)
			   (:bevel +CFFI/CAIRO_LINE_JOIN_BEVEL+)
			   (:round +CFFI/CAIRO_LINE_JOIN_ROUND+))))
	(cffi/cairo_set_line_width cr (coerce width 'double-float))
        (cffi/cairo_set_line_cap cr cap-shape)
	(cffi/cairo_set_line_join cr joint-shape)
	(when dashed?
          (let ((num-dashes (length dash)))
	    (cffi:with-foreign-object (dash-array :double num-dashes)
	      (loop for dash across dashes
		 for index from 0
		 do (setf (cffi:mem-aref dash-array :double index) (coerce dash 'double-float)))
	      ;; 'offset' is the offset into the dash pattern at which the
	      ;; stroke should start (always 0 for duim)
	      (let ((offset 0.0d0))
		(cffi/cairo_set_dash cr dash-array num-dashes offset)))))))))


#||
define sealed method establish-font
    (medium :: <gtk-medium>, font :: <gtk-font>, gcontext :: <GdkGC*>) => ()
  ignoring("establish-font");
  // gdk-gc-set-font(gcontext, font.%font-id)
end method establish-font;
||#

(defgeneric establish-font (medium font cr))

(defmethod establish-font ((medium <gtk-medium>) (font <gtk-font>) cr) ; cairo_t
  (declare (ignore cr))
  (ignoring "establish-font")
  ;; NOTE: <GTK-FONT> WRAPS A *NATIVE* FONT. SEE GTK-FONTS.
  ;; FIXME: IT MAY NOT BE NECESSARY TO ESTABLISH A FONT SINCE PANGO IS USED
  ;; TO DRAW TEXT. THINK ABOUT THIS.
  ;; WE SHOULD STILL "ESTABLISH" THE PANGO CONTEXT, AND CACHE IT IN "MEDIUM"
  ;; THIS *SHOULD* SPEED THINGS UP...
  ;; (gdk-gc-set-font gcontext (%font-id font))
  ;; TODO: Treat pangocontext/pangolayout similarly to the cairo_t. Do more
  ;; work here instead of in the draw-text method. Also - metrics.
  )



#||
/// Ink decoding

define generic decode-ink
    (medium :: <gtk-medium>, gcontext :: <GdkGC*>, brush)
 => (color :: <native-color>, fill-style, operation :: <integer>,
     image :: false-or(<image>));
||#

(defgeneric decode-ink (medium cr brush)
  (:documentation
"
Returns the values (colour fill-style operation image)
"))


#||
define sealed method decode-ink 
    (medium :: <gtk-medium>, gcontext :: <GdkGC*>, brush :: <foreground>)
 => (color :: <native-color>, fill-style, operation :: <integer>,
     image :: false-or(<image>))
  decode-ink(medium, gcontext, medium-foreground(medium))
end method decode-ink;
||#

(defmethod decode-ink ((medium <gtk-medium>) cr (brush <foreground>))
  (decode-ink medium cr (medium-foreground medium)))


#||
define sealed method decode-ink 
    (medium :: <gtk-medium>, gcontext :: <GdkGC*>, brush :: <background>)
 => (color :: <native-color>, fill-style, operation :: <integer>,
     image :: false-or(<image>))
  decode-ink(medium, gcontext, medium-background(medium))
end method decode-ink;
||#

(defmethod decode-ink ((medium <gtk-medium>) cr (brush <background>))
  (decode-ink medium cr (medium-background medium)))


#||
define sealed method decode-ink 
    (medium :: <gtk-medium>, gcontext :: <GdkGC*>, color :: <color>)
 => (color :: <native-color>, fill-style, operation :: <integer>,
     image :: false-or(<image>))
  values(allocate-color(color, medium.%palette), $GDK-SOLID, $boole-1, #f)
end method decode-ink;
||#

(defmethod decode-ink ((medium <gtk-medium>) cr (color <color>))
  (declare (ignore cr))
  (values color +CFFI/GDK-SOLID+ +boole-1+ nil))


#||
define sealed method decode-ink
    (medium :: <gtk-medium>, gcontext :: <GdkGC*>, color :: <contrasting-color>)
 => (color :: <native-color>, fill-style, operation :: <integer>,
     image :: false-or(<image>))
  let color = contrasting-color->color(color);
  values(allocate-color(color, medium.%palette), $GDK-SOLID, $boole-1, #f)
end method decode-ink;
||#

(defmethod decode-ink ((medium <gtk-medium>) cr (color <contrasting-color>))
  (declare (ignore cr))
  (let ((color (contrasting-color->color color)))
    (values color +CFFI/GDK-SOLID+ +boole-1+ nil)))

;;;;
;;;; IMAGE DECODING
;;;;

#||

Cairo defines the following image formats (see CAIRO_FORMAT_T):-

 CAIRO_FORMAT_ARGB32
    each pixel is a 32-bit quantity, with alpha in the upper 8 bits, then red, then green, then blue.
    The 32-bit quantities are stored native-endian. Pre-multiplied alpha is used. (That is, 50%
    transparent red is 0x80800000, not 0x80ff0000.) (Since 1.0)

    From Wikipedia (assuming an alpha of 0.5):

        However, if this pixel uses premultiplied alpha, all of the RGB values (0, 1, 0) are multiplied
        by 0.5 and then the alpha is appended to the end to yield (0, 0.5, 0, 0.5). In this case, the
        0.5 value for the G channel actually indicates 100% green intensity (with 50% opacity). For this
        reason, knowing whether a file uses premultiplied or straight alpha is essential to correctly
        process or composite it.

 CAIRO_FORMAT_RGB24
    each pixel is a 32-bit quantity, with the upper 8 bits unused. Red, Green, and Blue are stored
    in the remaining 24 bits in that order. (Since 1.0)

 CAIRO_FORMAT_A8
    each pixel is a 8-bit quantity holding an alpha value. (Since 1.0)

 CAIRO_FORMAT_A1
    each pixel is a 1-bit quantity holding an alpha value. Pixels are packed together into 32-bit
    quantities. The ordering of the bits matches the endianess of the platform. On a big-endian
    machine, the first pixel is in the uppermost bit, on a little-endian machine the first pixel is
    in the least-significant bit. (Since 1.0)

 CAIRO_FORMAT_RGB16_565
    each pixel is a 16-bit quantity with red in the upper 5 bits, then green in the middle 6 bits,
    and blue in the lower 5 bits. (Since 1.2)

 CAIRO_FORMAT_RGB30
    like RGB24 but with 10bpc. (Since 1.12)

||#

#||
define sealed method decode-ink
    (medium :: <gtk-medium>, gcontext :: <GdkGC*>, pattern :: <stencil>)
 => (color :: <native-color>, fill-style, operation :: <integer>,
     image :: false-or(<image>))
  not-yet-implemented("decode-ink");
  /*---*** Not yet implemented!
  let cache = medium.%ink-cache;
  let bitmap :: false-or(<image>)
    = gethash(cache, pattern)
      | begin
	  let (array, colors) = decode-pattern(pattern);
	  let width   :: <integer> = image-width(pattern);
	  let height  :: <integer> = image-height(pattern);
	  let ncolors :: <integer> = size(colors);
	  //---*** Should we create a DIB here or what?
	  let pixels   :: <simple-object-vector> = make(<simple-vector>, size: ncolors);
	  let pixarray :: <array> = make(<array>, dimensions: list(width, height));
	  without-bounds-checks
	    for (n :: <integer> from 0 below ncolors)
	      let pixel = decode-ink(medium, gcontext, colors[n]);
	      pixels[n] := pixel
	    end;
	    for (y :: <integer> from 0 below height)
	      for (x :: <integer> from 0 below width)
		pixarray[y,x] := pixels[array[y,x]]
	      end
	    end
	  end;
	  //---*** Fill in the DIB
	  let bitmap = list(pixels, pixarray);
	  gethash(cache, pattern) := bitmap;
	  bitmap
	end;
  values($native-white, $GDK-SOLID, $boole-1, bitmap)
  */
end method decode-ink;
||#

(defun stencil->a8 (medium cr stencil)
"
Returns a FFI pointer to a block of data representing the stencil
in the A8 image format; each pixel in the resulting data is an
8-bit value which represents only an opacity.

A DUIM <stencil> is a two-dimensional array where each array element
is a float between 0.0 and 1.0 representing the opacity of that pixel.
"
  (declare (ignore cr medium))
  (let ((width  (image-width stencil))
	(height (image-height stencil)))
    (multiple-value-bind (array colors transform)
	(decode-pattern stencil)
      ;; FIXME: DO WHAT WITH THE TRANSFORM?
      (declare (ignore colors transform))
      (let* ((stride (cffi/cairo_format_stride_for_width +CFFI/CAIRO_FORMAT_A8+ width))
	     ;; FIXME: THIS IS NEVER RELEASED
	     (data   (cffi:foreign-alloc :uint8 :count (* height stride) :initial-element 0)))
	(let ((index 0))
	  (loop for y from 0 below height
	     do (loop for x from 0 below width
	  	   do (setf (cffi:mem-aref data :uint8 index)
			    (float->8-bits (aref array y x))
			    index (+ index 1))
		   finally (when (< width stride)
			     ;; Increase index so we continue populating the native
			     ;; array at the right place...
			     (setf index (* stride y))))))
	data))))


(defun pattern->argb32 (medium cr pattern)
"
Returns an FFI pointer to a block of data representing the pattern
in the ARGB32 image format; each pixel in the resulting data is a
32 bit value representing pre-multiplied alpha colour values.

A DUIM <pattern> is a two-dimensional array where each array element
is the index into an array of <color>s which represents the colour
of that pixel.
"
  (let ((width  (image-width pattern))
	(height (image-height pattern)))
    (multiple-value-bind (array colors transform)
	(decode-pattern pattern)
      (declare (ignore transform))
      ;; STRIDE is in bytes; for an image with width=11, stride=44.
      (let* ((stride (cffi/cairo_format_stride_for_width +CFFI/CAIRO_FORMAT_ARGB32+ width))
	     ;; FIXME: THIS IS NEVER RELEASED -- SHOULD BE RELEASED IN "CLEAR-INK-CACHE".
	     (data   (cffi:foreign-alloc :uchar :count (* height stride) :initial-element 0)))
	(let ((index 0)
	      (little-endian t)
	      (big-endian nil))
	  (loop for y from 0 below height
	     do (loop for x from 0 below width
                   do (multiple-value-bind (red green blue alpha)
			  ;; Assume only <color> inks here, but not necessarily direct inks; could
			  ;; be <foreground> or <background> indirect inks.
			  ;; FIXME: CAN THEY EVER BE PIXMAPS? SHOULD THEY BE ABLE TO BE PIXMAPS?
			  (color-rgb (multiple-value-bind (color fill-style operation image)
					 (decode-ink medium cr (SEQUENCE-ELT colors (aref array y x)))
				       (declare (ignore fill-style operation image))
				       color))
			;; Premultiply color values, add as 8-bit values
			;; This will need swapping around for a big-endian machine. How to tell?
			;; sbcl has a :LITTLE-ENDIAN *feature*, not sure about CCL (or others).
			(when little-endian
			  (setf (cffi:mem-aref data :uchar (+ index 0)) (float->8-bits (* blue alpha)))
			  (setf (cffi:mem-aref data :uchar (+ index 1)) (float->8-bits (* green alpha)))
			  (setf (cffi:mem-aref data :uchar (+ index 2)) (float->8-bits (* red alpha)))
			  (setf (cffi:mem-aref data :uchar (+ index 3)) (float->8-bits alpha)))
			(when big-endian
			  (setf (cffi:mem-aref data :uchar (+ index 0)) (float->8-bits alpha))
			  (setf (cffi:mem-aref data :uchar (+ index 1)) (float->8-bits (* red alpha)))
			  (setf (cffi:mem-aref data :uchar (+ index 2)) (float->8-bits (* green alpha)))
			  (setf (cffi:mem-aref data :uchar (+ index 3)) (float->8-bits (* blue alpha))))
			(incf index 4))
		   ;; width is in px; stride is 4 bytes per px.
		   finally (when (< (* width 4) stride)
			     ;; Increase index so we continue populating the native
			     ;; array at the right place...
			     (setf index (* stride y))))))
	data))))


(defmethod decode-ink ((medium <gtk-medium>) cr (pattern <stencil>))
  ;; => (color :: <native-color>, fill-style, operation :: <integer>,
  ;;     image :: false-or(<image>))
  (let ((width  (image-width  pattern))
	(height (image-height pattern)))
    ;; XXX: Seems a bit round-about just to get the transform...
    (multiple-value-bind (array colors transform)
	(decode-pattern pattern)
      (declare (ignore array colors))
      (let* ((cache  (%ink-cache medium))
	     (bitmap (or (gethash pattern cache)
			 ;; <stencil>s contain only opacities
			 (let* ((a8-data (stencil->a8 medium cr pattern))
				(stride  (cffi/cairo_format_stride_for_width +CFFI/CAIRO_FORMAT_A8+ width))
				(bitmap  (make-instance '<gtk-image>
							:data a8-data
							:format +CFFI/CAIRO_FORMAT_A8+
							:width width :height height
							:stride stride
							:transform transform)))
			   (setf (gethash pattern cache) bitmap)) )))
	(let ((image-surface (cffi/cairo_image_surface_create_for_data (%data   bitmap)
								       (%format bitmap)
								       (%width  bitmap)
								       (%height bitmap)
								       (%stride bitmap))))
	  ;; FIXME: what should the fill-style be?
	  (values *white*
		  +CFFI/GDK-SOLID+
		  +boole-1+
		  ;; FIXME: WE LOSE THE TRANSFORM HERE. DOES THAT MATTER?
		  (make-instance '<gtk-pixmap>
				 :surface image-surface
				 :width   width
				 :height  height
				 :medium  medium)))))))


#||
define sealed method decode-ink
    (medium :: <gtk-medium>, gcontext :: <GdkGC*>, pixmap :: <pixmap>)
 => (color :: <native-color>, fill-style, operation :: <integer>,
     image :: false-or(<image>))
  not-yet-implemented("decode-ink for <pixmap>")
end method decode-ink;
||#

(defmethod decode-ink ((medium <gtk-medium>) cr (pixmap <pixmap>))
  (declare (ignore cr))
  ;; Any <pixmap> should be a <gtk-pixmap> of some kind... note that
  ;; the <gtk-image>s being created in other parts of this code serve
  ;; as a Cairo surface for a <gtk-pixmap>.
  (values *white* +CFFI/GDK-SOLID+ +boole-1+ pixmap))


(defmethod decode-ink ((medium <gtk-medium>) cr (pattern <pattern>))
  ;; => (color :: <native-color>, fill-style, operation :: <integer>,
  ;;     image :: false-or(<image>))
  (let ((width  (image-width  pattern))
	(height (image-height pattern)))
    ;; XXX: Seems a bit round-about just to get the transform...
    (multiple-value-bind (array colors transform)
	(decode-pattern pattern)
      (declare (ignore array colors))
      (let* ((cache  (%ink-cache medium))
	     (bitmap (or (gethash pattern cache)
			 ;; A <pattern> is a portable bitmap of arbitrary colors
			 (let* ((argb32-data (pattern->argb32 medium cr pattern))
				(stride  (cffi/cairo_format_stride_for_width +CFFI/CAIRO_FORMAT_ARGB32+ width))
				(bitmap  (make-instance '<gtk-image>
							:data argb32-data
							:format +CFFI/CAIRO_FORMAT_ARGB32+
							:width width :height height
							:stride stride
							:transform transform)))
			   (setf (gethash pattern cache) bitmap)) )))
	;; FIXME: what should the fill-style be?
	(values *white*
		+CFFI/GDK-SOLID+
		+boole-1+
		(make-instance '<gtk-pixmap>
			       :surface (cffi/cairo_image_surface_create_for_data (%data   bitmap)
										  (%format bitmap)
										  (%width  bitmap)
										  (%height bitmap)
										  (%stride bitmap))
			       :width   width
			       :height  height
			       :medium  medium))))))


#||
define sealed method decode-ink
    (medium :: <gtk-medium>, gcontext :: <GdkGC*>, brush :: <standard-brush>)
 => (color :: <native-color>, fill-style, operation :: <integer>,
     image :: false-or(<image>))
  let (color :: <native-color>, fill-style, operation :: <integer>, pattern)
    = decode-ink
        (medium, gcontext,
	 brush-tile(brush) | brush-stipple(brush) | brush-foreground(brush));
  // ignore(operation);
  values(color, fill-style, brush-mode(brush), pattern)
end method decode-ink;
||#

;;; XXX: BRUSH-MODE === OPERATION...

(defmethod decode-ink ((medium <gtk-medium>) cr (brush <standard-brush>))
  (multiple-value-bind (color fill-style operation pattern)
      (decode-ink medium
		  cr
		  (or (brush-tile brush)
		      (brush-stipple brush)
		      (brush-foreground brush)))
    (declare (ignore operation))
    (values color fill-style (brush-mode brush) pattern)))



#||
/// Clipping region decoding

define sealed method compute-clip-mask
    (medium :: <gtk-medium>) => (mask)
  let sheet   = medium-sheet(medium);
  let sregion = sheet-device-region(sheet);
  let mregion = medium-clipping-region(medium);
  let valid? = #t;
  if (sregion == $nowhere | mregion == $nowhere)
    #"none"
  else
    let (sleft, stop, sright, sbottom) = box-edges(sregion);
    unless (mregion == $everywhere)
      let (mleft, mtop, mright, mbottom) = box-edges(mregion);
      let (mleft, mtop, mright, mbottom)
	= transform-box(sheet-device-transform(sheet), 
			mleft, mtop, mright, mbottom);
      let (v, left, top, right, bottom)
	= ltrb-intersects-ltrb?(sleft, stop, sright, sbottom,
				mleft, mtop, mright, mbottom);
      sleft := left;
      stop  := top;
      sright  := right;
      sbottom := bottom;
      valid? := v
    end;
    if (valid?)
      vector(sleft, stop, sright, sbottom)
    else
      #"none"
    end
  end
end method compute-clip-mask;
||#

(defgeneric compute-clip-mask (medium))

(defmethod compute-clip-mask ((medium <gtk-medium>))
  (let* ((sheet   (medium-sheet medium))
	 (sregion (sheet-device-region sheet))
	 (mregion (medium-clipping-region medium))
	 (valid? t))
    (if (or (eql sregion *nowhere*) (eql mregion *nowhere*))
	:none
	;; else
	(multiple-value-bind (sleft stop sright sbottom)
	    (box-edges sregion)
	  (unless (eql mregion *everywhere*)
	    (multiple-value-bind (mleft mtop mright mbottom)
		(box-edges mregion)
	      (multiple-value-bind (mleft mtop mright mbottom)
		  (transform-box (sheet-device-transform sheet)
				 mleft mtop mright mbottom)
		(multiple-value-bind (v left top right bottom)
		    (ltrb-intersects-ltrb? sleft stop sright sbottom
					   mleft mtop mright mbottom)
		  (setf sleft left
			stop top
			sright right
			sbottom bottom
			valid? v)))))
	  (if valid?
	      (vector sleft stop sright sbottom)
	      :none)))))
