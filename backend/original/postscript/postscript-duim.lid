Library:      postscript-duim
Synopsis:     DUIM postscript backend
Author:       Scott McKay, Andy Armstrong
Files:  library
	module
        postscript-port
        postscript-medium
        laserwriter-metrics
Other-files:   Open-Source-License.txt
Major-version: 2
Minor-version: 1
Copyright:    Original Code is Copyright (c) 1995-2004 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND

