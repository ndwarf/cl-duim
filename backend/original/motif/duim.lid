Library:   DUIM
Synopsis:  DUIM core + Motif backend
Author:	   Scott McKay
Files:	   duim-library
Major-version: 2
Minor-version: 1
Executable:    DxDUIM
Base-address:  0x65c00000
Copyright:    Original Code is Copyright (c) 1995-2004 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND
Other-files: Open-Source-License.txt

