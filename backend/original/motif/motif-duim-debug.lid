Library:   motif-duim
Synopsis:  DUIM back-end for Motif (debug version)
Author:    Scott McKay
Files:  library
	module
	xm-utils
	xm-port
	xm-mirror
	xm-medium
	xm-colors
	xm-fonts
	xm-draw
	xm-keyboard
	xm-events
	xm-framem
	xm-display
	xm-pixmaps
	xm-top
	xm-gadgets
        xm-controls
	xm-menus
        xm-dialogs
	xm-help
	xm-clipboard
        xm-debug
Major-version: 2
Minor-version: 1
Copyright:    Original Code is Copyright (c) 1995-2004 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND
Other-files: Open-Source-License.txt

