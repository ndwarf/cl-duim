Library:      DUIM
Synopsis:     DUIM core + Macintosh backend
Author:	      Andy Armstrong
Files:	   duim-library
Major-version: 2
Minor-version: 1
Copyright:    Original Code is Copyright (c) 1995-2004 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND
Other-files: Open-Source-License.txt

