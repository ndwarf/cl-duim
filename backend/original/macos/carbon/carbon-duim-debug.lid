Library:      carbon-duim
Synopsis:     DUIM back-end for the Macintosh (debug version)
Author:       Andy Armstrong
Files:  library
	module
	carbon-utils
	carbon-port
	carbon-mirror
	carbon-medium
	carbon-colors
	carbon-fonts
	carbon-draw
	carbon-keyboard
	carbon-events
	carbon-framem
	carbon-display
	carbon-pixmaps
	carbon-top
	carbon-gadgets
	carbon-menus
        carbon-dialogs
	carbon-help
	carbon-clipboard
        carbon-debug
Other-files:   Open-Source-License.txt
Major-version: 2
Minor-version: 1
Copyright:    Original Code is Copyright (c) 1995-2004 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND

