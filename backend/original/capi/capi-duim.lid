Library:      CAPI-DUIM
Synopsis:     CAPI backend for DUIM
Author:	      Andy Armstrong
Files:	  library
	  module
          patches/toolkit-hacks.lisp
          patches/gp-patches.lisp
          patches/capi-hacks.lisp
          patches/tklib-hacks.lisp
          patches/event-handling-pane.lisp
          patches/import-hacks.lisp
	  imports
	  defs
          port
          frame-manager
          display
          medium
	  pixmap
          mirror
          events
          gadgets
          menus
          top-level
          dialogs
          clipboard
	  help
Other-files: Open-Source-License.txt
Copyright:    Original Code is Copyright (c) 1995-2004 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND

