;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

;;; Backend systems
;;; Currently only GTK implemented

(asdf:defsystem :backend
  :description "Empty placeholder for subsystems"
  :long-description "ASDF does not allow 'bare' sub-systems, so we need this as a placeholder. At some point we should consider moving win32 and gtk2 out of this file and into ASDF systems of their own, in their respective directories, e.g. gtk2.asd and win32.asd. Having more than one backend system defined here would be unwieldly. When that happens we can do away with this system and manage everything from the top level duim.asd.")

(asdf:defsystem :backend/gtk2
  :description "DUIM GTK2 back-end"
  :license     "BSD-2-Clause"
  :depends-on (#:duim
	       #:duim-extended
	       #:cffi)
  :components
  ((:module "gtk2"
	    :components
	    ((:file "package")
	     (:file "gtk2-utils")
 
	     (:file "cffi-types+constants")
	     (:file "cffi-glist")
	     (:file "cffi-structures")
	     (:file "cffi-window-api")
	     (:file "cffi-widget-api")
	     (:file "cffi-drawing-api")
	     (:file "cffi-menu-api")
	     (:file "cffi-event-api")
	     (:file "cffi-api")
	     (:file "cffi-cairo-api")
	     (:file "cffi-pango-cairo-api")

	     (:file "classes")
	     (:file "port")
	     (:file "mirror")
	     (:file "fonts")
	     (:file "medium")
	     (:file "colors")
	     (:file "draw")
	     (:file "keyboard")
	     (:file "events")
	     (:file "framem")
	     (:file "display")
	     (:file "pixmaps")
	     (:file "gadgets")
	     (:file "controls")
	     (:file "top")
	     (:file "menus")
	     (:file "dialogs")
	     #-(and)(:file "help")
	     (:file "clipboard")
	     (:file "debug")

	     ;; todo: put into own module (with cffi stuff)
	     #-(and)
	     (:file "test-menus"))
	    :serial t)))

(asdf:defsystem :backend/debug
  :description "Debug back end"
  :license "DCS-2-Clause"
  :depends-on (#:duim #:duim-extended)
  :in-order-to ((test-op (test-op "backend/debug/test")))
  :components
  ((:module "debug"
    :components
    ((:file "package")
     (:file "port")
     (:file "colors")
     )
    :serial t)))

(asdf:defsystem :backend/debug/test
  :description "Unit tests for debug backend"
  :licence "See backend/debug"
  :depends-on (#:duim-utilities #:fiveam)
  :pathname "debug/test/"
  :serial t
  :components
  ((:file "test-package")
   (:file "main")
   (:file "port"))
  :perform (test-op (o s)
		    (uiop:symbol-call :fiveam :run!
				      (uiop:find-symbol* :all-tests
							 :debug-backend-test))))

