Library:      win32-scribble
Author:       Scott McKay
Synopsis:     Win32 version of the DUIM scribble application
Files:  library
	scribble
        printing
	run
Other-files: Open-Source-License.txt
Linker-Options: $(guilflags)
Start-Function: scribble
Copyright:    Original Code is Copyright (c) 1995-2004 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND
Other-files: README.html
    Open-Source-License.txt

