Library:      duim-resource-example
Synopsis:     Example using windows resources in DUIM
Author:       Roman Budzianowski, Andy Armstrong, Scott McKay
Files: 	library
	resdecl
	example
Linker-Options: $(guilflags)
C-Header-Files:	resource.h
RC-Files:	example.rc
Other-files:    Open-Source-License.txt
Compilation-Mode: loose
Copyright:    Original Code is Copyright (c) 1995-2004 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND

