;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

(asdf:defsystem :tic-tac-toe
    :description  "DUIM tic-tac-toe example"
    :version      "2.1"
    :license      "BSD-2-Clause"
    :author       "Scott McKay, Andy Armstrong (Lisp port: Duncan Rose <duncan@robotcat.demon.co.uk>)"
    :depends-on   (#:alexandria
		   #:duim
		   #:duim/backend)
    :serial t
    :components
    ((:file "package")
     (:file "common")
     (:file "frame")
     (:file "game")))

