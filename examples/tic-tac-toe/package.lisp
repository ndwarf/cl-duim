;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

(defpackage #:tic-tac-toe
  (:use #:common-lisp
	#:alexandria
	#:duim)
  (:export #:<ttt-frame>
	   #:play-tic-tac-toe))
