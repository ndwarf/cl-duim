;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: TIC-TAC-TOE -*-
(in-package #:tic-tac-toe)

#||
Module:       tic-tac-toe
Author:       Jonathon Lee
Synopsis:     The classic game
||#

(defgeneric choose-players (frame type))
(defgeneric ttt-set-players (frame new-players))
(defgeneric display-winner (frame outcome winner))
(defgeneric about-ttt (frame))
(defgeneric play-tic-tac-toe ())

#||
define constant $application-name    = "Functional Developer Tic-Tac-Toe";
define constant $application-version = "version 1.0";
||#

(defparameter *application-name* "Functional Developer Tic-Tac-Toe")
(defparameter *application-version* "version 1.0")

#||
define constant $ttt-square-color    = $red;
define constant $ttt-edge-color      = $black;
define constant $tic-tac-toe-pen     = make(<pen>, width: 3, cap-shape: #"round");
||#

(defparameter *ttt-square-color* *red*)
(defparameter *ttt-edge-color* *black*)
(defparameter *tic-tac-toe-pen* (make-pen :width 3 :cap-shape :round))

#||
define constant $ttt-square-size     = 40;
define constant $ttt-square-rows     = 3;
define constant $ttt-square-columns  = 3;
define constant $max-number-of-moves = $ttt-square-rows * $ttt-square-columns;
||#

(defparameter *ttt-square-size* 40)
(defparameter *ttt-square-rows* 3)
(defparameter *ttt-square-columns* 3)
(defparameter *max-number-of-moves* (* *ttt-square-rows* *ttt-square-columns*))

#||
define constant <square-occupied> = false-or(limited(<integer>, min: 1, max: 2));

define class <ttt-square> (<simple-pane>)
  slot square-coordinates :: <vector>, init-keyword: square-coordinates:;  
  slot square-occupied :: <square-occupied> = #f;
end class <ttt-square>;
||#

(defclass <ttt-square> (<simple-pane>)
  ((square-coordinates :type vector :initarg :square-coordinates :accessor square-coordinates)
   (square-occupied :initform nil :accessor square-occupied)))

#||
define method ttt-square-setter 
    (square :: <ttt-square>, player :: <square-occupied>) => ()
  square.square-occupied := player;
  if (sheet-mapped?(square))
    repaint-sheet(square, $everywhere);
  end;
end method ttt-square-setter;
||#

(defmethod ttt-square-setter ((square <ttt-square>) player)
  (setf (square-occupied square) player)
  (when (sheet-mapped? square)
    (repaint-sheet square *everywhere*)))

#||
define method draw-ttt-piece 
    (ttt-square :: <ttt-square>, medium :: <medium>) => ()
  when (square-occupied(ttt-square))
    let (left, top, right, bottom) = box-edges(ttt-square);
    let half-width  = floor/(right - left, 2);
    let half-height = floor/(bottom - top, 2);
    let x-radius = floor/(right - left, 3);
    let y-radius = floor/(bottom - top, 3);
    let x-center = left + half-width;
    let y-center = top  + half-height;
    let color = select (square-occupied(ttt-square))
		  1 => $white;
		  2 => $black;
		end;
    with-drawing-options (medium, brush: color, pen: $tic-tac-toe-pen)
      if (square-occupied(ttt-square) = 2)
	draw-ellipse(medium, x-center, y-center, 
		     x-radius, 0, 0, y-radius, filled?: #f)
      else
	draw-line(medium, 
                  x-center - x-radius, y-center - y-radius,
                  x-center + x-radius, y-center + y-radius);
	draw-line(medium, 
                  x-center - x-radius, y-center + y-radius,
                  x-center + x-radius, y-center - y-radius);
      end if;
      force-display(medium)
    end;
  end;
end method draw-ttt-piece;
||#

(defmethod draw-ttt-piece ((ttt-square <ttt-square>) (medium <medium>))
  (when (square-occupied ttt-square)
    (multiple-value-bind (left top right bottom)
	(box-edges ttt-square)
      (let* ((half-width (floor (- right left) 2))
	     (half-height (floor (- bottom top) 2))
	     (x-radius (floor (- right left) 3))
	     (y-radius (floor (- bottom top) 3))
	     (x-center (+ left half-width))
	     (y-center (+ top half-height))
	     (color (ecase (square-occupied ttt-square)
		      (1 *white*)
		      (2 *black*))))
	(with-drawing-options (medium :brush color :pen *tic-tac-toe-pen*)
	  (if (= (square-occupied ttt-square) 2)
	      (draw-ellipse medium x-center y-center
			    x-radius 0 0 y-radius :filled? nil)
	      ;; else
	      (progn
		(draw-line medium
			   (- x-center x-radius) (- y-center y-radius)
			   (+ x-center x-radius) (+ y-center y-radius))
		(draw-line medium
			   (- x-center x-radius) (+ y-center y-radius)
			   (+ x-center x-radius) (- y-center y-radius))))
	  (force-display medium))))))

#||
define method handle-repaint
    (ttt-square :: <ttt-square>, medium :: <medium>, region :: <region>) => ()
  let (left, top, right, bottom) = box-edges(ttt-square);
  with-drawing-options (medium, brush: $ttt-square-color)
    draw-rectangle(medium, left, top, right, bottom, filled?: #t);
  end;
  with-drawing-options (medium, brush: $ttt-edge-color)
    draw-rectangle(medium, left, top, right, bottom, filled?: #f);
  end;
  if (ttt-square.square-occupied)
    draw-ttt-piece(ttt-square, medium);
  end if;
end method handle-repaint;
||#

(defmethod handle-repaint ((ttt-square <ttt-square>) (medium <medium>) (region <region>))
  (multiple-value-bind (left top right bottom)
      (box-edges ttt-square)
    (with-drawing-options (medium :brush *ttt-square-color*)
      (draw-rectangle medium left top right bottom :filled? t))
    (with-drawing-options (medium :brush *ttt-edge-color*)
      (draw-rectangle medium left top right bottom :filled? nil))
    (when (square-occupied ttt-square)
      (draw-ttt-piece ttt-square medium))))

#||
define method display-message
    (frame :: <frame>, message :: <string>, #key beep? = #f)
 => ()
  frame-status-message(frame) := message;
  when (beep?) beep(frame) end
end method display-message;
||#

(defmethod display-message ((frame <frame>) (message string) &key (beep? nil))
  (setf (frame-status-message frame) message)
  (when beep?
    (beep frame)))

#||
define method handle-event
    (ttt-square :: <ttt-square>, event :: <button-release-event>) => ()
  let frame = sheet-frame(ttt-square);
  if (frame)
    if (event-button(event) == $left-button &
	  ~(ttt-square.square-occupied))
      select (frame.game.turn)
        3                 => display-message(frame, "Game is over");
        *computer-player* => display-message(frame, "Its not your turn", beep?: #t);
        otherwise         => add-ttt-piece(ttt-square, frame);
      end select;
    end if;
  end if;
end method handle-event;
||#

(defmethod handle-event ((ttt-square <ttt-square>) (event <button-release-event>))
  (let ((frame (sheet-frame ttt-square)))
    (when frame
      (when (and (eql (event-button event) +left-button+)
		 (not (square-occupied ttt-square)))
	(case (turn (game frame))
	  (3 (display-message frame "Game is over"))
	  (*computer-player* (display-message frame "It's not your turn" :beep? t))
	  (t (add-ttt-piece ttt-square frame)))))))

#||
define frame <ttt-frame> (<simple-frame>)
  constant slot game :: <ttt-game> = make(<ttt-game>),
    init-keyword: game:;
  pane ttt-frame-table-pane (frame)
    make(<grid-layout>,
         contents: ttt-frame-squares(frame),
         cell-space-requirement: make(<space-requirement>,
                                      width: $ttt-square-size,
                                      height: $ttt-square-size),
         spacing: 0);
  pane buttons (frame) 
    make(<push-button>,
	 label: "New game",
	 activate-callback: method (button)
			      new-game(sheet-frame(button))
			    end);
  pane status (frame)
    make(<status-bar>);
  pane main-layout (frame)
    vertically (spacing: 10)
      horizontally (spacing: 2, x-alignment: #"center")
        frame.buttons;
      end;
      make(<drawing-pane>,
	   children: vector(ttt-frame-table-pane(frame)),
	   foreground: $ttt-square-color); 
    end;
  pane exit-button (frame)
    make(<push-menu-button>,
	 label: "E&xit",
	 activate-callback: method (button)
			      exit-frame(sheet-frame(button));
			    end);
  pane file-menu (frame)
    make(<menu>,
	 label: "&File",
	 children: 
	   vector(make(<menu-button>,
		       label: "New Game",
		       activate-callback: method (button)
					    new-game(sheet-frame(button))
					  end),
		  make(<push-menu-box>,     
		       children: vector(frame.exit-button))));
  pane options-menu (frame)
    make(<menu>,
	 label: "&Options",
	 children: 
	   vector(make(<radio-menu-box>,
		       items: #(#("Play &X",      #"x"),
			        #("Play &Y",      #"o"),
                                #("&Two Players", #"two-players")),
		       label-key: first,
		       value-key: second,
		       value-changed-callback:
			 method (b)
			   ttt-set-players(sheet-frame(b), gadget-value(b))
			 end)));
  pane help-menu (frame)
    make(<menu>,
	 label: "Help",
	 children: 
	   vector(make(<push-menu-button>,
		       label: format-to-string("About %s", $application-name),
		       activate-callback: 
			 method (button)
			   about-ttt(sheet-frame(button))
			 end)));
  menu-bar (frame)
    make(<menu-bar>,
	 children: vector(frame.file-menu, 		       
			  frame.options-menu,
			  frame.help-menu));
  layout (frame) frame.main-layout;
  status-bar (frame) frame.status;
  keyword title: = $application-name
end frame <ttt-frame>;
||#

(define-frame <ttt-frame> (<simple-frame>)
  ((game :type <ttt-game> :initform (make-instance '<ttt-game>) :initarg :game :reader game)
   (:pane ttt-frame-table-pane (frame)
	  (make-pane '<grid-layout>
		     :contents (ttt-frame-squares frame)
		     :cell-space-requirement (make-space-requirement
							    :width *ttt-square-size*
							    :height *ttt-square-size*)
		     :spacing 0))
   (:pane buttons (frame)
	  (make-pane '<push-button>
		     :label "New game"
		     :activate-callback #'(lambda (button)
					    (new-game (sheet-frame button)))))
   (:pane status (frame)
	  (make-pane '<status-bar>))
   (:pane main-layout (frame)
	  (vertically (:spacing 10)
	    (horizontally (:spacing 2 :x-alignment :center)
	      (buttons frame))
	    (make-pane '<drawing-pane>
		       :children (vector (ttt-frame-table-pane frame))
		       :foreground *ttt-square-color*)))
   (:pane exit-button (frame)
	  (make-pane '<push-menu-button>
		     :label "E&xit"
		     :activate-callback #'(lambda (button)
					    (exit-frame (sheet-frame button)))))
   (:pane file-menu (frame)
	  (make-pane '<menu>
		     :label "&File"
		     :children
		     (vector (make-pane '<menu-button>
					:label "New Game"
					:activate-callback
					#'(lambda (button)
					    (new-game (sheet-frame button))))
			     (make-pane '<push-menu-box>
					:children (vector (exit-button frame))))))
   (:pane options-menu (frame)
	  (make-pane '<menu>
		     :label "&Options"
		     :children
		     (vector (make-pane '<radio-menu-box>
;;; FIXME: SHOULD BE ABLE TO MAKE MENU-BOXes WITH CONTENTS THAT ARE LISTS!
;;; 					:items #((list "Play &X" :x)
;;; 						 (list "Play &Y" :o)
;;; 						 (list "&Two Players" :two-players))
;;; 					:label-key #'first
;;; 					:value-key #'second
					:items #(#("Play &X" :x)
						 #("Play &Y" :o)
						 #("&Two Players" :two-players))
					:label-key (rcurry #'aref 0)
					:value-key (rcurry #'aref 1)
					:value-changed-callback
					#'(lambda (b)
					    (ttt-set-players (sheet-frame b) (gadget-value b)))))))
   (:pane help-menu (frame)
	  (make-pane '<menu>
		     :label "Help"
		     :children
		     (vector (make-pane '<push-menu-button>
					:label (format nil "About ~a" *application-name*)
					:activate-callback
					#'(lambda (button)
					    (about-ttt (sheet-frame button)))))))
   (:menu-bar (frame)
	      (make-pane '<menu-bar>
			 :children
			 (vector (file-menu frame)
				 (options-menu frame)
				 (help-menu frame))))
   (:layout (frame)
	    (main-layout frame))
   (:status-bar (frame)
		(status frame)))
  (:default-initargs :title *application-name*))


#||
// Return something useful for the contents: init arg of <grid-pane>.
define method ttt-frame-squares
    (frame :: <ttt-frame>) => ()
  let array = frame.game.ttt-board
                | begin
                    frame.game.ttt-board := make-ttt-board();
                  end;
  map-as(<vector>,
         method (row)
           map-as(<vector>,
                  method (col) array[row, col] end,
                  range(from: 0, below: dimension(array, 1)))
         end,
         range(from: 0, below: dimension(array, 0)))
end method ttt-frame-squares;
||#

;;; FIXME: SHOULD "RANGE" BE EXPORTED FROM DUIM? PROBABLY BEST TO GET RID OF IT
;;; ALTOGETHER...
(defmethod ttt-frame-squares ((frame <ttt-frame>))
  (let ((array (or (ttt-board (game frame))
		   (setf (ttt-board (game frame)) (make-ttt-board)))))
    (map 'vector #'(lambda (row)
		     (map 'vector #'(lambda (col)
				      (aref array row col))
			  (duim-utilities:range :from 0 :below (array-dimension array 1))))
	 (duim-utilities:range :from 0 :below (array-dimension array 0)))))

#||
define method add-ttt-piece 
    (ttt-square :: <ttt-square>, frame :: <ttt-frame>) => ()
  let game = frame.game;
  if (game.turn = *human-player1* | game.turn = *human-player2*)
    ttt-square-setter(ttt-square, game.turn);
    game.moves := game.moves + 1;
    let over = over?(game, ttt-square);
    if (over)
      display-winner(frame, over, game.turn);
      //game.turn := 3;
    else
      game.turn := modulo(game.turn, 2) + 1;
      if (game.turn = *computer-player*) 
	computers-turn(frame);
      end if;
    end if;
  end if;
end method add-ttt-piece;
||#

(defmethod add-ttt-piece ((ttt-square <ttt-square>) (frame <ttt-frame>))
  (let ((game (game frame)))
    (when (or (= (turn game) *human-player1*)
	      (= (turn game) *human-player2*))
      (ttt-square-setter ttt-square (turn game))
      (setf (moves game) (+ (moves game) 1))
      (let ((over (over? game ttt-square)))
	(if over
	    (display-winner frame over (turn game))
	    ;; else
	    (progn
	      (setf (turn game) (+ (mod (turn game) 2) 1))
	      (when (= (turn game) *computer-player*)
		(computers-turn frame))))))))

#||
define method frame-play-computers-move
    (frame :: <ttt-frame>) => (row :: <integer>, col :: <integer>)
  let game = frame.game;
  game-play-computers-move(game);
end method frame-play-computers-move;
||#

(defmethod frame-play-computers-move ((frame <ttt-frame>))
  (let ((game (game frame)))
    (game-play-computers-move game)))

#||
define method computers-turn
    (frame :: <ttt-frame>)
  let (i, j) = frame-play-computers-move(frame);
  let game = frame.game;
  let over = game-computers-turn(game, i, j);
  
  if (over)
    display-winner(frame, over, *computer-player*);
    // game.turn := 3;
  else 
    game.turn := modulo(game.turn, 2) + 1;
  end if;
end method computers-turn;  
||#

(defmethod computers-turn ((frame <ttt-frame>))
  (multiple-value-bind (i j)
      (frame-play-computers-move frame)
    (let* ((game (game frame))
	   (over (game-computers-turn game i j)))
      (if over
	  (display-winner frame over *computer-player*)
	  (setf (turn game) (+ (mod (turn game) 2) 1))))))

#||
define method new-game (frame :: <ttt-frame>)
  display-message(frame, "");
  let game = frame.game;
  game.turn := 1;
  game.moves := 0;
  for (i from 0 below $ttt-square-rows)
    for (j from 0 below $ttt-square-columns)
      ttt-square-setter(aref(game.ttt-board, i, j), #f);
    end for;
  end for;
  if (*computer-player* = game.turn)
    computers-turn(frame);
  end if;
end method new-game;  
||#

(defmethod new-game ((frame <ttt-frame>))
  (display-message frame "")
  (let ((game (game frame)))
    (setf (turn game) 1)
    (setf (moves game) 0)
    (loop for i from 0 below *ttt-square-rows*
	 do (loop for j from 0 below *ttt-square-columns*
		 do (ttt-square-setter (aref (ttt-board game) i j) nil)))
    (when (= *computer-player* (turn game))
      (computers-turn frame))))

#||
define method choose-players 
    (frame :: <ttt-frame>, type :: one-of(#"x", #"o", #"two-players"))
 => ()
  select (type)
    #"x" =>
      *human-player1* := 1;
      *human-player2* := 1;
      *computer-player* := 2;
    #"o" =>
      *human-player1* := 2;
      *human-player2* := 2;
      *computer-player* := 1;
    #"two-players" =>
      *human-player1* := 1;
      *human-player2* := 2;
      *computer-player* := 5;
  end;
  new-game(frame)
end method choose-players;
||#

(defmethod choose-players ((frame <ttt-frame>) type)
  (case type
    (:x (setf *human-player1* 1)
	(setf *human-player2* 1)
	(setf *computer-player* 2))
    (:o (setf *human-player1* 2)
	(setf *human-player2* 2)
	(setf *computer-player* 1))
    (:two-players (setf *human-player1* 1)
		  (setf *human-player2* 2)
		  (setf *computer-player* 5)))
  (new-game frame))

#||
define method ttt-set-players
    (frame :: <ttt-frame>, new-players) => ()
  choose-players(frame, new-players)
end method ttt-set-players;
||#

(defmethod ttt-set-players ((frame <ttt-frame>) new-players)
  (choose-players frame new-players))

#||
define method display-winner 
    (frame :: <ttt-frame>, 
     outcome :: one-of(#"win", #"draw"), 
     winner :: <integer>) 
 => ()
  select(outcome)
    #"win" =>
      select (winner)
        1 => display-message(frame, "X wins!");
	2 => display-message(frame, "O wins!");
      end;
    #"draw" =>
      display-message(frame, "A draw.");
  end;
end method display-winner;
||#

(defmethod display-winner ((frame <ttt-frame>) outcome (winner integer))
  (case outcome
    (:win (case winner
	    (1 (display-message frame "X wins!"))
	    (2 (display-message frame "O wins!"))))
    (:draw (display-message frame "A draw."))))

#||
define method about-ttt (frame :: <ttt-frame>) => ()
  notify-user(format-to-string("%s %s", $application-name, $application-version),
              owner: frame)
end method about-ttt;
||#

(defmethod about-ttt ((frame <ttt-frame>))
  (notify-user (format nil "~a ~a" *application-name* *application-version*)
	       :owner frame))

#||
define method play-tic-tac-toe () => ()
  start-frame(make(<ttt-frame>));
end method play-tic-tac-toe;
||#

(defmethod play-tic-tac-toe ()
  (start-frame (make-instance '<ttt-frame>)))

