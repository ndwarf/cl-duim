Library:      tic-tac-toe
Author:       Jonathon Lee
Synopsis:     The classic game
Files:	library
        module 
        frame
        game
        start
Other-files: Open-Source-License.txt
Linker-Options: $(guilflags)
Start-Function: play-tic-tac-toe
Copyright:    Original Code is Copyright (c) 1995-2004 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND

