;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: TIC-TAC-TOE -*-
(in-package #:tic-tac-toe)

#||
Module:       tic-tac-toe
Author:       Jonathon Lee
Synopsis:     The classic game
||#

(defgeneric game-play-computers-move (game))
(defgeneric heuristic (board moves game-status player))
(defgeneric move (board position player))
(defgeneric undo-move (board position))
(defgeneric get-max-child-value (tree))
(defgeneric get-min-child-value (tree))
(defgeneric find-winner (board player))
(defgeneric end-state? (board moves player))
(defgeneric build-tree (board player moves))
(defgeneric find-good-move (tree))
(defgeneric tie-game? (moves))
(defgeneric on-down-diagonal? (move))
(defgeneric on-up-diagonal? (move))
(defgeneric down-diagonal-winner? (board move player))
(defgeneric up-diagonal-winner? (board move player))
(defgeneric row-winner? (board move player))
(defgeneric col-winner? (board move player))
(defgeneric winning-move? (board square))
(defgeneric over? (game square))
(defgeneric game-computers-turn (game i j))

#||
define variable *computer-player* :: <integer> = 2;
define variable *human-player1* :: <integer> = 1;
define variable *human-player2* :: <integer> = 1;
||#

;;;(defparameter *computer-player* 2)
;;;(defparameter *human-player1* 1)
;;;(defparameter *human-player2* 1)

#||
define constant <game-status>
  = type-union(singleton(#f), singleton(#"win"), singleton(#"draw"));

define class <tree> (<collection>)
  constant slot game :: false-or(<ttt-game>) = #f,
    init-keyword: game:;
  slot value :: false-or(<integer>) = #f,
    init-keyword: value:;
  slot children :: <array>;
end class <tree>;
||#

(defclass <tree> ()
  ((game :initform nil :initarg :game :reader game)
   (value :initform nil :initarg :value :accessor value)
   (children :type array :accessor children)))

#||
define method initialize (tree :: <tree>, #key) => ()
  next-method();
  tree.children := make(<array>, dimensions: #(3, 3), of: false-or(<tree>));
  for (i from 0 below 3)
    for (j from 0 below 3)
      tree.children[i, j] := #f;
    end for;
  end for;
end method initialize;
||#

(defmethod initialize-instance :after ((tree <tree>) &key)
  (setf (children tree) (make-array (list 3 3)))
  (loop for i from 0 below 3
       do (loop for j from 0 below 3
	       do (setf (aref (children tree) i j) nil))))

#||
define constant <ttt-board>
  = limited(<array>,  
	    dimensions: list($ttt-square-rows, $ttt-square-columns),
	    of: false-or(<ttt-square>)); 

define class <ttt-game> (<object>)
  slot ttt-board :: false-or(<ttt-board>) = #f,
    init-keyword: ttt-squares:;
  slot turn  :: limited(<integer>, min: 1, max: 2) = 1,
    init-keyword: turn:;
  slot moves :: limited(<integer>, min: 0, max: 9) = 0,
    init-keyword: moves:;
end class <ttt-game>;
||#

(defclass <ttt-game> ()
  ((ttt-board :initform nil :initarg :ttt-squares :accessor ttt-board)
   (turn :initform 1 :initarg :turn :accessor turn)
   (moves :initform 0 :initarg :moves :accessor moves)))

#||
define method make-ttt-board () => (squares :: <ttt-board>)
  let squares = make(<ttt-board>,
		     dimensions: list($ttt-square-rows, $ttt-square-columns),
		     fill: #f);
  for (i from 0 below $ttt-square-rows)
    for (j from 0 below $ttt-square-columns)
      squares[i, j] := make(<ttt-square>,
                            square-coordinates: vector(i, j));
    end for;
  end for;
  squares
end method make-ttt-board;
||#

(defun make-ttt-board ()
  (let ((squares (make-array (list *ttt-square-rows* *ttt-square-columns*) :initial-element nil)))
    (loop for i from 0 below *ttt-square-rows*
	 do (loop for j from 0 below *ttt-square-columns*
		 do (setf (aref squares i j) (make-instance '<ttt-square> :square-coordinates (vector i j)))))
    squares))

#||
define method heuristic 
    (board :: <ttt-board>, moves :: <integer>, game-status :: <game-status>, 
     player :: <integer>)
  if (game-status = #"win") 
    if (player = *computer-player*)
      1;
    else 
      -1;
    end if
  elseif (game-status = #"draw")
    0;
  else
    debug-message("hahahaha");
  end if;
end method heuristic;
||#

(defmethod heuristic (board (moves integer) game-status (player integer))
  (if (eql game-status :win)
      (if (eql player *computer-player*)
	  1
	  -1)
      (if (eql game-status :draw)
	  0
	  (format t "hahahaha"))))

#||
define method move
    (board :: <ttt-board>, position :: <vector>, player :: <integer>) => ()
  let i = position[0];
  let j = position[1];
  board[i, j].square-occupied := player;
end method move;
||#

(defmethod move (board (position array) (player integer))
  (let ((i (aref position 0))
	(j (aref position 1)))
    (setf (square-occupied (aref board i j)) player)))

#||
define method undo-move
    (board :: <ttt-board>, position :: <vector>) => ()
  let i = position[0];
  let j = position[1];
  board[i, j].square-occupied := #f;
end method undo-move;
||#

(defmethod undo-move (board (position array))
  (let ((i (aref position 0))
	(j (aref position 1)))
    (setf (square-occupied (aref board i j)) nil)))


#||
define method copy-game
    (old :: <ttt-board>) => (new :: <ttt-board>) 
  let temp = make(<ttt-board>,
		  dimensions: list($ttt-square-rows, $ttt-square-columns),
		  fill: #f);
  temp := make-ttt-board();
  for (i from 0 below 3)
    for (j from 0 below 3)
      temp[i, j].square-occupied := old[i, j].square-occupied;
      temp[i, j].square-coordinates := old[i, j].square-coordinates;
    end for;
  end for;
  temp;
end method copy-game;
||#

(defmethod copy-game (old)
  (let ((temp (make-ttt-board)))
    (loop for i from 0 below 3
       do (loop for j from 0 below 3
	     do (setf (square-occupied (aref temp i j)) (square-occupied (aref old i j)))
	     do (setf (square-coordinates (aref temp i j)) (square-coordinates (aref old i j)))))
    temp))

#||
define method get-max-child-value
    (tree :: <tree>) => (value :: <integer>)
  let max = -10000000;
  for (i from 0 below 3)
    for (j from 0 below 3)
      let child = tree.children[i, j];
      if (child)
	if (child.value > max)
	  max := child.value;
	end if;
      end if;
    end for;
  end for; 
  max;
end method get-max-child-value;
||#

(defmethod get-max-child-value ((tree <tree>))
  (let ((max -10000000))
    (loop for i from 0 below 3
	 do (loop for j from 0 below 3
	       do (let ((child (aref (children tree) i j)))
		    (when child
		      (when (> (value child) max)
			(setf max (value child)))))))
    max))

#||
define method get-min-child-value
    (tree :: <tree>) => (value :: <integer>)
  let min = 10000000;
  for (i from 0 below 3)
    for (j from 0 below 3)
      let child = tree.children[i, j];
      if (child)
	if (child.value < min)
	  min := child.value;
	end if;
      end if;
    end for;
  end for;
  min;
end method get-min-child-value;
||#

(defmethod get-min-child-value ((tree <tree>))
  (let ((min 10000000))
    (loop for i from 0 below 3
	 do (loop for j from 0 below 3
		 do (let ((child (aref (children tree) i j)))
		      (when child
			(when (< (value child) min)
			  (setf min (value child)))))))
    min))

#||
define method find-winner
    (board :: <ttt-board>, p :: <integer>) => (winner? :: <boolean>)
  let ans :: <boolean> = #f;
  let i = 0;
  let j = 0;

  while ((i < 3) & ~ans) 
    ans := row-winner?(board, vector(i, 0), p); 
    i := i + 1;
  end while;

  while ((j < 3) & ~ans)
    ans := col-winner?(board, vector(0, j), p); 
    j := j + 1;
  end while;

  if (~ans)
    ans := up-diagonal-winner?(board, vector(2, 0), p); 
  end if;

  if (~ans)
    ans := down-diagonal-winner?(board, vector(0, 0), p); 
  end if;
  ans;
end method find-winner;  
||#

(defmethod find-winner (board (p integer))
  (let ((ans nil)
	(i 0)
	(j 0))

    (loop while (and (< i 3) (not ans))
       do (setf ans (row-winner? board (vector i 0) p))
       do (setf i (1+ i)))

    (loop while (and (< j 3) (not ans))
       do (setf ans (col-winner? board (vector 0 j) p))
       do (setf j (1+ j)))

    (unless ans
      (setf ans (up-diagonal-winner? board (vector 2 0) p)))

    (unless ans
      (setf ans (down-diagonal-winner? board (vector 0 0) p)))
    ans))

#||
define method end-state? 
    (board :: <ttt-board>, moves :: <integer>, player :: <integer>)
 => (game-status :: <game-status>)
  case
    find-winner(board, player)   => #"win";
    moves = $max-number-of-moves => #"draw";
    otherwise                    => #f;
  end
end method end-state?;
||#

(defmethod end-state? (board (moves integer) (player integer))
  (cond ((find-winner board player) :win)
	((= moves *max-number-of-moves*) :draw)
	(t nil)))

#||  
define method build-tree
    (board :: <ttt-board>, player :: <integer>, moves :: <integer>)
 => (game-tree :: <tree>)
  let root = make(<tree>, game: copy-game(board));
  let other-player = select(player)
		       1 => 2;
		       2 => 1;
		     end;
  let over = end-state?(root.game.ttt-board, moves, other-player);
  if (over)
    root.value := heuristic(root.game.ttt-board, moves, over, other-player);
  else 
    for (i from 0 below $ttt-square-rows) 
      for (j from 0 below $ttt-square-columns)
	if (~board[i, j].square-occupied)
	  move(root.game.ttt-board, vector(i, j), player);
	  root.children[i, j] := build-tree(root.game.ttt-board,
					    other-player, moves + 1);
	  undo-move(root.game.ttt-board, vector(i, j));
	end if;
      end for;
    end for;
	      
    if (player = *computer-player*)
      root.value := get-max-child-value(root);
    else
      root.value := get-min-child-value(root);
    end if;
  end if;
  values(root);
end method build-tree;
||#

(defmethod build-tree (board (player integer) (moves integer))
  (let* ((root (make-instance '<tree> :game (copy-game board)))
	 (other-player (case player
			 (1 2)
			 (2 1)))
	 (over (end-state? (game root) moves other-player)))
    (if over
	(setf (value root) (heuristic (game root) moves over other-player))
	(progn
	  (loop for i from 0 below *ttt-square-rows*
	     do (loop for j from 0 below *ttt-square-columns*
		   do (when (not (square-occupied (aref board i j)))
			(move (game root) (vector i j) player)
			(setf (aref (children root) i j)
			      (build-tree (game root) other-player (1+ moves)))
			(undo-move (game root) (vector i j)))))
	  (if (= player *computer-player*)
	      (setf (value root) (get-max-child-value root))
	      (setf (value root) (get-min-child-value root)))))
    (values root)))

#||
define method find-good-move
    (tree :: <tree>) => (i :: <integer>, j :: <integer>)
  let loopX = 0 ;
  let loopY = 0;
  let equal = #f;
  let my-value = tree.value;
  let good = vector(0, 0);
  while ( (loopX < 3) & ~equal ) 
    while ( (loopY < 3) & ~equal )
      let this-child = tree.children[loopX, loopY]; 
      if (this-child)
	if (this-child.value = my-value)
	  if (~equal) 
	    good[0] := loopX;
	    good[1] := loopY;
	    equal := #t;
	  end if;
	end if;
      end if;
      if (~equal)
	loopY := loopY + 1;
      end if;
    end while;
    if (~equal)
      loopY := 0;
      loopX := loopX + 1;
    end if;
  end while;
  values( good[0], good[1]);
end method find-good-move;
||#

(defmethod find-good-move ((tree <tree>))
  (let ((loopX 0)
	(loopY 0)
	(equal nil)
	(my-value (value tree))
	(good (vector 0 0)))
    (loop while (and (< loopX 3) (not equal))
       do (loop while (and (< loopY 3) (not equal))
	       do (let ((this-child (aref (children tree) loopX loopY)))
		    (when this-child
		      (when (duim-utilities:equal? (value this-child) my-value)
			(unless equal
			  (setf (aref good 0) loopX)
			  (setf (aref good 1) loopY)
			  (setf equal t))))
		    (unless equal
		      (setf loopY (1+ loopY)))))
       do (unless equal
	    (setf loopY 0)
	    (setf loopX (1+ loopX))))
    (values (aref good 0) (aref good 1))))


#||
define method tie-game? ( moves :: <integer> ) => ( tie :: <boolean> )
  moves = $max-number-of-moves;
end method tie-game?;
||#

(defmethod tie-game? ((moves integer))
  (= moves *max-number-of-moves*))

#||
define method on-down-diagonal?( move :: <vector> ) => (b :: <boolean>)
  let row = first(move);
  let col = second(move);
  row = col;
end method on-down-diagonal?;
||#

(defmethod on-down-diagonal? ((move vector))
  (let ((row (aref move 0))
	(col (aref move 1)))
    (duim-utilities:equal? row col)))

#||    
define method on-up-diagonal?( move :: <vector> ) => (b :: <boolean>)
  let row = first(move);
  let col = second(move);
  row + col = 2;
end method on-up-diagonal?;
||#

(defmethod on-up-diagonal? ((move vector))
  (let ((row (aref move 0))
	(col (aref move 1)))
    (= (+ row col) 2)))

#||
define method down-diagonal-winner?
    (board :: <ttt-board>, move :: <vector>, player :: <integer>) 
 => (winner? :: <boolean>)
  (square-occupied(board[0, 0]) = square-occupied(board[1, 1]))
    & (square-occupied(board[1, 1]) = square-occupied(board[2, 2]))
    & (square-occupied(board[2, 2]) = player);
end method down-diagonal-winner?;
||#

(defmethod down-diagonal-winner? (board (move vector) (player integer))
  (and (duim-utilities:equal? (square-occupied (aref board 0 0)) (square-occupied (aref board 1 1)))
       (duim-utilities:equal? (square-occupied (aref board 1 1)) (square-occupied (aref board 2 2)))
       (duim-utilities:equal? (square-occupied (aref board 2 2)) player)))

#||
define method up-diagonal-winner?
    (board :: <ttt-board>, move :: <vector>, player :: <integer>) 
 => (winner? :: <boolean>)
  (square-occupied(board[0, 2]) = square-occupied(board[1, 1]))
    & (square-occupied(board[1, 1]) = square-occupied(board[2, 0]))
    & (square-occupied(board[2, 0]) = player);
end method up-diagonal-winner?;
||#

(defmethod up-diagonal-winner? (board (move vector) (player integer))
  (and (duim-utilities:equal? (square-occupied (aref board 0 2)) (square-occupied (aref board 1 1)))
       (duim-utilities:equal? (square-occupied (aref board 1 1)) (square-occupied (aref board 2 0)))
       (duim-utilities:equal? (square-occupied (aref board 2 0)) player)))

#||
define method row-winner?
    (board :: <ttt-board>, move :: <vector>, player :: <integer>) 
 => (winner? :: <boolean>)
  let row = move[0];
  (square-occupied(board[row, 0]) = square-occupied(board[row, 1]))
    & (square-occupied(board[row, 1]) = square-occupied(board[row, 2]))
    & (square-occupied(board[row, 2]) = player);
end method row-winner?;
||#

(defmethod row-winner? (board (move vector) (player integer))
  (let ((row (aref move 0)))
    (and (duim-utilities:equal? (square-occupied (aref board row 0)) (square-occupied (aref board row 1)))
	 (duim-utilities:equal? (square-occupied (aref board row 1)) (square-occupied (aref board row 2)))
	 (duim-utilities:equal? (square-occupied (aref board row 2)) player))))

#||
define method col-winner?
    (board :: <ttt-board>, move :: <vector>, player :: <integer>) 
 => (winner? :: <boolean>)
  let col = move[1];
  (square-occupied(board[0, col]) = square-occupied(board[1, col]))
    & (square-occupied(board[1, col]) = square-occupied(board[2, col]))
    & (square-occupied(board[2, col]) = player);
end method col-winner?;
||#

(defmethod col-winner? (board (move vector) (player integer))
  (let ((col (aref move 1)))
    (and (duim-utilities:equal? (square-occupied (aref board 0 col)) (square-occupied (aref board 1 col)))
	 (duim-utilities:equal? (square-occupied (aref board 1 col)) (square-occupied (aref board 2 col)))
	 (duim-utilities:equal? (square-occupied (aref board 2 col)) player))))

#||
define method winning-move?
    (b :: <ttt-board>, s :: <ttt-square>)
 => ( won :: <boolean> )
  let last-move = s.square-coordinates;
  let player = s.square-occupied; 
  if ( on-up-diagonal?( last-move ) )
    if (up-diagonal-winner?(b, last-move, player ))
      #t;
    elseif ( on-down-diagonal?(last-move) ) 
      if (down-diagonal-winner?(b, last-move, player ))
	#t;
      else
	(row-winner?(b, last-move, player ) 
	   | col-winner?(b, last-move, player ));
      end if;
    else
      (row-winner?(b, last-move, player ) 
	 | col-winner?(b, last-move, player ));
    end if;
  elseif ( on-down-diagonal?(last-move) ) 
    if (down-diagonal-winner?(b, last-move, player ))
      #t;
    else
      (row-winner?(b, last-move, player) 
	| col-winner?(b, last-move, player));
    end if;
  else
    (row-winner?(b, last-move, player) 
      | col-winner?(b, last-move, player));
  end if;
end method winning-move?;
||#

(defmethod winning-move? (b (s <ttt-square>))
  (let ((last-move (square-coordinates s))
	(player (square-occupied s)))
    (cond ((on-up-diagonal? last-move)
	   (cond ((up-diagonal-winner? b last-move player) t)
		 ((on-down-diagonal? last-move)
		  (if (down-diagonal-winner? b last-move player)
		      t
		      (or (row-winner? b last-move player)
			  (col-winner? b last-move player))))
		 (t (or (row-winner? b last-move player)
			(col-winner? b last-move player)))))
	  ((on-down-diagonal? last-move)
	   (if (down-diagonal-winner? b last-move player)
	       t
	       (or (row-winner? b last-move player)
		   (col-winner? b last-move player))))
	  (t (or (row-winner? b last-move player)
		 (col-winner? b last-move player))))))

#||
define method over? (g :: <ttt-game>, square :: <ttt-square>)
 => (type :: one-of(#f, #"win", #"draw"))
  if (winning-move?(g.ttt-board, square))
    #"win";
  elseif (tie-game?(g.moves))
    #"draw";
  else
    #f;
  end if;
end method over?;
||#

(defmethod over? ((g <ttt-game>) (square <ttt-square>))
  (cond ((winning-move? (ttt-board g) square) :win)
	((tie-game? (moves g)) :draw)
	(t nil)))

#||
define method game-computers-turn
    (game :: <ttt-game>, i :: <integer>, j :: <integer>) 
 => (type :: one-of(#f, #"win", #"draw"))
  ttt-square-setter(game.ttt-board[i, j], game.turn);  
  game.moves := game.moves + 1;
  let over = over?(game, game.ttt-board[i, j]);
  over;
end method game-computers-turn;
||#

(defmethod game-computers-turn ((game <ttt-game>) (i integer) (j integer))
  (ttt-square-setter (aref (ttt-board game) i j) (turn game))
  (setf (moves game) (+ (moves game) 1))
  (let ((over (over? game (aref (ttt-board game) i j))))
    over))

#||
define method game-play-computers-move
    (game :: <ttt-game>) => (row :: <integer>, col :: <integer>)
  let moves = game.moves;
  if (moves = 0)
    values(1, 1);
  elseif (moves = 1)
    if (game.ttt-board[1, 1].square-occupied)
      values(0, 0);
    else
      values(1, 1);
    end if;
  elseif (moves = 2)
    if (game.ttt-board[0, 0].square-occupied)
      values(0, 2);
    else
      values(0, 0); 
    end if;
  else
    let tree = build-tree(game.ttt-board, *computer-player*, moves);
    let (i, j) = find-good-move(tree);
    values(i, j);
  end if;
end method game-play-computers-move;
||#

(defmethod game-play-computers-move ((game <ttt-game>))
  (let ((moves (moves game)))
    (cond ((= moves 0) (values 1 1))
	  ((= moves 1)
	   (if (square-occupied (aref (ttt-board game) 1 1))
	       (values 0 0)
	       (values 1 1)))
	  ((= moves 2)
	   (if (square-occupied (aref (ttt-board game) 0 0))
	       (values 0 2)
	       (values 0 0)))
	  (t (let ((tree (build-tree (ttt-board game) *computer-player* moves)))
	       (multiple-value-bind (i j)
		   (find-good-move tree)
		 (values i j)))))))
