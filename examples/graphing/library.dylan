Module:       dylan-user
Author:       Scott McKay
Synopsis:     Simple bar charts for DUIM
Copyright:    Original Code is Copyright (c) 1995-2004 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND

define library duim-graphing
  use functional-dylan;
  use duim;

  export duim-graphing;
end library duim-graphing;
