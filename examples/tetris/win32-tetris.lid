Library:      tetris
Synopsis:     DUIM implementation of the game Tetris
Author:       Richard Tucker
Files:	library
	module
	game
	frame
	tetris
Other-files: Open-Source-License.txt
Start-Function:	main
Linker-Options:	$(guilflags)
Target-Type:	executable
Copyright:    Original Code is Copyright (c) 1995-2004 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND

