Library:      duim-examples
Author:       Andy Armstrong
Synopsis:     DUIM example code
Files:	  library
	  module
          harness
          simple-window
          event-tracer
          simple-dialogs
          smooth-graphics
          group-boxes
          clipboard
          scribble
          start-examples
Other-files: Open-Source-License.txt
Linker-Options: $(guilflags)
Copyright:    Original Code is Copyright (c) 1995-2004 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND

