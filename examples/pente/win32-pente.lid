Library:      pente
Author:       James Kirsch
Synopsis:     Pente game
Files:	library
	module
        board
        start
Other-files: Open-Source-License.txt
Linker-Options: $(guilflags)
Start-Function: play-pente
Copyright:    Original Code is Copyright (c) 1995-2004 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND

