;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

(asdf:defsystem #:duim-examples
  :description  "DUIM example functionality"
  :version      "2.1"
  :depends-on (#:tic-tac-toe)
  :components
  ((:system #:tic-tac-toe)))

