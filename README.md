# Introduction
DUIM consists of two layers, with the lower layer comprised of several
components. These components are modular in nature and should be
worked in independently. I.e. we avoid tight coupling.

# Project Structure
The core DUIM systems are each in a directory of their own, with their
own system definition file (ASDF file). You can conceptually think of
these as 'sub-systems' that, together, make up the core of DUIM.

From what we've seen, Dylan-DUIM's references to DUIM and DUIM-CORE
are inconsistent, both in the documentation and code. Likely, when
additions to the repository were made, 'duim-core' was used to
distinguish the core code from everything else and a later attempt was
made to rationalise the system (library, in Dylan terminology) naming.

You may see DUIM-CORE in the documentation of CL-DUIM that was
inherited from Dylan. The best way to think of 'core' is all of the
systems under the core/ directory. The other directories are easily
distinguished from the core DUIM system, with the exception of
DUIM-EXTENDED. As the name suggests, these are extensions built on top
of core. You can think of this as an incubator of sorts for system
that aren't quite yet ready to go into duim proper.

The following systems and corresponding packages are defined:

| System            | Package                |
| ----------------- | ---------------------- |
| UTILITIES         | DUIM-UTILITIES
| GEOMETRY          | DUIM-GEOMETRY
| DCS               | DUIM-DCS
| SHEETS            | DUIM-SHEETS
| GRAPHICS          | DUIM-GRAPHICS
| LAYOUTS           | DUIM-LAYOUTS
| EXTENDED-GEOMETRY | DUIM-EXTENDED-GEOMETRY
| GADGETS           | DUIM-GADGETS
| FRAMES            | DUIM-FRAMES
| RECORDING         | DUIM-RECORDING

The DUIM system and package includes all of the above systems and
packages. Currently, the DUIM-USER package is the same as DUIM. The
intention is to include systems outside of DUIM, for example
presentations, and create a single user package, similar to the use of
CL-USER.

## Extended-Geometry
Extended geometry is an extension to core geometry. Despite the name,
it is not an extension of core, but is part of core. An unfortunate
choice of names by the Dylan team.

# Working with  Emacs
Slime/swank is the best way to work with Lisp projects. Be sure to
load [slime-asdf][SLIME-ASDF] to get some shortcuts for building.

# Working with ASDF
When modifying the build system, it's helpful to be able to reread all
the configuration files. To do this use:
`(asdf:clear-source-registry)` and [ASDF][ASDF-MANUAL] will re-scan
your source tree the next time it tries to find a system

## Rebuilding components
You can rebuild parts, or all of, the system and components using
ASDFs [convenience functions][ASDF-CONVENIENCE-FUNCTIONS], which are
also integrated into [slime-asdf][SLIME-ASDF]. The most often used is
`,reload-system` which will recompile and load DUIM and any changed
dependent systems. You can also use `load-system` to recompile as
needed. These are slime wrappers around [ASDF's force
system][ASDF-FORCE-SYSTEM] options.

Note that whilst ASDF will handle transitive dependencies for
reloading and recompiling, it does not follow dependencies when using
`,delete-system-fasls`. For that you have to name each system
individually, e.g. `,delete-system-fasls :geometry` to delete the
geometry subsystems FASLs

## Versioning of Systems
ASDF systems have a version number that is set in the `version.sexp`
file in the same directory as the ASD file. You can query a system's
version using the `system-version` function, e.g. `(system-version
:dcs)`.

### Semantics of Versioning
Although CCL does not yet have a patch facility, one will be available
soon (we'll write one if the community does not). With that in mind
assume CCL provides a number of convenient tools and several
interfaces for loading patches. For example, users can load patches by
calling one of several Lisp functions or alternatively using Command
Processor commands. Users also have the choice of loading patches to
virtual memory (which means they disappear when the machine is booted)
or of saving the patches to disk. (Of course, new patches can be made
later, and then these will have to be loaded to get the very latest
version of a system.) In the case where users load a particular system
whenever they want to use it, the system-loading facility
automatically loads all the patches for that system.

Inevitably, a developer or system maintainer must stop accumulating
patches and recompile all the source files in a large program, for
example, when a system is changed in a far-reaching way that cannot be
accomplished with a patch. Only at this point do the source files
become important to system maintenance and distribution. After a
complete recompilation, the old patch files are useless and should not
be loaded.

To keep track of all the changing number of files in a large program,
the patch facility labels each version of a system with a two-part
number. The two parts are called the major version number and the
minor version number. The minor version number is increased every time
a new patch is made; the patch is identified by the major and minor
version number together. The major version number is increased when
the program is completely recompiled, and at that time the minor
version number is reset to zero. A complete system version is
identified by the major version number, followed by a dot, followed by
the minor version number.

The following typical scenario should clarify this scheme.

1. A new system is created; its initial version number is 1.0.

2. Then a patch file is created; the version of the program that
   results from loading the first patch file into version 1.0 is
   called 1.1.

3. Then another patch file might be created, and loading that patch
   file into system 1.1 creates version 1.2.

4. Then the entire system is recompiled, creating version 2.0 from
   scratch.

5. Now the two patch files are irrelevant, because they fix old
   1software; the changes that they reflect are integrated into system
   2.0.

Note that the second patch file should only be loaded into system 1.1

# Commenting Lisp Code
Deuce (will) differentiates between the different comment indicators
for different major modes. Comments in Lisp begin with a
semicolon. The Lisp reader ignores everything between a (significant)
semicolon and the next newline. By convention, there are three kinds
of comments, beginning with one, two, and three semicolons:

* Comments beginning with a single semicolon are placed to the right of
a line of code, start in a preset column (the _comment column_), and
describe what is going on in that line.

* A comment with two semicolons is a long comment about code within a
Lisp expression and has the same indentation as the code which it
refers. It describes the function of a group of lines.

* A comment headed by three semicolons is normally placed against the
left margin, and describes a large piece of code, such as a function
or group of functions.


# Running Tests
To run the tests manually, first `,load-system duim-tests` then
execute `(duim-tests::run-all-tests)`. To run the tests from slime, use
`,test-system duim`.

Individual tests can be run by changing to the `DUIM-TESTS` package
and running the specific test; for example:
```
CL-USER> (in-package :DUIM-TESTS)
#<Package "DUIM-TESTS">
DUIM-TESTS> (5am:run! 'test-frame-layouts-test)
```

The test names are unexported symbols in the `DUIM-TESTS` package so
another option is to prefix the test name accordingly:
```
CL-USER> (5am:run! 'duim-tests::test-frame-layouts-test)
```

[ASDF-MANUAL] https://www.common-lisp.net/project/asdf/asdf.html
[ASDF-FORCE-SYSTEM] https://github.com/fare/asdf/blob/master/doc/best_practices.md#force
[ASDF-CONVENIENCE-FUNCTIONS] https://www.common-lisp.net/project/asdf/asdf.html#Convenience-Functions
[SLIME-ASDF] https://www.common-lisp.net/project/slime/doc/html/ASDF.html
