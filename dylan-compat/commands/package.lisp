;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

(defpackage #:dylan-commands
  (:use #:common-lisp)
  (:export

   ;; Export module for Commands clients


   #:<basic-command>
   #:<basic-command-metaclass>
   #:<command>
   #:<command-metaclass>
   #:command-client
   #:command-server
   #:command-invoker
   #:command-results-to

   #:<command-results>
   #:<command-results-metaclass>
   #:command-results
   #:command-results-available?
   #:wait-for-command-results

   #:make-command
   #:execute-command-type
   #:execute-command
   #:do-execute-command

   #:<basic-undoable-command>
   #:<basic-undoable-command-metaclass>
   #:command-undoable?
   #:undo-command
   #:redo-command

   #:<functional-command>
   #:<functional-command-metaclass>
   #:command-function
   #:command-arguments

   #:<basic-string-command>
   #:<basic-string-command-metaclass>
   #:<string-command>
   #:<string-command-metaclass>
   #:string-for-command
   #:string-for-argument
;;   object-to-string,
;;   #:string-command-definer
;;   #:string-command-class-definer
;;   #:string-command-slots-definer
   #:define-string-command
   #:define-string-command-class
   #:define-string-command-slots
   
   ))

;; Implementation module

(defpackage #:dylan-commands-internals
  (:use #:common-lisp
	#:dylan-commands)
  (:export

   #:command-pattern-string
   #:command-argument-names

   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;;;
   ;;; Also export everything from :commands
   ;;;

   ;; Export module for Commands clients

   #:<basic-command>
   #:<basic-command-metaclass>
   #:<command>
   #:<command-metaclass>
   #:command-client
   #:command-server
   #:command-invoker
   #:command-results-to

   #:<command-results>
   #:<command-results-metaclass>
   #:command-results
   #:command-results-available?
   #:wait-for-command-results

   #:make-command
   #:execute-command-type
   #:execute-command
   #:do-execute-command

   #:<basic-undoable-command>
   #:<basic-undoable-command-metaclass>
   #:command-undoable?
   #:undo-command
   #:redo-command

   #:<functional-command>
   #:<functional-command-metaclass>
   #:command-function
   #:command-arguments

   #:<basic-string-command>
   #:<basic-string-command-metaclass>
   #:<string-command>
   #:<string-command-metaclass>
   #:string-for-command
   #:string-for-argument
;;   object-to-string,
;;   #:string-command-definer
;;   #:string-command-class-definer
;;   #:string-command-slots-definer
   #:define-string-command
   #:define-string-command-class
   #:define-string-command-slots

))



#||
Module:       Dylan-User
Synopsis:     Commands module
Author:       Scott McKay, Hugh Greene
Copyright:    Original Code is Copyright (c) 1998-2000 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND

// Export module for Commands clients
define module commands
  create <basic-command>,
	 <command>,
	 command-client,
	 command-server,
	 command-invoker,
	 command-results-to;

  create <command-results>,
	 command-results,
	 command-results-available?,
	 wait-for-command-results;

  create make-command,
	 execute-command-type,
	 execute-command,
	 do-execute-command;

  create <basic-undoable-command>,
	 command-undoable?,
	 undo-command,
	 redo-command;

  create <functional-command>,
	 command-function,
	 command-arguments;

  create <basic-string-command>,
	 <string-command>,
	 string-for-command,
	 string-for-argument,
	 object-to-string,
	 \string-command-definer,
	 \string-command-class-definer,
	 \string-command-slots-definer;
end module commands;

// Implementation module
define module commands-internals
  use functional-dylan;
  use simple-format;
  use threads;
  use commands, export: all;

  export command-pattern-string,
	 command-argument-names;
end module commands-internals;
||#
