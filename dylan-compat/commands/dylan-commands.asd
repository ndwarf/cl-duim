;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

(asdf:defsystem :dylan-commands
  :version "0.1"
  :serial t
;;  :depends-on ("test-framework")
  :components
  ((:file "package")
   (:file "commands")
   (:file "string-commands")))

