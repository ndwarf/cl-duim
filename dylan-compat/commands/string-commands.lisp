
(in-package :dylan-commands-internals)

#||
Module:       commands-internals
Synopsis:     Commands protocols and basic classes
Author:       Scott McKay, Hugh Greene
Copyright:    Original Code is Copyright (c) 1998-2000 Functional Objects, Inc.
              All rights reserved.
License:      Functional Objects Library Public License Version 1.0
Dual-license: GNU Lesser General Public License
Warranty:     Distributed WITHOUT WARRANTY OF ANY KIND

/// String-based commands

define open abstract class <string-command> (<command>)
end class <string-command>;
||#


(defclass <string-command> (<command>) ())  ;; FIXME:ABSTRACT

#||
define open generic command-pattern-string
    (command :: <string-command>) => (pattern :: <string>);
define open generic command-argument-names
    (command :: <string-command>) => (argument-names :: <object-table>);
define open generic string-for-command
    (command :: <string-command>) => (string :: <string>);
define open generic string-for-argument
    (server, command :: <command>, name :: <symbol>, value) => (string :: <string>);
||#

(defgeneric command-pattern-string (command))
(defgeneric command-argument-names (command))
(defgeneric string-for-command (command))
(defgeneric string-for-argument (server command name value))

#||
define open abstract primary class <basic-string-command>
    (<basic-command>, <string-command>)
  sealed constant slot command-pattern-string :: <string>,
    required-init-keyword: pattern-string:;
end class <basic-string-command>;
||#


(defclass <basic-string-command>          ;; FIXME:ABSTRACT
    (<basic-command> <string-command>)
  ((command-pattern-string :type string
			   :initarg :pattern-string
			   :initform (error "Initarg :PATTERN-STRING is required to instantiate <BASIC-STRING-COMMAND>")
			   :reader command-pattern-string)))

#||
define macro string-command-definer
  { define ?modifiers:* string-command ?class:name (?superclasses:*) ?slots:* end }
    => { define constant "$" ## ?class ## "-names" :: <object-table> = make(<table>);
	 define ?modifiers string-command-class ?class (?superclasses)
	   sealed class slot command-argument-names :: <object-table>
	     = "$" ## ?class ## "-names",
	     setter: #f;
	   ?slots
	  end;
	 define string-command-slots ?class ?slots end; }
end macro string-command-definer;
||#

;;; FIXME: migrate this to CLOS syntax

(defmacro define-string-command (modifiers name superclasses slots)
  (let ((hash-name (concatenate 'string "$" (symbol-name name) "-NAMES")))
  `(progn
     (defparameter ,(intern hash-name) (make-hash-table))
     (define-string-command-class ,modifiers ,name ,superclasses
       (((:sealed) :slot command-argument-names
                         :type hashtable
                         :initform ,(intern hash-name)
                         :reader command-argument-names
                         :writer nil)
	,@slots))
     (define-string-command-slots ,name ,slots))))

#||
define macro string-command-class-definer
  { define ?modifiers:* string-command-class ?class:name (?superclasses:*) ?slots:* end }
    => { define ?modifiers class ?class (?superclasses) ?slots end }
 slots:
  { } => { }
  { ?slot:*; ... } => { ?slot ... }
 slot:
  { ?modifiers:* named-argument ?arg-name:name is ?slot-name:name ?stuff:* }
    => { ?modifiers slot ?slot-name ?stuff; }
  { ?other:* } => { ?other; }
end macro string-command-class-definer;
||#

;;; FIXME: migrate this to CLOS syntax

(defmacro define-string-command-class (modifiers name superclasses slots)
  ;; The idea here is that slots passed to DEFINE-DYLAN-CLASS can either
  ;; be 'standard' slots, or command names defined (NAMED-ARGUMENT).
  (labels ((named-argument-slot-p (slot)
	       (or (and (listp (first slot))
			(eq (second slot) :named-argument)
			(eq (fourth slot) :is))
		   (and (not (listp (first slot)))
			(eq (first slot) :named-argument)
			(eq (third slot) :is))))
	   (canonicalise-named-argument-slot (slot)
	       (if (listp (first slot))
		   ;; ((:modifiers) :named-slot nm :is slot-name [more stuff])
		   (destructuring-bind (modifiers
					named-arg
					arg-name
					is
					slot-name
					&rest stuff)
		       slot
		     (declare (ignore named-arg arg-name is))
		     ;; Should check that appropriate reader / accessor /
		     ;; whatever is defined for the modifiers... :FIXME:
		     `(,modifiers :slot ,slot-name ,@stuff))
		 ;; (:named-slot nm :is slot-name [more stuff])
		 (destructuring-bind (named-arg
				      arg-name
				      is
				      slot-name
				      &rest stuff)
		     slot
		   (declare (ignore named-arg arg-name is))
		   ;; Should check that appropriate reader / writer /
		   ;; accessor is defined for (:open)... :FIXME:
		   `((:open) :slot ,slot-name ,@stuff)))))
    `(define-dylan-class ,modifiers ,name ,superclasses
       ,(loop for slot in slots
	      collecting (if (named-argument-slot-p slot)
			     (canonicalise-named-argument-slot slot)
			   ;; (else)
			   slot)))))

#||
define macro string-command-slots-definer
  { define string-command-slots ?class:name end }
    => { }
  { define string-command-slots ?class:name
      ?modifiers:* named-argument ?arg-name:name is ?slot-name:name ?stuff:*; ?more-slots:*
    end }
    => { "$" ## ?class ## "-names"[?#"arg-name"] := ?slot-name;
         define string-command-slots ?class ?more-slots end; }
  { define string-command-slots ?class:name
      ?other-slot:*; ?more-slots:*
    end }
    => { define string-command-slots ?class ?more-slots end; }
end macro string-command-slots-definer;
||#

(defmacro define-string-command-slots (class slots)
  (let ((hash-name (concatenate 'string "$" (symbol-name class) "-NAMES")))
    (labels ((named-argument-slot-p (slot)
	       (or (and (listp (first slot))
			(eq (second slot) :named-argument)
			(eq (fourth slot) :is))
		   (and (not (listp (first slot)))
			(eq (first slot) :named-argument)
			(eq (third slot) :is))))
	     (named-slot-has-modifiers-p (slot)
	         (listp (first slot))))
      `(progn
	,@(loop for slot in slots
		when (named-argument-slot-p slot)
		collect (if (named-slot-has-modifiers-p slot)
			    (destructuring-bind (modifiers
						 named-argument
						 arg-name
						 is
						 slot-name
						 &rest stuff)
				slot
			      (declare (ignore modifiers named-argument is stuff))
			      `(setf (gethash ',arg-name ,(intern hash-name))
				     ',slot-name))
			  (destructuring-bind (named-argument
					       arg-name
					       is
					       slot-name
					       &rest stuff)
			      slot
			    (declare (ignore named-argument is stuff))
			    `(setf (gethash ',arg-name ,(intern hash-name))
				   ',slot-name))))))))


#||
/// Argument substitution

define method string-for-command
    (command :: <basic-string-command>) => (string :: <string>)
  let server  = command-server(command);
  let pattern = command-pattern-string(command);
  let length :: <integer> = size(pattern);
  let result :: <stretchy-object-vector> = make(<stretchy-vector>);
  let i :: <integer> = 0;
  while (i < length)
    let char :: <character> = pattern[i];
    case
      char = '$' & i < length - 2 & pattern[i + 1] = '(' =>
	let open   = i + 1;
	let close  = find-char(pattern, ')', start: open, end: length);
	let name   = close & copy-sequence(pattern, start: open + 1, end: close);
	let name   = name & as(<symbol>, name);
	let getter = element(command-argument-names(command), name, default: #f);
	let object = getter & getter(command);
	let string = string-for-argument(server, command, name, object);
	if (getter)
	  for (j :: <integer> from 0 below size(string))
	    add!(result, string[j])
	  end;
	  i := close + 1
	else
	  for (j :: <integer> from i to (close | length - 1))
	    add!(result, pattern[j])
	  end;
	  i := (close | length - 1) + 1
	end;
      char = '$' & i < length - 2 & pattern[i + 1] = '$' =>
	add!(result, pattern[i]);
	i := i + 2;
      otherwise =>
	add!(result, pattern[i]);
	i := i + 1;
    end
  end;
  as(<string>, result)
end method string-for-command;
||#

(defmethod string-for-command ((command <basic-string-command>))
  (let* ((server  (command-server command))
	 (pattern (command-pattern-string command))
	 (length  (length pattern))
	 (result  (make-array 0 :adjustable t :fill-pointer t))
	 (i       0))
    (loop for char = (aref pattern i)
          while (< i length)
	  do (cond ((and (char= char #\$)
			 (< i (- length 2))
			 (char= (aref pattern (1+ i)) #\())
		    (let* ((open (1+ i))
			   (close (find-char pattern #\) :start open :end length))
			   (name (and close (subseq pattern (1+ open) close)))
			   (name (and name (intern name)))
			   (getter (gethash name (command-argument-names command) nil))
			   (object (and getter (funcall getter command)))
			   (string (string-for-argument server command name object)))
		      (if getter
			  (progn
			    (loop for j from 0 below (length string)
				 do (vector-push-extend (aref string j) result))
			    (setf i (1+ close)))
			  ;; else
			(progn
			  (loop for j from i to (or close (- length 1))
				do (vector-push-extend (aref pattern j) result))
			  (setf i (1+ (or close (- length 1))))))))
		   ((and (char= char #\$)
			 (< i (- length 2))
			 (char= (aref pattern (1+ i)) #\$))
		    (vector-push-extend (aref pattern i) result)
		    (setf i (+ i 2)))
		   (t
		    (vector-push-extend (aref pattern i) result)
		    (setf i (1+ i)))))
    (coerce result 'string)))

#||
// We get the server and the name into the action so that the value can be
// printed in special ways, e.g., some server might want booleans to be printed
// as "yes" and "no" for some arguments
define method string-for-argument
    (server, command :: <command>, name :: <symbol>, value)
 => (string :: <string>)
  object-to-string(value)
end method string-for-argument;
||#

(defmethod string-for-argument (server
				(command <command>)
				(name symbol)
				value)
  (declare (ignore server))
  (object-to-string value))

#||
define sealed method find-char
    (string :: <string>, char, #key start: _start = 0, end: _end = size(string))
 => (index :: false-or(<integer>))
  block (return)
    for (index :: <integer> from _start below _end)
      when (string[index] = char)
	return(index)
      end
    end
  end
end method find-char;
||#

(defun find-char (string char &key ((:start _start) 0) ((:end _end) (length string)))
  (loop for index from _start below _end
	when (char= (aref string index) char)
	do (return-from find-char index)))


#||
/// Value printing

define open generic object-to-string (object) => (string :: <string>);
||#

(defgeneric object-to-string (object))

#||
// Default just uses 'format-to-string'
define method object-to-string
    (object :: <object>) => (string :: <string>)
  format-to-string("%=", object)
end method object-to-string;
||#

(defmethod object-to-string ((object t))
  (format nil "~a" object))

#||
define sealed method object-to-string
    (string :: <string>) => (string :: <string>)
  string
end method object-to-string;
||#

(defmethod object-to-string ((string string))
  string)

#||
define sealed method object-to-string
    (char :: <character>) => (string :: <string>)
  make(<string>, size: 1, fill: char)
end method object-to-string;
||#

(defmethod object-to-string ((char character))
  (make-string 1 :initial-element char))

#||
define sealed method object-to-string
    (symbol :: <symbol>) => (string :: <string>)
  as(<string>, symbol)
end method object-to-string;
||#

(defmethod object-to-string ((symbol symbol))
  ;; Note: this will mung case (or at least, the reader will...)
  (symbol-name symbol))

#||
define sealed method object-to-string
    (integer :: <integer>) => (string :: <string>)
  integer-to-string(integer)
end method object-to-string;
||#

(defmethod object-to-string ((integer integer))
  (format nil "~d" integer))

#||
define sealed method object-to-string
    (float :: <float>) => (string :: <string>)
  float-to-string(float)
end method object-to-string;
||#

(defmethod object-to-string ((float float))
  (format nil "~d" float))

#||
define method object-to-string
    (sequence :: <sequence>) => (string :: <string>)
  select (size(sequence))
    0 => "";
    1 => object-to-string(sequence[0]);
    otherwise =>
      reduce1(method (s1, s2) concatenate(s1, ", ", s2) end method,
	      map(object-to-string, sequence))
  end
end method object-to-string;
||#

(defmethod object-to-string ((sequence sequence))
  (case (length sequence)
    (0 "")
    (1 (object-to-string (elt sequence 0)))
    (t (reduce #'(lambda (s1 s2)
		   (concatenate 'string s1 ", " s2))
	       ;; Note: liable to explode unless sequence is a list...
	       (mapcar 'object-to-string sequence)))))


#||
/// Sample usage

/*
define open abstract primary string-command <editor-command> (<basic-string-command>)
  named-argument pathname is %pathname     :: <string>,
    required-init-keyword: pathname:;
  named-argument start-line is %start-line :: <integer> = 0,
    init-keyword: start-line:;
  named-argument start-col  is %start-col  :: <integer> = 0,
    init-keyword: start-col:;
  named-argument end-line   is %end-line   :: <integer> = 0,
    init-keyword: end-line:;
  named-argument end-col    is %end-col    :: <integer> = 0,
    init-keyword: end-col:;
end string-command <editor-command>;


define sealed class <lisp-new-file-command> (<editor-command>)
  keyword pattern-string: = "(new-file \"$(pathname)\");";
end class <lisp-new-file-command>;


define sealed class <lisp-open-file-command> (<editor-command>)
  keyword pattern-string: = "(open-file \"$(pathname)\")"
			    "(go-to \"$(pathname)\" $(start-line) $(start-col))"
end class <lisp-open-file-command>;

define sealed class <lisp-close-file-command> (<editor-command>)
  keyword pattern-string: = "(close-file \"$(pathname)\");";
end class <lisp-close-file-command>;

define sealed class <dde-new-file-command> (<editor-command>)
  keyword pattern-string: = "FileNew($(pathname));";
end class <dde-new-file-command>;

define sealed class <dde-open-file-command> (<editor-command>)
  keyword pattern-string: = "FileOpen($(pathname));"
			    "GoTo($(pathname), $(start-line), $(start-col));"
end class <dde-open-file-command>;

define sealed class <dde-close-file-command> (<editor-command>)
  keyword pattern-string: = "FileClose($(pathname));";
end class <dde-close-file-command>;
*/
||#
