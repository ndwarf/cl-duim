;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DYLAN-COMMANDS-INTERNALS -*-
(in-package :dylan-commands-internals)

;;; Commands protocols and basic classes
;;; See: https://github.com/dylan-lang/opendylan/blob/30b4a9192eecf4f5458c88f3901866af9dc1cf90/sources/lib/commands/commands.dylan

;;; Command protocol

(defclass <command> () ()  ;; FIXME: ABSTRACT
  (:documentation
   "The class of commands. These are commands that can be grouped together
in a command table to form the set of commands available to an
application (available, for example, from the menu bar of the
application). The resulting command object can then be executed by
calling 'execute-command'.

The :function initarg is the command function that is called by the
command object. A command function is rather like a callback to a
<command> object; a command can be executed via 'execute-command',
which then invokes the command function. Command functions take at
least one argument: a <frame> object.

The :arguments initarg are the arguments passed to the command
function."))

#|| define constant <command-oid> = type-union(<command>, <function>);||#
(deftype <command-oid> () '(or <command> function)); This is the original

;;; Lifted from frames/commands.lisp
;;; See if we can make this work without the command-subclass-p
|#
(deftype <command-oid> ()
  " A <command-oid> is something that acts like a command:
  - a command class, or
  - a command object, or
  - a function that acts like a command"
  '(or (satisfies command-subclass-p) <command> function))
|#

#||
define open generic make-command
    (command-type :: <type>, #rest initargs,
     #key client, server, invoker, results-to, #all-keys)
 => (command :: <command>);
||#

(defgeneric make-command (command-type
			  &rest
			  initargs
			  &key
			  client server invoker results-to
			  &allow-other-keys))

#||
define open generic execute-command-type
    (command-type, #rest initargs,
     #key client, server, invoker, results-to, #all-keys)
 => (#rest values);			//--- should be (results :: false-or(<command-results>))
define open generic execute-command
    (command :: <command-oid>) => (#rest values);
define open generic do-execute-command
    (server, command :: <command-oid>) => (#rest values);
||#

(defgeneric execute-command-type (command-type
				  &rest
				  initargs
				  &key
				  client server invoker results-to
				  &allow-other-keys))
(defgeneric execute-command (command))
(defgeneric do-execute-command (server command))

(defgeneric command-client (command))
(defgeneric command-server (command))
(defgeneric command-invoker (command))
(defgeneric command-results-to (command))

#||
// If a command can be undone, COMMAND-UNDOABLE? must return T. A command is
// 'undone' by invoking another command that reverses the effect of the
// command being undone.
define open generic command-undoable?
    (command :: <command-oid>) => (undoable? :: <boolean>);
define open generic undo-command
    (command :: <command-oid>) => (#rest values);
define open generic redo-command
    (command :: <command-oid>) => (#rest values);
||#

(defgeneric command-undoable-p (command)
  (:documentation
   "Returns true if _command_ is undoable, that is, there is a specified
command that the user can choose (for instance, by choosing Edit >
Undo) that will reverse the effects of the command."))

(defgeneric undo-command (command)
  (:documentation
   "Calls the undo command for _command_, undoing the effects of calling
_command_. Note that _command_ must be undoable.

You can call this command directly in your own code, as well as
specialize it."))

(defgeneric redo-command (command)
  (:documentation
"Performs _command_ again. The command is the command that was last
executed using 'execute-command'.

Note that the command described by _command_ must be undoable.

You can both specialize this function and call it directly in your
code."))


;;;  Command results
#||
define open abstract class <command-results> (<object>)
end class <command-results>;
||#

(defclass <command-results> () ())  ;; FIXME:ABSTRACT

#||
define open generic command-results
    (results :: <command-results>) => (#rest values);
define open generic command-results-available?
    (results :: <command-results>) => (available? :: <boolean>);
define open generic wait-for-command-results
    (results :: <command-results>, #key timeout) => (timed-out? :: <boolean>);
||#

(defgeneric command-results (results))
(defgeneric command-results-available? (results))
(defgeneric wait-for-command-results (results &key timeout))


;;; Default methods for <command>
#||
define method make-command
    (command-type :: subclass(<command>), #rest initargs,
     #key client, server, invoker, results-to)
 => (command :: <command>)
  ignore(client, server, invoker, results-to);
  apply(make, command-type, initargs)
end method make-command;
||#

(defmethod make-command (command-type  ; <command-metaclass>)
			 &rest
			 initargs
			 &key
			 client server invoker results-to
			 &allow-other-keys)
  (declare (ignore client server invoker results-to))
  (assert (subtypep command-type (find-class '<command>)))
  (apply #'make-instance command-type initargs))

#||
define method execute-command-type
    (command :: <command>, #rest initargs,
     #key client, server, invoker, results-to)
 => (#rest values);			//--- should be (results :: false-or(<command-results>))
  ignore(initargs, client, server, invoker, results-to);
  execute-command(command)
end method execute-command-type;
||#

(defmethod execute-command-type ((command <command>)
				 &rest
				 initargs
				 &key
				 client server invoker results-to)
  (declare (ignore initargs client server invoker results-to))
  (execute-command command))

#||
define method execute-command-type
    (command-type :: subclass(<command>), #rest initargs,
     #key client, server, invoker, results-to)
 => (#rest values);			//--- should be (results :: false-or(<command-results>))
  ignore(client, server, invoker, results-to);
  let command = apply(make-command, command-type, initargs);
  execute-command(command)
end method execute-command-type;
||#

(defmethod execute-command-type (command-type
				 &rest
				 initargs
				 &key
				 client server invoker results-to)
  (declare (ignore client server invoker results-to))
  (assert (subtypep command-type (find-class '<command>)))
  (let ((command (apply #'make-command command-type initargs)))
    (execute-command command)))

#||
define method execute-command-type
    (command-type :: <list>, #rest initargs,
     #key client, server, invoker, results-to)
 => (#rest values);			//--- should be (results :: false-or(<command-results>))
  ignore(client, server, invoker, results-to);
  let initargs     = concatenate-as(<vector>, initargs, tail(command-type));
  let command-type = head(command-type);
  let command = apply(make-command, command-type, initargs);
  execute-command(command)
end method execute-command-type;
||#

(defmethod execute-command-type ((command-type list)
				 &rest
				 initargs
				 &key
				 client server invoker results-to)
  (declare (ignore client server invoker results-to))
  (let* ((initargs (append initargs (rest command-type)))
	 (command-type (first command-type))
	 (command (apply #'make-command command-type initargs)))
    (execute-command command)))

#||
define method execute-command
    (command :: <command>) => (#rest values)
  // Ask the server to execute the command
  do-execute-command(command-server(command), command)
end method execute-command;
||#

(defmethod execute-command ((command <command>))
  ;; Ask the server to execute the command
  (do-execute-command (command-server command) command))


;;; By default, we have a single-thread model where the client, server,
;;; invoker, and results-to are all the same.  Concrete subclasses of
;;; <command> must implement a method for 'command-server'.
(defmethod command-client ((command <command>))
  (command-server command))

(defmethod command-invoker ((command <command>))
  (command-server command))

(defmethod command-results-to ((command <command>))
  (command-invoker command))

(defmethod command-undoable-p ((command <command>))
  nil)


;;; Default methods for <function>
#||define sealed domain make-command (subclass(<function>)) ;||#

;;;  No applicable methods if you try to 'make' a function command type
(defmethod make-command ((command (eql (find-class 'function)))  ; portable?
			 &rest
			 initargs
			 &key
			 client server invoker results-to
			 &allow-other-keys)
  (declare (ignore initargs client server invoker results-to))
  (error "Don't invoke MAKE-COMMAND with a functional sub-class type"))

#||
define sealed method execute-command-type
    (function :: <function>, #rest initargs,
     #key client, server, invoker, results-to)
 => (#rest values);			//--- should be (results :: false-or(<command-results>))
  ignore(initargs, client, invoker, results-to);
  function(server)
end method execute-command-type;
||#

(defmethod execute-command-type ((function function)
				 &rest
				 initargs
				 &key
				 client server invoker results-to)
  (declare (ignore initargs client invoker results-to))
  (funcall function server))


(defmethod command-client ((command function))
  nil)

(defmethod command-server ((command function))
  nil)

(defmethod command-invoker ((command function))
  nil)

(defmethod command-results-to ((command function))
  nil)

(defmethod command-undoable-p ((command function))
  nil)


;;; Simple command classes

#||
define open abstract primary class <basic-command> (<command>)
  sealed slot command-client = #f,
    init-keyword: client:;
  sealed constant slot command-server,
    required-init-keyword: server:;
  sealed slot command-invoker = #f,
    init-keyword: invoker:;
  sealed slot command-results-to = #f,
    init-keyword: results-to:;
end class <basic-command>;
||#

(defclass <basic-command> (<command>)    ;; FIXME:ABSTRACT
  ((command-client :initarg :client
		   :initform nil
		   :accessor command-client)
   (command-server :initarg :server
		   :initform (error "Initarg :SERVER is required to instantiate <BASIC-COMMAND>")
		   :reader command-server)
   (command-invoker :initarg :invoker
		    :initform nil
		    :accessor command-invoker)
   (command-results-to :initarg :results-to
		       :initform nil
		       :accessor command-results-to))
  (:documentation
   "The class of simple commands. A simple command has an associated
function and some arguments. Simple commands are not undoable.

The first argument to the function is always the frame.

FIXME: I'm not sure this is the right place for this
documentation. It's from the entry for <simple-command>."))

#||
define method initialize
    (command :: <basic-command>, #key client, server, invoker, results-to) => ()
  unless (client)     command-client(command)     := server end;
  unless (invoker)    command-invoker(command)    := server end;
  unless (results-to) command-results-to(command) := invoker | server end;
end method initialize;
||#

;;; FIXME: this one fails if it's on the primary method, so make it
;;;        an 'after' method.

(defmethod initialize-instance :after ((command <basic-command>)
				&rest
				initargs
				&key
				client server invoker results-to
				&allow-other-keys)
  (declare (ignorable initargs))
  (unless client
    (setf (command-client command) server))
  (unless invoker
    (setf (command-invoker command) server))
  (unless results-to
    (setf (command-results-to command) (or invoker server))))

#||
define open abstract primary class <basic-undoable-command> (<basic-command>)
  sealed constant slot %undo-command :: false-or(<command>) = #f,
    init-keyword: undo-command:;
end class <basic-undoable-command>;
||#

(defclass <basic-undoable-command> (<basic-command>)    ;; FIXME:ABSTRACT
  ((%undo-command :type (or null <command>)
		  :initarg :undo-command
		  :initform nil
		  :reader %undo-command))
  (:documentation
   "The class of simple commands that can contain an undo action. A simple
undoable command is like a simple command, except that it points to a
command that can undo it, represented by the :undo-command initarg.

FIXME: I'm not sure this is the right place for this
documentation. It's from the entry for <simple-undoable-command>."))


(defmethod command-undoable-p ((command <basic-undoable-command>))
  t)

#||
define method undo-command
    (command :: <basic-undoable-command>) => (#rest values)
  when (command.%undo-command)
    execute-command(command.%undo-command)
  end
end method undo-command;
||#

(defmethod undo-command ((command <basic-undoable-command>))
  (when (%undo-command command)
    (execute-command (%undo-command command))))

#||
define method redo-command
    (command :: <basic-undoable-command>) => (#rest values)
  execute-command(command)
end method redo-command;
||#

(defmethod redo-command ((command <basic-undoable-command>))
  (execute-command command))


;;; A functional command has a function and some arguments, and isn't undoable.
;;; The first argument to the function is always the server (e.g., DUIM frame).
#||
define sealed class <functional-command> (<basic-command>)
  sealed constant slot command-function,
    required-init-keyword: function:;
  sealed constant slot command-arguments = #[],
    init-keyword: arguments:;
end class <functional-command>;
||#

(defgeneric command-arguments (command)
  (:documentation "Returns the arguments to _command_."))

(defgeneric command-function (command)
  (:documentation
   "Returns the function associated with _command_. A command function is
the function that is called by a <command> object. Command functions
are similar to callbacks, in that they are user functions that are
invoked in order to perform some action. Command functions take at
least one argument: a <frame> object."))

(defclass <functional-command> (<basic-command>)
  ((command-function :initarg :function
		     :reader command-function
		     :initform (error "Initarg :FUNCTION is required in <FUNCTIONAL-COMMAND>"))
   (command-arguments :initarg :arguments
		      :initform nil    ;; FIXME:IS THIS RIGHT?
		      :reader command-arguments)))

#||
define sealed domain make (singleton(<functional-command>));
define sealed domain initialize (<functional-command>);

define sealed inline method make
    (class == <command>, #rest initargs, #key, #all-keys)
 => (command :: <functional-command>)
  apply(make, <functional-command>, initargs)
end method make;
||#

(defmethod make-instance ((class (eql (find-class '<command>)))
                          &rest
			  initargs
                          &key
			  &allow-other-keys)
  (apply #'make-instance (find-class '<functional-command>) initargs))


;;; See http://www.nhplace.com/kent/PS/EQUAL.html for a discussion and
;;; why we're not using 'equal'
(defmethod equivalent ((command1 <functional-command>) (command2 <functional-command>))
  "Returns true if _command1_ and _command2_ are the equivalent."
  (or (eq command1 command2)	;; what other slots should we compare?
      (and (eql (command-function command1) (command-function command2))
	   (eql (command-arguments command1) (command-arguments command2)))))

#||
define sealed method do-execute-command
    (server, command :: <functional-command>) => (#rest values)
  // Apply the command function to the server and the arguments
  apply(command-function(command), server, command-arguments(command))
end method do-execute-command;
||#

(defmethod do-execute-command (server (command <functional-command>))
  ;; Apply the command function to the server and the arguments
  (apply (command-function command) server (command-arguments command)))

