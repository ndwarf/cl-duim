;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-
;;;; Note that this file defines multiple packages.

(defpackage #:duim-formatting
  (:use #:common-lisp
	#:duim-utilities
	#:duim-geometry
	#:duim-DCs
	#:duim-sheets
	#:duim-graphics
	#:duim-layouts
	#:duim-gadgets
	#:duim-frames
	#:duim-recording)
  (:export

       ;; Table formatting

       #:<cell-record>
       #:<cell-record-metaclass>
       #:<column-record>
       #:<column-record-metaclass>
       #:<row-record>
       #:<row-record-metaclass>
       #:<table-record>
       #:<table-record-metaclass>
       #:formatting-cell
       #:do-formatting-cell
       #:formatting-column
       #:do-formatting-column
       #:formatting-row
       #:do-formatting-row
       #:formatting-table
       #:do-formatting-table

       ;; Menu, aka item list, formatting

       #:<item-list-record>
       #:<item-list-record-metaclass>
       #:format-items
       #:formatting-items
       #:do-formatting-items

       ;; Graph formatting

       #:<arrow-graph-edge>
       #:<arrow-graph-edge-metaclass>
       #:<dag-graph-record>
       #:<dag-graph-record-metaclass>
       #:<line-graph-edge>
       #:<line-graph-edge-metaclass>
       #:<tree-graph-record>
       #:<tree-graph-record-metaclass>
       #:format-graph-from-roots))


(defpackage #:duim-formatting-internals
  (:use #:common-lisp
	#:duim-utilities
	#:duim-geometry-internals
	#:duim-DCs-internals
	#:duim-sheets-internals
	#:duim-graphics-internals
	#:duim-layouts-internals
	#:duim-gadgets-internals
	#:duim-frames-internals
	#:duim-recording-internals
	#:duim-formatting)
  (:export

       ;; Re-export everything from DUIM-FORMATTING

       ;; Table formatting

       #:<cell-record>
       #:<cell-record-metaclass>
       #:<column-record>
       #:<column-record-metaclass>
       #:<row-record>
       #:<row-record-metaclass>
       #:<table-record>
       #:<table-record-metaclass>
       #:formatting-cell
       #:do-formatting-cell
       #:formatting-column
       #:do-formatting-column
       #:formatting-row
       #:do-formatting-row
       #:formatting-table
       #:do-formatting-table

       ;; Menu, aka item list, formatting

       #:<item-list-record>
       #:<item-list-record-metaclass>
       #:format-items
       #:formatting-items
       #:do-formatting-items

       ;; Graph formatting

       #:<arrow-graph-edge>
       #:<arrow-graph-edge-metaclass>
       #:<dag-graph-record>
       #:<dag-graph-record-metaclass>
       #:<line-graph-edge>
       #:<line-graph-edge-metaclass>
       #:<tree-graph-record>
       #:<tree-graph-record-metaclass>
       #:format-graph-from-roots

       ;; ... and any internal stuff we want to expose

       #:process-spacing-arg

       #:<basic-graph-record>
       #:<basic-graph-record-metaclass>
       #:<graph-edge-record>
       #:<graph-edge-record-metaclass>
       #:<graph-node-record>
       #:<graph-node-record-metaclass>
       #:edge-attachment-points
       #:generate-graph-nodes
       #:graph-edge-from-node
       #:graph-edge-to-node
       #:graph-node-table
       #:graph-node-x
       #:graph-node-x-setter
       #:graph-node-y
       #:graph-node-y-setter
       #:graph-properties
       #:graph-root-nodes
       #:layout-graph-nodes
       #:layout-graph-edges))

