;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-FORMATTING-INTERNALS -*-
(in-package #:duim-formatting-internals)

#||
define macro formatting-table
  { formatting-table (?record:variable = ?sheet:variable, #rest ?options:expression)
      ?:body
    end }
    => { begin
           let formatting-table-body = method (?record) ?body end;
           do-formatting-table(?sheet, formatting-table-body, ?options)
         end }
  { formatting-table (?sheet:variable, #rest ?options:expression)
      ?:body
    end }
    => { begin
           let formatting-table-body = method (_record) ignore(_record); ?body end;
           do-formatting-table(?sheet, formatting-table-body, ?options)
         end }
end macro formatting-table;
||#

;; _drawable_ is a (record = sheet) form, or just a sheet.
(defmacro formatting-table ((drawable &rest options) &body body)
  (if (listp drawable)
      ;; (formatting-table ((record = sheet) ...) body) case
      (destructuring-bind (record = sheet)
	  drawable
	(declare (ignore =))
        `(let ((formatting-table-body (lambda (,record) ,@body)))
	   (do-formatting-table ,sheet formatting-table-body ,@options)))
      ;; else (formatting-table (sheet ...) body) case
      (let ((sheet drawable))
        `(let ((formatting-table-body (lambda (_record)
                                        (declare (ignore _record))
                                        ,@body)))
	   (do-formatting-table ,sheet formatting-table-body ,@options)))))


#||
define macro formatting-row
  { formatting-row (?record:variable = ?sheet:variable, #rest ?options:expression)
      ?:body
    end }
    => { begin
           let formatting-row-body = method (?record) ?body end;
           do-formatting-row(?sheet, formatting-row-body, ?options)
         end }
  { formatting-row (?sheet:variable, #rest ?options:expression)
      ?:body
    end }
    => { begin
           let formatting-row-body = method (_record) ignore(_record); ?body end;
           do-formatting-row(?sheet, formatting-row-body, ?options)
         end }
end macro formatting-row;
||#

(defmacro formatting-row ((drawable &rest options) &body body)
  (if (listp drawable)
      ;; formatting-row (record = sheet) case
      (destructuring-bind (record = sheet)
	  drawable
	(declare (ignore =))
        `(let ((formatting-row-body (lambda (,record) ,@body)))
	   (do-formatting-row ,sheet formatting-row-body ,@options)))
      ;; else formatting-row (sheet) case
      (let ((sheet drawable))
        `(let ((formatting-row-body (lambda (_record)
                                      (declare (ignore _record))
                                      ,@body)))
	   (do-formatting-row ,sheet formatting-row-body ,@options)))))


#||
define macro formatting-column
  { formatting-column (?record:variable = ?sheet:variable, #rest ?options:expression)
      ?:body
    end }
    => { begin
           let formatting-column-body = method (?record) ?body end;
           do-formatting-column(?sheet, formatting-column-body, ?options)
         end }
  { formatting-column (?sheet:variable, #rest ?options:expression)
      ?:body
    end }
    => { begin
           let formatting-column-body = method (_record) ignore(_record); ?body end;
           do-formatting-column(?sheet, formatting-column-body, ?options)
         end }
end macro formatting-column;
||#

(defmacro formatting-column ((drawable &rest options) &body body)
  (if (listp drawable)
      ;; formatting-column (record = sheet) case
      (destructuring-bind (record = sheet)
	  drawable
	(declare (ignore =))
        `(let ((formatting-column-body (lambda (,record) ,@body)))
	   (do-formatting-column ,sheet formatting-column-body ,options)))
      ;; else (formatting-row (sheet) case
      (let ((sheet drawable))
        `(let ((formatting-column-body (lambda (_record)
                                         (declare (ignore _record))
                                         ,@body)))
	   (do-formatting-column ,sheet formatting-row-body ,@options)))))


#||
define macro formatting-cell
  { formatting-cell (?record:variable = ?sheet:variable, #rest ?options:expression)
      ?:body
    end }
    => { begin
           let formatting-cell-body = method (?record) ?body end;
           do-formatting-cell(?sheet, formatting-cell-body, ?options)
         end }
  { formatting-cell (?sheet:variable, #rest ?options:expression)
      ?:body
    end }
    => { begin
           let formatting-cell-body = method (_record) ignore(_record); ?body end;
           do-formatting-cell(?sheet, formatting-cell-body, ?options)
         end }
end macro formatting-cell;
||#

(defmacro formatting-cell ((drawable &rest options) &body body)
  (if (listp drawable)
      ;; formatting-cell (record = sheet) case
      (destructuring-bind (record = sheet)
	  drawable
	(declare (ignore =))
        `(let ((formatting-cell-body (lambda (,record) ,@body)))
	   (do-formatting-cell ,sheet formatting-cell-body ,@options)))
      ;; else (formatting-cell (sheet) case
      (let ((sheet drawable))
        `(let ((formatting-cell-body (lambda (_record)
                                       (declare (ignore _record))
                                       ,@body)))
	   (do-formatting-cell ,sheet formatting-cell-body ,@options)))))


#||
define macro formatting-items
  { formatting-items (?record:variable = ?sheet:variable, #rest ?options:expression)
      ?:body
    end }
    => { begin
           let formatting-items-body = method (?record) ?body end;
           do-formatting-items(?sheet, formatting-items-body, ?options)
         end }
  { formatting-items (?sheet:variable, #rest ?options:expression)
      ?:body
    end }
    => { begin
           let formatting-items-body = method (_record) ignore(_record); ?body end;
           do-formatting-items(?sheet, formatting-items-body, ?options)
         end }
end macro formatting-items;
||#

(defmacro formatting-items ((drawable &rest options) &body body)
  (if (listp drawable)
      ;; formatting-items (record = sheet) case
      (destructuring-bind (record = sheet)
	  drawable
	(declare (ignore =))
        `(let ((formatting-items-body (lambda (,record) ,@body)))
	   (do-formatting-items ,sheet formatting-items-body ,@options)))
      ;; else (formatting-items (sheet) case
      (let ((sheet drawable))
        `(let ((formatting-items-body (lambda (_record)
                                        (declare (ignore _record))
                                        ,@body)))
	   (do-formatting-items ,sheet formatting-items-body ,@options)))))


