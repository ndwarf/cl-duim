;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GADGET-PANES-INTERNALS -*-
(in-package #:duim-gadget-panes-internals)

#||
/// Shared list/table/tree control pane functionality

define open abstract class <homegrown-control-mixin> (<collection-gadget>)
  sealed slot %framem :: <frame-manager>,
    required-init-keyword: frame-manager:;
  sealed slot %layout-pane :: false-or(<layout-pane>) = #f;
  sealed slot %old-selection = #[];
end class <homegrown-control-mixin>;
||#

(defclass <homegrown-control-mixin> (<collection-gadget>)
  ((%framem :type <frame-manager> :initarg :frame-manager :accessor %framem
	    :initform (required-slot ":frame-manager" "<homegrown-control-mixin>"))
   (%layout-pane :type (or null <layout-pane>) :initform nil :accessor %layout-pane)
   ;; FIXME: Should this have a fill-pointer?
   (%old-selection :initform #() :accessor %old-selection)))


(defgeneric layout-homegrown-control (pane))
(defgeneric item-to-index (control item))
(defgeneric index-to-item (control index))
(defgeneric item-selected? (control item))
(defgeneric control-for-item (sheet))
(defgeneric control-and-item-from-button (button))

(defgeneric set-control-selection (pane event))

#||
define method frame-manager
    (pane :: <homegrown-control-mixin>) => (framem :: false-or(<frame-manager>))
  pane.%framem | next-method()
end method frame-manager;
||#

(defmethod frame-manager ((pane <homegrown-control-mixin>))
  (or (%framem pane)
      (call-next-method)))


#||
define open abstract class <homegrown-control-layout-mixin> (<layout>)
  sealed slot %control-pane :: false-or(<homegrown-control-mixin>) = #f;
end class <homegrown-control-layout-mixin>;
||#

(defclass <homegrown-control-layout-mixin> (<layout>)
  ((%control-pane :type (or null <homegrown-control-mixin>) :initform nil :accessor %control-pane)))


#||
define thread variable *layout-delayed?* = #f;
||#

(define-thread-variable *layout-delayed?* nil)


#||
// Inhibit updating any scroll bars for the duration of the body
define macro delaying-layout
  { delaying-layout (?pane:expression) ?:body end }
    => { begin
	   let _pane = ?pane;
	   block ()
	     dynamic-bind (*layout-delayed?* = _pane)
               ?body
             end
	   cleanup
	     layout-homegrown-control(_pane)
	   end
	 end }
end macro delaying-layout;
||#

(defmacro delaying-layout ((pane) &body body)
  `(let ((_pane ,pane))
    (unwind-protect
         (dynamic-let ((*layout-delayed?* _pane))
           ,@body)
      ;; cleanup
      (layout-homegrown-control _pane))))


#||
define method layout-homegrown-control
    (pane :: <homegrown-control-mixin>) => ()
  when (sheet-mapped?(pane))
    unless (*layout-delayed?*)
      let layout = pane.%layout-pane;
      // Hack to get the screen to redisplay
      clear-box*(sheet-viewport(layout) | layout, sheet-viewport-region(layout));
      // Force relayout of all items, then notify up the sheet tree
      // if anything changed
      relayout-parent(layout);
      // Ensure that all kids are mapped
      sheet-mapped?(pane, do-repaint?: #f) := #t;
      repaint-sheet(layout, $everywhere);
      force-display(pane)
    end
  end
end method layout-homegrown-control;
||#

(defmethod layout-homegrown-control ((pane <homegrown-control-mixin>))
  (when (sheet-mapped? pane)
    (unless *layout-delayed?*
      (let ((layout (%layout-pane pane)))
        ;; Hack to get the screen to redisplay
        (clear-box* (or (sheet-viewport layout) layout) (sheet-viewport-region layout))
        ;; Force relayout of all items, then notify up the sheet tree
        ;; if anything changed
        (relayout-parent layout)
        ;; Ensure that all kids are mapped
        (setf (sheet-mapped? pane :do-repaint? nil) t)
        (repaint-sheet layout *everywhere*)
        (force-display pane)))))


#||
define method note-gadget-selection-changed
    (pane :: <homegrown-control-mixin>) => ()
  when (sheet-mapped?(pane))
    let layout = pane.%layout-pane;
    let n-items :: <integer> = size(sheet-children(layout));
    select (gadget-selection-mode(pane))
      #"none"   => #f;
      #"single", #"multiple" =>
	for (index :: <integer> in pane.%old-selection)
	  when (index < n-items)
	    let old = index-to-item(pane, index);
	    clear-box*(old, sheet-region(old));
	    repaint-sheet(old, $everywhere)
	  end
	end;
        for (index :: <integer> in gadget-selection(pane))
	  when (index < n-items)
	    let new = index-to-item(pane, index);
	    repaint-sheet(new, $everywhere)
	  end
        end
    end;
    force-display(pane)
  end;
  pane.%old-selection := gadget-selection(pane)
end method note-gadget-selection-changed;
||#

(defmethod note-gadget-selection-changed ((pane <homegrown-control-mixin>))
  (when (sheet-mapped? pane)
    (let* ((layout  (%layout-pane pane))
           (n-items (size (sheet-children layout))))
      (ecase (gadget-selection-mode pane)
        (:none nil)
        ((:single :multiple)
         (loop for index across (%old-selection pane)
	    do (when (< index n-items)
		 (let ((old (index-to-item pane index)))
		   (clear-box* old (sheet-region old))
		   (repaint-sheet old *everywhere*))))
         (loop for index across (gadget-selection pane)
	    do (when (< index n-items)
		 (let ((new (index-to-item pane index)))
		   (repaint-sheet new *everywhere*))))))
      (force-display pane)))
  (setf (%old-selection pane) (gadget-selection pane)))


#||
define method handle-event
    (control :: <homegrown-control-mixin>, event :: <button-press-event>) => ()
  when (gadget-enabled?(control))
    select (event-button(event))
      $right-button =>
	let x = event-x(event);
	let y = event-y(event);
	execute-popup-menu-callback
	  (control, gadget-client(control), gadget-id(control), #f, x: x, y: y);
      $left-button =>
        when (gadget-selection-mode(control) == #"multiple")
	  gadget-selection(control, do-callback?: #t) := #[]
	end;
      otherwise =>
	#f;
    end
  end
end method handle-event;
||#

(defmethod handle-event ((control <homegrown-control-mixin>) (event <button-press-event>))
  ;; FIXME: THIS SEEMS NOT TO EVER BE INVOKED. IS IT BEING SHADOWED BY SOMETHING?
  (duim-debug-message "extended;panes;list-control-mixins.HANDLE-EVENT entered, gadget-enabled? = ~a" (gadget-enabled? control))
  (when (gadget-enabled? control)
    (cond ((eql (event-button event) +right-button+)
	   (let ((x (event-x event))
		 (y (event-y event)))
	     (execute-popup-menu-callback control
					  (gadget-client control)
					  (gadget-id control)
					  nil
					  :x x :y y)))
	  ((eql (event-button event) +left-button+)
	   (when (eql (gadget-selection-mode control) :multiple)
	     (setf (gadget-selection control :do-callback? t) #())))
	  (t nil))))


#||
define method handle-event
    (pane :: <homegrown-control-layout-mixin>, event :: <button-press-event>) => ()
  let control = pane.%control-pane;
  when (gadget-enabled?(control))
    handle-event(event-handler(control), event)
  end
end method handle-event;
||#

(defmethod handle-event ((pane <homegrown-control-layout-mixin>) (event <button-press-event>))
  (let ((control (%control-pane pane)))
    (when (gadget-enabled? control)
      (handle-event (event-handler control) event))))


#||
define method handle-event
    (control :: <homegrown-control-mixin>, event :: <key-press-event>) => ()
  when (gadget-enabled?(control))
    execute-key-press-callback
      (control, gadget-client(control), gadget-id(control), event-key-name(event))
  end
end method handle-event;
||#

(defmethod handle-event ((control <homegrown-control-mixin>) (event <key-press-event>))
  (when (gadget-enabled? control)
    (execute-key-press-callback control
                                (gadget-client control)
                                (gadget-id control)
                                (event-key-name event))))


#||
define method handle-event
    (pane :: <homegrown-control-layout-mixin>, event :: <key-press-event>) => ()
  let control = pane.%control-pane;
  when (gadget-enabled?(control))
    handle-event(event-handler(control), event)
  end
end method handle-event;
||#

(defmethod handle-event ((pane <homegrown-control-layout-mixin>) (event <key-press-event>))
  (let ((control (%control-pane pane)))
    (when (gadget-enabled? control)
      (handle-event (event-handler control) event))))



#||

/// Shared list/table/tree control label button functionality

define constant $item-label-border :: <integer> = 5;
||#

(defconstant +item-label-border+ 5)


#||
define open abstract class <homegrown-control-button-mixin> (<abstract-sheet>)
end class <homegrown-control-button-mixin>;
||#

(defclass <homegrown-control-button-mixin>
    (<abstract-sheet>)
  ())


#||
define method do-compose-space
    (pane :: <homegrown-control-button-mixin>, #key width, height)
 => (space-req :: <space-requirement>)
  ignore(width, height);
  let (width, height) = gadget-label-size(pane);
  let extra-width = $item-label-border * 2;
  make(<space-requirement>,
       width: width + extra-width, height: height)
end method do-compose-space;
||#

(defmethod do-compose-space ((pane <homegrown-control-button-mixin>) &key width height)
  (declare (ignore width height))
  (multiple-value-bind (width height)
      (gadget-label-size pane)
    (let ((extra-width (* +item-label-border+ 2)))
      (make-space-requirement :width (+ width extra-width) :height height))))


#||
define method handle-repaint
    (pane :: <homegrown-control-button-mixin>, medium :: <medium>, region :: <region>) => ()
  let (control, item) = control-and-item-from-button(pane);
  let selected? = item-selected?(control, item);
  let offset = $item-label-border;
  when (selected?)
    let (left, top, right, bottom) = box-edges(pane);
    with-drawing-options (medium, brush: $foreground)
      draw-rectangle(medium, left, top, right, bottom, filled?: #t)
    end
  end;
  draw-gadget-label(pane, medium, 0 + offset, 0, 
                    brush: selected? & $background)
end method handle-repaint;
||#

(defmethod handle-repaint ((pane <homegrown-control-button-mixin>) (medium <medium>) (region <region>))
  (multiple-value-bind (control item)
      (control-and-item-from-button pane)
    (let ((selected? (item-selected? control item))
          (offset +item-label-border+))
      (when selected?
        (multiple-value-bind (left top right bottom)
            (box-edges pane)
          (with-drawing-options (medium :brush *foreground*)
            (draw-rectangle medium left top right bottom :filled? t))))
      (draw-gadget-label pane medium (+ 0 offset) 0
                         :brush (and selected? *background*)))))


#||
define method handle-event 
    (pane :: <homegrown-control-button-mixin>, event :: <button-press-event>) => ()
  when (gadget-enabled?(pane))
    // First set the selection
    let control = set-control-selection(pane, event);
    select (event-button(event))
      $left-button => #f;
      $middle-button =>
	activate-gadget(control);
      $right-button =>
	let (control, item) = control-and-item-from-button(pane);
	// Pass the value of the selected object to the callback
	let value = gadget-item-value(control, item-object(item));
	execute-popup-menu-callback
	  (control, gadget-client(control), gadget-id(control), value,
	   x: event-x(event), y: event-y(event));
    end
  end
end method handle-event;
||#

(defmethod handle-event ((pane <homegrown-control-button-mixin>) (event <button-press-event>))
  (when (gadget-enabled? pane)
    ;; First set the selection
    (let ((control (set-control-selection pane event))
	  (event-button (event-button event)))
      (cond ((eql event-button +left-button+) nil)
	    ((eql event-button +middle-button+) (activate-gadget control))
	    ((eql event-button +right-button+)
	     (multiple-value-bind (control item)
		 (control-and-item-from-button pane)
	       (let ((value (gadget-item-value control (item-object item))))
		 (execute-popup-menu-callback control
					      (gadget-client control) (gadget-id control) value
					      :x (event-x event) :y (event-y event)))))))))


#||
define method handle-event 
    (pane :: <homegrown-control-button-mixin>, event :: <double-click-event>) => ()
  when (gadget-enabled?(pane)
	& event-button(event) == $left-button)
    // Set the selection and activate
    let control = set-control-selection(pane, event);
    activate-gadget(control)
  end
end method handle-event;
||#

(defmethod handle-event ((pane <homegrown-control-button-mixin>) (event <double-click-event>))
  (when (and (gadget-enabled? pane)
             (eql (event-button event) +left-button+))
    ;; Set the selection and activate
    (let ((control (set-control-selection pane event)))
      (activate-gadget control))))


#||
define method handle-event
    (pane :: <homegrown-control-button-mixin>, event :: <key-press-event>) => ()
  let (control, item) = control-and-item-from-button(pane);
  when (gadget-enabled?(control))
    execute-key-press-callback
      (control, gadget-client(control), gadget-id(control), event-key-name(event))
  end
end method handle-event;
||#

(defmethod handle-event ((pane <homegrown-control-button-mixin>) (event <key-press-event>))
  (multiple-value-bind (control item)
      (control-and-item-from-button pane)
    (declare (ignore item))
    (when (gadget-enabled? control)
      (execute-key-press-callback control (gadget-client control) (gadget-id control) (event-key-name event)))))


#||
define method set-control-selection
    (pane :: <homegrown-control-button-mixin>, event) => (control)
  let (control, item) = control-and-item-from-button(pane);
  let index     = item-to-index(control, item);
  let selection = gadget-selection(control);
  select (gadget-selection-mode(control))
    #"none"   => #f;
    #"single" =>
      gadget-selection(control, do-callback?: #t) := vector(index);
    #"multiple" =>
      select (event-modifier-state(event))
	$shift-key =>
	  let min-index = reduce(min, index, selection);
	  let max-index = reduce(max, index, selection);
	  gadget-selection(control, do-callback?: #t)
	    := range(from: min-index, to: max-index);
	$control-key =>
	  if (member?(index, selection))
	    gadget-selection(control, do-callback?: #t) := remove(selection, index);
	  else
	    gadget-selection(control, do-callback?: #t) := add(selection, index);
	  end;
	otherwise =>
	  gadget-selection(control, do-callback?: #t) := vector(index);
      end;
  end;
  control
end method set-control-selection;
||#

(defmethod set-control-selection ((pane <homegrown-control-button-mixin>) event)
  (multiple-value-bind (control item)
      (control-and-item-from-button pane)
    (let ((index (item-to-index control item))
          (selection (gadget-selection control)))
      (ecase (gadget-selection-mode control)
        (:none     nil)
        (:single   (setf (gadget-selection control :do-callback? t) (vector index)))
        (:multiple (case (event-modifier-state event)
                     (+shift-key+
                      (let ((min-index (reduce #'min selection :initial-value index))
                            (max-index (reduce #'max selection :initial-value index)))
                        (setf (gadget-selection control :do-callback? t)
                              (range :from min-index :to max-index))))
                     (+control-key+
                      (if (MEMBER? index selection)
                          (setf (gadget-selection control :do-callback? t) (remove index selection))
                          ;; else
                          (setf (gadget-selection control :do-callback? t) (add selection index))))
                     (t
                      (setf (gadget-selection control :do-callback? t) (vector index))))))
      control)))


#||
define method item-to-index
    (control :: <homegrown-control-mixin>, item)
 => (index :: <integer>)
  let layout = control.%layout-pane;
  let index  = position(sheet-children(layout), item);
  index | error("Failed to find item %= in %=", item, control)
end method item-to-index;
||#

(defmethod item-to-index ((control <homegrown-control-mixin>) item)
  (let* ((layout (%layout-pane control))
         (index  (position item (sheet-children layout))))
    (or index (error "Failed to find item ~a in ~a" item control))))


#||
define method index-to-item
    (control :: <homegrown-control-mixin>, index :: <integer>)
 => (item)
  let layout = control.%layout-pane;
  sheet-children(layout)[index]
end method index-to-item;
||#

(defmethod index-to-item ((control <homegrown-control-mixin>) (index integer))
  (let ((layout (%layout-pane control)))
    (SEQUENCE-ELT (sheet-children layout) index)))


#||
define method item-selected?
    (control :: <homegrown-control-mixin>, item)
 => (selected? :: <boolean>)
  block (return)
    let layout   = control.%layout-pane;
    let children = sheet-children(layout);
    let n-children :: <integer> = size(children);
    for (index :: <integer> in gadget-selection(control))
      when (index < n-children)
	let child = children[index];
	when (child == item)
	  return(#t)
	end
      end
    end;
    #f
  end
end method item-selected?;
||#

(defmethod item-selected? ((control <homegrown-control-mixin>) item)
  (let* ((layout     (%layout-pane control))
	 (children   (sheet-children layout))
	 (n-children (size children)))
    (map nil #'(lambda (index)
		 (when (< index n-children)
		   (let ((child (SEQUENCE-ELT children index)))
		     (when (eql child item)
		       (return-from item-selected? t)))))
	 (gadget-selection control)))
  nil)


#||
define method control-for-item
    (sheet :: <sheet>) 
 => (sheet :: false-or(<homegrown-control-mixin>))
  let parent = sheet-parent(sheet);
  case
    instance?(parent, <homegrown-control-mixin>) =>
      parent;
    parent =>
      control-for-item(parent);
    otherwise =>
      #f;
  end
end method control-for-item;
||#

(defmethod control-for-item ((sheet <sheet>))
  (let ((parent (sheet-parent sheet)))
    (cond
      ((typep parent '<homegrown-control-mixin>)
       parent)
      (parent
       (control-for-item parent))
      (t nil))))


#||
define method control-and-item-from-button
    (button :: <homegrown-control-button-mixin>) => (control, item)
  // The parent of a list item button is always, and only, a list item
  let item = sheet-parent(button);
  //--- Can we make this more elegant?
  // The parent of a list item is always, and only, a list control,
  // except for table-controls.
  let parent = control-for-item(item);
  let control
    = if (instance?(parent, <homegrown-control-mixin>))
        parent
      else
        sheet-parent(parent)
      end;
  values(control, item)
end method control-and-item-from-button;
||#

(defmethod control-and-item-from-button ((button <homegrown-control-button-mixin>))
  ;; The parent of a list item button is always, and only, a list item
  (let* ((item (sheet-parent button))
         ;;--- Can we make this more elegant?
         ;; The parent of a list item is always, and only, a list control,
         ;; except for table-controls.
         (parent (control-for-item item))
         (control (if (typep parent '<homegrown-control-mixin>)
                      parent
                      (sheet-parent parent))))
    (values control item)))
