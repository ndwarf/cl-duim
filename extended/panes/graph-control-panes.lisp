;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GADGET-PANES-INTERNALS -*-
(in-package #:duim-gadget-panes-internals)

#||
/// Generic implementation of graph control panes

define sealed class <graph-control-pane>
    (<standard-input-mixin>,
     <standard-repainting-mixin>,
     <permanent-medium-mixin>,
     <homegrown-tree-control-mixin>,
     <graph-control>,
     <single-child-wrapping-pane>)
end class <graph-control-pane>;
||#

(defclass <graph-control-pane>
    (<standard-input-mixin>
     <standard-repainting-mixin>
     <permanent-medium-mixin>
     <homegrown-tree-control-mixin>
     <graph-control>
     <single-child-wrapping-pane>)
  ())


#||
define sideways method class-for-make-pane 
    (framem :: <frame-manager>, class == <graph-control>, #key graph-type = #"tree")
 => (class :: <class>, options :: false-or(<sequence>))
  select (graph-type)
    #"tree" => values(<tree-graph-pane>, #f);
    #"DAG"  => values(<DAG-graph-pane>,  #f);
  end
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <frame-manager>) (class (eql (find-class '<graph-control>))) &key (graph-type :tree))
  (ecase graph-type
    (:tree (values (find-class '<tree-graph-pane>) nil))
    (:DAG  (values (find-class '<DAG-graph-pane>) nil))))


#||
define sealed domain make (subclass(<graph-control-pane>));
define sealed domain initialize (<graph-control-pane>);

define method initialize
    (graph :: <graph-control-pane>,
     #key frame-manager: framem)
  next-method();
  with-frame-manager (framem)
    let layout-class
      = layout-class-for-graph(graph);
    let layout
      = make(layout-class, graph-control: graph);
    graph.%layout-pane   := layout;
    layout.%control-pane := graph;
    let scroll-bars = gadget-scroll-bars(graph);
    sheet-child(graph) 
      := if (scroll-bars == #"none")
	   layout
	 else
	   scrolling (scroll-bars: scroll-bars, border-type: #f)
	     layout
	   end
         end
  end;
end method initialize;
||#

(defmethod initialize-instance :after ((graph <graph-control-pane>) &key ((:frame-manager framem)) &allow-other-keys)
  (with-frame-manager (framem)
    (let* ((layout-class (layout-class-for-graph graph))
           (layout (make-pane layout-class :graph-control graph)))
      (setf (%layout-pane graph) layout)
      (setf (%control-pane layout) graph)
      (let ((scroll-bars (gadget-scroll-bars graph)))
	(setf (sheet-child graph)
	      (if (eql scroll-bars :none)
		  layout
		  ;; else
		  (scrolling (:scroll-bars scroll-bars :border-type nil)
			     layout)))))))




(defgeneric draw-edge (edge medium region))
(defgeneric edge-attachment-points (parent child orientation))
(defgeneric find-edge-at-position (layout x y))
(defgeneric set-edge-selection (graph edge event))
(defgeneric layout-class-for-graph (graph))
(defgeneric generate-graph-nodes (graph roots &key key test))
(defgeneric layout-graph-nodes (layout))
(defgeneric layout-graph-edges (layout &key reuse-edges?))
(defgeneric traverse-graph (roots child-mapper-fn node-table key new-node-fn old-node-fn &key max-depth))

#||

/// Roots and items

// Build the items for the first time when the sheet is fully attached
define method note-sheet-attached
    (graph :: <graph-control-pane>) => ()
  next-method();
  initialize-tree-control-icons(port(graph), graph);
  //--- Too bad we've lost the 'value:' initarg by the time we get here
  note-tree-control-roots-changed(graph)
end method note-sheet-attached;
||#

(defmethod note-sheet-attached ((graph <graph-control-pane>))
  (call-next-method)
  (initialize-tree-control-icons (port graph) graph)
  ;;--- Too bad we've lost the ':value' initarg by the time we get here
  (note-tree-control-roots-changed graph))


#||
define sealed method note-tree-control-roots-changed
    (graph :: <graph-control-pane>, #key value = $unsupplied) => ()
  with-busy-cursor (graph)
    next-method();
    let roots  = tree-control-roots(graph);
    let layout = graph.%layout-pane;
    let state  = sheet-state(graph);
    when (state == #"mapped")
      clear-box*(sheet-viewport(layout) | layout, sheet-viewport-region(layout))
    end;
    gadget-selection(graph) := #[];
    sheet-children(layout) := #[];
    graph.%visible-items := #f;
    delaying-layout (graph)
      generate-graph-nodes(graph, roots);
      let items = gadget-items(graph);
      // Try to preserve the old value and selection
      select (gadget-selection-mode(graph))
	#"single" =>
	  unless (empty?(items))
	    let index = supplied?(value) & position(items, value);
	    if (index)
	      gadget-selection(graph) := vector(index)
	    else
	      gadget-selection(graph) := #[0]
	    end
	  end;
	#"multiple" =>
	  let selection :: <stretchy-object-vector> = make(<stretchy-vector>);
	  when (supplied?(value))
	    for (v in value)
	      let index = position(items, v);
	      when (index)
		add!(selection, index)
	      end
	    end
	  end;
	  unless (empty?(selection))
	    gadget-selection(graph) := selection
	  end;
	otherwise =>
	  #f;
      end;
    end
  end
end method note-tree-control-roots-changed;
||#

(defmethod note-tree-control-roots-changed ((graph <graph-control-pane>) &key (value :unsupplied))
  (with-busy-cursor (graph)
    (call-next-method)
    (let ((roots (tree-control-roots graph))
	  (layout (%layout-pane graph))
	  (state (sheet-state graph)))
      (when (eql state :mapped)
	(clear-box* (or (sheet-viewport layout) layout) (sheet-viewport-region layout)))
      (setf (gadget-selection graph) (MAKE-SIMPLE-VECTOR))
      (setf (sheet-children layout) (MAKE-SIMPLE-VECTOR))
      (setf (%visible-items graph) nil)
      (delaying-layout (graph)
	(generate-graph-nodes graph roots)
	(let ((items (gadget-items graph)))
	  ;; Try to preserve the old value and selection
	  (case (gadget-selection-mode graph)
	    (:single (unless (empty? items)
		       (let ((index (and (supplied? value) (position value items))))
			 (if index
			     (setf (gadget-selection graph) (vector index))
			     (setf (gadget-selection graph) #(0))))))
	    (:multiple (let ((selection (MAKE-STRETCHY-VECTOR)))
			 (when (supplied? value)
			   (map nil #'(lambda (v)
					(let ((index (position v items)))
					  (when index
					    (ADD! selection index))))
				value))
			 (unless (empty? selection)
			   (setf (gadget-selection graph) selection))))
	    (t nil)))))))


#||
define sealed method gadget-items
    (graph :: <graph-control-pane>) => (items :: <sequence>)
  graph.%visible-items
  | begin
      let layout = graph.%layout-pane;
      if (layout)
	let items :: <stretchy-object-vector> = make(<stretchy-vector>);
	for (node in sheet-children(layout))
	  unless (sheet-withdrawn?(node))
	    add!(items, node-object(node))
	  end
	end;
	when (graph-edge-generator(graph))
	  for (edge :: <graph-edge> in layout.%edges)
	    when (node-state(graph-edge-from-node(edge)) == #"expanded")
	      unless (sheet-withdrawn?(graph-edge-to-node(edge)))
		add!(items, graph-edge-object(edge))
	      end
	    end
	  end
	end;
	graph.%visible-items := items
      else
        #[]
      end
    end
end method gadget-items;
||#

(defmethod gadget-items ((graph <graph-control-pane>))
  (or (%visible-items graph)
      (let ((layout (%layout-pane graph)))
	(if layout
	    (let ((items (MAKE-STRETCHY-VECTOR)))
	      (loop for node across (sheet-children layout)
		 do (unless (sheet-withdrawn? node)
		      (ADD! items (node-object node))))
	      (when (graph-edge-generator graph)
	        (map nil
		     #'(lambda (edge)
			 (when (eql (node-state (graph-edge-from-node edge)) :expanded)
			   (unless (sheet-withdrawn? (graph-edge-to-node edge))
			     (ADD! items (graph-edge-object edge)))))
		     (%edges layout)))
	      (setf (%visible-items graph) items))
	    ;; else
	    #()))))



#||

/// Generic implementation of graph node panes

define sealed class <graph-node-pane>
    (<tree-node-pane-mixin>, <graph-node>)
end class <graph-node-pane>;
||#

(defclass <graph-node-pane>
    (<tree-node-pane-mixin> <graph-node>)
  ())


#||
define sealed domain make (singleton(<graph-node-pane>));
define sealed domain initialize (<graph-node-pane>);

define method do-make-node 
    (graph :: <graph-control-pane>, class == <graph-node>,
     #rest initargs, #key object)
 => (item :: <graph-node-pane>)
  let framem = frame-manager(graph);
  let label-function = gadget-label-key(graph);
  let icon-function  = tree-control-icon-function(graph);
  let label = (label-function & label-function(object)) | "";
  let (icon, selected-icon)
    = if (icon-function) icon-function(object) else values(#f, #f) end;
  apply(make, <graph-node-pane>,
	tree:  graph,
	label: label,
	icon:  icon,
	selected-icon: selected-icon,
	x-spacing: 4, y-alignment: #"center",
	frame-manager: framem,
	initargs)
end method do-make-node;
||#

(defmethod do-make-node ((graph <graph-control-pane>) (class (eql (find-class '<graph-node>)))
			 &rest initargs &key object)
  (let* ((framem (frame-manager graph))
	 (label-function (gadget-label-key graph))
	 (icon-function (tree-control-icon-function graph))
	 (label (or (and label-function (funcall label-function object)) "")))
    (multiple-value-bind (icon selected-icon)
	(if icon-function (funcall icon-function object) (values nil nil))
      (apply #'make-pane (find-class '<graph-node-pane>)
	     :tree graph
	     :label label
	     :icon icon
	     :selected-icon selected-icon
	     :x-spacing 4 :y-alignment :center
	     :frame-manager framem
	     initargs))))


#||
define method node-x
    (node :: <graph-node-pane>) => (x :: <integer>)
  let (x, y) = sheet-position(node);
  ignore(y);
  x
end method node-x;
||#

(defmethod node-x ((node <graph-node-pane>))
  (multiple-value-bind (x y)
      (sheet-position node)
    (declare (ignore y))
    x))


#||
define method node-x-setter
    (new-x :: <integer>, node :: <graph-node-pane>) => (x :: <integer>)
  let (x, y) = sheet-position(node);
  ignore(x);
  set-sheet-position(node, new-x, y);
  new-x
end method node-x-setter;
||#

(defmethod (setf node-x) ((new-x integer) (node <graph-node-pane>))
  (multiple-value-bind (x y)
      (sheet-position node)
    (declare (ignore x))
    (set-sheet-position node new-x y))
  new-x)


#||
define method node-y
    (node :: <graph-node-pane>) => (y :: <integer>)
  let (x, y) = sheet-position(node);
  ignore(x);
  y
end method node-y;
||#

(defmethod node-y ((node <graph-node-pane>))
  (multiple-value-bind (x y)
      (sheet-position node)
    (declare (ignore x))
    y))


#||
define method node-y-setter
    (new-y :: <integer>, node :: <graph-node-pane>) => (y :: <integer>)
  let (x, y) = sheet-position(node);
  ignore(y);
  set-sheet-position(node, x, new-y);
  new-y
end method node-y-setter;
||#

(defmethod (setf node-y) ((new-y integer) (node <graph-node-pane>))
  (multiple-value-bind (x y)
      (sheet-position node)
    (declare (ignore y))
    (set-sheet-position node x new-y))
  new-y)


#||
define sealed method do-find-node
    (graph :: <graph-control-pane>, object, #key node: parent-node)
 => (node :: false-or(<graph-node-pane>))
  let key  = gadget-value-key(graph);
  let test = gadget-test(graph);
  let the-key = key(object);
  block (return)
    for (node in sheet-children(graph.%layout-pane))
      when (test(key(node-object(node)), the-key))
	// Is it a child of the requested node?
	when (~parent-node | member?(node, node-children(parent-node)))
	  return(node)
	end
      end
    end;
    #f
  end
end method do-find-node;
||#

(defmethod do-find-node ((graph <graph-control-pane>) object &key ((node parent-node)))
  (let* ((key (gadget-value-key graph))
         (test (gadget-test graph))
         (the-key (funcall key object)))
    (loop for node across (sheet-children (%layout-pane graph))
       do (when (funcall test (funcall key (node-object node)) the-key)
	    ;; Is it a child of the requested node?
	    (when (or (not parent-node) (MEMBER? node (node-children parent-node)))
	      (return-from do-find-node node))))
    (sheet-children (%layout-pane graph)))
  nil)


#||
// Note that we don't relayout the graph -- that only happens
// in 'expand-node' and 'contract-node'
define sealed method do-add-node
    (graph :: <graph-control-pane>, parent, node :: <graph-node-pane>, #key after) => ()
  graph.%visible-items := #f;
  unless (after)
    // The idea here is to ensure that the new node comes after
    // the last node (and all of that node's descendents) in its
    // own generation
    local method last-child (node) => (child)
	    if (empty?(node-children(node)))
	      node
	    else
	      last-child(last(node-children(node)))
	    end
	  end method;
    unless (empty?(node-parents(node)))
      after := last-child(node-parents(node)[0])
    end
  end;
  let layout = graph.%layout-pane;
  let index = after & position(sheet-children(layout), after);
  add-child(layout, node, index: if (index) index + 1 else #"end" end);
  sheet-withdrawn?(node, do-repaint?: #f) := #t
end method do-add-node;
||#

;; Note that we don't relayout the graph -- that only happens
;; in 'expand-node' and 'contract-node'
(defmethod do-add-node ((graph <graph-control-pane>) parent (node <graph-node-pane>) &key after)
  (declare (ignore parent))
  (setf (%visible-items graph) nil)
  (unless after
    ;; The idea here is to ensure that the new node comes after
    ;; the last node (and all of that node's descendents) in its
    ;; own generation
    (labels ((last-child (node)
               (if (empty? (node-children node))
                   node
                   ;; else
                   (LAST-CHILD (alexandria:last-elt (node-children node))))))
      (unless (empty? (node-parents node))
        (setf after (LAST-CHILD (SEQUENCE-ELT (node-parents node) 0))))))
  (let* ((layout (%layout-pane graph))
         (index (and after (position after (sheet-children layout)))))
    (add-child layout node :index (if index (+ index 1) :end))
    (setf (sheet-withdrawn? node :do-repaint? nil) t)))


#||
define sealed method do-add-nodes
    (graph :: <graph-control-pane>, parent, nodes :: <sequence>, #key after) => ()
  let selected-nodes = gadget-selected-nodes(graph);
  graph.%visible-items := #f;
  gadget-selection(graph) := #[];
  for (node in nodes)
    add-node(graph, parent, node, after: after)
  end;
  gadget-selection(graph) := compute-gadget-selection(graph, selected-nodes)
end method do-add-nodes;
||#

(defmethod do-add-nodes ((graph <graph-control-pane>) parent (nodes sequence) &key after)
  (let ((selected-nodes (gadget-selected-nodes graph)))
    (setf (%visible-items graph) nil)
    (setf (gadget-selection graph) #())
    (map nil #'(lambda (node)
		 (add-node graph parent node :after after))
	 nodes)
    (setf (gadget-selection graph) (compute-gadget-selection graph selected-nodes))))


#||
// Note that we don't relayout the graph -- that only happens
// in 'expand-node' and 'contract-node'
define sealed method do-remove-node
    (graph :: <graph-control-pane>, node :: <graph-node-pane>) => ()
  graph.%visible-items := #f;
  remove-child(graph.%layout-pane, node)
end method do-remove-node;
||#

(defmethod do-remove-node ((graph <graph-control-pane>) (node <graph-node-pane>))
  (setf (%visible-items graph) nil)
  (remove-child (%layout-pane graph) node))


#||
define sealed method gadget-selected-nodes
    (graph :: <graph-control-pane>) => (nodes :: <sequence>)
  let nodes :: <stretchy-object-vector> = make(<stretchy-vector>);
  let selection = gadget-selection(graph);
  let index :: <integer> = -1;
  for (node :: <graph-node-pane> in sheet-children(graph.%layout-pane))
    unless (sheet-withdrawn?(node))
      inc!(index);
      when (member?(index, selection))
	add!(nodes, node)
      end
    end
  end;
  nodes
end method gadget-selected-nodes;
||#

(defmethod gadget-selected-nodes ((graph <graph-control-pane>))
  (let ((nodes (MAKE-STRETCHY-VECTOR))
        (selection (gadget-selection graph))
        (index -1))
    (loop for node across (sheet-children (%layout-pane graph))
       do (unless (sheet-withdrawn? node)
	    (incf index)
	    (when (MEMBER? index selection)
	      (ADD! nodes node))))
    nodes))


#||
//--- Not correct if the same object appears twice in the graph
define sealed method compute-gadget-selection 
    (graph :: <graph-control-pane>, nodes) => (selection :: <sequence>)
  let new-selection :: <stretchy-object-vector> = make(<stretchy-vector>);
  let items = gadget-items(graph);
  for (node :: <graph-node-pane> in nodes)
    let index = position(items, node-object(node));
    when (index)
      add!(new-selection, index)
    end
  end;
  new-selection
end method compute-gadget-selection;
||#

(defmethod compute-gadget-selection ((graph <graph-control-pane>) nodes)
  (let ((new-selection (MAKE-STRETCHY-VECTOR))
        (items (gadget-items graph)))
    (map nil #'(lambda (node)
		 ;; FIXME -- TRAWL THROUGH FOR USES OF 'position' AND MAKE SURE
		 ;; THE ARGS ARE THE ORDERED CORRECTLY!
		 (let ((index (position (node-object node) items)))
		   (when index
		     (ADD! new-selection index))))
	 nodes)
    new-selection))


#||
define sealed method do-expand-node
    (graph :: <graph-control-pane>, node :: <graph-node-pane>) => ()
  graph.%visible-items := #f;
  delaying-layout (graph)
    let mapped? = sheet-mapped?(node);
    local method unwithdraw (node :: <graph-node-pane>)
	    // Only map in the first level
	    sheet-withdrawn?(node, do-repaint?: #f) := #f;
	    sheet-mapped?(node, do-repaint?: #f) := mapped?;
	  end method;
    do(unwithdraw, node-children(node));
  end
end method do-expand-node;
||#

(defmethod do-expand-node ((graph <graph-control-pane>) (node <graph-node-pane>))
  (setf (%visible-items graph) nil)
  (delaying-layout (graph)
    (let ((mapped? (sheet-mapped? node)))
      (labels ((unwithdraw (node)    ;;(node <graph-node-pane>))
                 ;; Only map in the first level
                 (setf (sheet-withdrawn? node :do-repaint? nil) nil)
		 (setf (sheet-mapped? node :do-repaint? nil) mapped?)))
	;; FIXME: SEQUENCE-DO ?
        (map nil #'unwithdraw (node-children node))))))


#||
define sealed method do-contract-node
    (graph :: <graph-control-pane>, node :: <graph-node-pane>) => ()
  graph.%visible-items := #f;
  delaying-layout (graph)
    local method withdraw (node :: <graph-node-pane>)
	    sheet-withdrawn?(node, do-repaint?: #f) := #t;
	    when (node-state(node) == #"expanded")
	      node-state(node) := #"contracted"
	    end;
	    // Withdraw all the way to the bottom
	    do(withdraw, node-children(node))
	  end method;
    do(withdraw, node-children(node))
  end
end method do-contract-node;
||#

(defmethod do-contract-node ((graph <graph-control-pane>) (node <graph-node-pane>))
  (setf (%visible-items graph) nil)
  (delaying-layout (graph)
    (labels ((withdraw (node)    ;;(node <graph-node-pane>))
               (setf (sheet-withdrawn? node :do-repaint? nil) t)
               (when (eql (node-state node) :expanded)
                 (setf (node-state node) :contracted))
               ;; Withdraw all the way to the bottom
               (map nil #'withdraw (node-children node))))
      (map nil #'withdraw (node-children node)))))


#||
define sealed method node-label
    (node :: <graph-node-pane>) => (label :: false-or(<string>))
  gadget-label(node)
end method node-label;
||#

(defmethod node-label ((node <graph-node-pane>))
  (gadget-label node))


#||
define sealed method node-label-setter
    (label :: false-or(<string>), node :: <graph-node-pane>) => (label :: false-or(<string>))
  gadget-label(node) := label;
  block (break)
    for (child in sheet-children(node))
      when (instance?(child, <tree-node-label-button>))
	gadget-label(child) := label;
	clear-box*(node, sheet-region(node));
	repaint-sheet(node, $everywhere);
	break()
      end
    end
  end;
  label
end method node-label-setter;
||#

(defmethod (setf node-label) ((label null) (node <graph-node-pane>))
  (setf (gadget-label node) label)
  (block break
    (loop for child across (sheet-children node)
       do (when (INSTANCE? child '<tree-node-label-button>)
	    (setf (gadget-label child) label)
	    (clear-box* node (sheet-region node))
	    (repaint-sheet node *everywhere*)
	    (return-from break))))
  label)

(defmethod (setf node-label) ((label string) (node <graph-node-pane>))
  (setf (gadget-label node) label)
  (block break
    (loop for child across (sheet-children node)
       do (when (INSTANCE? child '<tree-node-label-button>)
	    (setf (gadget-label child) label)
	    (clear-box* node (sheet-region node))
	    (repaint-sheet node *everywhere*)
	    (return-from break))))
  label)


#||
define sealed method node-icon
    (node :: <graph-node-pane>) => (icon :: false-or(<image>))
  node.%icon
end method node-icon;
||#

(defmethod node-icon ((node <graph-node-pane>))
  (%icon node))


#||
define sealed method node-icon-setter
    (icon :: false-or(<image>), node :: <graph-node-pane>) => (icon :: false-or(<image>))
  node.%icon := icon;
  block (break)
    for (child in sheet-children(node))
      when (instance?(child, <label>))
	gadget-label(child) := icon;
	clear-box*(node, sheet-region(node));
	repaint-sheet(node, $everywhere);
	break()
      end
    end
  end;
  icon
end method node-icon-setter;
||#

(defmethod (setf node-icon) ((icon null) (node <graph-node-pane>)))

(defmethod (setf node-icon) ((icon <image>) (node <graph-node-pane>))
  (setf (%icon node) icon)
  (block break
    (loop for child across (sheet-children node)
       do (when (INSTANCE? child '<label>)
	    (setf (gadget-label child) icon)
	    (clear-box* node (sheet-region node))
	    (repaint-sheet node *everywhere*)
	    (return-from break))))
  icon)



#||

/// Graph control edges

define open abstract primary class <graph-edge-pane> (<graph-edge>)
  sealed slot %x1 :: <integer>,
    required-init-keyword: x1:;
  sealed slot %y1 :: <integer>,
    required-init-keyword: y1:;
  sealed slot %x2 :: <integer>,
    required-init-keyword: x2:;
  sealed slot %y2 :: <integer>,
    required-init-keyword: y2:;
end class <graph-edge-pane>;
||#

(defclass <graph-edge-pane> (<graph-edge>)
  ((%x1 :type integer :initarg :x1 :accessor %x1 :initform (required-slot ":x1" "<graph-edge-pane>"))
   (%y1 :type integer :initarg :y1 :accessor %y1 :initform (required-slot ":y1" "<graph-edge-pane>"))
   (%x2 :type integer :initarg :x2 :accessor %x2 :initform (required-slot ":x2" "<graph-edge-pane>"))
   (%y2 :type integer :initarg :y2 :accessor %y2 :initform (required-slot ":y2" "<graph-edge-pane>"))))


#||
define sealed domain make (subclass(<graph-edge-pane>));
define sealed domain initialize (<graph-edge-pane>);


define sealed class <line-graph-edge> (<graph-edge-pane>)
end class <line-graph-edge>;
||#

(defclass <line-graph-edge>
    (<graph-edge-pane>)
  ())


#||
define sealed method draw-edge
    (edge :: <line-graph-edge>, medium :: <basic-medium>, region :: <region>) => ()
  let (x1, y1, x2, y2) = values(edge.%x1, edge.%y1, edge.%x2, edge.%y2);
  when (region-contains-position?(region, x1, y1)
	| region-contains-position?(region, x2, y2))
    draw-line(medium, x1, y1, x2, y2)
  end
end method draw-edge;
||#

(defmethod draw-edge ((edge <line-graph-edge>) (medium <basic-medium>) (region <region>))
  (multiple-value-bind (x1 y1 x2 y2)
      (values (%x1 edge) (%y1 edge) (%x2 edge) (%y2 edge))
    (when (or (region-contains-position? region x1 y1)
              (region-contains-position? region x2 y2))
      (draw-line medium x1 y1 x2 y2))))


#||
define sealed class <arrow-graph-edge> (<graph-edge-pane>)
  sealed slot %direction-flags :: <integer> = %to_head;
end class <arrow-graph-edge>;
||#

(defconstant %from-head #o01)
(defconstant %to-head   #o02)

(defclass <arrow-graph-edge> (<graph-edge-pane>)
  ((%direction-flags :type integer :initform %to-head :accessor %direction-flags)))


#||
define constant %from_head :: <integer> = #o01;
define constant %to_head   :: <integer> = #o02;

define method initialize
    (edge :: <arrow-graph-edge>,
     #key from-head? = #f, to-head? = #t)
  next-method();
  let bits = logior(if (from-head?) %from_head else 0 end,
		    if (to-head?)   %to_head   else 0 end);
  edge.%direction-flags := bits
end method initialize;
||#

(defmethod initialize-instance :after ((edge <arrow-graph-edge>)
				       &key (from-head? nil) (to-head? t) &allow-other-keys)
  (let ((bits (logior (if from-head? %from-head 0)
                      (if to-head?   %to-head   0))))
    (setf (%direction-flags edge) bits)))


#||
define sealed method draw-edge
    (edge :: <arrow-graph-edge>, medium :: <basic-medium>, region :: <region>) => ()
  let (x1, y1, x2, y2) = values(edge.%x1, edge.%y1, edge.%x2, edge.%y2);
  when (region-contains-position?(region, x1, y1)
	| region-contains-position?(region, x2, y2))
    draw-arrow(medium, x1, y1, x2, y2,
	       from-head?: logand(edge.%direction-flags, %from_head) ~= 0,
	       to-head?:   logand(edge.%direction-flags, %to_head)   ~= 0)
  end;
end method draw-edge;
||#

(defmethod draw-edge ((edge <arrow-graph-edge>) (medium <basic-medium>) (region <region>))
  (multiple-value-bind (x1 y1 x2 y2)
      (values (%x1 edge) (%y1 edge) (%x2 edge) (%y2 edge))
    (when (or (region-contains-position? region x1 y1)
              (region-contains-position? region x2 y2))
      (draw-arrow medium x1 y1 x2 y2
                  :from-head? (/= (logand (%direction-flags edge) %from-head) 0)
                  :to-head?   (/= (logand (%direction-flags edge) %to-head) 0)))))


#||
define method edge-attachment-points
    (parent :: <graph-node-pane>, child :: <graph-node-pane>, orientation)
 => (x1, y1, x2, y2)
  local method north (node) => (x, y)
	  let (left, top, right, bottom) = sheet-edges(node);
	  ignore(bottom);
	  values(floor/(right + left, 2), top - 1)
	end method,
        method south (node) => (x, y)
	  let (left, top, right, bottom) = sheet-edges(node);
	  ignore(top);
	  values(floor/(right + left, 2), bottom + 1)
	end method,
        method west (node) => (x, y)
	  let (left, top, right, bottom) = sheet-edges(node);
	  ignore(right);
	  values(left - 1, floor/(bottom + top, 2))
	end method,
        method east (node) => (x, y)
	  let (left, top, right, bottom) = sheet-edges(node);
	  ignore(left);
	  values(right + 1, floor/(bottom + top, 2))
	end method;
  select (orientation)
    #"vertical", #"down" =>
      let (x1, y1) = south(parent);
      let (x2, y2) = north(child);
      values(x1, y1, x2, y2);
    #"up" =>
      let (x1, y1) = north(parent);
      let (x2, y2) = south(child);
      values(x1, y1, x2, y2);
    #"horizontal", #"right" =>
      let (x1, y1) = east(parent);
      let (x2, y2) = west(child);
      values(x1, y1, x2, y2);
    #"left" =>
      let (x1, y1) = west(parent);
      let (x2, y2) = east(child);
      values(x1, y1, x2, y2);
  end
end method edge-attachment-points;
||#

(defmethod edge-attachment-points ((parent <graph-node-pane>) (child <graph-node-pane>) orientation)
  (labels ((north (node)
             (multiple-value-bind (left top right bottom)
                 (sheet-edges node)
               (declare (ignore bottom))
               (values (floor (+ right left) 2) (- top 1))))
           (south (node)
             (multiple-value-bind (left top right bottom)
                 (sheet-edges node)
               (declare (ignore top))
               (values (floor (+ right left) 2) (+ bottom 1))))
           (west (node)
             (multiple-value-bind (left top right bottom)
                 (sheet-edges node)
               (declare (ignore right))
               (values (- left 1) (floor (+ bottom top) 2))))
           (east (node)
             (multiple-value-bind (left top right bottom)
                 (sheet-edges node)
               (declare (ignore left))
               (values (+ right 1) (floor (+ bottom top) 2)))))
    (ecase orientation
      ((:vertical :down)
       (multiple-value-bind (x1 y1) (south parent)
         (multiple-value-bind (x2 y2) (north child)
           (values x1 y1 x2 y2))))
      (:up
       (multiple-value-bind (x1 y1) (north parent)
         (multiple-value-bind (x2 y2) (south child)
           (values x1 y1 x2 y2))))
      ((:horizontal :right)
       (multiple-value-bind (x1 y1) (east parent)
         (multiple-value-bind (x2 y2) (west child)
           (values x1 y1 x2 y2))))
      (:left
       (multiple-value-bind (x1 y1) (west parent)
         (multiple-value-bind (x2 y2) (east child)
           (values x1 y1 x2 y2)))))))



#||

/// Basic graph layout pane class

define abstract class <graph-control-layout>
    (<standard-repainting-mixin>,
     <layout-border-mixin>,
     <homegrown-control-layout-mixin>,
     <layout-pane>)
  sealed slot %edges :: <vector> = #[];
end class <graph-control-layout>;
||#

(defclass <graph-control-layout>   ;; FIXME: ABSTRACT
    (<standard-repainting-mixin>
     <layout-border-mixin>
     <homegrown-control-layout-mixin>
     <layout-pane>)
  ((%edges :type array :initform #() :accessor %edges)))

;;; FIXME -- put back occurrences of #[] with literal vector syntax #().

#||
define sealed domain make (subclass(<graph-control-layout>));
define sealed domain initialize (<graph-control-layout>);

define method do-compose-space
    (layout :: <graph-control-layout>, #key width, height)
 => (space-req :: <space-requirement>)
  ignore(width, height);
  let (width, height) = layout-graph-nodes(layout);
  layout-graph-edges(layout, reuse-edges?: #f);
  make(<space-requirement>,
       width: width, height: height)
end method do-compose-space;
||#

(defmethod do-compose-space ((layout <graph-control-layout>) &key width height)
  (declare (ignore width height))
  (multiple-value-bind (width height)
      (layout-graph-nodes layout)
    (layout-graph-edges layout :reuse-edges? nil)
    (make-space-requirement :width width :height height)))


#||
define method do-allocate-space
    (layout :: <graph-control-layout>, width :: <integer>, height :: <integer>) => ()
  ignore(width, height);
  layout-graph-nodes(layout);
  layout-graph-edges(layout, reuse-edges?: #t)
end method do-allocate-space;
||#

(defmethod do-allocate-space ((layout <graph-control-layout>) (width integer) (height integer))
  (declare (ignore width height))
  (layout-graph-nodes layout)
  (layout-graph-edges layout :reuse-edges? t))


#||
//---*** Still need to highlight the selected edges somehow...
//---*** Edges probably need to have labels, then we can highlight the labels
define method handle-repaint
    (layout :: <graph-control-layout>, medium :: <medium>, region :: <region>) => ()
  next-method();
  let graph = layout.%control-pane;
  when (tree-control-show-edges?(graph))
    with-drawing-options (medium, brush: $tree-control-gray)
      for (edge in layout.%edges)
	draw-edge(edge, medium, region)
      end
    end
  end
end method handle-repaint;
||#

;;---*** Still need to highlight the selected edges somehow...
;;---*** Edges probably need to have labels, then we can highlight the labels
(defmethod handle-repaint ((layout <graph-control-layout>) (medium <medium>) (region <region>))
  (duim-debug-message "[DUIM-EXTENDED;GRAPH-CONTROL-PANES HANDLE REPAINT] on <graph-control-layout> in region ~a"
		      region)
  (call-next-method)
  (let ((graph (%control-pane layout)))
    (when (tree-control-show-edges? graph)
      (with-drawing-options (medium :brush *tree-control-gray*)
	(map nil #'(lambda (edge)
		     (draw-edge edge medium region))
	     (%edges layout))))))


#||
// Useful macro
define macro with-node-breadth-and-depth-functions
  { with-node-breadth-and-depth-functions
       ((?breadthfun:name, ?depthfun:name,
	 ?breadth-start-setter:name, ?depth-start-setter:name,
	 ?depth-incrementer:name, ?depth-startfun:name) = ?orientation:expression)
      ?:body end }
    => { local method node-bottom (node)
		 node-y(node) + box-height(node)
	       end method,
	       method node-bottom-setter (b, node)
		 node-y(node) := b - box-height(node)
	       end method,
	       method node-right (node)
		  node-x(node) + box-width(node)
	       end method,
	       method node-right-setter (r, node)
		 node-x(node) := r - box-width(node)
	       end method;
	 let (?breadthfun :: <function>, ?depthfun :: <function>,
	      ?breadth-start-setter :: <function>, ?depth-start-setter :: <function>,
	      ?depth-startfun :: <function>)
	   = select (?orientation)
	       #"vertical", #"down", #"up" =>
		 values(box-width, box-height,
			node-x-setter,
			if (?orientation == #"up") node-bottom-setter else node-y-setter end,
			if (?orientation == #"up") node-bottom else node-y end);
	       #"horizontal", #"right", #"left" =>
		 values(box-height, box-width,
			node-y-setter,
			if (?orientation == #"left") node-right-setter else node-x-setter end,
			if (?orientation == #"left") node-right else node-x end);
	     end;
	 let ?depth-incrementer :: <function>
	   = select (?orientation)
	       #"vertical", #"down", #"horizontal", #"right" => \+;
	       #"up", #"left" => \-;
	     end;
	 ?body }
end macro with-node-breadth-and-depth-functions;
||#

(defmacro with-node-breadth-and-depth-functions (((breadthfun
                                                   depthfun
                                                   breadth-start-setter
                                                   depth-start-setter
                                                   depth-incrementor
                                                   depth-startfun) = orientation)
                                                 &body body)
  (declare (ignore =))
  `(labels ((node-bottom (node)
             (+ (node-y node) (box-height node)))
            (node-bottom-setter (b node)
             (setf (node-y node) (- b (box-height node))))
            (node-right (node)
             (+ (node-x node) (box-width node)))
            (node-right-setter (r node)
             (setf (node-x node) (- r (box-width node)))))
    (multiple-value-bind (,breadthfun ,depthfun
                          ,breadth-start-setter ,depth-start-setter
                          ,depth-startfun)
        (ecase ,orientation
          ((:vertical :down :up)
           (values #'box-width #'box-height
                   #'(setf node-x)   ;node-x-setter
                   (if (eq ,orientation :up) #'node-bottom-setter #'(setf node-y))   ;node-y-setter)
                   (if (eq ,orientation :up) #'node-bottom        #'node-y)))
          ((:horizontal :right :left)
           (values #'box-height #'box-width
                   #'(setf node-y)   ;node-y-setter
                   (if (eq ,orientation :left) #'node-right-setter #'(setf node-x))   ;node-x-setter)
                   (if (eq ,orientation :left) #'node-right        #'node-x))))
      (let ((,depth-incrementor (ecase ,orientation
                                  ((:vertical :down :horizontal :right) #'+)
                                  ((:up :left) #'-))))
        ,@body))))



#||

/// Event handling for edges

define method handle-event 
    (pane :: <graph-control-layout>, event :: <button-press-event>) => ()
  let graph = pane.%control-pane;
  when (gadget-enabled?(graph))
    let edge = find-edge-at-position(pane, event-x(event), event-y(event));
    if (edge)
      set-edge-selection(graph, edge, event);
      select (event-button(event))
	$left-button => #f;
	$middle-button =>
	  activate-gadget(graph);
	$right-button =>
	  let value = graph-edge-object(edge);
	  execute-popup-menu-callback
	    (graph, gadget-client(graph), gadget-id(graph), value,
	     x: event-x(event), y: event-y(event));
      end
    else
      next-method()
    end
  end
end method handle-event;
||#

(defmethod handle-event ((pane  <graph-control-layout>) (event <button-press-event>))
  (let ((graph (%control-pane pane)))
    (when (gadget-enabled? graph)
      (let ((edge (find-edge-at-position pane (event-x event) (event-y event))))
        (if edge
            (progn
              (set-edge-selection graph edge event)
	      (cond ((eql (event-button event) +left-button+)   nil)
		    ((eql (event-button event) +middle-button+) (activate-gadget graph))
		    ((eql (event-button event) +right-button+)  (let ((value (graph-edge-object edge)))
								  (execute-popup-menu-callback graph
											       (gadget-client graph)
											       (gadget-id graph) value
											       :x (event-x event)
											       :y (event-y event))))))
            ;; else
            (call-next-method))))))


#||
define method handle-event 
    (pane :: <graph-control-layout>, event :: <double-click-event>) => ()
  let graph = pane.%control-pane;
  when (gadget-enabled?(graph)
	& event-button(event) == $left-button)
    let edge = find-edge-at-position(pane, event-x(event), event-y(event));
    if (edge)
      set-edge-selection(graph, edge, event);
      activate-gadget(graph)
    else
      next-method()
    end
  end
end method handle-event;
||#

(defmethod handle-event ((pane <graph-control-layout>) (event <double-click-event>))
  (let ((graph (%control-pane pane)))
    (when (and (gadget-enabled? graph)
               (eql (event-button event) +left-button+))
      (let ((edge (find-edge-at-position pane (event-x event) (event-y event))))
        (if edge
            (progn
              (set-edge-selection graph edge event)
              (activate-gadget graph))
            ;; else
            (call-next-method))))))


#||
define sealed method find-edge-at-position
    (layout :: <graph-control-layout>, x :: <integer>, y :: <integer>)
 => (edge :: false-or(<graph-edge>))
  let graph = layout.%control-pane;
  when (graph-edge-generator(graph))
    block (return)
      for (edge :: <graph-edge-pane> in layout.%edges)
	when (position-close-to-line?(x, y,
				      edge.%x1, edge.%y1, edge.%x2, edge.%y2, thickness: 4))
	  return(edge)
	end
      end
    end
  end
end method find-edge-at-position;
||#

(defmethod find-edge-at-position ((layout <graph-control-layout>) (x integer) (y integer))
  (let ((graph (%control-pane layout)))
    (when (graph-edge-generator graph)
      (loop for edge in (%edges layout)
	 do (when (position-close-to-line? x y
					   (%x1 edge) (%y1 edge) (%x2 edge) (%y2 edge)
					   :thickness 4)
	      (return-from find-edge-at-position edge))))))


#||
define sealed method set-edge-selection
    (graph :: <graph-control-pane>, edge :: <graph-edge>, event) => ()
  let index     = position(gadget-items(graph), graph-edge-object(edge));
  let selection = gadget-selection(graph);
  select (gadget-selection-mode(graph))
    #"none"   => #f;
    #"single" =>
      gadget-selection(graph, do-callback?: #t) := vector(index);
    #"multiple" =>
      select (event-modifier-state(event))
	$shift-key =>
	  let min-index = reduce(min, index, selection);
	  let max-index = reduce(max, index, selection);
	  gadget-selection(graph, do-callback?: #t)
	    := range(from: min-index, to: max-index);
	$control-key =>
	  if (member?(index, selection))
	    gadget-selection(graph, do-callback?: #t) := remove(selection, index);
	  else
	    gadget-selection(graph, do-callback?: #t) := add(selection, index);
	  end;
	otherwise =>
	  gadget-selection(graph, do-callback?: #t) := vector(index);
      end;
  end
end method set-edge-selection;
||#

(defmethod set-edge-selection ((graph <graph-control-pane>) (edge <graph-edge>) event)
  ;; FIXME: INTRODUCE SEQUENCE-POSITION, make args same order as the Dylan call?
  (let ((index (position (graph-edge-object edge) (gadget-items graph)))
        (selection (gadget-selection graph)))
    (ecase (gadget-selection-mode graph)
      (:none nil)
      (:single
       (setf (gadget-selection graph :do-callback? t) (vector index)))
      (:multiple
       (cond ((eql (event-modifier-state event) +shift-key+)
	      (let ((min-index (reduce #'min selection :initial-value index))
		    (max-index (reduce #'max selection :initial-value index)))
		(setf (gadget-selection graph :do-callback? t)
		      (RANGE :from min-index :to max-index))))
	     ((eql (event-modifier-state event) +control-key+)
	      (if (MEMBER? index selection)
		  (setf (gadget-selection graph :do-callback? t) (remove selection index))
		  ;; else
		  (setf (gadget-selection graph :do-callback? t) (add selection index))))
	     (t
	      (setf (gadget-selection graph :do-callback? t) (vector index))))))))



#||

/// Tree graphs

define sealed class <tree-graph-pane> (<graph-control-pane>)
end class <tree-graph-pane>;
||#

(defclass <tree-graph-pane>
    (<graph-control-pane>)
  ())


#||
define sealed class <tree-graph-layout> (<graph-control-layout>)
end class <tree-graph-layout>;
||#

(defclass <tree-graph-layout>
    (<graph-control-layout>)
  ())


#||
define sealed method layout-class-for-graph
    (graph :: <tree-graph-pane>) => (class :: subclass(<graph-control-layout>))
  <tree-graph-layout>
end method layout-class-for-graph;
||#

(defmethod layout-class-for-graph ((graph <tree-graph-pane>))
  (find-class '<tree-graph-layout>))


#||
define method generate-graph-nodes
    (graph :: <tree-graph-pane>, roots :: <sequence>,
     #key key = identity, test = \==) => ()
  let generator :: <function> = tree-control-children-generator(graph);
  let predicate :: <function> = tree-control-children-predicate(graph);
  local method add-one (node, object, depth :: <integer>) => ()
	  let child-node = make-node(graph, object);
	  add-node(graph, node, child-node, setting-roots?: #t);
	  sheet-withdrawn?(child-node, do-repaint?: #f) := #f;
	  when (depth > 0 & predicate(object))
	    for (child in generator(object))
	      add-one(child-node, child, depth - 1)
	    end;
	    node-state(child-node) := #"expanded"
	  end
	end method;
  for (root in roots)
    add-one(graph, root, tree-control-initial-depth(graph))
  end
end method generate-graph-nodes;
||#

;; FIXME: Anywhere a type (or class) is given as 'sequence' it *SHOULD*
;; mean (or sequence <range>). Ofc for generics that means we need to
;; add extra methods to cope with the range case...

(defmethod generate-graph-nodes ((graph <tree-graph-pane>) (roots sequence)
                                 &key (key #'identity) (test #'eql))
  (declare (ignorable test key))
  (let ((generator (tree-control-children-generator graph))
        (predicate (tree-control-children-predicate graph)))
    (labels ((add-one (node object depth)
               (let ((child-node (make-node graph object)))
                 (add-node graph node child-node :setting-roots? t)
                 (setf (sheet-withdrawn? child-node :do-repaint? nil) nil)
                 (when (and (> depth 0) (funcall predicate object))
		   (map nil #'(lambda (child)
				(add-one child-node child (- depth 1)))
			(funcall generator object))
                   (setf (node-state child-node) :expanded)))))
      (map nil #'(lambda (root)
		   (add-one graph root (tree-control-initial-depth graph)))
	   roots))))


#||
//--- If anyone wanted, it would be easy to add an option which said which way
//--- the kids ran -- e.g. left-to-right or right-to-left in the case of vertical
//--- graphs.  Just change the one remaining '+' to call a new breadth incrementer.
define method layout-graph-nodes
    (layout :: <tree-graph-layout>)
 => (width :: <integer>, height :: <integer>)
  let graph :: <graph-control-pane> = layout.%control-pane;
  let root-nodes       = tree-control-root-nodes(graph);
  let orientation      = graph-orientation(graph);
  let inter-generation = graph-inter-generation-spacing(graph);
  let intra-generation = graph-intra-generation-spacing(graph);
  let most-negative-depth :: <integer> = 0;
  let start-breadth :: <integer> = 0;
  let width  :: <integer> = 0;
  let height :: <integer> = 0;
  //--- There might be a more elegant way to do this!
  for (node in sheet-children(layout))
    unless (sheet-withdrawn?(node))
      let space-req = compose-space(node);
      let (w, w-, w+, h, h-, h+) = space-requirement-components(node, space-req);
      ignore(w-, w+, h-, h+);
      set-sheet-size(node, w, h)
    end
  end;
  with-node-breadth-and-depth-functions
     ((breadthfun, depthfun,
       breadth-start-setter, depth-start-setter,
       depth-incrementer, depth-startfun) = orientation)
    local method layout-graph
	      (root, start-depth :: <integer>, start-breadth :: <integer>, tallest-sibling)
	    let children = node-children(root);
	    let breadth :: <integer> = start-breadth;
	    let root-breadth   :: <integer> = breadthfun(root);
	    let breadth-margin :: <integer> = floor/(intra-generation, 2);
	    let tallest-child  :: <integer> = 0;
	    let n-children     :: <integer> = 0;
	    for (child in children)
	      unless (sheet-withdrawn?(child))
		max!(tallest-child, depthfun(child));
		inc!(n-children);
	      end
	    end;
	    for (child in children)
	      unless (sheet-withdrawn?(child))
		inc!(breadth, breadth-margin);
		let child-breadth :: <integer>
		  = layout-graph(child,
				 depth-incrementer(start-depth, tallest-sibling + inter-generation),
				 breadth,
				 tallest-child);
		inc!(breadth, child-breadth);
		inc!(breadth, breadth-margin)
	      end
	    end;
	    let total-child-breadth :: <integer>
	      = breadth - start-breadth;
	    let my-breadth :: <integer>
	      = start-breadth + floor/(max(0, total-child-breadth - root-breadth), 2);
	    depth-start-setter(start-depth, root);
	    breadth-start-setter(my-breadth, root);
	    let (left, top, right, bottom) = sheet-edges(root);
	    ignore(left, top);
	    max!(width,  right);
	    max!(height, bottom);
	    when (~zero?(n-children))
	      min!(most-negative-depth,
		   depth-incrementer(start-depth, tallest-sibling))
	    end;
	    // Returns the breadth of the graph as a result
	    max(total-child-breadth, root-breadth)
	  end method;
    for (root-node in root-nodes)
      inc!(start-breadth,
           layout-graph(root-node, 0, start-breadth, depthfun(root-node)))
    end;
    // For up and right, we've laid out into negative coordinates.  Correct this.
    when (negative?(most-negative-depth))
      do-sheet-children(method (node)
			  depth-start-setter(depth-startfun(node) - most-negative-depth, node)
			end, layout)
    end
  end;
  values(width, height)
end method layout-graph-nodes;
||#

(defmethod layout-graph-nodes ((layout <tree-graph-layout>))
  (let* ((graph (%control-pane layout))
         (root-nodes (tree-control-root-nodes graph))
         (orientation (graph-orientation graph))
         (inter-generation (graph-inter-generation-spacing graph))
         (intra-generation (graph-intra-generation-spacing graph))
         (most-negative-depth 0)
	 (start-breadth 0)
         (width 0)
         (height 0))
    ;;--- There might be a more elegant way to do this!
    (loop for node across (sheet-children layout)
       do (unless (sheet-withdrawn? node)
	    (let ((space-req (compose-space node)))
	      (multiple-value-bind (w w- w+ h h- h+)
		  (space-requirement-components node space-req)
		(declare (ignore w- w+ h- h+))
		(set-sheet-size node w h)))))
    (with-node-breadth-and-depth-functions
        ((breadthfun depthfun
          breadth-start-setter depth-start-setter
          depth-incrementor depth-startfun) = orientation)
      (labels ((layout-graph (root start-depth start-breadth tallest-sibling)
                 (let* ((children (node-children root))
                        (breadth start-breadth)
                        (root-breadth (funcall breadthfun root))
                        (breadth-margin (floor intra-generation 2))
                        (tallest-child 0)
                        (n-children 0))
		   (loop for child across children
		      do (unless (sheet-withdrawn? child)
			   (setf tallest-child (max tallest-child (funcall depthfun child)))
			   (incf n-children)))
		   (loop for child across children
		      do (unless (sheet-withdrawn? child)
			   (incf breadth breadth-margin)
			   (let ((child-breadth (layout-graph child
							      (funcall depth-incrementor start-depth (+ tallest-sibling inter-generation))
							      breadth
							      tallest-child)))
			     (incf breadth child-breadth)
			     (incf breadth breadth-margin))))
                   (let* ((total-child-breadth (- breadth start-breadth))
                          (my-breadth (+ start-breadth (floor (max 0 (- total-child-breadth root-breadth)) 2))))
                     (funcall depth-start-setter start-depth root)
                     (funcall breadth-start-setter my-breadth root)
                     (multiple-value-bind (left top right bottom)
                         (sheet-edges root)
                       (declare (ignore left top))
                       (setf width (max width right))
                       (setf height (max height bottom))
                       (when (not (zerop n-children))
                         (setf most-negative-depth
                               (min most-negative-depth
                                    (funcall depth-incrementor start-depth tallest-sibling))))
                       ;; Returns the breadth of the graph as a result
                       (max total-child-breadth root-breadth))))))
	(map nil #'(lambda (root-node)
		     (incf start-breadth
			   (layout-graph root-node 0 start-breadth (funcall depthfun root-node))))
	     root-nodes)
        ;; For up and right, we've laid out into negative coordinates.  Correct this.
        (when (negative? most-negative-depth)
          (do-sheet-children #'(lambda (node)
                                 (funcall depth-start-setter (- (funcall depth-startfun node) most-negative-depth) node))
            layout))))
    (values width height)))


#||
define method layout-graph-edges
    (layout :: <tree-graph-layout>, #key reuse-edges? = #f) => ()
  let graph :: <graph-control-pane> = layout.%control-pane;
  let orientation = graph-orientation(graph);
  if (reuse-edges?)
    let edges :: <stretchy-object-vector> = layout.%edges;
    for (edge :: <graph-edge-pane> in edges)
      let from-node = graph-edge-from-node(edge);
      let to-node   = graph-edge-to-node(edge);
      let (x1, y1, x2, y2)
	= edge-attachment-points(from-node, to-node, orientation);
      edge.%x1 := x1;
      edge.%y1 := y1;
      edge.%x2 := x2;
      edge.%y2 := y2
    end
  else
    let root-nodes    = tree-control-root-nodes(graph);
    let edge-class    = graph-edge-class(graph) | <line-graph-edge>;
    let edge-initargs = graph-edge-initargs(graph);
    let edge-function = graph-edge-generator(graph);
    let edges :: <stretchy-object-vector> = make(<stretchy-vector>);
    local method make-edges (from-node :: <graph-node>)
	    // By definition, expanded nodes aren't withdrawn, so we
	    // do the more general test
	    when (node-state(from-node) == #"expanded")
	      for (to-node :: <graph-node> in node-children(from-node))
		when (~sheet-withdrawn?(to-node))
		  unless (empty?(node-children(to-node)))
		    make-edges(to-node)
		  end;
		  let (x1, y1, x2, y2)
		    = edge-attachment-points(from-node, to-node, orientation);
		  let object
		    = edge-function & edge-function(node-object(from-node), node-object(to-node));
		  let edge
		    = apply(make, edge-class,
			    x1: x1, y1: y1, x2: x2, y2: y2, 
			    from-node: from-node, to-node: to-node,
			    object: object,
			    edge-initargs);
		  add!(edges, edge)
		end
	      end
	    end
	  end method;
    for (root-node in root-nodes)
      make-edges(root-node)
    end;
    layout.%edges := edges
  end
end method layout-graph-edges;
||#

(defmethod layout-graph-edges ((layout <tree-graph-layout>) &key (reuse-edges? nil))
  (let* ((graph (%control-pane layout))
         (orientation (graph-orientation graph)))
    (if reuse-edges?
        (let ((edges (%edges layout)))   ;; XXX: Should be stretchy-vector...
	  (loop for edge across edges
	     do (let ((from-node (graph-edge-from-node edge))
		      (to-node   (graph-edge-to-node edge)))
		  (multiple-value-bind (x1 y1 x2 y2)
		      (edge-attachment-points from-node to-node orientation)
		    (setf (%x1 edge) x1)
		    (setf (%y1 edge) y1)
		    (setf (%x2 edge) x2)
		    (setf (%y2 edge) y2)))))
        ;; else
        (let ((root-nodes (tree-control-root-nodes graph))
              (edge-class (or (graph-edge-class graph) (find-class '<line-graph-edge>)))
              (edge-initargs (graph-edge-initargs graph))   ;; XXX: Should be a list...
              (edge-function (graph-edge-generator graph))
              (edges (MAKE-STRETCHY-VECTOR)))
          (labels ((make-edges (from-node)    ;;((from-node <graph-node>))
                     ;; By definition, expanded nodes aren't withdrawn, so we
                     ;; do the more general test
                     (when (eql (node-state from-node) :expanded)
                       (map nil #'(lambda (to-node)
				    (when (not (sheet-withdrawn? to-node))
				      (unless (empty? (node-children to-node))
					(make-edges to-node))
				      (multiple-value-bind (x1 y1 x2 y2)
					  (edge-attachment-points from-node to-node orientation)
					(let* ((object (and edge-function
							    (funcall edge-function (node-object from-node) (node-object to-node))))
					       ;; FIXME: MAKE-PANE?
					       (edge (apply #'make-instance edge-class
							    :x1 x1 :y1 y1 :x2 x2 :y2 y2
							    :from-node from-node :to-node to-node
							    :object object
							    ;; FIXME: edge-initargs need to be a LIST...
							    edge-initargs)))
					  (ADD! edges edge)))))
			    (node-children from-node)))))
	    (map nil #'(lambda (root-node)
			 (make-edges root-node))
		 root-nodes)
            (setf (%edges layout) edges))))))



#||

/// Directed graphs, both acyclic and cyclic

define sealed class <DAG-graph-pane> (<graph-control-pane>)
  sealed slot %node-table :: <object-table> = make(<table>),
    init-keyword: node-table:;
  sealed slot %n-generations :: <integer> = 0;
end class <DAG-graph-pane>;
||#

(defclass <DAG-graph-pane> (<graph-control-pane>)
  ((%node-table :type hash-table :initform (make-hash-table) :initarg :node-table :accessor %node-table)
   (%n-generations :type integer :initform 0 :accessor %n-generations)))


#||
define sealed class <DAG-graph-layout> (<graph-control-layout>)
end class <DAG-graph-layout>;
||#

(defclass <DAG-graph-layout>
    (<graph-control-layout>)
  ())


#||
define sealed method layout-class-for-graph
    (graph :: <DAG-graph-pane>) => (class :: subclass(<graph-control-layout>))
  <DAG-graph-layout>
end method layout-class-for-graph;
||#

(defmethod layout-class-for-graph ((graph <DAG-graph-pane>))
  (find-class '<DAG-graph-layout>))


#||
// Note that we don't use 'add-node' directly to do any of this
// because it's too simple-minded to do the sharing we need here
define method generate-graph-nodes
    (graph :: <DAG-graph-pane>, roots :: <sequence>,
     #key key = identity, test = \==) => ()
  let layout = graph.%layout-pane;
  let generator :: <function> = tree-control-children-generator(graph);
  let predicate :: <function> = tree-control-children-predicate(graph);
  let node-table :: <object-table> = graph.%node-table;
  let root-nodes :: <stretchy-object-vector> = make(<stretchy-vector>);
  let max-depth = tree-control-initial-depth(graph);
  graph.%n-generations := 0;
  local method do-children (function :: <function>, node)
	  do(function, generator(node))
	end method,
        method create-node (parent-object, parent-node, child-object, nothing)
          ignore(nothing);
	  let child-node = make-node(graph, child-object);
	  add!(gadget-items(graph), child-object);
	  add-child(layout, child-node);
	  when (parent-node)
	    node-state(parent-node) := #"expanded"
	  end;
	  // This guarantees that the next phase will have at least one
	  // node from which to start.  Otherwise the entire graph gets
	  // lost.  If the first node isn't really a root, it will be
	  // deleted from the set of roots when the cycle is detected.
	  when (empty?(root-nodes))
	    add!(root-nodes, child-node)
	  end;
	  initialize-node(parent-object, parent-node, child-object, child-node)
        end method,
        method initialize-node (parent-object, parent-node, child-object, child-node)
	  ignore(parent-object, child-object);
	  let old-generation = node-generation(child-node);
	  // Set the generation of this node to 1 greater than the parent,
	  // and keep track of the highest generation encountered.
	  max!(graph.%n-generations,
	       max!(node-generation(child-node),
		    if (parent-node) node-generation(parent-node) + 1 else 0 end));
	  // If the child node got its generation adjusted, then we must
	  // adjust the generation number of already-processed children,
	  // and their children, etc.
	  unless (node-generation(child-node) == old-generation)
	    increment-generation(child-node)
	  end;
	  // Preserve the ordering of the nodes
	  when (parent-node)
	    unless (member?(parent-node, node-parents(child-node)))
	      add!(node-parents(child-node), parent-node)
	    end;
	    unless (member?(child-node, node-children(parent-node)))
	      add!(node-children(parent-node), child-node)
	    end
	  end;
	  child-node
	end method,
        method increment-generation (node)
	  let new-generation = node-generation(node) + 1;
	  for (child in node-children(node))
	    // Remember which generation the child belonged to.
	    let old-generation = node-generation(child);
	    max!(graph.%n-generations,
		 max!(node-generation(child), new-generation));
	    // If it has changed, fix up the next generation recursively.
	    unless (node-generation(child) >= old-generation)
	      increment-generation(child)
	    end
	  end
	end method;
  traverse-graph(roots, do-children, node-table, key,
		 create-node, initialize-node, max-depth: max-depth);
  do(method (node)
       when (empty?(node-parents(node)))
	 add-new!(root-nodes, node)
       end
     end method,
     node-table);
  tree-control-root-nodes(graph) := root-nodes
end method generate-graph-nodes;
||#

;; Note that we don't use 'add-node' directly to do any of this
;; because it's too simple-minded to do the sharing we need here
(defmethod generate-graph-nodes ((graph <DAG-graph-pane>) (roots sequence)
                                 &key (key #'identity) (test #'eql))
  (declare (ignorable test))
  (let* ((layout (%layout-pane graph))
         (generator (tree-control-children-generator graph))
         (predicate (tree-control-children-predicate graph))
         (node-table (%node-table graph))
         (root-nodes (MAKE-STRETCHY-VECTOR))
         (max-depth (tree-control-initial-depth graph)))
    (declare (ignorable predicate))
    (setf (%n-generations graph) 0)
    (labels ((do-children (function node)
               (map nil function (funcall generator node)))
             (create-node (parent-object parent-node child-object nothing)
               (declare (ignore nothing))
               (let ((child-node (make-node graph child-object)))
                 (ADD! (gadget-items graph) child-object)
                 (add-child layout child-node)
                 (when parent-node
                   (setf (node-state parent-node) :expanded))
                 ;; This guarantees that the next phase will have at least one
                 ;; node from which to start.  Otherwise the entire graph gets
                 ;; lost.  If the first node isn't really a root, it will be
                 ;; deleted from the set of roots when the cycle is detected.
                 (when (empty? root-nodes)
                   (ADD! root-nodes child-node))
                 (initialize-node parent-object parent-node child-object child-node)))
             (initialize-node (parent-object parent-node child-object child-node)
               (declare (ignore parent-object child-object))
               (let ((old-generation (node-generation child-node)))
                 ;; Set the generation of this node to 1 greater than the parent,
                 ;; and keep track of the highest generation encountered.
                 (setf (%n-generations graph)
                       (max (%n-generations graph)
                            (setf (node-generation child-node)
                                  (max (node-generation child-node)
                                       (if parent-node (+ (node-generation parent-node) 1) 0)))))
                 ;; If the child node got its generation adjusted, then we must
                 ;; adjust the generation number of already-processed children,
                 ;; and their children, etc.
                 (unless (= (node-generation child-node) old-generation)
                   (increment-generation child-node))
                 ;; Preserve the ordering of the nodes
                 (when parent-node
                   (unless (MEMBER? parent-node (node-parents child-node))
                     (ADD! (node-parents child-node) parent-node))
                   (unless (MEMBER? child-node (node-children parent-node))
                     (ADD! (node-children parent-node) child-node)))
                 child-node))
             (increment-generation (node)
               (let ((new-generation (+ (node-generation node) 1)))
		 (loop for child across (node-children node)
		    do (let ((old-generation (node-generation child)))
			 ;; Remember which generation the child belonged to.
			 (setf (%n-generations graph)
			       (max (%n-generations graph)
				    (setf (node-generation child)
					  (max (node-generation child) new-generation))))
			 ;; If it has schanged, fix up the next generation recursively.
			 (unless (>= (node-generation child) old-generation)
			   (increment-generation child)))))))
      (traverse-graph roots #'do-children node-table key
                      #'create-node #'initialize-node :max-depth max-depth)
      (loop for node being each hash-value of node-table
	 when (empty? (node-parents node))
	 do (ADD-NEW! root-nodes node))
      (setf (tree-control-root-nodes graph) root-nodes))))


#||
define sealed method do-add-nodes
    (graph :: <DAG-graph-pane>, parent, nodes :: <sequence>, #key after) => ()
  let layout = graph.%layout-pane;
  let node-table :: <object-table> = graph.%node-table;
  let root-nodes :: <stretchy-object-vector> = tree-control-root-nodes(graph);
  let selected-nodes = gadget-selected-nodes(graph);
  graph.%visible-items := #f;
  gadget-selection(graph) := #[];
  local method do-children (function :: <function>, node)
	  do(function, node-children(node))
	end method,
        method record-node (parent, hash, child, depth)
	  ignore(parent, hash, depth);
	  child
	end method;
  // Record all the existing nodes
  traverse-graph(root-nodes, do-children, node-table, node-object,
		 record-node, ignore);
  // These two local methods are copied from 'generate-graph-nodes' above
  local method initialize-node (parent-node, child-node)
	  let old-generation = node-generation(child-node);
	  max!(graph.%n-generations,
	       max!(node-generation(child-node),
		    if (parent-node) node-generation(parent-node) + 1 else 0 end));
	  unless (node-generation(child-node) == old-generation)
	    increment-generation(child-node)
	  end;
	  // Preserve the ordering of the nodes
	  when (parent-node)
	    unless (member?(parent-node, node-parents(child-node)))
	      add!(node-parents(child-node), parent-node)
	    end;
	    unless (member?(child-node, node-children(parent-node)))
	      add!(node-children(parent-node), child-node)
	    end
	  end
	end method,
	method increment-generation (node)
	  let new-generation = node-generation(node) + 1;
	  for (child in node-children(node))
	    // Remember which generation the child belonged to.
	    let old-generation = node-generation(child);
	    max!(graph.%n-generations,
		 max!(node-generation(child), new-generation));
	    // If it has changed, fix up the next generation recursively.
	    unless (node-generation(child) >= old-generation)
	      increment-generation(child)
	    end
	  end
	end method;
  for (new-child in nodes)
    // The following is based on 'create-node' in 'generate-graph-nodes' above
    let object    = node-object(new-child);
    let old-child = gethash(node-table, object);
    unless (old-child)
      add!(gadget-items(graph), object);
      add-child(layout, new-child);
      gethash(node-table, object) := new-child
    end;
    when (parent)
      node-state(parent) := #"expanded"
    end;
    initialize-node(parent, old-child | new-child);
  end;
  gadget-selection(graph) := compute-gadget-selection(graph, selected-nodes)
end method do-add-nodes;
||#

(defmethod do-add-nodes ((graph <DAG-graph-pane>) parent (nodes sequence) &key after)
  (declare (ignorable after))
  (let* ((layout (%layout-pane graph))
         (node-table (%node-table graph))
         (root-nodes (tree-control-root-nodes graph))
         (selected-nodes (gadget-selected-nodes graph)))
    (setf (%visible-items graph) nil)
    (setf (gadget-selection graph) #())
    (labels ((do-children (function node)
               (map nil function (node-children node)))
             (record-node (parent hash child depth)
               (declare (ignore parent hash depth))
               child))
      ;; Record all the existing nodes
      (traverse-graph root-nodes #'do-children node-table #'node-object
                      #'record-node (constantly nil))
      ;; These two local methods are copied from 'generate-graph-nodes' above
      (labels ((initialize-node (parent-node child-node)
                 (let ((old-generation (node-generation child-node)))
                   (setf (%n-generations graph)
                         (max (%n-generations graph)
                              (setf (node-generation child-node)
                                    (max (node-generation child-node)
                                         (if parent-node (+ (node-generation parent-node) 1) 0)))))
                   (unless (eql (node-generation child-node) old-generation)
                     (increment-generation child-node))
                   ;; Preserve the ordering of the nodes
                   (when parent-node
                     (unless (MEMBER? parent-node (node-parents child-node))
                       (ADD! (node-parents child-node) parent-node))
                     (unless (MEMBER? child-node (node-children parent-node))
                       (ADD! (node-children parent-node) child-node)))))
               (increment-generation (node)
                 (let ((new-generation (1+ (node-generation node))))
		   (loop for child across (node-children node)
		      do (let ((old-generation (node-generation child)))  ;; Remember which generation the child belonged to.
			   (setf (%n-generations graph)
				 (max (%n-generations graph)
				      (setf (node-generation child)
					    (max (node-generation child) new-generation))))
			   ;; If it has changed, fix up the next generation recursively.
			   (unless (>= (node-generation child) old-generation)
			     (increment-generation child)))))))
	(map nil #'(lambda (new-child)
		     ;; The following is based on 'create-node' in 'generate-graph-nodes' above
		     (let* ((object (node-object new-child))
			    (old-child (gethash object node-table)))
		       (unless old-child
			 (ADD! (gadget-items graph) object)
			 (add-child layout new-child)
			 (setf (gethash object node-table) new-child))
		       (when parent
			 (setf (node-state parent) :expanded))
		       (initialize-node parent (or old-child new-child))))
	     nodes)
        (setf (gadget-selection graph) (compute-gadget-selection graph selected-nodes))))))


#||
define sealed method do-contract-node
    (graph :: <DAG-graph-pane>, node :: <graph-node-pane>) => ()
  graph.%visible-items := #f;
  delaying-layout (graph)
    local method withdraw (node :: <graph-node-pane>)
	    // Need to be careful not to withdraw a child node if there are
	    // any parents that still points to it...
	    when (every?(method (n) node-state(n) == #"contracted" end, node-parents(node)))
	      sheet-withdrawn?(node, do-repaint?: #f) := #t;
	      when (node-state(node) == #"expanded")
		node-state(node) := #"contracted"
	      end;
	      // Withdraw all the way to the bottom
	      do(withdraw, node-children(node))
	    end
	  end method;
    do(withdraw, node-children(node))
  end
end method do-contract-node;
||#

(defmethod do-contract-node ((graph <DAG-graph-pane>) (node <graph-node-pane>))
  (setf (%visible-items graph) nil)
  (delaying-layout (graph)
    (labels ((withdraw (node)    ;;(node <graph-node-pane>))
               ;; Need to be careful not to withdraw a child node if there are
               ;; any parents that still point to it...
               (when (EVERY? #'(lambda (n) (eql (node-state n) :contracted)) (node-parents node))
                 (setf (sheet-withdrawn? node :do-repaint? nil) t)
                 (when (eql (node-state node) :expanded)
                   (setf (node-state node) :contracted))
                 ;; Withdraw all the way to the bottom
                 (map nil #'withdraw (node-children node)))))
      (map nil #'withdraw (node-children node)))))


#||
define sealed class <generation-descriptor> (<object>)
  sealed slot generation-generation  :: <integer> = 0,	// generation number
    init-keyword: generation:;
  sealed slot generation-breadth     :: <integer> = 0;	// sum of breadth of all nodes in this generation
  sealed slot generation-depth       :: <integer> = 0;	// maximum depth of any node in this generation
  sealed slot generation-start-depth :: <integer> = 0,	// starting depth position for this generation
    init-keyword: start-depth:;
  sealed slot generation-size        :: <integer> = 0;	// number of nodes in this generation
  // "Temporaries" used during placement
  sealed slot generation-breadth-so-far :: <integer> = 0, // running placement on the breadth axis
    init-keyword: start-breadth:;
  sealed slot generation-inner-breadth-separation :: <integer> = 0;
  sealed slot generation-edge-breadth-separation  :: <integer> = 0;
  sealed slot generation-touched? :: <boolean> = #f;	// if #t, use inner breadth separation
end class <generation-descriptor>;
||#

(defclass <generation-descriptor> ()
  ((generation-generation               ;; generation number
    :type integer :initform 0 :initarg :generation :accessor generation-generation)
   (generation-breadth                  ;; sum of breadth of all nodes in this generation
    :type integer :initform 0 :accessor generation-breadth)
   (generation-depth                    ;; maximum depth of any node in this generation
    :type integer :initform 0 :accessor generation-depth)
   (generation-start-depth              ;; starting depth position for this generation
    :type integer :initform 0 :initarg :start-depth :accessor generation-start-depth)
   (generation-size                     ;; number of nodes in this generation
    :type integer :initform 0 :accessor generation-size)
   ;; "Temporaries" used during placement
   (generation-breadth-so-far           ;; running placement on the breadth axis
    :type integer :initform 0 :initarg :start-breadth :accessor generation-breadth-so-far)
   (generation-inner-breadth-separation
    :type integer :initform 0 :accessor generation-inner-breadth-separation)
   (generation-edge-breadth-separation
    :type integer :initform 0 :accessor generation-edge-breadth-separation)
   (generation-touched?                 ;; if T, use inner breadth separation
    :type boolean :initform nil :accessor generation-touched?)))


#||
define method layout-graph-nodes
    (layout :: <DAG-graph-layout>)
 => (width :: <integer>, height :: <integer>)
  let graph :: <graph-control-pane> = layout.%control-pane;
  let node-table :: <object-table> = graph.%node-table;
  let root-nodes :: <stretchy-object-vector> = tree-control-root-nodes(graph);
  let n-generations    = graph.%n-generations;
  let orientation      = graph-orientation(graph);
  let inter-generation = graph-inter-generation-spacing(graph);
  let intra-generation = graph-intra-generation-spacing(graph);
  let center-nodes?    = graph-center-nodes?(graph);
  let start-x :: <integer> = 0;
  let start-y :: <integer> = 0;
  let width   :: <integer> = 0;
  let height  :: <integer> = 0;
  //--- There might be a more elegant way to do this!
  for (node in sheet-children(layout))
    unless (sheet-withdrawn?(node))
      let space-req = compose-space(node);
      let (w, w-, w+, h, h-, h+) = space-requirement-components(node, space-req);
      ignore(w-, w+, h-, h+);
      set-sheet-size(node, w, h)
    end
  end;
  unless (empty?(root-nodes))
    local method do-children (function :: <function>, node)
	    do(function, node-children(node))
	  end method,
          method traverse (new-node-function :: <function>, old-node-function :: <function>)
	    traverse-graph(root-nodes, do-children, node-table, identity,
			   new-node-function, old-node-function)
	  end method,
	  method set-sheet-position-yx (sheet, y, x)
	    set-sheet-position(sheet, x, y)
	  end method;
    let (breadthfun :: <function>, depthfun :: <function>, position-setter :: <function>,
	 start-breadth :: <integer>, start-depth :: <integer>)
      = select (orientation)
	  #"vertical", #"down", #"up" =>
	    values(box-width, box-height, set-sheet-position,    start-x, start-y);
	  #"horizontal", #"right", #"left" =>
	    values(box-height, box-width, set-sheet-position-yx, start-y, start-x);
	end;
    let generation-descriptors :: <simple-object-vector>
      = make(<vector>, size: n-generations + 1);
    let max-gen-breadth :: <integer> = 0;
    let broadest-gen-descr = #f;
    for (generation :: <integer> from 0 to n-generations)
      generation-descriptors[generation] := make(<generation-descriptor>,
						 generation: generation,
						 start-breadth: start-breadth)
    end;
    when (orientation == #"up" | orientation == #"left")
      generation-descriptors := reverse!(generation-descriptors)
    end;
    // Determine the breadth and depth of each generation
    local method collect-node-size (parent, hash, child, depth)
	    ignore(parent, hash, depth);
	    unless (sheet-withdrawn?(child))
	      let descr = find-value(generation-descriptors, 
				     method (gd)
				       generation-generation(gd) = node-generation(child)
				     end);
	      inc!(generation-size(descr));
	      inc!(generation-breadth(descr), breadthfun(child));
	      max!(generation-depth(descr),   depthfun(child))
	    end
	  end method;
    traverse(collect-node-size, ignore);
    // Determine max-breadth and starting-depth
    let depth-so-far :: <integer> = start-depth;
    for (descr in generation-descriptors)
      let gen-breadth = generation-breadth(descr);
      when (~broadest-gen-descr | (gen-breadth > max-gen-breadth))
	max-gen-breadth := gen-breadth;
	broadest-gen-descr := descr
      end;
      generation-start-depth(descr) := depth-so-far;
      inc!(depth-so-far, inter-generation + generation-depth(descr))
    end;
    // Determine breadth-spacing
    inc!(max-gen-breadth, intra-generation * generation-size(broadest-gen-descr));
    for (descr in generation-descriptors)
      let excess = floor/(max-gen-breadth - generation-breadth(descr),
			  max(generation-size(descr), 1));
      generation-inner-breadth-separation(descr) := excess;
      generation-edge-breadth-separation(descr) := floor/(excess, 2)
    end;
    local method place-node (parent, hash, child, depth)
	    ignore(parent, hash, depth);
	    unless (sheet-withdrawn?(child))	    
	      let descr = find-value(generation-descriptors,
				     method (gd)
				       generation-generation(gd) = node-generation(child)
				     end);
	      inc!(generation-breadth-so-far(descr),
		   if (generation-touched?(descr))
		     generation-inner-breadth-separation(descr)
		   else
		     generation-touched?(descr) := #t;
		     generation-edge-breadth-separation(descr)
		   end);
	      position-setter(child,
			      generation-breadth-so-far(descr),
			      if (center-nodes?)
				generation-start-depth(descr)
				+ floor/(generation-depth(descr) - depthfun(child), 2)
			      else
				generation-start-depth(descr)
			      end);
	      let (left, top, right, bottom) = sheet-edges(child);
	      ignore(left, top);
	      max!(width,  right);
	      max!(height, bottom);
	      inc!(generation-breadth-so-far(descr), breadthfun(child))
	    end
	  end method;
    traverse(place-node, ignore)
  end;
  values(width, height)
end method layout-graph-nodes;
||#

(defmethod layout-graph-nodes ((layout <DAG-graph-layout>))
  (let* ((graph (%control-pane layout))
         (node-table (%node-table graph))
         (root-nodes (tree-control-root-nodes graph))  ;; XXX: stretchy
         (n-generations (%n-generations graph))
         (orientation (graph-orientation graph))
         (inter-generation (graph-inter-generation-spacing graph))
         (intra-generation (graph-intra-generation-spacing graph))
         (center-nodes? (graph-center-nodes? graph))
         (start-x 0)
         (start-y 0)
         (width   0)
         (height  0))
    ;;--- There might be a more elegant way to do this!
    (loop for node across (sheet-children layout)
       do (unless (sheet-withdrawn? node)
	    (let ((space-req (compose-space node)))
	      (multiple-value-bind (w w- w+ h h- h+)
		  (space-requirement-components node space-req)
		(declare (ignore w- w+ h- h+))
		(set-sheet-size node w h)))))
    (unless (empty? root-nodes)
      (labels ((do-children (function node)
                 (map nil function (node-children node)))
               (traverse (new-node-function old-node-function)
                 (traverse-graph root-nodes #'do-children node-table #'identity
                                 new-node-function old-node-function))
               (set-sheet-position-yx (sheet y x)
	         (set-sheet-position sheet x y)))
        (multiple-value-bind (breadthfun depthfun position-setter
					 start-breadth start-depth)
            (ecase orientation
              ((:vertical :down :up)
               (values #'box-width #'box-height #'set-sheet-position start-x start-y))
              ((:horizontal :right :left)
               (values #'box-height #'box-width #'set-sheet-position-yx start-y start-x)))
          (let ((generation-descriptors (make-array (+ n-generations 1)))   ;; XXX: simple-vector
                (max-gen-breadth 0)
                (broadest-gen-descr nil))
            (loop for generation from 0 to n-generations
	       do (setf (aref generation-descriptors generation) (make-instance '<generation-descriptor>
										:generation generation
										:start-breadth start-breadth)))
            (when (or (eql orientation :up) (eql orientation :left))
              (setf generation-descriptors (nreverse generation-descriptors)))
            ;; Determine the breadth and depth of each generation
            (labels ((collect-node-size (parent hash child depth)
                       (declare (ignore parent hash depth))
                       (unless (sheet-withdrawn? child)
                         (let ((descr (find-value generation-descriptors
                                                  #'(lambda (gd)
                                                      (equal? (generation-generation gd) (node-generation child))))))
                           (incf (generation-size descr))
                           (incf (generation-breadth descr) (funcall breadthfun child))
                           (setf (generation-depth descr)
                                 (max (generation-depth descr) (funcall depthfun child)))))))
              (traverse #'collect-node-size (constantly nil))
              ;; Determine max-breadth and starting-depth
              (let ((depth-so-far start-depth))
		(loop for descr across generation-descriptors
		   do (let ((gen-breadth (generation-breadth descr)))
			(when (or (not broadest-gen-descr) (> gen-breadth max-gen-breadth))
			  (setf max-gen-breadth gen-breadth)
			  (setf broadest-gen-descr descr))
			(setf (generation-start-depth descr) depth-so-far)
			(incf depth-so-far (+ inter-generation (generation-depth descr)))))
                ;; Determine breadth-spacing
                (incf max-gen-breadth (* intra-generation (generation-size broadest-gen-descr)))
		(loop for descr across generation-descriptors
		   do (let ((excess (floor (- max-gen-breadth (generation-breadth descr))
					   (max (generation-size descr) 1))))
			(setf (generation-inner-breadth-separation descr) excess)
			(setf (generation-edge-breadth-separation descr) (floor excess 2))))
                (labels ((place-node (parent hash child depth)
                           (declare (ignore parent hash depth))
                           (unless (sheet-withdrawn? child)
                             (let ((descr (find-value generation-descriptors
                                                      #'(lambda (gd)
                                                          (equal? (generation-generation gd) (node-generation child))))))
                               (incf (generation-breadth-so-far descr)
                                     (if (generation-touched? descr)
                                         (generation-inner-breadth-separation descr)
                                         ;; else
                                         (progn
                                           (setf (generation-touched? descr) t)
                                           (generation-edge-breadth-separation descr))))
                               (funcall position-setter
					child
					(generation-breadth-so-far descr)
					(if center-nodes?
					    (+ (generation-start-depth descr)
					       (floor (- (generation-depth descr) (funcall depthfun child)) 2))
					    ;; else
					    (generation-start-depth descr)))
                               (multiple-value-bind (left top right bottom)
                                   (sheet-edges child)
                                 (declare (ignore left top))
                                 (setf width (max width right))
                                 (setf height (max height bottom))
                                 (incf (generation-breadth-so-far descr) (funcall breadthfun child)))))))
                  (traverse #'place-node (constantly nil)))))))))
    (values width height)))


#||
define method layout-graph-edges
    (layout :: <DAG-graph-layout>, #key reuse-edges? = #f) => ()
  let graph :: <graph-control-pane> = layout.%control-pane;
  let orientation = graph-orientation(graph);
  if (reuse-edges?)
    let edges :: <stretchy-object-vector> = layout.%edges;
    for (edge :: <graph-edge-pane> in edges)
      let from-node = graph-edge-from-node(edge);
      let to-node   = graph-edge-to-node(edge);
      let (x1, y1, x2, y2)
	= edge-attachment-points(from-node, to-node, orientation);
      edge.%x1 := x1;
      edge.%y1 := y1;
      edge.%x2 := x2;
      edge.%y2 := y2
    end
  else
    let node-table :: <object-table> = graph.%node-table;
    let root-nodes :: <stretchy-object-vector> = tree-control-root-nodes(graph);
    let edge-class    = graph-edge-class(graph) | <arrow-graph-edge>;
    let edge-initargs = graph-edge-initargs(graph);
    let edge-function = graph-edge-generator(graph);
    let edges :: <stretchy-object-vector> = make(<stretchy-vector>);
    unless (empty?(root-nodes))
      local method do-children (function :: <function>, node)
	      do(function, node-children(node))
	    end method,
	    method create-edge (from-node, hash, to-node, depth)
	      ignore(hash, depth);
	      // By definition, expanded nodes aren't withdrawn, so we
	      // do the more general test; we don't want to draw edges
	      // from unexpanded nodes to "shared" child nodes
	      when (from-node & node-state(from-node) == #"expanded"
		    & ~sheet-withdrawn?(to-node))
		let (x1, y1, x2, y2)
		  = edge-attachment-points(from-node, to-node, orientation);
		let object
		  = edge-function & edge-function(node-object(from-node), node-object(to-node));
		let edge
		  = apply(make, edge-class,
			  x1: x1, y1: y1, x2: x2, y2: y2, 
			  from-node: from-node, to-node: to-node,
			  object: object,
			  edge-initargs);
		add!(edges, edge)
	      end
	    end method;
      traverse-graph(root-nodes, do-children, node-table, identity,
		     create-edge, create-edge)
    end;
    layout.%edges := edges
  end
end method layout-graph-edges;
||#

(defmethod layout-graph-edges ((layout <DAG-graph-layout>) &key (reuse-edges? nil))
  (let* ((graph (%control-pane layout))
         (orientation (graph-orientation graph)))
    (if reuse-edges?
        (let ((edges (%edges layout)))    ;; XXX: stretchy vector
	  (loop for edge across edges
	     do (let ((from-node (graph-edge-from-node edge))
		      (to-node   (graph-edge-to-node edge)))
		  (multiple-value-bind (x1 y1 x2 y2)
		      (edge-attachment-points from-node to-node orientation)
		    (setf (%x1 edge) x1)
		    (setf (%y1 edge) y1)
		    (setf (%x2 edge) x2)
		    (setf (%y2 edge) y2)))))
        ;; else
        (let ((node-table (%node-table graph))
              (root-nodes (tree-control-root-nodes graph))  ;; XXX: stretchy
              (edge-class (or (graph-edge-class graph) (find-class '<arrow-graph-edge>)))
              (edge-initargs (graph-edge-initargs graph))   ;; XXX: list
              (edge-function (graph-edge-generator graph))
              (edges (MAKE-STRETCHY-VECTOR)))               ;; XXX: stretchy
          (unless (empty? root-nodes)
            (labels ((do-children (function node)
                       (map nil function (node-children node)))
                     (create-edge (from-node hash to-node depth)
                       (declare (ignore hash depth))
                       ;; By definition, expanded nodes aren't withdrawn, so we
                       ;; do the more general test; we don't want to draw edges
                       ;; from unexpanded nodes to "shared" child nodes
                       (when (and from-node
                                  (eql (node-state from-node) :expanded)
                                  (not (sheet-withdrawn? to-node)))
                         (multiple-value-bind (x1 y1 x2 y2)
                             (edge-attachment-points from-node to-node orientation)
                           (let* ((object (and edge-function (funcall edge-function (node-object from-node) (node-object to-node))))
                                  (edge (apply #'make-instance edge-class
                                               :x1 x1 :y1 y1 :x2 x2 :y2 y2
                                               :from-node from-node :to-node to-node
                                               :object object
                                               edge-initargs)))
                             (ADD! edges edge))))))
              (traverse-graph root-nodes #'do-children node-table #'identity
                              #'create-edge #'create-edge)))
          (setf (%edges layout) edges)))))


#||
// 'roots' is a sequence of the roots of the graph.  
// 'child-mapper' is a function of two arguments, a function and an object
// over whose child objects the function should be applied.
// 'node-table' is a table that is used to record and detect when an object has
// already been included in the graph.
// 'key' is a function of one argument used to produce the hash table key.
// There is no 'test' function, since it is already captured in the hash table.
// 'new-node-function' is a function of four arguments (the parent object, the
// parent object's hash value, the child object, and "nothing").  Its return
// value will be stored as the hash value of the child object.
// 'old-node-function' is a function of four arguments (the parent object, the
// parent object's hash value, the child object, and the child object's hash 
// value).  Its return value is ignored.
// 'max-depth' is the cutoff depth of the tree, or #f for no cutoff.
//--- Potential bug: the cutoff may fall short in that you may reach a certain
//--- node at the maximum depth, mark that node as seen and decline to descend
//--- into its children, then find that node again through a shorter path.  If
//--- you really want to fix this, write a breadth-first descent of the graph.
define method traverse-graph
    (roots :: <sequence>, child-mapper :: <function>,
     node-table :: <object-table>, key :: <function>,
     new-node-function :: <function>, old-node-function :: <function>,
     #key max-depth :: false-or(<integer>) = #f) => ()
  remove-all-keys!(node-table);
  local method traverse (parent-object, parent-hashval, object, max-depth)
	  let object-hashval
            = new-node-function(parent-object, parent-hashval, object, #f);
	  gethash(node-table, key(object)) := object-hashval;
	  when (max-depth) dec!(max-depth) end;
	  when (~max-depth | max-depth >= 0)
	    local method traverse1 (child-object)
		    let child-key = key(child-object);
		    let (child-hashval, found?)
		      = gethash(node-table, child-key);
		    if (found?)
		      old-node-function(object, object-hashval, child-object, child-hashval)
		    else
		      traverse(object, object-hashval, child-object, max-depth)
		    end
		  end method;
	    child-mapper(traverse1, object)
	  end
	end method;
  do(method (root) traverse(#f, #f, root, max-depth) end, roots)
end method traverse-graph;
||#

;; 'roots' is a sequence of the roots of the graph.
;; 'child-mapper' is a function of two arguments, a function and an object
;; over whose child objects the function should be applied.
;; 'node-table' is a table that is used to record and detect when an object has
;; already been included in the graph.
;; 'key' is a function of one argument used to produce the hash table key.
;; There is no 'test' function, since it is already captured in the hash table.
;; 'new-node-function' is a function of four arguments (the parent object, the
;; parent object's hash value, the child object, and "nothing"). Its return
;; value will be stored as the hash value of the child object.
;; 'old-node-function' is a function of four arguments (the parent object, the
;; parent object's hash value, the child object, and the child object's hash
;; value). Its return value is ignored.
;; 'max-depth' is the cutoff depth of the tree, or #f for no cutoff.
;;--- Potential bug: the cutoff may fall short in that you may reach a certain
;;--- node at the maximum depth, mark that node as seen and decline to descend
;;--- into its children, then find that node again through a shorter path. If
;;--- you really want to fix this, write a breadth-first descent of the graph.
(defmethod traverse-graph ((roots sequence) (child-mapper function)
                           (node-table hash-table) (key function)
                           (new-node-function function) (old-node-function function)
                           &key (max-depth nil))
  (clrhash node-table) ;; XXX: REMOVE-ALL-KEYS! ?
  (labels ((traverse (parent-object parent-hashval object max-depth)
             (let ((object-hashval (funcall new-node-function parent-object
                                            parent-hashval object nil)))
               (setf (gethash (funcall key object) node-table) object-hashval)
               (when max-depth (decf max-depth))
               (when (or (not max-depth) (>= max-depth 0))
                 (labels ((traverse1 (child-object)
                            (let ((child-key (funcall key child-object)))
                              (multiple-value-bind (child-hashval found?)
                                  (gethash child-key node-table)
                                (if found?
                                    (funcall old-node-function object object-hashval child-object child-hashval)
                                    ;; else
                                    (traverse object object-hashval child-object max-depth))))))
                   (funcall child-mapper #'traverse1 object))))))
    (map nil #'(lambda (root) (traverse nil nil root max-depth)) roots)))
