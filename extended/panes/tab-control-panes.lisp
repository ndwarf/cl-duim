;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GADGET-PANES-INTERNALS -*-
(in-package #:duim-gadget-panes-internals)

#||
/// Generic implementation of tab control panes

define sealed class <tab-control-pane> (<column-layout>, <tab-control>)
  slot %button-box :: false-or(<button-box>) = #f;
  keyword y-spacing: = 2;
end class <tab-control-pane>;
||#

(defclass <tab-control-pane> (<column-layout> <tab-control>)
  ((%button-box :type (or null <button-box>) :initform nil :accessor %button-box))
  (:default-initargs :y-spacing 2))


(defgeneric tab-control-stack-layout (pane))
(defgeneric tab-control-map-page (pane new-page))
(defgeneric tab-control-mapped-page (pane))

#||
define sealed domain make (singleton(<tab-control-pane>));
define sealed domain initialize (<tab-control-pane>);

define sideways method class-for-make-pane
    (framem :: <frame-manager>, class == <tab-control>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<tab-control-pane>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <frame-manager>) (class (eql (find-class '<tab-control>))) &key)
  (values (find-class '<tab-control-pane>) nil))


#||
define sealed method initialize
    (pane :: <tab-control-pane>, #key frame-manager: framem)
  next-method();
  let pages = tab-control-pages(pane);
  let layout = make(<stack-layout>, children: pages);
  let tabs-at-top? = tab-control-tabs-position(pane) == #"top";
  let button-class
    = if (tabs-at-top?) <top-tab-button> else <bottom-tab-button> end;
  let button-box
    = with-frame-manager (framem)
        make(<radio-box>, 
	     items: pages,
	     label-key: method (page)
			  let label = gadget-label-key(pane)(page);
			  compute-mnemonic-from-label
			    (page, label, remove-ampersand?: #t)
			end,
	     spacing: 0,
	     value: tab-control-current-page(pane),
	     button-class: button-class,
	     value-changed-callback:
	       method (radio-box)
		 tab-control-map-page(pane, gadget-value(radio-box))
	       end method)
      end;
  pane.%button-box := button-box;
  let children
    = case
	~layout      => vector(button-box);
	tabs-at-top? => vector(button-box, layout);
	otherwise    => vector(layout, button-box);
      end;
  sheet-children(pane) := children
end method initialize;
||#

(defmethod initialize-instance :after ((pane <tab-control-pane>) &key ((:frame-manager framem))
				       &allow-other-keys)
  (let* ((pages (tab-control-pages pane))
	 (layout (make-pane '<stack-layout> :children pages))
	 (tabs-at-top? (eql (tab-control-tabs-position pane) :top))
	 (button-class (if tabs-at-top? (find-class '<top-tab-button>) (find-class '<bottom-tab-button>)))
	 (button-box (with-frame-manager (framem)
		       (make-pane '<radio-box>
				  :items pages
				  :label-key #'(lambda (page)
						 (let ((label (funcall (gadget-label-key pane) page)))
						   (compute-mnemonic-from-label page label :remove-ampersand? t)))
				  :spacing 0
				  :value (tab-control-current-page pane)
				  :button-class button-class
				  :value-changed-callback #'(lambda (radio-box)
							      (tab-control-map-page pane (gadget-value radio-box)))))))
    (setf (%button-box pane) button-box)
    (let ((children (cond
		      ;; Does this first clause mean that there are no
		      ;; children, or that the construction of a <stack-
		      ;; layout> instance failed?
		      ((not layout) (vector button-box))
		      (tabs-at-top? (vector button-box layout))
		      (t            (vector layout button-box)))))
      (setf (sheet-children pane) children))))


#||
define method tab-control-stack-layout
    (pane :: <tab-control-pane>) => (layout :: <stack-layout>)
  let children = sheet-children(pane);
  if (size(children) = 2) children[1] else children[0] end
end method tab-control-stack-layout;
||#

(defmethod tab-control-stack-layout ((pane <tab-control-pane>))
  (let ((children       (sheet-children pane))
	(tabs-at-top?   (eql (tab-control-tabs-position pane) :top)))
    ;; If tabs are at top, children are arranged: tab-buttons, stack-layout.
    ;; Otherwise they're arranged: stack-layout, tab-buttons.
    (if (or (= (size children) 1)   ;; If there are no tab-buttons...
	    (not tabs-at-top?))     ;; or the tab-buttons are at the bottom
	(SEQUENCE-ELT children 0)            ;; we want the first child
	(SEQUENCE-ELT children 1))))         ;; else we want the second.


#||
define method note-pages-changed
    (pane :: <tab-control-pane>) => ()
  let pages = tab-control-pages(pane);
  let old-page = tab-control-mapped-page(pane);
  let new-page = tab-control-current-page(pane);
  let swap-page? = (old-page ~== new-page);
  when (swap-page? & old-page)
    sheet-withdrawn?(old-page) := #t;
    sheet-withdrawn?(new-page) := #f
  end;
  let stack :: <stack-layout> = tab-control-stack-layout(pane);
  sheet-children(stack) := pages;
  //--- This is surely too aggressive, but seems to be needed
  //--- to get things layed out right
  invalidate-space-requirements(stack);
  when (sheet-attached?(stack))
    relayout-parent(stack);
    //---*** Can this be removed if we fix 'set-sheet-edges'?
    update-all-mirror-positions(stack)
  end;
  let buttons = pane.%button-box;
  when (buttons)
    gadget-items(buttons) := pages;
    gadget-value(buttons) := new-page;
    when (sheet-mapped?(buttons))
      repaint-sheet(buttons, sheet-region(buttons))
    end
  end;
  swap-page? & tab-control-map-page(pane, new-page)
end method note-pages-changed;
||#

(defmethod note-pages-changed ((pane <tab-control-pane>))
  (let* ((pages (tab-control-pages pane))
         (old-page (tab-control-mapped-page pane))
         (new-page (tab-control-current-page pane))
         (swap-page? (not (eql old-page new-page))))
    (when (and swap-page? old-page)
      (setf (sheet-withdrawn? old-page) t)
      (setf (sheet-withdrawn? new-page) nil))
    (let ((stack (tab-control-stack-layout pane)))
      (setf (sheet-children stack) pages)
      ;;--- This is surely too aggressive, but seems to be needed
      ;;--- to get things layed out right
      (invalidate-space-requirements stack)
      (when (sheet-attached? stack)
	(relayout-parent stack)
	;;---*** Can this be removed if we fix 'set-sheet-edges'? (dr: what's wrong with set-sheet-edges?)
	(update-all-mirror-positions stack))
      (let ((buttons (%button-box pane)))
	(when buttons
	  (setf (gadget-items buttons) pages)
	  (setf (gadget-value buttons) new-page)
	  (when (sheet-mapped? buttons)
	    (repaint-sheet buttons (sheet-region buttons))))))
    (and swap-page? (tab-control-map-page pane new-page))))


#||
define method tab-control-mapped-page
    (pane :: <tab-control-pane>) => (child :: false-or(<sheet>))
  let stack :: <stack-layout> = tab-control-stack-layout(pane);
  stack-layout-mapped-page(stack)
end method tab-control-mapped-page;
||#

(defmethod tab-control-mapped-page ((pane <tab-control-pane>))
  (let ((stack (tab-control-stack-layout pane)))
    (stack-layout-mapped-page stack)))


#||
define method tab-control-map-page
    (pane :: <tab-control-pane>, new-page :: <sheet>) => ()
  let stack :: <stack-layout> = tab-control-stack-layout(pane);
  stack-layout-mapped-page(stack) := new-page
end method tab-control-map-page;
||#

(defmethod tab-control-map-page ((pane <tab-control-pane>) (new-page <sheet>))
  (let ((stack (tab-control-stack-layout pane)))
    (setf (stack-layout-mapped-page stack) new-page)))


#||
define method note-gadget-value-changed
    (pane :: <tab-control-pane>) => ()
  when (sheet-mapped?(pane))
    tab-control-map-page(pane, tab-control-current-page(pane))
  end
end method note-gadget-value-changed;
||#

(defmethod note-gadget-value-changed ((pane <tab-control-pane>))
  (when (sheet-mapped? pane)
    (tab-control-map-page pane (tab-control-current-page pane))))



#||

/// Tab buttons

define sealed class <tab-button> (<radio-button>, <simple-pane>)
  sealed constant slot %position :: <vertical-position> = #"top",
    init-keyword: position:;
end class <tab-button>;
||#

(defclass <tab-button> (<radio-button> <simple-pane>)
  ((%position :type <vertical-position> :initform :top :initarg :position :reader %position)))


#||
define sealed domain make (subclass(<tab-button>));
define sealed domain initialize (<tab-button>);

define sealed class <top-tab-button> (<tab-button>)
  keyword position: = #"top";
end class <top-tab-button>;
||#

(defclass <top-tab-button> (<tab-button>)
  ()
  (:default-initargs :position :top))


#||
define sealed class <bottom-tab-button> (<tab-button>)
  keyword position: = #"bottom";
end class <bottom-tab-button>;
||#

(defclass <bottom-tab-button> (<tab-button>)
  ()
  (:default-initargs :position :bottom))


#||
define method do-compose-space
    (pane :: <tab-button>, #key width, height)
 => (space-req :: <space-requirement>)
  ignore(width, height);
  let (width, height) = gadget-label-size(pane);
  make(<space-requirement>,
       width: width + 12, height: height + 8)
end method do-compose-space;
||#

(defmethod do-compose-space ((pane <tab-button>) &key width height)
  (declare (ignore width height))
  (multiple-value-bind (width height)
      (gadget-label-size pane)
    (make-space-requirement :width (+ width 12) :height (+ height 8))))


#||
define method handle-repaint
    (pane :: <tab-button>, medium :: <medium>, region :: <region>) => ()
  let (left, top, right, bottom) = box-edges(pane);
  let bottom = bottom - 1;
  let indentation = 4;
  let selected? = gadget-value(pane);
  let position = pane.%position;
  with-brush (medium, 
	      foreground: if (selected?) $background else $foreground end)
    with-pen (medium, width: 2)
      draw-line(medium, left, bottom, right, bottom)
    end 
  end;
  inc!(left);
  dec!(right);
  inc!(top);
  with-pen (medium,
	    width: if (selected?) 2 else 1 end)
    let (y1, y2, y3)
      = select (position)
          #"top"    => values(bottom, top + indentation, top);
	  #"bottom" => values(top, bottom - indentation, bottom);
	end;
    draw-polygon(medium,
		 vector(left, y1,
			left, y2,
			left + indentation, y3,
			right - indentation, y3,
			right, y2,
			right, y1),
		 filled?: #f,
		 closed?: #f)
  end;
  draw-gadget-label(pane, medium, 6, 6)
end method handle-repaint;
||#

(defmethod handle-repaint ((pane <tab-button>) (medium <medium>) (region <region>))
  (multiple-value-bind (left top right bottom)
      (box-edges pane)
    (let ((bottom (- bottom 1))
	  (indentation 4)
	  (selected? (gadget-value pane))
	  (position (%position pane)))
      (with-brush (medium :foreground (if selected? *background* *foreground*))
        (with-pen (medium :width 2)
	  ;; FIXME -- revisit all this.
	  ;;---*** I'm not sure about incrementing the "top" values when the
	  ;;---*** buttons are on the bottom. It' seems to work though...
;;	  (if (eql position :top)
	      (draw-line medium left bottom right bottom)        ;; draw "thick" line at bottom if tab-buttons are on top
;;	      (draw-line medium left (1+ top) right (1+ top))))) ;; ... and at top if tab-buttons are on bottom.
	      ))
      (incf left)
      (decf right)
      (incf top)
      (with-pen (medium :width (if selected? 2 1))
        (multiple-value-bind (y1 y2 y3)
	    (ecase position
	      (:top    (values bottom (+ top indentation) top))
	      (:bottom (values top (- bottom indentation) bottom)))
	  (draw-polygon medium
			(vector left y1
				left y2
				(+ left indentation) y3
				(- right indentation) y3
				right y2
				right y1)
			:filled? nil
			:closed? nil)))
      (draw-gadget-label pane medium 6 6))))


#||
define method handle-event 
    (pane :: <tab-button>, event :: <button-press-event>) => ()
  when (gadget-enabled?(pane)
        & event-button(event) == $left-button
        & ~gadget-value(pane))
    gadget-value(pane, do-callback?: #t) := #t
  end
end method handle-event;
||#

(defmethod handle-event ((pane <tab-button>) (event <button-press-event>))
  (when (and (gadget-enabled? pane)
	     (eql (event-button event) +left-button+)
	     (not (gadget-value pane)))
    (setf (gadget-value pane :do-callback? t) t)))


#||
define method note-gadget-value-changed
    (pane :: <tab-button>) => ()
  when (sheet-mapped?(pane))
    clear-box*(pane, sheet-region(pane));
    repaint-sheet(pane, $everywhere)
  end
end method note-gadget-value-changed;
||#

(defmethod note-gadget-value-changed ((pane <tab-button>))
  (when (sheet-mapped? pane)
    (clear-box* pane (sheet-region pane))
    (repaint-sheet pane *everywhere*)))
