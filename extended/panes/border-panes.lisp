;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GADGET-PANES-INTERNALS -*-
(in-package #:duim-gadget-panes-internals)

#||
/// Generic implementation of borders

define open abstract class <border-pane-mixin>
    (<standard-repainting-mixin>)
  sealed slot %pen   :: false-or(<standard-pen>) = #f;
  sealed slot %brush :: false-or(type-union(<standard-brush>, <ink>)) = #f;
end class <border-pane-mixin>;
||#

(defclass <border-pane-mixin> (<standard-repainting-mixin>)
  ((%pen   :type (or null <standard-pen>)         :initform nil :accessor %pen)
   (%brush :type (or null <standard-brush> <ink>) :initform nil :accessor %brush)))


#||
define method initialize (sheet :: <border-pane-mixin>, #key thickness) => ()
  next-method();
  when (~thickness
	& member?(border-type(sheet),
		  #[#"sunken", #"raised", #"ridge", #"groove", #"input", #"output"]))
    border-thickness(sheet) := 3
  end
end method initialize;
||#

(defmethod initialize-instance :after ((sheet <border-pane-mixin>) &key thickness &allow-other-keys)
  (when (and (not thickness)
             (member (border-type sheet)
		     '(:sunken :raised :ridge :groove :input :output)))
    (setf (border-thickness sheet) 3)))


#||
define open generic border-characteristics
    (pane)
 => (thickness :: <integer>,
     pen :: <standard-pen>, brush :: type-union(<standard-brush>, <ink>));
||#

(defgeneric border-characteristics (pane))


#||
define method do-compose-space
    (pane :: <border-pane-mixin>, #key width, height)
 => (space-req :: <space-requirement>)
  let thickness*2 = border-thickness(pane) * 2;
  space-requirement+(pane,
		     next-method(pane,
				 width:  width  & width  - thickness*2,
				 height: height & height - thickness*2),
		     width: thickness*2, height: thickness*2)
end method do-compose-space;
||#

(defmethod do-compose-space ((pane <border-pane-mixin>) &key width height)
  (let ((thickness*2 (* (border-thickness pane) 2)))
    (space-requirement+ pane
                        (call-next-method pane
                                          :width (and width (- width thickness*2))
                                          :height (and height (- height thickness*2)))
                        :width thickness*2 :height thickness*2)))


#||
define method do-allocate-space
    (pane :: <border-pane-mixin>, width :: <integer>, height :: <integer>) => ()
  let child = sheet-child(pane);
  let thickness = border-thickness(pane);
  when (child)
    set-sheet-edges(child,
                    thickness, thickness,
                    width - thickness, height - thickness)
  end
end method do-allocate-space;
||#

(defmethod do-allocate-space ((pane <border-pane-mixin>) (width integer) (height integer))
  (let ((child (sheet-child pane))
        (thickness (border-thickness pane)))
    (when child
      (set-sheet-edges child
                       thickness thickness
                       (- width thickness) (- height thickness)))))


#||
define method handle-repaint
    (pane :: <border-pane-mixin>, medium :: <medium>, region :: <region>) => ()
  ignore(region);	// not worth checking
  let (left, top, right, bottom) = box-edges(pane);
  draw-border(pane, medium, border-type(pane), left, top, right, bottom)
end method handle-repaint;
||#

(defmethod handle-repaint ((pane <border-pane-mixin>) (medium <medium>) (region <region>))
  (declare (ignore region))   ;; not worth checking
  (multiple-value-bind (left top right bottom)
      (box-edges pane)
    (draw-border pane medium (border-type pane) left top right bottom)))


#||
define method draw-border
    (pane :: <border-pane-mixin>, medium :: <medium>, type :: <border-type>,
     left  :: <integer>, top    :: <integer>,
     right :: <integer>, bottom :: <integer>) => ()
  let (thickness, pen, brush) = border-characteristics(pane);
  with-drawing-options (medium, pen: pen, brush: brush)
    let thickness/2 = ceiling/(thickness, 2);
    draw-rectangle(medium, 
                   left  + thickness/2, top + thickness/2, 
                   right - thickness/2 - 1, bottom - thickness/2 - 1,
                   filled?: #f)
  end
end method draw-border;
||#

(defmethod draw-border ((pane <border-pane-mixin>) (medium <medium>) type
                        (left integer) (top integer)
                        (right integer) (bottom integer))
  (check-type type <border-type>)
  (multiple-value-bind (thickness pen brush)
      (border-characteristics pane)
    (with-drawing-options (medium :pen pen :brush brush)
      (let ((thickness/2 (ceiling thickness 2)))
        (draw-rectangle medium
                        (+ left thickness/2) (+ top thickness/2)
                        (- right thickness/2 1) (- bottom thickness/2 1)
                        :filled? nil)))))


#||

/// Border panes

define sealed class <border-pane>
    (<border-pane-mixin>, <border>, <basic-sheet>)
end class <border-pane>;
||#

(defclass <border-pane>
    (<border-pane-mixin> <border> <basic-sheet>)
  ())


#||
define method border-characteristics
    (pane :: <border-pane>)
 => (thickness :: <integer>,
     pen :: <standard-pen>, brush :: type-union(<standard-brush>, <ink>))
  let thickness = border-thickness(pane);
  unless (pane.%pen)
    pane.%pen := make(<pen>, width: thickness)
  end;
  unless (pane.%brush)
    pane.%brush := get-default-foreground(port(pane), pane)
  end;
  values(thickness, pane.%pen, pane.%brush)
end method border-characteristics;
||#

(defmethod border-characteristics ((pane <border-pane>))
  (let ((thickness (border-thickness pane)))
    (unless (%pen pane)
      (setf (%pen pane) (make-pen :width thickness)))
    (unless (%brush pane)
      (setf (%brush pane) (get-default-foreground (port pane) pane)))
    (values thickness (%pen pane) (%brush pane))))


#||
define sideways method class-for-make-pane 
    (framem :: <frame-manager>, class == <border>, #key label)
 => (class :: <class>, options :: false-or(<sequence>))
  let border-class = if (label) <group-box-pane> else <border-pane> end;
  values(border-class, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <frame-manager>) (class (eql (find-class '<border>))) &key label)
  (let ((border-class (if label (find-class '<group-box-pane>) (find-class '<border-pane>))))
    (values border-class nil)))


#||
define sealed domain make (singleton(<border-pane>));
define sealed domain initialize (<border-pane>);


/// Spacing panes

define sealed class <spacing-pane>
    (<border-pane-mixin>, <spacing>, <basic-sheet>)
end class <spacing-pane>;
||#

(defclass <spacing-pane>
    (<border-pane-mixin> <spacing> <basic-sheet>)
  ())


#||
define method border-characteristics
    (pane :: <spacing-pane>)
 => (thickness :: <integer>,
     pen :: <standard-pen>, brush :: type-union(<standard-brush>, <ink>))
  let thickness = border-thickness(pane);
  unless (pane.%pen)
    pane.%pen := make(<pen>, width: thickness)
  end;
  unless (pane.%brush)
    pane.%brush := get-default-background(port(pane), pane)
  end;
  values(thickness, pane.%pen, pane.%brush)
end method border-characteristics;
||#

(defmethod border-characteristics ((pane <spacing-pane>))
  (let ((thickness (border-thickness pane)))
    (unless (%pen pane)
      (setf (%pen pane) (make-pen :width thickness)))
    (unless (%brush pane)
      (setf (%brush pane) (get-default-background (port pane) pane)))
    (values thickness (%pen pane) (%brush pane))))


#||
define sideways method class-for-make-pane 
    (framem :: <frame-manager>, class == <spacing>, #key)
 => (class :: <class>, options :: false-or(<sequence>));
  values(<spacing-pane>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <frame-manager>) (class (eql (find-class '<spacing>))) &key)
  (values (find-class '<spacing-pane>) nil))


#||
define sealed domain make (singleton(<spacing-pane>));
define sealed domain initialize (<spacing-pane>);


/// Group box panes, aka labelled border panes

define sealed class <group-box-pane>
    (<border-pane-mixin>, <group-box>, <basic-sheet>)
  sealed slot %border-coords = #f;
end class <group-box-pane>;
||#

(defclass <group-box-pane>
    (<border-pane-mixin> <group-box> <basic-sheet>)
  ((%border-coords :initform nil :accessor %border-coords)))


#||
define method border-characteristics
    (pane :: <group-box-pane>)
 => (thickness :: <integer>,
     pen :: <standard-pen>, brush :: type-union(<standard-brush>, <ink>))
  let thickness = border-thickness(pane);
  unless (pane.%pen)
    pane.%pen := make(<pen>, width: thickness)
  end;
  unless (pane.%brush)
    pane.%brush := get-default-foreground(port(pane), pane)
  end;
  values(thickness, pane.%pen, pane.%brush)
end method border-characteristics;
||#

(defmethod border-characteristics ((pane <group-box-pane>))
  (let ((thickness (border-thickness pane)))
    (unless (%pen pane)
      (setf (%pen pane) (make-pen :width thickness)))
    (unless (%brush pane)
      (setf (%brush pane) (get-default-foreground (port pane) pane)))
    (values thickness (%pen pane) (%brush pane))))


#||
define sideways method class-for-make-pane 
    (framem :: <frame-manager>, class == <group-box>, #key)
 => (class :: <class>, options :: false-or(<sequence>));
  values(<group-box-pane>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <frame-manager>) (class (eql (find-class '<group-box>))) &key)
  (values (find-class '<group-box-pane>) nil))


#||
define sealed domain make (singleton(<group-box-pane>));
define sealed domain initialize (<group-box-pane>);

define constant $group-box-internal-border :: <integer> = 7;
||#

(defconstant +group-box-internal-border+ 7)


#||
define method do-compose-space
    (pane :: <group-box-pane>, #key width, height)
 => (space-req :: <space-requirement>)
  let child = sheet-child(pane);
  let border = (border-thickness(pane) + $group-box-internal-border) * 2;
  let (label-width, label-height) = gadget-label-size(pane);
  ignore(label-width);
  if (child)
    space-requirement+(pane,
		       compose-space(child,
				     width:  width  & width  - border,
				     height: height & height - border),
		       width:  border, height: border + label-height)
  else
    default-space-requirement(pane, width: width, height: height)
  end
end method do-compose-space;
||#

(defmethod do-compose-space ((pane <group-box-pane>) &key width height)
  (let ((child (sheet-child pane))
        (border (+ (border-thickness pane)
                   (* +group-box-internal-border+ 2))))
    (multiple-value-bind (label-width label-height)
        (gadget-label-size pane)
      ;; FIXME: what if the label is wider than the group box would be otherwise? This seems to happen all the time
      ;; in the gui tests for example.
      (declare (ignore label-width))
      (if child
          (space-requirement+ pane
                              (compose-space child
                                             :width (and width (- width border))
                                             :height (and height (- height border)))
                              :width border :height (+ border label-height))
          ;; else
          (default-space-requirement pane :width width :height height)))))


#||
define method do-allocate-space
    (pane :: <group-box-pane>, width :: <integer>, height :: <integer>) => ()
  let child = sheet-child(pane);
  let border = border-thickness(pane) + $group-box-internal-border;
  let (label-width, label-height) = gadget-label-size(pane);
  ignore(label-width);
  when (child)
    select (group-box-label-position(pane))
      #"top" =>
	set-sheet-edges(child,
                        border, label-height + border,
                        width - border, height - border);
      #"bottom" =>
	set-sheet-edges(child,
                        border, border,
                        width - border, height - label-height - border);
    end
  end;
  let (thickness, pen, brush) = border-characteristics(pane);
  ignore(pen, brush);
  let (left, top, right, bottom) = box-edges(pane);
  let thickness/2 = ceiling/(thickness, 2);
  inc!(left, thickness/2);
  inc!(top,  thickness/2);
  dec!(right,  thickness/2 + 1);
  dec!(bottom, thickness/2 + 1);
  let (label-width, label-height) = gadget-label-size(pane);
  inc!(top, truncate/(label-height, 2));
  pane.%border-coords
    := select (group-box-label-position(pane))
	 #"top" =>
	   vector(left + floor/(right - left - label-width, 2), top,
		  left, top,
		  left, bottom,
		  right, bottom,
		  right, top,
		  right - floor/(right - left - label-width, 2), top);
	 #"bottom" =>
	   vector(left + floor/(right - left - label-width, 2), bottom,
		  left, bottom,
		  left, top,
		  right, top,
		  right, bottom,
		  right - floor/(right - left - label-width, 2), bottom);
       end
end method do-allocate-space;
||#

(defmethod do-allocate-space ((pane <group-box-pane>) (width integer) (height integer))
  (let ((child (sheet-child pane))
        (border (+ (border-thickness pane) +group-box-internal-border+)))
    (multiple-value-bind (label-width label-height)
        (gadget-label-size pane)
      (declare (ignore label-width))
      (when child
        (ecase (group-box-label-position pane)
          (:top (set-sheet-edges child
                                 border (+ label-height border)
                                 (- width border) (- height border)))
          (:bottom (set-sheet-edges child
                                    border border
                                    (- width border) (- height label-height border)))))
      (multiple-value-bind (thickness pen brush)
          (border-characteristics pane)
        (declare (ignore pen brush))
        (multiple-value-bind (left top right bottom)
            (box-edges pane)
          (let ((thickness/2 (ceiling thickness 2)))
            (incf left thickness/2)
            (incf top thickness/2)
            (decf right (+ 1 thickness/2))
            (decf bottom (+ 1 thickness/2))
            (multiple-value-bind (label-width label-height)
                (gadget-label-size pane)
              (incf top (truncate label-height 2))
              (setf (%border-coords pane)
                    (ecase (group-box-label-position pane)
                      (:top (vector (+ left (floor (- right left label-width) 2)) top
				    left top
				    left bottom
				    right bottom
				    right top
				    (- right (floor (- right left label-width) 2)) top))
                      (:bottom (vector (+ left (floor (- right left label-width) 2)) bottom
				       left bottom
				       left top
				       right top
				       right bottom
				       (- right (floor (- right left label-width) 2)) bottom)))))))))))


#||
define method handle-repaint
    (pane :: <group-box-pane>, medium :: <medium>, region :: <region>) => ()
  ignore(region);
  let (thickness, pen, brush) = border-characteristics(pane);
  with-drawing-options (medium, pen: pen, brush: brush)
    let (left, top, right, bottom) = box-edges(pane);
    ignore(top);
    let thickness/2 = ceiling/(thickness, 2);
    inc!(left, thickness/2);
    inc!(top,  thickness/2);
    dec!(right,  thickness/2 - 1);
    dec!(bottom, thickness/2 - 1);
    select (group-box-label-position(pane))
      #"top" =>
	draw-polygon(medium, pane.%border-coords, closed?: #f, filled?: #f);
	draw-gadget-label
	  (pane, medium,
	   floor/(right + left, 2), pane.%border-coords[1],	// what can I say?
	   align-x: #"center", align-y: #"center");
      #"bottom" =>
	draw-polygon(medium, pane.%border-coords, closed?: #f, filled?: #f);
	draw-gadget-label
	  (pane, medium,
	   floor/(right - left, 2), pane.%border-coords[1],	// I'm embarrassed
	   align-x: #"center", align-y: #"center");
    end
  end
end method handle-repaint;
||#

(defmethod handle-repaint ((pane <group-box-pane>) (medium <medium>) (region <region>))
  (declare (ignore region))
  (multiple-value-bind (thickness pen brush)
      (border-characteristics pane)
    (with-drawing-options (medium :pen pen :brush brush)
      (multiple-value-bind (left top right bottom)
          (box-edges pane)
        (let ((thickness/2 (ceiling thickness 2)))
          (incf left thickness/2)
          (incf top thickness/2)
          (decf right (- thickness/2 1))     ;; FIXME: ALLOCATE SPACE *ADDS* 1 TO THICKNESS/2. SHOULD THESE BE DIFFERENT, REALLY?
          (decf bottom (- thickness/2 1))
          (ecase (group-box-label-position pane)
            (:top (draw-polygon medium (%border-coords pane) :closed? nil :filled? nil)
		  (draw-gadget-label pane medium
				     (floor (+ right left) 2)
				     (aref (%border-coords pane) 1)  ;; what can I say?
				     :align-x :center :align-y :center))
            (:bottom (draw-polygon medium (%border-coords pane) :closed? nil :filled? nil)
		     (draw-gadget-label pane medium
					(floor (- right left) 2) (aref (%border-coords pane) 1)  ;; I'm embarassed
					:align-x :center :align-y :center))))))))

