;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GADGET-PANES-INTERNALS -*-
(in-package #:duim-gadget-panes-internals)

#||
/// Generic implementation of table control panes

define sealed class <table-control-pane>
    (<standard-input-mixin>,
     <standard-repainting-mixin>,
     <permanent-medium-mixin>,
     <homegrown-control-mixin>,
     <table-control>,
     <single-child-wrapping-pane>)
end class <table-control-pane>;
||#

(defclass <table-control-pane>
    (<standard-input-mixin>
     <standard-repainting-mixin>
     <permanent-medium-mixin>
     <homegrown-control-mixin>
     <table-control>
     <single-child-wrapping-pane>)
  ())


#||
define sideways method class-for-make-pane 
    (framem :: <frame-manager>, class == <table-control>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<table-control-pane>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <frame-manager>) (class (eql (find-class '<table-control>))) &key)
  (values (find-class '<table-control-pane>) nil))


#||
define sealed domain make (singleton(<table-control-pane>));
define sealed domain initialize (<table-control-pane>);


define sealed class <table-control-layout>
    (<homegrown-control-layout-mixin>, <column-layout>)
end class <table-control-layout>;
||#

(defclass <table-control-layout>
    (<homegrown-control-layout-mixin>
     <table-layout>)
  ())


#||
define sealed domain make (singleton(<table-control-layout>));
define sealed domain initialize (<table-control-layout>);


define sealed method initialize
    (pane :: <table-control-pane>, #key frame-manager: framem) => ()
  next-method();
  let n-columns :: <integer> = size(table-control-columns(pane));
  with-frame-manager (framem)
    let layout  = make(<table-control-layout>,
		       x-spacing: 4,
		       columns:   n-columns);
    pane.%layout-pane    := layout;
    layout.%control-pane := pane;
    let scroll-bars = gadget-scroll-bars(pane);
    sheet-child(pane) 
      := if (scroll-bars == #"none")
	   layout
	 else
	   scrolling (scroll-bars: scroll-bars, border-type: #f)
	     layout
	   end
         end
  end;
end method initialize;
||#

(defmethod initialize-instance :after ((pane <table-control-pane>) &key ((:frame-manager framem))
				       &allow-other-keys)
  (let ((n-columns (length (table-control-columns pane))))
    (with-frame-manager (framem)
      (let ((layout (make-pane '<table-control-layout>
			       :x-spacing 4
			       :columns n-columns)))
	(setf (%layout-pane pane) layout)
	(setf (%control-pane layout) pane)
	(let ((scroll-bars (gadget-scroll-bars pane)))
	  (setf (sheet-child pane)
		(if (eql scroll-bars :none)
		    layout
		    (scrolling (:scroll-bars scroll-bars :border-type nil)
		      layout))))))))


(defgeneric make-table-control-children (framem pane)
  (:documentation
"
Keep the children of the layout pane in step with 'gadget-items'
"))


#||
// Build the items for the first time when the sheet is fully attached
define method note-sheet-attached (pane :: <table-control-pane>) => ()
  next-method();
  note-gadget-items-changed(pane)
end method note-sheet-attached;
||#

(defmethod note-sheet-attached ((pane <table-control-pane>))
  (call-next-method)
  (note-gadget-items-changed pane))


#||
define method note-gadget-items-changed 
    (pane :: <table-control-pane>) => ()
  next-method();
  delaying-layout (pane)
    let framem = frame-manager(pane);
    let layout = pane.%layout-pane;
    when (framem)
      sheet-children(layout) := make-table-control-children(frame-manager(pane), pane);
    end
  end
end method note-gadget-items-changed;
||#

(defmethod note-gadget-items-changed ((pane <table-control-pane>))
  (call-next-method)
  (delaying-layout (pane)
    (let ((framem (frame-manager pane))
	  (layout (%layout-pane pane)))
      (when framem
	(setf (sheet-children layout)
	      (make-table-control-children (frame-manager pane) pane))))))


#||
// Keep the children of the layout pane in step with 'gadget-items'
//--- This doesn't observe 'table-column-width' or 'table-column-alignment'
define method make-table-control-children
    (framem :: <frame-manager>, pane :: <table-control-pane>)
 => (children :: <stretchy-vector>)
  let children :: <stretchy-object-vector> = make(<stretchy-vector>);
  let columns = table-control-columns(pane);
  for (column in columns)
    let heading = table-column-heading(column);
    add!(children, 
	 make(<label-pane>,
	      label: heading,
              frame-manager: framem,
	      text-style: make(<text-style>, weight: #"bold")))
  end;
  for (item in gadget-items(pane))
    for (column in columns)
      let generator :: <function> = table-column-generator(column);
      let object = generator(item);
      add!(children, make-item(pane, object, frame-manager: framem))
    end
  end;
  children
end method make-table-control-children;
||#

(defmethod make-table-control-children ((framem <frame-manager>) (pane <table-control-pane>))
  (let ((children (MAKE-STRETCHY-VECTOR))
	(columns  (table-control-columns pane)))
    (map nil #'(lambda (column)
		 (let ((heading (table-column-heading column)))
		   (ADD! children
			 (make-pane '<label-pane>
				    :label heading
				    :frame-manager framem
				    :text-style
				    (make-text-style nil nil :bold nil nil)))))
	 columns)
    ;; FIXME: This will blow out if given a <range>...
    (map nil #'(lambda (item)
		 (map nil #'(lambda (column)
			      (let* ((generator (table-column-generator column))
				     (object    (funcall generator item)))
				(ADD! children (make-item pane object :frame-manager framem))))
		      columns))
	 (gadget-items pane))
    children))



#||

/// Generic implementation of table item panes

define sealed class <table-item-pane> 
    (<row-layout>, <labelled-gadget-mixin>, <basic-action-gadget>, <table-item>)
  sealed slot %icon = #f,
    init-keyword: icon:;
end class <table-item-pane>;
||#

(defclass <table-item-pane>
    (<row-layout> <labelled-gadget-mixin> <basic-action-gadget> <table-item>)
  ((%icon :initform nil :initarg :icon :accessor %icon)))


;;; Moved from lower in the file to fix compile error
#||
define sealed class <table-item-label-button>
    (<homegrown-control-button-mixin>, <push-button>, <simple-pane>)
end class <table-item-label-button>;
||#
(defclass <table-item-label-button>
    (<homegrown-control-button-mixin> <push-button> <simple-pane>)
  ())


#||
define sealed domain make (singleton(<table-item-pane>));
define sealed domain initialize (<table-item-pane>);

define sealed method initialize
    (item :: <table-item-pane>,
     #key label, small-icon, large-icon, frame-manager: framem)
  ignore(large-icon);
  next-method();
  with-frame-manager (framem)
    when (small-icon)
      add-child(item, make(<label>, label: small-icon))
    end;
    when (label)
      add-child(item, make(<table-item-label-button>, label: label))
    end
  end
end method initialize;
||#

(defmethod initialize-instance :after ((item <table-item-pane>)
				       &key label small-icon large-icon ((:frame-manager framem))
				       &allow-other-keys)
  (declare (ignore large-icon))
  (with-frame-manager (framem)
    (when small-icon
      (add-child item (make-pane '<label> :label small-icon)))
    (when label
      (add-child item (make-pane '<table-item-label-button> :label label)))))


#||
define method do-make-item 
    (pane :: <table-control-pane>, class == <table-item>,
     #rest initargs, #key object, frame-manager: framem = frame-manager(pane))
 => (item :: <table-item-pane>)
  let label-function = gadget-label-key(pane);
  let icon-function  = table-control-icon-function(pane);
  let label = (label-function & label-function(object)) | "";
  let (small-icon, large-icon) = (icon-function  & icon-function(object));
  apply(make, <table-item-pane>,
	label: label,
	small-icon: small-icon,
	large-icon: large-icon,
	x-spacing: 4, y-alignment: #"center",
	frame-manager: framem,
	initargs)
end method do-make-item;
||#

(defmethod do-make-item ((pane <table-control-pane>) (class (eql (find-class '<table-item>)))
			 &rest initargs &key object ((:frame-manager framem) (frame-manager pane)))
  (let* ((label-function (gadget-label-key pane))
	 (icon-function  (table-control-icon-function pane))
	 (label          (or (and label-function (funcall label-function object)) "")))
    (multiple-value-bind (small-icon large-icon)
	(and icon-function (funcall icon-function object))
      (apply #'make-pane '<table-item-pane>
	     :label label
	     :small-icon small-icon
	     :large-icon large-icon
	     :x-spacing 4 :y-alignment :center
	     :frame-manager framem
	     initargs))))


#||
define sealed method do-find-item
    (pane :: <table-control-pane>, object, #key)
 => (node :: false-or(<table-item-pane>))
  let key  = gadget-value-key(pane);
  let test = gadget-test(pane);
  let the-key = key(object);
  block (return)
    for (item in sheet-children(pane))
      when (test(key(item-object(item)), the-key))
	return(item)
      end
    end;
    #f
  end
end method do-find-item;
||#

(defmethod do-find-item ((pane <table-control-pane>) object &key)
  (let* ((key     (gadget-value-key pane))
	 (test    (gadget-test pane))
	 (the-key (funcall key object)))
    (map nil #'(lambda (item)
		 (when (funcall test (funcall key (item-object item)) the-key)
		   (return-from do-find-item item)))
	 (sheet-children pane))
    nil))


#||
define sealed method do-add-item
    (pane :: <table-control-pane>, item :: <table-item-pane>, #key after) => ()
  let layout = pane.%layout-pane;
  let index = after & position(sheet-children(layout), after);
  add-child(layout, item, index: index | #"end");
  sheet-mapped?(item, do-repaint?: #f) := sheet-mapped?(pane);
  layout-homegrown-control(pane)
end method do-add-item;
||#

(defmethod do-add-item ((pane <table-control-pane>) (item <table-item-pane>) &key after)
  (let* ((layout (%layout-pane pane))
	 (index (and after (position (sheet-children layout) after))))
    (add-child layout item :index (or index :end))
    (setf (sheet-mapped? item :do-repaint? nil) (sheet-mapped? pane))
    (layout-homegrown-control pane)))


#||
define sealed method do-remove-item
    (pane :: <table-control-pane>, item :: <table-item-pane>) => ()
  let layout = pane.%layout-pane;
  remove-child(layout, item);
  layout-homegrown-control(pane)
end method do-remove-item;
||#

(defmethod do-remove-item ((pane <table-control-pane>) (item <table-item-pane>))
  (let ((layout (%layout-pane pane)))
    (remove-child layout item)
    (layout-homegrown-control pane)))


#||
define sealed method do-add-column
    (pane :: <table-control-pane>, column :: <table-column>, index :: <integer>) => ()
  //---*** Do this, fixing up all existing items
end method do-add-column;
||#

(defmethod do-add-column ((pane <table-control-pane>) (column <table-column>) (index integer))
  ;;---*** Do this, fixing up all existing items
  )


#||
define sealed method do-remove-column
    (pane :: <table-control-pane>, index :: <integer>) => ()
  //---*** Do this, fixing up all existing items
end method do-remove-column;
||#

(defmethod do-remove-column ((pane <table-control-pane>) (index integer))
  ;;---*** Do this, fixing up all existing items
  )


#||
define sealed method item-label
    (item :: <table-item-pane>) => (label :: false-or(<string>))
  gadget-label(item)
end method item-label;
||#

(defmethod item-label ((item <table-item-pane>))
  (gadget-label item))


#||
define sealed method item-label-setter
    (label :: false-or(<string>), item :: <table-item-pane>) => (label :: false-or(<string>))
  gadget-label(item) := label;
  block (break)
    for (child in sheet-children(item))
      when (instance?(child, <table-item-label-button>))
	gadget-label(child) := label;
	clear-box*(item, sheet-region(item));
	repaint-sheet(item, $everywhere);
	break()
      end
    end
  end;
  label
end method item-label-setter;
||#

(defmethod (setf item-label) ((label null) (item <table-item-pane>))
  (setf (gadget-label item) label)
  (block break
    (map nil #'(lambda (child)
		 (when (typep child '<table-item-label-button>)
		   (setf (gadget-label child) label)
		   (clear-box* item (sheet-region item))
		   (repaint-sheet item *everywhere*)
		   (return-from break)))
	 (sheet-children item)))
  label)

(defmethod (setf item-label) ((label string) (item <table-item-pane>))
  (setf (gadget-label item) label)
  (block break
    (map nil #'(lambda (child)
		 (when (typep child '<table-item-label-button>)
		   (setf (gadget-label child) label)
		   (clear-box* item (sheet-region item))
		   (repaint-sheet item *everywhere*)
		   (return-from break)))
	 (sheet-children item)))
  label)


#||
define sealed method item-icon
    (item :: <table-item-pane>) => (icon :: false-or(<image>))
  item.%icon
end method item-icon;
||#

(defmethod item-icon ((item <table-item-pane>))
  (%icon item))


#||
define sealed method item-icon-setter
    (icon :: false-or(<image>), item :: <table-item-pane>) => (icon :: false-or(<image>))
  item.%icon := icon;
  block (break)
    for (child in sheet-children(item))
      when (instance?(child, <label>))
	gadget-label(child) := icon;
	clear-box*(item, sheet-region(item));
	repaint-sheet(item, $everywhere);
	break()
      end
    end
  end;
  icon
end method item-icon-setter;
||#

(defmethod (setf item-icon) ((icon null) (item <table-item-pane>))
  (setf (%icon item) icon)
  (block break
    (map nil #'(lambda (child)
		 (when (typep child '<label>)
		   (setf (gadget-label child) icon)
		   (clear-box* item (sheet-region item))
		   (repaint-sheet item *everywhere*)
		   (return-from break)))
	 (sheet-children item)))
  icon)

(defmethod (setf item-icon) ((icon <image>) (item <table-item-pane>))
  (setf (%icon item) icon)
  (block break
    (map nil #'(lambda (child)
		 (when (typep child '<label>)
		   (setf (gadget-label child) icon)
		   (clear-box* item (sheet-region item))
		   (repaint-sheet item *everywhere*)
		   (return-from break)))
	 (sheet-children item)))
  icon)


#||
define sealed class <table-item-label-button>
    (<homegrown-control-button-mixin>, <push-button>, <simple-pane>)
end class <table-item-label-button>;

---*** Moved earlier in file...

define sealed domain make (singleton(<table-item-label-button>));
define sealed domain initialize (<table-item-label-button>);


/// Mapping items <-> indices

define sealed method item-to-index
    (table :: <table-control-pane>, item)
 => (index :: <integer>)
  let layout = table.%layout-pane;
  let index  = position(sheet-children(layout), item);
  let n-columns :: <integer> = size(table-control-columns(table));
  if (index)
    floor/(index, n-columns) - 1
  else
    error("Failed to find item %= in %=", item, table)
  end
end method item-to-index;
||#

(defmethod item-to-index ((table <table-control-pane>) item)
  (let* ((layout (%layout-pane table))
	 (index (position item (sheet-children layout)))
	 (n-columns (length (table-control-columns table))))
    (if index
	(- (floor index n-columns) 1)
	(error "Failed to find item ~a in ~a" item table))))


#||
define sealed method index-to-item
    (table :: <table-control-pane>, index :: <integer>)
 => (item)
  let layout = table.%layout-pane;
  let n-columns :: <integer> = size(table-control-columns(table));
  sheet-children(layout)[index * n-columns + n-columns]
end method index-to-item;
||#

(defmethod index-to-item ((table <table-control-pane>) (index integer))
  (let ((layout (%layout-pane table))
	(n-columns (length (table-control-columns table))))
    (SEQUENCE-ELT (sheet-children layout) (+ (* index n-columns) n-columns))))


#||
define sealed method item-selected?
    (table :: <table-control-pane>, item)
 => (selected? :: <boolean>)
  block (return)
    let client   = table.%layout-pane;
    let children = sheet-children(client);
    let n-columns :: <integer> = size(table-control-columns(table));
    for (index :: <integer> in gadget-selection(table))
      let child = children[index * n-columns + n-columns];
      when (child == item)
        return(#t)
      end
    end;
    #f
  end
end method item-selected?;
||#

(defmethod item-selected? ((table <table-control-pane>) item)
  (let* ((client (%layout-pane table))
	 (children (sheet-children client))
	 (n-columns (length (table-control-columns table))))
    (map nil #'(lambda (index)
		 (let ((child (SEQUENCE-ELT children (+ (* index n-columns) n-columns))))
		   (when (eql child item)
		     (return-from item-selected? t))))
	 (gadget-selection table))
    nil))
