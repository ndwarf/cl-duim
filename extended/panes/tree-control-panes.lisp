;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GADGET-PANES-INTERNALS -*-
(in-package #:duim-gadget-panes-internals)

#||
/// Generic implementation of tree control panes

define sealed class <tree-control-pane>
    (<standard-input-mixin>,
     <standard-repainting-mixin>,
     <permanent-medium-mixin>,
     <homegrown-tree-control-mixin>,
     <tree-control>,
     <single-child-wrapping-pane>)
end class <tree-control-pane>;
||#

(defclass <tree-control-pane>
    (<standard-input-mixin>
     <standard-repainting-mixin>
     <permanent-medium-mixin>
     <homegrown-tree-control-mixin>
     <tree-control>
     <single-child-wrapping-pane>)
  ())

(defgeneric gadget-selected-nodes (tree))
(defgeneric compute-gadget-selection (tree nodes))


#||
define sideways method class-for-make-pane 
    (framem :: <frame-manager>, class == <tree-control>, #key)
 => (class :: <class>, options :: false-or(<sequence>))
  values(<tree-control-pane>, #f)
end method class-for-make-pane;
||#

(defmethod class-for-make-pane ((framem <frame-manager>) (class (eql (find-class '<tree-control>))) &key)
  (values (find-class '<tree-control-pane>) nil))


#||
define sealed domain make (singleton(<tree-control-pane>));
define sealed domain initialize (<tree-control-pane>);

define sealed method initialize
    (tree :: <tree-control-pane>,
     #key frame-manager: framem)
  next-method();
  with-frame-manager (framem)
    let layout = make(<tree-control-layout>);
    tree.%layout-pane    := layout;
    layout.%control-pane := tree;
    let scroll-bars = gadget-scroll-bars(tree);
    sheet-child(tree) 
      := if (scroll-bars == #"none")
	   layout
	 else
	   scrolling (scroll-bars: scroll-bars, border-type: #f)
	     layout
	    end
         end
  end
end method initialize;
||#

(defmethod initialize-instance :after ((tree <tree-control-pane>) &key ((:frame-manager framem))
				       &allow-other-keys)
  (with-frame-manager (framem)
    (let ((layout (make-pane '<tree-control-layout>)))
      (setf (%layout-pane tree) layout)
      (setf (%control-pane layout) tree)
      (let ((scroll-bars (gadget-scroll-bars tree)))
	(setf (sheet-child tree)
	      (if (eql scroll-bars :none)
		  layout
		  (scrolling (:scroll-bars scroll-bars :border-type nil)
		    layout)))))))



#||

/// Tree control layout

// A streamlined version of column-layouts, with drawing capabilities
define sealed class <tree-control-layout>
    (<homegrown-control-layout-mixin>, <column-layout>)
end class <tree-control-layout>;
||#

(defclass <tree-control-layout>
    (<homegrown-control-layout-mixin> <column-layout>)
  ())


(defconstant +tree-control-generation-indentation+ 16)
(defconstant +tree-control-indentation-offset+      4)


#||
define sealed domain make (singleton(<tree-control-layout>));
define sealed domain initialize (<tree-control-layout>);

// Like 'box-pane-allocate-space', but much more streamlined
define method do-allocate-space
    (layout :: <tree-control-layout>, width :: <integer>, height :: <integer>) => ()
  let nodes = sheet-children(layout);
  let indentation = $tree-control-generation-indentation;
  let offset = $tree-control-indentation-offset;
  let y-spacing  :: <integer> = layout-y-spacing(layout);
  let y-position :: <integer> = 0;
  for (node :: <tree-node-pane> in nodes)
    unless (sheet-withdrawn?(node))
      let x-position = indentation * node-generation(node) + offset;
      let space-req = compose-space(node);
      let (w, w-, w+, h, h-, h+) = space-requirement-components(node, space-req);
      ignore(w-, w+, h-, h+);
      let width  :: <integer> = w;
      let height :: <integer> = h;
      set-sheet-edges
	(node, x-position, y-position, x-position + width, y-position + height);
      inc!(y-position, height + y-spacing)
    end
  end
end method do-allocate-space;
||#

(defmethod do-allocate-space ((layout <tree-control-layout>) (width integer) (height integer))
  (let ((nodes (sheet-children layout))
	(indentation +tree-control-generation-indentation+)
	(offset +tree-control-indentation-offset+)
	(y-spacing (layout-y-spacing layout))
	(y-position 0))
    (loop for node across nodes
       do (unless (sheet-withdrawn? node)
	    (let ((x-position (+ (* indentation (node-generation node)) offset))
		  (space-req (compose-space node)))
	      (multiple-value-bind (w w- w+ h h- h+)
		  (space-requirement-components node space-req)
		(declare (ignore w- w+ h- h+))
		(let ((width w)
		      (height h))
		  (set-sheet-edges node x-position y-position (+ x-position width) (+ y-position height))
		  (incf y-position (+ height y-spacing)))))))))


#||
define method handle-repaint
    (layout :: <tree-control-layout>, medium :: <medium>, region :: <region>) => ()
  let tree :: <tree-control-pane> = layout.%control-pane;
  when (tree-control-show-edges?(tree))
   with-drawing-options (medium, brush: $tree-control-gray)
      //--- The fudge offsets here are dubious...
      local method draw-edges (node :: <tree-node-pane>) => ()
	      unless (sheet-withdrawn?(node))
		let (from-x, from-y) = sheet-position(node);
		inc!(from-x, 4);
		inc!(from-y, box-height(sheet-region(node)) - 2);
		for (child :: <tree-node-pane> in node-children(node))
		  unless (sheet-withdrawn?(child))
		    let (to-x, to-y) = sheet-position(child);
		    dec!(to-x, 4);
		    inc!(to-y, truncate/(box-height(sheet-region(child)), 2));
		    draw-line(medium, from-x, from-y, from-x, to-y);
		    draw-line(medium, from-x, to-y, to-x, to-y);
		    unless (empty?(node-children(child)))
		      draw-edges(child)
		    end
		  end
		end
	      end
	    end method;
      for (node in tree-control-root-nodes(tree))
	draw-edges(node)
      end
    end
  end
end method handle-repaint;
||#

(defmethod handle-repaint ((layout <tree-control-layout>) (medium <medium>) (region <region>))
  ;; WE DON'T SEE THIS... WHY? IS SOMETHING EATING IT? I THINK THE VIEWPORT CATCHES IT
  ;; AND DOESN'T PASS IT ON MAYBE
  (warn "repainting tree layout")
  (let ((tree (%control-pane layout)))
    (when (tree-control-show-edges? tree)
      ;; FIXME: WE SHOULD DRAW IN SOME "LOWLIGHT" COLOUR IF THE CONTROL ISN'T ENABLED.
      (with-drawing-options (medium :brush *tree-control-gray*)
        ;;--- The fudge offsets here are dubious...
	(labels ((draw-edges (node)    ;; (node <tree-node-pane>))
		   (unless (sheet-withdrawn? node)
		     (multiple-value-bind (from-x from-y)
			 (sheet-position node)
		       (incf from-x 4)
		       (incf from-y (- (box-height (sheet-region node)) 2))
		       (map nil #'(lambda (child)
				    (unless (sheet-withdrawn? child)
				      (multiple-value-bind (to-x to-y)
					  (sheet-position child)
					(decf to-x 4)
					(incf to-y (truncate (box-height (sheet-region child)) 2))
					(draw-line medium from-x from-y from-x to-y)
					(draw-line medium from-x to-y to-x to-y)
					(unless (empty? (node-children child))
					  (draw-edges child)))))
			    (node-children node))))))
	  (map nil #'(lambda (node)
		       (draw-edges node))
	       (tree-control-root-nodes tree)))))))



#||

/// Roots and items

// Build the items for the first time when the sheet is fully attached
define method note-sheet-attached
    (tree :: <tree-control-pane>) => ()
  next-method();
  initialize-tree-control-icons(port(tree), tree);
  //--- Too bad we've lost the 'value:' initarg by the time we get here
  note-tree-control-roots-changed(tree)
end method note-sheet-attached;
||#

(defmethod note-sheet-attached ((tree <tree-control-pane>))
  (call-next-method)
  (initialize-tree-control-icons (port tree) tree)
  ;;--- Too bad we've lost the ':value' initarg by the time we get here
  (note-tree-control-roots-changed tree))


#||
define sealed method note-tree-control-roots-changed
    (tree :: <tree-control-pane>, #key value = $unsupplied) => ()
  with-busy-cursor (tree)
    next-method();
    let roots  = tree-control-roots(tree);
    let layout = tree.%layout-pane;
    let state  = sheet-state(tree);
    when (state == #"mapped")
      clear-box*(sheet-viewport(layout) | layout, sheet-viewport-region(layout))
    end;
    gadget-selection(tree) := #[];
    sheet-children(layout) := #[];
    tree.%visible-items := #f;
    let generator :: <function> = tree-control-children-generator(tree);
    let predicate :: <function> = tree-control-children-predicate(tree);
    local method add-one (node, object, depth) => ()
	    let child-node = make-node(tree, object);
	    add-node(tree, node, child-node, setting-roots?: #t);
	    sheet-withdrawn?(child-node, do-repaint?: #f) := #f;
	    when (depth > 0 & predicate(object))
	      for (child in generator(object))
		add-one(child-node, child, depth - 1)
	      end;
	      node-state(child-node) := #"expanded"
	    end
	  end method;
    delaying-layout (tree)
      for (root in roots)
	add-one(tree, root, tree-control-initial-depth(tree))
      end;
      let items = gadget-items(tree);
      // Try to preserve the old value and selection
      select (gadget-selection-mode(tree))
	#"single" =>
	  unless (empty?(items))
	    let index = supplied?(value) & position(items, value);
	    if (index)
	      gadget-selection(tree) := vector(index)
	    else
	      gadget-selection(tree) := #[0]
	    end
	  end;
	#"multiple" =>
	  let selection :: <stretchy-object-vector> = make(<stretchy-vector>);
	  when (supplied?(value))
	    for (v in value)
	      let index = position(items, v);
	      when (index)
		add!(selection, index)
	      end
	    end
	  end;
	  unless (empty?(selection))
	    gadget-selection(tree) := selection
	  end;
	otherwise =>
	  #f;
      end;
    end
  end
end method note-tree-control-roots-changed;
||#

(defmethod note-tree-control-roots-changed ((tree <tree-control-pane>) &key (value :unsupplied))
  (warn "note-tree-control-roots-changed")
  (with-busy-cursor (tree)
    (call-next-method)
    (let ((roots (tree-control-roots tree))
	  (layout (%layout-pane tree))
	  (state (sheet-state tree)))
      (when (eql state :mapped)
	(clear-box* (or (sheet-viewport layout) layout) (sheet-viewport-region layout)))
      (setf (gadget-selection tree) (MAKE-STRETCHY-VECTOR))
      (setf (sheet-children layout) (MAKE-STRETCHY-VECTOR))
      (setf (%visible-items tree) nil)
      (let ((generator (tree-control-children-generator tree))
	    (predicate (tree-control-children-predicate tree)))
	(labels ((add-one (node object depth)
		   (let ((child-node (make-node tree object)))
		     (add-node tree node child-node :setting-roots? t)
		     (setf (sheet-withdrawn? child-node :do-repaint? nil) nil)
		     (when (and (> depth 0) (funcall predicate object))
		       (map nil #'(lambda (child)
				    (add-one child-node child (- depth 1)))
			    (funcall generator object))
		       (setf (node-state child-node) :expanded)))))
	  (delaying-layout (tree)
	    (map nil #'(lambda (root)
			 (add-one tree root (tree-control-initial-depth tree)))
		 roots)
	    (let ((items (gadget-items tree)))
	      ;; Try to preserve the old value and selection
	      (case (gadget-selection-mode tree)
		(:single
		 (unless (empty? items)
		   (let ((index (and (supplied? value) (position value items))))
		     (duim-debug-message "PANES;TREE-CONTROL-PANES:FIXME?")
		     (if index
			 (setf (gadget-selection tree) (vector index))
			 (setf (gadget-selection tree)
			       (make-array 1 :adjustable t :fill-pointer t :initial-element 0))))))
		(:multiple
		 (let ((selection (MAKE-STRETCHY-VECTOR)))
		   (when (supplied? value)
		     (map nil #'(lambda (v)
				  (let ((index (position v items)))
				    (when index
				      (ADD! selection index))))
			  value))
		   (unless (empty? selection)
		     (setf (gadget-selection tree) selection))))
		(t nil)))))))))



#||

/// Generic implementation of tree node panes

define sealed class <tree-node-pane>
    (<tree-node-pane-mixin>, <tree-node>)
end class <tree-node-pane>;
||#

(defclass <tree-node-pane>
    (<tree-node-pane-mixin> <tree-node>)
  ())


#||
define sealed domain make (singleton(<tree-node-pane>));
define sealed domain initialize (<tree-node-pane>);

define constant $tree-control-generation-indentation :: <integer> = 16;
define constant $tree-control-indentation-offset     :: <integer> =  4;
||#

;;; Move to higher up...
;;;(defconstant $tree-control-generation-indentation 16)
;;;(defconstant $tree-control-indentation-offset      4)

#||
// Add generation spacing to each row to account for the indentation
define method do-compose-space
    (node :: <tree-node-pane>, #key width, height)
 => (space-req :: <space-requirement>)
  ignore(width, height);
  let space-req   = next-method();
  let indentation = $tree-control-generation-indentation;
  let offset      = $tree-control-indentation-offset;
  let extra       = indentation * node-generation(node) + offset;
  space-requirement+(node, space-req,
		     width: extra, min-width: extra, max-width: extra)
end method do-compose-space;
||#

(defmethod do-compose-space ((node <tree-node-pane>) &key width height)
  (declare (ignore width height))
  (let* ((space-req   (call-next-method))
	 (indentation +tree-control-generation-indentation+)
	 (offset      +tree-control-indentation-offset+)
	 (extra       (+ (* indentation (node-generation node)) offset)))
    (space-requirement+ node space-req
			:width extra :min-width extra :max-width extra)))


#||
define method do-make-node 
    (tree :: <tree-control-pane>, class == <tree-node>,
     #rest initargs, #key object)
 => (item :: <tree-node-pane>)
  let framem = frame-manager(tree);
  let label-function = gadget-label-key(tree);
  let icon-function  = tree-control-icon-function(tree);
  let label = (label-function & label-function(object)) | "";
  let (icon, selected-icon)
    = if (icon-function) icon-function(object) else values(#f, #f) end;
  apply(make, <tree-node-pane>,
	tree:  tree,
	label: label,
	icon:  icon,
	selected-icon: selected-icon,
	x-spacing: 4, y-alignment: #"center",
	frame-manager: framem,
	initargs)
end method do-make-node;
||#

(defmethod do-make-node ((tree <tree-control-pane>) (class (eql (find-class '<tree-node>)))
			 &rest initargs &key object)
  (let* ((framem (frame-manager tree))
	 (label-function (gadget-label-key tree))
	 (icon-function (tree-control-icon-function tree))
	 (label (or (and label-function (funcall label-function object)) "")))
    (multiple-value-bind (icon selected-icon)
	(if icon-function (funcall icon-function object) (values nil nil))
      (apply #'make-pane '<tree-node-pane>
	     :tree tree
	     :label label
	     :icon icon
	     :selected-icon selected-icon
	     :x-spacing 4 :y-alignment :center
	     :frame-manager framem
	     initargs))))


#||
define sealed method do-find-node
    (tree :: <tree-control-pane>, object, #key node: parent-node)
 => (node :: false-or(<tree-node-pane>))
  let key  = gadget-value-key(tree);
  let test = gadget-test(tree);
  let the-key = key(object);
  block (return)
    for (node in sheet-children(tree.%layout-pane))
      when (test(key(node-object(node)), the-key))
	// Is it a child of the requested node?
	when (~parent-node | member?(node, node-children(parent-node)))
	  return(node)
	end
      end
    end;
    #f
  end
end method do-find-node;
||#

(defmethod do-find-node ((tree <tree-control-pane>) object &key ((node parent-node)))
  (let* ((key (gadget-value-key tree))
	 (test (gadget-test tree))
	 (the-key (funcall key object)))
    (map nil #'(lambda (node)
		 (when (funcall test (funcall key (node-object node)) the-key)
		   ;; Is it a child of the requested node?
		   (when (or (not parent-node) (find node (node-children parent-node)))
		     (return-from do-find-node node))))
	 (sheet-children (%layout-pane tree))))
  nil)


#||
// Note that we don't relayout the tree -- that only happens
// in 'expand-node' and 'contract-node'
define sealed method do-add-node
    (tree :: <tree-control-pane>, parent, node :: <tree-node-pane>, #key after) => ()
  tree.%visible-items := #f;
  unless (after)
    // The idea here is to ensure that the new node comes after
    // the last node (and all of that node's descendents) in its
    // own generation
    local method last-child (node) => (child)
	    if (empty?(node-children(node)))
	      node
	    else
	      last-child(last(node-children(node)))
	    end
	  end method;
    unless (empty?(node-parents(node)))
      after := last-child(node-parents(node)[0])
    end
  end;
  let layout = tree.%layout-pane;
  let index = after & position(sheet-children(layout), after);
  add-child(layout, node, index: if (index) index + 1 else #"end" end);
  sheet-withdrawn?(node, do-repaint?: #f) := #t
end method do-add-node;
||#

;; Note that we don't relayout the tree -- that only happens
;; in 'expand-node' and 'contract-node'
(defmethod do-add-node ((tree <tree-control-pane>) parent (node <tree-node-pane>) &key after)
  (declare (ignore parent))
  (setf (%visible-items tree) nil)
  (unless after
    ;; The idea here is to ensure that the new node comes after
    ;; the last node (and all of that node's descendents) in its
    ;; own generation
    (labels ((last-child (node)
	       (warn "GOT NODE ~a WITH CHILDREN ~a" node (node-children node))
               (if (empty? (node-children node))
		   node
		   ;; XXX:Are node-children ever anything other than vectors?
		   (last-child (alexandria:last-elt (node-children node))))))
      (unless (empty? (node-parents node))
	(setf after (last-child (SEQUENCE-ELT (node-parents node) 0))))))
  (let* ((layout (%layout-pane tree))
	 (index  (and after (position after (sheet-children layout)))))
    (add-child layout node :index (if index (+ index 1) :end))
    (setf (sheet-withdrawn? node :do-repaint? nil) t)))


#||
define sealed method do-add-nodes
    (tree :: <tree-control-pane>, parent, nodes :: <sequence>, #key after) => ()
  let selected-nodes = gadget-selected-nodes(tree);
  tree.%visible-items := #f;
  gadget-selection(tree) := #[];
  for (node in nodes)
    add-node(tree, parent, node, after: after)
  end;
  gadget-selection(tree) := compute-gadget-selection(tree, selected-nodes)
end method do-add-nodes;
||#

(defmethod do-add-nodes ((tree <tree-control-pane>) parent (nodes sequence) &key after)
  (let ((selected-nodes (gadget-selected-nodes tree)))
    (setf (%visible-items tree) nil)
    (setf (gadget-selection tree) (MAKE-STRETCHY-VECTOR))
    (map nil #'(lambda (node)
		 (add-node tree parent node :after after))
	 nodes)
    (setf (gadget-selection tree)
	  (compute-gadget-selection tree selected-nodes))))


#||
// Note that we don't relayout the tree -- that only happens
// in 'expand-node' and 'contract-node'
define sealed method do-remove-node
    (tree :: <tree-control-pane>, node :: <tree-node-pane>) => ()
  tree.%visible-items := #f;
  remove-child(tree.%layout-pane, node)
end method do-remove-node;
||#

(defmethod do-remove-node ((tree <tree-control-pane>) (node <tree-node-pane>))
  (setf (%visible-items tree) nil)
  (remove-child (%layout-pane tree) node))


#||
define sealed method node-children-setter
    (children :: <sequence>, node :: <tree-node-pane>)
 => (children :: <sequence>)
  let tree = control-for-item(node);
  let old-children = node-children(node);
  let selected-nodes = gadget-selected-nodes(tree);
  tree.%visible-items := #f;
  gadget-selection(tree) := #[];
  next-method();
  delaying-layout (tree)
    for (n in old-children)
      remove-child(tree.%layout-pane, n)
    end;
    for (n in children)
      add-node(tree, node, n)
    end;
    gadget-selection(tree) := compute-gadget-selection(tree, selected-nodes)
  end;
  children
end method node-children-setter;
||#

(defmethod (setf node-children) ((children sequence) (node <tree-node-pane>))
  (let* ((tree (control-for-item node))
	 (old-children (node-children node))
	 (selected-nodes (gadget-selected-nodes tree)))
    (setf (%visible-items tree) nil)
    ;; XXX: #[] defines a <SIMPLE-VECTOR> (i.e. not stretchy)
    (setf (gadget-selection tree) (MAKE-SIMPLE-VECTOR))
    (call-next-method)
    (delaying-layout (tree)
      (loop for n across old-children
	 do (remove-child (%layout-pane tree) n))
      (loop for n across children
	 do (add-node tree node n))
      (setf (gadget-selection tree) (compute-gadget-selection tree selected-nodes))))
  children)


#||
define sealed method gadget-selected-nodes
    (tree :: <tree-control-pane>) => (nodes :: <sequence>)
  let nodes :: <stretchy-object-vector> = make(<stretchy-vector>);
  let selection = gadget-selection(tree);
  let index :: <integer> = -1;
  for (node :: <tree-node-pane> in sheet-children(tree.%layout-pane))
    unless (sheet-withdrawn?(node))
      inc!(index);
      when (member?(index, selection))
	add!(nodes, node)
      end
    end
  end;
  nodes
end method gadget-selected-nodes;
||#

(defmethod gadget-selected-nodes ((tree <tree-control-pane>))
  (let ((nodes (MAKE-STRETCHY-VECTOR))
	(selection (gadget-selection tree))
	(index -1))
    (map nil #'(lambda (node)
		 (unless (sheet-withdrawn? node)
		   (incf index)
		   (when (MEMBER? index selection)
		     (ADD! nodes node))))
	 (sheet-children (%layout-pane tree)))
    nodes))


#||
//--- Not correct if the same object appears twice in the tree
define sealed method compute-gadget-selection 
    (tree :: <tree-control-pane>, nodes) => (selection :: <sequence>)
  let new-selection :: <stretchy-object-vector> = make(<stretchy-vector>);
  let items = gadget-items(tree);
  for (node :: <tree-node-pane> in nodes)
    let index = position(items, node-object(node));
    when (index)
      add!(new-selection, index)
    end
  end;
  new-selection
end method compute-gadget-selection;
||#

;;--- Not correct if the same object appears twice in the tree
(defmethod compute-gadget-selection ((tree <tree-control-pane>) nodes)
  (let ((new-selection (MAKE-STRETCHY-VECTOR))
	(items (gadget-items tree)))
    (map nil #'(lambda (node)
		 (let ((index (position (node-object node) items)))
		   (when index
		     (ADD! new-selection index))))
	 nodes)
    new-selection))


#||
define sealed method do-expand-node
    (tree :: <tree-control-pane>, node :: <tree-node-pane>) => ()
  tree.%visible-items := #f;
  delaying-layout (tree)
    let mapped? = sheet-mapped?(node);
    local method unwithdraw (node :: <tree-node-pane>)
	    // Only map in the first level
	    sheet-withdrawn?(node, do-repaint?: #f) := #f;
	    sheet-mapped?(node, do-repaint?: #f) := mapped?;
	  end method;
    do(unwithdraw, node-children(node));
  end
end method do-expand-node;
||#

(defmethod do-expand-node ((tree <tree-control-pane>) (node <tree-node-pane>))
  (warn "do-expand-node")
  (setf (%visible-items tree) nil)
  (delaying-layout (tree)
    (let ((mapped? (sheet-mapped? node)))
      (labels ((unwithdraw (node)    ;; (node <tree-node-pane>))
                 ;; Only map in the first level
                 (setf (sheet-withdrawn? node :do-repaint? nil) nil)
		 (setf (sheet-mapped? node :do-repaint? nil) mapped?)))
	(map nil #'unwithdraw (node-children node))))))


#||
define sealed method do-contract-node
    (tree :: <tree-control-pane>, node :: <tree-node-pane>) => ()
  tree.%visible-items := #f;
  delaying-layout (tree)
    local method withdraw (node :: <tree-node-pane>)
	    sheet-withdrawn?(node, do-repaint?: #f) := #t;
	    when (node-state(node) == #"expanded")
	      node-state(node) := #"contracted"
	    end;
	    // Withdraw all the way to the bottom
	    do(withdraw, node-children(node))
	  end method;
    do(withdraw, node-children(node))
  end
end method do-contract-node;
||#

(defmethod do-contract-node ((tree <tree-control-pane>) (node <tree-node-pane>))
  (warn "do-contract-node")
  (setf (%visible-items tree) nil)
  (delaying-layout (tree)
    (labels ((withdraw (node)    ;; (node <tree-node-pane>))
               (setf (sheet-withdrawn? node :do-repaint? nil) t)
	       (when (eql (node-state node) :expanded)
		 (setf (node-state node) :contracted))
	       ;; Withdraw all the way to the bottom
	       (map nil #'withdraw (node-children node))))
      (map nil #'withdraw (node-children node)))))


#||
define sealed method node-label
    (node :: <tree-node-pane>) => (label :: false-or(<string>))
  gadget-label(node)
end method node-label;
||#

(defmethod node-label ((node <tree-node-pane>))
  (gadget-label node))


#||
define sealed method node-label-setter
    (label :: false-or(<string>), node :: <tree-node-pane>) => (label :: false-or(<string>))
  gadget-label(node) := label;
  block (break)
    for (child in sheet-children(node))
      when (instance?(child, <tree-node-label-button>))
	gadget-label(child) := label;
	clear-box*(node, sheet-region(node));
	repaint-sheet(node, $everywhere);
	break()
      end
    end
  end;
  label
end method node-label-setter;
||#

(defmethod (setf node-label) ((label null) (node <tree-node-pane>))
  (setf (gadget-label node) label)
  (block break
    (map nil #'(lambda (child)
		 (when (INSTANCE? child '<tree-node-label-button>)
		   (setf (gadget-label child) label)
		   (clear-box* node (sheet-region node))
		   (repaint-sheet node *everywhere*)
		   (return-from break)))
	 (sheet-children node)))
  label)

(defmethod (setf node-label) ((label string) (node <tree-node-pane>))
  (setf (gadget-label node) label)
  (block break
    (map nil #'(lambda (child)
		 (when (INSTANCE? child '<tree-node-label-button>)
		   (setf (gadget-label child) label)
		   (clear-box* node (sheet-region node))
		   (repaint-sheet node *everywhere*)
		   (return-from break)))
	 (sheet-children node)))
  label)


#||
define sealed method node-icon
    (node :: <tree-node-pane>) => (icon :: false-or(<image>))
  node.%icon
end method node-icon;
||#

(defmethod node-icon ((node <tree-node-pane>))
  (%icon node))


#||
define sealed method node-icon-setter
    (icon :: false-or(<image>), node :: <tree-node-pane>) => (icon :: false-or(<image>))
  node.%icon := icon;
  block (break)
    for (child in sheet-children(node))
      when (instance?(child, <label>))
	gadget-label(child) := icon;
	clear-box*(node, sheet-region(node));
	repaint-sheet(node, $everywhere);
	break()
      end
    end
  end;
  icon
end method node-icon-setter;
||#

(defmethod (setf node-icon) ((icon null) (node <tree-node-pane>))
  (setf (%icon node) icon)
  (block break
    (map nil #'(lambda (child)
		 ;; FIXME: INSTANCE? == TYPEP
		 (when (typep child '<label>)
		   (setf (gadget-label child) icon)
		   (clear-box* node (sheet-region node))
		   (repaint-sheet node *everywhere*)
		   (return-from break)))
	 (sheet-children node)))
  icon)

(defmethod (setf node-icon) ((icon <image>) (node <tree-node-pane>))
  (setf (%icon node) icon)
  (block break
    (map nil #'(lambda (child)
		 ;; FIXME: INSTANCE? == TYPEP
		 (when (typep child '<label>)
		   (setf (gadget-label child) icon)
		   (clear-box* node (sheet-region node))
		   (repaint-sheet node *everywhere*)
		   (return-from break)))
	 (sheet-children node)))
  icon)
