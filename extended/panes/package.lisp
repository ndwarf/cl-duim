;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

;;; This is where we implement the pane types specified by Dylan that might
;;; not exist in any given native windowing system. It's probably preferable
;;; to make use of native widgets where they're available.

(defpackage #:duim-gadget-panes
  (:use #:common-lisp
	#:duim-utilities
	#:duim-geometry
	#:duim-DCs
	#:duim-sheets
	#:duim-graphics
	#:duim-layouts
	#:duim-gadgets
	#:duim-frames)
  (:export

       ;; Border and label implementation classes

       #:<border-pane>
       #:<border-pane-metaclass>
       #:<group-box-pane>
       #:<group-box-pane-metaclass>
       #:<spacing-pane>
       #:<spacing-pane-metaclass>

       ;; Spin box panes

       #:<spin-box-pane>
       #:<spin-box-pane-metaclass>

       ;; Tab control panes

       #:<tab-control-pane>
       #:<tab-control-pane-metaclass>

       ;; List and table control panes

       #:<list-control-pane>
       #:<list-control-pane-metaclass>
       #:<list-item-pane>
       #:<list-item-pane-metaclass>
       #:<table-control-pane>
       #:<table-control-pane-metaclass>
       #:<table-item-pane>
       #:<table-item-pane-metaclass>

       ;; Tree control panes

       #:<tree-control-pane>
       #:<tree-control-pane-metaclass>
       #:<tree-node-pane>
       #:<tree-node-pane-metaclass>

       ;; Graph control panes

       #:<graph-control-pane>
       #:<graph-control-pane-metaclass>
       #:<tree-graph-pane>
       #:<tree-graph-pane-metaclass>
       #:<DAG-graph-pane>
       #:<DAG-graph-pane-metaclass>
       #:<graph-node-pane>
       #:<graph-node-pane-metaclass>
       #:<graph-edge-pane>
       #:<graph-edge-pane-metaclass>
       #:<line-graph-edge>
       #:<line-graph-edge-metaclass>
       #:<arrow-graph-edge>
       #:<arrow-graph-edge-metaclass>

       ;; Splitters

       #:<column-splitter-pane>
       #:<column-splitter-pane-metaclass>
       #:<row-splitter-pane>
       #:<row-splitter-pane-metaclass>

       ;; Carets

       #:<simple-caret>
       #:<simple-caret-metaclass>))


;;; Reloading DUIM causes:
;;; Error: Making package DUIM-GADGET-PANES-INTERNALS use package DUIM-LAYOUTS-INTERNALS causes a name conflict:
;;; Package DUIM-GADGET-PANES-INTERNALS already contains DUIM-GADGET-PANES-INTERNALS::STACK
(defpackage :duim-gadget-panes-internals

  (:use #:common-lisp
	#:duim-utilities
	#:duim-geometry-internals
	#:duim-DCs-internals
	#:duim-sheets-internals
	#:duim-graphics-internals
	#:duim-layouts-internals
	#:duim-gadgets-internals
	#:duim-frames-internals
	#:duim-gadget-panes)
  (:shadow #:stack)				;avoid compile error until we can fix packages & symbols
  (:export

       ;; Re-export all from DUIM-GADGET-PANES

       ;; Border and label implementation classes

       #:<border-pane>
       #:<border-pane-metaclass>
       #:<group-box-pane>
       #:<group-box-pane-metaclass>
       #:<spacing-pane>
       #:<spacing-pane-metaclass>

       ;; Spin box panes

       #:<spin-box-pane>
       #:<spin-box-pane-metaclass>

       ;; Tab control panes

       #:<tab-control-pane>
       #:<tab-control-pane-metaclass>

       ;; List and table control panes

       #:<list-control-pane>
       #:<list-control-pane-metaclass>
       #:<list-item-pane>
       #:<list-item-pane>-metaclass>
       #:<table-control-pane>
       #:<table-control-pane-metaclass>
       #:<table-item-pane>
       #:<table-item-pane-metaclass>

       ;; Tree control panes

       #:<tree-control-pane>
       #:<tree-control-pane-metaclass>
       #:<tree-node-pane>
       #:<tree-node-pane-metaclass>

       ;; Graph control panes

       #:<graph-control-pane>
       #:<graph-control-pane-metaclass>
       #:<tree-graph-pane>
       #:<tree-graph-pane-metaclass>
       #:<DAG-graph-pane>
       #:<DAG-graph-pane-metaclass>
       #:<graph-node-pane>
       #:<graph-node-pane-metaclass>
       #:<graph-edge-pane>
       #:<graph-edge-pane-metaclass>
       #:<line-graph-edge>
       #:<line-graph-edge-metaclass>
       #:<arrow-graph-edge>
       #:<arrow-graph-edge-metaclass>

       ;; Splitters

       #:<column-splitter-pane>
       #:<column-splitter-pane-metaclass>
       #:<row-splitter-pane>
       #:<row-splitter-pane-metaclass>

       ;; Carets

       #:<simple-caret>
       #:<simple-caret-metaclass>


       ;; ... and any iternal stuff we want exposed.

       ;; Spin box panes

       #:<arrow-button-pane>
       #:<arrow-button-pane-metaclass>

       ;; Border support

       #:<border-pane-mixin>
       #:<border-pane-mixin-metaclass>
       #:border-characteristics

       ;; Dialog support

       #:make-top-level-drawing-pane
       #:dialog-needs-separator?
       #:dialog-needs-title-pane?
       #:default-dialog-frame-wrapper
       #:update-default-dialog-layout
       #:default-dialog-border
       #:default-dialog-extra-size
       #:default-dialog-button-spacing
       #:default-dialog-button-x-alignment
       #:default-dialog-button-y-alignment
       #:default-dialog-spacing
       #:make-exit-box
       #:make-exit-buttons
       #:make-exit-button

       ;; Homegrown controls

       #:<homegrown-control-mixin>
       #:<homegrown-control-mixin-metaclass>
       #:<homegrown-control-button-mixin>
       #:<homegrown-control-button-mixin-metaclass>
       #:<homegrown-control-layout-mixin>
       #:<homegrown-control-layout-mixin-metaclass>

       ;; Tree control panes

       #:$tree-control-black
       #:$tree-control-gray
       #:$tree-expand-icon
       #:$tree-contract-icon
       #:<homegrown-tree-control-mixin>
       #:<homegrown-tree-control-mixin-metaclass>
       #:<tree-control-layout>
       #:<tree-control-layout-metaclass>
       #:<tree-node-control-button>
       #:<tree-node-control-button-metaclass>
       #:<tree-node-label-button>
       #:<tree-node-label-button-metaclass>
       #:<tree-node-pane-mixin>
       #:<tree-node-pane-mixin-metaclass>
       #:tree-control-expand-icon
;;       #:tree-control-expand-icon-setter
;;       #:(setf tree-control-expand-icon)
       #:tree-control-contract-icon
;;       #:tree-control-contract-icon-setter
;;       #:(setf tree-control-contract-icon)
       #:initialize-tree-control-icons

       ;; Graph control panes

       #:<graph-control-layout>
       #:<graph-control-layout-metaclass>
       #:<tree-graph-layout>
       #:<tree-graph-layout-metaclass>
       #:<DAG-graph-layout>
       #:<DAG-graph-layout-metaclass>))



;;; :Note: depending on the back end 'loaded' many of the pane types are unnecessary (i.e.
;;;        there are native implementations that should be used instead).


