;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GADGET-PANES-INTERNALS -*-
(in-package #:duim-gadget-panes-internals)

#||
/// Generic implementation of dialogs

define constant $ok-label     = "OK";
define constant $cancel-label = "Cancel";
define constant $help-label   = "Help";
define constant $apply-label  = "Apply";
define constant $back-label   = "< Back";
define constant $next-label   = "Next >";
define constant $finish-label = "Finish";
||#

;; FIXME: I18N?

(defvar *ok-label*     "OK")
(defvar *cancel-label* "Cancel")
(defvar *help-label*   "Help")
(defvar *apply-label*  "Apply")
(defvar *back-label*   "< Back")
(defvar *next-label*   "Next >")
(defvar *finish-label* "Finish")

#||
define protocol <<default-dialog-protocol>> ()
  function default-dialog-border
    (framem :: <frame-manager>, dialog :: <dialog-frame>)
 => (border :: <integer>);
  function default-dialog-extra-size
    (framem :: <frame-manager>, dialog :: <dialog-frame>)
 => (width :: <integer>, height :: <integer>);
  function default-dialog-spacing
    (framem :: <frame-manager>, dialog :: <dialog-frame>)
 => (spacing :: <integer>);
  function default-dialog-button-spacing
    (framem :: <frame-manager>, dialog :: <dialog-frame>)
 => (spacing :: <integer>);
  function default-dialog-button-x-alignment
    (framem :: <frame-manager>, dialog :: <dialog-frame>)
 => (alignment :: <x-alignment>);
  function default-dialog-button-y-alignment
    (framem :: <frame-manager>, dialog :: <dialog-frame>)
 => (alignment :: <y-alignment>);
  function dialog-needs-title-pane?
    (framem :: <frame-manager>, dialog :: <dialog-frame>)
 => (needs-title-pane? :: <boolean>);
  function dialog-needs-separator?
    (framem :: <frame-manager>, dialog :: <dialog-frame>)
 => (needs-separator? :: <boolean>);
  function make-top-level-drawing-pane
    (framem :: <frame-manager>, children :: <sequence>)
 => (sheet :: <sheet>);
  function make-exit-box
    (framem :: <frame-manager>, dialog :: <dialog-frame>)
 => (layout :: false-or(<layout-pane>));
  function make-exit-buttons
    (framem :: <frame-manager>, dialog :: <dialog-frame>)
 => (buttons :: <sequence>);
  function make-exit-button
    (framem :: <frame-manager>, dialog :: <dialog-frame>,
     callback :: false-or(<callback-type>), label :: <string>,
     #rest initargs, #key, #all-keys)
 => (button :: false-or(<push-button>));
end protocol <<default-dialog-protocol>>;
||#

(define-protocol <<default-dialog-protocol>> ()
  (:function default-dialog-border (framem dialog) (:documentation " Defined in: <<DEFAULT-DIALOG-PROTOCOL>> "))
  (:function default-dialog-extra-size (framem dialog) (:documentation " Defined in: <<DEFAULT-DIALOG-PROTOCOL>> "))
  (:function default-dialog-spacing (framem dialog) (:documentation " Defined in: <<DEFAULT-DIALOG-PROTOCOL>> "))
  (:function default-dialog-button-spacing (framem dialog) (:documentation " Defined in: <<DEFAULT-DIALOG-PROTOCOL>> "))
  (:function default-dialog-button-x-alignment (framem dialog) (:documentation " Defined in: <<DEFAULT-DIALOG-PROTOCOL>> "))
  (:function default-dialog-button-y-alignment (framem dialog) (:documentation " Defined in: <<DEFAULT-DIALOG-PROTOCOL>> "))
  (:function dialog-needs-title-pane? (framem dialog) (:documentation " Defined in: <<DEFAULT-DIALOG-PROTOCOL>> "))
  (:function dialog-needs-separator? (framem dialog) (:documentation " Defined in: <<DEFAULT-DIALOG-PROTOCOL>> "))
  (:function make-top-level-drawing-pane (framem children) (:documentation " Defined in: <<DEFAULT-DIALOG-PROTOCOL>> "))
  (:function make-exit-box (framem dialog) (:documentation " Defined in: <<DEFAULT-DIALOG-PROTOCOL>> "))
  (:function make-exit-buttons (framem dialog) (:documentation " Defined in: <<DEFAULT-DIALOG-PROTOCOL>> "))
  (:function make-exit-button (framem dialog callback label
			       &rest initargs
			       &key &allow-other-keys) (:documentation " Defined in: <<DEFAULT-DIALOG-PROTOCOL>> ")))

;;; And some non-protocol gfs.

(defgeneric default-dialog-frame-wrapper (frame-manager dialog-frame layout))
(defgeneric update-default-dialog-layout (frame-manager dialog-frame))


#||
define pane <dialog-top-level-layout> ()
  slot dialog-user-layout :: false-or(<sheet>),
    init-keyword: user-layout:;
end pane <dialog-top-level-layout>;
||#

(define-pane <dialog-top-level-layout> ()
  ((dialog-user-layout :type (or null <sheet>) :initarg :user-layout :accessor dialog-user-layout)))


#||
define method default-dialog-extra-size
    (framem :: <frame-manager>, dialog :: <dialog-frame>)
 => (width :: <integer>, height :: <integer>)
  values(0, 0)
end method default-dialog-extra-size;
||#

(defmethod default-dialog-extra-size ((framem <frame-manager>) (dialog <dialog-frame>))
  (values 0 0))


#||
define method do-compose-space
    (layout :: <dialog-top-level-layout>, #key width, height)
 => (space-req :: <space-requirement>)
  let frame = sheet-frame(layout);
  let (extra-width, extra-height)
    = default-dialog-extra-size(frame-manager(frame), frame);
  let child-width  = width  & (width  - extra-width);
  let child-height = height & (height - extra-height);
  let space-req
    = next-method(layout, width: child-width, height: child-height);
  if (extra-width = 0 & extra-height = 0)
    space-req
  else
    space-requirement+(layout, space-req,
		       width: extra-width, height: extra-height)
  end
end method do-compose-space;
||#

(defmethod do-compose-space ((layout <dialog-top-level-layout>)
			     &key width height)
  (let ((frame (sheet-frame layout)))
    (multiple-value-bind (extra-width extra-height)
	(default-dialog-extra-size (frame-manager frame) frame)
      (let* ((child-width (and width (- width extra-width)))
	     (child-height (and height (- height extra-height)))
	     (space-req (call-next-method layout :width child-width :height child-height)))
	(if (and (= extra-width 0) (= extra-height 0))
	    space-req
	    ;; else
	    (space-requirement+ layout space-req
				:width extra-width :height extra-height))))))


#||
define method do-allocate-space
    (layout :: <dialog-top-level-layout>, width :: <integer>, height :: <integer>) => ()
  let frame = sheet-frame(layout);
  let (extra-width, extra-height)
    = default-dialog-extra-size(frame-manager(frame), frame);
  next-method(layout, width - extra-width, height - extra-height)
end method do-allocate-space;
||#

(defmethod do-allocate-space ((layout <dialog-top-level-layout>) (width integer) (height integer))
  (let ((frame (sheet-frame layout)))
    (multiple-value-bind (extra-width extra-height)
	(default-dialog-extra-size (frame-manager frame) frame)
      (call-next-method layout (- width extra-width) (- height extra-height)))))


#||
define constant $dialog-text-style
    = make(<text-style>, weight: #"bold", size: 14);
||#

(defvar *dialog-text-style* (make-text-style nil nil :bold nil 14))


#||
define method default-dialog-frame-wrapper
    (framem :: <frame-manager>, dialog :: <dialog-frame>, layout :: false-or(<sheet>))
 => (sheet :: false-or(<sheet>))
  with-frame-manager (framem)
    let children :: <stretchy-object-vector> = make(<stretchy-vector>);
    let drawing-children :: <stretchy-object-vector> = make(<stretchy-vector>);
    let title = frame-title(dialog);
    let exit-box = make-exit-box(framem, dialog);
    let spacing = default-dialog-spacing(framem, dialog);
    when (title & dialog-needs-title-pane?(framem, dialog))
      add!(drawing-children,
           make(<label-pane>,
                label: title,
                text-style: $dialog-text-style))
    end;
    when (layout | exit-box)
      let position = dialog-exit-buttons-position(dialog);
      let button-orientation
        = select (position)
            #"top", #"bottom" => #"vertical";
	    #"left", #"right" => #"horizontal";
	    otherwise         => #"vertical";
          end;
      let layout-children :: <stretchy-object-vector> = make(<stretchy-vector>);
      when (layout) add!(layout-children, layout) end;
      when (exit-box & dialog-needs-separator?(framem, dialog))
	let separator-orientation
	  = select (button-orientation)
	      #"horizontal" => #"vertical";
	      #"vertical"   => #"horizontal";
	    end;
	add!(layout-children, 
             make(<separator>, orientation: separator-orientation))
      end;
      when (exit-box) add!(layout-children, exit-box) end;
      when (position = #"left" | position = #"top") reverse!(layout-children) end;
      add!(drawing-children,
           select (button-orientation)
             #"horizontal" =>
               make(<row-layout>,
                    spacing: spacing,
                    y-alignment: #"center",
                    max-height: $fill,
                    children: layout-children);
             #"vertical" =>
               make(<column-layout>,
                    spacing: spacing,
                    x-alignment: #"center",
                    max-width: $fill,
                    children: layout-children);
           end)
    end;
    unless (empty?(drawing-children))
      add!(children,
           make(<spacing>,
                child: make-top-level-drawing-pane(framem, drawing-children),
                spacing: default-dialog-border(framem, dialog)))
    end;
    make(<dialog-top-level-layout>,
         user-layout: layout,
         child: unless (empty?(children))
                  make(<column-layout>,
                       spacing: spacing,
                       children: children)
                end)
  end
end method default-dialog-frame-wrapper;
||#

(defmethod default-dialog-frame-wrapper ((framem <frame-manager>) (dialog <dialog-frame>) (layout <sheet>))
  (with-frame-manager (framem)
    (let ((children (MAKE-STRETCHY-VECTOR))
	  (drawing-children (MAKE-STRETCHY-VECTOR))
	  (title (frame-title dialog))
	  (exit-box (make-exit-box framem dialog))
	  (spacing (default-dialog-spacing framem dialog)))
      (when (and title (dialog-needs-title-pane? framem dialog))
	(ADD! drawing-children
	      (make-pane '<label-pane>
			 :label title
			 :text-style *dialog-text-style*)))
      (when (or layout exit-box)
	(let* ((position (dialog-exit-buttons-position dialog))
	       (button-orientation
                (case position
                  ((:top :bottom) :vertical)
                  ((:left :right) :horizontal)
                  (t              :vertical)))
	       (layout-children (MAKE-STRETCHY-VECTOR)))
	  (when layout (ADD! layout-children layout))
	  (when (and exit-box (dialog-needs-separator? framem dialog))
	    (let ((separator-orientation (ecase button-orientation
					   (:horizontal :vertical)
					   (:vertical :horizontal))))
	      (ADD! layout-children (make-pane '<separator> :orientation separator-orientation))))
	  (when exit-box (ADD! layout-children exit-box))
	  (when (or (eql position :left) (eql position :top))
	    (setf layout-children (nreverse layout-children)))
	  (ADD! drawing-children
		(ecase button-orientation
		  (:horizontal (make-pane '<row-layout>
					  :spacing spacing
					  :y-alignment :center
					  :max-height +fill+
					  :children layout-children))
		  (:vertical   (make-pane '<column-layout>
					  :spacing spacing
					  :x-alignment :center
					  :max-width +fill+
					  :children layout-children))))))
      (unless (empty? drawing-children)
	(ADD! children
	      (make-pane '<spacing>
			 :child (make-top-level-drawing-pane framem drawing-children)
			 :spacing (default-dialog-border framem dialog))))
      (make-pane '<dialog-top-level-layout>
		 :user-layout layout
		 :child (unless (empty? children)
			  (make-pane '<column-layout>
				     :spacing spacing
				     :children children))))))

(defmethod default-dialog-frame-wrapper ((framem <frame-manager>) (dialog <dialog-frame>) (layout null))
  (with-frame-manager (framem)
    (let ((children (MAKE-STRETCHY-VECTOR))
	  (drawing-children (MAKE-STRETCHY-VECTOR))
	  (title (frame-title dialog))
	  (exit-box (make-exit-box framem dialog))
	  (spacing (default-dialog-spacing framem dialog)))
      (when (and title (dialog-needs-title-pane? framem dialog))
	(ADD! drawing-children
	      (make-pane '<label-pane>
			 :label title
			 :text-style *dialog-text-style*)))
      (when (or layout exit-box)
	(let* ((position (dialog-exit-buttons-position dialog))
	       (button-orientation
                (case position
                  ((:top :bottom) :vertical)
                  ((:left :right) :horizontal)
                  (t              :vertical)))
	       (layout-children (MAKE-STRETCHY-VECTOR)))
	  (when layout (ADD! layout-children layout))
	  (when (and exit-box (dialog-needs-separator? framem dialog))
	    (let ((separator-orientation (ecase button-orientation
					   (:horizontal :vertical)
					   (:vertical :horizontal))))
	      (ADD! layout-children (make-pane '<separator> :orientation separator-orientation))))
	  (when exit-box (ADD! layout-children exit-box))
	  (when (or (eql position :left) (eql position :top))
	    (setf layout-children (nreverse layout-children)))
	  (ADD! drawing-children
		(ecase button-orientation
		  (:horizontal (make-pane '<row-layout>
					  :spacing spacing
					  :y-alignment :center
					  :max-height +fill+
					  :children layout-children))
		  (:vertical   (make-pane '<column-layout>
					  :spacing spacing
					  :x-alignment :center
					  :max-width +fill+
					  :children layout-children))))))
      (unless (empty? drawing-children)
	(ADD! children
	      (make-pane '<spacing>
			 :child (make-top-level-drawing-pane framem drawing-children)
			 :spacing (default-dialog-border framem dialog))))
      (make-pane '<dialog-top-level-layout>
		 :user-layout layout
		 :child (unless (empty? children)
			  (make-pane '<column-layout>
				     :spacing spacing
				     :children children))))))


#||
define method default-dialog-border
    (framem :: <frame-manager>, dialog :: <dialog-frame>)
 => (border :: <integer>)
  8
end method default-dialog-border;
||#

(defmethod default-dialog-border ((framem <frame-manager>) (dialog <dialog-frame>))
  8)


#||
define method default-dialog-spacing
    (framem :: <frame-manager>, dialog :: <dialog-frame>)
 => (border :: <integer>)
  4
end method default-dialog-spacing;
||#

(defmethod default-dialog-spacing ((framem <frame-manager>) (dialog <dialog-frame>))
  4)


#||
define method default-dialog-button-spacing
    (framem :: <frame-manager>, dialog :: <dialog-frame>)
 => (border :: <integer>)
  4
end method default-dialog-button-spacing;
||#

(defmethod default-dialog-button-spacing ((framem <frame-manager>) (dialog <dialog-frame>))
  4)


#||
define method default-dialog-button-x-alignment
    (framem :: <frame-manager>, dialog :: <dialog-frame>)
 => (x-alignment :: <x-alignment>)
  #"right"
end method default-dialog-button-x-alignment;
||#

(defmethod default-dialog-button-x-alignment ((framem <frame-manager>) (dialog <dialog-frame>))
  :right)


#||
define method default-dialog-button-y-alignment
    (framem :: <frame-manager>, dialog :: <dialog-frame>)
 => (y-alignment :: <y-alignment>)
  #"top"
end method default-dialog-button-y-alignment;
||#

(defmethod default-dialog-button-y-alignment ((framem <frame-manager>) (dialog <dialog-frame>))
  :top)


#||
define method dialog-needs-title-pane?
    (framem :: <frame-manager>, dialog :: <dialog-frame>)
 => (needs-title-pane? :: <boolean>)
  #f
end method dialog-needs-title-pane?;
||#

(defmethod dialog-needs-title-pane? ((framem <frame-manager>) (dialog <dialog-frame>))
  nil)


#||
define method dialog-needs-separator?
    (framem :: <frame-manager>, dialog :: <dialog-frame>)
 => (needs-separator? :: <boolean>)
  #f
end method dialog-needs-separator?;
||#

(defmethod dialog-needs-separator? ((framem <frame-manager>) (dialog <dialog-frame>))
  nil)


#||
define method make-top-level-drawing-pane
    (framem :: <frame-manager>, children :: <sequence>)
 => (sheet :: <sheet>)
  make(<column-layout>,
       y-spacing: 6,
       x-alignment: #"center",
       children: children)
end method make-top-level-drawing-pane;
||#

(defmethod make-top-level-drawing-pane ((framem <frame-manager>) (children sequence))
  (make-pane '<column-layout>
	     :y-spacing 6
	     :x-alignment :center
	     :children children))


#||
define method update-default-dialog-layout
    (framem :: <frame-manager>, frame :: <dialog-frame>) => ()
  let top-sheet = top-level-sheet(frame);
  when (top-sheet)
    let top-layout = sheet-child(top-sheet);
    let old-layout = dialog-user-layout(top-layout);
    if (old-layout)
      let parent = sheet-parent(old-layout);
      let new-layout = frame-layout(frame);
      replace-child(parent, old-layout, new-layout);
      dialog-user-layout(top-layout) := new-layout;
      relayout-parent(new-layout);
      sheet-mapped?(new-layout) := frame-mapped?(frame)
    else
      //---*** What do you do if there wasn't a layout before?
      #f
    end
  end
end method update-default-dialog-layout;
||#

(defmethod update-default-dialog-layout ((framem <frame-manager>) (frame <dialog-frame>))
  (let ((top-sheet (top-level-sheet frame)))
    (when top-sheet
      (let* ((top-layout (sheet-child top-sheet))
             (old-layout (dialog-user-layout top-layout)))
	(if old-layout
	    (let ((parent (sheet-parent old-layout))
		  (new-layout (frame-layout frame)))
	      (replace-child parent old-layout new-layout)
	      (setf (dialog-user-layout top-layout) new-layout)
	      (relayout-parent new-layout)
	      (setf (sheet-mapped? new-layout) (frame-mapped? frame)))
	    ;; else
	    ;;---*** What do you do if there wasn't a layout before?
	    nil)))))


#||
// Makes a layout containing all of the exit buttons
define method make-exit-box 
    (framem :: <frame-manager>, dialog :: <dialog-frame>)
 => (layout :: false-or(<layout-pane>))
  when (dialog-exit-buttons-position(dialog))	// if no position, no exit box...
    with-frame-manager (framem)
      let children = make-exit-buttons(framem, dialog);
      let help-callback = dialog-help-callback(dialog);
      let help-button 
	= dialog-help-button(dialog)
	  | when (help-callback)
	      make-exit-button(framem, dialog, help-callback, $help-label)
	    end;
      when (help-button)
	add!(children, help-button);
	dialog-help-button(dialog) := help-button
      end;
      let spacing = default-dialog-button-spacing(framem, dialog);
      unless (empty?(children))
	select (dialog-exit-buttons-position(dialog))      
	  #"top", #"bottom" =>
	    let button-layout
	      = make(<row-layout>,
		     spacing: spacing,
		     equalize-widths?: #t, equalize-heights?: #t,
		     children: children);
	    make(<column-layout>,
		 children: vector(button-layout),
		 x-alignment: default-dialog-button-x-alignment(framem, dialog),
		 max-width: $fill);
	  #"left", #"right" =>
	    let button-layout
	      = make(<column-layout>,
		     spacing: spacing,
		     equalize-widths?: #t, equalize-heights?: #t,
		     children: children);
	    make(<row-layout>,
		 children: vector(button-layout),
		 y-alignment: default-dialog-button-y-alignment(framem, dialog),
		 max-height: $fill);
	end
      end
    end
  end
end method make-exit-box;
||#

(defmethod make-exit-box ((framem <frame-manager>) (dialog <dialog-frame>))
  (when (dialog-exit-buttons-position dialog)   ;; if no position, no exit box...
    (with-frame-manager (framem)
      (let* ((children (make-exit-buttons framem dialog))
             (help-callback (dialog-help-callback dialog))
             (help-button (or (dialog-help-button dialog)
                              (when help-callback
                                (make-exit-button framem dialog help-callback *help-label*)))))
	(when help-button
	  (add! children help-button)
	  (setf (dialog-help-button dialog) help-button))
	(let ((spacing (default-dialog-button-spacing framem dialog)))
	  (unless (empty? children)
	    (ecase (dialog-exit-buttons-position dialog)
	      ((:top :bottom)
	       (let ((button-layout (make-pane '<row-layout>
					       :spacing spacing
					       :equalize-widths? t :equalize-heights? t
					       :children children)))
		 (make-pane '<column-layout>
			    :children (vector button-layout)
			    :x-alignment (default-dialog-button-x-alignment framem dialog)
			    :max-width +fill+)))
	      ((:left :right)
	       (let ((button-layout (make-pane '<column-layout>
					       :spacing spacing
					       :equalize-widths? t :equalize-heights? t
					       :children children)))
		 (make-pane '<row-layout>
			    :children (vector button-layout)
			    :y-alignment (default-dialog-button-y-alignment framem dialog)
			    :max-height +fill+))))))))))


#||
// Returns a sequence of all the exit buttons
define method make-exit-buttons
    (framem :: <frame-manager>, dialog :: <dialog-frame>)
 => (buttons :: <sequence>)
  let children :: <stretchy-object-vector> = make(<stretchy-vector>);
  let exit-enabled? = dialog-exit-enabled?(dialog);
  let exit-button 
    = dialog-exit-button(dialog)
      | (dialog-exit-callback(dialog)
	 & make-exit-button(framem, dialog, dialog-exit-callback(dialog), $ok-label, 
			    enabled?: exit-enabled?));
  when (exit-button)
    add!(children, exit-button);
    dialog-exit-button(dialog) := exit-button;
    when (~frame-default-button(dialog) & exit-enabled?)
      frame-default-button(dialog) := exit-button
    end
  end;
  let cancel-button 
    = dialog-cancel-button(dialog)
      | (dialog-cancel-callback(dialog)
	 & make-exit-button(framem, dialog, dialog-cancel-callback(dialog), $cancel-label));
  when (cancel-button)
    add!(children, cancel-button);
    dialog-cancel-button(dialog) := cancel-button
  end;
  children
end method make-exit-buttons;
||#

;; Returns a sequence of all the exit buttons
(defmethod make-exit-buttons ((framem <frame-manager>) (dialog <dialog-frame>))
  (let* ((children (MAKE-STRETCHY-VECTOR))
         (exit-enabled? (dialog-exit-enabled? dialog))
         (exit-button (or (dialog-exit-button dialog)
                          (and (dialog-exit-callback dialog)
                               (make-exit-button framem dialog (dialog-exit-callback dialog) *ok-label*
                                                 :enabled? exit-enabled?)))))
    (when exit-button
      (add! children exit-button)
      (setf (dialog-exit-button dialog) exit-button)
      (when (and (not (frame-default-button dialog)) exit-enabled?)
	(setf (frame-default-button dialog) exit-button)))
    (let ((cancel-button (or (dialog-cancel-button dialog)
			     (and (dialog-cancel-callback dialog)
				  (make-exit-button framem dialog (dialog-cancel-callback dialog) *cancel-label*)))))
      (when cancel-button
	(add! children cancel-button)
	(setf (dialog-cancel-button dialog) cancel-button))
      children)))


#||
// Returns a single exit button
define method make-exit-button
    (framem :: <frame-manager>, dialog :: <dialog-frame>,
     callback :: false-or(<callback-type>), label :: <string>,
     #rest initargs, 
     #key enabled? = (callback ~= #f), #all-keys)
 => (button :: false-or(<push-button>))
  when (callback)
    with-frame-manager (framem)
      apply(make, <push-button>,
	    activate-callback: method (button)
				 let dialog = sheet-frame(button);
				 execute-callback(dialog, callback, dialog)
			       end,
	    label: label,
	    enabled?: enabled?,
	    initargs)
    end
  end
end method make-exit-button;
||#

(defmethod make-exit-button ((framem <frame-manager>) (dialog <dialog-frame>)
			     callback (label string)
			     &rest initargs
			     &key (enabled? callback) &allow-other-keys)
  (when callback
    (with-frame-manager (framem)
      (apply #'make-pane '<push-button>
	     :activate-callback #'(lambda (button)
				    (let ((dialog (sheet-frame button)))
				      (execute-callback dialog callback dialog)))
	     :label label
	     :enabled? enabled?
	     initargs))))


#||

/// Property frames

define method make-exit-buttons
    (framem :: <frame-manager>, dialog :: <property-frame>)
 => (buttons :: <sequence>)
  let children = next-method();
  let apply-callback = dialog-apply-callback(dialog);
  let apply-button 
    = make-exit-button(framem, dialog, apply-callback, $apply-label);
  when (apply-button)
    add!(children, apply-button);
    dialog-apply-button(dialog) := apply-button
  end;
  children  
end method make-exit-buttons;
||#

(defmethod make-exit-buttons ((framem <frame-manager>) (dialog <property-frame>))
  (let* ((children (call-next-method))
         (apply-callback (dialog-apply-callback dialog))
         (apply-button (make-exit-button framem dialog apply-callback *apply-label*)))
    (when apply-button
      (add! children apply-button)
      (setf (dialog-apply-button dialog) apply-button))
    children))


#||
define method property-frame-tab-control
    (dialog :: <property-frame>)
 => (tab-control :: false-or(<tab-control>))
  frame-layout(dialog)
end method property-frame-tab-control;
||#

(defgeneric property-frame-tab-control (dialog))

(defmethod property-frame-tab-control ((dialog <property-frame>))
  (frame-layout dialog))


#||
define sideways method dialog-current-page
    (dialog :: <property-frame>) => (page :: false-or(<sheet>))
  tab-control-current-page(property-frame-tab-control(dialog))
end method dialog-current-page;
||#

(defmethod dialog-current-page ((dialog <property-frame>))
  (tab-control-current-page (property-frame-tab-control dialog)))


#||
define sideways method dialog-current-page-setter
    (page :: false-or(<sheet>), dialog :: <property-frame>)
 => (page :: false-or(<sheet>))
  tab-control-current-page(property-frame-tab-control(dialog)) := page
end method dialog-current-page-setter;
||#

(defmethod (setf dialog-current-page) ((page <sheet>) (dialog <property-frame>))
  (setf (tab-control-current-page (property-frame-tab-control dialog)) page))


#||

/// Wizards

define method dialog-needs-separator?
    (framem :: <frame-manager>, dialog :: <wizard-frame>)
 => (needs-separator? :: <boolean>)
  #t
end method dialog-needs-separator?;
||#

(defmethod dialog-needs-separator? ((framem <frame-manager>) (dialog <wizard-frame>))
  t)


#||
define method make-exit-buttons
    (framem :: <frame-manager>, dialog :: <wizard-frame>)
 => (buttons :: <sequence>)
  let next-callback = dialog-next-callback(dialog);
  let back-callback = dialog-back-callback(dialog);
  let pages = dialog-pages(dialog);
  let multiple-pages? = (size(pages) > 1);
  let exit-enabled?   = dialog-exit-enabled?(dialog);
  let cancel-callback = dialog-cancel-callback(dialog) | cancel-dialog;
  let exit-callback   = dialog-exit-callback(dialog)   | exit-dialog;
  let cancel-button
    = make-exit-button(framem, dialog, cancel-callback, $cancel-label);
  let back-button
    = when (multiple-pages?)
	make-exit-button(framem, dialog, back-callback, $back-label,
                         enabled?: #f)
      end;
  let next-button
    = when (multiple-pages?)
	make-exit-button(framem, dialog, next-callback, $next-label)
      end;
  let exit-button
    = make-exit-button(framem, dialog, exit-callback, $finish-label,
		       //---*** How do we handle enabling of this button?
		       // enabled?: exit-enabled?,
		       withdrawn?: multiple-pages?);
  let buttons
    = if (multiple-pages?)
        vector(back-button, next-button, exit-button, cancel-button)
      else
        vector(exit-button, cancel-button)
      end;
  dialog-cancel-button(dialog) := cancel-button;
  dialog-exit-button(dialog) := exit-button;
  dialog-back-button(dialog) := back-button;
  dialog-next-button(dialog) := next-button;
  buttons
end method make-exit-buttons;
||#

(defmethod make-exit-buttons ((framem <frame-manager>) (dialog <wizard-frame>))
  (let* ((next-callback (dialog-next-callback dialog))
	 (back-callback (dialog-back-callback dialog))
	 (pages (dialog-pages dialog))
	 (multiple-pages? (> (length pages) 1))
	 (exit-enabled? (dialog-exit-enabled? dialog))
	 (cancel-callback (or (dialog-cancel-callback dialog) #'cancel-dialog))
	 (exit-callback   (or (dialog-exit-callback dialog)   #'exit-dialog))
	 (cancel-button (make-exit-button framem dialog cancel-callback *cancel-label*))
	 (back-button (when multiple-pages?
			(make-exit-button framem dialog back-callback *back-label* :enabled? nil)))
	 (next-button (when multiple-pages?
			(make-exit-button framem dialog next-callback *next-label*)))
	 (exit-button (make-exit-button framem dialog exit-callback *finish-label*
					;;---*** How do we handle enabling of this button?
					;; :enabled? exit-enabled?
					:withdrawn? multiple-pages?))
	 (buttons (if multiple-pages?
		      (vector back-button next-button exit-button cancel-button)
		      ;; else
		      (vector exit-button cancel-button))))
    (declare (ignore exit-enabled?))
    ;; TODO 2020-04-10 DR: check exit-enabled? isn't needed
    (setf (dialog-cancel-button dialog) cancel-button)
    (setf (dialog-exit-button dialog)   exit-button)
    (setf (dialog-back-button dialog)   back-button)
    (setf (dialog-next-button dialog)   next-button)
    buttons))


#||
define sideways method dialog-current-page
    (dialog :: <wizard-frame>) => (page :: false-or(<sheet>))
  let pages = dialog-pages(dialog);
  block (return)
    for (page in pages)
      unless (sheet-withdrawn?(page))
        return(page)
      end
    end
  end
end method dialog-current-page;
||#

(defmethod dialog-current-page ((dialog <wizard-frame>))
  (let ((pages (dialog-pages dialog)))
    (map nil #'(lambda (page)
		 (unless (sheet-withdrawn? page)
		   (return-from dialog-current-page page)))
	 pages)))


#||
define sideways method dialog-current-page-setter
    (new-page :: false-or(<sheet>), dialog :: <wizard-frame>)
 => (new-page :: false-or(<sheet>))
  for (page in dialog-pages(dialog))
    unless (page == new-page)
      sheet-withdrawn?(page) := #t
    end
  end;
  when (new-page)
    let parent = sheet-parent(new-page);
    sheet-withdrawn?(new-page) := #f;
    let mapped? = parent & sheet-mapped?(parent);
    when (mapped?)
      relayout-parent(new-page);
      sheet-mapped?(new-page) := #t
    end;
    let frame = sheet-frame(new-page);
    when (frame & page-initial-focus(new-page))
      frame-input-focus(frame) := page-initial-focus(new-page)
    end
  end;
  debug-assert(dialog-current-page(dialog) == new-page,
               "Internal error: failed to set the current page for %=",
               dialog);
  new-page
end method dialog-current-page-setter;
||#

(defmethod (setf dialog-current-page) ((new-page <sheet>) (dialog <wizard-frame>))
  (map nil #'(lambda (page)
	       (unless (eql page new-page)
		 (setf (sheet-withdrawn? page) t)))
       (dialog-pages dialog))
  (when new-page
    (let ((parent (sheet-parent new-page)))
      (setf (sheet-withdrawn? new-page) nil)
      (let ((mapped? (and parent (sheet-mapped? parent))))
	(when mapped?
	  (relayout-parent new-page)
	  (setf (sheet-mapped? new-page) t))
	(let ((frame (sheet-frame new-page)))
	  (when (and frame (page-initial-focus new-page))
	    (setf (frame-input-focus frame) (page-initial-focus new-page)))))))
  (unless (eql (dialog-current-page dialog) new-page)
    (error "Internal error: failed to set the current page for ~s" dialog))
  new-page)


#||
//---*** This is disgusting, there must be a way to avoid using sideways
define sideways method initialize
    (dialog :: <wizard-frame>, #key page, frame-manager: framem) => ()
  unless (frame-layout(dialog))
    //---*** There must be an easier way than this...
    let framem
      = framem
          | frame-manager(dialog)
          | port-default-frame-manager(default-port());
    with-frame-manager (framem)
      let pages = dialog-pages(dialog);
      let image = dialog-image(dialog);
      let stack-layout = make(<stack-layout>, children: pages);
      let layout
	= if (image)
	    //--- This spacing is just a guess!
	    let spacing = 2 * default-dialog-button-spacing(framem, dialog);
	    let label = make(<label>, label: image);
	    let border
	      = with-border (type: #"sunken")
	          label
	        end;
	    make(<row-layout>,
		 children: vector(border, stack-layout),
		 y-alignment: #"center",
		 spacing: spacing)
	  else
	    stack-layout
	  end;
      frame-layout(dialog) := layout;
      for (page in pages)
	sheet-withdrawn?(page) := #t
      end
    end
  end;
  next-method();
end method initialize;
||#

;; Run after shared-initialize, but before any of the other (superclass)
;; :after methods have had chance to run.
(defmethod initialize-instance ((dialog <wizard-frame>) &key page ((:frame-manager framem)) &allow-other-keys)
  (declare (ignore page))
  (call-next-method)
  (unless (frame-layout dialog)
    ;;---*** There must be an easier way than this...
    (let ((framem (or framem
		      (frame-manager dialog)
		      (port-default-frame-manager (default-port)))))
      (with-frame-manager (framem)
        (let* ((pages (dialog-pages dialog))
               (image (dialog-image dialog))
               (stack-layout (make-pane '<stack-layout> :children pages))
               (layout (if image
                           ;;--- This spacing is just a guess!
                           (let* ((spacing (* 2 (default-dialog-button-spacing framem dialog)))
                                  (label   (make-pane '<label> :label image))
                                  (border  (with-border (:type :sunken)
                                             label)))
                             (make-pane '<row-layout>
					:children (vector border stack-layout)
					:y-alignment :center
					:spacing spacing))
                           ;; else
                           stack-layout)))
	  (setf (frame-layout dialog) layout)
          (map nil #'(lambda (page)
		       (setf (sheet-withdrawn? page) t))
	       pages))))))



