;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

(defpackage #:duim-gifs
  (:use #:common-lisp
	#:duim)
  (:export
       #:<gif-image>
       #:<gif-image-metaclass>
       #:read-gif-image))


