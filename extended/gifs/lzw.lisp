;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GIFS -*-
(in-package #:duim-gifs)

#||
/// LZW decompression

define method remove-all-keys! (collection :: <table>)
  for (key in key-sequence(collection))
    remove-key!(collection, key)
  end
end method remove-all-keys!;
||#

(defmethod remove-all-keys! ((collection hashtable))
  (loop for key in (hashtable-keys collection)
        do (remove-key! collection key)))

#||
define class <lzw-dictionary> (<object>)
  slot dictionary-code-values = make(<vector>, size: 4096);
  slot dictionary-table = make(<hash-table>);
  slot dictionary-initial-size = 256,
    init-keyword: size:;
  slot dictionary-size = 256,
    init-keyword: size:;
end class <lzw-dictionary>;
||#

(defclass <lzw-dictionary> ()
  ((dictionary-code-values  :initform (make-array 4096)  :accessor dictionary-code-values)
   (dictionary-table        :initform (make-hash-table)  :accessor dictionary-table)    ;; FIXME: :TEST?
   (dictionary-initial-size :initform 256 :initarg :size :accessor dictionary-initial-size)
   (dictionary-suze         :initform 256 :initarg :size :accessor dictionary-size)))


#||
define method initialize-dictionary (dictionary :: <lzw-dictionary>)
  let size = dictionary-initial-size(dictionary);
  let table = dictionary-table(dictionary);
  let code-values = dictionary-code-values(dictionary);
  dictionary-size(dictionary) := size;
  remove-all-keys(dictionary-table(dictionary));
  for (i from 0 below size)
    table[i] := i;
    code-values[i] := i
  end
end method initialize-dictionary;
||#

(defmethod initialize-dictionary ((dictionary <lzw-dictionary>))
  (let* ((size (dictionary-initial-size dictionary))
         (table (dictionary-table dictionary))
         (code-values (dictionary-code-values dictionary))
         (setf (dictionary-size dictionary) size))
    (remove-all-keys (dictionary-table dictionary))
    (loop for i below size
          do (setf (aref table i) i
                   (aref code-values i) i))))

#||
define method decompress-lzw
    (input :: <sequence>, #key initial-database, buffer-size = 256)
 => (output :: <sequence>)
  let buffer = make(<vector>, size: buffer-size);
  decompress-lzw-into!(input, buffer, initial-database: initial-database)
end method decompress-lzw;
||#

(defmethod decompress-lzw ((input sequence)
                           &key initial-database (buffer-size 256))
  (let ((buffer (make-array buffer-size)))
    (decompress-lzw-into! input buffer :initial-database initial-database)))

#||
define method decompress-lzw-into!
    (input :: <sequence>, output :: <sequence>, #key initial-database)
 => (data :: <sequence>)
end method decompress-lzw-into!;
||#

(defmethod decompress-lzw-into! ((input sequence)
                                 (output sequence)
                                 &key initial-database)
  nil)
