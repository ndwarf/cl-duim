;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-GIFS -*-
(in-package #:duim-gifs)

#||
/// GIF image classes

define class <gif-image-descriptor> (<object>)
  slot gif-image-color-resolution :: <integer> = 0,
    init-keyword: color-resolution:;
  slot gif-image-background :: <integer> = 0, 
    init-keyword: background:;
  slot gif-image-aspect-ratio :: <integer> = 0,
    init-keyword: aspect-ratio:;
end class <gif-image-descriptor>;
||#

(defclass <gif-image-descriptor> ()
  ((gif-image-color-resolution :type integer :initform 0 :initarg :color-resolution
			       :accessor gif-image-color-resolution)
   (gif-image-background :type integer :initform 0 :initarg :background   :accessor gif-image-background)
   (gif-aspect-ratio     :type integer :initform 0 :initarg :aspect-ratio :accessor gif-aspect-ratio)))

#||
define class <gif-color-table> (<object>)
  slot gif-color-table-sorted? :: <boolean> = #f,
    init-keyword: sorted?:;
  slot gif-color-table-colors :: <vector>,
    required-init-keyword: colors:;
end class <gif-color-table>;
||#

(defclass <gif-color-table> ()
  ((gif-color-table-sorted? :type boolean :initform nil :initarg :sorted? :accessor gif-color-table-sorted?)
   (gif-color-table-colors :type array :initarg :colors
			   :initform (required-slot ":colors" "<gif-color-table>")
			   :accessor gif-color-table-colors)))

#||
define class <gif-image> (<image>)
  slot image-width,
    required-init-keyword: width:;
  slot image-height,
    required-init-keyword: height:;
  slot gif-image-version = "87a",
    init-keyword: version:;
  slot gif-image-color-table :: false-or(<gif-color-table>),
    init-keyword: color-table:;
  slot gif-image-descriptor = #f,
    init-keyword: descriptor:;
end class <gif-image>;
||#

(defclass <gif-image> (<image>)
  ((image-width :initarg :width :accessor image-width
		:initform (required-slot ":width" "<gif-image>"))
   (image-height :initarg :height :accessor image-height
		 :initform (required-slot ":height" "<gif-image>"))
   (gif-image-version :initform "87a" :initarg :version :accessor gif-image-version)
   (gif-image-color-table :type (or null <gif-color-table>) :initarg :color-table
			  :accessor gif-image-color-table)
   (gif-image-descriptor :initform nil :initarg :descriptor :accessor gif-image-descriptor)))
