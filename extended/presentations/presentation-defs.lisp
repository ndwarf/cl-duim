;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-PRESENTATIONS-INTERNALS -*-
(in-package #:duim-presentations-internals)

#||
/// Presentation record protocol classes

define open abstract class <abstract-presentation-record> (<abstract-sheet>) end;
||#

(defclass <abstract-presentation-record> (<abstract-sheet>) ())

#||
// The protocol class for all output records
define protocol-class presentation-record (<sheet>, <abstract-presentation-record>) end;
||#

(define-protocol-class presentation-record (<sheet> <abstract-presentation-record>) ())

#||
// The protocol class for views
define protocol-class view (<object>) end;
||#

(define-protocol-class view () ())
