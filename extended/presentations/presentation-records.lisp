;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-PRESENTATIONS-INTERNALS -*-
(in-package #:duim-presentations-internals)

#||
/// Presentation records

define class <presentation-record> (...)
end class <presentation-record>;
||#

(defclass <presentation-record> (...) ())

