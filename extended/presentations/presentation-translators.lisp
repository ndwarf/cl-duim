;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-PRESENTATIONS-INTERNALS -*-
(in-package #:duim-presentations-internals)

#||
/// Presentation type translators macros

define macro presentation-translator-definer
end macro presentation-translator-definer;
||#

(defmacro presentation-translator-definer ())

#||
define macro presentation-action-definer
end macro presentation-action-definer;
||#

(defmacro presentation-action-definer ())

#||
define macro command-translator-definer
end macro command-translator-definer;
||#

(defmacro command-translator-definer ())


#||

define macro drag-and-drop-translator-definer
end macro drag-and-drop-translator-definer;
||#

(defmacro drag-and-drop-translator-definer ())


#||

/// Basic global presentation translators

define presentation-translator identity-translator
    (object :: <object>, #key presentation) => (<bottom>)
    (command-table: XXX,
     tester: identity-translator-applicable?,
     tester-definitive: #t,
     documentation: XXX,
     gesture: XXX)
  values(object, presentation-type(presentation))
end presentation-translator identity-translator;
||#

(define-presentation-translator identity-translator ((object t)
						     &key presentation)
  (:command-table XXX
   :tester identity-translator-applicable?
   :tester-definitive t
   :documentation XXX
   :gesture XXX)
  (values object (presentation-type presentation)))

#||
define function identity-translator-applicable? (presentation context-type)
end function identity-translator-applicable?;
||#

(defun identity-translator-applicable? (presentation context-type)
  nil)

#||
define presentation-action presentation-menu-action
    (object :: <object>, #key presentation, frame, sheet, x, y) => (<bottom>)
    (command-table: XXX,
     documentation: "Menu",
     gesture: XXX)
  call-presentation-menu(presentation, *input-context*, frame,
			 sheet: sheet, x: x, y: y,
			 for-menu?: #t
end presentation-action presentation-menu-action;
||#

(define-presentation-action presentation-menu-action ((object t)
						      &key presentation frame sheet x y)
  (:command-table XXX
   :documentation "Menu"
   :gesture XXX)
  (call-presentation-menu presentation *input-context* frame
			  :sheet sheet :x x :y y
			  :for-menu? t))

#||
define method call-presentation-menu
    (presentation, input-context, frame :: <frame>,
     #key sheet, x, y, for-menu?, label)
end method call-presentation-menu;
||#

(defmethod call-presentation-menu (presentation input-context (frame <frame>)
				   &key sheet x y for-menu? label)
  nil)

