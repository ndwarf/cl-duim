;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-PRESENTATIONS-INTERNALS -*-
(in-package #:duim-presentations-internals)

#||
/// Presentation types

define generic do-accept (...) => (...);
define generic do-present (...) => (...);
define generic do-describe (...) => (...);
define generic do-highlight (...) => (...);
define generic do-position-test (...) => (...);
||#

(defgeneric do-accept (...))
(defgeneric do-present (...))
(defgeneric do-describe (...))
(defgeneric do-highlight (...))
(defgeneric do-position-test (...))


#||

/// Standard types

<real>
  <integer>
    limited(<integer>)
  <float>

<string>

<keyword>

<boolean>

<sequence>
  <enumeration>

<member>
<subset>
||#

;;; dr: :fixme: I guess we need to replicate what's in CLIM here (e.g. create presentation types
;;;             for all standard Lisp types).
