;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

(defpackage :duim-presentations
  (:use #:common-lisp
	#:duim-utilities
	#:duim-geometry
	#:duim-DCs
	#:duim-sheets
	#:duim-graphics
	#:duim-layouts
	#:duim-gadgets
	#:duim-frames
	#:duim-recording)
  (:export
       #:*input-context*
       #:<presentation-event-handler>
       #:<presentation-pane>
       #:<presentation-record>
       #:<presentation-sheet>
       #:<presentation-translator>
       #:accept
       #:do-accept
       #:command-translator-definer
       #:describe-presentation-type
       #:do-describe
       #:drag-and-drop-translator-definer
       #:highlight-presentation
       #:do-highlight
       #:input-context-type
       #:input-context-tag
       #:present
       #:do-present
       #:presentation-position-test
       #:do-position-test
       #:presentation-type
;;       #:presentation-type-setter
;;       #:(setf presentation-type)
       #:presentation-object
;;       #:presentation-object-setter
;;       #:(setf presentation-object)
       #:presentation-action-definer
       #:presentation-translator-definer
       #:with-input-context
       #:do-with-input-context
       #:with-output-as-presentation
       #:do-with-output-as-presentation))


(defpackage :duim-presentations-internals
  (:use #:duim-utilities
	#:duim-geometry-internals
	#:duim-DCs-internals
	#:duim-sheets-internals
	#:duim-graphics-internals
	#:duim-layouts-internals
	#:duim-gadgets-internals
	#:duim-frames-internals
	#:duim-recording-internals
	#:duim-presentations)
  (:export
       ;; Export all from DUIM-PRESENTATIONS

       #:*input-context*
       #:<presentation-event-handler>
       #:<presentation-pane>
       #:<presentation-record>
       #:<presentation-sheet>
       #:<presentation-translator>
       #:accept
       #:do-accept
       #:command-translator-definer
       #:describe-presentation-type
       #:do-describe
       #:drag-and-drop-translator-definer
       #:highlight-presentation
       #:do-highlight
       #:input-context-type
       #:input-context-tag
       #:present
       #:do-present
       #:presentation-position-test
       #:do-position-test
       #:presentation-type
;;       #:presentation-type-setter
;;       #:(setf presentation-type)
       #:presentation-object
;;       #:presentation-object-setter
;;       #:(setf presentation-object)
       #:presentation-action-definer
       #:presentation-translator-definer
       #:with-input-context
       #:do-with-input-context
       #:with-output-as-presentation
       #:do-with-output-as-presentation

       ;; ... and any internal stuff we want to expose

       #:<basic-presentation-record>
       #:call-presentation-menu
       #:find-applicable-presentation
       #:find-applicable-translators
       #:find-presentation-translators
       #:highlight-applicable-presentation
       #:sheet-accept
       #:sheet-present))


