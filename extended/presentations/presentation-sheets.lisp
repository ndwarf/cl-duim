;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-PRESENTATIONS-INTERNALS -*-
(in-package #:duim-presentations-internals)

#||
/// Presentation sheets

define class <presentation-event-handler> (<event-handler>)
end class <presentation-event-handler>;
||#

(defclass <presentation-event-handler> (<event-handler>) ())

#||
define class <presentation-sheet> (...)
end class <presentation-sheet>;
||#

(defclass <presentation-sheet> (...) ())

#||
define class <presentation-pane> (...)
end class <presentation-pane>;
||#

(defclass <presentation-pane> (...) ())

