;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-PRESENTATIONS-INTERNALS -*-
(in-package #:duim-presentations-internals)

#||
/// Presentation macros

define macro with-output-as-presentation
end macro with-output-as-presentation;
||#

(defmacro with-output-as-presentation ())

#||
define macro with-input-context
end macro with-input-context;
||#

(defmacro with-input-context ())

