;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-PRESENTATIONS-INTERNALS -*-
(in-package #:duim-presentations-internals)

#||
/// 'accept' and friends

// A list of conses
define thread variable *input-context* :: <list> = #();
||#

(defvar *input-context* ())

#||
define inline function input-context-type
    (context-entry) => (type :: <type>)
  head(context-entry)
end function input-context-type;
||#

(declaim (inline input-context-type))
(defun input-context-type (context-entry)
  (car context-entry))

#||
define inline function input-context-tag
    (context-entry) => (tag :: <function>)
  tail(context-entry)
end function input-context-tag;
||#

(declaim (inline input-context-tag))
(defun input-context-tag (context-entry)
  (cdr context-entry))

#||
define generic sheet-accept
    (sheet :: <sheet>, type :: <type>, view :: <view>,
     #key default, default-type :: false-or(<type>), provide-default,
	  prompt, prompt-mode)
 => (object, type :: false-or(<type>));
||#

(defgeneric sheet-accept (sheet type view
			  &key default default-type provide-default
			  prompt prompt-mode))

#||
define function accept
    (type :: <type>, sheet :: <sheet>,
     #key view :: false-or(<view>) = #f,
	  default = $unsupplied, default-type :: false-or(<type>) = #f, provide-default = #t,
	  prompt = #t, prompt-mode = #"normal")
 => (object, type :: false-or(<type>))
  //---*** Massage arguments...
  sheet-accept(sheet, type, view,
	       default: default, default-type: default-type, provide-default: provide-default,
	       prompt: prompt, prompt-mode: prompt-mode)
end function accept;
||#

(defmethod accept ((type type)
		   (sheet <sheet>)
		   &key
		   (view nil)
		   (default $unsupplied) (default-type nil) (provide-default t)
		   (prompt t) (prompt-mode :normal))
  ;;---*** Massage arguments...
  (sheet-accept sheet type view
		:default default
		:default-type default-type
		:provide-default provide-default
		:prompt prompt
		:prompt-mode prompt-mode))

#||
define method sheet-accept
    (sheet :: <presentation-sheet>, type :: <type>, view :: <view>,
     #key default = $unsupplied, default-type :: false-or(<type>) = #f, provide-default = #t,
	  prompt = #t, prompt-mode = #"normal")
 => (object, type :: false-or(<type>))
end method sheet-accept;
||#

(defmethod sheet-accept ((sheet <presentation-sheet>)
			 (type typespec)
			 (view <view>)
			 &key
			 (default $unsupplied)
			 (default-type nil)
			 (provide-default t)
			 (prompt t)
			 (prompt-mode :normal))
  nil)

#||
define function do-with-input-context
    (type, override, body-function, context-function)
end function do-with-input-context;
||#

(defun do-with-input-context (type override body-function context-function)
  nil)

#||
define function find-applicable-presentation (...)
end function find-applicable-presentation;
||#

(defun find-applicable-presentation (...)
  nil)

#||
define function highlight-applicable-presentation (...)
end function highlight-applicable-presentation;
||#

(defun highlight-applicable-presentation (...)
  nil)
