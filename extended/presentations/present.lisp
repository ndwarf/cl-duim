;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: DUIM-PRESENTATIONS-INTERNALS -*-
(in-package #:duim-presentations-internals)

#||
/// 'present' and friends

define generic sheet-present
    (sheet :: <sheet>, object, type :: <type>, view :: <view>,
     #key for-context-type)
 => (record);
||#

(defgeneric sheet-present (sheet object type view &key for-context-type))

#||
define function present
    (object, type :: <type>, sheet :: <sheet>,
     #key view :: false-or(<view>) = #f,
	  for-context-type)
 => (record)
  //---*** Massage arguments...
  sheet-present(sheet, object, type, view,
		for-context-type: for-context-type)
end function present;
||#

(defmethod present (object (type typespec) (sheet <sheet>)
		    &key (view nil) for-context-type)
  ;;---*** Massage arguments...
  (sheet-present sheet object type view
		 :for-context-type for-context-type))

#||
define method sheet-present
    (sheet :: <presentation-sheet>, object, type :: <type>, view :: <view>,
     #key for-context-type)
 => (record)
end method sheet-present;
||#

(defmethod sheet-present ((sheet <presentation-sheet>)
			  object
			  (type typespec)
			  (view <view>)
			  &key for-context-type)
  nil)
