;;;; -*- Mode: LISP; Base: 10; Syntax: ANSI-Common-Lisp; Package: CL-USER -*-

;;; 'Top level' system -- responsible for building all the other systems (note that presently, not
;;; all the child systems build at all... these are commented out).

(asdf:defsystem :duim-extended
  :description "Additional stuff for DUIM"
  :depends-on (:duim)
  :components
  ((:module "panes"
	    :components
	    ((:file "package")
	     (:file "border-panes")
	     (:file "spin-box-panes")
	     (:file "progress-control-panes")
	     (:file "tab-control-panes")
	     (:file "list-control-mixins")
	     (:file "list-control-panes")
	     (:file "table-control-panes")
	     (:file "tree-control-mixins")
	     (:file "tree-control-panes")
	     (:file "graph-control-panes")
	     (:file "splitter-panes")
	     (:file "simple-caret")
	     (:file "dialogs"))
	    :serial t)
   (:module "formatting"
	    :components
	    ((:file "package")
	     (:file "formatting-defs")
	     (:file "formatting-macros")
	     (:file "table-formatting")
	     (:file "menu-formatting")
	     (:file "graph-formatting"))
	    :serial t)
   #-(and)
   (:module "presentations"
	    :components
	    ((:file "package")
	     (:file "presentation-defs")
	     (:file "presentation-macros")
	     (:file "presentation-types")
	     (:file "presentation-sheets")
	     (:file "presentation-records")
	     (:file "presentation-translators")
	     (:file "accept")
	     (:file "present")
	     #-(and)
	     (:file "presentation-tests"))
	    :serial t)
   #-(and)
   (:module "gifs"
	    :components
	    ((:file "package")
	     (:file "classes")
	     (:file "lzw")
	     (:file "read-gif"))
	    :serial t)))
